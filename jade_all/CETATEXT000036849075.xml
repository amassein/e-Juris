<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036849075</ID>
<ANCIEN_ID>JG_L_2018_02_000000417267</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/90/CETATEXT000036849075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/02/2018, 417267, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417267</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417267.20180208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'Association des anciens interprètes afghans de l'armée française a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au ministre des armées et à l'ambassadeur de France en Afghanistan de mettre en place des modalités de réception des demandes de protection fonctionnelle des anciens auxiliaires afghans de l'armée française, en ce qui concerne les interprètes, adaptées à leur situation particulière de ressortissants vivant en Afghanistan, notamment en créant une adresse électronique spécifique dédiée à la réception des demandes, dans un délai de 48 heures à compter de la notification de l'ordonnance, sous astreinte de 150 euros par jour de retard, ainsi que d'enjoindre à ces autorités d'effectuer des mesures de publicité des modalités de réception des demandes en précisant les coordonnées de l'adresse électronique dédiée de manière visible sur le site internet du ministère des armées et de l'ambassade de France en Afghanistan et, à titre subsidiaire, d'enjoindre à l'ambassadeur de France en Afghanistan de laisser les anciens auxiliaires afghans de l'armée française, dont font partie les interprètes, accéder à l'ambassade pour y déposer une demande de protection fonctionnelle. Par une ordonnance n° 1719344/9 du 23 décembre 2017, le juge des référés du tribunal administratif de Paris a rejeté sa demande.    <br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 12 et 26 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association des anciens interprètes afghans de l'armée française demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) à défaut, d'ordonner toutes mesures susceptibles de mettre fin aux atteintes graves et manifestement illégales portées au respect de la vie, au droit de ne pas subir des traitements inhumains et dégradants, au droit à la protection fonctionnelle et au droit à un recours effectif des personnes dont elle défend les intérêts ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - ses conclusions sont recevables ;<br/>
              - la condition d'urgence est remplie dès lors que la carence de l'administration française place les anciens interprètes afghans de l'armée française, ainsi que leurs proches, dans une situation de péril imminent pour leur vie ou dans une situation de risque avéré de subir des violences ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit à la protection fonctionnelle, au droit à la vie privée, au droit de ne pas subir de traitements inhumains et dégradants et au droit à un recours effectif des anciens interprètes afghans de l'armée française, en ce que ceux-ci sont mis dans l'impossibilité, d'une part, de déposer utilement une demande de protection fonctionnelle depuis l'Afghanistan du fait de la carence de l'administration à mettre en place un mécanisme de traitement de leur demande de protection fonctionnelle adapté à la situation en Afghanistan et, d'autre part, de saisir la juridiction administrative compétente en cas de refus ; <br/>
              - l'ordonnance contestée est entachée d'erreur de qualification juridique des faits et d'erreur de droit en tant qu'elle concilie les considérations tenant à la sécurité des anciens interprète afghans de l'armée française par l'octroi du bénéfice de la protection fonctionnelle avec des préoccupations relatives aux risques pesant sur la représentation diplomatique française en Afghanistan ;<br/>
              - elle est entachée de contradiction de motifs, d'omission de tirer les conséquences légales de ses constatations et d'erreur de droit en ce qu'elle a relevé, pour écarter la demande, que l'administration ne s'opposait pas à la mesure demandée ;<br/>
              - l'ordonnance contestée est entachée d'erreur de fait en tant qu'elle relève que l'association aurait les moyens humains et financiers lui permettant d'établir et d'expédier par courrier en recommandé avec accusé de réception les demandes de protection fonctionnelle nécessaires aux anciens interprètes afghans de l'armée française.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 janvier 2018, le ministre de l'Europe et des affaires étrangères conclut, d'une part, à l'irrecevabilité des conclusions d'appel tendant à ce qu'il soit enjoint au ministre des armées et à l'ambassadeur de France en Afghanistan d'effectuer des mesures de publicité des modalités de réception des demandes sur le site internet du ministère des armées et de l'ambassade de France en Afghanistan et, d'autre part, au défaut d'urgence et à l'absence d'atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              Par un mémoire en défense, enregistré le 26 janvier 2018, la ministre des armées conclut, d'une part, à l'irrecevabilité des conclusions d'appel tendant à ce qu'il soit enjoint au ministre des armées et à l'ambassadeur de France en Afghanistan d'effectuer des mesures de publicité des modalités de réception des demandes sur le site internet du ministère des armées et de l'ambassade de France en Afghanistan et, d'autre part, au défaut d'urgence et à l'absence d'atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Association des anciens interprètes afghans de l'armée française, d'autre part, le ministre de l'Europe et des affaires étrangères et la ministre des armées ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du mercredi 30 janvier 2018 à 9 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Sureau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Association des anciens interprètes afghans de l'armée française ;<br/>
<br/>
              - le représentant du ministre de l'Europe et des affaires étrangères ;<br/>
<br/>
              - les représentants de la ministre des armées ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au jeudi 1er février à 17 heures, puis, après en avoir informé les parties, à 19 heures.<br/>
<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés le 1er février 2018, par lesquels le ministre de l'Europe et des affaires étrangères conclut, à titre principal, au non-lieu à statuer et, à titre subsidiaire, au rejet de la requête au fond ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 1er février 2018, par lequel la ministre des Armées conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 1er février 2018, par lequel l'Association des anciens interprètes afghans de l'armée française persiste dans ses précédentes écritures ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant que l'Association des anciens interprètes afghans de l'armée française a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre, à la ministre des armées et à l'ambassadeur de France en Afghanistan de mettre en place des modalités de réception des demandes de protection fonctionnelle des anciens auxiliaires afghans de l'armée française, en ce qui concerne les interprètes, adaptées à leur situation particulière de ressortissants vivant en Afghanistan, notamment en créant une adresse électronique spécifique dédiée à la réception des demandes, dans un délai de 48 heures à compter de la notification de l'ordonnance, sous astreinte de 150 euros par jour de retard, ainsi que d'enjoindre à ces autorités d'effectuer des mesures de publicité des modalités de réception des demandes en précisant les coordonnées de l'adresse électronique dédiée de manière visible sur le site internet du ministère des armées et de l'ambassade de France en Afghanistan, et, à titre subsidiaire, d'enjoindre à l'ambassadeur de France en Afghanistan de laisser les anciens interprètes afghans de l'armée française accéder à l'ambassade pour y déposer une demande de protection fonctionnelle ; qu'elle relève appel de l'ordonnance en date du 23 décembre 2017 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa demande ;<br/>
<br/>
              3. Considérant que l'Association des anciens interprètes afghans de l'armée française soutient que ses membres sont exposés à des risques élevés d'atteinte à leur vie ou à leur intégrité physique en raison du concours qu'ils ont accordé à des forces armées étrangères et que le refus, par les autorités compétentes, de les mettre en mesure de déposer une demande de protection fonctionnelle par la voie électronique porte une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
<br/>
              4. Considérant qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte ; que ces mesures doivent en principe présenter un caractère provisoire, sauf lorsqu'aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte ; que le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale ; que, toutefois, le juge des référés ne peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction, et notamment des échanges oraux lors de l'audience, que, si l'Association des anciens interprètes afghans de l'armée française demande que soit mise en place une adresse électronique dédiée à la réception des  demandes de protection fonctionnelle susceptibles d'être présentées par les intéressés, l'octroi d'une telle protection doit, selon l'association, se traduire par la délivrance de visas afin que ceux-ci puissent, une fois admis sur le territoire français, déposer une demande tendant à la reconnaissance de la qualité de réfugié ;<br/>
<br/>
              6. Considérant que, selon les éléments fournis par le ministre de l'Europe et des affaires étrangères et par le ministre des armées, éléments qui ne sont pas contestés, 776 personnels civils de recrutement local ont oeuvré au profit des forces françaises entre 2001 et 2014 ; que deux campagnes d'accueil successives ont été organisées entre 2012 et 2014, puis en 2015, aux termes desquelles 176 d'entre eux ont été accueillis sur le territoire français ; que le ministère des armées a reçu, à ce jour, 45 demandes de protection fonctionnelle, dont 41 émanant de personnes demeurant... ; qu'il a été indiqué lors de l'audience, et confirmé par écrit, qu'à la suite d'une concertation interministérielle, le Gouvernement avait arrêté, à la fin du mois de janvier, le principe d'un réexamen de l'ensemble des dossiers des demandeurs de visa précédemment déboutés, le ministère de l'Europe et des affaires étrangères devant déterminer les modalités selon lesquelles, an cas de réponse positive, les visas pourraient être délivrés, compte tenu de l'impossibilité, à l'heure actuelle, de faire assurer cette mission par l'ambassade de France à Kaboul ;<br/>
<br/>
              7. Considérant, par ailleurs, que les anciens personnels civils de droit local résidant en Afghanistan, s'ils ne peuvent se rendre physiquement dans les locaux de l'ambassade en raison des mesures sécuritaires applicables aux représentations diplomatiques, ne sont pas placés dans l'impossibilité de faire parvenir leur demande par voie postale et de former un recours pour excès de pouvoir contre la décision de refus qui leur serait opposée, soit explicitement, soit à la suite du silence gardé par l'administration sur leur demande ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que l'organisation mise en oeuvre par les services de l'Etat pour traiter les demandes dont il est saisi ne peut, eu égard aux circonstances particulières de l'espèce tenant à la situation en Afghanistan, être regardée comme manifestement illégale ; <br/>
<br/>
              9. Considérant, au surplus, que la mesure consistant à créer une adresse électronique dédiée ne saurait se concevoir sans être assortie de mécanismes hautement sécurisés, dans l'intérêt même des demandeurs et afin de ne pas compromettre les systèmes d'information du ministère des armées ; qu'elle serait, par elle-même, sans incidence sur la durée d'instruction des demandes ; qu'ainsi, elle ne satisfait pas à la condition d'urgence particulière prévue par l'article L. 521-2 du code de justice administrative, qui implique que la mesure sollicitée soit de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale dont la méconnaissance est alléguée ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la requête de l'Association des anciens interprètes afghans de l'armée française doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association des anciens interprètes afghans de l'armée française est rejetée<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association des anciens interprètes afghans de l'armée française, au ministre de l'Europe et des affaires étrangères et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
