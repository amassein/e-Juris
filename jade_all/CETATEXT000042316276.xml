<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042316276</ID>
<ANCIEN_ID>JG_L_2020_09_000000443588</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/31/62/CETATEXT000042316276.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/09/2020, 443588, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443588</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:443588.20200904</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au directeur de l'Office français de l'immigration et de l'intégration (OFII) de rétablir à son profit le bénéfice des conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile, dans un délai de trois jours à compter de la notification de l'ordonnance et sous astreinte de 50 euros par jour de retard. Par une ordonnance n° 2005966 du 12 août 2020, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 1er septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance, dans un délai de sept jours à compter de la notification de la présente ordonnance et sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'OFII la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés s'est fondé sur la circonstance selon laquelle il ne s'était pas présenté à l'aéroport pour justifier le refus de rétablissement des conditions matérielles d'accueil à son profit sans tenir compte ni du caractère sanctionnateur d'un tel retrait, ni des justifications, démarches et explications qu'il avait fournies ; <br/>
              - le juge des référés a considéré qu'il n'établissait pas une situation de vulnérabilité au sens de l'article L. 744-6 du code de l'entrée et du séjour des étrangers et du droit d'asile alors même qu'il justifie avoir subi des sévices et des traitements inhumains et dégradants caractérisant un tel état de vulnérabilité.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive (UE) n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.   Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation familiale. Il incombe au juge des référés d'apprécier, dans chaque situation, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation familiale de la personne intéressée.<br/>
<br/>
              3. Il résulte de l'instruction que M. A..., ressortissant guinéen né le 1er janvier 1999, a vu sa demande d'asile enregistrée le 26 avril 2018 auprès des services de la préfecture des Bouches-du-Rhône et a accepté le même jour les conditions matérielles d'accueil proposées par l'Office français de l'immigration et de l'intégration (OFII). Le préfet des Bouches-du-Rhône a décidé le transfert de M. A... aux autorités espagnoles, responsables de l'examen de sa demande d'asile, et lui a enjoint de se présenter à l'aéroport en vue d'embarquer à destination de Madrid le 27 décembre 2018. M. A... ne s'étant pas présenté à l'embarquement et ayant été déclaré en fuite pour ce motif, l'OFII lui a retiré le bénéfice des conditions matérielles d'accueil par décision du 23 janvier 2019 sur le fondement des dispositions de l'article L. 744-7 du code de l'entrée et du séjour des étrangers et du droit d'asile. A l'expiration du délai de transfert vers l'Espagne, M. A... a fait valoir que la France était devenue responsable de l'examen de sa demande d'asile, qui a été requalifiée en procédure normale le 20 mai 2020. Par une ordonnance du 12 août 2020 dont M. A... relève appel, le juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au directeur de l'OFII de rétablir à son profit le bénéfice des conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile. <br/>
<br/>
              4. Il résulte de l'instruction que l'intéressé, célibataire, âgé de 21 ans, a été en situation de fuite entre le 27 décembre 2018 et le 8 avril 2020, qu'il n'a pas contesté le retrait du bénéfice des conditions matérielles d'accueil prononcé le 23 janvier 2019 et qu'il n'en a demandé le rétablissement qu'au mois de juin 2020. Dans un tel cas, il appartient à l'OFII, pour statuer sur la demande de rétablissement, d'apprécier la situation particulière du demandeur à la date de la demande de rétablissement au regard notamment de sa vulnérabilité, de ses besoins en matière d'accueil ainsi que, le cas échéant, des raisons pour lesquelles il n'a pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil. <br/>
<br/>
              5. Dans les circonstances de l'espèce, d'une part, si l'intéressé fait valoir qu'il a dû être hospitalisé le jour de son départ à destination de l'Espagne, il se borne à reprendre en appel son argumentation de première instance qui, compte tenu de l'ensemble des éléments du dossier, a été écartée à bon droit par le juge des référés du tribunal administratif de Marseille en ce qu'il n'établit pas qu'il se trouvait dans l'incapacité de prendre l'avion en raison d'une pathologie grave et ne fait état, par suite, d'aucune raison susceptible de justifier qu'il n'ait pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil. <br/>
<br/>
              6. D'autre part, s'il fait valoir qu'il se trouve désormais dans une situation de grande vulnérabilité, M. A... n'apporte aucun élément nouveau susceptible d'infirmer l'appréciation du juge des référés du tribunal administratif de Marseille selon laquelle il ne résulte pas de l'instruction que le refus de rétablir les conditions matérielles d'accueil à l'intéressé porterait une atteinte grave et manifestement illégale au droit d'asile à laquelle il appartiendrait au juge des référés, statuant sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de mettre fin. <br/>
<br/>
              7. C'est, dès lors, à bon droit que le juge des référés du tribunal administratif de Marseille a rejeté la demande dont il était saisi. Il y a lieu, par suite, de rejeter l'appel formé par M. A..., y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au ministre de l'intérieur et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
