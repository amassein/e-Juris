<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034076420</ID>
<ANCIEN_ID>JG_L_2017_02_000000386325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/64/CETATEXT000034076420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 22/02/2017, 386325</TITRE>
<DATE_DEC>2017-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:386325.20170222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>M. Claude B...a demandé au tribunal administratif de Nancy, d'une part, d'annuler pour excès de pouvoir la décision du 12 novembre 2012 par laquelle le préfet de Meurthe-et-Moselle a rejeté son recours préalable obligatoire dirigé contre l'arrêté du 28 juin 2012 par lequel ce préfet s'est opposé à la déclaration déposée aux fins de régularisation des travaux conduits pour réaliser un plan d'eau à Aménoncourt (Meurthe-et-Moselle) et, d'autre part, d'enjoindre au préfet de lui délivrer le récépissé de cette déclaration.<br/>
<br/>
              Par un jugement n° 1300069 du 16 juillet 2013, le tribunal administratif DE Nancy a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC01943 du 9 octobre 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 décembre 2014 et 9 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article <br/>
L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du point 3.2.3.0 du tableau figurant à l'article R. 214-1 du code de l'environnement, pris pour l'application des articles L. 214-1 et suivants du même code, la création de plans d'eau, permanents ou non, est soumise à déclaration préalable si leur superficie en est supérieure à 0,1 hectare mais inférieure à trois hectares. Aux termes du point 3.3.1.0 du même tableau, la mise en eau d'une zone humide est soumise à autorisation si la zone mise en eau est d'une surface  supérieure ou égale à un hectare.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., qui est propriétaire, sur le territoire de la commune d'Aménoncourt (Meurthe-et-Moselle), de parcelles boisées, y a conduit, au cours des années 2005 à 2010, des travaux de création d'un plan d'eau d'une superficie de 2,5 hectares, sans déposer ni demande d'autorisation, ni déclaration préalable. Le 2 mai 2012, M. B...a déposé une déclaration préalable pour régulariser la création de ce plan d'eau. Par un arrêté du 28 juin 2012, le préfet de Meurthe-et-Moselle s'est opposé à cette déclaration au double motif que ces travaux avaient eu pour conséquence, d'une part, la destruction d'une zone humide en méconnaissance tant des dispositions de l'article R. 214-1 du code de l'environnement que des orientations du schéma d'aménagement et de gestion des eaux Rhin-Meuse, qui interdisent la création d'étangs dans les zones humides sensibles telles que les têtes de bassin, et, d'autre part, la destruction de l'habitat d'espèces protégées ainsi que de certains spécimens de ces espèces. Pour les mêmes motifs, il a, le 12 novembre 2012, rejeté le recours préalable obligatoire formé, en application de l'article R. 214-36 du code de l'environnement, par M. B...contre cette décision.<br/>
<br/>
              3. Le tribunal administratif de Nancy a rejeté la demande d'annulation formée par M. B...contre cette décision en confirmant le motif selon lequel la réalisation du plan d'eau avait abouti à la destruction de l'habitat d'espèces protégées, sans se prononcer sur l'autre motif de la décision du préfet, tiré de ce qu'il aurait été porté atteinte à une zone humide. Le tribunal a, en effet, estimé qu'il résultait de l'instruction que le préfet aurait pris la même décision s'il n'avait retenu que le motif tiré de ce que le plan d'eau portait atteinte à l'habitat d'espèces protégées. La cour administrative d'appel de Nancy, pour rejeter l'appel de M. B... et valider la décision de rejet du préfet a, quant à elle, retenu que le terrain d'assiette du plan d'eau litigieux était constitutif d'une zone humide et devait, en conséquence, nécessairement faire l'objet d'une procédure d'autorisation, sans examiner l'autre motif de la décision, tiré de ce que la création du plan d'eau portait atteinte à l'habitat d'espèces protégées. La cour a, en effet, estimé qu'il résultait de l'instruction que le préfet aurait pris la même décision s'il n'avait retenu que le motif tiré de l'existence d'une zone humide. <br/>
<br/>
              4. Aux termes de l'article L. 211-1 du code de l'environnement : " I. - Les dispositions des chapitres Ier à VII du présent titre ont pour objet une gestion équilibrée et durable de la ressource en eau ; cette gestion prend en compte les adaptations nécessaires au changement climatique et vise à assurer : / 1° La prévention des inondations et la préservation des écosystèmes aquatiques, des sites et des zones humides ; on entend par zone humide les terrains, exploités ou non, habituellement inondés ou gorgés d'eau douce, salée ou saumâtre de façon permanente ou temporaire ; la végétation, quand elle existe, y est dominée par des plantes hygrophiles pendant au moins une partie de l'année ; / (...) ". Il ressort de ces dispositions, éclairées par les travaux préparatoires de la loi sur l'eau du 3 janvier 1992 dont elles sont issues, qu'une zone humide ne peut être caractérisée, lorsque de la végétation y existe, que par la présence simultanée de sols habituellement inondés ou gorgés d'eau et, pendant au moins une partie de l'année, de plantes hygrophiles.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que, pour juger que le terrain d'assiette du plan d'eau litigieux était constitutif, dans sa totalité, d'une zone humide, la cour a retenu que les études pédologiques menées par un bureau d'études avaient mis en évidence la présence de sols fortement hydromorphes de type " réductisol " et " rédoxisol " ainsi que de traces redoxiques caractérisant des sols moyennement hydromorphes de type " pélosol-rédoxisol " et " luvisol rédoxique ". Elle a regardé comme dépourvue d'incidence la présence, sur le terrain d'assiette du plan d'eau, de pins sylvestres, espèce dont il n'est pas contesté qu'elle ne présente pas un caractère hygrophile, et s'est abstenue de rechercher si d'autres types de végétaux hygrophiles étaient présents sur ce terrain. Elle a, ainsi, regardé comme alternatifs les deux critères d'une zone humide, au sens de l'article L. 211-1 du code de l'environnement, alors que ces deux critères sont cumulatifs, ainsi qu'il a été dit au point 4, contrairement d'ailleurs à ce que retient l'arrêté du 24 juin 2008 précisant les critères de définition des zones humides en application des articles L. 214-7-1 et R. 211-108 du code de l'environnement. Elle a, en conséquence, entaché son arrêt d'erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 9 octobre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre de l'environnement, de l'énergie et de la mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-05 EAUX. GESTION DE LA RESSOURCE EN EAU. - PRÉSERVATION DES ZONES HUMIDES AU SENS DE L'ARTICLE L. 211-1 DU CODE DE L'ENVIRONNEMENT - NOTION - PRÉSENCE SIMULTANÉE DE SOLS HABITUELLEMENT INONDÉS OU GORGÉS D'EAU ET, PENDANT AU MOINS UNE PARTIE DE L'ANNÉE DE PLANTES HYGROPHILES.
</SCT>
<ANA ID="9A"> 27-05 Il ressort des dispositions de l'article L. 211-1 du code de l'environnement, éclairées par les travaux préparatoires de la loi sur l'eau du 3 janvier 1992 dont elles sont issues, qu'une zone humide ne peut être caractérisée, lorsque de la végétation y existe, que par la présence simultanée de sols habituellement inondés ou gorgés d'eau et, pendant au moins une partie de l'année, de plantes hygrophiles.,,,Cour ayant estimé, pour juger que le terrain d'assiette du plan d'eau litigieux était constitutif, dans sa totalité, d'une zone humide, que les études pédologiques menées par un bureau d'études avaient mis en évidence la présence de sols fortement et moyennement hydromorphes, et ayant regardé comme dépourvue d'incidence la présence, sur le terrain d'assiette du plan d'eau, de pins sylvestres, espèce dont il n'est pas contesté qu'elle ne présente pas un caractère hygrophile, tout en s'abstenant de rechercher si d'autres types de végétaux hygrophiles étaient présents sur ce terrain. Erreur de droit à avoir regardés comme alternatifs les deux critères d'une zone humide, au sens de l'article L. 211-1 du code de l'environnement, alors que, ces deux critères sont cumulatifs, contrairement d'ailleurs à ce que retient l'arrêté du 24 juin 2008 précisant les critères de définition des zones humides en application des articles L. 214-7-1 et R. 211-108 du code de l'environnement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
