<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143132</ID>
<ANCIEN_ID>JG_L_2020_07_000000435660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143132.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/07/2020, 435660, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP OHL, VEXLIARD ; SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:435660.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Besançon, en vertu d'une ordonnance du 9 juin 2016 du juge de la mise en état du tribunal de grande instance de Lons-le-Saunier, de déclarer que le sol situé sous le porche soutenant le premier étage de sa maison d'habitation, sur la parcelle cadastrée AB n° 150 de la commune d'Arbois (Jura), n'appartenait pas au domaine public de cette commune. Par un jugement n° 1601101 du 26 avril 2018, ce tribunal a fait droit à cette demande. <br/>
<br/>
              Par une ordonnance n° 18NC01802 du 25 octobre 2019, enregistrée le 30 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, sur le fondement de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 22 juin 2018 au greffe de cette cour, présenté par la commune d'Arbois contre ce jugement.  <br/>
<br/>
              Par ce pourvoi et un mémoire complémentaire, enregistré le 26 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune d'Arbois demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de surseoir à statuer jusqu'à ce que la juridiction judiciaire se soit prononcée sur sa demande tendant à ce que soit reconnue sa propriété par usucapion ;<br/>
<br/>
              3°) réglant l'affaire au fond, de déclarer que le passage public, situé sur la parcelle AB  150 sous le proche de l'habitation de M. A..., est affecté à l'usage du public et appartient au domaine public communal ;<br/>
<br/>
              4°) de mettre à la charge de M. A... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ohl, Vexliard, avocat de la commune d'Arbois et à la SCP Leduc, Vigand, avocat de M. A... ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A... est propriétaire d'un immeuble situé au 16, grande rue, à Arbois (Jura), implanté sur la parcelle cadastrée AB n° 150, composé de caves, de deux pièces en rez-de-chaussée ainsi que de deux étages soutenus par un porche situé en surplomb d'une portion d'une voie dénommée passage Saint-Vernier. Par acte du 29 juillet 2014, M. A... a assigné la commune d'Arbois devant le tribunal de grande instance de Lons-le-Saunier afin de faire constater l'absence de droit de passage sous le porche de son habitation, qu'il souhaitait clore par des grilles. La commune d'Arbois a, pour sa part, saisi ce tribunal d'une demande reconventionnelle tendant à ce que sa propriété sur le passage soit reconnue par prescription acquisitive, sur le fondement des dispositions de l'article 2261 du code civil. Par une ordonnance du 9 juin 2016, le juge de la mise en état de ce tribunal a sursis à statuer et invité M. A... à saisir le tribunal administratif de Besançon de la question de savoir si le porche litigieux appartient au domaine public de la commune. Par un jugement du 26 avril 2018, contre lequel la commune se pourvoit en cassation, ce tribunal administratif a déclaré que la portion du passage Saint-Vernier située sur la parcelle AB n° 150 sous le porche de l'habitation de M. A... n'appartenait pas au domaine public de la commune d'Arbois dès lors que cette commune n'en était pas propriétaire. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que la question de savoir qui était propriétaire de la portion de voie située sous le porche de l'habitation de M. A... faisait l'objet d'un débat entre les parties, ce dernier, d'une part, se prévalant de la propriété du sol résultant d'un acte de vente du 19 juin 1957, la commune d'Arbois, d'autre part, demandant que soit reconnu le fait qu'elle avait acquis la propriété de ce même bien par usucapion. En statuant sur cette question, qui relevait de la seule compétence des juridictions de l'ordre judiciaire, alors qu'elle soulevait en l'espèce une difficulté sérieuse, le tribunal administratif de Besançon a méconnu les règles de répartition de compétence entre les ordres de juridiction. <br/>
<br/>
              3. Dès lors, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la commune d'Arbois est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de l'instruction que le passage Saint-Vernier est ouvert au public au moins depuis la Libération et permet d'assurer la continuité du cheminement des piétons depuis le trottoir bordant la Grand'rue, et notamment des élèves qui se rendent au collège Louis Pasteur ou des visiteurs du musée situé au sein du château Pécauld. Il résulte également de l'instruction du dossier que la commune a fait procéder à ses frais, en 1993, à des travaux de réfection du revêtement du sol de ce passage, lequel fait l'objet d'un entretien régulier par les services municipaux et est pourvu de dispositifs d'éclairage public dont le coût est supporté par la commune, ainsi que d'une signalisation réservant son accès aux seuls piétons. La fraction en litige du passage Saint-Vernier est donc affectée à l'usage direct du public. Elle appartient, en conséquence, au domaine public de la commune, sous réserve que cette dernière en soit propriétaire.<br/>
<br/>
              6. Il résulte de tout ce qui précède qu'il doit être répondu à la question préjudicielle adressée par le juge de la mise en l'état du tribunal de grande instance de Lons-le-Saunier que, sous réserve que la commune d'Arbois en soit bien propriétaire, l'assiette de la partie du passage Saint-Vernier située sur la parcelle AB n° 150 et sous le porche de l'habitation de M. A... appartient au domaine public de cette commune. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 2 000 euros à verser à la commune d'Arbois en application des dispositions de l'article L. 761-1 du code justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette commune, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Besançon du 26 avril 2018 est annulé.<br/>
Article 2 : Il doit être répondu à la question préjudicielle adressée au tribunal administratif de Besançon par le juge de la mise en l'état du tribunal de grande instance de Lons-le-Saunier que, sous réserve que la commune d'Arbois en soit bien propriétaire, l'assiette de la partie du passage Saint-Vernier située sur la parcelle AB n° 150 et sous le porche de l'habitation de M. A... appartient au domaine public de cette commune.<br/>
<br/>
Article 3 : M. A... versera à la commune d'Arbois une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. A... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au président du tribunal judiciaire de Lons-le-Saunier, à la commune d'Arbois et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
