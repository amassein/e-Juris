<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791241</ID>
<ANCIEN_ID>JG_L_2018_04_000000415212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 11/04/2018, 415212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415212.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation et un déféré, M. A...B...et le haut-commissaire de la République en Polynésie française ont demandé au tribunal administratif de la Polynésie française d'annuler l'élection de M. C...D...en qualité de deuxième vice-président du comité du syndicat pour l'électrification des communes du sud de Tahiti (SECOSUD) à l'issue de l'élection qui s'est déroulée le 12 juillet 2017 et de proclamer élu M. A...B.... Par un jugement n°s 1700289, 1700293 du 25 septembre 2017, le tribunal administratif de la Polynésie française, après avoir joint cette protestation et ce déféré, a fait droit à leurs demandes. <br/>
<br/>
              Par une requête, enregistrée le 24 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a proclamé élu M. A...B...en qualité de deuxième vice-président du comité du SECOSUD ;<br/>
<br/>
              2°) d'enjoindre au SECOSUD de procéder à une nouvelle élection ;<br/>
<br/>
              3°) de mettre à la charge de M. A...B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu :<br/>
            - le code électoral ;<br/>
            - le code général des collectivités territoriales ;<br/>
            - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction qu'à l'issue du premier tour de l'élection du 2ème vice-président du comité du syndicat pour l'électrification des communes du sud de Tahiti, qui s'est déroulé le 12 juillet 2017, sur un total de huit suffrages exprimés, quatre se sont portés sur la candidature de M.B..., deux sur celle de M. D... et deux bulletins étaient blancs. Il a été procédé à un deuxième, puis à un troisième tour de scrutin, à l'issue duquel M. D... a été proclamé élu au bénéfice de l'âge. Par un jugement du 25 septembre 2017, le tribunal administratif de la Polynésie française a fait droit à la protestation de M. A...B...et au déféré du haut-commissaire de la République en Polynésie française, dirigés contre l'élection de M. C...D..., en annulant l'élection de ce dernier et en proclamant M. B...élu. M. D...relève appel de ce jugement en tant qu'il a proclamé M. B... élu en qualité de deuxième vice-président du comité du syndicat pour l'électrification des communes du sud de Tahiti.<br/>
<br/>
              2. Aux termes de l'article L. 5211-2 du code général des collectivités territoriales, applicable à l'élection des présidents et vice-présidents des établissements publics de coopération intercommunale de Polynésie française : " A l'exception de celles des deuxième à quatrième alinéas de l'article L. 2122-4, les dispositions du chapitre II du titre II du livre Ier de la deuxième partie relatives au maire et aux adjoints sont applicables au président et aux membres du bureau des établissements publics de coopération intercommunale, en tant qu'elles ne sont pas contraires aux dispositions du présent titre ". Aux termes de l'article L. 2122-7 du même code : " Le maire est élu au scrutin secret et à la majorité absolue. Si, après deux tours de scrutin, aucun candidat n'a obtenu la majorité absolue, il est procédé à un troisième tour de scrutin et l'élection a lieu à la majorité relative. En cas d'égalité de suffrages, le plus âgé est déclaré élu ". <br/>
<br/>
              3. Il est constant et n'est plus contesté par aucune des parties au litige qu'une erreur a été commise dans la fixation du nombre de suffrages nécessaires pour atteindre la majorité absolue qui était égale à la moitié des suffrages exprimés plus un et s'élevait ainsi à quatre voix à l'issue du premier tour. Il s'en suit que c'est à tort qu'il a été procédé à un deuxième, puis à un troisième tour de scrutin, à l'issue duquel M. D...a été proclamé élu au bénéfice de l'âge, alors que M. B... aurait dû être proclamé élu dès le premier tour. Compte tenu de cette erreur matérielle, dépourvue d'incidence sur la sincérité du vote des électeurs au premier tour, et eu égard à l'office du juge de l'élection, qui peut proclamer élu un candidat aux lieu et place de celui dont il annule l'élection, c'est à bon droit que le tribunal administratif de la Polynésie française, qui n'a pas  substitué son appréciation à celle des électeurs en rectifiant le nombre de voix nécessaires pour atteindre la majorité absolue, a proclamé M. B... élu. <br/>
<br/>
              4. Il résulte de qui précède que M. D...n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de la Polynésie française a proclamé M. B...élu en qualité de deuxième vice-président du comité du syndicat pour l'électrification des communes du sud de Tahiti.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. D...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C...D..., à M. A...B...et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
