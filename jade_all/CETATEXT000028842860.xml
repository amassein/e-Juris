<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842860</ID>
<ANCIEN_ID>JG_L_2014_04_000000360081</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 11/04/2014, 360081, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360081</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:360081.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 juin et 2 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Groupe Mendi Promotion, demeurant ... ; la société Groupe Mendi Promotion demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX00981 du 10 avril 2012 par lequel la cour administrative d'appel de Bordeaux, sur la requête de la commune de Dax, réformant le jugement n° 0902102 du 8 mars 2011 du tribunal administratif de Pau ayant condamné cette commune à verser à la société requérante la somme de 292 155,89 euros augmentée des intérêts légaux à compter du 20 juillet 2009, en réparation des préjudices résultant d'un refus de permis de construire illégal, a ramené à 87 895,53 euros la somme que la commune a été condamnée à verser à la société requérante ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de la commune de Dax et de faire droit à ses conclusions d'appel et de première instance ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Dax  la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative, ainsi que celle correspondant à la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la société Groupe Mendi Promotion et à la SCP Baraduc, Duhamel, Rameix, avocat de la commune de Dax ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par un jugement du 8 mars 2011, le tribunal administratif de Pau a condamné la commune de Dax à verser à la société Groupe Mendi Promotion la somme de 292 155,89 euros, augmentée des intérêts légaux à compter du 20 juillet 2009, en réparation des préjudices subis du fait de l'illégalité de l'arrêté du 25 juillet 2006 par lequel le maire de Dax a refusé la délivrance du  permis de construire sollicité par cette société ; que celle-ci se pourvoit en cassation contre l'arrêt du 10 avril 2012 par lequel la cour administrative d'appel de Bordeaux a ramené à 87 895,53 euros la somme que la commune est condamnée à verser à la société requérante ; <br/>
<br/>
              Sur la responsabilité pour faute de la commune de Dax :<br/>
<br/>
              2. Considérant que l'arrêt attaqué a retenu que la responsabilité de la commune de Dax était engagée à l'égard de la société Groupe Mendi Promotion du fait de l'illégalité de l'arrêté du 25 juillet 2006  du maire de Dax, mentionné ci-dessus ; que la cour n'a pas inexactement qualifié les faits soumis à son examen ni commis d'erreur de droit en jugeant qu'en dépit des nombreuses réunions préparatoires auxquelles la société Groupe Mendi Promotion avait participé à la mairie après le dépôt de sa demande de permis de construire, il ne résultait pas de l'instruction que la commune de Dax l'aurait incitée à poursuivre la réalisation de son projet ou pris à son égard des engagements qu'elle n'aurait pas tenus et  aurait ainsi commis une faute de nature à engager également sa responsabilité sur ce fondement ; <br/>
<br/>
              Sur l'indemnisation du préjudice subi :<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des énonciations de l'arrêt attaqué que, contrairement à ce qui est soutenu, pour écarter du montant du préjudice subi par la société requérante la somme de 204 260,36 euros, correspondant au solde des frais d'honoraires de l'architecte ayant établi les plans nécessaires au dépôt du permis de construire, la cour administrative d'appel de Bordeaux ne s'est pas fondée sur la seule circonstance que la société Groupe Mendi Promotion n'avait pas démontré avoir effectivement versé cette somme à l'architecte, mais a également relevé que la société n'établissait pas être débitrice envers son architecte de cette somme, qui correspondait à des missions dont il n'était pas justifié qu'elles auraient été remplies par l'architecte en vue du dépôt de la demande de permis de construire ; que, par suite, la cour qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que contrairement à ce que soutient la société requérante, en estimant que la seule production de la " note d'honoraire n°3 " n'établissait pas l'exigibilité des sommes dues au titre des missions remplies par l'architecte mentionnées ci-dessus, la cour n'a pas entaché son arrêt, qui est suffisamment motivé sur ce point, d'une contradiction de motifs, ni dénaturé les pièces du dossier qui étaient soumises à son examen ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en jugeant, par adoption d'un motif retenu par les premiers juges, pour rejeter la demande d'indemnisation de la société Groupe Mendi Promotion relatives aux frais de déplacement et de dossier que la société aurait exposés en vain du fait du refus de permis de construire, qu'elle ne contestait pas le fait d'avoir justifié de la réalité de ces frais par un tableau déclaratif des déplacements qui auraient été à sa charge, la cour n'a pas commis d'erreur de droit, ni de dénaturation des pièces du dossier et a suffisamment motivé son arrêt sur ce point; <br/>
<br/>
              6. Considérant, en quatrième lieu, qu'en estimant que la société Groupe Mendi Promotion n'était pas fondée à demander le remboursement des frais de notaire exposés lors de l'établissement du compromis de vente du terrain d'assiette du projet de construction litigieux, signé le 24 octobre 2003, et de l'avenant en date du 6 août 2005 prolongeant la durée de validité de ce compromis, au motif que ces documents ne faisaient apparaître la société Groupe Mendi Promotion, ni comme signataire du compromis, ni même comme étant représentée par la personne signataire de cet acte, la cour n'a pas dénaturé les pièces du dossier, ni commis d'erreur de droit ;<br/>
<br/>
              7. Considérant, en cinquième lieu, qu'en jugeant que le préjudice moral résultant de l'atteinte portée à sa réputation commerciale et le préjudice financier, invoqués par la société requérante au titre de l'impossibilité pour elle de commercialiser les appartements projetés sur ce terrain ayant fait l'objet du refus illégal de permis de construire, avait un caractère incertain, au motif qu'elle n'établissait pas qu'elle serait venue aux droits des signataires des actes notariés mentionnés au point 7 et qu'elle aurait eu un titre l'autorisant à construire sur le terrain d'assiette du projet à la date de l'arrêté annulé, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation, qui n'est pas susceptible d'être contestée devant le juge de cassation, et n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Groupe Mendi Promotion doit être rejeté ; <br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge du Groupe Mendi Promotion ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Dax qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Groupe Mendi Promotion la somme de 3 000 euros à verser à la commune de Dax, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Groupe Mendi Promotion est rejeté. <br/>
Article 2 : La société Groupe Mendi Promotion versera une somme de 3 000 euros à la commune de Dax au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Groupe Mendi Promotion et à la commune de Dax.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
