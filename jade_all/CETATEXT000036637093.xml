<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036637093</ID>
<ANCIEN_ID>JG_L_2018_02_000000402114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/70/CETATEXT000036637093.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 21/02/2018, 402114, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402114.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile de construction vente (SCCV) Les Balcons de l'Arly a saisi la cour administrative d'appel de Lyon d'une demande d'exécution du jugement n° 1205907 du 4 juillet 2014 par lequel le tribunal administratif de Grenoble a, d'une part, annulé l'arrêté du 17 septembre 2012 par lequel le maire de Crest-Voland a opposé un sursis à statuer à sa demande tendant à obtenir un permis de construire modificatif du permis initialement délivré le 12 avril 2007, d'autre part, enjoint au maire de cette commune de statuer à nouveau sur la demande de permis de construire modificatif, dans un délai de deux mois à compter de la notification du jugement. Par un arrêt n° 15LY01189 du 3 août 2015, la cour administrative d'appel a prononcé une astreinte à l'encontre de la commune de Crest-Voland si elle ne justifiait pas, dans le délai d'un mois suivant la notification de son arrêt, que son maire avait pris une nouvelle décision sur la demande de permis de construire modificatif présentée par la SCCV Les Balcons de l'Arly, en exécution du jugement du 4 juillet 2014 du tribunal administratif de Grenoble.<br/>
<br/>
              Par un arrêt n° 15LY01189 du 14 juin 2016, la cour administrative d'appel de Lyon a procédé à la liquidation de l'astreinte prononcée, pour la période du 6 août 2015 au 14 juin 2016, et ordonné en conséquence que la commune de Crest-Voland verse la somme de 6 260 euros à la SCCV Les Balcons de l'Arly et la somme de 25 040 euros à l'Etat en application des dispositions des articles L. 911-7 et L. 911-8 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 août et 21 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Crest-Voland demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la SCCV Les Balcons de l'Arly la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la commune de Crest-Voland ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Par un arrêt du 3 août 2015, la cour administrative d'appel de Lyon a prononcé une astreinte à l'encontre de la commune de Crest-Voland si elle ne justifiait pas, dans le délai d'un mois suivant la notification de l'arrêt, que son maire avait pris une nouvelle décision sur la demande de permis de construire modificatif présentée par la société civile de construction vente (SCCV) Les Balcons de l'Arly, en exécution du jugement du 4 juillet 2014 du tribunal administratif de Grenoble. Par un arrêt du 14 juin 2016, contre lequel la commune de Crest-Voland se pourvoit en cassation, la cour administrative d'appel de Lyon a procédé à la liquidation de cette astreinte. <br/>
<br/>
              2.	D'une part, aux termes de l'article L. 5 du code de justice administrative en vigueur à la date de l'arrêt attaqué : " L'instruction des affaires est contradictoire. Les exigences de la contradiction sont adaptées à celles de l'urgence. " Aux termes de l'article R. 611-1 du même code : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux. ".<br/>
<br/>
              3.	D'autre part, aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / (...) / Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte. (...). " Aux termes de l'article L. 911-7 du même code : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée. / (...) / Elle peut modérer ou supprimer l'astreinte provisoire, même en cas d'inexécution constatée. ".<br/>
<br/>
              4.	Il résulte de ces dispositions que la décision par laquelle la juridiction ayant prononcé une astreinte provisoire statue sur sa liquidation présente un caractère juridictionnel et doit, par suite, respecter les exigences du caractère contradictoire de la procédure.<br/>
<br/>
              5.	Il ressort des pièces du dossier soumis aux juges du fond que, par les mémoires présentés les 23 octobre et 14 décembre 2015, la commune de Crest-Voland soutenait que la SCCV Les Balcons de l'Arly avait, dans une convention d'objectifs conclue avec la commune postérieurement au prononcé de l'astreinte, renoncé au paiement de celle-ci. Dans le mémoire qu'elle a produit devant la cour administrative d'appel le 6 janvier 2016, la SCCV Les Balcons de l'Arly contestait avoir, par la convention d'objectifs invoquée, renoncé au paiement de l'astreinte. Ce mémoire, qui comportait des éléments nouveaux, devait être communiqué à la commune de Crest-Voland en application des dispositions du dernier alinéa de l'article R. 611-1 du code de justice administrative citées au point 2. Il ressort de l'arrêt attaqué que le mémoire n'a pas été communiqué à la commune. Par suite, la cour administrative d'appel de Lyon a méconnu le caractère contradictoire de la procédure. Cette irrégularité entache l'ensemble de l'arrêt, qui doit dès lors, sans qu'il soit besoin de se prononcer sur les autres moyens soulevés par la commune, être annulé.<br/>
<br/>
              6.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Crest-Voland au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 15LY01189 du 14 juin 2016 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Crest-Voland au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Crest-Voland et à la société civile de construction vente Les Balcons de l'Arly.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
