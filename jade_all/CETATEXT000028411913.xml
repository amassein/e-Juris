<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411913</ID>
<ANCIEN_ID>JG_L_2013_12_000000368941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/19/CETATEXT000028411913.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/12/2013, 368941, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368941.20131226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montreuil :<br/>
              - d'annuler pour excès de pouvoir les arrêtés des 23 mai et 8 juin 2005 par lesquels l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale de la Seine-Saint-Denis, l'a placée en congé de longue durée à demi-traitement du 17 décembre 2004 au 16 décembre 2005 ;<br/>
              - de condamner l'Etat à lui verser la différence entre le salaire à plein traitement et celui à demi-traitement pour la période du 17 décembre 2004 au 16 décembre 2006 avec intérêts capitalisés, sous astreinte de 150 euros par jour de retard ;<br/>
              - d'enjoindre à l'Etat de reconstituer sa carrière en ce qui concerne son accès à la hors-classe de professeur des écoles et les incidences de l'illégalité commise sur le calcul de ses droits à pension à compter du 2 septembre 2008 ;<br/>
              - de condamner l'Etat à lui verser une somme de 16 000 euros en réparation de son préjudice moral, de la perte de chance et des troubles dans les conditions d'existence.<br/>
<br/>
              Par un jugement n° 1011990 du 27 octobre 2011, le tribunal administratif de Montreuil a annulé l'arrêté de l'inspecteur d'académie du 8 juin 2005 en tant qu'il a un effet rétroactif et rejeté le surplus des conclusions de la demande de MmeB....<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un arrêt n° 11VE04300 du 18 avril 2013, enregistré le 30 mai 2013 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et les mémoires, enregistrés les 29 décembre 2011, 7 février 2012 et 28 septembre 2012 au greffe de cette cour, présentés par MmeB....<br/>
<br/>
              Par cette requête, Mme B...demande au juge administratif : <br/>
<br/>
              1°) d'annuler le jugement n° 1011990 du tribunal administratif de Montreuil du 27 octobre 2011 en tant qu'il n'a fait que partiellement droit à ses conclusions ;<br/>
<br/>
              2°) de condamner l'Etat à lui verser la différence entre le salaire à plein traitement et celui à demi-traitement pour la période du 17 décembre 2004 au 16 décembre 2006 avec intérêts capitalisés, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) d'enjoindre à l'Etat de reconstituer sa carrière en ce qui concerne son accès à la hors-classe de professeur des écoles et les incidences de l'illégalité commise sur le calcul de ses droits à pension à compter du 2 septembre 2008 ;<br/>
<br/>
              4°) de condamner l'Etat à lui verser une somme de 16 000 euros en réparation de son préjudice moral, de la perte de chance et des troubles dans les conditions d'existence ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 2 035 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire enregistré le 7 février 2012 au greffe de la cour administrative d'appel de Versailles, Mme B...déclare se désister de ses conclusions indemnitaires.<br/>
<br/>
              Vu :<br/>
- les autres pièces du dossier ;<br/>
- le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code. L'article R. 222-14 fixe ce montant à 10 000 euros et l'article R. 222-15 précise que celui-ci est déterminé par la valeur totale des sommes demandées dans la requête introductive d'instance.<br/>
<br/>
              2. Mme B...a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir les arrêtés des 23 mai et 8 juin 2005 la plaçant en congé de longue durée à demi-traitement du 17 décembre 2004 au 16 décembre 2005, d'enjoindre à l'Etat de reconstituer sa carrière et de condamner l'Etat à lui verser la différence entre le salaire à plein traitement et celui à demi-traitement pour la période du 17 décembre 2004 au 16 décembre 2006, ainsi qu'une somme de 16 000 euros en réparation de divers préjudices qu'elle estime avoir subis. Par application des dispositions précédemment mentionnées du code de justice administrative, ce litige est susceptible d'appel sans qu'y fasse obstacle la circonstance que Mme B...se soit désistée, en cours d'instance, de ses conclusions indemnitaires. Il y a lieu, par suite, d'attribuer à la cour administrative d'appel de Versailles le jugement des conclusions de la requête de Mme B... tendant à l'annulation du jugement du tribunal administratif de Montreuil en tant qu'il n'a fait que partiellement droit à ses conclusions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de Mme B...est attribué à la cour administrative d'appel de Versailles.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B..., au ministre de l'éducation nationale et au président de la cour administrative d'appel de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
