<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028886084</ID>
<ANCIEN_ID>JG_L_2014_04_000000363345</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/88/60/CETATEXT000028886084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 30/04/2014, 363345, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363345</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:363345.20140430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>	VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société par actions simplifiée (SAS) Distribution Guy Degrenne, dont le siège est situé à Vire (14500), a demandé au tribunal administratif de Caen de prononcer la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2006 à 2008 dans les rôles de la commune de Vire. Par un jugement n° 1001445 du 3 mai 2011, le tribunal administratif de Caen a déchargé la société de la cotisation supplémentaire de taxe professionnelle à laquelle elle a été assujettie au titre de l'année 2006 et rejeté le surplus des conclusions relatif aux années 2007 et 2008.<br/>
<br/>
              Par un arrêt n° 11NT01808 du 27 septembre 2012, la cour administrative d'appel de Nantes a fait droit à l'appel formé par la SAS Distribution Guy Degrenne et accordé la décharge des cotisations supplémentaires de taxe professionnelle auxquelles la société a été assujettie au titre des années 2007 et 2008.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un mémoire en réplique enregistrés les 11 octobre 2012 et 30 août 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, demande au Conseil d'Etat d'annuler l'arrêt n° 11NT01808 du 27 septembre 2012 de la cour administrative d'appel de Nantes.<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la SAS Distribution Guy Degrenne ;<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " La taxe professionnelle a pour base : / 1° Dans le cas des contribuables autres que ceux visés au 2° : / a. la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de celles qui ont été détruites ou cédées au cours de la même période (...) ". Aux termes de l'article 1469 du même code, dans sa rédaction alors applicable : " La valeur locative est déterminée comme suit: / (...) 3° quater. Le prix de revient d'un bien cédé n'est pas modifié lorsque ce bien est rattaché au même établissement avant et après la cession et lorsque, directement ou indirectement : / a. l'entreprise cessionnaire contrôle l'entreprise cédante ou est contrôlée par elle ; / b. ou ces deux entreprises sont contrôlées par la même entreprise (...) ".<br/>
<br/>
              2. Il résulte des termes mêmes des dispositions précitées du 3° quater de l'article 1469 du code général des impôts qu'ainsi que l'a relevé l'arrêt attaqué, les cessions de biens qu'elles visent s'entendent des seuls transferts de propriété consentis entre un cédant et un cessionnaire. Ces dispositions, dont les termes renvoient à une opération définie et régie par le droit civil, ne sauraient s'entendre comme incluant toutes autres opérations qui, sans constituer des " cessions " proprement dites, ont pour conséquence une mutation patrimoniale. Cependant, la notion de cession au sens du droit civil recouvre tous les transferts de propriété consentis entre un cédant et un cessionnaire, effectués à titre gratuit ou à titre onéreux.<br/>
<br/>
              3. Par suite, en jugeant que l'opération par laquelle une société apporte une partie de ses éléments d'actif à une autre société et reçoit en contrepartie des droits sociaux de la société bénéficiaire de l'apport ne peut, eu égard à la nature de cette contrepartie qui associe l'apporteur aux aléas de la société bénéficiaire et ne constitue pas un prix, être regardée comme une cession au sens du droit civil, pour en déduire que les apports partiels d'actifs n'entrent pas dans les prévisions du 3° quater de l'article 1469 du code général des impôts, la cour administrative d'appel de Nantes a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, est fondé à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt n° 11NT01808 du 27 septembre 2012 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions de la SAS Distribution Guy Degrenne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SAS Distribution Guy Degrenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
