<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038145145</ID>
<ANCIEN_ID>JG_L_2019_02_000000419939</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/14/51/CETATEXT000038145145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 08/02/2019, 419939, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419939</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419939.20190208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 18 avril et 18 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. C...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 26 juillet 2017 rapportant le décret du 4 décembre 2013 en ce qu'il lui avait accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ". <br/>
<br/>
              2.	Il ressort des pièces de dossier que M.A..., ressortissant marocain, a déposé une demande de naturalisation le 13 décembre 2011, par laquelle il a indiqué être divorcé. Au vu de ses déclarations, il a été naturalisé par décret du 4 décembre 2013. Toutefois, par bordereau reçu le 27 juillet 2015, le ministre des affaires étrangères et du développement international a informé le ministre chargé des naturalisations que M. A...avait épousé au Maroc, le 15 août 2013, une ressortissante marocaine résidant au Maroc. Par décret du 26 juillet 2017, le Premier ministre a rapporté le décret du 4  décembre 2013 prononçant la naturalisation de M. A...au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé sur sa situation familiale. M. A...demande l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              3.	En premier lieu, le délai de deux ans imparti par l'article 27-2 du code civil pour rapporter le décret de naturalisation de M. A...a commencé de courir à la date à laquelle la réalité de la situation familiale de l'intéressé a été portée à la connaissance du ministre chargé des naturalisations. <br/>
<br/>
              4.	A cet égard, il ressort des pièces du dossier que les services du ministre chargé des naturalisations n'ont été informés de la réalité de la situation familiale du requérant que le 27 juillet 2015, date à laquelle ils ont reçu les documents relatifs au mariage de l'intéressé transmis par bordereau du ministre des affaires étrangères. La circonstance que M. A...ait, antérieurement, déposé une demande de regroupement familial n'est pas de nature à établir que son mariage aurait été porté à la connaissance des services du ministre chargé des naturalisations à une date antérieure au 27 juillet 2015. Dans ces conditions, le décret attaqué, qui a été signé le 26 juillet 2017, a été pris avant l'expiration du délai de deux ans prévu par les dispositions de l'article 27-2 du code civil.<br/>
<br/>
              5.	En deuxième lieu, l'article 21-16 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressé ait dissimulé s'être marié au Maroc avec une ressortissante marocaine était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts. <br/>
<br/>
              6.	En troisième lieu, il ressort des pièces du dossier que M. A...s'est marié le 15 août 2013 à Casablanca avec une ressortissante marocaine résidant au Maroc. Ce mariage a constitué un changement de sa situation personnelle et familiale que l'intéressé aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, comme il s'y était engagé en déposant sa demande de naturalisation, ce qu'il n'a pas fait avant que ne lui soit accordée la nationalité française. Si M. A...soutient qu'il était de bonne foi et qu'il a sollicité lui-même une demande de regroupement familial en faveur de son épouse auprès de l'Office français de l'immigration et de l'intégration, il ne fait état d'aucune circonstance qui l'aurait mis dans l'impossibilité de faire part de son changement de situation familiale au service chargé de l'instruction de sa demande de naturalisation avant l'intervention du décret lui accordant la nationalité française. L'intéressé, qui maîtrise la langue française, ainsi qu'il ressort du procès-verbal d'assimilation du 4 juillet 2012, ne pouvait se méprendre sur la teneur de l'engagement qu'il avait pris sur l'honneur en déposant sa demande de naturalisation. Dans ces conditions, M. A...doit être regardé comme ayant volontairement dissimulé le changement de sa situation familiale. Par suite, en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude, le ministre d'Etat, ministre de l'intérieur, n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil. <br/>
<br/>
              7.	En quatrième lieu, un décret qui rapporte pour fraude un décret de naturalisation est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M. A... garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              8.	Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
