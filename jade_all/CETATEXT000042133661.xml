<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042133661</ID>
<ANCIEN_ID>JG_L_2020_07_000000434019</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/13/36/CETATEXT000042133661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 10/07/2020, 434019, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434019</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434019.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Besançon, sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du 12 juin 2019 par laquelle la présidente du centre communal d'action sociale (CCAS) de la commune de Valdoie a prononcé son licenciement et d'enjoindre au CCAS, dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir et sous astreinte de 50 euros par jour de retard, de prononcer sa réintégration. Par une ordonnance n° 1901259 du 13 août 2019, le juge des référés du tribunal administratif de Besançon a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 28 août et 13 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du CCAS de la commune de Valdoie la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 83-634 du 11 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Buk Lament - Robillot, avocat de Mme A... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune de Valdoie ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que Mme B... A... a été recrutée par le centre communal d'action sociale (CCAS) de la commune de Valdoie pour exercer les fonctions d'assistance maternelle au sein du service d'accueil familial à domicile et était titulaire, en dernier lieu, d'un contrat à durée indéterminée prenant effet le 1er janvier 2014. Par une délibération du 3 juin 2019, le conseil d'administration du CCAS a décidé de supprimer ce service d'accueil familial à domicile en indiquant, dans les motifs de cette délibération, que cette réorganisation entraînerait une procédure de licenciement de six agents titulaires d'un contrat à durée indéterminée et que le contrat à durée déterminée d'un agent ne serait pas renouvelé. Après avoir convoqué Mme A... à un entretien préalable de licenciement le 7 juin 2019, la présidente du CCAS a décidé, le 12 juin 2019, de prononcer son licenciement à compter du 12 août 2019 sur le fondement des dispositions combinées de l'article L. 422-1 et L. 423-10 du code de l'action sociale et des familles. Mme A... se pourvoit en cassation contre l'ordonnance du 13 août 2019 par laquelle le juge des référés du tribunal administratif de Besançon a rejeté sa demande tendant à la suspension de l'exécution de cette décision de licenciement et à ce qu'il soit enjoint au CCAS, dans un délai de quinze jours à compter de la notification de l'ordonnance et sous astreinte de 50 euros par jour de retard, de prononcer sa réintégration.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. En vertu d'un principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés dont l'emploi est supprimé que les règles du statut général de la fonction publique, qui imposent de donner, dans un délai raisonnable, aux fonctionnaires en activité dont l'emploi est supprimé une nouvelle affectation correspondant à leur grade, il incombe à l'administration, avant de pouvoir prononcer le licenciement d'un agent contractuel recruté en vertu d'un contrat à durée indéterminée, motivé par la suppression dans le cadre d'une réorganisation du service de l'emploi qu'il occupait, de proposer à l'intéressé un emploi de niveau équivalent ou, à défaut d'un tel emploi et si l'intéressé le demande, de tout autre emploi et, en cas d'impossibilité, de prononcer le licenciement dans les conditions qui lui sont applicables. Ce principe général du droit s'applique aux assistants maternels, qui sont des agents de droit public, recrutés en vertu d'un contrat à durée indéterminée en application des articles L. 422-1 à L. 422-8, L. 423-3 et R. 422-1 du code de l'action sociale et des familles.<br/>
<br/>
              4. En jugeant qu'en raison du caractère spécifique, mentionné à l'article L. 422-6 du code de l'action sociale et des familles, de l'activité d'un assistant maternel et du régime juridique particulier attaché à cet emploi, la mise en oeuvre de ce principe général du droit implique seulement que l'administration propose à cet agent, s'il existe et est susceptible d'être vacant, un emploi de même nature au sein de la collectivité, le juge des référés a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ordonnance attaquée doit être annulée.<br/>
<br/>
              5. Il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              6. Premièrement, si Mme A... soutient que la présidente du CCAS n'était pas compétente pour supprimer les emplois d'assistantes maternelles et que, faute d'une suppression décidée par le conseil d'administration, son licenciement ne pouvait être fondé que sur le motif de la disparition du besoin d'accueil familial, ce moyen n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de la décision dont elle demande la suspension dès lors que la décision de suppression des emplois d'assistants maternels relevant du service d'accueil familial à domicile a été décidée par le conseil d'administration du CCAS par une délibération du 3 juin 2019 et que la présidente du CCAS a pris la décision de licencier Mme A... en application de cette délibération.<br/>
<br/>
              7. Deuxièmement, si Mme A... soutient que le CCAS n'a pas mis en oeuvre, avant de la licencier, les dispositions de l'article 39-5 du décret du 15 février 1988 pris pour l'application de l'article 138 de la loi du 28 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique territoriale et relatif aux agents non titulaires de la fonction publique territoriale qui lui imposaient, d'une part, de saisir pour avis la commission consultative paritaire avant de lui notifier la décision de licenciement et, d'autre part, de mettre en oeuvre une procédure de reclassement en l'invitant, notamment, à présenter une demande écrite de reclassement, ce moyen n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de la décision dont elle demande la suspension dès lors que les dispositions de cet article ne sont pas applicables aux assistants maternels, en vertu de l'article R. 422-1 du code de l'action sociale et des familles.<br/>
<br/>
              8. Troisièmement, le moyen tiré de ce que le CCAS n'a pas mis en oeuvre l'obligation de reclassement qui pesait sur lui n'est pas, eu égard aux diligences qu'il déclare, sans être réellement contredit, avoir effectuées, de nature à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la demande de Mme A... doit être rejetée, y compris les conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le CCAS au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 13 août 2019 du juge des référés du tribunal administratif de Besançon est annulée.<br/>
Article 2 : La demande présentée par Mme A... devant le juge des référés du tribunal administratif de Besançon et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : Les conclusions du centre communal d'action sociale de la commune de Valdoie présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et au centre communal d'action sociale de la commune de Valdoie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
