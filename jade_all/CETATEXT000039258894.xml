<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258894</ID>
<ANCIEN_ID>JG_L_2019_10_000000434537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 16/10/2019, 434537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Catherine de Salins</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:434537.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 12 septembre et 1er octobre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 2 septembre 2019 de la ministre de la transition écologique et solidaire, relatif à la capture des vanneaux et des pluviers dorés dans le département des Ardennes pour la campagne 2019-2020 ; <br/>
<br/>
              2°) de saisir, à titre subsidiaire, la Cour de justice de l'Union européenne d'une question préjudicielle relative à la compatibilité des méthodes de chasse traditionnelles mises en oeuvre en France et impliquant l'utilisation de gluaux, filets, pantes, matoles et lacs avec le critère de sélectivité exigé par les dispositions de la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - son recours est recevable dès lors qu'elle présente un intérêt lui conférant qualité à agir contre l'arrêté litigieux ;<br/>
              - la condition d'urgence est remplie dès lors que l'arrêté litigieux dont le début d'exécution est imminent porte une atteinte suffisamment grave et immédiate à la préservation et à la défense des oiseaux notamment de leur bien-être, en autorisant la capture d'un nombre particulièrement élevé de vanneaux et de pluviers dorés au moyen de filets pour la campagne 2019-2020 ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté contesté a été pris au terme d'une procédure irrégulière en ne respectant pas le septième alinéa de l'article L. 123-19-1 du code de l'environnement ;<br/>
              - il a été pris en méconnaissance du principe de non-régression énoncé au 9° du II de l'article L. 110-1 du code de l'environnement ; <br/>
              - l'arrêté attaqué, tout comme l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux, méconnaît également le principe de prévention tel qu'il ressort de l'article L. 110-1 du code de l'environnement ; <br/>
              - il a été pris en méconnaissance de l'article L. 424-4 du code de l'environnement et des articles 8 et 9 de la directive 2009/147/CE du Parlement et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages dès lors que l'emploi de filets ne permet pas de respecter le critère de sélectivité nécessaire à l'autorisation de cette pratique, l'absence d'autre solution satisfaisante à la capture par l'utilisation de filets n'est pas justifiée de manière précise et adéquate et les éléments produits par le ministère ne garantissent pas le respect de l'exigence de prélèvements de petites quantités ;<br/>
              - il a été pris en méconnaissance de l'article 6 de la Charte de l'environnement. <br/>
<br/>
<br/>
              Par un mémoire en défense et des pièces complémentaires, enregistrés les 27 et 30 septembre 2019, la ministre de la transition écologique et solidaire conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 1er octobre 2019, la Fédération nationale des chasseurs demande au juge des référés du Conseil d'Etat de rejeter les conclusions de l'association One Voice.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association One Voice et, d'autre part, la ministre de la transition écologique et solidaire et la Fédération nationale des chasseurs ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 2 octobre 2019, à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Lyon-Caen avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association One Voice ;<br/>
<br/>
              - les représentants de l'association One Voice ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique et solidaire ;<br/>
<br/>
              - Me Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération nationale des chasseurs ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au lundi 7 octobre à 12 heures puis au mardi 8 octobre 2019 à 12 heures ;<br/>
<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 4 octobre 2019, présenté par la ministre de la transition écologique et solidaire concluant au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 7 octobre 2019, présenté par l'association One Voice qui maintient ses conclusions à fin de suspension ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 7 octobre 2019, présenté par la Fédération nationale des chasseurs ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment la Charte de l'environnement ;<br/>
              - la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes de l'article de l'article L. 424-4 du code de l'environnement : " Dans le temps où la chasse est ouverte, le permis donne à celui qui l'a obtenu le droit de chasser de jour, soit à tir, soit à courre, à cor et à cri, soit au vol, suivant les distinctions établies par des arrêtés du ministre chargé de la chasse. Le jour s'entend du temps qui commence une heure avant le lever du soleil au chef-lieu du département et finit une heure après son coucher. [...] Pour permettre, dans des conditions strictement contrôlées et de manière sélective, la chasse de certains oiseaux de passage en petites quantités, le ministre chargé de la chasse autorise, dans les conditions qu'il détermine, l'utilisation des modes et moyens de chasse consacrés par les usages traditionnels, dérogatoires à ceux autorisés par le premier alinéa ". Aux termes de l'article 1er de l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes : " La capture des vanneaux huppés et des pluviers dorés à l'aide de filets à nappes fixés à terre, dénommée tenderie aux vanneaux, est autorisée dans les communes d'Amagne, Ambly-Fleury, Brécy-Brières, Challerange, Corny-Machéroménil, Ecly, Falaie, Givry-sur-Aisne, Monthois, Movion-Porcien, Novy-Chevrières, Rilly-sur-Aisne, Sorbon, Terron-sur-Aisne, Vandy, Voncq et Vouziers, du département des Ardennes, et dans les conditions strictement contrôlées définies ci-après afin de permettre la capture sélective et en petites quantités de ces oiseaux, puisqu'il n'existe pas d'autre solution satisfaisante ". Aux termes de l'article 5 de cet arrêté : " Le nombre maximum d'oiseaux pouvant être capturés pendant la campagne est fixé chaque année par le ministre chargé de la chasse ". <br/>
<br/>
              3. Par arrêté du 2 septembre 2019 relatif à la capture des vanneaux et des pluviers dorés dans le département des Ardennes pour la campagne 2019-2020, dont l'association One Voice demande la suspension, la ministre de la transition écologique et solidaire a fixé à respectivement 1 200 et 30 le nombre maximum de vanneaux et de pluviers dorés pouvant être chassés par tenderie dans le département des Ardennes pour la campagne 2019-2020, soit du 15 septembre 2019 au 29 février 2020 en application de l'arrêté du préfet des Ardennes du 29 mai 2019.<br/>
<br/>
              4. La Fédération nationale des chasseurs a intérêt au maintien de l'arrêté du 2 septembre 2019. Par suite son intervention en défense est recevable. <br/>
<br/>
              5. Pour caractériser l'urgence qu'il y aurait à suspendre l'arrêté du 2 septembre 2019, l'association One Voice invoque l'imminence de la date de début de la campagne 2019-2020 ainsi que la souffrance animale provoquée par la méthode de capture autorisée. Elle soutient en outre que le prélèvement autorisé dans le seul département des Ardennes de 1 200 vanneaux huppés et de 30 pluviers dorés serait excessif au regard de la population " française " totale de ces deux espèces. Toutefois, l'association requérante se borne à faire état de considérations générales sur la souffrance animale suscitée par ce type de chasse traditionnelle ainsi que sur l'évolution des populations d'oiseaux de campagne en France et ne conteste pas sérieusement les estimations de populations des espèces considérées tant en Europe qu'en France citées en défense par la ministre de la transition écologique et solidaire et la Fédération nationale des chasseurs pour établir le caractère limité du risque résultant des pratiques contestées au regard de la population d'oiseaux en cause. Dans ces conditions, ces éléments ne suffisent pas à caractériser une situation d'urgence de nature à justifier la suspension de l'arrêté. <br/>
<br/>
              6. Il suit de là, sans qu'il soit besoin d'examiner si l'un des moyens invoqués est de nature à créer un doute sérieux sur la légalité de l'arrêté attaqué, qu'il y a lieu de rejeter la requête, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
      Article 1er : L'intervention de la Fédération nationale des chasseurs est admise.<br/>
Article 2 : La requête de l'association One Voice est rejetée.  <br/>
Article 3: La présente ordonnance sera notifiée à l'association One Voice, à la ministre de la transition écologique et solidaire et à la Fédération nationale des chasseurs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
