<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956640</ID>
<ANCIEN_ID>JG_L_2015_07_000000381261</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 381261, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381261</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:381261.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif d'Amiens la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2001, correspondant, à proportion de ses droits sociaux, à la réduction du résultat bénéficiaire de la SCI Atheni. Par un jugement n° 1000233 du 13 décembre 2012, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13DA00292 du 10 avril 2014, la cour administrative d'appel de Douai, faisant partiellement droit à l'appel formé par M. A...contre ce jugement, a réduit le résultat bénéficiaire de la SCI Atheni au titre de l'exercice clos en 2001 de la somme de 677 205 euros, déchargé M. A...des suppléments d'impositions en litige, correspondant, à proportion de ses droits sociaux, à la réduction prononcée, et rejeté le surplus des conclusions de la requête de M.A.... <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 13 juin 2014 et 11 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 1er, 2 et 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter entièrement l'appel de M.A....<br/>
<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité portant sur les années 2001 à 2003, l'administration fiscale a partiellement réintégré au résultat de la SCI Altheni,  au titre de l'année 2001, dans la catégorie des revenus fonciers, la somme transactionnelle d'un montant de 793 809 euros correspondant au coût des travaux de réparations mentionné dans le procès-verbal de transaction, qui lui a été versée par ses locataires afin de clore une procédure judiciaire tendant à leur imposer la remise en état des bâtiments qu'elle leur louait ; que, estimant que la somme litigieuse devait s'analyser comme des dommages-intérêts compensant la dépréciation de la valeur vénale des immeubles et, à ce titre, n'était pas imposable, M. A...a contesté les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2001, correspondant, à proportion de ses droits sociaux, à la réduction du résultat bénéficiaire de la SCI Atheni ; que le ministre délégué, chargé du budget demande l'annulation de l'arrêt du 10 avril 2014 de la cour administrative d'appel de Douai en tant que, faisant droit à l'appel contre le jugement du 13 décembre 2012 du tribunal administratif d'Amiens, formé par M. A...qui avait admis, en cours d'instance qu'à hauteur de 116 202,95 euros, la somme litigieuse correspondait au coût de travaux de réparations et était, par suite, imposable, il a réduit le résultat bénéficiaire de la SCI Atheni au titre de l'exercice clos en 2001 de la somme de 677 205 euros et déchargé M. A...des suppléments d'imposition en litige, correspondant, à proportion de ses droits sociaux, à la réduction prononcée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 29 du code général des impôts dans sa rédaction applicable à l'année d'imposition en litige : " Sous réserve des dispositions des articles 33 ter et 33 quater, le revenu brut des immeubles ou parties d'immeubles donnés en location, est constitué par le montant des recettes brutes perçues par le propriétaire, augmenté du montant des dépenses incombant normalement à ce dernier et mises par les conventions à la charge des locataires et diminué du montant des dépenses supportées par le propriétaire pour le compte des locataires. Les subventions et indemnités destinées à financer des charges déductibles sont comprises dans le revenu brut./ Dans les recettes brutes de la propriété sont comprises notamment celles qui proviennent de la location du droit d'affichage ou du droit de chasse, de la concession du droit d'exploitation des carrières, de redevances tréfoncières ou autres redevances analogues ayant leur origine dans le droit de propriété ou d'usufruit. " ; qu'il résulte des dispositions du premier alinéa de cet article  que seules les recettes perçues par le propriétaire ou l'usufruitier trouvant leur source dans la propriété ou l'usufruit de l'immeuble ainsi que les subventions et les indemnités destinées à financer des charges déductibles de l'immeuble doivent être comprises dans le revenu brut foncier ; qu'il résulte des termes mêmes du second alinéa de ce même article que ne sont assimilées au revenu brut de l'immeuble que les redevances versées en rémunération d'un droit attaché à la propriété et concédé à des tiers ; que les dispositions de cet alinéa n'ont ni pour objet ni pour effet de permettre de qualifier comme revenu de l'immeuble toutes les sommes versées à un propriétaire à raison de son droit de propriété ;<br/>
<br/>
              3. Considérant qu'après avoir relevé que la SCI Altheni avait saisi la juridiction judiciaire d'une demande de condamnation de ses deux locataires à lui payer une somme destinée, notamment, au financement des travaux de remise en état des bâtiments qu'ils avaient pris en location et que pour mettre fin au litige, les parties étaient convenues du versement par les deux locataires à la SCI Altheni d'une indemnité transactionnelle dont l'administration avait considéré qu'elle était partiellement imposable, la cour a jugé que la somme en litige ne constituait pas une redevance versée en rémunération d'un droit attaché à la propriété et concédé à des tiers, et ne pouvait être regardée, dans cette mesure, comme un revenu brut de l'immeuble au sens et pour l'application du premier alinéa de l'article 29 du code général des impôts ; <br/>
<br/>
              4. Considérant qu'en jugeant que la somme litigieuse ne devait pas être prise en compte dans le calcul de la base imposable à l'impôt sur le revenu et aux contributions sociales au nom des associés de la société sans rechercher si l'immeuble avait effectivement subi une dépréciation permettant de considérer que l'indemnité versée était destinée à compenser un tel préjudice et sans examiner si elle ne devait pas être comprise dans le revenu foncier de la SCI Altheni en application du premier alinéa de l'article 29 du code général des impôts, seul fondement légal invoqué par l'administration, la cour a insuffisamment motivé sa décision et commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le ministre est fondé à demander l'annulation des articles 1 à 3 de l'arrêt qu'il attaque ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1 à 3 de l'arrêt du 10 avril 2014 de la cour administrative d'appel de Douai sont annulés. <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation décidée à l'article 1er, à la cour administrative d'appel de Douai. <br/>
<br/>
Article 3 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
