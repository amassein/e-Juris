<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844919</ID>
<ANCIEN_ID>JG_L_2020_12_000000443688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/49/CETATEXT000042844919.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 29/12/2020, 443688, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:443688.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... U..., Mme AJ... F..., M. K... AB..., Mme AE... B..., M. AJ... AA..., Mme AO... A..., M. AC... M..., Mme Q... I..., M. V... W..., Mme P... O..., M. N... AK..., Mme AM... Z..., M. S... AL..., Mme AD... AN..., M. L... AI..., Mme AH... AF..., M. T... AP..., Mme X... G..., M. H... Y..., Mme E... J... et M. L... D... ont demandé au tribunal administratif de Strasbourg d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Dannemarie (Haut-Rhin) pour l'élection des conseillers municipaux et communautaires. <br/>
<br/>
              Par un jugement n° 2002231 du 31 juillet 2020, rectifié par une ordonnance du 21 août 2020 prise sur le fondement de l'article R. 741-11 du code de justice administrative, ce tribunal a rejeté leur protestation. <br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 septembre, 5 octobre et 30 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. U... et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif ;<br/>
<br/>
              2°) d'annuler les opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de chaque défendeur une somme de 300 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de M. U... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Dannemarie (Haut-Rhin), les 19 sièges de conseillers municipaux et les 6 sièges de conseillers communautaires ont été pourvus. 15 sièges au conseil municipal et 5 sièges au conseil communautaire ont été attribués à des candidats de la liste " Vivons Dannemarie Ensemble " conduite par M. AG... R..., qui a obtenu 53,70 % des suffrages exprimés, tandis que 4 sièges au conseil municipal et le siège restant au conseil communautaire ont été attribués à des candidats de la liste " Unis pour Dannemarie ", conduite par M. C... U..., qui a obtenu 46,29 % des suffrages exprimés. M. U... et autres relèvent appel du jugement du 31 juillet 2020 par lequel le tribunal administratif de Strasbourg a rejeté la protestation qu'ils avaient formée contre ces opérations électorales. Par la voie de l'appel incident, M. R... et autres demandent l'annulation de l'ordonnance du 21 août 2020 par laquelle le président du tribunal administratif de Strasbourg, rectifiant le jugement du 31 juillet 2020 en application des dispositions de l'article R. 741-11 du code de justice administrative, a rejeté la demande qu'ils avaient présentée au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              3. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              4. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". <br/>
<br/>
              5. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020, le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              6. En l'espèce, les requérants font valoir que le niveau d'abstention observé à Dannemarie lors du premier tour des élections municipales aurait rompu l'égalité entre candidats dès lors que celui-ci, plus élevé chez les électeurs les plus âgés, aurait davantage pénalisé leur liste que celle de M. R.... Toutefois, ils n'établissent ni que l'abstention aurait été plus forte chez les électeurs les plus âgés, leurs propres écritures faisant au contraire état d'une abstention de 48,6 % chez les plus de 65 ans contre 53,8 % en moyenne dans la commune, ni, en tout état de cause, qu'une telle abstention aurait été particulièrement préjudiciable à la liste conduite par M. U.... Dans ces conditions, le niveau d'abstention constaté ne peut en l'espèce être regardé comme ayant rompu l'égalité entre candidats. Par suite, les requérants ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté la protestation qu'ils avaient formée contre les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de Dannemarie.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. R... et autres qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a, par ailleurs, lieu, dans les circonstances de l'espèce, de faire droit ni aux conclusions présentées par M. R... et autres sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative dans la présente instance devant le Conseil d'Etat, ni, en tout état de cause, à celles qu'ils présentent sur le même fondement par la voie de l'appel incident au titre de l'instance devant le tribunal administratif.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. U... et autres est rejetée.<br/>
Article 2 : L'appel incident de M. R... et autres et leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. C... U..., premier requérant dénommé, à M. AG... R..., premier défendeur dénommé, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
