<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569465</ID>
<ANCIEN_ID>JG_L_2020_02_000000436603</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 12/02/2020, 436603</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436603</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436603.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une demande, enregistrée au greffe du tribunal administratif de Cergy-Pontoise le 2 août 2019, la société JCDecaux France a demandé à ce tribunal de condamner l'Etat à lui verser une somme de 42 678,12 euros en réparation des dommages qu'elle estime avoir subis dans le département de la Seine-Maritime à l'occasion de sept manifestations ayant eu lieu dans ce département entre le 1er décembre 2018 et le 7 avril 2019. Par une ordonnance n° 1909976 du 5 novembre 2019, le président de la 10eme chambre du tribunal administratif de Cergy-Pontoise a transmis cette demande au tribunal administratif de Rouen.<br/>
<br/>
              Par une ordonnance n° 1903994 du 10 décembre 2019, la présidente du tribunal administratif de Rouen a, sur le fondement de l'article R. 351-6 du code de justice administrative, transmis le dossier de la demande au président de la section du contentieux du Conseil d'Etat, afin qu'il règle la question de compétence. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- le code de la sécurité intérieure ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'article R. 312-1 du code de justice administrative dispose que : " Lorsqu'il n'en est pas disposé autrement par les dispositions de la section 2 du présent chapitre ou par un texte spécial, le tribunal administratif territorialement compétent est celui dans le ressort duquel a légalement son siège l'autorité qui, soit en vertu de son pouvoir propre, soit par délégation, a pris la décision attaquée (...) ". S'agissant des actions en responsabilité fondées sur une cause autre que la méconnaissance d'un contrat ou d'un quasi-contrat et dirigées contre l'Etat, l'article R. 312-14 du même code dispose que de telles actions relèvent : " 1° Lorsque le dommage invoqué est imputable à une décision qui a fait ou aurait pu faire l'objet d'un recours en annulation devant un tribunal administratif, de la compétence de ce tribunal ; / 2° Lorsque le dommage invoqué est un dommage de travaux publics ou est imputable soit à un accident de la circulation, soit à un fait ou à un agissement administratif, de la compétence du tribunal administratif dans le ressort duquel se trouve le lieu où le fait générateur du dommage s'est produit ; / 3° Dans tous les autres cas, de la compétence du tribunal administratif dans le ressort duquel se trouvait, au moment de l'introduction de la demande, la résidence de l'auteur ou du premier des auteurs de cette demande, s'il est une personne physique, ou son siège, s'il est une personne morale ".<br/>
<br/>
              2. Aux termes de premier alinéa de l'article L. 211-10 du code de la sécurité intérieure : " L'Etat est civilement responsable des dégâts et dommages résultant des crimes et délits commis, à force ouverte ou par violence, par des attroupements ou rassemblements armés ou non armés, soit contre les personnes, soit contre les biens ". Les actions indemnitaires engagées sur le fondement de ces dispositions doivent être regardées comme ressortissant, en application du 2° de l'article R. 312-14 du code de justice administrative, à la compétence du tribunal administratif dans le ressort duquel se trouve le lieu où s'est produit le fait générateur du dommage.<br/>
<br/>
              3. La demande de la société JCDecaux France tend à l'indemnisation, sur le fondement de l'article L. 211-10 du code de la sécurité intérieure, de dommages survenus lors d'attroupements qui ont eu lieu dans le département de la Seine-Maritime. Par suite, il y a lieu d'en attribuer le jugement au tribunal administratif de Rouen, compétent pour en connaître en vertu du 2° de l'article R. 312-14 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la demande de la société JCDecaux France est attribué au tribunal administratif de Rouen.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société JCDecaux France et aux présidents des tribunaux administratifs de Rouen et de Cergy-Pontoise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - ACTIONS EN RÉPARATION DES DOMMAGES RÉSULTANT DES ATTROUPEMENTS ET RASSEMBLEMENTS (ART. L. 211-10 DU CSI) - COMPÉTENCE DU TA DANS LE RESSORT DUQUEL SE TROUVE LE LIEU OÙ S'EST PRODUIT LE FAIT GÉNÉRATEUR DU DOMMAGE (2° DE L'ART. R. 312-14 DU CJA) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. - ACTIONS EN RÉPARATION DES DOMMAGES RÉSULTANT DES ATTROUPEMENTS ET RASSEMBLEMENTS (ART. L. 211-10 DU CSI) - COMPÉTENCE DU TA DANS LE RESSORT DUQUEL SE TROUVE LE LIEU OÙ S'EST PRODUIT LE FAIT GÉNÉRATEUR DU DOMMAGE (2° DE L'ART. R. 312-14 DU CJA) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-05-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. RESPONSABILITÉ RÉGIE PAR DES TEXTES SPÉCIAUX. ATTROUPEMENTS ET RASSEMBLEMENTS (ART. L. 2216-3 DU CGCT). - ACTIONS EN RÉPARATION DES DOMMAGES RÉSULTANT DES ATTROUPEMENTS ET RASSEMBLEMENTS (ART. L. 211-10 DU CSI) - COMPÉTENCE DU TA DANS LE RESSORT DUQUEL SE TROUVE LE LIEU OÙ S'EST PRODUIT LE FAIT GÉNÉRATEUR DU DOMMAGE (2° DE L'ART. R. 312-14 DU CJA) [RJ1].
</SCT>
<ANA ID="9A"> 17-05-01-02 Les actions indemnitaires engagées sur le fondement de l'article L. 211-10 du code de la sécurité intérieure (CSI) doivent être regardées comme ressortissant, en application du 2° de l'article R. 312-14 du code de justice administrative (CJA), à la compétence du tribunal administratif dans le ressort duquel se trouve le lieu où s'est produit le fait générateur du dommage.</ANA>
<ANA ID="9B"> 60-01-02-01 Les actions indemnitaires engagées sur le fondement de l'article L. 211-10 du code de la sécurité intérieure (CSI) doivent être regardées comme ressortissant, en application du 2° de l'article R. 312-14 du code de justice administrative (CJA), à la compétence du tribunal administratif dans le ressort duquel se trouve le lieu où s'est produit le fait générateur du dommage.</ANA>
<ANA ID="9C"> 60-01-05-01 Les actions indemnitaires engagées sur le fondement de l'article L. 211-10 du code de la sécurité intérieure (CSI) doivent être regardées comme ressortissant, en application du 2° de l'article R. 312-14 du code de justice administrative (CJA), à la compétence du tribunal administratif dans le ressort duquel se trouve le lieu où s'est produit le fait générateur du dommage.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la compétence du TA dans le ressort duquel se trouve le lieu où s'est produit le fait générateur du dommage en cas de carence des services de police, CE, 25 mars 1992, Compagnie d'assurance Mercator N.V. et autres, n° 102632, T. pp. 846-1285-1289-1294.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
