<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039772887</ID>
<ANCIEN_ID>JG_L_2019_12_000000431898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/77/28/CETATEXT000039772887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/12/2019, 431898, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:431898.20191227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... C... a demandé au tribunal administratif de Nice de condamner le centre hospitalier de Cannes, ou, à titre subsidiaire, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), à lui verser la somme de 240 213,62 euros en réparation des préjudices qu'il estime avoir subis du fait de sa prise en charge dans cet établissement hospitalier. Par un jugement n° 1202063 du 11 mai 2015, le tribunal administratif de Nice a condamné le centre hospitalier de Cannes à verser à M. C... la somme de 75 187 euros.<br/>
<br/>
              Par un arrêt avant dire droit n° 15MA03017 du 8 mars 2018, la cour administrative d'appel de Marseille a, sur appel du centre hospitalier de Cannes, annulé les articles 1er et 2 de ce jugement, condamné le centre hospitalier de Cannes à payer à M. C... la somme de 75 500 euros dont 72 000 euros à titre provisionnel et ordonné une expertise.<br/>
<br/>
              Par un arrêt n° 15MA03017 du 11 avril 2019, rectifié par une ordonnance du 24 avril 2019 de la présidente de la cour administrative d'appel de Marseille, la cour administrative d'appel de Marseille a annulé le jugement du 11 mai 2015 en tant qu'il omet de se prononcer sur la dévolution des frais d'expertise, condamné le centre hospitalier de Cannes à verser à M. C... la somme de 442 107,90 euros en réparation des préjudices subis par ce dernier et à verser à la caisse primaire d'assurance maladie du Var la somme de 257 545 euros au titre du remboursement des prestations versées et la somme de 1 080 euros au titre de l'indemnité forfaitaire de gestion. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juin et 23 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Cannes demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du Centre Hospitalier De Cannes.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'il attaque, le centre hospitalier de Cannes soutient :<br/>
              - qu'il doit être annulé par voie de conséquence de l'annulation par le Conseil d'Etat, statuant au contentieux, de l'arrêt avant dire droit de la même cour du 8 mars 2018 ;<br/>
              - qu'il est entaché d'erreur de droit en ce qu'il indemnise, en violation du principe de réparation intégrale du préjudice, l'assistance par une tierce personne sur la base d'un taux horaire pour la période du 7 mars 2011 au 11 avril 2019 excessif ;<br/>
              - qu'il est entaché de dénaturation des pièces du dossier et de contradiction de motifs en ce qu'il estime que les montants de l'indemnité au titre de l'assistance pas une tierce personne sont, pour les deux périodes considérées, erronés au regard des éléments retenus pour leur liquidation.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il se prononce sur l'indemnisation de l'assistance par une tierce personne pour les périodes du 15 mai 2014 au 1er août 2017 ainsi que du 7 décembre 2017 jusqu'à la date de l'arrêt attaqué. En revanche, aucun des moyens n'est de nature à permettre l'admission du surplus des conclusions.<br/>
<br/>
<br/>
<br/>
<br/>D É C I D E :<br/>
              --------------<br/>
<br/>
Article 1er :  Les conclusions du pourvoi du centre hospitalier de Cannes qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'indemnisation de l'assistance par une tierce personne pour les périodes du 15 mai 2014 au 1er août 2017 ainsi que du 7 décembre 2017 jusqu'à la date de l'arrêt du 11 avril 2019 de la cour administrative d'appel de Marseille sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi du centre hospitalier de Cannes n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée au centre hospitalier de Cannes.<br/>
Copie en sera adressée à M. D... C..., à la caisse primaire d'assurance maladie du Var et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
