<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041407296</ID>
<ANCIEN_ID>JG_L_2020_01_000000436704</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/40/72/CETATEXT000041407296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/01/2020, 436704, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436704</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:436704.20200114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 décembre 2019 et 7 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la société Alpha Europe Energy demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution :<br/>
              - de la décision du 15 novembre 2019 par laquelle la ministre de la<br/>
transition écologique et solidaire l'a sanctionnée à la suite de l'opération de contrôle<br/>
n° 4525NOB/CTR0001 et l'a mise en demeure d'acquérir les certificats d'économies<br/>
d'énergie nécessaires à l'application de cette sanction, <br/>
              - de la décision n° CL181019ANLS823265665N0 du 15 novembre 2019 par laquelle la ministre de la transition écologique et solidaire a annulé des certificats d'économies d'énergie d'un montant de 6 975 568 kWh " hors précarité énergétique " qui lui avaient été délivrés ;<br/>
              - de la décision n° PR181019ANLS823265665N0 du 15 novembre 2019 par laquelle la ministre de la transition écologique et solidaire a annulé des certificats d'économies d'énergie d'un montant de 56 914 615 kWh " précarité " qui lui avaient été délivrés ;<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que les décisions litigieuses préjudicient de manière grave et immédiate à ses intérêts économiques et financiers en faisant obstacle à ce qu'elle obtienne pendant dix-huit mois des certificats d'économies d'énergie (CEE), l'empêchant ainsi d'exercer son unique activité au cours de la même période, en lui imposant d'acquérir les certificats d'économies d'énergie nécessaires à l'application des sanctions et en lui infligeant des pertes financières substantielles alors qu'elle se trouve déjà dans une situation difficile ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              - ces décisions ont été adoptées au terme d'une procédure qui a méconnu les droits de la défense et le principe du contradictoire garantis par les articles L. 222-3 et L. 222-5 du code de l'énergie, dès lors que des éléments déterminants ne lui ont pas été transmis ; <br/>
              - ces décisions sont fondées sur des faits matériellement inexacts ou non établis, dès lors qu'elle a démontré être de bonne foi et n'avoir commis aucun manquement ;<br/>
              - la durée du retrait d'éligibilité et la mesure de rejet des demandes en cours sont disproportionnées, dès lors qu'aucune falsification de document ne saurait lui être reprochée et qu'elle se trouve dans une situation financière très délicate ;<br/>
              - la mesure de rejet des demandes de certificats d'économie d'énergie est entachée d'une erreur de droit dans l'application des articles L. 222-2 et R. 222-9 du code de l'énergie, dès lors que ces demandes avaient déjà fait l'objet d'une décision implicite d'acceptation.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 janvier 2020, la ministre de la transition écologique et solidaire conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Alpha Europe Energy et, d'autre part, la ministre de la transition écologique et solidaire ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 9 janvier 2020 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Alpha Europe Energy ;<br/>
<br/>
              -  le représentant de la société Alpha Europe Energy ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique et solidaire ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes de l'article L. 222-1 du code de l'énergie : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements qu'il constate, de la part des personnes mentionnées à l'article L. 221-1, aux dispositions des articles L. 221-1 à L. 221-5 ou aux dispositions réglementaires prises pour leur application. ". Aux termes de l'article L. 222-2 du même code : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. Il peut rendre publique cette mise en demeure. / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : / 1° Prononcer à son encontre une sanction pécuniaire dont le montant est proportionné à la gravité du manquement et à la situation de l'intéressé, sans pouvoir excéder le double de la pénalité prévue au premier alinéa de l'article L. 221-4 par kilowattheure d'énergie finale concerné par le manquement et sans pouvoir excéder 2 % du chiffre d'affaires hors taxes du dernier exercice clos, porté à 4 % en cas de nouveau manquement à la même obligation ; / 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé (...) ". Aux termes de l'article L. 222-6 du même code : " Les décisions sont motivées, notifiées à l'intéressé et publiées au Journal officiel ". Enfin, aux termes de l'article R. 222-12 du même code : " Les décisions du ministre chargé de l'énergie prononçant les sanctions prévues à l'article L. 222-2 peuvent faire l'objet d'un recours de pleine juridiction et d'une demande de référé tendant à la suspension de leur exécution devant le Conseil d'Etat (...) ".<br/>
<br/>
              3. La société Alpha Europe Energy, spécialisée dans les services relatifs aux certificats d'économie d'énergie, est soumise aux obligations d'économie d'énergie prévues aux articles L. 221-1 et suivants du code de l'énergie. Par un courrier du 25 juillet 2017, le ministère de la transition écologique et solidaire a engagé une procédure de contrôle sur plusieurs opérations réalisées par la société Alpha Europe Energy. La ministre a demandé à la société, par un courrier du 16 novembre 2017, sur le fondement de l'article R. 222-9 du code de l'énergie, de présenter des justificatifs dans le délai d'un mois et elle l'a informée, sur le fondement du même article, de la suspension du délai d'acceptation implicite des demandes de certificats d'économie d'énergie non encore délivrés. Par deux courriers du 23 août et du 13 décembre 2017, la société requérante a transmis au ministère plusieurs documents dans le cadre de cette procédure. Le<br/>
14 juin 2018, la ministre a informé la société Alpha Europe Energy de son intention de prendre des sanctions et le 12 juillet 2018, la société requérante a été reçue au ministère de la transition écologique et solidaire pour y présenter ses observations sur les manquements reprochés, ce qu'elle a également fait par écrit dans un courrier du 18 juillet 2018. Par un courrier du<br/>
15 novembre 2019, la ministre de la transition écologique et solidaire a notifié à la société requérante les décisions de sanction prises à son encontre. La société Alpha Europe Energy demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de ces décisions.<br/>
              4. Les moyens invoqués par la société Alpha Europe Energy à l'appui de sa demande de suspension et tirés de ce que les sanctions attaquées de la ministre de la transition écologique et solidaire ont été adoptées au terme d'une procédure qui a méconnu les droits de la défense et le principe du contradictoire, de ce que ces sanctions sont fondées sur des faits matériellement inexacts ou non établis, de ce que la durée du retrait d'éligibilité et la mesure de rejet des demandes de certificats d'économie d'énergie sont disproportionnées et de ce que la mesure de rejet des demandes de certificats d'économie d'énergie est entachée d'une erreur de droit dans l'application des articles L. 222-2 et R. 222-9 du code de l'énergie ne paraissent pas, en l'état de l'instruction, propres à créer un doute sérieux sur la légalité des dispositions contestées. Dès lors, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par la société Alpha Europe Energy tendant à ce que soit suspendue l'exécution des trois décisions attaquées du 15 novembre 2019 doit être rejetée. <br/>
<br/>
              5. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par la société Alpha Europe Energy soit mise à la charge de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Alpha Europe Energy est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Alpha Europe Energy et à la ministre de la transition écologique et solidaire. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
