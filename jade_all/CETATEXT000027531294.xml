<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027531294</ID>
<ANCIEN_ID>JG_L_2013_06_000000341889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/53/12/CETATEXT000027531294.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 10/06/2013, 341889, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:341889.20130610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 juillet et 26 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Institut de participation de l'Ouest (IPO), dont le siège est 32, rue Camus à Nantes (44000), représentée par son président directeur général ; la société IPO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT01626 du 17 mai 2010 par lequel la cour administrative d'appel de Nantes a annulé le jugement n° 08-4187 du 12 mars 2009 du tribunal administratif de Nantes et remis à sa charge les cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2001 à 2003 ainsi que les pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu la loi n° 85-695 du 11 juillet 1985 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la SA Institut de participation de l'Ouest ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières " ; qu'en vertu de l'article 1er de cette loi, elle doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés ; qu'il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1647 E du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I. - La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 50 millions de francs est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies. (...) Par exception, le taux visé au premier alinéa est fixé à 1 % au titre de 1999 et à 1,2 % au titre de 2000. " ; que selon le II de l'article 1647 B sexies du même code, dans sa rédaction alors en vigueur : " 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes, les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion de valeurs mobilières est égale à la différence entre : / d'une part, les produits d'exploitation bancaires et produits accessoires ; / et, d'autre part, les charges d'exploitation bancaires. (...) " ; qu'eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des seules entreprises qui exercent cette activité pour leur propre compte ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande en décharge des cotisations supplémentaires de taxe professionnelle auxquelles la SA IPO a été assujettie au titre des années 2001 à 2003, la cour administrative d'appel de Nantes a jugé que cette société de capital-risque n'était pas soumise aux modalités de calcul de la valeur ajoutée prévues pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières au motif qu'elle gère pour son propre compte des participations financières ; que la cour a ainsi commis une erreur de droit au regard des dispositions précitées du 3 du II de l'article 1647 B sexies du code général des impôts ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 4 000 euros à verser à la société CM-CIC Investissement, venant aux droits de la SA IPO, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 17 mai 2010 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat versera à la société CM-CIC Investissement une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société CM-CIC Investissement et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
