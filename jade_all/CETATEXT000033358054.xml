<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033358054</ID>
<ANCIEN_ID>JG_L_2016_11_000000387090</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/35/80/CETATEXT000033358054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 03/11/2016, 387090, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387090</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387090.20161103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Distribution Casino France, l'Immobilière Groupe Casino, M. A...B...et les consorts C...ont demandé au tribunal administratif de Grenoble d'annuler la délibération du 20 octobre 2011 par laquelle le conseil municipal de la commune de Saint-Martin-d'Hères a approuvé le plan local d'urbanisme de cette commune. Par un jugement nos 1106771-1106810-1200803 du 10 octobre 2013, le tribunal administratif de Grenoble a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 13LY03241 du 13 novembre 2014, la cour administrative d'appel de Lyon a, sur appel de la SAS Distribution Casino France et de l'Immobilière Groupe Casino, annulé ce jugement ainsi que la délibération du conseil municipal de la commune de Saint Martin d'Hères du 20 octobre 2011.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 janvier et 13 avril 2015 et le 1er juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Martin d'Hères demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la SAS Distribution Casino France et de l'Immobilière Groupe Casino ;<br/>
<br/>
              3°) de mettre à la charge de la SAS Distribution Casino France et de l'Immobilière Groupe Casino la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la commune de Saint-Martin d'Hères et à la SCP Foussard, Froger, avocat de la SAS Distribution Casino France et autre.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 19 juin 2008, le conseil municipal de Saint-Martin-d'Hères a prescrit la révision du plan local d'urbanisme de la commune et s'est prononcé sur ses objectifs et les modalités de la concertation qui devait la précéder, en application des articles L. 123-6 et L. 300-2 du code de l'urbanisme ; que, par une délibération du 18 octobre 2011, le conseil municipal a approuvé le plan local d'urbanisme ainsi révisé ; que, par un arrêt du 13 novembre 2014, contre lequel la commune de Saint-Martin-d'Hères se pourvoit en cassation, la cour administrative d'appel de Lyon a annulé cette délibération ;<br/>
<br/>
              2. Considérant que l'article L. 123-6 du code de l'urbanisme, dans sa rédaction en vigueur à la date de la délibération litigieuse, dispose : " Le plan local d'urbanisme est élaboré à l'initiative et sous la responsabilité de la commune. La délibération qui prescrit l'élaboration du plan local d'urbanisme et précise les modalités de concertation, conformément à l'article L. 300-2, est notifiée au préfet (...) " ; qu'aux termes des deux premiers alinéas de l'article L. 300-2 du même code, dans sa rédaction en vigueur à la même date : " I - Le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale délibère sur les objectifs poursuivis et sur les modalités d'une concertation associant, pendant toute la durée de l'élaboration du projet, les habitants, les associations locales et les autres personnes concernées (...), avant : / a) Toute élaboration ou révision du schéma de cohérence territoriale ou du plan local d'urbanisme ; " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le conseil municipal doit, avant que ne soit engagée la concertation, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser un document d'urbanisme, d'autre part, sur les modalités de la concertation avec les habitants, les associations locales et les autres personnes concernées ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la délibération du 19 juin 2008 du conseil municipal de Saint-Martin-d'Hères indique que la réflexion conduite pour la révision du document d'urbanisme doit " porter sur l'avenir de la commune et de ses habitants à l'échelle de la ville, de ses quartiers et de l'agglomération, et s'articuler autour des grands enjeux suivants : - la densification qualitative, au travers de l'économie d'espace, du travail sur la forme urbaine, les espaces publics et le cadre de vie, de la réponse aux objectifs du Programme Local de l'Habitat dont la mixité sociale, et du maintien, voire d'une légère croissance de la population martinéroise ; / - une approche environnementale plus forte pour faire face aux enjeux climatiques, portant sur le développement et sur la gestion de la ville, tout en s'articulant avec la dimension sociale et économique ; / - la dynamisation de la mixité urbaine, au travers de la cohabitation des fonctions et des territoires de la ville (habitat, zones économiques, commerces, ... ) ; / - la recherche de la continuité urbaine entre tous les quartiers de la ville ; / - des centralités secondaires à pérenniser ou à bâtir à l'échelle des quartiers, en lien avec le développement du commerce et des services de proximité et une réflexion sur les espaces publics fédérateurs ; / - la réflexion sur les grands territoires à enjeux, tels que les zones économiques des Glairons et de Champ Roman, la section centrale de l'avenue Gabriel Péri en lien avec le domaine universitaire, ou encore des sites à projets comme le couvent des Minimes, le secteur Chopin / Paul Bert ; / - l'élaboration de projets urbains pour les zones à urbaniser ; / - la question de l'insertion urbaine de la rocade et de la voie ferrée, avec une réflexion sur les possibilités de réduction de la coupure que ces infrastructures génèrent au niveau du territoire communal, sur l'urbanisation à proximité et sur le traitement des nuisances et du paysage ; / - le renforcement d'une vie sociale au travers d'une diversité culturelle - inscrire le développement de Saint-Martin-d'Hères dans une dynamique d'agglomération, en valorisant ses ambitions et ses richesses " ; qu'en estimant que cette délibération ne pouvait être regardée comme ayant fixé, au moins dans leur grandes lignes, les objectifs poursuivis par la révision du plan local d'urbanisme, en application des dispositions de l'article L. 300-2 du code de l'urbanisme rappelées ci-dessus, la cour a, en tout état de cause, entaché son arrêt de dénaturation ; qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Saint-Martin-d'Hères est fondée, pour ce motif, à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Saint-Martin-d'Hères qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SAS Distribution Casino France et de l'Immobilière Groupe Casino la somme de 1 500 euros chacune à verser à la commune de Saint-Martin-d'Hères au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 novembre 2014 de la cour administrative d'appel de Lyon est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La SAS Distribution Casino France et l'Immobilière Groupe Casino verseront chacune à la commune de Saint-Martin-d'Hères une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la SAS Distribution Casino France et de l'Immobilière Groupe Casino présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Saint-Martin-d'Hères, à la SAS Distribution Casino France et à l'Immobilière Groupe Casino.<br/>
     Copie en sera adressée à la ministre du logement et de l'habitat durable.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
