<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026829997</ID>
<ANCIEN_ID>JG_L_2012_12_000000364181</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/82/99/CETATEXT000026829997.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/12/2012, 364181, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364181</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:364181.20121213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Mervyn Christopher A, demeurant ... ; M. A demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret du 20 septembre 2012 par lequel le Premier ministre a accordé son extradition aux autorités britanniques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que : <br/>
              - la condition d'urgence est remplie dès lors qu'il est placé sous écrou extraditionnel depuis le 4 mai 2012 ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - en effet, la procédure d'arrestation provisoire est fondée sur un mandat d'arrêt européen qui n'est pas applicable ;<br/>
              - cette procédure a méconnu les dispositions des articles 696-13 et 696-23 du code de procédure pénale ainsi que celles des articles 3 et 6 du troisième protocole additionnel à la convention européenne d'extradition ;<br/>
              - le décret litigieux est entaché d'une insuffisance de motivation ;<br/>
              - il n'est pas signé par le Premier ministre et le garde des sceaux et son seul signataire ne justifie pas d'une habilitation à cet effet ;<br/>
              - le décret litigieux est entaché d'une erreur manifeste d'appréciation ;<br/>
              - l'arrêt de la chambre de l'instruction de la cour d'appel de Bordeaux du 5 juillet 2012 visé par le décret contesté est entaché de plusieurs irrégularités ;<br/>
              - le décret est illégal en ce qu'il accorde l'extradition pour des faits dont le caractère imprescriptible est contraire à l'ordre public français ;<br/>
              - le décret contesté est contraire à la première réserve émise par la France à la convention européenne d'extradition ainsi qu'au septième alinéa de l'article 694-4 du code de procédure pénale ;<br/>
              - il est contraire à l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 7 décembre 2012, présenté par la garde  des sceaux, ministre de la justice, qui conclut au rejet de la requête ; elle soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que le Gouvernement entend suivre la pratique constante selon laquelle un décret d'extradition n'est pas mis à exécution avant que le Conseil d'Etat ait statué lorsqu'il est saisi d'un recours en annulation contre le décret, comme c'est le cas en l'espèce ;<br/>
              - aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Vu le mémoire aux fins de production, enregistré le 10 décembre 2012, présenté par M. A ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A et, d'autre part, la garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 décembre 2012 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Haas, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A ;<br/>
<br/>
              - le représentant de M. A ;<br/>
- les représentants de la garde des sceaux, ministre de la justice ; <br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que M. A demande, sur le fondement de ces dispositions, la suspension de l'exécution du décret du 20 septembre 2012 accordant son extradition aux autorités britanniques ; qu'il a formé un recours pour excès de pouvoir contre ce décret le 28 novembre 2012 ;<br/>
<br/>
              3. Considérant que, selon un usage constant, un décret d'extradition n'est pas mis à exécution tant que le délai de recours contre ce décret n'est pas expiré et tant que le Conseil d'Etat, saisi d'un recours dans ce délai, n'a pas statué ; que la garde des sceaux, ministre de la justice a d'ailleurs indiqué dans son mémoire écrit et confirmé lors de l'audience publique que, conformément à cet usage, le Gouvernement ne procédera pas à l'exécution du décret contesté avant que le Conseil d'Etat ait statué sur le recours introduit par M. A ; que la levée de l'écrou extraditionnel sous lequel le requérant est placé n'est pas conditionnée par la suspension demandée ; que, dans ces conditions, l'urgence ne peut être regardée comme établie ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'une des conditions à laquelle est subordonné l'exercice, par le juge des référés, des pouvoirs qu'il tient de l'article L. 521-1 du code de justice administrative n'est pas remplie ; qu'il y a lieu, en conséquence, de rejeter l'ensemble des conclusions de la requête, y compris celles présentées en application des dispositions de l'article L. 761-1 de ce code ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Mervyn Christopher A et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
