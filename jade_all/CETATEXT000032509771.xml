<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032509771</ID>
<ANCIEN_ID>JG_L_2016_05_000000381635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/50/97/CETATEXT000032509771.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 02/05/2016, 381635, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:381635.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Toffolutti a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir la décision de refus du préfet de la région Haute-Normandie de lui communiquer certains documents administratifs relatifs au marché de travaux publics, pour lequel elle s'était portée candidate, en vue de la fabrication, du transport et de la mise en oeuvre d'enrobés hydrocarbonés pour l'entretien du réseau routier national du périmètre de Caen, notamment le bordereau unitaire de prix de l'entreprise attributaire. Par un jugement n° 1203719 du 24 avril 2014, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 23 juin, 17 septembre et 27 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Toffolutti demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
- la loi n° 78-753 du 17 juillet 1978 ;<br/>
- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Toffolutti ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Toffolutti s'est portée candidate dans le cadre d'un appel d'offres ouvert lancé par la direction interdépartementale des routes Nord-Ouest pour la fabrication, le transport et la mise en oeuvre d'enrobés hydrocarbonés destinés au réseau routier national du périmètre de Caen ; que, par courrier du 9 janvier 2012, la requérante a été informée par le préfet de la région Haute-Normandie que son offre n'avait pas été retenue, ce courrier précisant le nom de l'entreprise attributaire ainsi que les motifs du rejet de sa candidature ; que, le 17 février 2012, l'administration, faisant partiellement droit à sa demande tendant à la communication de l'ensemble des pièces relatives à ce marché, lui a communiqué le rapport d'analyse des offres dans une version qui ne comportait que les informations la concernant ainsi que celles de l'attributaire du marché et l'a informée que les détails techniques et financiers de l'offre du candidat retenu n'étaient pas communicables ; que, sur saisine de la société Toffolutti, la commission d'accès aux documents administratifs a rendu un avis favorable, sous certaines réserves, à la communication de l'ensemble des documents demandés; que la société Toffolutti se pourvoit en cassation contre le jugement du 24 avril 2014 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant à l'annulation de la décision par laquelle le préfet a refusé de lui communiquer certaines des pièces demandées, au nombre desquelles le bordereau unitaire de prix de l'entreprise attributaire ainsi que le rapport non occulté d'analyse des offres des autres candidats ;<br/>
<br/>
              3. Considérant que, devant les juridictions administratives et dans l'intérêt d'une bonne justice, le juge a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci ; qu'il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser ; que, s'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser ;<br/>
<br/>
              4. Considérant que la SA Toffolutti a, la veille de l'audience devant le tribunal administratif adressé par télécopie un nouveau mémoire ; qu'il résulte des mentions enregistrées dans l'application Sagace que ce mémoire a été régularisé le 3 avril 2014, jour de l'audience ; que ce mémoire n'est pas visé par le jugement attaqué ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société Toffolutti est fondée à soutenir que l'arrêt qu'elle attaque a été rendu au terme d'une procédure irrégulière et à en demander, pour ce motif, l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal alors en vigueur : " Le droit de toute personne à l'information est précisé et garanti par les dispositions des chapitres Ier, III et IV du présent titre en ce qui concerne la liberté d'accès aux documents administratifs. / Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, directives, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions et décisions. (...) " ; qu'aux termes de l'article 2 de cette même loi : " Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent titre. / Le droit à communication ne s'applique qu'à des documents achevés. " ; que le II de l'article 6 de cette loi dispose que : " II.-Ne sont communicables qu'à l'intéressé les documents administratifs : / - dont la communication porterait atteinte à la protection de la vie privée, au secret médical et au secret en matière commerciale et industrielle (...) ", que ces dispositions sont aujourd'hui codifiées aux articles L. 311-1 à L. 311-6 du code des relations entre le public et l'administration ;<br/>
<br/>
              7.  Considérant qu'il résulte des dispositions précitées que les marchés publics et les documents qui s'y rapportent, y compris les documents relatifs au contenu des offres, sont des documents administratifs au sens des dispositions de l'article 1er de la loi du 17 juillet 1978 ; que, saisis d'un recours relatif à la communication de tels documents, il revient aux juges du fond d'examiner si, par eux-mêmes, les renseignements contenus dans les documents dont il est demandé la communication peuvent, en affectant la concurrence entre les opérateurs économiques, porter atteinte au secret industriel et commercial et faire ainsi obstacle à cette communication en application des dispositions du II de l'article 6 de la loi du 17 juillet 1978 ; qu'au regard des règles de la commande publique, doivent ainsi être regardés comme communicables, sous réserve des secrets protégés par la loi, l'ensemble des pièces du marché ; que si notamment l'acte d'engagement, le prix global de l'offre et les prestations proposées par l'entreprise attributaire sont en principe communicables,  le bordereau unitaire de prix de l'entreprise attributaire, en ce qu'il reflète la stratégie commerciale de l'entreprise opérant dans un secteur d'activité, n'est quant à lui, en principe, pas communicable ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier que le marché auquel se rapportent les documents litigieux est intervenu dans le secteur des enrobés hydrocarbonés pour les routes, les procédés de fabrication qui revêt un caractère concurrentiel; qu'il suit de là que la communication du détail de l'offre de prix de l'entreprise attributaire, qui constitue une composante essentielle de sa stratégie commerciale, est susceptible, même au terme de l'exécution du marché, eu égard à la nature de celui-ci, d'altérer le libre jeu de la concurrence entre les opérateurs économiques et de porter ainsi atteinte à la protection du secret commercial ; que le préfet pouvait donc légalement refuser de communiquer à la requérante le bordereau unitaire de prix de l'entreprise attributaire ; qu'eu égard aux motifs énoncés au point 6 de la présente décision, il en va différemment des informations relatives au montant global des offres présentées par les entreprises candidates qui n'ont pas été retenues ;  <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède, que la société Toffolutti, dont la requête est recevable, contrairement à ce que soutient le préfet, n'est fondée à demander l'annulation de la décision qu'elle attaque qu'en tant qu'elle lui refuse la communication des informations relatives au montant global des offres présentées par les entreprises candidates qui n'ont pas été retenues  ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la requérante d'une somme de 1500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 24 avril 2014 du tribunal administratif de Rouen est annulé.<br/>
Article 2 : La décision du préfet de la région Haute-Normandie est annulée en tant qu'elle refuse la communication des informations relatives au montant global des offres présentées par les entreprises candidates qui n'ont pas été retenues.  <br/>
Article 3 : L'Etat versera à la société Toffolutti une somme de 1500 euros au titre de l'article L 76-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de la société Toffolutti est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Toffolutti et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
