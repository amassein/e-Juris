<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038064788</ID>
<ANCIEN_ID>JG_L_2019_01_000000417999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/06/47/CETATEXT000038064788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 28/01/2019, 417999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417999.20190128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...E..., veuve G...F..., a demandé au tribunal administratif de Rennes d'annuler la décision du 9 juillet 2015 par laquelle le comité d'indemnisation des victimes des essais nucléaires (CIVEN) a rejeté sa demande d'indemnisation présentée au titre de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français et de condamner le CIVEN à l'indemniser intégralement. Par un jugement n° 1304189 du 31 décembre 2015, le tribunal administratif de Rennes a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16NT00788 du 8 décembre 2017, la cour administrative d'appel de Nantes a, sur appel de MM.A..., B...et H...G...F..., ayants droit de Mme G... F..., annulé ce jugement et enjoint au CIVEN de réexaminer la demande d'indemnisation. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 8 février 2018 et 8 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le CIVEN demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des ayants droit de Mme G... F....<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - la loi n° 2018-1317 du 28 décembre 2018 ;<br/>
              - le décret n° 2014-1049 du 15 septembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. G...F...et autres. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C...G... F..., né le 31 janvier 1931, employé par le Commissariat à l'Energie Atomique, a été affecté sur le site des expérimentations nucléaires en Polynésie française entre le 5 février et le 5 avril 1980. Il est décédé le 24 septembre 2004 d'un sarcome diagnostiqué en 2001. Mme D... E...veuve G...F..., aujourd'hui décédée, a présenté une demande d'indemnisation, sur le fondement de la loi du 5 janvier 2010 relative à la reconnaissance et l'indemnisation des victimes des essais nucléaires français, au comité d'indemnisation des victimes des essais nucléaires (CIVEN), qui a été rejetée par une décision du 9 juillet 2015, au motif que le risque imputable aux essais nucléaires dans la survenance de la maladie de M. G... F... pouvait être qualifié de négligeable. Par un jugement du 31 décembre 2015, le tribunal administratif de Rennes a rejeté la demande de Mme G...F...tendant à l'annulation de cette décision. Par un arrêt du 8 décembre 2017 contre lequel le CIVEN se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé ce jugement, en faisant application de la loi du 28 février 2017 modifiant la loi du 5 janvier 2010, et a enjoint au CIVEN de réexaminer cette demande.<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. / Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit ". L'article 2 de cette même loi définit les conditions de temps et de lieu de séjour ou de résidence que le demandeur doit remplir. Le premier alinéa du V de l'article 4 de cette loi, dans sa rédaction issue de la loi du 28 février 2017, dispose que le CIVEN "  examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité ". Enfin, les conditions de pathologie et de lieu ont été précisées par le décret du 15 septembre 2014 visé ci-dessus. Le législateur a ainsi entendu que, dès lors qu'il satisfait aux conditions de temps, de lieu et de pathologie, un demandeur bénéficie de la présomption de causalité entre l'exposition aux rayonnements ionisants dus aux essais nucléaires français et la survenance de sa maladie. Cette présomption ne peut être renversée que si l'administration établit que la pathologie de l'intéressé résulte exclusivement d'une cause étrangère à l'exposition aux rayonnements ionisants dus aux essais nucléaires, en particulier parce qu'il n'a subi aucune exposition à de tels rayonnements. Les modifications de la loi du 5 janvier 2010 introduites par la loi de finances pour 2019 du 28 décembre 2018, postérieurement à la lecture de l'arrêt attaqué, ne peuvent être utilement invoquées par le CIVEN devant le juge de cassation.<br/>
<br/>
              3. En premier lieu, il résulte des dispositions précitées de la loi du 5 janvier 2010, dans sa rédaction issue de la loi du 28 février 2017, éclairées par les travaux préparatoires de cette dernière, que pour établir la certitude d'une absence d'exposition aux rayonnements, l'administration ne peut utilement se fonder sur le fait qu'aucun tir atmosphérique ou souterrain n'aurait été réalisé durant le séjour de la victime, dès lors que celui-ci s'est déroulé durant les périodes mentionnées à l'article 2 de la loi du 5 janvier 2010. Elle ne peut pas davantage se fonder sur le lieu d'affectation de la victime, dès lors qu'il se situe à l'intérieur des zones mentionnées au même article 2, ni sur le délai entre le séjour et le diagnostic de la pathologie, dès lors que cette dernière figure sur la liste annexée au décret pris sur le fondement de l'article 1er de la même loi. Par suite, en jugeant que l'administration ne pouvait se prévaloir de la circonstance que M. G...F...n'a pas eu accès à la zone où ont été réalisés cinq tirs souterrains confinés au cours de son séjour ni du fait que sa pathologie est apparue vingt-et-un ans après son départ de Polynésie française, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En deuxième lieu, en jugeant que la circonstance que les résultats des trois dosimètres individuels et de l'examen anthroporadiométrique n'ont rien révélé d'anormal n'est pas suffisante pour établir que M. G...F...n'a subi aucune exposition aux rayonnements ionisants, la cour administrative d'appel de Nantes a suffisamment motivé son arrêt et s'est livrée à une appréciation souveraine des pièces du dossier exempte de dénaturation. <br/>
<br/>
              5. En dernier lieu, il résulte de ce qui a été dit au point 2 que la cour administrative d'appel de Nantes, après avoir constaté que toute possibilité d'exposition aux rayonnements ionisants résultant des essais nucléaires n'était pas écartée, n'a pas commis d'erreur de droit en imposant à l'administration, pour renverser la présomption de causalité prévue par la loi, d'établir que la pathologie de la victime résultait exclusivement d'une cause étrangère à l'exposition aux rayonnements ionisants dus aux essais nucléaires.<br/>
<br/>
              6. Il résulte de ce qui précède que le CIVEN n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme globale de 3 500 euros à M. G...F...et autres, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du comité d'indemnisation des victimes des essais nucléaires est rejeté.<br/>
Article 2 : L'Etat versera à MM.A..., B...et H...G...F...une somme globale de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au comité d'indemnisation des victimes des essais nucléaires et à M. A...G...F..., représentant unique pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
