<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042386654</ID>
<ANCIEN_ID>JG_L_2020_09_000000440228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/66/CETATEXT000042386654.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/09/2020, 440228, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440228.20200930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A..., à l'appui de sa demande tendant à l'annulation de la sanction que la Commission nationale des sanctions a prononcée à son encontre le 19 décembre 2018, a produit un mémoire, enregistré le 5 novembre 2019 au greffe du tribunal administratif de la Guadeloupe, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 19000463 du 21 avril 2020, enregistrée le 23 avril 2020 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de la Guadeloupe, avant qu'il soit statué sur la demande de M. A..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 561-33 du code monétaire et financier dans sa rédaction issue de la loi n° 2013-100 du 28 janvier 2013, de l'article L. 561-37 du même code dans sa rédaction issue de la loi n° 2010-476 du 12 mai 2010 et de l'article L. 561-40 du même code.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 et la loi organique n° 2020-365 du 30 mars 2020 ;<br/>
              - le code monétaire et financier ;<br/>
              -  la loi n° 2010-476 du 12 mai 2010 ;<br/>
              - la loi n° 2013-100 du 28 janvier 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... D..., conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. D'une part, le premier alinéa de l'article L. 561-33 du code monétaire et financier, dans sa rédaction issue de la loi du 28 janvier 2013 portant diverses dispositions d'adaptation de la législation au droit de l'Union européenne en matière économique et financière, dispose que " Les personnes mentionnées à l'article L. 561-2 assurent la formation et l'information régulières de leurs personnels en vue du respect des obligations prévues aux chapitres Ier et II du présent titre (...) ", lesquelles se rapportent aux obligations relatives respectivement à la lutte contre le blanchiment des capitaux et le financement du terrorisme et au gel des avoirs.<br/>
<br/>
              3. D'autre part, l'article L. 561-37 du code monétaire et financier, dans sa rédaction résultant de la loi du 12 mai 2010 relative à l'ouverture à la concurrence et à la régulation du secteur des jeux d'argent et de hasard en ligne, prévoit que " Tout manquement aux dispositions des sections 3, 4, 5 et 6 du présent chapitre par les personnes mentionnées aux 8°, 9°, 9° bis et 15° de l'article L. 561-2 est passible des sanctions prévues par l'article L. 561-40 ". Aux termes de l'article L. 561-40 du même code : " I. - La Commission nationale des sanctions peut prononcer l'une des sanctions administratives suivantes : / 1° L'avertissement ; / 2° Le blâme ; / 3° L'interdiction temporaire d'exercice de l'activité ou d'exercice de responsabilités dirigeantes au sein d'une personne morale exerçant cette activité pour une durée n'excédant pas cinq ans ; / 4° Le retrait d'agrément ou de la carte professionnelle. [...] / La commission peut prononcer, soit à la place, soit en sus de ces sanctions, une sanction pécuniaire dont le montant ne peut être supérieur à cinq millions d'euros ou, lorsque l'avantage retiré du manquement peut être déterminé, au double de ce dernier. Les sommes sont recouvrées par le Trésor public. (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions que l'article L. 561-33 du code monétaire et financier cité précédemment fait peser sur les personnes qu'il mentionne une obligation dont la méconnaissance est susceptible de donner lieu au prononcé d'une sanction ayant le caractère d'une punition.<br/>
<br/>
              5. Il appartient au législateur, en application de l'article 34 de la Constitution ainsi que du principe de la légalité des délits et des peines posé par l'article 8 de la Déclaration des droits de l'homme et du citoyen, de fixer lui-même le champ d'application de la loi pénale et de définir les crimes et délits en termes suffisamment clairs et précis pour permettre la détermination des auteurs d'infractions et d'exclure l'arbitraire dans le prononcé des peines. Cette exigence ne concerne pas seulement les peines prononcées par les juridictions répressives mais s'étend à toute sanction ayant le caractère d'une punition, même si le législateur a laissé le soin de la prononcer à une autorité de nature non juridictionnelle. Toutefois, appliquée en dehors du droit pénal, l'exigence d'une définition des infractions sanctionnées se trouve satisfaite, en matière administrative, dès lors que les textes applicables font référence aux obligations auxquelles les intéressés sont soumis en raison de l'activité qu'ils exercent, de la profession à laquelle ils appartiennent, de l'institution dont ils relèvent ou de la qualité qu'ils revêtent.<br/>
<br/>
              6. A l'appui de sa question prioritaire de constitutionnalité, M. A... soutient que les dispositions citées précédemment de l'article L. 561-33 du code monétaire et financier, sur le fondement desquelles il a fait l'objet d'une sanction prononcée par la Commission nationale des sanctions, méconnaissent le principe de légalité des délits et des peines, faute de déterminer avec suffisamment de précision le champ de l'obligation de formation et d'information régulière de leurs personnels qu'elles mettent à la charge des personnes mentionnées à l'article L. 561-2 du code monétaire et financier et dont la méconnaissance est susceptible d'être sanctionnée en application des articles L. 561-37 et L. 561-40 précités.<br/>
<br/>
              7. Toutefois, il résulte des termes mêmes de ces dispositions que celles-ci font reposer sur les personnes assujetties aux obligations de lutte contre le blanchiment des capitaux et le financement du terrorisme en application de l'article L. 561-2 du code monétaire et financier l'obligation de déterminer et de faire connaître à leurs personnels et préposés, par des actions de formation et d'information régulières et en tenant notamment compte du niveau hiérarchique et de la nature des fonctions de ces derniers, les obligations professionnelles auxquelles elles sont assujetties, en vue d'assurer le respect des obligations prévues par le code monétaire et financier en matière de lutte contre le blanchiment des capitaux et le financement du terrorisme et de gel des avoirs, l'absence de mise en oeuvre de telles actions donnant lieu aux sanctions prévues par l'article L. 561-40 du code monétaire et financier précité. Ces dispositions définissent ainsi avec suffisamment de précision le champ de l'obligation dont la méconnaissance est susceptible d'être sanctionnée en application des articles L. 561-37 et L. 561-40 du code monétaire et financier précités.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Il n'y a pas lieu, par suite, de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de la Guadeloupe. <br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre de l'économie,  des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre ainsi qu'au tribunal administratif de la Guadeloupe. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
