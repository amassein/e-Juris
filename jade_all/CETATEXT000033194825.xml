<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194825</ID>
<ANCIEN_ID>JG_L_2016_10_000000389822</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194825.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 05/10/2016, 389822, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389822</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:389822.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Me  B..., agissant en qualité de liquidateur judiciaire de l'EURL Pénélope, a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires d'impôt sur les sociétés et des rappels de taxe sur la valeur ajoutée auxquels l'EURL Pénélope a été assujettie respectivement au titre de l'exercice clos en 2007 et au titre de la période du 26 avril 2006 au 30 juin 2007. Par un jugement n° 1101620 du 5 novembre 2013, le tribunal administratif de Châlons-en-Champagne a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 13NC02191 du 5 mars 2015, la cour administrative d'appel de Nancy a, sur appel du ministre des finances et des comptes publics, annulé ce jugement et remis Pénélope les impositions litigieuses à la charge de l'EURL Pénélope. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 avril et 27 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Me A...C..., agissant en qualité de liquidateur judiciaire de l'EURL Pénélope, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre des finances et des comptes publics ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Me C...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'EURL Pénélope, exploitante d'une discothèque, a fait l'objet d'une vérification de comptabilité portant, en matière de taxe sur le chiffre d'affaires, sur la période du 26 avril 2006 au 30 juin 2007 et, en matière d'impôt sur les sociétés, sur l'exercice clos en 2007 et qu'après avoir écarté comme non probante la comptabilité présentée par l'EURL Pénélope, l'administration a reconstitué le chiffre d'affaires de la discothèque en faisant application de la méthode dite de "comptabilité matière". MeC..., agissant en qualité de liquidateur judiciaire de l'EURL Pénélope, se pourvoit en cassation contre l'arrêt du 5 mars 2015 par lequel la cour administrative d'appel de Nancy a fait droit à l'appel formé par le ministre délégué, chargé du budget, contre le jugement du 5 novembre 2013 par lequel le tribunal administratif de Châlons-en-Champagne a déchargé l'EURL Pénélope des impositions litigieuses ainsi que des pénalités correspondantes, au motif que la méthode de reconstitution des recettes retenue par l'administration était viciée dans son principe. <br/>
<br/>
              2. Aux termes de l'article L. 192 du livre des procédures fiscales : " Lorsque l'une des commissions visées à l'article L. 59 est saisie d'un litige ou d'une rectification, l'administration supporte la charge de la preuve en cas de réclamation, quel que soit l'avis rendu par la commission. / Toutefois, la charge de la preuve incombe au contribuable lorsque la comptabilité comporte de graves irrégularités et que l'imposition a été établie conformément à l'avis de la commission. La charge de la preuve des graves irrégularités invoquées par l'administration incombe, en tout état de cause, à cette dernière lorsque le litige ou la rectification est soumis au juge. / Elle incombe également au contribuable à défaut de comptabilité ou de pièces en tenant lieu, comme en cas de taxation d'office à l'issue d'un examen contradictoire de la situation fiscale personnelle en application des dispositions des articles L. 16 et L. 69. ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a relevé que l'administration s'était fondée sur les " tickets Z " pour déterminer, à partir du nombre total des ventes d'alcools à la bouteille ou au verre, un ratio de 13 % pour les consommations à la bouteille et de 87 %  pour les consommations au verre, qu'elle a appliqué aux seuls alcools vendus à la fois à la bouteille et au verre. Après avoir relevé que le requérant n'établissait pas que le ratio avait été calculé à partir de données autres que ce qui provenait des " tickets Z ", la cour a jugé qu'il n'était pas fondé à soutenir que la méthode de reconstitution de recettes mise en oeuvre par le service était entachée d'une telle imprécision qu'elle serait viciée dans son principe. En statuant ainsi, alors que le ratio indiqué ci-dessus, calculé en pourcentage du nombre de ventes, avait été appliqué à la variation des stocks de l'entreprise sans tenir compte des volumes respectifs d'un verre et d'une bouteille ni de la nature des boissons vendues, la cour a donné aux faits qu'elle a relevés une qualification juridique inexacte. Le requérant est par suite, sans qu'il soit nécessaire de statuer sur les autres moyens du pourvoi, fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 500 euros à verser au requérant au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 5 mars 2015 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat versera une somme de 3 500 euros à Me C...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Me A...C...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
