<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815851</ID>
<ANCIEN_ID>JG_L_2019_07_000000426964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815851.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 24/07/2019, 426964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426964.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 9 janvier et 18 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. E...A...D...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 12 juillet 2018 rapportant le décret du 28 décembre 2015 en ce qu'il lui avait accordé la nationalité française ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de lui rétablir le bénéfice de la nationalité française ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. A...D..., ressortissant marocain, a déposé une demande de naturalisation le 12 mai 2015 dans laquelle il a déclaré être divorcé et s'est engagé sur l'honneur à signaler à l'administration chargée d'instruire sa demande tout changement qui viendrait à survenir dans sa situation personnelle et familiale. M. A... D...a été naturalisé par un décret du 28 décembre 2015. Toutefois, par bordereau reçu le 13 juillet 2016, le ministre des affaires étrangères et du développement international a informé le ministre de l'intérieur que M. A...D...avait épousé au Maroc, le 4 août 2014, Mme B...C..., ressortissante marocaine résidant au Maroc. Par un décret du 12 juillet 2018, publié au journal officiel le 13 juillet 2018, le Premier ministre a rapporté le décret prononçant la naturalisation de M. A...D...au motif qu'il avait été pris sur la base d'informations mensongères délivrées par l'intéressé sur sa situation familiale.<br/>
<br/>
              3.	Aux termes de l'article 21-16 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition se trouve remplie, l'autorité administrative peut notamment prendre en compte la situation familiale en France de l'intéressé. Par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressé ait dissimulé s'être marié au Maroc était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              4.	En premier lieu, il ressort des pièces et faits du dossier que le mariage contracté au Maroc le 4 août 2014 par M. A... D..., avant le dépôt de sa demande de naturalisation, est un fait qu'il aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, ce qu'il n'a pas fait contrairement à l'engagement qu'il avait pris. Si M. A... D...soutient qu'il est de bonne foi, il ne fait état d'aucune circonstance qui l'aurait mis dans l'impossibilité de faire part de sa situation matrimoniale au service chargé de l'instruction de son dossier avant l'intervention du décret lui accordant la nationalité française. Au demeurant, il est constant que ses démarches en vue d'obtenir le divorce n'ont pas abouti dès lors qu'il a volontairement renoncé à ce divorce le 28 mai 2015, date à laquelle il a appris que son épouse était enceinte. En outre, au cours de l'entretien qui s'est déroulé à la préfecture de la Seine-Saint-Denis le 12 mai 2015, il n'a mentionné, au titre des attaches qu'il conservait avec son pays d'origine, que ses parents, ses frères et soeurs, ainsi que son enfant. L'intéressé, qui maîtrise parfaitement la langue française, ainsi qu'il ressort du procès-verbal d'assimilation établi à l'issue de cet entretien, ne pouvait se méprendre sur la portée de ses obligations et doit être regardé comme ayant volontairement dissimulé sa situation matrimoniale. Par suite, en rapportant le décret naturalisant M. A...D...dans le délai de deux ans à compter de la découverte de la fraude, le ministre de l'intérieur n'a pas commis d'erreur manifeste dans l'appréciation de la situation de M. A...D....<br/>
<br/>
              5.	En deuxième lieu, un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M.D.... <br/>
<br/>
              6.	Il résulte de ce qui précède que M. A... D...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 12 juillet 2018 rapportant le décret du 28 décembre 2015 qui lui avait accordé la nationalité française. Par suite, ses conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...D...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. E...A...D...et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
