<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077300</ID>
<ANCIEN_ID>JG_L_2019_01_000000375020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/01/2019, 375020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:375020.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 29 juin 2016, le Conseil d'Etat statuant au contentieux, avant de statuer sur les conclusions de la société d'aménagement du domaine de Château Barrault tendant à la condamnation de l'Etat et de la commune de Cursan à lui verser une indemnité en réparation des préjudices subis du fait de la modification, en 2005, des règles d'urbanisme applicables dans la commune a ordonné une expertise en vue d'évaluer le montant des préjudices subis par la société et résultant du montant des dépenses exposées en pure perte pour la réalisation de l'opération d'aménagement envisagée et de la perte de la valeur vénale des terrains dont elle est propriétaire.<br/>
<br/>
              L'expert a déposé son rapport le 17 octobre 2018.<br/>
<br/>
              Par un mémoire, enregistré au secrétariat de la section du contentieux le 9 novembre 2018, le ministre d'Etat, ministre de la transition écologique et solidaire produit ses observations sur le rapport d'expertise.<br/>
<br/>
              Par des mémoires, enregistrés les 19 novembre et 10 décembre 2018, la commune de Cursan produit des observations sur le rapport d'expertise et demande qu'une somme de 5 000 euros soit mise à la charge de la société d'aménagement du domaine du Château Barrault au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu  le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Château Barrault et de la société d'aménagement du domaine de Château Barrault, à la SCP Delvolvé et Trichet, avocat de la mairie de Cursan et à la SCP Lyon-Caen, Thiriez, avocat du ministre d'Etat, ministre de la transition écologique et solidaire ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 29 juin 2016, le Conseil d'Etat statuant au contentieux a déclaré l'Etat et la commune de Cursan responsables à parts égales de la moitié des préjudices subis par la société d'aménagement du domaine de Château Barrault et résultant des dépenses exposées en pure perte pour la réalisation de l'opération d'aménagement qu'elle envisageait sur le territoire de la commune de Cursan et de la perte de la valeur vénale des terrains dont elle est propriétaire et, avant de statuer sur les conclusions de la société d'aménagement du domaine de Château Barrault tendant à la condamnation de l'Etat et de la commune de Cursan à lui verser une indemnité en réparation des préjudices subis du fait de la modification, en 2005, des règles d'urbanisme applicables dans la commune, a ordonné une expertise en vue d'évaluer le montant de ces préjudices. L'expert a déposé son rapport le 17 octobre 2018.<br/>
<br/>
              Sur le préjudice résultant des dépenses exposées en pure perte :<br/>
<br/>
              2. Il résulte de l'instruction, notamment des factures produites par la société requérante et des conclusions du rapport d'expertise, que la société d'aménagement du domaine de Château Barrault a engagé, en 1990 et 1991, des dépenses d'un montant total de 138 305,70 euros pour la réalisation de l'opération d'aménagement projetée. La circonstance que les factures produites ne comportent pas la mention " payée " ou que l'une d'entre elle soit entachée d'une erreur matérielle ne fait pas obstacle à ce qu'elles soient prises en compte pour la détermination du préjudice subi dès lors qu'il ne ressort pas des pièces du dossier que ces factures n'auraient pas été régulièrement acquittées.  Par ailleurs, il est constant que la société requérante a versé une somme totale de 1 205 000 francs, soit 183 701,07 euros, au titre de l'assainissement et du raccordement en eau du terrain dont elle était propriétaire. La circonstance qu'il ait été jugé par la cour administrative d'appel de Bordeaux, dont l'arrêt n'a pas été annulé sur ce point, que l'action en restitution engagée par la société afin d'obtenir le remboursement de cette somme était prescrite, ne fait pas obstacle à ce que les dépenses correspondantes soient prises en compte pour l'évaluation du préjudice subi du fait des dépenses exposées en pure perte pour la réalisation du projet.<br/>
<br/>
              3. En revanche, il n'y a pas lieu de tenir compte, dans l'évaluation de ce préjudice, des intérêts d'emprunts supportés par la société requérante dès lors que ces intérêts n'ont pas été exposés en vue de la réalisation de l'opération d'aménagement envisagée mais sont liés à l'acquisition du terrain et à la qualité de propriétaire de la société qui ne justifie, par ailleurs, pas avoir eu à supporter des intérêts supplémentaires du fait de l'impossibilité dans laquelle elle s'est trouvée de réaliser l'opération.<br/>
<br/>
              Sur le préjudice résultant de la perte de valeur vénale des terrains :<br/>
<br/>
              4. Il résulte de l'instruction, et notamment des conclusions circonstanciées du rapport d'expertise, que la valeur vénale des terrains dont la société d'aménagement du domaine de Château Barrault est propriétaire sur le territoire de la commune de Cursan pouvait être évaluée à la somme de  4 523 000 euros, en juin 2005, avant l'adoption de la carte communale, et que leur valeur résiduelle, après l'adoption de cette carte, n'était plus que de 14 500 euros aboutissant à une perte de valeur vénale de 4 508 500 euros. <br/>
<br/>
              5. Il résulte de tout ce qui précède qu'il peut être fait une juste appréciation du préjudice total subi par la société requérante en le fixant à la somme de 6 000 000 euros, tous intérêts compris au jour de la présente décision. Eu égard au partage de responsabilité opéré par la décision du 29 juin 2016 mentionnée au point 1, il y a lieu, dès lors, de condamner l'Etat et la commune de Cursan à verser à la société d'aménagement du domaine de Château Barrault la somme totale de 3 000 000 euros, tous intérêts compris à la date du présent jugement, soit la somme de 1 500 000 euros chacun.<br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, d'une part, de mettre les frais de l'expertise ordonnée par le Conseil d'Etat pour moitié à la charge de l'Etat et pour moitié à la charge de la commune de Cursan et, d'autre part, de mettre une somme de 10 000 euros, pour moitié à la charge de l'Etat et pour moitié à la charge de la commune de Cursan, à verser à la société d'aménagement du domaine de Château Barrault au titre de l'article L. 761-1 du code de justice administrative, pour l'ensemble de la procédure. Les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société d'aménagement du domaine de Château Barrault qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'Etat et la commune de Cursan sont condamnés à verser à la société d'aménagement du domaine de Château Barrault, pour moitié chacun, la somme de 3 000 000 euros tous intérêts compris.<br/>
Article 2 : Les frais d'expertise sont mis, pour moitié, à la charge de l'Etat, pour moitié, à la charge de la commune de Cursan.<br/>
Article 3 : L'Etat et la commune de Cursan verseront à la société d'aménagement du domaine de Château Barrault, pour moitié chacun, une somme de 10 000 euros, au titre de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure.<br/>
Article 4 : Le surplus des conclusions présentées par la société d'aménagement du domaine de Château Barrault est rejeté.<br/>
Article 5 : Les conclusions présentées par la commune de Cursan au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la Société d'aménagement du domaine de Château Barrault, à la commune de Cursan, au ministre d'Etat, ministre de la transition écologique et solidaire et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée à M. B...A..., expert.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
