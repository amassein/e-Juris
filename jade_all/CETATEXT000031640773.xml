<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640773</ID>
<ANCIEN_ID>JG_L_2015_12_000000387815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/07/CETATEXT000031640773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 16/12/2015, 387815</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:387815.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 9 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite du 26 novembre 2014 par laquelle le Premier ministre a refusé d'abroger le deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger le deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite dans un délai de trois mois ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ; <br/>
              - le décret n° 2010-1741 du 30 décembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 24 du code des pensions civiles et militaires de retraite, issu de la loi du 9 novembre 2010 : " I. - La liquidation de la pension intervient : (...) 3° Lorsque le fonctionnaire civil est parent d'un enfant vivant, âgé de plus d'un an et atteint d'une invalidité égale ou supérieure à 80 %, à condition qu'il ait, pour cet enfant, interrompu ou réduit son activité dans des conditions fixées par décret en Conseil d'Etat et qu'il ait accompli quinze années de services effectifs. (...) Sont assimilés à l'enfant mentionné au premier alinéa les enfants énumérés au II de l'article L. 18 que l'intéressé a élevés dans les conditions prévues au III dudit article. Les conditions d'ouverture du droit liées à l'enfant doivent être remplies à la date de la demande de pension " ; qu'aux termes de l'article R. 37 du même code, tel que modifié par le décret du 30 décembre 2010 portant application aux fonctionnaires, aux militaires et aux ouvriers des établissements industriels de l'Etat des articles 44 et 52 de la loi n° 2010-1330 du 9 novembre 2010 portant réforme des retraites, applicable au litige : " I. - L'interruption d'activité prévue au premier alinéa du 3° du I et au premier alinéa du 1 bis du II de l'article L. 24 doit avoir eu une durée continue au moins égale à deux mois et être intervenue alors que le fonctionnaire ou le militaire était affilié à un régime de retraite obligatoire. La réduction d'activité prévue au même article doit avoir eu une durée continue au moins égale à celle mentionnée au II bis du présent article. / Cette interruption ou réduction d'activité doit avoir eu lieu pendant la période comprise entre le premier jour de la quatrième semaine précédant la naissance ou l'adoption et le dernier jour du trente-sixième mois suivant la naissance ou l'adoption. / Par dérogation aux dispositions de l'alinéa précédent, pour les enfants énumérés aux troisième, quatrième, cinquième et sixième alinéas du II de l'article L. 18 que l'intéressé a élevés dans les conditions prévues au III dudit article, l'interruption ou la réduction d'activité doit intervenir soit avant leur seizième anniversaire, soit avant l'âge où ils ont cessé d'être à charge au sens des articles L. 512-3 et R. 512-2 à R. 512-3 du code de la sécurité sociale. (...) " ; <br/>
<br/>
              2. Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées du deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite que le bénéfice d'un départ anticipé à la retraite avec jouissance immédiate, tel que défini à l'article L. 24 du même code, est conditionné à une interruption ou une réduction d'activité du parent fonctionnaire durant les trois ans suivant la naissance de l'enfant handicapé ; que la différence de traitement qui résulte de ces dispositions réglementaires entre les parents d'un enfant handicapé qui ont réduit ou interrompu leur activité avant que leur enfant ait atteint l'âge de trois ans et ceux qui ont réduit ou interrompu leur activité après que leur enfant a atteint cet âge alors  qu'il est encore à leur charge, ne se ne se justifie ni par un motif d'intérêt général, ni par une différence de situation au regard des préjudices de carrière liées à la charge supplémentaire qu'impose l'éducation d'un enfant handicapé, que la mesure vise à compenser ; qu'il suit de là que les dispositions réglementaires contestées méconnaissent le principe d'égalité en excluant du bénéfice du départ anticipé à la retraite avec jouissance immédiate les parents d'enfants handicapés ayant interrompu ou réduit leur activité après que leur enfant handicapé a atteint  trois ans et alors  qu'il est encore à leur charge ;  <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le requérant est fondé à demander l'annulation de la décision implicite du 26 novembre 2014 par laquelle le Premier ministre a refusé d'abroger le deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite ; que cette annulation implique nécessairement l'abrogation des dispositions réglementaires dont l'illégalité a été constatée ; qu'il y a lieu pour le Conseil d'Etat d'ordonner cette mesure dans un délai de six mois à compter de la présente décision ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite du Premier ministre du 26 septembre 2014 est annulée. <br/>
<br/>
Article 2 : Il est enjoint au Premier ministre d'abroger le deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite dans un délai de six mois à compter de la présente décision. <br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au Premier ministre et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS. - DÉPART À LA RETRAITE ANTICIPÉ AVEC JOUISSANCE IMMÉDIATE DE LA PENSION POUR LE PARENT D'UN ENFANT HANDICAPÉ AYANT INTERROMPU OU RÉDUIT SON ACTIVITÉ (3° DE L'ART. L. 24 DU CPCMR) - LIMITATION, PAR LE I DE L'ARTICLE R. 37 DU CPCMR, AUX PARENTS AYANT INTERROMPU OU RÉDUIT LEUR ACTIVITÉ AVANT LES TROIS ANS DE L'ENFANT - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-05 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AVANTAGES FAMILIAUX. - DÉPART ANTICIPÉ À LA RETRAITE AVEC JOUISSANCE IMMÉDIATE DE LA PENSION POUR LE PARENT D'UN ENFANT HANDICAPÉ AYANT INTERROMPU OU RÉDUIT SON ACTIVITÉ (3° DE L'ART. L. 24 DU CPCMR) - LIMITATION, PAR LE I DE L'ARTICLE R. 37 DU CPCMR, AUX PARENTS AYANT INTERROMPU OU RÉDUIT LEUR ACTIVITÉ AVANT LES TROIS ANS DE L'ENFANT - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ.
</SCT>
<ANA ID="9A"> 01-04-03-03-02 Il résulte des dispositions du deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite (CPCMR) que le bénéfice d'un départ anticipé à la retraite avec jouissance immédiate, tel que défini à l'article L. 24 du même code, est conditionné à une interruption ou une réduction d'activité du parent fonctionnaire durant les trois ans suivant la naissance de l'enfant handicapé. La différence de traitement qui résulte de ces dispositions réglementaires entre les parents d'un enfant handicapé qui ont réduit ou interrompu leur activité avant que leur enfant ait atteint l'âge de trois ans et ceux qui ont réduit ou interrompu leur activité après que leur enfant a atteint cet âge alors qu'il est encore à leur charge, ne se justifie ni par un motif d'intérêt général, ni par une différence de situation au regard des préjudices de carrière liées à la charge supplémentaire qu'impose l'éducation d'un enfant handicapé, que la mesure vise à compenser. Il suit de là que le deuxième alinéa du I de l'article R. 37 du CPCMR méconnaît le principe d'égalité en excluant du bénéfice du départ anticipé à la retraite avec jouissance immédiate les parents d'enfants handicapés ayant interrompu ou réduit leur activité après que leur enfant handicapé a atteint  trois ans et alors  qu'il est encore à leur charge.</ANA>
<ANA ID="9B"> 48-02-01-05 Il résulte des dispositions du deuxième alinéa du I de l'article R. 37 du code des pensions civiles et militaires de retraite (CPCMR) que le bénéfice d'un départ anticipé à la retraite avec jouissance immédiate, tel que défini à l'article L. 24 du même code, est conditionné à une interruption ou une réduction d'activité du parent fonctionnaire durant les trois ans suivant la naissance de l'enfant handicapé. La différence de traitement qui résulte de ces dispositions réglementaires entre les parents d'un enfant handicapé qui ont réduit ou interrompu leur activité avant que leur enfant ait atteint l'âge de trois ans et ceux qui ont réduit ou interrompu leur activité après que leur enfant a atteint cet âge alors qu'il est encore à leur charge, ne se justifie ni par un motif d'intérêt général, ni par une différence de situation au regard des préjudices de carrière liées à la charge supplémentaire qu'impose l'éducation d'un enfant handicapé, que la mesure vise à compenser. Il suit de là que le deuxième alinéa du I de l'article R. 37 du CPCMR méconnaît le principe d'égalité en excluant du bénéfice du départ anticipé à la retraite avec jouissance immédiate les parents d'enfants handicapés ayant interrompu ou réduit leur activité après que leur enfant handicapé a atteint  trois ans et alors  qu'il est encore à leur charge.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
