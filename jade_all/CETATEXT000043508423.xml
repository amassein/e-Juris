<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043508423</ID>
<ANCIEN_ID>JG_L_2021_04_000000451563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/50/84/CETATEXT000043508423.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/04/2021, 451563, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451563.20210428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 11 et 23 avril 2021 au secrétariat du contentieux du Conseil d'Etat, Mme K... I... H..., Mme J... I... F..., M. D... G..., Mme A... C... et Mme B... E... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du 25 janvier 2021 du ministre des solidarités et de la santé et de la ministre de l'enseignement supérieur, de la recherche et de l'innovation fixant le nombre d'étudiants de première année commune aux études de sante´ autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique à la rentrée universitaire 2021-2022 ;<br/>
<br/>
              2°) d'enjoindre aux ministres des solidarités et de la santé et de l'enseignement supérieur, de la recherche et de l'innovation de fixer le nombre d'étudiants de première année commune aux études de sante´ autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique à la rentrée universitaire 2021-2022 en tenant compte, pour chaque filière, des capacités d'accueil de chaque université et du nombre total des candidats incluant les bacheliers inscrits en parcours " accès santé " spécifique et en licence avec une option " accès santé " ;<br/>
<br/>
              3°) à titre subsidiaire, d'ordonner la suspension de l'exécution de l'arrêté interministériel du 25 janvier 2021 en tant seulement qu'il concerne l'université de Montpellier et d'enjoindre aux ministres des solidarités et de la santé et de l'enseignement supérieur, de la recherche et de l'innovation de fixer le nombre d'étudiants de première année commune aux études de sante´ autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique à la rentrée universitaire 2021-2022 à l'université de Montpellier en tenant compte, pour chaque filière, de ses capacités d'accueil et du nombre total des candidats incluant les bacheliers inscrits en parcours " accès santé " spécifique et en licence avec une option " accès santé "; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que l'exécution de l'arrêté contesté laisse un nombre de places résiduel insuffisant aux étudiants de première année non redoublants, compromettant de façon grave et immédiate leur passage en deuxième année des études de santé, alors que les examens et concours doivent se dérouler en mai 2021 ;<br/>
              - il existe un doute sérieux sur la légalité de l'arrêté contesté compte tenu de l'illégalité de l'article 6 du décret du 4 novembre 2019 relatif à l'accès aux formations de médecine, de pharmacie, d'odontologie et de maïeutique qui, en premier lieu, méconnaît les dispositions du VII de l'article 1er de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé réservant l'application des dispositions transitoires aux étudiants ayant suivi une première année commune aux études de sante´ avant la publication de cette loi, en deuxième lieu, est entaché d'incompétence au regard de l'article 34 de la Constitution réservant au législateur la possibilité d'instaurer un concours d'accès aux études de santé, en troisième lieu, méconnaît le principe d'égalité en prévoyant une formation distincte au bénéfice des étudiants ayant antérieurement suivi une première année commune aux études de santé, avec un concours propre et des places qui leur sont réservées en deuxième année et, en dernier lieu, omet de prévoir les critères de détermination du nombre de ces places ;<br/>
              - l'arrêté contesté est entaché d'erreur manifeste d'appréciation, en premier lieu, en ce qu'il a été pris avant que les capacités d'accueil aient été définies par les universités et sans qu'elles aient pu l'être au vu des objectifs pluriannuels mentionnés à l'article L. 631-1 du code de l'éducation, en lien avec l'agence régionale de santé et sur proposition de la conférence nationale associant des représentants des acteurs du système de santé prévue à l'article R. 631 1 6 du même code et, en second lieu, en ce qu'il conduit à réserver la grande majorité des places en deuxième année des études de santé aux étudiants ayant redoublé la première année commune aux études de sante´, en méconnaissance du principe d'égalité entre les étudiants souhaitant poursuivre des études de santé et sans considération de l'absence de possibilité de redoublement pour les autres étudiants de première année, inscrits en parcours " accès santé " spécifique et en licence avec une option " accès santé ".  <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 21 et 22 avril 2021, la ministre de l'enseignement supérieur, de la recherche et de l'innovation conclut au rejet de la requête. Elle soutient que la requête est irrecevable, que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est de nature à créer un doute sérieux sur la légalité de l'arrêté contesté. <br/>
<br/>
              La requête a été communiquée au ministre des solidarités et de la santé, qui n'a pas produit de mémoire. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de l'éducation ; <br/>
              - la loi n° 2019-774 du 24 juillet 2019 ; <br/>
              - le décret n° 2019-1125 du 4 novembre 2019 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les requérants et d'autre part, le ministre des solidarités et de la santé et la ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 23 avril 2021, à 14 heures 30 : <br/>
              - Me Chevallier, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
              - la représentante des requérants ; <br/>
              - les représentantes de la ministre de l'enseignement supérieur, de la recherche et de l'innovation ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 26 avril 2021 à 16 heures.<br/>
<br/>
              Vu les deux notes en délibéré, enregistrées le 26 avril 2021 après la clôture de l'instruction, présentées par les requérants ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 avril 2021 après la clôture de l'instruction, présentée par la ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Les requérants, étudiants en parcours " accès santé " spécifique, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 25 janvier 2021 du ministre des solidarités et de la santé et de la ministre de l'enseignement supérieur, de la recherche et de l'innovation fixant le nombre d'étudiants de première année commune aux études de santé autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique en deuxième année de premier cycle à la rentrée universitaire 2021-2022. <br/>
<br/>
              3. En premier lieu, il est constant que l'arrêté contesté du 25 janvier 2021 s'applique aux étudiants inscrits en première année commune aux études de santé en 2020-2021, et non aux requérants, actuellement en parcours " accès santé " spécifique. Toutefois, il résulte de l'instruction que le nombre des places réservées en application de l'arrêté contesté aux étudiants actuellement en première année commune aux études de sante´ s'impute, de fait, pour la rentrée universitaire 2021-2022, sur le nombre des places disponibles en deuxième année de premier cycle en médecine, maïeutique, odontologie et pharmacie. Dans ces conditions, l'arrêté contesté a pour effet de laisser un nombre de places résiduel aux étudiants actuellement en parcours " accès santé " spécifique souhaitant poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique et affecte donc de façon directe et certaine les intérêts des requérants.<br/>
<br/>
              4. En deuxième lieu, le moyen tiré de ce que les ministres des solidarités et de la santé et de l'enseignement supérieur, de la recherche et de l'innovation ont entaché l'arrêté contesté d'erreur manifeste d'appréciation en se fondant uniquement sur des taux de réussite constatés par le passé pour fixer le nombre des étudiants en première année commune aux études de santé autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique lors de l'année universitaire 2021-2022, sans prendre en considération les places disponibles en deuxième année de premier cycle dans chacune de ces filières pour l'année universitaire 2021-2022 ni, par suite, le sort des étudiants ayant suivi en 2020-2021 le nouveau parcours " accès santé " spécifique ou une année de licence avec une option " accès santé ", est, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité de cet arrêté.<br/>
<br/>
              5. En dernier lieu, l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications apportées par le requérant, si les effets de l'acte en litige sont de nature à caractériser une urgence justifiant que, sans attendre le jugement au fond, l'exécution de la décision soit suspendue. <br/>
<br/>
              6. Au vu de l'imminence des décisions d'admission en deuxième année de premier cycle en médecine, maïeutique, odontologie et pharmacie pour l'année universitaire 2021-2022, l'exécution de l'arrêté contesté affecterait de façon grave et immédiate les intérêts des requérants, tandis que, contrairement à ce qui est allégué en défense, sa suspension ne conduirait pas à invalider la formation suivie par les étudiants en première année commune aux études de santé, ni les épreuves qu'ils ont subies ou qui doivent encore se dérouler au mois de mai. La condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit donc en l'espèce être regardée comme remplie. <br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens soulevés, que les requérants sont fondés à demander la suspension de l'arrêté interministériel du 25 janvier 2021 contesté. En revanche, la présente décision n'implique pas qu'il soit enjoint au ministre des solidarités et de la santé et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation d'édicter un nouvel arrêté dans un sens déterminé, si bien que les conclusions à fin d'injonction doivent être rejetées.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 500 euros à verser aux requérants, au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'exécution de l'arrêté du 25 janvier 2021 du ministre des solidarités et de la santé et de la ministre de l'enseignement supérieur, de la recherche et de l'innovation fixant le nombre d'étudiants de première année commune aux études de santé autorisés à poursuivre leurs études en médecine, odontologie, pharmacie et maïeutique a` la rentrée universitaire 2021-2022 est suspendue.<br/>
<br/>
Article 2 : L'Etat versera la somme de 500 euros aux requérants au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 4 : La présente ordonnance sera notifiée à Mme K... I... H..., première dénommée, pour l'ensemble des requérants, à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
