<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074605</ID>
<ANCIEN_ID>JG_L_2020_06_000000421002</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/06/2020, 421002, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421002</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:421002.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              Par une décision du 28 novembre 2018, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme D... B... et de M. E... A... dirigées contre l'arrêt n° 15LY02500, 15LY02581 du 29 mars 2018 de la cour administrative d'appel de Lyon en tant qu'il se prononce sur l'évaluation des préjudices de leur fils, M. C... A..., au titre des frais d'assistance par tierce personne.<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 mars 2019, le centre hospitalier de Vienne conclut au rejet du pourvoi. Il soutient que les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 10 décembre 2019, Mme B... et M. A... reprennent les conclusions de leur pourvoi avec les mêmes moyens.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ; <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme B... et de M. A... et à Me Le Prado, avocat du centre hospitalier de Vienne.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à sa naissance le 28 février 2007 au centre hospitalier de Vienne, M. C... A... a développé une infection septique et méningée.  Imputant à des fautes du centre hospitalier les troubles neurologiques sévères ainsi que les troubles visuels, cognitifs et intellectuels dont leur fils est atteint depuis lors, Mme B... et M. A... ont recherché la responsabilité de l'établissement devant le tribunal administratif de Grenoble. Par un jugement du 12 mai 2015, le tribunal a jugé que les fautes commises par le centre hospitalier de Vienne lors de l'accouchement de Mme B... ont été à l'origine d'une perte de chance pour l'enfant d'échapper à son infirmité qu'il a évaluée à 80 % et a, par suite, condamné l'établissement à indemniser les requérants, en leur qualité de représentants légaux de leur enfant et en leur nom propre. Sur appels du centre hospitalier de Vienne et de Mme B... et M. A..., la cour administrative d'appel de Lyon a, par un arrêt du 29 mars 2018, retenu l'existence de fautes ayant entraîné une perte de chance d'échapper aux dommages de 75 %, modifié le montant des indemnités et de la rente mises à la charge du centre hospitalier et prévu que les sommes dues à l'enfant seraient versées sous déduction des sommes perçues au titre de la prestation de compensation du handicap. Par le présent pourvoi, Mme B... et M. A... demandent l'annulation de cet arrêt en tant qu'il se prononce sur l'évaluation des préjudices au titre des frais d'assistance par tierce personne.<br/>
<br/>
              2. En vertu des principes qui régissent l'indemnisation par une personne publique des victimes d'un dommage dont elle doit répondre, il y a lieu de déduire de l'indemnisation allouée à la victime d'un dommage corporel au titre des frais d'assistance par une tierce personne le montant des prestations dont elle bénéficie par ailleurs et qui ont pour objet la prise en charge de tels frais. Il en est ainsi alors même que les dispositions en vigueur n'ouvrent pas à l'organisme qui sert ces prestations un recours subrogatoire contre l'auteur du dommage. La déduction n'a toutefois pas lieu d'être lorsqu'une disposition particulière permet à l'organisme qui a versé la prestation d'en réclamer le remboursement au bénéficiaire s'il revient à meilleure fortune.<br/>
<br/>
              3. Les règles rappelées au point précédent ne trouvent à s'appliquer que dans la mesure requise pour éviter une double indemnisation de la victime. Par suite, lorsque la personne publique responsable n'est tenue de réparer qu'une fraction du dommage corporel, notamment parce que la faute qui lui est imputable n'a entraîné qu'une perte de chance d'éviter ce dommage, la déduction ne se justifie, le cas échéant, que dans la mesure nécessaire pour éviter que le montant cumulé de l'indemnisation et des prestations excède le montant total des frais d'assistance par une tierce personne.<br/>
<br/>
              4. Aux termes de l'article L. 245-1 du code de l'action sociale et des familles : " Toute personne handicapée résidant de façon stable et régulière en France (...) dont le handicap répond à des critères définis par décret prenant notamment en compte la nature et l'importance des besoins de compensation au regard de son projet de vie, a droit à une prestation de compensation (...) ". Aux termes de l'article L. 245-3 du même code : " La prestation de compensation peut être affectée, dans des conditions définies par décret, à des charges : / 1° liées à un besoin d'aides humaines y compris, le cas échéant, celles apportées par des aidants familiaux (...) ". Aux termes de son article L. 245-4 : " L'élément de la prestation relevant du 1° de l'article L. 245-3 est accordé à toute personne handicapée (...)  lorsque son état nécessite l'aide effective d'une tierce personne pour les actes essentiels de l'existence ou requiert une surveillance régulière (..). Le montant attribué à la personne handicapée est évalué en fonction du nombre d'heures de présence requis par sa situation et fixé en équivalent-temps plein, en tenant compte du coût réel de rémunération des aides humaines en application de la législation du travail et de la convention collective en vigueur. ". Enfin, aux termes de l'article L. 245-7 du même code : " (...) Les sommes versées au titre de cette prestation ne font pas l'objet d'un recouvrement à l'encontre du bénéficiaire lorsque celui-ci est revenu à meilleure fortune (...) ".<br/>
<br/>
              5. Il résulte des dispositions citées ci-dessus que le montant de la prestation de compensation du handicap peut être déduit d'une rente ou indemnité allouée au titre de l'assistance par tierce personne. Ainsi qu'il a été dit au point 3, lorsque l'auteur de la faute n'est tenu de réparer qu'une fraction du dommage corporel, cette déduction n'a lieu d'être que lorsque le montant cumulé de l'indemnisation incombant normalement au responsable et de l'allocation ainsi que de son complément excède le montant total des frais d'assistance par tierce personne. L'indemnisation doit alors être diminuée du montant de cet excédent.<br/>
<br/>
              6. Par suite, en jugeant que les sommes perçues par les requérants au titre de la prestation de compensation du handicap seraient intégralement déduites de l'indemnité qu'elle a allouée à leur enfant au titre des frais passés d'assistance par une tierce personne et de la rente qu'elle lui a accordée au titre des frais futurs, sans limiter cette déduction au montant permettant d'éviter que le cumul des prestations et de l'indemnisation mise à la charge du centre hospitalier excède le montant total des frais d'assistance, la cour administrative d'appel de Lyon a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'arrêt attaqué doit être annulé en tant qu'il statue sur l'évaluation des préjudices subis par Thibaut A... au titre de son besoin d'assistance par une tierce personne. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Vienne une somme de 1 500 euros à verser à Mme B... et une somme de 1 500 euros à verser M. A..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 15LY02500, 15LY02581 du 29 mars 2018 de la cour administrative d'appel de Lyon est annulé en tant qu'il statue sur l'évaluation des préjudices de Thibaut A... au titre des frais d'assistance par tierce personne.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le centre hospitalier de Vienne versera à Mme B... et M. A... une somme de 1 500 euros chacun, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme D... B..., à M. E... A... et au centre hospitalier de Vienne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
