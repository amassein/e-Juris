<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039120964</ID>
<ANCIEN_ID>JG_L_2019_09_000000416528</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/12/09/CETATEXT000039120964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 18/09/2019, 416528, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416528</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416528.20190918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir la décision du 5 novembre 2013 par laquelle la ministre des affaires sociales et de la santé, après avoir procédé au réexamen de sa demande d'autorisation d'exercer en France la profession de médecin dans la spécialité " médecine générale " en exécution de l'arrêt n° 12MA01284 du 25 juin 2013 de la cour administrative d'appel de Marseille, a refusé de lui délivrer cette autorisation. Par un jugement n° 1306082 du 12 janvier 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA00941 du 13 octobre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement.  <br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 13 décembre 2017 et 13 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n°2019-774 du 24 juillet 2019, notamment son article 70 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de Mme A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions du I de l'article L. 4111-2 du code de la santé publique, dans leur rédaction applicable à la date de la décision attaquée, que : " Le ministre chargé de la santé peut, après avis d'une commission comprenant notamment des délégués des conseils nationaux des ordres et des organisations nationales des professions intéressées, choisis par ces organismes, autoriser individuellement à exercer les personnes titulaires d'un diplôme, certificat ou autre titre permettant l'exercice de la profession de médecin (...) dans le pays d'obtention de ce diplôme, certificat ou titre. / Ces personnes doivent avoir satisfait à des épreuves anonymes de vérification des connaissances, organisées par profession, discipline ou spécialité, et justifier d'un niveau suffisant de maîtrise de la langue française. (...) / Les lauréats, candidats à la profession de médecin, doivent en outre justifier de trois ans de fonctions accomplies dans un service ou organisme agréé pour la formation des internes. Toutefois, les fonctions exercées avant la réussite à ces épreuves peuvent être prises en compte après avis de la commission mentionnée au premier alinéa, dans des conditions fixées par voie réglementaire (...) ". En outre, aux termes du I de l'article D. 4111-6 du même code, dans sa rédaction alors applicable : " Les fonctions requises, par les dispositions du I de l'article L. 4111-2, des candidats à l'autorisation d'exercice de la profession de médecin, lauréats des épreuves de vérification des connaissances, sont accomplies dans un lieu de stage agréé pour la formation des internes à temps plein ou à temps partiel pour une durée de trois ans en équivalent temps plein (...) ". Enfin, aux termes de l'article D. 4111-7 du même code : " Les candidats à l'autorisation ministérielle d'exercice de la profession de médecin (...), lauréats des épreuves de vérification des connaissances et justifiant de fonctions hospitalières antérieures en qualité d'attaché associé, de praticien attaché associé, d'assistant associé ou de fonctions universitaires en qualité de chef de clinique associé des universités ou d'assistant associé des universités, à condition d'avoir été chargés de fonctions hospitalières dans le même temps, ou d'interne à titre étranger (...) peuvent être dispensés, après avis de la commission d'autorisation d'exercice, en tout ou partie de l'exercice des fonctions prévues à l'article D. 4111-6. / Les candidats à l'autorisation d'exercice de la profession de médecin doivent justifier de trois années (...) de fonctions hospitalières dans l'un des statuts susmentionnés à la date du dépôt du dossier de demande d'autorisation d'exercice. Ces fonctions doivent avoir été effectuées à temps plein ou à temps partiel par période d'au moins trois mois consécutifs. / Pour être décomptées, les fonctions à temps partiel doivent avoir été effectuées à concurrence d'au moins cinq demi-journées par semaine. Elles sont prises en compte proportionnellement à la durée des fonctions à temps plein ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision en date du 15 octobre 2009, le ministre chargé de la santé a refusé d'accorder à Mme B..., de nationalité française et titulaire d'un diplôme de docteur en médecine délivré le 30 novembre 1993 par l'Université d'Istanbul, l'autorisation d'exercer en France la profession de médecin dans la spécialité " médecine générale ", en application du I de l'article L. 4111-2 du code de la santé publique. Par un arrêt du 25 juin 2013, devenu définitif, la cour administrative d'appel de Marseille a annulé cette décision et a enjoint au ministre de procéder au réexamen de la demande de Mme B... dans un délai de trois mois à compter de la notification de son arrêt. Par une nouvelle décision du 5 novembre 2013, la ministre des affaires sociales et de la santé, après avoir procédé au réexamen de la demande de Mme B..., lui a de nouveau refusé cette autorisation. Mme B... se pourvoit en cassation contre l'arrêt du 13 octobre 2017 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Montpellier ayant rejeté sa demande tendant à l'annulation de la décision du 5 novembre 2013.<br/>
<br/>
              3. Pour annuler la décision du 15 octobre 2009 par laquelle le ministre chargé de la santé, au vu de l'avis défavorable de la commission d'autorisation d'exercice, a refusé d'accorder à Mme B... l'autorisation d'exercer en France la profession de médecin dans la spécialité " médecine générale ", la cour administrative d'appel de Marseille, par son arrêt du 25 juin 2013, devenu définitif, s'est notamment fondée sur les motifs tirés de ce que Mme B... avait obtenu son diplôme de docteur en médecine en Turquie en 1993, qu'elle avait, au titre de la session 2008, réussi les épreuves de vérification des connaissances et de maîtrise de la langue française prévues par les dispositions précitées de l'article L. 4111-2 du code de la santé publique et que, conformément à ce que prévoient ces mêmes dispositions, elle justifiait de l'accomplissement pendant plus de trois années, avant la réussite à ces épreuves, de fonctions dans des services ou organismes agréés pour la formation des internes, comprenant des gardes et des consultations en pédiatrie et en gynécologie. La cour administrative d'appel de Marseille en a déduit que le ministre avait commis une erreur manifeste d'appréciation en retenant, par la décision contestée, que l'expérience et la formation en médecine générale de Mme B... étaient insuffisantes.<br/>
<br/>
              4. L'autorité de chose jugée s'attachant au dispositif de cet arrêt d'annulation devenu définitif ainsi qu'aux motifs qui en sont le support nécessaire faisait obstacle à ce que, en l'absence de modification de la situation de droit ou de fait, l'autorisation sollicitée soit à nouveau refusée par l'autorité administrative pour le même motif.<br/>
<br/>
              5. Pour rejeter les conclusions de Mme B... tendant à l'annulation de la décision du 5 novembre 2013 lui refusant de nouveau l'autorisation d'exercer en France la profession de médecin dans la spécialité " médecine générale ", la cour administrative d'appel de Marseille s'est fondée, d'une part, sur le fait que le nouveau refus en litige, bien qu'il ait mentionné que les trois années de fonctions hospitalières requises avaient été accomplies par Mme B..., avait relevé les insuffisances d'expérience et de formation de Mme B... en médecine générale, le fait que le parcours professionnel de Mme B... s'est longtemps limité à la médecine d'urgence et les trois domaines insuffisamment abordés, à savoir la gynécologie, la pédiatrie ainsi que le dépistage et la prise en charge des maladies chroniques, et d'autre part, sur le fait qu'il ressortait des pièces du dossier, notamment des attestations déjà produites par l'intéressée au soutien de sa première demande d'autorisation, que Mme B... n'était pas en mesure de justifier d'une réelle expérience dans le dépistage et la prise en charge des maladies chroniques.<br/>
<br/>
              6. En statuant ainsi sans relever aucun changement de circonstance, alors que les motifs de la nouvelle décision de refus d'autorisation, quant à l'expérience et la formation de Mme B..., étaient en substance les mêmes que ceux qui avaient été jugés être entachés d'erreur manifeste d'appréciation par l'arrêt du 25 juin 2013 devenu définitif, la cour administrative d'appel de Marseille s'est affranchie de l'autorité de la chose jugée s'attachant à son précédent arrêt et a, ce faisant, commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme B... est fondée à demander, pour ce motif, l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              8. Il ressort des pièces du dossier, ainsi qu'il a été dit précédemment, que les motifs de la décision du 5 novembre 2013 par laquelle la ministre des affaires sociales et de la santé a de nouveau rejeté la demande d'autorisation d'exercer en France la profession de médecin dans la spécialité " médecine générale " présentée par Mme B... méconnaissent l'autorité de la chose jugée qui s'attache à l'arrêt définitif du 25 juin 2013 de la cour administrative d'appel de Marseille, en ce que ce nouveau refus se fonde sur l'insuffisance d'expérience et de formation que présenterait Mme B... sur l'ensemble du champ de la spécialité de " médecine générale ". <br/>
<br/>
              9. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa requête d'appel, Mme B... est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a rejeté ses conclusions tendant à l'annulation de la décision du 5 novembre 2013 de la ministre des affaires sociales et de la santé. <br/>
<br/>
              10. Le C du VIII de l'article 70 de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé prévoyant que " Les dispositions du I de l'article L. 4111-2 et de l'article L. 4221-12 du code de la santé publique, dans leur rédaction antérieure à l'entrée en vigueur des IV et V du présent article, demeurent applicables pour les lauréats des épreuves de vérification des connaissances antérieures à 2020 et au plus tard jusqu'au 31 décembre 2021 ", il y a lieu, dans les circonstances de l'espèce, d'enjoindre à la ministre des solidarités et de la santé de délivrer à Mme Noyan l'autorisation d'exercer la profession de médecin dans la spécialité " médecine générale " dans le délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 13 octobre 2017 et le jugement du 12 janvier 2016 du tribunal administratif de Montpellier sont annulés.<br/>
<br/>
Article 2 : La décision du 5 novembre 2013 de la ministre des affaires sociales et de la santé est annulée. <br/>
Article 3 : Il est enjoint à la ministre des solidarités et de la santé de délivrer à Mme B... l'autorisation d'exercer la profession de médecin dans la spécialité " médecine générale ", dans le délai d'un mois à compter de la notification de la présente décision.<br/>
Article 4 : L'Etat versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de la justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme A... B... et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins et à la section du rapport et des études.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
