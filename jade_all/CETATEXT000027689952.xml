<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689952</ID>
<ANCIEN_ID>JG_L_2013_07_000000362193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 10/07/2013, 362193, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP BORE, SALVE DE BRUNETON ; SCP ODENT, POULET ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362193.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 août et 27 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat des copropriétaires de l'immeuble sis 7/9 place de la gare, dont le siège est 3 avenue du Mesnil à La Varenne Saint-Hilaire (94210) ; le syndicat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA02052 du 19 juin 2012 par lequel la cour administrative d'appel de Paris a, premièrement, rejeté ses conclusions tendant à la condamnation à son profit de la commune de Saint-Maur-des-Fossés, de la société Eiffage Construction IDF Paris, de M. A...et de la société Bureau Veritas au titre de leur responsabilité de vendeur et de constructeurs pour les désordres ayant affecté l'immeuble, deuxièmement, mis hors cause la commune de Saint-Maur-des-Fossés, troisièmement, condamné solidairement la société Eiffage Construction IDF Paris, M. A...et la société Bureau Veritas à verser à la SCI Gamma La Varenne, à la SCI Le Mesnil RER et à la société Cendres et Métaux la somme de 108 212,05 euros au titre des travaux de remise en état des locaux sur le fondement de la responsabilité décennale ainsi que les sommes de 10 000 et 5 000 euros, respectivement, à la SCI Gamma La Varenne et à la SCI Le Mesnil RER au titre des troubles de jouissance et, quatrièmement, réformé sur ces points le jugement n° 0202698/2 du 4 décembre 2008 du tribunal administratif de Melun ; <br/>
<br/>
              2°) de mettre à la charge de la société Eiffage Construction IDF Paris, de M. A... et de la société Bureau Veritas la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 67-223 du 17 mars 1967 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat du syndicat des copropriétaires de l'immeuble sis 7/9 place de la gare, à la SCP Odent, Poulet, avocat de la société Eiffage construction IDF Paris, à la SCP Boré, Salve de Bruneton, avocat de la société Bureau Veritas et à la SCP Boulloche, avocat de la commune de Saint-Maur-des-Fossés et de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 55 du décret du 17 mars 1967 pris pour l'application de la loi du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis : " Le syndic ne peut agir en justice au nom du syndicat sans y avoir été autorisé par une décision de l'assemblée générale (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Saint-Maur-des-Fossés a confié à la société Quillery, aux droits de laquelle est venue la société Eiffage Construction IDF Paris, la construction d'un ensemble immobilier de bureaux ; que postérieurement à la réception des travaux, intervenue sans réserve en 1986, la commune a cédé l'immeuble, qui est devenu une copropriété privée ; qu'à compter de 1993, des désordres liés à des infiltrations d'eau ont conduit le syndicat des copropriétaires de l'immeuble sis 7/9 place de la gare ainsi que trois sociétés copropriétaires à rechercher la responsabilité des constructeurs et de la commune, en sa qualité de vendeur, au titre de la garantie décennale ; que, par l'arrêt attaqué, la cour administrative d'appel de Paris a jugé irrecevable l'action du syndicat des copropriétaires et a, en conséquence, rejeté sa demande de première instance ainsi que son appel incident ;<br/>
<br/>
              3. Considérant que, pour faire droit à la fin de non-recevoir soulevée par les appelants principaux et tirée du défaut d'habilitation du syndic, la cour a relevé qu'il ressortait du procès-verbal de l'assemblée générale du 22 décembre 1994 que les copropriétaires s'étaient bornés à habiliter leur nouveau syndic à engager une action contre l'ancien, mais ne l'avaient pas autorisé à agir en justice contre le vendeur et les constructeurs sur le fondement de la garantie décennale ; qu'en statuant ainsi, et ne se fondant pas sur une autre habilitation, datée de 2001, qui avait été produite par le syndicat devant les premiers juges mais dont il ne se prévalait pas en appel, y compris en réponse à cette fin de non-recevoir, la cour n'a pas dénaturé les pièces du dossier ; que, dès lors qu'elle jugeait irrecevable l'action du syndicat des copropriétaires, la cour n'a pas commis d'irrégularité en ne répondant pas expressément à ses conclusions relatives aux frais d'expertise et à certains débours ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que le pourvoi du syndicat doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par les défendeurs ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : Le pourvoi du syndicat des copropriétaires de l'immeuble sis 7/9 place de la gare est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la société Eiffage Construction IDF Paris, de M.A..., de la société Bureau Veritas et de la commune de Saint-Maur-des-Fossés au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat des copropriétaires de l'immeuble sis 7/9 place de la gare, à la société Bureau Veritas, à la commune de Saint-Maur-des-Fossés, à la société Eiffage Construction IDF Paris et à M. B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
