<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034683902</ID>
<ANCIEN_ID>JG_L_2017_05_000000405313</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/68/39/CETATEXT000034683902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 11/05/2017, 405313, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405313</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405313.20170511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de l'arrêt n° 15PA01113 du 21 septembre 2016 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 1410537/2-3 du 15 janvier 2015 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2008 et 2009 ainsi que des majorations afférentes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 2° du 1 de l'article 109 et du a) de l'article 111 du code général des impôts.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. D'une part, aux termes de l'article 109 du code général des impôts : " 1. Sont considérés comme revenus distribués:/ (...) 2° Toutes les sommes ou valeurs mises à la disposition des associés, actionnaires ou porteurs de parts et non prélevées sur les bénéfices ". <br/>
<br/>
              3. D'autre part, aux termes de l'article 111 du même code : " Sont notamment considérés comme revenus distribués /: a) Sauf preuve contraire, les sommes mises à la disposition des associés directement ou par personnes ou sociétés interposées à titre d'avances, de prêts ou d'acomptes. Nonobstant toutes dispositions contraires, lorsque ces sommes sont remboursées postérieurement au 1er janvier 1960, à la personne morale qui les avait versées, la fraction des impositions auxquelles leur attribution avait donné lieu est restituée aux bénéficiaires ou à leurs ayants cause dans des conditions et suivant des modalités fixées par décret. (...) ".<br/>
<br/>
              4. Les principes constitutionnels d'égalité devant la loi et devant les charges publiques ne s'opposent ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la loi qui l'établit. Pour assurer le respect du principe d'égalité, le législateur doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Par ailleurs, cette appréciation ne doit pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              5. M. B...soutient, en premier lieu, que les dispositions mentionnées aux points 2 et 3, telles qu'interprétées par le Conseil d'Etat, méconnaissent les principes d'égalité devant la loi et les charges publiques proclamés par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, en tant qu'elles introduisent une différence de traitement défavorable aux associés gérants de sociétés, notamment au regard des sommes mises à disposition sur leurs comptes courants, par rapport aux gérants non associés, laquelle différence n'a pas de lien avec l'objectif poursuivi et n'est justifiée par aucun motif d'intérêt général. <br/>
<br/>
              6. Toutefois, l'objectif poursuivi par les dispositions citées aux points 2 et 3 est d'imposer dans la catégorie des revenus de capitaux mobiliers les sommes dont les sociétés peuvent faire bénéficier leurs associés, qu'ils soient gérants ou non, à travers des distributions officieuses, notamment par le biais des comptes courants d'associés, afin de lutter contre la fraude fiscale. S'agissant des gérants non associés, qui ne peuvent détenir de tels comptes courants, ils peuvent être imposés, à raison des revenus qui leur sont distribués, sur d'autres fondements, soit dans la même catégorie, en application du 1° du 1 de l'article 109 du code général des impôts lorsque les résultats de la société sont excédentaires, ou en application du c) de l'article 111 lorsque les rémunérations et avantages en cause ont un caractère occulte, soit dans une autre catégorie et notamment celle des traitements et salaires ou des bénéfices non commerciaux, selon la nature des revenus dont ils ont bénéficié. La différence de traitement instituée par les dispositions contestées, qui repose sur des critères objectifs et rationnels en fonction de l'objectif poursuivi, n'est pas disproportionnée par rapport à la différence de situation existant entre ces deux catégories de contribuables. <br/>
<br/>
              7. M. B...soutient, en second lieu, que les dispositions contestées, en tant qu'elles qualifient de revenus les sommes inscrites au compte courant des associés, quand bien même l'associé ne les aurait pas retirées, méconnaissent le principe de respect des droits de la défense garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              8. Il résulte, toutefois, de la jurisprudence constante du Conseil d'Etat que si les sommes inscrites au crédit d'un compte courant d'associé sont réputées, de ce seul fait, avoir le caractère de revenus imposables dans la catégorie des capitaux mobiliers en application des dispositions contestées, l'associé titulaire du compte peut néanmoins apporter la preuve contraire. La présomption que ces dispositions instituent n'est donc pas irréfragable.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B.... <br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au Premier ministre et au ministre de l'économie et des finances. <br/>
Copie en sera adressée au Conseil constitutionnel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
