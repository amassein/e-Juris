<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037092063</ID>
<ANCIEN_ID>JG_L_2018_06_000000413978</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/09/20/CETATEXT000037092063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre jugeant seule, 21/06/2018, 413978, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413978</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413978.20180621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...et MmeB..., épouseA..., ont demandé à la Cour nationale du droit d'asile d'annuler respectivement les décisions du 19 octobre 2015 et du 28 juin 2016 de l'Office français de protection des réfugiés et apatrides (OFPRA) qui ont rejeté leurs demandes d'asile et refusé de leur reconnaître la qualité de réfugié ou, à défaut, de leur accorder le bénéfice de la protection subsidiaire. Par une décision n° 16013344, 16026469 du 2 mai 2017, la Cour nationale du droit d'asile a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 4 septembre 2017 et le 4 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et MmeA..., demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette  décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de leur reconnaitre la qualité de réfugié ou à défaut leur accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 2 000 euros à verser à leur avocat, la SCP de Nervo et Poupet au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat ;  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des stipulations du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ".<br/>
<br/>
              2. Aux termes de l'article L. 712-1 du même code : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : a) La peine de mort ou une exécution ; b) La torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ".<br/>
<br/>
              3. Il appartient à la Cour nationale du droit d'asile, qui statue comme juge de plein contentieux sur le recours d'un demandeur d'asile dont la demande a été rejetée par l'OFPRA, de se prononcer elle-même sur le droit de l'intéressé à la qualité de réfugié ou, à défaut, au bénéfice de la protection subsidiaire, au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. A ce titre, il lui revient, pour apprécier la réalité des risques invoqués par le demandeur, de prendre en compte l'ensemble des pièces que celui-ci produit à l'appui de ses prétentions. En particulier, lorsque le demandeur produit devant elle des pièces qui comportent des éléments circonstanciés en rapport avec les risques allégués, il lui incombe, après avoir apprécié si elle doit leur accorder crédit et les avoir confrontées aux faits rapportés par le demandeur, d'évaluer les risques qu'elles sont susceptibles de révéler et, le cas échéant, de préciser les éléments qui la conduisent à ne pas regarder ceux-ci comme sérieux.<br/>
<br/>
              4. Pour rejeter la demande de M. et MmeA..., la Cour a jugé, par une appréciation souveraine, que les déclarations des requérants se sont révélées parcellaires et évasives quant aux circonstances de leur départ d'Arménie en 2009, qu'ils n'ont en outre apporté aucune précision tangible sur l'état de leurs craintes en cas de retour dans ce pays près de huit ans après leur départ et que ni les pièces du dossier ni les déclarations faites en séance devant la Cour ne permettaient de tenir pour établis les faits allégués. En statuant ainsi, alors qu'un des deux certificats médicaux qui lui avait été soumis faisait état de façon circonstanciée de plusieurs blessures et traumatismes sur la personne de M.A..., la Cour, qui n'a pas cherché à évaluer les risques que cette pièce était susceptible de révéler ni précisé les éléments qui la conduisaient à ne pas les regarder comme sérieux, a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que M. et Mme A...sont fondés à demander l'annulation de la décision qu'ils attaquent.<br/>
<br/>
              6. M. et Mme A...ont obtenu le bénéfice de l'aide juridictionnelle. Par suite, leur avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que la SCP de Nervo et Poupet, avocat de M. et MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 2 000 euros à verser la SCP de Nervo et Poupet<br/>
<br/>
<br/>
<br/>
<br/>
                           D E C I D E :<br/>
                           ---------------<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 2 mai 2017 est annulée. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP de Nervo et Poupet, avocat de M. et MmeA..., la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme C...A...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
