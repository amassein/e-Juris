<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978226</ID>
<ANCIEN_ID>JG_L_2016_02_000000386416</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978226.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème SSR, 03/02/2016, 386416</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386416</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:386416.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 25 septembre 2013 par laquelle le préfet de la Seine-Saint-Denis a rejeté sa demande d'autorisation de travail.<br/>
<br/>
              Par un jugement n° 1311463 du 13 février 2014, le tribunal administratif de Montreuil a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n°s 14VE00716, 14VE00793 du 25 septembre 2014, la cour administrative d'appel de Versailles a rejeté l'appel et la demande de sursis à exécution formés par le ministre de l'intérieur contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 12 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat ;<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'accord franco-algérien du 27 décembre 1968 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 7 de l'accord franco-algérien du 27 décembre 1968 : " ... b) les ressortissants algériens désireux d'exercer une activité  professionnelle salariée reçoivent, après le contrôle médical d'usage et sur présentation d'un contrat de travail visé par les services du ministre  chargé de l'emploi, un certificat de résidence valable un an  pour toutes professions et toutes régions, renouvelable et portant la  mention "salarié" ; cette mention constitue l'autorisation de travail  exigée par la législation française " ; qu'en prévoyant l'apposition de la mention " salarié " sur le certificat de résidence délivré aux ressortissants algériens, les auteurs de l'accord, qui ont précisé que cette mention constitue l'autorisation de travail exigée par la législation française, ont habilité les services compétents à opérer sur l'exercice d'une activité salariée par ces ressortissants un contrôle de la nature de celui que prévoit l'article R. 5221-20 du code du travail ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 5221-20 du code du travail : " Pour accorder ou refuser l'une des autorisations de travail mentionnées à l'article R. 5221-11, le préfet prend en compte les éléments d'appréciation suivants :/ (...) 2° L'adéquation entre la qualification, l'expérience, les diplômes ou titres de l'étranger et les caractéristiques de l'emploi auquel il postule ; (...) " ; qu'aux termes de l'article R. 5221-23 du même code : " La décision relative à la demande d'autorisation de travail mentionnée à l'article R. 5221-11 est prise par le préfet. Elle est notifiée à l'employeur ou au mandataire qui a présenté la demande, ainsi qu'à l'étranger " ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de la Seine-Saint-Denis a, le 25 septembre 2013, refusé de délivrer à M.B..., ressortissant algérien titulaire d'un certificat de résidence portant la mention " étudiant " jusqu'au 30 septembre 2013, une autorisation de travail aux motifs que l'emploi d'assistant commercial sollicité par l'intéressé était accessible à partir d'un diplôme CAP/BEP à Bac + 2 en hôtellerie, restauration alors que l'intéressé était titulaire d'une maîtrise de lettres et langues étrangères et d'un master 2 en littérature française et que son expérience professionnelle n'avait été acquise que dans le cadre d'un emploi accessoire à ses études, sans lien avec ces dernières et afin de subvenir à ses besoins ; que le ministre de l'intérieur se pourvoit en cassation contre l'arrêt du 25 septembre 2014 par lequel la cour administrative d'appel de Versailles a confirmé l'annulation de la décision du préfet de la Seine-Saint-Denis du 25 septembre 2013 par le jugement du 13 février 2014 du tribunal administratif de Montreuil ; <br/>
<br/>
              4.	Considérant qu'en retenant, d'une part, que la circonstance que M. B...ait exercé son activité dans le cadre d'un emploi sous couvert d'un titre de séjour " étudiant " l'autorisant à travailler à titre accessoire ne saurait suffire à écarter l'expérience acquise au titre de cet emploi et qu'ainsi, le préfet ne pouvait refuser pour ce seul motif l'autorisation de travail sollicitée, la cour, qui s'est livrée à une appréciation souveraine des pièces du dossier sans les dénaturer, n'a pas commis d'erreur de droit ;<br/>
<br/>
              5.	Considérant qu'en retenant, d'autre part, que la circonstance que la fiche correspondant à l'emploi d'assistant commercial sollicité indique que ce dernier est accessible à partir d'un diplôme CAP/BEP à Bac + 2 en hôtellerie, restauration, soit un diplôme d'un niveau inférieur à celui détenu par l'intéressé, ne permettait pas, à elle seule, d'établir le défaut d'adéquation entre les critères énumérés au 2° de l'article R. 5221-20 du code du travail et l'emploi occupé, la cour, qui s'est livrée à une appréciation souveraine des pièces du dossier sans les dénaturer, n'a pas davantage commis d'erreur de droit ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu dans les circonstances de l'espèce, et sous réserve que la SCP Masse-Dessen, Thouvenin, Coudray avocat de M.B..., renonce à percevoir la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Masse-Dessen, Thouvenin, Coudray  une somme de 3 000 euros en application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à  M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-06-02-01 ÉTRANGERS. EMPLOI DES ÉTRANGERS. MESURES INDIVIDUELLES. TITRE DE TRAVAIL. - AUTORISATION DE TRAVAIL - APPRÉCIATION DE L'ADÉQUATION ENTRE LA QUALIFICATION, L'EXPÉRIENCE, LES DIPLÔMES OU TITRES DE L'ÉTRANGER ET LES CARACTÉRISTIQUES DE L'EMPLOI AUQUEL IL POSTULE (2° DE L'ART. R. 5221-20 DU CODE DU TRAVAIL) - 1) CIRCONSTANCE QUE L'EXPÉRIENCE A ÉTÉ ACQUISE DANS LE CADRE D'UNE ACTIVITÉ EXERCÉE SOUS COUVERT D'UN TITRE ÉTUDIANT - CIRCONSTANCE SANS INCIDENCE - 2) CIRCONSTANCE QUE L'EMPLOI SOLLICITÉ SOIT ACCESSIBLE À PARTIR D'UN DIPLÔME D'UN NIVEAU INFÉRIEUR À CELUI DE L'INTÉRESSÉ - CIRCONSTANCE INSUSCEPTIBLE, À ELLE SEULE, D'ÉTABLIR LE DÉFAUT D'ADÉQUATION.
</SCT>
<ANA ID="9A"> 335-06-02-01 1) La circonstance qu'un étranger ait exercé son activité dans le cadre d'un emploi sous couvert d'un titre de séjour étudiant l'autorisant à travailler à titre accessoire ne permet pas d'écarter l'expérience acquise au titre de cet emploi de l'appréciation de l'adéquation entre la qualification, l'expérience, les diplômes ou titres de l'étranger et les caractéristiques de l'emploi auquel il postule prévue par le 2° de l'article R. 5221-20 du code du travail.,,,2) La circonstance que la fiche correspondant à l'emploi sollicité indique que celui-ci est accessible à partir d'un diplôme d'un niveau inférieur à celui détenu par l'intéressé ne permet pas, à elle seule, d'établir le défaut d'adéquation entre les critères énumérés au 2° de l'article R. 5221-20 du code du travail et l'emploi sollicité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
