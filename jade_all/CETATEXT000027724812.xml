<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724812</ID>
<ANCIEN_ID>JG_L_2013_07_000000368260</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/48/CETATEXT000027724812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 17/07/2013, 368260</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368260</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Anne-Françoise Roul</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:368260.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1100538 du 18 avril 2013, enregistré le 3 mai 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif d'Amiens, avant de statuer sur la demande de Mme A... B...tendant à la condamnation du centre hospitalier universitaire d'Amiens à lui verser une indemnité de 235 556 euros, assortie des intérêts et de leur capitalisation, en réparation des préjudices ayant résulté pour elle de son accouchement dans cet établissement le 6 mai 2003, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si, dans l'hypothèse où la saisine de la commission régionale de conciliation et d'indemnisation intervient antérieurement à la présentation d'une demande d'indemnité à l'établissement de santé, l'absence, dans la notification de la décision expresse de rejet de cette demande, de la mention de ce que le délai de recours contentieux est suspendu en cas de saisine de la commission régionale de conciliation et d'indemnisation rend inopposable le délai de recours contentieux ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne-Françoise Roul, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. La loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé a créé une procédure de règlement amiable des litiges relatifs aux accidents médicaux, aux affections iatrogènes et aux infections nosocomiales graves. En vertu des dispositions, issues de cette loi, de l'article L. 1142-7 du code de la santé publique, toute personne s'estimant victime d'un dommage imputable à une activité de prévention, de diagnostic ou de soins, de même que les ayants droit d'une personne décédée à la suite d'un acte de prévention, de diagnostic ou de soins, peut saisir la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (CRCI) qui émettra un avis sur le dommage et les responsabilités encourues. Le dernier alinéa du même article prévoit que la saisine de cette commission " suspend les délais de prescription et de recours contentieux jusqu'au terme de la procédure ".<br/>
<br/>
              2. Dans le cas où le dommage est imputé à un établissement public de santé, ces dispositions législatives doivent être combinées avec les dispositions du code de justice administrative relatives à l'exercice des recours contentieux. En vertu de l'article R. 421-1 et du 1° de l'article R. 421-3 de ce code, la personne qui a saisi une collectivité publique d'une demande d'indemnité et à laquelle a été notifiée une décision expresse de rejet dispose d'un délai de deux mois à compter de cette notification pour rechercher la responsabilité de la collectivité devant le tribunal administratif. Conformément aux dispositions de l'article R. 421-5, ce délai n'est toutefois opposable qu'à la condition d'avoir été mentionné, ainsi que les voies de recours, dans la notification de la décision.<br/>
<br/>
              I - Sur le délai du recours contentieux contre une décision expresse par laquelle un établissement public de santé rejette une demande d'indemnité :<br/>
<br/>
              3. Les dispositions du dernier alinéa de l'article L. 1142-7 du code de la santé publique selon lesquelles la saisine de la CRCI suspend le délai de recours contentieux jusqu'au terme de la procédure sont sans application lorsqu'à la date de notification de la décision de l'établissement public de santé rejetant une demande d'indemnité la CRCI a déjà notifié un avis à l'intéressé à sa demande. Une seconde saisine de la commission ne saurait suspendre le délai de recours contre la décision de l'établissement. <br/>
<br/>
              4. Ces mêmes dispositions trouvent, en revanche, à s'appliquer lorsque, dans les deux mois de la date à laquelle l'établissement public de santé lui a notifié une décision expresse rejetant sa demande d'indemnité, l'intéressé saisit pour la première fois la commission d'une demande de règlement amiable. Elles trouvent également à s'appliquer dans le cas où, à la date de notification de la décision de l'établissement, la commission est déjà saisie d'une telle demande mais n'a pas encore notifié son avis à l'intéressé. Dans ces deux hypothèses, le demandeur disposera, pour saisir le tribunal administratif d'un recours indemnitaire contre l'établissement public de santé, d'un délai de deux mois à compter de la date à laquelle l'avis de la commission lui sera notifié. En effet, eu égard à la nature et à la durée du délai de recours contentieux, il y a lieu de considérer que ce délai court à nouveau pour sa durée intégrale lorsque la cause de suspension prend fin.<br/>
<br/>
              II - Sur l'opposabilité du délai du recours contentieux :<br/>
<br/>
              5. Conformément aux dispositions de l'article R. 421-5 du code de justice administrative, il appartient à l'établissement public de santé qui notifie une décision rejetant une demande d'indemnité d'informer l'intéressé des voies et délai de recours. L'absence d'une telle information entraîne l'inopposabilité du délai.<br/>
<br/>
              6. Dans les cas où le délai de recours contentieux est susceptible d'être suspendu par application des dispositions du dernier alinéa de l'article L. 1142-7 du code de la santé publique, l'information donnée à l'intéressé doit préciser les conditions de cette suspension. Compte tenu de ce qui a été dit au point 4 du présent avis, cette précision s'impose, à peine d'inopposabilité du délai de recours, lorsqu'à la date à laquelle l'établissement lui notifie sa décision l'intéressé soit n'a pas encore saisi la CRCI, soit l'a saisie mais n'a pas encore reçu notification d'un avis. <br/>
<br/>
              7. En revanche, dans le cas, envisagé au point 3, où, à la date de la notification de la décision de l'établissement, l'intéressé a déjà reçu notification d'un avis de la CRCI, aucune mention relative à la suspension du délai de recours contentieux n'est requise. L'absence d'une telle mention n'a donc, dans ce cas, aucune incidence sur l'opposabilité du délai. <br/>
<br/>
              8. Il est loisible à l'établissement public de santé, soit de ne mentionner dans la notification de sa décision que les indications correspondant à la situation du demandeur à la date de cette notification, soit d'y faire figurer une information relative aux trois situations possibles. Une telle information peut, par exemple, être libellée en ces termes : <br/>
<br/>
              " La présente décision peut faire l'objet dans un délai de deux mois d'un recours devant le tribunal administratif. En vertu du dernier alinéa de l'article L. 1142-7 du code de la santé publique, la saisine de la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (CRCI) suspend ce délai. <br/>
<br/>
               Si vous avez déjà saisi la CRCI et qu'elle vous a déjà notifié son avis, votre recours contre la présente décision doit parvenir au tribunal administratif dans les deux mois de la date à laquelle cette décision vous est notifiée. <br/>
<br/>
              Si vous avez déjà saisi la CRCI et qu'elle ne vous a pas encore notifié son avis, ou si vous la saisissez pour la première fois dans les deux mois de la notification de la présente décision, vous disposerez, pour saisir le tribunal administratif, d'un délai de deux mois à compter de la date à laquelle l'avis de la commission vous sera notifié. "<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif d'Amiens, à Mme A...B..., à la caisse primaire d'assurance maladie de la Somme, au centre hospitalier universitaire d'Amiens, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la ministre des affaires sociales et de la santé.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. - DÉLAI DE RECOURS CONTENTIEUX CONTRE UNE DÉCISION EXPRESSE PAR LAQUELLE UN ÉTABLISSEMENT PUBLIC DE SANTÉ REJETTE UNE DEMANDE D'INDEMNITÉ - SUSPENSION DE CE DÉLAI JUSQU'AU TERME DE LA PROCÉDURE PAR LA SAISINE DE LA CRCI (DERNIER AL. DE L'ART. L. 1142-7 DU CSP) - 1) EXCLUSION - CAS DANS LEQUEL, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, LA CRCI A DÉJÀ NOTIFIÉ UN AVIS À L'INTÉRESSÉ À SA DEMANDE - CIRCONSTANCE QUE L'INTÉRESSÉ SAISISSE UNE SECONDE FOIS LA CRCI - INCIDENCE - ABSENCE - 2) INCLUSION - A) CAS DANS LEQUEL, DANS LES DEUX MOIS SUIVANT LA NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ SAISIT POUR LA PREMIÈRE FOIS LA CRCI D'UNE DEMANDE DE RÈGLEMENT AMIABLE - B) CAS DANS LEQUEL, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, LA CRCI EST DÉJÀ SAISIE D'UNE TELLE DEMANDE MAIS N'A PAS ENCORE NOTIFIÉ SON AVIS - C) EFFET - DÉLAI DONT DISPOSE L'INTÉRESSÉ POUR SAISIR LE TRIBUNAL ADMINISTRATIF - DÉLAI DE DEUX MOIS À COMPTER DE LA NOTIFICATION DE L'AVIS DE LA CRCI - 3) OBLIGATION D'INFORMER L'INTÉRESSÉ, À PEINE D'INOPPOSABILITÉ DU DÉLAI DE RECOURS CONTENTIEUX, SUR LES CONDITIONS DE SUSPENSION DE CE DÉLAI PAR APPLICATION DU DERNIER ALINÉA DE L'ARTICLE L. 1142-7 DU CSP - A) CAS OÙ, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ N'A PAS ENCORE SAISI LA CRCI OU L'A SAISIE MAIS N'A PAS ENCORE REÇU NOTIFICATION D'UN AVIS - EXISTENCE [RJ1] - B) CAS OÙ, À LA DATE DE LA NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ A DÉJÀ REÇU NOTIFICATION D'UN AVIS DE LA CRCI - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - DEMANDE INDEMNITAIRE DE LA VICTIME - REJET EXPRÈS PAR L'ÉTABLISSEMENT PUBLIC DE SANTÉ - DÉLAI DE RECOURS CONTENTIEUX - SUSPENSION DE CE DÉLAI JUSQU'AU TERME DE LA PROCÉDURE PAR LA SAISINE DE LA CRCI (DERNIER AL. DE L'ART. L. 1142-7 DU CSP) - 1) EXCLUSION - CAS DANS LEQUEL, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, LA CRCI A DÉJÀ NOTIFIÉ UN AVIS À L'INTÉRESSÉ À SA DEMANDE - CIRCONSTANCE QUE L'INTÉRESSÉ SAISISSE UNE SECONDE FOIS LA CRCI - INCIDENCE - ABSENCE - 2) INCLUSION - A) CAS DANS LEQUEL, DANS LES DEUX MOIS SUIVANT LA NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ SAISIT POUR LA PREMIÈRE FOIS LA CRCI D'UNE DEMANDE DE RÈGLEMENT AMIABLE - B) CAS DANS LEQUEL, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, LA CRCI EST DÉJÀ SAISIE D'UNE TELLE DEMANDE MAIS N'A PAS ENCORE NOTIFIÉ SON AVIS - C) EFFET - DÉLAI DONT DISPOSE L'INTÉRESSÉ POUR SAISIR LE TRIBUNAL ADMINISTRATIF - DÉLAI DE DEUX MOIS À COMPTER DE LA NOTIFICATION DE L'AVIS DE LA CRCI - 3) OBLIGATION D'INFORMER L'INTÉRESSÉ, À PEINE D'INOPPOSABILITÉ DU DÉLAI DE RECOURS CONTENTIEUX, SUR LES CONDITIONS DE SUSPENSION DE CE DÉLAI PAR APPLICATION DU DERNIER ALINÉA DE L'ARTICLE L. 1142-7 DU CSP - A) CAS OÙ, À LA DATE DE NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ N'A PAS ENCORE SAISI LA CRCI OU L'A SAISIE MAIS N'A PAS ENCORE REÇU NOTIFICATION D'UN AVIS - EXISTENCE [RJ1] - B) CAS OÙ, À LA DATE DE LA NOTIFICATION DE LA DÉCISION DE L'ÉTABLISSEMENT, L'INTÉRESSÉ A DÉJÀ REÇU NOTIFICATION D'UN AVIS DE LA CRCI - ABSENCE.
</SCT>
<ANA ID="9A"> 54-01-07-04 1) Les dispositions du dernier alinéa de l'article L. 1142-7 du code de la santé publique (CSP) selon lesquelles la saisine de la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (CRCI) suspend le délai de recours contentieux jusqu'au terme de la procédure sont sans application lorsqu'à la date de notification de la décision de l'établissement public de santé rejetant une demande d'indemnité, la CRCI a déjà notifié un avis à l'intéressé à sa demande. Une seconde saisine de la commission ne saurait suspendre le délai de recours contre la décision de l'établissement.... ,,2) a) Ces mêmes dispositions trouvent, en revanche, à s'appliquer lorsque, dans les deux mois de la date à laquelle l'établissement public de santé lui a notifié une décision expresse rejetant sa demande d'indemnité, l'intéressé saisit pour la première fois la commission d'une demande de règlement amiable.... ,,b) Elles trouvent également à s'appliquer dans le cas où, à la date de notification de la décision de l'établissement, la commission est déjà saisie d'une telle demande mais n'a pas encore notifié son avis à l'intéressé.... ,,c) Dans ces deux hypothèses, le demandeur dispose, pour saisir le tribunal administratif d'un recours indemnitaire contre l'établissement public de santé, d'un délai de deux mois à compter de la date à laquelle l'avis de la commission lui est notifié. En effet, eu égard à la nature et à la durée du délai de recours contentieux, il y a lieu de considérer que ce délai court à nouveau pour sa durée intégrale lorsque la cause de suspension prend fin.,,,3) a) Dans les cas où le délai de recours contentieux est susceptible d'être suspendu par application des dispositions du dernier alinéa de l'article L. 1142-7 du CSP, l'information donnée à l'intéressé doit préciser les conditions de cette suspension. Cette précision s'impose, à peine d'inopposabilité du délai de recours, lorsqu'à la date à laquelle l'établissement lui notifie sa décision l'intéressé soit n'a pas encore saisi la CRCI, soit l'a saisie mais n'a pas encore reçu notification d'un avis.... ,,b) En revanche, dans le cas où, à la date de la notification de la décision de l'établissement, l'intéressé a déjà reçu notification d'un avis de la CRCI, aucune mention relative à la suspension du délai de recours contentieux n'est requise. L'absence d'une telle mention n'a donc, dans ce cas, aucune incidence sur l'opposabilité du délai.</ANA>
<ANA ID="9B"> 60-02-01-01-02 1) Les dispositions du dernier alinéa de l'article L. 1142-7 du code de la santé publique (CSP) selon lesquelles la saisine de la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (CRCI) suspend le délai de recours contentieux jusqu'au terme de la procédure sont sans application lorsqu'à la date de notification de la décision de l'établissement public de santé rejetant une demande d'indemnité, la CRCI a déjà notifié un avis à l'intéressé à sa demande. Une seconde saisine de la commission ne saurait suspendre le délai de recours contre la décision de l'établissement.... ,,2) a) Ces mêmes dispositions trouvent, en revanche, à s'appliquer lorsque, dans les deux mois de la date à laquelle l'établissement public de santé lui a notifié une décision expresse rejetant sa demande d'indemnité, l'intéressé saisit pour la première fois la commission d'une demande de règlement amiable.... ,,b) Elles trouvent également à s'appliquer dans le cas où, à la date de notification de la décision de l'établissement, la commission est déjà saisie d'une telle demande mais n'a pas encore notifié son avis à l'intéressé.... ,,c) Dans ces deux hypothèses, le demandeur dispose, pour saisir le tribunal administratif d'un recours indemnitaire contre l'établissement public de santé, d'un délai de deux mois à compter de la date à laquelle l'avis de la commission lui est notifié. En effet, eu égard à la nature et à la durée du délai de recours contentieux, il y a lieu de considérer que ce délai court à nouveau pour sa durée intégrale lorsque la cause de suspension prend fin.,,,3) a) Dans les cas où le délai de recours contentieux est susceptible d'être suspendu par application des dispositions du dernier alinéa de l'article L. 1142-7 du CSP, l'information donnée à l'intéressé doit préciser les conditions de cette suspension. Cette précision s'impose, à peine d'inopposabilité du délai de recours, lorsqu'à la date à laquelle l'établissement lui notifie sa décision l'intéressé soit n'a pas encore saisi la CRCI, soit l'a saisie mais n'a pas encore reçu notification d'un avis.... ,,b) En revanche, dans le cas où, à la date de la notification de la décision de l'établissement, l'intéressé a déjà reçu notification d'un avis de la CRCI, aucune mention relative à la suspension du délai de recours contentieux n'est requise. L'absence d'une telle mention n'a donc, dans ce cas, aucune incidence sur l'opposabilité du délai.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 septembre 2012, Office national de l'indemnisation des accidents médicaux, n° 360280, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
