<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230330</ID>
<ANCIEN_ID>JG_L_2017_03_000000390889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230330.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 20/03/2017, 390889</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:390889.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...a porté plainte contre M. D...B...devant la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur-Corse de l'ordre des médecins. Le conseil départemental des Alpes-Maritimes de l'ordre des médecins s'est associé à la plainte. Par une décision du 18 septembre 2013, la chambre disciplinaire a prononcé contre M. B... la sanction d'interdiction d'exercer la médecine pour une durée de trois ans dont une année assortie du sursis. <br/>
<br/>
              Par une décision n° 12110 du 5 mai 2015, la chambre disciplinaire nationale de l'ordre des médecins a, sur appel de M.B..., annulé cette décision et infligé à M. B... la même sanction en précisant que la partie non assortie du sursis serait exécutée du 1er septembre 2015 au 31 août 2017.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 9 juin et 4 août 2015, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge du conseil départemental des Alpes-Maritimes de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C... a été, de 1986 à 2011, le patient de M.B..., médecin généraliste ; qu'en juin 1994, M. B... a constaté que M. C..., alors âgé de 57 ans, était contaminé, vraisemblablement depuis de nombreuses années, par le virus de l'hépatite C ; que, sans orienter son patient vers un confrère spécialiste en hépatologie ni lui prescrire un traitement spécifique par interférons, M. B... s'est borné à prescrire un contrôle régulier de l'évolution de la contamination ; qu'il n'a conseillé à M. C... de consulter un médecin spécialiste en hépatologie qu'en 2011, après que des examens avaient révélé une charge virale élevée ; que, M. C... a porté plainte contre M. B... devant la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur-Corse de l'ordre des médecins en dénonçant le défaut de prise en charge et de suivi de sa maladie ainsi que des prescriptions non conformes aux données acquises de la science ; que par la décision du 5 mai 2015 contre laquelle M. B... se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des médecins a jugé que son comportement, entre 1994 et 2011, avait méconnu les dispositions de l'article R. 4127-32 du code de la santé publique et lui a infligé la sanction d'interdiction d'exercer la médecine pendant une durée de trois ans ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4127-32 du code de la santé publique : " Dès lors qu'il a accepté de répondre à une demande, le médecin s'engage à assurer personnellement au patient des soins consciencieux, dévoués et fondés sur les données acquises de la science, en faisant appel, s'il y a lieu, à l'aide de tiers compétents " ; qu'il ressort des termes de la décision attaquée que, pour infliger une sanction disciplinaire à M. B..., la chambre disciplinaire nationale s'est fondée sur ce qu'il avait méconnu les obligations déontologiques résultant de ces dispositions, au motif que, pendant plus de seize années, il s'était abstenu de " faire appel à des tiers compétents pour évaluer l'évolution de l'affection [de son patient] ainsi que les différents traitements qu'il aurait été possible de prescrire " ; qu'en jugeant que de tels faits étaient, eu égard à la gravité de l'affection dont le patient était atteint et à la durée de la période en cause, de nature à justifier une sanction disciplinaire, la chambre disciplinaire nationale n'a ni inexactement qualifié les faits dont elle était saisie ni entaché sa décision d'erreur de droit ; <br/>
<br/>
              3. Considérant, toutefois, que si le choix de la sanction relève de l'appréciation des juges du fond au vu de l'ensemble des circonstances de l'espèce, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu dès lors être légalement prise ;<br/>
<br/>
              4. Considérant qu'en infligeant à M. B...une sanction d'interdiction d'exercer la médecine pendant trois ans, alors qu'il ressort des termes de sa décision qu'elle n'a retenu à son encontre que le grief d'avoir, pendant une longue durée, décidé seul du traitement de son patient sans solliciter l'avis d'autres praticiens, la chambre disciplinaire nationale a prononcé une sanction hors de proportion avec la faute reprochée ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, il y a lieu, par suite, d'annuler la décision de la chambre disciplinaire nationale de l'ordre des médecins ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental des Alpes-Maritimes de l'ordre des médecins la somme de 3 000 euros que M. B... demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des médecins du 5 mai 2015 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 3 : Le conseil départemental des Alpes-Maritimes de l'ordre des médecins versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. D... B..., à M. A... C...et au conseil départemental des Alpes-Maritimes de l'ordre des médecins.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - OBLIGATION DÉONTOLOGIQUE DE FAIRE APPEL, S'IL Y A LIEU, À L'AIDE DE TIERS COMPÉTENTS (ART. R. 4127-32 DU CSP) - MANQUEMENT - ELÉMENTS D'APPRÉCIATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-02-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. MÉDECINS. - FAIT DE S'ABSTENIR DE FAIRE APPEL À L'AIDE DE TIERS COMPÉTENTS (ART. R. 4127-32 DU CSP) - EXISTENCE - ELÉMENTS D'APPRÉCIATION.
</SCT>
<ANA ID="9A"> 55-03-01-02 L'article R. 4127-32 du code de la santé publique (CSP) prévoit que dès lors qu'il a accepté de répondre à une demande, le médecin s'engage à assurer personnellement au patient des soins consciencieux, dévoués et fondés sur les données acquises de la science, en faisant appel, s'il y a lieu, à l'aide de tiers compétents.,,,Constitue un manquement à cette obligation déontologique, eu égard à la gravité de l'affection dont le patient était atteint et à la durée de la période en cause, le fait pour un médecin de s'être abstenu, pendant plus de seize ans, de faire appel à des tiers compétents pour évaluer l'évolution de l'affection de son patient ainsi que les différents traitements qu'il aurait été possible de prescrire.</ANA>
<ANA ID="9B"> 55-04-02-01-01 L'article R. 4127-32 du code de la santé publique (CSP) prévoit que dès lors qu'il a accepté de répondre à une demande, le médecin s'engage à assurer personnellement au patient des soins consciencieux, dévoués et fondés sur les données acquises de la science, en faisant appel, s'il y a lieu, à l'aide de tiers compétents.,,,Constitue un manquement à cette obligation déontologique, eu égard à la gravité de l'affection dont le patient était atteint et à la durée de la période en cause, le fait pour un médecin de s'être abstenu, pendant plus de seize ans, de faire appel à des tiers compétents pour évaluer l'évolution de l'affection de son patient ainsi que les différents traitements qu'il aurait été possible de prescrire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
