<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445624</ID>
<ANCIEN_ID>JG_L_2015_03_000000371895</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445624.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 20/03/2015, 371895</TITRE>
<DATE_DEC>2015-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371895</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371895.20150320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              L'EURL " La Compagnie des immeubles du Midi " a demandé au tribunal administratif de Marseille d'annuler l'arrêté du 10 juillet 2009 du préfet des Bouches-du-Rhône portant déclaration d'utilité publique de l'acquisition d'un immeuble lui appartenant, situé 10, boulevard des Italiens à Marseille, qui avait été déclaré insalubre, et déclaration de cessibilité. Par un jugement n° 0906212 du 13 juillet 2011, le tribunal administratif de Marseille a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 11MA03666 du 4 juillet 2013, la cour administrative d'appel de Marseille, sur la requête de l'EURL " La Compagnie des immeubles du Midi ", a annulé ce jugement ainsi que l'arrêté préfectoral du 10 juillet 2009. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 septembre et 4 décembre 2013 et le 2 février 2015 au secrétariat du contentieux du Conseil d'Etat, la société Urbanis aménagement demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'EURL " La Compagnie des immeubles du Midi " ; <br/>
<br/>
              3°) de mettre à la charge de l'EURL " La Compagnie des immeubles du Midi " le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
- le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société Urbanis aménagement et à Me Le Prado, avocat de l'EURL " La Compagnie des immeubles du Midi " ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 12 juin 2008, le préfet des Bouches-du-Rhône a déclaré insalubre à titre irrémédiable un immeuble situé 10 boulevard des Italiens à Marseille, appartenant à l'EURL " La Compagnie des immeubles du Midi " ; que, par un arrêté du 10 juillet 2009, le préfet a déclaré d'utilité publique l'acquisition de cet immeuble par la société Urbanis aménagement et en a prononcé la cessibilité, afin qu'il soit mis un terme à cette insalubrité ; que, par un jugement du 13 juillet 2011, le tribunal administratif de Marseille a rejeté la demande de l'EURL " La Compagnie des immeubles du Midi " tendant à l'annulation pour excès de pouvoir de cet arrêté ; que la société Urbanis aménagement se pourvoit en cassation contre l'arrêt du 4 juillet 2013 par lequel la cour administrative d'appel de Marseille, faisant droit à l'appel de l'EURL " La Compagnie des immeubles du Midi ", a annulé le jugement du 13 juillet 2011 ainsi que l'arrêté préfectoral du 10 juillet 2009 ; <br/>
<br/>
              2. Considérant que l'ensemble formé par un arrêté déclarant un immeuble insalubre à titre irrémédiable et l'arrêté préfectoral déclarant d'utilité publique le projet d'acquisition de cet immeuble et prononçant sa cessibilité, en vue de permettre la réalisation de nouvelles constructions, constitue une opération complexe ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant qu'alors même que l'arrêté du 12 juin 2008 déclarant l'immeuble litigieux insalubre à titre irrémédiable n'avait pas été contesté dans le délai de recours contentieux, l'EURL " La Compagnie des immeubles du Midi " était recevable à exciper de son illégalité à l'appui de son recours pour excès de pouvoir dirigé contre l'arrêté du 10 juillet 2009 déclarant d'utilité publique l'acquisition de l'immeuble et prononçant sa cessibilité ; que l'arrêt attaqué est suffisamment motivé sur ce point ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1331-26 du code de la santé publique : " Lorsqu'un immeuble, bâti ou non, vacant ou non, attenant ou non à la voie publique, un groupe d'immeubles, un îlot ou un groupe d'îlots constitue, soit par lui-même, soit par les conditions dans lesquelles il est occupé ou exploité, un danger pour la santé des occupants ou des voisins, le préfet, saisi d'un rapport motivé du directeur départemental des affaires sanitaires et sociales ou, par application du troisième alinéa de l'article L. 1422-1, du directeur du service communal d'hygiène et de santé concluant à l'insalubrité de l'immeuble concerné, invite la commission départementale compétente en matière d'environnement, de risques sanitaires et technologiques à donner son avis dans le délai de deux mois : 1° Sur la réalité et les causes de l'insalubrité ; 2° Sur les mesures propres à y remédier. L'insalubrité d'un bâtiment doit être qualifiée d'irrémédiable lorsqu'il n'existe aucun moyen technique d'y mettre fin, ou lorsque les travaux nécessaires à sa résorption seraient plus coûteux que la reconstruction. (...) " ; qu'aux termes de l'article L. 1331-28 du même code : " I. - Lorsque la commission ou le haut conseil conclut à l'impossibilité de remédier à l'insalubrité, le représentant de l'Etat dans le département déclare l'immeuble insalubre à titre irrémédiable, prononce l'interdiction définitive d'habiter et, le cas échéant, d'utiliser les lieux et précise, sur avis de la commission, la date d'effet de cette interdiction, qui ne peut être fixée au-delà d'un an. Il peut également ordonner la démolition de l'immeuble. (...) " ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond qu'en se fondant sur un rapport du service communal d'hygiène et de santé de la ville de Marseille de juin 2007 qui concluait à l'insalubrité de l'immeuble, le préfet des Bouches-du-Rhône a sollicité l'avis du conseil départemental de l'environnement et des risques sanitaires et technologiques ; que cette instance s'est bornée à indiquer qu'elle émettait un avis favorable aux propositions de son rapporteur, sans se prononcer expressément ni sur la réalité et les causes de l'insalubrité, ni sur les mesures propres à y remédier ; qu'en relevant que l'avis ainsi émis était irrégulier, faute de comporter les indications exigées par les dispositions précitées de l'article L. 1331-26 du code de la santé publique, et en jugeant que cette irrégularité avait pu exercer une influence sur la décision du préfet et entachait par suite d'illégalité son arrêté du 12 juin 2008, la cour administrative d'appel n'a pas commis d'erreur de droit et n'a pas dénaturé les éléments qui lui étaient soumis ; <br/>
<br/>
              5. Considérant, en second lieu, que la cour s'est également prononcée sur la légalité de l'arrêté du 12 juin 2008 au regard des dispositions du quatrième alinéa de l'article L. 1331-26 du code de la santé publique selon lesquelles l'insalubrité d'un bâtiment doit être qualifiée d'irrémédiable lorsqu'il n'existe aucun moyen technique d'y mettre fin ou lorsque les travaux nécessaires à sa résorption seraient plus coûteux que la reconstruction ; que, faisant une exacte application de ces dispositions, elle a recherché, d'une part, s'il existait des moyens techniques permettant de mettre fin à l'insalubrité affectant l'immeuble en cause et, d'autre part, quel serait le coût des travaux nécessaires à cette fin ; qu'après avoir estimé que des travaux de réhabilitation étaient techniquement possibles et qu'il n'était pas établi que leur coût excèderait celui d'une reconstruction, elle a jugé que le préfet n'avait pas pu légalement déclarer l'immeuble insalubre à titre irrémédiable ;<br/>
<br/>
              6. Considérant, d'une part, que c'est par une appréciation souveraine exempte de dénaturation que la cour a estimé qu'il existait des moyens techniques de mettre fin à l'insalubrité ;  <br/>
<br/>
              7. Considérant, d'autre part, que pour juger que le coût des travaux de réhabilitation n'excèderait pas celui d'une reconstruction de l'immeuble, la cour a relevé que si la société " Urbanis Aménagement " soutenait qu'il ressortait d'un compte rendu de visite du bâtiment effectué en décembre 2009 que le coût de ces travaux s'élèverait à 932 880 euros et le coût d'une reconstruction à 665 000 euros seulement, ces éléments " ne sauraient être pris en compte pour motiver l'insalubrité irrémédiable de l'immeuble à la date de l'arrêté du 12 juin 2008 " ; que la cour a ainsi estimé que, statuant sur une exception tirée de l'illégalité de l'arrêté du 12 juin 2008, elle ne pouvait prendre en considération que l'état de l'immeuble à la date de cet arrêté ; que, toutefois, saisie d'un recours pour excès de pouvoir contre l'arrêt du 10 juillet 2009 portant déclaration d'utilité publique de l'acquisition de l'immeuble, il lui incombait de déterminer si cette mesure était légalement justifiée par la situation de fait existant à la date à laquelle elle avait été prise  ; que l'erreur de droit dont l'arrêt est entaché sur ce point n'en justifie cependant pas l'annulation, dès lors que les éléments que la cour a écartés pour ce motif étaient relatifs à une situation postérieure aussi à l'arrêté du 10 juillet 2009 ; qu'au surplus, le motif tiré de ce que l'arrêté du 12 juin 2008 était intervenu à l'issue d'une procédure irrégulière devait à lui seul entraîner l'annulation prononcée ; <br/>
<br/>
              8. Considérant, en dernier lieu, que c'est sans erreur de droit que la cour administrative d'appel a jugé que l'arrêté du 10 juillet 2009 portant déclaration d'utilité publique et de cessibilité était entaché d'illégalité en raison de l'illégalité de l'arrêté du 12 juin 2008 déclarant l'insalubrité irrémédiable de l'immeuble en cause ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Urbanis aménagement n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par la société Urbanis aménagement soit mise à la charge de l'EURL " La Compagnie des immeubles du Midi " qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre au même titre à la charge de la société Urbanis aménagement le versement à l'EURL " La Compagnie des immeubles du Midi " d'une somme de 3 000 euros ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la société Urbanis aménagement est rejeté. <br/>
<br/>
Article 2 : La société Urbanis aménagement versera à l'EURL " La Compagnie des immeubles du Midi " une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Urbanis aménagement, à l'EURL " La Compagnie des immeubles du Midi " et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-03-03 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÉGIMES SPÉCIAUX. DIVERS  RÉGIMES SPÉCIAUX. - EXPROPRIATION DES IMMEUBLES INSALUBRES - RECOURS CONTRE LA DÉCLARATION D'UTILITÉ PUBLIQUE - APPRÉCIATION DE L'ÉTAT DE L'IMMEUBLE - OBLIGATIONS DU JUGE DE L'EXCÈS DE POUVOIR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-002 POLICE. POLICES SPÉCIALES. - IMMEUBLE INSALUBRE - ARRÊTÉ CONSTATANT L'INSALUBRITÉ IRRÉMÉDIABLE SUIVI D'UNE DÉCLARATION D'UTILITÉ PUBLIQUE - RECOURS CONTRE LA DÉCLARATION D'UTILITÉ PUBLIQUE - APPRÉCIATION DE L'ÉTAT DE L'IMMEUBLE - OBLIGATIONS DU JUGE DE L'EXCÈS DE POUVOIR.
</SCT>
<ANA ID="9A"> 34-03-03 Arrêté déclarant un immeuble insalubre à titre irrémédiable, suivi d'un arrêté déclarant d'utilité publique l'acquisition de l'immeuble.,,,Saisi d'un recours pour excès de pouvoir contre l'acte déclaratif d'utilité publique de l'acquisition de l'immeuble, il incombe au juge, saisi le cas échéant d'une exception d'illégalité dirigée contre l'arrêté d'insalubrité, non de se placer à la date de cet arrêté, mais de déterminer si la déclaration d'utilité publique était légalement justifiée par la situation de fait existant à la date à laquelle elle a été prise s'agissant, en particulier, des coûts comparés des travaux de réhabilitation et d'une reconstruction.</ANA>
<ANA ID="9B"> 49-05-002 Arrêté déclarant un immeuble insalubre à titre irrémédiable, suivi d'un arrêté déclarant d'utilité publique l'acquisition de l'immeuble.,,,Saisi d'un recours pour excès de pouvoir contre l'acte déclaratif d'utilité publique de l'acquisition de l'immeuble, il incombe au juge, saisi le cas échéant d'une exception d'illégalité dirigée contre l'arrêté d'insalubrité, non de se placer à la date de cet arrêté, mais de déterminer si la déclaration d'utilité publique était légalement justifiée par la situation de fait existant à la date à laquelle elle a été prise s'agissant, en particulier, des coûts comparés des travaux de réhabilitation et d'une reconstruction.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
