<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487282</ID>
<ANCIEN_ID>JG_L_2021_12_000000459183</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487282.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/12/2021, 459183, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459183</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459183.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 6 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale des supporters demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre un arrêté du ministre de l'intérieur du 6 décembre 2021 portant interdiction de déplacement des supporters du club du Rangers Football Club lors de la rencontre du jeudi 9 décembre 2015 à 18 heures 45 avec l'Olympique lyonnais ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de mettre en œuvre toute mesure de nature à préserver les libertés fondamentales des supporters du club visiteur ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que l'arrêté litigieux interdit le déplacement des 2 000 supporters de Glasgow autorisés à assister à la rencontre par un arrêté du préfet du Rhône ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir, à la liberté d'association, à la liberté de réunion et à la liberté d'expression ; <br/>
              - l'arrêté contesté a été édicté en méconnaissance de la procédure prévue par la circulaire du 18 novembre 2019 ;<br/>
              - l'arrêté litigieux n'a pas respecté la procédure prévue par la circulaire du 18 novembre 2019 ;<br/>
              - il n'est justifié par aucune circonstance particulière, en l'absence d'antécédents et de toute rivalité entre supporters des deux équipes et en l'absence de toute précision sur la disponibilité limitée des forces de l'ordre ; <br/>
              - la mesure est manifestement disproportionnée, en l'absence de toute précision sur le nombre de supporters attendus et sur les effectifs de forces de l'ordre nécessaires et disponibles et du fait qu'il n'interdit pas aux supporters de se déplacer mais d'afficher leur soutien à leur équipe.<br/>
<br/>
              Par un mémoire en défense, enregistré le 7 décembre 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la demande est dépourvue d'objet ou, à tout le moins, que la condition d'urgence n'est pas satisfaite, dès lors que l'arrêté n'a pas été publié au Journal officiel et ne fera pas l'objet de cette publication conditionnant son entrée en vigueur.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Association nationale des supporters, et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 8 décembre 2021, à 11 heures : <br/>
<br/>
              - Me Le Guerer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Association nationale des supporters ;<br/>
<br/>
              - le représentant de l'Association nationale des supporters ;<br/>
<br/>
              - le représentant du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ".<br/>
<br/>
              2. Aux termes de l'article L. 332-16-1 du code du sport : " Le ministre de l'intérieur peut, par arrêté, interdire le déplacement individuel ou collectif de personnes se prévalant de la qualité de supporter d'une équipe ou se comportant comme tel sur les lieux d'une manifestation sportive et dont la présence est susceptible d'occasionner des troubles graves pour l'ordre public. / L'arrêté énonce la durée, limitée dans le temps, de la mesure, les circonstances précises de fait qui la motivent ainsi que les communes de point de départ et de destination auxquelles elle s'applique. / Le fait pour les personnes concernées de ne pas se conformer à l'arrêté pris en application des deux premiers alinéas est puni de six mois d'emprisonnement et d'une amende de 30 000 €. / Dans le cas prévu à l'alinéa précédent, le prononcé de la peine complémentaire d'interdiction judiciaire de stade prévue à l'article L. 332-11 pour une durée d'un an est obligatoire, sauf décision contraire spécialement motivée. " Aux termes de l'article L. 332-16-2 du code du sport : " Le représentant de l'Etat dans le département ou, à Paris, le préfet de police peut, par arrêté, restreindre la liberté d'aller et de venir des personnes se prévalant de la qualité de supporter d'une équipe ou se comportant comme tel sur les lieux d'une manifestation sportive et dont la présence est susceptible d'occasionner des troubles graves pour l'ordre public. / L'arrêté énonce la durée, limitée dans le temps, de la mesure, les circonstances précises de fait et de lieu qui la motivent, ainsi que le territoire sur lequel elle s'applique. / Le fait pour les personnes concernées de ne pas se conformer à l'arrêté pris en application des deux premiers alinéas est puni de six mois d'emprisonnement et d'une amende de 30 000 euros. (...) ".<br/>
<br/>
              3. Aux termes de l'article L. 221-2 du code des relations entre le public et l'administration : " L'entrée en vigueur d'un acte réglementaire est subordonnée à l'accomplissement de formalités adéquates de publicité, notamment par la voie, selon les cas, d'une publication ou d'un affichage, sauf dispositions législatives ou réglementaires contraires ou instituant d'autres formalités préalables ".<br/>
<br/>
              4. Il résulte de l'instruction qu'une rencontre de football doit se tenir le 9 décembre 2021 entre l'Olympique Lyonnais et le Rangers Football Club de Glasgow au Groupama Stadium de Décines Charpieu. Sur le fondement des dispositions de l'article L. 332-16-2 du code du sport citées au point 2, le préfet du Rhône a pris, le 1er décembre 2021, un arrêté interdisant l'accès au stade et à ses abords à toute personne se prévalant de la qualité de supporter du Rangers FC de Glasgow non détentrice de l'un des 2 000 billets de l'espace visiteurs remis à ce club pour être commercialisé par son circuit officiel de vente. Par un arrêté du 6 décembre 2021, le ministre de l'intérieur a, sur le fondement des dispositions de l'article L. 332-16-1 du code du sport également citées au point 2, interdit le déplacement individuel ou collectif de toute personne se prévalant de la qualité de supporter du Rangers Football Club entre les points frontières français et les communes de Décines-Charpieu, Meyzieu et Lyon. L'Association nationale des supporters, qui ne conteste pas l'arrêté du préfet du Rhône, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution du second arrêté pris par le ministre de l'intérieur.<br/>
<br/>
              5. Pour établir l'existence d'une situation d'urgence justifiant que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative, l'association requérante fait valoir que l'arrêté du ministre de l'intérieur interdit le déplacement des 2 000 supporters du FC Rangers autorisés à assister à la rencontre par l'arrêté du préfet du Rhône. Il résulte toutefois de l'instruction et des échanges tenus au cours de l'audience publique que l'administration s'est engagée à ne pas publier l'arrêté contesté au Journal officiel. En application de l'article L. 221-2 du code de relations entre le public et l'administration, cet arrêté n'entrera donc pas en vigueur. Il ne pourra donc s'opposer au déplacement des supporters du Rangers FC qui détiennent un des billets remis à ce club dans le cadre de la rencontre avec l'Olympique Lyonnais. Dans ces conditions, la condition d'urgence particulière requise par l'article L. 521-2 du code de justice administrative ne peut plus être regardée comme remplie.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu de rejeter la requête de l'Association nationale des supporters, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Compte tenu de l'engagement pris par l'administration et rappelé au point 5, la requête de l'Association nationale des supporters est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association nationale des supporters et au ministre de l'intérieur.<br/>
Fait à Paris, le 8 décembre 2021<br/>
Signé : Mathieu Herondart<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
