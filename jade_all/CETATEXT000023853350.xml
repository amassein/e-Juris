<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023853350</ID>
<ANCIEN_ID>JG_L_2011_04_000000330534</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/85/33/CETATEXT000023853350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 07/04/2011, 330534</TITRE>
<DATE_DEC>2011-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330534</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Vialettes Maud</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:330534.20110407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 août et 6 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Aline A, demeurant ... ; Mme A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NT03211 du 2 juin 2009 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 07-4410 du 25 septembre 2008 du tribunal administratif d'Orléans rejetant sa demande dirigée contre l'arrêté du 10 septembre 2007 du préfet d'Indre-et-Loire accordant à la SELARL Pharmacie de la Forge une licence en vue de la création d'une officine de pharmacie au 73, avenue du Général de Gaulle à Fondettes (Indre-et-Loire), d'autre part, à l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de la santé publique ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de Mme A et de la SCP Delaporte, Briard, Trichet, avocat de la SELARL Pharmacie de la Forge, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de Mme A et à la SCP Delaporte, Briard, Trichet, avocat de la SELARL Pharmacie de la Forge ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 5125-3 du code de la santé publique : " Les créations, les transferts et les regroupements d'officines de pharmacie doivent permettre de répondre de façon optimale aux besoins en médicaments de la population résidant dans les quartiers d'accueil de ces officines (...) " ; qu'aux termes de l'article L. 5125-11 de ce code, dans sa rédaction applicable à la date de l'arrêté en litige : " (...) Dans les communes d'une population égale ou supérieure à 2 500 habitants et inférieure à 30 000 habitants, une création d'officine ne peut être accordée que lorsque le nombre d'habitants par pharmacie est égal ou supérieur à 2 500. / Dans ce cas, il ne peut être délivré qu'une licence par tranche entière de 2 500 habitants recensés dans les limites de la commune (...) " ; qu'enfin, aux termes de l'article L. 5125-10 du même code, dans sa rédaction applicable à la date de l'arrêté en litige : " La population dont il est tenu compte pour l'application des articles L. 5125-11, L. 5125-13 et L. 5125-14 est la population municipale telle qu'elle est issue du dernier recensement général de la population ou, le cas échéant, des recensements complémentaires. " ;<br/>
<br/>
              Considérant que, par l'arrêt du 2 juin 2009 dont Mme A demande l'annulation, la cour administrative d'appel de Nantes a confirmé le jugement du 25 septembre 2008 du tribunal administratif d'Orléans qui a rejeté sa demande dirigée contre l'arrêté du 10 septembre 2007 par lequel le préfet d'Indre-et-Loire avait accordé à la SELARL Pharmacie de la Forge une licence en vue de la création d'une quatrième officine de pharmacie dans la commune de Fondettes ;<br/>
<br/>
              Considérant que la cour a pu, sans dénaturer les mémoires de la requérante, estimer que Mme A ne soulevait aucun moyen tiré de l'insuffisance de motivation de l'arrêté en litige ; que d'ailleurs, un arrêté autorisant l'ouverture d'une pharmacie n'étant pas au nombre des actes qui doivent être motivés, la cour n'aurait pas été tenue de répondre à ce moyen inopérant ; qu'ainsi, et en tout état de cause, son arrêt n'est pas entaché d'insuffisance de motivation ; <br/>
<br/>
              Considérant que, pour écarter le moyen tiré de ce que le préfet aurait méconnu les dispositions de l'article L. 5125-3 du code de la santé publique, la cour a constaté, d'une part, que le lieu d'implantation de la nouvelle officine, sur un axe principal de circulation, était situé à égale distance des pharmacies existantes et que, d'autre part, il n'était pas établi que les premiers juges avaient dénombré de façon erronée la population desservie par l'officine de pharmacie en l'estimant à 1 200 habitants ; que la réponse apportée par la cour n'est, eu égard à l'argumentation soulevée devant elle, pas entachée d'insuffisance de motivation ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'officine autorisée est située à égale distance de deux parties densément peuplées de la commune de Fondettes, sur l'axe principal qui les relie, dans une zone non immédiatement desservie par les autres officines ; qu'ainsi, la cour a pu, sans inexactement qualifier les faits qui lui étaient soumis, estimer que l'arrêté litigieux répondait, conformément aux dispositions de l'article L. 5125-3 du code de la santé publique, à la satisfaction optimale des besoins de la population ;<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article L. 5125-10 du code de la santé publique cité ci-dessus que l'administration est tenue, pour la vérification des quotas prévus par l'article L. 5125-11 de ce code, de se référer aux chiffres issus des recensements généraux de la population ou, le cas échéant, des recensements complémentaires, publiés au Journal officiel ; que la cour administrative d'appel de Nantes, devant laquelle il n'était pas utilement soutenu, faute de référence à un recensement publié au Journal officiel, qu'à la date de la décision contestée la population de la commune était inférieure à 10 000 habitants, a pu, sans commettre d'erreur de droit ni dénaturer les pièces du dossier qui lui était soumis, se fonder sur un arrêté du 23 décembre 2005 portant modification du chiffre de la population, publié au Journal officiel du 29 janvier 2006, pour estimer que la condition tenant à la population de la commune d'accueil prévue à l'article L. 5125-11 du code de la santé publique était remplie en l'espèce ;<br/>
<br/>
              Considérant, enfin, que contrairement à ce que soutient Mme A, la cour n'a pas dénaturé les termes de l'arrêté en litige en estimant qu'il résultait de ses termes mêmes que le préfet avait bien examiné la demande de licence au regard des dispositions de l'article L. 5125-3 du code de la santé publique ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Mme A n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes du 2 juin 2009 ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de Mme A le versement à la SELARL Pharmacie de la Forge de la somme de 3 000 euros ; que ces dispositions font en revanche obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, une somme au titre des frais exposés par Mme A et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A est rejeté.<br/>
Article 2 : Mme A versera à la SELARL Pharmacie de la Forge une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme Aline A, à la SELARL Pharmacie de la Forge et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04 SANTÉ PUBLIQUE. PHARMACIE. - CRÉATION D'OFFICINES DE PHARMACIES - QUOTAS EN FONCTION DE LA POPULATION - POPULATION PRISE EN COMPTE (ART. L. 5125-10 DU CGCT) - POPULATION RÉSULTANT DE RECENSEMENTS PUBLIÉS AU JOURNAL OFFICIEL.
</SCT>
<ANA ID="9A"> 61-04 Il résulte de l'article L. 5125-10 du code de la santé publique, y compris dans sa version précédant la modification résultant de la loi n° 2007-1786 du 19 décembre 2007, que l'administration est tenue, pour la vérification des quotas prévus par l'article L. 5125-11 de ce code, de se référer aux chiffres issus des recensements généraux de la population ou, le cas échéant, des recensements complémentaires, qui sont publiés au Journal officiel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
