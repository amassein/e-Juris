<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029170344</ID>
<ANCIEN_ID>JG_L_2014_06_000000369919</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/17/03/CETATEXT000029170344.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/06/2014, 369919, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369919</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369919.20140630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Limoges d'annuler la décision du 25 novembre 2010 par laquelle la Caisse nationale de retraites des agents des collectivités locales a rejeté sa demande de révision du montant de sa pension de retraite. Par un jugement n° 1100087 du 7 mai 2013, le tribunal administratif de Limoges a annulé cette décision.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 juillet et 25 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, la Caisse des dépôts et consignations demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1100087 du tribunal administratif de Limoges du 7 mai 2013 ;<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
                          Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - l'ordonnance n° 82-298 du 31 mars 1982 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 2 de l'ordonnance du 31 mars 1982 relative à la cessation progressive d'activité des agents titulaires des collectivités locales et de leurs établissements publics à caractère administratif, applicable au litige : " Les agents admis au bénéfice de la cessation progressive d'activité s'engagent à y demeurer jusqu'à la date à laquelle ils atteignent l'âge d'ouverture de leurs droits à la retraite. / Le bénéfice de la cessation progressive d'activité cesse sur demande à compter de cette date, ou lorsque les agents justifient d'une durée d'assurance, telle que définie à l'article L. 14 du code des pensions civiles et militaires de retraite, égale au nombre de trimestres nécessaire pour obtenir le pourcentage de la pension mentionné à l'article L. 13 du même code, et au plus tard à la limite d'âge. Les agents concernés sont alors mis à la retraite (...) ". Aux termes de l'article L. 13 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable au litige : " I.- La durée des services et bonifications admissibles en liquidation s'exprime en trimestres. Le nombre de trimestres nécessaires pour obtenir le pourcentage maximum de la pension civile ou militaire est fixé à cent soixante trimestres. / Ce pourcentage maximum est fixé à 75 % du traitement ou de la solde (...) / II.-Le nombre de trimestres mentionné au premier alinéa du I évolue dans les conditions définies, pour la durée d'assurance ou de services, à l'article 5 de la loi n° 2003-775 du 21 août 2003 portant réforme des retraites ". Enfin, en vertu des dispositions de l'article L. 14 du même code : " I. - La durée d'assurance totalise la durée des services et bonifications admissibles en liquidation prévue à l'article L. 13, augmentée, le cas échéant, de la durée d'assurance et des périodes reconnues équivalentes validées dans un ou plusieurs autres régimes de retraite de base obligatoires. / Lorsque la durée d'assurance est inférieure au nombre de trimestres nécessaire pour obtenir le pourcentage de la pension mentionné à l'article L. 13, un coefficient de minoration de 1,25 % par trimestre s'applique au montant de la pension liquidée en application des articles L. 13 et L. 15 dans la limite de vingt trimestres (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions que le bénéfice de la cessation progressive d'activité cesse au plus tard lorsque les agents atteignent la limite d'âge ou justifient d'une durée d'assurance, calculée comme la somme de la durée des services et bonifications admissibles en liquidation en vertu de l'article L. 13 du code des pensions civiles et militaires de retraite et de la durée d'assurance et des périodes reconnues équivalentes validées dans un ou plusieurs autres régimes de retraite de base obligatoires, égale au nombre de trimestres mentionné à l'article L. 13, qui est celle qui permet d'éviter l'application du coefficient de minoration prévu au deuxième alinéa de l'article L. 14 du même code. Par suite, le tribunal a commis une erreur de droit en jugeant que l'administration n'était pas tenue de mettre fin à la cessation progressive d'activité de M. A...tant qu'il ne comptait pas une durée des services et bonifications admissibles en liquidation lui ouvrant droit au pourcentage maximum de liquidation de sa pension prévu par l'article L. 13.<br/>
<br/>
              3. Il suit de là que la Caisse des dépôts et consignations est fondée, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, à demander l'annulation du jugement du tribunal administratif de Limoges.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Limoges du 7 mai 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Limoges.<br/>
Article 3 : La présente décision sera notifiée à la Caisse des dépôts et consignations et à M. B... A....<br/>
Copie en sera adressée au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
