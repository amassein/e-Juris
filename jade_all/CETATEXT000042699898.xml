<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042699898</ID>
<ANCIEN_ID>JG_L_2020_12_000000441444</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/69/98/CETATEXT000042699898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 17/12/2020, 441444, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441444</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:441444.20201217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1°/ Sous le n° 441444, par une requête et un mémoire complémentaire, enregistrés les 25 juin et 3 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société US Orléans Loiret Football demande Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 27 mai 2020 par laquelle le comité exécutif de la Fédération française de football a réformé, en la privant de tout effet, la décision de l'assemblée générale de la Ligue de football professionnel du 20 mai 2020 portant à vingt-deux clubs le format de la Ligue 2 et maintenant en Ligue 2, pour la saison 2020-2021, les clubs classés en 19ème et 20ème position de ce championnat à l'issue de la saison 2019-2020 ; <br/>
<br/>
              2°) d'annuler, par voie de conséquence, la décision du conseil d'administration de la Ligue de football professionnel du 5 juin 2020 prononçant la relégation en National 1 des clubs classés en 19ème et 20ème position de Ligue 2 ;<br/>
<br/>
              3°) de mettre à la charge de la Fédération française de football la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2°/ Sous le n° 441451, par une requête et un mémoire complémentaire, enregistrés les 25 juin et 3 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société anonyme sportive professionnelle (SASP) Le Mans Football Club demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les mêmes décisions des 27 mai et 5 juin 2020 ;  <br/>
<br/>
              2°) de mettre à la charge de la Fédération française de football la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              3°/ Sous le n° 442159, par une requête et un mémoire complémentaire, enregistrés les 24 juillet et 4 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union sportive Boulogne Côte d'Opale (USCO) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du conseil d'administration de la Ligue de football professionnel du 5 juin 2020 de ne pas organiser de barrages entre le 18ème de Ligue 2 et le 3ème de National 1 et la décision du comité exécutif de la Fédération française de football du 22 juin 2020 approuvant cette décision ;  <br/>
<br/>
              2°) de mettre à la charge de la Fédération française de football et de la Ligue de football professionnel la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de la santé publique ; <br/>
              - le code du sport ; <br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - le décret n° 2020-293 du 23 mars 2020 ; <br/>
              - les statuts de la Fédération française de football ;<br/>
              - le règlement administratif de la Ligue de football professionnel 2019/2020 ; <br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rouseau, Tapie, avocat de la société US Orléans Loiret football et de l'Union sportive Boulogne Côte d'Opale, de la SCP Foussard, Froger, avocat de la société Le Mans football club, de la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Fédération française de football et de la Ligue de football professionnel ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur les circonstances dans lesquelles est intervenue la décision contestée : <br/>
<br/>
              1.	En raison de l'émergence d'un nouveau coronavirus, de caractère pathogène et particulièrement contagieux, et de sa propagation sur le territoire français, le ministre des solidarités et de la santé, par plusieurs arrêtés successifs pris à compter du 4 mars 2020, a interdit, de façon de plus en plus stricte, les rassemblements, réunions ou activités mettant en présence de manière simultanée un certain nombre de personnes, et a décidé la fermeture d'un nombre croissant de catégories d'établissements recevant du public. Par un décret du 16 mars 2020, le Premier ministre a interdit le déplacement de toute personne hors de son domicile à l'exception des déplacements pour des motifs limitativement énumérés. La loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, terme ultérieurement reporté au 10 juillet 2020 par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire. Le Premier ministre, par un décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, a réitéré le principe de l'interdiction des déplacements, la prohibition de tout rassemblement, réunion ou activité mettant en présence de manière simultanée plus de 100 personnes en milieu clos ou ouvert et la fermeture de la plupart des établissements accueillant du public, notamment les établissements sportifs couverts et les établissements de plein air, ainsi que les établissements dans lesquels sont pratiquées des activités physiques ou sportives. <br/>
<br/>
              2.	A partir de l'intervention du décret du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, les déplacements n'ont été autorisés que dans la limite de cent kilomètres, a été maintenue l'interdiction de tout rassemblement, réunion ou activité à un titre autre que professionnel sur la voie publique ou dans un lieu public, mettant en présence de manière simultanée plus de dix personnes, les établissements sportifs couverts sont demeurés fermés et si les établissements sportifs de plein air ont pu organiser la pratique de certaines activités physiques et sportives, celle des sports collectifs est demeurée interdite. Le décret du 31 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire a maintenu ce régime juridique, sauf pour l'interdiction des déplacements, dans les départements classés en zone orange ; dans ceux classés en zone verte, la pratique des sports collectifs a été rendue possible pour les sportifs de haut niveau et les sportifs professionnels, à l'exception de toute pratique compétitive, cette dernière réserve n'ayant été levée que par un décret du 21 juin 2020. Par ailleurs, aucun événement réunissant plus de 5 000 personnes ne peut se dérouler sur le territoire de la République jusqu'au 30 octobre 2020. <br/>
<br/>
              3.	Le 30 avril 2020, le conseil d'administration de la Ligue de football professionnel a notamment décidé, d'une part, de prononcer l'arrêt définitif des championnats de Ligue 1 et de Ligue 2 pour la saison 2019/2020, d'autre part, pour la Ligue 2, d'arrêter un classement définitif sur la base de celui existant à l'issue de la 28ème journée, d'enregistrer ce classement et d'attribuer le titre de champion de France de Ligue 2 au FC Lorient et, enfin, de renvoyer à l'assemblée générale de la Ligue de football professionnel la question du format du championnat de Ligue 2 pour la saison 2020/2021. <br/>
<br/>
              4.	Lors de son assemblée générale du 20 mai 2020, la Ligue de football professionnel a décidé de porter à vingt-deux le nombre de clubs participant au championnat de Ligue 2 pour la saison 2020-2021, de sorte qu'aucune relégation vers le championnat de National 1 n'aurait lieu. Toutefois, le 27 mai 2020, le comité exécutif de la Fédération française de football a réformé et privé de tout effet la décision de la Ligue de football professionnel et décidé que seraient rétrogradés en National 1 les clubs classés 19ème et 20ème de la Ligue 2 à l'issue de la saison 2019-2020. Par une décision du 5 juin 2020, le conseil d'administration de la Ligue de football professionnel en a pris acte en prononçant la relégation en National 1 des clubs du Mans FC et de l'US Orléans.<br/>
<br/>
              5.	La société Union Sportive Orléans Loiret Football et la société anonyme sportive professionnelle (SASP) Le Mans Football Club, respectivement sous les n°s 441444 et 441451, demandent l'annulation pour excès de pouvoir de la décision du 20 mai 2020 et, par voie de conséquence, de celle du 5 juin 2020 en tant qu'elle prononce les relégations en National 1.<br/>
<br/>
              6.	Par cette même décision du 5 juin 2020, le conseil d'administration de la Ligue a par ailleurs décidé, sous réserve de l'accord de la Fédération, de renoncer à l'organisation du barrage entre le 18ème de Ligue 2 et le 3ème de National 1 pour la saison 2019/2020. Par une décision du 22 juin 2020, le comité exécutif de la Fédération française de football a approuvé cette décision.<br/>
<br/>
              7.	L'Union sportive Boulogne Côte d'Opale sous le n° 442159 demande l'annulation pour excès de pouvoir de ces deux décisions. <br/>
<br/>
              8.	Les différentes requêtes présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.  <br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              9.	D'une part, aux termes de l'article L. 131-14 du code du sport : " Dans chaque discipline sportive et pour une durée déterminée, une seule fédération agréée reçoit délégation du ministre chargé des sports ". Aux termes de l'article L. 131-15 du même code : " Les fédérations délégataires : 1° Organisent les compétitions sportives à l'issue desquelles sont délivrés les titres internationaux, nationaux, régionaux ou départementaux (...) ". Aux termes de l'article L. 131-16 du même code : " Les fédérations délégataires édictent : 1° Les règles techniques propres à leur discipline ainsi que les règles ayant pour objet de contrôler leur application et de sanctionner leur non-respect par les acteurs des compétitions sportives (...). " Aux termes de l'article R. 131-32 du même code : " Les règles techniques édictées par les fédérations sportives délégataires comprennent : 1° Les règles du jeu applicables à la discipline sportive concernée ; / 2° Les règles d'établissement d'un classement national, régional, départemental ou autre, des sportifs, individuellement ou par équipe ; / 3° Les règles d'organisation et de déroulement des compétitions ou épreuves aboutissant à un tel classement ; / 4° Les règles d'accès et de participation des sportifs, individuellement ou par équipe, à ces compétitions et épreuves. " Aux termes de l'article L. 132-1 du même code : " Les fédérations sportives délégataires peuvent créer une ligue professionnelle, pour la représentation, la gestion et la coordination des activités sportives à caractère professionnel des associations qui leur sont affiliées et des sociétés sportives (...). " Aux termes de l'article R. 132-1 du même code, une fédération sportive délégataire peut créer une ligue professionnelle dotée de la personnalité morale soit pour organiser les compétitions sportives qu'elle définit, soit pour fixer, pour les compétitions sportives qu'elle définit, leurs conditions d'organisation et celles de la participation des sportifs. En application de l'article R. 132-12 du même code, la réglementation et la gestion des compétitions mentionnées à l'article R. 132-1 relèvent de la compétence de la ligue professionnelle, sous réserve des compétences propres de la fédération et des compétences exercées en commun par la fédération et par la ligue mentionnées aux articles R. 132-10 et R. 132-11. <br/>
<br/>
              10.	D'autre part, les relations entre la fédération délégataire et la ligue qu'elle crée sont, en vertu de l'article R. 132-9 du code du sport, fixées par une convention qui, selon l'article R. 132-15 du même code, " précise les conditions dans lesquelles l'instance dirigeante de la fédération peut réformer les décisions arrêtées par les organes de la ligue professionnelle qui sont contraires aux statuts ou aux règlements de la fédération ". Par une convention conclue entre la Fédération française de football et la Ligue de football professionnel en application de l'article R. 132-9, la gestion du football professionnel a été déléguée à la Ligue de football professionnel, notamment chargée d'organiser, de gérer et de réglementer le championnat de Ligue 1 et le championnat de Ligue 2. Aux termes de l'article 5 de cette convention : " A l'exception des décisions d'ordre disciplinaire le Comité Exécutif peut se saisir, conformément à l'article 13 du Règlement Intérieur de la F.F.F., pour éventuellement les réformer, de toutes les décisions prises par l'Assemblée et par les instances élues ou nommées de la L.F.P., qu'il jugerait contraires à l'intérêt supérieur du football ou aux statuts et règlements ".<br/>
<br/>
              Sur les deux relégations de Ligue 2 en National 1 :<br/>
<br/>
              En ce qui concerne la décision du comité exécutif de la Fédération française de football du 27 mai 2020 réformant, pour la priver d'effet, la décision du 20 mai 2020 de l'assemblée générale de la Ligue de football professionnel portant à vingt-deux le nombre de clubs participant au championnat de Ligue 2 pour la saison 2020-2021 :<br/>
<br/>
              Quant à la légalité externe :<br/>
<br/>
              11.	Aux termes de l'article 199 des règlements généraux de la Fédération française de football, auquel renvoie l'article 13 du règlement intérieur de la Fédération combiné avec l'article 5 de la convention entre la Fédération et la Ligue de football professionnel qui détermine, conformément à ce que prévoit l'article R. 132-15 du code du sport, les conditions dans lesquelles la Fédération française de football peut réformer les décisions de la Ligue de football professionnel : " 1. Pour éventuellement les réformer, dès lors qu'il les jugerait contraires à l'intérêt supérieur du football ou aux statuts et règlements, le Comité Exécutif peut se saisir de toutes décisions sauf en matière disciplinaire. / 2. A peine de nullité, la demande d'évocation devra être revêtue de la signature d'au moins six membres du Comité Exécutif. / 3. Cette demande doit être adressée au Secrétariat du Comité Exécutif dans un délai maximum de dix jours, suivant la date de notification ou de publication de la décision définitive contestée. / 4. Si le Comité Exécutif se saisit lui-même, le délai est porté à un mois. 5. La procédure est exclusivement écrite, tout intéressé pouvant faire valoir par écrit son argumentation qui est soumise à l'examen du Comité Exécutif ".<br/>
<br/>
              12.	En premier lieu, il ressort des pièces du dossier que des demandes d'évocation ont été présentées par onze membres du comité exécutif. Par ailleurs, ces demandes ayant été signées de façon manuscrite, les dispositions du code civil relatives à la signature électronique ne peuvent être utilement invoquées. Par suite, les moyens tirés de l'irrégularité de la demande d'évocation doivent être écartés. <br/>
<br/>
              13.	En deuxième lieu, si la Fédération française de football n'était pas tenue, s'agissant de la réformation de dispositions réglementaires, d'inviter les clubs requérants à produire des observations avant de prendre la décision contestée, il lui était loisible de le faire et, en ce cas, elle devait leur laisser un délai suffisant à cette fin. Dans les circonstances de l'espèce, le délai de quarante-huit heures qui leur avait été imparti ne saurait être regardé comme insuffisant. Le moyen tiré de l'irrégularité de la procédure contradictoire préalable ne peut, par suite, qu'être écarté. <br/>
<br/>
              14.	En troisième lieu, la décision contestée, qui, ainsi qu'il vient d'être dit, procède à la réformation de dispositions réglementaires, n'avait pas à être motivée.<br/>
<br/>
              Quant à la légalité interne :<br/>
<br/>
              15.	Il résulte de ce qui a été dit au point 10 qu'il revient à la Fédération française de football, le cas échéant, de réformer les décisions de la Ligue qui seraient contraires à ses statuts et règlements ou qui porteraient atteinte aux intérêts généraux dont elle a la charge. <br/>
<br/>
              16.	Il ressort des pièces du dossier que le comité exécutif de la Fédération française de football, confronté à l'impossibilité de poursuivre les championnats amateurs, a fait le choix, le 16 avril 2020, s'agissant de ces championnats, d'arrêter des classements au vu des matchs joués, et de procéder à des accessions au niveau supérieur et à des relégations au niveau inférieur sur la base des classements ainsi arrêtés alors que l'architecture pyramidale des championnats amateurs crée une interdépendance entre les différents niveaux et était susceptible de rendre particulièrement complexe l'application de règles différentes. Par ailleurs, la Ligue de football professionnel a retenu un parti identique pour la Ligue 1 et la Ligue 2, en ce qui concerne le principe et les modalités du classement ainsi que le choix de procéder à des accessions et des relégations entre l'une et l'autre. En revanche, elle a décidé de porter à vingt-deux le nombre de clubs participant au championnat de Ligue 2 pour la saison 2020-2021, de sorte qu'aucune relégation vers le championnat de National 1 n'ait lieu. La conséquence de cette décision a donc été de déroger, dans le seul cas de la Ligue 2, à la règle, par ailleurs applicable à l'ensemble des championnats professionnels et amateurs, selon laquelle la saison 2019-2020 donnerait lieu à des relégations. Dès lors, en se fondant à la fois sur l'intérêt s'attachant à ce que le système des accessions et relégations, solution regardée comme la plus conforme à la prise en compte du mérite sportif, s'applique de façon uniforme et sans exception à l'intégralité des compétitions, sur le risque de distorsion des règles applicables selon les championnats, ainsi que sur les possibles conséquences en cascade de l'absence de relégation de clubs de Ligue 2 en National 1, le comité exécutif de la Fédération française de football a pu légalement estimer que la décision de l'assemblée générale de la Ligue du 20 mai 2020 portait atteinte aux intérêts généraux de la discipline, pris dans leur ensemble, dont la Fédération a la charge. <br/>
<br/>
              17.	Il résulte de l'instruction que la Fédération française de football aurait pris la même décision si elle ne s'était fondée que sur ces motifs, qui sont, à eux seuls, de nature à la fonder. <br/>
<br/>
              18.	Il résulte de tout ce qui précède que la société US Orléans Loiret Football et la SASP Le Mans Football Club ne sont pas fondées à demander l'annulation pour excès de pouvoir de la décision du comité exécutif de la Fédération française de football du 27 mai 2020.<br/>
<br/>
              En ce qui concerne la décision du conseil d'administration de la Ligue de football professionnel du 5 juin 2020 en tant qu'elle prononce la relégation en National 1 des clubs classés 19ème et 20ème de la Ligue 2 :<br/>
<br/>
              19.	Si la société US Orléans Loiret Football et la SASP Le Mans Football Club demandent l'annulation pour excès de pouvoir de la décision du 5 juin 2020, en tant qu'elle prononce la relégation de leurs clubs professionnels, par voie de conséquence de l'annulation de la décision du comité exécutif de la Fédération française de football du 27 mai 2020, il résulte de ce qui a été dit au point 18 que ces conclusions ne peuvent qu'être rejetées.<br/>
<br/>
              Sur la suppression du barrage entre le 18ème de Ligue 2 et le 3ème de National 1 :<br/>
<br/>
              En ce qui concerne la décision du conseil d'administration de la Ligue de football professionnel du 5 juin 2020 en tant qu'elle renonce à l'organisation des barrages :<br/>
<br/>
              20.	En premier lieu, d'une part, il ressort des pièces du dossier que la convocation à la réunion du conseil d'administration du 5 juin 2020 a été adressée aux membres de droit avec voix consultative, sans qu'ait d'incidence le fait que deux d'entre eux n'aient pas été présents à cette réunion. D'autre part, il ressort du procès-verbal de cette réunion que MM. Belgodere et Diallo et Mme A... ont assisté régulièrement à la séance en leurs qualités respectives de directeurs de Première Ligue et de directeur général de l'Union des clubs professionnels de football, membres avec voix consultative du conseil d'administration conformément aux dispositions de l'article 18 des statuts de la Ligue. Il s'ensuit que les moyens tirés de l'irrégularité de la délibération du 5 juin 2020 du conseil d'administration de la Ligue de football professionnel doivent être écartés.<br/>
<br/>
              21.	En deuxième lieu, aux termes de l'article 24 des statuts de la Ligue de football professionnel, le conseil d'administration a compétence pour " établir le règlement administratif de la Ligue et le règlement des compétitions qu'elle organise ". Ces dispositions investissent en principe le conseil d'administration de la Ligue de la compétence d'édicter toute disposition réglementaire régissant les compétitions organisées par la Ligue.<br/>
<br/>
              22.	D'une part, s'il appartient, aux termes de l'article 12 des statuts de la Ligue de football professionnel, à l'assemblée générale de la Ligue de décider du changement de format des compétitions organisées par la Ligue, dans la limite des termes de la convention liant la Ligue à la Fédération, cette notion de format doit être entendue comme visant le nombre de clubs admis à participer aux championnats. La décision de ne pas organiser de barrage ne remettant pas en cause, par elle-même, le format des compétitions, elle ne relevait pas de la compétence de l'assemblée générale de la Ligue résultant de l'article 12 des statuts de la Ligue, mais bien de la compétence du conseil d'administration découlant de son article 24. <br/>
<br/>
              23.	D'autre part, si, aux termes de l'article 411 du règlement administratif de la Ligue de football professionnel 2019/2020, la commission des compétitions " est compétente pour l'organisation tant de la compétition que des matchs du championnat " de Ligue 2 et le bureau de la Ligue " pour décider de sa propre initiative de la programmation d'un match, dans le cas de circonstances exceptionnelles ", ces dispositions n'investissent pas ces organes du pouvoir réglementaire d'organisation des compétitions qui est dévolu, ainsi qu'il a été dit, en vertu de l'article 24 des statuts, au conseil d'administration de la Ligue.<br/>
<br/>
              24.	Par suite, le moyen tiré de ce que le conseil d'administration n'aurait pas eu compétence pour prendre la décision contestée, qui adopte des dispositions réglementaires dérogeant au règlement normalement applicable aux compétitions, doit être écarté.<br/>
<br/>
              25.	En troisième lieu, dès lors que le conseil d'administration de la Ligue de football professionnel, tout comme le comité exécutif de la Fédération française de football, avaient écarté le principe d'une " saison blanche ", il leur appartenait de décider selon quelles modalités les relégations et accessions auraient lieu. Alors qu'aux dates des 16 et 30 avril 2020, tant la Fédération que la Ligue avaient constaté l'impossibilité de poursuivre les championnats, alors qu'en juin 2020, une très grande incertitude affectait encore l'évolution de la situation sanitaire et obérait l'hypothèse d'une possible tenue du barrage en temps utile et dans des conditions préservant la santé de tous les acteurs, et alors que les clubs participant aux deux championnats concernés avaient besoin de visibilité quant à leur devenir sportif pour la prochaine saison et que la suppression du barrage avait été décidée pour les autres championnats de football, il n'apparaît pas que la Ligue aurait porté une appréciation manifestement erronée, y compris au regard de la prise en compte des intérêts éthiques et des objectifs d'équité du sport, en renonçant à l'organisation du barrage entre le 3ème de National 1 et le 18ème de Ligue 2.  <br/>
<br/>
              26.	En quatrième lieu, eu égard aux circonstances qui ont conduit à l'interruption des compétitions, la décision attaquée, qui ne prive le club classé 3ème de National 1 que d'une chance d'accéder à la division supérieure, un tel accès ne pouvant de toute façon être regardé comme acquis au vu des résultats obtenus au cours des journées de championnat qui ont pu se tenir, eu égard aux aléas inhérents au déroulement de la compétition, ne peut être regardée comme méconnaissant le principe d'égalité de traitement entre les équipes participant à un même championnat. Au surplus, en raison de la différence de situation entre le club classé 3ème de National 1 et ceux classés 1er et 2ème, laquelle résulte des dispositions réglementaires qui ne prévoyaient d'accession automatique que pour ces derniers, la circonstance que la décision attaquée ait eu pour effet de supprimer la possibilité pour le club classé 3ème de National 1 d'accéder éventuellement à la Ligue 2 alors que les équipes classées aux deux premières places ont conservé le bénéfice d'une telle accession ne traduit pas d'atteinte au principe d'égalité. <br/>
<br/>
              27.	Il résulte de tout ce qui précède que l'Union sportive Boulogne Côte d'Opale n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision du conseil d'administration de la Ligue de football professionnel du 5 juin 2020 en tant qu'elle renonce à l'organisation du barrage.<br/>
<br/>
              En ce qui concerne la décision du comité exécutif de la Fédération française de football du 22 juin 2020 approuvant la suppression du barrage :<br/>
<br/>
              28.	En premier lieu, si, aux termes de l'article 11 des statuts de la Fédération française de football, l'assemblée fédérale adopte et amende les règlements généraux et les dispositions des règlements des compétitions nationales relatives au nombre de clubs, aux accessions et aux rétrogradations, l'article 18 des mêmes statuts donne compétence au comité exécutif, qui " administre, dirige et gère la Fédération ", pour statuer sur " tous les problèmes présentant un intérêt supérieur pour le football et sur tous les cas non prévus par les statuts ou règlements ". Par ailleurs, l'article 3 des règlements généraux dispose que " le comité exécutif peut, en application de l'article 18 des statuts, prendre toute mesure modificative ou dérogatoire que dicterait l'intérêt supérieur du football " et " rend compte de ses décisions à la plus proche assemblée fédérale. ". En l'absence, dans la réglementation des compétitions organisées par la Fédération française de football, de dispositions ayant prévu par avance des règles à suivre lorsque des circonstances imprévues conduisent à interrompre les compétitions de façon définitive avant leur terme, il appartenait au comité exécutif de la Fédération de déterminer les conséquences à tirer de l'interruption des championnats, le cas échéant en dérogeant aux règles relatives aux accessions et relégations. Dans ces conditions, le moyen tiré de l'incompétence du comité exécutif pour prendre la décision contestée ne peut qu'être écarté.<br/>
<br/>
              29.	En second lieu, pour les motifs énoncés aux points 25 et 26, les moyens tirés de l'erreur manifeste d'appréciation et de l'atteinte au principe d'égalité et à l'équité sportive doivent être écartés.<br/>
<br/>
              30.	Il résulte de tout ce qui précède que les conclusions des requêtes de la société US Orléans Loiret Football, de la SASP Le Mans Football Club et de l'Union sportive Boulogne Côte d'Opale doivent être rejetées, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des requérants une somme au titre du même article.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les requêtes de la société US Orléans Loiret Football, de la SASP Le Mans Football Club et de l'Union sportive Boulogne Côte d'Opale sont rejetées.<br/>
<br/>
Article 2 : Les conclusions présentées par la Fédération française de football et la Ligue de football professionnel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société US Orléans Loiret Football, à la SASP Le Mans Football Club, à l'Union sportive Boulogne Côte d'Opale, à la Ligue de football professionnel et à la Fédération française de football.<br/>
Copie en sera adressée au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
