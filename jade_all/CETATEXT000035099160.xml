<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035099160</ID>
<ANCIEN_ID>JG_L_2017_06_000000411589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/91/CETATEXT000035099160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/06/2017, 411589, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411589.20170629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 16 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la SCP GPN Office Notarial du Perreux-sur-Marne et M. B...A...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du 16 septembre 2016 pris en application de l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, et ses annexes, par lequel le garde des sceaux, ministre de la justice et le ministre de l'économie et des finances ont établi la carte des zones d'installation des notaires et fixé les recommandations sur le rythme d'installation. <br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que, d'une part, les arrêtés de nomination pourraient intervenir avant que le recours en annulation ne soit jugé et, d'autre part, l'éventuelle remise en cause, après plusieurs mois de fonctionnement, de la légalité de la création de plusieurs offices notariaux et de la désignation de leur titulaire, qui pourrait découler de l'illégalité de l'arrêté contesté, serait de nature à porter gravement atteinte à l'intérêt général ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'avis de l'Autorité de la concurrence du 9 juin 2016, sur lequel est fondé l'arrêté contesté, est entaché d'illégalité en ce que :<br/>
              1°) il ne justifie pas des exigences posées aux articles L. 461-2 et L. 461-3 du code de commerce,<br/>
              2°) il a retenu le découpage préétabli de la " zone d'emploi " définie par l'INSEE pour déterminer les zones d'installation libre et les zones d'installation contrôlée, sans effectuer l'analyse du détail de la localisation géographique des usagers des services notariaux requise par l'article 52 de la loi du 6 août 2015 et le décret du 26 février 2016, entraînant notamment un surdimensionnement de la zone de Paris,<br/>
              - l'arrêté contesté est insuffisamment motivé dès lors qu'il ne comporte aucun détail des critères ayant conduit à la délimitation des différentes zones d'installation, ni ne justifie que les critères règlementaires ont bien été pris en compte ; <br/>
              - il est entaché d'une erreur de droit dès lors que l'avis de l'Autorité de la concurrence sur lequel il se fonde pour définir les zones d'installation  n'a pas pris en compte les critères définis par le décret du 26 février 2016 ;<br/>
              - le zonage retenu est susceptible de bouleverser les conditions d'activité des offices existants, laissant la concentration géographique des offices au hasard des demandes des candidats ;<br/>
              - le nombre d'installations recommandées fixé par l'arrêté contesté est entaché d'une erreur d'appréciation en ce qu'il se fonde sur les recommandations de l'avis de l'Autorité de la concurrence qui sont surévaluées, notamment dans la zone Paris. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi du 25 ventôse an XI ;<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le décret n° 73-609 du 5 juillet 1973 ;<br/>
              - le décret n° 2016-216 du 26 février 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              2. Aux termes de l'article 52 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " I.- les notaires, les huissiers de justice et les commissaires priseurs judiciaires  peuvent librement s'installer dans les zones où l'implantation d'offices apparaît utile pour renforcer la proximité ou l'offre de service. / Ces zones sont déterminées par une carte établie conjointement par les ministres de la justice et de l'économie, sur proposition de l'Autorité de la concurrence en application de l'article L. 462-4-1 du code de commerce.../(...) cette carte est assortie de recommandations sur le rythme d'installation compatible avec une augmentation progressive du nombre de professionnels de la zone concernée " (...) / II.- dans les zones mentionnées au I, lorsque le demandeur remplit les conditions de nationalité, d'aptitude, d'honorabilité, d'expérience et d'assurance, requises pour être nommé en qualité de notaire (...) le ministre de la justice le nomme titulaire de l'office de notaire (...) créé. Un décret précise les conditions d'application du présent alinéa. ".<br/>
<br/>
              3. A la suite de l'intervention du décret n° 2016-216 du 26 février 2016 relatif à l'établissement de la carte instituée par le I de l'article 52 précité, l'Autorité de la concurrence a, par un avis n° 16-A-13 du 9 juin 2016, proposé une carte des zones d'implantation, assortie de recommandations sur le rythme de création de nouveaux offices notariaux puis, par un arrêté du 16 septembre 2016, le garde des sceaux, ministre de la justice et le ministre de l'économie et des finances ont établi la carte d'installation des notaires pour une période de deux ans. Par ailleurs, le décret n° 2016-661 du 20 mai 2016 a notamment modifié, en application des dispositions du II de l'article 52 précité, les dispositions du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire concernant la nomination des notaires dans les offices créés. Par un arrêté du 24 janvier 2017, le garde des sceaux, ministre de la justice a fixé les modalités des opérations du tirage au sort éventuellement nécessaire pour ces nominations. Les opérations de tirage au sort ont débuté en février 2017 et se sont poursuivies. <br/>
<br/>
              4. Les requérants ont introduit le 21 novembre 2016 un recours en annulation dirigé contre l'arrêté du 16 septembre 2016. Leur demande de suspension a été enregistrée le 16 juin 2017, soit 10 mois après l'intervention de l'arrêté attaqué et 8 mois après l'introduction de leur recours en annulation, or ils ne font état d'aucun élément véritablement nouveau, intervenu entre temps, qui serait de nature à caractériser la survenance d'une situation d'urgence. La circonstance que, pour des offices créés dans les zones ayant d'ores et déjà fait l'objet d'un tirage au sort, des arrêtés de nomination seraient susceptibles d'intervenir avant que ne soit jugé le recours au fond ce qui créerait des situations difficilement réversibles, n'est pas, compte tenu de l'objet de l'arrêté contesté, de nature à créer une situation d'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative, alors, d'une part, que la suspension de la carte entraînerait une grave perturbation dans la mise en oeuvre des dispositions de l'article 52 de la loi du 6 août 2015 et, d'autre part, que la requête en annulation dont est saisi le Conseil d'Etat est susceptible d'être examinée dans un délai de quatre mois. <br/>
<br/>
              5. Il résulte de ce qui précède que la condition d'urgence requise par l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie, sans qu'il soit besoin de se prononcer sur l'existence d'un doute sérieux quant à la légalité de l'arrêté contesté. La requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit dès lors être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la SCP GPN Office Notarial du Perreux-sur-Marne et de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la SCP GPN Office Notarial du Perreux-sur-Marne, à M. B...A...et à la garde des sceaux, ministre de la justice.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
