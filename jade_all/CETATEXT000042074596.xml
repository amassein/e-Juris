<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074596</ID>
<ANCIEN_ID>JG_L_2020_02_000000429228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/45/CETATEXT000042074596.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/02/2020, 429228, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429228.20200210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Averous et Simay a demandé au tribunal administratif de Marseille de condamner le département des Bouches-du-Rhône à lui verser une somme de 66 400 euros TTC, assortie des intérêts légaux à compter du 15 septembre 2014, correspondant au montant de la prime qui aurait dû lui être versée dans le cadre du concours de maîtrise d'oeuvre relatif au projet de restructuration et d'extension du collège Versailles à Marseille. Par un jugement n° 1501065 du 4 juillet 2017, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA03703 du 28 janvier 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Averous et Simay contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 mars et 28 juin 2019 et le 17 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la société Averous et Simay demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département des Bouches-du-Rhône la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - la loi n° 85-704 du 12 juillet 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société Averous et Simay, et à la SCP Lyon-Caen, Thiriez, avocat du département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que le département des Bouches-du-Rhône, agissant par son mandataire, la société Terra Treize, a organisé un concours d'architecture et d'ingénierie sur esquisse en vue de l'attribution du marché de maîtrise d'oeuvre de la restructuration et de l'extension du collège Versailles à Marseille, le nombre de concurrents admis à participer étant fixé à cinq. Le 18 décembre 2013, le pouvoir adjudicateur a dressé la liste des cinq équipes de concepteurs admises à concourir, parmi lesquelles un groupement ayant pour mandataire la société Averous et Simay, qui a présenté une offre. Par des courriers du 15 septembre, du 17 novembre et du 12 décembre 2014, la société Averous et Simay a réclamé le paiement de la prime de concours. Par un courrier du 3 décembre 2014, la société Terra Treize l'a informée de ce que le maître de l'ouvrage avait décidé, le 24 novembre 2014, de ne pas donner suite à la procédure. Par un courrier du 12 décembre 2014, la société Terra Treize lui a indiqué que le département avait décidé de ne pas lui allouer la prime de concours, conformément à la proposition du jury du concours, réuni le 2 juillet 2014, qui avait estimé que sa prestation ne répondait pas au programme. Par un jugement du 4 juillet 2017, le tribunal administratif de Marseille a rejeté sa demande tendant à ce que le département des Bouches-du-Rhône soit condamné à lui verser la somme de 66 400 euros TTC, assortie des intérêts légaux à compter du 5 décembre 2014, correspondant au montant de cette prime. La société Averous et Simay se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 28 janvier 2019 qui a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 5 du règlement du concours : " 5.1. Composition du jury de concours : Le jury, arrêté par le maître d'ouvrage dans le respect des dispositions de l'article 24 du code des marchés publics, est composé des membres suivants : a) Membres avec voix délibératives : - le président du jury / - les cinq membres de la commission d'appel d'offres ou leurs suppléants / - les personnes désignées par le président du jury conformément à l'article 24.I du code des marchés publics. / b) Membres avec voix consultatives : - le payeur départemental, comptable public, ou son représentant / - le représentant du service en charge de la concurrence / - les agents du pouvoir adjudicateur compétents dans la matière qui fait l'objet de la consultation ; (...) 5.3. Suite à donner à la consultation : Le jury, après examen des prestations, formule un avis motivé et dresse un procès-verbal : le procès-verbal indiquera notamment : l'organisation et le déroulement du jury, les noms des concurrents exclus du jugement du concours et les motifs d'exclusion, l'avis motivé du jury, la proposition finale de classement des projets par ordre décroissant et de versement des indemnités. L'anonymat sera levé une fois que le procès-verbal sera signé par tous les membres du jury à voix délibérative (...). Le lauréat de concours ainsi que chaque concurrent non retenu ayant remis des prestations répondant au programme, recevra une prime d'un montant de 66 400 euros TTC. L'indemnité de concours est décomposée de la façon suivante : - maquette : 6 400 euros TTC / - esquisse : 60 000 euros TTC. / Dans le cas où une offre serait incomplète ou ne répondrait pas au programme, une réduction ou la suppression de la prime pourra être effectuée par le maître de l'ouvrage sur proposition du jury ".<br/>
<br/>
              3. En interprétant ces stipulations comme excluant qu'un candidat dont l'offre ne répondait pas au programme puisse percevoir une prime, alors qu'il résultait de leurs termes mêmes qu'en un tel cas, il appartient au maître d'ouvrage, sur proposition du jury, de déterminer s'il convient de la verser, de la réduire ou de la supprimer, la cour administrative d'appel de Marseille les a dénaturées. La société Averous et Simay est dès lors fondée, sans qu'il soit besoin d'examiner ses autres moyens, à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département des Bouches-du-Rhône la somme de 3 000 euros à verser à la société Averous et Simay au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce que la somme que demande le département des Bouches-du-Rhône au même titre soit mise à la charge de la société Averous et Simay qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 janvier 2019 de la cour administrative d'appel de Marseille est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le département des Bouches-du-Rhône versera une somme de 3 000 euros à la société Averous et Simay au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par le département sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Averous et Simay et au département des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
