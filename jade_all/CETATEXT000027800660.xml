<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027800660</ID>
<ANCIEN_ID>JG_L_2013_08_000000368814</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/80/06/CETATEXT000027800660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 01/08/2013, 368814, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368814</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368814.20130801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12BX00907 du 15 mai 2013, enregistrée le 24 mai 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 343-3 du code de justice administrative, la requête présentée devant cette cour par la commune d'Orgueil ;<br/>
<br/>
              Vu la requête, enregistrée le 10 avril 2012 au greffe de la cour administrative d'appel de Bordeaux, présenté par la commune d'Orgueil (82370), représentée par son maire ; la commune demande :<br/>
<br/>
              1°) d'annuler le jugement n° 0802945 du 9 février 2012 par lequel le tribunal administratif de Toulouse a annulé, à la demande de M. A...B..., d'une part, la décision du 30 mai 2008 par laquelle le maire lui a délivré un certificat d'urbanisme négatif pour le projet de construction d'une maison d'habitation et, d'autre part, les deux arrêtés du 30 mai 2008 par lesquels le maire s'est opposé à ses déclarations préalables des 7 avril et 6 mai 2008 tendant à la division d'un terrain en deux lots à bâtir puis en un lot à bâtir ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions dirigées contre le jugement attaqué, en tant qu'il annule le certificat d'urbanisme du 30 mai 2008 :<br/>
<br/>
              1. Considérant que des demandes distinctes relevant de voies de recours différentes ne sauraient présenter entre elles un lien de connexité sauf, au profit de l'appel, dans les cas limitativement énumérés par l'article R. 811-1 du code de justice administrative ; que, par suite, le Conseil d'Etat saisi d'un pourvoi en cassation n'a pas compétence pour connaître, par la voie de la connexité, des conclusions d'une requête relevant de la compétence d'appel d'une cour administrative d'appel ;<br/>
<br/>
              2. Considérant que les litiges relatifs aux certificats d'urbanisme ne sont pas au nombre des litiges dans lesquels le tribunal administratif statue en premier et dernier ressort en application des dispositions combinées des articles R. 811-1 et R. 222-13 du code de justice administrative ; que, par suite, alors même que le tribunal administratif de Toulouse a statué par une seule décision sur les conclusions de M. B...dirigées contre, d'une part, la décision du 30 mai 2008 par laquelle le maire de la commune d'Orgueil lui a délivré un certificat d'urbanisme négatif pour le projet de construction d'une maison d'habitation et, d'autre part, les deux arrêtés du 30 mai 2008 par lesquels le maire s'est opposé à ses déclarations préalables des 7 avril et 6 mai 2008 tendant à la division d'un terrain en deux lots à bâtir puis en un lot à bâtir, le Conseil d'Etat n'est pas compétent pour connaître des conclusions de la requête de la commune d'Orgueil tendant à l'annulation du jugement du tribunal administratif de Toulouse du 9 février 2012 en tant que celui-ci annule le certificat d'urbanisme négatif du 30 mai 2008 ; qu'il y a lieu, dès lors, de renvoyer le jugement de ces conclusions à la cour administrative d'appel de Bordeaux ; <br/>
<br/>
              Sur les conclusions dirigées contre le jugement attaqué, en tant qu'il annule les décisions d'opposition à déclaration préalable du 30 mai 2008 :<br/>
<br/>
              3. Considérant que le tribunal administratif de Toulouse était compétent pour connaître en premier et dernier ressort, par application des dispositions combinées de l'article R. 811-1 et du 1° de l'article R. 222-13 du code de justice administrative, des conclusions de la requête de M. B...tendant à l'annulation des deux arrêtés du 30 mai 2008 par lesquels le maire d'Orgueil s'est opposé à ses déclarations préalables des 7 avril et 6 mai 2008 tendant à la division d'un terrain en deux lots à bâtir puis en un lot à bâtir, qui sont au nombre des déclarations préalables prévues par l'article L. 421-4 du code de l'urbanisme ; que, par suite, les conclusions de la commune tendant à l'annulation du jugement du tribunal administratif de Toulouse du 9 février 2012 en tant qu'il annule ces arrêtés revêtent le caractère d'un pourvoi en cassation ;<br/>
<br/>
              4. Considérant toutefois que ces conclusions ont été présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, alors que ce ministère est obligatoire en vertu de l'article R. 821-3 du code de justice administrative ; que, par suite, en l'absence de mention de cette obligation dans la notification du jugement attaqué, il y a lieu de surseoir à statuer sur cette partie des conclusions présentées par la commune d'Orgueil afin de lui permettre de régulariser sa requête dans un délai d'un mois à compter de la notification de la présente décision ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la commune d'Orgueil dirigées contre le jugement du tribunal administratif de Toulouse du 9 février 2012 en tant qu'il a annulé le certificat d'urbanisme du 30 mai 2008 est attribué à la cour administrative d'appel de Bordeaux.<br/>
Article 2 : Il est sursis à statuer sur les conclusions de la commune d'Orgueil dirigées contre le jugement du 9 février 2012 du tribunal administratif de Toulouse en tant qu'il a annulé les deux arrêtés du 30 mai 2008 d'opposition à déclaration préalable, afin de lui permettre de régulariser son pourvoi en recourant au ministère d'un avocat au Conseil d'Etat et à la Cour de cassation. Un délai d'un mois à compter de la notification de la présente décision lui est imparti à cet effet.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Orgueil, à M. A...B...et au président de la cour administrative d'appel de Bordeaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
