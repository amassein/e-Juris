<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033685047</ID>
<ANCIEN_ID>JG_L_2016_12_000000389318</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/50/CETATEXT000033685047.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 23/12/2016, 389318, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389318</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:389318.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Siva a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre des années 2008 à 2010 et de la période couverte par ces années, ainsi que des pénalités correspondantes. Par un jugement n° 1305964 du 11 décembre 2013, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA00722 du 3 février 2015, la cour administrative d'appel de Paris a déchargé la société Siva des cotisations supplémentaires d'impôt sur les sociétés et des pénalités correspondantes auxquelles elle a été assujettie, au titre des années 2008 à 2010, du fait de la réintégration dans son résultat des dépenses relatives à la villa de Ramatuelle et a rejeté le surplus des conclusions de sa requête d'appel. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 avril et 9 juillet 2015 et 30 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société Siva demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'ensemble de ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes ;<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Société Siva ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Siva, qui exerce une activité de marchand de biens, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier 2008 au 31 décembre 2010 à l'issue de laquelle l'administration a remis en cause la déduction de son résultat imposable de sommes correspondant à des travaux et dépenses de fonctionnement concernant une villa située à Ramatuelle, ainsi que la déductibilité de la taxe sur la valeur ajoutée afférente à ces dépenses. La société a contesté les cotisations supplémentaires d'impôt sur les sociétés et les rappels de taxe sur la valeur ajoutée, ainsi que les pénalités correspondantes, résultant de ces rehaussements. Par un jugement du 11 décembre 2013, le tribunal administratif de Paris a rejeté sa demande de décharge. Par l'arrêt attaqué du 3 février 2015, la cour administrative d'appel de Paris l'a déchargée des cotisations supplémentaires d'impôt sur les sociétés et des pénalités correspondantes résultant de la réintégration, dans son résultat imposable, des dépenses relatives à la villa de Ramatuelle mais a rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              2. Aux termes du 1 du I de l'article 271 du code général des impôts : " La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération ". Si, dans sa rédaction issue du décret n° 98-1141 du 15 décembre 1998, le 1 de l'article 230 de l'annexe II à ce code disposait que : " La taxe sur la valeur ajoutée ayant grevé les biens et services que les assujettis à cette taxe acquièrent ou qu'ils se livrent à eux-mêmes n'est déductible que si ces biens et services sont nécessaires à l'exploitation ", les dispositions de cet article ont été abrogées, à compter du 1er janvier 2008, par l'article 1er du décret n°2007-566 du 16 avril 2007. Par suite, en se fondant notamment sur ces dernières dispositions pour confirmer le bien-fondé du rappel de taxe sur la valeur ajoutée opéré par l'administration fiscale au titre de la période du 1er janvier 2008 au 31 décembre 2010, la cour a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              3. Il résulte de ce qui précède que la société Siva est fondée, par ce seul moyen, à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté le surplus des conclusions de sa requête. Il n'est pas nécessaire, en conséquence, d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Siva sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Paris du 3 février 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à la société Siva une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Siva et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
