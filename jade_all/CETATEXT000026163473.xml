<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026163473</ID>
<ANCIEN_ID>JG_L_2012_07_000000356749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/16/34/CETATEXT000026163473.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 09/07/2012, 356749</TITRE>
<DATE_DEC>2012-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356749.20120709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>I. Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 février et 14 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société par actions simplifiée Sepur, dont le siège est 54, rue Alexandre Dumas B.P. 53 à Plaisir (78370) ;  elle demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 10VE01208 du 29 novembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement n° 0709873 du 16 février 2010 par lequel le  tribunal administratif de Versailles a rejeté sa demande de décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles auxquels elle a été assujettie au titre des années 1996 à 2001 ainsi que des pénalités correspondantes, en deuxième lieu, de prononcer la décharge de ces impositions et des pénalités correspondantes et, en dernier lieu, de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
<br/>
<br/>
              II. Vu le mémoire, enregistré le 15 février 2012, présenté pour la société par actions simplifiée Sepur et tendant à ce que le Conseil d'Etat annule l'ordonnance du 7 juillet 2011 par laquelle le président de la troisième chambre de la cour administrative d'appel de Versailles a rejeté sa demande tendant à ce que soit transmise au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles 21 de la loi du 30 décembre 2002 et 37 de la loi du 30 décembre 2003 ; elle soutient que c'est à tort que l'auteur de cette ordonnance a estimé que cette question était dépourvue de caractère sérieux ;<br/>
<br/>
              III. Vu le mémoire, enregistré le 14 mai 2012, présenté pour la société par actions simplifiée Sepur en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; elle demande, à l'appui du pourvoi visé ci-dessus, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles 21 de la loi n° 2002-1576 du 30 décembre 2002 et 37 de la loi n° 2003-1312 du 30 décembre 2003  ; elle soutient que ces dispositions sont applicables au litige et n'ont pas été déclarées conformes à la Constitution ; qu'elles soulèvent une question sérieuse dès lors qu'en confiant aux comptables de la direction générale des impôts le soin de recouvrer l'impôt sur les sociétés et les contributions soumises au même régime aux lieu et place des comptables du Trésor, chargés du recouvrement des impôts directs sans préciser, à la différence des mentions apportées pour l'imposition forfaitaire annuelle et la taxe sur les salaires, que ces impositions seraient de ce fait recouvrées non plus par voie de rôle mais par avis de mise en recouvrement, le législateur a adopté des dispositions qui, faute de mettre le contribuable à même de disposer d'une connaissance suffisante des normes qui lui sont applicables en ce qui concerne en particulier les modalités de recouvrement, de contestation et de poursuites relatives à cet impôt, méconnaissent l'article 34 de la Constitution et portent atteinte à l'exercice, garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, par les contribuables de leurs droits ;<br/>
<br/>
              Vu l'arrêt et l'ordonnance attaqués ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 21 juin 2012, présenté par le ministre délégué, chargé du budget ; il soutient que la question prioritaire de constitutionnalité n'est pas nouvelle et ne présente pas un caractère sérieux ; que la société ne peut se fonder sur les dispositions de l'article 14 de la Déclaration de 1789 pour invoquer l'inconstitutionnalité des dispositions législatives contestées ; que les articles 21 de la loi du 30 décembre 2002 et 37 de la loi du 30 décembre 2003 n'instaurent aucune incertitude quant à la garantie des droits du contribuable prévue par l'article 16 de cette déclaration, dès lors que le transfert de compétence à la direction générale des impôts du recouvrement de l'impôt sur les sociétés implique nécessairement le recouvrement de cet impôt par avis de mise en recouvrement ;	<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la société par actions simplifiée Sepur, <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la société par actions simplifiée Sepur ;<br/>
<br/>
<br/>
<br/>
              Considérant que la société par actions simplifiée Sepur se pourvoit en cassation contre l'arrêt du 29 novembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du 16 février 2010 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles auxquelles elle a été assujettie au titre des années 1996 à 2001 ainsi que des pénalités correspondantes ; qu'elle conteste, à l'occasion de ce pourvoi, l'ordonnance du 7 juillet 2011 par laquelle le président de la troisième chambre de la cour a refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 21 de la loi de finances rectificative pour 2002 du 30 décembre 2002 et de l'article 37 de la loi de finances rectificative pour 2003 du 30 décembre 2003 ; qu'en outre, par un mémoire intitulé " question prioritaire de constitutionnalité ", elle demande au Conseil d'Etat de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de ces mêmes dispositions ;<br/>
<br/>
              Sur le mémoire intitulé " question prioritaire de constitutionnalité " :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat ou de la Cour de cassation, le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est, à peine d'irrecevabilité, présenté dans un écrit distinct et motivé " ; qu'aux termes de l'article 23-2 de la même ordonnance : " (...) Le refus de transmettre la question ne peut être contesté qu'à l'occasion d'un recours contre la décision réglant tout ou partie du litige " ; qu'aux termes du premier alinéa de l'article R. 771-16 du code de justice administrative : " Lorsque l'une des parties entend contester devant le Conseil d'Etat, à l'appui d'un appel ou d'un pourvoi en cassation formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité précédemment opposé, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai de recours dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions que, lorsqu'une cour administrative d'appel a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus, à l'occasion du pourvoi en cassation formé contre l'arrêt qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte de l'arrêt, dont il joint alors une copie, ou directement par cet arrêt ; que les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité devant une juridiction statuant en dernier ressort de s'affranchir des conditions, définies par les dispositions citées plus haut de la loi organique et du code de justice administrative, selon lesquelles le refus de transmission peut être contesté devant le juge de cassation ;<br/>
<br/>
              Considérant que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles 21 de la loi du 30 décembre 2002 et 37 de la loi du 30 décembre 2003, la société soutient qu'en confiant par ces dispositions aux comptables de la direction générale des impôts le soin de recouvrer l'impôt sur les sociétés et les contributions soumises au même régime aux lieu et place des comptables du Trésor, chargés du recouvrement des impôts directs, sans préciser, à la différence des mentions apportées pour l'imposition forfaitaire annuelle et la taxe sur les salaires, que ces impositions seraient de ce fait recouvrées non plus par voie de rôle mais par avis de mise en recouvrement, le législateur a, faute de mettre les contribuables à même de disposer d'une connaissance suffisante des normes qui seront désormais applicables en ce qui concerne, en particulier, les modalités de recouvrement, de contestation et de poursuites relatives à ces impositions, méconnu l'article 34 de la Constitution ainsi que l'article 14 de la Déclaration des droits de l'homme et du citoyen  du 24 août 1789 et porté atteinte à l'exercice, garanti par l'article 16 de cette Déclaration, par les contribuables de leurs droits ; <br/>
<br/>
              Considérant que, devant la cour, la société avait fondé sa demande de transmission au Conseil d'Etat de la question prioritaire de constitutionnalité sur le moyen tiré de ce que ces articles législatifs méconnaissaient le principe constitutionnel d'annualité de l'impôt, issu du principe du consentement annuel à l'impôt prévu à l'article 14  de la Déclaration des droits de l'homme et du citoyen et à l'article 34 de la Constitution  ainsi que l'article 16 de cette Déclaration ; que, par suite et alors même que, devant le Conseil d'Etat, la société se prévaut, à l'appui de sa demande, de la méconnaissance de ces mêmes normes constitutionnelles, la question prioritaire de constitutionnalité ainsi posée ne porte pas, par les mêmes moyens, sur la même question que celle soumise à la cour ; <br/>
<br/>
               Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que celui-ci est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant, d'une part, que la société soutient que les articles 21 de la loi du 30 décembre 2002 et 37 de la loi du 30 décembre 2003 sont contraires à l'article 14 de la Déclaration des droits de l'homme et du citoyen en ce que le législateur n'a pas précisé que les articles 1658, 1659 et 1663-1 du code général des impôts cessaient d'être applicables au recouvrement de l'impôt sur les sociétés ainsi que des contributions soumises au même régime et a ainsi méconnu l'étendue de sa compétence ; que, toutefois, les dispositions de l'article 14 de la Déclaration de 1789 sont mises en oeuvre par l'article 34 de la Constitution et n'instituent pas un droit ou une liberté qui puisse être invoqué, à l'occasion d'une instance devant une juridiction, à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution ;<br/>
<br/>
              Considérant, d'autre part, que les modalités de recouvrement, de contestation et d'engagement des poursuites par l'administration, relatives à l'impôt sur les sociétés et aux contributions soumises au même régime, faisant l'objet des réformes introduites par ces articles, sont suffisamment précisées par les articles législatifs relatifs aux procédures applicables aux impôts recouvrés par les comptables de la direction générale des impôts, notamment par les articles 1668 du code général des impôts et L. 190, L. 256 et suivants du livre des procédures fiscales ; que, dès lors, en ne précisant pas à nouveau ces règles à l'occasion des modifications qu'il a introduites par les articles dont la conformité à la Constitution est contestée,  le législateur n'a pas méconnu l'étendue de sa compétence et n'a pas porté atteinte au principe de la garantie des droits, issu de l'article 16 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              Considérant que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions législatives contestées méconnaissent les droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur le pourvoi en cassation :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              Considérant que, pour demander l'annulation de l'ordonnance attaquée, la société par actions simplifiée Sepur soutient que le président de la troisième chambre de la cour administrative d'appel de Versailles a, à tort, rejeté sa demande de transmission au Conseil d'Etat de la question de la conformité aux droits et libertés garantis par la Constitution des articles 21 de la loi du 30 décembre 2002 et 37 de la loi du 30 décembre 2003 au motif que cette question était dépourvue de caractère sérieux ;  que, pour demander  l'annulation de l'arrêt attaqué, elle soutient que la cour administrative d'appel de Versailles a commis une erreur de droit au regard des articles L. 13 et L. 47 du livre des procédures fiscales, une erreur de qualification juridique des faits et a entaché son arrêt d'inexactitude matérielle en jugeant que l'administration avait effectué un contrôle sur pièces pour les exercices correspondant aux années 1996 à 1998 alors qu'elle avait consulté des pièces comptables ; qu'elle a commis une erreur de droit ainsi qu'une erreur de qualification juridique des faits et a entaché son arrêt d'inexactitude matérielle en jugeant que la vérification de comptabilité portant sur les exercices correspondant aux années 1999 à 2001 était régulière alors que l'administration avait fait usage pendant le contrôle de son droit de communication auprès du juge d'instruction  et avait obtenu un état récapitulatif des extraits des comptabilités des sociétés dirigées par M. Matusewski, son dirigeant ; qu'en jugeant que les avis de mise en recouvrement du 7 septembre 2005 étaient réguliers alors qu'ils ne comportaient pas de référence au rôle, à la date de mise en recouvrement et à la date d'exigibilité des cotisations, la cour administrative d'appel s'est méprise sur le sens et la portée des articles 21 de la loi de finances rectificative pour 2002 du 30 décembre 2002 et 37 de la loi de finances rectificative pour 2003 du 30 décembre 2003 et a violé les articles 1658 et 1659 ainsi que le 1 de l'article  1663 du code général des impôts, qui n'ont pas été modifiés par ces lois  ; qu'elle a violé les articles 38 et 39 du même code en jugeant que les redressements effectués au titre de frais d'annonces et d'insertions étaient justifiés alors que l'administration n'avait pas établi que ces dépenses n'avaient pas été engagées, au moins en partie, pour ses besoins ; qu'en se fondant, pour refuser la déduction des sommes correspondantes, sur l'affectation par les associations des dons qu'elle leur a consentis, elle a violé les dispositions de l'article 238 bis de ce code ; que la cour a violé les dispositions des articles 1729 de ce code et L. 195-A du livre des procédures fiscales en laissant à sa charge les pénalités pour manoeuvres frauduleuses sans établir qu'elle avait participé à un montage destiné à égarer l'administration dans son pouvoir de contrôle, et alors qu'elle n'a tiré aucun profit des opérations pour lesquelles son dirigeant a fait l'objet de poursuites pour abus de biens sociaux ; <br/>
<br/>
              Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission des pourvois ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
Article 1er  : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 21 de la loi n° 2002-1576 du 30 décembre 2002 et de l'article 37 de la loi n° 2003-1312 du 30 décembre 2003.<br/>
<br/>
Article 2 : Le pourvoi de la société par actions simplifiée Sepur n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Sepur et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-02 PROCÉDURE. - QUESTION POSÉE DEVANT UNE JURIDICTION QUI A REFUSÉ SA TRANSMISSION AU CONSEIL D'ETAT - QUESTION SOULEVÉE À NOUVEAU DEVANT LE JUGE DE CASSATION - RECEVABILITÉ, ALORS MÊME QUE LES NORMES CONSTITUTIONNELLES INVOQUÉES SONT LES MÊMES, DÈS LORS QUE SONT SOULEVÉS DES MOYENS NOUVEAUX [RJ1].
</SCT>
<ANA ID="9A"> 54-10-02 Si les dispositions de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité (QPC) devant une juridiction statuant en dernier ressort de s'affranchir des conditions, notamment de délai, dans lesquelles le refus de transmission peut être contesté devant le juge de cassation, un requérant peut former à l'appui de son pourvoi devant le Conseil d'Etat, alors même que le délai de recours est expiré, une QPC portant sur les mêmes dispositions et reposant sur l'invocation des mêmes normes constitutionnelles que la question soumise aux juges du fond, mais soulevant des moyens différents à leur égard.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er février 2011, SARL Prototype Technique Industrie (Prototech), n° 342536, à publier au Recueil ; CE, 1er février 2012, Région Centre, n° 351795, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
