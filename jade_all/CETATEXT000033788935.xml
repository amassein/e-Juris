<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788935</ID>
<ANCIEN_ID>JG_L_2016_12_000000386352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788935.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 28/12/2016, 386352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. François Monteagle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386352.20161228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Strasbourg, en premier lieu, d'annuler les décisions des 26 juin 2012 et 14 août 2012 par lesquelles le maire de Neuf-Brisach (Haut-Rhin) a mis fin à son engagement en tant que directeur de l'Harmonie municipale, ainsi que la décision implicite de rejet de son recours gracieux formé contre ces deux décisions, en deuxième lieu, d'enjoindre à ce maire, sous astreinte de 50 euros par jour de retard, de procéder à sa réintégration dans ses fonctions,  et enfin de condamner la commune de Neuf-Brisach à l'indemniser des préjudices qui lui ont été causés par les décisions litigieuses.<br/>
<br/>
              Par un jugement n° 1300465 du 19 novembre 2013, le tribunal administratif de Strasbourg a annulé les décisions litigieuses et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n° 14NC00109 du 5 décembre 2014, la présidente de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat le pourvoi formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 28 mai 2014 au greffe de la cour administrative d'appel de Nancy, et par des mémoires complémentaires enregistrés les 16 mars et 17 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) de renvoyer le litige devant la cour administrative d'appel de Nancy ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler ce jugement en tant qu'il a, par son article 3, rejeté ses conclusions indemnitaires ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Neuf-Brisach la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
		Vu les autres pièces du dossier ;<br/>
		Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Monteagle, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. A...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Neuf-Brisach ;<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., assistant d'enseignement artistique employé par la communauté de communes du pays de Brisach, a été recruté le 6 novembre 2006 par la commune de Neuf-Brisach dans le cadre d'un cumul d'activités autorisé par son employeur, pour exercer les fonctions de directeur de l'Harmonie municipale de Neuf-Brisach, qui a le caractère d'une personne morale de droit privé. Par une lettre du 26 juin 2012, le maire de Neuf-Brisach lui a indiqué que l'abrogation des délibérations du conseil municipal des 30 août 2005, 28 mars 2006 et 29 août 2006 fixant les modalités d'exercice de son activité accessoire et de l'arrêté en date du 6 novembre 2006 conduirait à mettre un terme à son activité à la tête de l'Harmonie municipale le 31 août 2012. Par décision du 14 août 2012, le maire de Neuf-Brisach a prononcé le licenciement de M. A... de ses fonctions. Par un jugement du 19 novembre 2013, le tribunal administratif de Strasbourg a annulé les décisions des 26 juin et 14 août 2012 mais a rejeté les conclusions de M. A... tendant à ce qu'il soit enjoint sous astreinte à la commune de procéder à sa réintégration et à ce que celle-ci l'indemnise des préjudices subis. M. A... se pourvoit en cassation contre ce jugement en tant seulement qu'il a, par son article 3, rejeté ses conclusions indemnitaires. La commune de Neuf-Brisach forme quant à elle contre ce même jugement un pourvoi incident, par lequel elle demande l'annulation de ses articles 1er et 2 annulant les décisions litigieuses.<br/>
<br/>
              Sur le pourvoi incident de la commune de Neuf-Brisach :<br/>
<br/>
              2. Les conclusions incidentes, enregistrées après l'expiration du délai de recours en cassation, par lesquelles la commune de Neuf-Brisach demande l'annulation des articles 1er et 2 du jugement du tribunal administratif de Strasbourg du 19 novembre 2013, annulant pour excès de pouvoir les décisions du maire de cette commune des 26 juin et 14 août 2012, soulèvent un litige distinct de celui qui a fait l'objet du pourvoi principal de M. A..., dirigé contre l'article 3 de ce même jugement rejetant ses conclusions indemnitaires. Ce pourvoi incident est, par suite, irrecevable.<br/>
<br/>
              Sur le pourvoi de M. A... :<br/>
<br/>
              3. En premier lieu, M. A... fait valoir, à l'appui de sa demande, que si, dans son volet indemnitaire, le jugement du tribunal administratif a été rendu en dernier ressort puisque ses demandes chiffrées n'excédaient pas la somme de 10 000 euros, le litige indemnitaire était cependant connexe à un litige d'excès de pouvoir relevant du juge d'appel dès lors qu'il avait reproché à ce tribunal administratif de n'avoir pas fait droit à ses conclusions à fins d'injonction de réintégration et que ces conclusions d'injonction étaient elles-mêmes susceptibles d'appel puisque se rattachant à des décisions administratives mettant fin à la carrière d'un agent public. Il soutient que l'ensemble du litige relève de la voie de l'appel, en application des dispositions de l'article R. 811-1 du code de justice administrative et demande le renvoi de l'affaire devant la cour administrative d'appel de Nancy.<br/>
<br/>
              4. Il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles de l'article R. 222-13 du même code, dans leur rédaction applicable à la date du jugement attaqué, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service et sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code.<br/>
<br/>
              5. Les conclusions de la demande par laquelle M. A..., qui avait conservé la qualité d'agent titulaire de communauté de communes du pays de Brisach, a sollicité du tribunal administratif de Strasbourg l'annulation des décisions des 26 juin et 14 août 2012 le licenciant des fonctions qu'il occupait, à titre accessoire et dans le cadre d'un cumul d'activités, au sein de l'Harmonie municipale de Neuf-Brisach, soulevaient un litige étranger à l'entrée au service, à la discipline ou à la sortie du service. Dès lors, ce litige est au nombre de ceux sur lesquels le tribunal administratif a statué en premier et dernier ressort. Par suite la demande de M. A... tendant au renvoi de l'affaire à la cour administrative d'appel de Nancy, au motif que son différend indemnitaire serait connexe à un litige relevant de la compétence du juge d'appel, ne peut qu'être rejetée.<br/>
<br/>
              6. En second lieu, en jugeant, par un jugement suffisamment motivé, que les vices de légalité externe qui l'avaient conduit à annuler les décisions des 26 juin et 14 août 2012 du maire de Neuf-Brisach ne pouvaient être considérés comme la cause des préjudices subis par M. A..., dès lors que c'est l'irrégularité de son recrutement qui avait justifié la mesure de licenciement mise en oeuvre par ces décisions, le tribunal administratif de Strasbourg n'a commis aucune erreur de droit. Par ailleurs, si M. A... a demandé la condamnation de la commune de Neuf-Brisach à réparer les préjudices qu'il estime avoir subi du fait de l'illégalité de son licenciement, ses conclusions indemnitaires n'étaient pas fondées sur l'existence de préjudices causés par l'illégalité de la décision le recrutant pour être mis à la disposition de l'Harmonie municipale. Par suite, le tribunal administratif n'a entaché son jugement ni d'erreur de droit ni d'insuffisance de motivation en ne recherchant pas si d'autres fautes que celles invoquées devant lui étaient susceptibles de justifier les demandes indemnitaires de M. A....<br/>
<br/>
              7. Il résulte de tout de ce qui précède que M. A... n'est pas fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Neuf-Brisach la somme que demande M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. En tout état de cause, il n'y a pas davantage lieu de mettre à la charge de M. A... la somme que demande au même titre la commune de Neuf-Brisach.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... et le pourvoi incident de la commune de Neuf-Brisach sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à M.B... A... et à la commune de Neuf-Brisach.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
