<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027206125</ID>
<ANCIEN_ID>JG_L_2013_02_000000366193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/20/61/CETATEXT000027206125.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/02/2013, 366193, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:366193.20130228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la Société Orthofix, dont le siège est 21/31 rue de Stalingrad, 24/28 Villa Baudran à Arcueil (94110), représentée par son directeur général délégué ; la société demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution, d'une part, de la décision implicite par laquelle a été rejetée sa demande d'abrogation de l'arrêté du 6 juillet 2012 portant radiation du dispositif médical " Physio-Stim " de la liste des produits et prestations remboursables prévue par l'article L. 165-1 du code de la sécurité sociale, d'autre part, de la décision du 18 décembre 2012 refusant de réinscrire le dispositif sur cette liste ;<br/>
<br/>
              2°) d'enjoindre à la ministre des affaires sociales et de la santé, d'une part, d'abroger l'arrêté du 6 juillet 2012 et, d'autre part, de prendre un nouvel arrêté portant inscription temporaire du dispositif médical " Physio-Stim " sur la liste précitée, et ce, dans un délai de 10 jours suivant la notification de l'ordonnance à intervenir sous astreinte du 1 500 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens de l'instance, y compris le coût du timbre fiscal en application de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que les décisions contestées portent un préjudice grave et immédiat tant à sa situation financière qu'à l'intérêt des patients ;<br/>
              - il existe un doute quant à la légalité des décisions contestées dès lors qu'elles sont intervenues sur le fondement d'un avis irrégulier de la Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé (CNEDIMTS) ; <br/>
<br/>
<br/>
              Vu la décision du 18 décembre 2012 ;<br/>
<br/>
              Vu la demande, adressée le 18 février 2013, tendant à l'abrogation de l'arrêté du 6 juillet 2012 ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation des décisions dont la suspension est demandée ; <br/>
              Vu les autres pièces du dossier ; <br/>
              Vu le code de la sécurité sociale ;<br/>
              Vu le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l' instruction, un doute sérieux quant à la légalité de la décision. " ; qu'en application de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'en vertu du premier alinéa de l'article L. 165-1 du code de la sécurité sociale, le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute autorité de santé ; que les articles R. 165-1 et suivants du même code prévoient que cette liste est établie par arrêté du ministre chargé de la sécurité sociale et du ministre chargé de la santé, prohibent notamment l'inscription sur la liste des produits qui n'apportent ni amélioration du service qui en est attendu ou du service qu'ils rendent, ni économie dans le coût du traitement et disposent que l'inscription, d'une durée maximale de cinq ans, ne peut être renouvelée, après avis de la Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé (CNEDIMTS), que si le produit apporte un service rendu suffisant pour justifier le maintien de son remboursement ;<br/>
<br/>
              3. Considérant qu'à la suite de la demande de renouvellement de l'inscription sur la liste mentionnée à l'article L. 165-1 du produit dénommé " Physio-Stim ", commercialisé par la société Orthofix, et de l'avis de la CNEDIMTS rendu le 29 novembre 2011 jugeant insuffisant pour un renouvellement d'inscription le service rendu par ce dispositif, a été pris, le 6 juillet 2012, un arrêté interministériel le radiant de cette liste ; que la demande tendant à une nouvelle inscription de ce produit sur la liste, déposée en août 2012, a fait l'objet, le 18 décembre 2012, d'une décision de rejet, après que la CNEDIMTS a estimé, dans un avis rendu le 20 novembre 2012, qu'au vu des données disponibles, le service attendu de ce produit était insuffisant ; <br/>
<br/>
              4. Considérant, en premier lieu, que la Société Orthofix a sollicité, le 18 février 2013, l'abrogation de l'arrêté interministériel du 6 juillet 2012 ; qu'en l'absence de décision expresse et faute que soit écoulé le délai de deux mois nécessaire à la naissance d'une décision implicite de rejet, il n'a été justifié, ni à la date de l'introduction de la requête aux fins de suspension ni, à ce jour, d'aucune décision administrative dont la suspension serait susceptible d'être ordonnée par le juge des référés du Conseil d'Etat sur le fondement de l'article L. 521-1 du code de justice administrative ; que les conclusions tendant à la suspension de l'exécution d'une décision rejetant la demande d'abrogation de cet arrêté interministériel sont donc manifestement irrecevables ;<br/>
<br/>
              5. Considérant, en second lieu, que pour demander la suspension de la décision du 18 décembre 2012, la Société Orthofix présente un unique moyen, tiré de l'irrégularité de l'avis de la CNEDIMTS, laquelle, selon la requête, ne pouvait, pour estimer insuffisant le service attendu du " Physio-Stim ", se fonder ni sur les problèmes méthodologiques rendant incertains les résultats des études produites à l'appui de la demande, ni sur l'absence de données cliniques comparatives ; qu'un tel moyen n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de cette décision ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la requête de la Société Orthofix doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris les conclusions à fin d'injonction et d'astreinte et celles présentées sur le fondement des articles L. 761-1 et R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Société Orthofix est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la Société Orthofix.<br/>
Copie de la présente ordonnance sera transmise pour information à la ministre des affaires sociales et de la santé et au ministre délégué auprès du ministre de l'économie et des finances, chargé du budget.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
