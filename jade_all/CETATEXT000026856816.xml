<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856816</ID>
<ANCIEN_ID>JG_L_2012_12_000000346779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/68/CETATEXT000026856816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 28/12/2012, 346779, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346779.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 0912216 du 16 février 2011, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société Mac GmbH ;<br/>
<br/>
              Vu la requête, enregistrée le 22 juillet 2009 au greffe du tribunal administratif de Paris, présentée par la société Mac GmbH, dont le siège est Alte Landstrasse 15 à Sigmarszell (88138), Allemagne, représentée par son président directeur général en exercice ; la société Mac GmbH demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 22 mai 2009 par laquelle le ministre de l'agriculture et de la pêche a retiré l'autorisation de mise sur le marché à titre d'importation parallèle du produit dénommé " Star 100 " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ;<br/>
<br/>
              Vu le décret n° 2008-636 du 30 juin 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier qu'à la suite de contrôles opérés par la brigade nationale d'enquêtes vétérinaires et phytosanitaires auprès d'entreprises distribuant des produits phytopharmaceutiques et d'analyses de la composition du produit phytopharmaceutique dénommé " Star 100 ", le ministre chargé de l'agriculture a procédé, par décision du 22 mai 2009, au retrait de l'autorisation de mise sur le marché à titre d'importation parallèle de ce produit dont la société Endres-Merath était titulaire ; que la société Mac GmbH, qui a racheté la société Endres-Merath,  demande l'annulation de cette décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 253-1 du code rural, dans sa rédaction en vigueur à la date de la décision attaquée : " I. - Sont interdites la mise sur le marché, l'utilisation et la détention par l'utilisateur final des produits phytopharmaceutiques s'ils ne bénéficient pas d'une autorisation de mise sur le marché (...). / II. - Au sens du présent chapitre, on entend par : / 1° Produits phytopharmaceutiques : les préparations contenant une ou plusieurs substances actives (...) destinés à : / a) Protéger les végétaux (...) contre tous les organismes nuisibles ou à prévenir leur action ; / b) Exercer une action sur les processus vitaux des végétaux, dans la mesure où il ne s'agit pas de substances nutritives ; / c) Assurer la conservation des produits végétaux, à l'exception des substances et produits faisant l'objet d'une réglementation communautaire particulière relative aux agents conservateurs ; / d) Détruire les végétaux indésirables ; / e) Détruire des parties de végétaux, freiner ou prévenir une croissance indésirable des végétaux ; / 2° Mise sur le marché : toute remise à titre onéreux ou gratuit autre qu'une remise pour stockage et expédition consécutive en dehors du territoire de la Communauté européenne. L'importation d'un produit phytopharmaceutique constitue une mise sur le marché. / (...) " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article R. 253-52 du même code, dans sa version en vigueur à la date de la décision attaquée : " L'introduction sur le territoire national d'un produit phytopharmaceutique en provenance d'un État partie à l'accord sur l'Espace économique européen dans lequel il bénéficie déjà d'une autorisation de mise sur le marché délivrée conformément à la directive 91/414/CEE du Conseil du 15 juillet 1991, et identique à un produit dénommé ci-après "produit de référence", est autorisée dans les conditions suivantes : / Le produit de référence doit bénéficier d'une autorisation de mise sur le marché délivrée par le ministre chargé de l'agriculture en application de la sous-section 1 de la section 3. / L'identité du produit introduit sur le territoire national avec le produit de référence est appréciée au regard des trois critères suivants : / 1° Origine commune des deux produits en ce sens qu'ils ont été fabriqués, suivant la même formule, par la même société ou par des entreprises liées ou travaillant sous licence ; / 2° Fabrication en utilisant la ou les mêmes substances actives ; / 3° Effets similaires des deux produits compte tenu des différences qui peuvent exister au niveau des conditions agricoles, phytosanitaires et environnementales, notamment climatiques, liées à l'utilisation des produits " ; qu'aux termes de l'article R. 253-53 du même code : " L'introduction sur le territoire national d'un produit phytopharmaceutique provenant d'un État partie à l'accord sur l'Espace économique européen doit faire l'objet d'une demande d'autorisation de mise sur le marché. / Un arrêté du ministre chargé de l'agriculture, pris après avis des ministres chargés de l'industrie, de la consommation, de l'environnement et de la santé, fixe la liste des informations à fournir à l'appui de la demande, notamment celles relatives au demandeur de l'autorisation et au produit objet de la demande. / En outre, pour établir l'identité entre le produit introduit sur le territoire national et le produit de référence, le ministre chargé de l'agriculture peut : / 1° Utiliser les informations contenues dans le dossier du produit de référence ; / 2° Demander au détenteur de l'autorisation du produit de référence de lui fournir les renseignements dont il dispose ; / 3° Demander des renseignements aux autorités de l'État qui a autorisé le produit faisant l'objet de l'introduction sur le territoire national ainsi que le prévoient les dispositions de l'article 9, paragraphe 5, de la directive 91/414/CEE du Conseil du 15 juillet 1991  (...) " ; qu'aux termes de l'article R. 253-55 du même code : " L'autorisation de mise sur le marché du produit introduit sur le territoire national peut être (...) retirée : / 1° Pour des motifs tirés de la protection de la santé humaine et animale ainsi que de l'environnement ; / 2° Pour défaut d'identité, au sens de l'article R. 253-52 avec le produit de référence ; / (...) / Préalablement à (...) un retrait d'autorisation de mise sur le marché, le (...) titulaire de l'autorisation est mis en mesure de présenter ses observations au ministre chargé de l'agriculture. " ; qu'enfin, aux termes de l'article R. 253-46 du code : " L'autorisation de mise sur le marché peut être retirée ou modifiée par le ministre chargé de l'agriculture, le cas échéant après avis de l'Agence française de sécurité sanitaire des aliments. / L'autorisation de mise sur le marché d'un produit phytopharmaceutique est retirée : / 1° Si les conditions requises pour son obtention ne sont plus remplies ; / (...). Lorsqu'un produit phytopharmaceutique est l'objet d'un retrait d'autorisation, toute mise sur le marché doit cesser. Toutefois, le ministre chargé de l'agriculture peut accorder un délai pour supprimer, écouler, utiliser les stocks existants dont la durée est en rapport avec la cause du retrait. " ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, entré en vigueur le 1er octobre 2005 : " A compter du jour suivant la publication au Journal officiel de la République Française de l'acte les nommant dans leurs fonctions (...), peuvent signer au nom du ministre (...), et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : 1° (...) les directeurs d'administration centrale (...)  " ; qu'aux termes de l'article 4 du décret du 30 juin 2008 fixant l'organisation de l'administration centrale du ministère chargé de l'agriculture : " La direction générale de l'alimentation exerce les compétences du ministère relatives à (...) la santé des plantes et des animaux (...). / (...) / (...) elle est chargée de la règlementation, de l'homologation et du contrôle des produits phytopharmaceutiques (...) " ; qu'il ressort des pièces du dossier que, par une décision du 2 mars 2009, publiée au Journal officiel le 5 mars 2009, M. Jean-Marc Bournigal, nommé directeur général de l'alimentation au ministère de l'agriculture et de la pêche par un décret du 3 mars 2006, publié au Journal officiel le 4 mars 2006, a donné délégation de signature à Mme Emmanuelle Soubeyran, inspectrice en chef de la santé publique vétérinaire, à l'effet de signer, au nom du ministre chargé de l'agriculture, à l'exclusion des décrets, tous actes, arrêtés et décisions, dans la limite des attributions de la sous-direction de la qualité et de la protection des végétaux ; que, dès lors, le moyen tiré de l'incompétence de Mme Soubeyran, signataire de la décision attaquée, doit être écarté ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier qu'à la date de la décision attaquée, la société Mac GmbH n'était pas titulaire de l'autorisation de mise sur le marché du produit dénommé " Star 100 " ; que, dès lors,  ni la décision de retrait de cette autorisation ni les analyses sur lesquelles elle se fondait n'avaient à lui être notifiées ; qu'elle n'avait pas davantage à être mise en mesure de présenter des observations préalablement à l'adoption de la décision attaquée ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 (...) n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. (...) / Les dispositions de l'alinéa précédent ne sont pas applicables : / 1° En cas d'urgence ou de circonstances exceptionnelles ; / (...) / 3° Aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière. / (...) " ; qu'il résulte de ces dispositions qu'en l'absence de dispositions législatives instaurant une procédure contradictoire particulière, la décision par laquelle le ministre chargé de l'agriculture retire l'autorisation de mise sur le marché d'un produit phytopharmaceutique, qui est au nombre des décisions devant être motivées en application de la loi du 11 juillet 1979, ne peut intervenir qu'après que son destinataire a été mis à même de présenter ses observations, sauf en cas d'urgence ou de circonstances exceptionnelles ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que, les analyses effectuées sur le produit dénommé " Star 100 " ayant révélé que la concentration de ce produit en naphtalène était deux mille six cents fois supérieure à celle du produit dénommé " Astor ", qui constituait son produit de référence au sens de l'article R. 253-52 du code rural, et qu'elle atteignait un niveau de 11,5%, le ministre a décidé, dès réception de ces analyses, de retirer l'autorisation de mise sur le marché de ce produit ; qu'eu égard au caractère cancérogène suspecté du naphtalène lorsque sa concentration excède 1%, la situation d'urgence doit être regardée comme constituée ; que, par suite, le ministre a pu, sans entacher sa décision d'irrégularité, s'abstenir de mettre en mesure la société titulaire de l'autorisation de mise sur le marché de présenter ses observations préalablement à l'adoption de la décision attaquée, et, en tout état de cause, s'abstenir de lui transmettre les analyses effectuées par le laboratoire Toxlab ;<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              8. Considérant qu'il ressort des termes de la décision attaquée que, pour adopter cette décision, le ministre s'est fondé sur trois motifs ; que, par application des dispositions précitées des 1° et 2° de l'article R. 253-55 du code rural, il s'est fondé, d'une part, sur la protection de la santé humaine, d'autre part, sur le défaut d'identité, au sens de l'article R. 253-52 du même code, du produit autorisé avec le produit de référence ; qu'il s'est également fondé sur la circonstance que le produit dénommé " Star 100 " ne bénéficiait plus d'une autorisation de mise sur le marché dans le pays d'exportation, alors que, en vertu des dispositions combinées des articles R. 253-46 et R. 253-52 du code rural, l'existence d'une telle autorisation est une condition requise pour la délivrance d'une autorisation de mise sur le marché à titre d'importation parallèle en France ;<br/>
<br/>
              9. Considérant qu'il résulte des dispositions précitées du 1° de l'article R. 253-55 du code rural que le ministre chargé de l'agriculture peut retirer, à titre de mesure de précaution, une autorisation de mise sur le marché à titre d'importation parallèle s'il fait état d'indices sérieux permettant d'avoir un doute raisonnable sur l'innocuité du produit à l'égard de la santé publique et de l'environnement ou sur le bénéfice qu'il apporte ; <br/>
<br/>
              10. Considérant qu'en estimant que la concentration du produit en naphtalène, mesurée à 11,5 %, alors que cette substance a un effet cancérogène suspecté lorsque sa concentration excède 1 % constituait un indice sérieux permettant d'avoir un doute raisonnable sur l'innocuité du produit à l'égard de la santé publique et de l'environnement, le ministre n'a pas commis d'erreur manifeste d'appréciation ;<br/>
<br/>
              11. Considérant qu'il résulte de l'instruction que le ministre chargé de l'agriculture aurait pris la même décision s'il s'était fondé seulement sur ce motif ; que, dès lors, les moyens de la société requérante qui visent les autres motifs de la décision attaquée sont inopérants ;<br/>
<br/>
              12. Considérant, enfin, qu'eu égard au risque pour la santé publique, la mesure de retrait de l'autorisation de mise sur le marché sans délai pour supprimer, écouler, ou utiliser les stocks existants, n'est pas contraire au principe de proportionnalité ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la société Mac GmbH n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Mac GmbH est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Mac GmbH et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
