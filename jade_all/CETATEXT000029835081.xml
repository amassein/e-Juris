<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835081</ID>
<ANCIEN_ID>JG_L_2014_11_000000385679</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/50/CETATEXT000029835081.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/11/2014, 385679, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385679</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:385679.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 12 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'Etablissement d'hébergement pour personnes âgées dépendantes (EHPAD) " Maisons de retraite de Neuilly-sur-Seine ", dont le siège est situé résidence Roger Teullé, 20, rue des Graviers, à Neuilly-sur-Seine (92200) ; l'Etablissement demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1410389 du 28 octobre 2014 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a enjoint à son directeur de supprimer, à compter de la notification de l'ordonnance, l'affichage de la note intitulée " Note aux familles au sujet de la prestation de pédicure podologue " de tous les espaces où elle a pu être apposée dans chacune des deux résidences et d'afficher, en lieux et places, une note rectificative annulant explicitement la précédente et reprenant l'intégralité des motifs de l'ordonnance, sous astreinte de 50 euros par jour de retard, et mis à sa charge le versement d'une somme de 1 500 à Mme B...euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) de rejeter la demande de première instance de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...le versement d'une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que :<br/>
              - l'affichage de la note contestée ne porte pas une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
              - les informations figurant dans la note litigieuse sont exactes et l'affichage de cette note, qui n'est pas manifestement illégal, ne porte pas atteinte à la réputation de Mme B... ;<br/>
              - l'affichage de la note contestée ne place pas Mme B... dans une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
<br/>
                          Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 14 novembre 2014, présenté pour MmeB..., qui conclut au rejet de la requête et à ce que soit mis à la charge de l'EHPAD " Maisons de retraite de Neuilly-sur-Seine " la somme de 2 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors qu'à la suite de l'affichage de la note contestée,  elle a reçu plusieurs demandes de remboursement et a vu sa réputation entachée d'une suspicion de comportement délictueux à l'égard de personnes âgées ;<br/>
              - la note contestée porte une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
              - le droit à la protection de la réputation est une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'EHPAD Maisons de retraite de Neuilly-sur-Seine et, d'autre part, Mme B...;<br/>
<br/>
              Vu le procès verbal de l'audience publique du lundi 17 novembre à 11 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Garreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'EHPAD Maisons de retraite de Neuilly-sur-Seine ;<br/>
<br/>
              - les représentants de l'EHPAD Maisons de retraite de Neuilly-sur-Seine ;<br/>
<br/>
              - Me Thiriez, avocat au Conseil d'Etat et à la Cour de cassation, avocat de MmeB..., qui a soutenu que la note litigieuse portait atteinte à la présomption d'innocence ; <br/>
<br/>
              - Mme B...;<br/>
<br/>
              - la représentante de MmeB... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; que l'usage par le juge des référés des pouvoirs qu'il tient de ces dispositions est ainsi subordonné à la condition qu'une urgence particulière rende nécessaire l'intervention dans les quarante-huit heures d'une décision destinée à la sauvegarde d'une liberté fondamentale, qu'une atteinte grave ait été portée à cette liberté et que l'illégalité de cette atteinte soit manifeste ;  <br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que MmeB..., employée à temps partiel en qualité de pédicure au sein de l'Etablissement d'hébergement pour personnes âgées dépendantes  (EHPAD) " Maisons de retraite de Neuilly-sur-Seine ", établissement public, exerce également cette activité à titre libéral au profit de certains résidents ; que, prenant ses fonctions à la suite de graves dysfonctionnements au sein de l'établissement et estimant que, pour une partie au moins de son activité, Mme B...facturait aux résidents des prestations pour lesquelles elle était salariée, le directeur des maisons de retraite a fait afficher, le 7 octobre 2014, dans les locaux, une note afin d'informer les résidents et leurs familles de ce que, rémunérée par l'établissement, l'intéressée ne pouvait y facturer ses services à ce titre et qu'il leur était possible d'obtenir le remboursement de prestations indument payées ; que, par une ordonnance du 28 octobre 2014, le juge des référés du tribunal administratif de Cergy-Pontoise, saisi par Mme B...sur le fondement de l'article L. 521-2 du code de justice administrative, a enjoint au directeur de l'établissement de supprimer l'affichage de cette note, d'afficher en lieu et place une note reprenant les motifs de son ordonnance, le tout sous astreinte de 50 euros par jour de retard ; que l'EHPAD " Maisons de retraite de Neuilly-sur-Seine " relève appel de cette ordonnance ;<br/>
<br/>
              3. Considérant que si Mme B...soutient que le contenu de la note litigieuse et son affichage portent atteinte à sa réputation et à la présomption d'innocence et lui ont causé un préjudice matériel, en raison des demandes de remboursement dont elle a été saisie, il n'apparaît pas, en tout état de cause, eu égard en particulier aux divergences entre la direction de l'établissement et Mme B...tant sur ses obligations en qualité d'employée que sur son emploi du temps à ce titre, que ce préjudice procède d'une décision manifestement illégale ; qu'en outre, il n'apparaît pas non plus, dans les circonstances de l'espèce, que la publicité donnée à cette note au sein de l'établissement était de nature à créer une situation d'urgence caractérisée rendant nécessaire dans un délai de quarante-huit heures que le juge des référés intervienne et fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative, le tribunal administratif de Cergy-Pontoise n'ayant au demeurant été saisi que plus de deux semaines après cet affichage ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'EHPAD " Maisons de retraite de Neuilly-sur-Seine " est fondé à demander l'annulation de l'ordonnance attaquée et le rejet de la demande de Mme B...devant le juge des référés ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'EHPAD " Maisons de retraite de Neuilly-sur-Seine " le versement de la somme que demande MmeB... ; qu'il n'y a pas lieu de faire droit à la demande que présente au même titre l'EHPAD " Maisons de retraite de Neuilly-sur-Seine " ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise est annulée.<br/>
Article 2 : La demande présentée par Mme B...devant le juge des référés  du tribunal administratif de Cergy-Pontoise et ses conclusions présentées devant le juge des référés du Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Les conclusions de l'Etablissement d'hébergement pour personnes âgées dépendantes  " Maisons de retraite de Neuilly-sur-Seine " au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente ordonnance sera notifiée à l'Etablissement d'hébergement pour personnes âgées dépendantes  " Maisons de retraite de Neuilly-sur-Seine " et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
