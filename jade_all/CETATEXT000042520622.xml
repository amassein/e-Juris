<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520622</ID>
<ANCIEN_ID>JG_L_2020_11_000000428737</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520622.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/11/2020, 428737, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428737</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428737.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 2 décembre 2019, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de Mme B... dirigées contre l'arrêt du 8 janvier 2019 de la cour administrative d'appel de Marseille en tant que cet arrêt a statué sur le préjudice invoqué au titre du caractère abusif des renouvellements de ses contrats à durée déterminée et sur la réparation de son préjudice moral. <br/>
<br/>
              Par un mémoire en défense, enregistré le 15 juin 2020, la commune de Vitrolles conclut au rejet de ces conclusions du pourvoi et à ce que la somme de 3 000 euros soit mise à la charge de Mme B... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 1999/70/CE du Conseil du 28 juin 1999 ;<br/>
              - la loi n°83-634 du 13 juillet 1983 ; <br/>
              - la loi n°84-53 du 26 janvier 1984 ;<br/>
              - le décret n°88-145 du 15 février 1988 ;<br/>
              - la loi n°91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A... B... et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Vitrolles ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 octobre 2020, présentée par la commune de Vitrolles ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que du 29 septembre 2005 au 30 décembre 2013, Mme B... a fait l'objet de recrutements successifs par la commune de Vitrolles en tant qu'agent non titulaire à temps partiel. Après rejet par son employeur d'une demande d'indemnisation relative, d'une part, au préjudice financier de 55 066,20 euros qu'elle estimait lié à la faute commise par celui-ci en ayant eu recours à de multiples contrats à durées déterminée et non à un unique contrat à durée indéterminée et, d'autre part, au préjudice moral de 10 000 euros lié à la situation de précarité en résultant, elle a saisi le tribunal administratif de Marseille qui, par un jugement du 25 janvier 2017, a rejeté sa demande. Elle s'est pourvue en cassation contre l'arrêt du 8 janvier 2019 par lequel la cour administrative de Marseille a rejeté l'appel qu'elle a formé contre ce jugement. Par une décision du 2 décembre 2019, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions de son pourvoi en tant seulement que cet arrêt a statué sur le préjudice invoqué au titre du caractère abusif des renouvellements de ses contrats à durée déterminée et sur son préjudice moral.<br/>
<br/>
              Sur le préjudice relatif au renouvellement des contrats à durée déterminée :<br/>
<br/>
              2. Aux termes de l'article 1er de la directive 1999/70/CE du Conseil du 28 juin 1999 concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée : " La présente directive vise à mettre en oeuvre l'accord cadre sur le travail à durée déterminée, figurant en annexe, conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) ". Aux termes de l'article 2 de cette directive : " Les États membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive au plus tard le 10 juillet 2001 ou s'assurent, au plus tard à cette date, que les partenaires sociaux ont mis en place les dispositions nécessaires par voie d'accord, les États membres devant prendre toute disposition nécessaire leur permettant d'être à tout moment en mesure de garantir les résultats imposés par la présente directive. (...) ". En vertu des stipulations de la clause 5 de l'accord-cadre annexé à la directive, relative aux mesures visant à prévenir l'utilisation abusive des contrats à durée déterminée : " 1. Afin de prévenir les abus résultant de l'utilisation de contrats ou de relations de travail à durée déterminée successifs, les États membres, après consultation des partenaires sociaux, conformément à la législation, aux conventions collectives et pratiques nationales, et/ou les partenaires sociaux, quand il n'existe pas des mesures légales équivalentes visant à prévenir les abus, introduisent d'une manière qui tienne compte des besoins de secteurs spécifiques et/ou de catégories de travailleurs, l'une ou plusieurs des mesures suivantes : a) des raisons objectives justifiant le renouvellement de tels contrats ou relations de travail ; b) la durée maximale totale de contrats ou relations de travail à durée déterminée successifs ; c) le nombre de renouvellements de tels contrats ou relations de travail. 2. Les États membres, après consultation des partenaires sociaux et/ou les partenaires sociaux, lorsque c'est approprié, déterminent sous quelles conditions les contrats ou relations de travail à durée déterminée : a) sont considérés comme "successifs" ; b) sont réputés conclus pour une durée indéterminée ".<br/>
<br/>
              3. Il résulte des dispositions de cette directive, telles qu'elles ont été interprétées par la Cour de justice de l'Union européenne, qu'elles imposent aux États membres d'introduire de façon effective et contraignante dans leur ordre juridique interne, s'il ne le prévoit pas déjà, l'une au moins des mesures énoncées aux a) à c) du paragraphe 1 de la clause 5, afin d'éviter qu'un employeur ne recoure de façon abusive au renouvellement de contrats à durée déterminée. Lorsque l'État membre décide de prévenir les renouvellements abusifs en recourant uniquement aux raisons objectives prévues au a), ces raisons doivent tenir à des circonstances précises et concrètes de nature à justifier l'utilisation de contrats de travail à durée déterminée successifs. Il ressort également de l'interprétation de la directive retenue par la Cour de justice de l'Union européenne que le renouvellement de contrats à durée déterminée afin de pourvoir au remplacement temporaire d'agents indisponibles répond, en principe, à une raison objective au sens de la clause citée ci-dessus, y compris lorsque l'employeur est conduit à procéder à des remplacements temporaires de manière récurrente, voire permanente, alors même que les besoins en personnel de remplacement pourraient être couverts par le recrutement d'agents sous contrats à durée indéterminée. Dès lors que l'ordre juridique interne d'un État membre comporte, dans le secteur considéré, d'autres mesures effectives pour éviter et, le cas échéant, sanctionner l'utilisation abusive de contrats de travail à durée déterminée successifs au sens du point 1 de la clause 5 de l'accord, la directive ne fait pas obstacle à l'application d'une règle de droit national interdisant, pour certains agents publics, de transformer en un contrat de travail à durée indéterminée une succession de contrats de travail à durée déterminée qui, ayant eu pour objet de couvrir des besoins permanents et durables de l'employeur, doivent être regardés comme abusifs.<br/>
<br/>
              4. Il résulte des dispositions de l'article 3 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans leur rédaction antérieure à la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique - qui ont été reprises aux articles 3-1, 3-2 et 3-3 de la loi du 26 janvier 1984 dans leur rédaction issue de la loi du 12 mars 2012 -, que les collectivités territoriales de plus de 2 000 habitants ne peuvent recruter par contrat à durée déterminée des agents non titulaires que, d'une part, en vue d'assurer des remplacements momentanés ou d'effectuer des tâches à caractère temporaire ou saisonnier définies à ces alinéas et, d'autre part, dans le cadre des dérogations au principe selon lequel les emplois permanents sont occupés par des fonctionnaires, lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer certaines fonctions, ou lorsque, pour des emplois de catégorie A, la nature des fonctions ou les besoins des services le justifient. Dans ce dernier cas, les agents recrutés sont engagés par des contrats à durée déterminée, d'une durée maximale de trois ans. Ces contrats sont renouvelables, par reconduction expresse. La durée des contrats successifs ne peut excéder six ans. Si, à l'issue de la période maximale de six ans, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée.<br/>
<br/>
              5. Ces dispositions se réfèrent ainsi, s'agissant de la possibilité de recourir à des contrats à durée déterminée, à des " raisons objectives ", de la nature de celles auxquelles la directive renvoie. Elles ne font nullement obstacle à ce qu'en cas de renouvellement abusif de contrats à durée déterminée, l'agent concerné puisse se voir reconnaître un droit à l'indemnisation du préjudice éventuellement subi lors de l'interruption de la relation d'emploi, évalué en fonction des avantages financiers auxquels il aurait pu prétendre en cas de licenciement s'il avait été employé dans le cadre d'un contrat à durée indéterminée. Il incombe au juge, pour apprécier si le renouvellement des contrats présente un caractère abusif, de prendre en compte l'ensemble des circonstances de fait qui lui sont soumises, notamment la nature des fonctions exercées, le type d'organisme employeur ainsi que le nombre et la durée cumulée des contrats en cause. <br/>
<br/>
              6. Il ressort des énonciations non contestées de l'arrêt attaqué que si certains des arrêtés relatifs aux recrutements successifs de Mme B... en qualité de surveillante interclasse puis d'adjoint technique de 2ème classe se référaient aux dispositions de l'article 3 de la loi du 26 janvier 1984 autorisant le recrutement d'agents non titulaires pour exercer des fonctions correspondant à un besoin saisonnier ou faire face à un besoin occasionnel, l'intéressée devait être regardée comme ayant occupé un emploi permanent et qu'elle n'entrait dans aucune des dérogations au principe selon lequel un tel emploi doit, en principe, être occupé par un fonctionnaire. En outre, il ressort des pièces du dossier soumis aux juges du fond que Mme B... a été recrutée par la commune de Vitrolles, qui compte plus de 30 000 habitants, dans des fonctions similaires, par treize arrêtés et deux lettres, pour les années scolaires allant de septembre 2005 à juillet 2011 puis du 1er janvier au 9 juillet 2012, du 16 au 31 août 2012, du 1er septembre au 31 décembre 2012, du 1er janvier au 30 juin 2013 et du 19 août au 30 décembre 2013, c'est-à-dire pendant plus de huit ans à la seule exception du deuxième semestre 2011 où la commune lui a néanmoins versé des salaires de septembre à décembre et d'une partie des vacances scolaires. Dans ces conditions, Mme B... est fondée à soutenir que la cour administrative d'appel a commis une erreur de qualification juridique en retenant que le recours à une telle succession de contrats ne pouvait être regardé comme ayant revêtu un caractère abusif.<br/>
<br/>
              7. La personne qui a demandé en première instance la réparation des conséquences dommageables d'un fait qu'elle impute à une administration est recevable à détailler ces conséquences devant le juge d'appel, en invoquant le cas échéant des chefs de préjudice dont elle n'avait pas fait état devant les premiers juges, dès lors que ces chefs de préjudice se rattachent au même fait générateur et que ses prétentions demeurent dans la limite du montant total de l'indemnité chiffrée en première instance, augmentée le cas échéant des éléments nouveaux apparus postérieurement au jugement, sous réserve des règles qui gouvernent la recevabilité des demandes fondées sur une cause juridique nouvelle.<br/>
<br/>
              8. En demandant pour la première fois en appel la réparation d'un préjudice évalué à 4 387,52 euros résultant du caractère abusif des renouvellements de ses contrats, Mme B... n'a pas formulé de demande nouvelle qui soit fondée sur une cause juridique distincte de celle qui avait été invoquée en première instance mais s'est bornée à détailler les chefs de préjudice se rattachant au fait générateur constitué par la succession des contrats à durée déterminée par lesquels la commune de Vitrolles l'a recrutée. Par ailleurs, de telles conclusions ne nécessitaient pas d'être précédées du rejet par l'administration d'une demande préalable d'indemnisation portant spécifiquement sur ce point et elles ne pouvaient conduire, à elles seules, à obtenir une réparation supérieure au montant de l'indemnité initialement chiffré. Par suite, la commune de Vitrolles n'est pas fondée à demander de substituer au motif censuré au point 6 celui tiré du caractère irrecevable des conclusions en cause.<br/>
<br/>
              Sur le préjudice moral : <br/>
<br/>
              9. En second lieu, pour rejeter les conclusions de Mme B... tendant à la réparation du préjudice moral qui résultait selon elle du caractère précaire de sa situation et de la méconnaissance par la commune des dispositions de l'article 38-1 du décret du 15 février 1988 qui prévoient la notification au plus tard huit jours avant le terme de l'engagement du non-renouvellement de celui-ci, la cour s'est fondée sur la connaissance qu'avait Mme B... de ce que ses recrutements intervenaient chaque année pour la durée maximale d'une année scolaire. En statuant ainsi, alors que la connaissance de sa situation de précarité par Mme B..., qui résulte de l'illégalité de ses conditions de recrutement, n'est pas de nature à exclure l'existence d'un préjudice moral résultant de cette précarité, la cour a commis une erreur de droit.<br/>
<br/>
              10. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que Mme B... est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a statué sur le préjudice invoqué au titre du caractère abusif des renouvellements de ses contrats et sur son préjudice moral. <br/>
<br/>
              11. Mme B... ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Lyon-Caen, Thiriez, avocat de Mme B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la commune de Vitrolles la somme de 3 000 euros à verser à cette société. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Mme B..., qui n'est pas dans la présente instance la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative de Marseille du 8 janvier 2019 est annulé en tant qu'il a statué sur le préjudice invoqué par Mme B... au titre du caractère abusif des renouvellements de ses contrats à durée déterminée et sur son préjudice moral. <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille. <br/>
<br/>
Article 3 : La commune de Vitrolles versera à la SCP Lyon-Caen, Thiriez, avocat de Mme B..., une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Vitrolles devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A... B... et à la commune de Vitrolles. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
