<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038844585</ID>
<ANCIEN_ID>JG_L_2019_07_000000416270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/84/45/CETATEXT000038844585.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 24/07/2019, 416270, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP L. POULET, ODENT</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416270.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. L...F..., Mme AD...N..., M. AE...M..., M. G...AF..., M. O...K..., M. J...T..., M. AC...Q..., M. H...X..., Mme I...Y..., Mme C...A..., M. S...AB..., M. B...U..., M. H...V..., M. D...Z..., la société civile immobilière Morvan et le syndicat des copropriétaires de la résidence Fun ont demandé au tribunal administratif Poitiers d'annuler pour excès de pouvoir l'arrêté du 28 juillet 2015 par lequel le maire de La Rochelle a délivré à la société Médiatim Promotion le permis de construire une résidence comportant quarante logements, sur un terrain situé 13-17 impasse des Coureilles, ainsi que la décision du 4 novembre 2015 rejetant leur recours gracieux. Par un jugement n° 1600034 du 5 octobre 2017, le tribunal administratif de Poitiers a, à l'article 1er, annulé ces décisions en tant qu'elles autorisent la création de neuf logements sociaux au lieu de dix et, à l'article 2, rejeté le surplus de la demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 décembre 2017 et 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M.F..., MmeY..., M.U..., M.AB..., la SCI Morvan, M. M..., M.Z..., M. AF...et M. T...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ; <br/>
<br/>
              3°) de mettre à la charge de la société Médiatim Promotion et de la commune de la Rochelle la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M.T..., de MmeY..., de M.U..., de M.AB..., de la SCI Morvan, de M.M..., de M.Z..., de M. AF...et de M. F..., à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de La Rochelle, et à la SCP L. Poulet, Odent, avocat de la société Mediatim Promotion ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 28 juillet 2015, le maire de La Rochelle a délivré à la société Médiatim Promotion le permis de construire une résidence de quarante logements sur un terrain situé 13-17 impasse de Coureilles. Les requérants se pourvoient en cassation contre l'article 2 du jugement du 5 octobre 2017 par lequel le tribunal administratif de Poitiers a, après avoir à l'article 1er annulé partiellement ce permis de construire et le rejet de leur recours gracieux au motif que la résidence devait compter non pas neuf mais dix logements sociaux, rejeté le surplus de leur demande d'annulation de ces décisions.<br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              2. M.E..., M.R..., M.AG..., M.W..., Mme P...et Mme AA...justifient d'un intérêt suffisant à l'annulation du jugement attaqué. Leur intervention est, par suite, recevable.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              3. Il ressort des pièces du dossier transmis par le tribunal administratif de Poitiers que, contrairement à ce que soutiennent les requérants, la minute du jugement du 5 octobre 2017 comporte les signatures exigées par les dispositions de l'article R. 741-7 du code de justice administrative.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              4. En premier lieu, il résulte des dispositions de l'article UA 3 du règlement du plan local d'urbanisme de la commune de La Rochelle que l'accès au projet doit présenter " des caractéristiques permettant de satisfaire aux exigences de la sécurité, de la défense contre l'incendie, de la protection civile et du ramassage des déchets ménagers et répondre à l'importance ou à la destination de l'immeuble ou de l'ensemble d'immeubles envisagé ". <br/>
<br/>
              5. Le tribunal a relevé que l'accès à la construction projetée s'effectuait depuis une impasse large de 6,56 mètres, rectiligne, ne présentant aucun problème de visibilité et comportant une aire de retournement à son extrémité et qu'il avait une largeur suffisante pour permettre à un véhicule entrant et à un véhicule sortant de se croiser. En déduisant de ces éléments, non argués de dénaturation, que le projet ne méconnaissait pas les dispositions de l'article UA 3 du règlement du plan local d'urbanisme, le tribunal n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En deuxième lieu, l'article UA 9 du règlement du plan local d'urbanisme limite à 75 % l'emprise des constructions sur l'ensemble de la superficie de l'unité foncière. Le lexique annexé au règlement du plan local d'urbanisme précise que cette emprise correspond à la superficie de la projection verticale au sol des éléments bâtis au nu extérieur des murs et que sont exclus de son calcul les balcons et terrasses quelle que soit leur hauteur. C'est par une appréciation souveraine, exempte de dénaturation, que le tribunal a relevé que la dalle végétalisée qui recouvrait la voie d'accès au parking souterrain ne dépassait pas sensiblement le niveau du sol naturel, ce dont il a déduit qu'elle ne pouvait être regardée comme une construction pour l'application de l'article UA 9 et n'avait pas à être intégrée dans le calcul de l'emprise au sol.<br/>
<br/>
              7. En troisième lieu, d'une part, si le tribunal a décrit le projet, qui comporte trois étages surplombés d'un attique, comme consistant en la construction d'un " bâtiment en R + 3 dont le dernier étage est prévu en attique ", il n'en résulte pas, alors qu'il s'est fondé sur la hauteur de la construction de 14,2 mètres en façade, que son appréciation de l'insertion du projet dans son environnement urbain reposerait sur une dénaturation des faits de l'espèce. D'autre part, pour juger que la hauteur du projet n'apparaissait pas disproportionnée par rapport à celle des constructions environnantes et que son architecture contemporaine n'était pas de nature à porter atteinte aux caractéristiques du bâti environnant, le tribunal a relevé que le secteur ne comportait pas seulement un habitat individuel de plain pied ou en R + 1 mais également des immeubles collectifs en R + 3 implantés à l'extrémité de l'impasse à l'est du projet et que les quelques maisons recouvertes de bois voisines du projet n'étaient pas représentatives de l'architecture générale du quartier, qui ne présentait pas d'harmonie particulière. Il ne ressort pas des pièces du dossier soumis aux juges du fond que l'appréciation souveraine à laquelle le tribunal s'est ainsi livré serait entachée de dénaturation.<br/>
<br/>
              8. En quatrième lieu, en jugeant que le mur enduit blanc, comportant des menuiseries aluminium, implanté à l'alignement de la voie publique était un élément de façade sur rue du projet, le tribunal n'a pas dénaturé les pièces du dossier qui lui était soumis. C'est sans erreur de droit qu'il en a déduit que ce mur n'était pas soumis au respect des prescriptions du règlement du plan local d'urbanisme applicables aux clôtures.<br/>
<br/>
              9. En dernier lieu, l'article UA 13 du règlement du plan local d'urbanisme impose que 20 % de la surface des parcelles supérieures à 500 mètres carrés soient " aménagés sous forme de pleine terre ", le lexique de ce règlement précisant que : " Les espaces de pleine terre comprennent les surfaces perméables qui ne sont pas destinées à la circulation et au stationnement automobile (hors véhicule de secours) (...) ". En jugeant, pour écarter la méconnaissance de ces dispositions par le projet, qu'elles n'excluaient pas la prise en compte des espaces du rez-de-chaussée plantés situés sous les balcons du premier étage, le tribunal n'a pas commis d'erreur de droit. Il n'a pas davantage dénaturé les pièces du dossier en estimant qu'en prenant en compte ces espaces, les espaces de pleine terre représentaient en l'espèce une surface totale de 246,50 mètres carrés, supérieure à la surface de 242,60 mètres carrés requise sur la parcelle.<br/>
<br/>
              10. Il résulte de tout de ce qui précède que les requérants ne sont pas fondés à demander l'annulation du jugement du tribunal administratif de Poitiers qu'ils attaquent.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Médiatim Promotion ou de la commune de La Rochelle, qui ne sont pas, dans la présente instance, les parties perdantes. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de chacun des requérants une somme de 100 euros à verser tant à la société Médiatim Promotion qu'à la commune de La Rochelle, au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de M.E..., M.R..., M.AG..., M.W..., Mme P...et Mme AA...est admise.<br/>
Article 2 : Le pourvoi de M. F...et des autres requérants est rejeté. <br/>
Article 3 : M.F..., MmeY..., M.U..., M.AB..., la SCI Morvan, M.M..., M.Z..., M. AF...et M. T...verseront chacun 100 euros à la société Médiatim Promotion et 100 euros à la commune de La Rochelle au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée, pour l'ensemble des requérants, à M. L...F..., représentant désigné, à la société Médiatim Promotion, à la commune de La Rochelle et, pour l'ensemble des intervenants, à M. AH...-J...E..., premier dénommé.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
