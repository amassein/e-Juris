<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033404335</ID>
<ANCIEN_ID>JG_L_2016_11_000000386141</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/40/43/CETATEXT000033404335.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/11/2016, 386141, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386141</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386141.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris : <br/>
              - d'annuler, pour excès de pouvoir, la décision implicite par laquelle le préfet de police, agissant au nom de la ville de Paris, a refusé de lui communiquer la liste des bénéficiaires, pour les années 2012 et 2013, de cartes de circulation invitant les agents de l'autorité et de la force publique à faciliter la libre circulation et le stationnement de véhicules ; <br/>
              - d'enjoindre au préfet de police de lui communiquer cette liste dans un délai d'un mois suivant la notification de son jugement, sous astreinte de 30 euros par jour de retard à compter de l'expiration de ce délai. <br/>
<br/>
              Par un jugement n° 130933/6-2 du 28 octobre 2014, le tribunal administratif de Paris, faisant partiellement droit à la demande de M.B..., a annulé la décision implicite par laquelle le préfet de police a refusé de lui communiquer la liste des bénéficiaires de cartes de circulation, enjoint à ce dernier de communiquer au requérant la liste demandée dans un délai d'un mois à compter de la notification de son jugement et rejeté le surplus des conclusions dont il était saisi. <br/>
<br/>
              M. B...a demandé au tribunal administratif de Paris, statuant sur le fondement de l'article L. 911-4 du code de justice administrative, de prescrire au préfet de police toutes mesures d'exécution de ce jugement du 28 octobre 2014 et de mettre à la charge de l'Etat une somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative. Par un jugement n° 1508388/5-1 du 11 février 2016, le tribunal administratif de Paris a rejeté sa demande au motif que la liste dont la communication était enjointe au préfet de police aurait été détruite. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er décembre 2014 et 2 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 1er du jugement n° 130933/6-2 du 28 octobre 2014 du tribunal administratif de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de rejeter la demande de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la ville de Paris et à la SCP Bouzidi, Bouhanna, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les écritures de M. B...présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, bien que l'intéressé ait été informé de l'obligation de recourir à ce ministère, doivent être écartées des débats.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au tribunal administratif de Paris que M. B...a demandé au préfet de police de lui communiquer la liste des bénéficiaires, pour les années 2012 et 2013, des cartes de circulation délivrées par ses soins invitant les agents de l'autorité et de la force publique à faciliter la libre circulation et le stationnement irrégulier de véhicules. En l'absence de réponse, M. B...a saisi la Commission d'accès aux documents administratifs (CADA), qui a émis un avis favorable à la communication de cette liste le 25 avril 2013, sous réserve de l'occultation des mentions pouvant porter atteinte au respect de la vie privée. La ville de Paris se pourvoit en cassation contre le jugement du 28 octobre 2014 par lequel le magistrat désigné du tribunal administratif de Paris a annulé la décision implicite de refus qu'il a opposée à la demande de communication de l'intéressé et lui a enjoint de procéder à cette communication dans un délai d'un mois à compter de la notification de ce jugement. <br/>
<br/>
              3. Contrairement à ce qui est soutenu, la demande présentée par M. B...relative à la communication de la liste des bénéficiaires de cartes de circulation tendant à favoriser la libre circulation et le stationnement irrégulier de véhicules de leurs titulaires, et celle présentée par l'intéressé devant le tribunal administratif de Paris, portant sur la communication de la liste des bénéficiaires de cartes de circulation tendant à favoriser la libre circulation de véhicules, doivent être regardées comme ayant le même objet. Il suit de là que c'est sans erreur de droit que le tribunal administratif a écarté la fin de non-recevoir soulevée pour ce motif par la ville de Paris. <br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi de la ville de Paris, doit être rejeté. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris une somme de 2 000 euros à verser à la SCP Bouzidi-Bouhanna, avocat de M.B..., au titre des dispositions combinées de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la ville de Paris est rejeté. <br/>
<br/>
Article 2 : La ville de Paris versera à la SCP Bouzidi-Bouhanna, avocat de M.B..., une somme de 2 000 euros au titre des dispositions combinées de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la ville de Paris et à M. A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
