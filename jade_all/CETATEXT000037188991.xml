<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037188991</ID>
<ANCIEN_ID>JG_L_2018_07_000000421466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/18/89/CETATEXT000037188991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/07/2018, 421466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421466.20180709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Lyon de l'admettre au bénéfice de l'aide juridictionnelle et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de suspendre l'exécution par le préfet de l'Ain de l'arrêté du 19 mars 2018 par lequel le préfet du Rhône a refusé de lui délivrer une attestation de demande d'asile, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi, d'autre part, d'enjoindre au préfet de l'Ain de cesser immédiatement toute mesure de contrôle ou de surveillance à son égard et, enfin, d'enjoindre au préfet du Rhône de lui accorder une autorisation provisoire de séjour, de réexaminer sa situation administrative et de lui délivrer une attestation de demandeur d'asile dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1803647 du 30 mai 2018, le juge des référés du tribunal administratif de Lyon, après avoir admis M. A...au bénéfice de l'aide juridictionnelle provisoire, a suspendu l'exécution de la décision du 19 mars 2018 par laquelle le préfet du Rhône a décidé que M. A...serait reconduit en Turquie jusqu'à ce que ce préfet se prononce de nouveau sur le pays de destination duquel M. A...est susceptible d'être reconduit et a rejeté le surplus des conclusions de la requête.<br/>
              Par une requête, enregistrée le 13 juin 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la requête présentée par M. A...en première instance.<br/>
<br/>
<br/>
<br/>
              Le ministre d'Etat, ministre de l'intérieur soutient que :<br/>
              - la demande de première instance de M. A...était irrecevable dès lors qu'il n'a apporté aucun élément nouveau de nature à caractériser un changement de circonstances de droit ou de fait permettant de considérer que son éloignement vers la Turquie est susceptible d'emporter des effets excédant ceux s'attachant normalement à la mise en oeuvre d'une décision d'éloignement ; <br/>
              - il n'est porté aucune atteinte grave et manifestement illégale au droit de M. A... à ne pas être exposé à des traitements inhumains et dégradant, au sens de l'article 3 de la convention européenne de sauvegarde des droits de l'homme, dès lors que, d'une part, il n'établit pas de manière suffisamment claire et précise qu'il serait exposé à titre personnel à de tels risques en cas d'éloignement vers la Turquie et, d'autre part, une condamnation pénale étrangère, à la supposer établie, ne porte pas en soi une atteinte à l'article 3 de la convention européenne de sauvegarde des droits de l'homme.<br/>
<br/>
              Par un mémoire en défense, enregistré le 27 juin 2018, M. A... conclut au rejet de la requête et que la somme de 2 000 euros soit mise à la charge de l'Etat en application de l'article L. 761-1 du code de justice administrative. Il soutient que les moyens soulevés par le ministre requérant ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre d'Etat, ministre de l'intérieur, d'autre part, M. A... ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 2 juillet 2018 à 11 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - les représentantes du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              - le représentant de M. A...;<br/>
<br/>
              - M.A..., assisté de l'interprète assermentée désignée à sa demande par le président de la section du contentieux ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Il résulte de l'instruction que M. B...A..., ressortissant turc, entré irrégulièrement en France, a introduit le 20 octobre 2014 une demande d'asile. Par une décision du 6 février 2015, confirmée par une décision du 23 juillet 2015 de la Cour nationale du droit d'asile, l'Office français de protection des réfugiés et apatrides a rejeté sa demande. Le 11 juillet 2017, M. A...a demandé le réexamen de sa demande d'asile. Par une décision du 18 juillet 2017, l'Office français de protection des réfugiés et apatrides a rejeté sa demande de réexamen comme irrecevable. Le 19 mars 2018, M. A...a de nouveau sollicité l'asile. Par arrêté du même jour, le préfet du Rhône lui a, d'une part, refusé la délivrance d'une attestation de demande d'asile, d'autre part, fait obligation de quitter le territoire français dans un délai de trente jours, en fixant la Turquie comme pays de destination à l'issue de ce délai et, enfin, interdit le retour sur le territoire français pour une durée de six mois. Par une décision du 16 avril 2018, l'Office français de protection des réfugiés et apatrides a rejeté à nouveau la demande de réexamen de M. A... comme irrecevable. Le 17 avril 2018, M. A...a demandé au tribunal administratif de Lyon l'annulation de l'arrêté du 19 mars 2018 du préfet du Rhône. Par un jugement du 18 mai 2018, le tribunal administratif de Lyon a rejeté sa demande comme tardive. Le 24 mai 2018, M. A... a été interpellé dans l'Ain puis placé en rétention administrative par le préfet de ce département. Par une ordonnance du 26 mai 2018, le juge des libertés et de la détention du tribunal de grande instance de Lyon a prolongé de 28 jours la mesure de rétention administrative prise à son encontre. Par une ordonnance du 30 mai 2018, le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a ordonné la suspension de l'exécution de l'arrêté du préfet du Rhône du 19 mars 2018 en tant qu'il a fixé la Turquie comme pays à destination duquel M. A...serait reconduit, jusqu'à ce que le préfet du Rhône se soit de nouveau prononcé sur le pays à destination duquel l'intéressé est susceptible d'être reconduit. Par une ordonnance du 1er juin 2018, le juge des libertés et de la détention de Lyon a ordonné la mainlevée de sa rétention administrative. Il résulte des précisions apportées à l'audience par le ministre d'Etat, ministre de l'intérieur, qu'il relève appel de l'ordonnance du 30 mai 2018 du juge des référés du tribunal administratif de Lyon en tant que, par l'article 2 de cette ordonnance, celui-ci a suspendu l'exécution de la décision fixant la Turquie comme pays de renvoi de M.A....<br/>
<br/>
              3. Aux termes de l'article L. 743-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le demandeur d'asile dont l'examen de la demande relève de la compétence de la France et qui a introduit sa demande auprès de l'Office français de protection des réfugiés et apatrides bénéficie du droit de se maintenir sur le territoire français jusqu'à la notification de la décision de l'office ou, si un recours a été formé, jusqu'à la notification de la décision de la Cour nationale de droit d'asile (...). ". Aux termes de l'article L. 743-2 du même code : " Par dérogation à l'article L. 743-1, sous réserve du respect des stipulations de l'article 33 de la convention relative au statut des réfugiés, signée à Genève le 28 juillet 1951, et de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, adoptée à Rome le 4 novembre 1950, le droit de se maintenir sur le territoire français prend fin et l'attestation de demande d'asile peut être refusée, retirée ou son renouvellement refusé lorsque : (...) 5° L'étranger présente une nouvelle demande de réexamen après le rejet définitif d'une première demande de réexamen ". Aux termes de l'article L. 743-3 du même code : " L'étranger auquel la reconnaissance de la qualité de réfugié ou le bénéfice de la protection subsidiaire a été définitivement refusé ou qui ne bénéficie plus du droit de se maintenir sur le territoire français en application de l'article L. 743-2 et qui ne peut être autorisé à demeurer sur le territoire à un autre titre doit quitter le territoire français, sous peine de faire l'objet d'une mesure d'éloignement prévue au titre Ier du livre V et, le cas échéant, des pénalités prévues au chapitre Ier du titre II du livre VI ". Aux termes du I de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'autorité administrative peut obliger à quitter le territoire français un étranger non ressortissant d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse et qui n'est pas membre de la famille d'un tel ressortissant au sens des 4° et 5° de l'article L. 121-1, lorsqu'il se trouve dans l'un des cas suivants : (...) 6° Si la reconnaissance de la qualité de réfugié ou le bénéfice de la protection subsidiaire a été définitivement refusé à l'étranger ou si l'étranger ne bénéficie plus du droit de se maintenir sur le territoire français en application de l'article L. 743-2, à moins qu'il ne soit titulaire d'un titre de séjour en cours de validité (...) / L'obligation de quitter le territoire français fixe le pays à destination duquel l'étranger est renvoyé en cas d'exécution d'office. ".<br/>
<br/>
              4. Le 4° de l'article R. 776-1 du code de justice administrative prévoit que la décision fixant le pays de renvoi, dont l'article R. 513-3 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit qu'elle constitue une décision distincte de la mesure d'éloignement elle-même, est jugée selon les dispositions de l'article L. 512-1 de ce code. Le I bis de ce dernier article prévoit que " L'étranger qui fait l'objet d'une obligation de quitter le territoire français sur le fondement des 1°, 2°, 4° ou 6° du I de l'article L. 511-1 et qui dispose du délai de départ volontaire mentionné au premier alinéa du II du même article L. 511-1 peut, dans un délai de quinze jours à compter de sa notification, demander au président du tribunal administratif l'annulation de cette décision, ainsi que l'annulation de la décision mentionnant le pays de destination et de la décision d'interdiction de retour sur le territoire français qui l'accompagnent le cas échéant. ". Il résulte de l'article L. 513-3 du code de l'entrée et du séjour des étrangers et du droit d'asile que le recours contentieux contre la décision fixant le pays de renvoi est suspensif d'exécution, dans les conditions prévues au second alinéa de l'article L. 512-3 de ce code, s'il est présenté en même temps que le recours contre la mesure d'éloignement qu'elle vise à exécuter.<br/>
<br/>
              5. Il ressort des dispositions du I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu organiser une procédure spéciale applicable au cas où un étranger fait l'objet, sur le fondement de certaines des dispositions de l'article L. 511-1 de ce code, d'une obligation de quitter le territoire français assortie d'un délai de retour volontaire. L'introduction d'un recours sur le fondement de ces dispositions, qui prévoient que l'étranger peut, dans le délai de quinze jours suivant sa notification, demander au président du tribunal administratif l'annulation de cette décision, ainsi que l'annulation de la décision relative au séjour, de la décision mentionnant le pays de destination et de la décision d'interdiction de retour sur le territoire français qui l'accompagnent le cas échéant, a par elle-même pour effet de suspendre l'exécution de l'obligation de quitter le territoire français jusqu'à ce que le tribunal administratif ait statué, en vertu de l'article L. 512-3 du même code. Saisi au plus tard quinze jours après la notification de l'obligation de quitter le territoire français, le tribunal administratif statue dans un délai de six semaines. Dans ce cadre, il dispose d'un pouvoir d'annulation non seulement de la mesure d'éloignement mais également des autres mesures contestées devant lui et peut également connaître de conclusions à fin d'injonction présentées au titre des articles L. 911-1 et L. 911-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte des pouvoirs ainsi confiés au juge par les dispositions du I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, des délais qui lui sont impartis pour se prononcer et des conditions de son intervention que la procédure spéciale prévue par le code de l'entrée et du séjour des étrangers et du droit d'asile présente des garanties au moins équivalentes à celles des procédures régies par le livre V du code de justice administrative. Ces procédures particulières sont exclusives de celles prévues par le livre V du code de justice administrative. Il en va autrement dans le cas où les modalités selon lesquelles il est procédé à l'exécution d'une obligation de quitter le territoire français emportent des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi sur le fondement du I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à sa mise à exécution.<br/>
<br/>
              7. A l'appui de sa demande présentée au titre de l'article L. 521-2 du code de justice administrative, M. A...fait valoir qu'il a fait l'objet le 1er décembre 2014 d'une condamnation à sept ans et dix mois d'emprisonnement par la cour d'assises de Mus en raison de sa qualité de membre du parti pour la paix et de la démocratie (BDP), engagé en faveur de la cause kurde, et pour avoir participé le 7 octobre 2014 à une manifestation organisée par le parti des travailleurs du Kurdistan (PKK) contre les massacres commis contre les Kurdes à Kobané, qu'il fait l'objet de recherches en vue de l'exécution de cette peine, prononcée pour des motifs politiques, et qu'il est exposé à des actes de torture en détention. M. A...a, pour la première fois en cause d'appel, produit des traductions assermentées, en date du 21 juin 2018, de témoignages manuscrits, datés du 15 mai 2018, de sa mère, de son père et de son frère, accompagnés de photos des cartes d'identité des intéressés, faisant état de perquisitions répétées des forces de police turques en 2014, 2015, 2016 et 2017 en vue de la recherche de l'intéressé. Il a, en outre, présenté lors de l'audience d'appel l'original de sa carte d'identité. Il a, enfin, produit la décision du 12 juin 2018 par laquelle le président du bureau d'aide juridictionnelle près la Cour nationale du droit d'asile l'a admis au bénéfice de l'aide juridictionnelle totale en vue de former un recours contre la décision du 16 avril 2018 de l'Office français de protection des réfugiés et apatrides rejetant comme irrecevable sa demande de réexamen de sa demande d'asile.<br/>
<br/>
              8. Si, par elle-même, la condamnation prononcée à l'encontre de M. A...le 1er décembre 2014 ne peut, dès lors qu'elle était déjà connue de l'intéressé et avait été produite par lui à l'appui de sa demande de réexamen présentée à l'Office français de protection des réfugiés et apatrides le 11 juillet 2017, être regardée comme un changement dans les circonstances de droit ou de fait et si le ministre requérant est par suite fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Lyon a jugé la demande de l'intéressé recevable au vu de cette seule circonstance, les éléments ainsi réunis et apportés pour la première fois en cause d'appel et à l'audience traduisent, en l'état de l'instruction, en ce qu'elles sont susceptibles d'établir la réalité de cette condamnation le concernant, dont le doute sur l'authenticité fondait la décision litigieuse, un changement dans les circonstances de fait qui ont conduit le préfet du Rhône à fixer la Turquie comme le pays où le requérant serait reconduit. M. A... est, par suite, recevable à saisir le juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande de suspension de l'exécution de l'arrêté du 19 mars 2018 en tant qu'il ordonne son éloignement vers la Turquie en invoquant, au vu de ces nouvelles circonstances, l'atteinte grave et manifestement illégale que son exécution porterait à son droit de ne pas être soumis à des traitements inhumains ou dégradants.<br/>
<br/>
              9. Le ministre requérant ne contestant pas que la condition d'urgence doit être regardée comme étant remplie, l'éloignement de M. A...décidé par l'arrêté du 19 mars 2018 du préfet du Rhône étant susceptible d'intervenir à tout moment, et les éléments décrits au point précédant étant de nature, en l'état de l'instruction et dans les circonstances particulières de l'espèce, à faire craindre une atteinte au droit de M. A... de ne pas être soumis à des traitements inhumains ou dégradants en cas de retour en Turquie, le ministre d'Etat, ministre de l'intérieur, n'est pas fondé à se plaindre de ce que le juge des référés du tribunal administratif de Lyon a ordonné la suspension de l'exécution de l'arrêté du préfet du Rhône 19 mars 2018 en tant qu'il a fixé la Turquie comme pays à destination duquel M. A...serait reconduit jusqu'à ce que le préfet du Rhône se soit de nouveau prononcé sur le pays à destination duquel l'intéressé est susceptible d'être reconduit.<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme que M. A...demande en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du ministre d'Etat, ministre de l'intérieur est rejetée.<br/>
Article 2 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
