<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571749</ID>
<ANCIEN_ID>JG_L_2016_05_000000393785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571749.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 20/05/2016, 393785, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mme Célia Verot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393785.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Cergy-Pontoise d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, au maire de Sceaux, en tant que directeur de la publication de " Sceaux-mag ", de supprimer du bulletin municipal en ligne sur le site Internet de la ville la note de la rédaction figurant, au sein de l'espace réservé aux groupes d'opposition, sous la tribune du groupe d'opposition " La Voix des Scéens ", de rétablir la police d'écriture initiale de cette tribune et de faire état de ce litige dans le prochain bulletin. <br/>
<br/>
              Par une ordonnance n° 1507372 du 10 septembre 2015, le juge des référés du tribunal administratif de Cergy-Pontoise a enjoint au maire de Sceaux, en tant que directeur de la publication du bulletin municipal " Sceaux mag " et du site internet, de supprimer la " note de la rédaction " insérée à la suite de la tribune du groupe " La Voix des Scéens " disponible sur le site internet de la commune et de mettre en forme cette tribune dans une police d'écriture équivalente à celle utilisée pour les tribunes des deux autres groupes n'appartenant pas à la majorité municipale. Le juge des référés a ensuite rejeté le surplus de la demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 septembre et 12 octobre 2015 et 23 mars 2016 au secrétariat du contentieux du Conseil d'État, la commune de Sceaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M.A....<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Célia Verot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de la commune de Sceaux ;  <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ;<br/>
<br/>
              2. Saisi sur le fondement de l'article L. 521-3 d'une demande qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, notamment sous forme d'injonctions adressées à l'administration, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse. En raison du caractère subsidiaire du référé régi par l'article L. 521-3, le juge saisi sur ce fondement ne peut prescrire les mesures qui lui sont demandées lorsque leurs effets pourraient être obtenus par les procédures de référé régies par les articles L. 521-1 et L. 521-2. Enfin, il ne saurait faire obstacle à l'exécution d'une décision administrative, même celle refusant la mesure demandée, à moins qu'il ne s'agisse de prévenir un péril grave.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que le maire de la commune de Sceaux avait rejeté, par une décision du 13 août 2015, la demande de M. A... tendant à l'adoption des mesures qu'il a ensuite demandé au juge des référés d'ordonner, sur le fondement de l'article L. 521-3 du code de justice administrative. Tenu de ne pas faire obstacle à l'exécution de ces décisions, ce juge ne pouvait que rejeter les demandes dont il était ainsi saisi. Dès lors, en enjoignant au maire de Sceaux en tant que directeur de la publication du bulletin municipal " Sceaux mag " et du site internet, de procéder à la suppression de la " note de la rédaction " insérée à la suite de la tribune du groupe " La voix des Scéens " disponible sur le site internet de la commune et par voie de conséquence, à la mise en forme de cette tribune dans une police d'écriture équivalente à celle utilisée pour les tribunes des deux autres groupes n'appartenant pas à la majorité municipale, le juge des référés a commis une erreur de droit. Par suite, l'article 1er de l'ordonnance attaquée doit être annulé.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par M.A..., en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A...n'est pas fondé à demander au juge des référés, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre au maire de la commune de Sceaux, en sa qualité de directeur de publication du bulletin municipal de " Sceaux mag ", de modifier la page 42 du bulletin municipal n° 460 des mois de juillet-août 2015 disponible sur le site internet en procédant à la suppression de la " note de la rédaction de Sceaux mag " insérée en bas de la tribune du groupe " La Voix des Scéens " et au rétablissement de la police d'écriture initiale de cette tribune. Par suite, ses demandes ne peuvent qu'être rejetées, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. Par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 2 000 euros demandée au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 3 de l'ordonnance du 10 septembre 2015 du juge des référés du tribunal administratif de Cergy-Pontoise est annulé.<br/>
Article 2 : Les conclusions de M. A...tendant à ce que le juge des référés enjoigne, sur le fondement de l'article L. 521-3 du code de justice administrative, au maire de la commune de Sceaux, en sa qualité de directeur de publication du bulletin municipal de " Sceaux mag ", de modifier la page 42 du bulletin municipal n° 460 des mois de juillet-août 2015 disponible sur le site internet en procédant à la suppression de la " note de la rédaction de Sceaux mag " insérée en bas de la tribune du groupe " La Voix des Scéens " et au rétablissement de la police d'écriture initiale de cette tribune, ainsi que ses conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : Les conclusions de la commune de Sceaux au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Sceaux et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
