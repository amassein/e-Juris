<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041935993</ID>
<ANCIEN_ID>JG_L_2020_05_000000440481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/93/59/CETATEXT000041935993.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/05/2020, 440481, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440481.20200526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Poitiers, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au département des Deux-Sèvres de procéder à son hébergement dans une structure adaptée et de prendre en charge ses besoins alimentaires, sanitaires et médicaux dans un délai de 24 heures, sous astreinte de 100 euros par jour de retard et jusqu'à ce que l'autorité judiciaire ait définitivement statué sur son recours fondé sur les articles 375 et suivants du code civil. Par une ordonnance n° 2001029 du 24 avril 2020, le juge des référés du tribunal administratif de Poitiers a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 8 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge du département des Deux-Sèvres la somme de 1 200 euros au titre des dispositions combinées des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable eu égard, en premier lieu, à sa qualité de mineur isolé, en deuxième lieu, à l'absence de voie de recours d'urgence devant le juge judiciaire et, en dernier lieu, à la caractérisation d'une situation d'urgence de nature à justifier une intervention du juge des référés saisi au titre de l'article L. 521-2 du code de justice administrative ;<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, il ne dispose d'aucune prise en charge, d'aucun hébergement et d'aucun moyen de subsistance et, en second lieu, la pandémie due à la maladie coronavirus 19 ou covid-19 le place dans une situation de danger exceptionnel compte tenu des risques de contamination et de son impossibilité à respecter les consignes sanitaires ;<br/>
              - la carence de l'autorité administrative à assurer sa prise en charge au titre de l'aide sociale à l'enfance est constitutive d'une atteinte grave et manifestement illégale à l'intérêt supérieur de l'enfant, au droit au respect de la vie, au respect de la dignité humaine, au droit de ne pas être soumis à des traitements inhumains et dégradants et au droit à un recours effectif ; <br/>
              - le département des Deux-Sèvres a méconnu son obligation de satisfaire à l'hébergement et aux besoins des mineurs isolés en situation de détresse alors que les documents qui lui ont été communiqués et produits devant le juge des référés de première instance font foi et établissent à la fois l'identité et la minorité du détenteur.<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 mai 2020, le président du conseil départemental des Deux-Sèvres conclut au rejet de la requête. Il soutient, en premier lieu, que l'office du juge administratif des référés ne lui permet pas d'ordonner l'admission d'un mineur à l'aide sociale à l'enfance, de se prononcer sur sa minorité ou de se substituer au juge judiciaire pour pourvoir à cette mission et, en second lieu, que la condition d'urgence n'est pas satisfaite et qu'aucune atteinte grave et manifestement illégale n'est portée à une liberté fondamentale.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 22 mai 2020 à 18 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
              2. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Poitiers que M. A... B..., qui indique être un ressortissant albanais né le 28 mai 2004, s'est présenté aux services du département des Deux-Sèvres le 26 septembre 2919 pour y solliciter l'accueil provisoire d'urgence en qualité de mineur étranger isolé. A l'issue de l'évaluation de la situation de l'intéressé, le président du conseil départemental, par une décision du 5 novembre 2019, a refusé sa prise en charge au titre de l'aide sociale à l'enfance. Le 16 avril 2020, M. B... a saisi de sa situation le tribunal pour enfants C....  <br/>
<br/>
              3. M. B... relève appel de l'ordonnance du 24 avril 2020 par laquelle le juge des référés du tribunal administratif de Poitiers, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint, sous astreinte, au président du conseil départemental de lui proposer, dans l'attente de la décision du juge des enfants à intervenir, un hébergement d'urgence incluant la prise en charge de ses besoins alimentaires quotidiens, de sa santé et de son éducation.<br/>
<br/>
              Sur les dispositions applicables :<br/>
<br/>
              4. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". L'article R. 221-11 du code de l'action sociale et des familles dispose que : " I. Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II. Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". Aux termes par ailleurs de l'article 375-3 du code civil : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ".<br/>
<br/>
              5. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. A cet égard, une obligation particulière pèse sur ces autorités lorsqu'un mineur privé de la protection de sa famille est sans abri et que sa santé, sa sécurité ou sa moralité est en danger. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, en tenant compte des moyens dont l'administration départementale dispose ainsi que de la situation du mineur intéressé, quelles sont les mesures qui peuvent être utilement ordonnées sur le fondement de l'article L. 521-2.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              6. En l'espèce, il résulte de ce qu'il a été dit au point 2, et il n'est d'ailleurs pas contesté, que M. B..., s'étant déclaré mineur et isolé, le département des Deux-Sèvres  a assuré sa prise en charge au titre de l'accueil provisoire d'urgence prévu par les dispositions mentionnées aux points précédents et, dans ce cadre, l'évaluation de sa situation prévue par l'article R. 221-11 du code de l'action sociale et des familles.  <br/>
<br/>
              7. Dans ces conditions, le département des Deux-Sèvres ayant satisfait en l'espèce aux obligations qui lui incombaient au titre des dispositions mentionnées aux points précédents, son refus de poursuivre la prise en charge de l'intéressé en raison des conclusions de l'évaluation mettant en évidence des doutes sérieux sur la situation d'isolement  de ce mineur, ne révèle de la part du département, en l'état de l'instruction, aucune atteinte grave et manifestement illégale au droit à l'hébergement et à la prise en charge éducative d'un enfant mineur et dépourvu de la protection de sa famille. M. B... se borne à reprendre en appel l'argumentation qui, compte tenu de l'ensemble des éléments du dossier, a été écartée à bon droit par le juge des référés du tribunal administratif de Poitiers. Par suite, compte tenu tant de l'office du juge du référé-liberté que de la situation du requérant, son appel ne peut être accueilli. Dès lors, il y a lieu de rejeter la requête de M. B..., y compris les conclusions tendant à l'application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative sans qu'il y ait lieu d'admettre le requérant à titre provisoire au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B... et au président du conseil départemental des Deux-Sèvres.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
