<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045099894</ID>
<ANCIEN_ID>JG_L_2022_01_000000441800</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/09/98/CETATEXT000045099894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 31/01/2022, 441800, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-01-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441800</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:441800.20220131</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union des métiers et des industries de l'hôtellerie (UMIH) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le I de l'article 45 du décret n° 2020-860 du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé, qui interdit l'accueil du public dans les établissements recevant du public du type P "salles de danse" ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de mettre fin à cette interdiction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ; <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - la loi n° 2020-856 du 9 juillet 2020 ; <br/>
              - l'ordonnance n° 2020-317 du 25 mars 2020 ; <br/>
              - le décret n° 2020-860 du 10 juillet 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public, dont les salles de danse relevant de la catégorie des établissements de type P définis en application de l'article R. 123-12 du code de construction et de l'habitation, ont été fermés au public. <br/>
<br/>
              2. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020 puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'article 2 de la loi du 23 mars 2020 a introduit dans la code de la santé publique un article L. 3131-15 permettant au Premier ministre, aux seules fins de garantir la santé publique, de " 5° Ordonner la fermeture provisoire d'une ou plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion, à l'exception des établissements fournissant des biens ou des services de première nécessité ". Par un décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique, plusieurs fois modifié et complété depuis lors, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Le législateur a ensuite, par la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence, autorisé le Premier ministre à prendre à compter du 11 juillet 2020, et jusqu'au 30 octobre 2020 inclus, diverses mesures dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19. Le Premier ministre peut notamment, dans ce cadre, ordonner la fermeture provisoire d'une ou plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion lorsqu'ils accueillent des activités qui, par leur nature même, ne permettent pas de garantir la mise en œuvre des mesures de nature à prévenir les risques de propagation du virus ou lorsqu'ils se situent dans certaines parties du territoire dans lesquelles est constatée une circulation active du virus. En application de ces dispositions, le décret du 10 juillet 2020, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé, a maintenu, au I de son article 45, la fermeture des salles de danse. <br/>
<br/>
              3. L'UMIH demande au Conseil d'Etat, d'une part, d'annuler le décret du 10 juillet 2020 en ce qu'il maintient fermés les discothèques et établissements de nuit (établissements de type " P, salles de danse ") sur l'ensemble du territoire national et, d'autre part, d'enjoindre à l'Etat d'autoriser sans délai l'ouverture au public de ces mêmes établissements sur l'ensemble du territoire national. <br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              4. Si le VI de l'article 1er de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire prévoit que le comité des scientifiques mentionné à l'article <br/>
L. 3131-19 du code de la santé publique se réunit pendant la période du 11 juillet au 30 octobre 2020 et rend périodiquement des avis sur les mesures prescrites en application du I de cet article, ainsi que sur les mesures prises par le ministre chargé de la santé en application de l'article <br/>
L. 3131-1 du code de la santé publique, il ne prévoit pas que les mesures prises par le Premier ministre en application du I devraient nécessairement être précédées d'une consultation de ce comité. Par suite, l'UMIH n'est pas fondée à soutenir que le décret du 10 juillet 2020 est entaché d'un vice de procédure faute pour le Premier ministre d'avoir pris en considération l'avis du comité des scientifiques pour décider de maintenir la fermeture des salles de danse.<br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              5. Le maintien, en vertu du I de l'article 45 du décret du 10 juillet 2020, de la fermeture des établissements de type P, " salles de danse ", répond à la recommandation émise par le Haut conseil de la santé publique dans un avis publié le 1er juin 2020 au sujet des " mesures barrières et de distanciation physique dans les espaces culturels ", selon lequel " Les discothèques et les festivals accueillant de très nombreux spectateurs ne peuvent respecter les recommandations du HCSP relatives à cette période de déconfinement et de reprise d'activité ". <br/>
<br/>
              6. En premier lieu, si l'UMIH se prévaut du guide sanitaire qu'elle a élaboré en mai 2020 qui prévoit notamment l'obligation de respecter dans les boites de nuit, en toutes circonstances, une distance d'au moins un mètre entre les personnes, le caractère clos des établissements en cause, les horaires d'ouverture étendus et la nature d'activité physique de la danse ne paraissent pas de nature à garantir le respect des règles de distanciation physique. Par suite, l'interdiction faite aux établissements de type P d'exploiter leur activité de salle de danse ne revêt pas, au regard de l'objectif de protection de la santé publique poursuivi, un caractère disproportionné. <br/>
<br/>
              7. En deuxième lieu, la requérante ne saurait utilement se prévaloir du traitement dont font l'objet les salles de jeux ou les lieux d'exercice de sports collectifs, qui se trouvent dans une situation différente de celle des salles de danse, ni soutenir que la mesure contestée serait incohérente<br/>
<br/>
              8. En dernier lieu, la circonstance que le décret attaqué, comme l'arrêté du 14 mars 2020 et les décrets qui l'ont précédé, se réfère, pour identifier les catégories d'établissements recevant du public afin de déterminer ceux d'entre eux pouvant faire l'objet de mesures restrictives d'accueil du public, ou d'une fermeture provisoire, au classement établi sur le fondement de l'article R. 123-12 du code de la construction et de l'habitation en vue de la protection contre les risques d'incendie et de panique, n'imposait pas à ses auteurs de distinguer les établissements appartenant aux différentes catégories selon leur capacité d'accueil, dès lors que ce critère n'était pas utile pour déterminer les mesures les plus efficaces pour lutter contre la propagation du virus. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la requête de l'Union des métiers et des industries de l'hôtellerie doit être rejetée. Il en va de même, en conséquence, de ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requête de l'Union des métiers et des industries de l'hôtellerie est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Union des métiers et des industries de l'hôtellerie, au Premier ministre et au ministre des solidarités et de la santé.<br/>
              Délibéré à l'issue de la séance du 7 janvier 2022 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; M. Cyril Roger-Lacan, conseiller d'Etat et Mme Catherine Moreau, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 31 janvier 2022.<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Moreau<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
