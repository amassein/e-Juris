<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043335962</ID>
<ANCIEN_ID>JG_L_2021_04_000000428233</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/33/59/CETATEXT000043335962.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 07/04/2021, 428233</TITRE>
<DATE_DEC>2021-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428233</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:428233.20210407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir la délibération du 10 février 2014 par laquelle le conseil municipal de Sète a approuvé le plan local d'urbanisme de la commune ainsi que la décision du 24 avril 2014 par laquelle le maire de Sète a rejeté son recours gracieux contre cette délibération. Par un jugement n° 1403158 du 4 février 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA01283 du 16 décembre 2016, la cour administrative d'appel de Marseille a, sur appel de Mme A..., annulé ce jugement ainsi que la délibération du 10 février 2014 par laquelle le conseil municipal de Sète a approuvé le plan local d'urbanisme en tant que ce dernier crée l'emplacement réservé n° 29 pour la réalisation d'une voie publique et en tant qu'il ne classe pas les parcelles cadastrées section BM n° 73 et 89 en espaces boisés classés, ainsi que la décision du 24 avril 2014 rejetant son recours gracieux, dans cette mesure, et rejeté le surplus des conclusions présentées par Mme A....<br/>
<br/>
              Par une décision n° 408068 du 30 mai 2018, le Conseil d'Etat statuant au contentieux a annulé les articles 1, 2 et 4 de cet arrêt et renvoyé l'examen de l'affaire à la cour administrative d'appel de Marseille.<br/>
<br/>
              Par un arrêt n° 18MA02615 du 20 décembre 2018, la cour administrative d'appel de Marseille, d'une part, a annulé le jugement du 4 février 2016 du tribunal administratif de Montpellier et la délibération du 10 février 2014, en tant qu'elle crée l'emplacement réservé n° 29 et qu'elle ne classe pas les parcelles cadastrées section BM n° 73 et 89 en espaces boisés classés, ainsi que la décision du 24 avril 2014 rejetant le recours gracieux de Mme A..., dans cette mesure, d'autre part, a rejeté les conclusions de la commune de Sète tendant à la mise en oeuvre des dispositions de l'article L. 600-9 du code de l'urbanisme <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février et 20 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Sète demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A... contre le jugement du 4 février 2016 ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Sète et à la SCP Spinosi, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 10 février 2014, le conseil municipal de Sète a approuvé le plan local d'urbanisme de la commune. Par un jugement du 4 février 2016, le tribunal administratif de Montpellier a rejeté la demande d'annulation pour excès de pouvoir formée par Mme A... contre ce document d'urbanisme ainsi que contre la décision du maire de Sète du 24 avril 2014 rejetant son recours gracieux. Par un arrêt n° 16MA01283 du 16 décembre 2016, la cour administrative d'appel de Marseille a, sur appel de Mme A..., annulé le jugement ainsi que le plan local d'urbanisme approuvé le 10 février 2014 en tant que, d'une part, il crée un emplacement réservé n° 29 sur les parcelles cadastrées section BM n° 73 et 89 dans la partie nordest du Mont SaintClair en vue de la prolongation du boulevard Grangent et, d'autre part, il ne classe pas ces deux parcelles en espaces boisés classés, et, dans cette même mesure, la décision du 24 avril 2014. Saisi d'un pourvoi présenté par la commune, le Conseil d'État, statuant au contentieux, a, par une décision n° 408068 du 30 mai 2018, annulé les articles 1, 2 et 4 de de cet arrêt et renvoyé, dans cette mesure, l'affaire à la cour administrative d'appel de Marseille. Par un arrêt n° 18MA02615 du 20 décembre 2018, la cour administrative d'appel de Marseille a, de nouveau, annulé le jugement du tribunal administratif ainsi que le plan local d'urbanisme approuvé le 10 février 2014 en tant que, d'une part, il crée un emplacement réservé n° 29 sur les parcelles cadastrées section BM n° 73 et 89 dans la partie nord-est du Mont Saint-Clair et, d'autre part, il ne classe pas ces deux parcelles en espaces boisés classés, ainsi que, dans cette même mesure, la décision du 24 avril 2014. Si la commune de Sète se pourvoit en cassation contre cet arrêt, ses conclusions doivent être regardées comme ne tendant à l'annulation que de ses articles 2 à 5.<br/>
<br/>
              Sur les moyens dirigés contre l'arrêt en tant qu'il annule la délibération approuvant le plan local d'urbanisme en tant qu'elle crée l'emplacement réservé n° 29 :<br/>
<br/>
              2. Aux termes du premier aliéna de l'article L. 146-6 du code de l'urbanisme, dans sa rédaction alors en vigueur : " Les documents et décisions relatifs à la vocation des zones ou à l'occupation et à l'utilisation des sols préservent les espaces terrestres et marins, sites et paysages remarquables ou caractéristiques du patrimoine naturel et culturel du littoral, et les milieux nécessaires au maintien des équilibres biologiques. Un décret fixe la liste des espaces et milieux à préserver, comportant notamment, en fonction de l'intérêt écologique qu'ils présentent, (...) les forêts et zones boisées côtières, (...) ". L'article R. 146-1 du même code, dans sa rédaction applicable en l'espèce, prévoit que : " En application du premier alinéa de l'article L. 146-6, sont préservés, dès lors qu'ils constituent un site ou un paysage remarquable ou caractéristique du patrimoine naturel et culturel du littoral, sont nécessaires au maintien des équilibres biologiques ou présentent un intérêt écologique : / (...) b) Les forêts et zones boisées proches du rivage de la mer et des plans d'eau intérieurs d'une superficie supérieure à 1 000 hectares ; / (...) ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué, exemptes de dénaturation sur ce point, que les parcelles cadastrées section BM n° 73 et n° 89, terrain d'assiette de l'emplacement réservé n° 29, présentent un boisement en continuité sur une longueur d'environ 250 mètres avec la forêt résiduelle du bois des Pierres blanches, forêt domaniale de Sète, située sur la partie ouest du Mont Saint-Clair, classée au titre des dispositions précitées, bien que ne présentant pas des caractéristiques faunistiques et floristiques remarquables, en raison de son caractère pittoresque. Il ressort toutefois des pièces du dossier soumis au juge du fond que ces parcelles, dont il n'est pas contesté qu'elles ne constituent pas, par elles-mêmes, un espace remarquable, sont situées au pied du bois des Pierres blanches, dans un secteur caractérisé par une forte déclivité et bordé de plusieurs constructions importantes faisant écran, ne sont pas visibles du littoral, contrairement à ce bois, et ne sont pas nécessaires à la préservation de l'espace remarquable pittoresque du bois des Pierres blanches, avec lequel, elles ne constituent pas, par suite, une unité paysagère. Dès lors, en jugeant que les auteurs du plan local d'urbanisme avaient commis une erreur d'appréciation en ne classant pas ces parcelles en espace remarquable au titre des dispositions précitées, la cour a inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              Sur le moyen dirigé contre l'arrêt en tant qu'il annule la délibération approuvant le plan local d'urbanisme en tant qu'elle ne classe pas les parcelles cadastrées section BM n° 73 et BM n° 89 en espace boisé classé :<br/>
<br/>
              4. Aux termes du dernier alinéa de l'article L. 146-6 du code de l'urbanisme, dans sa rédaction applicable en l'espèce : " Le plan local d'urbanisme doit classer en espaces boisés, au titre de l'article L. 130-1 du présent code, les parcs et ensembles boisés existants les plus significatifs de la commune ou du groupement de communes, après consultation de la commission départementale compétente en matière de nature, de paysages et de sites. ". Par ailleurs, l'article L. 130-1 du même code, également dans sa rédaction applicable, dispose que : " Les plans locaux d'urbanisme peuvent classer comme espaces boisés, les bois, forêts, parcs à conserver, à protéger ou à créer, qu'ils relèvent ou non du régime forestier, enclos ou non, attenant ou non à des habitations. Ce classement peut s'appliquer également à des arbres isolés, des haies ou réseaux de haies, des plantations d'alignements. / Le classement interdit tout changement d'affectation ou tout mode d'occupation du sol de nature à compromettre la conservation, la protection ou la création des boisements. / (...) ". <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que le plan local d'urbanisme approuvé par la délibération litigieuse a classé en espace boisé au sens des dispositions de l'article L. 130-1 du code de l'urbanisme et du dernier alinéa de l'article L. 146-6 du même code, précités, outre le bois privé de Listel sur le Lido, la majeure partie du bois des Pierres blanches à l'exception d'une partie limitée de ce dernier situé dans sa frange nord-est, et recouvrant notamment les parcelles BM n° 73 et BM n° 89 qui, ainsi qu'il a été dit au point 3, ne  constituent pas avec ce dernier une unité paysagère. Par suite, en jugeant que ces parcelles, eu égard aux caractéristiques paysagères qu'elles partagent avec l'ensemble du bois des Pierres et blanches, faisaient ainsi partie des parcs et ensembles boisés les plus significatifs de la commune que l'autorité compétente était tenue de classer au titre de l'article L. 130-1 du code de l'urbanisme, la cour administrative d'appel a dénaturé les faits et pièces du dossier qui lui était soumis.<br/>
<br/>
              6. Il résulte de ce qui précède qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Sète est fondée demander l'annulation des articles 2 à 5 de l'arrêt attaqué de la cour administrative d'appel de Marseille.<br/>
<br/>
              7. L'affaire faisant l'objet d'un second recours en cassation, il y a lieu, dans la mesure précisée au point précédent, de la régler au fond en application des dispositions du second alinéa de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. D'une part, en vertu de l'article L. 1231 du code de l'urbanisme, dans sa rédaction alors en vigueur, " Le plan local d'urbanisme (...) comprend un rapport de présentation, un projet d'aménagement et de développement durables, des orientations d'aménagement et de programmation, un règlement et des annexes ". L'article L. 12313 du même code, dans sa rédaction alors en vigueur, dispose que : " Le projet d'aménagement et de développement durables définit les orientations générales des politiques d'aménagement, d'équipement, d'urbanisme, de protection des espaces naturels, agricoles et forestiers, et de préservation ou de remise en bon état des continuités écologiques. / Le projet d'aménagement et de développement durables arrête les orientations générales concernant l'habitat, les transports et les déplacements, le développement des communications numériques, l'équipement commercial, le développement économique et les loisirs, retenues pour l'ensemble de l'établissement public de coopération intercommunale ou de la commune. / Il fixe des objectifs de modération de la consommation de l'espace et de lutte contre l'étalement urbain. ". L'article L. 12315 du code de l'urbanisme, dans sa rédaction alors en vigueur, dispose que : " Le règlement fixe, en cohérence avec le projet d'aménagement et de développement durables, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 1211, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. / (...) / A ce titre, le règlement peut : / (...) 8° Fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts ; (...) ".<br/>
<br/>
              9. Pour apprécier la cohérence ainsi exigée au sein du plan local d'urbanisme entre le règlement et le projet d'aménagement et de développement durables, il appartient au juge administratif de rechercher, dans le cadre d'une analyse globale le conduisant à se placer à l'échelle du territoire couvert par le document d'urbanisme, si le règlement ne contrarie pas les orientations générales et objectifs que les auteurs du document ont définis dans le projet d'aménagement et de développement durables, compte tenu de leur degré de précision. Par suite, l'inadéquation d'une disposition du règlement du plan local d'urbanisme à une orientation ou un objectif du projet d'aménagement et de développement durables ne suffit pas nécessairement, compte tenu de l'existence d'autres orientations ou objectifs au sein de ce projet, à caractériser une incohérence entre ce règlement et ce projet.<br/>
<br/>
              10. Il ressort des pièces du dossier que si le projet d'aménagement et développement durables du plan local d'urbanisme de la commune de Sète comporte une orientation générale IV tendant à " préserver et valoriser les identités sétoises, anticiper le changement climatique, rechercher l'excellence environnementale et mieux considérer les risques naturels et les nuisances ", précisant notamment un objectif particulier de " préservation des éléments paysagers remarquables notamment sur le Mont Saint-Clair, entité patrimoniale et emblématique de la ville et de son centre ancien ", il comporte également une orientation III qui vise à " organiser les déplacements pour limiter les nuisances et mettre en valeur la ville ", en précisant notamment l'objectif d'une " amélioration de la hiérarchisation du réseau viaire dans l'optique d'une voirie pour tous : par la poursuite des aménagements viaires structurants à vocation de diffusion des flux sur une trame viaire complétée (prolongement boulevard Grangent, ...) (...) ". La création de l'emplacement réservé n° 29, destiné à la réalisation d'une voie publique permettant de prolonger le boulevard Grangent jusqu'au chemin de la Croix de Marcenac, qui répond directement à l'orientation générale III, ne révèle pas, par suite, une incohérence entre le projet d'aménagement et de développement durables et le règlement du plan local d'urbanisme. Il s'ensuit que le moyen tiré de l'absence de cohérence entre les orientations du projet d'aménagement et de développement durables et les dispositions du règlement créant l'emplacement réservé n° 29 doit être écarté.<br/>
<br/>
              11. D'autre part, il résulte ce qui a été dit au point 3 que Mme A... n'est pas fondée à soutenir que les auteurs du plan local d'urbanisme ont commis une erreur d'appréciation en ne classant pas ces parcelles au nombre des espaces remarquables du littoral de la commune en application des dispositions précitées et, par suite, en créant l'emplacement réservé n° 29 relatif à la création d'une voirie de dix mètres de largeur sur ces terrains.<br/>
<br/>
              12. Enfin, ainsi qu'il a été dit au point 5, les parcelles litigieuses ne sauraient être regardées comme faisant partie des parcs et ensembles boisés les plus significatifs que l'autorité communale était tenue de classer au titre de l'article L. 1301 du code de l'urbanisme.<br/>
<br/>
              13. Il résulte de ce qui précède que la demande présentée par Mme A... devant le tribunal administratif de Montpellier tendant à l'annulation de la délibération du 10 février 2014 par laquelle le conseil municipal de la commune de Sète a approuvé son plan local d'urbanisme en tant qu'il crée l'emplacement réservé n° 29 et en tant qu'il ne classe pas les parcelles cadastrées section BM n° 73 et 89 en espaces boisés classés, ainsi qu'à l'annulation de la décision du 24 avril 2014 rejetant son recours gracieux, doit être rejetée. Par suite, il n'y a pas lieu de statuer sur les conclusions subsidiaires de la commune de Sète tendant à la mise en oeuvre des dispositions de l'article L. 600-9 du code de l'urbanisme.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Sète qui n'est pas, dans la présente instance, la partie perdante. Par ailleurs, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par cette commune sur le même fondement.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 à 5 de l'arrêt de la cour administrative d'appel de Marseille du 20 décembre 2018 sont annulés.<br/>
Article 2 : Les conclusions de Mme A... devant le tribunal administratif de Montpellier tendant à l'annulation de la délibération du 10 février 2014 par laquelle le conseil municipal de la commune de Sète a approuvé son plan local d'urbanisme, en tant qu'il crée l'emplacement réservé n° 29 et en tant qu'il ne classe pas les parcelles cadastrées section BM n° 73 et 89 en espaces boisés classés, ainsi que l'annulation de la décision du 24 avril 2014 rejetant son recours gracieux, sont rejetées.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions subsidiaires de la commune de Sète devant la cour administrative d'appel de Marseille tendant à la mise en oeuvre des dispositions de l'article L. 600-9 du code de l'urbanisme.  <br/>
Article 4 : Les conclusions présentées par Mme A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : Le surplus des conclusions de la commune de Sète est rejeté.<br/>
Article 6 : La présente décision sera notifiée à la commune de Sète et à Madame B... A.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - NOTION D'ESPACE REMARQUABLE (ART. L. 146-6 DU CODE DE L'URBANISME) [RJ1] - PARCELLE FORMANT AVEC UN TEL ESPACE UNE UNITÉ PAYSAGÈRE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-001-01-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. PRESCRIPTIONS D'AMÉNAGEMENT ET D'URBANISME. RÉGIME ISSU DE LA LOI DU 3 JANVIER 1986 SUR LE LITTORAL. - PRÉSERVATION DES ESPACES REMARQUABLES (ART. L. 146-6 DU CODE DE L'URBANISME) - NOTION D'ESPACE REMARQUABLE - PARCELLE FORMANT AVEC UN TEL ESPACE UNE UNITÉ PAYSAGÈRE [RJ2] - CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE DES FAITS [RJ3].
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique des faits sur le point de savoir si une parcelle forme avec un espace remarquable une unité paysagère justifiant dans son ensemble cette qualification de site ou paysage remarquable à préserver pour l'application de l'article L. 146-6 du code de l'urbanisme.</ANA>
<ANA ID="9B"> 68-001-01-02-03 Le juge de cassation exerce un contrôle de qualification juridique des faits sur le point de savoir si une parcelle forme avec un espace remarquable une unité paysagère justifiant dans son ensemble cette qualification de site ou paysage remarquable à préserver pour l'application de l'article L. 146-6 du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 septembre 2009, Commune de Canet-en-Roussillon et Seran, n°s 306298 306468, T. pp. 924-983.,,[RJ2] Cf., sur ce critère d'inclusion d'une parcelle dans un espace remarquable, CE, 30 mai 2018, Commune de Sète, n° 408068, T. pp. 951-952-953.,,[RJ3] Rappr., s'agissant du contrôle sur la qualification d'espace remarquable, CE, 3 septembre 2009, Commune de Canet-en-Roussillon et Seran, n°s 306298 306468, T. pp. 924-983.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
