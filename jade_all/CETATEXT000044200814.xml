<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044200814</ID>
<ANCIEN_ID>JG_L_2021_10_000000446457</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/08/CETATEXT000044200814.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 12/10/2021, 446457, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446457</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SAS CABINET BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446457.20211012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune du Lavandou a demandé au juge des référés du tribunal administratif de Toulon, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution du contrat d'amodiation conclu le 9 mai 2020 entre la société SAUR, la société civile immobilière (SCI) AJC Immobilier et la société à responsabilité limitée (SARL) GB. Par une ordonnance n° 2002776 du 27 octobre 2020, le juge des référés a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 et 27 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la commune du Lavandou demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension de l'exécution du contrat d'amodiation conclu le 9 mai 2020 ;<br/>
<br/>
              3°) de mettre à la charge de la société SAUR la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des propriétés des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune du Lavandou et à la SAS Cabinet Boulloche, avocat de la société SAUR ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Toulon que la commune du Lavandou a conclu en 1985 avec la société Sogea-Balency, aux droits de laquelle est venue la société SAUR, un contrat de concession pour l'exploitation d'une partie des ouvrages de son port de plaisance constituant des dépendances du domaine public maritime, parmi lesquels des locaux à usage commercial. Le cahier des charges de cette concession prévoit de possibles amodiations de longue durée en rapport avec l'utilisation du port, transmissibles à un autre bénéficiaire sous réserve de l'accord du concessionnaire et de l'autorité concédante. Ayant constaté qu'avait été conclu le 9 mai 2020 entre la société SAUR, la société AJC immobilier et la société GB, sans lui avoir été préalablement soumis pour approbation, un contrat prévoyant la transmission à titre onéreux à la société GB de l'amodiation précédemment consentie à la société AJC, la commune du Lavandou a demandé au juge des référés de prononcer la suspension de son exécution sur le fondement de l'article L. 521-1 du code de justice administrative. Elle se pourvoit en cassation contre l'ordonnance du 27 octobre 2020 par laquelle le juge des référés a rejeté sa demande. <br/>
<br/>
              2. Le juge des référés du tribunal administratif de Toulon a estimé que le contrat d'amodiation en cause devait être regardé comme une mesure d'exécution du contrat de concession liant la commune à la société SAUR. Après avoir rappelé que, saisi par la partie à un contrat administratif d'un litige relatif à une mesure d'exécution du contrat autre qu'une résiliation, le juge des référés pouvait seulement rechercher si cette mesure était intervenue dans des conditions de nature à ouvrir droit à indemnité, il en a déduit que ce contrat d'amodiation ne pouvait faire l'objet ni d'une annulation ni, a fortiori, de la suspension demandée par la commune. <br/>
<br/>
              3. En statuant ainsi, alors que la commune était un tiers au contrat d'amodiation en litige et que sa demande tendait en réalité à la mise en œuvre de la possibilité ouverte à tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses de former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat et d'assortir ce recours d'une demande tendant à la suspension de l'exécution de ce dernier sur le fondement de l'article L. 521-1 du code de justice administrative, le juge des référés a commis une erreur de droit. <br/>
<br/>
              4. La commune du Lavandou est, par suite, fondée à demander l'annulation de l'ordonnance qu'elle attaque, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société SAUR une somme de 3 000 euros à verser à la commune du Lavandou au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce que soit mise à la charge de la commune du Lavandou, qui n'est pas la partie perdante dans la présente instance, la somme demandée à ce titre par la société SAUR. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 27 octobre 2020 du juge des référés du tribunal administratif de Toulon est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulon. <br/>
Article 3 : La société SAUR versera à la commune du Lavandou la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la société SAUR tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la commune du Lavandou, à la société SAUR, à la société civile immobilière AJC immobilier et à la société à responsabilité limitée GB.<br/>
              Délibéré à l'issue de la séance du 28 septembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Benoît Bohnert, conseiller d'Etat et Mme Ophélie Champeaux, maître des Requêtes-rapporteure. <br/>
<br/>
<br/>
              Rendu le 12 octobre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		La rapporteure : <br/>
      Signé : Mme Ophélie Champeaux<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
