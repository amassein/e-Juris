<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928824</ID>
<ANCIEN_ID>JG_L_2016_07_000000388077</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928824.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/07/2016, 388077, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388077</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:388077.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Auchan France a demandé au tribunal administratif de Lyon de la décharger des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2009 et 2010 dans les rôles de la commune de Saint-Genis-Laval à raison du magasin dont elle est propriétaire. Par un jugement nos 1007276, 1201416 du 25 novembre 2014, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 février, 24 avril et 12 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Auchan France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Auchan France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la société Auchan France se pourvoit en cassation contre le jugement du 25 novembre 2014 par lequel le tribunal administratif de Lyon a refusé de la décharger des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2009 et 2010 dans les rôles de la commune de Saint-Genis-Laval à raison du magasin dont elle est propriétaire ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes des dispositions du I de l'article 1520 du code général des impôts, applicable aux établissements publics de coopération intercommunale, dans sa rédaction en vigueur pendant les années d'imposition en cause : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal. (...) " ;  qu'en vertu des articles 1521 et 1522 du même code, cette taxe a pour assiette celle de la taxe foncière sur les propriétés bâties ; que la taxe d'enlèvement des ordures ménagères n'a pas le caractère d'un prélèvement opéré sur les contribuables en vue de pourvoir à l'ensemble des dépenses budgétaires mais a exclusivement pour objet de couvrir les dépenses exposées par la commune pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales ; qu'il en résulte que le produit de cette taxe et, par voie de conséquence, son taux, ne doivent pas être manifestement disproportionnés par rapport au montant de ces dépenses, tel qu'il peut être estimé à la date du vote de la délibération fixant ce taux ; que, d'autre part, aux termes de l'article L. 2333-76 du code général des collectivités territoriales : " Les communes, les établissements publics de coopération intercommunale et les syndicats mixtes qui bénéficient de la compétence prévue à l'article L. 2224-13 peuvent instituer une redevance d'enlèvement des ordures ménagères calculée en fonction du service rendu dès lors qu'ils assurent au moins la collecte des déchets des ménages. (...) " ; qu'aux termes de l'article L. 2333-78 du même code : " (...) A compter du 1er janvier 1993, les communes, les établissements publics de coopération intercommunale ainsi que les syndicats mixtes qui n'ont pas institué la redevance prévue à l'article L. 2333-76 créent une redevance spéciale afin d'assurer l'élimination des déchets visés à l'article L. 2224-14. (...) / Elles peuvent décider, par délibération motivée, d'exonérer de la taxe d'enlèvement des ordures ménagères les locaux dont disposent les personnes assujetties à la redevance spéciale visée au premier alinéa " ; que les déchets mentionnés à l'article L. 2224-14 sont les déchets non ménagers que ces collectivités peuvent, eu égard à leurs caractéristiques et aux quantités produites, collecter et traiter sans sujétions techniques particulières ; qu'il résulte de ces dispositions, d'une part, que l'instauration de la redevance spéciale est obligatoire en l'absence de redevance d'enlèvement des ordures ménagères, d'autre part, que la taxe d'enlèvement des ordures ménagères n'a pas pour objet de financer l'élimination des déchets non ménagers, alors même que la redevance spéciale n'aurait pas été instituée ;<br/>
<br/>
              3. Considérant qu'à l'appui de son moyen tiré de ce que le produit de la taxe d'enlèvement des ordures ménagères instituée par la communauté urbaine du Grand Lyon au titre des années en litige et, par voie de conséquence, son taux, étaient manifestement disproportionnés par rapport au montant des dépenses qu'elle avait exclusivement pour objet de couvrir, tel qu'il pouvait être estimé à la date du vote de la délibération fixant ce taux, la société Auchan France, en se prévalant notamment d'extraits du rapport annuel, émanant de la communauté urbaine, relatifs au prix et à la qualité du service public d'élimination des déchets au titre de 2009 et de 2010, a fait valoir que le produit de cette taxe était supérieur à 100 millions d'euros par an, pour un coût global de traitement des déchets supérieur à 110 millions d'euros, que la communauté urbaine percevait des recettes non fiscales de l'ordre de 22 millions d'euros par an couvrant en partie le coût de traitement des déchets, si bien que le montant annuel de la taxe ne pouvait, en tout état de cause, légalement excéder une somme de l'ordre de 90 millions d'euros, que la communauté urbaine n'avait pas institué la redevance spéciale prévue à l'article L. 2333-78 du code général des collectivités territoriales en vue de financer l'élimination de déchets non ménagers, qu'en se fondant sur une moyenne nationale de 20 % estimée par la cour des comptes dans un rapport de 2011, le coût de traitement des déchets ménagers était de l'ordre de 80 % du coût global, soit environ 90 millions d'euros par an  et que dès lors, le montant de la taxe ne pouvait excéder une somme comprise entre 66 et 68 millions d'euros ; <br/>
<br/>
              4. Considérant qu'il appartenait au tribunal administratif de Lyon, dès lors que la société requérante avait ainsi produit, à l'appui de ses demandes de décharge, des éléments de nature à établir l'illégalité de la délibération fixant les taux de la taxe en litige, de rechercher, au besoin en demandant par jugement avant dire droit à la communauté urbaine de produire ses observations sur ces éléments, si  le produit de cette taxe et, par voie de conséquence, son taux, étaient manifestement disproportionnés par rapport au montant des dépenses exposées par la communauté urbaine pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales, tel qu'il pouvait être estimé sur une base annuelle à la date du vote de la délibération fixant ce taux ; que par suite, en jugeant d'une part que la société ne le mettait pas en mesure d'apprécier la portée de son moyen en ne précisant pas, en ce qui concerne l'exception d'illégalité des délibérations du conseil communautaire fixant les taux en litige pour les années 2009 et 2010, de quelles délibérations il s'agissait, quelles étaient leurs dates et quels taux elles fixaient et en regardant d'autre part comme insuffisants les éléments produits devant lui, sans rechercher si le produit de la taxe et, par voie de conséquence, son taux, étaient manifestement disproportionnés par rapport au montant de ces dépenses pour chaque année en litige, le tribunal administratif a commis une erreur de droit et méconnu les règles relatives à la charge de la preuve ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Auchan France est fondée à demander, pour ce motif, l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Auchan France de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              	         D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le jugement du 25 novembre 2014 du tribunal administratif de Lyon est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
<br/>
Article 3 : L'Etat versera à la société Auchan France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Auchan France et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée, pour information, à la communauté urbaine du Grand Lyon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
