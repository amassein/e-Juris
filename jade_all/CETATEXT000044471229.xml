<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044471229</ID>
<ANCIEN_ID>JG_L_2021_12_000000440458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/47/12/CETATEXT000044471229.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 10/12/2021, 440458</TITRE>
<DATE_DEC>2021-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA-PRIGENT-DRUSCH</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440458.20211210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme G... H... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 1er août 2017 du directeur spécialisé des finances publiques pour l'assistance publique - hôpitaux de Paris en tant qu'elle lui interdit d'accéder aux locaux de la direction à compter du 2 août 2017. Par un jugement n° 1715884 du 7 février 2019, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19PA01222 du 5 février 2020, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme H... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 mai et 5 août 2020, ainsi que le 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme H... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -la loi n°83-634 du 13 juillet 1983 ;<br/>
              -la loi n°84-16 du 11 janvier 1984 ;<br/>
              -le décret n°82-447 du 28 mai 1982 ;<br/>
              -le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Melka-Prigent-Drusch, avocat de Mme H... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme H..., contrôleuse principale des finances publiques, s'est rendue le 1er août 2017 à la direction spécialisée des finances publiques (DSFP) pour l'Assistance Publique - Hôpitaux de Paris, où elle était affectée jusqu'à la fin du mois. Par un courrier daté du même jour, le directeur spécialisé des finances publiques, après avoir listé les congés dont Mme H... devait bénéficier au cours du mois d'août 2017, lui a interdit de se présenter dans les locaux de la direction à compter du deuxième jour du même mois et lui a demandé de restituer la clef du local syndical et du panneau d'affichage syndical, ainsi que son badge. Par un jugement du 7 février 2019, le tribunal administratif de Paris a rejeté pour irrecevabilité la demande de Mme H... tendant à l'annulation pour excès de pouvoir de cette décision en tant qu'elle lui interdit d'accéder aux locaux de la DSFP. Mme H... se pourvoit en cassation contre l'arrêt du 5 février 2020 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Les mesures prises à l'égard d'agents publics qui, compte tenu de leurs effets, ne peuvent être regardées comme leur faisant grief, constituent de simples mesures d'ordre intérieur insusceptibles de recours. Il en va ainsi des mesures qui, tout en modifiant leur affectation ou les tâches qu'ils ont à accomplir, ne portent pas atteinte aux droits et prérogatives qu'ils tiennent de leur statut ou à l'exercice de leurs droits et libertés fondamentaux, ni n'emportent perte de responsabilités ou de rémunération. Le recours contre de telles mesures, à moins qu'elles ne traduisent une discrimination ou une sanction, est irrecevable.<br/>
<br/>
              3. Il ressort des énonciations non contestées de l'arrêt attaqué que Mme H... avait la qualité de responsable syndicale au sein de la DSFP et accédait à ce titre au local syndical ainsi qu'au panneau d'affichage syndical. La décision par laquelle le directeur spécialisé des finances publiques a interdit à Mme H... d'accéder aux locaux de la DSFP à compter du 2 août 2017 et lui a demandé de remettre la clef du local syndical et celle du panneau d'affichage syndical porte ainsi atteinte à l'exercice de la liberté syndicale qui est au nombre des droits et libertés fondamentaux. Par suite, elle ne présente pas le caractère d'une mesure d'ordre intérieur mais constitue un acte susceptible de recours. Il résulte de ce qui précède qu'en jugeant que cette décision ne pouvait être regardée comme faisant grief à Mme H... au motif qu'elle était en congé au mois d'août et n'avait ainsi plus vocation à accéder à ces locaux, la cour administrative d'appel de Paris a commis une erreur de droit. Par suite et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, Mme H... est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme H..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 5 février 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à Mme H... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme G... H... et au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 26 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. I... F..., M. Frédéric Aladjidi, présidents de chambre; Mme A... N..., M. D... E..., Mme K... B..., M. L... C..., Mme Catherine Fischer-Hirtz, conseillers d'Etat et M. Lionel Ferreira, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 10 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		Le rapporteur : <br/>
      Signé : M. Lionel Ferreira<br/>
                 La secrétaire :<br/>
                 Signé : Mme J... M...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01-02-01 FONCTIONNAIRES ET AGENTS PUBLICS. - CONTENTIEUX DE LA FONCTION PUBLIQUE. - CONTENTIEUX DE L'ANNULATION. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS SUSCEPTIBLES DE RECOURS. - DÉCISION FAISANT OBSTACLE À L'ACCÈS D'UN RESPONSABLE SYNDICAL AUX LOCAUX PROFESSIONNELS, AU LOCAL SYNDICAL ET AU PANNEAU SYNDICAL - MESURE D'ORDRE INTÉRIEUR - ABSENCE, EU ÉGARD À L'ATTEINTE PORTÉE À L'EXERCICE DE LA LIBERTÉ SYNDICALE [RJ1] ET INDÉPENDAMMENT DE SES EFFETS CONCRETS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-03 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - MESURES D'ORDRE INTÉRIEUR. - ABSENCE, EU ÉGARD À L'ATTEINTE PORTÉE À L'EXERCICE DE LA LIBERTÉ SYNDICALE [RJ1] ET INDÉPENDAMMENT DE SES EFFETS CONCRETS - DÉCISION FAISANT OBSTACLE À L'ACCÈS D'UN RESPONSABLE SYNDICAL AUX LOCAUX PROFESSIONNELS, AU LOCAL SYNDICAL ET AU PANNEAU SYNDICAL.
</SCT>
<ANA ID="9A"> 36-13-01-02-01 La décision par laquelle l'autorité hiérarchique interdit à un responsable syndical d'accéder aux locaux professionnels et lui demande de remettre la clef du local syndical et celle du panneau d'affichage syndical porte atteinte à l'exercice de la liberté syndicale qui est au nombre des droits et libertés fondamentaux de l'intéressé. Par suite, et alors même que ce dernier est en congé au mois d'août et n'a ainsi pas vocation à accéder aux locaux, elle ne présente pas le caractère d'une mesure d'ordre intérieur mais constitue un acte susceptible de recours.</ANA>
<ANA ID="9B"> 54-01-01-02-03 La décision par laquelle l'autorité hiérarchique interdit à un responsable syndical d'accéder aux locaux professionnels et lui demande de remettre la clef du local syndical et celle du panneau d'affichage syndical porte atteinte à l'exercice de la liberté syndicale qui est au nombre des droits et libertés fondamentaux de l'intéressé. Par suite, et alors même que ce dernier est en congé au mois d'août et n'a ainsi pas vocation à accéder aux locaux, elle ne présente pas le caractère d'une mesure d'ordre intérieur mais constitue un acte susceptible de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 25 septembre 2015, Mme Bourjolly, n° 372624, p. 322.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
