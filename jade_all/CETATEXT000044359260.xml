<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359260</ID>
<ANCIEN_ID>JG_L_2021_11_000000430958</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/92/CETATEXT000044359260.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 22/11/2021, 430958, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430958</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; CARBONNIER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:430958.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Beiser Environnement a porté plainte contre Mme Q... B... devant la chambre disciplinaire de première instance d'Alsace de l'ordre des médecins. Par une décision du 7 février 2017, la chambre disciplinaire de première instance a rejeté sa plainte. <br/>
<br/>
              Par une décision du 21 mars 2019, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par la société Beiser Environnement contre cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 22 août 2019 au secrétariat du contentieux du Conseil d'Etat, la société Beiser Environnement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du travail ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Carbonnier, avocat de la société Beiser Environnement et à la SCP Richard, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., médecin spécialiste, qualifiée en médecine du travail, exerçant au sein de l'Association Alsace Santé au Travail (AST 67), a été affectée au centre médical de Bouxwiller dont relève la société Beiser Environnement. Le 17 septembre 2015, la société Beiser Environnement a porté plainte contre Mme B... devant le conseil départemental du Bas-Rhin de l'ordre des médecins, qui a transmis la plainte, sans s'y associer, à la chambre disciplinaire de première instance d'Alsace de l'ordre des médecins, en soutenant notamment qu'elle avait établi des certificats d'inaptitude en méconnaissance des dispositions des articles R. 4127-28 et R. 4127-76 du code de la santé publique et montré à son égard une attitude partiale et tendancieuse. La société Beiser Environnement se pourvoit en cassation contre la décision du 21 mars 2019 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel qu'elle avait formé contre la décision du 7 février 2017 par laquelle la chambre disciplinaire de première instance avait rejeté sa plainte.<br/>
<br/>
              Sur la régularité de la décision attaquée :<br/>
<br/>
              2. En premier lieu, en vertu des dispositions de l'article R. 4126-37 du code de la santé publique, rendues applicables devant la chambre disciplinaire nationale de l'ordre des médecins par l'article R. 4126-43 du même code, la décision de la juridiction ordinale est " rendue publique par affichage ". Dès lors qu'il ressort des mentions de la décision attaquée, qui font foi jusqu'à preuve contraire, que celle-ci a été rendue publique par affichage le 21 mars 2019, la société requérante n'est pas fondée à soutenir que la décision attaquée n'aurait pas été rendue publique. <br/>
<br/>
              3. En deuxième lieu, si, en application des dispositions de l'article R. 4126-17 et R. 4126-18 du code de la santé publique, un des membres composant la chambre disciplinaire est désigné comme rapporteur et peut procéder, dans le cadre et pour les besoins du débat contradictoire entre les parties, à des mesures d'instruction qui ont pour objet de vérifier la pertinence des griefs et observations des parties et dont les résultats sont versés au dossier pour donner lieu à communication contradictoire, de telles attributions ne diffèrent pas de celles que la formation collégiale de jugement pourrait elle-même exercer et ne confèrent pas au rapporteur le pouvoir de décider par lui-même de modifier le champ de la saisine de la juridiction. Ainsi, et alors même qu'il incombe par ailleurs au rapporteur de remettre un exposé des faits consistant en une présentation de l'affaire, l'ensemble de ces dispositions n'a pas pour effet de lui conférer des fonctions qui, au regard du principe d'impartialité comme des autres stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, feraient obstacle à sa participation au délibéré de la chambre disciplinaire. Par suite, la société requérante n'est pas fondée à soutenir que la seule participation du rapporteur au délibéré de l'affaire par la chambre disciplinaire nationale de l'ordre des médecins aurait porté atteinte au principe d'impartialité au sens de ces stipulations.<br/>
<br/>
              4. En troisième lieu, il ressort des termes mêmes de la décision attaquée que la chambre disciplinaire nationale de l'ordre des médecins a d'abord écarté les griefs tirés de la méconnaissance, par Mme B..., de ses obligations déontologiques dans le suivi de MM. P... et S..., puis a estimé qu'il ne ressortait pas de l'instruction que Mme B... avait excédé les limites de ses attributions concernant l'aptitude au travail de plusieurs salariés de l'entreprise requérante et que la circonstance, à la supposer établie, que le nombre des avis d'inaptitude avait augmenté ne saurait suffire à établir un manquement à ses obligations déontologiques. En statuant ainsi, la chambre disciplinaire nationale de l'ordre des médecins, qui n'était pas tenue de se prononcer sur le détail de l'argumentation des parties, a suffisamment motivé sa décision.<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              5. Aux termes de l'article R. 4127-3 du code de la santé publique : " Le médecin doit en toutes circonstances, respecter les principes de moralité, de probité et de dévouement indispensables à l'exercice de la médecine ".  Aux termes de l'article R. 4127-28 du code la santé publique : " La délivrance d'un rapport tendancieux ou d'un certificat de complaisance est interdite ". Aux termes de l'article R. 4127-76 du code de la santé publique : " L'exercice de la médecine comporte normalement l'établissement par le médecin, conformément aux constatations médicales qu'il est en mesure de faire, des certificats, attestations et documents dont la production est prescrite par les textes législatifs et réglementaires ". Ces obligations déontologiques s'imposent aux médecins du travail comme à tout médecin y compris dans l'exercice des missions qui leur sont confiées par les dispositions du titre II du livre VI de la quatrième partie du code du travail. Il appartient toutefois au juge disciplinaire d'apprécier le respect des obligations déontologiques en tenant compte des conditions dans lesquelles le médecin exerce son art et, en particulier, s'agissant des médecins du travail, des missions et prérogatives qui sont les leurs. <br/>
<br/>
              6. En vertu de l'article R. 4624-31 du code du travail dans sa version applicable à l'espèce, le médecin du travail ne peut constater l'inaptitude médicale du salarié à son poste de travail que s'il a réalisé deux examens médicaux de l'intéressé espacés de deux semaines, accompagnés le cas échéant des examens complémentaires. Lorsque le maintien du salarié à son poste de travail entraîne un danger immédiat pour sa santé ou sa sécurité ou celles des tiers ou lorsqu'un examen de pré-reprise a eu lieu dans un délai de trente jours au plus, l'avis d'inaptitude médicale peut être délivré en un seul examen. <br/>
<br/>
              7. D'une part, s'agissant du constat d'inaptitude médicale concernant M. S..., c'est sans commettre d'erreur de droit que la chambre disciplinaire nationale de l'ordre des médecins a jugé que la seule circonstance que Mme B... avait prononcé cette inaptitude après un seul examen médical, sans que son avis ne mentionne l'existence d'un danger immédiat pour la santé ou la sécurité de M. S..., ne suffisait pas à caractériser une méconnaissance, par ce praticien, des obligations déontologiques mentionnées au point 5, alors même que la régularité et le bien-fondé d'un tel avis pourraient être contestés devant le juge compétent. Par ailleurs, c'est par une appréciation souveraine exempte de dénaturation qu'elle a jugé qu'il n'était pas établi que Mme B... n'avait pas procédé à l'étude du poste du salarié et de ses conditions de travail dans l'entreprise. <br/>
<br/>
              8. D'autre part, en retenant qu'il ne résultait pas de l'instruction, s'agissant du certificat d'inaptitude médicale concernant M. P..., que Mme B... se soit basée sur des faits qu'elle n'avait pas elle-même constatés ou ait produit un rapport tendancieux, la chambre disciplinaire nationale ne s'est pas fondée, contrairement à ce que soutient la requérante, sur des circonstances inopérantes. <br/>
<br/>
              9. Enfin, c'est par une appréciation souveraine exempte de dénaturation que la chambre disciplinaire nationale de l'ordre des médecins a retenu qu'il ne résultait pas de l'instruction que Mme B... aurait excédé les limites de ses attributions ou fait montre de partialité ou d'animosité à l'égard de la société Beiser Environnement.<br/>
<br/>
              10. Il résulte de tout ce qui précède que le pourvoi de la société Beiser Environnement, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejeté. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Beiser Environnement une somme de 3 000 euros à verser à Mme B... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Beiser Environnement est rejeté.<br/>
Article 2 : La société Beiser Environnement versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Beiser Environnement et à Mme Q... B.... <br/>
Copie en sera adressée au Conseil national de l'ordre des médecins et à la ministre du travail, de l'emploi et de l'insertion. <br/>
              Délibéré à l'issue de la séance du 22 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; Mme A... R..., Mme G... O..., présidentes de chambre ; M. C... N..., M. L... J..., Mme K... M..., Mme E... I..., M. Damien Botteghi, conseillers d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. T... D...<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
                 La secrétaire :<br/>
                 Signé : Mme F... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
