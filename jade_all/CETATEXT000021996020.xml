<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021996020</ID>
<ANCIEN_ID>JG_L_2010_02_000000329100</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/99/60/CETATEXT000021996020.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 10/02/2010, 329100, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329100</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile  Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:329100.20100210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 22 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Franck A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2008-1356 du 19 décembre 2008 relatif au relèvement de certains seuils du code des marchés publics en tant qu'il modifie l'article 28 du code des marchés publics, ensemble la décision implicite par laquelle le Premier ministre a rejeté sa demande d'abrogation ; <br/>
<br/>
              2°) de mettre la somme de 3 000 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, Auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la fin de non recevoir opposée à la requête : <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que M. A a, par lettre du 18 février 2009 reçue le 20 février 2009, demandé au Premier ministre l'abrogation du décret susvisé du 19 décembre 2008 publié au journal officiel de la République française le 20 décembre 2008 ; que le Premier ministre ayant omis de notifier à M. A un accusé de réception indiquant notamment les voies et délais de recours contre une éventuelle décision explicite ou implicite de rejet, le délai de recours n'a pas commencé à courir ; que, par suite, le recours formé le 22 juin 2009 devant le Conseil d'Etat par M. A n'est pas tardif ; que dès lors la fin de non recevoir soulevée par le ministre tirée de la tardiveté de la requête ne peut qu'être écartée ; <br/>
<br/>
              Sur les conclusions dirigées contre le décret du 19 décembre 2008 :<br/>
<br/>
              Considérant qu'aux termes du quatrième alinéa de l'article 28 du code des marchés publics dans sa rédaction antérieure à celle du décret du 19 décembre 2008 susvisé : "(...) Le pouvoir adjudicateur peut décider que le marché sera passé sans publicité ni concurrence préalable si les circonstances le justifient, ou si son montant estimé est inférieur à 4 000 euros HT..." ; que selon l'article 1er du décret du 19 décembre 2008 : "(...) au quatrième alinéa de l'article 28 (...) les mots "4 000 euros HT" sont remplacés par les mots "20 000 euros HT" ; <br/>
<br/>
              Considérant que M. A justifie, en sa qualité d'avocat ayant vocation à passer des marchés de prestation de service avec des collectivités territoriales ou de conseil de ces mêmes collectivités, d'un intérêt lui donnant qualité pour demander l'annulation de la disposition attaquée en tant qu'elle modifie l'article 28 du code des marchés publics dès lors que l'article 30 du même code relatif notamment aux marchés de prestations juridiques prévoit que les marchés relevant de cet article peuvent être passés selon une procédure adaptée dans les conditions prévues par l'article 28 ;  <br/>
<br/>
              Considérant que les marchés passés en application du code des marchés publics sont soumis aux principes qui découlent de l'exigence d'égal accès à la commande publique et qui sont rappelés par le II de l'article 1er du code des marchés publics dans sa rédaction issue du décret du 1er août 2006 selon lequel: "Les marchés publics et les accords-cadres (...) respectent les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures (...)" ; que ces principes ne font pas obstacle à ce que le pouvoir réglementaire puisse permettre au pouvoir adjudicateur de décider que le marché sera passé sans publicité, voire sans mise en concurrence, dans les seuls cas où il apparaît que de telles formalités sont impossibles ou manifestement inutiles notamment en raison de l'objet du marché, de son montant ou du degré de concurrence dans le secteur considéré ; que, par suite, en relevant de 4 000 à 20 000 euros, de manière générale, le montant en deçà duquel tous les marchés entrant dans le champ de l'article 28 du code des marchés publics sont dispensés de toute publicité et mise en concurrence, le pouvoir réglementaire a méconnu les principes d'égalité d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures ; que par suite M. A est fondé à demander l'annulation du décret attaqué  en tant qu'il relève le seuil applicable aux marchés passés selon la procédure de l'article 28 du code des marchés publics ;<br/>
<br/>
              Sur les conséquences de l'illégalité du décret annulé : <br/>
<br/>
              Considérant que l'annulation d'un acte administratif implique en principe que cet acte est réputé n'être jamais intervenu ; que toutefois, l'annulation rétroactive de l'article 1er du décret du 19 décembre 2008 en tant qu'il prévoit le relèvement du seuil des marchés susceptibles d'être passés sur le fondement de l'article 28 du code des marchés publics porterait eu égard au grand nombre de contrats en cause et à leur nature une atteinte manifestement excessive à la sécurité juridique ; que, dès lors, il y a lieu, dans les circonstances de l'espèce, de ne prononcer l'annulation des dispositions du décret attaqué qu'à compter du 1er mai 2010 sous réserve des actions engagées à la date de la présente décision contre les actes pris sur leur fondement ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros que M. A demande au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur leur fondement, le décret du 19 décembre 2008 relatif au relèvement de certains seuils du code des marchés publics est annulé, en tant qu'il relève le seuil applicable aux marchés passés selon la procédure adaptée fixée à l'article 28 du même code, à compter du 1er mai 2010. <br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de M. A est rejeté. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A, au Premier ministre et au ministre de l'économie, de l'industrie et de l'emploi. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. - PRINCIPES DE LA COMMANDE PUBLIQUE - VIOLATION - EXISTENCE - DÉCRET RELEVANT DE MANIÈRE GÉNÉRALE LE SEUIL EN DEÇÀ DUQUEL TOUS LES MARCHÉS ENTRANT DANS LE CHAMP DE L'ARTICLE 29 DU CODE DES MARCHÉS PUBLICS SONT DISPENSÉS DE TOUTE PUBLICITÉ ET MISE EN CONCURRENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. MARCHÉS. - PROCÉDURE ADAPTÉE DE L'ARTICLE 28 DU CODE DES MARCHÉS PUBLICS - PRINCIPES DE LA COMMANDE PUBLIQUE - VIOLATION - EXISTENCE - DÉCRET RELEVANT DE MANIÈRE GÉNÉRALE LE SEUIL EN DEÇÀ DUQUEL TOUS LES MARCHÉS ENTRANT DANS LE CHAMP DE L'ARTICLE 29 DU CODE DES MARCHÉS PUBLICS SONT DISPENSÉS DE TOUTE PUBLICITÉ ET MISE EN CONCURRENCE.
</SCT>
<ANA ID="9A"> 01-04 Les principes de la commande publique, rappelés à l'article 1er du code des marchés publics, ne font pas obstacle à ce que le pouvoir réglementaire puisse permettre au pouvoir adjudicateur de décider que le marché sera passé sans publicité, voire sans mise en concurrence, s'il apparaît que de telles formalités sont impossibles ou manifestement inutiles notamment en raison de l'objet du marché, de son montant ou du degré de concurrence dans le secteur considéré. Cependant, en relevant, de manière générale, de 4 000 à 20 000 &#128; le seuil en deçà duquel tous les marchés entrant dans le champ de l'article 29 du code des marchés publics  sont dispensés de toute publicité et mise en concurrence, le pouvoir réglementaire a méconnu les principes d'égalité d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures.</ANA>
<ANA ID="9B"> 39-01-03-02 Les principes de la commande publique, rappelés à l'article 1er du code des marchés publics, ne font pas obstacle à ce que le pouvoir réglementaire puisse permettre au pouvoir adjudicateur de décider que le marché sera passé sans publicité, voire sans mise en concurrence, s'il apparaît que de telles formalités sont impossibles ou manifestement inutiles notamment en raison de l'objet du marché, de son montant ou du degré de concurrence dans le secteur considéré. Cependant, en relevant, de manière générale, de 4 000 à 20 000 &#128; le seuil en deçà duquel tous les marchés entrant dans le champ de l'article 29 du code des marchés publics sont dispensés de toute publicité et mise en concurrence, le pouvoir réglementaire a méconnu les principes d'égalité d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
