<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041775176</ID>
<ANCIEN_ID>JG_L_2020_03_000000432717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/77/51/CETATEXT000041775176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 25/03/2020, 432717</TITRE>
<DATE_DEC>2020-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432717.20200325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Cergy-Pontoise de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2017 dans les rôles de la commune de Vanves (Hauts-de-Seine), à raison d'un ensemble immobilier composé de trois bâtiments situés 25 et 27 rue Barbès. Par une ordonnance n° 1806403 du 17 mai 2019, le président de la cinquième chambre de ce tribunal a donné acte du désistement de cette demande en application des dispositions du second alinéa de l'article R. 611-8-1 du code de justice administrative. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 juillet et 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme A... se pourvoit en cassation contre l'ordonnance du 17 mai 2019 par laquelle le président de la cinquième chambre du tribunal administratif de Cergy-Pontoise lui a donné acte, en application des dispositions du second alinéa de l'article R. 611-8-1 du code de justice administrative, du désistement de sa demande tendant à ce que soit prononcée la décharge des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2017 dans les rôles de la commune de Vanves (Hauts-de-Seine), à raison d'un ensemble immobilier composé de trois bâtiments situés 25 et 27 rue Barbès.<br/>
<br/>
              2. Aux termes de l'article R. 611-8-1 du code de justice administrative : " Le président de la formation de jugement ou le président de la chambre chargée de l'instruction peut demander à l'une des parties de reprendre, dans un mémoire récapitulatif, les conclusions et moyens précédemment présentés dans le cadre de l'instance en cours, en l'informant que, si elle donne suite à cette invitation, les conclusions et moyens non repris seront réputés abandonnés. (...) / Le président de la formation de jugement ou le président de la chambre chargée de l'instruction peut en outre fixer un délai, qui ne peut être inférieur à un mois, à l'issue duquel, à défaut d'avoir produit le mémoire récapitulatif mentionné à l'alinéa précédent, la partie est réputée s'être désistée de sa requête ou de ses conclusions incidentes. La demande de production d'un mémoire récapitulatif informe la partie des conséquences du non-respect du délai fixé. ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis au tribunal administratif de Cergy-Pontoise que, par un courrier du 2 avril 2019, le président de la cinquième chambre de ce tribunal a demandé à Mme A..., sur le fondement du second alinéa précité de l'article R. 611-8-1 du code de justice administrative, de produire, dans un délai de quarante jours, un mémoire récapitulatif, en précisant, d'une part, que les conclusions et les moyens qui ne seraient pas repris dans ce mémoire seraient réputés abandonnés et, d'autre part, qu'à défaut de production de ce mémoire dans le délai imparti, elle serait réputée s'être désistée de sa requête. Il ressort toutefois du bordereau de suivi du pli recommandé contenant ce courrier produit par Mme A... que si ce pli a été présenté au domicile de la requérante le 3 avril 2019, elle ne l'a retiré au bureau de poste que le 16 avril suivant. Dans ces conditions, le délai de quarante jours qui lui était imparti pour produire un mémoire récapitulatif n'était pas expiré à la date à laquelle, le 17 mai 2019, le tribunal administratif lui a donné acte de son désistement. Elle est, par suite, fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 17 mai 2019 du président de la cinquième chambre du tribunal administratif de Cergy-Pontoise est annulée.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Cergy-Pontoise. <br/>
Article 3 : L'Etat versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Marie-France A... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01-03 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. PRODUCTION ORDONNÉE. - PRODUCTION D'UN MÉMOIRE RÉCAPITULATIF SOUS PEINE DE DÉSISTEMENT D'OFFICE (ART. R. 611-8-1 DU CJA) - POINT DE DÉPART DU DÉLAI - DATE DE RETRAIT DU PLI RECOMMANDÉ CONTENANT LA DEMANDE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-04-03 PROCÉDURE. INCIDENTS. DÉSISTEMENT. DÉSISTEMENT D'OFFICE. - DÉSISTEMENT D'OFFICE D'UN REQUÉRANT N'AYANT PAS PRODUIT DE MÉMOIRE RÉCAPITULATIF À L'EXPIRATION DU DÉLAI QUI LUI ÉTAIT IMPARTI (ART. R. 611-8-1 DU CJA) - POINT DE DÉPART DU DÉLAI - DATE DE RETRAIT DU PLI RECOMMANDÉ CONTENANT LA DEMANDE [RJ1].
</SCT>
<ANA ID="9A"> 54-04-01-03 Le délai fixé par le juge pour la production, sous peine de désistement d'office, d'un mémoire récapitulatif en application de l'article R. 611-8-1 du code de justice administrative (CJA) court, lorsque l'intéressé a retiré le pli recommandé contenant la demande dans le délai de conservation au guichet postal, à compter de la date de ce retrait.</ANA>
<ANA ID="9B"> 54-05-04-03 Le délai fixé par le juge pour la production, sous peine de désistement d'office, d'un mémoire récapitulatif en application de l'article R. 611-8-1 du code de justice administrative (CJA) court, lorsque l'intéressé a retiré le pli recommandé contenant la demande dans le délai de conservation au guichet postal, à compter de la date de ce retrait.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du délai de recours contentieux, CE, 2 mai 1980,,, n° 18391, T. p. 831.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
