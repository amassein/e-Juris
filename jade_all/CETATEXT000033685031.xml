<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033685031</ID>
<ANCIEN_ID>JG_L_2016_12_000000388086</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/50/CETATEXT000033685031.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 23/12/2016, 388086, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388086</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:388086.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Mme A...B...a demandé au tribunal départemental des pensions de l'Hérault d'annuler la décision du 6 juillet 2010 par laquelle le ministre de la défense a rejeté sa demande tendant au rétablissement d'une pension de réversion au titre du code des pensions militaires d'invalidité et des victimes de la guerre. Par un jugement n° 11/00002 du 13 décembre 2011, ce tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 12/00025 du 2 juillet 2014, la cour régionale des pensions de Montpellier a, sur appel de MmeB..., annulé ce jugement et fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14/00040 du 3 juin 2015, la cour régionale des pensions de Montpellier a fait droit à la requête présentée par Mme B...tendant à la rectification pour erreur matérielle de l'arrêt du 2 juillet 2014 et fixé les arrérages de sa pension au 1er janvier 2007. <br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              1° Sous le n° 388086, par un pourvoi, enregistré le 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 2 juillet 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 389171, par une requête, enregistrée le 2 avril 2015 au secrétariat de la section du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de prononcer une astreinte sur le fondement de l'article L. 911-5 du code de justice administrative à l'encontre de l'Etat en vue d'assurer l'exécution de l'arrêt du 2 juillet 2014 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un acte, enregistré le 9 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...déclare se désister de sa requête.<br/>
<br/>
<br/>
              3° Sous le n° 392243, par un pourvoi, enregistré le 31 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 3 juin 2015 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de MmeB....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur le pourvoi n° 388086 :<br/>
<br/>
              2. Par un arrêt du 2 juillet 2014, la cour régionale des pensions de Montpellier a reconnu à Mme B...le droit à une pension de réversion en application du code des pensions militaires d'invalidité et des victimes de la guerre. Parallèlement à l'introduction de son pourvoi contre cet arrêt, Mme B...a saisi la cour d'un recours en rectification d'erreur matérielle en raison de l'omission de cette cour de statuer sur ses conclusions tendant à ce que le point de départ du versement des arrérages de sa pension soit fixé au 1er janvier 1999. Par un arrêt du 3 juin 2015, postérieur à l'introduction du pourvoi, cette cour, statuant sur ces conclusions, y a partiellement fait droit. Dès lors, il n'y a pas lieu de statuer sur le pourvoi de Mme B...tendant à l'annulation de l'arrêt du 2 juillet 2014, en tant qu'il a omis de statuer sur ces conclusions. <br/>
<br/>
              Sur la requête n° 389171 :<br/>
<br/>
              3. Le désistement de Mme B...est pur et simple. Rien ne s'oppose à ce qu'il en soit donné acte.<br/>
<br/>
              Sur le pourvoi n° 392243 : <br/>
<br/>
              4. Aux termes des dispositions de l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre : " Ont droit à pension : 2° Les conjoints survivants des militaires et marins dont la mort a été causée par des maladies contractées ou aggravées par suite de fatigues, dangers ou accidents survenus par le fait ou à l'occasion du service (...) ". Aux termes de l'article 48 du même code : " Les conjoints survivants qui contractent un nouveau mariage, un nouveau pacte civil de solidarité ou vivent en état de concubinage notoire perdent leur droit à pension. (...) Le conjoint survivant remarié ou ayant conclu un pacte civil de solidarité redevenu veuf, divorcé, séparé de corps ou dont le nouveau pacte civil de solidarité a pris fin, ainsi que celui qui cesse de vivre en état de concubinage notoire peut, s'il le désire recouvrer leur droit à pension et demander qu'il soit mis fin à l'application qui a pu être faite des dispositions du deuxième alinéa ci-dessus ". Aux termes de l'article L. 6 du même code : " La pension prévue par le présent code est attribuée sur demande de l'intéressé après examen, à son initiative, par une commission de réforme selon des modalités fixées par décret en Conseil d'Etat. / L'entrée en jouissance est fixée à la date du dépôt de la demande ". Aux termes de l'article 108 du même code : " Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la troisième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages, afférents à l'année au cours de laquelle la demande a été déposée et aux trois années antérieures ". Aux termes de l'article L. 24 du même code, alors applicable " Les pensions militaires prévues par le présent code sont liquidées et concédées, sous réserve de la confirmation ou modification prévues à l'alinéa ci-après, par le ministre des anciens combattants et victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet. Les décisions de rejet des demandes de pension sont prises dans la même forme ". Aux termes de l'article L. 25 du même code : " La notification des décisions prises en vertu de l'article L. 24, premier alinéa, du présent code, doit mentionner que le délai de recours contentieux court à partir de cette notification et que les décisions confirmatives à intervenir n'ouvrent pas de nouveau délai de recours ". Il résulte de ces dispositions que le conjoint survivant titulaire d'une pension de réversion qui a perdu son droit à pension du fait de son remariage a droit, en cas de décès de son nouveau conjoint, au rétablissement de sa pension de réversion du chef de son premier conjoint décédé à compter de la date de sa demande. Si, du fait personnel de l'intéressé, la demande de rétablissement de la pension de réversion est déposée postérieurement à l'expiration de la troisième année qui suit celle du décès de son nouveau conjoint, il ne peut prétendre qu'aux arrérages correspondant à l'année au cours de laquelle la demande a été déposée et aux trois années antérieures. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que Mme B...s'est vue attribuer une pension de réversion, au titre du code des pensions militaires d'invalidité et des victimes de la guerre, du chef de son mari décédé le 20 octobre 1954 d'une maladie contractée en service. Du fait de son remariage, le 6 janvier 1960, le versement de sa pension de réversion a été interrompu. Postérieurement au décès de son second époux, intervenu le 11 janvier 1987, Mme B...a demandé au ministre de la défense le rétablissement à son profit du service de la pension de réversion dont elle bénéficiait du chef de son premier époux. Le ministre de la défense a rejeté cette demande par une décision du 6 juillet 2010. Par un arrêt du 2 juillet 2014 devenu définitif sur ce point, la cour régionale des pensions de Montpellier a reconnu à MmeB..., comme il a été dit, le droit au rétablissement de sa pension de réversion, en application de l'article L. 48 du code des pensions militaires d'invalidité et des victimes de la guerre. Par un arrêt du 3 juin 2015, rendu sur le recours en rectification d'erreur matérielle formé par MmeB..., cette même cour, statuant sur les conclusions de l'intéressée tendant à ce que le point de départ des arrérages de sa pension soit fixé conformément aux dispositions de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre, a fixé ce point de départ au 1er janvier 2007. <br/>
<br/>
              6. Il ressort toutefois des pièces du dossier soumis aux juges du fond que si la décision de rejet opposée à Mme B...par le ministre de la défense le 6 juillet 2010 statuait sur une demande présentée par l'intéressée le 19 juin 2008, celle-ci avait initialement formé sa demande par lettre du 8 avril 2002, reçue par le ministre de la défense le 26 avril 2002. Cette demande, qui n'a pas fait l'objet d'une décision de rejet notifiée dans les conditions prévues à l'article L. 25 du code des pensions militaires d'invalidité et des victimes de la guerre, doit être regardée comme celle à partir de laquelle sont appliquées les dispositions des articles L. 6 et L. 108 du même code. Il s'ensuit qu'en fixant au 1er janvier 2007 le point de départ des arrérages de la pension de réversion de la requérante au motif que celle-ci ne pouvait se prévaloir d'une demande de rétablissement de sa pension antérieure à 2008, la cour régionale des pensions de Montpellier a entaché son arrêt d'erreur de droit. Mme B...est fondée, en conséquence, à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il fixe le point de départ des arrérages de sa pension de réversion.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Mme B...ayant demandé par une lettre du 8 avril 2002, reçue le 26 avril 2002, le rétablissement de sa pension, elle peut prétendre, en application des principes rappelés au point 4, aux arrérages de sa pension à compter du 1er janvier 1999.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Boutet-Hourdeaux, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à la SCP Boutet-Hourdeaux de la somme de 2 500 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Il est donné acte du désistement du pourvoi n° 389171 de MmeB....<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur le pourvoi n° 388086 de MmeB....<br/>
<br/>
Article 3 : L'arrêt de la cour régionale des pensions de Montpellier du 3 juin 2015 est annulé.<br/>
<br/>
Article 4 : L'Etat versera à MmeB..., conformément aux motifs de la présente décision, les arrérages correspondant à sa pension de réversion à compter du 1er janvier 1999.<br/>
<br/>
Article 5 : L'Etat versera à la SCP Boutet-Hourdeaux, avocat de MmeB..., une somme de 2 500 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat au titre de l'aide juridictionnelle.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A...B...et au ministre de la défense.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
