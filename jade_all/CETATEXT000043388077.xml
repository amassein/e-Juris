<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043388077</ID>
<ANCIEN_ID>JG_L_2021_04_000000450956</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/38/80/CETATEXT000043388077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 01/04/2021, 450956, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450956</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450956.20210401</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 et 29 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2021-296 du 19 mars 2021 modifiant le décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire en tant qu'il s'applique aux personnes vaccinées, celles-ci n'entrant pas dans la liste des exceptions permettant de déroger a` l'obligation de rester chez soi ; <br/>
<br/>
              2°) d'enjoindre au premier ministre d'abroger cet article en tant qu'il s'applique aux personnes vaccinées, celles-ci n'entrant pas dans la liste des exceptions permettant de déroger a` l'obligation de rester chez soi ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que les dispositions contestées, qui instituent un principe d'interdiction de déplacement hors de sa résidence pour tous les habitants de la région Ile-de-France, ont les mêmes effets qu'une assignation à résidence et restreignent considérablement ses déplacements, portant ainsi atteinte à sa liberté d'aller et venir ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir ; <br/>
              - les dispositions litigieuses sont disproportionnées en ce qu'elles s'appliquent de manière générale sans distinction entre les personnes vaccinées ou non, alors même que de nombreuses études scientifiques démontrent que les vaccins sont pleinement efficaces et que, partant, les personnes vaccinées présentent des risques d'hospitalisation et de transmission du virus moins considérables. <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 28 et 30 mars 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 30 mars 2021, présenté par le ministre des solidarités et de la santé, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 31 mars 2021, présenté par M. Benhebri, qui maintient ses conclusions et ses moyens ; <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 31 mars 2021, présenté par M. Benhebri ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret 2021-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-296 du 19 mars 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. Benhebri, et d'autre part, le ministre des solidarités et de la santé et le Premier ministre ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 30 mars 2021, à 9 heures : <br/>
<br/>
              - Me de Chaisemartin, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. Benhebri ;<br/>
<br/>
              - les représentants de M. Benhebri ;<br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 1er avril 2021 à 12 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. La nouvelle progression de l'épidémie de Covid-19 à l'automne 2020 en France a conduit le président de la République à déclarer, par décret du 14 octobre 2020 pris sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, l'état d'urgence sanitaire à compter du 17 octobre 2020, sur l'ensemble du territoire national. L'état d'urgence a été successivement prorogé en dernier lieu jusqu'au 1er juin 2021. Sur le fondement de l'article L. 3131-15 du code de la santé publique, le premier ministre a pris le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'urgence sanitaire. Par décret du 19 mars 2021, l'article 4 du décret du 29 octobre 2020 a été modifié, de façon à ce que dans les départements mentionnés à l'annexe II de ce décret, les déplacements de personnes hors de leur lieu de résidence soient limités d'une part entre six heures et dix-neuf heures (" confinement "), d'autre part entre dix-neuf heures et six heures (" couvre-feu "), hormis pour les motifs que prévoient ces dispositions. M. Benhebri, qui réside dans l'un des départements concernés par cette mesure, demande au juge des référés, en application de l'article L. 521-2, de suspendre ces obligations pour toute personne ayant été vaccinée contre la Covid-19.<br/>
<br/>
              3. Les mesures en cause ont été motivées, ainsi que le ministère de la santé a pu le confirmer à l'audience, par la nécessité de freiner la diffusion du virus, que favorisent les contacts interpersonnels, en limitant ceux-ci le plus possible, et celle de protéger la population, notamment les personnes les plus vulnérables à raison de leur âge, aussi longtemps que des mesures de prévention ou de soin ne sont pas disponibles, contre le risque de contracter des formes graves de la maladie, qui, d'une part, sont d'une morbidité élevée pour cette catégorie, et d'autre part, sollicitent à l'excès les capacités de soins hospitaliers, au risque d'en réduire la disponibilité pour d'autres catégories de malades.<br/>
<br/>
              4. La reprise de la diffusion de l'épidémie s'est traduite par une aggravation significative sur l'ensemble du territoire national de la diffusion du virus, sollicitant de manière accrue les capacités hospitalières en raison d'un nombre élevé de personnes souffrant de la maladie et notamment de ses formes les plus graves et conduisant les pouvoirs publics à annoncer la généralisation des mesures jusqu'à présent imposées à un nombre limité de départements.<br/>
<br/>
              5. Il est toutefois soutenu par M. Benhebri que la vaccination contre la Covid-19 permet d'atteindre les objectifs que s'assignent les mesures de restriction des déplacements, et que dès lors, le couvre-feu et le confinement ne sont plus nécessaires, ni adaptés, en ce qui concerne les personnes vaccinées.<br/>
<br/>
              6. Il ressort de l'ensemble de la procédure que pour efficace que soit la vaccination, qui ne concerne encore qu'une faible fraction des personnes les plus vulnérables, elle n'élimine pas complètement la possibilité que les personnes vaccinées demeurent porteuses du virus. Si une étude américaine produite en délibéré semble indiquer que le nombre en serait faible, elle ne suffit pas à ce stade à démontrer, au regard de l'accélération de l'épidémie, que seul le respect des gestes barrières par les personnes concernées suffirait à limiter suffisamment la participation à la circulation du virus de celles d'entre elles qui en seraient porteuses, contribuant dès lors à aggraver le risque pour les personnes les plus vulnérables non encore vaccinées qui demeurent majoritaires, même si elles sont désormais moins nombreuses dans les services hospitaliers. A la date de la présente ordonnance, l'effet de la vaccination en matière de réduction de la circulation du virus n'est atteint, dans certains pays, comme l'a relevé le conseil scientifique créé en application de l'article L. 3131-19 du code de la santé publique dans son avis du 11 mars, que par un niveau suffisant de vaccination au sein de l'ensemble de la population. <br/>
<br/>
              7. Au regard de l'ensemble de ces éléments, s'il est vraisemblable, en l'état, que la vaccination assure une protection efficace des bénéficiaires, même si l'impact des évolutions de l'épidémie dues aux variants demeure incertain, les personnes vaccinées peuvent cependant demeurer porteuses du virus et ainsi contribuer à la diffusion de l'épidémie dans une mesure à ce stade difficile à quantifier, ce qui ne permet donc pas d'affirmer que seule la pratique des gestes barrières limiterait suffisamment ce risque. En conséquence, l'atteinte à la liberté individuelle résultant des mesures de couvre-feu et de confinement ne peut, en l'état, au regard des objectifs poursuivis, être regardée comme disproportionnée, en tant qu'elles s'appliquent aux personnes vaccinées. Dès lors, M. Benhebri n'est pas fondé à soutenir que les dispositions qu'il critique portent une atteinte manifestement illégale aux droits et libertés de nature à justifier que le juge des référés du Conseil d'Etat fasse usage des pouvoirs qu'il tient de l'article L. 521-2. Il s'ensuit que les conclusions de sa requête ne peuvent qu'être rejetées, y compris en tant qu'elles tendent à ce que l'Etat verse une somme d'argent à M. Benhebri sur le fondement de l'article L. 761-1 du code de justice administrative, qui, dès lors que l'Etat n'est pas la partie perdante, y font obstacle.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. Benhebri est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. Ahmed Benhebri et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
