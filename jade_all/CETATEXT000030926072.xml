<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926072</ID>
<ANCIEN_ID>JG_L_2015_07_000000375129</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926072.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 22/07/2015, 375129, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375129</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP LYON-CAEN, THIRIEZ ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375129.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Lillers a demandé au tribunal administratif de Lille d'annuler les arrêtés des 7 et 23 décembre 2005 par lesquels le préfet du Pas-de-Calais a procédé à la répartition de l'actif et du passif de la zone d'activité concertée de l'Université en conséquence de son retrait de la communauté de communes du Béthunois. Par un jugement n° 0601167 du 7 décembre 2010, le tribunal administratif de Lille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11DA00185 du 3 décembre 2013, la cour administrative d'appel de Douai a rejeté l'appel formé par la commune de Lillers contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 février et 5 mai 2014 et 1er avril 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Lillers demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de lui allouer la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la commune de Lillers, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société communaute de communes de Noeux et environs et à la SCP Lyon-Caen, Thiriez, avocat de la société communaute de communes Artois-Lys ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par délibération du 19 octobre 1992, le comité syndical du SIVOM de la communauté du Béthunois a décidé d'aménager un site universitaire, situé sur les communes de Béthune et de Beuvry, sous la forme d'une zone d'aménagement concertée, dénommée ZAC de l'Université ; que, le 22 novembre 1992, la communauté de communes du Béthunois s'est substituée au SIVOM de la communauté du Béthunois ; que, par arrêté du 21 décembre 2001, le préfet du Pas-de-Calais a autorisé le retrait de la commune de Lillers de la communauté de communes du Béthunois et son adhésion, à compter du 1er janvier 2002, à la communauté de communes Artois-Lys ; que, par des arrêtés des 7 et 23 décembre 2005, le préfet du Pas-de-Calais a réparti l'actif et le passif de l'opération de la ZAC de l'Université en conséquence du retrait de la commune de Lillers de la communauté de communes du Béthunois et de l'adhésion de cette commune à la communauté de communes Artois-Lys ; que, par un jugement du 7 décembre 2010, le tribunal administratif de Lille a rejeté la demande de la commune de Lillers tendant à l'annulation des arrêtés des 7 et 23 décembre 2005 ; que cette commune se pourvoit en cassation contre l'arrêt du 3 décembre 2013 par lequel la cour administrative d'appel de Douai a rejeté son appel formé contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 5214-26 du code général des collectivités territoriales, dans sa rédaction applicable au litige : " Par dérogation à l'article L. 5211-19, une commune peut être autorisée, par le représentant de l'Etat dans le département après avis de la commission départementale de la coopération intercommunale réunie dans la formation prévue au second alinéa de l'article L. 5211-45, à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale à fiscalité propre dont le conseil communautaire a accepté la demande d'adhésion. L'avis de la commission départementale de la coopération intercommunale est réputé négatif s'il n'a pas été rendu à l'issue d'un délai de deux mois. / Ce retrait s'effectue dans les conditions fixées par l'article L. 5211-25-1. (...) " ; qu'aux termes de l'article L. 5211-25-1 du même code : " En cas de retrait de la compétence transférée à un établissement public de coopération intercommunale : / 1° Les biens meubles et immeubles mis à la disposition de l'établissement bénéficiaire du transfert de compétences sont restitués aux communes antérieurement compétentes et réintégrés dans leur patrimoine pour leur valeur nette comptable, avec les adjonctions effectuées sur ces biens liquidées sur les mêmes bases. Le solde de l'encours de la dette transférée afférente à ces biens est également restituée à la commune propriétaire ; / 2° Les biens meubles et immeubles acquis ou réalisés postérieurement au transfert de compétences sont répartis entre les communes qui reprennent la compétence ou entre la commune qui se retire de l'établissement public de coopération intercommunale et l'établissement ou, dans le cas particulier d'un syndicat dont les statuts le permettent, entre la commune qui reprend la compétence et le syndicat de communes. Il en va de même pour le produit de la réalisation de tels biens, intervenant à cette occasion. Le solde de l'encours de la dette contractée postérieurement au transfert de compétences est réparti dans les mêmes conditions entre les communes qui reprennent la compétence ou entre la commune qui se retire et l'établissement public de coopération intercommunale ou, le cas échéant, entre la commune et le syndicat de communes. A défaut d'accord entre l'organe délibérant de l'établissement public de coopération intercommunale et les conseils municipaux des communes concernés, cette répartition est fixée par arrêté du ou des représentants de l'Etat dans le ou les départements concernés. (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le représentant de l'Etat dans le département ne peut procéder, en cas de retrait d'une ou plusieurs communes d'un établissement public de coopération intercommunale, à la répartition par arrêté des biens de cet établissement public, ainsi que du solde de l'encours de la dette qu'il a contractée, entre cet établissement public et la ou les communes l'ayant quitté qu'en l'absence d'accord intervenu entre l'organe délibérant de cet établissement public et les conseils municipaux des communes l'ayant quitté pour régler les conséquences financières de ce retrait ; que, dès lors, la cour administrative d'appel a commis une erreur de droit en jugeant que le préfet du Pas-de-Calais avait pu, en application de ces dispositions, fixer par arrêtés des 7 et 23 décembre 2005 la répartition de l'actif et du passif relatifs à la zone d'activité concertée de l'Université entre la communauté de communes du Béthunois et la commune de Lillers au motif qu'à la date de ces arrêtés la communauté de communes de Noeux et Environs, continuatrice de la communauté de communes du Béthunois, et la communauté de communes Artois-Lys n'avaient pas trouvé d'accord quant aux conséquences financières du retrait de la commune de Lillers de la communauté de communes du Béthunois et de son adhésion à la communauté de communes Artois-Lys ; que, par suite, la commune de Lillers est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par la commune de Lillers, qui ne sont dirigées contre aucune des parties à l'instance ; qu'elles font également obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Lillers qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 3 décembre 2013 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : Les conclusions de la communauté d'agglomération Artois Comm et de la communauté de communes Artois-Lys présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Lillers, au ministre de l'intérieur, à la communauté d'agglomération Artois Comm et à la communauté de communes Artois-Lys.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
