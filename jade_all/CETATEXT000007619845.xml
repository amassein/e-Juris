<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007619845</ID>
<ANCIEN_ID>JFXLX1986X03X0000047927</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/61/98/CETATEXT000007619845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 9 / 8 SSR, du 5 mars 1986, 47927, inédit au recueil Lebon</TITRE>
<DATE_DEC>1986-03-05</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>47927</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9 / 8 SSR</FORMATION>
<TYPE_REC>Plein contentieux fiscal</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Lévis</RAPPORTEUR>
<COMMISSAIRE_GVT>Racine</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>     Vu la requête enregistrée le 15 janvier 1983 au secrétariat du Contentieux du Conseil d'Etat, présentée par la Société Anonyme "UNIBIS", dont le siège social est ... à Toulouse  31000 , représentée par son président-directeur général en exercice, et tendant à ce que le Conseil d'Etat :<br/>    1°  annule le jugement du 10 novembre 1982 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à la décharge de l'imposition supplémentaire à l'impôt sur les sociétés et de la contribution exceptionnelle auxquelles elle a été assujettie, respectivement au titre de l'année 1973 et de l'année 1974, ainsi que des pénalités de retard y afférentes,<br/>    2°  lui accorde la décharge des impositions et des pénalités contestées,<br/>     Vu les autres pièces du dossier ;<br/>    Vu le code général des impôts ;<br/>    Vu le code des tribunaux administratifs ;<br/>    Vu l'ordonnance du 31 juillet 1945 et le décret du 30 septembre 1953 ;<br/>    Vu la loi du 30 décembre 1977 ;<br/>    Vu la loi du 29 décembre 1983, portant loi de finances pour 1984, notamment son article 93-II ;<br/>    Après avoir entendu :<br/>    - le rapport de M. Lévis, Auditeur,<br/>    - les conclusions de M. Racine, Commissaire du gouvernement ;<br/>
<br/>     Considérant qu'à la suite, notamment, d'une tentative d'informatisation de ses écritures comptables, la société anonyme "Gomez Basquaise Paré", aux droits de laquelle se trouve la Société Anonyme "UNIBIS" a, au cours de l'exercice clos en 1973, relevé une différence de 1 134 567,23 F entre le compte collectif clients et la balance des comptes individuels des clients ; qu'elle a constitué, dans le bilan de clôture de l'exercice 1973, une provision pour créances douteuses, d'un montant égal à la différence ainsi constatée ; que l'administration ayant estimé que cette provision n'était pas de la nature de celles dont la déductibilité peut être admise en vertu de l'article 39-1 5° du code général des impôts, en a, en conséquence, réintégré le montant dans le bénéfice imposable au titre de l'année 1973 ;<br/>     Sur le bien-fondé des impositions contestées : <br/>    Considérant qu'aux termes de l'article 38 du code général des impôts : "1-... Le bénéfice imposable est le bénéfice net, déterminé d'après les résultats d'ensemble des opérations, de toute nature, effectuées par les entreprises... 2- Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt..." ; qu'aux termes de l'article 39 du code : "1- Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant notamment... 5° Les provisions constituées en vue de faire face à des pertes ou charges nettement précisées que des évènements en cours rendent probables, à condition qu'elles aient été effectivement constatées dans les écritures de l'exercice..." ; <br/>
<br/>     Considérant, d'une part, que pour justifier la provision de 1 134 567,23 F constituée dans les conditions sumentionnées à la clôturede l'exercice 1973, la société requérante se borne à alléguer que la société "Gomez-Basquaise, Paré" n'était pas en mesure de retrouver, à cette date, l'origine de l'écart constaté entre le montant du compte collectif clients et celui qui ressortait de la totalisation des comptes individuels clients, à partir des factures dont elle disposait ; qu'elle n'apporte, ainsi, aucun élément permettant de connaître avec une précision suffisante le risque de non recouvrement des créances auxquelles se rapporte la provision litigieuse ; qu'elle ne justifie, dès lors, pas du caractère de probabilité de la perte à laquelle cette provision avait pour objet de faire face ; <br/>     Considérant, d'autre part, qu'il résulte de l'instruction qu'après avoir fait procéder à une estimation du coût des opérations de vérification comptable qu'aurait exigées l'identification des créances litigieuses, la société "Gomez-Basquaise Paré" s'est estimée hors d'état de faire autrement que renoncer à poursuivre le recouvrement de la somme susmentionnée de 1 134 567,23 F et a, en conséquence, débité cette somme de son compte profits et pertes au titre de l'exercice clos en 1974 ; qu'elle n'établit pas, par cette seule circonstance, que les créances dont s'agit étaient, à la date de clôture de l'exercice 1973, devenues irrecouvrables ; que, dans ces conditions, la société requérante n'est pas fondée à soutenir que la somme susmentionnée a été portée au bilan de l'exercice clos en 1973 à titre de provision à la suite d'une erreur comptable et qu'elle doit être prise en compte comme charge dudit exercice pour la détermination des bases de l'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'année 1973, ainsi que de la contribution exceptionnelle qui lui a été assignée au titre de l'année 1974 ; <br/>     Sur la demande de compensation formulée par la Société "UNIBIS" : <br/>
<br/>    Considérant que si la Société "UNIBIS" demande que les impositions susmentionnées mises à la charge de la société "Gomez Basquaire Paré" soient compensées, à concurrence d'une somme de 102 787,28 F, correspondant à une imposition de même nature qui a été assignée à celle-ci à raison de la prise en compte, pour la détermination du bénéfice imposable de l'exercice clos en 1973, d'une différence constatée entre le montant du compte collectif fournisseurs et la balance des comptes individuels des fournisseurs, elle n'apporte aucune justification de l'excédent dont elle fait état dans l'assiette des impositions contestées ; <br/>     Considérant qu'il résulte de tout ce qui précède que la société requérante n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse a rejeté sa demande en décharge et, subsidiairement, en réduction des impositions contestées ;<br/>Article 1er : La requête de la Société Anonyme "UNIBIS" est rejetée. <br/>
<br/>     Article 2 : La présente décision sera notifiée à la Société Anonyme "UNIBIS" et au ministre de l'économie, des finances et du budget. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">19-04-01-04 CONTRIBUTIONS ET TAXES - IMPOTS SUR LES REVENUS ET BENEFICES - REGLES GENERALES PROPRES AUX DIVERS IMPOTS - IMPOT SUR LES BENEFICES DES SOCIETES ET AUTRES PERSONNES MORALES</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
