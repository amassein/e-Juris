<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041982565</ID>
<ANCIEN_ID>JG_L_2020_06_000000433253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/25/CETATEXT000041982565.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/06/2020, 433253, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433253.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C... a demandé au juge des référés du tribunal administratif de Nice, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de prononcer la suspension de l'exécution du courrier du 19 octobre 2018 par lequel le maire de Nice l'a informé du non renouvellement de l'autorisation d'occupation du domaine public dont il était titulaire ainsi que de la décision du 18 janvier 2019 rejetant sa demande de renouvellement de cette autorisation. Par une ordonnance n° 1903449 du 18 juillet 2019, le président de la 3ème chambre de ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 20 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Nice la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
- le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. C... et à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la commune de Nice ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 1er juillet 2011, le maire de Nice a délivré à M. C... une autorisation d'occupation temporaire du domaine public en vue de l'exercice d'une activité de commerce ambulant de pizzas, tous les jours de la semaine entre 16 heures et 22 heures, à l'angle du boulevard Pierre Sémard et de la rue Toesca, pour une durée d'un an, renouvelable par tacite reconduction. Par un courrier du 19 octobre 2018, le maire a informé M. C... que la commune entendait, sur l'ensemble du territoire de la ville, attribuer de nouvelles autorisations pour l'exercice de ce type de commerce à l'issue d'une procédure de mise en concurrence et, qu'en conséquence, son autorisation prendrait fin à l'échéance annuelle du 30 juin 2019, sauf à ce qu'il présente sa candidature pour l'emplacement qu'il occupait jusqu'à cette date et que cette candidature soit retenue. Par une décision du 18 janvier 2019, la maire a rejeté, pour le même motif, la demande présentée par M. C... tendant à la délivrance d'une nouvelle autorisation d'occupation du domaine public à l'issue de l'autorisation en cours. M. C... se pourvoit en cassation contre l'ordonnance du 18 juillet 2019 par laquelle le juge des référés du tribunal administratif de Nice a rejeté sa demande tendant à la suspension, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, en tant qu'ils lui font grief, des courriers du maire de Nice des 19 octobre 2018 et 18 janvier 2019.<br/>
<br/>
              2. En premier lieu, la seule circonstance que la date à compter de laquelle le maire de Nice a, par son courrier du 19 octobre 2018, décidé d'abroger l'autorisation d'occupation du domaine public dont bénéficiait M. C... soit passée n'a pas pour effet de priver d'objet le pourvoi formé contre l'ordonnance par laquelle le juge des référés du tribunal administratif a rejeté la demande tendant à la suspension de l'exécution de cette décision. Par ailleurs, la décision du 18 janvier 2019 n'a pas pour objet de rejeter un recours gracieux formé contre cette décision d'abrogation mais de rejeter la demande de M. C... tendant à ce qu'une nouvelle autorisation lui soit accordée à l'issue de l'autorisation en cours. Les conclusions tendant à la suspension de l'exécution de cette décision de refus n'ont pas davantage perdu leur objet. La commune de Nice n'est pas suite pas fondée à soutenir qu'il n'y aurait plus lieu de statuer sur le pourvoi formé par M. C.... <br/>
<br/>
              3. En deuxième lieu, M. C... soutient que le juge des référés aurait entaché son ordonnance d'irrégularité en se bornant à juger que les moyens qu'il soulevait à l'encontre des décisions contestées n'étaient pas de nature à faire naître un doute sérieux sur leur légalité, en omettant de mentionner deux de ces moyens dans les visas ou les motifs de cette ordonnance ou d'y répondre. Il ressort toutefois de l'ordonnance attaquée que le juge des référés a, d'une part, relevé que les deux courriers par lesquels la commune de Nice avait successivement informé M. C... de sa décision de ne pas l'autoriser à occuper, au-delà du 30 juin 2019, le domaine public pour y exploiter un camion pizza mentionnaient l'intention de la commune de procéder à une mise en concurrence. Il doit ainsi être regardé comme ayant écarté le moyen tiré de ce que les décisions contestées n'étaient pas motivées. Il ressort, d'autre part, de l'ordonnance qu'elle relève que le requérant faisait valoir, au soutien de ses demandes de suspension, que le maire ne pouvait agir en la matière qu'en vertu de ses pouvoirs de police et non en vertu de ses compétences de gestion du domaine public. Par suite, ne peut qu'être écarté le moyen tiré de ce que le juge des référés aurait omis de faire mention du moyen tiré de l'incompétence de l'auteur des décisions contestées.<br/>
<br/>
               4. En troisième lieu, si M. C... soutient que le juge des référés aurait commis une erreur de droit en ne regardant pas comme de nature à faire naître un doute sérieux sur la légalité des décisions contestées le moyen tiré de l'incompétence de leur signataire, au motif que le maire de Nice lui aurait délégué sa signature en méconnaissance des dispositions des articles L. 2122-19 et  L. 5411-4-2 du code général des collectivités territoriales, il résulte des dispositions de l'article L. 2122-19 de ce code, seules applicables en l'espèce dès lors que les décisions ont été signées au nom du maire de la commune, que celui-ci peut donner délégation de signature, notamment, au directeur général adjoint des services de mairie. Il ressort des pièces du dossier soumis au juge des référés que M. A..., signataire des décisions contestées, les a signées en qualité de directeur général adjoint des services de la mairie de Nice. Le moyen ne peut, par suite, qu'être écarté.<br/>
<br/>
               5. En quatrième lieu, le juge des référés, après avoir relevé que les décisions attaquées indiquaient qu'elles avaient été prises en vue de permettre à la commune d'engager une procédure de mise en concurrence pour l'attribution des autorisations d'occupation du domaine public à des fins d'activité économique, n'a, en tout état de cause, pas entaché son ordonnance d'erreur de droit en écartant comme n'étant pas de nature à faire naître un doute sérieux sur la légalité des décisions contestées le moyen tiré de leur absence de motivation.<br/>
<br/>
              6. En cinquième lieu, le juge des référés n'a pas commis d'erreur droit en écartant comme n'étant pas de nature à faire naître un doute sérieux le moyen tiré de ce que la volonté de l'autorité gestionnaire du domaine public de mettre en oeuvre une procédure de mise en concurrence en vue d'attribuer des autorisations d'occupation de ce domaine à des fins d'activités économiques ne saurait, en dehors des cas dans lesquels une telle procédure est rendue obligatoire par l'article L. 2122-1-1 du code général de la propriété des personnes publiques, justifier légalement l'abrogation d'autorisations en cours.<br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi de M. C... doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge à ce titre la somme de 3 000 euros à verser à la commune de Nice.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C... est rejeté.<br/>
Article 2 : M. C... versera à la commune de Nice une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. B... C... et à la commune de Nice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
