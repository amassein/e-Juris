<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023296359</ID>
<ANCIEN_ID>JG_L_2010_12_000000334780</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/29/63/CETATEXT000023296359.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/12/2010, 334780</TITRE>
<DATE_DEC>2010-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334780</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Pascal  Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Landais Claire</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:334780.20101223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 18 décembre 2009 et 18 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE LABORATOIRES MEDIPHA SANTE, dont le siège est Les Fjords, immeuble Oslo à Courtaboeuf cedex (91953) ; la SOCIETE LABORATOIRES MEDIPHA SANTE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 19 octobre 2009 par laquelle le directeur général de l'Agence française de sécurité sanitaire des produits de santé (AFSSAPS) a rejeté sa demande d'autorisation de mise sur le marché des spécialités Ramipril/Hydrochlorothiazide Altiso, Ramipril/Hydrochlorothiazide Authou, Ramipril/Hydrochlorothiazide Axorel et Ramipril/Hydrochlorothiazide Clexni ;<br/>
<br/>
<br/>
              2°) d'enjoindre à l'Agence française de sécurité sanitaire des produits de santé de lui délivrer ces autorisations dans un délai de quatre semaines à compter de la notification de la décision à intervenir, sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'AFSSAPS le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 novembre 2010, présentée pour la SOCIETE LABORATOIRES MEDIPHA SANTE ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de la SOCIETE LABORATOIRES MEDIPHA SANTE, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat de la SOCIETE LABORATOIRES MEDIPHA SANTE ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que la SOCIETE LABORATOIRES MEDIPHA SANTE a déposé le 20 mars 2009 des demandes d'autorisation de mise sur le marché pour quatre spécialités pharmaceutiques préconisées dans le traitement de l'hypertension artérielle associant toutes, selon la proportion 5 mg / 12,5 mg, les substances actives Ramipril et Hyrdrochlorothiazide, et présentées comme des génériques de la spécialité de référence Cotriatec 5 mg/12,5 mg, elle-même présentée comme une extension de gamme de la spécialité Tritazide 5 mg/25 mg qui avait bénéficié d'une autorisation de mise sur le marché en 1994 ; que la société requérante demande l'annulation pour excès de pouvoir de la décision du 19 octobre 2009 par laquelle le directeur général de l'Agence française de sécurité sanitaire des produits de santé (AFSSAPS) a refusé de lui délivrer ces autorisations au motif que, la spécialité de référence Cotriatec n'étant pas autorisée depuis au moins dix ans en France ou dans un autre Etat membre de l'Union européenne et la spécialité Tritazide se présentant sous un dosage différent, la procédure dite " abrégée " applicable aux génériques n'était pas applicable en l'espèce et que, dès lors, la demande d'autorisation était incomplète faute d'être accompagnée d'études pré-cliniques et cliniques ; <br/>
<br/>
              Considérant qu'aux termes de l'article R. 5121-42 du code de la santé publique : " Le directeur général de l'Agence française de sécurité sanitaire des produits de santé refuse l'autorisation de mise sur le marché pour les motifs mentionnés à l'article L. 5121-9 " ; que ce dernier article dispose notamment que : " L'autorisation prévue à l'article L. 5121-8 est refusée (...) lorsque la documentation et les renseignements fournis ne sont pas conformes au dossier qui doit être présenté à l'appui de la demande " ; qu'enfin, aux termes de l'article R. 5121-50 du même code : " Les décisions mentionnées aux articles (...)  R. 5121-42, (...) sont prises par le directeur général de l'Agence française de sécurité sanitaire des produits de santé après avis de la commission d'autorisation de mise sur le marché " ; <br/>
<br/>
              Considérant que la décision litigieuse, qui supposait de porter une appréciation sur la possibilité, pour la SOCIETE LABORATOIRES MEDIPHA SANTE, d'invoquer l'existence, pour les spécialités pour lesquelles elle demandait une autorisation, d'une spécialité de référence déjà autorisée depuis au moins huit ans en France ou dans un Etat membre de l'Union européenne, ne saurait être regardée comme ayant été prise en situation de compétence liée ; que l'AFSSAPS n'est, dès lors, pas fondée à soutenir que tous les moyens dirigés contre la décision du 19 octobre 2009 seraient inopérants ; <br/>
<br/>
              Considérant qu'il résulte des textes cités ci-dessus que le directeur général de l'AFSSAPS est tenu, même lorsqu'il refuse une autorisation de mise sur le marché pour le seul motif que la composition du dossier de demande n'est pas conforme aux dispositions légales, notamment du fait de l'absence de résultats d'essais précliniques et cliniques, de saisir préalablement pour avis la commission d'autorisation de mise sur le marché ; qu'il est constant que le directeur général de l'agence n'a pas saisi cette commission avant de refuser de faire droit aux demandes de la SOCIETE LABORATOIRES MEDIPHA SANTE ; que, contrairement à ce que soutient le directeur général de l'Agence française de sécurité sanitaire des produits de santé, il ne ressort pas des pièces du dossier que, compte tenu de l'appréciation à porter sur la nature de génériques des spécialités en cause, ce vice ne serait pas substantiel ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, la SOCIETE LABORATOIRES MEDIPHA SANTE est fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Considérant que, pour l'application des dispositions de l'article L. 911-1 du code de justice administrative, la présente décision n'implique pas nécessairement, compte tenu du motif d'annulation, la délivrance des autorisations demandées, mais seulement qu'il soit enjoint au directeur général de l'AFSSAPS de se prononcer à nouveau sur ces demandes, dans un délai de trois mois suivant sa notification ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction du prononcé d'une astreinte ; <br/>
<br/>
              Considérant, enfin, qu'en application de l'article L. 5322-2 du code de la santé publique, les décisions prises par le directeur général de l'AFSSAPS dans l'exercice des pouvoirs qu'il tient du code de la santé publique le sont au nom de l'Etat ; que, par suite, les conclusions de la SOCIETE LABORATOIRES MEDIPHA SANTE tendant à ce qu'une somme soit mise à la charge de l'AFSSAPS au titre des dispositions de l'article L. 761-1 du  code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 19 octobre 2009 du directeur général de l'Agence française de sécurité sanitaire des produits de santé (AFSSAPS) est annulée.<br/>
Article 2 : Il est enjoint au directeur général de l'Agence française de sécurité sanitaire des produits de santé de se prononcer sur la demande de la SOCIETE LABORATOIRES MEDIPHA SANTE dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus des conclusions de la requête de la SOCIETE LABORATOIRES MEDIPHA SANTE est rejeté. <br/>
Article 4 : La présente décision sera notifiée à la SOCIETE LABORATOIRES MEDIPHA SANTE et à l'Agence française de sécurité sanitaire des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - DÉFAUT DE CONSULTATION OBLIGATOIRE - VICE SUBSTANTIEL - EXCEPTION - ABSENCE EN L'ESPÈCE, COMPTE TENU DE LA NATURE DE L'APPRÉCIATION À PORTER [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-04-01-01 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. AUTORISATIONS DE MISE SUR LE MARCHÉ. - REFUS - CONSULTATION OBLIGATOIRE DE LA COMMISSION D'AUTORISATION DE MISE SUR LE MARCHÉ (ART. R. 5121-45 ET R. 5121-50 DU CSP) - EXISTENCE - MÉDICAMENT GÉNÉRIQUE - VICE SUBSTANTIEL - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-02-02 L'absence de consultation obligatoire préalablement à l'édiction d'un acte administratif constitue en principe un vice substantiel. Il peut toutefois en aller différemment eu égard notamment à l'objet de la consultation et à la nature de l'appréciation à porter. En l'espèce, il ne ressort pas du dossier que le défaut de consultation de la commission d'autorisation de mise sur le marché prévue à l'article R. 5121-50 du code de la santé publique constituerait un vice non substantiel, compte tenu de l'appréciation à porter sur la nature de génériques des spécialités faisant l'objet de la demande d'autorisation. Annulation de la mesure prise sans cette consultation préalable.</ANA>
<ANA ID="9B"> 61-04-01-01 Il résulte des articles R. 5121-42 et R. 5121-50 du code de la santé publique (CSP) que la commission d'autorisation de mise sur le marché doit être obligatoirement consultée avant toute décision de refus d'autorisation, y compris lorsque ce refus est fondé sur le caractère incomplet du dossier de demande. S'agissant de l'autorisation de mise sur le marché de médicaments génériques, l'absence de consultation de la commission ne peut, compte tenu de l'appréciation à porter sur la nature de génériques des spécialités en cause, être regardée comme un vice non substantiel. Annulation du refus d'autorisation opposé en l'absence d'une telle consultation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol contr., 7 avril 2010, Laboratoire Pasteur Cerba et laboratoire Biomnis, n° 325883, à mentionner aux tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
