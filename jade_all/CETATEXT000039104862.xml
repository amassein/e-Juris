<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039104862</ID>
<ANCIEN_ID>JG_L_2019_09_000000431828</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/10/48/CETATEXT000039104862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/09/2019, 431828, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431828</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:431828.20190916</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
               L'entreprise unipersonnelle à responsabilité limitée (EURL) Abo Wind, à l'appui de sa demande tendant à la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos le 31 décembre 2015 et des pénalités correspondantes, a produit un mémoire, enregistré le 10 avril 2019 au greffe du tribunal administratif de Toulouse, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1901891  du 24 mai 2019, enregistrée le 19 juin 2019 au secrétariat de la section du contentieux du Conseil d'État, la présidente de la 1ère chambre du tribunal administratif de Toulouse, avant qu'il soit statué sur la demande de l'EURL Abo Wind a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du a septies du I de l'article 219 du code général des impôts.      <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
        - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
        - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
        - le code général des impôts ; <br/>
        - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'État a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Dans la question prioritaire transmise, la société Abo Wind soutient, à l'appui de sa contestation de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice 2015 et des pénalités correspondantes, que les dispositions du a septies du I de l'article 219 du code général des impôts, dans sa version issue de l'article 41 de la loi du 28 décembre 2011 de finances rectificative pour 2011, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques au motif, d'une part, qu'elles instituent, en ce qui concerne le régime fiscal des plus-values et moins-values de cession de titres de participation détenus depuis moins de deux ans, une différence de traitement non justifiée entre sociétés liées et sociétés non liées au sens du 12 de l'article 39 du code général des impôts et une asymétrie de traitement fiscal entre plus-values et moins-values de cession et, d'autre part, qu'elles instituent une présomption irréfragable de fraude ou d'évasion fiscale sans réserver la possibilité pour le contribuable d'apporter la preuve que la cession des titres de participation à l'occasion de laquelle est constatée une moins-value ne poursuit pas un but exclusivement fiscal.<br/>
<br/>
              3. Aux termes de l'article 6 de la Déclaration des droits de l'Homme et du citoyen du 26 août 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Aux termes de l'article 13 de cette Déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité devant les charges publiques, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              4.  Aux termes du a septies du I de l'article 219 du code général des impôts, dans sa rédaction issue de l'article 41 de la loi du 28 décembre 2011 de finances rectificative pour 2011 : " Lorsqu'il existe des liens de dépendance entre l'entreprise cédante et l'entreprise cessionnaire au sens du 12 de l'article 39, la déduction des moins-values de cession de titres de participation définis au dix-septième alinéa du 5° du 1 de l'article 39, autres que ceux mentionnés au a sexies-0 bis du présent I, et détenus depuis moins de deux ans, intervient à la première des dates suivantes : / 1°) La date à laquelle l'entreprise cédante cesse d'être soumise à l'impôt sur les sociétés ou est absorbée par une entreprise qui, à l'issue de l'absorption, n'est pas liée à l'entreprise détenant les titres cédés ; / 2°) La date à laquelle les titres cédés cessent d'être détenus par une entreprise liée à l'entreprise cédante, à l'exception du cas où la société dont les titres ont été cédés a été absorbée par une autre entreprise liée ou qui le devient à cette occasion et pour toute la période où elle demeure liée ; / 3°) La date correspondant à l'expiration d'un délai de deux ans, décompté à partir du jour où l'entreprise cédante a acquis les titres. / L'imposition est établie au nom de l'entreprise cédante ou, en cas d'absorption dans des conditions autres que celles mentionnées au 1°, de l'entreprise absorbante, selon le régime de moins-value qui aurait été applicable si l'entreprise avait cédé les titres à cette date et, le cas échéant, les avait détenus depuis la date d'acquisition par l'entreprise absorbée. / L'entreprise joint à sa déclaration de résultat au titre de chaque exercice concerné un état conforme au modèle fourni par l'administration, faisant apparaître les éléments nécessaires au calcul des moins-values et ceux relatifs à l'identification de l'entreprise qui détient les titres, explicitant les liens de dépendance qui les unissent ".<br/>
<br/>
              5. Il ressort des travaux préparatoires de la loi du 28 décembre 2011 que les dispositions du a septies du I de l'article 219 du code général des impôts, dans leur rédaction en litige, ont pour objet de faire obstacle à une pratique d'optimisation fiscale consistant à céder à une filiale ou à une société soeur, dans les deux années de leur acquisition, des titres de participation ayant normalement vocation à être détenus sur le long terme, afin de constater des moins-values à court terme déductibles des résultats imposables à l'impôt sur les sociétés. A cette fin, elles soumettent au régime des moins-values à long terme l'ensemble des cessions réalisées entre des sociétés liées, y compris celles qui interviennent dans un délai de deux ans après l'acquisition des titres, sous réserve de la survenance des événements mentionnés au 1° et au 2° de cet article. En adoptant les dispositions contestées, qui ne peuvent être regardées comme instituant une présomption de fraude ou d'évasion fiscale, le législateur a retenu des critères objectifs et rationnels en fonction du but poursuivi, dès lors que la cession, dans les deux ans de leur acquisition, de titres de participation ayant subi une dépréciation était effectivement susceptible de caractériser une pratique d'optimisation fiscale au sein des groupes de sociétés. <br/>
<br/>
              6. En outre, la seule circonstance que, dans leur version en litige, les dispositions contestées ont pour effet de soumettre les seules moins-values de cession à un régime de report d'imposition alors que, dans leur version antérieure, issue de l'article 13 de la loi du 29 décembre 2010 de finances pour 2011, ces mêmes dispositions soumettaient tant les plus-values que les moins-values de cessions réalisées entre des sociétés liées dans un délai de moins de deux ans après l'acquisition des titres à un régime de long terme ne saurait avoir pour effet de faire peser sur les contribuables concernés une imposition revêtant un caractère confiscatoire.<br/>
<br/>
              7. Par suite, le grief tiré de la méconnaissance du principe d'égalité devant les charges publiques ne présente pas un caractère sérieux. <br/>
<br/>
              8. Par ailleurs, si les dispositions contestées soumettent les moins-values de cession de titres de participation détenus depuis moins de deux ans à un traitement fiscal différent selon que la cession intervient entre des sociétés liées ou entre des sociétés non liées, une telle différence de traitement répond à une différence objective de situation et, au regard de l'objectif poursuivi de faire obstacle à la pratique d'optimisation fiscale décrite ci-dessus, est en rapport direct avec l'objet de la loi. Par suite, le grief tiré de la méconnaissance du principe d'égalité devant la loi ne présente pas un caractère sérieux.<br/>
<br/>
              9. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée par la société requérante, qui n'est pas nouvelle, ne peut être regardée comme présentant un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
                                             D E C I D E :<br/>
                                              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Abo Wind devant le tribunal administratif de Toulouse.<br/>
Article 2 : La présente décision sera notifiée à l'entreprise unipersonnelle à responsabilité limitée Abo Wind et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre, au Conseil constitutionnel et au tribunal administratif de Toulouse.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
