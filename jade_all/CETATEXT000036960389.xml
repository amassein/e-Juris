<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036960389</ID>
<ANCIEN_ID>JG_L_2018_05_000000405937</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/96/03/CETATEXT000036960389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 23/05/2018, 405937</TITRE>
<DATE_DEC>2018-05-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405937</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405937.20180523</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir le permis de construire deux bâtiments à usage d'habitation, sur une parcelle située 115 boulevard de la Villette et 1-3 square Jean Falck, délivré le 12 août 2014 par le maire de Paris à l'office public de l'habitat Paris Habitat, ainsi que la décision du 15 janvier 2015 par laquelle ce maire a rejeté son recours gracieux. <br/>
<br/>
              Par un jugement n° 1504306 du 13 octobre 2016, le tribunal administratif de Paris a fait droit à cette demande.<br/>
<br/>
              1° Sous le n° 405937, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 décembre 2016 et 13 mars 2017 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande du syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck ;<br/>
<br/>
              3°) de mettre à la charge de ce syndicat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 405976, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 décembre 2016 et 14 mars 2017 au secrétariat du contentieux du Conseil d'Etat, l'office public de l'habitat Paris Habitat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande du syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck ;<br/>
<br/>
              3°) de mettre à la charge de ce syndicat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la ville de Paris, à la SCP Delvolvé et Trichet, avocat de la société SDC du 117 boulevard de la Villette et du 2-4 square Jean Falck et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'office public de l'habitat Paris Habitat.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 12 août 2014, le maire de Paris a délivré à l'office public de l'habitat Paris Habitat un permis de construire, modifié par arrêté du 27 mai 2015, en vue de l'édification de deux bâtiments à usage d'habitation sur une parcelle située 115, boulevard de la Villette et 1-3, square Jean Falck. Le syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck a formé contre le permis initial un recours gracieux, que le maire de Paris a rejeté par une décision notifiée le 21 janvier 2015. Ce syndicat a alors demandé au tribunal administratif de Paris l'annulation pour excès de pouvoir de l'autorisation d'urbanisme en cause et de la décision de rejet de son recours gracieux. Par un jugement du 13 octobre 2016, contre lequel la ville de Paris et l'office public de l'habitat Paris Habitat se pourvoient en cassation, le tribunal administratif de Paris a annulé l'arrêté du 12 août 2014. Il y a lieu de joindre ces pourvois, dirigés contre le même jugement.<br/>
<br/>
              Sur la fin de non-recevoir opposée par le syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck :<br/>
<br/>
              2. L'article R. 600-1 du code de l'urbanisme n'impose pas à l'auteur de la décision litigieuse ou au bénéficiaire de l'autorisation, lorsqu'ils se pourvoient en cassation à l'encontre d'un jugement ou d'un arrêt annulant, au moins partiellement, un document d'urbanisme ou une décision valant autorisation d'occupation ou d'utilisation du sol, l'obligation de notifier le pourvoi dirigé contre un tel jugement ou arrêt. Il suit de là que la fin de non-recevoir opposée sur ce point par le syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck à la ville de Paris et à l'office public de l'habitat Paris Habitat ne peut qu'être écartée.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur la légalité du permis de construire :<br/>
<br/>
              3. Aux termes de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier ". <br/>
<br/>
              4. Saisi d'un pourvoi dirigé contre une décision juridictionnelle reposant sur plusieurs motifs dont l'un est erroné, le juge de cassation, à qui il n'appartient pas de rechercher si la juridiction aurait pris la même décision en se fondant uniquement sur les autres motifs, doit, hormis le cas où ce motif erroné présenterait un caractère surabondant, accueillir le pourvoi. Il en va cependant autrement, en principe, lorsque la décision juridictionnelle attaquée prononce l'annulation pour excès de pouvoir d'un acte administratif, dans la mesure où l'un quelconque des moyens retenus par le juge du fond peut suffire alors à justifier son dispositif d'annulation. En pareille hypothèse - et sous réserve du cas où la décision qui lui est déférée aurait été rendue dans des conditions irrégulières - il appartient au juge de cassation, si l'un des moyens reconnus comme fondés par cette décision en justifie légalement le dispositif, de rejeter le pourvoi, après avoir, en raison de l'autorité de chose jugée qui s'attache aux motifs constituant le soutien nécessaire du dispositif de la décision juridictionnelle déférée, censuré celui ou ceux de ces motifs qui étaient erronés. <br/>
<br/>
              5. Il ressort des énonciations du jugement attaqué que le tribunal a annulé le permis de construire du 12 août 2014 aux motifs qu'il méconnaissait, d'une part, l'article L. 425-3 du code de l'urbanisme, pour ce qui concerne l'aménagement du commerce prévu au rez-de-chaussée de l'un des deux bâtiments, et, d'autre part, les règles de prospect prévues par le 2° de l'article UG 8.1 du règlement du plan local d'urbanisme de la ville de Paris.<br/>
<br/>
              6. En premier lieu, aux termes de l'article UG. 8 du règlement du plan local d'urbanisme de la ville de Paris : " Lorsque les dispositions inscrites aux documents graphiques du règlement ne sont pas conformes aux dispositions du présent article, elles prévalent sur ces dernières (...) ". Cet article prévoit, en outre, dans sa partie UG. 8.1 : " Dispositions générales : / 1°- Façades comportant des baies constituant l'éclairement premier de pièces principales : / Lorsque des façades ou parties de façade de constructions en vis-à-vis sur un même terrain comportent des baies constituant l'éclairement premier de pièces principales, elles doivent être édifiées de telle manière que la distance de l'une d'elles au point le plus proche de l'autre soit au moins égale à 6 mètres. (...) / 2°- Façades comportant des baies dont aucune ne constitue l'éclairement premier de pièces principales : / Lorsque des façades ou parties de façade de constructions en vis-à-vis sur un même terrain comportent des baies dont aucune ne constitue l'éclairement premier de pièces principales, elles doivent être édifiées de telle manière que la distance de l'une d'elles au point le plus proche de l'autre soit au moins égale à 3 mètres. (...) ". <br/>
<br/>
              7. Il résulte des dispositions précitées, ainsi que de la figure 4 inscrite au règlement du plan local d'urbanisme, que seules les parties de façades comportant des baies doivent, pour leur implantation, respecter les distances ainsi définies, calculées entre la baie et le point le plus proche de la façade en vis-à-vis. Par suite, en jugeant qu'une partie de la façade ouest du bâtiment sur rue et l'extrémité de la façade est du bâtiment sur cour du projet en cause seraient édifiées en vis-à-vis à une distance inférieure à 3 mètres et qu'ainsi le permis contesté méconnaissait le 2° précité de l'article UG. 8.1, sans rechercher si ces parties de façade comportaient des baies, le tribunal a commis une erreur de droit.<br/>
<br/>
              8. En second lieu, aux termes de l'article L. 425-3 du code de l'urbanisme : " Lorsque le projet porte sur un établissement recevant du public, le permis de construire tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation dès lors que la décision a fait l'objet d'un accord de l'autorité administrative compétente qui peut imposer des prescriptions relatives à l'exploitation des bâtiments en application de l'article L. 123-2 du code de la construction et de l'habitation. Le permis de construire mentionne ces prescriptions. Toutefois, lorsque l'aménagement intérieur d'un établissement recevant du public ou d'une partie de celui-ci n'est pas connu lors du dépôt d'une demande de permis de construire, le permis de construire indique qu'une autorisation complémentaire au titre de l'article L. 111-8 du code de la construction et de l'habitation devra être demandée et obtenue en ce qui concerne l'aménagement intérieur du bâtiment ou de la partie de bâtiment concernée avant son ouverture au public ".<br/>
<br/>
              9. Il résulte de ces dispositions que lorsque, comme en l'espèce, l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public, qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation, n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le code de la construction et de l'habitation, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public, et ce alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation. Par suite, et alors que la ville de Paris ne peut utilement se prévaloir du contenu de la demande de permis de construire pour soutenir que les juges du fond auraient dénaturé sur ce point les faits et les pièces du dossier, le tribunal n'a pas commis d'erreur de droit en jugeant que le permis de construire litigieux était illégal au motif qu'il ne comportait pas, pour l'aménagement du local commercial prévu au rez-de-chaussée de l'un des deux bâtiments, la mention exigée par l'article L. 425-3 du code de l'urbanisme.<br/>
<br/>
              Sur jugement attaqué, en tant qu'il statue sur la possibilité de régularisation du permis de construire :<br/>
<br/>
              10. Aux termes de l'article L. 600-5 du code de l'urbanisme : " Le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice n'affectant qu'une partie du projet peut être régularisé par un permis modificatif, peut limiter à cette partie la portée de l'annulation qu'il prononce et, le cas échéant, fixer le délai dans lequel le titulaire du permis pourra en demander la régularisation ".<br/>
<br/>
              11. Pour l'application de ces dispositions, le juge administratif doit, en particulier, apprécier si le vice qu'il a relevé peut être régularisé par un permis modificatif. Un tel permis ne peut être délivré que si, d'une part, les travaux autorisés par le permis initial ne sont pas achevés - sans que la partie intéressée ait à établir devant le juge l'absence d'achèvement de la construction ou que celui-ci soit tenu de procéder à une mesure d'instruction en ce sens - et si, d'autre part, les modifications apportées au projet initial pour remédier au vice d'illégalité ne peuvent être regardées, par leur nature ou leur ampleur, comme remettant en cause sa conception générale. A ce titre, la seule circonstance que ces modifications portent sur des éléments tels que son implantation, ses dimensions ou son apparence ne fait pas, par elle-même, obstacle à ce qu'elles fassent l'objet d'un permis modificatif. <br/>
<br/>
              12. En l'espèce, le tribunal a annulé le permis de construire litigieux après avoir jugé que la méconnaissance des règles de prospect prévues par le 2° de l'article UG. 8.1 du règlement du plan local d'urbanisme impliquait " la modification de la conception des deux façades en cause ou de l'une d'elles au moins ", ce qui, selon les termes de son jugement, remettait en cause l'ensemble du projet de construction et faisait, dès lors, obstacle à l'application de l'article L. 600-5 du code de l'urbanisme. Toutefois, c'est par un motif entaché d'erreur de droit, ainsi qu'il a été dit au point 7, qu'il a retenu que le permis attaqué méconnaissait ces dispositions du règlement. Au surplus, il résulte de ce qui a été dit au point précédent que la circonstance que l'illégalité concernait le respect des règles de prospect, et donc l'implantation, les dimensions ou l'apparence des façades en cause, ne faisait pas par elle-même obstacle à la régularisation par un permis de construire modificatif. Par suite, en refusant pour ce motif de faire application de l'article L. 600-5 du code de l'urbanisme, le tribunal a également commis une erreur de droit. Si, enfin, ainsi qu'il a été dit au point 9, le tribunal a également jugé à bon droit que le permis méconnaissait l'article L. 425-3 du code de l'urbanisme, il ne résulte pas de son jugement qu'il aurait aussi regardé ce vice comme faisant obstacle à la régularisation du permis litigieux par un permis modificatif.<br/>
<br/>
              13. Si le juge de cassation, lorsqu'il écarte les moyens dirigés contre un ou plusieurs motifs retenant des vices qui entachent d'illégalité un permis de construire mais censure une erreur commise par les juges du fond dans la mise en oeuvre de l'article L. 600-5 du code de l'urbanisme, n'est pas tenu d'annuler l'arrêt ou le jugement attaqué en tant qu'il juge que le permis litigieux est entaché de certains vices, il résulte toutefois de tout ce qui précède qu'il y a lieu, dans les circonstances de l'espèce et sans qu'il soit besoin d'examiner les autres moyens des pourvois, d'annuler le jugement attaqué dans son entier.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la ville de Paris et de l'office public de l'habitat Paris Habitat, qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de ces derniers présentées au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 13 octobre 2016 est annulé. <br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Paris.<br/>
Article 3 : Les conclusions des parties présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la ville de Paris, à l'office public de l'habitat Paris Habitat et au syndicat des copropriétaires du 117, boulevard de la Villette et du 2-4, square Jean Falck.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-003 POLICE. POLICES SPÉCIALES. - DEMANDE DE PERMIS DE CONSTRUIRE - CAS DANS LEQUEL L'AMÉNAGEUR INTÉRIEUR N'EST PAS CONNU LORS DE LA DEMANDE - OBLIGATION POUR L'AUTORITÉ COMPÉTENTE POUR DÉLIVRER LE PERMIS DE MENTIONNER, À PEINE D'ILLÉGALITÉ, L'OBLIGATION D'OBTENIR L'AUTORISATION PRÉVUE AU TITRE DE L'ARTICLE L. 111-8 DU CCH.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - PERMIS RELATIF À UN ERP - CAS DANS LEQUEL L'AMÉNAGEUR INTÉRIEUR N'EST PAS CONNU LORS DE LA DEMANDE DE PERMIS DE CONSTRUIRE - OBLIGATION POUR L'AUTORITÉ COMPÉTENTE POUR DÉLIVRER LE PERMIS DE MENTIONNER, À PEINE D'ILLÉGALITÉ, L'OBLIGATION D'OBTENIR L'AUTORISATION PRÉVUE AU TITRE DE L'ARTICLE L. 111-8 DU CCH.
</SCT>
<ANA ID="9A"> 49-05-003 Il résulte de l'article L. 425-3 du code de l'urbanisme que lorsque l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public (ERP), qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation (CCH), n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le CCH, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public, et ce alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation.</ANA>
<ANA ID="9B"> 68-03-02-02 Il résulte de l'article L. 425-3 du code de l'urbanisme que lorsque l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public (ERP), qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation (CCH), n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le CCH, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public, et ce alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
