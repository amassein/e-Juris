<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042528971</ID>
<ANCIEN_ID>JG_L_2020_11_000000445481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/89/CETATEXT000042528971.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/11/2020, 445481, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445481.20201112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête et deux nouveaux mémoires, enregistrés les 20 et 23 octobre et le 7 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, dans le dernier état de ses écritures :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 14 septembre 2020 par laquelle le groupe 3 du Conseil national des universités a rejeté sa candidature à l'inscription sur la liste de qualification aux fonctions de professeur des universités ;<br/>
<br/>
              2°) d'enjoindre que soient gelés tous les postes de professeur des universités en anglais juridique susceptibles d'être mis au concours ;<br/>
<br/>
              3°) d'enjoindre à la ministre de l'enseignement supérieur et de la recherche de produire à l'instance, en premier lieu, la confirmation que le procès-verbal du 5 septembre 2020 relate intégralement le vote qui a conclu l'examen de sa candidature, en deuxième lieu, les dix bulletins décomptés du procès-verbal, en troisième lieu, la position prise par le onzième membre du groupe, signataire de la liste d'émargement mais non décompté parmi les votants, en quatrième lieu, les éventuelles démissions écrites du groupe qui lui auraient été notifiées régulièrement avant le 4 septembre 2020, en cinquième lieu, des précisions relatives aux " vérifications " opérées postérieurement au procès-verbal ainsi que, le cas échéant, le compte-rendu écrit de ces vérifications et, en dernier lieu, la " proposition du groupe ", qui sert de base au vote ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elle soutient que:<br/>
              - la condition d'urgence est remplie en ce que la décision contestée lui cause un préjudice professionnel et moral, un préjudice financier, ainsi qu'une perte de chance irréversible, dès lors qu'elle la prive de la possibilité de se porter candidate à un poste de professeur, et donc de la rémunération et des droits à la retraite qui y correspondent ;  <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - le groupe 3 du Conseil national des universités n'était pas compétent pour statuer sur sa capacité dans le domaine de la recherche, laquelle a été souverainement reconnue par le jury d'habilitation à diriger des recherches en vertu de l'article L. 612-7 du code de l'éducation et sa décision est, en conséquence, entachée d'erreur de droit ;<br/>
              - la décision contestée est insuffisamment motivée et indique, à tort, que sa candidature a été " très majoritairement " rejetée ;<br/>
              - elle est entachée d'un vice de procédure dès lors que le quorum aurait dû être calculé par rapport au nombre de membres du groupe 3 du Conseil national des universités ;<br/>
              - le procès-verbal de la réunion du 4 septembre 2020 du groupe 3 est daté du 5 septembre, soit 9 jours avant la date de la décision critiquée et ne retrace ni les débats du groupe ni la proposition qui a été soumise à son vote par son président ;<br/>
              - cette position était vraisemblablement défavorable à sa candidature, compte tenu de la substance des rapports des deux rapporteurs, ce dont il résulte que cette candidature a, en réalité, été validée, puisque la proposition a été rejetée par cinq voix contre quatre ; <br/>
              - elle méconnaît l'article 1er du décret du 16 janvier 1992 relatif au Conseil national des universités dès lors que les critères et les modalités d'appréciation des candidatures à la qualification n'ont pas été rendus publics ;<br/>
              - elle constitue une discrimination à l'encontre des candidats dits " atypiques ", qui se sont engagés dans une vie professionnelle à l'extérieur de l'université avant de devenir enseignant-chercheur.<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 novembre 2020, le ministre de l'enseignement supérieur, de la recherche et de l'innovation conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'aucun des moyens n'est de nature à créer un doute sur la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84431 du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences ;<br/>
              - le décret n° 9270 du 16 janvier 1992 ;<br/>
              - l'arrêté du 19 mars 2010 fixant les modalités de fonctionnement du Conseil national des universités ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme B... et, d'autre part, le ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
              A été entendu lors de l'audience publique du 9 novembre 2020, à 15 heures, à laquelle Mme B... n'était ni présente, ni représentée :<br/>
<br/>
              - le représentant du ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ".<br/>
<br/>
              2. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit, enfin, être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés statue.<br/>
<br/>
              3. Il résulte de l'instruction que Mme B..., qui exerce en qualité de maître de conférences à l'Université Paris II Panthéon-Assas, a sollicité son inscription sur la liste de qualification aux fonctions de professeur des universités, laquelle a été rejetée en dernier lieu par la décision du 14 septembre 2020 du groupe 3 du Conseil national des universités. Par sa requête, Mme B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de la décision du 14 septembre 2020 et, d'autre part, d'enjoindre au Conseil national des universités de geler tous les postes de professeur des universités en anglais juridique susceptibles d'être mis au concours.<br/>
<br/>
              4. Mme B... fait valoir, au titre de l'urgence, que la décision contestée la prive de la possibilité de se porter candidate à un poste de professeur en anglais juridique, alors même que ces types de postes sont rares, et qu'ainsi, elle porte une atteinte grave et immédiate à sa situation professionnelle et financière et crée une perte de chance irréversible. Toutefois, d'une part, cette décision ne place pas Mme B... dans l'impossibilité de poursuivre l'activité professionnelle qu'elle exerce et la rémunération dont elle est assortie, d'autre part, l'inscription sur la liste de qualification ne constitue qu'une étape d'un éventuel recrutement de Mme B... aux fonctions de professeur des universités, qui revêtirait ainsi, même à supposer que l'exécution de la décision contestée soit suspendue, un caractère hypothétique. Enfin, il ne résulte pas de l'instruction que l'anglais juridique constituerait une spécialité suffisamment rare pour que la décision contestée puisse être regardée comme entraînant pour Mme B..., à qui il est loisible de présenter à nouveau sa candidature à une inscription sur la liste de qualification, une perte de chance irréversible. Dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'existence de moyens propres à créer un doute sérieux sur la légalité de la décision contestée, la requête ne peut être accueillie, y compris ses conclusions à fin d'injonction et ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
