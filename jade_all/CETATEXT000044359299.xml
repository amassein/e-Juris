<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359299</ID>
<ANCIEN_ID>JG_L_2021_11_000000453026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/92/CETATEXT000044359299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 22/11/2021, 453026, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:453026.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal administratif de Toulon, sur le fondement de l'article L. 52-15 du code électoral, de sa décision du 15 février 2021 constatant le dépôt hors délai du compte de campagne de M. D... A..., candidat tête de liste aux élections municipales et communautaires des 15 mars et 28 juin 2020 dans la commune du Beausset (Var). Par un jugement n°2100498 du 27 avril 2021, le tribunal administratif de Toulon a décidé que la CNCCFP avait constaté à bon droit le dépôt hors délai du compte de campagne de M. A..., déclaré celui-ci inéligible pour une durée de six mois, prononcé sa démission d'office de son mandat de conseiller municipal du Beausset et proclamé Mme B... C... élue à sa place.<br/>
<br/>
              Par une requête, enregistrée le 27 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2, 3 et 4 de ce jugement qui le déclarent inéligible pour une durée de six mois, prononcent sa démission d'office de son mandat de conseiller municipal du Beausset et proclament Mme B... C... élue à sa place ;<br/>
<br/>
              2°) de ne pas le déclarer inéligible.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. M. D... A... a été candidat aux élections municipales et communautaires des 15 mars et 28 juin 2020 dans la commune du Beausset (Var). Il a été élu conseiller municipal à l'issue du deuxième tour de cette élection. Par une décision du 15 février 2021, la commission nationale des comptes de campagne et des financements politiques (CNCCFP) a constaté le dépôt hors délai de son compte de campagne, en méconnaissance des dispositions combinées de l'article L. 52-12 du code électoral et du 4° du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et a saisi le tribunal administratif de Toulon de cette décision, en application de l'article L. 52-15 de ce code. M. A... fait appel du jugement du 27 avril 2021 par lequel le tribunal administratif de Toulon l'a déclaré inéligible, a prononcé sa démission d'office de son mandat de conseiller municipal du Beausset et a proclamé Mme B... C... élue à sa place.<br/>
<br/>
              2. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction antérieure à la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) : Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. ". Aux termes de ce même article dans sa rédaction issue de cette loi : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; (...) ".<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 15 de la loi du 2 décembre 2019 : " La présente loi, à l'exception de l'article 6, entre en vigueur le 30 juin 2020 ". Aux termes du XVI de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " A l'exception de son article 6, les dispositions de la loi n° 2019-1269 du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral ne sont pas applicables au second tour de scrutin régi par la présente loi ". Il résulte de ces dispositions que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne. <br/>
<br/>
              4. Toutefois, l'inéligibilité prévue par les dispositions de l'article L. 118-3 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe, dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue. Le législateur n'ayant pas entendu, par les dispositions citées au point 2, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point des dispositions de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L. 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part,  n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrit par l'article L. 52-12 de ce même code.<br/>
<br/>
              5. Aux termes du deuxième alinéa de l'article L. 52-12 du code électoral : " Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes, notamment d'une copie des contrats de prêts conclus en application de l'article L. 52-7-1 du présent code, ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. ". Aux termes du 4° du XII de l'article 19 de la loi n° 2020-290 du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " Pour les listes de candidats présentes au seul premier tour, la date limite mentionnée à la première phrase du deuxième alinéa de l'article L. 52-12 du code électoral est fixée au 10 juillet 2020 à 18 heures. Pour celles présentes au second tour, la date limite est fixée au 11 septembre 2020 à 18 heures ". Aux termes du troisième alinéa de l'article L. 52-15 du code électoral : " Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection. " <br/>
<br/>
              6. Il résulte des dispositions combinées de l'article L. 52-12 du code électoral et du 4° du XII de l'article 19 de la loi du 23 mars 2020 citées ci-dessus que M. A..., dont la liste était présente au second tour des élections municipales et communautaires du Beausset, devait déposer son compte de campagne auprès de la CNCCFP avant le vendredi 11 septembre 2020. Il résulte de l'instruction, et il n'est d'ailleurs pas contesté par M. A..., que cette échéance n'a pas été respectée par lui.<br/>
<br/>
              7. En ne respectant pas le délai de dépôt de son compte de campagne qui s'imposait à lui, M. A... a méconnu une règle substantielle relative au financement des campagnes électorales. S'il invoque sa charge de travail, notamment après le second tour des élections municipales, due aux conséquences de la crise sanitaire résultant de l'épidémie de covid-19 sur la situation économique de son entreprise, il ne produit aucun élément permettant d'apprécier la réalité de cette affirmation. <br/>
<br/>
              8. Dans ces conditions, M. A... a commis un manquement d'une particulière gravité aux règles de financement des campagnes électorales justifiant qu'une sanction d'inéligibilité soit prise à son encontre sur le fondement des dispositions de l'article L. 118-3 du code électoral issues de la loi du 2 décembre 2019.<br/>
<br/>
              9. Il résulte de l'instruction que M. A... a déposé son compte de campagne à la CNCCFP le 21 janvier 2021. Ce compte est excédentaire et fait apparaître que les dépenses de sa campagne électorale ont été effectuées par le mandataire qu'il avait désigné. Ainsi, en tenant compte de ces circonstances pour fixer à 6 mois la durée de l'inéligibilité de M. A..., le tribunal administratif de Toulon a fait une exacte application des dispositions de l'article L. 118-3 du code électoral. Il en résulte que la requête de M. A... doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M.  A..., à la commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur. <br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Stéphane Verclytte, conseiller d'Etat, présidant ; M. Christian Fournier, conseiller d'Etat et Mme Pauline Berne, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
                 Le Président : <br/>
                 Signé : M. Stéphane Verclytte<br/>
 		La rapporteure : <br/>
      Signé : Mme Pauline Berne<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... F...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
