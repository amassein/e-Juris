<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035736457</ID>
<ANCIEN_ID>JG_L_2017_10_000000403537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/73/64/CETATEXT000035736457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 04/10/2017, 403537, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403537.20171004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 15 septembre 2016 et 31 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B..., Mme F... D...et M. C...E...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 9 août 2016 par laquelle le ministre chargé des transports a refusé l'établissement et la publication du bilan des résultats économiques et sociaux de l'infrastructure ferroviaire dite " Perpignan-Figueras " ;   <br/>
<br/>
              2°) avant dire droit, d'enjoindre au ministre chargé des transports de produire la preuve de la date du dernier versement de la subvention forfaitaire de 54 millions d'euros par les Etats concédants à la société TP Ferro, concessionnaire au titre de l'article 14.4.1 du contrat de concession ; <br/>
<br/>
              3°) d'enjoindre au ministre chargé des transports de faire réaliser et de publier le bilan demandé dans un délai de deux mois, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des transports ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 1511-2 du code des transports : " Les grands projets d'infrastructures et les grands choix technologiques sont évalués sur la base de critères homogènes intégrant les impacts des effets externes des transports sur, notamment, l'environnement, la sécurité et la santé et permettant des comparaisons à l'intérieur d'un même mode de transport ainsi qu'entre les modes ou les combinaisons de modes de transport " ; qu'aux termes de l'article L. 1511-6 du même code : " Lorsque les opérations mentionnées à l'article L. 1511-2 sont réalisées avec le concours de financements publics, un bilan des résultats économiques et sociaux est établi au plus tard cinq ans après leur mise en service. Ce bilan est rendu public " ; qu'aux termes de l'article R. 1511-8 du même code : " Le bilan, prévu par l'article L. 1511-6, des résultats économiques et sociaux des infrastructures dont le projet avait été soumis à l'évaluation, est établi par le maître d'ouvrage au moins trois ans et au plus cinq ans après la mise en service des infrastructures concernées. / La collecte des informations nécessaires au bilan est organisée par le maître d'ouvrage dès la réalisation du projet  " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que, par lettre du 9 août 2016, le ministre chargé des transports a rejeté la demande que lui avaient adressée M. B...et Mme D...tendant à la transmission du bilan des résultats économiques et sociaux de la section ferroviaire internationale Perpignan-Figueras devant être établi en application des dispositions précédemment citées des articles L. 1511-2, L. 1511-6 et R. 1511-8 du code des transports, au motif que cette section n'était entrée en service qu'en 2013 et que le délai de cinq ans imparti par l'article R. 1511-8 du code des transports pour l'établissement d'un tel bilan n'était, en conséquence, pas expiré ; que M.B..., Mme D...et M. E...demandent l'annulation pour excès de pouvoir de cette lettre du 9 août 2016 qui doit être regardée comme refusant d'établir et de rendre public avant 2018 ce bilan de la section ferroviaire internationale Perpignan-Figueras ; <br/>
<br/>
              3.	Considérant que si le refus de réaliser et de rendre public à l'expiration du délai prévu par l'article L. 1511-6 du code de transports le bilan des résultats économiques et sociaux d'un grand projet d'infrastructure constitue, eu égard à l'objet de ces dispositions, une décision susceptible de recours qui peut être contestée par toute personne devant le juge de l'excès de pouvoir, un tel refus ne présente pas un caractère réglementaire ; que, par suite, ni les dispositions de l'article R. 311-1 du code de justice administrative, selon lesquelles " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : / (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale ", ni aucune autre disposition ne donnent compétence au Conseil d'Etat pour connaître en premier ressort des conclusions tendant à l'annulation de la décision prise en l'espèce par le ministre chargé des transports ; qu'il y a lieu, dès lors, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-1 du même code ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de M. B...et autres est attribué au tribunal administratif de Paris. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., premier requérant dénommé, au ministre d'Etat, ministre de la transition écologique et solidaire et à la présidente du tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - REFUS DE RÉALISER ET DE RENDRE PUBLIC LE BILAN DES RÉSULTATS ÉCONOMIQUES ET SOCIAUX D'UN GRAND PROJET D'INFRASTRUCTURES (ART. L. 1511-6 DU CODE DES TRANSPORTS) - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - REFUS DE RÉALISER ET DE RENDRE PUBLIC LE BILAN DES RÉSULTATS ÉCONOMIQUES ET SOCIAUX D'UN GRAND PROJET D'INFRASTRUCTURES (ART. L. 1511-6 DU CODE DES TRANSPORTS) - INCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-04-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. - REFUS DE RÉALISER ET DE RENDRE PUBLIC LE BILAN DES RÉSULTATS ÉCONOMIQUES ET SOCIAUX D'UN GRAND PROJET D'INFRASTRUCTURES (ART. L. 1511-6 DU CODE DES TRANSPORTS) - RECOURS OUVERT À TOUTE PERSONNE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">65 TRANSPORTS. - REFUS DE RÉALISER ET DE RENDRE PUBLIC LE BILAN DES RÉSULTATS ÉCONOMIQUES ET SOCIAUX D'UN GRAND PROJET D'INFRASTRUCTURES (ART. L. 1511-6 DU CODE DES TRANSPORTS) - CARACTÈRE RÉGLEMENTAIRE - ABSENCE.
</SCT>
<ANA ID="9A"> 17-05-01-01-01 Si le refus de réaliser et de rendre public, à l'expiration du délai prévu par l'article L. 1511-6 du code de transports, le bilan des résultats économiques et sociaux d'un grand projet d'infrastructure constitue, eu égard à l'objet de ces dispositions, une décision susceptible de recours qui peut être contestée par toute personne devant le juge de l'excès de pouvoir, un tel refus ne présente pas un caractère réglementaire.... ,,Par suite, ni les dispositions de l'article R. 311-1 du code de justice administrative (CJA), ni aucune autre disposition ne donnent compétence au Conseil d'Etat pour connaître en premier ressort des conclusions tendant à l'annulation de la décision prise en l'espèce par le ministre chargé des transports.</ANA>
<ANA ID="9B"> 54-01-01-01 Le refus de réaliser et de rendre public, à l'expiration du délai prévu par l'article L. 1511-6 du code de transports, le bilan des résultats économiques et sociaux d'un grand projet d'infrastructure constitue, eu égard à l'objet de ces dispositions, une décision susceptible de recours qui peut être contestée par toute personne devant le juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-01-04-02 Le refus de réaliser et de rendre public, à l'expiration du délai prévu par l'article L. 1511-6 du code de transports, le bilan des résultats économiques et sociaux d'un grand projet d'infrastructure constitue, eu égard à l'objet de ces dispositions, une décision susceptible de recours qui peut être contestée par toute personne devant le juge de l'excès de pouvoir.</ANA>
<ANA ID="9D"> 65 Si le refus de réaliser et de rendre public, à l'expiration du délai prévu par l'article L. 1511-6 du code de transports, le bilan des résultats économiques et sociaux d'un grand projet d'infrastructure constitue, eu égard à l'objet de ces dispositions, une décision susceptible de recours qui peut être contestée par toute personne devant le juge de l'excès de pouvoir, un tel refus ne présente pas un caractère réglementaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
