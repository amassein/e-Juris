<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358758</ID>
<ANCIEN_ID>JG_L_2021_03_000000450160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358758.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/03/2021, 450160, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450160.20210315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              I. Sous le n° 450160, par une requête et un mémoire en réplique, enregistrés les 25 février et 9 mars 2021 au secrétariat du contentieux du Conseil d'Etat, l'Association française des industries de la détergence (AFISE), l'Association nationale des industries alimentaires (ANIA), la Fédération du commerce et de la distribution (FCD), la Fédération des entreprises de la beauté (FEBEA) et le Groupement français des fabricants de produits à usage unique pour l'hygiène, la santé et l'essuyage (GROUP'HYGIENE) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'ordonner la suspension de l'exécution de l'arrêté de la ministre de la transition écologique du 25 décembre 2020 portant modification de l'arrêté du 29 novembre 2016 relatif à la procédure d'agrément et portant cahier des charges des éco organismes de la filière des emballages ménagers, jusqu'à ce qu'il soit statué au fond sur sa légalité ; <br/>
<br/>
              2°) à titre subsidiaire, d'ordonner la suspension de l'exécution du 4° du II de l'annexe à cet arrêté ; <br/>
<br/>
              3°) d'enjoindre à l'Etat de modifier le 4° du II de l'annexe à cet arrêté, afin que l'obligation de fait de supprimer le " Point Vert " ne s'applique qu'à compter du 1er janvier 2022 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à chaque requérant au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - leur requête est recevable ;<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que les dispositions contestées préjudicient de manière grave et immédiate à leurs intérêts en tant qu'elles font abstraction des contraintes de temps incompressibles et des coûts élevés que représentent les opérations de production et, d'autre part, que les mesures transitoires d'application, qui prévoient un délai d'écoulement des stocks insuffisant et inadapté eu égard aux circonstances conjoncturelles résultant de la crise sanitaire, ne font pas obstacle à la caractérisation de l'urgence ; <br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ; <br/>
              - elles n'ont pas été soumises au Conseil national d'évaluation des normes en méconnaissance de l'article L. 1212-2 du code général des collectivités territoriales ; <br/>
              - l'article 62 de la loi n° 2020-105 du 10 février 2020 est inopposable faute d'avoir été notifié à la Commission européenne en méconnaissance de l'article 5 de la directive (UE) 2015/1535 ;<br/>
              - l'interdiction de fait d'apposer le " Point Vert " sur les produits et emballages mis sur le marché français constitue une entrave injustifiée à la libre circulation des marchandises au sein de l'Union européenne ;<br/>
              - cette interdiction méconnaît l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce qu'elle porte atteinte aux intérêts économiques des entreprises qu'ils représentent dès lors que celles-ci sont contraintes de concevoir de nouveaux emballages pour l'ensemble de leurs produits dans des délais très brefs et devront à nouveau faire face à des contraintes logistiques et financières afin de mettre en oeuvre, à compter de janvier 2022, les obligations d'information prévues par les articles 13 et 17 de la loi n° 2020-105 du 10 février 2020 et que les mesures contestées sont disproportionnées au regard des bénéfices qui pourraient en être attendus dès lors qu'aucun élément ne permet d'établir que la disparition du " Point Vert " puisse être de nature à modifier les gestes de tri des consommateurs et entraîner un impact significatif pour la préservation de l'environnement ; <br/>
              - les dispositions contestées sont entachées d'une erreur manifeste d'appréciation dès lors que les entreprises concernées sont contraintes, soit de supporter la pénalité financière prévue, soit de procéder, dans un intervalle de temps très bref, à deux modifications successives de leurs emballages afin de s'assurer de la conformité de leurs produits. <br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 4 et 11 mars 2021, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas satisfaite et qu'aucun moyen invoqué n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
<br/>
              II. Sous le n° 450164, par une requête et un mémoire en réplique, enregistrés les 25 février et 9 mars 2021 au secrétariat du contentieux du Conseil d'Etat, l'Association française des industries de la détergence (AFISE), l'Association nationale des industries alimentaires (ANIA), la Fédération du commerce et de la distribution (FCD), la Fédération des entreprises de la beauté (FEBEA) et le Groupement français des fabricants de produits à usage unique pour l'hygiène, la santé et l'essuyage (GROUP'HYGIENE) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'arrêté de la ministre de la transition écologique du 30 novembre 2020 relatif aux signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit, jusqu'à ce qu'il soit statué au fond sur sa légalité ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à chaque requérant au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soulèvent les mêmes moyens que ceux analysés sous la requête n° 450160. <br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 4 et 11 mars 2021, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas satisfaite et qu'aucun moyen invoqué n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la directive 94/62/CE du 20 décembre 1994 ;<br/>
              - la directive (UE) 2015/1535 du 9 septembre 2015 ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2020-105 du 10 février 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction a été fixée le 11 mars 2021 à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              3. Aux termes des deux premiers alinéas de l'article L. 540-1 du code de l'environnement : " I.- En application du principe de responsabilité élargie du producteur, il peut être fait obligation à toute personne physique ou morale qui élabore, fabrique, manipule, traite, vend ou importe des produits générateurs de déchets ou des éléments et matériaux entrant dans leur fabrication, dite producteur au sens de la présente sous-section, de pourvoir ou de contribuer à la prévention et à la gestion des déchets qui en proviennent (...). / Les producteurs s'acquittent de leur obligation en mettant en place collectivement des éco-organismes agréés dont ils assurent la gouvernance et auxquels ils transfèrent leur obligation et versent en contrepartie une contribution financière. (...) ". Aux termes des deux premiers alinéas de l'article L. 540-1-3 du même code : " Les contributions financières versées par les producteurs qui remplissent collectivement les obligations mentionnées à l'article L. 541-10 sont modulées, lorsque cela est possible au regard des meilleures techniques disponibles, pour chaque produit ou groupe de produits similaires, en fonction de critères de performance environnementale, parmi lesquels la quantité de matière utilisée, l'incorporation de matière recyclée, l'emploi de ressources renouvelables gérées durablement, la durabilité, la réparabilité, les possibilités de réemploi ou de réutilisation, la recyclabilité, la visée publicitaire ou promotionnelle du produit, l'absence d'écotoxicité et la présence de substances dangereuses telles que définies par le décret prévu à l'article L. 541-9-1, en particulier lorsque celles-ci sont susceptibles de limiter la recyclabilité ou l'incorporation de matières recyclées. / La modulation prend la forme d'une prime accordée par l'éco-organisme au producteur lorsque le produit remplit les critères de performance et celle d'une pénalité due par le producteur à l'éco-organisme lorsque le produit s'en s'éloigne. Les primes et pénalités sont fixées de manière transparente et non discriminatoire ".<br/>
<br/>
              4. L'article 62 de la loi du 10 février 2020 relative à la lutte contre le gaspillage et à l'économie circulaire, dite AGEC, a introduit un dernier alinéa à l'article L. 541-10-3 du code de l'environnement selon lequel : " Les signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit sont affectés d'une pénalité qui ne peut être inférieure au montant de la contribution financière nécessaire à la gestion des déchets. Ces signalétiques et marquages sont définis par arrêté du ministre chargé de l'environnement ". En application de cette disposition, par arrêté du 30 novembre 2020, la ministre de la transition écologique a défini les signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit comme étant " les figures graphiques représentant deux ou plusieurs flèches enroulées et inscrites dans un cercle, à l'exception : 1° De la signalétique définie à l'annexe de l'article R. 541-12-17 du code de l'environnement ; 2° Des signalétiques encadrées réglementairement par un autre Etat membre de l'Union européenne dès lors que ces signalétiques informent le consommateur que le produit fait l'objet d'une règle de tri ou que le produit est recyclable ; 3° Des logos associés à la marque sous laquelle est vendu ou distribué un produit ou associés à l'entreprise qui vend ou distribue le produit ". La pénalité due en cas d'utilisation de telles signalétiques et marquages et le calendrier de mise en oeuvre ont été fixés au 4° du I de l'annexe à l'arrêté du 25 décembre 2020 portant modification de l'arrêté du 29 novembre 2016 relatif à la procédure d'agrément et portant cahier des charges des éco-organismes de la filière des emballages ménagers : " Signalétiques et marquages pouvant induire une confusion sur la règle de tri : / A partir du 1er avril 2021, une pénalité équivalente au montant de la contribution hors primes ou autres pénalités est affectée aux emballages de produits sur lesquels est apposée une des signalétiques ou un des marquages définis en application du 5ème alinéa de l'article L. 541-10-3. Sont exemptés de cette pénalité : / - les produits emballés ou les emballages fabriqués ou importés avant le 1er avril 2021 qui bénéficient d'un délai d'écoulement des stocks n'excédant pas 18 mois à compter de cette date ; /- les produits emballés ou les emballages fabriqués ou importés sur lesquels cette signalétique ou ce marquage sont apposés en application d'une obligation réglementaire fixée par un autre Etat membre de l'Union européenne, lorsque le producteur commercialise le produit dans un emballage identique sur le territoire national et dans cet autre Etat membre, et jusqu'au 1er janvier 2022. Ces emballages ou produits emballés avant cette date bénéficient en outre d'un délai d'écoulement des stocks n'excédant pas 12 mois à compter de cette date ".<br/>
<br/>
              Sur les requêtes en référé : <br/>
<br/>
              5. L'Association française des industries de la détergence (AFISE), l'Association nationale des industries alimentaires (ANIA), la Fédération du commerce et de l'industrie (FCD), la Fédération des entreprises de la beauté (FEBEA) et le Groupement français des produits à usage unique pour l'hygiène, la santé et l'essuyage (Group'Hygiène) demandent au juge des référés, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, sous le n° 450160, la suspension de l'exécution de l'arrêté du 25 décembre 2020 modifiant le cahier des charges des éco-organismes et, sous le n° 450164, la suspension de l'exécution de l'arrêté du 30 novembre 2020 relatif aux signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit.<br/>
<br/>
              6. Eu égard aux moyens soulevés dans la requête n° 450160, les requérants doivent être regardés comme ne demandant que la suspension de l'exécution des dispositions du 4° du II de l'annexe à l'arrêté du 25 décembre 2020 modifiant le cahier des charges des éco-organismes, cité au point 4 ci-dessus, lesquelles sont divisibles des autres dispositions prévues dans cet arrêté et son annexe. <br/>
<br/>
              Sur le doute sérieux quant à la légalité des dispositions contestées : <br/>
<br/>
              7. Il ressort des travaux parlementaires relatifs à l'article 62 de la loi AGEC que le dernier alinéa de l'article L. 541-10-3 du code de l'environnement a pour objet de pénaliser financièrement les producteurs qui utilisent des emballages sur lesquels est apposée la signalétique dite " Point vert " au motif que ce marquage qui atteste du paiement par les producteurs d'emballages d'une contribution financière aux éco-organismes chargés de la gestion des déchets d'emballages, induit en erreur les consommateurs sur les règles de tri et contribue directement à des erreurs de tri. Ainsi, l'arrêt du 30 novembre 2020 qui définit les signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit comme étant les " figures graphiques représentant deux ou plusieurs flèches enroulées et inscrites dans un cercle " décrit précisément et uniquement la signalétique " Point Vert " qui, ainsi qu'il résulte de l'instruction, est utilisée dans 29 pays en Europe dont l'Espagne et Chypre dans lesquels il est obligatoire.<br/>
<br/>
              8. D'une part, bien qu'indistinctement applicables à tous les produits commercialisés sur le marché français, qu'ils soient fabriqués en France ou importés, les dispositions contestées qui rendent plus onéreux, du fait de la pénalité financière qu'elles instituent, les emballages revêtus de la signalétique " Point Vert " ont pour objet et pour effet de dissuader les producteurs d'utiliser de tels emballages en France et, par suite, les contraignent de facto à prévoir des emballages différents en fonction du lieu de commercialisation et à organiser des circuits de distribution cloisonnés de façon à s'assurer que les produits dont les emballages sont revêtus du " Point Vert " ne soient pas commercialisés en France. A cet égard, de telles dispositions, qui ne peuvent être regardées comme de simples modalités de vente dès lors que la pression qu'elles exercent sur les producteurs est directement liée au type d'emballage des marchandises commercialisées, affectent les caractéristiques des produits. <br/>
<br/>
              9. D'autre part, si le ministre soutient que les dispositions contestées sont nécessaires pour satisfaire à une exigence impérative tenant à la protection de l'environnement dès lors que le geste de tri contribue à économiser des ressources naturelles, à éviter le gaspillage et à limiter la pollution, il n'apporte pas, par la seule référence à un rapport de la Cour des Comptes de 2016 et la production d'une enquête d'opinion réalisée en juin 2020 relative à la perception par les Français des logos concernant les consignes de recyclage, d'éléments permettant de considérer que le marquage " Point Vert ", qui est apposé sur des emballages généralement recyclables, serait effectivement une cause significative d'erreurs de tri perturbant la chaine de recyclage et que sa disparition serait de nature, à elle seule, à modifier les gestes de tri. Il ne conteste pas non plus que la confusion des consommateurs sur la signification, purement financière, du " Point Vert " pourrait être corrigée par des mesures moins contraignantes, notamment grâce à une information adéquate. Par ailleurs, quand bien même l'article 130 de la loi AGEC a fixé au 1er janvier 2021 la date d'entrée en vigueur de l'article L. 541-10-3 du code de l'environnement dans sa rédaction issue de l'article 62 de cette même loi, l'application de la pénalité contestée, dès le 1er avril 2021, pour les produits emballés ou les emballages fabriqués ou importés après cette date et revêtus de la signalétique " Point Vert " résulte seulement du 4° du II de l'annexe à l'arrêté du 25 décembre 2020. Les producteurs sont ainsi contraints de concevoir de nouveaux emballages ou d'adapter leur circuit de distribution dans un délai très bref, sous peine d'être financièrement pénalisés. <br/>
<br/>
              10. Dans ces conditions, le moyen tiré de ce que les dispositions contestées sont constitutives d'une mesure d'effet équivalent à une restriction quantitative, au sens de l'article 34 du traité sur le fonctionnement de l'Union européenne, disproportionnée au regard de l'objectif poursuivi est de nature, en l'état de l'instruction, à créer un doute sérieux quant à leur légalité. <br/>
<br/>
              Sur la condition d'urgence : <br/>
<br/>
              11. Ainsi qu'il a été dit, les dispositions contestées ont pour effet de contraindre les producteurs qui ne souhaitent pas être soumis à la pénalité prévue par l'arrêté du 25 décembre 2020 de modifier leurs emballages ou leurs circuits de distribution afin de ne plus commercialiser, sur le marché français, des produits emballés ou des emballage fabriqués ou importés après le 1er avril 2021 revêtus de la signalétique " Point Vert ". Dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie. <br/>
<br/>
              12. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens qu'ils invoquent, les requérants sont fondés à demander la suspension de l'exécution du 4° du II de l'annexe à l'arrêté du 25 décembre 2020 portant modification du cahier des charges des éco-organismes et de l'arrêté du 30 novembre 2020 relatif aux signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit. Compte tenu de la suspension ainsi décidée, il n'y a pas lieu d'enjoindre au ministre à modifier le calendrier prévu par le 4° du II de l'annexe à l'arrêté du 25 décembre 2020. <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 3 000 euros à verser aux requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'exécution du 4° du II de l'annexe à l'arrêté du 25 décembre 2020 portant modification du cahier des charges des éco-organismes et de l'arrêté du 30 novembre 2020 relatif aux signalétiques et marquages pouvant induire une confusion sur la règle de tri ou d'apport du déchet issu du produit est suspendue.<br/>
Article 2 : L'Etat versera aux requérants la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à l'Association française des industries de la détergence, première dénommée, pour l'ensemble des requérants, et à la ministre de la transition écologique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
