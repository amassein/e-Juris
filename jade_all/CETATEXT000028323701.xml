<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323701</ID>
<ANCIEN_ID>JG_L_2013_12_000000370901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/37/CETATEXT000028323701.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 06/12/2013, 370901, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370901.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 et 20 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département de la Corse du sud, représenté par le président du conseil général ; le département de la Corse du sud demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300526 du 19 juillet 2013 par laquelle le juge des référés du tribunal administratif de Bastia, statuant en application de l'article L. 551-1 du code de justice administrative a, à la demande de l'agence MB Architecture, annulé la procédure de passation d'un marché de maîtrise d'oeuvre pour la réhabilitation de l'établissement thermal de Guagno-les-Bains ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de l'agence MB Architecture ; <br/>
<br/>
              3°) de mettre à la charge de l'agence MB Architecture le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code des marchés publics ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat du département de la Corse du sud et à la SCP Coutard, Munier-Apaire, avocat de l'agence MB Architecture ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bastia que le département de la Corse du sud a lancé une procédure négociée pour la passation d'un marché public ayant pour objet la maîtrise d'oeuvre des travaux de réhabilitation de l'établissement thermal de Guagno-les-Bains ; que, par l'ordonnance attaquée du 19 juillet 2013, le juge des référés, saisi par l'agence MB Architecture sur le fondement de l'article L. 551-1 du code de justice administrative, a annulé, pour trois motifs,  la procédure de passation de ce marché ;<br/>
<br/>
              Sur l'ordonnance en tant qu'elle juge que des manquements allégués sont susceptibles d'avoir lésé l'agence MB architecture : <br/>
<br/>
              3. Considérant que le juge des référés a pu, sans erreur de droit ni contradiction de motifs, juger que les manquements allégués devant lui par l'agence MB Architecture, relatifs à la communication incomplète et tardive des critères de sélection des offres par le pouvoir adjudicateur, ainsi qu'au choix par ce dernier d'une hiérarchisation et non d'une pondération de ces critères, avaient été susceptibles, eu égard à leur portée et au stade de la procédure auquel ils se rapportaient, de léser l'agence MB Architecture et pouvaient par suite être utilement invoqués par elle à l'appui de son référé précontractuel ; que le juge des référés a pu à cet égard, sans erreur de droit, regarder comme sans incidence la triple circonstance que l'agence MB Architecture ne s'était pas plainte de ces manquements au cours de la procédure d'attribution, qu'elle avait pu présenter une offre répondant aux caractéristiques exigées, et qu'elle avait obtenu, à une exception près, les mêmes notes que le candidat retenu ; <br/>
<br/>
              Sur la  confusion qu'aurait opérée le juge des référés  entre méthode de notation et critère de sélection des offres :<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le juge des référés n'a pas regardé comme un critère de sélection des offres, mais comme une méthode de notation, le processus par lequel le pouvoir adjudicateur a, au vu des résultats obtenus sur les différents critères, procédé au classement des offres ; que, par suite, le moyen tiré de ce que le juge des référés aurait inexactement qualifié cette méthode de critère de sélection des offres ne peut qu'être écarté  ;<br/>
<br/>
              Sur le bien fondé des trois motifs d'annulation retenus par le juge des référés :<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes du II de l'article 53 du code des marchés publics " Pour les marchés passés selon une procédure formalisée autre que le concours et lorsque plusieurs critères sont prévus, le pouvoir adjudicateur précise leur pondération (...). /Le pouvoir adjudicateur qui estime pouvoir démontrer que la pondération n'est pas possible notamment du fait de la complexité du marché, indique les critères par ordre décroissant d'importance " ;<br/>
<br/>
              6. Considérant, que le juge des référés a pu, par une appréciation souveraine qui n'est pas entachée de dénaturation, juger que, en dépit des difficultés techniques s'attachant au projet de réhabilitation de l'établissement thermal, le département de la Corse du sud ne démontrait pas, pour le marché de maîtrise d'oeuvre en cause, l'impossibilité technique d'une pondération des critères de sélection des offres choisis par lui ; que le juge des référés a pu en déduire, sans commettre d'erreur de droit, que le pouvoir adjudicateur avait manqué, pour ce premier motif, à ses obligations de publicité et de mise en concurrence ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que, pour assurer le respect des principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, l'information appropriée des candidats sur les critères d'attribution d'un marché public est nécessaire dès l'engagement de la procédure de passation du marché ; que, s'agissant des marchés passés selon une procédure formalisée autre que le concours, l'article 53 du code des marchés publics précise que, compte tenu de l'obligation qui pèse sur le pouvoir adjudicateur de pondérer ou, lorsqu'il est dans l'impossibilité de procéder à cette pondération, de hiérarchiser les critères qu'il entend retenir pour attribuer le marché, " les critères ainsi que leur pondération ou leur hiérarchisation sont indiqués dans l'avis d'appel public à la concurrence ou dans les documents de la consultation " ;<br/>
<br/>
              8. Considérant que le juge des référés a relevé, par une appréciation souveraine exempte de dénaturation, que la mention, dans le règlement de la consultation, de la liste des critères de " négociation des offres " n'apportait pas aux candidats une information suffisante sur l'existence ou non d'autres critères susceptibles d'intervenir dans la sélection des offres ; que c'est également par une appréciation souveraine, qui n'est pas entachée de dénaturation, qu'il a estimé que la portée précise d'un de ces critères était, en outre, difficile à appréhender ; que, par suite, c'est sans erreur de droit qu'il a jugé que,  en dépit des précisions qui avaient été apportées aux candidats quelques jours avant la date limite de remise de leurs offres, le pouvoir adjudicateur avait manqué, pour ce deuxième motif, à ses obligations de publicité et de mise en concurrence ;<br/>
<br/>
              9. Considérant, enfin, que lorsque le pouvoir adjudicateur décide, pour mettre en oeuvre les critères de sélection des offres, de faire usage de sous-critères pondérés ou hiérarchisés, il est tenu de porter à la connaissance des candidats la pondération ou la hiérarchisation de ces sous-critères lorsque, eu égard à leur nature et à l'importance de cette pondération ou hiérarchisation, ils sont susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection, et doivent en conséquence être eux-mêmes regardés comme des critères de sélection ;<br/>
<br/>
              10. Considérant que le juge du référé a exactement qualifié les faits qui lui étaient soumis en jugeant que, eu égard à la méthode de notation mise en oeuvre par le département, la hiérarchisation des différents sous-critères du critère des " engagements contractuels " des candidats, notamment celle du sous-critère relatif au prix des offres, avait été susceptible d'exercer une influence sur la présentation des offres et que ce sous-critère devait ainsi être assimilé à un critère de sélection ; qu'ayant souverainement estimé, sans dénaturer les pièces du dossier, qu'aucune information n'avait été apportée aux candidats sur la hiérarchisation de ce sous-critère, il a pu, sans commettre d'erreur de droit, juger que le pouvoir adjudicateur avait manqué, pour ce troisième motif, à ses obligations de publicité et de mise en concurrence ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le pourvoi du département de la Corse du sud doit être rejeté ; qu'il en va de même, par voie de conséquence, de ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge du département, au titre des mêmes dispositions, le versement d'une somme de 3 000 euros à l'agence MB Architecture ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
    --------------<br/>
Article 1er : Le pourvoi du département de la Corse du sud est rejeté.<br/>
Article 2 : Le département de la Corse du sud versera une somme de 3 000 euros à l'agence MB Architecture au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au département de la Corse du sud et à l'agence MB Architecture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
