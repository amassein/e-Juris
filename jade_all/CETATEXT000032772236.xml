<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032772236</ID>
<ANCIEN_ID>JG_L_2016_06_000000395481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/77/22/CETATEXT000032772236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 17/06/2016, 395481, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:395481.20160617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1. Sous le numéro 395481, par une protestation et deux nouveaux mémoires, enregistrés les 22 décembre 2015, 29 février 2016 et 30 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. G...J...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les opérations électorales qui se sont déroulées les 6 et 13 décembre 2015 pour l'élection des membres du conseil régional de la Réunion ;<br/>
<br/>
              2°) de rejeter le compte de campagne de M. Didier Robert, de le déclarer inéligible, ainsi que les 46 autres candidats de la liste " Réunionnous " et d'annuler leur élection en qualité de conseillers régionaux.<br/>
<br/>
<br/>
              2. Sous le numéro 395502, par une protestation, enregistrée au secrétariat du contentieux du Conseil d'Etat le 22 décembre 2015, M. I...C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les mêmes opérations électorales qui se sont déroulées les 6 et 13 décembre 2015 pour l'élection des membres du conseil régional de la Réunion ;<br/>
<br/>
              2°) de rejeter le compte de campagne des candidats de la " liste de rassemblement " conduite par Mme H...A...et M. G...D..., de déclarer ceux-ci, ainsi que M. F...E..., inéligibles et d'annuler leur élection en qualité de conseillers régionaux.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le règlement (UE) n° 1303/2013 du Parlement européen et du Conseil du 17 décembre 2013 ; <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. G...J...D...et à la SCP Gatineau, Fattaccini, avocat de M. Didier Robert ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du deuxième tour de scrutin qui a été organisé à la Réunion, pour les élections régionales, le 13 décembre 2015, la liste " Réunionnous ", conduite par M. Didier Robert a obtenu 173 592 voix, soit 52,69 % des suffrages exprimés. La liste conduite par Mme H...A..., seule autre liste ayant présenté des candidats au second tour, a obtenu 155 896 voix, soit 47,31 % des suffrages exprimés. En application des modalités de répartition des sièges prévues par le troisième alinéa de l'article L. 338 du code électoral, 29 sièges, sur les 45 que compte le conseil régional, ont été attribués à la liste conduite par M. Didier Robert et 16 sièges à la liste conduite par MmeA.... M. G...D..., tête de liste au premier tour du scrutin, candidat au second tour du scrutin sur la liste conduite par Mme A...a formé une protestation tendant, d'une part, à l'annulation des résultats de ce scrutin et, d'autre part, à ce qu'en conséquence du rejet du compte de campagne de M. Didier Robert, celui-ci, ainsi que l'ensemble des autres candidats figurant sur sa liste soient déclarés inéligibles. M. C... a également formé une protestation, tendant, d'une part, à l'annulation du même scrutin et, d'autre part, à ce qu'en conséquence du rejet de leur compte de campagne, MmeA..., M. G... D... et M. E...soient déclarés inéligibles. Il y a lieu de joindre ces protestations pour statuer par une même décision.<br/>
<br/>
              Sur la protestation n° 395481 présentée par M. G...D... :<br/>
<br/>
              En ce qui concerne la régularité du scrutin :<br/>
<br/>
              2. En premier lieu, aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". <br/>
<br/>
              3. Il résulte de l'instruction, ainsi que le soutient M. G...D..., que les numéros 34 à 38 du magazine de la région Réunion " Réunion'mag ", parus dans le délai de six mois prévu par les dispositions qui viennent d'être citées, ont présenté les réalisations de la collectivité, et, en particulier, la mise en oeuvre du programme touristique intitulé " Iles Vanille ", la réhabilitation du musée Stella Matutina, ou encore le bilan et les actions de la région dans le domaine de la formation et de l'emploi des jeunes ou en matière d'apprentissage. Ces éditions contenaient également un éditorial signé de M. Didier Robert, président de la région et candidat tête de liste, ainsi que diverses photographies présentant celui-ci ou d'autres conseillers régionaux, également candidats sur sa liste. Toutefois, le contenu des articles et de l'éditorial, dont la maquette était identique à celle des éditions parues au cours des précédentes années, revêtait un caractère informatif et étaient dépourvus de tout élément de promotion électorale. Ils ont été publiés en relation avec des événements ou des actions liées à l'actualité de la région. Ni ces articles, ni les photographies, ne mettaient exagérément en évidence le président de la région ou les conseillers régionaux figurant sur sa liste. Dès lors, la publication de ces numéros du magazine régional n'a pas revêtu la nature d'une campagne de promotion publicitaire au sens du second alinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              4. Il en va de même, en tout état de cause, de la campagne d'information financée par la région sur l'évolution du chantier de la nouvelle route du littoral, parue dans divers journaux ou magazines, et de l'interview de M. Didier Robert sur ce sujet parue dans le magazine Memento, au demeurant publiés avant le délai de six mois, qui revêtaient un caractère informatif, ne contenaient pas d'élément de polémique électorale, et ne mettaient pas en évidence de manière exagérée l'action personnelle du président de la région sortant, ni des membres du conseil régional figurant sur sa liste. Si les campagnes d'information annonçant le lancement des programmes des Fonds européens de développement régional rappelaient le rôle de la région dans la gestion de ces fonds, et si le slogan de l'une d'entre elle pouvait évoquer le slogan de campagne électorale de la liste conduite par M. Didier Robert, il résulte de l'instruction que ces campagnes relatives aux fonds européens, dont la mise en oeuvre dans un calendrier contraint résultait d'une obligation faite à la région, en qualité d'autorité de gestion des fonds, par le règlement du 17 décembre 2013 du Parlement européen et du Conseil, avaient pour objet d'informer les bénéficiaires potentiels des objectifs fixés pour l'attribution de ces fonds et des modalités d'organisation retenues par la région pour leur attribution. Cette campagne n'a ainsi, elle non plus, pas revêtu la nature d'une campagne de promotion publicitaire prohibée par le second alinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              5. Contrairement à ce que soutient M. G...D..., n'ont pas, non plus, revêtu ce caractère, eu égard à leur teneur objective, à leur caractère informatif et dépourvu de tout élément de promotion électorale, ni le publi-reportage financé par la région mettant en valeur l'action de l'agence pour l'observation de la Réunion, l'aménagement et l'habitat (Agorah), ni les éditions de juillet et novembre 2015 du journal municipal de la commune de Saint Philippe, " Saint-Philippe Mag ", les articles parus dans ce dernier étant en outre, pour la plupart, en relation avec des événements de l'actualité récente de la commune, et les photographies ne présentant le maire de la commune, candidat sur la liste de M. Didier Robert, ou ce dernier, que dans leurs fonctions d'élus.  <br/>
<br/>
              6. Il ne résulte pas non plus de l'instruction que les articles publiés dans le magazine " Maisons Créole ", qui présentaient, il est vrai, de manière favorable l'action de la région dans la rénovation du musée Stella Matutina, et pour la nouvelle route du littoral, aient été publiés autrement que sous la seule responsabilité éditoriale du journal et puissent être regardés comme une " campagne de promotion publicitaire " des réalisations ou de la gestion de la région.<br/>
<br/>
              7. Ni les " visites de terrain " effectuées au cours du délai de six mois précédant les élections par le président de la chambre de commerce et d'industrie, également candidat sur la liste conduite par M.D..., dont il résulte de l'instruction qu'elles ont été réalisées en cette qualité par l'intéressé, ainsi qu'il avait coutume de le faire auparavant, en relation avec des actions relevant de la compétence de cet établissement, ni la visite du chantier de construction du Lycée nord Bois-de-Nèfles, effectuée par M. Didier Robert, en qualité de président du conseil régional, au début des travaux, et dont les supports de communication ont revêtu un caractère informatif, ni la conférence de presse annonçant le déploiement d'un réseau wifi gratuit, dont il ne résulte pas de l'instruction qu'elle ait revêtu un caractère autre qu'informatif, et dont la date d'organisation, un mois avant le scrutin, s'expliquait par la reprise des travaux de déploiement du réseau, consécutive à la décision du Conseil d'Etat mettant fin au contentieux qui avait conduit à son interruption, ne peuvent être regardées comme des campagnes de promotion publicitaire.   <br/>
<br/>
              8. En deuxième lieu, aux termes de l'article L. 106 du code électoral : " Quiconque, par des dons ou libéralités en argent ou en nature, par des promesses de libéralités, de faveurs, d'emplois publics ou privés ou d'autres avantages particuliers, faits en vue d'influencer le vote d'un ou de plusieurs électeurs aura obtenu ou tenté d'obtenir leur suffrage, soit directement, soit par l'entremise d'un tiers, quiconque, par les mêmes moyens, aura déterminé ou tenté de déterminer un ou plusieurs d'entre eux à s'abstenir, sera puni de deux ans d'emprisonnement et d'une amende de 15 000 euros./Seront punis des mêmes peines ceux qui auront agréé ou sollicité les mêmes dons, libéralités ou promesses. ". En vertu des dispositions de l'article L. 335 du même code : " Les conseillers régionaux et les membres de l'Assemblée de Corse sont élus dans les conditions fixées par les dispositions du titre Ier du livre Ier du présent code et par celles du présent livre (...) ". S'il n'appartient pas au juge de l'élection de faire application de ces dispositions en ce qu'elles édictent des sanctions pénales, il lui revient, en revanche, de rechercher si des pressions telles que définies par celles-ci ont été exercées sur les électeurs et ont été de nature à altérer la sincérité du scrutin. <br/>
<br/>
              9. Il ne résulte pas de l'instruction que l'octroi d'aides de 100 à 150 euros à des entreprises pour l'embauche de stagiaires, qui était pratiquée de manière récurrente par la région, ou l'annonce d'un programme d'aide aux entreprises par la chambre de commerce et d'industrie, dans le cadre des conventions de partenariat que celle-ci passait de manière habituelle, ou l'annonce du déploiement d'un réseau wifi en accès gratuit, de nature à favoriser l'accès aux technologies de l'information, aient revêtu la nature de promesses de dons de nature à altérer la sincérité du scrutin. Il en va de même de la subvention de 90 000 euros accordée à l'association des maires du département de la Réunion, dont il résulte de l'instruction qu'elle a été accordée pour la mise en oeuvre d'un programme de formation, effectivement réalisé depuis, dont les thématiques relevaient, pour partie, des compétences de la région.   <br/>
<br/>
              10. En troisième lieu, ni l'organisation, en dehors de la période de campagne officielle, de réunions électorales, qui sont tenues librement, en vertu des lois du 30 juin 1881 sur la liberté de réunion et du 28 mars 1907 relative aux réunions publiques, ni la circulation de véhicules aux couleurs de la liste conduite par M. Didier Robert n'ont constitué, en l'espèce, des manoeuvres de nature à altérer la sincérité du scrutin. <br/>
<br/>
              11. En revanche, le journal de la commune de Cilaos, paru pour la première fois en novembre 2015 et dont l'éditorial, signé du maire de la commune, candidat aux élections régionales sur la liste conduite par M. Didier Robert, qui présentait de manière particulièrement laudative, voire polémique, le bilan de l'action de la région dans la commune, a revêtu le caractère d'une promotion publicitaire, alors même que M. Didier Robert, candidat tête de liste et président de la région, n'aurait pas été à l'origine de cette publication. Toutefois, cette irrégularité n'a pas été de nature, notamment au regard de sa diffusion, à altérer la sincérité du premier tour du scrutin, eu égard à l'écart séparant le nombre de voix obtenues par les différentes listes du seuil permettant à une liste de se maintenir au second tour. Elle n'a pas non plus altéré la sincérité du deuxième tour du scrutin, ni, eu égard à l'écart entre le nombre de voix obtenues par les deux listes présentes à ce tour de scrutin, pour l'attribution de la prime majoritaire, ni pour la répartition des sièges à la proportionnelle ou pour l'attribution du dernier siège, qui est revenu à la liste conduite par MmeA..., selon la règle de la plus forte moyenne. <br/>
<br/>
              12. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les fins de non recevoir opposées en défense, que les conclusions de M. G...D...tendant à l'annulation du scrutin ne peuvent qu'être rejetées.  <br/>
<br/>
              En ce qui concerne les conclusions à fin d'inéligibilité :<br/>
<br/>
              13. Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. ". Le premier alinéa de l'article L. 52-11 du même code dispose : " Pour les élections auxquelles l'article L. 52-4 est applicable, il est institué un plafond des dépenses électorales, autres que les dépenses de propagande directement prises en charge par l'Etat, exposées par chaque candidat ou liste de candidats, ou pour leur compte, au cours de la période mentionnée au même article. ".  L'article L. 52-11-1 de ce code institue un dispositif de remboursement forfaitaire partiel des dépenses électorales par l'Etat et prévoit que ce remboursement n'est pas versé aux candidats dont le compte de campagne est rejeté. L'article L. 52-12 de ce code prévoit que " chaque candidat ou candidat tête de liste " soumis au plafonnement prévu à l'article L. 52-11 est tenu d'établir un compte de campagne " retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 ". L'article L. 52-15 du même code détermine les compétences de la commission nationale des comptes de campagne et des financements politiques en matière de contrôle des comptes de campagne, de détermination du montant du remboursement forfaitaire dû par l'Etat et de fixation de la somme qu'un candidat est tenu de reverser au Trésor public en cas de dépassement du plafond des dépenses électorales. Aux termes de l'article L. 118-3 du code électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales ". Son article L. 118-4 dispose que : " Saisi d'une contestation formée contre l'élection, le juge de l'élection peut déclarer inéligible, pour une durée maximale de trois ans, le candidat qui a accompli des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin ".<br/>
<br/>
              14. En premier lieu, contrairement à ce qu'a estimé la commission nationale des comptes de campagne et des financements politiques, ainsi qu'il a été dit, ni la publication d'une interview de M. Didier Robert dans le magazine Memento, ni les éditions du journal de la région " Réunion'Mag " dans les six mois qui ont précédé le scrutin n'ont revêtu le caractère de campagnes de promotion publicitaire au sens de l'article L. 52-1 du code électoral ni de dons prohibés par l'article L. 52-8 du code électoral. Le coût de l'édition de la publication et de la distribution du journal municipal de Cilaos a revêtu le caractère d'un tel don, dès lors que sa liste a bénéficié de ces opérations. Toutefois, compte tenu de la circonstance selon laquelle M. Didier Robert a inclus les coûts correspondants dans son compte de campagne après qu'il eut été informé de la teneur de cette publication, il n'y a pas lieu de prononcer le rejet du compte de campagne. Il en résulte qu'il n'y a pas lieu de prononcer l'inéligibilité de M. Didier Robert, ni des autres candidats figurant sur la liste qu'il conduisait, sur le fondement de l'article L. 118-3 du code électoral.<br/>
<br/>
              15. En second lieu, eu égard à sa nature et à ses effets, l'irrégularité relevée au point 11, dont il ne résulte par ailleurs pas de l'instruction que M. B...en ait été informé, ne peut être regardée comme une manoeuvre frauduleuse ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin. Ainsi il n'y a pas lieu de prononcer l'inéligibilité de M. Didier sur le fondement de l'article L. 118-4 du code électoral.  <br/>
<br/>
              16. Il résulte de ce qui précède que les conclusions de M. Robert tendant à ce que M. Didier Robert et les autres candidats figurant sur sa liste soient déclarés inéligibles doivent être rejetées. <br/>
<br/>
              Sur la protestation n° 395502 présentée par M.C... :<br/>
<br/>
              17. Si à l'occasion du meeting électoral organisé le 18 octobre à Saint-Leu par la " liste de rassemblement ", les militants ont pu bénéficier de transports gratuits, il ne résulte pas de l'instruction, contrairement à ce que soutient M.C..., que le montant de ces transports aurait été pris en charge par la commune de Saint-Denis ou celle de Saint-Leu. Il ne résulte pas, par ailleurs, des seules photographies de rassemblements électoraux produites par M. C..., qu'à l'occasion des meetings organisés par cette même liste entre les deux tours des élections, le 10 décembre à Saint-Denis, le 11 décembre à Saint-Joseph et le 12 décembre à Saint-Leu, des services de transport gratuits auraient été mis à la disposition des militants. Ainsi, M. C...n'est pas fondé à soutenir que le scrutin aurait été entaché d'une irrégularité l'entachant d'insincérité. <br/>
<br/>
              18. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les fins de non recevoir opposées en défense, que M. C...n'est pas fondé à demander l'annulation des opérations électorales. Sa demande tendant à ce que les dépenses résultant des irrégularités qu'il invoquait soient rapportées aux comptes de campagnes des candidats figurant sur la " liste de rassemblent " ne peut également qu'être rejetée. Il en va de même de sa demande tendant au rejet de ces comptes de campagne et à ce que MmeA..., M. G...D...et M. E...soient déclarés inéligibles. <br/>
<br/>
              19. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...la somme que demande M. E...au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Les protestations de M. G...D...et de M. I...C...sont rejetées.<br/>
<br/>
Article 2 : Les conclusions présentées par M. E...sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. G...D..., M. I...C..., M. Didier Robert, M. F...E..., à la commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
