<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188989</ID>
<ANCIEN_ID>JG_L_2016_03_000000385130</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 09/03/2016, 385130</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385130</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:385130.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 385130, par une requête et deux mémoires en réplique, enregistrés les 14 octobre 2014, 2 avril 2015 et 8 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Astrazeneca demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du collège des directeurs de l'Union nationale des caisses d'assurance maladie (UNCAM) du 24 juin 2014 relative à la procédure d'accord préalable pour bénéficier de la prise en charge de la rosuvastatine ou, subsidiairement, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne d'une question préjudicielle relative à l'interprétation de l'article 6 de la directive du Conseil du 21 décembre 1988 ;<br/>
<br/>
              2°) de mettre à la charge de l'UNCAM la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 385629, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 novembre 2014, 21 janvier 2015 et 2 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la société Astrazeneca France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de l'UNCAM du 24 juin 2014 relative à la procédure d'accord préalable des prestations de l'article L. 315-1 du code de la sécurité sociale en application des dispositions de l'article L. 315-2 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'UNCAM la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - le code civil ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Union nationale des caisses d'assurance maladie ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article L. 315-1 du code de la sécurité sociale : " Le contrôle médical porte sur tous les éléments d'ordre médical qui commandent l'attribution et le service de l'ensemble des prestations de l'assurance maladie, maternité et invalidité ainsi que des prestations prises en charge en application des articles L. 251-2 et L. 254-1 du code de l'action sociale et des familles " ; qu'aux termes de l'article L. 315-2 du même code : " Les avis rendus par le service du contrôle médical portant sur les éléments définis au I de l'article L. 315-1 s'imposent à l'organisme de prise en charge. / Le bénéfice de certaines prestations mentionnées au I de l'article L. 315-1 peut être subordonné à l'accord préalable du service du contrôle médical. Cet accord préalable peut être exigé pour les prestations dont : / - la nécessité doit être appréciée au regard d'indications déterminées ou de conditions particulières d'ordre médical ; / - la justification, du fait de leur caractère innovant ou des risques encourus par le bénéficiaire, doit être préalablement vérifiée eu égard notamment à l'état du bénéficiaire et aux alternatives thérapeutiques possibles ; / - le caractère particulièrement coûteux doit faire l'objet d'un suivi particulier afin d'en évaluer l'impact sur les dépenses de l'assurance maladie ou de l'Etat en ce qui concerne les prestations servies en application des articles L. 251-2 et L. 254-1 du code de l'action sociale et des familles. / Il est précisé lors de la prise en charge des prestations mentionnées au I de l'article L. 315-1 que leur bénéfice est, le cas échéant, subordonné à l'accord préalable mentionné ci-dessus. / Les conditions d'application des alinéas précédents sont fixées par décision du collège des directeurs de l'Union nationale des caisses d'assurance maladie (...) " ; que ces dispositions sont susceptibles d'être appliquées à toutes les prestations de l'assurance maladie, maternité et invalidité, parmi lesquelles figure le remboursement des médicaments ;<br/>
<br/>
              2. Considérant que, par deux requêtes qu'il y a lieu de joindre, la société Astrazeneca demande l'annulation de deux décisions du collège des directeurs de l'Union nationale des caisses d'assurance maladie (UNCAM) du 24 juin 2014 qui, pour l'une, fixe les conditions d'application des deuxième à sixième alinéas de l'article L. 315-2 du code de la sécurité sociale, relatifs à la procédure d'accord préalable du service du contrôle médical, et, pour l'autre, subordonne la prise en charge de toute instauration d'un traitement par rosuvastatine à compter du 1er novembre 2014 à un tel accord préalable ; <br/>
<br/>
              Sur la légalité de la décision du 24 juin 2014 relative à la procédure d'accord préalable des prestations de l'article L. 315-1 du code de la sécurité sociale prise en application des dispositions de l'article L. 315-2 du code de la sécurité sociale :<br/>
<br/>
              3. Considérant, en premier lieu, que, par sa décision du 24 juin 2014, prise sur le fondement de l'article L. 315-2 du code de la sécurité sociale, le collège des directeurs de l'UNCAM, établissement public à caractère administratif, a ainsi précisé, à son article 1er, les critères fixés par la loi en vertu desquels le bénéfice de certaines prestations peut être subordonné à l'accord préalable du service du contrôle médical : " - le respect des indications déterminées ou des conditions particulières d'ordre médical s'apprécie notamment au regard des recommandations de la Haute Autorité de santé, de l'Agence nationale de sécurité du médicament et des produits de santé ou de l'Institut national du cancer ; / - le caractère innovant de la prestation et les risques encourus par le bénéficiaire s'apprécient notamment au regard des études de pharmacovigilance ou de pharmaco-épidémiologie des autorités sanitaires ou de l'assurance maladie, des recommandations de la Haute Autorité de santé ou des études d'équivalence thérapeutique ; / - le caractère particulièrement coûteux de la prestation s'apprécie soit pour des prestations qui sont intrinsèquement coûteuses, soit pour des prestations dont l'utilisation massive ou non conforme aux recommandations engendre un coût global important pour l'assurance maladie " ; <br/>
<br/>
              4. Considérant, d'une part, que les précisions apportées aux conditions définies par le législateur pour décider de subordonner le bénéfice de certaines prestations de l'assurance maladie, maternité et invalidité à l'accord préalable du service du contrôle médical ne relèvent pas des " principes fondamentaux (...) de la sécurité sociale " dont l'article 34 de la Constitution réserve la détermination à la loi ;<br/>
<br/>
              5. Considérant que, par les dispositions, citées au point 3, de l'article 1er de la décision attaquée, le collège des directeurs de l'UNCAM s'est borné à préciser, sans les méconnaître, les conditions, définies par le législateur à l'article L. 315-2 du code de la sécurité sociale, que les prestations doivent remplir pour que leur prise en charge puisse être subordonnée à l'accord préalable du service du contrôle médical, ainsi qu'il lui était loisible de le faire ; que, ce faisant, il n'a pas empiété sur la compétence du législateur ; que la société requérante n'est pas plus fondée à soutenir qu'il aurait méconnu, en ajoutant à la loi ou, à l'inverse, en ne précisant pas suffisamment les conditions légales, les dispositions de l'article L. 315-2 ; <br/>
<br/>
              6. Considérant, d'autre part, qu'il résulte des termes mêmes de l'article L. 315-2 du code de la sécurité sociale que législateur a expressément habilité le collège des directeurs de l'UNCAM à fixer ses conditions d'application ; que, dès lors, la société requérante ne peut utilement soutenir que l'article 1er de la décision attaquée, prise sur le fondement de cet article, aurait méconnu, d'une part, les dispositions de l'article L. 182-2 du code de la sécurité sociale définissant les missions de cet établissement public national à caractère administratif, d'autre part, celles des articles L. 182-2-3 et L. 182-2-4 du même code définissant les attributions respectives de son conseil et du collège des directeurs ; <br/>
<br/>
              7. Considérant, en second lieu, que la même décision du 24 juin 2014 prévoit, à son article 2, que la décision de mise en oeuvre de la procédure d'accord préalable est prise par le collège des directeurs ; <br/>
<br/>
              8. Considérant, d'une part, que la détermination de l'autorité compétente pour subordonner le bénéfice de certaines prestations de l'assurance maladie, maternité et invalidité à l'accord préalable du service du contrôle médical ne relève pas plus des " principes fondamentaux (...) de la sécurité sociale " dont l'article 34 de la Constitution réserve la détermination à la loi ; <br/>
<br/>
              9. Considérant, d'autre part, qu'il résulte des dispositions du septième alinéa de l'article L. 315-2 du code de la sécurité sociale, éclairées par les travaux préparatoires de la loi du 17 décembre 2008 de financement de la sécurité sociale pour 2009 dont il est issu, que le législateur a entendu confier au collège des directeurs de l'UNCAM, établissement public à caractère administratif, le soin de fixer, par dérogation au pouvoir réglementaire du Premier ministre, l'ensemble des conditions d'application de cet article, y compris la désignation de l'autorité compétente pour y recourir ; <br/>
<br/>
              10. Considérant, dès lors, qu'en déterminant, à l'article 2 de la décision attaquée, l'autorité compétente pour décider de la mise en oeuvre de la procédure d'accord préalable du service du contrôle médical, le collège des directeurs de l'UNCAM n'a pas méconnu la compétence du législateur ; qu'il n'a pas non plus méconnu l'article L. 315-2 du code de la sécurité sociale en prévoyant qu'il serait lui-même cette autorité ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la société Astrazeneca n'est pas fondée à demander l'annulation de la décision relative à la procédure d'accord préalable des prestations de l'article L. 315-1 du code de la sécurité sociale qu'elle attaque ;<br/>
<br/>
              Sur la légalité de la décision du collège des directeurs de l'UNCAM du 24 juin 2014 relative à la procédure d'accord préalable pour bénéficier de la prise en charge de la rosuvastatine :<br/>
<br/>
              12. Considérant que des mesures réglementaires peuvent être prises pour l'application d'une disposition existante mais non encore publiée ou non encore opposable, à la condition qu'elles n'entrent pas en vigueur avant que la disposition sur laquelle elles se fondent ait été régulièrement rendue opposable aux tiers ; qu'en revanche, l'autorité administrative ne peut, sans méconnaître le principe selon lequel la légalité d'un acte administratif s'apprécie au regard des dispositions en vigueur à la date de sa signature, appliquer un texte qui n'est pas encore entré en vigueur et, à ce titre, adopter des mesures, même réglementaires, faisant application d'un régime juridique avant son entrée en vigueur ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier que la décision du collège des directeurs de l'UNCAM du 24 juin 2014 relative à la procédure d'accord préalable des prestations de l'article L. 315-1 du code de la sécurité sociale prise en application des dispositions de l'article L. 315-2 du code de la sécurité sociale, applicable à toutes les prestations de l'assurance maladie, maternité et invalidité, dont le remboursement des médicaments, et aux prestations prises en charge en application des articles L. 251-2 et L. 254-1 du code de l'action sociale et des familles, a été publiée au Journal officiel de la République française du 9 septembre 2014 ; que cette décision, qui revêt un caractère réglementaire, n'est ainsi entrée en vigueur, en application de l'article 1er du code civil, que le lendemain de sa publication, soit le 10 septembre 2014 ; que, dès lors, la décision attaquée du 24 juin 2014, relative à la procédure d'accord préalable applicable aux spécialités contenant de la rosuvastatine, ne pouvait légalement être prise à cette date, bien qu'elle fût mentionnée dans ses visas, en application de la décision du même jour, relative à la procédure d'accord préalable applicable à toutes les prestations ; qu'à cette même date, l'application de l'article L. 315-2 du code de la sécurité sociale à une catégorie particulière de spécialités était manifestement impossible en l'absence de mesure règlementaire précisant, pour l'ensemble des prestations, ses conditions d'application, notamment l'autorité compétente pour décider de la mise en oeuvre de la procédure d'accord préalable et les modalités d'intervention de l'accord du service du contrôle médical ; qu'ainsi, la décision attaquée du 24 juin 2014 est dépourvue de base légale ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la société Astrazeneca est fondée à demander l'annulation de la décision relative à la procédure d'accord préalable pour bénéficier de la prise en charge de la rosuvastatine qu'elle attaque ;<br/>
<br/>
              15. Considérant qu'il ne ressort pas des pièces du dossier que la disparition rétroactive des dispositions de la décision attaquée entraînerait des conséquences manifestement excessives, eu égard aux intérêts en présence et aux inconvénients que présenterait une limitation dans le temps des effets de leur annulation ; qu'il n'y a pas lieu, par suite, d'assortir l'annulation de ces dispositions d'une telle limitation ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la société Astrazeneca et de l'UNCAM présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du collège des directeurs de l'UNCAM du 24 juin 2014 relative à la procédure d'accord préalable pour bénéficier de la prise en charge de la rosuvastatine est annulée.<br/>
Article 2 : Le surplus des conclusions des requêtes de la société Astrazeneca est rejeté.<br/>
Article 3 : Les conclusions de l'UNCAM présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Astrazeneca et à l'Union nationale des caisses d'assurance maladie. <br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-03-17 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DU RÈGLEMENT. MESURES NE PORTANT PAS ATTEINTE AUX PRINCIPES FONDAMENTAUX DE LA SÉCURITÉ SOCIALE. - PRÉCISIONS APPORTÉES AUX CONDITIONS DÉFINIES PAR LE LÉGISLATEUR POUR DÉCIDER DE SUBORDONNER LE BÉNÉFICE DE CERTAINES PRESTATIONS DE L'ASSURANCE MALADIE, MATERNITÉ ET INVALIDITÉ À L'ACCORD PRÉALABLE DU SERVICE DU CONTRÔLE MÉDICAL ET DÉTERMINATION DE L'AUTORITÉ COMPÉTENTE POUR SUBORDONNER LE BÉNÉFICE DE CES PRESTATIONS À UN TEL ACCORD.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-02-01-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DIVERSES DÉTENTRICES D`UN POUVOIR RÉGLEMENTAIRE. - POUVOIR RÉGLEMENTAIRE CONFIÉ PAR LE LÉGISLATEUR AU COLLÈGE DES DIRECTEURS DE L'UNCAM - ETENDUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-07-02-035 ACTES LÉGISLATIFS ET ADMINISTRATIFS. PROMULGATION - PUBLICATION - NOTIFICATION. PUBLICATION. EFFETS D'UN DÉFAUT DE PUBLICATION. - DÉFAUT DE PUBLICATION D'UN ACTE RÉGLEMENTAIRE D'APPLICATION D'UNE DISPOSITION LÉGISLATIVE - ILLÉGALITÉ DE LA DÉCISION PRISE SUR LE FONDEMENT DE CET ACTE AVANT SA PUBLICATION, L'APPLICATION DE LA DISPOSITION LÉGISLATIVE ÉTANT MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE DE MESURE RÉGLEMENTAIRE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">62-02-01-01 SÉCURITÉ SOCIALE. RELATIONS AVEC LES PROFESSIONS ET LES ÉTABLISSEMENTS SANITAIRES. RELATIONS AVEC LES PROFESSIONS DE SANTÉ. MÉDECINS. - CONTRÔLE MÉDICAL - SUBORDINATION DU BÉNÉFICE DE CERTAINES PRESTATIONS À L'ACCORD PRÉALABLE DU SERVICE DU CONTRÔLE MÉDICAL (ART. L. 315-2 DU CSS) - POUVOIR RÉGLEMENTAIRE CONFIÉ PAR LE LÉGISLATEUR AU COLLÈGE DES DIRECTEURS DE L'UNCAM - ETENDUE.
</SCT>
<ANA ID="9A"> 01-02-01-03-17 Les précisions apportées aux conditions définies par le législateur pour décider de subordonner le bénéfice de certaines prestations de l'assurance maladie, maternité et invalidité à l'accord préalable du service du contrôle médical et la détermination de l'autorité compétente pour subordonner le bénéfice de ces prestations à un tel contrôle ne relèvent pas des principes fondamentaux (&#133;) de la sécurité sociale dont l'article 34 de la Constitution réserve la détermination à la loi.</ANA>
<ANA ID="9B"> 01-02-02-01-07 Il résulte des termes mêmes de l'article L. 315-2 du code de la sécurité sociale (CSS) que législateur a expressément habilité le collège des directeurs de l'UNCAM à fixer ses conditions d'application. Le collège des directeurs de l'UNCAM était donc compétent pour préciser les conditions que les prestations doivent remplir pour que leur prise en charge puisse être subordonnée à l'accord préalable du service du contrôle médical.,,,Il résulte des dispositions du septième alinéa de l'article L. 315-2 du CSS, éclairées par les travaux préparatoires de la loi n° 2008-1330 du 17 décembre 2008 dont il est issu, que le législateur a entendu confier au collège des directeurs de l'UNCAM, établissement public à caractère administratif, le soin de fixer, par dérogation au pouvoir réglementaire du Premier ministre, l'ensemble des conditions d'application de cet article, y compris la désignation de l'autorité compétente pour décider de mettre en oeuvre la procédure d'accord préalable du service du contrôle médical.</ANA>
<ANA ID="9C"> 01-07-02-035 La décision du collège des directeurs de l'UNCAM du 24 juin 2014 relative à la procédure d'accord préalable applicable à toutes les prestations, prise en application de l'article L. 315-2 du code de la sécurité sociale (CSS), a été publiée au Journal officiel de la République française du 9 septembre 2014. Cette décision, qui revêt un caractère réglementaire, n'est ainsi entrée en vigueur, en application de l'article 1er du code civil, que le lendemain de sa publication, soit le 10 septembre 2014. Dès lors, la décision attaquée du 24 juin 2014, relative à la procédure d'accord préalable applicable aux spécialités contenant de la rosuvastatine, ne pouvait légalement être prise à cette date, en application de la décision du même jour relative à la procédure d'accord préalable applicable à toutes les prestations, bien qu'elle fût mentionnée dans ses visas. A cette même date, l'application de l'article L. 315-2 du CSS à une catégorie particulière de spécialités était manifestement impossible en l'absence de mesure règlementaire précisant, pour l'ensemble des prestations, ses conditions d'application. Ainsi, la décision attaquée du 24 juin 2014 est dépourvue de base légale.</ANA>
<ANA ID="9D"> 62-02-01-01 Il résulte des termes mêmes de l'article L. 315-2 du code de la sécurité sociale (CSS) que législateur a expressément habilité le collège des directeurs de l'UNCAM à fixer ses conditions d'application. Le collège des directeurs de l'UNCAM était donc compétent pour préciser les conditions que les prestations doivent remplir pour que leur prise en charge puisse être subordonnée à l'accord préalable du service du contrôle médical.,,,Il résulte des dispositions du septième alinéa de l'article L. 315-2 du CSS, éclairées par les travaux préparatoires de la loi n° 2008-1330 du 17 décembre 2008 dont il est issu, que le législateur a entendu confier au collège des directeurs de l'UNCAM, établissement public à caractère administratif, le soin de fixer, par dérogation au pouvoir réglementaire du Premier ministre, l'ensemble des conditions d'application de cet article, y compris la désignation de l'autorité compétente pour décider de mettre en oeuvre la procédure d'accord préalable du service du contrôle médical.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 30 juillet 2003, Groupement des éleveurs mayennais de trotteurs (GEMTROT), n° 237201, p. 346 ; CE, 4 mai 2007, Association Les amis du comité des travaux historiques et scientifiques et des sociétés savantes et autres, n° 291481, inédite au Recueil ; CE, 9 mars 2016, Société MSD France, n° 385180, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
