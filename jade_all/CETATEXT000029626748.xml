<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626748</ID>
<ANCIEN_ID>JG_L_2014_10_000000377421</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626748.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 24/10/2014, 377421, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377421</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:377421.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Par une requête et un mémoire en réplique, enregistrés sous le n° 377421 les 11 avril et 8 août 2014 au secrétariat du contentieux du Conseil d'Etat, le département de l'Orne demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-247 du 25 février 2014 portant délimitation des cantons dans le département de l'Orne. <br/>
<br/>
<br/>
              2° Par une requête et un mémoire en réplique, enregistrés sous le n° 379738 les 25 avril et 7 août 2014 au secrétariat du contentieux du Conseil d'Etat, M. C...A...demande au Conseil d'Etat d'annuler pour excès de pouvoir le même décret n° 2014-247 du 25 février 2014.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Par une requête, enregistrée sous le n° 380250 le 29 avril 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...D...demande au Conseil d'Etat d'annuler pour excès de pouvoir le même décret n° 2014-247 du 25 février 2014. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces des dossiers ; <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 90-1103 du 11 décembre 1990 ;<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes du département de l'Orne, de M. A...et de M. D...sont dirigées contre le même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              3. Le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de l'Orne, compte tenu de l'exigence de réduction du nombre des cantons de ce département de quarante à vingt-et-un résultant de l'article L. 191-1 du code électoral.<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              4. En premier lieu, il résulte des termes mêmes des dispositions législatives citées ci-dessus qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons. <br/>
<br/>
              5. En deuxième lieu, ni les dispositions législatives citées ci-dessus, ni aucun autre texte non plus qu'aucun principe n'imposaient au Premier ministre de transmettre au département, en vue de sa consultation sur le projet de décret, les avis rendus, le cas échéant, par certains élus, lors de l'élaboration de ce projet. <br/>
<br/>
              6. En troisième lieu, le décret attaqué, qui délimite les circonscriptions électorales pour l'élection des conseillers départementaux de l'Orne et désigne les bureaux centralisateurs des nouveaux cantons, ne procède pas au transfert des sièges des chefs-lieux de canton. Par suite, les requérants ne sont pas fondés à soutenir que la consultation du conseil général aurait été irrégulière, faute de porter également sur le transfert des sièges des chefs-lieux de canton.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              7. En premier lieu, aux termes de l'article 7 de la loi du 11 décembre 1990 organisant la concomitance des renouvellements des conseils généraux et des conseils régionaux : " Il ne peut être procédé à aucun redécoupage des circonscriptions électorales dans l'année précédant l'échéance normale de renouvellement des assemblées concernées (...) ". Le décret attaqué a été signé par le Premier ministre le 25 février 2014, soit plus d'un an avant l'échéance normale de renouvellement des conseils généraux qui était, à la date de ce décret, prévue au mois de mars 2015. La circonstance que son entrée en vigueur est, conformément aux dispositions de l'article 51 de la loi du 17 mai 2013, différée à la date du prochain renouvellement général des assemblées départementales est sans incidence sur le respect, par le Premier ministre, des dispositions de l'article 7 de la loi du 11 décembre 1990, qui se réfèrent à la date à laquelle il est procédé au redécoupage et non à celle de son entrée en vigueur. <br/>
<br/>
              8. En deuxième lieu, la modification des limites territoriales des cantons doit être effectuée selon les règles prévues aux III et IV de l'article L. 3113-2 du code général des collectivités territoriales, sur des bases essentiellement démographiques, en respectant le nombre de cantons résultant de l'application des dispositions de l'article L. 191-1 du code électoral. Par suite, le moyen tiré de ce que certains cantons auraient une taille excessive et compteraient un trop grand nombre de communes et que le nouveau découpage conduirait à une méconnaissance du principe d'égalité entre territoires ainsi qu'entre conseillers départementaux dans les conditions d'exercice de leur mandat ne peut qu'être écarté. De même, le décret attaqué n'ayant ni pour objet ni pour effet de réorganiser l'implantation des services publics au sein du département, les requérants ne peuvent utilement invoquer les conséquences économiques et sociales d'une telle réorganisation.<br/>
<br/>
              9. En troisième lieu, ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe, n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les périmètres des établissements publics de coopération intercommunale, les limites des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ou la carte des services publics. D'une part, si M. A...critique le rattachement des communes de Saint-Pierre-d'Entremont et de Moncy au canton n° 13 (Flers-1) plutôt qu'au canton n° 11 (Domfront) qui regroupe les autres communes membres de la même communauté de communes, il ressort des pièces du dossier que le rattachement opéré, sans lequel l'écart entre la population du canton de Domfront et la moyenne départementale aurait atteint 20,1 %, est justifié par le respect de l'exigence tenant aux bases essentiellement démographiques de la délimitation des cantons. D'autre part, il ne ressort pas des pièces du dossier que le rattachement de la commune de Saint-Pierre-du-Regard au canton n° 14 (Flers-2) plutôt qu'au canton n° 6 (Athis-de-l'Orne) comme les autres communes membres de la même communauté de communes, qui est justifié par le respect de la même exigence, reposerait sur des considérations arbitraires ou serait entaché d'erreur manifeste d'appréciation. <br/>
<br/>
              10. En quatrième lieu, la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un bureau centralisateur sans mentionner les chefs-lieux de canton est sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales pour l'élection des conseillers départementaux de l'Orne. <br/>
<br/>
              11. En dernier lieu, s'il est soutenu que les conditions d'application du décret dans le temps différeraient selon les collectivités, le moyen n'est pas assorti des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              12. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du département de l'Orne, de M. A...et de M. D...sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au département de l'Orne, à M. C...A..., à M. B... D...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
