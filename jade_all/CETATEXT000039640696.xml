<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039640696</ID>
<ANCIEN_ID>JG_L_2019_12_000000416819</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/06/CETATEXT000039640696.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 18/12/2019, 416819</TITRE>
<DATE_DEC>2019-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416819</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:416819.20191218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2015-0024 du 24 septembre 2015, la chambre régionale des comptes de Champagne-Ardenne, Lorraine a déclaré Mme C... B..., la société de gestion des cliniques d'Epinal réunies (SOGECLER) et Mme D... A... conjointement et solidairement comptables de fait des deniers du centre hospitalier d'Epinal pour avoir dissimulé au comptable l'objet réel de l'acquisition des biens incorporels de la maternité Arc-en-Ciel pour un montant de 530 000 euros.<br/>
<br/>
              Par un arrêt n° S 2017-2905 du 26 octobre 2017, la Cour des comptes a, d'une part, infirmé ce jugement en tant qu'il a déclaré Mme D... A... comptable de fait des deniers du centre hospitalier Jean Monnet à Epinal, et, d'autre part, rejeté l'appel formé par le procureur financier près la chambre régionale des comptes de Champagne-Ardenne, Lorraine, par Mme C... B... et par la SOGECLER contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 décembre 2017, 26 mars 2018 et 17 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la SOGECLER demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt, en tant qu'il a jugé qu'elle devait être attraite à la gestion de fait et déclarée comptable de fait ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des juridictions financières ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n°63-156 du 23 février 1963 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la SOGECLER ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un projet de création d'un groupement de coopération sanitaire, le centre hospitalier Jean Monnet d'Epinal a fait l'acquisition auprès de la société de gestion des cliniques d'Epinal réunies (SOGECLER), d'une part, des biens immobiliers de la maternité Arc-en-Ciel pour un montant de 3 683 000 euros, et, d'autre part, des biens mobiliers et incorporels de cette dernière pour un montant de 1 160 000 euros, cette dernière somme comprenant 630 000 euros au titre des biens mobiliers et 530 000 euros au titre du fonds de commerce résultant de l'exploitation de 45 lits en gynécologie-obstétrique, dont l'autorisation délivrée à cette fin par l'agence régionale d'hospitalisation. La SOGECLER a toutefois poursuivi l'exploitation de la maternité Arc-en-Ciel sur le fondement d'une convention d'occupation précaire conclue avec le centre hospitalier, ultérieurement transformée en bail commercial, et moyennant moyennant le versement d'un loyer. A la suite de l'abandon du projet de création de groupement de coopération sanitaire, le centre hospitalier a revendu le 24 décembre 2013 les biens immobiliers et mobiliers de la maternité Arc-en-Ciel à la société " Matelots ", associée unique de la SOGECLER, pour un montant de 2,7 millions d'euros. Par un jugement du 24 septembre 2015, la chambre régionale des comptes de Champagne-Ardenne, Lorraine, a jugé que l'acquisition en 2004, par le centre hospitalier d'Epinal, des biens incorporels de la maternité Arc-en-Ciel pour un montant de 530 000 euros caractérisait, en l'absence de contrepartie, l'existence d'une gestion de fait et a déclaré conjointement et solidairement comptables de fait des deniers du centre hospitalier d'Epinal Mme C... B..., directrice de ce dernier, la SOGECLER et Mme D... A..., directrice départementale des affaires sanitaires et sociales du département des Vosges au moment des faits. Par un arrêt du 26 octobre 2017, contre lequel la SOGECLER se pourvoit en cassation, la Cour des comptes a infirmé ce jugement en tant seulement qu'il avait déclaré Mme A... comptable de fait et rejeté les requêtes du procureur financier près la chambre régionale des comptes de Champagne-Ardenne, Lorraine, de Mme B... et de la SOGECLER.<br/>
<br/>
              2. Aux termes du XI de l'article 60 de la loi du 23 février 1963 de finances pour 1963 : " Toute personne qui, sans avoir la qualité de comptable public ou sans agir sous contrôle et pour le compte d'un comptable public, s'ingère dans le recouvrement de recettes affectées ou destinées à un organisme public doté d'un poste comptable ou dépendant d'un tel poste doit, nonobstant les poursuites qui pourraient être engagées devant les juridictions répressives, rendre compte au juge financier de l'emploi des fonds ou valeurs qu'elle a irrégulièrement détenus ou maniés. / Il en est de même pour toute personne qui reçoit ou manie directement ou indirectement des fonds ou valeurs extraits irrégulièrement de la caisse d'un organisme public et pour toute personne qui, sans avoir la qualité de comptable public, procède à des opérations portant sur des fonds ou valeurs n'appartenant pas aux organismes publics, mais que les comptables publics sont exclusivement chargés d'exécuter en vertu de la réglementation en vigueur. / Les gestions de fait sont soumises aux mêmes juridictions et entraînent les mêmes obligations et responsabilités que les gestions régulières. Néanmoins, le juge des comptes peut, hors le cas de mauvaise foi ou d'infidélité du comptable de fait, suppléer par des considérations d'équité à l'insuffisance des justifications produites. / Les comptables de fait pourront, dans le cas où ils n'ont pas fait l'objet pour les mêmes opérations des poursuites au titre du délit prévu et réprimé par l'article 433-12 du Code pénal, être condamnés aux amendes prévues par la loi ".<br/>
<br/>
              3. En premier lieu, la Cour des comptes a, par l'arrêt attaqué, déclaré la SOGECLER comptable de fait après avoir, d'une part, constaté notamment que la clinique avait continué à exercer son activité après l'acte de cession de ses biens et que l'autorisation d'exercer avait été renouvelée à son profit par l'agence régionale d'hospitalisation au cours des années suivantes. Elle a, d'autre part, retenu que l'acquisition d'un fonds de commerce par le centre hospitalier ne pouvait être justifié par le projet de groupement de coopération sanitaire, qui impliquait que la clinique dispose toujours de son autorisation. Elle a, enfin, estimé que, en l'absence de contrepartie, cette acquisition revêtait un caractère purement fictif, lequel avait été dissimulé au comptable public. En statuant ainsi, contrairement à ce qui est soutenu, la Cour des comptes n'a pas éludé l'ensemble des circonstances ayant présidé au projet, ultérieurement abandonné, de création d'un groupement de coopération sanitaire entre les deux établissements mais a jugé, sans entacher son arrêt d'insuffisance de motivation, que ces circonstances révélaient que la vente au centre hospitalier du fonds de commerce de la maternité Arc-en-Ciel en 2004 pour un montant de 530 000 euros avait été dépourvue de contrepartie réelle.<br/>
<br/>
              4. En deuxième lieu, d'une part, il ressort des énonciations de l'arrêt attaqué que, malgré la vente au centre hospitalier d'un fonds de commerce correspondant à l'exploitation de 45 lits en gynécologie-obstétrique de la maternité Arc-en-Ciel, dont l'autorisation délivrée par l'agence régionale d'hospitalisation, la SOGECLER a continué à exploiter la maternité après la cession, moyennant une convention conclue avec le centre hospitalier et le versement d'un loyer. Il en ressort également que, au cours des années suivantes, l'autorisation d'exploiter a été renouvelée au bénéfice de la SOGECLER et non du centre hospitalier, la revente des biens par le centre hospitalier à la société " Matelots ", associée unique de la SOGECLER, en 2013 ne faisant plus apparaître l'existence ou la valorisation d'un fonds de commerce. Il en ressort enfin que l'acquisition par le centre hospitalier des biens mobiliers et incorporels de la clinique a fait l'objet d'un mandat global de 1 160 000 euros et d'une imputation sur un unique compte de biens corporels. Pour estimer que les pièces soumises au comptable public du centre hospitalier à l'appui de ce mandat révélaient le caractère non justifié de l'acquisition du fonds de commerce, alors même qu'il était fait référence dans ce mandat à la cession de 45 lits, la cour a relevé, par une appréciation souveraine non arguée de dénaturation, que la convention conclue le même jour permettant à la SOGECLER de continuer d'exploiter la maternité n'avait pas été soumise au comptable et contredisait le mandat, qui indiquait que la cession se faisait libre d'occupation. La Cour des comptes n'a pas commis d'erreur de qualification juridique en jugeant qu'en l'absence de transfert réel des biens incorporels à l'hôpital et de contrepartie réelle à cette partie de l'acquisition, celle-ci était purement fictive et caractérisait une gestion de fait.<br/>
<br/>
              5. En troisième lieu, la procédure de gestion de fait permet de saisir en leur chef toutes les personnes ayant contribué à la mise en place de la gestion de fait, même si elles n'ont pas manipulé de deniers publics. Celles-ci peuvent être déclarées comptables de fait si elles ont participé, fût-ce indirectement, aux irrégularités financières, ou si elles les ont facilitées, par leur inaction, ou même tolérées.<br/>
<br/>
              6. En jugeant, pour attraire la SOGECLER à la gestion de fait, que celle-ci devait être regardée, d'une part, comme ayant " connu " la dissimulation au comptable du caractère non justifié du paiement litigieux et de la convention d'occupation précaire, et, d'autre part, comme ayant toléré cette manoeuvre puisqu'elle en avait été la principale bénéficiaire en encaissant le produit de la vente d'un fonds de commerce non cédé dans les faits, la Cour des comptes, qui a pu, pour juger du caractère fictif de la dépense à la date de son engagement, se fonder sur l'ensemble des éléments portés à sa connaissance, y compris postérieurs au paiement litigieux, n'a entaché son arrêt ni d'erreur de droit, ni d'erreur de qualification juridique.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la SOGECLER n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOGECLER est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société de gestion des cliniques d'Epinal réunies et à la procureure générale près la Cour des comptes. <br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-04 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. JUGEMENT DES COMPTES. - GESTION DE FAIT - MANDAT FICTIF - ILLUSTRATION - ACQUISITION PAR UN CENTRE HOSPITALIER DU FONDS DE COMMERCE CORRESPONDANT À L'EXPLOITATION D'UNE MATERNITÉ ALORS QUE CELLE-CI CONTINUE D'ÊTRE EXPLOITÉE PAR LA SOCIÉTÉ VENDEUSE.
</SCT>
<ANA ID="9A"> 18-01-04 Centre hospitalier ayant, dans le cadre d'un projet de création d'un groupement de coopération sanitaire (GCS), fait l'acquisition, auprès de la société gérant une maternité, des biens immobiliers et mobiliers de cette maternité et du bien incorporel que constituait le fonds de commerce résultant de son exploitation. Société ayant poursuivi l'exploitation de la maternité sur le fondement d'une convention d'occupation précaire conclue avec le centre hospitalier, ultérieurement transformée en bail commercial, et moyennant le versement d'un loyer. Centre hospitalier ayant enfin, après l'abandon du projet de GCS, revendu à cette société les biens immobiliers et mobiliers de la maternité, sans mention du fonds de commerce.,,,Pour estimer que les pièces soumises au comptable public du centre hospitalier à l'appui du mandat d'acquisition révélaient le caractère non justifié de l'acquisition du fonds de commerce, alors même qu'il était fait référence dans ce mandat à la cession de 45 lits, la Cour des comptes a relevé, par une appréciation souveraine non arguée de dénaturation, que la convention conclue le même jour permettant à la société de continuer d'exploiter la maternité n'avait pas été soumise au comptable et contredisait le mandat, qui indiquait que la cession se faisait libre d'occupation. La Cour n'a pas commis d'erreur de qualification juridique en jugeant qu'en l'absence de transfert réel des biens incorporels à l'hôpital et de contrepartie réelle à cette partie de l'acquisition, celle-ci était purement fictive et caractérisait une gestion de fait.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
