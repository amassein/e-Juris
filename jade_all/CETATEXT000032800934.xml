<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032800934</ID>
<ANCIEN_ID>JG_L_2016_06_000000386581</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/80/09/CETATEXT000032800934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 29/06/2016, 386581, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386581</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386581.20160629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme Q...S..., M. L...C..., M. T...N..., M. F... H..., M. R...I..., M. M...J..., M. P...A..., M. D...K..., Mme O...E...et M. G...B...ont demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 13 janvier 2014 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de la région Ile-de-France a procédé à la validation de l'accord partiel et à l'homologation du document unilatéral fixant le plan de sauvegarde de l'emploi de la société Astérion France. Par un jugement n° 1401992 du 3 juin 2014, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14VE02351 du 22 octobre 2014, la cour administrative d'appel de Versailles a, sur appel de Mme S...et autres, annulé ce jugement et la décision du 13 janvier 2014.<br/>
<br/>
<br/>
              1° Sous le n° 386581, par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 18 décembre 2014, 23 janvier 2015, 15 septembre 2015 et 16 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société Astérion France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme S...et autres ;<br/>
<br/>
              3°) de mettre à la charge de Mme S...et autres la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 386844, par un pourvoi enregistré le 31 décembre 2014, le ministre du travail, de l'emploi et du dialogue social demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt analysé ci-dessus ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme S...et autres ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la Société Astérion France et à la SCP Didier, Pinet, avocat de Mme S... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les pourvois de la société Astérion France et du ministre du travail, de l'emploi et du dialogue social sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'une réorganisation de la société Astérion France comportant la suppression de 32 de ses 493 emplois, le directeur régional des entreprises, de la consommation, de la concurrence, du travail et de l'emploi d'Ile-de-France a, par une décision du 13 janvier 2014, d'une part validé l'accord collectif relatif au plan de sauvegarde de l'emploi et, d'autre part, homologué la décision unilatérale de l'employeur fixant le nombre de licenciements et la pondération des critères d'ordre des licenciements ; que la société Astérion France et le ministre du travail, de l'emploi et du dialogue social se sont pourvus en cassation contre l'arrêt du 22 octobre 2014 par lequel la cour administrative d'appel de Versailles a annulé cette décision ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour annuler la décision du directeur régional des entreprises, de la consommation, de la concurrence, du travail et de l'emploi d'Ile-de-France, la cour s'est fondée sur l'irrégularité de la procédure d'information et de consultation conduite par l'employeur, en raison des vices ayant entaché la consultation du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) de l'établissement de Caen de la société Astérion France ; <br/>
<br/>
              Sur le droit applicable :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre (...) " ; que les articles L. 1233-24-1 et L. 1233-24-4 du même code prévoient que le contenu de ce plan de sauvegarde de l'emploi est déterminé par un accord collectif d'entreprise et que, lorsque cet accord n'a pas fixé le nombre des licenciements et la pondération des critères d'ordre des licenciements, ces éléments sont précisés par un document élaboré unilatéralement par l'employeur ; qu'aux termes de l'article L. 1233-57-2 du même code : " L'autorité administrative valide l'accord collectif mentionné à l'article L. 1233-24-1 dès lors qu'elle s'est assurée de : (...) / 2° la régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail (...) " ; qu'aux termes de son article L. 1233-57-3 : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise et, le cas échéant, du comité d'hygiène, de sécurité et des conditions de travail (...) " ; qu'enfin, aux termes de l'article L. 4612-8 du même code, dans sa rédaction alors en vigueur : " Le comité d'hygiène, de sécurité et des conditions de travail est consulté avant toute décision d'aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produits ou de l'organisation du travail (...) " ; qu'il résulte de l'ensemble de ces dispositions que lorsque l'autorité administrative est saisie d'une demande de validation d'un accord collectif ou d'homologation d'un document unilatéral fixant le contenu d'un plan de sauvegarde de l'emploi pour une opération qui, parce qu'elle modifie de manière importante les conditions de santé et de sécurité ou les conditions de travail des salariés de l'entreprise, requiert la consultation du ou des comités d'hygiène, de sécurité et des conditions de travail concernés, elle ne peut légalement accorder la validation ou l'homologation demandée que si cette consultation a été régulière ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes des dispositions de l'article L. 4614-9 du code du travail : " Le comité d'hygiène, de sécurité et des conditions de travail reçoit de l'employeur les informations qui lui sont nécessaires pour l'exercice de ses missions, ainsi que les moyens nécessaires à la préparation et à l'organisation des réunions et aux déplacements imposés par les enquêtes ou inspections " ; qu'aux termes de l'article L. 4614-12 du même code, dans sa rédaction alors en vigueur : " Le comité d'hygiène, de sécurité et des conditions de travail peut faire appel à un expert agréé : (...) / 2° En cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail, prévu à l'article L. 4612-8. (...) " ; qu'aux termes du troisième alinéa de l'article L. 4614-13 du même code : " L'employeur ne peut s'opposer à l'entrée de l'expert dans l'établissement. Il lui fournit les informations nécessaires à l'exercice de sa mission " ; qu'il résulte de ces dispositions qu'il appartient à l'administration, saisie dans les conditions mentionnées au point précédent, de s'assurer, en tenant compte des conditions dans lesquelles l'expert le cas échéant désigné a pu exercer sa mission, que le ou les CHSCT concernés ont pu, lorsque leur consultation est requise, se prononcer sur l'opération projetée en toute connaissance de cause ;<br/>
<br/>
              6. Considérant, enfin, qu'aux termes des dispositions de l'article L. 1233-57-5 du code du travail relatif à la procédure d'injonction : " Toute demande tendant, avant transmission de la demande de validation ou d'homologation, à ce qu'il soit enjoint à l'employeur de fournir les éléments d'information relatifs à la procédure en cours ou de se conformer à une règle de procédure prévue par les textes législatifs, les conventions collectives ou un accord collectif est adressée à l'autorité administrative. Celle-ci se prononce dans un délai de cinq jours " ; qu'aux termes de l'article D. 1233-12 du même code : " La demande mentionnée à l'article L. 1233-57-5 est adressée par le comité d'entreprise, ou, à défaut, les délégués du personnel, ou, en cas de négociation d'un accord mentionné à l'article L. 1233-24-1 par les organisations syndicales représentatives dans l'entreprise, au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi compétent (...). / S'il décide de faire droit à la demande, le directeur régional adresse une injonction à l'employeur par tout moyen permettant de lui conférer une date certaine. Il adresse simultanément copie de cette injonction à l'auteur de la demande, au comité d'entreprise et aux organisations syndicales représentatives en cas de négociation d'un accord mentionné à l'article L. 1233-24-1 " ; que, par ailleurs, aux termes du deuxième alinéa de l'article L. 4614-13 du même code portant sur les contestations relatives à l'expert désigné par le CHSCT : " (...) toute contestation relative à l'expertise avant transmission de la demande de validation ou d'homologation prévue à l'article L. 1233-57-4 est adressée à l'autorité administrative, qui se prononce dans un délai de cinq jours " ; que l'article R. 4616-10 du même code dispose que ces contestations : " (...) doivent être dûment motivées et adressées au directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi territorialement compétent, par tout moyen permettant de conférer une date certaine (...)  / 2° Par les membres de l'instance lorsque les conditions fixées par l'alinéa 3 de l'article L. 4614-13 ne sont pas réunies. / Le directeur régional se prononce dans un délai de cinq jours à compter de la date de réception de la demande. Une copie de la décision est adressée aux autres parties " ; qu'alors même que l'article D. 1233-12 n'en prévoit pas expressément la possibilité, il résulte de l'ensemble de ces dispositions que, lorsque sa consultation est requise, le CHSCT peut, au cours de la procédure d'information et de consultation préalable à la transmission d'une demande de validation ou d'homologation relative à un plan de sauvegarde de l'emploi, saisir l'autorité administrative de toute atteinte à l'exercice de sa mission ou de celle de l'expert qu'il a le cas échéant désigné, en formulant, selon le cas, une demande d'injonction ou une contestation relative à l'expertise ; qu'il résulte également de ces dispositions que l'autorité administrative doit, dans les deux cas, se prononcer dans un délai de cinq jours et doit, si elle prononce une injonction sur le fondement de l'article L. 1233-57-5 de ce code, en informer le CHSCT qui l'a saisie ainsi que le comité d'entreprise et, en cas de négociation d'un accord mentionné à l'article L. 1233-24-1, les organisations syndicales représentatives ;<br/>
<br/>
              7. Considérant que, les procédures de demande d'injonction et de contestation relative à l'expertise mentionnées ci-dessus ayant notamment pour objet de permettre aux institutions représentatives du personnel de contribuer elles-mêmes au respect de la procédure d'information et de consultation préalable à une demande de validation ou d'homologation relative à un plan de sauvegarde de l'emploi, l'administration saisie d'une telle demande peut, par suite, légalement tenir compte, dans l'appréciation globale qui lui incombe au titre de la régularité de cette procédure d'information et de consultation, de la circonstance qu'un CHSCT n'aurait formulé aucune demande d'injonction ni aucune contestation relative à l'expertise ; qu'une telle circonstance ne saurait néanmoins dispenser l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que ce CHSCT a effectivement disposé des informations utiles pour se prononcer sur l'opération projetée en toute connaissance de cause ;<br/>
<br/>
              Sur l'espèce :<br/>
<br/>
              8. Considérant que la cour a pu régulièrement se borner à analyser, dans les visas de son arrêt, les moyens en défense de la société Astérion France comme soutenant que les moyens d'appel n'étaient pas fondés ; qu'elle n'a pas entaché son arrêt d'insuffisance de motivation en faisant droit au moyen tiré des vices ayant entaché la consultation du CHSCT de l'établissement de Caen sans répondre à l'ensemble des arguments présentés en défense sur ce point ; <br/>
<br/>
              9. Considérant que la cour a pu à bon droit rappeler, conformément au principe rappelé au point 7, que l'absence de recours, par le CHSCT de l'établissement de Caen, aux procédures de demande d'injonction ou de contestation relative à l'expertise, ne faisait pas obstacle à ce que soit soulevé devant le juge de l'excès de pouvoir le moyen tiré de l'irrégularité de la procédure d'information et de consultation ; que le ministre chargé du travail ne saurait, en tout état de cause, soutenir que l'arrêt serait irrégulier pour avoir, ce faisant, répondu à un moyen en défense qui n'était pas soulevé ; <br/>
<br/>
              10. Considérant qu'ainsi qu'il a été dit au point 4, les vices qui affectent le cas échéant la consultation d'un CHSCT ne sont susceptibles d'entacher d'irrégularité la procédure d'information et de consultation et, par suite, de faire obstacle à toute validation ou d'homologation d'un accord ou d'un document fixant le contenu d'un plan de sauvegarde de l'emploi, que lorsque cette consultation est obligatoire en vertu des dispositions de l'article L. 4612-8 du code du travail, actuellement reprises à l'article L. 4612-8-1 du même code ; qu'en jugeant, alors qu'il ressortait des pièces du dossier qui lui était soumis que l'établissement de Caen de la société Astérion France devait perdre près de 20 % de ses effectifs et voir le contenu et l'organisation de ses activités substantiellement modifiés, que la consultation de son CHSCT était obligatoire, la cour administrative d'appel, qui n'était pas tenue, en l'absence de toute contestation de ce point devant elle, de motiver davantage son arrêt, a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et n'a pas commis d'erreur de droit ;<br/>
<br/>
              11. Considérant que, après avoir relevé que l'expert désigné par le CHSCT de l'établissement de Caen n'avait pu obtenir de l'employeur les documents relatifs à la future organisation de l'établissement, que les autres informations écrites et orales fournies au CHSCT lors de ses réunions des 13 novembre et 18 décembre 2013 ne lui avaient pas davantage apporté les éléments dont disposait l'employeur sur les modifications envisagées de l'organisation du travail dans l'établissement et, enfin, que l'inspecteur du travail compétent avait, entre les deux réunions du comité, alerté sa hiérarchie sur l'insuffisance des informations fournies, la cour a pu, sans entacher son arrêt d'erreur de droit, estimer, par une appréciation souveraine qui n'est pas entachée de dénaturation, que le CHSCT n'avait pas disposé des informations utiles pour se prononcer en toute connaissance de cause sur l'opération projetée, alors même qu'il n'avait formulé aucune demande d'injonction ni aucune contestation relative à l'expertise ; qu'à cet égard, contrairement à ce que soutient le ministre chargé du travail, la cour n'a pas imposé que l'information du CHSCT obéisse à des conditions particulières de forme ou de contenu ; qu'elle n'a pas davantage assimilé à une demande d'injonction le courrier adressé à sa hiérarchie par l'inspecteur du travail ; qu'enfin, son arrêt n'est pas insuffisamment motivé sur ce point ; <br/>
<br/>
              12. Considérant qu'ayant jugé que le CHSCT de Caen, dont la consultation était obligatoire, n'avait pu se prononcer sur l'opération projetée en toute connaissance de cause, la cour administrative d'appel a pu, sans erreur de droit, en déduire que la procédure d'information et de consultation était entachée d'irrégularité ; que l'irrégularité de la procédure d'information et de consultation faisant obstacle à ce que l'administration puisse légalement homologuer le plan de sauvegarde de l'emploi, c'est par suite sans erreur de droit que la cour en a déduit, sans avoir à rechercher l'influence exercée par cette irrégularité sur la décision en litige ni examiner si elle avait privé les salariés d'une garantie, que la décision de validation et d'homologation du directeur de la consommation, de la concurrence, du travail et de l'emploi d'Ile-de-France du 13 janvier 2014 était entachée d'excès de pouvoir ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la société Astérion France et le ministre du travail, de l'emploi et du dialogue social ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme S... et autres qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Astérion France une somme de 300 euros à verser à chaque défendeur à ce même titre ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
<br/>
Article 1er : Le pourvoi de la société Astérion France et le pourvoi du ministre du travail, de l'emploi et du dialogue social sont rejetés.  <br/>
Article 2 : La société Astérion France versera une somme de 300 euros chacun à Mme S..., à M.C..., à M.N..., à M.H..., à M.I..., à M.J..., à M. A..., à M.K..., à Mme E...et à M.B....<br/>
Article 3 : La présente décision sera notifiée à la société Astérion France, à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à Mme Q...S..., premier défendeur dénommé. Les autres défendeurs seront informés de la présente décision par la SCP Gatineau, Fattachini, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE (LOI DU 14 JUIN 2013) - 1) CONTRÔLE DE L'ADMINISTRATION - RÉGULARITÉ DE LA CONSULTATION DU CHSCT - A) EXISTENCE [RJ1] - B) CHAMP - C) PORTÉE [RJ2] - D) CONSÉQUENCES DE L'IRRÉGULARITÉ DE LA CONSULTATION OBLIGATOIRE - ANNULATION DE LA DÉCISION - 2) PROCÉDURES DE DEMANDE D'INJONCTION (ART. L. 1233-57-5 C. TRAV.) ET DE CONTESTATION RELATIVE À L'EXPERTISE (ART. L. 4614-13 C. TRAV.) - A) FACULTÉ POUR LE CHSCT D'Y RECOURIR - EXISTENCE - B) OBLIGATIONS DE L'AUTORITÉ ADMINISTRATIVE - C) PRISE EN COMPTE DE CETTE FACULTÉ DANS L'APPRÉCIATION DE LA RÉGULARITÉ DE LA PROCÉDURE D'INFORMATION ET DE CONSULTATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 66-07 1) a) Lorsque l'autorité administrative est saisie d'une demande de validation d'un accord collectif fixant le contenu d'un plan de sauvegarde de l'emploi (PSE) pour une opération qui, parce qu'elle modifie de manière importante les conditions de santé et de sécurité ou les conditions de travail des salariés de l'entreprise, requiert la consultation du ou des comités d'hygiène, de sécurité et des conditions de travail (CHSCT) concernés, elle ne peut légalement accorder la validation demandée que si cette consultation a été régulière.,,,b) Les vices qui affectent le cas échéant la consultation d'un CHSCT ne sont susceptibles d'entacher d'irrégularité la procédure d'information et de consultation et, par suite, de faire obstacle à toute validation ou d'homologation d'un accord ou d'un document fixant le contenu d'un plan de sauvegarde de l'emploi, que lorsque cette consultation est obligatoire en vertu des dispositions de l'article L. 4612-8 du code du travail, actuellement reprises à l'article L. 4612-8-1 du même code.,,,c) Il appartient à l'autorité administrative de s'assurer que le CHSCT a disposé des informations utiles pour se prononcer en toute connaissance de cause sur l'opération projetée.... ,,d) L'irrégularité de la procédure d'information et de consultation obligatoire faisant obstacle à ce que l'administration puisse légalement homologuer le plan de sauvegarde de l'emploi, il appartient au juge, lorsqu'il constate que la procédure a été irrégulière, d'annuler la décision de validation ou d'homologation, sans avoir à rechercher l'influence exercée par cette irrégularité sur la décision en litige ni examiner si elle avait privé les salariés d'une garantie.,,,2) a) Alors même que l'article D. 1233-12 du code du travail (C. Trav.) n'en prévoit pas expressément la possibilité, il résulte des articles L. 1233-57-5, D. 1233-12, L. 4614-13 et R. 4616-10 de ce code que, lorsque sa consultation est requise, le CHSCT peut, au cours de la procédure d'information et de consultation préalable à la transmission d'une demande de validation ou d'homologation relative à un plan de sauvegarde de l'emploi, saisir l'autorité administrative de toute atteinte à l'exercice de sa mission ou de celle de l'expert qu'il a le cas échéant désigné, en formulant, selon le cas, une demande d'injonction ou une contestation relative à l'expertise.... ,,b) L'autorité administrative doit, dans les deux cas, se prononcer dans un délai de cinq jours et doit, si elle prononce une injonction sur le fondement de l'article L. 1233-57-5 de ce code, en informer l'organe qui l'a saisie ainsi que le comité d'entreprise et, en cas de négociation d'un accord mentionné à l'article L. 1233-24-1, les organisations syndicales représentatives.,,,c) Les procédures de demande d'injonction et de contestation relative à l'expertise mentionnées ci-dessus ayant notamment pour objet de permettre aux institutions représentatives du personnel de contribuer elles-mêmes au respect de la procédure d'information et de consultation préalable à une demande de validation ou d'homologation relative à un plan de sauvegarde de l'emploi, l'administration saisie d'une telle demande peut, par suite, légalement tenir compte, dans l'appréciation globale qui lui incombe au titre de la régularité de cette procédure d'information et de consultation, de la circonstance qu'un CHSCT n'aurait formulé aucune demande d'injonction ni aucune contestation relative à l'expertise. Une telle circonstance ne saurait néanmoins dispenser l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que ce CHSCT a effectivement disposé des informations utiles pour se prononcer sur l'opération projetée en toute connaissance de cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 21 octobre 2015, Syndicat CGT SKF Montigny et autres, n° 386123, T. pp. 793-896.,,[RJ2]Rappr., s'agissant du comité d'entreprise, CE, 21 octobre 2015, Comité d'entreprise de la société Norbert Dentressangle Silo et autres, n° 385683, T. p. 897.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
