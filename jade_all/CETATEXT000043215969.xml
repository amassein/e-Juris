<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043215969</ID>
<ANCIEN_ID>JG_L_2021_02_000000449959</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/59/CETATEXT000043215969.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 25/02/2021, 449959, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449959</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449959.20210225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, d'enjoindre au préfet du Val-d'Oise de renouveler le récépissé attestant de son admission au statut de réfugié dans un délai de quarante-huit heures à compter de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard, en deuxième lieu, d'enjoindre au préfet du Val-d'Oise de lui délivrer une carte de résident dans un délai d'un mois à compter de la notification de l'ordonnance à intervenir et, en dernier lieu, d'enjoindre à l'Office français de protection des réfugiés et apatrides (OFPRA) de lui délivrer une attestation provisoire de protection juridique. Par une ordonnance n° 2102194 du 19 février 2021, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 20 et 22 février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de condamner l'Etat à lui verser la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - l'ordonnance est entachée d'une erreur de droit et d'une dénaturation des faits pour avoir estimé qu'elle s'était placée elle-même dans une situation d'urgence en ne procédant pas à temps au renouvellement de ses titres de séjour, et ce alors qu'elle s'est présentée à plusieurs reprises au guichet de la préfecture du Val d'Oise à cet effet ;<br/>
              - elle ne mentionne pas l'atteinte portée à son droit d'asile par l'OFPRA, qui n'a pas délivré l'attestation provisoire prévue par l'article L. 751-3 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, le délai de trois ans pour statuer sur une demande de séjour crée une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative, en deuxième lieu, le comportement du préfet la place dans une situation manifeste d'extrême précarité qui n'est pas compatible avec son âge et son état de santé et, en dernier lieu, elle a la qualité de réfugié ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile ; <br/>
              - le préfet du Val-d'Oise a porté une atteinte manifeste à son droit d'asile dès lors que, en ne l'informant pas qu'elle devait utiliser le téléservice mis en place par le ministère de l'intérieur pour faire sa demande de renouvellement, il a indûment retardé la délivrance de la carte de résident à laquelle elle a droit en tant que réfugiée.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 22 février 2021, la Cimade conclut à ce qu'il soit fait droit aux conclusions de la requête, aux moyens de laquelle elle s'associe, et soutient que son intervention est recevable.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". Selon l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte des dispositions précitées que lorsqu'un requérant fonde son action sur la procédure particulière instituée à l'article L. 521-2 du code de justice administrative, il lui appartient de justifier de circonstances caractérisant une situation d'urgence qui implique, sous réserve que les autres conditions posées par l'article L. 521-2 soient remplies, qu'une mesure visant à sauvegarder une liberté fondamentale doive être prise dans les quarante-huit heures.<br/>
<br/>
              Sur l'intervention de la Cimade :<br/>
<br/>
              3. Eu égard à son objet, la Cimade a intérêt à contester l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise. Son intervention est donc admise.<br/>
<br/>
              Sur les conclusions d'appel de Mme B... :<br/>
<br/>
              4. Mme B..., ressortissante burundaise née le 25 décembre 1954, est entrée en France en 1987 ou 1988 et a bénéficié d'une carte de résident valable jusqu'au 14 septembre 2017 dont elle n'a sollicité le renouvellement que le 18 mai 2018. La préfecture du Val d'Oise lui a alors délivré un récépissé de demande de titre de séjour valable jusqu'au 17 août 2018, avant de délivrer une carte de séjour temporaire d'une durée d'un an que Mme B... n'a pas retirée auprès des services de la préfecture. La requérante a alors poursuivi et obtenu devant l'OFPRA l'admission au statut de réfugié, prononcée le 27 décembre 2019. La préfecture du Val d'Oise a mis l'intéressée en possession d'un récépissé de demande de titre de séjour valable jusqu'au 9 septembre 2020, dont elle n'a demandé le renouvellement qu'après son expiration le 6 décembre 2020.<br/>
<br/>
              5. Il résulte de la chronologie rappelée au point 3. ci-dessus que Mme B..., qui soutient que sa situation irrégulière au regard des dispositions régissant le séjour des étrangers en France la place dans une grande précarité incompatible avec son âge et son état de santé, s'est abstenue de demander en temps utile le renouvellement de son récépissé de demande de titre de séjour, après avoir laissé expirer sans davantage en demander le renouvellement en temps utile sa carte de résident. Les circonstances qu'elle invoque ne sauraient caractériser une situation d'urgence justifiant l'intervention à très bref délai du juge des référés statuant sur le fondement des dispositions citées ci-dessus de l'article L. 521-2 du code de justice administrative. Par suite, Mme B... n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Cergy-Pontoise a, pour ce motif, rejeté sa demande. Par suite, il y a lieu de rejeter sa requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'intervention de la Cimade est admise.<br/>
Article 2 : La requête de Mme B... est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme A... B....<br/>
Copie en sera adressée au préfet du Val d'Oise, à l'Office français de protection des réfugiés et apatrides et à la Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
