<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687488</ID>
<ANCIEN_ID>JG_L_2012_11_000000349529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/74/CETATEXT000026687488.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 26/11/2012, 349529</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349529.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu, 1°) sous le n° 349529, la requête sommaire et le mémoire complémentaire, enregistrés les 23 mai et 10 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Union syndicale de la promotion audiovisuelle, dont le siège est 5 rue Cernuschi à Paris (75017) ; l'Union syndicale de la promotion audiovisuelle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 mars 2011 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa demande tendant à ce que la société Métropole Télévision (M6) soit mise en demeure de respecter les obligations qui lui sont imposées s'agissant de sa contribution à la production d'oeuvres patrimoniales ainsi qu'à la production indépendante et d'oeuvres d'expression française ; <br/>
<br/>
              2°) de mettre à la charge du CSA le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu, 2°) sous le n° 349530, la requête sommaire et le mémoire complémentaire, enregistrés les 23 mai et 10 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat des producteurs de films d'animation, dont le siège est 5 rue Cernuschi à Paris (75017) ; le syndicat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la même décision du 15 mars 2011 du Conseil supérieur de l'audiovisuel (CSA) ; <br/>
<br/>
              2°) de mettre à la charge du CSA le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986, modifiée notamment par les lois n° 2007-309 du 5 mars 2007 et n° 2009-258 du 5 mars 2009 ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 88-248 DC du 17 janvier 1989 ;<br/>
<br/>
              Vu le décret n° 2001-609 du 9 juillet 2001 ;<br/>
<br/>
              Vu le décret n° 2009-1271 du 21 octobre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, Auditeur, <br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat de l'Union syndicale de la promotion audiovisuelle, et du Syndicat des producteurs de films d'animation  et de la SCP Lyon-Caen, Thiriez, avocat de la société Métropole Télévision M6,<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat de l'Union syndicale de la promotion audiovisuelle, et du Syndicat des producteurs de films d'animation et à la SCP Lyon-Caen, Thiriez, avocat de la société Métropole Télévision M6 ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes de l'Union syndicale de la promotion audiovisuelle et du Syndicat des producteurs de films d'animation sont dirigées contre la même décision du 15 mars 2011 ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 3-1 de la loi du 30 septembre 1986 relative à la liberté de communication, le Conseil supérieur de l'audiovisuel " veille à la qualité et à la diversité des programmes, au développement de la production et de la création audiovisuelle nationale (...) " ; qu'aux termes de l'article 42 de la même loi, dans sa rédaction résultant de la loi du 9 juillet 2010 : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1. / Le Conseil supérieur de l'audiovisuel rend publiques ces mises en demeure. / Les organisations professionnelles et syndicales représentatives du secteur de la communication audiovisuelle, le Conseil national des langues et cultures régionales, les associations familiales et les associations de défense des droits des femmes ainsi que les associations ayant dans leur objet social la défense des intérêts des téléspectateurs peuvent demander au Conseil supérieur de l'audiovisuel d'engager la procédure de mise en demeure prévue au premier alinéa du présent article " ; <br/>
<br/>
              3. Considérant que les dispositions de l'article 27 de la loi du 30 septembre 1986 prévoient que les éditeurs de services contribuent au développement de la production, en tout ou partie indépendante à leur égard, d'oeuvres cinématographiques et audiovisuelles ; que le décret du 21 octobre 2009, relatif à la contribution à la production audiovisuelle des éditeurs de services de télévision diffusés par voie hertzienne terrestre en mode analogique, pris notamment pour l'application des dispositions de l'article 27 de cette loi, impose aux éditeurs de services de consacrer chaque année une part annuelle de leur chiffre d'affaires annuel net de l'exercice précédent à des dépenses contribuant au développement de la production d'oeuvres audiovisuelles européennes ou d'expression originale française et fixe cette part, soit à 15% au moins, dont au moins 10,5% consacrés à des dépenses contribuant au développement de la production d'oeuvres patrimoniales, soit à 12,5% au moins lorsque ces dépenses sont entièrement consacrées à des oeuvres patrimoniales ; qu'il dispose, par ailleurs, qu'une part des dépenses obligatoires est consacrée au développement de la production indépendante et précise les critères, définis à l'article 71-1 de cette loi, selon lesquels ces dépenses sont prises en compte au titre du développement de la production indépendante ;<br/>
<br/>
              4. Considérant qu'après que la société M6 lui a transmis au cours du premier semestre 2010 son rapport sur les conditions d'exécution de ses obligations et de ses engagements au titre de l'année 2010, le Conseil supérieur de l'audiovisuel a établi un document intitulé " Bilan de la société M6 - Année 2009 " ; que ce bilan indique que " le nouveau décret relatif à la contribution au développement de la production audiovisuelle ayant été publié tardivement, M6 n'a pas été en mesure de régir ses investissements selon les nouvelles dispositions du décret 2009-1271 du 21 octobre 2009. Par conséquent M6 ne respecte ni son obligation de production d'oeuvres patrimoniales ni les obligations de production indépendante et de production d'oeuvres d'expression française qui y sont rattachées " ; qu'à la suite de la publication de ce bilan, l'Union syndicale de la promotion audiovisuelle, le Syndicat des producteurs de films d'animation et deux autres organisations professionnelles du secteur audiovisuel ont demandé au Conseil supérieur de l'audiovisuel, par une lettre en date du 23 février 2011, de faire application du 3ème alinéa de l'article 42, en mettant la société M6 en demeure de respecter ses obligations ; que, par la décision attaquée en date du 15 mars 2011, le Conseil supérieur de l'audiovisuel a décidé de ne pas adresser de mise en demeure à la société M6 pour les manquements constatés à ses obligations au cours de l'année 2009 ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que le procès-verbal de la séance du 15 mars 2011 au cours de laquelle le Conseil supérieur de l'audiovisuel a rejeté la demande des organisations requérantes comporte, contrairement à ce qui est soutenu, la signature du président ; que ce procès-verbal énonce les éléments de droit et de fait sur lesquels le Conseil supérieur de l'audiovisuel s'est fondé ; qu'ainsi le moyen tiré de ce que la motivation de la décision du 15 mars 2001 serait insuffisante manque en fait ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              6. Considérant, en premier lieu, que les dispositions des articles 3-1 et 42 de la loi du 30 septembre 1986, n'ont pas pour effet d'obliger le Conseil supérieur de l'audiovisuel, qui dispose d'autres moyens pour conduire les titulaires d'autorisation pour l'exploitation d'un service de communication audiovisuelle à respecter les obligations de production audiovisuelle qui leur sont imposées, à adresser auxdits titulaires une mise en demeure lorsqu'il est saisi d'une telle demande en application du 3ème alinéa de l'article 42 précité ; que ces dispositions laissent au Conseil supérieur de l'audiovisuel le soin d'apprécier sous le contrôle du juge si, compte tenu des circonstances et de la nature des manquements constatés, il y a lieu pour lui de prendre immédiatement une telle mesure ; qu'il s'ensuit que les organisations requérantes ne sont pas fondées à soutenir que le Conseil supérieur de l'audiovisuel était tenu de prononcer la mise en demeure qui lui était demandée ;<br/>
<br/>
              7. Considérant, en second lieu, que le Conseil supérieur de l'audiovisuel pouvait sans erreur de droit tenir compte, pour décider de ne pas mettre en demeure M6, d'une part, de la publication tardive du décret du 21 octobre 2009 fixant les nouveaux niveaux de contribution  et d'autre part, du fait que, si M6 n'avait pas atteint ses quotas d'oeuvres patrimoniales indépendantes et d'oeuvres d'expression originale française en 2009, cette société avait au cours de cette année, contribué à hauteur de 16,3% de son chiffre d'affaires au financement d'oeuvres audiovisuelles alors qu'elle n'était réglementairement tenue qu'à une obligation de 15% ; que, dans ces conditions, la décision de ne pas faire usage des pouvoirs que lui confère l'article 42 de la loi du 30 septembre 1986 n'est pas, dans les circonstances de l'espèce, entachée d'une erreur manifeste d'appréciation ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les organisations requérantes ne sont pas fondées à demander l'annulation de la décision attaquée ; que doivent être rejetées, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'Union syndicale de la promotion audiovisuelle et du Syndicat des producteurs de films d'animation sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Union syndicale de la promotion audiovisuelle, au Syndicat des producteurs de films d'animation, au Conseil supérieur de l'audiovisuel et à la société Métropole Télévision (M6).<br/>
		Copie en sera adressée au ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - REFUS DU CSA D'ADRESSER UNE MISE EN DEMEURE À UN TITULAIRE D'AUTORISATION AYANT COMMIS DES MANQUEMENTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-01 RADIODIFFUSION SONORE ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - DEMANDE TENDANT À CE QUE LE CSA ADRESSE UNE MISE EN DEMEURE (3È AL. DE L'ART. 42 DE LA LOI DU 30 SEPTEMBRE 1986 - OBLIGATION POUR LE CSA D'Y ACCÉDER - ABSENCE - FACULTÉ, EN FONCTION DES CIRCONSTANCES ET DE LA NATURE DES MANQUEMENTS CONSTATÉS - EXISTENCE - CONTRÔLE DU JUGE - CONTRÔLE DE L'ERREUR MANIFESTE.
</SCT>
<ANA ID="9A"> 54-07-02-04 Le juge exerce un contrôle de l'erreur manifeste sur l'appréciation par laquelle le Conseil supérieur de l'audiovisuel (CSA), lorsqu'il est saisi d'une demande tendant à ce qu'il adresse une mise en demeure au titulaire d'une autorisation d'exploitation d'un service de communication audiovisuelle, sur le fondement du 3ème alinéa de l'article 42 de la loi n° 86-1067 du 30 septembre 1986, décide si, compte tenu des circonstances et de la nature des manquements constatés, il y a lieu pour lui de prendre immédiatement une telle mesure.</ANA>
<ANA ID="9B"> 56-01 Les dispositions des articles 3-1 et 42 de la loi n° 86-1067 du 30 septembre 1986 n'ont pas pour effet d'obliger le Conseil supérieur de l'audiovisuel (CSA), qui dispose d'autres moyens pour conduire les titulaires d'autorisation pour l'exploitation d'un service de communication audiovisuelle à respecter les obligations de production audiovisuelle qui leur sont imposées, à adresser à ces titulaires une mise en demeure lorsqu'il est saisi d'une telle demande en application du 3ème alinéa de l'article 42. Ces dispositions laissent au CSA le soin d'apprécier sous le contrôle du juge, qui exerce en la matière un contrôle de l'erreur manifeste, si, compte tenu des circonstances et de la nature des manquements constatés, il y a lieu pour lui de prendre immédiatement une telle mesure.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
