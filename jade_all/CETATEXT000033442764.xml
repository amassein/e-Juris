<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033442764</ID>
<ANCIEN_ID>JG_L_2016_11_000000388832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/44/27/CETATEXT000033442764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 21/11/2016, 388832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:388832.20161121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler la décision, en date du 10 mai 2013, par laquelle la caisse d'allocations familiales de Paris a mis à sa charge un indu d'aide personnalisée au logement (APL) d'un montant de 5 155,30 euros, et de le décharger de l'obligation de payer cet indu, d'annuler la décision du 12 novembre 2013 par laquelle la commission départementale des aides publiques au logement a rejeté sa demande de remise de dette d'aide personnalisée au logement et prévu un remboursement par prélèvement mensuel, d'enjoindre à la caisse d'allocations familiales de Paris de lui restituer l'intégralité des sommes imputées au titre du remboursement du trop-perçu d'APL à hauteur de 949,64 euros au 4 juin 2014 et de condamner la caisse d'allocations familiales de Paris à lui verser une indemnité de 500 euros en réparation des préjudices subis.<br/>
<br/>
              Par un jugement n° 1317306/6-2 du 3 novembre 2014, le tribunal administratif de Paris a rejeté la demande de M.B....<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 19 mars et 1er avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat au profit de la SCP Rocheteau, Uzan-Sarano, son avocat, la somme de 2 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 10 mai 2013, la caisse d'allocations familiales de Paris a mis à la charge de M. A... B...le remboursement de la somme de 5 155,30 euros qu'il avait perçue au titre de l'aide personnalisée au logement depuis le 1er janvier 2012, au motif qu'il avait cessé à cette date de remplir les conditions légales pour bénéficier de cette prestation ; que l'intéressé a présenté le 23 mai 2013 un recours administratif contre cette décision, en soutenant qu'il n'était pas redevable de la somme qui lui était réclamée et en précisant que sa situation financière ne lui permettait pas de la reverser ; que la commission départementale des aides publiques au logement prévue par les dispositions alors en vigueur de l'article L. 351-14 du code de la construction et de l'habitation ne s'est pas prononcée sur le recours contre la décision de la caisse, qui a ainsi fait l'objet d'une décision implicite de rejet ; qu'en revanche, cette commission a estimé être saisie d'une demande de remise gracieuse qu'elle a rejetée par une décision du 12 novembre 2013 indiquant à l'intéressé que la somme réclamée serait retenue sur les sommes qui lui seraient dues au titre du revenu de solidarité active ; que le tribunal administratif de Paris, saisi par M. B... d'une demande tendant à l'annulation de la décision 10 mai 2013 et des décisions de la commission départementale, d'une demande d'injonction et d'une demande d'indemnisation, a rejeté ces conclusions par un jugement du 3 novembre 2014 contre lequel l'intéressé se pourvoit en cassation ;<br/>
<br/>
              Sur le rejet par le tribunal administratif des conclusions dirigées contre la décision du 10 mai 2013 de la caisse d'allocation familiales de Paris :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des décisions administratives et à l'amélioration des relations entre l'administration et le public, alors applicable et désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent./ A cet effet, doivent être motivées les décisions qui : / (...) imposent des sujétions (...) " ; que la décision par laquelle une caisse d'allocation familiale procède à la récupération des sommes indûment versées au titre de l'aide personnalisée au logement est au nombre des décisions imposant une sujétion et doit, par suite, être motivée en application de ces dispositions ; que, par suite, en estimant que la décision du 20 mai 2013 de la caisse d'allocation familiale de Paris, qui avait un tel objet, n'était pas soumise à l'obligation de motivation, le tribunal a commis une erreur de droit ;  <br/>
<br/>
              3. Considérant, toutefois, qu'aux termes de l'article L. 351-14 du code de la construction et de l'habitation, dans sa rédaction en vigueur à la date des décisions litigieuses : " Il est créé dans chaque département une commission compétente pour :/ (...) 2° Statuer sur les demandes de remise de dettes présentées à titre gracieux par les bénéficiaires de l'aide personnalisée au logement en cas de réclamation d'un trop-perçu effectuée par l'organisme payeur ;/ 3° Statuer sur les contestations des décisions des organismes ou services chargés du paiement de l'aide personnalisée au logement ou de la prime de déménagement./ (...) Les recours relatifs à ces décisions sont portés devant la juridiction administrative " ; qu'aux termes de l'article R. 351-47 du même code, alors applicable : " Les compétences prévues à l'article  L. 351-14 sont exercées par une commission dénommée "commission départementale des aides publiques au logement" (...) " ; qu'il résulte de ces dispositions que le recours qu'elles organisent devant les commissions départementales des aides publiques au logement contre les décisions prises par les caisses d'allocations familiales en matière d'aide personnalisée au logement est un préalable obligatoire à l'exercice d'un recours contentieux ; que la décision par laquelle la commission départementale statue sur un tel recours se substitue à la décision initiale de la caisse et peut seule faire l'objet d'un recours contentieux ; qu'il en résulte que la décision par laquelle la commission départementale des aides publiques au logement a rejeté le recours formé par M. B...contre la décision du 10 mai 2013 de la caisse d'allocation familiales de Paris s'est substituée à celle de la caisse ; que les conclusions présentées par M. B... contre cette dernière étaient donc sans objet et, par suite, irrecevables ;  que ce motif, qui n'implique l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs par lesquels le tribunal a rejeté les conclusions dirigées contre cette décision ; <br/>
<br/>
              Sur le rejet par le tribunal administratif des conclusions dirigées contre les décisions de la commission départementale des aides publiques au logement : <br/>
<br/>
              En ce qui concerne la décision rejetant la demande de décharge de la somme de 5 155,30 euros :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 1er de la loi du 11 juillet 1979, alors applicable et désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent./ A cet effet, doivent être motivées les décisions qui : (...) -rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire ; (...) " ; qu'il en résulte qu'en jugeant qu'une décision prise par une commission départementale des aides publiques au logement sur le fondement du 3° de l'article L. 351-14 du code de la construction et de l'habitation cité ci-dessus n'était pas soumise à l'obligation de motivation, le tribunal a entaché son jugement d'une erreur de droit ; que, toutefois, ainsi qu'il a été dit ci-dessus, la commission départementale a rejeté le recours formé par M. B...contre la décision de la caisse d'allocations familiales par une décision implicite ; qu'aux termes de l'article 5 de la loi du 11 juillet 1979, alors applicable  : " Une décision implicite intervenue dans les cas où la décision explicite aurait dû être motivée n'est pas illégale du seul fait qu'elle n'est pas assortie de cette motivation. Toutefois, à la demande de l'intéressé, formulée dans les délais du recours contentieux, les motifs de toute décision implicite de rejet devront lui être communiqués dans le mois suivant cette demande " ; qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que M. B... ait demandé que lui soit communiqués les motifs de la décision de rejet de la commission départementale ; qu'il n'était, par suite, pas fondé à soutenir que la commission aurait méconnu l'obligation de motivation qui s'imposait à elle ; <br/>
<br/>
              5. Considérant, toutefois, qu'il résulte des dispositions des articles R. 351-4 et R. 351-5 du code de la construction et de l'habitation que l'aide personnalisée au logement versée au cours d'une année civile est calculée au 1er janvier de cette année, en fonction des ressources perçues par le bénéficiaire, son conjoint et les personnes vivant habituellement au foyer au cours de l'avant-dernière année, dite " année civile de référence " ; qu'aux termes de l'article R. 351-7 du même code : " I.- Il est procédé à une évaluation forfaitaire des ressources de la personne et de son conjoint ou concubin lorsque les conditions ci-après sont réunies :/ 1° D'une part, (...), à l'occasion du renouvellement du droit autre que le premier, lorsqu'au cours de l'année civile de référence ni le bénéficiaire, ni son conjoint, ni son concubin n'a disposé de ressources appréciées selon les dispositions de l'article R. 351-5 ;/ 2° D'autre part, le bénéficiaire, son conjoint ou son concubin perçoit une rémunération./ (...) La condition relative à l'existence d'une activité professionnelle rémunérée (...) est appréciée au cours (...) du mois de novembre précédant le renouvellement du droit./ II.- L'évaluation forfaitaire correspond soit à 12 fois la rémunération mensuelle perçue par l'intéressé (...) le mois de novembre précédant le renouvellement du droit (...), soit, s'il s'agit d'une personne exerçant une activité professionnelle en qualité d'employeur ou de travailleur indépendant, à 1 500 fois le salaire minimum de croissance horaire en vigueur au 1er juillet qui précède (...) le renouvellement du droit " ; qu'il résulte de ces dispositions que lorsqu'un ménage n'a pas perçu de ressources prises en considération pour le calcul de l'aide personnalisée au logement au cours de l'année civile de référence mais qu'il apparaît, au mois de novembre de l'année suivante, qu'un de ses membres exerce désormais une activité professionnelle rémunérée, la caisse procède à une évaluation forfaitaire des ressources pour déterminer si les conditions d'un renouvellement pour une année civile sont remplies et fixer, le cas échéant, le montant de l'aide qui sera versée à compter du 1er janvier suivant ; qu'une activité professionnelle rémunérée au sens de ces dispositions est une activité qui permet à la personne qui l'exerce de disposer de revenus professionnels réguliers ;  <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui n'avait pas déclaré de ressources et percevait l'aide personnalisée au logement au taux maximum, a présenté en mars 2013 une demande de complément de revenu de solidarité active pour non salarié, en déclarant qu'il avait depuis janvier 2009 une activité d'auteur ; que la caisse d'allocation familiales a alors procédé à un réexamen de ses droits fondé sur une évaluation forfaitaire de ses revenus professionnels et a conclu qu'il ne remplissait pas les conditions pour obtenir le renouvellement à compter du 1er janvier 2012 de l'aide personnalisée au logement ; qu'à l'appui de son recours contre cette décision, l'intéressé a fait valoir devant le tribunal administratif qu'il n'avait perçu des droits d'auteur qu'en 2012 et 2013, à hauteur respectivement de 3 785 euros et de 1 419 euros ; que, sans prendre parti sur l'exactitude de cette affirmation, le jugement attaqué énonce que, dès lors qu'à l'occasion du renouvellement de son droit le requérant ne disposait pas de ressources appréciées selon les dispositions de l'article R. 351-5 du code de la construction et de l'habitation, la caisse était fondée à procéder à une évaluation forfaitaire ; qu'il résulte toutefois des dispositions citées au point 5 que le renouvellement du droit de percevoir l'aide personnalisée au logement ne pouvait être refusé au 1er janvier 2012 que si l'intéressé exerçait une activité professionnelle rémunérée en novembre 2011 ; que les faits allégués par l'intéressé, à les supposer exacts, impliquaient que cette condition n'était pas remplie ; que, s'agissant du renouvellement au 1er janvier 2013, il y avait lieu d'apprécier si les droits d'auteur que M. B...indiquait avoir perçus en 2012 pour un faible montant devaient le faire regarder comme exerçant désormais une activité lui procurant des revenus professionnels réguliers ou présentaient seulement le caractère d'un revenu ponctuel ; qu'il suit de là qu'en se prononçant comme il l'a fait, le tribunal administratif a commis une erreur de droit ; <br/>
<br/>
              En ce qui concerne la décision du 12 novembre 2013 par laquelle la commission départementale a prévu que le reversement serait imputé sur les prestations dues à l'intéressé : <br/>
<br/>
              7. Considérant qu'il ressort des mentions du jugement attaqué que le tribunal a rejeté les conclusions dirigées contre cette décision sans répondre aux moyens soulevés par M. B... à son encontre ; que son jugement est entaché sur ce point d'une insuffisance de motivation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le jugement attaqué doit être annulé en tant qu'il statue, d'une part, sur les conclusions de M. B...tendant à la décharge de la somme de 5 155,30 euros et, d'autre part, sur ses conclusions dirigées contre la décision de la commission départementale prévoyant l'imputation de cette somme sur les prestations qui lui étaient dues ; <br/>
<br/>
              9. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rocheteau et Uzan-Sarano, avocat de M. B... , renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Rocheteau et Uzan-Sarano ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 3 novembre 2014 est annulé en tant qu'il statue d'une part, sur les conclusions de M. B...tendant à la décharge de la somme de 5 155,30 euros et, d'autre part, sur ses conclusions dirigées contre la décision de la commission départementale des aides publiques au logement prévoyant l'imputation de ce montant sur les prestations qui lui étaient dues.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris dans la mesure de la cassation ainsi prononcée.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté. <br/>
Article 4 : l'Etat versera à la SCP Rocheteau et Uzan-Sarano, avocat de M. B..., la somme de 2 500 euros en application des dispositions des articles L. 761-1 et du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à la caisse d'allocations familiales de Paris et au ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
