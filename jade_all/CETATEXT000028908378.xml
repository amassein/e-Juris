<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028908378</ID>
<ANCIEN_ID>JG_L_2014_05_000000362741</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/83/CETATEXT000028908378.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 07/05/2014, 362741, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362741</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362741.20140507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre délégué, chargé du budget, enregistré le 13 septembre 2012 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat  d'annuler l'arrêt n° 11LY00196 du 13 juillet 2012 par lequel la cour administrative d'appel de Lyon, faisant droit à l'appel présenté par M. et Mme B...A..., a annulé le jugement n° 0705463 du 17 novembre 2010 du tribunal administratif de Grenoble et accordé aux contribuables la décharge des cotisations supplémentaires d'impôt sur le revenu qui leur ont été assignées au titre de l'année 2003 et la restitution de la somme de 117 231 euros acquittée au titre de la même année ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. ou Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'au cours de l'année 2003, la société Avenir Entretien, après avoir renoncé à l'acquisition, prévue dans un protocole d'accord du 12 novembre 2002, de la totalité du capital de la société Entreprise GuyC..., a versé aux actionnaires de cette société, dont MmeA..., néeC..., une somme de 1 000 000 euros ; que Mme A...a personnellement reçu la somme de 448 301 euros, correspondant au pourcentage de sa participation dans la société Entreprise GuyC..., qu'elle a déclarée en tant que gain sur cession de valeurs mobilières taxable au taux proportionnel ; qu'à l'issue d'un contrôle sur pièces, l'administration fiscale a imposé cette somme en tant que bénéfice non commercial selon le barème du taux progressif ; que le ministre délégué, chargé du budget, se pourvoit en cassation contre l'arrêt du 13 juillet 2012 par lequel la cour administrative d'appel de Lyon, faisant droit à la requête présentée par M. et MmeA..., a annulé le jugement du 17 novembre 2010 du tribunal administratif de Grenoble et accordé aux contribuables la décharge des cotisations supplémentaires d'impôt sur le revenu, résultant de ce redressement, qui leur ont été assignées au titre de l'année 2003 ainsi que la restitution de l'imposition primitive de la somme litigieuse résultant de sa déclaration comme plus-value de cession de valeurs mobilières ;<br/>
<br/>
              2. Considérant, en premier lieu, que la cour administrative d'appel a retenu, dans son arrêt, que la quote-part d'indemnité perçue par Mme A...devait être regardée comme compensant un préjudice sans lien avec une quelconque prestation et, par suite, que c'était à tort que l'administration fiscale avait considéré la somme en litige comme un revenu imposable dans la catégorie des bénéfices non commerciaux dès lors qu'elle ne relevait d'aucune autre catégorie de revenus ; que la cour a, ce faisant, jugé que cette somme n'était pas imposable ; qu'elle a ainsi implicitement mais nécessairement rejeté la demande présentée à titre subsidiaire par le ministre tendant à ce que ce montant soit imposé au taux proportionnel en tant que plus-value de cession de valeurs mobilières ; que, par suite, le moyen tiré de ce que la cour aurait omis de répondre à la demande de substitution de base légale présentée par le ministre, au demeurant sans aucun fondement  en l'absence de toute cession de valeurs mobilières, doit être écarté ;  <br/>
<br/>
              3. Considérant en second lieu, qu'aux termes de l'article 92 du code général des impôts : " 1. Sont considérés comme provenant de l'exercice d'une profession non commerciale ou comme revenus assimilés aux bénéfices non commerciaux les bénéfices des professions libérales (...) et de toutes occupations, exploitations lucratives et sources de profits ne se rattachant pas à une autre catégorie de bénéfices ou de revenus. (...) " ; <br/>
<br/>
              4. Considérant que la cour administrative d'appel de Lyon a relevé dans l'arrêt attaqué qu'en vertu d'une convention du 12 novembre 2002, les actionnaires de la société Entreprise Guy C...s'étaient engagés à céder à la société Avenir Entretien la totalité des actions de leur société ; que cet acte prévoyait notamment que le prix de vente, qui devait être payé en totalité le 15 mai 2003, serait définitivement évalué au 31 décembre 2002 en fonction de paramètres connus à cette date, en particulier le chiffre d'affaires définitif et le bénéfice consolidé de la société, et que le cessionnaire, à qui les comptes consolidés devaient être remis au plus tard le 31 mars 2003, bénéficierait d'un délai minimum de 45 jours pour effectuer son propre audit ; que l'article 5 de la convention accordait au cessionnaire un droit de rétractation, qu'il pouvait exercer, soit sans indemnité si l'audit pratiqué révélait une mauvaise gestion de la société ou de fausses informations données par les cédants sur des éléments essentiels, soit pour convenance personnelle moyennant le versement d'une indemnité de 1 250 000 euros ; que le cessionnaire a entendu user de son droit de rétractation sans toutefois pouvoir justifier de l'une des causes exonératoires du versement de l'indemnité ; que les parties ont alors conclu, le 4 février 2003, une transaction en exécution de laquelle le cessionnaire a versé aux cédants une indemnité de 1 250 000 euros, que les parties ont dénommée " prix de l'option " à hauteur de 1 000 000 euros, au bénéfice des actionnaires, et " dommages intérêts " pour le surplus, au bénéfice de la société Entreprise Guy C...; <br/>
<br/>
              5. Considérant qu'en analysant le protocole d'accord du 12 novembre 2002, qui stipule que la société Avenir Entretien " accepte " d'acquérir la totalité des actions de la société Entreprise Guy C...sous certaines conditions expressément mentionnées, comme une promesse synallagmatique d'achat et de vente assortie d'une faculté de rétractation en faveur du cessionnaire, la cour a souverainement apprécié les stipulations de cette convention sans les dénaturer ; qu'en jugeant ensuite, par une décision suffisamment motivée et après avoir porté une appréciation souveraine exempte de dénaturation sur les conditions dans lesquelles la société Avenir Entretien avait renoncé, sans justification, à l'acquisition du capital de la société Entreprise Guy C...et avait accepté de verser l'indemnité litigieuse aux actionnaires de cette société, que cette indemnité, versée par la société cessionnaire en compensation du préjudice subi par les cédants du fait de la rupture de l'accord ainsi conclu, ne pouvait être regardée, contrairement à ce que soutenait le ministre, comme une indemnité d'immobilisation susceptible d'être imposée sur le fondement de l'article 92 du code général des impôts ni comme un autre revenu entrant dans le champ de cet article, la cour n'a pas commis d'erreur de droit ni entaché sa décision d'une erreur de qualification juridique ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le ministre n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre délégué, chargé du budget, est rejeté. <br/>
Article 2 : L'Etat versera à M. et Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
