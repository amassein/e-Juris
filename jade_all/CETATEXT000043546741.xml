<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546741</ID>
<ANCIEN_ID>JG_L_2021_05_000000441977</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546741.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 27/05/2021, 441977, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441977</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441977.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 20 juillet 2020 et 18 janvier 2021, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-487 du 28 avril 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " système d'information sur les armes " ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'article 1er de ce décret ;<br/>
<br/>
              3°) à titre très subsidiaire d'annuler les dispositions de l'article R. 312-85 du code de la sécurité intérieure tel qu'il résulte de l'article 1er de ce décret ;<br/>
<br/>
              4°) à titre infiniment subsidiaire, d'annuler le VI de l'article R. 312-85 et l'article R. 312-90 du même code tels qu'ils résultent de l'article 1er de ce décret.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ;<br/>
              - la directive (UE) 2016/680 du Parlement européen et du Conseil du 27 avril 2016 ;<br/>
              - la directive (UE) 2017/853 du Parlement européen et du Conseil du 17 mai 2017 ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B... demande l'annulation pour excès de pouvoir du décret n° 2020-487 du 28 avril 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " système d'information sur les armes ", qui a pour finalités la gestion et le suivi des titres ou autorisations d'acquisition et de détention, de fabrication, commerce et intermédiation, de port et de transport d'armes, de munitions et de leurs éléments, ainsi que la dématérialisation des formalités administratives relatives aux armes pour les usagers. Le décret insère dans le code de la sécurité intérieure des articles R. 312-84 à R. 312-90, qui définissent, outre les finalités de ce traitement, la nature et la durée de conservation des données enregistrées, les catégories de personnes y ayant accès ainsi que celles qui en sont destinataires, et précise les modalités d'exercice des droits des personnes concernées. <br/>
<br/>
              Sur la publication de l'avis de la Commission nationale de l'informatique et des libertés :<br/>
<br/>
              2. L'article 31 de la loi n° 78-17 du 6 janvier 1978 dispose que : " I. - Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; / 2° Ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté. / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. - Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission. Cet avis est publié avec le décret autorisant le traitement ". <br/>
<br/>
              3. Ces dispositions, qui régissent les modalités de publication du décret attaqué, sont sans incidence sur la légalité de ce décret. Par suite, le requérant ne peut utilement soutenir que la circonstance que l'avis de la Commission nationale de l'informatique et des libertés, donné par délibération du 9 janvier 2020, ait été publié quelques jours après la publication du décret entacherait ce dernier d'irrégularité. <br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              En ce qui concerne les dispositions du décret relatives à la conservation, la collecte et le traitement de certaines données personnelles sensibles :<br/>
<br/>
              4. Le V de l'article R. 312-85 du code de la sécurité intérieure prévoit que peuvent être enregistrées dans le traitement des données issues de l'enquête administrative prévue aux 1° et 2° de l'article R. 114-5 et relative aux autorisations ou agréments régissant l'ensemble des activités afférentes aux armes. Aux termes du VI de ce même article, créé par le décret attaqué : " (...) sont autorisés, en cas de nécessité absolue pour les seules fins et dans le respect des conditions applicables à ce traitement, la collecte, la conservation et le traitement de données strictement nécessaires, adéquates et non excessives qui figurent dans l'enquête administrative mentionnée au V et relatives : / 1° Aux opinions politiques, aux convictions religieuses, à l'appartenance syndicale ou à la santé de la personne faisant l'objet de cette enquête administrative ; / 2° A la prétendue origine raciale ou ethnique, aux opinions politiques, aux convictions religieuses, à l'appartenance syndicale, à la vie sexuelle ou l'orientation sexuelle d'une personne autre que celle faisant l'objet de l'enquête administrative mentionnée au V, sous réserve que ces données se rapportent à une procédure dans laquelle la personne faisant l'objet de l'enquête administrative est mise en cause ". <br/>
<br/>
              5. Ainsi que le prévoit le considérant n° 32 de la directive (UE) 2017/853 du 17 mai 2017, le " système d'information sur les armes " créé par le décret attaqué, qui a pour objet principal l'organisation de la traçabilité des armes à feu sur le territoire national, est soumis aux obligations du règlement (UE) 2016/679 du 27 avril 2016 dit règlement général sur la protection des données (" RGPD "), ainsi qu'à la directive (UE) 2016/680 du 27 avril 2016, lorsque des données recueillies par ce système d'information sont traitées à des fins de prévention et de détection des infractions pénales, d'enquêtes et de poursuites en la matière ou d'exécution de sanctions pénales. Il relève également des dispositions communes prévues par le titre I de la loi du 6 janvier 1978, et en particulier de son article 4, aux termes duquel : " Les données à caractère personnel doivent être : / 1° Traitées de manière loyale et licite (...) ; 2° Collectées pour des finalités déterminées, explicites et légitimes, et ne pas être traitées ultérieurement d'une manière incompatible avec ces finalités. (...) / 3° Adéquates, pertinentes et au regard des finalités pour lesquelles elles sont collectées, limitées à ce qui est nécessaire ou, pour les traitements relevant des titres III et IV, non excessives (...) ; / 5° Conservées sous une forme permettant l'identification des personnes concernées pendant une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées ". Il résulte de ces dispositions que l'ingérence dans l'exercice du droit de toute personne au respect de sa vie privée que constituent la collecte, la conservation et le traitement, par une autorité publique, de données à caractère personnel, ne peut être légalement autorisée que si elle répond à des finalités légitimes et que le choix, la collecte et le traitement des données sont effectués de manière adéquate et proportionnée au regard de ces finalités.<br/>
<br/>
              6. Le I de l'article 6 de la loi du 6 janvier 1978 précitée interdit le traitement " des données à caractère personnel qui révèlent la prétendue origine raciale ou l'origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale d'une personne physique ou de traiter des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique ". Son II prévoit la possibilité de déroger à cette interdiction " dans les conditions prévues par le 2 de l'article 9 du règlement (UE) 2016/679 du 27 avril 2016 et par la présente loi ". Aux termes du g) du 2 de l'article 9 du règlement du 27 avril 2016, le traitement de telles données sensibles est autorisé " pour des motifs d'intérêt public important, sur la base du droit de l'Union ou du droit d'un Etat membre qui doit être proportionné à l'objectif poursuivi, respecter l'essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée ". Par ailleurs, en vertu de l'article 88 de la loi du 6 janvier 1978, relatif aux traitements relevant de la directive (UE) 2016/680 du 27 avril 2016 : " Le traitement de données mentionnées au I de l'article 6 est possible uniquement en cas de nécessité absolue, sous réserve de garanties appropriées pour les droits et libertés de la personne concernée, et soit s'il est autorisé par une disposition législative ou réglementaire, soit s'il vise à protéger les intérêts vitaux d'une personne physique, soit s'il porte sur des données manifestement rendues publiques par la personne concernée ".<br/>
<br/>
              7. Il résulte de ce qui précède que, s'agissant du traitement automatisé de données à caractère personnel dénommé " système d'information sur les armes ", le cadre juridique applicable aux dérogations à l'interdiction fixée par le I de l'article 6 est soit celui fixé par le RGPD, soit celui fixé par la directive (UE) 2016/680 du 27 avril 2016, selon la finalité poursuivie par le traitement. La collecte de données sensibles issues d'enquêtes administratives, qui a pour objet de s'assurer que la personne sollicitant l'autorisation de fabriquer, vendre, acquérir, détenir, porter ou transporter des armes à feu, n'a pas un comportement incompatible avec la délivrance d'une telle autorisation, qui sont conservées pendant une durée maximale d'un an en cas de déclaration ou de demande d'autorisation ou, en cas de litige, jusqu'à ce qu'il ait été statué définitivement sur ce dernier, poursuit une finalité qui se rattache à celles régies par la directive (UE) 2016/680 du 27 avril 2016.<br/>
<br/>
              8. En premier lieu, si le 1° du VI de l'article R. 312-85 autorise la collecte, la conservation et le traitement des données relatives aux opinions politiques, aux convictions religieuses, à l'appartenance syndicale ou à la santé des personnes ayant fait l'objet d'une enquête administrative, il n'ouvre une telle possibilité qu'en cas de nécessité absolue, pour les seules donnée strictement nécessaires, adéquates et non excessives, et aux seules fins assignées au traitement, dans le respect des conditions qui lui sont applicables. Il interdit toute sélection d'une catégorie particulière de personnes à partir de ces seules données. Dans ces conditions, le décret attaqué ne méconnaît pas les exigences légales auxquelles est soumis le traitement de ces données en application de l'article 88 de la loi du 6 janvier 1978.<br/>
<br/>
              9. En deuxième lieu, si le 2° du même VI autorise la collecte, la conservation et le traitement des données relatives à la prétendue origine raciale ou ethnique, aux opinions politiques, aux convictions religieuses, à l'appartenance syndicale, à la vie sexuelle ou à l'orientation sexuelle de tiers, cette possibilité est, d'une part, soumise aux mêmes conditions que celles citées au point 8 - n'étant ainsi ouverte qu'en cas de nécessité absolue, pour les seules donnée strictement nécessaires, adéquates et non excessives, et aux seules fins assignées au traitement, dans le respect des conditions qui lui sont applicables - et n'est, d'autre part, admise que si ces données se rapportent à une procédure dans laquelle la personne faisant l'objet d'une enquête est mise en cause. Ces dispositions, qui tendent ainsi, en particulier, à permettre que figurent dans le traitement des données relatives, le cas échéant, aux personnes qui auraient été les victimes d'un comportement répréhensible de la part de la personne faisant l'objet de l'enquête, sans permettre leur identification directe, lorsque ces données portent sur des éléments qui ont été la cause ou le facteur aggravant de ce comportement ne méconnaissent ni les exigences légales auxquelles est soumis le traitement de ces données, ni l'objectif d'accessibilité et d'intelligibilité de la norme.<br/>
<br/>
              En ce qui concerne la limitation aux droits d'opposition, d'information, d'accès et de rectification :<br/>
<br/>
              10. En vertu des articles 13 à 16 de la directive (UE) 2016/680 du 27 avril 2016, les Etats membres peuvent adopter des mesures législatives limitant les droits d'information, d'accès, de rectification et à la limitation des données ouverts en principe aux personnes concernées " dès lors et aussi longtemps qu'une mesure de cette nature constitue une mesure nécessaire et proportionnée dans une société démocratique, en tenant dûment compte des droits fondamentaux et des intérêts légitimes de la personne physique concernée pour : a) éviter de gêner des enquêtes, des recherches ou des procédures officielles ou judiciaires ; b) éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ; c) protéger la sécurité publique ; d) protéger la sécurité nationale ; e) protéger les droits et libertés d'autrui ". Ces dispositions ont été transposées aux articles 107 et 110 de la loi du 6 janvier 1978. L'article 23 du RGPD permet également de limiter ces droits pour garantir, notamment, la sécurité nationale, la défense nationale, la sécurité publique, la prévention et la détection d'infractions pénales, ainsi que les enquêtes et les poursuites en la matière, la protection contre les menaces pour la sécurité publique et la prévention de telles menaces.<br/>
<br/>
              11. Le I de l'article R. 312-90 du code de la sécurité intérieure, créé par l'article 1er du décret attaqué, exclut tout droit d'opposition de la personne concernée à ce que ses données à caractère personnel puissent faire l'objet d'un traitement. Le II du même article prévoit que les droits d'information, d'accès, de rectification et à la limitation des données s'exercent auprès du service central des armes ou du préfet territorialement compétent, en fonction de leurs attributions respectives, tout en précisant qu'" afin d'éviter de gêner des enquêtes, des recherches ou des procédures administratives ou judiciaires ou d'éviter de nuire à la prévention ou à la détection d'infractions pénales, aux enquêtes ou aux poursuites en la matière ou à l'exécution de sanctions pénales ou de protéger la sécurité publique les droits d'accès, de rectification et à la limitation peuvent faire l'objet de restrictions en application de l'article 52 et des 2° et 3° du II et du III de l'article 107 de la loi du 6 janvier 1978 ".<br/>
<br/>
              12. Contrairement à ce que soutient M. B..., les données personnelles enregistrées dans le traitement dénommé " système d'information sur les armes " sont susceptibles d'être exploitées dans le cadre d'enquêtes, de recherches ou de procédures liées à la prévention ou à la poursuite d'infractions pénales ou à la protection de la sécurité publique. En outre, ni l'article 35 de la loi de 1978, qui est relatif au contenu obligatoire de tout acte portant création d'un traitement, ni aucune autre disposition, ne font obligation de rappeler, dans cet acte, le droit d'information dont disposent de plein droit les personnes concernées, conformément aux articles 104 et 105 de la même loi. Il s'ensuit que le moyen tiré de ce que le décret ne pouvait légalement permettre que soient mises en oeuvre les restrictions autorisées par l'article 23 du RGPD et les articles 107 à 110 de la loi du 6 janvier 1978 ne peut, par suite, qu'être écarté.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
