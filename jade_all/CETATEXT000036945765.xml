<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945765</ID>
<ANCIEN_ID>JG_L_2018_05_000000415695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945765.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème chambre jugeant seule, 25/05/2018, 415695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415695.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 14 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société anonyme Technicolor demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe n° 220 des commentaires administratifs publiés le 10 avril 2013 au Bulletin officiel des finances publiques sous la référence BOI-IS-DEF-10-30 en tant qu'ils interprètent la loi comme excluant les entreprises qui consentent un abandon de créance à des entreprises en difficulté du bénéfice de la majoration du plafond d'imputation des déficits prévue par le 4ème alinéa du I de l'article 209 du code général des impôts ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la décision n° 2018-700 QPC du 13 avril 2018 du Conseil constitutionnel ; <br/>
              - le code général des impôts ;<br/>
              - la loi n° 2012-1509 du 29 décembre 2012 ;<br/>
              - la loi n° 2016-1917 du 29 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu du 3ème alinéa du I de l'article 209 du code général des impôts : " Sous réserve de l'option prévue à l'article 220 quinquies, en cas de déficit subi pendant un exercice, ce déficit est considéré comme une charge de l'exercice suivant et déduit du bénéfice réalisé pendant ledit exercice dans la limite d'un montant de 1 000 000 &#128; majoré de 50 % du montant correspondant au bénéfice imposable dudit exercice excédant ce premier montant (...) ". Selon le 4ème alinéa de ce I, dans sa version résultant de l'article 24 de la loi du 29 décembre 2012 de finances pour 2013 : " La limite de 1 000 000 &#128; mentionnée au troisième alinéa est majorée du montant des abandons de créances consentis à une société en application d'un accord constaté ou homologué dans les conditions prévues à l'article L. 611-8 du code de commerce ou dans le cadre d'une procédure de sauvegarde, de redressement judiciaire ou de liquidation judiciaire ouverte à son nom ". Dans sa version issue du I de l'article 17 de la loi du 29 décembre 2016 de finances pour 2017, ce 4ème alinéa dispose : " Pour les sociétés auxquelles sont consentis des abandons de créances dans le cadre d'un accord constaté ou homologué dans les conditions prévues à l'article L. 611-8 du code de commerce ou lors d'une procédure de sauvegarde, de redressement judiciaire ou de liquidation judiciaire ouverte à leur nom, la limite de 1 000 000 euros mentionnée à l'avant-dernier alinéa du présent article est majorée du montant desdits abandons de créances ". Le II de cet article 17 dispose : " Les dispositions du I ont un caractère interprétatif ".<br/>
<br/>
              2. Par sa décision n° 2018-700 QPC du 13 avril 2018, le Conseil constitutionnel a jugé que, dans le but de lever toute ambiguïté sur la détermination des sociétés bénéficiaires de la majoration du plafond d'imputation des déficits prévue par les dispositions citées ci-dessus, la loi du 29 décembre 2016 avait remplacé les dispositions issues de la loi du 29 décembre 2012 par d'autres, plus claires, ayant le même objet et la même portée et a déclaré le paragraphe II de l'article 17 de la loi du 29 décembre 2016, qui confère un caractère interprétatif à cette nouvelle rédaction, conforme à la Constitution.<br/>
<br/>
              3. Par suite, la société requérante ne peut utilement soutenir que les commentaires qu'elle conteste méconnaîtraient le sens et la portée des dispositions du 4ème alinéa du I de l'article 209 du code général des impôts, dans leur rédaction issue de la loi de finances pour 2013, dès lors que la rédaction de ces dispositions issue de la loi de finances pour 2017 s'y est rétroactivement substituée et doit ainsi être regardée comme celle en vigueur à la date à laquelle les commentaires litigieux ont été publiés.<br/>
<br/>
              4. Il résulte de ce qui précède que la requête de la société Technicolor ne peut qu'être rejetée, y compris ses conclusions tendant à la mise en oeuvre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société Technicolor est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société anonyme Technicolor et au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
