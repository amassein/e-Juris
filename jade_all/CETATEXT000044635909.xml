<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635909</ID>
<ANCIEN_ID>JG_L_2021_12_000000437489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635909.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 29/12/2021, 437489</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; CORLAY</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437489.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au tribunal administratif d'Orléans d'annuler l'arrêté du 18 janvier 2016 du maire de Saint-Lubin-des-Joncherets (Eure-et-Loir) le mettant à la retraite pour invalidité et l'avis de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) du 15 janvier 2016 pris à la suite de l'avis du 28 octobre 2014 de sa commission de réforme ainsi que, subsidiairement, d'ordonner une expertise médicale.<br/>
<br/>
              Par un jugement nos 1400757, 1403123, 1403331, 1501951, 1600989 du 4 octobre 2016, le tribunal administratif d'Orléans a, notamment, rejeté comme irrecevables les conclusions dirigées contre l'avis de la CNRACL, écarté les moyens tirés de l'illégalité externe de l'avis de la commission de réforme et, avant-dire droit, ordonné une expertise médicale.<br/>
<br/>
              Par un jugement n° 1600989 du 17 octobre 2017, le tribunal administratif d'Orléans a annulé l'arrêté du 18 janvier 2016.<br/>
<br/>
              Par un arrêt n° 17NT03809 du 8 novembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel dirigé par la commune de Saint-Lubin-des-Joncherets contre le jugement du 17 octobre 2017.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 janvier et 20 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Lubin-des-Joncherets demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du 17 octobre 2017 du tribunal administratif d'Orléans ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le décret n° 2010-1357 du 9 novembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Corlay, avocat de la commune de Saint-Lubin-des-Joncherets et à la SCP Waquet, Farge, Hazan, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... B..., technicien territorial principal de 1ère classe, employé au sein des services de la commune de Saint-Lubin-des-Joncherets (Eure-et-Loir) depuis le 1er octobre 1996, a été admis à la retraite pour invalidité, à compter du 1er avril 2015, par un arrêté du 18 janvier 2016 du maire de cette commune. Par un jugement du 17 octobre 2017, le tribunal administratif d'Orléans, après avoir prescrit, par un jugement avant-dire droit du 4 octobre 2016, une expertise médicale portant sur l'aptitude de M. B... à reprendre ses fonctions au jour de l'édiction de l'arrêté du 18 janvier 2016, a annulé cette décision. La commune se pourvoit en cassation contre l'arrêt du 8 novembre 2019 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 30 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales : " Le fonctionnaire qui se trouve dans l'impossibilité définitive et absolue de continuer ses fonctions par suite de maladie, blessure ou infirmité grave dûment établie peut être admis à la retraite soit d'office, soit sur demande. / (...) / La mise en retraite d'office pour inaptitude définitive à l'exercice de l'emploi ne peut être prononcée qu'à l'expiration des congés de maladie, des congés de longue maladie et des congés de longue durée dont le fonctionnaire bénéficie en vertu des dispositions statutaires qui lui sont applicables, sauf dans les cas prévus à l'article 39 si l'inaptitude résulte d'une maladie ou d'une infirmité que son caractère définitif et stabilisé ne rend pas susceptible de traitement. (...) ". Aux termes de l'article 31 de ce décret : " Une commission de réforme est constituée dans chaque département pour apprécier la réalité des infirmités invoquées, la preuve de leur imputabilité au service, les conséquences et le taux d'invalidité qu'elles entraînent, l'incapacité permanente à l'exercice des fonctions. (...) / Le pouvoir de décision appartient dans tous les cas à l'autorité qui a qualité pour procéder à la nomination, sous réserve de l'avis conforme de la Caisse nationale de retraites des agents des collectivités locales. / (...) / La Caisse nationale de retraites des agents des collectivités locales peut, à tout moment, obtenir la communication du dossier complet de l'intéressé, y compris les pièces médicales. Tous renseignements médicaux ou pièces médicales dont la production est indispensable pour l'examen des droits définis au présent titre pourront être communiqués, sur leur demande, aux services administratifs dépendant de l'autorité à laquelle appartient le pouvoir de décision ainsi qu'à ceux de la Caisse nationale de retraites des agents des collectivités locales. / (...) ". Aux termes de l'article 39 de ce décret : " Le fonctionnaire qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'une invalidité ne résultant pas du service peut être mis à la retraite par anticipation soit sur demande, soit d'office dans les délais prévus au troisième alinéa de l'article 30 (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions que lorsqu'un fonctionnaire territorial, ayant épuisé ses droits aux congés de maladie, de longue maladie et de longue durée, se trouve définitivement inapte à l'exercice de tout emploi, il est admis à la retraite, soit d'office, soit à sa demande, après avis de la commission de réforme et que l'autorité territoriale doit, préalablement à la mise à la retraite, obtenir un avis conforme de la Caisse nationale de retraites des agents des collectivités locales. La légalité de la décision qu'il appartient à l'autorité territoriale de prendre en vue du placement d'office d'un fonctionnaire à la retraite par anticipation, pour les motifs et, lorsqu'elles sont réunies, dans les conditions déterminées par ces dispositions, s'apprécie au regard de l'ensemble des pièces et renseignements propres à établir la réalité de la situation effective de santé de ce fonctionnaire au jour de cette décision, y compris au regard de ceux de ces renseignements ou pièces qui n'auraient pas été communiqués à l'autorité territoriale préalablement à sa décision ou qui auraient été établis ou analysés postérieurement à celle-ci, dès lors qu'ils éclairent cette situation. Le juge administratif exerce un contrôle normal sur l'appréciation portée par l'autorité territoriale sur l'inaptitude définitive d'un fonctionnaire. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond, d'une part, que la commission de réforme des fonctionnaires des collectivités locales, lors de sa séance du 28 octobre 2014, rejoignant le sens de l'avis adopté le 17 juin 2014 par le comité médical départemental qui avait été défavorable à la réintégration du fonctionnaire à l'issue de son congé de longue maladie le 9 juin 2014, a été d'avis que M. B... était dans l'impossibilité absolue et définitive de continuer ses fonctions et, d'autre part, que la Caisse nationale de retraites des agents des collectivités locales a adopté, le 15 janvier 2016, un avis favorable au placement d'office de ce fonctionnaire à la retraite pour invalidité, qui a été suivi par l'arrêté contesté pris le 18 janvier 2016. Toutefois, le médecin désigné en exécution du jugement du tribunal administratif d'Orléans du 4 octobre 2016 a conclu dans son rapport d'expertise du 16 novembre 2016 qu'il résultait des pièces médicales du dossier que l'état de santé de M. B..., tel qu'il devait être constaté au 18 janvier 2016, était exempt de pathologie et ne le rendait pas inapte à l'exercice de ses fonctions ou de tout autre poste de travail. Parmi les pièces médicales examinées par cet expert et fondant sa conclusion, qui n'avaient pas été communiquées à la commune avant l'adoption de l'arrêté contesté, figurent notamment les rapports et certificats établis, à l'époque de la séance de la commission de réforme, par le médecin traitant de M. B..., le 22 septembre 2014, ainsi que par deux médecins spécialistes, le 20 octobre 2014 et le 28 octobre 2014.<br/>
<br/>
              5. Dès lors que ce rapport de l'expert désigné par le tribunal administratif ainsi que les pièces et renseignements médicaux sur lesquels il s'est fondé pour l'établir étaient propres à établir la réalité de l'état de santé de M. B... au 18 janvier 2016, c'est sans commettre d'erreur de droit que la cour administrative d'appel de Nantes a jugé, sur le fondement des constatations non contredites résultant de ces rapport, pièces et renseignements, par un arrêt qui est suffisamment motivé, que le maire de Saint-Lubin-des-Joncherets avait commis une erreur d'appréciation en estimant que M. B... présentait, au 18 janvier 2016, une inaptitude définitive et absolue à l'exercice de ses fonctions. Par suite, la commune n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. B..., qui n'est pas la partie perdante dans la présente instance, la somme que la commune de Saint-Lubin-des-Joncherets demande à ce titre. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Lubin-des-Joncherets la somme de 3 000 euros à verser à M. B... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Saint-Lubin-des-Joncherets est rejeté.<br/>
Article 2 : La commune de Saint-Lubin-des-Joncherets versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Lubin-des-Joncherets et à M. C... B....<br/>
              Délibéré à l'issue de la séance du 26 novembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. K... E..., M. Pierre Collin, présidents de chambre ; M. I... M..., M. J... G..., M. F... L..., M. D... N..., M. Pierre Boussaroque, conseillers d'Etat et M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Laurent-Xavier Simonel<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-03 FONCTIONNAIRES ET AGENTS PUBLICS. - CESSATION DE FONCTIONS. - MISE À LA RETRAITE D'OFFICE. - INAPTITUDE DÉFINITIVE D'UN FONCTIONNAIRE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-09-01 FONCTIONNAIRES ET AGENTS PUBLICS. - CESSATION DE FONCTIONS. - RADIATION DES CADRES. - INAPTITUDE PHYSIQUE. - INAPTITUDE DÉFINITIVE D'UN FONCTIONNAIRE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. - POUVOIRS ET DEVOIRS DU JUGE. - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. - APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - INAPTITUDE DÉFINITIVE D'UN FONCTIONNAIRE [RJ1].
</SCT>
<ANA ID="9A"> 36-10-03 Le juge administratif exerce un contrôle normal sur l'appréciation portée par l'autorité administrative sur l'inaptitude définitive d'un fonctionnaire.</ANA>
<ANA ID="9B"> 36-10-09-01 Le juge administratif exerce un contrôle normal sur l'appréciation portée par l'autorité administrative sur l'inaptitude définitive d'un fonctionnaire.</ANA>
<ANA ID="9C"> 54-07-02-03 Le juge administratif exerce un contrôle normal sur l'appréciation portée par l'autorité territoriale sur l'inaptitude définitive d'un fonctionnaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant plus généralement des appréciations portées sur l'aptitude physique des fonctionnaires, CE, 19 juin 1970, Ministre de l'Economie et des Finances c/ Clary, n° 76538, T. pp. 1079-1090-1164. Rappr., s'agissant du licenciement d'un stagiaire pour inaptitude physique, CE, 16 juin 1995, Commune de Savigny-le-Temple c/ Mme Bieret, n° 114957, p. 250.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
