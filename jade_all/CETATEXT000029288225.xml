<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288225</ID>
<ANCIEN_ID>JG_L_2014_07_000000360473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288225.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 23/07/2014, 360473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360473.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juin et 24 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 11LY02071 du 5 avril 2012 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre le jugement n° 0905389 du 17 juin 2011 du tribunal administratif de Grenoble rejetant sa demande tendant, d'une part, à l'annulation de la décision du 5 octobre 2009 du directeur des Hôpitaux du Léman selon laquelle son contrat viendrait à terme le 2 novembre suivant et ne serait pas renouvelé, d'autre part, à ce qu'il soit enjoint aux Hôpitaux du Léman de le réintégrer dans ses fonctions ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge des Hôpitaux du Léman la somme de 5 450 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ; <br/>
<br/>
              Vu le décret n° 91-155 du 6 février 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par un contrat d'une durée de six mois signé le 26 janvier 2009, M. A... a été engagé par les Hôpitaux du Léman pour faire face à la vacance d'un poste pendant la période comprise entre le 2 février et le 2 août 2009 ; que, conformément à l'article 15 du contrat, prévoyant qu'il pourrait être renouvelé dans des conditions devant faire l'objet " d'un avenant soumis à l'agent préalablement au terme initialement fixé ", le directeur de l'établissement a transmis à M.A..., le 16 juillet 2009, un avenant prolongeant le contrat initial pour la période comprise entre le 3 août et le 2 novembre 2009 ; que l'intéressé n'a pas signé cet avenant mais a néanmoins continué à exercer ses fonctions ; que, par une lettre du 5 octobre 2009, le directeur de l'établissement l'a informé que son contrat ne serait pas renouvelé après le 2 novembre suivant ; qu'estimant être titulaire, depuis le 3 août 2009, soit d'un contrat à durée indéterminée soit, à tout le moins, d'un contrat d'une durée égale à celle de son contrat initial, M. A...a demandé au tribunal administratif de Grenoble d'annuler ce qu'il regardait comme une décision de mettre fin à ses fonctions en cours de contrat ; que cette demande a été rejetée par un jugement du 17 juin 2011 confirmé par un arrêt du 5 avril 2012 de la cour administrative d'appel de Lyon contre lequel il se pourvoit en cassation ;  <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que la cour administrative d'appel a relevé que, les contrats successifs dont M. A...était titulaire comportant une durée fixe et un terme certain, ces engagements n'avaient pas fait naître, à son profit, un contrat à durée indéterminée, alors même que le contrat initial était susceptible de déboucher sur un contrat à durée indéterminée en fonction de sa manière de servir et que l'intéressé s'était abstenu de signer l'avenant qui lui avait été proposé ; qu'elle en a déduit que la décision contestée du 5 octobre 2009 présentait le caractère non pas d'un licenciement, qui aurait dû être précédé d'un préavis et d'un entretien préalable, mais d'un refus de renouvellement qui n'avait pas à faire l'objet de telles garanties ; que la cour, devant laquelle M. A...n'avait pas repris le moyen qu'il avait invoqué en première instance à titre subsidiaire, tiré de ce qu'il devait à tout le moins être regardé comme titulaire à compter du 3 août 2009 d'un contrat d'une durée de six mois, a ainsi donné à son arrêt une motivation suffisante ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt : <br/>
<br/>
              3. Considérant qu'aux termes de l'article 9-1 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, dans sa rédaction applicable à la date de la décision litigieuse : " Les établissements peuvent recruter des agents contractuels pour assurer le remplacement momentané de fonctionnaires hospitaliers indisponibles ou autorisés à exercer leurs fonctions à temps partiel. Les agents ainsi recrutés sont engagés par des contrats d'une durée déterminée. / Ils peuvent également recruter des agents contractuels pour faire face temporairement et pour une durée maximale d'un an à la vacance d'un emploi qui ne peut être immédiatement pourvu dans les conditions prévues par le présent titre. / Ils peuvent, en outre, recruter des agents contractuels pour exercer des fonctions occasionnelles pour une durée maximale d'un an " ; que, selon l'article 41 du décret du 6 février 1991 relatif aux dispositions générales applicables aux agents contractuels des établissements publics de santé : " Lorsque l'agent contractuel a été recruté pour une période déterminée susceptible d'être reconduite, l'autorité signataire du contrat notifie son intention de renouveler ou non le contrat, au plus tard : / (...) 2° Au début du mois précédant le terme de l'engagement pour l'agent recruté pour une durée égale à six mois et inférieure à deux ans. (...) " ; qu'il résulte de ces dispositions que les contrats passés par les établissements publics de santé, sur le fondement de l'article 9-1 de la loi du 9 janvier 1986, en vue de recruter des agents contractuels, notamment pour faire face temporairement à la vacance d'un emploi, ne peuvent être conclus que pour une durée déterminée et ne peuvent être renouvelés que par reconduction expresse ; <br/>
<br/>
              4. Considérant que la seule circonstance qu'un agent contractuel employé dans le cadre des dispositions précitées soit maintenu en fonctions au-delà du terme de son contrat initial, alors qu'il n'a pas signé l'avenant qui lui était proposé afin de reconduire ce contrat pour une durée déterminée, ne saurait faire regarder l'intéressé comme titulaire d'un contrat à durée indéterminée, ni d'un contrat à durée déterminée d'une durée autre que celle mentionnée dans l'avenant ; qu'ainsi, la cour administrative d'appel n'a pas commis d'erreur de droit et n'a pas davantage entaché son arrêt de dénaturation en regardant la décision de mettre un terme aux fonctions de M. A... après le 2 novembre 2009, terme de l'engagement prévu par l'avenant qui lui avait été proposé, comme une décision de ne pas renouveler son contrat et non comme un licenciement en cours de contrat, auquel l'établissement aurait procédé sans le faire bénéficier des garanties prévues en pareil cas ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et aux Hôpitaux du Léman.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
