<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254047</ID>
<ANCIEN_ID>JG_L_2018_07_000000418417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254047.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 418417, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418417.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 418417,<br/>
<br/>
              La société d'aménagement et de valorisation de la gare Saint-Lazare (SOAVAL) a demandé au juge des référés du tribunal administratif de Paris d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à la société Rose et Lys de libérer les emplacements M 82 C et M 83 R appartenant au domaine public ferroviaire de la gare Saint-Lazare qu'elle occupe illégalement sous astreinte de 2 500 euros par jour de retard et de l'autoriser à y procéder d'office, au besoin avec le concours de la force publique. Par une ordonnance n° 1800773 du 5 février 2018, le juge des référés a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 février, 8 mars et 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société d'aménagement et de valorisation de la gare Saint-Lazare demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la société Rose et Lys la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 418938, <br/>
<br/>
              La société d'aménagement et de valorisation de la gare Saint-Lazare (SOAVAL) a demandé au juge des référés du tribunal administratif de Paris d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à la société Rose et Lys de libérer les emplacements M 82 C et M 83 R appartenant au domaine public ferroviaire de la gare Saint-Lazare qu'elle occupe illégalement sous astreinte de 2 500 euros par jour de retard et de l'autoriser à y procéder d'office, au besoin avec le concours de la force publique. Par une ordonnance n° 1802864 du 23 février 2018, le juge des référés a rejeté cette demande.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 12 mars et 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société d'aménagement et de valorisation de la gare Saint-Lazare demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la société Rose et Lys la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
               Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société d'aménagement et de valorisation de la gare Saint-Lazare et à la SCP Piwnica, Molinié, avocat de la société Rose et Lys enseigne Heller.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de la société d'aménagement et de valorisation de la gare Saint-Lazare présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge des référés que la société d'aménagement et de valorisation de la gare Saint-Lazare a, le 26 décembre 2011, conclu avec la société Elirose, devenue la société Rose et Lys, une convention autorisant cette dernière à occuper, pour une durée de six ans, le commerce M 82 C et le local M 83 R au sein de l'espace commercial situé dans la gare Saint-Lazare à Paris. Par deux requêtes successives présentées sur le fondement de l'article L. 521-3 du code de justice administrative, la société d'aménagement et de valorisation de la gare Saint-Lazare a demandé au juge des référés du tribunal administratif de Paris d'enjoindre à la société Rose et Lys, au motif qu'elle les occupe illégalement, de libérer les emplacements M 82 C et M 83 R appartenant au domaine public ferroviaire de la gare Saint-Lazare, sous astreinte de 2 500 euros par jour de retard et de l'autoriser à procéder d'office à son explusion au besoin avec le concours de la force publique. La société d'aménagement et de valorisation de la gare Saint-Lazare se pourvoit en cassation contre les ordonnances du 5 février 2018 et du 23 février 2018 par lesquelles le juge des référés du tribunal administratif de Paris a rejeté ces demandes.<br/>
<br/>
              3. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ".<br/>
<br/>
              4. Si le juge judiciaire est compétent pour connaître des litiges nés d'un contrat de droit privé passé entre une personne privée occupante du domaine public, qui n'est pas délégataire de service public ou qui n'agit pas pour le compte d'une personne publique, et une autre personne privée, même si ce contrat comporte occupation du domaine public, il n'appartient qu'au juge administratif de se prononcer sur les demandes par lesquelles le propriétaire ou le gestionnaire du domaine public lui demande l'expulsion de l'occupant irrégulier de ce domaine, quelle que soit la nature du titre dont cet occupant était, le cas échéant, titulaire et qui, antérieurement à son extinction, en permettait l'occupation régulière.<br/>
<br/>
              5. Pour rejeter la demande de la société d'aménagement et de valorisation de la gare Saint-Lazare comme portée devant une juridiction incompétente pour en connaître, le juge des référés du tribunal administratif de Paris a relevé, dans chacune des ordonnances attaquées, que le contrat conclu entre cette société et la société Rose et Lys était de droit privé et que la société d'aménagement et de valorisation de la gare Saint-Lazare ne produisait pas les pièces, notamment la convention conclue le 8 juillet 2008 avec la SNCF, permettant d'établir qu'elle aurait la qualité de gestionnaire des espaces commerciaux de la gare Saint-Lazare. En statuant ainsi, alors que les emplacements occupés par la société Rose et Lys, compris dans l'enceinte de la gare Saint-Lazare, appartiennent au domaine public ferroviaire, et que la société d'aménagement et de valorisation de la gare Saint-Lazare sollicitait l'expulsion de cette société en soutenant qu'elle occupait irrégulièrement le domaine public, le juge des référés du tribunal administratif de Paris, qui était saisi d'une demande n'échappant pas manifestement à la compétence du juge administratif, a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que la société d'aménagement et de valorisation de la gare Saint-Lazare est fondée, sans qu'il soit besoin d'examiner les autres moyens de ses pourvois, à demander l'annulation des ordonnances qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre des procédures de référé engagées par la société d'aménagement et de valorisation de la gare Saint-Lazare en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Ainsi qu'il a été dit aux points 4 et 5, il appartient au juge administratif de se prononcer sur les demandes par lesquelles le propriétaire ou le gestionnaire des emplacements M 82 C et M 83 R, appartenant au domaine public ferroviaire, lui demande, au motif de leur occupation irrégulière, de prononcer l'expulsion de la société Rose et Lys.<br/>
<br/>
              9. Il résulte de l'instruction, notamment de l'avenant n° 2 à la convention conclue le 8 juillet 2008 entre la SNCF et la société d'aménagement et de valorisation de la gare Saint-Lazare, intitulée " autorisation d'occupation du domaine public ferroviaire constitutive et non constitutive de droits réels ", qui a pour objet de confier à la société d'aménagement et de valorisation de la gare Saint-Lazare la commercialisation et la gestion des surfaces commerciales existantes ou construites dans la gare Saint-Lazare, que l'article 10.1 de cette convention stipule que la société d'aménagement et de valorisation de la gare Saint-Lazare a pour mission la commercialisation des commerces, la signature des contrats de sous-occupation, la gestion des commerces comprenant la facturation et le recouvrement des créances, le suivi administratif de l'exploitation commerciale, la prise en charge de toutes les obligations et de tous frais relatifs à cette exploitation et à cette gestion, et le contrôle de l'application et du respect des obligations contractuelles par les exploitants. Si cet avenant n° 2 indique seulement qu'il a été signé en 2013, sans précision sur sa date exacte, la société d'aménagement et de valorisation de la gare Saint-Lazare, fait valoir, sans que cela soit utilement contesté, en produisant une attestation de SNCF Mobilités à l'appui de la seconde demande qu'elle a présentée devant le juge des référés du tribunal administratif, qu'il a été conclu le 15 novembre 2013. Il résulte des stipulations de la convention du 8 juillet 2008 modifiée, alors même qu'elle prévoit également que la SNCF a le pouvoir de délivrer un agrément aux exploitants choisis, que la société d'aménagement et de valorisation de la gare Saint-Lazare a pour mission d'assurer la gestion des surfaces commerciales de la gare Saint-Lazare, comprenant les emplacements M 82 C et M 83 R, et qu'elle dispose, à cette fin, du pouvoir d'accorder sur ce domaine des autorisations d'occupation. La société d'aménagement et de valorisation de la gare Saint-Lazare justifie ainsi de sa qualité de gestionnaire du domaine public. Elle est par suite recevable à demander l'expulsion de la société Rose et Lys.<br/>
<br/>
              10. Il résulte de l'instruction que la convention conclue le 26 décembre 2011 entre la société d'aménagement et de valorisation de la gare Saint-Lazare et la société Elirose, devenue la société Rose et Lys, avait pris effet, en vertu de son article 2.B, le 5 janvier 2012, date à laquelle les emplacements en litige ont été mis à la disposition de la société Elirose. La circonstance que la société Elirose n'a ouvert son commerce au public qu'ultérieurement est sans incidence sur la date d'effet de la convention, qui n'a pas été modifiée sur ce point par les avenants conclus les 19 septembre et 26 novembre 2012. Cette convention a, en l'absence de reconduction, expiré au terme des six ans prévus par son article 2.L, soit le 5 janvier 2018. La société Rose et Lys occupe irrégulièrement, depuis cette date, les emplacements M 82 C et M 83 R.<br/>
<br/>
              11. Il résulte de l'instruction que le maintien dans les lieux de la société Rose et Lys, empêchant l'installation dans les emplacements M 82 C et M 83 R de la société Lush France, qui a signé un contrat de sous-occupation avec la société d'aménagement et de valorisation de la gare Saint-Lazare, fait obstacle à l'utilisation normale du domaine public par ce nouvel occupant. La société d'aménagement et de valorisation de la gare Saint-Lazare justifie ainsi de l'urgence à obtenir l'expulsion de la société Rose et Lys, ainsi que de l'utilité de cette mesure.<br/>
<br/>
              12. Si la société Rose et Lys fait valoir qu'elle n'a pu, en raison de la durée des travaux, ouvrir son commerce que le 11 mai 2012 et si, selon elle, le devoir de loyauté s'imposant à la société d'aménagement et de valorisation de la gare Saint-Lazare obligeait cette dernière à négocier de bonne foi un renouvellement ou une prorogation du contrat, ces moyens ne sauraient permettre de regarder la demande d'expulsion comme se heurtant à une contestation sérieuse. <br/>
<br/>
              13. Il y a dès lors lieu d'enjoindre à la société Rose et Lys de libérer les emplacements M 82 C et M 83 R situés dans la gare Saint-Lazare, qu'elle occupe irrégulièrement, dans un délai de quinze jours à compter de la notification de la présente décision et sous astreinte de 300 euros par jour de retard passé ce délai. Dès lors, en revanche, qu'il n'entre pas dans l'office du juge administratif d'autoriser la société d'aménagement et de valorisation de la gare Saint-Lazare à demander à l'Etat, sur le fondement du code des procédures civiles d'exécution, le concours de la force publique pour l'exécution de la présente décision, les conclusions sur ce point de la société d'aménagement et de valorisation de la gare Saint-Lazare ne peuvent qu'être rejetées.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Rose et Lys la somme de 2 000 euros à verser à la société d'aménagement et de valorisation de la gare Saint-Lazare au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société d'aménagement et de valorisation de la gare Saint-Lazare qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                         D E C I D E :<br/>
                                       --------------<br/>
<br/>
Article 1er : Les ordonnances n° 1800773 du 5 février 2018 et n° 1802864 du 23 février 2018 du juge des référés du tribunal administratif de Paris sont annulées.<br/>
<br/>
Article 2 : Il est enjoint à la société Rose et Lys de libérer les emplacements M 82 C et M 83 R situés dans la gare Saint-Lazare, sous astreinte de 300 euros par jour de retard à compter de l'expiration d'un délai de quinze jours à compter de la notification de la présente décision.<br/>
<br/>
Article 3 : La société Rose et Lys versera à la société d'aménagement et de valorisation de la gare Saint-Lazare la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de la société d'aménagement et de valorisation de la gare Saint-Lazare est rejeté.<br/>
<br/>
Article 5 : Les conclusions de la société Rose et Lys présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la société d'aménagement et de valorisation de la gare Saint-Lazare et à la société Rose et Lys.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
