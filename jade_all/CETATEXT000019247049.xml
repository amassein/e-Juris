<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019247049</ID>
<ANCIEN_ID>JG_L_2008_07_000000316867</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/24/70/CETATEXT000019247049.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 21/07/2008, 316867, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316867</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Dandelot</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Marc  Dandelot</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 6 juin 2008 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Henry A, demeurant ... ; M. A demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre la décision du 14 février 2008 du consul général de France au Nigéria refusant de lui délivrer un visa en qualité de conjoint de ressortissante française ;<br/>
<br/>
              2°) d'enjoindre, à titre principal, au consul général de France au Nigéria de délivrer le visa sollicité ;<br/>
<br/>
              3°) d'enjoindre, à titre subsidiaire, au consul général de France au Nigéria de procéder au réexamen de la demande de visa dans un délai de quinze jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que la condition d'urgence est remplie dès lors que la séparation imposée aux époux porte une atteinte grave et immédiate à leur situation ; qu'il existe un doute sérieux quant à la légalité de la décision contestée ; qu'en effet, l'administration a commis une erreur de droit et une erreur de fait en fondant sa décision sur l'absence d'intention matrimoniale de l'union alors même que le mariage a été préalablement transcrit sur les actes d'état civil des époux et que les époux ont intention de vivre ensemble et de fonder une famille ; que leur mariage a été célébré après plus d'une année de relation amoureuse ; qu'elle méconnaît l'article 8 de la convention européenne de sauvegarde des libertés fondamentales et porte une atteinte disproportionnée au respect de leur vie privée et familiale ;<br/>
<br/>
<br/>
              Vu la requête à fin d'annulation de la même décision et le recours présenté le 3 avril 2008 à la commission de recours contre les décisions de refus de visa d'entrée en France ;<br/>
<br/>
              Vu, enregistré le 15 juillet 2008, le mémoire en défense présenté par le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire, qui conclut au rejet de la requête ; que les conclusions dirigées contre la décision des autorités consulaires françaises à Lagos sont irrecevables dans la mesure où la décision implicite de refus de la commission de recours contre les refus de visas d'entrée en France en date du 4 juin 2008 l'a substituée ; que les conclusions à fin d'injonction sont irrecevables ; qu'il n'est pas satisfait à la condition d'urgence, l'union n'ayant été contractée qu'afin de permettre à l'intéressé de s'établir en France ; qu'elle n'est pas entachée d'une erreur manifeste d'appréciation, dès lors qu'un faisceau d'indices précis et concordants établissent que le mariage a été contracté dans le but exclusif de permettre au requérant de s'établir en France ; que la décision n'a pas porté d'atteinte excessive au respect de la vie privée et familiale de la requérante dans la mesure où son époux peut la rejoindre dans son pays ;<br/>
<br/>
              Vu, enregistré le 17 juillet 2008, le mémoire en réplique présenté pour M. A qui tend aux mêmes fins que la requête par les mêmes moyens ; il soutient en outre que M. Benedict A n'est plus hébergé par Mme C, ainsi qu'en attestent les pièces produites ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part M. Henry A et d'autre part, le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 17 juillet 2008 à 12h30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat du requérant ;<br/>
              - la représentante du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : « Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision » ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que Mme C, de nationalité française, s'est rendue à Lagos (Nigéria) où elle a épousé M. A le 14 septembre 2006, après l'avoir rencontré pour la première fois trois jours plus tôt ; que si le requérant soutient qu'il a entretenu pendant les dix-huit mois précédant la rencontre des relations par internet et par voie épistolaire avec Mme C, il n'a fourni au dossier copie d'aucun message électronique ni d'aucun courrier ; que postérieurement au mariage, il fait état de conversations téléphoniques dont l'existence n'est pas établie, sans même alléguer la poursuite de relations par courrier ou par message électronique, dont, au demeurant, le dossier ne porte pas trace ; qu'ainsi, dans les circonstances de l'espèce, les moyen tirés de ce que l'administration aurait commis des erreurs de fait et d'appréciation, en estimant que le mariage avait été conclu aux fins exclusives de favoriser l'obtention d'un visa, et de ce que le refus de visa aurait ainsi méconnu le droit à la vie familiale du requérant, ne paraissent pas, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de la décision attaquée ; qu'en l'état de l'instruction, la demande de suspension ne peut donc qu'être rejetée ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. Henry A est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Henry A.<br/>
Copie en sera adressée au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
