<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041785969</ID>
<ANCIEN_ID>JG_L_2020_03_000000436557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/59/CETATEXT000041785969.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 27/03/2020, 436557</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436557.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Par une requête enregistrée le 2 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la présidente de la province Sud de Nouvelle-Calédonie demande au Conseil d'Etat, en application du 6e alinéa de l'article 197 de la loi n° 99-209 organique du 19 mars 1999 relative à la Nouvelle-Calédonie, d'apprécier la compatibilité des fonctions de membre du conseil d'administration de la société Eramet de MM. D... A... et B... C... avec leur mandat de membre de l'assemblée de province.<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
- la Constitution, notamment son article 77 ;<br/>
- la loi n° 99-209 organique du 19 mars 1999 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
     Après avoir entendu en séance publique :<br/>
<br/>
     - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,<br/>
<br/>
     - les conclusions de M. Alexandre Lallet, maître des requêtes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article 197 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie dispose que : " Le membre d'une assemblée de province ou du congrès qui, lors de son élection, se trouve dans l'un des cas d'incompatibilité prévus au présent titre doit, dans les trente jours qui suivent son entrée en fonction ou, en cas de contestation de l'élection, la décision du Conseil d'Etat, démissionner de son mandat ou mettre fin à la situation incompatible avec l'exercice de celui-ci. Si la cause d'incompatibilité survient postérieurement à l'élection, le droit d'option est ouvert dans les mêmes conditions./ Par dérogation au premier alinéa, au plus tard trois mois après son entrée en fonction ou, en cas de contestation de l'élection, la date de la décision du Conseil d'Etat, le membre d'une assemblée de province ou du congrès qui se trouve dans un cas d'incompatibilité mentionné au V bis de l'article 196 met fin à cette situation soit en cédant tout ou partie de la participation, soit en prenant les dispositions nécessaires pour que tout ou partie de celle-ci soit gérée, pendant la durée de son mandat, dans des conditions excluant tout droit de regard de sa part./ A l'expiration des délais prévus aux premier et deuxième alinéas le membre d'une assemblée de province ou du congrès qui se trouve dans un des cas d'incompatibilité prévus au présent titre est déclaré démissionnaire d'office par le Conseil d'Etat, statuant au contentieux, à la requête du haut-commissaire de la République ou de tout membre du congrès ou de l'assemblée de province intéressée./ Toutefois, à l'expiration du délai de trente jours, les deuxième et troisième alinéas de l'article LO 151 du code électoral sont applicables au membre d'une assemblée de province ou du congrès de la Nouvelle-Calédonie qui se trouve dans un des cas d'incompatibilité visés au II de l'article 196 de la présente loi organique./Dans le délai prévu au premier alinéa, tout membre d'une assemblée de province ou du congrès est tenu d'adresser au haut-commissaire de la République une déclaration certifiée sur l'honneur exacte et sincère comportant la liste des activités professionnelles ou d'intérêt général, même non rémunérées, qu'il envisage de conserver ou attestant qu'il n'en exerce aucune. Cette déclaration énumère également les participations directes ou indirectes qui confèrent le contrôle d'une société, d'une entreprise ou d'un organisme dont l'activité consiste principalement dans la fourniture de prestations de conseil. En cours de mandat, il doit déclarer dans les mêmes formes tout élément de nature à modifier sa déclaration initiale. Ces déclarations sont publiées au Journal officiel de la Nouvelle-Calédonie. / Le haut-commissaire de la République examine si les activités ainsi déclarées sont compatibles avec le mandat de membre de l'assemblée de province ou du congrès. S'il y a doute sur la compatibilité des fonctions et activités professionnelles ou d'intérêt général exercées ou en cas de contestation à ce sujet, le haut-commissaire, l'auteur de la déclaration ou tout autre membre du congrès ou de l'assemblée de province concernée saisit le Conseil d'Etat, statuant au contentieux, qui apprécie si le membre du congrès ou de l'assemblée de province intéressé se trouve dans un cas d'incompatibilité./ Par dérogation aux dispositions qui précèdent, le membre d'une assemblée de province ou du congrès qui a méconnu l'une des interdictions édictées au dernier alinéa de l'article 101 et aux IV à IX de l'article 196 est déclaré démissionnaire d'office, sans délai, par le Conseil d'Etat, à la requête du haut-commissaire de la République ou de tout membre du congrès ou de l'assemblée de province intéressée. La démission d'office n'entraîne pas d'inéligibilité ".<br/>
<br/>
              2. La question posée au Conseil d'Etat par la présidente de l'assemblée de la province Sud de Nouvelle-Calédonie est celle de savoir si, en raison de leur fonction de membre du conseil d'administration de la société Eramet, MM. D... A... et B... C..., membres de l'assemblée de la province Sud de Nouvelle-Calédonie, se trouvent dans un des cas d'incompatibilité prévus par le code électoral.<br/>
<br/>
              3. Il résulte des dispositions citées au point 1 que, pour l'appréciation de la compatibilité des fonctions ou activités d'un membre d'une assemblée de province de Nouvelle-Calédonie avec l'exercice de son mandat, il appartient d'abord au haut-commissaire de la République de procéder à l'examen de la question. Le Conseil d'Etat ne peut se prononcer que si le haut-commissaire de la République, ayant procédé à cet examen et ayant un doute, le saisit ou si le membre de l'assemblée de province concerné ou tout autre membre de cette assemblée entend contester devant lui la position prise par le haut-commissaire.<br/>
<br/>
              4. Il ne résulte pas de l'instruction que le haut-commissaire de la République ait pris position sur la compatibilité de la fonction de membre du conseil d'administration de la société Eramet de MM. D... A... et B... C... avec leur mandat de membre de l'assemblée de la province Sud de Nouvelle-Calédonie à la date de la présente décision. Il s'ensuit que la présidente de cette assemblée n'est pas recevable à saisir le Conseil d'Etat, statuant au contentieux, sur le fondement de l'article 197 de la loi organique du 19 mars 1999 afin qu'il apprécie cette compatibilité. Il résulte de ce qui précède que la requête de la présidente de l'assemblée de la province Sud de Nouvelle-Calédonie doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la présidente de l'assemblée de la province Sud de Nouvelle-Calédonie est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la présidente de l'assemblée de la province Sud de Nouvelle-Calédonie, à M. D... A..., à M. B... C..., au haut-commissaire de la République en Nouvelle-Calédonie et au ministre des Outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - RÉGIME DES INCOMPATIBILITÉS APPLICABLES AUX MEMBRES DES ASSEMBLÉES DE PROVINCE DE NOUVELLE-CALÉDONIE (ART. 197 DE LA LOI DU 19 MARS 1999) - 1) EXAMEN PRÉALABLE PAR LE HAUT-COMMISSAIRE DE LA RÉPUBLIQUE - 2) SAISINE DU CONSEIL D'ETAT - RECEVABILITÉ - HYPOTHÈSES - A) SAISINE PAR LE HAUT-COMMISSAIRE EN CAS DE DOUTE - B) SAISINE PAR LE MEMBRE CONCERNÉ OU UN AUTRE MEMBRE DE L'ASSEMBLÉE CONTESTANT LA POSITION DU HAUT-COMMISSAIRE [RJ1].
</SCT>
<ANA ID="9A"> 46-01-02-01 1) Il résulte de l'article 197 de la loi organique n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie que, pour l'appréciation de la compatibilité des fonctions ou activités d'un membre d'une assemblée de province de Nouvelle-Calédonie avec l'exercice de son mandat, il appartient d'abord au haut-commissaire de la République de procéder à l'examen de la question....  ,,2) Le Conseil d'Etat ne peut se prononcer que a) si le haut-commissaire de la République, ayant procédé à cet examen et ayant un doute, le saisit ou b) si le membre de l'assemblée de province concerné ou tout autre membre de cette assemblée entend contester devant lui la position prise par le haut-commissaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du contrôle des incompatibilités parlementaires, Cons. const., 13 octobre 2015, n° 2015-31 I ; Cons. const., 26 septembre 2018, n° 2018-41 I.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
