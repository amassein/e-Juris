<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030681336</ID>
<ANCIEN_ID>JG_L_2015_06_000000389610</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/68/13/CETATEXT000030681336.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 05/06/2015, 389610, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389610</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389610.20150605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler la décision en date du 7 septembre 2011 par laquelle le conseil médical de l'aéronautique civile l'a déclaré inapte classe 1 et classe 2 ainsi que la décision en date du 8 février 2012 par laquelle cette même autorité l'a, de nouveau, déclaré inapte classe 1 et classe 2. Par un jugement n° 1120245-1206526 du 23 avril 2013, le tribunal administratif de Paris a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 13PA02442 en date du 16 février 2015, la cour administrative d'appel de Paris a, d'une part, rejeté l'appel formé par la ministre de l'écologie, du développement durable et de l'énergie contre ce jugement et, d'autre part, lui a enjoint de procéder à un nouvel examen de la demande de dérogation présentée par M. A...dans un délai de deux mois à compter de la notification de l'arrêt.<br/>
<br/>
              Par un recours, enregistré le 20 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat :<br/>
<br/>
              1°) de prononcer le sursis à exécution de cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A...la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'aviation civile ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du ministre de l'écologie, du développement durable de l'énergie et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de A...;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., commandant de bord de la compagnie Air France, a été déclaré inapte classe 1 (pilote professionnel) et classe 2 (pilote non professionnel) par le médecin du centre d'expertise médicale aéronautique de Paris respectivement les 21 mai et 25 juin 2010 ; qu'il a saisi le conseil médical de l'aéronautique civile d'une demande de dérogation aux normes d'aptitude médicale ; que ce conseil, a, par une décision du 6 avril 2011, rejeté sa demande en l'invitant à représenter son dossier après une expertise complète de classe 1 par le centre principal d'expertise médicale du personnel naviguant de l'hôpital d'instruction des armées Percy ; qu'au vu de ladite expertise, le conseil médical de l'aéronautique civile l'a, par une décision en date du 7 septembre 2011, déclaré inapte classe 1 et classe 2 ; que M. A...a, le 17 janvier 2012, formé devant ce conseil un recours à l'encontre des décisions des 21 mai et 25 juin 2010 ; que, par une décision en date du 8 février 2012, le conseil médical de l'aéronautique civile, rejetant ce recours, l'a déclaré inapte classe 1 et classe 2 ; que l'arrêt de la cour administrative d'appel de Paris en date du 16 février 2015, a, d'une part, confirmé le jugement du 23 avril 2013 par lequel le tribunal administratif de Paris a prononcé l'annulation des décisions des 7 septembre 2011 et 8 février 2012 et, d'autre part, enjoint à la ministre de l'écologie, du développement durable et de l'énergie de procéder à un nouvel examen de la demande de dérogation de M. A...;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 821-5 du code de justice administrative, la formation de jugement peut, à la demande de l'auteur d'un pourvoi en cassation, " ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond (...) ";<br/>
<br/>
              3. Considérant, d'une part, que, dans les circonstances de l'espèce, l'exécution de l'arrêt attaqué serait susceptible d'entraîner des conséquences difficilement réparables, notamment au regard des risques pour la sécurité aérienne et la sécurité des personnes ;<br/>
<br/>
              4. Considérant, d'autre part, que le moyen tiré de ce que la cour aurait commis une erreur de droit, une inexacte qualification juridique des faits et une dénaturation des pièces du dossier paraît, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de cet arrêt, l'infirmation de la solution retenue par les juges du fond ;<br/>
<br/>
              5. Considérant que, dans ces conditions, il y a lieu d'ordonner le sursis à exécution de l'arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la ministre de l'écologie, du développement durable et de l'énergie au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi de la ministre de l'écologie, du développement durable et de l'énergie contre l'arrêt de la cour administrative d'appel de Paris en date du 16 février 2015, il sera sursis à l'exécution de cet arrêt.<br/>
Article 2 : Les conclusions de la ministre de l'écologie, du développement durable et de l'énergie et par M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
