<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036898109</ID>
<ANCIEN_ID>JG_L_2018_05_000000407824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/89/81/CETATEXT000036898109.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 09/05/2018, 407824, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407824.20180509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler la décision du 1er septembre 2014 par laquelle le maire de la commune de Saint-André-de-Sangonis (Hérault) lui a attribué le bénéfice de l'allocation d'aide au retour à l'emploi, en tant que le montant journalier de cette allocation a été fixé à 23,77 euros au lieu de 43,09 euros, que son point de départ a été fixé au 7 août 2014 au lieu du 10 juillet 2014 et que la durée de l'indemnisation a été fixée à 673 jours au lieu de 730, et, d'autre part, d'enjoindre à la commune de lui verser une somme correspondant à la différence entre l'allocation versée et celle à laquelle elle estime avoir droit. Par un jugement n° 1405695 du 3 juin 2016, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA03965 du 7 février 2017, enregistré le 10 février suivant au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi et le mémoire complémentaire, enregistrés les 24 et 31 octobre 2016 au greffe de cette cour, présentés par MmeA.... Par ce pourvoi, ce mémoire complémentaire et un mémoire en réplique, enregistré au secrétariat du contentieux du Conseil d'Etat le 16 février 2018, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier du 3 juin 2016 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande, en enjoignant à la commune de Saint-André-de-Sangonis de lui verser la somme de 15 615 euros dans le délai d'un mois à compter de la décision à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-André-de-Sangonis la somme de 2 000 euros à verser à son avocat, la SCP de Chaisemartin, Courjon, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de Mme A...et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Saint -André-de-Sangonis.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeA..., qui occupait les fonctions de responsable du service jeunesse de la commune de Saint-André-de-Sangonis, a présenté sa démission, laquelle a été acceptée à compter du 1er avril 2013, puis, après avoir travaillé pour deux autres employeurs, s'est retrouvée sans emploi à compter du 1er juillet 2014 et a demandé à la commune de lui verser l'allocation d'aide au retour à l'emploi. Elle a demandé au tribunal administratif de Montpellier d'annuler la décision du 1er septembre 2014 par laquelle le maire de Saint-André-de-Sangonis lui a attribué le bénéfice de l'allocation d'aide au retour à l'emploi, en tant que le montant journalier de cette allocation a été fixé à 23,77 euros et non à 43,09 euros, que son point de départ a été fixé au 7 août 2014 et non au 10 juillet 2014 et que la durée de l'indemnisation a été fixée à 673 jours et non à 730. Elle se pourvoit contre le jugement du 3 juin 2016 par lequel le tribunal a rejeté sa demande.<br/>
<br/>
              2. Il résulte des dispositions des articles L. 5424-1, L. 5422-2 et L. 5422-3 et L. 5422-20 du code du travail que les agents des collectivités territoriales involontairement privés d'emploi ont droit à une allocation d'assurance dans les conditions définies par l'accord prévu par l'article L. 5422-20, dès lors qu'un tel accord est intervenu et a été agréé et qu'il n'est pas incompatible avec les règles qui gouvernent les agents publics. <br/>
<br/>
              3. Il ressort des prétentions précises formulées par Mme A...dans ses écritures devant le tribunal administratif de Montpellier qu'en se prévalant, pour contester le point de départ du versement de l'allocation d'aide au retour à l'emploi, sa durée et son montant, des termes de la circulaire interministérielle du 3 janvier 2012 relative à l'indemnisation du chômage des agents du secteur public, dont elle relevait qu'elle informait les employeurs publics des nouvelles règles définies par la convention du 6 mai 2011 relative à l'indemnisation du chômage agréée par arrêté du 15 juin 2011, l'intéressée avait en réalité entendu invoquer les stipulations de la convention prévue à l'article L. 5422-20 du code du travail et de son règlement général annexé. Dans ces conditions, Mme A...est fondée à soutenir que le tribunal, qui a écarté ces prétentions au seul motif qu'elle ne pouvait utilement se prévaloir des termes de la circulaire du 3 janvier 2012, a insuffisamment motivé son jugement.<br/>
<br/>
              4. Il résulte de ce qui précède que Mme A...est fondée à demander l'annulation du jugement attaqué. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens du pourvoi. <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme A...au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Les dispositions de l'article L. 761-1 du code de justice administrative font, en outre, obstacle à ce que soit mise à la charge de l'intéressée, qui n'est pas la partie perdante dans la présente instance, la somme demandée par la commune de Saint-André-de-Sangonis sur ce fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 3 juin 2016 du tribunal administratif de Montpellier est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
Article 3 : Le surplus des conclusions du pourvoi, ainsi que les conclusions de la commune de Saint-André-de-Sangonis présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la commune de Saint-André-de-Sangonis. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
