<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032047983</ID>
<ANCIEN_ID>JG_L_2016_02_000000396164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/04/79/CETATEXT000032047983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 12/02/2016, 396164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:396164.20160212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance nos 1521307, 1521342 du 31 décembre 2015, le juge des référés du tribunal administratif de Paris a suspendu, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 18 décembre 2015 par lequel le préfet de la région d'Ile-de-France a fixé le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Val d'Yerres Val de Seine ".<br/>
<br/>
              Par un pourvoi, enregistré le 15 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er, 3 et 4 de cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de suspension des communes de Montgeron et de Draveil.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2014-58 du 27 janvier 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le VI de l'article 11 de la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles dispose : " Si, avant la publication de l'arrêté portant création, extension ou fusion d'un établissement public de coopération intercommunale à fiscalité propre en application des III à V du présent article, le nombre et la répartition des sièges au sein de l'organe délibérant de l'établissement public n'ont pas été fixés, les conseils municipaux des communes intéressées disposent, à compter de la date de publication de l'arrêté, d'un délai de trois mois pour délibérer sur la composition de l'organe délibérant, sans que cette délibération puisse être prise après le 15 décembre 2015. / Le représentant de l'Etat dans la région constate la composition de l'organe délibérant de l'établissement public de coopération intercommunale à fiscalité propre fixée selon les modalités prévues au premier alinéa du présent VI (...)". Aux termes du 2° du I de l'article L. 5211-6-1 du code général des collectivités territoriales : " I.-Le nombre et la répartition des sièges de conseiller communautaire sont établis : / 2° Soit, (...) dans les communautés d'agglomération, par accord des deux tiers au moins des conseils municipaux des communes membres représentant plus de la moitié de la population de celles-ci ou de la moitié au moins des conseils municipaux des communes membres représentant plus des deux tiers de la population de celles-ci. Cette majorité doit comprendre le conseil municipal de la commune dont la population est la plus nombreuse, lorsque celle-ci est supérieure au quart de la population des communes membres ".<br/>
<br/>
              2. En application de ces dispositions, les communes rejoignant, à compter du 1er janvier 2016, la nouvelle communauté d'agglomération " Val d'Yerres Val de Seine ", ont, par un accord, fixé le nombre et la répartition des sièges au sein du conseil communautaire de cette communauté d'agglomération. Par un arrêté du 18 décembre 2015, le préfet de la région d'Ile-de-France a, en application des dispositions précitées de la loi du 27 janvier 2014, établi le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Val d'Yerres Val de Seine " conformément à l'accord des communes intéressées. <br/>
<br/>
              3. Les communes de Draveil et de Montgeron ont demandé au juge des référés du tribunal administratif de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de cet arrêté. Elles ont, à l'appui de leur demande de suspension, soulevé une question prioritaire de constitutionnalité relative aux dispositions du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales. Par une ordonnance nos 1521307,  1521342 du 31 décembre 2015, le juge des référés du tribunal administratif de Paris a, d'une part, suspendu l'exécution de l'arrêté litigieux au seul motif que le moyen tiré de l'inconstitutionnalité des dispositions du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales devait être regardé, en l'état de l'instruction, comme propre à créer un doute sérieux quant à la légalité de l'arrêté attaqué et, d'autre part, décidé de ne pas transmettre la question prioritaire de constitutionnalité soulevée devant lui par ces communes au Conseil d'Etat au motif que le Conseil d'Etat était déjà saisi d'une question mettant en cause, par les mêmes motifs, ces dispositions.<br/>
<br/>
              4. Ainsi qu'il a été dit ci-dessus, l'arrêté du préfet de la région d'Ile-de-France du 18 décembre 2015 a été pris en application des dispositions du VI de l'article 11 de la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles. Cet acte se borne à constater, conformément à l'accord des communes intéressées obtenu selon les règles de majorité fixées à l'article L. 5211-6-1 du code général des collectivités territoriales, le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération "Val d'Yerres Val de Seine ". Les dispositions du c) du 1° de l'article L. 5211-6-2 du code général de collectivités territoriales, qui ont pour objet de fixer les modalités par lesquelles les communes désignent leurs conseillers communautaires au sein de l'organe délibérant de l'établissement public de coopération intercommunale qu'elles rejoignent, lorsque le nombre de sièges qui leur est attribué est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général des conseils municipaux, ne sont par conséquent pas applicables aux litiges soulevés par les communes de Draveil et de Montgeron. Il suit de là qu'en jugeant qu'était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la l'arrêté attaqué, le moyen tiré de l'absence de conformité à la Constitution des dispositions du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales, le juge des référés du tribunal administratif de Paris a commis une erreur de droit. Par suite, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de statuer sur les demandes de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Les demandes de suspension présentées par les communes de Montgeron et de Draveil portent sur le même arrêté et présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule ordonnance.<br/>
<br/>
              7. En premier lieu, il ressort des pièces du dossier que, pour prendre l'arrêté du 18 décembre 2015 fixant le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération " Val d'Yerres Val de Seine ", le préfet de la région d'Ile-de-France a tout d'abord rappelé que, conformément aux dispositions du VI de l'article 11 de la loi du 27 janvier 2014 et du 2° du I de l'article L. 5211-6-1 du code général des collectivités territoriales, " le nombre et la répartition des sièges de conseillers communautaires des communes au sein de l'organe délibérant des communautés (...) d'agglomération peuvent être établis par un accord des deux tiers au moins des conseils municipaux des communes intéressées représentant plus de la moitié de la population totale de celles-ci ou de la moitié au moins des conseils municipaux des communes intéressées représentant plus des deux tiers de la population totale de celles-ci ". Il a ensuite ajouté que " cette majorité doit comprendre le conseil municipal de la commune dont la population est la plus importante, lorsque celle-ci est supérieure au quart de la population des communes membres " avant, toutefois, de constater " qu'aucune commune ne représente plus du quart de la population totale ". Il a enfin constaté que " les conseils municipaux des communes membres ont, par accord, établi le nombre total et la répartition des sièges du conseil communautaire et de la communauté d'agglomération "Val d'Yerres Val de Seine ". Il suit de là que les moyens tirés de ce que l'arrêté litigieux serait insuffisamment motivé et méconnaîtrait les dispositions du VI de l'article 11 de la loi du 27 janvier 2014 et du 2° du I de l'article L. 5211-6-1 du code général des collectivités territoriales ne sont pas, en l'état de l'instruction, propres à créer un doute sérieux quant à la légalité de l'arrêté attaqué.<br/>
<br/>
              8. En deuxième lieu, ainsi qu'il a été dit au point 3, les dispositions de l'article L. 5211-6-2 du code général des collectivités territoriales ne sont pas applicables aux litiges soulevés par les communes de Montgeron et de Draveil. Par suite, le moyen tiré de l'absence de conformité à la Constitution des dispositions du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales n'est pas, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de l'arrêté attaqué.<br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que les requêtes des communes de Draveil et Montgeron doivent être rejetées. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 1er, 3 et 4 de l'ordonnance du 31 décembre 2015 du juge des référés du tribunal administratif de Paris sont annulés.<br/>
<br/>
Article 2 : Les requêtes des communes de Draveil et de Montgeron sont rejetées.<br/>
Article 3 : La présente décision sera notifiée aux communes de Draveil et de Montgeron et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
