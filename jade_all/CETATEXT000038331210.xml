<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038331210</ID>
<ANCIEN_ID>JG_L_2019_03_000000428483</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/33/12/CETATEXT000038331210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/03/2019, 428483, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428483</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:428483.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...B..., représenté par MeC..., a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de l'admettre au bénéfice de l'aide juridictionnelle provisoire, et d'enjoindre au conseil départemental des Bouches-du-Rhône, d'une part, d'assurer son hébergement dans un délai de 24 heures à compter de la notification de la décision à intervenir, sous astreinte de 250 euros par jour de retard et, d'autre part, de mettre en oeuvre la prise en charge ordonnée par le juge judiciaire sous les mêmes conditions de délai et d'astreinte. Par une ordonnance n° 1901324 du 20 février 2019, le juge des référés du tribunal administratif de Marseille, en premier lieu, a admis M. B...à l'aide juridictionnelle provisoire, en deuxième lieu, a enjoint au conseil départemental des Bouches-du-Rhône d'assurer l'hébergement et la prise en charge de M. B...dans un délai de 24 heures à compter de la notification de son ordonnance, sous astreinte de 250 euros par jour de retard et, en troisième lieu, a mis à la charge de l'Etat la somme de 300 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 à verser à l'avocate de M.B..., sous réserve que celle-ci renonce à percevoir la somme correspondante à la part contributive de l'Etat. <br/>
<br/>
              Par une requête, enregistrée le 28 février 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A...C...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) à titre principal, de réformer l'ordonnance du 20 février 2019 en condamnant l'Etat à lui verser la somme de 1 000 euros sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 au titre de l'instance qui s'est déroulée devant le juge des référés du tribunal administratif ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'article 3 de l'ordonnance attaquée et de renvoyer l'affaire devant le tribunal administratif de Marseille afin qu'il soit de nouveau statué sur ce point.<br/>
<br/>
<br/>
<br/>
              Elle soutient que c'est à tort que le juge des référés du tribunal administratif de Marseille ne lui a alloué que 300 euros au titre des frais irrépétibles, dès lors qu'il ne pouvait lui allouer un montant inférieur à la part contributive de l'Etat, d'un montant de 307,20 euros. <br/>
<br/>
              Par un mémoire en défense, enregistré le 8 mars 2019, le conseil départemental des Bouches-du-Rhône conclut à ce que soit réformée l'ordonnance du 20 février 2019 en limitant à 307,20 euros la somme mise à sa charge au titre des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique. Il soutient que :<br/>
              - le conseil départemental des Bouches-du-Rhône met tout en oeuvre pour accueillir les mineurs non accompagnés, mais fait face à de réelles difficultés eu égard à la croissance importante de leur nombre depuis plusieurs années. <br/>
              - Mme C...ne saurait obtenir une somme supérieure à 307,20 euros au titre des frais irrépétibles dès lors, d'une part, que le contentieux de l'hébergement des mineurs isolés est récurent et ne donne lieu qu'à un volume limité de travail pour les avocats, comme le démontre la quasi-identité entre les mémoires déposés par Mme C...dans le cadre de sa défense de différents mineurs isolés et son absence aux audiences y afférentes et, d'autre part, que la fixation de frais trop élevés, eu égard à l'ampleur croissante du contentieux relatif aux mineurs isolés, serait de nature à amputer le budget du conseil départemental des Bouches-du-Rhône de ressources précieuses qui pourraient notamment être utilisées pour développer des solutions d'hébergement destinées à accueillir les mineurs isolés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, Mme C... et, d'autre part, le conseil départemental des Bouches-du-Rhône ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 mars 2019 à 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme C... ;<br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat du conseil départemental des Bouches-du-Rhône ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation. " Aux termes de l'article 37 de la loi du 10 juillet 1991 : " Les auxiliaires de justice rémunérés selon un tarif peuvent renoncer à percevoir la somme correspondant à la part contributive de l'Etat et poursuivre contre la partie condamnée aux dépens et non bénéficiaire de l'aide juridictionnelle le recouvrement des émoluments auxquels ils peuvent prétendre. / Dans toutes les instances, le juge condamne la partie tenue aux dépens, ou qui perd son procès, et non bénéficiaire de l'aide juridictionnelle, à payer à l'avocat du bénéficiaire de l'aide juridictionnelle, partielle ou totale, une somme qu'il détermine et qui ne saurait être inférieure à la part contributive de l'Etat, au titre des honoraires et frais non compris dans les dépens que le bénéficiaire de l'aide aurait exposés s'il n'avait pas eu cette aide. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation. Si l'avocat du bénéficiaire de l'aide recouvre cette somme, il renonce à percevoir la part contributive de l'Etat. (...) ".<br/>
<br/>
              3. Mme C...fait appel de l'ordonnance du 20 février 2019 du juge des référés du tribunal administratif de Marseille en tant que cette ordonnance, après avoir admis son client M. B...au bénéfice de l'aide juridictionnelle provisoire et fait droit aux conclusions présentées par celui-ci sur le fondement de l'article L. 521-2 du code de justice administrative, lui alloue une somme de 300 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
              4. Il résulte des termes mêmes de l'article 37 de la loi du 10 juillet 1991 cité ci-dessus que la somme que la partie perdante peut être amenée à payer sur ce fondement au conseil du bénéficiaire de l'aide juridictionnelle ne saurait être inférieure au montant de la part contributive de l'Etat. Il n'est pas contesté en défense que le montant de la part contributive de l'Etat qui devrait être allouée à Mme C...au titre de l'instance de référé-liberté dans laquelle elle représentait M. B...s'élève à 307,20 euros. Par suite, Mme C... est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés a mis à la charge de l'Etat au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 une somme de 300 euros, inférieure au montant de la part contributive de l'Etat. L'ordonnance attaquée doit donc être annulée dans la mesure où elle statue sur les frais non compris dans les dépens.<br/>
<br/>
              5. Il y a lieu pour le Conseil d'Etat de régler l'affaire, dans cette mesure. Il y a lieu, dans les circonstances de l'espèce, de mettre la somme de 1 000 euros à la charge de l'Etat, de verser à Mme C...sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que celle-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'article 3 de l'ordonnance du 20 février 2019 du juge des référés du tribunal administratif de Marseille est annulé. <br/>
Article 2 : L'Etat versera à Mme C...une somme de 1 000 euros sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du<br/>
10 juillet 1991, sous réserve que Mme C...renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 3 : La présente ordonnance sera notifiée à Mme C...et au conseil départemental des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
