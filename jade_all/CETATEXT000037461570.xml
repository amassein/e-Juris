<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461570</ID>
<ANCIEN_ID>JG_L_2018_10_000000411900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 03/10/2018, 411900</TITRE>
<DATE_DEC>2018-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411900.20181003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 juin, 25 septembre 2017 et 26 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. Yves Squercioni demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 26 avril 2017 par laquelle le premier président de la cour d'appel de Fort-de-France lui a infligé un avertissement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat  la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Par une décision du 26 avril 2017, le premier président de la cour d'appel de Fort-de-France a adressé à M. Yves Squercioni, premier vice-président du tribunal de grande instance de Fort-de-France, un avertissement. M. A...demande au Conseil d'Etat d'annuler cette décision. L'intervention au soutien de la requête présentée par le syndicat national des magistrats Force Ouvrière, qui justifie d'un intérêt suffisant à l'annulation de la de la décision attaquée, est recevable.<br/>
<br/>
              2.	Aux termes de l'article 43 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Tout manquement par un magistrat aux devoirs de son état, à l'honneur, à la délicatesse ou à la dignité, constitue une faute disciplinaire. (...) ". Aux termes de l'article 44 de la même ordonnance : " En dehors de toute action disciplinaire, l'inspecteur général, chef de l'inspection générale de la justice, les premiers présidents, les procureurs généraux et les directeurs ou chefs de service à l'administration centrale ont le pouvoir de donner un avertissement aux magistrats placés sous leur autorité. / Le magistrat à l'encontre duquel il est envisagé de délivrer un avertissement est convoqué à un entretien préalable. Dès sa convocation à cet entretien, le magistrat a droit à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure. Il est informé de son droit de se faire assister de la personne de son choix. / (...) / L'avertissement est effacé automatiquement du dossier au bout de trois ans si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenu pendant cette période. ". L'article 45 définit par ailleurs les sanctions disciplinaires applicables aux magistrats.<br/>
<br/>
              3.	S'il ne constitue pas une sanction disciplinaire au sens de l'article 45 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, l'avertissement est une mesure prise en considération de la personne, qui est mentionnée au dossier du magistrat, duquel il ne peut être effacé automatiquement que si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenu pendant les trois années suivantes. L'avertissement doit, dès lors, respecter les droits de la défense. Il résulte des dispositions citées au point précédent que le magistrat a droit, dès sa convocation à l'entretien préalable, à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure et qu'il est informé de son droit de se faire assister de la personne de son choix. Le droit à la communication du dossier comporte, pour le magistrat concerné, celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.<br/>
<br/>
              4.	Il ressort des pièces du dossier que si le dossier individuel de M. A... a été mis à sa disposition, le premier président de la cour d'appel de Fort-de-France a refusé de faire droit à sa demande réitérée, dont il n'est pas soutenu qu'elle présentait un caractère abusif, d'être autorisé à prendre copie de ses pièces. Il résulte de ce qui a été dit au point 3 que la décision attaquée a été prise au terme d'une procédure irrégulière, qui a privé M. A... d'une garantie. Par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, M. A...est fondé à demander l'annulation de la décision d'avertissement qu'il attaque. <br/>
<br/>
              5.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, le syndicat national des magistrats Force Ouvrière, n'étant pas, en sa qualité d'intervenant, partie à la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme lui soit versée par l'Etat à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention du syndicat national des magistrats Force Ouvrière est admise.<br/>
<br/>
Article 2 : La décision d'avertissement du premier président de la cour d'appel de Fort-de-France du 26 avril 2017 est annulée.<br/>
<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le syndicat national des magistrats Force ouvrière au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Yves Squercioni, à la garde des sceaux, ministre de la justice et au syndicat national des magistrats Force Ouvrière.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. RESPECT DES DROITS DE LA DÉFENSE. - AVERTISSEMENT PRONONCÉ À L'ENCONTRE D'UN MAGISTRAT (ART. 44 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) - PORTÉE DU PRINCIPE [RJ1] - DROIT, POUR LE MAGISTRAT, À LA COMMUNICATION DE SON DOSSIER, COMPORTANT CELUI D'EN PRENDRE COPIE - EXISTENCE, SAUF CARACTÈRE ABUSIF DE LA DEMANDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-04-02-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE JUDICIAIRE. DISCIPLINE. - AVERTISSEMENT (ART. 44 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) - OBLIGATION DE RESPECTER LES DROITS DE LA DÉFENSE [RJ1] - PORTÉE - DROIT, POUR LE MAGISTRAT, À LA COMMUNICATION DE SON DOSSIER, COMPORTANT CELUI D'EN PRENDRE COPIE - EXISTENCE, SAUF CARACTÈRE ABUSIF DE LA DEMANDE.
</SCT>
<ANA ID="9A"> 01-04-03-07-03 L'avertissement doit respecter les droits de la défense. Le magistrat a droit, dès sa convocation à l'entretien préalable, à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure et est informé de son droit de se faire assister de la personne de son choix. Le droit à la communication du dossier comporte, pour le magistrat concerné, celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.</ANA>
<ANA ID="9B"> 37-04-02-02 L'avertissement doit respecter les droits de la défense. Le magistrat a droit, dès sa convocation à l'entretien préalable, à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure et est informé de son droit de se faire assister de la personne de son choix. Le droit à la communication du dossier comporte, pour le magistrat concerné, celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 21 juin 2017, Mme,, n° 398830, T. pp. 447-656.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
