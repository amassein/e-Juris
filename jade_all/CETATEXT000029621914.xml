<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029621914</ID>
<ANCIEN_ID>JG_L_2014_10_000000361464</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/19/CETATEXT000029621914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 22/10/2014, 361464, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361464</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361464.20141022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1°, sous le n° 361464, la requête, enregistrée le 30 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Métropole Télévision (M6), dont le siège est 89 avenue Charles de Gaulle à Neuilly-sur-Seine (92275 cedex) ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision implicite de rejet résultant du silence gardé pendant plus de deux mois par le Conseil supérieur de l'audiovisuel (CSA), sur sa demande, présentée le 23 avril 2012, tendant à l'attribution d'une autorisation d'usage de la ressource radioélectrique pour la diffusion d'un service de télévision à vocation nationale en application de l'article 103 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 366191, la requête sommaire et le mémoire complémentaire, enregistrés les 19 février et 20 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Métropole Télévision (M6), dont le siège est 89 avenue Charles de Gaulle à Neuilly-sur-Seine (92275 cedex) ; la société Métropole Télévision demande au Conseil d'Etat :<br/>
<br/>
              1°) de condamner l'Etat à lui verser, en réparation de ses différents préjudices résultant de la décision du CSA contestée sous le n° 366191, une indemnité de 98,77 millions d'euros, augmentée des intérêts calculés au taux légal à compter du 18 octobre 2012, les intérêts dus pour une année entière étant capitalisés à cette date, puis à chaque échéance annuelle ultérieure ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 12 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le Traité sur le fonctionnement de l'Union européenne, notamment son article 258 ;<br/>
<br/>
              Vu la directive 2002/20/CE du Parlement européen et du Conseil du 7 mars 2002 relative à l'autorisation de réseaux et de services de communications électroniques ; <br/>
<br/>
              Vu la directive 2002/77/CE de la Commission du 16 septembre 2002 relative à la concurrence dans les marchés des réseaux et des services de communications électroniques ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986, notamment ses articles 99, 103 et 104 ;<br/>
<br/>
              Vu la loi n° 2007-309 du 5 mars 2007, notamment son article 6 ;<br/>
<br/>
              Vu la loi n° 2013-1028 du 15 novembre 2013, notamment son article 30 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Métropole Télévision(M6) ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article 99 de la loi du 30 septembre 1986, dans sa rédaction issue de l'article 6 de la loi du 5 mars 2007 relative à la modernisation de la diffusion audiovisuelle et à la télévision du futur, a mis fin de façon anticipée et progressive, entre le 31 mars 2008 et le 30 novembre 2011, à la diffusion des services nationaux de télévision par voie hertzienne terrestre en mode analogique ; qu'en contrepartie de la réduction, par l'effet de cette disposition, de la durée des autorisations de diffusion qui avaient été accordées aux éditeurs des services en cause, le législateur a mis en place un dispositif dit de " canal compensatoire ", prévu à l'article 103 de la loi 30 septembre 1986 issu du même article 6 de la loi du 5 mars 2007, aux termes duquel : " A l'extinction complète de la diffusion par voie hertzienne en mode analogique d'un service national de télévision préalablement autorisé sur le fondement de l'article 30, le Conseil supérieur de l'audiovisuel accorde à l'éditeur de ce service qui lui en fait la demande, sous réserve du respect des articles 1er, 3-1, 26 et 39 à 41-4, un droit d'usage de la ressource radioélectrique pour la diffusion d'un autre service de télévision à vocation nationale, à condition que ce service ne soit lancé qu'à compter du 30 novembre 2011 et qu'il remplisse les conditions et critères énoncés aux deuxième et troisième alinéas du III de l'article 30-1, souscrive à des obligations renforcées de soutien à la création en matière de diffusion et de production d'oeuvres cinématographiques et audiovisuelles européennes et d'expression originale française fixées par décret en Conseil d'Etat et soit édité par une personne morale distincte, contrôlée par cet éditeur au sens du 2° de l'article 41-3 " ; qu'un article 104, également introduit dans la loi du 30 septembre 1986 par l'article 6 de la loi du 5 mars 2007, a exclu toute autre forme de réparation pour les éditeurs concernés ; que le décret prévu à l'article 103 n'a pas été pris ; que les articles 103 et 104 de la loi du 30 septembre 1986 ont été abrogés par l'article 30 de la loi du 15 novembre 2013 ;<br/>
<br/>
              2. Considérant que, sur le fondement de l'article 103 de la loi du 30 septembre 1986, la société Métropole Télévision, éditeur du service national de télévision " M6 ", a demandé le 23 avril 2012 au Conseil supérieur de l'audiovisuel (CSA) l'attribution d'un canal compensatoire ; que le silence gardé par le CSA pendant plus de deux mois sur cette demande a fait naître une décision de rejet dont la société Métropole Télévision demande, sous le n° 361464, l'annulation pour excès de pouvoir ; que la société recherche en outre, sous le n° 366191, la responsabilité de l'Etat au titre des fautes qu'il aurait commises, d'une part, en rejetant sa demande d'attribution du canal compensatoire et, d'autre part, en s'abstenant de prendre le décret d'application de l'article 103 de la loi du 30 septembre 1986 ; qu'elle recherche également la responsabilité de l'Etat au titre de l'édiction des dispositions de cet article 103, de la méconnaissance des principes de sécurité juridique et de confiance légitime et de la réduction de la durée de son autorisation d'émettre le service M6 en mode analogique ; que ces deux requêtes présentant à juger des questions connexes, il y a lieu de les joindre afin de statuer par une seule décision ;  <br/>
<br/>
              Sur l'entrée en vigueur des articles 103 et 104 de la loi du 30 septembre 1986 :<br/>
<br/>
              3. Considérant que les dispositions de l'article 103 de la loi du 30 septembre 1986 prévoyaient que l'attribution, à l'éditeur d'un service national de télévision préalablement autorisé à diffuser en mode analogique, d'un droit d'usage pour la diffusion d'un autre service de télévision à vocation nationale serait notamment soumise à la condition que cet éditeur " souscrive à des obligations renforcées de soutien à la création en matière de diffusion et de production d'ouvres cinématographiques et audiovisuelles européennes et d'expression originale française fixées par décret en Conseil d'Etat " ; qu'en l'absence dans la loi de toute précision sur les obligations renforcées ainsi prévues, et alors que le décret qui devait définir ces obligations n'a pas été pris, ni l'article 103, ni l'article 104 qui en était indivisible, n'ont pu entrer en vigueur ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus du CSA d'accorder un canal compensatoire à la société Métropole Télévision : <br/>
<br/>
              4. Considérant qu'en l'absence d'entrée en vigueur de l'article 103 de la loi du 30 septembre 1986, le CSA était tenu de rejeter la demande de la société Métropole Télévision tendant à l'attribution du canal compensatoire prévu par cet article ; qu'il suit de là que les moyens soulevés à l'encontre de ce refus sont inopérants ; que, par suite, les conclusions présentées par la société Métropole Télévision tendant à l'annulation pour excès de pouvoir de la décision du CSA rejetant implicitement la demande qu'elle avait présentée au titre de ces dispositions ne peuvent être accueillies ;<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              En ce qui concerne la responsabilité pour faute : <br/>
<br/>
              5. Considérant, en premier lieu, qu'en l'absence d'entrée en vigueur de l'article 103 de la loi du 30 septembre 1986, le rejet de la demande présentée par la société Métropole Télévision au titre des dispositions de cet article par le CSA n'est pas constitutive d'une faute de nature à ouvrir droit à réparation ;<br/>
<br/>
              6. Considérant, en second lieu, que la société Métropole Télévision entend obtenir réparation des préjudices nés de l'abstention à prendre le décret d'application de ce même article, dans sa rédaction résultant de l'article 6 de la loi du 5 mars 2007 ; <br/>
<br/>
              7. Considérant que les préjudices qui résultent du retard mis à prendre, au delà d'un délai raisonnable, un décret nécessaire à l'application d'une loi sont, en principe, de nature à ouvrir droit à réparation ; qu'en l'espèce, le caractère fautif de l'abstention du Gouvernement doit s'apprécier en tenant compte, d'une part, de ce que ce décret aurait dû intervenir en temps utile pour permettre le lancement des services de télévision concernés à compter de la date à laquelle les dispositions de l'article 103 de la loi du 30 septembre 1986 relatives aux canaux compensatoires de la télévision numérique terrestre avaient vocation à s'appliquer effectivement, soit le 30 novembre 2011 et, d'autre part, de la date du fait générateur du dommage que la requérante impute à cette faute, soit celle de la décision implicite de rejet née du silence gardé par le CSA sur sa demande présentée le 23 avril 2012 en vue d'obtenir le bénéfice de ces dispositions ;<br/>
<br/>
              8. Considérant que le 10 avril 2008, le dispositif de canal compensatoire institué par le législateur a fait l'objet d'une plainte auprès de la Commission européenne ; qu'à la suite de cette plainte, la Commission a adressé aux autorités françaises, le 20 février 2009, une demande de communication d'informations ; que, malgré les éléments de réponse transmis par les autorités françaises, la Commission a formellement contesté la compatibilité de l'article 103 de la loi du 30 septembre 1986 avec le droit de l'Union européenne et, notamment, avec l'article 4 de la directive 2002/77/CE du 16 septembre 2002 relative à la concurrence dans les marchés des réseaux et des services de communications et l'article 5 point 2 de la directive 2002/20/CE du 7 mars 2002 relative à l'autorisation de réseaux et de services de communications électroniques ; que, le 24 novembre 2010, la commission a adressé aux autorités françaises une mise en demeure ; que, malgré les nouveaux éléments de réponse transmis par les autorités françaises, cette mise en demeure a été suivie le 29 septembre 2011 d'un avis motivé ;<br/>
<br/>
              9. Considérant qu'aux termes de cet avis motivé, les autorités françaises étaient invitées, en application de l'article 258 du Traité sur le fonctionnement de l'Union européenne, à prendre les mesures requises pour rétablir, dans un délai de deux mois, la compatibilité de la législation française avec les objectifs de ces directives ; qu'à la date d'expiration de ce délai, le 30 novembre 2011, le Parlement a été saisi de dispositions tendant à leur abrogation, et dont l'exposé des motifs faisait référence à la nécessité de se conformer à l'avis motivé du 29 septembre 2011 et de prévenir une saisine de la Cour de justice de l'Union européenne ;<br/>
<br/>
              10. Considérant qu'eu égard à l'ensemble de ces circonstances, et alors qu'au demeurant, l'abrogation des dispositions législatives en cause est effectivement intervenue par l'effet de l'article 30 de la loi du 15 novembre 2013 relative à l'indépendance de l'audiovisuel public, le fait qu'à la date du fait générateur du dommage mentionnée au point 7, le décret nécessaire à l'application de ces dispositions n'avait pas été pris ne révèle pas une faute de nature à ouvrir droit à réparation ; <br/>
<br/>
              En ce qui concerne la responsabilité du fait des lois et du fait de la méconnaissance des principes de confiance légitime et de sécurité juridique :<br/>
<br/>
              11. Considérant que la responsabilité de l'Etat du fait des lois est susceptible d'être engagée, d'une part, sur le fondement de l'égalité des citoyens devant les charges publiques, pour assurer la réparation de préjudices nés de l'adoption d'une loi, à la condition que cette loi n'ait pas exclu toute indemnisation et que le préjudice dont il est demandé réparation, revêtant un caractère grave et spécial, ne puisse, dès lors, être regardé comme une charge incombant normalement aux intéressés, d'autre part, en raison des obligations qui sont les siennes pour assurer le respect des conventions internationales par les autorités publiques, pour réparer l'ensemble des préjudices qui résultent de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France, au nombre desquels figure le respect des principes de sécurité juridique et de confiance légitime reconnus par le droit communautaire et, désormais, par le droit de l'Union européenne ;<br/>
<br/>
              Quant à l'indemnisation de préjudices imputés à l'édiction des dispositions de l'article 103 de la loi du 30 septembre 1986 :<br/>
<br/>
              12. Considérant que la société Métropole Télévision soutient que l'adoption par le législateur de dispositions qui figuraient à l'article 103 de la loi du 30 septembre 1986 lui a causé des préjudices dont la réparation incomberait à l'Etat en application des principes rappelés ci-dessus ; que, toutefois, si la société allègue des préjudices liés aux frais qu'elle a exposés pour préparer la demande de canal compensatoire, cette demande n'a été présentée au CSA qu'en avril 2012 ; qu'ainsi qu'il a été dit ci-dessus, le Gouvernement avait déposé un projet de loi tendant à l'abrogation de ces dispositions législatives en novembre 2011 et que, dès le mois de novembre 2010, la contestation de ces dispositions par la Commission européenne avait été rendue publique ; que, dans ces circonstances, la société requérante ne pouvait ignorer le risque de remise en cause des dispositions dont elle sollicitait le bénéfice ; qu'elle n'est, dès lors, pas fondée à solliciter au titre de la responsabilité du fait des lois une indemnisation couvrant les frais qu'elle a néanmoins décidé d'exposer à compter du mois de novembre 2010 pour préparer sa demande ; qu'elle n'établit pas en outre que les frais qu'elle a exposés avant cette date l'auraient été, de manière directe et certaine, en préparation de la demande qu'elle a présentée au CSA en avril 2012 ; que pour le même motif, elle n'est pas fondée, en tout état de cause, à rechercher la responsabilité de l'Etat au titre d'une éventuelle atteinte aux principes de confiance légitime et de sécurité juridique ;<br/>
<br/>
              Quant à l'indemnisation des conséquences de l'extinction anticipée de la diffusion en mode analogique résultant de la loi du 5 mars 2007 :<br/>
<br/>
              13. Considérant qu'en vertu de l'article 99 de la loi du 30 septembre 1986 dans sa rédaction issue de l'article 6 de la loi du 5 mars 2007, l'autorisation de diffusion en mode analogique qui avait été accordée à la société Métropole Télévision a pris fin le 30 novembre 2011 au lieu du 1er février 2012 ; que cette société recherche la responsabilité de l'Etat sur le fondement de l'égalité des citoyens devant les charges publiques ; <br/>
<br/>
              14. Considérant que les dispositions des articles 103 et 104 de la loi du 30 septembre 1986 organisant un mécanisme spécial de compensation des conséquences de l'interruption anticipée de l'analogique et excluant tout autre droit à réparation ont été abrogées par l'article 30 de la loi du 15 novembre 2013 relative à l'indépendance de l'audiovisuel public ; qu'à la suite de cette abrogation, la loi n'exclut pas toute indemnisation des éditeurs nationaux de télévision concernés par cette interruption anticipée ;<br/>
<br/>
              15. Considérant, toutefois, que les éléments avancés par la société Métropole Télévision à l'appui de la demande d'indemnisation qu'elle présente au titre de l'extinction anticipée du signal analogique ne permettent pas d'établir l'existence d'un préjudice direct et certain présentant un caractère de gravité de nature à lui ouvrir droit à réparation ; qu'en particulier, elle n'établit pas la réalité des coûts ou du manque à gagner qu'aurait entraînés pour elle l'abrogation par l'effet de la loi, trois mois seulement avant la date initialement prévue, de son autorisation d'émettre en mode analogique, alors que s'y est aussitôt substituée une autorisation d'émettre sur la télévision numérique terrestre ; <br/>
<br/>
              16. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non recevoir opposées par le Conseil supérieur de l'audiovisuel et le ministre de la culture et de la communication, les requêtes de la société Métropole Télévision doivent être rejetées, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Les requêtes n° 361464 et n°366191 sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Métropole Télévision (M6), au Conseil supérieur de l'audiovisuel et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. ENTRÉE EN VIGUEUR SUBORDONNÉE À L'INTERVENTION DE MESURES D'APPLICATION. - ABSENCE D'INTERVENTION DU DÉCRET NÉCESSAIRE À L'APPLICATION D'UNE LOI APRÈS UN DÉLAI RAISONNABLE - 1) PRINCIPE - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE L'ETAT - MODALITÉS D'APPRÉCIATION EN L'ESPÈCE - 2) EXCEPTION - GOUVERNEMENT TIRANT LES CONSÉQUENCES D'UN AVIS MOTIVÉ DE LA COMMISSION EUROPÉENNE ESTIMANT LA LOI CONTRAIRE AU DROIT DE L'UNION EUROPÉENNE, NOTAMMENT EN DÉPOSANT UN PROJET DE LOI D'ABROGATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-07 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RESPONSABILITÉ POUR MANQUEMENT AU DROIT DE L'UNION EUROPÉENNE. - ABSENCE D'INTERVENTION DU DÉCRET NÉCESSAIRE À L'APPLICATION D'UNE LOI APRÈS UN DÉLAI RAISONNABLE - 1) PRINCIPE - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE L'ETAT - MODALITÉS D'APPRÉCIATION EN L'ESPÈCE - 2) EXCEPTION - GOUVERNEMENT TIRANT LES CONSÉQUENCES D'UN AVIS MOTIVÉ DE LA COMMISSION EUROPÉENNE ESTIMANT LA LOI CONTRAIRE AU DROIT DE L'UNION EUROPÉENNE, NOTAMMENT EN DÉPOSANT UN PROJET DE LOI D'ABROGATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. AGISSEMENTS ADMINISTRATIFS SUSCEPTIBLES D'ENGAGER LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RETARDS. - ABSENCE D'INTERVENTION DU DÉCRET NÉCESSAIRE À L'APPLICATION D'UNE LOI APRÈS UN DÉLAI RAISONNABLE - 1) PRINCIPE - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DE L'ETAT - MODALITÉS D'APPRÉCIATION EN L'ESPÈCE - 2) EXCEPTION - GOUVERNEMENT TIRANT LES CONSÉQUENCES D'UN AVIS MOTIVÉ DE LA COMMISSION EUROPÉENNE ESTIMANT LA LOI CONTRAIRE AU DROIT DE L'UNION EUROPÉENNE, NOTAMMENT EN DÉPOSANT UN PROJET DE LOI D'ABROGATION [RJ1].
</SCT>
<ANA ID="9A"> 01-08-01-02 1) Les préjudices qui résultent du retard mis à prendre, au delà d'un délai raisonnable, un décret nécessaire à l'application d'une loi sont, en principe, de nature à ouvrir droit à réparation. En l'espèce, le caractère fautif de l'abstention du Gouvernement doit s'apprécier en tenant compte, d'une part, de ce que ce décret aurait dû intervenir en temps utile pour permettre l'application de la disposition législative en cause, d'autre part, de la date du fait générateur du dommage que le requérant impute à cette faute.... ,,2) Toutefois, en l'espèce, le dispositif législatif nécessitant l'intervention d'un décret d'application a fait l'objet d'une plainte auprès de la Commission européenne, à la suite de laquelle celle-ci a adressé aux autorités françaises une demande de communication d'informations. Malgré les éléments de réponse transmis par les autorités françaises, la Commission a ensuite formellement contesté la compatibilité de la disposition législative en cause avec le droit de l'Union européenne et adressé aux autorités françaises une mise en demeure suivie d'un avis motivé. Aux termes de cet avis motivé, les autorités françaises étaient invitées, en application de l'article 258 du Traité sur le fonctionnement de l'Union européenne, à prendre les mesures requises pour rétablir, dans un délai de deux mois, la compatibilité de la législation française avec les objectifs de ces directives. A la date d'expiration de ce délai, le 30 novembre 2011, le Parlement a été saisi de dispositions tendant à leur abrogation, et dont l'exposé des motifs faisait référence à la nécessité de se conformer à l'avis motivé du 29 septembre 2011 et de prévenir une saisine de la Cour de justice de l'Union européenne.... ,,Eu égard à l'ensemble de ces circonstances, le fait que, à la date du fait générateur du dommage invoqué par le requérant, le décret nécessaire à l'application de ces dispositions n'avait pas été pris, ne révèle pas une faute de nature à ouvrir droit à réparation.</ANA>
<ANA ID="9B"> 15-07 1) Les préjudices qui résultent du retard mis à prendre, au delà d'un délai raisonnable, un décret nécessaire à l'application d'une loi sont, en principe, de nature à ouvrir droit à réparation. En l'espèce, le caractère fautif de l'abstention du Gouvernement doit s'apprécier en tenant compte, d'une part, de ce que ce décret aurait dû intervenir en temps utile pour permettre l'application de la disposition législative en cause, d'autre part, de la date du fait générateur du dommage que le requérant impute à cette faute.... ,,2) Toutefois, en l'espèce, le dispositif législatif nécessitant l'intervention d'un décret d'application a fait l'objet d'une plainte auprès de la Commission européenne, à la suite de laquelle celle-ci a adressé aux autorités françaises une demande de communication d'informations. Malgré les éléments de réponse transmis par les autorités françaises, la Commission a ensuite formellement contesté la compatibilité de la disposition législative en cause avec le droit de l'Union européenne et adressé aux autorités françaises une mise en demeure suivie d'un avis motivé. Aux termes de cet avis motivé, les autorités françaises étaient invitées, en application de l'article 258 du Traité sur le fonctionnement de l'Union européenne, à prendre les mesures requises pour rétablir, dans un délai de deux mois, la compatibilité de la législation française avec les objectifs de ces directives. A la date d'expiration de ce délai, le 30 novembre 2011, le Parlement a été saisi de dispositions tendant à leur abrogation, et dont l'exposé des motifs faisait référence à la nécessité de se conformer à l'avis motivé du 29 septembre 2011 et de prévenir une saisine de la Cour de justice de l'Union européenne.... ,,Eu égard à l'ensemble de ces circonstances, le fait que, à la date du fait générateur du dommage invoqué par le requérant, le décret nécessaire à l'application de ces dispositions n'avait pas été pris, ne révèle pas une faute de nature à ouvrir droit à réparation.</ANA>
<ANA ID="9C"> 60-01-03-01 1) Les préjudices qui résultent du retard mis à prendre, au delà d'un délai raisonnable, un décret nécessaire à l'application d'une loi sont, en principe, de nature à ouvrir droit à réparation. En l'espèce, le caractère fautif de l'abstention du Gouvernement doit s'apprécier en tenant compte, d'une part, de ce que ce décret aurait dû intervenir en temps utile pour permettre l'application de la disposition législative en cause, d'autre part, de la date du fait générateur du dommage que le requérant impute à cette faute.... ,,2) Toutefois, en l'espèce, le dispositif législatif nécessitant l'intervention d'un décret d'application a fait l'objet d'une plainte auprès de la Commission européenne, à la suite de laquelle celle-ci a adressé aux autorités françaises une demande de communication d'informations. Malgré les éléments de réponse transmis par les autorités françaises, la Commission a ensuite formellement contesté la compatibilité de la disposition législative en cause avec le droit de l'Union européenne et adressé aux autorités françaises une mise en demeure suivie d'un avis motivé. Aux termes de cet avis motivé, les autorités françaises étaient invitées, en application de l'article 258 du Traité sur le fonctionnement de l'Union européenne, à prendre les mesures requises pour rétablir, dans un délai de deux mois, la compatibilité de la législation française avec les objectifs de ces directives. A la date d'expiration de ce délai, le 30 novembre 2011, le Parlement a été saisi de dispositions tendant à leur abrogation, et dont l'exposé des motifs faisait référence à la nécessité de se conformer à l'avis motivé du 29 septembre 2011 et de prévenir une saisine de la Cour de justice de l'Union européenne.... ,,Eu égard à l'ensemble de ces circonstances, le fait que, à la date du fait générateur du dommage invoqué par le requérant, le décret nécessaire à l'application de ces dispositions n'avait pas été pris, ne révèle pas une faute de nature à ouvrir droit à réparation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., s'agissant de l'absence d'intervention d'un décret nécessaire à l'application d'une loi incompatible avec le droit de l'Union européenne ou avec une autre convention internationale, CE, 24 février 1999, Association de patients de la médecine d'orientation anthroposophique et autres, n° 195354, p. 29 ; CE, 16 juillet 20008, M. Masson, n° 300458, p. 286.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
