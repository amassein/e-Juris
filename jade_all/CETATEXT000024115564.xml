<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024115564</ID>
<ANCIEN_ID>JG_L_2011_06_000000337992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/11/55/CETATEXT000024115564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 01/06/2011, 337992, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:337992.20110601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Larbi A, ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle la commission de recours contre les décisions de refus de visa d'entrée en France a rejeté son recours dirigé contre la décision du 2 décembre 2009 par laquelle le consul général de France à Alger (Algérie) lui a refusé un visa de long séjour ; <br/>
<br/>
              2°) d'enjoindre au consul général de France à Alger de lui délivrer un visa de long séjour dans un délai d'un mois à compter de la notification de la décision à intervenir et ce, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que M. Larbi A, né en 1938 et de nationalité algérienne, a résidé en France depuis 1972 et est père de onze enfants ; qu'en raison de faits commis en 1994, qualifiés de participation à une association de malfaiteurs en vue de la préparation d'un crime, de détention sans autorisation de munition ou d'arme de 1ère ou 4ème catégorie et d'utilisation de fréquence ou d'installation radioélectrique sans autorisation, il a été condamné par un jugement du tribunal de grande instance de Paris en date du 7 février 1997, confirmé par un arrêt de la cour d'appel de Paris en date du 22 septembre 1997, à une peine de cinq ans d'emprisonnement et à une interdiction du territoire d'une durée de dix ans ; que le préfet de police a pris à l'encontre de M. A, le 12 février 1998, un arrêté d'expulsion du territoire français fondé sur l'existence d'une menace grave pour l'ordre public ; que le recours pour excès de pouvoir formé par M. A contre cet arrêté a été définitivement rejeté par un arrêt de la cour administrative d'appel de Paris en date du 2 octobre 2001 ; que, par décision du 21 mars 2009, le préfet de l'Essonne a mis à exécution cet arrêté d'expulsion ; que sa décision a toutefois été suspendue par une ordonnance du juge des référés du tribunal  administratif de Versailles en date du 23 mars 2009, qui a également enjoint aux autorités consulaires françaises en Algérie de prendre toutes mesures de nature à permettre le retour en France de M. A ; que, par une ordonnance du 10 avril 2009, le juge des référés du Conseil d'Etat a rejeté l'appel formé contre cette ordonnance par le ministre de l'intérieur, de l'outre-mer et des collectivités territoriales ; que M. A, qui se trouve en Algérie à la suite de son éloignement, a demandé un visa de long séjour qui lui a été refusé par une décision du 2 décembre 2009 du consul général de France à Alger, implicitement confirmée par la commission de recours contre les décisions de refus de visa d'entrée en France ; que M. A demande l'annulation de cette décision implicite ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que la commission de recours contre les décisions de refus de visa d'entrée en France s'est fondée, pour confirmer le refus du consul général de France à Alger, sur la menace à l'ordre public que constituerait la présence en France du requérant ;   <br/>
<br/>
              Considérant qu'il appartient au juge de l'excès de pouvoir, dans l'exercice de ses pouvoirs généraux de direction de la procédure, de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction sur les points en litige ;<br/>
<br/>
              Considérant que, pour se prononcer sur une requête, assortie d'allégations sérieuses, dirigée contre un refus de visa justifié par un motif d'ordre public, le juge de l'excès de pouvoir doit être en mesure d'apprécier, à partir d'éléments précis, le bien-fondé du motif retenu par l'administration ; qu'il appartient en conséquence à celle-ci de verser au dossier, dans le respect des exigences liées à la sécurité nationale, les renseignements nécessaires pour que le juge statue en pleine connaissance de cause ;<br/>
<br/>
              Considérant que M. A fait valoir que, compte tenu de l'ancienneté et de la régularité de son séjour en France, jusqu'à l'intervention d'un arrêté d'expulsion qui ne sera mis à exécution qu'après onze années, et eu égard au fait que son épouse et ses enfants, dont la plupart ont la nationalité française, vivent ou ont toujours vécu en France, le refus de visa qui lui a été opposé porte une atteinte excessive à son droit au respect de sa vie privée et familiale ; que le ministre de l'immigration, de l'intégration et de l'identité nationale s'est borné en défense à faire valoir que le motif d'ordre public retenu par l'administration est corroboré par une " note blanche " établie par la direction centrale du renseignement intérieur, sans produire cette note et en n'en indiquant la teneur qu'en termes très généraux ; qu'ainsi, en l'état de l'instruction, le Conseil d'Etat ne dispose pas, au regard de l'argumentation avancée par M. A, des informations nécessaires pour apprécier la légalité du refus de visa attaqué ; qu'il y a lieu, en conséquence, d'ordonner avant-dire droit au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration de produire, dans un délai d'un mois, les éléments qui peuvent être versés au dossier dans le respect des exigences liées à la sécurité nationale, permettant au Conseil d'Etat de se prononcer sur le bien-fondé du motif d'ordre public retenu à l'appui de la décision attaquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : Avant-dire droit sur la requête de M. A, il est ordonné au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration de produire, dans un délai d'un mois à compter de la notification de la présente décision, les éléments qui peuvent être versés au dossier dans le respect des exigences liées à la sécurité nationale, permettant au Conseil d'Etat de se prononcer sur le bien-fondé du motif d'ordre public retenu à l'appui du refus de visa attaqué.<br/>
Article 2 : La présente décision sera notifiée à M. Larbi A et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-005-01 ÉTRANGERS. ENTRÉE EN FRANCE. VISAS. - REFUS DE VISA POUR MOTIF D'ORDRE PUBLIC - OBLIGATION, POUR L'ADMINISTRATION, DE FOURNIR DES ÉLÉMENTS PRÉCIS PERMETTANT AU JUGE D'APPRÉCIER LE BIEN-FONDÉ DE CE MOTIF - CONSÉQUENCE - POSSIBILITÉ POUR LE CONSEIL D'ETAT D'ORDONNER AVANT-DIRE DROIT LA PRODUCTION DE TELS ÉLÉMENTS, DANS LE RESPECT DES EXIGENCES LIÉES À LA SÉCURITÉ NATIONALE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01-03 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. PRODUCTION ORDONNÉE. - REFUS DE VISA POUR MOTIF D'ORDRE PUBLIC - OBLIGATION, POUR L'ADMINISTRATION, DE FOURNIR DES ÉLÉMENTS PRÉCIS PERMETTANT AU JUGE D'APPRÉCIER LE BIEN-FONDÉ DE CE MOTIF - CONSÉQUENCE - POSSIBILITÉ POUR LE CONSEIL D'ETAT D'ORDONNER AVANT-DIRE DROIT LA PRODUCTION DE TELS ÉLÉMENTS, DANS LE RESPECT DES EXIGENCES LIÉES À LA SÉCURITÉ NATIONALE [RJ1].
</SCT>
<ANA ID="9A"> 335-005-01 Pour se prononcer sur une requête, assortie d'allégations sérieuses, dirigée contre un refus de visa justifié par un motif d'ordre public, le juge de l'excès de pouvoir doit être en mesure d'apprécier, à partir d'éléments précis, le bien-fondé du motif retenu par l'administration. Il appartient en conséquence à celle-ci de verser au dossier, dans le respect des exigences liées à la sécurité nationale, les renseignements nécessaires pour que le juge statue en pleine connaissance de cause. Si elle ne l'a pas fait, le Conseil d'Etat peut, avant-dire droit sur la requête, ordonner la production des éléments qui peuvent être versés au dossier dans le respect des exigences liées à la sécurité nationale.</ANA>
<ANA ID="9B"> 54-04-01-03 Pour se prononcer sur une requête, assortie d'allégations sérieuses, dirigée contre un refus de visa justifié par un motif d'ordre public, le juge de l'excès de pouvoir doit être en mesure d'apprécier, à partir d'éléments précis, le bien-fondé du motif retenu par l'administration. Il appartient en conséquence à celle-ci de verser au dossier, dans le respect des exigences liées à la sécurité nationale, les renseignements nécessaires pour que le juge statue en pleine connaissance de cause. Si elle ne l'a pas fait, le Conseil d'Etat peut, avant-dire droit sur la requête, ordonner la production des éléments qui peuvent être versés au dossier dans le respect des exigences liées à la sécurité nationale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 6 novembre 2002, M. Moon Sun Myung c/ CNIL, n° 194295, p. 380.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
