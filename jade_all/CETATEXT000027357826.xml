<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027357826</ID>
<ANCIEN_ID>JG_L_2013_03_000000348165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/35/78/CETATEXT000027357826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 13/03/2013, 348165, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LAUGIER, CASTON ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348165.20130313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 avril et 5 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX01548 du 1er février 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement n° 0700459 du tribunal administratif de Bordeaux du 27 avril 2010 qui avait rejeté sa demande d'annulation de la décision du 17 novembre 2006 par laquelle le directeur du centre hospitalier Charles Perrens a procédé à son hospitalisation à la demande d'un tiers ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Charles Perrens le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu les décisions du Conseil Constitutionnel n° 2010-71 QPC du 26 novembre 2010 et n° 2012 - 235 QPC du 20 avril 2012 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Laugier, Caston, avocat de M. A...et de Me Le Prado, avocat du centre hospitalier Charles Perrens,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Laugier, Caston, avocat de M. A... et à Me Le Prado, avocat du centre hospitalier Charles Perrens ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la régularité de l'arrêt attaqué : <br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le centre hospitalier Charles Perrens a transmis son premier mémoire en défense par télécopie au greffe de la cour administrative d'appel de Bordeaux le 31 décembre 2011 ; que ce mémoire a été communiqué par le greffe à M. A...le lundi 3 janvier 2011 en l'invitant à produire, le cas échéant, un mémoire en réplique " aussi rapidement que possible " ; qu'en dépit de la demande du même jour de M. A...que l'audience soit reportée ou que le mémoire en défense soit écarté des débats, la cour a examiné l'affaire à l'audience du 4 janvier 2011 ; qu'en procédant de la sorte, elle n'a pas mis M. A...en mesure d'exercer ses droits et a méconnu le caractère contradictoire de la procédure ; que, par suite, M. A...est fondé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Sur la régularité du jugement du tribunal administratif :<br/>
<br/>
              4. Considérant que, devant le tribunal administratif de Bordeaux, M. A... a invoqué l'irrégularité de la mesure d'hospitalisation sous contrainte dont il a fait l'objet au motif que son fils, le tiers qui a présenté la demande d'hospitalisation, n'aurait pas formulé cette demande librement ; que le tribunal administratif de Bordeaux, en ne répondant pas à ce moyen, qui n'était pas inopérant, a entaché son jugement d'une insuffisance de motivation ; que, par suite, M. A...est fondé à en demander l'annulation pour ce motif ;<br/>
<br/>
              5. Considérant qu'il y a lieu d'évoquer et de statuer  immédiatement sur la demande présentée par M. A...devant le tribunal administratif de Bordeaux ;  <br/>
<br/>
              Sur la régularité de la décision d'admission :<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 3212-1 du code de la santé publique, dans sa rédaction alors en vigueur : " Une personne atteinte de troubles mentaux ne peut être hospitalisée sans son consentement sur demande d'un tiers que si : 1° Ses troubles rendent impossible son consentement ; 2° Son état impose des soins immédiats assortis d'une surveillance constante en milieu hospitalier. / La demande d'admission est présentée soit par un membre de la famille du malade, soit par une personne susceptible d'agir dans l'intérêt de celui-ci, à l'exclusion des personnels soignants dès lors qu'ils exercent dans l'établissement d'accueil. / Cette demande doit être manuscrite et signée par la personne qui la formule (...). / La demande d'admission est accompagnée de deux certificats médicaux datant de moins de quinze jours et circonstanciés, attestant que les conditions prévues par les deuxième et troisième alinéas sont remplies. / Le premier certificat médical ne peut être établi que par un médecin n'exerçant pas dans l'établissement accueillant le malade ; il constate l'état mental de la personne à soigner, indique les particularités de sa maladie et la nécessité de la faire hospitaliser sans son consentement. Il doit être confirmé par un certificat d'un deuxième médecin qui peut exercer dans l'établissement accueillant le malade (...) " ;  <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que la demande d'hospitalisation a été établie conformément à ces dispositions ; que si les dispositions de l'article L. 3212-2 du code de la santé publique imposent notamment que le directeur de l'établissement vérifie le respect de ces formalités, elles ne l'obligent pas à recevoir personnellement le tiers à l'origine de la demande ; que la circonstance que ce tiers regretterait d'avoir signé une telle demande au motif qu'il n'en aurait pas mesuré la portée - alors d'ailleurs que les membres de la famille énumérés par l'article L. 3212-9 et, sous réserve de l'opposition d'un parent, la personne qui a signé la demande d'admission peuvent en vertu de cet article requérir la levée de l'hospitalisation - est sans effet sur la légalité de la décision d'admission ; qu'il ne ressort pas des pièces du dossier que la demande signée par le fils du requérant devrait être regardée comme émanant du centre hospitalier ; que, dès lors, les moyens de M. A...tirés de ce que la demande d'hospitalisation n'a pu légalement fonder la mesure d'hospitalisation dont il a fait l'objet, son fils ayant signé cette demande sans pouvoir en mesurer la portée et sans que le directeur de l'établissement ne vérifie la régularité de sa demande et ne s'assure de l'identité de son auteur, doivent être écartés ;   <br/>
<br/>
              8. Considérant, en second lieu, qu'aucun texte n'exige que la demande d'admission soit formée avant le ou les certificats médicaux requis, pourvu que la personne souffrant de troubles rendant impossible son consentement et nécessitant des soins assortis d'une surveillance constante en milieu hospitalier ne soit retenue que le temps strictement nécessaire à la mise en place de la mesure et qu'au moment de l'admission prononcée par le directeur d'établissement, l'ensemble des éléments exigés aient été réunis ; qu'en l'espèce, il ressort des pièces du dossier que, le 17 novembre 2006, M. A...a été examiné par un médecin dès son arrivée au centre hospitalier Charles Perrens et maintenu moins de deux heures dans l'établissement avant son admission, soit le temps strictement nécessaire à la mise en place de la mesure d'hospitalisation sous contrainte et du recueil de l'ensemble des éléments légalement exigés, notamment la demande du tiers ; que, par suite, le moyen du requérant tiré de ce que le premier examen médical qui matérialise, selon lui, la décision d'hospitalisation de l'irrégularité serait intervenu avant la demande émanant de son fils ne peut qu'être écarté ; <br/>
<br/>
              Sur le recours à la procédure exceptionnelle mentionnée à l'article L. 3212-3 du code de la santé publique :<br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 3212-3 du code de la santé publique, dans sa rédaction alors en vigueur : " A titre exceptionnel et en cas de péril imminent pour la santé du malade dûment constaté par le médecin, le directeur de l'établissement peut prononcer l'admission au vu d'un seul certificat médical émanant éventuellement d'un médecin exerçant dans l'établissement d'accueil " ;<br/>
<br/>
              10. Considérant qu'il résulte de ces dispositions que le recours à la procédure qui permet au directeur d'établissement de prononcer l'admission au vu d'un seul certificat médical émanant éventuellement d'un médecin exerçant dans l'établissement, constitue une modalité particulière d'hospitalisation sur demande d'un tiers ; que, par suite, le moyen tiré de ce que le centre hospitalier Charles Perrens ne pouvait valablement mettre en oeuvre cette procédure exceptionnelle, tout en sollicitant du tiers une demande d'admission au titre de l'article L. 3212-1, doit être écarté ;<br/>
<br/>
              11. Considérant qu'il ressort des pièces du dossier que M. A...a été examiné par un médecin de l'établissement qui a établi, en application des articles L. 3212-1 et L. 3212-3 du code de la santé publique, un certificat médical décrivant de façon circonstanciée un état pathologique rendant impossible son consentement, soulignant " le risque de péril pour le patient en l'absence de soin " et concluant au caractère imminent de ce risque rendant nécessaire la mise en place immédiate de soins sous la forme d'une hospitalisation à la demande d'un tiers selon la procédure mentionnée à l'article L. 3212-3 du code de la santé publique ; que ce certificat médical n'avait pas à caractériser le risque de trouble à l'ordre public, qui relève seulement de la procédure d'hospitalisation d'office ; que, par suite, le directeur de l'établissement pouvait valablement prononcer l'admission de M.A..., contrairement à ce qui est soutenu, au vu de ce seul certificat médical ;<br/>
<br/>
              Sur la décision d'admission :<br/>
<br/>
              12. Considérant qu'aux termes de l'article L. 3212-2 du code de la santé publique, dans sa rédaction alors en vigueur : " Avant d'admettre une personne en hospitalisation sur demande d'un tiers, le directeur de l'établissement vérifie que la demande a été établie conformément aux dispositions de l'article L. 3212-1 ou de l'article L. 3212-3 et s'assure de l'identité de la personne pour laquelle l'hospitalisation est demandée et de celle de la personne qui demande l'hospitalisation (...) " ; que, contrairement à ce que soutient M.A..., cette disposition n'exige pas que la décision d'admission, qui a pu être légalement prise au vu de la demande présentée par un tiers et du seul certificat médical exigé en application de l'article L. 3212-3 du code de la santé publique, soit formalisée par écrit ; <br/>
<br/>
              Sur les formalités postérieures à l'admission :<br/>
<br/>
              13. Considérant que, s'il est soutenu que les proches de M. A...n'auraient pas été informés des droits du malade hospitalisé et des possibilités de mainlevée et que le procureur de la République n'aurait pas été informé de l'hospitalisation de M.A..., ces circonstances, à les supposer établies, se rapportent à des obligations qui n'ont pas à être accomplies avant l'adoption de la décision et sont, par suite, sans influence sur la légalité de celle-ci ; <br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision du 17 novembre 2006 par laquelle le directeur du centre hospitalier Charles Perrens a procédé à son hospitalisation à la demande d'un tiers ;<br/>
<br/>
              Sur les conclusions aux fins d'indemnisation :<br/>
<br/>
              15. Considérant que, si l'autorité judiciaire est seule compétente pour apprécier la nécessité d'une mesure de placement d'office en hôpital psychiatrique et les conséquences qui peuvent en résulter, il appartient à la juridiction administrative, s'agissant des recours dont elle a été saisie avant le 1er janvier 2013, d'apprécier la régularité de la décision administrative qui ordonne le placement ; que, lorsque cette dernière s'est prononcée sur ce point, l'autorité judiciaire est compétente pour statuer sur les conséquences dommageables de l'ensemble des irrégularités entachant la mesure de placement d'office ; que, par suite, les conclusions tendant à l'indemnisation du préjudice invoqué par M. A..., quand bien même il résulterait des irrégularités invoquées, doivent être rejetées comme portées devant une juridiction incompétente pour en connaître ;<br/>
<br/>
              Sur les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du centre hospitalier Charles Perrens qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 1er février 2011 est annulé.<br/>
Article 2 : Le jugement du tribunal administratif de Bordeaux du 27 avril 2010 est annulé.<br/>
Article 3 : La demande présentée par M. A...devant le tribunal administratif de Bordeaux et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au centre hospitalier Charles Perrens.<br/>
Copie sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
