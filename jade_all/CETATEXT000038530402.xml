<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038530402</ID>
<ANCIEN_ID>JG_L_2019_05_000000426433</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/53/04/CETATEXT000038530402.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 29/05/2019, 426433, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426433</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426433.20190529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...B...a demandé le 29 novembre 2018 au juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de suspendre l'exécution de l'arrêté du 26 novembre 2018 par laquelle le préfet de Mayotte lui a fait obligation de quitter sans délai le territoire français et lui a interdit le retour pour une durée d'un an, en deuxième lieu, d'enjoindre au préfet, en cas de reconduite préalable à l'audience, d'organiser son retour en France dans le délai de 24 heures à compter de la notification de l'ordonnance, sous astreinte de 200 euros par jour de retard, en troisième lieu, de l'admettre au bénéfice de l'aide juridictionnelle, et en quatrième lieu, de mettre à la charge de l'Etat une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n° 1801756 du 29 novembre 2018, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 13 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
	Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              1.	Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) " Aux termes de l'article L. 522-1 du même code : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale. / Lorsqu'il lui est demandé de prononcer les mesures visées aux articles L. 521-1 et L. 521-2, de les modifier ou d'y mettre fin, il informe sans délai les parties de la date et de l'heure de l'audience publique (...) ". Selon l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1. ". Et, aux termes de l'article L. 523-1 du même code : " Les décisions rendues en application des articles L. 521-1, L. 521-3, L. 521-4 et L. 522-3 sont rendues en dernier ressort. / Les décisions rendues en application de l'article L. 521-2 sont susceptibles d'appel devant le Conseil d'Etat dans les quinze jours de leur notification. (...) " <br/>
<br/>
              2.	Lorsque le juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative a estimé, au vu de la requête dont il est saisi, qu'il y avait lieu, non de la rejeter en l'état pour l'un des motifs mentionnés à l'article L. 522-3 du code de justice administrative, mais d'engager la procédure prévue à l'article L. 522-1 de ce code, il lui incombe de poursuivre cette procédure et, notamment, de tenir une audience publique. Il en va différemment lorsque, après que cette procédure a été engagée, intervient un désistement ou un évènement rendant sans objet la requête. Dans ce cas, le juge des référés peut, dans le cadre de son office, donner acte du désistement ou constater un non-lieu sans tenir d'audience. La décision qu'il rend, qui n'entre dans aucune des hypothèses mentionnées par l'article L. 522-3 du code de justice administrative, est susceptible d'appel devant le juge des référés du Conseil d'Etat, en application du deuxième alinéa de l'article L. 523-1 de ce code. <br/>
<br/>
              Sur le litige :<br/>
<br/>
              3.	Il résulte de l'instruction que M. C...A...B..., ressortissant comorien, après avoir bénéficié de titres de séjour en qualité de conjoint de ressortissant français du 4 novembre 2013 au 28 octobre 2016, a vu sa demande d'admission au séjour sur le fondement du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile rejetée par un arrêté du préfet de Mayotte en date du 30 août 2018 lui faisant également obligation de quitter le territoire français dans le délai d'un mois. Il s'est maintenu irrégulièrement sur le territoire et a fait l'objet d'une interpellation le 26 novembre 2018. Par un arrêté du même jour, le préfet de Mayotte l'a obligé à quitter le territoire sans délai et a prononcé une interdiction de retour d'un an. Le 28 novembre 2018, M. A...B...a introduit devant le juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, une requête tendant à la suspension de cet arrêté, qui a été rejetée par une ordonnance n° 1801736 du même jour. Le 29 novembre 2018 à 12 heures (heure locale), il a été reconduit vers l'île d'Anjouan. Le même jour, il a introduit une nouvelle requête en référé-liberté qui a été enregistrée au greffe du tribunal administratif de Mayotte à 15 heures 14 (heure locale), pour demander, d'une part, la suspension de l'arrêté du 26 novembre 2018 et, d'autre part, qu'il soit enjoint au préfet, en cas de reconduite préalable à l'audience, d'organiser son retour en France dans le délai de 24 heures à compter de la notification de l'ordonnance, sous astreinte de 200 euros par jour de retard. Le juge des référés, après avoir relevé que la mesure d'éloignement avait été exécutée avant qu'il ait été saisi, a jugé qu'il n'y avait pas lieu de statuer sur les conclusions à fin de suspension de l'arrêté litigieux et a rejeté le surplus des conclusions de la requête. M. A...B...fait appel de ce rejet. <br/>
<br/>
              4. D'une part, il résulte de ce qui a été dit au point précédent que la mesure d'éloignement prise en application de l'arrêté litigieux avait été exécutée avant que le juge des référés qui a rendu l'ordonnance attaquée ne soit saisi. Par suite, le juge des référés a estimé à bon droit qu'il n'y avait pas lieu de statuer sur les conclusions tendant à la suspension de l'arrêté litigieux en tant qu'il portait obligation de quitter le territoire français. <br/>
<br/>
              5. D'autre part, les documents produits par M. A...B...n'établissent pas qu'il contribue effectivement à l'entretien et à l'éducation de son enfant, ni que sa présence auprès de lui serait indispensable. Par suite, il ne résulte pas de l'instruction que l'arrêté du 26 novembre 2018 par lequel le préfet de Mayotte lui a fait obligation de quitter sans délai le territoire français et lui a interdit le retour pour une durée d'un an serait de nature à porter une atteinte grave et manifestement illégale à l'intérêt supérieur de son enfant. <br/>
<br/>
              6.	Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur l'urgence, que l'appel de M. A...B...ne peut être accueilli. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C...A...B....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
