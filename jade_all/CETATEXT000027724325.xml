<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724325</ID>
<ANCIEN_ID>JG_L_2013_07_000000349135</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/43/CETATEXT000027724325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 17/07/2013, 349135</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349135</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349135.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU> Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 mai et 9 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...à Dakar (Sénégal) ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07/00057 du 11 mars 2010 par lequel la cour régionale des pensions militaires de Paris a, sur appel du ministre de la défense, d'une part, infirmé le jugement n° 02/00127 du 19 septembre 2007 du tribunal départemental des pensions de Paris faisant droit à sa demande tendant à la décristallisation de sa pension militaire d'invalidité et, d'autre part, rejeté cette demande comme irrecevable ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... A..., ressortissant sénégalais, est titulaire d'une pension militaire d'invalidité transformée en indemnité personnelle et viagère en application de l'article 71-I de la loi de finances pour 1960 du 26 décembre 1959, modifié par l'article 14 de la loi de finances rectificative pour 1979 du 21 décembre 1979 ; qu'il a sollicité du ministre chargé des anciens combattants la revalorisation de cette pension dans les mêmes conditions que celles qui sont applicables aux pensions servies à des ressortissants français ; qu'à la suite du rejet de sa demande, M. A...a présenté devant le tribunal départemental des pensions de Paris, conjointement avec quarante-neuf  autres titulaires de pensions se trouvant dans une situation similaire à la sienne, des conclusions tendant à la décristallisation de sa pension ; que ce tribunal a, par un jugement du 19 septembre 2007, fait droit à ces conclusions ; que M. A...se pourvoit en cassation contre l'arrêt du 11 mars 2010 par lequel la cour régionale des pensions militaires de Paris, après avoir annulé le jugement du tribunal départemental, a rejeté ses conclusions comme irrecevables ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 57 du code des pensions militaires d'invalidité : " La requête par laquelle le tribunal est saisi et qui est adressée par lettre recommandée au greffier doit indiquer les nom, prénoms, profession et domicile du demandeur. Elle précise l'objet de la demande et les moyens invoqués ; si elle n'est pas accompagnée de la décision attaquée, elle doit en faire connaître la date " ; qu'en outre, la recevabilité d'une requête présentée conjointement par plusieurs requérants contre plusieurs décisions est subordonnée à la condition que la solution du litige ne nécessite pas un examen distinct de la situation individuelle de chacun des requérants ; <br/>
<br/>
              3. Considérant , d'une part, que la cour a dénaturé les pièces de la procédure devant le tribunal départemental des pensions de Paris en jugeant que la requête collective soumise au tribunal par le requérant et quarante-neuf autres demandeurs, avait été présentée par un simple " bordereau d'envoi des requêtes " formé d'une liste de noms, prénoms et numéros d'inscription qui ne comportait pas l'ensemble des éléments requis par l'article R. 57 précité du code des pensions militaires d'invalidité ; que, d'autre part, s'il ressort des pièces de la procédure devant le tribunal départemental des pensions de Paris que la demande collective dont celui-ci était saisi nécessitait, eu égard  au fait que chacun des demandeurs était titulaire d'une pension présentant des caractéristiques propres, un examen distinct de chaque situation individuelle, la cour a commis une erreur de droit en jugeant irrecevable la demande de M. A... devant le tribunal, dès lors qu'il ressortait des pièces  qui lui étaient soumises que le tribunal, qui avait divisé la demande collective dont il était saisi en cinquante demandes, enregistrées sous des numéros distincts, puis procédé à une instruction séparée de chaque demande et rendu cinquante jugements distincts, avait procédé, de sa propre initiative, à la régularisation de cette demande ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 11 mars 2010 de la cour régionale des pensions militaires de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour régionale des pensions militaires de Paris. <br/>
Article 3 : La présente décision sera notifiée à M. B...A... et au ministre de la défense.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-08-02-01-02 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONTENTIEUX. PROCÉDURE DEVANT LES JURIDICTIONS SPÉCIALES DES PENSIONS. RÈGLES COMMUNES AU TRIBUNAL DÉPARTEMENTAL ET À LA COUR RÉGIONALE DES PENSIONS. INTRODUCTION DE L'INSTANCE. - DEMANDE COLLECTIVE PRÉSENTÉE AU TRIBUNAL DÉPARTEMENTAL DES PENSIONS ET NÉCESSITANT UN EXAMEN DISTINCT DE CHAQUE SITUATION INDIVIDUELLE - IRRECEVABILITÉ DE LA DEMANDE DU REQUÉRANT DEVANT LE TRIBUNAL - ABSENCE, DÈS LORS QUE LE TRIBUNAL, QUI AVAIT DIVISÉ LA DEMANDE COLLECTIVE EN DEMANDES ENREGISTRÉES SOUS DES NUMÉROS DISTINCTS, PUIS PROCÉDÉ À UNE INSTRUCTION SÉPARÉE DE CHAQUE DEMANDE ET RENDU DES JUGEMENTS DISTINCTS, A PROCÉDÉ, DE SA PROPRE INITIATIVE, À LA RÉGULARISATION DE CETTE DEMANDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-08-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. REQUÊTE COLLECTIVE. - TRIBUNAL DÉPARTEMENTAL DES PENSIONS - DEMANDE COLLECTIVE NÉCESSITANT UN EXAMEN DISTINCT DE CHAQUE SITUATION INDIVIDUELLE - IRRECEVABILITÉ DE LA DEMANDE DU REQUÉRANT DEVANT LE TRIBUNAL - ABSENCE, DÈS LORS QUE LE TRIBUNAL, QUI AVAIT DIVISÉ LA DEMANDE COLLECTIVE EN DEMANDES ENREGISTRÉES SOUS DES NUMÉROS DISTINCTS, PUIS PROCÉDÉ À UNE INSTRUCTION SÉPARÉE DE CHAQUE DEMANDE ET RENDU DES JUGEMENTS DISTINCTS, A PROCÉDÉ, DE SA PROPRE INITIATIVE, À LA RÉGULARISATION DE CETTE DEMANDE.
</SCT>
<ANA ID="9A"> 48-01-08-02-01-02 Tribunal départemental des pensions ayant été saisi d'une demande collective qui nécessitait, eu égard au fait que chacun des demandeurs était titulaire d'une pension présentant des caractéristiques propres, un examen distinct de chaque situation individuelle.... ,,La demande du requérant devant le tribunal n'était pas irrecevable, dès lors que le tribunal, qui avait divisé la demande collective dont il était saisi en cinquante demandes, enregistrées sous des numéros distincts, puis procédé à une instruction séparée de chaque demande et rendu cinquante jugements distincts, avait procédé, de sa propre initiative, à la régularisation de cette demande.</ANA>
<ANA ID="9B"> 54-01-08-03 Tribunal départemental des pensions ayant été saisi d'une demande collective qui nécessitait, eu égard au fait que chacun des demandeurs était titulaire d'une pension présentant des caractéristiques propres, un examen distinct de chaque situation individuelle.... ,,La demande du requérant devant le tribunal n'était pas irrecevable, dès lors que le tribunal, qui avait divisé la demande collective dont il était saisi en cinquante demandes, enregistrées sous des numéros distincts, puis procédé à une instruction séparée de chaque demande et rendu cinquante jugements distincts, avait procédé, de sa propre initiative, à la régularisation de cette demande.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
