<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043328486</ID>
<ANCIEN_ID>JG_L_2021_04_000000428312</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/84/CETATEXT000043328486.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/04/2021, 428312</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428312</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:428312.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lyon d'annuler la décision du 20 octobre 2016 par laquelle la directrice des Hospices civils de Lyon lui a refusé le bénéfice de l'allocation d'aide au retour à l'emploi et d'enjoindre aux Hospices civils de Lyon de lui accorder le bénéfice de cette aide ou, subsidiairement, de réexaminer sa demande. Par un jugement n° 1609296 du 21 décembre 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 février et 21 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge des Hôpitaux civils de Lyon la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - l'arrêté du 25 juin 2014 portant agrément de la convention du 14 mai 2014 relative à l'indemnisation du chômage et les textes qui lui sont associés ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme B... et à la SCP Ohl, Vexliard, avocat des Hospices civils de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., employée par les Hospices civils de Lyon par l'effet de plusieurs contrats à durée déterminée successifs de brève durée, a informé son employeur de son intention de ne pas renouveler le contrat en cours d'exécution qui expirait le 31 août 2016. Par une décision du 20 octobre 2016, les Hospices civils de Lyon lui ont refusé le bénéfice de l'allocation d'aide au retour à l'emploi. Elle se pourvoit en cassation contre le jugement du 21 décembre 2018 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à se voir reconnaître le bénéfice de cette allocation. <br/>
<br/>
              2. Aux termes de l'article L. 5421-1 du code du travail, dans sa rédaction applicable à l'espèce : " En complément des mesures tendant à faciliter leur reclassement ou leur conversion, les travailleurs involontairement privés d'emploi ou dont le contrat de travail a été rompu conventionnellement selon les modalités prévues aux articles L. 1237-11 et suivants du présent code ou à l'article L. 421-12-2 du code de la construction et de l'habitation, aptes au travail et recherchant un emploi, ont droit à un revenu de remplacement dans les conditions fixées au présent titre ". Aux termes de l'article L. 5424-1 de ce code, dans sa rédaction alors applicable : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : (...) 2° Les agents non titulaires des collectivités territoriales et les agents non statutaires des établissements publics administratifs autres que ceux de l'Etat et ceux mentionnés au 4° ainsi que les agents non statutaires des groupements d'intérêt public (...) ". <br/>
<br/>
              3. Pour l'application de ces dispositions, il appartient à l'autorité administrative, sous le contrôle du juge de l'excès de pouvoir, de déterminer si les circonstances dans lesquelles un contrat de travail à durée déterminée n'a pas été renouvelé permettent de l'assimiler à une perte involontaire d'emploi. A ce titre, et ainsi que le prévoit désormais le décret n° 2020-741 du 16 juin 2020, l'agent qui refuse le renouvellement de son contrat de travail ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime, qui peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle et sans justification par l'employeur.<br/>
<br/>
              4. En jugeant que les considérations personnelles invoquées par Mme B..., tirées de la nécessité d'assurer seule, en raison de la séparation récente d'avec son conjoint, la garde de ses deux jeunes enfants, dont un n'était pas scolarisé, et de son emménagement dans un nouveau domicile distant d'une vingtaine de kilomètres de son lieu de travail, ne constituaient pas un motif légitime de refus de renouvellement de son contrat de travail à durée déterminée pour une durée de trois mois, le tribunal a inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              5. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, Mme B... est fondée à demander l'annulation du jugement qu'elle attaque en tant que, par ce jugement, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Si Mme B... a fait part aux Hospices civils de Lyon de son intention de ne pas demander le renouvellement de son contrat expirant le 31 août 2016, il résulte de l'instruction que sa décision était fondée sur des considérations d'ordre personnel, tenant à sa séparation d'avec son conjoint, à son déménagement et aux nécessités de la garde de ses enfants. Dans les circonstances de l'espèce, ces considérations constituaient un motif légitime pour ne pas demander le renouvellement du contrat à durée déterminée d'une durée de trois mois qui la liait aux Hospices civils de Lyon. Il s'ensuit que Mme B... est fondée, sans qu'il soit besoin de se prononcer sur l'autre moyen assortissant sa demande, à demander l'annulation de la décision qu'elle attaque, qui repose sur le motif qu'elle ne justifiait pas d'un motif légitime de refus de renouvellement de son contrat, de sorte qu'elle ne pouvait être regardée comme ayant été involontairement privée d'emploi. <br/>
<br/>
              8. L'exécution de la présente décision implique que les Hospices civils de Lyon accordent à Mme B... le bénéfice de l'allocation d'aide au retour à l'emploi. Il y a lieu, par suite, d'enjoindre à l'établissement, dans un délai d'un mois à compter de la notification de la présente décision, de lui accorder le bénéfice de cette allocation et de déterminer son montant dans les conditions prévues par les textes en vigueur. Il n'y a pas lieu d'assortir cette injonction d'une astreinte.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme B..., qui n'est pas dans la présente instance la partie perdante, la somme que demandent les Hospices civils de Lyon à ce titre. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des Hospices civils de Lyon le versement à Mme B..., au même titre, d'une somme de 4 500 euros, au titre des frais exposés dans le cadre de la présente instance ainsi que devant le tribunal administratif de Lyon.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du tribunal administratif de Lyon du 21 décembre 2018 est annulé.<br/>
Article 2 : La décision de la directrice des Hospices civils de Lyon du 20 octobre 2016 est annulée.<br/>
Article 3 : Il est enjoint aux Hospices civils de Lyon d'accorder à Mme B... le bénéfice de l'allocation d'aide au retour à l'emploi et de déterminer son montant dans les conditions prévues par les textes en vigueur dans un délai d'un mois.<br/>
Article 4 : Les Hospices civils de Lyon verseront à Mme B... une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions des Hospices civils de Lyon présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A... B... et aux Hospices civils de Lyon. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-04 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. ALLOCATION POUR PERTE D'EMPLOI. - AGENT INVOLONTAIREMENT PRIVÉ D'EMPLOI (ART. L. 5424-1 DU CODE DU TRAVAIL) - 1) NOTION - EXCLUSION - AGENT QUI REFUSE LE RENOUVELLEMENT DE SON CDD, SAUF MOTIF LÉGITIME [RJ1] - 2) ESPÈCE - MOTIF LÉGITIME - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - AGENT INVOLONTAIREMENT PRIVÉ D'EMPLOI (ART. L. 5424-1 DU CODE DU TRAVAIL) - 1) NOTION - EXCLUSION - AGENT QUI REFUSE LE RENOUVELLEMENT DE SON CDD, SAUF MOTIF LÉGITIME [RJ1] - 2) ESPÈCE - MOTIF LÉGITIME - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-10-06-04 1) Pour l'application des articles L. 5421-1 et L. 5424-1 du code du travail, il appartient à l'autorité administrative, sous le contrôle du juge de l'excès de pouvoir, de déterminer si les circonstances dans lesquelles un contrat de travail à durée déterminée (CDD) n'a pas été renouvelé permettent de l'assimiler à une perte involontaire d'emploi.,,A ce titre, et ainsi que le prévoit désormais le décret n° 2020-741 du 16 juin 2020, l'agent qui refuse le renouvellement de son contrat de travail ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime, qui peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle et sans justification par l'employeur.,,2) Agent n'ayant pas demandé le renouvellement de son contrat pour des considérations d'ordre personnel, tenant à sa séparation d'avec son conjoint, à son déménagement et aux nécessités de la garde de ses enfants. Dans les circonstances de l'espèce, ces considérations constituent un motif légitime pour ne pas demander le renouvellement de son CDD d'une durée de trois mois.</ANA>
<ANA ID="9B"> 66-10-02 1) Pour l'application des articles L. 5421-1 et L. 5424-1 du code du travail, il appartient à l'autorité administrative, sous le contrôle du juge de l'excès de pouvoir, de déterminer si les circonstances dans lesquelles un contrat de travail à durée déterminée (CDD) n'a pas été renouvelé permettent de l'assimiler à une perte involontaire d'emploi.,,A ce titre, et ainsi que le prévoit désormais le décret n° 2020-741 du 16 juin 2020, l'agent qui refuse le renouvellement de son contrat de travail ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime, qui peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle et sans justification par l'employeur.,,2) Agent n'ayant pas demandé le renouvellement de son contrat pour des considérations d'ordre personnel, tenant à sa séparation d'avec son conjoint, à son déménagement et aux nécessités de la garde de ses enfants. Dans les circonstances de l'espèce, ces considérations constituent un motif légitime pour ne pas demander le renouvellement de son CDD d'une durée de trois mois.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 janvier 2003, Centre communal d'action sociale de Puyravault, n° 229251, T. pp. 837-1020. Rappr., s'agissant d'une démission, CE, 1er octobre 2001, Commune de Bouc-Bel-Air c/ Mme,, n° 215499, p. 451 ; s'agissant du refus de transformation d'un CDD en CDI, CE, 8 novembre 2019, Ministre de l'éducation nationale c/ Mme,, n° 408514, T. pp. 802-961-1057.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
