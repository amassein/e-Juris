<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029781243</ID>
<ANCIEN_ID>JG_L_2014_11_000000379945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/12/CETATEXT000029781243.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 19/11/2014, 379945, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379945.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 25 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le département du Haut-Rhin, représenté par le président de son conseil général ; le département demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-207 du 21 février 2014 portant délimitation des cantons dans le département du Haut-Rhin ainsi que la décision du 21 mars 2014 par laquelle le ministre de l'intérieur a rejeté son recours gracieux dirigé contre ce décret ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 90-1103 du 11 décembre 1990 ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le décret n° 2012-1479 du 27 décembre 2012<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013, prévoit que : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, en application de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département du Haut-Rhin, compte tenu de l'exigence de réduction de 31 à 17 du nombre des cantons de ce département résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aucune disposition législative ou réglementaire n'imposait au Premier ministre de mentionner dans le décret les motifs retenus pour la création des nouveaux cantons ainsi que les raisons pour lesquelles l'avis du conseil général n'avait pas été suivi et pour lesquelles des modifications avaient été apportées au projet soumis à l'assemblée départementale ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il est constant que le décret a été pris après avis du conseil général du Haut-Rhin, émis le 20 décembre 2013, après examen du projet transmis par le préfet au président du conseil général le 18 novembre 2013 ; qu'il ressort des pièces du dossier que le président du conseil général a été destinataire du projet de décret, d'un rapport de présentation générale rappelant les principes qui ont été retenus pour déterminer les nouvelles cartes cantonales, de cartes faisant apparaître les nouveaux cantons, les nouveaux cantons superposés aux anciens cantons, les nouveaux cantons et les limites des établissements publics de coopération intercommunale à compter du 1er janvier 2014, les cantons urbains pour les communes fractionnées ainsi que d'un tableau des nouveaux cantons mentionnant leur population et le nombre des communes et d'un tableau de répartition des communes par canton ; que ces éléments permettaient à l'assemblée départementale d'émettre un avis sur les modalités de mise en oeuvre de la nouvelle délimitation des cantons prévue par le législateur et de faire des propositions spécifiques pour le département, sans qu'il ait été nécessaire que des éléments relatifs aux " bassins de vie " et d'emploi, aux aires urbaines, à la géographie et à la topographie des nouveaux cantons aient été portés à la connaissance des conseillers généraux ; qu'est sans influence sur la régularité de cette consultation la circonstance que le conseil général n'aurait été ni informé des raisons pour lesquelles le pouvoir réglementaire proposait de rattacher certaines communes à certains cantons ou n'aurait pas tenu compte des propositions formulées par le président du conseil général ou d'autres élus, ni destinataire de ces dernières ; que, par suite, le moyen tiré de l'irrégularité de l'avis du conseil général doit être écarté ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des maires, élus ou présidents d'établissements publics de coopération intercommunale et à la consultation individuelle des conseillers généraux du département indépendamment de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales ; que, de même, aucune disposition législative ou réglementaire ne faisait obligation au Gouvernement de recourir aux " mécanismes de collaboration mis en place sur les territoires ruraux " et de procéder à une étude d'impact préalablement à l'édiction du décret attaqué ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              6. Considérant, en premier lieu, que, compte tenu des modalités de consultation qui ont été prévues par le législateur, le département du Haut-Rhin ne saurait utilement faire valoir que le  Gouvernement aurait commis une erreur manifeste d'appréciation en prenant le décret attaqué sans avoir préalablement consulté les maires, les présidents d'établissements publics de coopération intercommunale, les élus des communes et les conseillers généraux à titre individuel ;  <br/>
<br/>
              7. Considérant, en deuxième lieu, que l'article 7 de la loi du 11 décembre 1990 organisant la concomitance des renouvellements des conseils généraux et des conseils régionaux prévoit qu'il ne peut être procédé à aucune nouvelle délimitation des circonscriptions électorales dans l'année précédant l'échéance normale de renouvellement des assemblées concernées ; que l'article 51 de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral détermine les modalités d'entrée en vigueur de cette loi et prévoit, dans son premier alinéa, que le titre Ier de la loi, qui comprend les dispositions relatives au futur conseil départemental, " s'applique à compter du prochain renouvellement général des conseils généraux suivant la promulgation de la présente loi " ; que les dispositions des articles L. 191 et L. 191-1 du code électoral, dans leur version applicable à compter du prochain renouvellement général des conseils généraux suivant la promulgation de la loi du 17 mai 2013, prévoyant notamment que les électeurs de chaque canton du département élisent au conseil départemental deux membres de sexe différent et que le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier, impliquent qu'il soit procédé à une nouvelle délimitation de l'ensemble des circonscriptions cantonales, qui sera applicable à compter du prochain renouvellement général des conseils généraux, fixé, à la date de la présente décision, au mois de mars 2015 ;<br/>
<br/>
              8. Considérant qu'eu égard à l'échéance mentionnée au point 7, la délimitation des nouvelles circonscriptions cantonales devait, conformément aux dispositions de l'article 7 de la loi du 11 juillet 1990, être effectuée au plus tard un an avant le mois de mars 2015 ; que le décret, pris le 21 février 2014 et publié au Journal officiel le 25 février 2014, satisfait à ces prescriptions, alors même qu'il prévoit dans son article 19, conformément à l'article 51 de la loi du 17 mai 2013, qu'il n'entrera en vigueur qu'au prochain renouvellement général des assemblées départementales suivant sa publication ; que le décret attaqué ne concerne que la délimitation des nouvelles circonscriptions cantonales ; que, dès lors, le moyen tiré de ce qu'il aurait des répercussions sur la future campagne électorale nécessitant l'édiction de dispositions transitoires n'est pas fondé ;  <br/>
<br/>
              9. Considérant, en troisième lieu, qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale, les limites des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ou les " pays " au sens de la loi du 25 juin 1999 d'orientation de l'aménagement durable du territoire, pas plus qu'elles ne doivent respecter les limites des anciens cantons ; que ni la proximité géographique de l'ensemble des communes composant un canton ni l'absence de disparité de superficie entre cantons ne sont davantage des critères de délimitation des circonscriptions électorales définis à l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, le département ne saurait utilement soutenir que la nouvelle délimitation cantonale aurait méconnu ces différentes données ; <br/>
<br/>
              10. Considérant, en quatrième lieu, que le décret attaqué réduit sensiblement le rapport entre la population du canton le moins peuplé et celle du canton le plus peuplé, qui était de 1 à 5,16 et qui passe à 1 à 1,43 et que la population moyenne par canton s'établit à 44 105 habitants ; qu'en outre, et sans que le pouvoir réglementaire y ait été tenu, la nouvelle délimitation cantonale respecte en les rattachant à un seul canton le périmètre de vingt et un établissements de coopération intercommunale sur les vingt-trois du département dont la population est inférieure à la moyenne départementale ;  		<br/>
<br/>
              11.  Considérant que si le département soutient que le nouveau canton de Masevaux (n° 9) comprend plus de soixante communes qui n'ont pas de lien entre elles, il ressort des pièces du dossier que ce canton réunit les trois communautés de communes de la Vallée de la Doller et du Soultzach, de la région de Dannemarie et de la Largue ; que, si la communauté de communes des Trois frontières est scindée entre les cantons de Brunstatt (n° 2) et de Saint-Louis (n° 14), ce dernier canton, qui regroupe au demeurant les treize communes de la communauté de communes de la Porte du Sundgau situées dans l'aire d'influence de l'agglomération de Saint-Louis, ne pouvait comprendre l'intégralité de la communauté de communes des Trois frontières sans contrevenir aux exigences démographiques résultant des dispositions législatives ; que le nouveau canton de Saint-Louis (n° 14) reprend le périmètre du canton d'Huningue dont toutes les communes font partie du " bassin de vie " de la commune de Saint-Louis ; que les communes du " Pays de Saint-Louis ", territoire de projet au sens de la loi du 25 juin 1999, comprenant notamment les communautés de communes des Trois frontières, de la Porte du Sundgau et du Pays de Sierentz, devaient être rattachées à plusieurs cantons compte tenu de la population de ce territoire de près de 75 000 habitants, sans que le pouvoir réglementaire ait été tenu de ne les regrouper qu'au sein de deux cantons ; que le " Pays de Sundgau ", qui comprend environ 62 000 habitants, ne pouvait former deux cantons distincts ; qu'alors que le département revendique le rattachement des communes de Bartenheim et Kembs au canton de Saint-Louis, celles-ci, qui n'appartiennent pas au " bassin de vie " de Saint-Louis, ont été rattachées au canton de Brunstatt (n° 2) comme l'ensemble des communes de l'ancien canton de Sierentz ; que le rattachement de la commune de Soultzmatt-Wintzfelden au canton de Wintzenheim (n° 16), qui comprend notamment la commune d'Osenbach, se justifie par le fait que cette commune, d'une population inférieure à 3 500 habitants et qui comprend une enclave au nord-ouest de la commune d'Osenbach, ne pouvait être fractionnée mais devait être comprise dans un même canton, ce qui interdisait son rattachement au canton de Guebwiller (n°7) auquel appartiennent les autres communes de la communauté de communes de la Région de Guebwiller ; que la circonstance que cette enclave dans la commune d'Osenbach est constituée par une forêt dépourvue d'habitant n'est pas de nature à entacher le choix du pouvoir réglementaire d'une erreur manifeste d'appréciation, compte tenu notamment du critère de continuité territoriale ;  que le canton de Wintzenheim (n° 16) regroupe les communes composant les communautés de communes de la Vallée de Munster et du Pays de Rouffach ainsi que celles appartenant à la communauté d'agglomération de Colmar situées à l'ouest du canton de Colmar 1 (n° 4), en raison de la nécessité de diviser l'agglomération pour répondre aux exigences démographiques ; que la délimitation du nouveau canton de Sainte-Marie-aux Mines (n° 15) a été effectuée en fonction des périmètres des communautés de communes du Pays de Ribeauvillé, de la Vallée de Kaysersberg et du Val d'Argent ; qu'ainsi, et alors que d'autres délimitations auraient été possibles, ce qui est sans influence sur la légalité du décret attaqué, le moyen tiré de ce que la nouvelle carte cantonale serait arbitraire et reposerait sur une erreur manifeste d'appréciation doit être écarté ; <br/>
<br/>
              12. Considérant, en cinquième lieu,  que le moyen tiré de ce que la nouvelle délimitation cantonale serait entachée d'une erreur manifeste d'appréciation au motif que l'augmentation de la taille des cantons nuirait aux  liens entretenus par les élus cantonaux avec les électeurs, les maires et les élus intercommunaux ne peut qu'être écarté dès lors que sont respectées les règles posées par les dispositions législatives  ;<br/>
<br/>
              13. Considérant, en sixième lieu, que la circonstance que la nouvelle délimitation cantonale serait de nature à fragiliser le fonctionnement de l'administration locale n'est pas susceptible d'entacher le décret attaqué d'une erreur manifeste d'appréciation ;<br/>
<br/>
              14. Considérant, en septième lieu, ainsi qu'il a été dit au point 9, que la modification des limites territoriales des cantons doit répondre aux règles prévues au III et au IV de l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, le département ne peut utilement invoquer les circonstances que les zones rurales seraient, du fait de la mise en oeuvre de ces règles, qui n'est pas contestée, moins bien représentées au sein de l'assemblée départementale que les zones urbaines ; que le moyen tiré de ce que les cantons correspondant aux aires urbaines comporteraient une population inférieure à la moyenne départementale, alors que la population des cantons ruraux serait supérieure à cette moyenne, est dépourvu des précisions permettant d'en apprécier le bien-fondé, de même que le moyen selon lequel la nouvelle carte cantonale conduirait à un déséquilibre entre cantons ruraux ; que si la population du nouveau canton de Guebwiller (n° 7) de 36 558 habitants est inférieure de 17,11 % à la moyenne départementale, cet écart est justifié par la définition du canton en fonction du périmètre de la communauté de communes de la Région de Guebwiller, duquel a dû être soustraite la commune de Soultzmatt-Wintzfelden, pour des motifs précisés au point 11 ; que le nouveau canton de Wintzenheim (n° 16) compte 49 387 habitants et présente ainsi un écart de faible ampleur par rapport à la moyenne des cantons ; que, dès lors, le décret n'est pas entaché d'erreur manifeste d'appréciation ;<br/>
<br/>
              15. Considérant, en huitième lieu, que pour l'application des critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales, il appartenait au pouvoir réglementaire, pour la prise en compte des bases démographiques, de retenir les chiffres de population constatés à partir des données du décret du 27 décembre 2012 authentifiant les chiffres de population de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, et non de simples prévisions ; qu'ainsi, les circonstances que le canton de Cernay (n° 3) compte plus de 50 000 habitants, alors que ses perspectives d'évolution démographique seraient supérieures à celles des cantons de Colmar 1 et 2, et que la population du canton de Saint-Louis (n° 14) devrait connaître une forte croissance, alors qu'il comporte 52 298 habitants, sont sans influence sur la légalité du décret attaqué ; <br/>
<br/>
              16. Considérant, enfin, que les décrets de révision des cartes cantonales définissent des bureaux centralisateurs et non des chefs-lieux de canton ; que, par suite, la circonstance que la perte de la qualité de chef-lieu serait susceptible d'entraîner un préjudice de notoriété et la perte de la dotation de solidarité rurale pour les communes d'Illzach, de Soultz et de Habsheim  est sans incidence sur la légalité du décret attaqué ; qu'alors qu'il est constant que le choix des bureaux centralisateurs s'est porté sur les communes les plus peuplées de chaque canton et que lorsque le périmètre d'un canton reste inchangé ou diffère peu du précédent, le bureau centralisateur correspond à l'actuel chef-lieu, le moyen tiré de ce que le pouvoir réglementaire aurait commis une erreur manifeste d'appréciation en retenant comme bureaux centralisateurs certaines communes excentrées ne peut qu'être écarté ;<br/>
<br/>
              17. Considérant qu'il résulte de ce qui précède que la requête du département du Haut-Rhin doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
<br/>
Article  1er : La requête du département du Haut-Rhin  est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au département du Haut-Rhin et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
