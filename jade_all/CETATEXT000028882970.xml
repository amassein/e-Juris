<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028882970</ID>
<ANCIEN_ID>JG_L_2014_04_000000371820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/88/29/CETATEXT000028882970.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 28/04/2014, 371820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:371820.20140428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 septembre et 2 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) à titre principal, de renvoyer l'affaire devant la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler le jugement n° 1108825/14 et 1108826/14 du 1er juillet 2013 par lequel le tribunal administratif de Melun a rejeté ses demandes tendant, d'une part, à l'annulation de la délibération du 27 septembre 2011 par laquelle le conseil municipal de La Rochette a supprimé le poste d'adjoint d'animation de 2ème classe dans les emplois municipaux, d'autre part, à l'annulation de la décision du 21 octobre 2011 par laquelle le maire de La Rochette l'a placée en surnombre à compter du 1er octobre 2011 ;<br/>
<br/>
              3°) de mettre à la charge de la commune de La Rochette le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant que, par une première demande, Mme A...a demandé au tribunal administratif de Melun l'annulation pour excès de pouvoir de l'arrêté du 21 octobre 2011 par lequel le maire de La Rochette l'a maintenue en surnombre pendant un an à compter du 1er octobre 2011 ; que, par une seconde demande, elle a demandé au tribunal administratif l'annulation pour excès de pouvoir de la délibération du 27 septembre 2011 du conseil municipal de La Rochette en tant que cette délibération a supprimé l'emploi d'adjoint d'animation de 2ème classe ; que, par un jugement en date du 1er juillet 2013 que Mme A...conteste par la voie du recours en cassation, le tribunal administratif de Melun, après avoir joint les deux demandes, a rejeté l'ensemble des conclusions présentées par la requérante ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, dans sa rédaction applicable à la date du jugement attaqué, combinées avec celles de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics, à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code ; qu'une demande tendant à l'annulation pour excès de pouvoir d'un acte à caractère réglementaire relatif à l'organisation du service n'est pas au nombre des litiges relatifs à la situation individuelle des agents publics visés au deuxième alinéa de l'article R. 811-1 ;  <br/>
<br/>
              3. Considérant qu'il suit de ce qui a été dit au point précédent que le litige relatif à la délibération du 27 septembre 2011, laquelle présente le caractère d'un acte réglementaire d'organisation du service, n'entre pas dans le champ d'application des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative dans sa version en vigueur à la date à laquelle le jugement a été rendu ; que le jugement attaqué n'a, par suite et dans cette mesure, pas été rendu en dernier ressort ; que les conclusions dirigées contre cette partie du jugement présentent ainsi le caractère d'un appel qui ressortit à la compétence de la cour administrative d'appel de Paris ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, et alors même que les demandes de Mme A...ont été jointes par le tribunal administratif, qu'il y a lieu d'attribuer à la cour administrative d'appel de Paris le jugement des conclusions de l'intéressée tendant à l'annulation du jugement du 1er juillet 2013 en tant qu'il a statué sur sa seconde demande ; que les conclusions de Mme A...dirigées contre le jugement en tant qu'il a statué sur sa première demande ont, en revanche, le caractère d'un pourvoi en cassation qui relève de la compétence du Conseil d'Etat ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              6. Considérant que pour demander l'annulation du jugement attaqué en tant qu'il a statué sur les conclusions de sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 21 octobre 2011 par lequel le maire de La Rochette l'a maintenue en surnombre pendant un an à compter du 1er octobre 2011, Mme A...soutient que le juge a insuffisamment motivé sa décision, faute d'avoir répondu aux moyens soulevés par la requérante et tirés, d'une part, de l'absence d'examen des possibilités de reclassement avant la suppression du poste et le placement de l'agent en surnombre, d'autre part, de ce que la décision de placement en surnombre était entachée d'erreur manifeste d'appréciation ; que le juge a dénaturé et inexactement qualifié les faits en refusant de considérer que les décisions contestées constituaient des sanctions disciplinaires déguisées ; que le tribunal a commis une erreur de droit au regard des règles relatives à la dévolution de la charge de la preuve en imposant à Mme A... de faire la preuve des agissements de harcèlement moral dont elle avait été victime, alors qu'il suffisait à cette dernière d'apporter des éléments de fait susceptibles de faire présumer l'existence d'un tel harcèlement ; que le tribunal a dénaturé et inexactement qualifié les faits en jugeant que Mme A...n'avait pas été victime d'agissements constitutifs de harcèlement moral ; que le jugement est enfin entaché de dénaturation des pièces du dossier, faute d'avoir annulé la décision la plaçant en surnombre par voie de conséquence de l'erreur manifeste d'appréciation qui affectait la décision de suppression de son emploi ; <br/>
<br/>
              7. Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi, en tant qu'il conteste le jugement du 1er juillet 2013 en tant qu'il a statué sur l'arrêté du 21 octobre 2011 ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de Mme A...dirigées contre le jugement du tribunal administratif de Melun du 1er juillet 2013 en tant qu'il a rejeté sa demande enregistrée sous le n° 1108826 est attribué à la cour administrative d'appel de Paris.<br/>
Article 2 : Le pourvoi de Mme A...dirigé contre le jugement du tribunal administratif de Melun du 1er juillet 2013 en tant qu'il a rejeté sa demande enregistrée sous le n° 1108825 n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au président de la cour administrative d'appel de Paris.<br/>
Copie en sera adressée pour information à la commune de La Rochette.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
