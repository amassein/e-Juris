<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029476917</ID>
<ANCIEN_ID>JG_L_2014_09_000000362568</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/47/69/CETATEXT000029476917.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 19/09/2014, 362568</TITRE>
<DATE_DEC>2014-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362568</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362568.20140919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 septembre et 7 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Ortec Méca, dont le siège est Parc de Pichaury, 550 rue Pierre Berthier, à Aix-en-Provence (13799), représentée par son président directeur général en exercice ; la société Ortec Méca demande au Conseil d'Etat : <br/>
<br/>
              1° d'annuler l'arrêt n° 11MA01186 du 10 juillet 2012 par lequel la cour administrative d'appel de Marseille a, sur appel de M. A...B..., annulé le jugement n° 0808647 du 25 janvier 2011 du tribunal administratif de Marseille et la décision du 9 octobre 2008 par laquelle le ministre du travail, des relations sociales, de la famille et de la solidarité a autorisé le licenciement de M. B...; <br/>
<br/>
              2° de mettre à la charge de M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la société Ortec Méca et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. " ; que ce délai est un délai franc ; qu'en vertu de la règle rappelée à l'article 642 du code de procédure civile, un délai qui expirerait normalement un samedi, un dimanche ou un jour férié ou chômé, est prorogé jusqu'au premier jour ouvrable suivant ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 2422-1 du code du travail, qui est relatif aux modalités d'exercice d'un recours hiérarchique contre une décision de l'inspecteur du travail statuant sur une demande d'autorisation de licencier un salarié protégé : " Le ministre chargé du travail peut annuler ou réformer la décision de l'inspecteur du travail sur le recours de l'employeur, du salarié ou du syndicat que ce salarié représente ou auquel il a donné mandat à cet effet./ Ce recours est introduit dans un délai de deux mois à compter de la notification de la décision de l'inspecteur. (...) " ;<br/>
<br/>
              3. Considérant qu'en tant qu'elles fixent un délai au recours hiérarchique formé contre une décision de l'inspecteur du travail statuant sur une demande d'autorisation de licencier un salarié protégé, les dispositions de l'article R. 2422-1 du code du travail ont entendu se référer au délai de recours contentieux et à la règle générale du contentieux administratif selon laquelle un recours gracieux ou hiérarchique contre une décision administrative doit être exercé avant l'expiration du délai de recours contentieux pour interrompre ce délai ; que, par suite, le délai de deux mois mentionné à l'article R. 2422-1 du code du travail est un délai franc qui, s'il expire un samedi, un dimanche ou un jour férié ou chômé, est prorogé jusqu'au premier jour ouvrable suivant ; <br/>
<br/>
              4. Considérant que, par suite, la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant que le délai applicable au recours administratif prévu à l'article R. 2422-1 du code du travail était un délai de deux mois non franc auquel n'était pas applicable la règle rappelée à l'article 642 du code de procédure civile ; qu'il y a lieu, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, d'annuler son arrêt ;<br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme que demande la société Ortec Méca au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette société qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 10 juillet 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Ortec Méca et à M. A...B.... <br/>
Copie en sera adressée, pour information, au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - DÉLAI DE DEUX MOIS FIXÉ PAR UN TEXTE À UN RECOURS ADMINISTRATIF NE CONSTITUANT PAS UN PRÉALABLE OBLIGATOIRE À UN RECOURS CONTENTIEUX - DÉLAI AYANT ENTENDU SE RÉFÉRER AU DÉLAI DE RECOURS CONTENTIEUX - CONSÉQUENCES A) DÉLAI FRANC - EXISTENCE - B) PROROGATION AU PREMIER JOUR OUVRABLE SUIVANT S'IL EXPIRE UN SAMEDI, UN DIMANCHE OU JOUR FÉRIÉ OU CHÔMÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. - DÉLAI DE DEUX MOIS DU RECOURS HIÉRARCHIQUE CONTRE LA DÉCISION DE L'INSPECTEUR DU TRAVAIL PRÉVU PAR L'ARTICLE R. 2422-1 DU CODE DU TRAVAIL - DÉLAI AYANT ENTENDU SE RÉFÉRER AU DÉLAI DE RECOURS CONTENTIEUX - CONSÉQUENCES - A) DÉLAI FRANC - EXISTENCE - B) PROROGATION AU PREMIER JOUR OUVRABLE SUIVANT S'IL EXPIRE UN SAMEDI, UN DIMANCHE OU JOUR FÉRIÉ OU CHÔMÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-02-01 Le délai de deux mois prévu par un texte pour l'exercice d'un recours administratif ne constituant pas un préalable obligatoire au recours contentieux doit être entendu comme se référant au délai de recours contentieux dans lequel ce recours administratif doit être exercé pour interrompre le délai de recours contentieux. a) Il s'agit donc d'un délai franc. b) Ce délai, s'il expire un samedi, un dimanche ou un jour férié ou chômé, est prorogé jusqu'au premier jour ouvrable suivant.</ANA>
<ANA ID="9B"> 66-07-01 Le délai de deux mois prévu par un texte pour l'exercice d'un recours administratif ne constituant pas un préalable obligatoire au recours contentieux doit être entendu comme se référant au délai de recours contentieux dans lequel ce recours administratif doit être exercé pour interrompre le délai de recours contentieux. a) Il s'agit donc d'un délai franc. b) Ce délai, s'il expire un samedi, un dimanche ou un jour férié ou chômé, est prorogé jusqu'au premier jour ouvrable suivant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
