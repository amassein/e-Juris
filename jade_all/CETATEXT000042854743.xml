<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854743</ID>
<ANCIEN_ID>JG_L_2020_12_000000438295</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2020, 438295, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438295</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438295.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nice de condamner le centre hospitalier régional universitaire (CHRU) de Nice à lui verser la somme provisionnelle de 200 000 euros, en réparation des préjudices qu'il estime avoir subis du fait d'une intervention chirurgicale réalisée le 21 avril 2000, ainsi que la somme de 20 000 euros en réparation de son préjudice d'impréparation et de prescrire une mesure d'expertise afin d'évaluer l'ensemble de ses préjudices. Par un jugement n° 1501996 du 12 décembre 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18MA00447 du 4 juillet 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 février et 26 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du CHRU de Nice la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n°2002-303 du 4 mars 2002 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A... et à Me Le Prado, avocat du centre hospitalier régional universitaire de Nice.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a fait l'objet le 21 avril 2000 au centre hospitalier régional universitaire (CHRU) de Nice d'une intervention par radiothérapie stéréotaxique qui a provoqué une radionécrose cérébrale dont il conserve des séquelles. Il demande l'annulation de l'arrêt du 4 juillet 2019 par lequel la cour administrative d'appel de Marseille a rejeté ses conclusions tendant à ce que le CHRU de Nice soit condamné à lui verser à titre provisionnel une somme de 200 000 euros, à ce qu'il soit condamné à l'indemniser de son préjudice d'impréparation et à ce qu'une nouvelle expertise portant sur le montant de ses préjudices soit ordonnée. <br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur la responsabilité sans faute du service public hospitalier :<br/>
<br/>
              2. Lorsque, antérieurement à l'entrée en vigueur de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, un acte médical nécessaire au diagnostic ou au traitement du malade présentait un risque dont l'existence était connue, mais dont la réalisation était exceptionnelle et dont aucune raison ne permettait de penser que le patient y serait particulièrement exposé, la responsabilité du service public hospitalier était engagée si l'exécution de cet acte était la cause directe de dommages sans rapport avec l'état initial du patient comme avec l'évolution prévisible de cet état, et présentait un caractère d'extrême gravité.<br/>
<br/>
              3. La cour administrative d'appel de Marseille, qui n'a pas dénaturé les pièces du dossier en retenant que, dans les circonstances propres à l'espèce, le taux de survenance du risque de radionécrose cérébrale qui s'est réalisé lors de l'opération du 21 avril 2000 était d'au moins 3 %, n'a pas commis d'erreur de droit en en déduisant que sa réalisation n'était pas exceptionnelle au sens du principe rappelé ci-dessus.<br/>
<br/>
              4. Il résulte du même principe qu'en l'absence de caractère exceptionnel de la réalisation du risque médical, la responsabilité du service hospitalier ne peut pas être reconnue sur son fondement. Ainsi, le moyen tiré de ce que la cour aurait dénaturé les pièces du dossier et commis une erreur de droit en jugeant que les dommages subis par M. A... n'étaient pas sans rapport avec son état initial ou avec l'évolution prévisible de cet état doit être regardé comme dirigé contre une partie surabondante de l'arrêt attaqué. Il est, par suite, inopérant.<br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur l'obligation d'information :<br/>
<br/>
              5. En estimant que, même si aucun document signé par M. A... n'attestait la délivrance, par l'établissement de santé, d'une information préalable sur les risques liés à la réalisation d'une intervention de radiothérapie stéréotaxique, la preuve de cette délivrance était apportée par les autres pièces du dossier, notamment par le rapport d'expertise dont il résultait que M. A... avait été informé de ce risque lors de la consultation pré-opératoire du 11 avril 2000, la cour s'est livrée à une appréciation souveraine et exempte de dénaturation des pièces du dossier et n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, ainsi que, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au centre hospitalier régional universitaire de Nice.<br/>
Copie en sera adressée à la Société hospitalière d'assurances mutuelles et à la caisse primaire d'assurance maladie des Alpes-Maritimes. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
