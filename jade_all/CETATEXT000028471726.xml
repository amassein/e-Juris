<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028471726</ID>
<ANCIEN_ID>JG_L_2014_01_000000362495</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/47/17/CETATEXT000028471726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 15/01/2014, 362495, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362495</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362495.20140115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 septembre et 6 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour La Poste SA, dont le siège est au 44 boulevard de Vaugirard à Paris Cedex 15 (75757) ; La Poste SA demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11DA00971 du 28 juin 2012 de la cour administrative d'appel de Douai en tant qu'il a, à la demande de M. A...B..., d'une part, annulé le jugement n°s 0901458-0903377 du 13 avril 2011 du tribunal administratif de Lille en ce qu'il a rejeté les conclusions de M. B...tendant à l'annulation de la décision du 30 décembre 2008 par laquelle la Poste lui a infligé la sanction d'exclusion temporaire de fonctions pour une durée de deux ans, d'autre part, annulé pour excès de pouvoir cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de La Poste, et à la SCP Laugier, Caston, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., entré au service de la Poste le 20 avril 1977, a été promu aux fonctions de chef d'équipe au centre de tri de Lille-Moulins le 1er février 1995 ; que, par une décision du 30 décembre 2008, le directeur des opérations des ressources humaines de La Poste a prononcé à l'encontre de l'intéressé une sanction d'exclusion temporaire de fonctions pour une durée de deux ans, au motif qu'il avait eu un comportement inadapté et équivoque à l'égard d'agents féminins placés sous son autorité, constitutif d'un harcèlement moral et sexuel ; que, par un jugement en date du  13 avril 2011, le tribunal administratif de Lille a rejeté les demandes de M. B... tendant, d'une part, à l'annulation de cette décision et, d'autre part, à ce que La Poste soit condamnée à lui verser la somme de 5 990.85 euros en réparation de ses préjudices matériels et moraux ; que la Poste se pourvoit contre l'arrêt du 28 juin 2012, par lequel la cour administrative d'appel de Douai a annulé ce jugement et condamné La Poste à verser à M. B... une somme de 500 euros, en réparation du préjudice moral né du prolongement illégal, au-delà de quatre mois, de la suspension prononcée à son encontre ; que la requérante ne conteste, cependant, cet arrêt qu'en tant qu'il a annulé sa décision du 30 septembre 2008 prononçant l'exclusion temporaire de M. B...; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article 6 ter de la loi du 13 juillet 1983 portant droits et obligation des fonctionnaires, dans sa rédaction alors en vigueur : " Aucune mesure concernant notamment le recrutement, la titularisation, la formation, la notation, la discipline, la promotion, l'affectation et la mutation ne peut être prise à l'égard d'un fonctionnaire en prenant en considération : 1° Le fait qu'il a subi ou refusé de subir les agissements de harcèlement de toute personne dont le but est d'obtenir des faveurs de nature sexuelle à son profit ou au profit d'un tiers ; 2° Le fait qu'il a formulé un recours auprès d'un supérieur hiérarchique ou engagé une action en justice visant à faire cesser ces agissements ; 3° Ou bien le fait qu'il a témoigné de tels agissements ou qu'il les a relatés./ Est passible d'une sanction disciplinaire tout agent ayant procédé ou enjoint de procéder aux agissements définis ci-dessus./ Les dispositions du présent article sont applicables aux agents non titulaires de droit public " ; qu'il résulte de ces dispositions que des propos, ou des comportements à connotation sexuelle, répétés ou même, lorsqu'ils atteignent un certain degré de gravité, non répétés, tenus dans le cadre ou à l'occasion du service, non désirés par celui ou celle qui en est le destinataire et ayant pour objet ou pour effet soit de porter atteinte à sa dignité, soit, notamment lorsqu'ils sont le fait d'un supérieur hiérarchique ou d'une personne qu'elle pense susceptible d'avoir une influence sur ses conditions de travail ou le déroulement de sa carrière, de créer à l'encontre de la victime, une situation intimidante, hostile ou offensante sont constitutifs de harcèlement sexuel et, comme tels, passibles d'une sanction disciplinaire ;  <br/>
<br/>
              3.	Considérant que la cour administrative d'appel de Douai a relevé qu'il ressortait des pièces du dossier et notamment des différents témoignages d'agents ayant côtoyé M. B...dans ses fonctions de chef d'équipe que ce dernier s'était comporté de manière très familière avec plusieurs agents féminins placés sous son autorité ; qu'en particulier, l'un de ces agents, affecté au guichet, avait fait l'objet d'attentions particulières et subi des propos et des gestes déplacés et réitérés malgré ses refus, sur une période de plus de dix ans, qui n'avait été interrompue que par un congé parental pris par cet agent ; <br/>
<br/>
              4.	Considérant qu'il résulte des dispositions précitées de l'article 6 ter de la loi du 13 juillet 1983 que la cour administrative d'appel de Douai ne pouvait juger, sans commettre une erreur de qualification juridique, que quoique fautifs, ces faits, dont elle estimait la réalité établie, n'étaient pas constitutifs de harcèlement sexuel ; que, dès lors et sans qu'il soit besoin de répondre aux autres moyens du pourvoi, il y a lieu d'y faire droit et d'annuler l'arrêt attaqué en tant qu'il prononce l'annulation de la sanction infligée à M. B...; <br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6.	Considérant qu'il ressort, en premier lieu, des pièces du dossier que M. B... a été régulièrement informé de l'ouverture d'une procédure disciplinaire à son encontre ; qu'il a consulté, à trois reprises, son dossier administratif et a pu réunir des témoignages en sa faveur ; qu'il n'est pas établi que les procès verbaux des auditions et le déroulement de ces dernières seraient entachés d'irrégularités ; que M. B...n'est, dès lors, pas fondé à soutenir que la procédure disciplinaire suivie à son encontre serait irrégulière ; <br/>
<br/>
              7.	Considérant, en second lieu, qu'il ressort également des pièces du dossier et, notamment, des témoignages de différentes personnes et de celui du médecin de prévention de La Poste, que M.B..., chef d'équipe affecté à un centre de tri, a eu, à l'égard de plusieurs des agents féminins placés sous son autorité, un comportement indécent persistant, malgré une première mise en garde dans son précédent poste ; qu'il a, en particulier, tenu des propos déplacés visant à obtenir des faveurs sexuelles, accompagnés de gestes de privauté, à l'un de ces agents, affecté au guichet, qu'il a renouvelés durant une longue période et qui ont attiré sur elle, en raison de ses refus réitérés, les moqueries de ses collègues devant des clients de l'agence ; que le rapport du médecin de prévention, établi dans le cadre de la procédure d'enquête, fait état de la souffrance de l'intéressée, ainsi que du malaise de deux anciennes guichetières, ayant subi les mêmes comportements lors de leur prise de fonction dans ce bureau de poste ; que ces faits sont constitutifs de harcèlement sexuel, au sens des dispositions précitées de l'article 6 ter de la loi du 13 juillet 1983 ; que, dès lors, et  sans qu'il y ait lieu de statuer sur leur caractère  de harcèlement moral, ces faits sont de nature à justifier une sanction disciplinaire ; que compte tenu de la position hiérarchique de M.B..., de la gravité des faits qu'il a commis et de leur réitération, la sanction d'exclusion temporaire de fonctions pour une durée de deux années, proposée à l'unanimité du conseil de discipline, n'est pas  disproportionnée ; qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par La Poste, que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille a rejeté sa requête dirigée contre la décision du 30 décembre 2008 l'excluant de ses fonctions pour une durée de deux ans ; <br/>
<br/>
              8.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de La Poste, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par La Poste au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 28 juin 2012 est annulé en tant qu'il statue sur les conclusions de M. B...dirigées contre la décision du 30 décembre 2008 de La Poste.<br/>
<br/>
Article 2 : Les conclusions de la requête présentée par M. B...devant la cour administrative d'appel de Douai, en tant qu'elles sont dirigées contre la décision du 30 décembre 2008 de La Poste, sont rejetées, ainsi que ses conclusions présentées devant le Conseil d'Etat sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de La Poste est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée à La Poste SA et à M. A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DROITS ET OBLIGATIONS DES FONCTIONNAIRES (LOI DU 13 JUILLET 1983). - PROTECTION CONTRE LE HARCÈLEMENT SEXUEL (ART. 6 TER DE LA LOI DU 13 JUILLET 1983) - NOTION DE HARCÈLEMENT SEXUEL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-09-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. MOTIFS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. - HARCÈLEMENT SEXUEL (ART. 6 TER DE LA LOI DU 13 JUILLET 1983) - NOTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-13 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. - NOTION DE HARCÈLEMENT SEXUEL (ART. 6 TER DE LA LOI DU 13 JUILLET 1983) - CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE DES FAITS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - NOTION DE HARCÈLEMENT SEXUEL (ART. 6 TER DE LA LOI DU 13 JUILLET 1983) - CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE DES FAITS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 36-07-01-01 Il résulte des dispositions de l'article 6 ter de la loi n° 83-634 du 13 juillet 1983, dans sa rédaction issue de la loi n° 2005-843 du 26 juillet 2005, que sont constitutifs de harcèlement sexuel des propos ou des comportements à connotation sexuelle, répétés ou même, lorsqu'ils atteignent un certain degré de gravité, non répétés, tenus dans le cadre ou à l'occasion du service, non désirés par celui ou celle qui en est le destinataire et ayant pour objet ou pour effet soit de porter atteinte à sa dignité, soit, notamment lorsqu'ils sont le fait d'un supérieur hiérarchique ou d'une personne qu'elle pense susceptible d'avoir une influence sur ses conditions de travail ou le déroulement de sa carrière, de créer à l'encontre de la victime, une situation intimidante, hostile ou offensante.</ANA>
<ANA ID="9B"> 36-09-03-01 Il résulte des dispositions de l'article 6 ter de la loi n° 83-634 du 13 juillet 1983, dans sa rédaction issue de la loi n° 2005-843 du 26 juillet 2005, que sont constitutifs de harcèlement sexuel et, comme tels, passibles d'une sanction disciplinaire, des propos, ou des comportements à connotation sexuelle, répétés ou même, lorsqu'ils atteignent un certain degré de gravité, non répétés, tenus dans le cadre ou à l'occasion du service, non désirés par celui ou celle qui en est le destinataire et ayant pour objet ou pour effet soit de porter atteinte à sa dignité, soit, notamment lorsqu'ils sont le fait d'un supérieur hiérarchique ou d'une personne qu'elle pense susceptible d'avoir une influence sur ses conditions de travail ou le déroulement de sa carrière, de créer à l'encontre de la victime, une situation intimidante, hostile ou offensante.</ANA>
<ANA ID="9C"> 36-13 Le juge de cassation exerce un contrôle de qualification juridique des faits sur la notion de harcèlement sexuel figurant à l'article 6 ter de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires.</ANA>
<ANA ID="9D"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique des faits sur la notion de harcèlement sexuel figurant à l'article 6 ter de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour le harcèlement moral, CE, 30 décembre 2011, Commune de Saint-Perray n° 332366, T. pp. 991-1109.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
