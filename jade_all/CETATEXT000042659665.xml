<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659665</ID>
<ANCIEN_ID>JG_L_2020_12_000000437983</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 08/12/2020, 437983</TITRE>
<DATE_DEC>2020-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437983</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437983.20201208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sogetra a demandé au juge des référés du tribunal administratif de la Guadeloupe, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser, à titre de provision, la somme de 107 504,92 euros, assortie des intérêts au taux légal eux-mêmes capitalisés, en paiement du décompte général définitif d'un marché conclu le 5 novembre 2015 relatif à la réalisation de travaux de construction d'unités éducatives et d'un logement de fonction au sein de l'établissement de placement éducatif d'insertion du Lamentin. Par une ordonnance n° 1900349 du 28 juin 2019, le juge des référés du tribunal administratif de la Guadeloupe a condamné l'Etat à verser à la société Sogetra la somme de 47 882,10 euros à titre de provision.<br/>
<br/>
              Par une ordonnance n°s 19BX02625, 19BX02664 du 12 décembre 2019, le juge des référés de la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Sogetra contre cette ordonnance ainsi que l'appel incident formé par la garde des sceaux, ministre de la justice.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 janvier, 11 février et 11 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Sogetra demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - le décret n° 2013-269 du 29 mars 2013 ;<br/>
              - l'arrêté du 8 septembre 2009 modifié portant approbation du cahier des clauses administratives générales applicables aux marchés publics de travaux ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de la société Sogetra ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que le préfet de la Guadeloupe a, par un acte d'engagement signé le 27 octobre 2015 et notifié le 5 novembre 2015, confié à la société Dodin Guadeloupe, désignée comme mandataire du groupement solidaire au sein duquel figure notamment la société requérante Sogetra, la réalisation de travaux de construction au sein de l'établissement de placement éducatif d'insertion dans la commune du Lamentin. La société Sogetra a demandé au juge des référés du tribunal administratif de la Guadeloupe, statuant sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser, à titre de provision, la somme de 107 504,92 euros, assortie des intérêts au taux légal eux-mêmes capitalisés, en paiement du décompte général définitif de ce marché public. Par une ordonnance du 28 juin 2019, le juge des référés du tribunal administratif de la Guadeloupe a condamné l'Etat à verser à la société Sogetra la somme de 47 882,10 euros à titre de provision et rejeté le surplus des demandes. Par l'ordonnance attaquée du 12 décembre 2019, le juge des référés de la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Sogetra et l'appel incident formé par le garde des sceaux, ministre de la justice, contre cette ordonnance.<br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice :<br/>
<br/>
              2. Les délais supplémentaires de distance prévus par les articles R. 421-7 et R. 811-5 du code de justice administrative sont, en vertu de l'article R. 821-2, applicables aux recours en cassation. Il ressort des pièces du dossier que la société requérante, domiciliée à la Guadeloupe, a introduit le 27 janvier 2020 son pourvoi à l'encontre de la décision attaquée, dont elle a reçu notification le 12 décembre 2019. Elle a ainsi régulièrement introduit son pourvoi dans les délais, augmentés du délai de distance prévu par ces articles. Par suite, la fin de non-recevoir soulevée par le garde des sceaux, ministre de la justice et des libertés ne peut qu'être écartée.<br/>
<br/>
              Sur les moyens du pourvoi :<br/>
<br/>
              3. Aux termes de l'article 13.3.1 du cahier des clauses administratives générales (CCAG) Travaux, dans sa rédaction issue de l'arrêté du 3 mars 2014 : " Après l'achèvement des travaux, le titulaire établit le projet de décompte final, concurremment avec le projet de décompte mensuel afférent au dernier mois d'exécution des prestations ou à la place de ce dernier. / Ce projet de décompte final est la demande de paiement finale du titulaire, établissant le montant total des sommes auquel le titulaire prétend du fait de l'exécution du marché dans son ensemble, son évaluation étant faite en tenant compte des prestations réellement exécutées (...) ". Selon l'article 13.3.2 : " Le titulaire transmet son projet de décompte final, simultanément au maître d'oeuvre et au représentant du pouvoir adjudicateur, par tout moyen permettant de donner une date certaine, dans un délai de trente jours à compter de la date de notification de la décision de réception des travaux telle qu'elle est prévue à l'article 41.3 ou, en l'absence d'une telle notification, à la fin de l'un des délais de trente jours fixés aux articles 41.1.3 et 41.3. / Toutefois, s'il est fait application des dispositions de l'article 41.5, la date du procès-verbal constatant l'exécution des travaux visés à cet article est substituée à la date de notification de la décision de réception des travaux comme point de départ des délais ci-dessus. / S'il est fait application des dispositions de l'article 41.6, la date de notification de la décision de réception des travaux est la date retenue comme point de départ des délais ci-dessus ". Aux termes de l'article 41.5 : " S'il apparaît que certaines prestations prévues par les documents particuliers du marché et devant encore donner lieu à règlement n'ont pas été exécutées, le maître de l'ouvrage peut décider de prononcer la réception, sous réserve que le titulaire s'engage à exécuter ces prestations dans un délai qui n'excède pas trois mois. La constatation de l'exécution de ces prestations doit donner lieu à un procès-verbal dressé dans les mêmes conditions que le procès-verbal des opérations préalables à la réception prévu à l'article 41.2 ". Selon l'article 41.6 : " Lorsque la réception est assortie de réserves, le titulaire doit remédier aux imperfections et malfaçons correspondantes dans le délai fixé par le représentant du pouvoir adjudicateur ou, en l'absence d'un tel délai, trois mois avant l'expiration du délai de garantie défini à l'article 44.1 (...) ".<br/>
<br/>
              4. Il ressort des énonciations de l'ordonnance attaquée que le juge des référés a estimé que la réception avait été prononcée le 21 décembre 2017 sur le fondement de l'article 41.5 du CCAG Travaux relatif à la réception " sous réserve " de la réalisation de prestations non encore exécutées. En jugeant ainsi, alors qu'il ressort des pièces du dossier soumis au juge des référés que le garde des sceaux, ministre de la justice, soutenait dans ses écritures, sans être contredit sur ce point, que la réception avait été prononcée " avec réserves ", procédure prévue par l'article 41.6 de ce CCAG, et non " sous réserves ", et que le procès-verbal du 26 avril 2018, postérieur à la réception des travaux, levait un certain nombre des réserves émises lors de cette réception, le juge des référés a dénaturé les pièces du dossier. La société Sogetra est dès lors fondée, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              Sur les conclusions à fin de non-lieu partiel présentées par le garde des sceaux, ministre de la justice :<br/>
<br/>
              6. Le garde des sceaux, ministre de la justice, après avoir contesté, par la voie de l'appel incident, l'ordonnance du 28 juin 2019 du juge des référés du tribunal administratif de la Guadeloupe en tant qu'elle a condamné l'Etat à verser à la société Sogetra la somme de 47 882,10 euros, demande, dans le dernier état de ses écritures d'appel, qu'il soit jugé qu'il n'y a plus lieu de statuer sur les conclusions indemnitaires de la requête à hauteur de cette somme. Toutefois, s'il résulte de l'instruction que le ministre de la justice a versé la somme de 47 882,10 euros à la société Sogetra et à ses sous-traitants, cette société ne peut être regardée comme ayant obtenu entière satisfaction à hauteur de cette somme dès lors que ce versement a été effectué en exécution de l'ordonnance du juge des référés du tribunal administratif de la Guadeloupe. Par suite, les conclusions à fin de non-lieu partiel présentées par le garde des sceaux, ministre de la justice, doivent être regardées comme un désistement pur et simple de ses conclusions d'appel incident. Rien ne s'oppose à ce qu'il en soit donné acte.<br/>
<br/>
              Sur l'existence d'une obligation non sérieusement constable :<br/>
<br/>
              7. Aux termes de l'article 13.3.4 du CCAG Travaux, dans sa rédaction issue de l'arrêté du 3 mars 2014 : " En cas de retard dans la transmission du projet de décompte final et après mise en demeure restée sans effet, le maître d'oeuvre établit d'office le décompte final aux frais du titulaire. Ce décompte final est alors notifié au titulaire avec le décompte général tel que défini à l'article 13.4 ". Selon l'article 13.4.2 : " Le projet de décompte général est signé par le représentant du pouvoir adjudicateur et devient alors le décompte général. / Le représentant du pouvoir adjudicateur notifie au titulaire le décompte général à la plus tardive des deux dates ci-après:/ - trente jours à compter de la réception par le maître d'oeuvre de la demande de paiement finale transmise par le titulaire ; / - trente jours à compter de la réception par le représentant du pouvoir adjudicateur de la demande de paiement finale transmise par le titulaire (...) ". Aux termes de l'article 13.4.3 : " Dans un délai de trente jours compté à partir de la date à laquelle ce décompte général lui a été notifié, le titulaire envoie au représentant du pouvoir adjudicateur, avec copie au maître d'oeuvre, ce décompte revêtu de sa signature, avec ou sans réserves, ou fait connaître les motifs pour lesquels il refuse de le signer. / Si la signature du décompte général est donnée sans réserve par le titulaire, il devient le décompte général et définitif du marché. La date de sa notification au pouvoir adjudicateur constitue le départ du délai de paiement. / Ce décompte lie définitivement les parties (...) ". Selon l'article 13.4.4 : " Si le représentant du pouvoir adjudicateur ne notifie pas au titulaire le décompte général dans les délais stipulés à l'article 13.4.2, le titulaire notifie au représentant du pouvoir adjudicateur, avec copie au maître d'oeuvre, un projet de décompte général signé (...). / Si, dans [un] délai de dix jours, le représentant du pouvoir adjudicateur n'a pas notifié au titulaire le décompte général, le projet de décompte général transmis par le titulaire devient le décompte général et définitif (...) ".<br/>
<br/>
              8. Il résulte des stipulations citées aux points 3 et 7 que, lorsque le pouvoir adjudicateur entend prononcer la réception en faisant application des dispositions de l'article 41.6 du CCAG Travaux relatives à la réception avec réserve des travaux, la date de notification de la décision de réception des travaux, et non la date de levée des réserves comme pour la réception sous réserves prévues par l'article 41-5 de ce CCAG, constitue le point de départ des délais prévus au premier alinéa de l'article 13.3.2, quelle que soit l'importance des réserves émises par le pouvoir adjudicateur. <br/>
<br/>
              9. Avant la date de notification de la décision de réception des travaux, le projet de décompte final qui serait adressé par le titulaire au pouvoir adjudicateur doit être regardé comme précocement transmis, en application de l'article 13.3.1, et ne peut faire courir le délai de trente jours prévu à l'article 13.4.2.<br/>
<br/>
              10. Après la date de notification de la décision de réception des travaux, il résulte de la combinaison des mêmes stipulations que, même si elle intervient après l'expiration du délai de trente jours prévu à l'article 13.3.2 du CCAG Travaux, courant à compter de la réception des travaux, la réception, par le maître d'ouvrage et le maître d'oeuvre, du projet de décompte final, établi par le titulaire du marché, est le point de départ du délai de trente jours prévu à l'article 13.4.2, dont le dépassement peut donner lieu à l'établissement d'un décompte général et définitif tacite dans les conditions prévues par l'article 13.4.4. <br/>
<br/>
              11. Il résulte de l'instruction que la société Dodin Guadeloupe, mandataire, a adressé le projet de décompte final du groupement le 23 avril 2018 au représentant du pouvoir adjudicateur et à la maîtrise d'oeuvre, lesquels l'ont reçu le 26 avril 2018. Cette transmission, intervenue après la réception des travaux du 21 décembre 2017, alors même qu'elle avait été prononcée avec des réserves, a fait courir, en application de l'article 13.4.4, les délais prévus à l'article 13.4.2. Par un courrier du 1er juin 2018, dont elle soutient sans être contredite qu'il a été reçu le même jour, cette société a adressé au représentant du pouvoir adjudicateur un projet de décompte général. Faute pour le maître d'ouvrage d'avoir notifié le décompte général dans un délai de dix jours, le projet de décompte général transmis par la société Dodin Guadeloupe est devenu le décompte général et définitif du marché. Ce décompte général et définitif fait apparaître, s'agissant de la société Sogetra, un solde à régler de 107 504,93 euros. Dans ces conditions, l'obligation dont se prévaut la société Sogetra doit être regardée comme non sérieusement contestable. Il suit de là que la société Sogetra est fondée à demander la réformation de l'ordonnance du juge des référés du tribunal administratif de la Guadeloupe en tant qu'elle n'a pas fait droit à ses conclusions tendant à l'octroi d'une provision au-delà de la somme de 47 882,10 euros.<br/>
<br/>
              Sur les intérêts moratoires et l'indemnité forfaitaire pour frais de recouvrement :<br/>
<br/>
              12. Aux termes de l'article 1er du décret du 29 mars 2013 relatif à la lutte contre les retards de paiement dans les contrats de la commande publique : " Le délai de paiement prévu au premier alinéa de l'article 37 de la loi du 28 janvier 2013 susvisée est fixé à trente jours pour les pouvoirs adjudicateurs, y compris lorsqu'ils agissent en tant qu'entité adjudicatrice. (...) ". Selon son article 2 : " I. - Le délai de paiement court à compter de la date de réception de la demande de paiement par le pouvoir adjudicateur ou, si le contrat le prévoit, par le maître d'oeuvre ou toute autre personne habilitée à cet effet. / Toutefois : (...) / 2° Pour le paiement du solde des marchés de travaux soumis au code des marchés publics, le délai de paiement court à compter de la date de réception par le maître de l'ouvrage du décompte général et définitif établi dans les conditions fixées par le cahier des clauses administratives générales applicables aux marchés publics de travaux ; (...) ". Aux termes de son article 7 : " Lorsque les sommes dues en principal ne sont pas mises en paiement (...) à l'expiration du délai de paiement, le créancier a droit, sans qu'il ait à les demander, au versement des intérêts moratoires et de l'indemnité forfaitaire pour frais de recouvrement prévus aux articles 39 et 40 de la loi du 28 janvier 2013 susvisée ".<br/>
<br/>
              13. Il résulte de ces dispositions du décret du 29 mars 2013 qu'il y a lieu de faire droit aux conclusions de la société Sogetra tendant à ce que la somme de 107 504,93 euros qui lui est octroyée à titre de provision porte intérêts à compter de la date de réception du décompte général et définitif et à ce que l'Etat soit condamné à lui verser la somme de 40 euros au titre de l'indemnité forfaitaire pour frais de recouvrement. La société Sogetra a également droit, en application de l'article 1343-2 du code civil, à la capitalisation des intérêts au 11 juillet 2019, puis à chaque échéance annuelle à compter de cette date.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Sogetra, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 décembre 2019 du juge des référés de la cour administrative d'appel de Bordeaux est annulée.<br/>
Article 2 : Il est donné acte du désistement des conclusions d'appel incident du garde des sceaux, ministre de la justice.<br/>
Article 3 : La somme que l'Etat a été condamnée à verser à la société Sogetra à titre de provision sur le règlement des sommes dues au titre du marché du 5 novembre 2015 relatif à la réalisation de travaux de construction d'unités éducatives et d'un logement de fonction par l'ordonnance du 28 juin 2019 du juge des référés du tribunal administratif de la Guadeloupe est portée à 107 504,93 euros.<br/>
Article 4 : L'Etat versera à la société Sogetra la somme de 40 euros au titre de l'indemnité forfaitaire pour frais de recouvrement.<br/>
Article 5 : La somme de 107 504,93 euros versée à titre de provision portera intérêts à compter du 11 juillet 2018, au taux prévu par les dispositions du I de l'article 8 du décret du 29 mars 2013 relatif à la lutte contre les retards de paiement dans les contrats de la commande publique. Les intérêts échus à la date du 11 juillet 2019 puis à chaque échéance annuelle à compter de cette date seront capitalisés à chacune de ces dates pour produire eux-mêmes intérêts.<br/>
Article 6 : L'ordonnance du 28 juin 2019 du juge des référés du tribunal administratif de la Guadeloupe est réformée en ce qu'elle a de contraire à la présente décision.<br/>
Article 7 : L'Etat versera à la société Sogetra la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 8 : La présente décision sera notifiée à la société Sogetra et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÈGLEMENT DES MARCHÉS. DÉCOMPTE GÉNÉRAL ET DÉFINITIF. - POINT DE DÉPART DU DÉLAI D'ÉTABLISSEMENT DU DÉCOMPTE FINAL - 1) CAS D'UNE RÉCEPTION AVEC RÉSERVES (ART. 41.6 DU CCAG TRAVAUX) - DATE DE NOTIFICATION DE LA DÉCISION DE RÉCEPTION DES TRAVAUX [RJ1] - 2) CAS D'UNE RÉCEPTION SOUS RÉSERVES (ART. 41.5 DU CCAG) - DATE DE LEVÉE DES RÉSERVES.
</SCT>
<ANA ID="9A"> 39-05-02-01 1) Lorsque le pouvoir adjudicateur entend prononcer la réception en faisant application de l'article 41.6 du cahier des clauses administratives générales (CCAG) Travaux relatif à la réception avec réserves des travaux, la date de notification de la décision de réception des travaux, et non 2) la date de levée des réserves comme pour la réception sous réserves prévues par l'article 41.5 de ce CCAG, constitue le point de départ des délais d'établissement du décompte final, quelle que soit l'importance des réserves émises par le pouvoir adjudicateur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'obligation pour le maître d'ouvrage, en cas de réception avec réserves, de faire état dans le décompte général des sommes nécessaires à la levée de réserves, CE, 20 mars 2013, Centre hospitalier de Versailles, n° 357636, T. p. 698.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
