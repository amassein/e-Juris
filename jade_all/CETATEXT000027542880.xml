<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027542880</ID>
<ANCIEN_ID>JG_L_2013_06_000000347406</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/54/28/CETATEXT000027542880.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 12/06/2013, 347406</TITRE>
<DATE_DEC>2013-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347406</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347406.20130612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 mars et 6 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la ville de Marseille, représentée par son maire ; la ville de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02395 du 30 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0605890 du 6 mars 2008 par lequel le tribunal administratif de Marseille a annulé la décision de son maire du 18 août 2006 confirmant le non-renouvellement du contrat de Mme B...;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 1999/70/CE du Conseil du 28 juin 1999 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2005-843 du 26 juillet 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la ville de Marseille et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a été recrutée par la ville de Marseille à compter du 13 juin 1991 en qualité d'attachée territoriale de 2ème classe non titulaire, par un contrat du 20 juin 1991, qui a fait l'objet de renouvellements successifs ; que le dernier contrat, en date du 31 décembre 2003, conclu sur le fondement du troisième alinéa de l'article 3 de la loi du 26 janvier 1984, prévoit l'engagement de l'intéressée pour une durée d'un an du 1er janvier 2004 au 31 décembre 2004 inclus ; que le 14 septembre 2004, le maire a informé l'intéressée de sa décision de ne pas renouveler son contrat à son échéance et a rejeté, le 18 août 2006, le dernier recours gracieux qu'elle a formé le 1er août 2006 contre cette décision ; que la ville de Marseille se pourvoit en cassation contre l'arrêt du 30 novembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle a interjeté du jugement du 6 mars 2008 par lequel le tribunal administratif de Marseille a annulé la décision du 18 août 2006 en tant qu'elle a confirmé le non renouvellement du contrat de MmeB... ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999 concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée : " La présente directive vise à mettre en oeuvre l'accord-cadre sur le travail à durée déterminée, figurant en annexe, conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP)." ; qu'aux termes de l'article 2 de cette directive : " Les Etats membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive au plus tard le 10 juillet 2001 ou s'assurent, au plus tard à cette date, que les partenaires sociaux ont mis en place les dispositions nécessaires par voie d'accord, les Etats membres devant prendre toute disposition nécessaire leur permettant d'être à tout moment en mesure de garantir les résultats imposés par la présente directive. Ils en informent immédiatement la Commission. (...) " ; qu'aux termes des stipulations de la clause 5 de l'accord-cadre, relative aux mesures visant à prévenir l'utilisation abusive des contrats à durée déterminée : " 1. Afin de prévenir les abus résultant de l'utilisation de contrats ou de relations de travail à durée déterminée successifs, les États membres, après consultation des partenaires sociaux, conformément à la législation, aux conventions collectives et pratiques nationales, et/ou les partenaires sociaux, quand il n'existe pas des mesures légales équivalentes visant à prévenir les abus, introduisent d'une manière qui tienne compte des besoins de secteurs spécifiques et/ou de catégories de travailleurs, l'une ou plusieurs des mesures suivantes : a) des raisons objectives justifiant le renouvellement de tels contrats ou relations de travail ; b) la durée maximale totale de contrats ou relations de travail à durée déterminée successifs ; c) le nombre de renouvellements de tels contrats ou relations de travail. 2. Les États membres, après consultation des partenaires sociaux et/ou les partenaires sociaux, lorsque c'est approprié, déterminent sous quelles conditions les contrats ou relations de travail à durée déterminée : a) sont considérés comme "successifs" ; b) sont réputés conclus pour une durée indéterminée." ; que les stipulations de la clause 5 de l'accord-cadre annexé à la directive 1999/70/CE ont été transposées par les dispositions figurant au chapitre III de la loi du 26 juillet 2005 portant diverses mesures de transposition du droit communautaire à la fonction publique, postérieurement à la date de la décision litigieuse de non renouvellement du dernier contrat de Mme B... ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 3 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Sauf dérogation prévue par une disposition législative, les emplois civils permanents de l'Etat, des régions, des départements, des communes et de leurs établissements publics à caractère administratif sont, à l'exception de ceux réservés aux magistrats de l'ordre judiciaire et aux fonctionnaires des assemblées parlementaires, occupés soit par des fonctionnaires régis par le présent titre, soit par des fonctionnaires des assemblées parlementaires, des magistrats de l'ordre judiciaire ou des militaires dans les conditions prévues par leur statut " ; qu'aux termes de l'article 3 du la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction antérieure à la loi du 26 juillet 2005 : " Les collectivités et établissements mentionnés à l'article 2 ne peuvent recruter des agents non titulaires pour occuper des emplois permanents que pour assurer le remplacement momentané de titulaires autorisés à exercer leurs fonctions à temps partiel ou indisponibles en raison d'un congé de maladie, d'un congé de maternité ou d'un congé parental, ou de l'accomplissement du service national, du rappel ou du maintien sous les drapeaux, ou pour faire face temporairement et pour une durée maximale d'un an à la vacance d'un emploi qui ne peut être immédiatement pourvu dans les conditions prévues par la présente loi. / Ces collectivités et établissements peuvent, en outre, recruter des agents non titulaires pour exercer des fonctions correspondant à un besoin saisonnier pour une durée maximale de six mois pendant une même période de douze mois et conclure pour une durée maximale de trois mois, renouvelable une seule fois à titre exceptionnel, des contrats pour faire face à un besoin occasionnel. / Des emplois permanents peuvent être occupés par des agents contractuels dans les mêmes cas et selon les mêmes conditions de durée que ceux mentionnés à l'article 4 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat. " ; qu'aux termes de l'article 4 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction antérieure à la loi du 26 juillet 2005 : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants : 1° Lorsqu'il n'existe pas de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; 2° Pour les emplois du niveau de la catégorie A et, dans les représentations de l'Etat à l'étranger, des autres catégories, lorsque la nature des fonctions ou les besoins des services le justifient. Les agents ainsi recrutés sont engagés par des contrats d'une durée maximale de trois ans qui ne peuvent être renouvelés que par reconduction expresse. " ; <br/>
<br/>
              4. Considérant que les deux premiers alinéas de l'article 3 de la loi du 26 janvier 1984, qui décrivent avec précision les conditions dans lesquelles il peut être recouru, pour des besoins ponctuels ou saisonniers, à des agents non titulaires recrutés par contrats à durée déterminée ainsi que la durée et les conditions limitées de renouvellement de ces contrats, permettent de prévenir, conformément aux objectifs fixés par la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999, l'utilisation abusive des contrats à durée déterminée dans la fonction publique territoriale ; qu'en revanche, les dispositions du troisième alinéa de cet article, combinées avec celles de l'article 4 de la loi du 11 janvier 1984, dans leur rédaction antérieure à la loi du 26 juillet 2005, permettaient le recrutement d'agents non titulaires par contrats à durée déterminée " en l'absence de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes " ou, pour les emplois de catégorie A, " lorsque la nature des fonctions ou les besoins des services le justifient " et ne limitaient ni la durée maximale totale des contrats de travail successifs, ni le nombre de leurs renouvellements ; que ces modalités de recrutement, qui excluaient la conclusion de contrats à durée indéterminée, n'étaient pas justifiées par des éléments suffisamment concrets et objectifs tenant à la nature des activités exercées et aux conditions de leur exercice et n'étaient donc pas compatibles avec les objectifs de la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999 ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, en jugeant que les dispositions alors en vigueur des trois alinéas de l'article 3 de la loi du 26 janvier 1984 et de l'article 4 de la loi du 11 janvier 1984 étaient, dans leur totalité, incompatibles avec les objectifs cette directive, la cour administrative d'appel a commis une erreur de droit ; que la ville de Marseille est dès lors fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la ville de Marseille au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la ville de Marseille, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 30 novembre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions de la ville de Marseille et de Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ville de Marseille et à Mme A...B....<br/>
Copie en sera adressée pour information à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-085 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DIRECTIVE 1999/70/CE DU CONSEIL DE L'UNION EUROPÉENNE DU 28 JUIN 1999 CONCERNANT L'ACCORD-CADRE CES, UNICE ET CEEP SUR LE TRAVAIL À DURÉE DÉTERMINÉE - RECOURS PAR DES COLLECTIVITÉS TERRITORIALES À DES AGENTS CONTRACTUELS (ART. 3 DE LA LOI DU 26 JANVIER 1984, DANS SA RÉDACTION ANTÉRIEURE À LA LOI DU 26 JUILLET 2005) - COMPATIBILITÉ AVEC LES OBJECTIFS FIXÉS PAR LA DIRECTIVE - 1) DEUX PREMIERS ALINÉAS - EXISTENCE - 2) TROISIÈME ALINÉA - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-12 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. - RECOURS PAR DES COLLECTIVITÉS TERRITORIALES À DES AGENTS CONTRACTUELS (ART. 3 DE LA LOI DU 26 JANVIER 1984, DANS SA RÉDACTION ANTÉRIEURE À LA LOI DU 26 JUILLET 2005) - COMPATIBILITÉ AVEC LES OBJECTIFS FIXÉS PAR LA DIRECTIVE 1999/70/CE DU CONSEIL DE L'UNION EUROPÉENNE DU 28 JUIN 1999 - 1) DEUX PREMIERS ALINÉAS - EXISTENCE - 2) TROISIÈME ALINÉA - ABSENCE.
</SCT>
<ANA ID="9A"> 15-05-085 1) Les deux premiers alinéas de l'article 3 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, qui décrivent avec précision les conditions dans lesquelles il peut être recouru, pour des besoins ponctuels ou saisonniers, à des agents non titulaires recrutés par contrats à durée déterminée ainsi que la durée et les conditions limitées de renouvellement de ces contrats, permettent de prévenir, conformément aux objectifs fixés par la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999 concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée, l'utilisation abusive des contrats à durée déterminée dans la fonction publique territoriale.,,,2) En revanche, les dispositions du troisième alinéa de cet article, combinées avec celles de l'article 4 de la loi n° 84-16 du 11 janvier 1984, dans leur rédaction antérieure à la loi n° 2005-843 du 26 juillet 2005, permettaient le recrutement d'agents non titulaires par contrats à durée déterminée  en l'absence de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes  ou, pour les emplois de catégorie A,  lorsque la nature des fonctions ou les besoins des services le justifient  et ne limitaient ni la durée maximale totale des contrats de travail successifs, ni le nombre de leurs renouvellements. Ces modalités de recrutement, qui excluaient la conclusion de contrats à durée indéterminée, n'étaient pas justifiées par des éléments suffisamment concrets et objectifs tenant à la nature des activités exercées et aux conditions de leur exercice et n'étaient donc pas compatibles avec les objectifs de la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999.</ANA>
<ANA ID="9B"> 36-12 1) Les deux premiers alinéas de l'article 3 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, qui décrivent avec précision les conditions dans lesquelles il peut être recouru, pour des besoins ponctuels ou saisonniers, à des agents non titulaires recrutés par contrats à durée déterminée ainsi que la durée et les conditions limitées de renouvellement de ces contrats, permettent de prévenir, conformément aux objectifs fixés par la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999  concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée, l'utilisation abusive des contrats à durée déterminée dans la fonction publique territoriale.,,,2) En revanche, les dispositions du troisième alinéa de cet article, combinées avec celles de l'article 4 de la loi n° 84-16 du 11 janvier 1984, dans leur rédaction antérieure à la loi n° 2005-843 du 26 juillet 2005, permettaient le recrutement d'agents non titulaires par contrats à durée déterminée  en l'absence de corps de fonctionnaires susceptibles d'assurer les fonctions correspondantes  ou, pour les emplois de catégorie A,  lorsque la nature des fonctions ou les besoins des services le justifient  et ne limitaient ni la durée maximale totale des contrats de travail successifs, ni le nombre de leurs renouvellements. Ces modalités de recrutement, qui excluaient la conclusion de contrats à durée indéterminée, n'étaient pas justifiées par des éléments suffisamment concrets et objectifs tenant à la nature des activités exercées et aux conditions de leur exercice et n'étaient donc pas compatibles avec les objectifs de la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
