<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037470471</ID>
<ANCIEN_ID>JG_L_2018_10_000000419131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/47/04/CETATEXT000037470471.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 05/10/2018, 419131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419131.20181005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Rennes d'annuler, d'une part, la décision de la caisse d'allocations familiales d'Ille-et-Vilaine du 15 septembre 2015 de récupérer un indu d'aide exceptionnelle de fin d'année pour 2014 d'un montant de 152,45 euros, la décision du 4 décembre 2015 la mettant en demeure de payer cette somme ainsi que celle du 22 décembre 2015 rejetant son recours gracieux et, d'autre part, la décision du 11 février 2016 par laquelle le président du conseil départemental d'Ille-et-Vilaine a confirmé la récupération d'un indu de revenu de solidarité active d'un montant de 6 646,01 euros. Par un jugement n°s 1600870, 1600950, 1602414 du 14 décembre 2017, le tribunal administratif de Rennes a rejeté ses demandes. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 20 mars et 18 juin 2018, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Rennes ; <br/>
<br/>
              2°)  réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge du département d'Ille-et-Vilaine, de la caisse d'allocations familiales d'Ille-et-Vilaine et de l'Etat la somme de 3 000 euros à verser à la SCP Delamarre, Jehannin, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
     	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2014-1709 du 30 décembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation du jugement du tribunal administratif de Rennes qu'elle attaque, Mme A...soutient que : <br/>
              - ce tribunal a commis une erreur de droit en se fondant, pour juger qu'elle était redevable d'un indu de revenu de solidarité active, sur des éléments relatifs à ses revenus fonciers pour l'année 2013 qui n'avaient pas été produits et alors qu'aucune pièce de la procédure ne permettait de déterminer le montant de ses revenus fonciers des années 2013 et 2015 ; <br/>
              - il a méconnu l'article R. 262-6 du code de l'action sociale et des familles en s'abstenant de rechercher si la production des déclarations de l'année 2013 et du début de l'année 2015 ne pouvait pas révéler des éléments de preuve utiles à l'appréciation de ses ressources ;<br/>
              - il a méconnu l'article R. 611-7 du code de justice administrative et commis une erreur de droit en retenant d'office, sans solliciter les observations des parties sur ce moyen, que la caisse d'allocations familiales était en situation de compétence liée pour récupérer l'indu d'aide exceptionnelle de fin d'année et en en déduisant que les moyens de légalité externe dirigés contre la décision de récupération étaient inopérants.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur l'indu d'aide exceptionnelle de fin d'année pour 2014. En revanche, s'agissant des conclusions dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur l'indu de revenu de solidarité active, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de Mme A...qui sont dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur l'indu d'aide exceptionnelle de fin d'année pour 2014 sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme A...n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à la ministre des solidarités et de la santé. <br/>
Copie en sera adressée au département d'Ille-et-Vilaine et à la caisse d'allocations familiales d'Ille-et-Vilaine. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
