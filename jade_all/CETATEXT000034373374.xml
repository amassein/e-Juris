<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034373374</ID>
<ANCIEN_ID>JG_L_2017_03_000000397162</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/37/33/CETATEXT000034373374.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/03/2017, 397162, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397162</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:397162.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de condamner Pôle emploi à lui verser la somme de 28 214,44 euros en réparation du préjudice qu'il estime avoir subi en raison du défaut d'information sur ses droits à l'allocation équivalent retraite et de versement de cette allocation, sous réserve d'actualisation ultérieure. Par un jugement n° 1304281 du 29 septembre 2015, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA04450 du 16 février 2016, enregistré le 22 février 2016 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté par M.B.... Par ce pourvoi et par un nouveau mémoire, enregistré le 20 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Marseille du 29 septembre 2015 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande, portée à la somme de 36 138,37 euros ;<br/>
<br/>
              3°) de mettre à la charge de Pôle emploi la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - le décret n° 2009-608 du 29 mai 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B..., et à la SCP Boullez, avocat de Pôle emploi.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la compétence de la juridiction administrative :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a engagé une action en responsabilité contre Pôle emploi, en raison d'un défaut d'information sur les conditions d'octroi de l'allocation équivalent retraite dont cet établissement public à caractère administratif assurait le versement pour le compte de l'Etat, en vertu du décret du 29 mai 2009 instituant à titre exceptionnel une allocation équivalent retraite pour certains demandeurs d'emploi, et auquel il estimait pouvoir prétendre à compter du 25 février 2009. Un tel litige relève de la compétence de la juridiction administrative, sans que les dispositions de l'article L. 5312-12 du code du travail relatives aux litiges portant sur les prestations servies par Pôle emploi trouvent à s'appliquer.   <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              Aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions, prévue par ces dispositions, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. En conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative. Cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public. <br/>
<br/>
              Il ressort des pièces de la procédure devant les premiers juges que le sens des conclusions du rapporteur public sur l'affaire litigieuse a été porté à la connaissance des parties, par l'intermédiaire du système informatique de suivi de l'instruction, quatre heures avant le début de l'audience du tribunal administratif de Marseille. M. B...ne peut, dans les circonstances de l'espèce, être regardé comme ayant été mis en mesure de connaître, dans un délai raisonnable avant l'audience, le sens des conclusions du rapporteur public. <br/>
<br/>
              Par suite, M. B...est fondé à soutenir que le jugement attaqué a été rendu au terme d'une procédure irrégulière et à en demander l'annulation. Le motif retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Pôle emploi la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise au même titre à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 29 septembre 2015 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 : Pôle emploi versera la somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de Pôle emploi présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à Pôle emploi. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
