<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220671</ID>
<ANCIEN_ID>JG_L_2018_07_000000400485</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220671.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 18/07/2018, 400485, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400485</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:400485.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Exxonmobil France Holding a demandé au tribunal administratif de Cergy-Pontoise de prononcer la restitution de l'imposition forfaitaire annuelle qu'elle a acquittée au titre de l'année 2007, en sa qualité de société mère d'un groupe fiscal intégré, pour le compte de quatre filiales qu'elle a absorbées le 31 décembre 2007, avec effet rétroactif au 1er janvier de la même année. Par un jugement n° 0804904 du 23 juin 2011, le tribunal administratif de Cergy-Pontoise a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11VE02730 du 16 avril 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par la SAS Exxonmobil France Holding contre ce jugement.<br/>
<br/>
              Par une décision n° 370712 du 23 novembre 2015, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Versailles.<br/>
<br/>
              Par un arrêt n° 15VE03567 du 14 avril 2016, la cour administrative d'appel de Versailles a rejeté l'appel dont elle était saisie.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 juin 2016, 1er septembre 2016 et 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la SAS Exxonmobil France Holding demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la SAS Exxonmobil France Holding.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société par actions simplifiées (SAS) Exxonmobil France Holding, tête d'un groupe fiscalement intégré, a acquitté pour les sociétés de ce groupe l'imposition forfaitaire annuelle au titre de l'année 2007. Le 31 décembre 2007, une des sociétés membres de ce groupe, la société Exxonmobil Chemical France, a absorbé, avec effet rétroactif au 1er janvier 2007, trois autres sociétés du même groupe, la société du Caoutchouc Butyl, la SAS Exxonmobil Chemical Polymères et la SNC Exxonmobil Chemical Polymères, cette dernière ayant elle-même absorbé la société Compagnie industrielle des polyethylènes de Normandie à la même date, avec effet rétroactif au 1er janvier 2007 également. Par un jugement du 23 juin 2011, le tribunal administratif de Cergy-Pontoise a rejeté la demande par laquelle la SAS Exxonmobil France Holding, se prévalant de la disparition rétroactive des quatre sociétés absorbées, sollicitait la restitution des sommes qu'elle avait versées au titre de l'imposition forfaitaire annuelle pour leur compte. Elle se pourvoit en cassation contre l'arrêt du 14 avril 2016 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Après avoir jugé qu'elle ne pouvait pas obtenir satisfaction sur le terrain de la loi fiscale, la cour a examiné l'argumentation par laquelle la société requérante se prévalait, sur le fondement de l'article L. 80 A du livre des procédures fiscales, des prévisions de l'instruction 4 I-2-00 du 3 août 2000 relative aux fusions de sociétés et opérations assimilées, ainsi rédigée : " 129. Toutefois, dès lors que les sociétés bénéficiaires des apports prennent en compte, pour la détermination de leurs résultats imposables, les résultats provenant des activités apportées à compter de la date d'effet de l'opération, il est admis que les sociétés absorbées ou scindées soient considérées comme n'étant plus passibles de l'impôt sur les sociétés, au sens de l'article 223 septies du code déjà cité, à compter de la date d'effet. /(...) 130. Il est admis que l'imposition forfaitaire annuelle de l'année de la fusion ou de la scission n'est pas due par la société absorbée ou scindée. (...) / 131. L'imposition forfaitaire annuelle acquittée par la société absorbée ou scindée peut être restituée ou, à titre pratique, transférée à la société bénéficiaire des apports (venant aux droits et aux obligations de la société absorbée ou scindée) (...) ". Le paragraphe 130 de cette instruction pose expressément la règle générale selon laquelle l'imposition forfaitaire annuelle de l'année de la fusion n'est pas due par la société absorbée. Son paragraphe 131 se borne, en revanche, à détailler l'un des cas d'application de la règle générale prévue au paragraphe 130. La cour a donc commis une erreur de droit en jugeant que la SAS Exxonmobil France Holding n'était pas fondée à invoquer cette instruction au motif qu'elle ne prévoit pas l'hypothèse dans laquelle l'imposition forfaitaire annuelle due par les sociétés membres d'un groupe fiscal intégré qui ont été absorbées a été acquittée, en application de l'article 223 A du code général des impôts, par la société tête de groupe et que cette dernière n'est pas l'absorbante de ces sociétés. Par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              3. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              4. En vertu de l'article 223 A du code général des impôts, dans sa rédaction applicable au litige, lorsqu'une société s'est constituée seule redevable de l'impôt sur les sociétés dû sur l'ensemble des résultats d'un groupe fiscalement intégré, " elle est également redevable de l'imposition forfaitaire annuelle due par les sociétés du groupe ". Aux termes de l'article 223 septies du même code : " Les personnes morales passibles de l'impôt sur les sociétés sont assujetties à une imposition forfaitaire annuelle (...) ". Selon l'article 1668 A de ce code : " L'imposition forfaitaire visée à l'article 223 septies doit être payée spontanément à la caisse du comptable de la direction générale des impôts chargé du recouvrement de l'impôt sur les sociétés, au plus tard le 15 mars. (...) ". Il résulte de ces dispositions que l'imposition forfaitaire annuelle est due par les sociétés passibles de l'impôt sur les sociétés qui existent au 1er janvier de l'année d'imposition. Une clause de rétroactivité au 1er janvier, contenue dans une convention de fusion conclue en cours d'année, ne peut avoir pour effet de retirer rétroactivement à la société absorbée la qualité de redevable de cette imposition, qui lui a légalement été impartie lors de la réalisation de son fait générateur. Par suite, dès lors que les quatre sociétés membres du groupe fiscal intégré qui ont été absorbées le 31 décembre 2007 exerçaient le 1er janvier de la même année, date du fait générateur de l'imposition forfaitaire annuelle, une activité à caractère lucratif à raison de laquelle elles étaient passibles de l'impôt sur les sociétés, elles étaient également passibles de l'imposition forfaitaire au titre de cette année, nonobstant la circonstance que les clauses des traités de fusion-absorption litigieux aient prévu leur disparition rétroactive au 1er janvier 2007. Il suit de là que la société Exxonmobil France Holding était elle-même redevable de cette imposition due par ces sociétés membres du groupe intégré, en application des dispositions précitées de l'article 223 A du code général des impôts.<br/>
<br/>
              5. Toutefois, il découle de ce qui a été dit au point 2 que la société requérante est fondée à se prévaloir, sur le fondement du second alinéa de l'article L. 80 A du livre des procédures fiscales, des prévisions du paragraphe n° 130 de l'instruction 4 I-2-0 du 3 août 2000 relative aux fusions de sociétés et opérations assimilées, en vertu desquelles l'imposition forfaitaire annuelle de l'année de la fusion n'est pas due par la société absorbée, pour obtenir la restitution de l'imposition forfaitaire annuelle due par les sociétés membres du groupe qui ont été absorbées au cours de l'année 2007 et qu'elle a acquittée, à leur place, en application de l'article 223 A du code général des impôts.<br/>
<br/>
              6. Il résulte de ce qui précède que la SAS Exxonmobil France Holding est fondée à soutenir que c'est à tort que le tribunal administratif de Cergy-Pontoise a rejeté ses conclusions tendant à la restitution de l'imposition forfaitaire annuelle due par les quatre sociétés membres du groupe fiscal intégré qui ont été absorbées au cours de l'année 2007, et qu'elle a acquittée en sa qualité de société mère de ce groupe.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 5 000 euros à verser à la SAS Exxonmobil France Holding au titre de l'article L. 761-1 du code de justice administrative, pour l'ensemble de la procédure juridictionnelle.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 14 avril 2016 de la cour administrative d'appel de Versailles et le jugement du 23 juin 2011 du tribunal administratif de Cergy-Pontoise sont annulés.<br/>
Article 2 : Il est accordé à la SAS Exxonmobil France Holding la restitution de l'imposition forfaitaire annuelle qu'elle a acquittée au titre de l'année 2007, en sa qualité de société mère d'un groupe fiscal intégré, pour le compte des quatre filiales qui ont été absorbées au cours de cette année.<br/>
Article 3 : L'Etat versera une somme de 5 000 euros à la SAS Exxonmobil France Holding au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SAS Exxonmobil France Holding et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
