<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036028797</ID>
<ANCIEN_ID>JG_L_2017_11_000000400542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/02/87/CETATEXT000036028797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 15/11/2017, 400542</TITRE>
<DATE_DEC>2017-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400542.20171115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL Foncière Chapal a demandé au tribunal administratif de Lyon d'annuler le titre exécutoire émis à son encontre le 3 avril 2012 par l'Agence nationale pour l'amélioration de l'habitat (ANAH) pour un montant de 285 929 euros et de la décharger de cette somme. Par un jugement n° 1205482 du 18 décembre 2014, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15LY0103615LY01038 du 12 avril 2016, la cour administrative d'appel de Lyon a, d'une part, rejeté la requête par laquelle la SARL Foncière Chapal avait fait appel de ce jugement et, d'autre part, dit qu'il n'y avait pas lieu de statuer sur sa requête tendant à ce qu'il soit sursis à son exécution.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 juin et 1er septembre 2016 au secrétariat du contentieux du Conseil d'Etat, la SARL Foncière Chapal demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ANAH une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - l'arrêté du 2 février 2011 portant approbation du régime général de l'Agence nationale de l'Habitat ;<br/>
<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la SARL Foncière Chapal et à la SCP Gadiou, Chevallier, avocat de l'Agence nationale pour l'amélioration de l'habitat ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 31 octobre 2017, présentée par l'ANAH ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL Foncière Chapal a bénéficié de subventions de l'Agence nationale pour l'amélioration de l'habitat (ANAH) en vue de la rénovation de dix-sept logements lui appartenant situés à Saint-Etienne (Loire) ; qu'à la suite de la vente de ces logements à la société anonyme d'HLM Néolia, l'ANAH a exigé le reversement des subventions par un titre exécutoire émis le 3 avril 2012 pour un montant de 285 929 euros ; que la SARL Foncière Chapal a présenté contre ce titre un recours contentieux que le tribunal administratif de Lyon a rejeté par un jugement du 18 décembre 2014 ; qu'elle se pourvoit en cassation contre l'arrêt du 12 avril 2016 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 321-13 du code de la construction et de l'habitation : " Sous réserve de l'application des dispositions des 4°, 9°, 10°, 11° du I ainsi que des III, IV et V de l'article R. 321-12 (...), les organismes d'habitations à loyer modéré mentionnés à l'article L. 411-2 et les sociétés d'économie mixte ayant pour objet statutaire la construction ou la gestion de logements ou la restructuration urbaine ne peuvent bénéficier de l'aide de l'agence " ; qu'aux termes des dispositions de l'article R. 321-21 du même code : " Le retrait et le reversement total ou partiel peuvent légalement être prononcés en cas de non-respect des prescriptions de la présente section ou des conventions conclues en application des articles L. 321-4 et L. 321-8, ou de toute autre convention liée au bénéfice des aides de l'agence, selon les modalités fixées par le règlement général de l'agence " ; que, par ailleurs, l'article L. 321-11 dispose que : " En cas de mutation d'un bien faisant l'objet d'une convention mentionnée à l'article L. 321-4 ou à l'article L. 321-8, la convention en cours s'impose de plein droit au nouveau propriétaire. Les engagements de la convention en cours sont obligatoirement mentionnés dans l'acte de mutation. Un avenant précisant l'identité du nouveau propriétaire est signé entre celui-ci et l'Agence nationale de l'habitat. A défaut, l'Agence nationale de l'habitat peut appliquer au propriétaire vendeur les sanctions prévues à l'article L. 321-2 " ;<br/>
<br/>
              3. Considérant que les dispositions précitées de l'article R. 321-13 du code de la construction et de l'habitation, en vertu desquelles les organismes de logement à loyer modéré ne peuvent bénéficier de l'aide de l'ANAH, n'ont ni pour objet, ni pour effet de faire obstacle à ce qu'un propriétaire privé ayant réalisé des travaux de réhabilitation dans un logement lui appartenant et ayant perçu, à ce titre, une subvention de l'ANAH dans le cadre d'une convention signée en application de l'article L. 321-4 ou de l'article L. 321-8 du même code vende ultérieurement le logement à un organisme d'habitations à loyer modéré ; que la cour administrative d'appel de Lyon n'a relevé aucun élément de nature à établir que la SARL Foncière Chapal aurait agi pour le compte de la société anonyme d'HLM Néolia lorsqu'elle a sollicité les subventions de l'agence ou que l'opération aurait eu pour objet d'assurer à cet organisme le bénéfice des subventions ; qu'il ressort, par ailleurs, des pièces du dossier qui lui était soumis que les engagements contenus dans la convention conclue entre l'ANAH et la SARL Foncière Chapal lors de l'octroi des subventions litigieuses ont été repris par la société anonyme d'HLM Néolia lors de la cession, dans les conditions prévues à l'article L. 321-1 du code de la construction et de l'habitation ; qu'il suit de là qu'en jugeant que l'ANAH avait pu légalement se fonder sur cette cession pour exiger le reversement des subventions, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SARL Foncière Chapal est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ANAH le versement de la somme de 3 000 euros à la SARL Foncière Chapal au titre  de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce que soit mis à la charge de la SARL Foncière Chapal, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que l'ANAH demande au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 12 avril 2016 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'ANAH versera à la SARL Financière Chapal une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par l'ANAH au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la SARL Financière Chapal et à l'Agence nationale pour l'amélioration de l'habitat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-03-03-01 LOGEMENT. AIDES FINANCIÈRES AU LOGEMENT. AMÉLIORATION DE L'HABITAT. AGENCE NATIONALE POUR L'AMÉLIORATION DE L'HABITAT. - PROPRIÉTAIRE PRIVÉ AYANT BÉNÉFICIÉ D'UNE AIDE - POSSIBILITÉ DE CÉDER CE LOGEMENT À UN ORGANISME HLM - EXISTENCE, NONOBSTANT L'IMPOSSIBILITÉ POUR LES ORGANISMES HLM ET LES SEM EXERÇANT UNE ACTIVITÉ DE CONSTRUCTION ET DE GESTION DE LOGEMENTS DE BÉNÉFICIER D'UNE AIDE DE L'ANAH (ART. R. 321-13 DU CCH).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">38-04-01 LOGEMENT. HABITATIONS À LOYER MODÉRÉ. ORGANISMES D'HABITATION À LOYER MODÉRÉ. - IMPOSSIBILITÉ POUR LES ORGANISMES HLM ET LES SEM EXERÇANT UNE ACTIVITÉ DE CONSTRUCTION ET DE GESTION DE LOGEMENTS DE BÉNÉFICIER D'UNE AIDE DE L'ANAH (ART. R. 321-13 DU CCH) - INCIDENCE SUR LA POSSIBILITÉ POUR UN PROPRIÉTAIRE PRIVÉ DE CÉDER UN LOGEMENT, AU TITRE DUQUEL IL A PERÇU UNE AIDE DE L'ANAH, À UN ORGANISME HLM - ABSENCE.
</SCT>
<ANA ID="9A"> 38-03-03-01 L'article R. 321-13 du code de la construction et de l'habitation (CCH), en vertu duquel les organismes de logement à loyer modéré ne peuvent bénéficier de l'aide de l'Agence nationale de l'habitat (ANAH), n'a ni pour objet, ni pour effet de faire obstacle à ce qu'un propriétaire privé ayant réalisé des travaux de réhabilitation dans un logement lui appartenant et ayant perçu, à ce titre, une subvention de l'ANAH dans le cadre d'une convention signée en application de l'article L. 321-4 ou de l'article L. 321-8 du même code, vende ultérieurement le logement à un organisme d'habitations à loyer modéré (HLM).</ANA>
<ANA ID="9B"> 38-04-01 L'article R. 321-13 du code de la construction et de l'habitation (CCH), en vertu duquel les organismes de logement à loyer modéré ne peuvent bénéficier de l'aide de l'Agence nationale de l'habitat (ANAH), n'a ni pour objet, ni pour effet de faire obstacle à ce qu'un propriétaire privé ayant réalisé des travaux de réhabilitation dans un logement lui appartenant et ayant perçu, à ce titre, une subvention de l'ANAH dans le cadre d'une convention signée en application de l'article L. 321-4 ou de l'article L. 321-8 du même code, vende ultérieurement le logement à un organisme d'habitations à loyer modéré.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
