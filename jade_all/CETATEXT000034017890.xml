<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017890</ID>
<ANCIEN_ID>JG_L_2017_02_000000389806</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/78/CETATEXT000034017890.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/02/2017, 389806</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389806</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389806.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société NotreFamille.com a demandé au tribunal administratif de Poitiers d'annuler la décision du directeur général des services du département de la Vienne du 1er juillet 2010 rejetant sa demande tendant à l'abrogation de la délibération du conseil général de ce département du 18 décembre 2009 fixant les conditions de réutilisation par des tiers des archives publiques conservées par le service des archives du département. Par un jugement n° 1002347 du 31 janvier 2013, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13BX00856 du 26 février 2015, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société NotreFamille.com contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 27 avril 2015, 16 juin 2015 et 13 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société NotreFamille.com demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) subsidiairement, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, sur le fondement de l'article 267 du Traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              4°) de mettre à la charge du département de la Vienne la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du patrimoine ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le code de la propriété intellectuelle ; <br/>
              - la décision du 14 septembre 2015 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société NotreFamille.com ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société NotreFamille.com et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du département de la Vienne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1 Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 18 décembre 2009 le conseil général de la Vienne a fixé les conditions de réutilisation, par les tiers, des archives publiques conservées par le service des archives départementales. Cette délibération précise, notamment, que la réutilisation des archives publiques s'effectue à partir de la consultation des documents d'archives, soit en salle de lecture, soit sur le site internet du département. Telle que sa portée a été précisée par le président du conseil général à la suite d'une demande formulée par la société NotreFamille.com, cette délibération a notamment pour objet et pour effet d'interdire de collecter, au moyen d'un logiciel de collecte et d'indexation systématique, les données figurant dans la base de données rendue accessible publiquement en ligne, contenant, sous une forme numérisée, l'ensemble des archives publiques du département relatives à l'état civil. Elle n'autorise également la cession, par le département, des fichiers numériques contenant ces archives que si elle est nécessaire à l'accomplissement d'une mission de service public. Par une décision du 1er juillet 2010, le président du conseil général de la Vienne a rejeté la demande de la société NotreFamille.com tendant à l'abrogation de cette délibération au motif que le département tenait de l'article L. 342-1 du code de la propriété intellectuelle, en sa qualité de producteur de base de donnée, le droit d'interdire l'extraction et à la réutilisation des informations contenues dans la base de données publique des archives départementales. Par un jugement du 31 janvier 2013 le tribunal administratif de Poitiers a rejeté la demande de la société NotreFamille.com tendant à l'annulation de cette décision. La société NotreFamille.com se pourvoit en cassation contre l'arrêt du 26 février 2015 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. Aux termes des dispositions alors applicables de l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public : " Sont considérés comme documents administratifs, (...) quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission ". L'article 10 de cette même loi précise que les informations figurant dans les documents ainsi mentionnés, quel que soit le support, peuvent être utilisées par toute personne qui le souhaite à d'autres fins que celles de la mission de service public pour les besoins de laquelle les documents ont été produits ou reçus. Cet article dispose néanmoins, dans son c), que les informations contenues dans des documents sur lesquels " des tiers " détiennent des droits de propriété intellectuelle ne sont pas considérés comme des informations publiques pour l'application des dispositions de la loi relatives à la réutilisation de ces informations. L'article 11 de la loi du 17 juillet 1978 prévoit que les établissements, organismes ou services culturels peuvent fixer les conditions dans lesquelles les informations peuvent être réutilisées lorsqu'elles figurent dans des documents produits ou reçus par eux. Les articles 15 et 16 de cette loi prévoient que la réutilisation d'informations publiques peut donner lieu au versement de redevances pour la fixation desquelles l'administration peut aussi tenir compte des coûts de collecte et de production des informations et inclure dans l'assiette de la redevance une rémunération raisonnable de ses investissements comprenant, le cas échéant, une part au titre des droits de propriété intellectuelle.<br/>
<br/>
              3. Il résulte de l'ensemble de ces dispositions que les articles 15 et 16 de la loi du 17 juillet 1978 alors applicable régissent de manière complète les conditions dans lesquelles les personnes mentionnées à l'article 1er de la loi du 17 juillet 1978 ainsi que les établissements, organismes ou services culturels qui en relèvent, exercent les droits de propriété intellectuelle ou les droits voisins que, le cas échéant, ils détiennent sur les informations publiques, comme sur les procédés de collecte, de production, de mise à disposition ou de diffusion de ces informations. Il s'ensuit que ces dispositions font obstacle à ce que les personnes et services qui viennent d'être mentionnés, qui ne sont pas des tiers au sens et pour l'application du c) de l'article 10 de la loi du 17 juillet 1978, puissent se fonder sur les droits que tient le producteur de bases de données de l'article L. 342-1 du code de la propriété intellectuelle, pour s'opposer à l'extraction ou à la réutilisation du contenu de telles bases, lorsque ce contenu revêt la nature d'informations publiques au sens des dispositions du même article. Il s'ensuit qu'en jugeant qu'un service culturel producteur d'une base de données pouvait se prévaloir du droit qu'il tient, en cette qualité, de l'article L. 342-1 du code de la propriété intellectuelle pour  interdire la réutilisation de la totalité ou d'une partie substantielle du contenu de cette base, la cour administrative d'appel de Bordeaux a entaché son arrêt d'erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société NotreFamille.com est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu de mettre à la charge du département de la Vienne la somme de 3 000 euros à verser à la société NotreFamille.com au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 26 février 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le département de la Vienne versera à la société NotreFamille.com une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 4 : La présente décision sera notifiée à la société NotreFamille.com, à la ministre de la culture et de la communication et au département de la Vienne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-04 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. - DROITS DE PROPRIÉTÉ INTELLECTUELLE SUR LES INFORMATIONS PUBLIQUES - NOTION DE TIERS (C DE L'ART. 10 DE LA LOI DU 17 JUILLET 1978) - PERSONNES MENTIONNÉES À L'ART. 1ER DE LA LOI DU 17 JUILLET 1978 ET ORGANISMES OU SERVICES CULTURELS QUI EN RELÈVENT - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. - RÉUTILISATION D'INFORMATIONS PUBLIQUES - NOTION DE TIERS (C DE L'ART.10 DE LA LOI DU 17 JUILLET 1978) - PERSONNES MENTIONNÉES À L'ART. 1ER DE LA LOI DU 17 JUILLET 1978 ET ORGANISMES OU SERVICES CULTURELS QUI EN RELÈVENT - EXCLUSION.
</SCT>
<ANA ID="9A"> 26-04 Il résulte des dispositions de l'article 1er, du c) de l'article 10 et de l'article 11 de la loi n° 78-753 du 17 juillet 1978 que les articles 15 et 16 de cette loi, qui prévoient les conditions dans lesquelles la réutilisation d'informations publiques peut donner lieu au versement d'une redevance pouvant, le cas échéant, inclure une part au titre des droits de propriété intellectuelle, régissent de manière complète les conditions dans lesquelles les personnes mentionnées à l'article 1er de la loi du 17 juillet 1978 ainsi que les établissements, organismes ou services culturels qui en relèvent, exercent les droits de propriété intellectuelle ou les droits voisins que, le cas échéant, ils détiennent sur les informations publiques, comme sur les procédés de collecte, de production, de mise à disposition ou de diffusion de ces informations.,,,Il s'ensuit que ces dispositions font obstacle à ce que les personnes et services qui viennent d'être mentionnés, qui ne sont pas des tiers au sens et pour l'application du c) de l'article 10 de la loi du 17 juillet 1978, puissent se fonder sur les droits que tient le producteur de bases de données de l'article L. 342-1 du code de la propriété intellectuelle, pour s'opposer à l'extraction ou à la réutilisation du contenu de telles bases, lorsque ce contenu revêt la nature d'informations publiques au sens des dispositions du même article.</ANA>
<ANA ID="9B"> 26-06 Il résulte des dispositions de l'article 1er, du c) de l'article 10 et de l'article 11 de la loi n° 78-753 du 17 juillet 1978 que les articles 15 et 16 de cette loi, qui prévoient les conditions dans lesquelles la réutilisation d'informations publiques peut donner lieu au versement d'une redevance pouvant, le cas échéant, inclure une part au titre des droits de propriété intellectuelle, régissent de manière complète les conditions dans lesquelles les personnes mentionnées à l'article 1er de la loi du 17 juillet 1978 ainsi que les établissements, organismes ou services culturels qui en relèvent, exercent les droits de propriété intellectuelle ou les droits voisins que, le cas échéant, ils détiennent sur les informations publiques, comme sur les procédés de collecte, de production, de mise à disposition ou de diffusion de ces informations.,,,Il s'ensuit que ces dispositions font obstacle à ce que les personnes et services qui viennent d'être mentionnés, qui ne sont pas des tiers au sens et pour l'application du c) de l'article 10 de la loi du 17 juillet 1978, puissent se fonder sur les droits que tient le producteur de bases de données de l'article L. 342-1 du code de la propriété intellectuelle, pour s'opposer à l'extraction ou à la réutilisation du contenu de telles bases, lorsque ce contenu revêt la nature d'informations publiques au sens des dispositions du même article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
