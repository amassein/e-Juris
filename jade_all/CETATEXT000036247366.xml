<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036247366</ID>
<ANCIEN_ID>JG_L_2017_12_000000402383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/24/73/CETATEXT000036247366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 20/12/2017, 402383</TITRE>
<DATE_DEC>2017-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MARLANGE, DE LA BURGADE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402383.20171220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Sélestat (Bas-Rhin) a demandé au juge des référés du tribunal administratif de Strasbourg, sur le fondement de l'article L. 521-3 du code de justice administrative, d'ordonner l'expulsion de M. B...A..., technicien supérieur forestier de l'Office national des forêts, ainsi que de tous ses biens, meubles et occupants de son chef, de la maison forestière Danielsrain, qui lui appartient et qu'elle a concédée par nécessité absolue de service aux agents de l'Office national des forêts occupant le poste de triage auquel cette maison est rattachée. Par une ordonnance n° 1604165 du 29 juillet 2016, le juge des référés a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 et 26 août 2016 et le 28 septembre 2017, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la commune de Sélestat ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sélestat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code forestier, notamment ses articles L. 211-1, L. 221-2 et L. 222-6 ;<br/>
              - la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905, notamment son article 65 ;<br/>
              - la loi n° 79-587 du 11 juillet 1979, notamment son article 1er ;<br/>
              - la loi n° 84-16 du 11 janvier 1984, notamment son article 60 ;<br/>
              - le décret n° 82-451 du 28 mai 1982 ;<br/>
              - le décret n° 2013-1173 du 17 décembre 2013 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M.A..., à la SCP Marlange, de la Burgade, avocat de la commune de Selestat et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'office national des forêts.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 décembre 2017, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M.A..., technicien supérieur forestier en poste à la Vancelle gare -Kintzheim (Bas-Rhin), a fait l'objet d'un arrêté en date du 23 octobre  2015 par lequel le directeur général de l'Office national des forêts a prononcé sa mutation d'office dans l'intérêt du service au poste de Wolfgantzen à compter du 16 novembre 2015. Estimant sa mutation illégale, M. A...a refusé de quitter la maison forestière de Danielsrain, qui appartient à la commune de Sélestat (Bas-Rhin) et fait l'objet d'une concession de logement par nécessité absolue de service aux agents de l'Office national des forêts affectés au poste de triage correspondant en vertu d'une délibération du conseil municipal de cette commune en date du 3 mars 1961. Devant l'opposition persistante de M. A...et compte tenu de l'arrivée de son successeur, nommé à compter du 1er juin 2016, la commune de Sélestat a saisi le juge des référés du tribunal administratif de Strasbourg, sur le fondement de l'article L. 521-3 du code de justice administrative, d'une demande tendant à l'expulsion de M. A...de la maison forestière du Danielsrain. Ce dernier se pourvoit en cassation contre l'ordonnance du 29 juillet 2016 par lequel ce juge a fait droit à cette demande. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Il ressort de la délibération du conseil municipal de la commune de Sélestat du 3 mars 1961 que la maison forestière du Danielsrain, qui appartient à cette commune, a fait l'objet d'une concession par nécessité absolue de service aux agents de l'Office national des forêts affectés au poste de triage correspondant. La circonstance qu'à la suite d'une réorganisation des postes de triage de cet office en 2002, la commune de Sélestat soit sortie du périmètre du poste de l'agent occupant cette maison et qu'ainsi ce dernier ne rende pas directement de services à cette commune, est sans incidence sur l'affectation du bâtiment à cette fin. Dès lors, la juridiction administrative n'est pas manifestement compétente pour connaître du présent litige.<br/>
<br/>
              3. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ".<br/>
<br/>
              4. Il résulte de ces dispositions que lorsque le juge des référés est saisi, sur le fondement de ces dispositions, d'une demande d'expulsion d'un occupant d'un logement concédé par nécessité absolue de service, y compris lorsque celui-ci ne fait pas partie du domaine public de la personne publique propriétaire, il lui appartient de rechercher si, au jour où il statue, cette demande présente un caractère d'urgence et ne se heurte à aucune contestation sérieuse. S'agissant de cette dernière condition, dans le cas où la demande d'expulsion fait suite à la décision du gestionnaire ou du propriétaire du logement de retirer ou de refuser de renouveler le titre dont bénéficiait l'occupant et où, alors que cette décision exécutoire n'est pas devenue définitive, l'occupant en conteste devant lui la validité, le juge des référés doit rechercher si, compte tenu tant de la nature que du bien-fondé des moyens ainsi soulevés à l'encontre de cette décision, la demande d'expulsion doit être regardée comme se heurtant à une contestation sérieuse.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Strasbourg que l'arrêté du 23 octobre 2015 prononçant la mutation d'office de M. A...dans l'intérêt du service doit être regardée comme une décision de l'Office national des forêts retirant le titre dont disposait jusqu'alors cet agent pour l'occupation de la maison forestière du Danielsrain. Cette décision a fait l'objet d'un recours, alors pendant, devant le tribunal administratif de Strasbourg et n'était, dès lors, pas devenue définitive. Par suite, en jugeant que ni l'existence d'un recours formé par M. A...contre l'arrêté du 23 octobre 2015 prononçant sa mutation d'office, ni l'existence alléguée de moyens sérieux d'annulation de cette décision ne pouvaient faire obstacle à la mesure d'expulsion sollicitée par la commune de Sélestat, sans rechercher si, compte tenu tant de la nature que du bien-fondé de ces moyens, la demande de la commune devait être regardée comme se heurtant à une contestation sérieuse, le juge des référés du tribunal de Strasbourg a commis une erreur de droit. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur l'intervention de l'Office national des forêts :<br/>
<br/>
              7. L'Office national des forêts a intérêt à l'expulsion de M. A...de la maison forestière du Danielsrain, laquelle est concédée à l'agent occupant le poste de triage correspondant. M. A...soutient qu'en l'absence de nomination d'un nouveau conseil d'administration de l'Office national des forêts, lequel, en vertu de l'article D. 222-7 du code forestier, délibère sur les actions en justice, et, s'il a été nommé entre temps, en l'absence de renouvellement de la délégation qui avait été donnée au directeur général de l'Office le 27 mars 2013 conformément aux dispositions de  l'article D. 222-8 du même code, il n'est pas établi que le  représentant légal  de l'Office national des forêts indiqué dans la requête ait eu qualité pour agir au nom de cet organisme. Toutefois, eu égard au caractère d'urgence qui s'attache à l'action en référé de l'article L. 521-3 du code de justice administrative, le chef du département juridique de l'Office, qui avait reçu délégation du directeur général par décision du 27 juillet 2015, qui lui-même avait reçu délégation du conseil d'administration conformément aux articles D. 222-7 et D. 222-8 du code forestier, avait qualité pour intervenir dans la présente requête en référé, quand bien même un nouveau conseil d'administration n'aurait pas été nommé ou, s'il avait été nommé, n'aurait pas encore renouvelé la délégation accordée au directeur général de l'Office national des forêts. Il en résulte que l'intervention de l'Office national des forêts à l'appui de la requête de la commune de Sélestat doit être admise.<br/>
<br/>
              Sur la qualité pour agir de la commune de Sélestat :<br/>
<br/>
              8. M. A...soutient que la commune de Sélestat n'a pas qualité pour demander son expulsion au motif que les communes de Kintzheim, Ohnenheim, Muttershlotz, Hilsenheim, Ebermunster et Ebersheim, seraient devenues locataires de la maison forestière du Danielsrain, mise à sa disposition. Toutefois, si par une délibération du 29 novembre 2012, le conseil municipal de la commune de Sélestat a fixé un loyer de référence de cette maison dans le but de déterminer le montant de la participation des communes de Kintzheim, Châtenois et Orschwiller, dont les forêts sont situées dans le ressort de la maison forestière de Danielsrain, aux frais d'entretien de cette dernière, cette délibération n'a eu  ni pour objet ni pour effet de conférer à ces communes la qualité de locataires de la maison.  Par suite, la fin de non recevoir soulevée par M. A...ne peut, en tout état de cause, qu'être écartée.<br/>
<br/>
              Sur la demande d'expulsion :<br/>
<br/>
              9. D'une part, la maison forestière de Danielsrain est un logement concédé par nécessité absolue de service qui doit impérativement pouvoir être occupé par le successeur de M. A..., pour les besoins bon accomplissement de ses missions. Contrairement à ce que soutient M.A..., son successeur a bien été nommé sur le poste 2054 dans l'unité territoriale de Sélestat, qu'occupait M. A...avant sa nouvelle affectation, ainsi qu'il ressort de l'arrêté du 18 mars 2016 prononçant sa mutation. La circonstance que le successeur de M. A... aurait la possibilité d'être logé chez sa compagne, ou dans d'autres immeubles de la commune de Sélestat ou que l'arrêté du 18 mars 2016 comporte la mention " résidence administrative : Sélestat " est sans influence sur la nécessité absolue qui s'attache à sa présence dans la maison forestière du Danielsrain. Le maintien de M. A...compromet ainsi le bon fonctionnement du service et la demande d'expulsion formée par la commune de Selestat présente un caractère d'utilité et d'urgence.<br/>
<br/>
              10. D'autre part, il ressort ce qui a été dit au point 1 que M. A...ne dispose plus d'aucun droit ni titre à occuper la maison forestière qui lui avait été concédée par nécessité absolue de service. Si, par arrêté du 17 novembre 1965, le maire de la commune de Sélestat a fixé à trois mois le délai d'évacuation en cas de mutation, ce délai est expiré, M. A...ayant été nommé à partir du 16 novembre 2015 sur un autre poste. En outre, les circonstances que son procès verbal de désinstallation aurait été dressé irrégulièrement en son absence ou qu'il n'aurait pas bénéficié d'un congé de désinstallation sont, en tout état de cause, sans incidence sur la légalité de la mesure d'expulsion demandée. De même, la circonstance que M. A...se soit trouvé ou se trouve encore en arrêt maladie n'est pas de nature à faire obstacle à son expulsion dès lors qu'en tout état de cause, son placement dans cette situation est postérieur à l'expiration de son titre d'occupation de la maison forestière du Danielsrain. Pour s'opposer à la demande de la commune de Sélestat, M. A...se prévaut, par ailleurs, de l'illégalité dont serait entachée, selon lui, la décision de mutation d'office dans l'intérêt du service du 23 octobre 2015 dont il a fait l'objet, contre laquelle il a formé un recours pour excès de pouvoir devant le tribunal administratif de Strasbourg, toujours pendant. Il soutient, tout d'abord, qu'il n'a pas obtenu communication du rapport du délégué territorial en date du 11 septembre 2015 et qu'il s'est vu refuser la communication préalable de son dossier. Il affirme également que la composition de la commission administrative paritaire consultée avant l'intervention de cette décision était irrégulière. Il fait valoir, enfin, que la décision de mutation d'office prise à son encontre a le caractère d'une sanction disciplinaire déguisée, qu'elle n'est pas justifiée dès lors qu'il n'avait plus de contact avec ceux de ses collègues à l'origine de la plainte déposée à son encontre et sur laquelle la décision repose et qu'aucun fait objectif ne lui était imputable, qu'elle est insuffisamment motivée, que sa motivation est en contradiction avec l'appréciation portée sur sa manière de servir lors de l'entretien annuel du 30 mars 2015 et qu'elle est entachée de détournement de pouvoir. En l'état de l'instruction, aucun de ces moyens ne peut être regardé comme soulevant, dans les circonstances de l'espèce, une contestation sérieuse de la mesure d'expulsion demandée par la commune de Sélestat.<br/>
<br/>
              11. Dès lors, il y a lieu d'enjoindre à  M. A...et à tous occupants de son chef de libérer la maison forestière du Danielsrain, M. A...n'étant, en tout état de cause, pas fondé à demander qu'un délai lui soit accordé en application des articles  L. 613-1 du code de la construction et de l'habitation, et L. 412-4 du code des procédures civiles d'exécution dès lors qu'il est un agent public dont le service implique, par nécessité absolue, qu'il loge dans la maison forestière attachée au poste de Wolfgantzen, où il a été muté. Il y a lieu d'assortir cette injonction d'une astreinte de 300 euros par jour de retard à compter de l'expiration d'un délai de vingt jours à compter de la notification de la présente décision.<br/>
<br/>
              Sur les conclusions de la commune de Sélestat tendant à la réparation de son préjudice :<br/>
<br/>
               12. Il n'entre pas dans l'office du juge des référés, qui ne peut prononcer que des mesures provisoires, de procéder à l'indemnisation de la commune de Sélestat à raison des préjudices qu'elle aurait subis du fait du maintien sans droit ni titre de M. A...dans la maison forestière sans acquitter de loyer.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 2 000 euros à verser à la commune de Sélestat et la somme de 2 000 euros à verser à l'Office national des forêts, au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Sélestat ou de l'Office national des forêts, qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : L'ordonnance en date du 29 juillet 2016 du juge des référés du tribunal administratif  de Strasbourg  est annulée.<br/>
<br/>
Article 2 : L'intervention de l'Office national des forêts est admise. <br/>
<br/>
Article 3 : Il est enjoint à M. A...et à tous occupants de son chef de quitter la maison forestière du Danielsrain qu'il occupe irrégulièrement à La Vancelle-gare et qui appartient à la commune de Sélestat, sous astreinte de 300 euros par jour de retard à compter de l'expiration d'un délai de vingt jours à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : M. A...versera à la commune de Sélestat et à l'Office national des forêts la somme de 2 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions de la demande est rejeté.<br/>
<br/>
Article 6 : Les conclusions de M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 7 : La présente décision sera notifiée à M. B...A..., à la commune de Sélestat et à l'Office national des forêts.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - DEMANDE PRÉSENTÉE DEVANT LE JUGE DES RÉFÉRÉS TENDANT À L'EXPULSION D'UN LOCAL OCCUPÉ POUR NÉCESSITÉ ABSOLUE DE SERVICE - INCOMPÉTENCE MANIFESTE DE LA JURIDICTION ADMINISTRATIVE - ABSENCE, Y COMPRIS DANS L'HYPOTHÈSE OÙ CE LOCAL N'APPARTIENDRAIT PAS AU DOMAINE PUBLIC [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. - DEMANDE D'EXPULSION D'UN OCCUPANT D'UN LOGEMENT CONCÉDÉ PAR NÉCESSITÉ ABSOLUE DE SERVICE, Y COMPRIS LORSQUE CELUI-CI NE FAIT PAS PARTIE DU DOMAINE PUBLIC - OFFICE DU JUGE DES RÉFÉRÉS (ART. L. 521-3 DU CJA) - 1) OBLIGATION DE RECHERCHER SI LA DEMANDE PRÉSENTE UN CARACTÈRE D'URGENCE ET NE SE HEURTE À AUCUNE CONTESTATION SÉRIEUSE - 2) APPRÉCIATION DU CARACTÈRE SÉRIEUX DE LA CONTESTATION - CONTESTATION DE LA VALIDITÉ DU RETRAIT OU DU REFUS DE RENOUVELLEMENT DU TITRE - OBLIGATION POUR LE JUGE DE TENIR COMPTE TANT DE LA NATURE QUE DU BIEN-FONDÉ DES MOYENS SOULEVÉS À L'ENCONTRE DE LA DÉCISION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">24-02-02 DOMAINE. DOMAINE PRIVÉ. RÉGIME. - DEMANDE D'EXPULSION D'UN OCCUPANT D'UN LOGEMENT CONCÉDÉ PAR NÉCESSITÉ ABSOLUE DE SERVICE, Y COMPRIS LORSQUE CELUI-CI NE FAIT PAS PARTIE DU DOMAINE PUBLIC - OFFICE DU JUGE DES RÉFÉRÉS (ART. L. 521-3 DU CJA) - 1) OBLIGATION DE RECHERCHER SI LA DEMANDE PRÉSENTE UN CARACTÈRE D'URGENCE ET NE SE HEURTE À AUCUNE CONTESTATION SÉRIEUSE - 2) APPRÉCIATION DU CARACTÈRE SÉRIEUX DE LA CONTESTATION - CONTESTATION DE LA VALIDITÉ DU RETRAIT OU DU REFUS DE RENOUVELLEMENT DU TITRE - OBLIGATION POUR LE JUGE DE TENIR COMPTE TANT DE LA NATURE QUE DU BIEN-FONDÉ DES MOYENS SOULEVÉS À L'ENCONTRE DE LA DÉCISION [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">24-02-03-01 DOMAINE. DOMAINE PRIVÉ. CONTENTIEUX. COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE. - DEMANDE PRÉSENTÉE DEVANT LE JUGE DES RÉFÉRÉS TENDANT À L'EXPULSION D'UN LOCAL OCCUPÉ POUR NÉCESSITÉ ABSOLUE DE SERVICE - INCOMPÉTENCE MANIFESTE DE LA JURIDICTION ADMINISTRATIVE - ABSENCE, Y COMPRIS DANS L'HYPOTHÈSE OÙ CE LOCAL N'APPARTIENDRAIT PAS AU DOMAINE PUBLIC [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-035-04-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - DEMANDE D'EXPULSION D'UN OCCUPANT D'UN LOGEMENT CONCÉDÉ PAR NÉCESSITÉ ABSOLUE DE SERVICE, Y COMPRIS LORSQUE CELUI-CI NE FAIT PAS PARTIE DU DOMAINE PUBLIC - 1) OBLIGATION DE RECHERCHER SI LA DEMANDE PRÉSENTE UN CARACTÈRE D'URGENCE ET NE SE HEURTE À AUCUNE CONTESTATION SÉRIEUSE - 2) APPRÉCIATION DU CARACTÈRE SÉRIEUX DE LA CONTESTATION - CONTESTATION DE LA VALIDITÉ DU RETRAIT OU DU REFUS DE RENOUVELLEMENT DU TITRE - OBLIGATION POUR LE JUGE DE TENIR COMPTE TANT DE LA NATURE QUE DU BIEN-FONDÉ DES MOYENS SOULEVÉS À L'ENCONTRE DE LA DÉCISION [RJ2].
</SCT>
<ANA ID="9A"> 17-03-02 La juridiction administrative n'est pas manifestement incompétente pour connaître d'une demande  d'expulsion d'un local occupé pour nécessité absolue de service présentée devant le juge des référés saisi sur le fondement de l'article L. 521-3 du code de justice administrative, y compris dans l'hypothèse où ce local n'appartiendrait pas au domaine public.</ANA>
<ANA ID="9B"> 24-01-02-01 1) ll résulte de l'article L. 521-3 du code de justice administrative (CJA) que lorsque le juge des référés est saisi, sur ce fondement, d'une demande d'expulsion d'un occupant d'un logement concédé par nécessité absolue de service, y compris lorsque celui-ci ne fait pas partie du domaine public de la personne publique propriétaire, il lui appartient de rechercher si, au jour où il statue, cette demande présente un caractère d'urgence et ne se heurte à aucune contestation sérieuse.... ,,2) S'agissant de cette dernière condition, dans le cas où la demande d'expulsion fait suite à la décision du gestionnaire ou du propriétaire du logement de retirer ou de refuser de renouveler le titre dont bénéficiait l'occupant et où, alors que cette décision exécutoire n'est pas devenue définitive, l'occupant en conteste devant lui la validité, le juge des référés doit rechercher si, compte tenu tant de la nature que du bien-fondé des moyens ainsi soulevés à l'encontre de cette décision, la demande d'expulsion doit être regardée comme se heurtant à une contestation sérieuse.</ANA>
<ANA ID="9C"> 24-02-02 1) ll résulte de l'article L. 521-3 du code de justice administrative (CJA) que lorsque le juge des référés est saisi, sur ce fondement, d'une demande d'expulsion d'un occupant d'un logement concédé par nécessité absolue de service, y compris lorsque celui-ci ne fait pas partie du domaine public de la personne publique propriétaire, il lui appartient de rechercher si, au jour où il statue, cette demande présente un caractère d'urgence et ne se heurte à aucune contestation sérieuse.... ,,2) S'agissant de cette dernière condition, dans le cas où la demande d'expulsion fait suite à la décision du gestionnaire ou du propriétaire du logement de retirer ou de refuser de renouveler le titre dont bénéficiait l'occupant et où, alors que cette décision exécutoire n'est pas devenue définitive, l'occupant en conteste devant lui la validité, le juge des référés doit rechercher si, compte tenu tant de la nature que du bien-fondé des moyens ainsi soulevés à l'encontre de cette décision, la demande d'expulsion doit être regardée comme se heurtant à une contestation sérieuse.</ANA>
<ANA ID="9D"> 24-02-03-01 La juridiction administrative n'est pas manifestement incompétente pour connaître d'une demande  d'expulsion d'un local occupé pour nécessité absolue de service présentée devant le juge des référés saisi sur le fondement de l'article L. 521-3 du code de justice administrative, y compris dans l'hypothèse où ce local n'appartiendrait pas au domaine public.</ANA>
<ANA ID="9E"> 54-035-04-04 1) Il résulte de l'article L. 521-3 du code de justice administrative (CJA) que lorsque le juge des référés est saisi, sur ce fondement, d'une demande d'expulsion d'un occupant d'un logement concédé par nécessité absolue de service, y compris lorsque celui-ci ne fait pas partie du domaine public de la personne publique propriétaire, il lui appartient de rechercher si, au jour où il statue, cette demande présente un caractère d'urgence et ne se heurte à aucune contestation sérieuse.... ,,2) S'agissant de cette dernière condition, dans le cas où la demande d'expulsion fait suite à la décision du gestionnaire ou du propriétaire du logement de retirer ou de refuser de renouveler le titre dont bénéficiait l'occupant et où, alors que cette décision exécutoire n'est pas devenue définitive, l'occupant en conteste devant lui la validité, le juge des référés doit rechercher si, compte tenu tant de la nature que du bien-fondé des moyens ainsi soulevés à l'encontre de cette décision, la demande d'expulsion doit être regardée comme se heurtant à une contestation sérieuse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 février 2000, Région de Bourgogne, n° 188954, T. pp. 904-1156.,   ,[RJ2] Cf., s'agissant d'une demande d'expulsion d'un occupant du domaine public, CE, Section, 16 mai 2003, SARL Icomatex, n° 249880, p. 228.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
