<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625075</ID>
<ANCIEN_ID>JG_L_2015_05_000000383222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 22/05/2015, 383222, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383222.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...D...a demandé au tribunal administratif de Limoges d'annuler l'inscription de M. B...C...sur les listes électorales de la commune de Villegouin (Indre) et l'élection de l'intéressé en qualité de conseiller municipal à l'issue du premier tour des élections municipales qui se sont déroulées le 23 mars 2014 dans cette commune.<br/>
<br/>
              Par un jugement n° 1400629 du 26 juin 2014, le tribunal administratif de Limoges a rejeté les conclusions de M. D...tendant à la radiation de M. C...des listes électorales comme portées devant un ordre de juridiction incompétent pour en connaître, mais a annulé l'élection de M. C...en qualité de conseiller municipal.  <br/>
<br/>
              Par une requête enregistrée le 26 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. C...demande : <br/>
<br/>
              1°) d'annuler ce jugement du 26 juin 2014, en tant qu'il a annulé son élection en qualité de conseiller municipal ;<br/>
<br/>
              2°) de rejeter la protestation de M. D...;<br/>
<br/>
              3°) de mettre à la charge de M. A...D...la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code électoral ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code général des impôts ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. C...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. D...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue du premier tour de scrutin des élections municipales qui se sont déroulées le 23 mars 2014, M. C...a été élu conseiller municipal de la commune de Villegouin ; qu'à la demande de M.D..., candidat non élu, le tribunal administratif de Limoges a, par un jugement du 26 juin, annulé l'élection de M.C..., mais rejeté les conclusions du requérant tendant à la radiation des listes électorales de M. C...comme portées devant une juridiction incompétente pour en connaître ; que M. C...relève appel de ce jugement, en tant qu'il a annulé son élection ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Considérant, d'une part, qu'il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119, R. 120 et R. 121 du code électoral relatifs aux délais impartis pour l'instruction et le jugement des protestations dirigées contre l'élection des conseillers municipaux, que, par dérogation aux dispositions de l'article R. 611-5 du code de justice administrative, le tribunal administratif n'est pas tenu de notifier aux conseillers dont l'élection est contestée les pièces jointes produites à l'appui de la protestation ; qu'il appartient seulement au tribunal administratif, une fois ces pièces enregistrées, de les tenir à la disposition des parties de sorte que celles-ci soient à même, si elles l'estiment utile, d'en prendre connaissance ; qu'en l'espèce, il n'est pas contesté que les photographies jointes au mémoire en réplique du 18 avril 2014 de M. D...ont été tenues, au greffe du tribunal administratif, à la disposition des conseillers dont l'élection était contestée ; que, par suite, M. C... n'est pas fondé à soutenir que le jugement attaqué serait intervenu en méconnaissance du principe du caractère contradictoire de la procédure juridictionnelle ;<br/>
<br/>
              3. Considérant, d'autre part, que le moyen tiré de ce que le jugement du 26 juin 2014 ne comporterait pas l'ensemble des signatures exigées par l'article R. 741-7 du code de justice administrative manque en fait ; <br/>
<br/>
              Sur le bien fondé du jugement attaqué :<br/>
<br/>
              4. Considérant qu'aux termes du deuxième alinéa de l'article L. 228 du code électoral : " Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection. " ; que l'article L. 11 du même code dispose que : " Sont inscrits sur la liste électorale, sur leur demande : 1° Tous les électeurs qui ont leur domicile réel dans la commune ou y habitent depuis six mois au moins ; 2° Ceux qui figurent pour la cinquième fois sans interruption, l'année de la demande d'inscription, au rôle d'une des contributions directes communales et, s'ils ne résident pas dans la commune, ont déclaré vouloir y exercer leurs droits électoraux (...) " ; qu'aux termes de l'article 1407 du code général des impôts : " La taxe d'habitation est due : 1° Pour tous les locaux meublés affectés à l'habitation (...). " ; que les articles 1408 et 1415 du même code précisent, respectivement, que " la taxe est établie au nom des personnes qui ont, à quelque titre que ce soit, la disposition ou la jouissance des locaux imposables " et que cette taxe est due pour l'année entière, d'après les faits existants au 1er janvier de l'année d'imposition ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que M. C...a conclu le 23 décembre 2013 un contrat de prêt à usage, enregistré le 27 décembre suivant au centre des finances publiques de Châteauroux, en vue de l'occupation d'une maison d'habitation située à Villegouin ; qu'il résulte de l'instruction, et notamment des photographies produites en appel, que cette habitation, meublée, avait le caractère d'un local affecté à l'habitation au sens des dispositions qui précèdent, en dépit de sa vétusté et de son état d'entretien ; qu'ainsi M.C..., dont les liens anciens avec la commune de Villegouin sont au demeurant attestés par ses précédentes fonctions de maire et de secrétaire général de la mairie, avait vocation, en application des articles 1407 et 1408 du code général des impôts, à être inscrit au rôle des contributions directes de la commune de Villegouin au 1er janvier 2014 ; qu'il devait par suite être regardé comme éligible ; qu'il en résulte que M. C...est fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Limoges, se fondant sur l'existence d'une manoeuvre dans la conclusion du contrat de prêt à usage précité, a annulé son élection en qualité de conseiller municipal de Villegouin ; qu'aucun autre grief n'a été soulevé par M. D...dans les conclusions de sa protestation dirigées contre l'élection de M. C...; qu'il y a donc lieu de valider son élection ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. D...le versement à M. C...d'une somme en application de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce que soit mis à la charge de M.C..., qui n'est pas dans la présente instance la partie perdante, la somme que demande M. D...au titre des frais non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Limoges du 26 juin 2014 est annulé en tant qu'il a annulé l'élection de M. C...en qualité de conseiller municipal de Villegouin. <br/>
<br/>
Article 2 : L'élection de M. C...en qualité de conseiller municipal de Villegouin est validée. <br/>
Article 3 : Les conclusions de M. A...D...tendant à l'annulation de l'élection de M. C... en qualité de conseiller municipal de Villegouin et les conclusions présentées par M. C... et M. D...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...C..., à M. A...D...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
