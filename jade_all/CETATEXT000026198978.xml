<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026198978</ID>
<ANCIEN_ID>JG_L_2012_07_000000346349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/19/89/CETATEXT000026198978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 16/07/2012, 346349, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Christine Maugüé</PRESIDENT>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:346349.20120716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 février et 2 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Société Saria Industries, dont le siège est au 17, avenue d'Italie à Illzach (68315) ; la Société Saria Industries demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08NC00049 du 6 décembre 2010 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0300377 en date du 25 octobre 2007 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une somme de 1 574 806,56 euros, augmentée des intérêts au taux légal à compter du 1er octobre 2002 et de la capitalisation de ces intérêts, d'autre part, à la condamnation de l'Etat à lui verser une somme de 499 335,90 euros augmentée des intérêts au taux légal à compter du 1er octobre 2002 et de leur capitalisation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 2000-1166 du 1er décembre 2000 ;<br/>
<br/>
              Vu le décret n° 2001-231 du 16 mars 2001 ;<br/>
<br/>
              Vu le décret n° 2001-723 du 31 juillet 2001 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de la Société Saria Industries.<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de la Société Saria Industries ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par une première convention signée le 31 juillet 2001, la Société Saria Industries et l'Etat ont fixé les modalités financières de l'élimination de farines animales ; que selon les termes mêmes de l'article 4 de cette convention, la requérante assurait l'élimination des farines dans des cimenteries situées en Belgique et pouvait, dès lors, prétendre à une indemnisation suivant le barème de l'annexe II du décret du 16 mars 2001 instituant une mesure d'indemnisation pour les entreprises propriétaires de matériels à risques spécifiés et modifiant le décret n° 2000-1166 du 1er décembre 2000 instituant une mesure d'indemnisation pour les entreprises productrices de certaines farines et graisses ; que cette convention n'a pu être exécutée à la suite de l'opposition des autorités belges ; qu'une seconde convention, signée le 19 septembre 2001, ne fait que reprendre les termes de la première convention en remplaçant les cimenteries situées en Belgique par des cimenteries situées en France ; que, toutefois, alors que le barème de l'annexe II du décret du 1er décembre 2000 modifié par le décret du 16 mars 2001 avait été modifié par le décret n° 2001-723 du 31 juillet 2001, il ressort des termes comparés des deux conventions que l'Etat a omis de modifier le barème de référence applicable ; que l'Etat a néanmoins fait application du barème modifié par le décret du 31 juillet 2001, moins favorable à la Société Saria Industries ; que cette dernière a demandé au tribunal administratif de Strasbourg de condamner l'Etat à lui verser la somme correspondant à la différence entre les paiements effectués et les sommes qu'elle aurait reçues en application du barème issu du décret du 16 mars 2001 ; que le tribunal administratif de Strasbourg a rejeté sa demande par un jugement du 25 octobre 2007 ; que la Société Saria Industries se pourvoit en cassation contre l'arrêt du 6 décembre 2010 par lequel la cour administrative d'appel de Nancy a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant que devant les juges du fond la Société Saria Industries avait, à titre principal, recherché la responsabilité de l'Etat sur le fondement de sa responsabilité contractuelle, lui reprochant de ne pas avoir respecté ses engagements contractuels en n'appliquant pas le barème défini par la convention du 19 septembre 2001; qu'à titre subsidiaire, elle avait également recherché la responsabilité de l'Etat pour la faute qu'il aurait commise en l'induisant en erreur sur le barême de la tarification à laquelle elle pouvait prétendre ; que la cour administrative d'appel de Nancy a rejeté le premier terrain des conclusions en jugeant que l'Etat avait exécuté les stipulations contractuelles de la convention du 19 septembre 2001 ; qu'elle a en revanche omis de statuer sur les conclusions tendant à l'engagement de la responsabilité quasi délictuelle de l'Etat pour faute résultant de l'inclusion dans la convention d'un tarif différent du tarif réglementaire ; qu'il résulte de ce qui précède que la Société Saria Industries est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la Société Saria Industries, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 6 décembre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat versera à la Société Saria Industries une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Société Saria Industries et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
