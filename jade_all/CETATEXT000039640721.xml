<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039640721</ID>
<ANCIEN_ID>JG_L_2019_12_000000431696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/07/CETATEXT000039640721.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/12/2019, 431696</TITRE>
<DATE_DEC>2019-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:431696.20191218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sunrock a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la procédure de passation d'un accord-cadre mono-attributaire à bons de commande, ayant pour objet la fourniture de pistolets semi-automatiques de calibre 9 x 19 mm et de leurs étuis, de porte-chargeurs et de prestations annexes, lancé par la direction générale des infrastructures, des transports et de la mer du ministère de la transition écologique et solidaire afin de répondre aux besoins du dispositif de contrôle et de surveillance des affaires maritimes. Par une ordonnance n° 1905292 du 29 mai 2019, le juge des référés du tribunal administratif de Cergy-Pontoise a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 14 juin et 22 août 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre de la transition écologique et solidaire demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Sunrock.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ; <br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le décret n° 2016-361 du 25 mars 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice, <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société Sunrock ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la direction générale des infrastructures, des transports et de la mer du ministère de la transition écologique et solidaire a lancé une consultation, par un avis d'appel à la concurrence envoyé à la publication le 27 mars 2019, en vue de l'attribution, selon une procédure adaptée, d'un accord-cadre mono-attributaire à bons de commande ayant pour objet la fourniture de pistolets semi-automatiques de calibre 9 x 19 mm et de leurs étuis, de porte-chargeurs et de prestations annexes, afin de répondre aux besoins du dispositif de contrôle et de surveillance des affaires maritimes. La date limite de remise des offres était fixée au 26 avril 2019. Le 25 avril 2019, la société Sunrock a demandé au juge des référés l'annulation de la procédure de passation de ce marché, au motif que la rédaction des " spécifications techniques " l'empêchait de se porter utilement candidate. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Cergy-Pontoise, saisi sur le fondement de l'article L. 551-1 du code de justice administrative, a annulé la procédure de passation de ce marché.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) ". Aux termes de l'article L. 551-2 du même code : " I. - Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, (...). / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. / II. - Toutefois, le I n'est pas applicable aux contrats passés dans les domaines de la défense ou de la sécurité au sens de l'article 6 de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics. / Pour ces contrats, il est fait application des articles L. 551-6 et L. 551 7 ". Aux termes de l'article L. 551-6 de ce code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations en lui fixant un délai à cette fin. Il peut lui enjoindre de suspendre l'exécution de toute décision se rapportant à la passation du contrat (...) ". <br/>
<br/>
              3. En premier lieu, aux termes de l'article 2 de la directive 2009/81/CE du 13 juillet 2009 relative à la coordination des procédures de passation de certains marchés de travaux, de fournitures et de services par les pouvoirs adjudicateurs ou entités adjudicatrices dans les domaines de la défense et de la sécurité : " (...) la présente directive s'applique aux marchés passés dans les domaines de la défense et de la sécurité ayant pour objet : a) la fourniture d'équipements militaires ; b) la fourniture d'équipements sensibles (...) ". L'article 1er de la directive définit l'" équipement militaire " comme " un équipement spécifiquement conçu ou adapté à des fins militaires, destiné à être utilisé comme arme, munitions ou matériel de guerre " et l' " équipement sensible " comme un équipement destiné à des fins de sécurité faisant intervenir, nécessitant ou comportant des informations classifiées. Aux termes du neuvième considérant de la directive, les exigences particulières auxquelles doit être soumis l'achat d'équipements de défense et de sécurité, compte tenu de leur caractère crucial pour la sécurité et la souveraineté des Etats membres, " concernent surtout les achats d'armes, de munitions et de matériels de guerre (...) mais aussi certains achats particulièrement sensibles dans le domaine de la sécurité non militaire ". Aux termes de son dixième considérant : " Par " équipements militaires ", au sens de la présente directive, il faudrait entendre notamment les types de produits visés par la liste d'armes, de munitions et de matériel de guerre adoptée par la décision n° 255/58 du Conseil du 15 avril 1958 et les Etats membres peuvent se limiter à utiliser cette seule liste pour la transposition de la présente directive. Cette liste ne comprend que les équipements qui sont conçus, développés et produits à des fins spécifiquement militaires. (...) Au sens de la présente directive, le terme " équipement militaire " devrait couvrir également les produits qui, bien qu'initialement conçus pour une utilisation civile, ont ensuite été adaptés à des fins militaires pour pouvoir être utilisés comme armes, munitions ou matériel de guerre ". La liste figurant dans la décision n° 255/58 du Conseil du 15 avril 1958 inclut notamment les " armes à feu portatives et automatiques, telles que fusils, carabines, revolvers, pistolets, mitraillettes et mitrailleuses, à l'exception des armes de chasse, pistolets et autres armes à petit calibre, d'un diamètre inférieur à 7 mm ". Aux termes de l'article 6 de l'ordonnance du 23 juillet 2015 relative aux marchés publics, alors applicable, qui assurait la transposition des dispositions de la directive : " Les marchés publics de défense ou de sécurité sont les marchés publics passés par l'Etat ou ses établissements publics ayant un caractère autre qu'industriel et commercial et ayant pour objet : / 1° La fourniture d'équipements, y compris leurs pièces détachées, composants ou sous assemblages, qui sont destinés à être utilisés comme armes, munitions ou matériel de guerre, qu'ils aient été spécifiquement conçus à des fins militaires ou qu'ils aient été initialement conçus pour  une utilisation civile puis adaptés à des fins militaires ; 2° La fourniture d'équipements destinés à la sécurité (...) et qui font intervenir, nécessitent ou comportent des supports ou informations protégées ou classifiées dans l'intérêt de la sécurité nationale (...) ". Il résulte de ce qui précède qu'au titre du 1° de l'article 6 de l'ordonnance du 23 juillet 2015, seuls les achats, par l'Etat ou par ses établissements publics, pour les besoins de la défense ou de la sécurité nationale, d'équipements conçus ou adaptés à des fins spécifiquement militaires, sont soumis à des exigences particulières justifiant le régime dérogatoire applicable aux marchés de défense et de sécurité. La circonstance que des équipements figurent sur la liste établie par la décision n° 255/58 du conseil du 15 avril 1958 ne suffit pas, à elle seule, pour qualifier les marchés de fourniture de ces équipements de marchés de défense et de sécurité. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du référé précontractuel que le marché contesté a été conclu pour doter le service compétent des moyens matériels destinés à l'exercice de missions de police en mer. Un tel marché ne peut être regardé comme un marché de défense et de sécurité au sens de l'ordonnance du 23 juillet 2015. Par suite, le moyen tiré de ce que le juge du référé précontractuel aurait commis une erreur de droit en annulant la procédure, en méconnaissance des dispositions précitées des articles L 551-2 et L. 551-6 du code de justice administrative, doit être écarté. <br/>
<br/>
              5. Le ministre soutient, il est vrai, à titre subsidiaire, qu'à supposer que le marché ne puisse pas être regardé comme un marché de défense et de sécurité, le juge des référés a entaché son ordonnance d'erreur de droit en faisant application des dispositions du décret du 25 mars 2016 relatif aux marchés de défense et de sécurité. Toutefois, si l'ordonnance attaquée mentionne les dispositions de l'article 6 de ce décret relatives aux spécifications techniques, ces dispositions sont identiques à celles de l'article 8 du décret du même jour relatif aux marchés publics. L'erreur commise par le juge des référés sur le texte applicable est donc, en l'espèce, dépourvue d'incidence sur le bien-fondé de l'ordonnance attaquée.<br/>
<br/>
              6. En deuxième lieu, aux termes de l'article 8 du décret du 25 mars 2016 relatif aux marchés publics : " Les spécifications techniques ne peuvent pas faire mention d'un mode ou procédé de fabrication particulier ou d'une provenance ou origine déterminée, ni faire référence à une marque, à un brevet ou à un type lorsqu'une telle mention ou référence est susceptible de favoriser ou d'éliminer certains opérateurs économiques ou certains produits. Toutefois, une telle mention ou référence est possible si elle est justifiée par l'objet du marché public ou, à titre exceptionnel, dans le cas où une description suffisamment précise et intelligible de l'objet du marché public n'est pas possible sans elle et à la condition qu'elle soit accompagnée des termes " ou équivalent ". <br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge du référé précontractuel qu'aux termes de l'article 4.1.1 II du cahier des clauses techniques particulières, les pistolets à fournir devaient être conformes aux " 28 spécifications techniques principales ". En jugeant que certaines des caractéristiques techniques qui devaient ainsi être respectées, comme " l'absence de pédale de sûreté à l'arrière de la poignée ", avaient pour effet d'exclure irrégulièrement a priori certains opérateurs, dont la société Sunrock, alors qu'elles n'étaient pas justifiées par l'objet du marché, le juge du référé précontractuel a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation, par une ordonnance suffisamment motivée sur ce point, sans qu'ait d'incidence à cet égard la circonstance qu'il ait qualifié ces caractéristiques techniques de modes ou procédés de fabrication au sens de l'article 6 du décret du 25 mars 2016.  <br/>
<br/>
              8. Il résulte de ce qui précède que la ministre de la transition écologique et solidaire n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à la société Sunrock au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de la transition écologique et solidaire est rejeté. <br/>
Article 2 : L'Etat versera à la société Sunrock une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la ministre de la transition écologique et solidaire et à la société Sunrock.<br/>
Copie en sera adressée au ministre de l'économie et des finances et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-20 ARMÉES ET DÉFENSE. - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ (ART. 6 DE L'ORDONNANCE DU 6 JUILLET 2015) - 1) NOTION - ACHATS PAR L'ETAT OU PAR SES ÉTABLISSEMENTS PUBLICS, POUR LES BESOINS DE LA DÉFENSE OU DE LA SÉCURITÉ NATIONALE, D'ÉQUIPEMENTS CONÇUS OU ADAPTÉS À DES FINS SPÉCIFIQUEMENT MILITAIRES - 2) APPLICATION - MARCHÉ PORTANT SUR DES MOYENS MATÉRIELS DESTINÉS À L'EXERCICE DE MISSIONS DE POLICE EN MER - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-13 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. MARCHÉS PUBLICS. - DIRECTIVE 2009/81/CE DU 13 JUILLET 2009 - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ (ART. 6 DE L'ORDONNANCE DU 6 JUILLET 2015) - 1) NOTION - ACHATS PAR L'ETAT OU PAR SES ÉTABLISSEMENTS PUBLICS, POUR LES BESOINS DE LA DÉFENSE OU DE LA SÉCURITÉ NATIONALE, D'ÉQUIPEMENTS CONÇUS OU ADAPTÉS À DES FINS SPÉCIFIQUEMENT MILITAIRES - 2) APPLICATION - MARCHÉ PORTANT SUR DES MOYENS MATÉRIELS DESTINÉS À L'EXERCICE DE MISSIONS DE POLICE EN MER - EXCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-01-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. MARCHÉS. - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ (ART. 6 DE L'ORDONNANCE DU 6 JUILLET 2015) - 1) NOTION - ACHATS PAR L'ETAT OU PAR SES ÉTABLISSEMENTS PUBLICS, POUR LES BESOINS DE LA DÉFENSE OU DE LA SÉCURITÉ NATIONALE, D'ÉQUIPEMENTS CONÇUS OU ADAPTÉS À DES FINS SPÉCIFIQUEMENT MILITAIRES - 2) APPLICATION - MARCHÉ PORTANT SUR DES MOYENS MATÉRIELS DESTINÉS À L'EXERCICE DE MISSIONS DE POLICE EN MER - EXCLUSION.
</SCT>
<ANA ID="9A"> 08-20 1) Il résulte des articles 1er et 2 de la directive 2009/81/CE du 13 juillet 2009, éclairés par ses neuvième et dixième considérants, de la liste figurant dans la décision n° 255/58 du Conseil du 15 avril 1958 et de l'article 6 de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics qu'au titre du 1° de cet article 6, seuls les achats, par l'Etat ou par ses établissements publics, pour les besoins de la défense ou de la sécurité nationale, d'équipements conçus ou adaptés à des fins spécifiquement militaires, sont soumis à des exigences particulières justifiant le régime dérogatoire applicable aux marchés de défense et de sécurité. La circonstance que des équipements figurent sur la liste établie par la décision n° 255/58 du Conseil du 15 avril 1958 ne suffit pas, à elle seule, pour qualifier les marchés de fourniture de ces équipements de marchés de défense et de sécurité....  ,,2) Un marché portant sur des moyens matériels destinés à l'exercice de missions de police en mer ne peut être regardé comme un marché de défense et de sécurité au sens de l'ordonnance du 23 juillet 2015.</ANA>
<ANA ID="9B"> 15-05-13 1) Il résulte des articles 1er et 2 de la directive 2009/81/CE du 13 juillet 2009, éclairés par ses neuvième et dixième considérants, de la liste figurant dans la décision n° 255/58 du Conseil du 15 avril 1958 et de l'article 6 de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics qu'au titre du 1° de cet article 6, seuls les achats, par l'Etat ou par ses établissements publics, pour les besoins de la défense ou de la sécurité nationale, d'équipements conçus ou adaptés à des fins spécifiquement militaires, sont soumis à des exigences particulières justifiant le régime dérogatoire applicable aux marchés de défense et de sécurité. La circonstance que des équipements figurent sur la liste établie par la décision n° 255/58 du Conseil du 15 avril 1958 ne suffit pas, à elle seule, pour qualifier les marchés de fourniture de ces équipements de marchés de défense et de sécurité....  ,,2) Un marché portant sur des moyens matériels destinés à l'exercice de missions de police en mer ne peut être regardé comme un marché de défense et de sécurité au sens de l'ordonnance du 23 juillet 2015.</ANA>
<ANA ID="9C"> 39-01-03-02 1) Il résulte des articles 1er et 2 de la directive 2009/81/CE du 13 juillet 2009, éclairés par ses neuvième et dixième considérants, de la liste figurant dans la décision n° 255/58 du Conseil du 15 avril 1958 et de l'article 6 de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics qu'au titre du 1° de cet article 6, seuls les achats, par l'Etat ou par ses établissements publics, pour les besoins de la défense ou de la sécurité nationale, d'équipements conçus ou adaptés à des fins spécifiquement militaires, sont soumis à des exigences particulières justifiant le régime dérogatoire applicable aux marchés de défense et de sécurité. La circonstance que des équipements figurent sur la liste établie par la décision n° 255/58 du Conseil du 15 avril 1958 ne suffit pas, à elle seule, pour qualifier les marchés de fourniture de ces équipements de marchés de défense et de sécurité....  ,,2) Un marché portant sur des moyens matériels destinés à l'exercice de missions de police en mer ne peut être regardé comme un marché de défense et de sécurité au sens de l'ordonnance du 23 juillet 2015.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
