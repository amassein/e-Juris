<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035506189</ID>
<ANCIEN_ID>JG_L_2017_09_000000413733</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/50/61/CETATEXT000035506189.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/09/2017, 413733, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413733</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:413733.20170901</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 28 août 2017 au secrétariat du contentieux du Conseil d'Etat, la société MEI Partners demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision implicite de rejet née du silence gardé par le Premier ministre et par le ministre chargé du budget sur sa demande tendant au paiement d'une astreinte de 445 857 euros par jour jusqu'à la récupération par l'Etat d'une aide illégalement accordée à la Caisse des dépôts et consignations ; <br/>
<br/>
              2°) d'enjoindre à l'Etat de payer, avant le 21 septembre 2017, le montant hebdomadaire des astreintes, des pénalités et des frais accessoires, facturé à compter du 27 avril 2017, sur une compte dédié ouvert en son nom dans les livres de la Banque de France et portant l'intitulé " Compte provisoire pour MEI Partners " ;<br/>
<br/>
              3°) d'autoriser la Banque de France à communiquer, à titre d'information, à la requérante l'encours hebdomadaire de ce compte dédié ;<br/>
<br/>
              4°) d'ordonner que l'encours créditeur de ce compte puisse servir aux saisies-attributions prononcées à son encontre, aux fins de sursoir à l'émission d'avis à tiers détenteurs par le comptable public à l'encontre de la requérante ; <br/>
<br/>
              5°) d'ordonner que les mesures précédentes prennent fin au plus tard lorsqu'il sera statué sur la requête n° 409521 et qu'en cas d'annulation des décisions attaquées et de leurs effets, le solde créditeur de ce compte reviendra à la requérante, sans préjudice du quantum définitif ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              La société MEI Partners soutient que :<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, elle a subi un préjudice financier ainsi qu'une immobilisation d'actif importants, d'autre part, son équilibre financier est menacé à court terme, rendant probable une liquidation judiciaire à bref délai et, enfin, un intérêt public commande de recouvrer rapidement une aide d'un montant important ;<br/>
              - il existe un doute sérieux quand à la légalité de la décision implicite de rejet ;<br/>
              - l'aide accordée dans le cadre de la convention du 9 mai 2012 l'a été en méconnaissance de l'obligation de notification prescrite par l'article 108, paragraphe 3, du traité sur le fonctionnement de l'Union européenne ;<br/>
              - en ne récupérant pas l'aide illégale, le ministre en charge du budget et des comptes publics a décidé de maintenir un avantage financier illégalement constitué, alors qu'aucun motif d'intérêt général ne fait obstacle à la récupération de l'aide ; <br/>
              - l'inexécution de l'obligation de récupération de l'aide illégale et de paiement des astreintes l'a empêché de répondre à ses engagements ;<br/>
              - la décision attaquée ne peut se fonder sur l'ordonnance du juge des référés du tribunal administratif de Strasbourg 26 juin 2017 qui a interprété de manière erronée les termes de la demande dont il était saisi.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que la société MEI Partners a demandé à l'Etat, par des courriers adressés le 24 mars 2017 au Premier ministre et au ministre du budget, de récupérer auprès de la Caisse des dépôts et consignations (CDC) et de Bpifrance, au plus tard le 26 avril 2017, ce qu'elle estimait être une aide d'Etat illégale, d'un montant qu'elle fixait alors à 621 375 657 euros ; qu'elle soutenait, par ces mêmes courriers, qu'à défaut, une " astreinte journalière " de 445 587 euros devrait être " appliquée " à compter du 27 avril 2017 ; qu'une décision implicite de rejet de cette demande est née du silence conservé par l'administration pendant deux mois à compter de la réception de ces courriers ; que, par ailleurs, la société a saisi le Conseil d'Etat, le 3 avril 2017, sous le n° 409521, d'un recours pour excès de pouvoir contre la convention du 9 mai 2012 conclue entre l'Etat, la Caisse des dépôts et consignations et l'Agence de l'environnement et de la maîtrise de l'énergie, support, selon elle, de l'aide illégale en cause, recours qu'elle a assorti de conclusions tendant à ce que le Conseil d'Etat enjoigne à l'Etat, à compter du 27 avril 2017, à titre conservatoire, de récupérer cette aide sous astreinte de 445 857 euros par jour de retard ; que, par un mémoire en réplique enregistré le 7 août 2017 produit dans l'instance n° 409521, la société a étendu ses conclusions à l'annulation de la décision implicite de rejet du paiement de l'astreinte, qu'elle estime être intervenue le 27 juin 2017 ;<br/>
<br/>
              3. Considérant que, par la présente requête, la société MEI Partners demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite de rejet de sa demande de versement d'une astreinte journalière de 445 857 euros à compter du 27 avril 2017 ;<br/>
<br/>
              4. Considérant qu'aucune disposition ne prévoit qu'une personne publique, dans l'hypothèse où elle refuse de faire droit à une demande tendant à la récupération de sommes dont il est soutenu qu'elles constitueraient une aide d'Etat illégale, devrait s'acquitter, à titre conservatoire, et avant même qu'un juge ne se soit prononcé sur la légalité de ce refus, d'une " astreinte " dont le montant devrait bénéficier au demandeur ; qu'il en résulte qu'il est manifeste que la requête de la société MEI Partners, y compris ses conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative, ne peut être accueillie ; que, par suite, sans qu'il soit besoin de s'interroger sur la compétence en premier ressort du juge des référés du Conseil d'Etat, sa requête doit être rejetée selon la procédure prévue par l'article L. 522-3 du même code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société MEI Partners est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société MEI Partners.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
