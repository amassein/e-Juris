<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044331893</ID>
<ANCIEN_ID>JG_L_2021_11_000000430655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/18/CETATEXT000044331893.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 15/11/2021, 430655, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:430655.20211115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... C... a demandé au tribunal administratif de Montpellier de prononcer la décharge de l'obligation de payer les taxes d'urbanisme résultant de l'avis à tiers détenteur émis le 11 mars 2016 par le centre des finances publiques de Montpellier auprès de sa banque. Par un jugement n° 1603782 du 5 octobre 2018, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 19MA01730 du 10 mai 2019, enregistrée le 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application des dispositions de l'article R. 351-2 du code de justice administrative, le pourvoi présenté par Mme C... contre ce jugement. <br/>
<br/>
              Par ce pourvoi, enregistré au greffe de la cour administrative d'appel de Marseille le 14 avril 2019, un mémoire complémentaire et un mémoire en réplique, enregistrés le 2 septembre 2019 et 6 mai 2021 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP de Chaisemartin, Doumic-Seiller, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que le maire de Ganges (Hérault) a délivré un permis de construire à Mme C... le 18 juin 2008 pour l'extension d'une véranda et l'aménagement des combles d'un appartement situé dans cette commune. Les taxes d'urbanisme correspondantes ont fait l'objet d'un premier avis d'imposition de 1 329 euros, émis le 31 octobre 2008, et dont l'échéancier prévoyait deux versements de 720 et 609 euros à effectuer, respectivement, les 20 décembre 2009 et 20 juillet 2011. Le 24 septembre 2009, les services fiscaux ont émis un avis rectificatif d'imposition qui a porté la somme totale à payer à 1 587 euros, et qui prévoyait le versement d'une première échéance de 849 euros avant le 18 décembre 2009 et d'une seconde échéance de 738 euros avant le 20 juin 2011. Mme C... a reçu un avis d'échéance de 849 euros le 3 novembre 2009 dont elle s'est acquittée par un chèque du 17 décembre 2009. Elle a été rendue destinataire d'un avis à tiers détenteur en date du 15 avril 2013 pour un montant de 794 euros correspondant à l'échéance de 738 euros, majoré d'une somme de 56 euros en pénalités, qui a été annulé par un jugement, devenu définitif, du juge de l'exécution du tribunal de grande instance de Montpellier en date du 19 janvier 2015. Un second avis à tiers détenteur, en date du 11 mars 2016, a été adressé à nouveau à Mme C..., pour le même montant. Par un jugement du 5 octobre 2018, le tribunal administratif de Montpellier a rejeté la demande de Mme C... tendant la décharge de l'obligation de payer les taxes d'urbanisme résultant de l'avis à tiers détenteur émis le 11 mars 2016.<br/>
<br/>
              2. Le premier alinéa de l'article L. 274 du livre des procédures fiscales dispose que : " Les comptables publics des administrations fiscales qui n'ont fait aucune poursuite contre un redevable pendant quatre années consécutives à compter du jour de la mise en recouvrement du rôle ou de l'envoi de l'avis de mise en recouvrement sont déchus de tous droits et de toute action contre ce redevable ". <br/>
<br/>
              3. L'annulation contentieuse d'un avis à tiers détenteur a pour conséquence qu'il est réputé n'avoir jamais existé et fait, dès lors, obstacle à ce que lui soit attaché un effet interruptif de prescription. <br/>
<br/>
              4. Il résulte de ce qui a été dit au point 3 qu'en jugeant que l'action en recouvrement de la somme mise à la charge de Mme C... n'était pas prescrite le 11 mars 2016, date d'émission du dernier avis à tiers détenteur adressé à l'intéressée, au motif que le délai de prescription avait été interrompu par la notification de l'avis à tiers détenteur du 15 avril 2013, qui constituait le dernier acte de poursuite précédent celui du 11 mars 2016, alors que cet acte avait été annulé par le juge de l'exécution du tribunal de grande instance de Montpellier le 19 janvier 2015, le tribunal administratif de Montpellier a commis une erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, Mme C... est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de ce qui a été dit au point 4 qu'en l'absence de tout acte interruptif dans le délai de quatre ans précédent la notification de l'avis à tiers détenteur du 11 mars 2016, la prescription de l'action en recouvrement était acquise à cette date. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens de sa demande, Mme C... est fondée à demander la décharge de l'obligation de payer les taxes d'urbanisme résultant de cet acte de poursuite.<br/>
<br/>
              7. Mme C... ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. L'Etat devant être regardé, dans la présente instance, comme la partie perdante, il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Chaisemartin, Doumic-Seiller, avocat de Mme C..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de ce dernier le versement à cette SCP de la somme de 3 000 euros demandée à ce titre. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 5 octobre 2018 du tribunal administratif de Montpellier est annulé.<br/>
Article 2 :  Mme C... est déchargée de l'obligation de payer les taxes d'urbanisme résultant de l'avis à tiers détenteur du 11 mars 2016.<br/>
Article 3 : L'Etat versera à la SCP Chaisemartin, Doumic-Seiller avocat de Mme C..., la somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat<br/>
Article 4 : La présente décision sera notifiée à Mme A... C... et à la ministre de la transition écologique.<br/>
              Délibéré à l'issue de la séance du 28 octobre 2021 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; Mme Anne Egerszegi, conseillère d'Etat et M. Lionel Ferreira, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 15 novembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Lionel Ferreira<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... D...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
