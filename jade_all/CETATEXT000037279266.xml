<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037279266</ID>
<ANCIEN_ID>JG_L_2018_08_000000408173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/92/CETATEXT000037279266.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 02/08/2018, 408173, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408173.20180802</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Sens a demandé au tribunal administratif de Dijon de condamner l'Etat à lui payer la somme de 1 297 449 euros assortie des intérêts au taux légal à compter de la réception de sa demande indemnitaire préalable, en réparation de son préjudice causé par la minoration, à hauteur du montant du produit de la taxe sur les surfaces commerciales perçue au profit de l'Etat, de ses dotations de compensation au titre, respectivement, des années 2012, 2013. Par un jugement n° 1500820 du 17 mai 2016, ce tribunal a fait droit à sa demande.<br/>
<br/>
              Par un arrêt nos 16LY02383-16LY02438 du 20 décembre 2016, la cour administrative d'appel de Lyon a, sur recours du ministre de l'intérieur, annulé ce jugement et rejeté la demande d'indemnisation présentée par la commune de Sens.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février et 22 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Sens demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le recours du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
              - la loi n° 2014-1654 du 29 décembre 2014 ;<br/>
              - la décision n° 2017-644 QPC du 21 juillet 2017 du Conseil constitutionnel ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Sens ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Sens a saisi le tribunal administratif de Dijon d'une demande tendant à ce que l'Etat soit condamné au paiement d'une indemnité de 1 297 449 euros en réparation des conséquences dommageables des décisions préfectorales ayant minoré ses dotations de compensation pour les années 2012, 2013 et 2014 du produit de la taxe sur les surfaces commerciales perçu par l'Etat sur son territoire en 2010. Par un jugement du 17 mai 2016, le tribunal administratif a condamné l'Etat à payer à la commune de Sens une indemnité de 1 297 449 euros assortie des intérêts au taux légal à compter du 23 décembre 2014. Sur appel du ministre de l'intérieur, la cour administrative d'appel de Lyon a, par un arrêt du 20 décembre 2016, annulé ce jugement et rejeté la demande de première instance de la commune de Sens. Celle-ci demande l'annulation des articles 1er et 2 de l'arrêt de la cour.<br/>
<br/>
              2. En premier lieu, aux termes du paragraphe 1.2.4.2 de l'article 77 de la loi du 30 décembre 2009 de finances pour 2010 : " Le montant de la compensation prévue au D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) ou de la dotation de compensation prévue à l'article L. 5211-28-1 du code général des collectivités territoriales est diminué en 2011 d'un montant égal, pour chaque collectivité territoriale ou établissement public de coopération intercommunale à fiscalité propre, au produit de la taxe sur les surfaces commerciales perçu par l'État en 2010 sur le territoire de la collectivité territoriale ou de l'établissement public de coopération intercommunale ". Aux termes du b) du 2° du paragraphe 1.2.4.3 de l'article 77 de la même loi, l'article L. 2334-7 du code général des collectivités territoriales est ainsi modifié : " Il est ajouté un alinéa ainsi rédigé : "Pour les communes et établissements publics de coopération intercommunale à fiscalité propre, lorsque le montant de la compensation prévue au D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) ou de la dotation de compensation prévue à l'article L. 5211-28-1 du présent code est, en 2011, inférieur au montant de la diminution à opérer en application du 1.2.4.2 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010, le solde est prélevé au profit du budget général de l'Etat, prioritairement sur le montant correspondant aux montants antérieurement perçus au titre du 2° bis du II de l'article 1648 B du code général des impôts dans sa rédaction antérieure à la loi n° 2003-1311 du 30 décembre 2003 de finances pour 2004 et enfin sur le produit de la taxe foncière sur les propriétés bâties, de la taxe foncière sur les propriétés non bâties, de la taxe d'habitation et de la contribution économique territoriale perçu au profit de ces communes et établissements" ". Il résulte de ces dispositions que les mécanismes de diminution et de prélèvement portant sur les dotations et sur les recettes fiscales perçues par les communes et les établissements publics de coopération intercommunale à fiscalité propre, mis en place pour compenser le transfert du produit de la taxe sur les surfaces commerciales de l'Etat aux communes et à leurs groupements, ne sont applicables qu'au titre de l'année 2011.<br/>
<br/>
              3. En second lieu, aux termes de l'article 114 de la loi du 29 décembre 2014 de finances pour 2015 : " I.-Au dernier alinéa du II de l'article L. 2334-7 du code général des collectivités territoriales, les mots : ", en 2011, " sont supprimés. / II.-Au 1.2.4.2 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010, les mots : " en 2011 " sont supprimés ". Toutefois, en supprimant les termes " en 2011 " du dispositif décrit au point 2 ci-dessus, ces dispositions n'ont eu ni pour objet ni pour effet de lui conférer une portée rétroactive pour les années 2012 à 2014.<br/>
<br/>
              4. Dès lors, en jugeant que les dispositions précitées de l'article 114 de la loi du 29 décembre 2014 de finances pour 2015 avaient seulement un caractère interprétatif dont l'objet était de " rectifier une erreur légistique et clarifier ainsi la portée d'un mécanisme qui vise, par une intégration en base dans le calcul des dotations, à assurer la neutralité, pour le budget de l'Etat, du transfert opéré " et en en déduisant que, dans ces conditions, en procédant à la minoration de la dotation de compensation de la commune de Sens pour l'année 2014 d'un montant équivalent au produit de la taxe sur les surfaces commerciales perçu par l'Etat sur le territoire de cette communauté en 2010, le préfet de l'Yonne n'avait pas commis d'illégalité fautive et n'avait ainsi pu causer de préjudice à la commune, la cour a entaché son arrêt d'erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la commune de Sens est fondée à en demander l'annulation des articles 1er et 2 de cet arrêt. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. L'article 133 de la loi du 29 décembre 2016 de finances rectificative pour 2016 dispose que : " Sous réserve des décisions de justice passées en force de chose jugée sont validés les arrêtés préfectoraux pris au titre des exercices 2012, 2013 et 2014 constatant le prélèvement opéré sur le montant de la compensation prévue au D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) ou de la dotation de compensation prévue à l'article L. 5211-28-1 du code général des collectivités territoriales en tant que leur légalité serait contestée par le moyen tiré de ce qu'il aurait été fait application au-delà de 2011 des dispositions du paragraphe 1.2.4.2 de l'article 77 de la loi n°2009-1673 du 30 décembre 2009 de finances pour 2010 et de l'article L. 2334-7 du code général des collectivités territoriales, dans leur rédaction antérieure à la loi n° 2014-1654 du 29 décembre 2014 de finances pour 2015 ".<br/>
<br/>
              7. Compte tenu de l'intervention de ces dispositions, les décisions préfectorales prises au titre des années 2012 à 2014 sont désormais validées en tant qu'elles appliquent les mécanismes de diminution et de prélèvement, portant sur les dotations et les recettes fiscales perçues par les communes et établissements publics de coopération intercommunale à fiscalité propre, mis en place pour compenser le transfert du produit de la taxe sur les surfaces commerciales de l'Etat à ces personnes publiques.  <br/>
<br/>
              8. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de son recours, le ministre de l'intérieur est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Dijon a fait droit à la demande de la commune de Sens.  <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Lyon du 20 décembre 2016 et le jugement du tribunal administratif de Dijon du 17 mai 2016 sont annulés.<br/>
Article 2 : La demande de la commune de Sens est rejetée.<br/>
Article 3 : Les conclusions présentées par la commune de Sens au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Sens et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
