<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032853061</ID>
<ANCIEN_ID>JG_L_2016_07_000000392728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/85/30/CETATEXT000032853061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 06/07/2016, 392728</TITRE>
<DATE_DEC>2016-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392728.20160706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Sous le n° 392728, par une requête et un nouveau mémoire, enregistrés le 18 août 2015 et le 9 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du garde des sceaux, ministre de la justice, du 27 avril 2015 prononçant sa révocation;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° sous le n° 394484, par une requête et un nouveau mémoire enregistrés le 9 novembre 2015 et le 9 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 6 août 2015 par lequel le Président de la République a prononcé sa radiation des cadres de la magistrature à compter du 18 juin 2015 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Philippe Mochon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les deux requêtes présentées par Mme A...contre la décision de révocation et le décret de radiation des cadres de la magistrature dont elle a fait l'objet présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              En ce qui concerne la requête n° 392728 dirigée contre la décision du garde des sceaux, ministre de la justice du 27 avril 2015 :<br/>
<br/>
              2. Considérant que MmeA..., substitute du procureur de la République auprès du tribunal de grande instance de Clermont-Ferrand, demande l'annulation pour excès de pouvoir de la décision du 27 avril 2015 par laquelle le garde des sceaux, ministre de la justice a prononcé à son encontre la sanction de révocation prévue par le 7° de l'article 45 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 43 de l'ordonnance du 22 décembre 1958 : "Tout manquement par un magistrat aux devoirs de son état, à l'honneur, à la délicatesse ou à la dignité constitue une faute disciplinaire" ; qu'aux termes de l'article 45 de la même ordonnance : "Les sanctions disciplinaires applicables aux magistrats sont : (...) 7° La révocation avec ou sans suspension des droits à pension" ; qu'il résulte des articles 48 et 59 de cette même ordonnance que les sanctions disciplinaires contre les magistrats du parquet sont prononcées par le garde des sceaux, ministre de la justice, après avis de la formation compétente du Conseil supérieur de la magistrature ; <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 63, applicable à la procédure disciplinaire concernant les magistrats du parquet : " Le Conseil supérieur de la magistrature est saisi par la dénonciation des faits motivant les poursuites disciplinaires que lui adresse le garde des sceaux, ministre de la justice (...). Dès la saisine du Conseil supérieur de la magistrature, le magistrat a droit à la communication de son dossier et des pièces de l'enquête préliminaire, s'il y a été procédé. " ; qu'il ressort des pièces du dossier que le garde des sceaux, ministre de la justice a saisi le 8 février 2013 le président de la formation du Conseil supérieur de la magistrature compétente en matière de discipline des magistrats du parquet ; que la seule circonstance que la lettre qui a été adressée à Mme A...le 4 mars 2013 par le président de cette formation n'ait pas été revêtue de la mention suivant laquelle elle en aurait pris connaissance et reçu copie ne saurait entacher d'irrégularité la procédure disciplinaire ; <br/>
<br/>
              Sur la légalité interne de la décision attaquée : <br/>
<br/>
              5. Considérant que MmeA..., qui était avocate au barreau de Lyon depuis janvier 2000, a déposé le 23 septembre 2010 une candidature en vue d'une intégration directe dans le corps judiciaire en application de l'article 22 de l'ordonnance du 22 décembre 1958 ; qu'elle a été nommée par décret du 21 août 2012 substitute du procureur de la République près le tribunal de grande instance de Clermont-Ferrand ; que, compte tenu des éléments parvenus en décembre 2012 à la connaissance du garde des sceaux, ministre de la justice, une procédure disciplinaire a été ouverte à son encontre en février 2013 ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que Mme A...a fait l'objet de deux condamnations, en novembre 2011 par le tribunal de grande instance de Villefranche-sur-Saône et en septembre 2012 par le tribunal de grande instance de Bourg-en-Bresse pour défaut de paiement de dettes envers une institution de prévoyance professionnelle et une société d'édition professionnelle, sans être ni présente ni représentée lors des deux audiences ayant précédé ces condamnations ; que, par ailleurs, elle s'est abstenue, entre août 2011 et janvier 2012, de régler des rétrocessions d'honoraires dues à sa collaboratrice au sein de son cabinet d'avocat ; qu'elle s'est vu signifier de ce fait, en juillet 2012, par le bâtonnier de l'ordre des avocats, une rupture à ses torts exclusifs de ce contrat de collaboration, eu égard à la gravité de ses manquements aux " obligations contractuelles et déontologiques les plus élémentaires " et une condamnation à verser les sommes dues à sa collaboratrice ; que, Mme A...n'ayant pas justifié de l'apurement de ses dettes en contractant un crédit comme elle s'y était engagée, la chambre des procédures collectives du tribunal de grande instance de Lyon a prononcé en décembre 2012 la liquidation judiciaire de son activité libérale ; que le mandataire liquidateur a fait valoir dans son rapport de février 2013 que le passif du cabinet était ancien et s'élevait à plus de 120 000 euros et que l'intéressée n'avait pas jugé utile de collaborer avec lui, mais avait continué à utiliser la carte bancaire de son compte débiteur ;<br/>
<br/>
              7. Considérant que, contrairement à ce que soutient MmeA..., les faits ainsi relevés par le garde des sceaux, ministre de la justice ne sont entachés d'aucune erreur matérielle ; que si certains d'entre eux sont antérieurs à la procédure d'intégration de l'intéressée, ils n'ont été connus du ministre qu'après son intégration ; qu'en outre, les comportements décrits ci-dessus se sont souvent poursuivis après le début de la procédure d'intégration et même après l'intégration de Mme A...; que le ministre a ainsi pu légalement prendre en compte tous les faits reprochés à l'intéressée ; que ceux-ci constituent des manquements graves aux exigences de dignité, de probité et d'honneur et aux devoirs de l'état de magistrat et sont de nature à porter une atteinte grave et durable au crédit et à l'image de l'institution judiciaire ; que de tels manquements constituent une faute disciplinaire ; que, eu égard à leur gravité et à leur nombre, la sanction de révocation prononcée par le ministre ne peut être regardée comme disproportionnée ; <br/>
<br/>
              8. Considérant, par ailleurs, que si Mme A...soutient que la décision de sanction serait illégale en tant qu'elle ne prévoit pas le report de sa date d'effet à l'expiration de son congé maladie, la circonstance qu'un agent soit placé en congé pour maladie ne fait pas obstacle à l'exercice de l'action disciplinaire à son égard ni, le cas échéant, à l'entrée en vigueur d'une décision de révocation ; que, dès lors, ce moyen ne peut qu'être écarté ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision de la garde des sceaux, ministre de la justice, du 27 avril 2015 ; <br/>
<br/>
              En ce qui concerne la requête n° 394484 dirigée contre le décret du Président de la République du 6 août 2015 :<br/>
<br/>
              10. Considérant que les conclusions dirigées contre la décision du garde des sceaux, ministre de la justice du 27 avril 2015 étant rejetées par la présente décision, le moyen tiré de ce que le décret radiant des cadres l'intéressée devrait être annulé par voie de conséquence de l'annulation de la décision de révocation  ne peut qu'être écarté ; <br/>
<br/>
              11. Considérant qu'aux termes de l'article 73 de l'ordonnance du 22 décembre 1958 : " La cessation définitive des fonctions entraînant radiation des cadres et (...) perte de la qualité de magistrat, résulte : (...) 3° De la révocation " ; que le Président de la République est tenu de tirer les conséquences sur le plan statutaire de la décision par laquelle est prononcée une sanction disciplinaire et, en particulier, lorsque la sanction consiste en une révocation, de procéder, par décret, à la radiation des cadres du magistrat sanctionné ; qu'il suit de là que le moyen tiré de l'insuffisance de motivation du décret attaqué ne peut, en tout état de cause, être utilement soulevé ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation du décret du président de la République du 6 août 2015 ; <br/>
<br/>
              En ce qui concerne les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans les présentes instances, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de Mme A...sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-02-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE JUDICIAIRE. DISCIPLINE. - RÉVOCATION D'UN MAGISTRAT INTÉGRÉ - PRISE EN COMPTE DE FAITS ANTÉRIEURS À L'INTÉGRATION.
</SCT>
<ANA ID="9A"> 37-04-02-02 Si certains des faits retenus par le Garde des sceaux, ministre de la justice, pour prononcer la révocation du magistrat requérant sont antérieurs à la procédure d'intégration de l'intéressé, ils n'ont été connus du ministre qu'après son intégration. En outre, les comportements en cause se sont souvent poursuivis après le début de la procédure d'intégration et même après l'intégration de l'intéressé. Le ministre a ainsi pu légalement prendre en compte tous les faits reprochés à l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
