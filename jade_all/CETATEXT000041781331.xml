<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041781331</ID>
<ANCIEN_ID>JG_L_2020_03_000000434424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/13/CETATEXT000041781331.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 25/03/2020, 434424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434424.20200325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B..., veuve A..., a demandé au tribunal administratif de Lille de condamner, en application de la loi n° 2010-2 du 5 janvier 2010, le comité d'indemnisation des victimes des essais nucléaires (CIVEN) à l'indemniser des préjudices subis par son mari, M. D... A.... Par un jugement n° 1303203 du 31 janvier 2017, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17DA00589 du 8 juillet 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par Mme A... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 9 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - la loi n° 2017-256 du 28 février 2017 ;<br/>
              - la loi n° 2018-1317 du 28 décembre 2018 ;<br/>
              - le décret n° 2014-1049 du 15 septembre 2014 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., né le 3 janvier 1954, est décédé le 10 juillet 2009 d'un cancer de l'intestin grêle détecté en 2007. Il a servi en tant qu'appelé dans la Marine nationale à Mururoa du 4 octobre 1973 au 1er août 1974. Durant cette période, quatre tirs nucléaires aériens et deux tirs de sécurité ont eu lieu. Son épouse, Mme A..., née B..., a demandé le 28 novembre 2011 à être indemnisée des préjudices subis par son mari. Le comité d'indemnisation des victimes des essais nucléaires (CIVEN) a proposé, le 22 janvier 2013, le rejet de sa demande. Le ministre a suivi cet avis le 19 décembre 2013, au motif du caractère négligeable de l'exposition de M. A... au risque attribuable aux essais nucléaires. Mme A... se pourvoit en cassation contre l'arrêt du 8 juillet 2019 par lequel la cour administrative d'appel de Douai a rejeté son appel dirigé contre le jugement du 31 janvier 2017 du tribunal administratif de Lille rejetant sa demande de condamnation du CIVEN à l'indemniser des préjudices subis par son mari.<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " I. Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. / II. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit (...) ". Aux termes de l'article 2 de cette même loi : " La personne souffrant d'une pathologie radio-induite doit avoir résidé ou séjourné : / 1° Soit entre le 13 février 1960 et le 31 décembre 1967 au Centre saharien des expérimentations militaires, ou entre le 7 novembre 1961 et le 31 décembre 1967 au Centre d'expérimentations militaires des oasis ou dans les zones périphériques à ces centres ; / 2° Soit entre le 2 juillet 1966 et le 31 décembre 1998 en Polynésie française. / (...) ". Aux termes du I de l'article 4 de la même loi : " I. - Les demandes individuelles d'indemnisation sont soumises au comité d'indemnisation des victimes des essais nucléaires (...) ". En vertu du V du même article 4, dans sa rédaction résultant de l'article 113 de la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique, dont les dispositions sont applicables aux instances en cours à la date de son entrée en vigueur, soit le lendemain de la publication de la loi au Journal officiel de la République française : " V. - Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité (...) ". Désormais, le V de l'article 4, dans sa rédaction issue de l'article 232 de la loi du 28 décembre 2018 de finances pour 2019, dispose : " Ce comité examine si les conditions sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité, à moins qu'il ne soit établi que la dose annuelle de rayonnements ionisants dus aux essais nucléaires français reçue par l'intéressé a été inférieure à la limite de dose efficace pour l'exposition de la population à des rayonnements ionisants fixée dans les conditions prévues au 3° de l'article L. 1333-2 du code de la santé publique ". <br/>
<br/>
              3. En modifiant les dispositions du V de l'article 4 de la loi du 5 janvier 2010 issues de l'article 113 de la loi du 28 février 2017, l'article 232 de la loi du 28 décembre 2018 élargit la possibilité, pour l'administration, d'écarter la présomption de causalité dont bénéficient les personnes qui demandent une indemnisation lorsque les conditions de celle-ci sont réunies. Il doit être regardé, en l'absence de dispositions transitoires, comme ne s'appliquant qu'aux demandes qui ont été déposées après son entrée en vigueur, intervenue le lendemain de la publication de la loi du 28 décembre 2018 au Journal officiel de la République française. <br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que Mme A... a déposé sa demande au CIVEN le 28 novembre 2011. Il résulte de ce qui a été dit aux points 2 et 3 ci-dessus qu'en faisant application des dispositions de la loi du 5 janvier 2010 dans leur rédaction issue de la loi du 28 décembre 2018, alors qu'étaient applicables les dispositions issues de la loi du 28 février 2017, la cour administrative d'appel de Douai a méconnu le champ d'application de la loi. Dès lors, sans qu'il soit besoin d'examiner les moyens du pourvoi, l'arrêt attaqué doit être annulé. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 8 juillet 2019 de la cour administrative d'appel de Douai est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai. <br/>
Article 3 : L'Etat versera à Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mme C... B..., veuve A..., et au comité d'indemnisation des victimes des essais nucléaires.<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
