<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042401309</ID>
<ANCIEN_ID>JG_L_2020_10_000000424440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/13/CETATEXT000042401309.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/10/2020, 424440, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424440.20201005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 septembre et 24 décembre 2018 et le 25 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'office public de l'habitat de Rennes Métropole - Archipel Habitat demande au Conseil d'Etat :<br/>
<br/>
              1° d'annuler la délibération n° SAN-2018-007 du 24 juillet 2018 par laquelle la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a prononcé à son encontre une sanction pécuniaire de 30 000 euros et décidé de rendre publique sa délibération pendant une durée de deux ans à l'expiration de laquelle elle sera anonymisée ;<br/>
<br/>
              2° à titre subsidiaire, de substituer à cette sanction un avertissement ou, le cas échéant, de réduire son montant à 5 000 euros ;<br/>
<br/>
              3° d'enjoindre à la CNIL de publier sur son site internet la décision d'annulation ou de réformation du Conseil d'Etat ;<br/>
<br/>
              4° de mettre à la charge de l'Etat la somme de 6 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de l'office public de l'habitat de Rennes Métropole - Archipel Habitat ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que, saisie le 27 octobre 2017 d'une plainte relative à l'envoi, le 9 octobre 2017, par la présidente de l'office public de l'habitat de Rennes Métropole - Archipel Habitat d'un courrier aux locataires de logements sociaux relevant de l'office, la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a, après avoir fait recueillir par ses services les observations de ce dernier et sur la base du rapport établi par un commissaire rapporteur, prononcé, par une délibération du 24 juillet 2018, une sanction pécuniaire à l'encontre de l'office public de l'habitat de 30 000 euros et a décidé de rendre publique cette décision en prévoyant son anonymisation à l'expiration d'un délai de deux ans à compter de sa publication.<br/>
<br/>
              Sur la régularité de la délibération attaquée :<br/>
<br/>
              2. En premier lieu, le deuxième alinéa de l'article 17 de la loi du 6 janvier 1978, dans sa rédaction applicable à la délibération attaquée, dispose que les membres de la formation restreinte de la CNIL " délibèrent hors de la présence des agents de la commission, à l'exception de ceux chargés de la tenue de la séance ". Aux termes du deuxième alinéa de l'article 18 de la même loi, dans sa rédaction applicable à la délibération attaquée : " Le commissaire du Gouvernement assiste à toutes les délibérations de la commission réunie en formation plénière (...). Il peut assister aux séances de la formation restreinte, sans être présent au délibéré ". Conformément au premier alinéa de l'article 3 du décret du 20 octobre 2005 pris pour l'application de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, dans sa rédaction applicable à la délibération attaquée : " Les délibérations de la commission sont prises à la majorité absolue des membres présents ". Contrairement à ce qui est soutenu, ni ces dispositions ni aucun principe n'impliquent que les décisions de la formation restreinte de la CNIL comportent des mentions attestant de la présence des seuls agents de la commission chargés de la tenue de la séance, de la présence ou de l'absence du commissaire du gouvernement ou du vote de chacun des membres ayant délibéré.<br/>
<br/>
              3. En second lieu, la délibération attaquée est suffisamment circonstanciée en fait comme en droit. Il s'ensuit que le moyen tiré de l'insuffisance de sa motivation ne peut qu'être écarté.<br/>
<br/>
              Sur le bien-fondé de la délibération attaquée :<br/>
<br/>
              4. L'article 6 de la loi du 6 janvier 1978, dans sa version applicable aux faits litigieux, dispose que : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : / 1° Les données sont collectées de manière loyale et licite. / 2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. (...) ".<br/>
<br/>
              En ce qui concerne la sanction pécuniaire prononcée par la formation restreinte de la CNIL :<br/>
<br/>
              5. En premier lieu, l'office public de l'habitat est autorisé à utiliser les données à caractère personnel qu'il a collectées et qui font l'objet d'un traitement pour plusieurs finalités, en particulier la gestion du parc social immobilier de son ressort, mais également l'information de ses locataires, conformément à la délibération à caractère général de la CNIL n° 2006-138 du 9 mai 2006, aux termes de laquelle font l'objet d'une dispense de déclaration les traitements qui ont " pour seules finalités la constitution et l'exploitation d'un fichier d'adresses à des fins d'information ou de communication externe se rapportant au but ou à l'activité poursuivie par la personne physique ou morale qui met en oeuvre le traitement. "<br/>
<br/>
              6. Il résulte de l'instruction que le courrier adressé aux locataires par la présidente de l'office a pour objet, pour une part, de les informer des conséquences qu'un projet de réforme de l'aide personnalisée au logement est susceptible d'avoir sur la situation financière de l'office et sur sa capacité à entretenir et à réhabiliter son patrimoine immobilier. Il ne méconnaît pas, sur ce point, les finalités du traitement mentionné au point 5 ci-dessus. Toutefois, il résulte aussi de l'instruction que ce courrier comporte par ailleurs une critique virulente d'" une attaque contre les locataires d'HLM [qui] doit être stoppée " et un appel à la mobilisation des locataires contre le projet de réforme. Dès lors, la formation restreinte de la CNIL, qui n'a pas, contrairement à ce qui est soutenu, qualifié le courrier de " communication politique ", a pu estimer que le contenu du courrier n'était " pas de nature purement informative " et qu'un manquement à l'obligation de respecter les finalités pour lesquelles le traitement avait été autorisé, de nature à justifier une sanction, était caractérisé.  <br/>
<br/>
              7. En second lieu, c'est par un motif surabondant de sa délibération que la formation restreinte de la CNIL a relevé que le but poursuivi par le courrier litigieux aurait pu être atteint par un affichage dans les halls d'immeubles, sans porter atteinte à la protection des données personnelles. Le moyen tiré de ce qu'elle aurait ainsi entaché sa décision d'erreur de droit ne peut, par suite, en tout état de cause, qu'être écarté.<br/>
<br/>
              En ce qui concerne le quantum de cette sanction :<br/>
<br/>
              8. L'article 47 de la loi du 6 janvier 1978, dans sa rédaction applicable aux faits litigieux, dispose que : " Le montant de la sanction pécuniaire prévu au I de l'article 45 est proportionné à la gravité du manquement commis et aux avantages tirés de ce manquement. La formation restreinte de la Commission nationale de l'informatique et des libertés prend notamment en compte le caractère intentionnel ou de négligence du manquement, les mesures prises par le responsable de traitement pour atténuer les dommages subis par les personnes concernées, le degré de coopération avec la commission afin de remédier au manquement et d'atténuer ses effets négatifs éventuels, les catégories de données à caractère personnel concernées et la manière dont le manquement a été porté à la connaissance de la commission ".<br/>
<br/>
              9. Eu égard au caractère intentionnel de l'utilisation des données personnelles des locataires des logements sociaux dans un but non conforme aux finalités du traitement, à la nature de la règle ainsi méconnue par cette personne publique et au nombre important des personnes concernées, la formation restreinte de la CNIL n'a pas infligé à l'office public de l'habitat une sanction disproportionnée aux faits de l'espèce en prononçant une sanction pécuniaire d'un montant de 30 000 euros.<br/>
<br/>
              En ce qui concerne la sanction complémentaire de publication :<br/>
<br/>
              10. Le deuxième alinéa de l'article 46 de la loi du 6 janvier 1978, dans sa rédaction applicable aux faits litigieux, dispose que : " La formation restreinte peut rendre publiques les sanctions qu'elle prononce. (...) Elle peut également ordonner leur insertion dans des publications, journaux et supports qu'elle désigne aux frais des personnes sanctionnées ".<br/>
<br/>
              11. Eu égard à la nature du manquement en cause, la sanction complémentaire contestée, qui vise à renforcer le caractère dissuasif de la sanction principale en lui assurant une publicité tant à l'égard des destinataires du courrier que des acteurs du secteur de l'habitat social, ne peut être regardée comme disproportionnée. Par ailleurs, si la commission restreinte de la CNIL n'a pas précisé le support de cette publication, elle n'a pas, contrairement à ce qui est soutenu, commis d'erreur de droit sur ce point, alors qu'il résulte du règlement intérieur de la CNIL que les décisions prises par la commission sont publiées sur son site internet et sur le site Légifrance dès leur publication.<br/>
<br/>
              12. Il résulte de tout ce qui précède que l'office public de l'habitat Rennes Métropole - Archipel Habitat n'est pas fondé à demander l'annulation de la délibération attaquée. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de l'office public de l'habitat de Rennes Métropole - Archipel Habitat est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'office public de l'habitat de Rennes Métropole - Archipel Habitat et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
