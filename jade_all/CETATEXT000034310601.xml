<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034310601</ID>
<ANCIEN_ID>JG_L_2017_03_000000379685</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/31/06/CETATEXT000034310601.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 29/03/2017, 379685, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379685</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:379685.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés les 20 janvier et 2 mars 2017 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société Edenred France demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation partielle de l'arrêt n° 11VE00625 du 6 mars 2014 de la cour administrative d'appel de Versailles, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 1734 ter du code général des impôts, dans sa rédaction issue de la loi n° 99-1173 du 30 décembre 1999 de finances rectificative pour 1999, et du e) du I de l'article 1763 de ce même code, issu de l'ordonnance n° 2005-1512 du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités, en tant que ces deux articles prévoient une amende réprimant le défaut de production, ou le caractère inexact ou incomplet, de l'état prévu au I de l'article 54 septies de ce code. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et  son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 99-1173 du 30 décembre 1999 ;<br/>
              - l'ordonnance n° 2005-1512 du 7 décembre 2005, ratifiée par la loi n° 2009-526 du 12 mai 2009 ; <br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la Société Edenred France  ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
               2. Aux termes de l'article 1734 ter du code général des impôts, dans sa rédaction issue de la loi du 30 décembre 1999 de finances rectificative pour 1999 : " (...) si l'état prévu au I de l'article 54 septies (...) n'est pas produit au titre de l'exercice au cours duquel est réalisée l'opération visée par ces dispositions ou au titre des exercices ultérieurs, ou si les renseignements qui sont portés sur ces états sont inexacts ou incomplets, il est prononcé une amende égale à 5 % des résultats omis (...) ". Aux termes de l'article 1763 de ce même code, dans sa rédaction issue de l'ordonnance du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités : " I. - Entraîne l'application d'une amende égale à 5 % des sommes omises le défaut de production ou le caractère inexact ou incomplet des documents suivants : / (...) e. Etat prévu (...) au I de l'article 54 septies (...) ". <br/>
<br/>
              3. Les dispositions de ces deux articles sont applicables au présent litige dès lors que l'administration fiscale a prononcé l'amende litigieuse sur le fondement des dispositions précitées de l'article 1763 du code général des impôts, alors même que la société soutient, dans le cadre de son action contentieuse, que seules les dispositions précitées de l'article 1734 ter du code général des impôts étaient applicables à l'exercice en cause. Ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment aux principes de proportionnalité et d'individualisation des peines garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789, soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'amende de l'article 1734 ter du code général des impôts, dans sa rédaction issue de la loi n° 99-1173 du 30 décembre 1999 de finances rectificative pour 1999, et du e) du I de l'article 1763 de ce même code, issu de l'ordonnance n° 2005-1512 du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités, en tant que ces articles prévoient une amende réprimant le défaut de production, ou le caractère inexact ou incomplet, de l'état prévu au I de l'article 54 septies de ce code, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la société Edenred France jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à la société Edenred France et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
