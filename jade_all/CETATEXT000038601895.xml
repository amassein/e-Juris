<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601895</ID>
<ANCIEN_ID>JG_L_2019_06_000000423130</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 12/06/2019, 423130, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423130</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Olivier Yeznikian</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:423130.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Eau Secours Vallée de l'Ariège, M. D...B...et Mme A...C...ont demandé au tribunal administratif de Toulouse, d'une part, d'annuler le procès-verbal de la commission d'appel d'offres de la commune d'Auterive du 10 novembre 2011 relatif à l'attribution du marché de production et distribution d'eau potable et de la décision implicite née du silence gardé par le maire sur le recours gracieux et d'enjoindre au maire de la commune d'Auterive qu'il saisisse le juge du contrat afin qu'il prononce la nullité du contrat conclu avec la société Veolia, à moins d'une résolution amiable, et d'autre part, d'annuler la délibération du conseil municipal du 25 novembre 2011 autorisant le maire à signer ce marché avec la société Veolia et la décision implicite née du silence gardé par le maire sur le recours gracieux contre cette délibération. Par un jugement n°s 1202513, 1202514 du 15 décembre 2015, le tribunal administratif de Toulouse a donné acte des désistements de Mme C...et rejeté les demandes des autres requérants.<br/>
<br/>
              Par un arrêt n° 16BX00710 du 12 juin 2018, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par l'association Eau Secours Vallée de l'Ariège et M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 août, 13 novembre 2018 et 17 mai 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Eau Secours Vallée de l'Ariège et M. B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Auterive la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Yeznikian, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'association Eau Secours Vallée de l'Ariege et de M.B....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 mai 2019, présentée par l'association Eau Secours Vallée de l'Ariège et M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'ils attaquent, l'association Eau Secours Vallée de l'Ariège et M. B...soutiennent que la cour administrative d'appel de Bordeaux a :<br/>
              - insuffisamment motivé sa décision et commis une erreur de droit en ne recherchant pas si le niveau de rémunération susceptible d'être assuré par la réalisation des branchements particuliers était de nature à affecter l'équilibre du contrat en faisant peser le risque d'exploitation sur le concessionnaire, de sorte que le marché aurait dû être requalifié de délégation de service public ;<br/>
              - insuffisamment motivé sa décision et commis une erreur de droit en jugeant que l'article 23-2 du cahier des clauses administratives particulières avait pu conférer au prestataire une exclusivité pour la réalisation des nouveaux branchements, dont le coût est à la charge des pétitionnaires, sans méconnaître le principe de liberté du commerce et de l'industrie ;<br/>
              - méconnu son office et statué au-delà des conclusions dont elle était saisie en accordant à la commune d'Auterive une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative, alors qu'elle ne sollicitait à ce titre qu'une somme de 1 000 euros ;<br/>
              - insuffisamment motivé sa décision, dénaturé ses écritures d'appel, commis une erreur de droit et dénaturé les pièces du dossier en jugeant que le conseil municipal devait être regardé comme s'étant prononcé sur tous les éléments essentiels du contrat ;<br/>
              - insuffisamment motivé sa décision, commis une erreur de droit et dénaturé les pièces du dossier en ne relevant pas que la commune avait méconnu l'obligation d'indiquer un montant estimatif des travaux payés sur bordereau et des branchements particuliers et, en tout état de cause, qu'elle n'avait pas défini son besoin, faute d'avoir évalué le montant des prestations sur travaux.<br/>
<br/>
              3. Eu égard au moyen soulevé, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les sommes dues au titre de l'article L. 761-1 du code de justice administrative. En revanche, aucun des moyens soulevés n'est de nature à permettre l'admission des autres conclusions dirigées contre cet arrêt.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de l'association Eau Secours Vallée de l'Ariège et M. B... qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les sommes dues au titre de l'article L. 761-1 du code de justice administrative sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de l'association Eau Secours Vallée de l'Ariège et M. B...n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à l'association Eau Secours Vallée de l'Ariège et à M. D...B....<br/>
Copie en sera adressée à la commune d'Auterive.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
