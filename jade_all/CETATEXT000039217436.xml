<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217436</ID>
<ANCIEN_ID>JG_L_2019_10_000000422092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217436.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 14/10/2019, 422092, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422092.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés le 9 juillet 2018 et les 26 juin et 25 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des agents phares et balises et sécurité maritime - Confédération générale du travail (SNPBSM-CGT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre de rejet de son recours gracieux du 7 mai 2018 tendant à la modification du dernier alinéa de l'article 2 du décret n° 2013-435 du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier dans un délai ne dépassant pas deux mois le dernier alinéa de l'article 2 du décret n° 2013-435 du 27 mai 2013 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
              - la loi n° 2015-1785 du 29 décembre 2015 ;<br/>
              - la loi n° 2017-1837 du 30 décembre 2017 ;<br/>
              - le décret n° 2017-435 du 28 mars 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du syndicat national des agents phares et balises et sécurité Maritime ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du IV de l'article 146 de la loi du 29 décembre 2015 de finances pour 2016, tel que modifié par l'article 134 de la loi du 30 décembre 2017 de finances pour 2018, applicable à la date de la présente décision : "  Les fonctionnaires et les agents contractuels de droit public exerçant ou ayant exercé certaines fonctions dans des établissements ou parties d'établissement de construction ou de réparation navales du ministère chargé de la défense ou du ministère chargé de la mer pendant les périodes au cours desquelles y étaient traités l'amiante ou des matériaux contenant de l'amiante peuvent demander à bénéficier d'une cessation anticipée d'activité et percevoir à ce titre une allocation spécifique. / Les deuxième, troisième et avant-dernier alinéas du I sont applicables aux bénéficiaires du régime prévu au présent IV. / Un décret en Conseil d'Etat fixe les conditions d'application du présent IV, notamment les conditions d'âge et de cessation d'activité ainsi que les modalités d'affiliation au régime de sécurité sociale, les conditions de cessation du régime prévu au présent IV et, par dérogation à l'article L. 161-17-2 du code de la sécurité sociale et à l'avant-dernier alinéa du I du présent article, l'âge auquel l'allocation est alors remplacée par la ou les pensions de vieillesse auxquelles les intéressés peuvent prétendre (...) ". Aux termes des trois derniers alinéas de l'article 2 du décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer, qui fixe les conditions de mise en oeuvre  de la cessation anticipée d'activité et de l'allocation spécifique de cessation anticipée d'activité (ASCAA) créées par les dispositions de l'article 157 de la loi du 29 décembre 2010 de finances pour 2011, auxquelles se sont substituées les dispositions précitées : " La période pendant laquelle le fonctionnaire perçoit l'allocation spécifique est prise en compte pour la constitution de ses droits à pension. Elle est considérée comme l'accomplissement de services effectifs. Pendant cette période, le fonctionnaire bénéficiaire n'acquiert aucun droit à avancement ". Aux termes de l'article 4 du même décret : " Le droit à l'allocation spécifique est ouvert au premier jour du mois civil suivant la date de la notification de la décision d'admission. A compter de la date d'ouverture du droit à l'allocation spécifique et jusqu'à son admission à la retraite, le bénéficiaire ne peut plus occuper un emploi ". Aux termes de l'article 8 du même décret : " Pendant la période au cours de laquelle ils bénéficient de l'allocation spécifique, les fonctionnaires ne sont pas pris en compte dans les effectifs du ministère chargé de la mer ".<br/>
<br/>
              2. Par un courrier du 7 mars 2018, le syndicat national des agents phares et balises et sécurité maritime - Confédération générale du travail (SNPBSM-CGT) a demandé au Premier ministre de modifier le dernier alinéa de l'article 2 du décret du 27 mai 2013 afin que l'exclusion du droit à l'avancement des bénéficiaires de l'ASCAA soit limitée au seul avancement de grade. Le SNPBSM-CGT demande l'annulation pour excès de pouvoir de la décision implicite de rejet opposée à sa demande et à ce qu'il soit enjoint au Premier ministre de modifier, dans un délai ne dépassant pas deux mois, le dernier alinéa de l'article 2 du décret du 27 mai 2013.  <br/>
<br/>
              3. En premier lieu, aux termes de l'article 57 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " L'avancement d'échelon est accordé de plein droit. Il a lieu de façon continue d'un échelon à l'échelon immédiatement supérieur. Il est fonction de l'ancienneté (...) ". Aux termes de l'article 58 de la même loi : " L'avancement de grade a lieu de façon continue d'un grade au grade immédiatement supérieur. / Il peut être dérogé à cette règle dans les cas où l'avancement est subordonné à une sélection professionnelle.  L'avancement de grade peut être subordonné à la justification d'une durée minimale de formation professionnelle au cours de la carrière (...) / Sauf pour les emplois laissés à la décision du Gouvernement, l'avancement de grade a lieu, selon les proportions définies par les statuts particuliers, suivant l'une ou plusieurs des modalités ci-après : 1° Soit au choix, (....) 2° Soit par voie d'inscription à un tableau annuel d'avancement, (...) 3° Soit par sélection opérée exclusivement par voie de concours professionnel (...) ". Aux termes de l'article L. 5 du code des pensions civiles et militaires de retraite : " Les services pris en compte dans la constitution du droit à pension sont : 1° Les services accomplis par les fonctionnaires titulaires et stagiaires mentionnés à l'article 2 de la loi n° 83-634 du 13 juillet 1983 précitée (...) ". Aux termes de l'article L. 9 du même code : " Le temps passé dans une position statutaire ne comportant pas l'accomplissement de services effectifs au sens de l'article L. 5 ne peut entrer en compte dans la constitution du droit à pension, sauf :1° Dans la limite de trois ans par enfant né ou adopté à partir du 1er janvier 2004, sous réserve que le titulaire de la pension ait bénéficié : a) D'un temps partiel de droit pour élever un enfant ; b) D'un congé parental ; c) D'un congé de présence parentale ; d) Ou d'une disponibilité pour élever un enfant de moins de huit ans (...) ". <br/>
<br/>
              4. Il résulte des dispositions citées au point 1 que l'agent public bénéficiaire de l'ASCAA cesse définitivement d'exercer ses fonctions, ne fait plus partie des effectifs du ministère chargé de la mer et n'est plus susceptible d'être rappelé en activité jusqu'à son admission à la retraite. Dès lors, c'est sans méconnaître l'article 57 de la loi du 11 janvier 1984 que le pouvoir règlementaire a pu prévoir, compte-tenu de la situation du fonctionnaire bénéficiaire de l'ASCAA, qui ne peut être regardé comme en position d'activité, que ce dernier ne peut bénéficier d'un avancement durant la période où il la perçoit, alors même que celle-ci est considérée comme valant accomplissement de services effectifs aux termes du même article 2 du décret du 27 mai 2013. Cette dernière disposition, qui se borne à reprendre les termes de l'article 146 de la loi du 29 décembre 2015 modifiée selon lequel la durée de la cessation anticipée d'activité est prise en compte pour la constitution et la liquidation des droits à pension, a seulement pour objectif de permettre de prendre en compte la période pendant laquelle l'agent est en cessation anticipée d'activité comme services effectifs pour la constitution de ses droits à pension. <br/>
<br/>
              5. En second lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que la différence de traitement qui en résulte soit, dans l'un comme l'autre cas, en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des différences de situation susceptibles de la justifier. <br/>
<br/>
              6. Il ressort des pièces du dossier que l'article 10 du décret du 28 mars 2017 relatif à la cessation anticipée d'activité des agents de la fonction publique reconnus atteints d'une maladie professionnelle provoquée par l'amiante prévoit que ces agents peuvent bénéficier du régime de cessation anticipée d'activité, avec versement d'une allocation spécifique, et exclut seulement qu'ils bénéficient pendant cette période d'un avancement de grade. Eu égard à la reconnaissance d'une maladie professionnelle et à ses conséquences sur le déroulement de leur carrière au sein de la fonction publique, la différence de traitement dont ils bénéficient par rapport aux agents de la fonction publique relevant du décret du 27 mai 2013, qui sont placés dans une situation différente dès lors qu'ils n'ont pas été reconnus comme atteints d'une telle maladie, est en rapport avec l'objet de cette allocation et n'est pas manifestement disproportionnée. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le SNPBSM-CGT n'est pas fondé à demander l'annulation de la décision implicite de rejet qu'il attaque. Par suite, ses conclusions aux fins d'injonction et celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat national des agents phares et balises et sécurité maritime - Confédération générale du travail est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat national des agents phares et balises et sécurité maritime - Confédération générale du travail, au Premier ministre, au ministre de la transition énergétique et écologique, au ministre de l'action et des comptes public et au ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
