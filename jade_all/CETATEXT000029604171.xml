<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029604171</ID>
<ANCIEN_ID>JG_L_2014_10_000000369427</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/41/CETATEXT000029604171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 15/10/2014, 369427</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369427</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369427.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 juin et 18 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...F..., demeurant..., Mme C...F..., demeurant..., Mme A...F..., demeurant..., M. G...F..., demeurant..., M. I...F..., demeurant..., M. D... F..., demeurant ... et M. E...F..., demeurant ... ; Mme F...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY01724 du 18 avril 2013 par lequel la cour administrative d'appel de Lyon a rejeté leur requête tendant, d'une part, à l'annulation du jugement n° 1003361 du 25 avril 2012 par lequel le tribunal administratif de Lyon a rejeté leur demande tendant à la condamnation de l'Institut national des sciences appliquées (INSA) de Lyon à leur payer la somme de 15 000 euros chacun, soit au total 105 000 euros, en réparation du préjudice qu'ils ont subi du fait du décès accidentel de leur frère, M. H...F..., le 9 novembre 2000, et d'autre part, à la condamnation demandée ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Institut national des sciences appliquées de Lyon le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme F...et autres et à la SCP Gadiou, Chevallier, avocat de l'institut national des sciences appliquées de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. H...F..., élève inscrit en formation continue en cinquième année de préparation du diplôme d'ingénieur civil à l'Institut national des sciences appliquées (INSA) de Lyon, a été affecté comme stagiaire auprès de la société d'études de fondations et d'injections (SEFI) par une convention de stage en entreprise en date du 16 juin 2000 ; qu'alors qu'il accomplissait en Egypte une partie de son stage en entreprise, il a été victime d'un accident mortel sur un chantier le 9 novembre 2000 ; que, par un arrêt du 11 mai 2006, la cour d'appel de Nîmes a jugé qu'en manquant à l'obligation de sécurité de résultat qui lui incombait en sa qualité d'organisme de formation, l'INSA de Lyon avait commis une faute inexcusable justifiant le versement de rentes et de dommages intérêts aux enfants de M.F..., à son épouse et à ses parents, et décidé que le versement de ces sommes serait garanti par la société SEFI ; que, postérieurement à cet arrêt, les frères et soeurs de M. F...ont recherché auprès de l'INSA de Lyon l'indemnisation du préjudice résultant pour eux du décès de leur frère à raison de la responsabilité fautive de l'établissement ; que, par un jugement du 25 avril 2012, le tribunal administratif de Lyon a rejeté leur demande tendant à la condamnation de cet établissement au versement d'une indemnité totale de 105 000 euros ; que Mme F...et autres se pourvoient en cassation contre l'arrêt du 18 avril 2013 de la cour administrative d'appel de Lyon rejetant leur appel contre ce jugement ;<br/>
<br/>
              2. Considérant que, lorsqu'un élève ou un étudiant effectue un stage dans le cadre de ses études, il demeure sous la responsabilité de l'établissement d'enseignement dont il relève ; que l'exercice de cette responsabilité implique, notamment, que l'institut de formation s'assure, au titre du bon fonctionnement du service public dont il a la charge, que le stage se déroule dans des conditions ne mettant pas en danger la sécurité du stagiaire, en particulier lorsque le stage se déroule à l'étranger ; qu'un manquement à cette obligation est susceptible d'entraîner la responsabilité pour faute de l'établissement d'enseignement ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la convention passée par l'INSA de Lyon avec la société SEFI ne comportait aucune clause de nature à assurer que le stage de M. F...se déroulerait dans des conditions ne mettant à pas en danger la sécurité de ce dernier, notamment si tout ou partie du stage avait lieu à l'étranger, et que l'INSA n'a pris aucune mesure pour assurer que le stage se déroulerait dans de telles conditions alors qu'il avait été informé au préalable par la société SEFI, le 30 juin 2000, de ce que M. F...serait amené à effectuer une partie de son stage en Egypte en qualité d'ingénieur travaux ; qu'en retenant que l'INSA de Lyon n'avait pas commis de faute de nature à engager sa responsabilité, au motif que la convention de stage ne méconnaissait pas la réglementation applicable, même en cas de stage accompli à l'étranger, la cour a entaché son arrêt d'une erreur de qualification juridique des faits ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'INSA de Lyon la somme de 3 500 euros demandée par Mme F...et autres au titre de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce que soit mise à la charge de Mme F...et autres la somme que demande l'INSA de Lyon ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 18 avril 2013 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'INSA de Lyon versera à Mme F...et autres la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par l'INSA de Lyon au titre de l'article L. 761-1 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...F..., à Mme C...F..., à Mme A...F..., à M. G...F..., à M. I...F..., à M. D...F..., à M. E... F...et à l'institut national des sciences appliquées de Lyon. <br/>
Copie en sera adressée pour information à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES CONCERNANT LES ÉLÈVES. - STAGES EFFECTUÉS PAR LES ÉLÈVES - RESPONSABILITÉ DE L'ÉTABLISSEMENT D'ENSEIGNEMENT - EXISTENCE - ETENDUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. AGISSEMENTS ADMINISTRATIFS SUSCEPTIBLES D'ENGAGER LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. - STAGES EFFECTUÉS PAR LES ÉLÈVES - RESPONSABILITÉ DE L'ÉTABLISSEMENT D'ENSEIGNEMENT - EXISTENCE - ETENDUE.
</SCT>
<ANA ID="9A"> 30-01-03 Lorsqu'un élève ou un étudiant effectue un stage dans le cadre de ses études, il demeure sous la responsabilité de l'établissement d'enseignement dont il relève. L'exercice de cette responsabilité implique, notamment, que l'établissement s'assure, au titre du bon fonctionnement du service public dont il a la charge, que le stage se déroule dans des conditions ne mettant pas en danger la sécurité du stagiaire, en particulier lorsque le stage se déroule à l'étranger. Un manquement à cette obligation est susceptible d'entraîner la responsabilité pour faute de l'établissement d'enseignement.</ANA>
<ANA ID="9B"> 60-01-03 Lorsqu'un élève ou un étudiant effectue un stage dans le cadre de ses études, il demeure sous la responsabilité de l'établissement d'enseignement dont il relève. L'exercice de cette responsabilité implique, notamment, que l'établissement s'assure, au titre du bon fonctionnement du service public dont il a la charge, que le stage se déroule dans des conditions ne mettant pas en danger la sécurité du stagiaire, en particulier lorsque le stage se déroule à l'étranger. Un manquement à cette obligation est susceptible d'entraîner la responsabilité pour faute de l'établissement d'enseignement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
