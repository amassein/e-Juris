<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031649094</ID>
<ANCIEN_ID>JG_L_2015_12_000000369834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/90/CETATEXT000031649094.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/12/2015, 369834</TITRE>
<DATE_DEC>2015-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369834.20151218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 2 juillet 2013 et 2 juin 2014 au secrétariat du contentieux du Conseil d'Etat, M. F... C..., M. B...-G...A..., M. N...P..., M. H...J..., M. B...-Q...E..., Mme K...M...et M. G... O...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du ministre de l'intérieur du 13 juin 2013 relative aux conséquences du refus illégal de célébrer un mariage de la part d'un officier d'état civil ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 035 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le pacte international relatif aux droits civils et politiques ; <br/>
              - le code civil ; <br/>
              - le code pénal ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi de finances du 22 avril 1905 ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - la loi n° 2013-404 du 17 mai 2013 ;<br/>
              - la décision n° 369834 du 18 septembre 2013 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par les requérants ;  <br/>
              - la décision n° 2013-353 QPC du 18 octobre 2013 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par les requérants ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. F...C..., de M. B...-michelA..., de M. N...P..., de B...-yvesE..., de XavierJ..., de ClotildeM..., de Michel O...et de M. L... I...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à la suite de l'adoption de loi du 17 mai 2013 ouvrant le mariage aux personnes de même sexe, le ministre de l'intérieur a édicté la circulaire attaquée, par laquelle il a rappelé aux préfets les conditions dans lesquelles les autorités compétentes peuvent célébrer un mariage et les conséquences auxquelles elles s'exposent en cas de refus illégal de procéder à une telle célébration ; que le ministre a également prescrit aux préfets de faire preuve d'une vigilance particulière à l'égard des officiers de l'état-civil dont le comportement aurait pour objet d'empêcher le mariage de deux personnes de même sexe sur le territoire d'une commune et de le tenir informé de ces situations ; <br/>
<br/>
              Sur les interventions : <br/>
<br/>
              2. Considérant qu'en leur qualité de maire, adjoint au maire ou conseiller municipal, les intervenants justifient d'un intérêt de nature à rendre recevables leurs interventions au soutien de la requête présentée par M. C...et autres ; que leurs interventions doivent, par suite, être admises ;<br/>
<br/>
              Sur le moyen tiré de la liberté de conscience : <br/>
<br/>
              3. Considérant, en premier lieu, que les requérants soutiennent que la circulaire attaquée est contraire à la liberté de conscience garantie par la Constitution au motif qu'elle ne rappelle pas l'existence d'une " clause de conscience " permettant aux officiers d'état-civil de refuser de procéder à un mariage entre personnes de même sexe ; que, toutefois, le Conseil constitutionnel a jugé, par sa décision n° 2013-353 QPC du 18 octobre 2013 par laquelle il a statué sur la question prioritaire de constitutionnalité soulevée par M. C... et autres, qu'en ne permettant pas aux officiers de l'état-civil de se prévaloir de leur désaccord avec la loi du 17 mai 2013 pour se soustraire à l'accomplissement des attributions qui leur sont conférées par la loi, le législateur a entendu assurer le bon fonctionnement et la neutralité du service public de l'état-civil et n'a, ce faisant, pas porté atteinte à la liberté de conscience des officiers de l'état-civil ; que les requérants ne sauraient, par suite et en tout état de cause, utilement soutenir que la circulaire qu'ils attaquent méconnaîtrait la liberté de conscience constitutionnellement garantie ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il résulte des stipulations de l'article 9 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 18 du pacte international relatif aux droits civils et politiques que le droit à la liberté de pensée, de conscience et de religion prévu par ces textes peut faire l'objet des restrictions, prévues par la loi, qui sont nécessaires à la sécurité publique, à la protection de l'ordre, de la santé ou de la morale publiques, ou à la protection des droits et libertés d'autrui ; qu'aucun texte ni aucun principe ne fait obligation aux officiers d'état-civil d'approuver les choix de vie des personnes dont ils célèbrent le mariage et auxquelles ils délivrent des actes d'état-civil, et notamment le mariage entre personnes de même sexe ; qu'eu égard à l'intérêt général qui s'attache, ainsi qu'il a été dit, au bon fonctionnement et à la neutralité du service public de l'état-civil au regard de l'orientation sexuelle des époux, la circulaire attaquée ne méconnaît pas, contrairement à ce qui est soutenu, la liberté de conscience garantie par ces stipulations ; que ce moyen doit, par suite, être écarté ;  <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              5. Considérant, en premier lieu, que la circulaire attaquée n'a pas pour objet de créer un traitement de données à caractère personnel ; que les requérants ne peuvent, par suite, utilement soutenir que le signalement par les préfets des situations dans lesquelles des officiers de l'état-civil feraient obstacle à la célébration de mariage entre personnes de même sexe aurait pour conséquence la création d'un traitement automatisé de données à caractère personnel dans des conditions contraires aux dispositions de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que les dispositions de l'article 65 de la loi du 22 avril 1905, qui impliquent seulement qu'un agent public faisant l'objet d'une mesure prise en considération de sa personne soit mise à même de demander la communication de son dossier préalablement à cette mesure, ne sont pas relatives aux mentions pouvant ou non figurer dans ce dossier ; que le moyen tiré de ce que le signalement par le préfet des situations dans lesquelles un officier de l'état-civil refuserait de célébrer un mariage entre personnes de même sexe méconnaîtrait les dispositions de l'article 65 de la loi du 22 avril 1905 est, par suite, inopérant ; <br/>
<br/>
              7. Considérant, en troisième lieu, que la circulaire attaquée rappelle que l'officier de l'état-civil ne peut refuser de célébrer un mariage en dehors des cas légalement prévus ; que, ce faisant, elle n'énonce aucune règle de nature à faire obstacle à ce qu'un officier de l'état-civil s'abstienne de célébrer un mariage en lieu et place d'un autre officier de l'état-civil de la commune ; <br/>
<br/>
              8. Considérant, en quatrième lieu, qu'en se bornant à rappeler que le refus illégal de célébrer un mariage par un officier de l'état-civil est susceptible d'entraîner l'application des articles 432-1 et 432-7 du code pénal, relatifs respectivement aux cas dans lesquels une personne dépositaire de l'autorité publique fait obstacle à l'application de la loi ou commet des discriminations, la circulaire attaquée, qui ne qualifie pas les actes sanctionnés par ces articles, n'a pas fait une interprétation erronée de ces dispositions ; <br/>
<br/>
              9. Considérant, en cinquième lieu, qu'aux termes de l'article L. 2122-34 du code général des collectivités territoriales : " Dans le cas où le maire, en tant qu'agent de l'Etat, refuserait ou négligerait de faire un des actes qui lui sont prescrits par la loi, le représentant de l'Etat dans le département peut, après l'en avoir requis, y procéder d'office par lui-même ou par un délégué spécial " ; que l'article 34-1 du code civil, créé par la loi du 17 mai 2013, prévoit que : " Les actes de l'état civil sont établis par les officiers de l'état civil. Ces derniers exercent leurs fonctions sous le contrôle du procureur de la République " ; qu'il résulte de ces dispositions, à moins qu'un texte particulier n'en dispose autrement, que le pouvoir de substitution conféré au préfet par les dispositions l'article L. 2122-34 du code général des collectivités territoriales ne s'applique que dans la limite des compétences des maires qui s'exercent dans le domaine administratif sous l'autorité ou le contrôle du préfet, et ne s'étend pas, alors même que les maires agissent au nom de l'Etat, aux actes résultant de l'exercice des fonctions d'officier d'état-civil, qui sont placés sous le contrôle du procureur de la République ; qu'il suit de là que la circulaire attaquée n'a pas méconnu les dispositions de l'article L. 2122-34 du code général des collectivités territoriales en rappelant qu'elles n'autorisaient pas le préfet à se substituer au maire pour procéder à la célébration d'un mariage ; <br/>
<br/>
              10. Considérant, enfin, qu'une circulaire ne peut jamais être contestée par le moyen tiré de ce qu'elle ne prévoirait pas certaines dispositions ; qu'il suit de là que les moyens tirés de ce que la circulaire attaquée serait illégale au motif qu'elle ne rappellerait pas les dispositions relatives à la protection fonctionnelle des agents publics ainsi que le droit de ces derniers à la communication de leur dossier avant le prononcé de toute mesure prise en considération de leur personne sont inopérants ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la requête de M. C...et autres doit être rejetée ; que leurs conclusions tendant à l'application des articles L. 761-1 et R. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de M. D...et autres sont admises.  <br/>
Article 2 : La requête de M. F...C...et autres est rejetée. <br/>
Article 3 : La présente décision sera notifiée à M. F...C..., M. B...-G...A..., M. N... P..., M. H...J..., M. B...-Q...E..., Mme K...M..., M. G... O...et au ministre de l'intérieur. <br/>
Copie en sera adressée à la garde des sceaux, ministre de la justice, au Premier ministre et à M. L... I..., premier intervenant dénommé. Les autres intervenants seront informés de la présente décision par la SCP Delaporte, Briard, Trichet, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. Les intervenants n'étant pas représentés par le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation pourront prendre connaissance de la présente décision sur le site Internet du Conseil d'Etat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-03-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. ATTRIBUTIONS EXERCÉES AU NOM DE L'ETAT. - EXERCICE DES FONCTIONS D'OFFICIER D'ÉTAT-CIVIL - POUVOIR DE SUBSTITUTION DU PRÉFET - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-03-01-04-02 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ORGANISATION DU DÉPARTEMENT. REPRÉSENTANTS DE L'ETAT DANS LE DÉPARTEMENT. POUVOIRS DU PRÉFET. - POUVOIR DE SUBSTITUTION AU MAIRE AGISSANT EN TANT QU'AGENT DE L'ETAT (ART. L. 2122-34 DU CGCT) - APPLICATION AUX ACTES RÉSULTANT DE L'EXERCICE DES FONCTIONS D'OFFICIER D'ÉTAT-CIVIL - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-065 PROCÉDURE. JUGEMENTS. - INFORMATION DES INTERVENANTS NON REPRÉSENTÉS PAR UN AVOCAT AUX CONSEILS LORSQUE L'AFFAIRE A DONNÉ LIEU À UN TRÈS GRAND NOMBRE D'INTERVENTION - INVITATION À PRENDRE CONNAISSANCE DE LA DISPOSITION SUR LE SITE INTERNET DU CONSEIL D'ETAT [RJ1].
</SCT>
<ANA ID="9A"> 135-02-01-02-02-03-02 Il résulte de l'article 34-1 du code civil, à moins qu'un texte particulier n'en dispose autrement, que le pouvoir de substitution conféré au préfet par l''article L. 2122-34 du code général des collectivités territoriales (CGCT) ne s'applique que dans les limites des compétences des maires qui s'exercent dans le domaine administratif sous l'autorité ou le contrôle du préfet, et ne s'étend pas, alors même que les maires agissent au nom de l'Etat, aux actes résultant de l'exercice des fonctions d'officier d'état-civil, qui sont placés sous le contrôle du procureur de la République.</ANA>
<ANA ID="9B"> 135-03-01-04-02 Il résulte de l'article 34-1 du code civil, à moins qu'un texte particulier n'en dispose autrement, que le pouvoir de substitution conféré au préfet par l''article L. 2122-34 du code général des collectivités territoriales (CGCT) ne s'applique que dans les limites des compétences des maires qui s'exercent dans le domaine administratif sous l'autorité ou le contrôle du préfet, et ne s'étend pas, alors même que les maires agissent au nom de l'Etat, aux actes résultant de l'exercice des fonctions d'officier d'état-civil, qui sont placés sous le contrôle du procureur de la République.</ANA>
<ANA ID="9C"> 54-06-065 Lorsque l'affaire donne lieu à un très grand nombre d'interventions, le dispositif invite les intervenants qui ne sont pas représentés par un avocat au Conseil d'Etat et à la Cour de cassation à prendre connaissance de la décision sur le site Internet du Conseil d'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. décision du même jour, Union départementale des associations familiales des Hauts-de-Seine et autres, n°s 370459, 370468, 370583, 370697, inédite.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
