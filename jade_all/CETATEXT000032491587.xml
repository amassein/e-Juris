<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491587</ID>
<ANCIEN_ID>JG_L_2016_05_000000380548</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/15/CETATEXT000032491587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 04/05/2016, 380548</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380548</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:380548.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir la délibération du 28 juin 2013 du jury d'attribution du brevet professionnel de la jeunesse, de l'éducation populaire et du sport en tant qu'elle lui a refusé la validation, par acquis de l'expérience, des unités de compétences 4, 5, 6, 7, 8, 9 et 10 pour l'attribution de ce brevet en spécialité " activités gymniques de la forme et de la force ", mention " forme en cours collectif ". Par une ordonnance n° 1302298 du 12 novembre 2013, le président de la 2ème chambre du tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 14LY00031 du 20 mars 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A...contre cette ordonnance. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 20 août 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par une ordonnance du 12 novembre 2013, le président de la 2ème chambre du tribunal administratif de Dijon a rejeté la demande de M. A...tendant à l'annulation pour excès de pouvoir de la délibération du jury du brevet professionnel de la jeunesse, de l'éducation populaire et du sport du 28 juin 2013, en ce qu'elle lui a refusé la validation, par acquis de l'expérience, de certaines unités de compétence ; que par l'arrêt du 20 mars 2014 qu'il attaque, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A... contre cette ordonnance ;  <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne " ; que le rapporteur public qui, après avoir communiqué le sens de ses conclusions en application de ces dispositions, envisage de modifier sa position doit, à peine d'irrégularité de la décision, mettre les parties à même de connaître ce changement ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure d'appel que le requérant a été informé, le l2 février 2014, par un avis d'audience du greffe de la cour administrative d'appel de Lyon, qu'il pourrait, s'il le souhaitait, prendre connaissance du sens des conclusions que le rapporteur public prononcerait sur son affaire lors de l'audience du 27 février 2014, en consultant, au moyen d'un code d'accès confidentiel, l'application Sagace qui serait renseignée à cet effet dans un délai de l'ordre de deux jours avant l'audience ; que le sens des conclusions du rapporteur public a été mis en ligne dans l'application Sagace le 25 février 2014 à 10 heures, mais a été ensuite modifié, à deux reprises, le lendemain, à 10 heures 20 puis à 10 heures 23 ; <br/>
<br/>
              4. Considérant que le requérant soutient qu'après avoir consulté l'application Sagace deux jours avant l'audience, comme l'y invitait l'avis du 12 février et eu, ce faisant, connaissance du sens des conclusions indiqué par cette première mise en ligne, il n'a pas été mis à même d'avoir connaissance des deux changements ultérieurs, faute pour lui d'avoir été informé spécifiquement de ces nouvelles mises en ligne ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit au point 2 que l'intervention, après une première mise en ligne par le rapporteur public du sens de ses conclusions dans l'application Sagace, d'une nouvelle mise en ligne modifiant le sens de ces conclusions, sans que les parties soient informées de ce qu'un nouvel élément est intervenu dans cette application, ne met pas ces dernières en mesure de connaître le sens des conclusions du rapporteur public et méconnaît, en principe, les dispositions, citées ci-dessus, de l'article R. 711-3 du code de justice administrative ; <br/>
<br/>
              6. Considérant, toutefois, qu'il ne ressort pas de la procédure d'appel et n'est d'ailleurs pas soutenu par le requérant que le rapporteur public aurait, à l'audience, prononcé des conclusions dans un sens différent de celui dont il avait eu connaissance ; que, par suite, alors même que les parties n'ont pas été spécifiquement informées de ce que de nouveaux éléments étaient intervenus dans l'application Sagace, M. A...n'est pas fondé à soutenir que les dispositions de l'article R. 711-3 du code de justice administrative ont, en l'espèce, été méconnues ; <br/>
<br/>
              7. Considérant qu'en jugeant que M. A...s'était borné à contester, dans sa demande de première instance, le bien-fondé de l'appréciation du jury sans soutenir que celui-ci s'était fondé sur des faits matériellement inexacts, la cour administrative d'appel de Lyon, qui a suffisamment motivé sa décision sur ce point, n'a pas méconnu la portée des écritures  du requérant devant le tribunal administratif ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par suite, être également rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la ville, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-02 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. - OBLIGATION DE METTRE LES PARTIES EN MESURE DE CONNAÎTRE LE SENS DES CONCLUSIONS DU RAPPORTEUR PUBLIC - CAS D'UNE MODIFICATION DU SENS DES CONCLUSIONS PAR LE RAPPORTEUR PUBLIC DANS L'APPLICATION SAGACE - OBLIGATION D'INFORMER LES PARTIES DE L'INTERVENTION D'UN ÉLÉMENT NOUVEAU DANS CETTE APPLICATION [RJ1].
</SCT>
<ANA ID="9A"> 54-06-02 Le rapporteur public qui, après avoir communiqué le sens de ses conclusions, envisage de modifier sa position doit, à peine d'irrégularité de la décision, mettre les parties à même de connaître ce changement. Par suite, l'intervention, après une première mise en ligne par le rapporteur public du sens de ses conclusions dans l'application Sagace, d'une nouvelle mise en ligne modifiant le sens de ces conclusions, sans que les parties soient informées de ce qu'un nouvel élément est intervenu dans cette application, ne met pas ces dernières en mesure de connaître le sens des conclusions du rapporteur public et méconnaît, en principe, l'article R.711-3 du code de justice administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 mai 2006, Société Mullerhof, n° 259957, p. 232 ; CE, Section, 21 juin 2013, Communauté d'agglomération du pays de Martigues, n° 352427, p. 167.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
