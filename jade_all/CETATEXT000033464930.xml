<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033464930</ID>
<ANCIEN_ID>JG_L_2016_11_000000399523</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/46/49/CETATEXT000033464930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 23/11/2016, 399523, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399523</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:399523.20161123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la conférence des présidents d'université demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande d'abrogation du décret n° 2010-889 du 29 juillet 2010 relatif à la nomination des recteurs d'académie ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ce décret dans un délai de deux mois ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la conférence des présidents d'université ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les dispositions de l'article R.* 222-13 du code de l'éducation, dans leur rédaction issue du décret du 29 juillet 2010 relatif à la nomination des recteurs d'académie disposent : " Nul ne peut être nommé recteur s'il n'est habilité à diriger des recherches. / Toutefois, dans la limite de 20% des effectifs d'emplois correspondants, peuvent être nommées recteurs : / 1° Des personnes ayant exercé les fonctions de secrétaire général de ministère ou de directeur d'administration centrale pendant au moins trois ans ; / 2° Des personnes titulaires du doctorat et justifiant d'une expérience professionnelle de dix ans au moins dans le domaine de l'enseignement, de la formation ou de la recherche " ; que la requête par laquelle la conférence des présidents d'université demande l'annulation du rejet, par le Premier ministre, de sa demande d'abrogation de ces dispositions doit être regardée comme ne demandant l'annulation de cette décision qu'en tant qu'elle a refusé d'abroger le troisième alinéa (1°) de cet article ; <br/>
<br/>
              2. Considérant que l'article L. 232-1 du code de l'éducation, dans sa rédaction en vigueur à la date de la signature du décret du 29 juillet 2010, dispose que le Conseil national de l'enseignement supérieur et de la recherche est obligatoirement consulté sur la politique proposée par les pouvoirs publics pour assurer la cohésion des formations supérieures, les orientations générales des contrats d'établissements pluriannuels et la répartition des moyens entre les différents établissements ; que, contrairement à ce que soutient la conférence des présidents d'université, ces dispositions n'imposaient pas la consultation du Conseil national de l'enseignement supérieur et de la recherche préalablement à l'édiction du décret dont l'abrogation a été demandée ;<br/>
<br/>
              3. Considérant que le principe d'autonomie des universités, s'il est consacré par l'article L. 711-1 du code de l'éducation cité ci-dessous, n'a pas, en revanche, de valeur constitutionnelle ; que les requérants ne sauraient par suite utilement soutenir que les dispositions dont ils demandent l'abrogation seraient contraires à la Constitution en raison de ce qu'elles méconnaîtraient ce principe ; <br/>
<br/>
              4. Considérant que, contrairement à ce que soutient la conférence des présidents d'université, ni les dispositions de l'article L. 711-1 du code de l'éducation, aux termes desquelles : " Les établissements publics à caractère scientifique, culturel et professionnel sont des établissements nationaux d'enseignement supérieur et de recherche jouissant de la personnalité morale et de l'autonomie pédagogique et scientifique, administrative et financière. (...) Ils sont autonomes. Exerçant les missions qui leur sont conférées par la loi, ils définissent leur politique de formation, de recherche et de documentation dans le cadre de la réglementation nationale et dans le respect de leurs engagements contractuels. (...) ", en particulier le principe d'autonomie des universités qu'elles consacrent, ni aucune autre disposition ni aucun principe n'imposent au pouvoir réglementaire, compétent pour fixer les conditions de nomination à l'emploi de recteur, de subordonner l'accès à cet emploi à une condition de diplôme ou de titre universitaire ;<br/>
<br/>
              5. Considérant, enfin, qu'aux termes de l'article L. 222-2 du code de l'éducation : " Le recteur d'académie, en qualité de chancelier des universités, représente le ministre chargé de l'enseignement supérieur auprès des établissements publics à caractère scientifique, culturel et professionnel dans les conditions fixées à l'article L. 711-8. / Il assure la coordination des enseignements supérieurs avec les autres ordres d'enseignement. / Il dirige la chancellerie, établissement public national à caractère administratif qui, notamment, assure l'administration des biens et charges indivis entre plusieurs établissements " ; qu'il ressort des pièces du dossier qu'eu égard à la nature de ces attributions qui comportent, en plus de la mise en oeuvre des pouvoirs de tutelle exercés par le ministre chargé de l'enseignement supérieur sur les universités, des responsabilités étendues en matière d'enseignement primaire et secondaire ainsi que d'administration générale, les dispositions dont l'abrogation est demandée ne sont pas, en tant qu'elles autorisent la nomination de personnes ayant exercé les fonctions de secrétaire général de ministère ou de directeur d'administration centrale pendant au moins trois ans, entachées d'erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, la conférence des présidents d'université n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que ses conclusions doivent, par suite, être rejetées, y compris, par voie de conséquence, ses conclusions à fins d'injonction et celles qu'elle présente au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la conférence des présidents d'université est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la conférence des présidents d'université, au Premier ministre et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
