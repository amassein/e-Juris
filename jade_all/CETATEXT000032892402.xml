<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892402</ID>
<ANCIEN_ID>JG_L_2016_07_000000359366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892402.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 13/07/2016, 359366, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Action en astreinte</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:359366.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 16 mars 2014, le Conseil d'Etat statuant au contentieux a décidé qu'une astreinte était prononcée à l'encontre de l'Etat s'il ne justifiait pas avoir, dans les deux mois suivant la notification de cette décision, exécuté la décision du 16 mars 2011 du Conseil d'Etat statuant au contentieux ayant enjoint, d'une part, aux ministres chargés de la sécurité, de la ville, de la fonction publique et du budget d'examiner si le lieu d'affectation de Mme A...à Dreux, pour la période allant du 1er janvier 1995 au 31 août 1998, se situait dans une circonscription de police, ou une subdivision de circonscription, correspondant à un " quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles " au sens de l'article 11 de la loi du 26 juillet 1991 et de l'article 1er du décret du 21 mars 1995 pris pour son application, et, d'autre part, au ministre de l'intérieur de réexaminer la situation de Mme A... pour l'attribution de l'avantage spécifique d'ancienneté au titre de la même période.<br/>
<br/>
              La section du rapport et des études du Conseil d'Etat a exécuté les diligences qui lui incombent en vertu du code de justice administrative.<br/>
<br/>
              Par un mémoire, enregistré le 23 juin 2014, Mme A...demande au Conseil d'Etat de procéder à la liquidation de l'astreinte ; elle soutient que l'exécution de la décision est incomplète dès lors que la reconstitution de carrière à laquelle le ministre a procédé ne prend pas en compte ses chances d'obtenir un avancement de grade et que la condamnation de l'Etat au paiement d'une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative n'a pas été exécutée. <br/>
<br/>
              Par un mémoire, enregistré le 26 août 2014, le ministre de l'intérieur conclut au rejet de la demande de liquidation de l'astreinte ; il soutient que l'Etat a exécuté la décision du  16 mars 2011 du  Conseil d'Etat statuant au contentieux. <br/>
<br/>
              Par un nouveau mémoire enregistré le 1er décembre 2014, le ministre de l'intérieur reprend les conclusions de son précédent mémoire ; il soutient que la condamnation de l'Etat au titre de l'article L. 761-1 du code de justice administrative a été exécutée et que la demande relative à un avancement de grade soulève un litige distinct.<br/>
<br/>
              Par un nouveau mémoire enregistré le 8 juin 2015, Mme A...reprend les conclusions de son précédent mémoire ; elle soutient que sa demande relative à un avancement de grade ne soulève pas un litige distinct. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de MmeA... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision du 26 mars 2014, le Conseil d'Etat statuant au contentieux a décidé qu'une astreinte était prononcée à l'encontre de l'Etat s'il ne justifiait pas avoir, dans les deux mois suivant notification de cette décision, exécuté la décision du 16 mars 2011 ayant enjoint, d'une part, aux ministres chargés de la sécurité, de la ville, de la fonction publique et du budget d'examiner si le lieu d'affectation de Mme A...à Dreux, pour la période allant du 1er janvier 1995 au 31 août 1998, se situait dans une circonscription de police, ou une subdivision de circonscription, correspondant à un " quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles " au sens et pour l'application de l'article 11 de la loi du 26 juillet 1991 et de l'article 1er du décret du 21 mars 1995 pris pour son application, et, d'autre part, au ministre de l'intérieur de réexaminer la situation de Mme A... pour l'attribution de l'avantage spécifique d'ancienneté au titre de la même période ; que, par la même décision, le taux de cette astreinte a été fixé à 500 euros par jour à compter de l'expiration du délai de deux mois suivant la notification de cette décision ; <br/>
<br/>
              2. Considérant que la décision du Conseil d'Etat a été notifiée le 28 mars 2014 aux ministres intéressés ; qu'il ressort des documents communiqués par le ministre de l'intérieur qu'à la date du 21 mai 2014, celui-ci a justifié avoir, par un arrêté du 8 avril 2014, accordé à Mme A... le bénéfice de l'avantage spécifique d'ancienneté pour la période en litige, tout en procédant à la reconstitution de son avancement d'échelon ; que le ministre a, en outre, justifié de ce que l'intéressée avait effectivement perçu les sommes qui lui étaient dues au titre de cet avantage ; que le ministre de l'intérieur doit, par suite, être regardé comme ayant pris les mesures nécessaires pour assurer l'exécution de la décision du Conseil d'Etat statuant au contentieux en date du 16 mars 2011 ; <br/>
<br/>
              3. Considérant, par ailleurs, que, si Mme A...soutient que, pour assurer l'exécution complète de la décision du 16 mars 2011, le ministre de l'intérieur aurait dû procéder à une reconstitution de sa carrière en examinant également ses chances d'obtenir un avancement de grade, cette contestation, qui nécessite une appréciation ne résultant pas directement de la décision du Conseil d'Etat, soulève un litige distinct de l'exécution de cette décision ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de liquider l'astreinte prononcée à l'encontre de l'Etat. <br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
