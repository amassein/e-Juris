<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029621911</ID>
<ANCIEN_ID>JG_L_2014_10_000000359711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/19/CETATEXT000029621911.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 22/10/2014, 359711, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359711.20141022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 mai et 23 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C...B..., demeurant... ; M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX02418 du 27 mars 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à la réformation du jugement du 30 juin 2011 par lequel le tribunal administratif de Saint-Denis a limité à 7 955,65 euros le montant de la somme que le centre hospitalier Félix-Guyon a été condamné à lui verser, d'autre part, à la condamnation de ce centre hospitalier à lui verser la somme de 26 300,11 euros au titre de l'exécution de son contrat, cette somme portant intérêts de droit à compter du 16 août 2008, majorés de la capitalisation des intérêts à compter du 16 août 2009 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Félix-Guyon le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu la directive 2003/88/CE du 4 novembre 2003 du Parlement et du Conseil ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le centre hospitalier Félix-Guyon de Saint-Denis (La Réunion) a recruté M. A...B...le 1er mars 2007 comme praticien hospitalier contractuel à temps complet dans le cadre d'un contrat d'une durée de six mois, renouvelé pour une durée de cinq mois ; qu'ayant dû interrompre son activité le 31 janvier 2008 en raison d'un accident, l'intéressé a sollicité du centre hospitalier le versement de diverses indemnités ; qu'il a demandé au tribunal administratif de Saint-Denis de condamner l'établissement à lui verser, notamment, l'indemnité de précarité et des indemnités au titre de jours de congé non pris, de la perte de jours de repos hebdomadaire et d'un temps de travail additionnel ; que le tribunal administratif a fait droit à sa demande relative à l'indemnité de précarité et rejeté le surplus de ses conclusions ; que M. B...se pourvoit en cassation contre l'arrêt du 27 mars 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel contre ce jugement ; <br/>
<br/>
              Sur les jours de congé non pris : <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 6152-705 du code de la santé publique, dans sa rédaction applicable au litige, antérieure à l'intervention du décret n° 2010-1218 du 14 octobre 2010 : " (...) En cas de cessation définitive de fonctions, l'intéressé est tenu au préalable de solder son compte épargne-temps. A défaut, il perd ses droits " ; que ces dispositions font obstacle à toute indemnisation des jours de congés figurant sur le compte épargne-temps qui n'ont pas été pris, quelle que soit la cause de la cessation définitive de fonctions ; qu'ainsi la cour administrative d'appel n'a pas commis d'erreur de droit en rejetant les conclusions de M. B... relatives à l'indemnisation de la perte de jours de congés pour récupération du temps de travail ; <br/>
<br/>
              Sur la perte de jours de repos hebdomadaire :<br/>
<br/>
              3. Considérant que M. B...a demandé à être indemnisé de la perte de jours de repos hebdomadaire non pris en invoquant l'article 5 de la directive 2003/88/CE du 4 novembre 2003 du Parlement et du Conseil, qui prévoit que : " Les Etats membres prennent les mesures nécessaires pour que tout travailleur bénéficie, au cours de chaque période de sept jours, d'une période minimale de repos sans interruption de vingt-quatre heures auxquelles s'ajoutent les onze heures de repos journalier prévues à l'article 3 " ; que, pour  rejeter ces conclusions, la cour administrative d'appel a rappelé les termes de l'article 17 de la même directive, qui permet notamment de déroger tant à son article 5 qu'à son article 3 " pour les activités de garde, de surveillance et de permanence caractérisées par la nécessité d'assurer la protection des biens et des personnes " à la condition " que des périodes équivalentes de repos compensateur soient accordées aux travailleurs concernés ou que, dans des cas exceptionnels dans lesquels l'octroi de telles périodes équivalentes de repos compensateur n'est pas possible pour des raisons objectives, une protection appropriée soit accordée aux travailleurs concernés " ; qu'en constatant que ces dispositions n'ouvraient aucun droit au versement d'une indemnité compensatrice pour jours de repos hebdomadaire non pris, la cour, qui ne s'est pas méprise sur la portée de la directive invoquée devant elle, a suffisamment motivé son arrêt ; qu'elle n'a, en tout état de cause, pas méconnu le droit au respect d'un bien garanti par l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Sur le temps de travail additionnel :<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 6152-407 du code de la santé publique, dans sa rédaction alors en vigueur : " Le service hebdomadaire des praticiens contractuels exerçant à temps plein est fixé à dix demi-journées hebdomadaires, sans que la durée de travail puisse excéder quarante-huit heures par semaine, cette durée étant calculée en moyenne sur une période de quatre mois. (...) Lorsque l'activité médicale est organisée en temps continu, l'obligation de service hebdomadaire des praticiens est, par dérogation à l'alinéa ci-dessus, calculée en heures, en moyenne sur une période de quatre mois, et ne peut dépasser quarante-huit heures. / Les praticiens peuvent accomplir, sur la base du volontariat au-delà de leurs obligations de service hebdomadaires, un temps de travail additionnel donnant lieu soit à récupération, soit à indemnisation, dans les conditions prévues à l'article R. 6152-417(...) " ; <br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que, lorsque l'activité médicale n'est pas organisée en temps continu, les obligations de service hebdomadaires des praticiens contractuels exerçant à temps plein sont fixées à dix demi-journées hebdomadaires et ne peuvent excéder quarante-huit heures par semaine en moyenne sur une période de quatre mois ; que par suite, les indemnités forfaitaires dues, en application du b) du 1° de l'article D. 6152-23-1 du même code, pour tout temps de travail additionnel accompli, sur la base du volontariat, au-delà des obligations de service hebdomadaires doivent être versées au praticien contractuel exerçant à temps plein qui a accompli plus de dix demi-journées par semaine mais aussi à celui qui justifie avoir travaillé plus de quarante-huit heures en moyenne sur une période de quatre mois ; que, dès lors, en rejetant les conclusions de M. B... tendant au versement de ces indemnités, fondées sur un décompte horaire tendant à démontrer un dépassement de la moyenne hebdomadaire de quarante-huit heures de travail, au motif que l'activité médicale du service dans lequel il exerçait n'était pas organisée en temps continu et que, par suite, le centre hospitalier n'avait pas à calculer en heures le service hebdomadaire, la cour administrative d'appel de Bordeaux a commis une erreur de droit ;   <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...est seulement fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il se prononce sur l'indemnisation de son temps de travail additionnel ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier Félix-Guyon la somme de 2 000 euros à verser à M. B...au titre des frais exposés par lui et non compris dans les dépens <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 mars 2012 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il statue sur l'indemnisation du temps de travail additionnel de M.B....<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux, dans la limite de la cassation ainsi prononcée.<br/>
Article 3 : Le centre hospitalier Félix-Guyon versera une somme de 2 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. A... C...B..., au centre hospitalier Félix-Guyon et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
