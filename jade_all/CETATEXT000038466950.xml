<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038466950</ID>
<ANCIEN_ID>JG_L_2019_05_000000418320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/69/CETATEXT000038466950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 13/05/2019, 418320, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418320.20190513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 19 février, 16 mai et 29 août 2018, la société Soprodi Radio Régions SAS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 19 décembre 2017 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa demande tendant à ce que soient modifiées les caractéristiques techniques relatives à la diffusion de Radio Star, service autorisé sur la fréquence 105,2 Mhz à La Roche-Morey, en lui permettant de déplacer son émetteur sur le territoire de la commune de Chargey-les-Port ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel, sur le fondement de l'article L.911-1 du code de justice administrative, de lui accorder l'autorisation sollicitée ;<br/>
<br/>
              3°) de mettre à la charge du Conseil supérieur de l'audiovisuel une somme de 4 500 euros au titre de l'article L.761 1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Soprodi Radios Régions SAS ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que la société Soprodi Radios Régions SAS a été autorisée à exploiter un service de radio dénommé " Radio Star ", diffusé par voie hertzienne terrestre en modulation de fréquence dans la zone de La Roche-Morey (Haute-Saône) à partir d'un émetteur situé au lieudit " Haut du Cros " dans la commune de Bourguignon-lès-Morey. Le 22 février 2017, la SAS Soprodi Radios Régions a demandé au Conseil supérieur de l'audiovisuel l'autorisation de déplacer cet émetteur dans la commune de Chargey-les-Port, distante d'une vingtaine de kilomètres. Par la décision attaquée du 29 novembre 2017, le Conseil a rejeté cette demande.<br/>
<br/>
              2. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 4° Des recours dirigés contre les décisions prises par les organes des autorités suivantes, au titre de leur mission de contrôle ou de régulation : (...) / - le Conseil supérieur de l'audiovisuel, sous réserve des dispositions de l'article R. 311-2 (...) ". Aux termes de l'article R. 311-2 du même code : " La cour administrative d'appel de Paris est compétente pour connaître en premier et dernier ressort : 2° Des litiges relatifs aux décisions prises par le Conseil supérieur de l'audiovisuel en application des articles 28-1, 28-3 et 29 à 30-7 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, à l'exception de celles concernant les services de télévision à vocation nationale ". <br/>
<br/>
              3. Aux termes du premier alinéa de l'article 29 de la loi du 30 septembre 1986 relative à la liberté de communication : " Sous réserve des dispositions de l'article 26 de la présente loi, l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article ". Aux termes de l'article 25 de la même loi : " L'usage de la ressource radioélectrique pour la diffusion de services de communication audiovisuelle par voie hertzienne terrestre est subordonné au respect des conditions techniques définies par le Conseil supérieur de l'audiovisuel et concernant notamment : / 1° Les caractéristiques des signaux émis et des équipements de transmission et de diffusion utilisés ; / 1° bis Les conditions techniques du multiplexage et les caractéristiques des équipements utilisés ; / 2° Le lieu d'émission ;  / 3° La limite supérieure et, le cas échéant, inférieure de puissance apparente rayonnée. En zone de montagne, il est tenu compte des contraintes géographiques pour appréhender la limite supérieure de la puissance apparente rayonnée ; / 4° La protection contre les interférences possibles avec l'usage des autres techniques de télécommunications (...) ". Aux termes de l'article 42-3 de la même loi : " L'autorisation peut être retirée, sans mise en demeure préalable, en cas de modification substantielle des données au vu desquelles l'autorisation avait été délivrée, notamment des changements intervenus dans la composition du capital social ou des organes de direction et dans les modalités de financement. (...) / Dans le respect des critères mentionnés à l'article 29, notamment le juste équilibre entre les réseaux nationaux et les services locaux, régionaux et thématiques indépendants, le Conseil supérieur de l'audiovisuel peut donner son agrément à un changement de titulaire d'autorisation pour la diffusion de services de radio lorsque ce changement bénéficie à la personne morale qui contrôle ou qui est contrôlée par le titulaire initial de l'autorisation au regard des critères figurant à l'article L. 233-3 du code de commerce. A l'occasion de ce changement de titulaire de l'autorisation, le conseil peut, dans les mêmes conditions, donner son agrément à un changement de la catégorie pour laquelle le service est autorisé. Ce changement ne peut être agréé hors appel aux candidatures par le Conseil supérieur de l'audiovisuel s'il est incompatible avec la préservation des équilibres des marchés publicitaires, notamment locaux. / (...) / Sous réserve du respect des articles 1er et 3-1, le Conseil supérieur de l'audiovisuel peut, par décision motivée, donner son agrément à une modification des modalités de financement lorsqu'elle porte sur le recours ou non à une rémunération de la part des usagers. (...). / Sans préjudice de l'application du premier alinéa, tout éditeur de services détenteur d'une autorisation délivrée en application des articles 29,29-1,30-1,30-5 et 96 doit obtenir un agrément du Conseil supérieur de l'audiovisuel en cas de modification du contrôle direct ou indirect, au sens de l'article L. 233-3 du code de commerce, de la société titulaire de l'autorisation. (...) ". Aux termes de l'article 42-8 : " Les éditeurs et les distributeurs de services de communication audiovisuelle peuvent former un recours de pleine juridiction devant le Conseil d'Etat contre les décisions du Conseil supérieur de l'audiovisuel prises en application des articles 17-1, 42-1, 42-3 et 42-4 ".  <br/>
<br/>
              4. Il résulte de la combinaison des dispositions citées aux points 2 et 3 que si le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des recours contre les décisions du Conseil supérieur de l'audiovisuel relatives à l'agrément de modifications d'éléments mentionnés à l'article 42-3 de la loi du 30 septembre 1986, les recours contre les décisions relatives à la modification d'autres éléments des autorisations d'émettre, prises sur le fondement de l'article 29 de la même loi, relèvent de la compétence de premier et dernier ressort de la cour administrative d'appel de Paris. <br/>
<br/>
              5. La demande de la société Soprodi Radios Régions tendait à ce que le Conseil supérieur de l'audiovisuel modifie l'autorisation relative au service " Radio Star " qu'elle diffuse dans la zone de La Roche-Morey, en tant qu'elle fixe le lieu d'émission. Cette demande n'entrait pas dans les prévisions de l'article 42-3 de la loi du 30 septembre 1986. Il résulte de ce qui précède que la cour administrative d'appel de Paris est compétente pour connaître en premier et dernier ressort de la décision prise par le Conseil supérieur de l'audiovisuel sur cette demande. Le jugement de la requête doit, par suite, être attribué à cette juridiction.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la société Soprodi Radios Régions SAS est attribué à la cour administrative d'appel de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Soprodi Radios Régions SAS, au Conseil supérieur de l'audiovisuel et au président de la cour administrative d'appel de Paris.<br/>
		Copie en sera adressée au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
