<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551428</ID>
<ANCIEN_ID>JG_L_2016_12_000000383728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551428.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 02/12/2016, 383728, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:383728.20161202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. et Mme B...A...ont demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 2007 et 2008. Par un jugement n° 1200965 du 18 avril 2013, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13PA02114 du 17 juin 2014, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement par M. et MmeA....<br/>
<br/>
              Sous le n° 383728, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 18 août et 5 novembre 2014 et le 1er octobre 2015 au secrétariat du contentieux du Conseil d'État, M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              2° M et Mme B...A...ont demandé au tribunal administratif de Paris de les décharger des contributions sociales auxquelles ils ont été assujettis au titre des années 2007 et 2008. Par un jugement n° 1308823 du 25 avril 2014, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n°14PA02717 du 19 décembre 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par M. et Mme A...contre ce jugement.<br/>
<br/>
              Sous le n° 388128, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 19 février, 12 mai et 16 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur place de la société civile immobilière Paricap dont ils étaient les associés et dont Mme A...était gérante, M. et Mme A...ont fait l'objet d'un examen de leur situation fiscale personnelle portant sur les années 2007 et 2008, à la suite duquel ils ont été assujettis à des cotisations supplémentaires d'impôt sur le revenu dans la catégorie des revenus de capitaux mobiliers à raison des revenus considérés par l'administration fiscale comme leur ayant été distribués par la société Paricap, et aux cotisations supplémentaires de contributions sociales correspondantes. <br/>
<br/>
              2. Il y a lieu de joindre leurs pourvois, relatifs pour le premier à l'impôt sur le revenu et pour le second aux contributions sociales, pour statuer par une seule décision. <br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              3. Aux termes de l'article L. 76 B du livre des procédures fiscales : " L'administration est tenue d'informer le contribuable de la teneur et de l'origine des renseignements et documents obtenus de tiers sur lesquels elle s'est fondée pour établir l'imposition faisant l'objet de la proposition prévue au premier alinéa de l'article L. 57 (...). Elle communique, avant la mise en recouvrement, une copie des documents susmentionnés au contribuable qui en fait la demande ". <br/>
<br/>
              4. En premier lieu, la cour n'a pas méconnu les dispositions de l'article L. 76 B précité du livre des procédure fiscales en jugeant, après avoir relevé, par une appréciation souveraine des faits non arguée de dénaturation, que l'administration fiscale ne s'était pas fondée, pour établir les impositions en litige, sur les relevés bancaires obtenus par l'exercice de son droit de communication auprès des organismes bancaires teneurs des comptes des requérants, que l'absence de communication aux contribuables de ces relevés bancaires n'avait pas entaché d'irrégularité la procédure d'imposition. <br/>
<br/>
              5. En deuxième lieu, s'il appartient à l'administration d'informer les contribuables, avant la mise en recouvrement des impositions, de l'origine et de la teneur des renseignements recueillis par elle auprès de tiers, notamment dans l'exercice de son droit de communication, cette information peut être fournie dans tout document antérieur à la mise en recouvrement. La cour n'a par suite pas commis d'erreur de droit en jugeant qu'était sans incidence sur la régularité de la procédure d'imposition la circonstance que les requérants n'avaient reçu une partie de l'information exigée par les dispositions de l'article L. 76 B du livre des procédures fiscales que par un courrier du 5 octobre 2010, postérieur à la proposition de rectification. En jugeant par ailleurs qu'il résultait de l'instruction que le courrier du 5 octobre 2010 était accompagné des copies des demandes de communication faites aux tiers et des réponses obtenues de ceux-ci, en tant qu'elles comportaient des éléments utilisés pour établir les impositions, la cour n'a pas entaché son arrêt de dénaturation.<br/>
<br/>
              6. En troisième lieu, la cour n'a pas méconnu les dispositions précitées de l'article L.76 B du livre des procédures fiscales en jugeant que M. et Mme A...ne pouvaient utilement faire valoir un défaut de communication, par l'administration, de factures d'entremise établies par la société Paricap dès lors que celles-ci lui avaient été adressées par M. A...lui-même, dans le cadre de la vérification de la société. Elle n'a pas davantage commis d'erreur de droit en regardant comme sans incidence sur la régularité de la procédure suivie à l'égard des contribuables, la circonstance que l'administration aurait méconnu les dispositions de l'article L. 76 B du livre des procédures fiscales lors de la vérification de cette société.  <br/>
<br/>
              7. En quatrième lieu, la cour, après avoir relevé, par une appréciation souveraine des termes de la proposition de rectification du 26 juillet 2010 adressée aux contribuables non arguée de dénaturation, que celle-ci comportait la désignation de l'impôt concerné, de l'année d'imposition et de la base d'imposition et énonçait les motifs sur lesquels l'administration entendait se fonder pour justifier les rectifications envisagées, de façon à permettre au contribuable de formuler ses observations de façon entièrement utile, a pu sans erreur de droit en déduire que cette proposition ne méconnaissait pas les dispositions des articles L. 57 et R. 57-1 du livre des procédures fiscales.<br/>
<br/>
              Sur le bien fondé des impositions :<br/>
<br/>
              8. En premier lieu, si l'administration peut, à tout moment de la procédure, invoquer un nouveau motif de droit propre à fonder l'imposition, une telle substitution ne saurait avoir pour effet de priver le contribuable de la faculté, prévue par les articles L. 59 et L. 59 A du livre des procédures fiscales, de demander la saisine de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires afin que celle-ci se prononce sur un désaccord portant sur des questions de fait dont la solution commande le bien-fondé du nouveau motif invoqué par l'administration. Pour juger que l'administration était fondée à solliciter une substitution de motif, tirée de ce que la SCI Paricap, dont les bénéfices rehaussés ont été regardés comme distribués aux requérants, avait exercé une activité commerciale en 2008, la cour s'est fondée sur ce que les requérants ne contestaient pas la réalité des locations en meublés auxquelles s'était livrée la société, de sorte que cette substitution ne soulevait pas de questions de fait dont la société aurait pu demander qu'elles soient soumises à l'examen de la commission. En jugeant ainsi, la cour n'a, en tout état de cause, pas commis d'erreur de droit. <br/>
<br/>
              9. En deuxième lieu, aux termes du II de l'article 202 ter du code général des impôts, si une société dont les revenus n'ont pas la nature de bénéfices d'une entreprise industrielle, commerciale, artisanale ou minière, d'une exploitation agricole ou d'une activité non commerciale cesse totalement ou partiellement d'être soumise au régime de l'impôt sur le revenu, " l'impôt sur le revenu est établi au titre de la période d'imposition précédant immédiatement le changement de régime, à raison des revenus et des plus-values non encore imposés à la date du changement de régime, y compris ceux qui proviennent des produits acquis et non encore perçus ainsi que des plus-values latentes incluses dans le patrimoine ou l'actif social (...) ". Aux termes du III  du même article : " Les sociétés et organismes définis aux I et II doivent, dans un délai de soixante jours à compter de la réalisation de l'évènement qui entraîne le changement de régime ou d'activité mentionné auxdits I et II, produire le bilan d'ouverture de la première période d'imposition ou du premier exercice au titre duquel le changement prend effet ". La mise en évidence, à l'occasion d'un contrôle fiscal, de l'exercice par une société d'une activité commerciale de nature à entraîner son assujettissement à l'impôt sur les sociétés ne constitue pas, par elle-même, un changement de régime fiscal au sens de ces dispositions. Par suite, la cour n'a pas commis d'erreur de droit en écartant le moyen tiré de ce que l'assujettissement de la SCI Paricap à l'impôt sur les sociétés à compter du 1er janvier 2006, à la suite du contrôle de celle-ci opéré par l'administration fiscale, devait être assimilé, par ses conséquences, à un changement de régime fiscal conduisant à établir l'impôt sur le revenu pour la période immédiatement antérieure au 1er janvier 2006 en prenant en compte les plus-values latentes incluses dans son patrimoine et, en conséquence, à imposer les plus-values réalisées au cours des exercices clos en 2006 et 2008, durant lesquels la société était passible de l'impôt sur les sociétés, en ne retenant pour base imposable que la différence entre le prix de cession et la valeur vénale des biens à la date du 1er janvier 2006.<br/>
<br/>
              10. En troisième lieu, la cour n'a pas méconnu les règles de dévolution de la charge de la preuve en se fondant, pour rattacher, en application des dispositions du 2 bis de l'article 38 du code général des impôts, un produit correspondant à une créance sur la clientèle à l'exercice clos par la SCI Paricap en 2007 sur ce que la facture correspondante était datée du 8 janvier 2007 et en écartant comme dépourvue d'incidence l'existence, sur cette facture, de la mention imprécise "note de présentation d'affaires 2006".<br/>
<br/>
              11. En quatrième lieu, la cour n'a pas méconnu la portée de l'instruction fiscale référencée BOI 4 A-1-92 du 31 décembre 1991, en ce qu'elle mentionne que la réalisation, par les sociétés civiles, d'opérations commerciales définies aux articles 34 et 35 du code général des impôts compte parmi les événements mentionnés à l'article 202 ter dudit code, en jugeant que celle-ci n'ajoutait pas à la loi fiscale et a pu en déduire sans erreur de droit que les requérants ne pouvaient utilement s'en prévaloir sur le fondement de l'article L. 80 A du livre des procédures fiscales.<br/>
<br/>
              12. Il résulte de ce qui précède que les pourvois de M. et Mme A...doivent être rejetés, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de M. et Mme A...sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B...A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
