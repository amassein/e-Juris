<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587354</ID>
<ANCIEN_ID>JG_L_2015_12_000000371710</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587354.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 07/12/2015, 371710, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371710</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371710.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Groupe Saint-Germain a demandé au tribunal administratif de Melun de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution sur cet impôt auxquelles elle a été assujettie au titre des années 2001 à 2003, des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier 2001 au 31 décembre 2003 ainsi que des pénalités correspondantes et de l'amende à laquelle elle a été soumise en application de l'article 1759 du code général des impôts au titre des années 2002 et 2003.<br/>
<br/>
              Par un jugement n° 0705886 du 12 juillet 2011, le tribunal a fait partiellement droit à sa demande en jugeant qu'il n'y avait pas lieu de statuer à hauteur d'un dégrèvement accordé en cours d'instance par l'administration fiscale et en la déchargeant d'une partie des impositions et pénalités ainsi que de l'amende mise à sa charge et en rejetant le surplus des conclusions de cette demande.<br/>
<br/>
              Par un arrêt n° 11PA04144 du 27 juin 2013, la cour administrative d'appel de Paris a réduit les bases d'imposition et les pénalités et amendes correspondantes, s'agissant des redressements au titre d'intérêts non perçus sur les avances faites à des sociétés dont la société était l'associée et a rejeté le surplus des conclusions de son appel. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 août et 28 novembre 2013 et 13 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Groupe Saint-Germain demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
              - le décret n° 79-1163 du 29 décembre 1979 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Groupe Saint-Germain ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er décembre 2015, présentée par la société Groupe Saint-Germain ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur la période du 1er janvier 2001 au 31 décembre 2003, la société Groupe Saint-Germain a été assujettie à des cotisations supplémentaires d'impôt sur les sociétés et de contribution à cet impôt et à des rappels de taxe sur la valeur ajoutée au titre de cette période, assorties de pénalités pour mauvaise foi, et soumise à une amende au titre des années 2002 et 2003 en application de l'article 1759 du code général des impôts dans sa rédaction alors applicable ; que, par un jugement du 12 juillet 2011, le tribunal administratif de Melun n'a que partiellement fait droit à ses conclusions tendant à la décharge de ces impositions, rappels de droits, pénalités et amende ; que la société demande l'annulation de l'article 4 de l'arrêt du 27 juin 2013 par lequel la cour administrative d'appel de Paris, après avoir partiellement réduit ces impositions, pénalités et amende, a rejeté le surplus des conclusions de sa requête ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du paragraphe 2 de l'article 17 de la sixième directive  du 17 mai 1977, alors applicable : " Dans la mesure où les biens et les services sont utilisés pour les besoins de ses opérations taxées, l'assujetti est autorisé à déduire de la taxe dont il est redevable : a) la taxe sur la valeur ajoutée due ou acquittée à l'intérieur du pays pour les biens qui lui sont ou lui seront livrés et pour les services qui lui sont ou lui seront rendus par un autre assujetti (...) " ; qu'aux termes du paragraphe 6 du même article 17 : " Au plus tard avant l'expiration d'une période de quatre ans à compter de la date d'entrée en vigueur de la présente directive, le Conseil, statuant à l'unanimité sur proposition de la Commission, déterminera les dépenses n'ouvrant pas droit à déduction de la taxe sur la valeur ajoutée. (...) Jusqu'à l'entrée en vigueur des règles visées ci-dessus, les Etats membres peuvent maintenir toutes les exclusions prévues par leur législateur national au moment de l'entrée en vigueur de la présente directive " ; qu'il résulte clairement de ces dernières dispositions qu'elles fixent comme objectif aux autorités nationales de ne pas étendre, à compter de l'entrée en vigueur de la directive, le champ des exclusions du droit à déduction de la taxe sur la valeur ajoutée prévues par les textes nationaux applicables à cette date ; qu'elles impliquent également que, dans la mesure où la réglementation d'un Etat membre modifie, postérieurement à l'entrée en vigueur de la sixième directive, en le réduisant, le champ des exclusions existantes et se rapproche par là même de l'objectif de cette directive, cette réglementation est couverte par la dérogation prévue à l'article 17 paragraphe 6 second alinéa de la sixième directive et ne méconnaît pas le paragraphe 2 de cet article ; qu'en revanche, une réglementation nationale qui a pour effet d'étendre, postérieurement à l'entrée en vigueur de la directive, le champ des exclusions existantes, en s'éloignant ainsi de l'objectif de cette directive, ne constitue pas une dérogation permise le paragraphe 6 et méconnaît, dès lors, le paragraphe 2 de l'article 17 ; qu'il en va ainsi pour toute modification postérieure à l'entrée en vigueur de la sixième directive qui étend le champ des exclusions applicables immédiatement avant cette modification ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 238 de l'annexe II au code général des impôts dans sa rédaction applicable à la période d'imposition en cause : " N'est pas déductible la taxe ayant grevé : 1° des biens cédés sans rémunération ou moyennant une rémunération très inférieure à leur prix normal (...) sauf quand il s'agit de biens de très faible valeur " ; que, dans sa rédaction antérieure à la date d'entrée en vigueur de la sixième directive, le 1er janvier 1978, cet article excluait du droit à déduction les biens distribués à titre de cadeau et précisait que cette exclusion ne concernait pas les objets de faible valeur conçus spécialement pour la publicité ; <br/>
<br/>
              4. Considérant, d'une part, que, depuis l'entrée en vigueur de l'article 26 du décret du 29 décembre 1979, la déduction de la taxe sur la valeur ajoutée ayant grevé des cadeaux n'est plus subordonnée à la condition que les objets offerts soient de faible valeur et conçus spécialement pour la publicité, mais à la condition que les objets offerts soient de très faible valeur ; que ces dispositions ont ainsi eu pour effet de permettre la déduction de la taxe grevant des objets distribués à titre de cadeaux qui ne sont pas spécialement conçus pour la publicité et sont de très faible valeur, alors qu'avant l'entrée en vigueur de la sixième directive, seuls les cadeaux spécialement destinés à la publicité bénéficiaient du droit à déduction ; que le droit à déduction a de ce fait été étendu ; que, d'autre part, si, dans sa rédaction issue de ce même article 26 du décret du 29 décembre 1979, l'article 238 de l'annexe II au code général des impôts permet de déduire la taxe ayant grevé les cadeaux spécialement conçus pour la publicité ayant une très faible valeur, alors que, précédemment, le décret prévoyait que ces cadeaux ouvraient droit à déduction s'ils étaient de faible valeur, la modification de ce texte ainsi intervenue après l'entrée en vigueur de la sixième directive ne saurait, eu égard à l'absence de délimitation précise de la notion de biens de faible valeur ou de très faible valeur, ainsi d'ailleurs que le révèlent les pratiques administratives antérieures et postérieures à la modification du texte, avoir eu pour effet, par elle-même, d'étendre le champ des exclusions du droit à déduction de taxe sur la valeur ajoutée ;  que, par suite, la cour, en adoptant les motifs du tribunal administratif, n'a pas commis d'erreur de droit en jugeant que le décret n'avait pas restreint les droits à déduction définis antérieurement à l'intervention de la sixième directive ;  <br/>
<br/>
              5. Considérant, en deuxième lieu, que le moyen tiré de ce que la cour aurait, par adoption des motifs retenus par le tribunal administratif de Melun, regardé comme applicable au litige l'article 23 N de l'annexe IV au code général des impôts manque en fait ; que, par suite, le moyen tiré de ce qu'il aurait pour ce motif fait une inexacte application de cet article, du 8 de l'article 257 du code général des impôts et de l'article 238 de l'annexe II au même code en rejetant ses conclusions tendant à la décharge de taxe sur la valeur ajoutée relative aux cadeaux publicitaires ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant, en dernier lieu, que la société soutient que la cour aurait méconnu les articles 256 et 261 C du code général des impôts et dénaturé des pièces du dossier qui lui était soumis en ne la déchargeant pas des rappels de taxe sur la valeur ajoutée mis à sa charge à raison d'une somme versée par l'une de ses filiales, qu'elle aurait dénaturé les pièces du dossier et insuffisamment motivé son arrêt en estimant fondé le rappel de taxe sur la valeur ajoutée relatif à un encaissement en date du 9 mars 2011 provenant d'une autre filiale, qu'elle aurait dénaturé les pièces du dossier et méconnu les articles 38 et 39 du code général des impôts en jugeant non justifiée la déduction de ses résultats imposables de dépenses correspondant notamment à des programmes immobiliers et à des frais de déplacement au Portugal et au Maroc, qu'elle aurait  méconnu les articles 1729 du code général des impôts et L. 195 A du livre des procédures fiscales, entaché son arrêt d'une contradiction de motifs et d'une insuffisance de motivation en laissant à sa charge les pénalités pour mauvaise foi au titre des années 2002 et 2003 et qu'elle aurait dénaturé les pièces du dossier et méconnu l'article 1763 A du code général des impôts en jugeant régulière la procédure d'établissement de l'amende ; qu'aucun de ces autres moyens n'est de nature à justifier l'annulation de l'arrêt attaqué ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Groupe Saint-Germain doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Groupe Saint-Germain est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Groupe Saint-Germain et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
