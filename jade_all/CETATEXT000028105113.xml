<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105113</ID>
<ANCIEN_ID>JG_L_2013_10_000000356976</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 21/10/2013, 356976, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356976</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356976.20131021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 18 février 2013 au secrétariat du contentieux du Conseil d'État, présenté pour les sociétés Numéricâble SAS et NC Numéricâble, dont le siège est 10 rue Albert Einstein, à Champs-sur-Marne (77420), représentées par leur représentant légal, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; les sociétés requérantes demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation de la décision n° 2011-1469 du 20 décembre 2011 par laquelle l'Autorité de régulation des communications électroniques et des postes a prononcé une sanction pécuniaire à leur encontre en application de l'article L. 36-11 du code des postes et des communications électroniques, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 36-8 et L. 36-11 du code des postes et des communications électroniques ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code des postes et des communications électroniques, notamment ses articles L. 36-8 et L. 36-11 ;<br/>
<br/>
              Vu la loi n° 96-659 du 26 juillet 1996 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu l'ordonnance n° 2011-1012 du 24 août 2011 ;<br/>
<br/>
              Vu la décision n° 2006-0044 du 10 janvier 2006 de l'Autorité de régulation des communications électroniques et des postes portant règlement intérieur ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Numéricâble SAS ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que l'article L. 36-8 du code des postes et des communications électroniques définit la procédure de règlement des différends applicable devant l'Autorité de régulation des communications électroniques et des postes ; que l'article L. 36-11 du même code définit la procédure de sanction applicable devant cette autorité ; que les sociétés requérantes soutiennent que ces dispositions, en ce qu'elles organisent une procédure de règlement des différends et une procédure de sanction sans prévoir de stricte séparation, d'une part, entre les fonctions de poursuite, d'instruction et de jugement, et, d'autre part, entre les attributions de l'autorité en matière de règlement des différends et la compétence qu'elle détient pour  prononcer une sanction à l'encontre d'un exploitant de réseau ou fournisseur de services qui ne se serait pas conformé, dans les délais fixés, à une décision de règlement d'un différend, méconnaissent les principes d'indépendance et d'impartialité résultant de l'article 16 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              3.	Considérant que l'article L. 36-8 du code des postes et des communications électroniques, qui est exclusivement relatif à la procédure de règlement des différends devant l'Autorité de régulation des communications électroniques et des postes ne constitue pas le fondement de la décision attaquée par les sociétés requérantes, qui est une décision de sanction prononcée par cette autorité en application de l'article L. 36-11 du même code ; que, par suite, l'article L. 36-8 du code des postes et des communications électroniques n'est pas applicable au litige ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions de l'article L. 36-8 du code des postes et des communications électroniques porteraient atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              4.	Considérant, en revanche, que l'article L. 36-11 du code des postes et des communications électroniques est applicable au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; <br/>
<br/>
              5.	Considérant que le Conseil constitutionnel a, dans les motifs et le dispositif de sa décision n° 96-378 DC du 23 juillet 1996,  déclaré conformes à la Constitution les dispositions de l'article L. 36-11, telles qu'issues de la loi du 26 juillet 1996 de réglementation des télécommunications ; que, toutefois, les développements de la jurisprudence du Conseil constitutionnel, dont se prévaut la requête, en ce qui concerne les principes d'indépendance et d'impartialité, manifestés notamment par les décisions n° 2011-200 QPC du 2 décembre 2011 et n° 2012-280 QPC du 12 octobre 2012, constituent une circonstance de droit nouvelle de nature à justifier que la conformité de cette disposition à la Constitution soit à nouveau examinée par le Conseil constitutionnel ; <br/>
<br/>
              6.	Considérant que le moyen tiré de ce que les dispositions de l'article L. 36-11 du code des postes et des communications électroniques portent atteinte aux droits et libertés garantis par la Constitution, notamment aux principes d'indépendance et d'impartialité découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen, soulève une question qui présente un caractère sérieux au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; <br/>
<br/>
              7.	Considérant qu'il résulte de tout ce qui précède qu'il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à l'encontre du seul article L. 36-11 du code des postes et des communications électroniques ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 36-11 du code des postes et des communications électroniques est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 36-8 du code des postes et des communications électroniques.<br/>
<br/>
Article 3 : La présente décision sera notifiée aux sociétés Numéricâble SAS et NC Numéricâble, à l'Autorité de régulation des communications électroniques et des postes et au ministre du redressement productif. Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
