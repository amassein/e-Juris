<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288253</ID>
<ANCIEN_ID>JG_L_2014_07_000000365675</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 23/07/2014, 365675, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365675</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:365675.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 14 février 2014 par laquelle le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. et Mme A...qui sont dirigées contre l'arrêt n° 10PA03878 du 29 novembre 2012 de la cour administrative d'appel de Paris en tant qu'il s'est prononcé sur le bien-fondé de la fraction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales correspondant à la réintégration dans le revenu imposable de M. et Mme A...des amortissements déduits des bénéfices non commerciaux réalisés par M.A... ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 juillet 2014, présentée pour M. A...; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les éléments d'actif affectés à l'exercice d'une profession non commerciale et visés au 1 de l'article 93 du code général des impôts s'entendent, soit de biens qui, spécifiquement nécessaires à l'activité du contribuable, ne peuvent être distraits par celui-ci de son actif professionnel, soit de biens qui, de la nature de ceux dont l'usage est requis pour l'exercice de cette activité, sont effectivement utilisés à cette fin par le contribuable, et que, s'il en est propriétaire, celui-ci peut, à son choix, maintenir dans son patrimoine personnel ou rattacher à son actif professionnel et porter, dans ce dernier cas, sur le registre des immobilisations prévu à l'article 99 du code général des impôts ; qu'en revanche, un bien dont la détention ne revêt aucune utilité professionnelle ne peut, alors même que le contribuable l'aurait, à tort, inscrit sur le registre de ses immobilisations, constituer, au regard de la loi fiscale, un élément de son actif professionnel ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des termes de l'arrêt attaqué que, pour juger que les dépenses afférentes aux travaux de maçonnerie, à la création d'un escalier intérieur et d'une rambarde, aux travaux d'électricité, aux travaux de serrurerie, à la réfection des revêtements, à l'acquisition de tapis-sol et aux honoraires de bureau d'études n'étaient pas des actifs affectés à l'exercice de la profession de M.A..., la cour administrative d'appel s'est bornée à juger que ces dépenses ne portaient pas sur des installations et aménagements spécifiquement nécessaires à l'activité de médecin exercée par ce dernier ; qu'en statuant ainsi, sans rechercher si, ainsi qu'il était soutenu devant elle, ces travaux constituaient des éléments d'actifs qui, de la nature de ceux dont l'usage est requis pour l'exercice de l'activité de M.A..., étaient effectivement utilisés à cette fin par ce dernier, la cour a commis une erreur de droit ; que son arrêt doit, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi venant à l'appui des mêmes conclusions, être annulé en tant qu'il a statué sur le bien-fondé de la fraction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans le revenu imposable de M. et Mme A...des amortissements de ces travaux déduits des bénéfices non commerciaux réalisés par M. A... ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des termes de l'arrêt attaqué que, pour juger que M. et Mme A...n'avaient pu déduire des bénéfices non commerciaux réalisés en 1999 par M. A...l'amortissement des dépenses afférentes à la réfection des lignes électriques pour un montant de 26 180 francs, la cour n'a pas jugé que ces dépenses ne constituaient pas un élément d'actif affecté à l'exercice de la profession de M. A..., mais qu'elles ne se rattachaient pas à l'année 1999 ; qu'en statuant ainsi, elle a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine exempte de dénaturation ; que M. et Mme A...ne sont donc pas fondés à demander l'annulation de l'arrêt attaqué en tant qu'il a statué sur le bien-fondé de la fraction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mis à leur charge résultant de la réintégration dans leur revenu imposable au titre de l'année 1999 de l'amortissement des dépenses afférentes à la réfection des lignes électriques ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. et Mme A... sont seulement fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il a statué sur le bien-fondé de la fraction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mis à leur charge résultant de la réintégration dans leur revenu imposable des amortissements des travaux énumérés au point 2 ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme A...au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 29 novembre 2012 est annulé en tant qu'il a statué sur le bien-fondé de la fraction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mis à la charge de M. et Mme A...résultant de la réintégration dans leur revenu imposable des amortissements des travaux de maçonnerie, de la création d'un escalier intérieur et d'une rambarde, des travaux d'électricité à l'exclusion des dépenses afférentes à la réfection des lignes électriques pour un montant de 26 180 francs, des travaux de serrurerie, de la réfection des revêtements, de l'acquisition de tapis-sol et des honoraires de bureau d'études.<br/>
Article 2 : Le surplus des conclusions admises par la décision du 14 février 2014 est rejeté.<br/>
Article 3 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 4 : L'Etat versera à M. et Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
