<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043335956</ID>
<ANCIEN_ID>JG_L_2021_03_000000450364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/33/59/CETATEXT000043335956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/03/2021, 450364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450364.20210322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au préfet des Alpes-Maritimes d'examiner sa demande de renouvellement de titre de séjour dans un délai de sept jours à compter de la notification de l'ordonnance et, d'autre part, de lui délivrer un récépissé de renouvellement de titre de séjour avec autorisation de travail dans un délai de 48 heures. Par une ordonnance n° 2100755 du 16 février 2021, le juge des référés du tribunal administratif de Nice a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 et 12 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le juge des référés de première instance a commis une erreur de droit en ce qu'il a jugé que l'urgence n'était pas caractérisée en l'absence de contestation dans le délai imparti du refus implicite de lui délivrer un récépissé alors que la circonstance qu'une décision administrative soit opposée au requérant ne fait pas obstacle à la saisine du juge des référés au titre de l'article L. 521-2 du code de justice administrative ; <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, le refus de lui délivrer un récépissé fait obstacle à la continuité de sa vie professionnelle et, ce faisant, la prive des ressources nécessaires pour subvenir aux besoins essentiels de son foyer, en deuxième lieu, ce refus a été contesté par son avocat qui a adressé aux services de la préfecture, le 4 février 2021, un courrier tendant à ce que ce document lui soit délivré, en troisième lieu, l'absence de récépissé ne lui est pas imputable dès lors que la demande de renouvellement de son titre de séjour a été déposée dans le délai imparti et, en dernier lieu, en s'abstenant de renouveler son autorisation provisoire de séjour, le préfet des Alpes-Maritimes a porté une atteinte grave et manifestement illégale à ses libertés fondamentales, notamment à sa liberté de travailler et à sa liberté d'aller et venir ;<br/>
              - le récépissé lui a été délivré postérieurement à l'ordonnance du juge des référés.<br/>
<br/>
              Par un mémoire en défense, enregistré le 10 mars 2021, le ministre de l'intérieur conclut à l'irrecevabilité de la requête. Il soutient que la requérante a obtenu satisfaction avant l'introduction de sa requête puisqu'elle s'est vu délivrer, le 23 février 2021, un récépissé valable pendant une période de six mois.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 12 mars 2021 2021 à 18 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Par une ordonnance du 16 février 2021, le juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de Mme A... tendant à ce qu'il soit enjoint au préfet des Alpes-Maritimes d'examiner sa demande de renouvellement de titre de séjour dans un délai de sept jours à compter de la notification de l'ordonnance et, d'autre part, de lui délivrer un récépissé de renouvellement de titre de séjour avec autorisation de travail dans un délai de 48 heures. Mme A... relève appel de cette ordonnance.  <br/>
<br/>
              3. Il ressort du mémoire en défense produit par le ministre de l'intérieur que le préfet des Alpes-Maritimes a délivré le 23 février 2021 à Mme A... un récépissé de sa demande de renouvellement de titre de séjour valable jusqu'au 22 août 2021. Par suite, les conclusions de Mme A... sont devenues sans objet. Il n'y a donc plus lieu à statuer.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat le versement de la somme de 2 000 euros à Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête présentée par Mme A... sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
Article 2 : L'Etat versera à Mme A... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à Mme B... A... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
