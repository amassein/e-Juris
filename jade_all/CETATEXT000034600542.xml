<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600542</ID>
<ANCIEN_ID>JG_L_2017_05_000000403758</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 05/05/2017, 403758, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403758</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403758.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...A...et Mme B...C...épouse A...ont demandé au juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 15 juillet 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France rejetant le recours formé contre la décision du consul général de France à Tunis refusant la délivrance à M. A...d'un visa de long séjour en qualité de conjoint de ressortissant français. Par une ordonnance n° 1607002 du 6 septembre 2016, le juge des référés du tribunal administratif de Nantes a suspendu l'exécution de la décision de la commission de recours et enjoint au ministre de l'intérieur de procéder au réexamen de la demande de visa dans le délai d'un mois. <br/>
<br/>
              Par un pourvoi, enregistré le 23 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par M. et MmeA....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. et Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 211-2-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " (...) Le visa de long séjour ne peut être refusé à un conjoint de Français qu'en cas de fraude, d'annulation du mariage ou de menace à l'ordre public. Le visa de long séjour est délivré de plein droit au conjoint de Français qui remplit les conditions prévues au présent article " ; <br/>
<br/>
              3.	Considérant que l'administration peut faire valoir devant le juge des référés que la décision dont il lui est demandé de suspendre l'exécution, sur le fondement de l'article L. 521-1 du code de justice administrative, est légalement justifiée par un motif, de droit ou de fait, autre que celui initialement indiqué, mais également fondé sur la situation existant à la date de cette décision ; qu'il appartient alors au juge des référés, après avoir mis à même l'auteur de la demande, dans des conditions adaptées à l'urgence qui caractérise la procédure de référé, de présenter ses observations sur la substitution ainsi sollicitée, de rechercher s'il ressort à l'évidence des données de l'affaire, en l'état de l'instruction, que ce motif est susceptible de fonder légalement la décision et que l'administration aurait pris la même décision si elle s'était fondée initialement sur ce motif ; que, dans l'affirmative et à condition que la substitution demandée ne prive pas le requérant d'une garantie procédurale liée au motif substitué, le juge des référés peut procéder à cette substitution pour apprécier s'il y a lieu d'ordonner la suspension qui lui est demandée ;<br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, pour confirmer la décision du consul général de France à Tunis refusant la délivrance à M. A... d'un visa de long séjour en qualité de conjoint de ressortissant français, la commission de recours contre les décisions de refus de visa d'entrée en France s'est fondée sur l'absence de maintien des liens matrimoniaux entre M. A...et Mme C...et sur le caractère complaisant du mariage contracté ; que, pour établir que la décision attaquée était légale, le ministre des affaires étrangères a invoqué, dans son mémoire en défense communiqué à M. et MmeA..., un autre motif, tiré de la menace que la présence de M. A...sur le territoire ferait peser sur l'ordre public ; que le juge des référés du tribunal administratif a toutefois estimé que l'existence de cette menace ne ressortait pas à l'évidence des données de l'affaire ; qu'il ressort toutefois des éléments produits dans le cadre de l'instruction menée par le juge des référés, en particulier des procès-verbaux en date du 27 novembre 2012 et du 15 décembre 2014 établissant que M. A...a exercé des violences physiques et morales sur sa première épouse qui a mis fin à ses jours en novembre 2014, qu'il a fait l'objet d'interpellations pour des infractions à la législation sur les stupéfiants et des différends de voisinage, et qu'il s'est maintenu irrégulièrement sur le territoire malgré le refus de délivrance d'un titre de séjour et une l'obligation de quitter le territoire délivrés à son encontre le 8 avril 2015 ; qu'en ne retenant pas en l'état de l'instruction la menace à l'ordre public soulevée par le ministre, le juge des référés du tribunal administratif de Nantes a dénaturé les pièces du dossier qui lui était soumis ; que, dès lors, le ministre de l'intérieur est fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par M. et Mme A...;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit précédemment qu'en l'état de l'instruction, la menace que la présence de M. A...sur le territoire français est de nature à porter à l'ordre public est de nature à fonder légalement la décision de refus de visa prise par la commission de recours contre les décisions de refus de visa d'entrée en France ; qu'il résulte de l'instruction que la commission aurait pris la même décision de refus de visa en se fondant sur ce motif ; que la substitution de ce motif ne prive M. A...d'aucune garantie procédurale ; qu'il s'ensuit que les moyens soulevés à l'encontre de la décision contestée ne sont pas de nature à créer un doute sérieux sur sa légalité ; que, dès lors, M. et Mme A...ne sont pas fondés à demander la suspension de l'exécution de la décision du 15 juillet 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France rejetant le recours formé contre la décision du consul général de France à Tunis refusant la délivrance à M. A...d'un visa de long séjour en qualité de conjoint de ressortissant français ;<br/>
<br/>
              7. Considérant que les dispositions des articles L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions de la requête présentées sur le fondement de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 6 septembre 2016 du juge des référés du tribunal administratif de Nantes est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. et Mme A...devant le juge des référés du tribunal administratif de Nantes est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées devant le Conseil d'Etat au titre des articles L. 761 1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 4 : La présente ordonnance sera notifiée au ministre de l'intérieur, à M. D...A...et Mme B...C...épouseA....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
