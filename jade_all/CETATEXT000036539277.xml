<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036539277</ID>
<ANCIEN_ID>JG_L_2018_01_000000399726</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/53/92/CETATEXT000036539277.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 24/01/2018, 399726, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399726</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:399726.20180124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Poitiers de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre des années 2008 et 2009. Par un jugement no 1200601, 1200602 du 17 juillet 2014, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14BX02773 du 10 mars 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 mai et 4 août 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 4500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B...a inclus dans le nombre de parts qu'elle a déclaré pour déterminer le quotient familial applicable au calcul de son impôt sur le revenu des années 2008 et 2009 un quart de part pour chacun de ses deux enfants mineurs, en résidence alternée chez leurs deux parents. A la suite d'un contrôle sur pièces, l'administration fiscale a remis en cause cette majoration, au motif que les deux enfants étaient rattachés au foyer fiscal de leur père. Mme B...se pourvoit en cassation contre l'arrêt du 10 mars 2016 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel dirigé contre le jugement du 17 juillet 2014 du tribunal administratif de Poitiers rejetant sa demande en décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre des années 2008 et 2009.<br/>
<br/>
              2. Aux termes du I de l'article 194 du code général des impôts, dans leur rédaction applicable : " (...) / En cas de résidence alternée au domicile de chacun des parents et sauf disposition contraire dans la convention homologuée par le juge, la décision judiciaire ou, le cas échéant, l'accord entre les parents, les enfants mineurs sont réputés être à la charge égale de l'un et de l'autre parent. Cette présomption peut être écartée s'il est justifié que l'un d'entre eux assume la charge principale des enfants. / Lorsque les enfants sont réputés être à la charge égale de chacun des parents, ils ouvrent droit à une majoration de : / a) 0,25 part pour chacun des deux premiers et 0,5 part à compter du troisième, lorsque par ailleurs le contribuable n'assume la charge exclusive ou principale d'aucun enfant ; / (...) ". Il résulte de ces dispositions que, pour déterminer le nombre de parts de quotient familial à prendre en considération pour la division du revenu imposable prévue à l'article 193 du même code, les enfants mineurs en résidence alternée sont réputés être à la charge égale de chacun de leurs deux parents, sauf lorsqu'une convention homologuée par le juge, une décision du juge tranchant un désaccord ou un accord extrajudiciaire des parents en dispose autrement. La présomption de charge égale des enfants peut, toutefois, être écartée s'il est justifié que l'un des parents assume la charge principale des enfants.<br/>
<br/>
              3. Devant la cour, Mme B...soutenait que ses deux enfants mineurs devaient être regardés, pour l'application des dispositions du I de l'article 194 du code général des impôts, comme pris en charge, de manière égale, par chacun des deux parents. La cour, après avoir relevé que l'ordonnance du 11 mars 2004 du juge aux affaires familiales du tribunal de grande instance de La Rochelle fixant les effets sur les enfants de la séparation avec son ex-conjoint avait constaté l'accord des parents en cours d'audience, et énoncé notamment que Mme B...bénéficierait seule des ressources provenant des prestations familiales et se verrait, en outre, rembourser par le père des enfants la moitié des dépenses qu'elle exposerait, en a déduit, par une motivation suffisante, que ces dispositions formalisaient un accord des parents prévoyant que les enfants, quoiqu'en résidence alternée chez leurs deux parents, seraient à la charge principale de leur père. Ces circonstances faisant obstacle à ce que la charge soit réputée également répartie entre les parents, la cour n'a pas commis d'erreur de droit en ne recherchant pas si l'application qui était faite des termes de cette ordonnance ne conduisait pas, en réalité, à une répartition égale de la charge des enfants, en l'absence d'élément révélant une modification, en ce sens, des termes de l'accord survenu entre les parents.<br/>
<br/>
              4. Il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente affaire, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
