<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037258916</ID>
<ANCIEN_ID>JG_L_2018_06_000000408819</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/89/CETATEXT000037258916.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 20/06/2018, 408819</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408819</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408819.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 408819, par une décision du 28 juin 2017, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) dirigées contre l'arrêt n° 15LY01810, 16LY02174, 16LY02175 de la cour administrative d'appel de Lyon du 10 janvier 2017, en tant seulement que cet arrêt a statué sur la réparation des préjudices extrapatrimoniaux permanents de M. C...A....<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 décembre 2017, M. D... A..., venant aux droits de M. C...A..., conclut au rejet du pourvoi et à ce qu'une somme de 3 000 euros soit mise à la charge de l'ONIAM au titre de l'article L. 761-1 du code de justice administrative. Il soutient que les moyens du pourvoi ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 410118, par une requête, enregistrée le 26 avril 2017 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat d'ordonner le sursis à exécution de cet arrêt.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. D... A..., venant aux droits de M. C...A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C... A...a, le 17 septembre 1984, subi une transfusion sanguine à l'hôpital Bellevue de Saint-Etienne lors d'une intervention chirurgicale pratiquée à la suite d'une fracture de la malléole droite ; qu'imputant à ces transfusions sa contamination par le virus de l'hépatite C, diagnostiquée en 1997, il a saisi l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) d'une demande d'indemnisation, qui a été rejetée le 21 avril 2011 ; que, par un jugement du 9 avril 2015, le tribunal administratif de Lyon a retenu l'origine transfusionnelle de la contamination ; que, par un jugement du 12 mai 2016, il condamné l'ONIAM à verser à M. A..., au titre de la solidarité nationale, une somme de 255 732,50 euros en réparation des préjudices de tous ordres imputables à sa contamination et à l'hépatite qu'elle avait provoquée, ainsi qu'une rente annuelle d'un montant de 28 565,30 euros ; que M. A...est décédé le 19 mai 2016 des conséquences de sa pathologie hépatique ; que, par un arrêt du 10 janvier 2017, la cour administrative d'appel de Lyon, statuant sur appel de l'ONIAM et appel incident de M. D... A..., venu aux droits de son père, a porté à 259 391,50 euros l'indemnité mise à la charge de l'ONIAM et annulé le jugement du tribunal administratif de Lyon en tant qu'il condamnait l'office au versement d'une rente annuelle de 28 565,30 euros ; que, par une décision du 28 juin 2017, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de l'ONIAM dirigées contre cet arrêt en tant qu'il statue sur la réparation des préjudices extrapatrimoniaux permanents de M.A... ; que l'ONIAM a présenté une requête tendant à ce qu'il soit sursis à l'exécution de l'arrêt ; qu'il y a lieu de joindre cette requête au pourvoi afin de statuer par une même décision ; <br/>
<br/>
              2. Considérant que le droit à la réparation d'un dommage, quelle que soit sa nature, s'ouvre à la date à laquelle se produit le fait qui en est directement la cause ; que si la victime du dommage décède sans que ses droits aient été définitivement fixés, c'est-à-dire, en cas de litige, avant qu'une décision juridictionnelle définitive ait fixé le montant de l'indemnisation, son droit, entré dans son patrimoine avant son décès, est transmis à ses héritiers ; que, cependant, le préjudice subi par la victime, ayant cessé au moment du décès, doit être évalué à la date de cet événement, y compris lorsque le décès est lié au fait ouvrant droit à indemnisation, auquel cas d'ailleurs ce décès peut être pris en compte au titre du droit à réparation des proches de la victime ; que ces règles sont également applicables à l'indemnisation de dommages corporels au titre de la solidarité nationale ; <br/>
<br/>
              3. Considérant que si la cour administrative d'appel a évalué les préjudices patrimoniaux subis par M. A...à la date du décès de celui-ci, survenu le 19 mai 2016, postérieurement au jugement du tribunal administratif fixant ses droits à indemnisation, en refusant notamment l'indemnisation de frais futurs afférents à l'assistance d'une tierce personne, il ressort des termes de son arrêt qu'elle a, en revanche, estimé que le décès ne devait avoir aucune incidence sur l'évaluation de ses préjudices extrapatrimoniaux permanents ; qu'en se prononçant ainsi, alors que l'évaluation de ces préjudices effectuée par les premiers juges tenait compte de l'espérance de vie de la victime, la cour a méconnu les règles rappelées ci-dessus ; que son arrêt est, par suite, entaché d'erreur de droit et doit être annulé en tant qu'il statue sur l'indemnisation des préjudices extrapatrimoniaux permanents de M.A... ;<br/>
<br/>
              4. Considérant que, dès lors que la présente décision statue sur le pourvoi de l'ONIAM, sa requête tendant à ce qu'il soit sursis à l'exécution de l'arrêt perd son objet ; qu'il n'y a plus lieu d'y statuer ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme de 3 000 euros à verser à l'ONIAM au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'ONIAM qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 10 janvier 2017 est annulé en tant qu'il statue sur l'indemnisation des préjudices extrapatrimoniaux permanents de M.A....<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon dans la limite de la cassation prononcée.<br/>
<br/>
		Article 3 : Il n'y a pas lieu de statuer sur la requête n° 410118 de l'ONIAM.<br/>
<br/>
Article 4 : M. A...versera à l'ONIAM une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à M. D... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-05 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. RESPONSABILITÉ RÉGIE PAR DES TEXTES SPÉCIAUX. - INDEMNISATION DE DOMMAGES CORPORELS AU TITRE DE LA SOLIDARITÉ NATIONALE PAR L'ONIAM - RÈGLES APPLICABLES LORSQUE LA VICTIME DÉCÈDE AVANT QUE SES DROITS AIENT ÉTÉ DÉFINITIVEMENT FIXÉS - TRANSFERT DU DROIT À RÉPARATION AUX HÉRITIERS - EXISTENCE [RJ1] - DATE D'ÉVALUATION DU PRÉJUDICE SUBI PAR LA VICTIME - DATE DE DÉCÈS DE LA VICTIME, Y COMPRIS LORSQUE LE DÉCÈS EST LIÉ AU FAIT OUVRANT DROIT À INDEMNISATION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. DATE D'ÉVALUATION. - VICTIME DÉCÉDÉE AVANT QUE SES DROITS AIENT ÉTÉ DÉFINITIVEMENT FIXÉS - 1) TRANSFERT DU DROIT À RÉPARATION AUX HÉRITIERS - EXISTENCE [RJ1] - DATE D'ÉVALUATION DU PRÉJUDICE SUBI PAR LA VICTIME - DATE DE DÉCÈS DE LA VICTIME, Y COMPRIS LORSQUE LE DÉCÈS EST LIÉ AU FAIT OUVRANT DROIT À INDEMNISATION [RJ2] - 2) APPLICATION À L'INDEMNISATION DES DOMMAGES CORPORELS AU TITRE DE LA SOLIDARITÉ NATIONALE.
</SCT>
<ANA ID="9A"> 60-01-05 Le droit à la réparation d'un dommage, quelle que soit sa nature, s'ouvre à la date à laquelle se produit le fait qui en est directement la cause. Si la victime du dommage décède sans que ses droits aient été définitivement fixés, c'est-à-dire, en cas de litige, avant qu'une décision juridictionnelle définitive ait fixé le montant de l'indemnisation, son droit, entré dans son patrimoine avant son décès, est transmis à ses héritiers. Cependant, le préjudice subi par la victime, ayant cessé au moment du décès, doit être évalué à la date de cet événement, y compris lorsque le décès est lié au fait ouvrant droit à indemnisation, auquel cas d'ailleurs ce décès peut être pris en compte au titre du droit à réparation des proches de la victime. Ces règles sont également applicables à l'indemnisation de dommages corporels au titre de la solidarité nationale.</ANA>
<ANA ID="9B"> 60-04-03-01 1) Le droit à la réparation d'un dommage, quelle que soit sa nature, s'ouvre à la date à laquelle se produit le fait qui en est directement la cause. Si la victime du dommage décède sans que ses droits aient été définitivement fixés, c'est-à-dire, en cas de litige, avant qu'une décision juridictionnelle définitive ait fixé le montant de l'indemnisation, son droit, entré dans son patrimoine avant son décès, est transmis à ses héritiers. Cependant, le préjudice subi par la victime, ayant cessé au moment du décès, doit être évalué à la date de cet événement, y compris lorsque le décès est lié au fait ouvrant droit à indemnisation, auquel cas d'ailleurs ce décès peut être pris en compte au titre du droit à réparation des proches de la victime.... ,,2) Ces règles sont également applicables à l'indemnisation de dommages corporels au titre de la solidarité nationale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 29 mars 2000, Assistance publique - Hôpitaux de Paris, n° 195662, p. 147., ,[RJ2] Cf. CE, 17 juillet 1950, M. Mouret, p. 447 ; CE, Section, 8 novembre 1968, Commune de Mounes-Prohencoux, n° 68823, p. 563.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
