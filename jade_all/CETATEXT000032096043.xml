<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032096043</ID>
<ANCIEN_ID>JG_L_2016_02_000000384750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/09/60/CETATEXT000032096043.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 19/02/2016, 384750, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:384750.20160219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulon de condamner l'Etat à lui verser une indemnité de 39 194 euros en réparation du préjudice résultant de la décision du 26 juin 2009 du préfet du Var lui retirant le concours de la force publique, qu'il lui avait accordé par une décision du 26 janvier 2009 à compter du 1er juillet suivant, en exécution d'un jugement du tribunal d'instance de Saint-Tropez du 11 juillet 2008 ordonnant l'expulsion des occupants d'une villa dont elle est propriétaire à Gassin. Par un jugement n° 1302871 du 23 juillet 2014, le tribunal administratif a condamné l'Etat à lui verser une somme de 1 000 euros au titre de son préjudice moral et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire enregistrés les 24 septembre et 26 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code des procédures civiles d'exécution ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le tribunal d'instance de Saint-Tropez a, par un jugement du 11 juillet 2008, résilié le bail et ordonné l'expulsion des deux occupants d'une villa sise à Gassin et appartenant à Mme A... ; que, le 1er octobre 2008, cette dernière a demandé au préfet du Var de lui accorder le concours de la force publique afin de procéder à l'exécution de cette décision de justice ; que, par une décision du 26 janvier 2009, le préfet a accordé ce concours à compter du 1er juillet 2009 ; qu'il a toutefois retiré cette décision le 26 juin 2009 ; que Mme A...a recherché la responsabilité de l'Etat devant le tribunal administratif de Toulon, en demandant la condamnation de l'Etat à lui verser une somme de 39 194 euros correspondant au préjudice tenant aux frais engagés inutilement pour se rendre à Gassin à la date prévue pour la libération de la maison, soit 1 194,50 euros, à la privation de jouissance de la villa en juillet et août 2009, soit 24 000 euros, aux frais d'expulsion, soit 4 000 euros, et à son préjudice moral, chiffré à 10 000 euros ; que, par le jugement du 23 juillet 2014 contre lequel elle se pourvoit en cassation, le tribunal administratif lui a alloué une somme de 1 000 euros au titre de son préjudice moral ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond, parmi lesquelles figure l'accusé de réception postal de ce courrier, que l'avocat de Mme A...a été régulièrement avisé de la date de l'audience au cours de laquelle le tribunal administratif a examiné sa demande, par un courrier dont il a accusé réception le 31 mai 2014 ; que le moyen tiré de la méconnaissance des dispositions de l'article R. 711-2 du code de justice administrative ne saurait, dès lors, être accueilli ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que le tribunal administratif, appelé à statuer comme juge de plein contentieux sur les droits à indemnisation de MmeA..., n'avait pas à se prononcer sur la décision par laquelle le préfet du Var, répondant à la réclamation préalable qu'elle avait présentée afin de lier le contentieux en demandant le versement d'une somme de 39 344,50 euros, lui avait proposé à titre amiable une somme totale de 4 900 euros ; que le moyen tiré de ce que le juge du fond aurait méconnu son office doit, par suite, être écarté ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond qu'au vu de la décision du 26 janvier 2009 lui accordant le concours de la force publique, Mme A...a, le 26 juin 2009, quitté la Belgique où elle résidait et s'est rendue à Gassin afin de prendre possession de sa villa à compter du 1er juillet, date fixée pour l'expulsion des occupants ; qu'elle n'a eu connaissance qu'après son arrivée de la décision du 26 juin 2009 lui retirant le concours de la force publique ; qu'en rejetant ses conclusions tendant à ce que les frais afférents à ce déplacement soient mis à la charge de l'Etat, au motif qu'il n'existait pas de lien direct entre la décision du 26 juin 2009 et le préjudice tenant à ces frais, le tribunal administratif a inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              5. Considérant enfin que, pour rejeter les conclusions de Mme A...tendant à l'octroi d'une indemnité de 24 000 euros, évaluée par référence au loyer d'une villa comparable pendant deux mois, au titre de la privation de jouissance de sa villa en juillet et août 2009, le tribunal administratif a jugé que ce préjudice ne pouvait être évalué que sur la base de la valeur locative prise en compte par le juge civil pour fixer les indemnités d'occupation mises à la charge des locataires ; qu'en statuant ainsi, alors que l'appréciation portée par le juge civil ne s'imposait pas à lui, le tribunal a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède et alors que Mme A...n'a pas soulevé de moyens relatifs à l'évaluation de son préjudice moral ni au rejet par le tribunal de l'indemnisation des frais d'expulsion, que le jugement attaqué doit être annulé en tant qu'il statue sur les conclusions de Mme A...tendant à l'octroi d'indemnités au titre des frais de déplacement exposés en juin 2009 et de la privation de jouissance de sa villa en juillet et août 2009 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 23 juillet 2014 du tribunal administratif de Toulon est annulé en tant qu'il statue sur les conclusions de Mme A...tendant à l'octroi d'indemnités au titre des frais de déplacement exposés en juin 2009 et de la privation de jouissance de sa villa en juillet et août 2009.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulon dans la mesure de la cassation prononcée.<br/>
Article 3 : L'Etat versera à Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
