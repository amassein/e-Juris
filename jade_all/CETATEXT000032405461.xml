<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032405461</ID>
<ANCIEN_ID>JG_L_2016_04_000000387922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/54/CETATEXT000032405461.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 13/04/2016, 387922, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387922.20160413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir les décisions des 3, 4 et 15 février 2010 par lesquelles le directeur de l'établissement public de santé de Ville-Evrard a successivement décidé son hospitalisation à la demande d'un tiers et l'a maintenue en hospitalisation sous ce régime. Par un jugement n° 1209539 du 17 octobre 2013, le tribunal administratif de Montreuil a annulé ces décisions.<br/>
<br/>
              Par un arrêt n° 13VE03725 du 16 décembre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'établissement public de santé de Ville-Evrard contre le jugement du tribunal administratif de Montreuil.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 février, 23 mars, et 8 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, l'établissement public de santé de Ville-Evrard demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - la loi n° 2011-803 du 5 juillet 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'établissement public de santé de Ville-Evrard, et à la SCP Boré, Salve de Bruneton, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...B...a été hospitalisée sur demande d'un tiers à l'établissement public de santé de Ville-Evrard par décision du 3 février 2010 ; que cette hospitalisation a été prolongée par deux décisions des 4 et 15 février 2010 ; que, par un jugement du 17 octobre 2013, le tribunal administratif de Montreuil a annulé la décision du 3 février 2010 et les décisions subséquentes au motif que l'établissement de santé devait être regardé comme n'ayant pas produit la demande d'hospitalisation formée par le tiers et que, dès lors, le moyen tenant à l'illégalité de la demande du tiers devait être accueilli ; que, par l'arrêt attaqué du 16 décembre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'établissement public de santé de Ville-Evrard contre ce jugement ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 3211-3 du code de la santé publique, dans sa rédaction applicable à la date des décisions attaquées : " Lorsqu'une personne atteinte de troubles mentaux est hospitalisée sans son consentement en application des dispositions des chapitres II et III du présent titre ou est transportée en vue de cette hospitalisation, les restrictions à l'exercice de ses libertés individuelles doivent être limitées à celles nécessitées par son état de santé et la mise en oeuvre de son traitement. En toutes circonstances, la dignité de la personne hospitalisée doit être respectée et sa réinsertion recherchée. / Elle doit être informée dès l'admission et par la suite, à sa demande, de sa situation juridique et de ses droits (...) " ; qu'aux termes de l'article L. 3212-1 du même code, dans sa rédaction applicable à la même date : " Une personne atteinte de troubles mentaux ne peut être hospitalisée sans son consentement sur demande d'un tiers que si : / 1° Ses troubles rendent impossible son consentement ; / 2° Son état impose des soins immédiats assortis d'une surveillance constante en milieu hospitalier. / La demande d'admission est présentée soit par un membre de la famille du malade, soit par une personne susceptible d'agir dans l'intérêt de celui-ci, à l'exclusion des personnels soignants dès lors qu'ils exercent dans l'établissement d'accueil. / Cette demande doit être manuscrite et signée par la personne qui la formule (...) / Elle comporte les nom, prénoms, profession, âge et domicile tant de la personne qui demande l'hospitalisation que de celle dont l'hospitalisation est demandée et l'indication de la nature des relations qui existent entre elles ainsi que, s'il y a lieu, de leur degré de parenté (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, que l'article L. 1111-7 du code de la santé publique, dans sa rédaction applicable au litige, prévoit que : " Toute personne a accès à l'ensemble des informations concernant sa santé détenues, à quelque titre que ce soit, par les professionnels et établissements de santé, qui sont formalisées ou ont fait l'objet d'échanges écrits entre professionnels de santé, notamment des résultats d'examen, comptes rendus de consultation, d'intervention, d'exploration ou d'hospitalisation, des protocoles et prescriptions thérapeutiques mis en oeuvre, feuilles de surveillance, correspondances entre professionnels de santé, à l'exception des informations mentionnant qu'elles ont été recueillies auprès de tiers n'intervenant pas dans la prise en charge thérapeutique ou concernant un tel tiers (...) A titre exceptionnel, la consultation des informations recueillies, dans le cadre d'une hospitalisation sur demande d'un tiers ou d'une hospitalisation d'office, peut être subordonnée à la présence d'un médecin désigné par le demandeur en cas de risques d'une gravité particulière. En cas de refus du demandeur, la commission départementale des hospitalisations psychiatriques est saisie. Son avis s'impose au détenteur des informations comme au demandeur (...) " ; que les dispositions du II de l'article 6 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, aujourd'hui codifiées à l'article L. 311-6 du code des relations entre le public et l'administration, disposent, par ailleurs, que ne sont communicables qu'à l'intéressé les documents administratifs faisant apparaître le comportement d'une personne, dès lors que la divulgation de ce comportement pourrait lui porter préjudice ;<br/>
<br/>
              4. Considérant, en premier lieu, que le contrôle de la régularité d'une décision d'hospitalisation à la demande d'un tiers, qu'il appartient au juge administratif d'exercer lorsqu'il a été saisi avant l'entrée en vigueur de la loi du 5 juillet 2011 relative aux droits et à la protection des personnes faisant l'objet de soins psychiatriques et aux modalités de leur prise en charge, doit notamment lui permettre de vérifier, en présence d'une contestation sur ce point, l'existence d'une demande d'hospitalisation répondant aux exigences des dispositions précitées de l'article L. 3212-1 du code de la santé publique, et, en particulier, le degré de parenté ou la nature des relations de son auteur avec la personne hospitalisée ; qu'afin de permettre au juge administratif d'exercer son office, il appartient à l'établissement de santé, le cas échéant après une mesure d'instruction diligentée à cet effet, de produire une copie de la demande d'hospitalisation formée par le tiers, sans occultation de son identité ; que si le juge entend se fonder sur cette pièce, le caractère contradictoire de la procédure impose, en principe, qu'elle soit préalablement communiquée à chacune des parties, sans que puissent y faire obstacle les dispositions de l'article L. 1111-7 du code de la santé publique et du II de l'article 6 de la loi du 17 juillet 1978 aujourd'hui codifiées à l'article L. 311-6 du code des relations entre le public et l'administration ; que, par suite, l'établissement public de santé de Ville-Evrard n'est pas fondé à soutenir que la cour administrative d'appel de Versailles a commis une erreur de droit en jugeant que ni les dispositions de l'article L. 1111-7 du code de la santé publique ni celles de la loi du 17 juillet 1978 n'avaient pour objet ou pour effet de déroger au caractère contradictoire de la procédure contentieuse consacré par l'article L. 5 du code de justice administrative et qu'elle ne pouvait, en conséquence, fonder son arrêt sur la demande d'hospitalisation de la requérante formée par un tiers sans l'avoir préalablement communiquée à cette dernière ; <br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond qu'en réponse au moyen tenant à l'illégalité de la demande d'hospitalisation formée par le tiers, l'établissement public de santé de Ville-Evrard a produit devant la cour administrative d'appel une copie de cette demande d'hospitalisation, en demandant expressément à la cour que " par exception au principe du contradictoire celle-ci ne soit pas communiquée à MmeB... " ; que, par suite, en jugeant qu'elle ne pouvait régulièrement statuer sans avoir préalablement communiqué ce document à la partie défenderesse et qu'il y avait lieu d'assimiler cette communication conditionnelle à un refus de communication dont il lui appartenait de tirer les conséquences, la cour n'a ni dénaturé les pièces du dossier ni commis une erreur de droit ; <br/>
<br/>
              6. Considérant, en troisième lieu, qu'il résulte de ce qui précède que l'établissement public de santé de Ville-Evrard ne saurait davantage faire grief à la cour de ne pas avoir usé de ses pouvoirs d'instruction pour faire verser au dossier des éléments d'information propres à justifier la non-communication à l'autre partie de cette demande d'hospitalisation ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'établissement public de santé de Ville-Evrard n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Versailles du 16 décembre 2014 ;<br/>
<br/>
              8. Considérant que Mme B...ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Boré, Salve de Bruneton, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'établissement public de santé de Ville-Evrard la somme de 3 000 euros à verser à cette société ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'établissement public de santé de Ville-Evrard est rejeté.<br/>
Article 2 : L'établissement public de santé de Ville-Evrard versera à la SPC Boré, Salve de Bruneton, avocat de MmeB..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à l'établissement public de santé de Ville-Evrard et à Mme A...B.... <br/>
Copie en sera adressée pour information à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
