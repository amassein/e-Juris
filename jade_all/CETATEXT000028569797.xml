<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569797</ID>
<ANCIEN_ID>JG_L_2014_02_000000353037</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/97/CETATEXT000028569797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 06/02/2014, 353037, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353037</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BOUTHORS ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:353037.20140206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 septembre et 29 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association syndicale du front de Seine, dont le siège social est 27-31, rue Robert de Flers à Paris (75015), représentée par son président en exercice ; l'association syndicale du front de Seine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA04735 du 29 juillet 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0612372-0612377 du 28 mai 2009 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la délibération 2006 DU 21-1° du 16 mai 2006 du conseil de Paris ayant pour objet de constater l'achèvement de l'opération de rénovation urbaine du secteur de Beaugrenelle (Paris 15ème) et de résilier la convention de gestion de l'ouvrage-dalle de ce secteur, et de la délibération 2006 DU 21-2° du 16 mai 2006 du même conseil ayant pour objet la passation avec la SEMEA XV d'une convention de compensation de charges d'ouverture au public de la dalle de Beaugrenelle, d'autre part, à l'annulation pour excès de pouvoir de ces délibérations, et enfin, à ce qu'il soit enjoint à la Ville de Paris, sur le fondement de l'article L. 911-1 du code de justice administrative, si elle ne peut obtenir de la SEMEA XV qu'elle accepte la résolution de la convention de compensation de charges annexée à la délibération attaquée DU 21-2° du 16 mai 2006, de saisir le juge du contrat dans un délai de deux mois à compter de la notification de l'arrêt à intervenir, aux fins de voir prononcer la résolution de la convention, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bouthors, avocat de l'association syndicale du front de Seine et à Me Foussard, avocat de la Ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par deux délibérations DU 21-1° et 21-2° du 16 mai 2006, le conseil de Paris a, d'une part, constaté l'achèvement de l'opération de rénovation urbaine du secteur de Beaugrenelle dans le quinzième arrondissement de Paris et résilié la convention de gestion de l'ouvrage-dalle de ce secteur, d'autre part, passé avec la société d'économie mixte SEMEA XV une convention de compensation des charges d'ouverture au public de la dalle de Beaugrenelle ; que, saisi notamment par l'association syndicale du front de Seine, le tribunal administratif de Paris a, par un jugement du 28 mai 2009, rejeté la demande tendant à l'annulation pour excès de pouvoir de ces deux délibérations ; que, par un arrêt du 29 juillet 2011, contre lequel l'association syndicale du front de Seine se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté la requête d'appel présentée notamment par l'association requérante, au motif que celle-ci n'avait pas intérêt à agir contre les délibérations litigieuses ; <br/>
<br/>
              2. Considérant qu'il ressort des énonciations de l'arrêt attaqué qu'après avoir confronté les délibérations attaquées et les statuts de l'association requérante, aux termes desquels celle-ci a pour objet de " veiller à la conservation et l'amélioration du Quartier du Front de Seine, au maintien de son aspect architectural et de son caractère résidentiel, à la qualité de son environnement sous tous ses aspects ainsi qu'à la sécurité des personnes et des biens ; (...) défendre l'ensemble de ses membres en tant que représentants des copropriétaires et des résidants également locataires de parkings, dans leurs rapports avec les tiers et les pouvoirs publics ; (...) vérifier le bien-fondé des charges imputées à ses membres au titre de l'ouvrage-dalle et des parkings ", la cour administrative d'appel de Paris a jugé que les délibérations litigieuses ne contenaient aucune décision susceptible de porter atteinte aux intérêts collectifs défendus par l'association ; qu'elle en a déduit que celle-ci ne justifiait pas d'un intérêt pour demander l'annulation des délibérations litigieuses ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, d'une part, la délibération du conseil de Paris 2006 DU 21-1°, en date du 16 mai 2006, qui a pour objet de constater l'achèvement de l'opération de rénovation urbaine du secteur Beaugrenelle dont avait la charge la SEMEA XV, de résilier la convention de gestion par la même société de l'ouvrage-dalle édifié dans ce secteur et de transférer dans le patrimoine de la Ville de Paris une partie des baux à construire sur l'ouvrage-dalle qui demeure, pour sa part, la propriété de la SEMEA XV, n'emporte, par elle-même, aucune opération de travaux ou d'aménagement susceptible d'affecter le quartier à la protection duquel l'association requérante entend veiller ; que, d'autre part, la délibération du conseil de Paris 2006 DU 21-2°, en date du 16 mai 2006, qui a pour objet la passation avec la SEMEA XV d'une convention de compensation de charges prévoyant le versement par la Ville de Paris d'une participation financière annuelle à titre de compensation des charges afférentes à l'ouverture au public de la dalle de Beaugrenelle, n'a pas d'incidence sur les charges imputées aux membres de l'association requérante au titre de l'ouvrage-dalle ; que, par suite, en jugeant que l'association ne justifiait d'un intérêt pour agir contre aucune des deux délibérations litigieuses, la cour administrative d'appel de Paris a fait une exacte application des règles relatives à la recevabilité du recours pour excès de pouvoir ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'association syndicale du front de Seine n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association syndicale du front de Seine la somme que la Ville de Paris demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association syndicale du front de Seine est rejeté.<br/>
Article 2 : Les conclusions de la Ville de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association syndicale du front de Seine et à la Ville de Paris.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
