<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566411</ID>
<ANCIEN_ID>JG_L_2019_06_000000412056</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 05/06/2019, 412056, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412056</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412056.20190605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
M. et Mme A...B...ont demandé au tribunal administratif de Nice de prononcer la réduction des contributions sociales auxquelles ils ont été assujettis au titre de l'année 2008 ainsi que des intérêts de retard correspondants. Par un jugement n° 1401114 du 11 décembre 2015, le tribunal administratif de Nice a rejeté cette demande.<br clear="none"/>
<br clear="none"/>
Par un arrêt n° 16MA00125 du 2 mai 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. et Mme B...contre ce jugement.<br clear="none"/>
<br clear="none"/>
Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 juillet et 3 octobre 2017 et le 7 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler cet arrêt ;<br clear="none"/>
<br clear="none"/>
2°) réglant l'affaire au fond, de faire droit à leur appel ;<br clear="none"/>
<br clear="none"/>
3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- le code général des impôts et le livre des procédures fiscales ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Nicolas Agnoux, maître des requêtes,<br clear="none"/>
<br clear="none"/>
- les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et MmeB....<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Considérant ce qui suit :<br clear="none"/>
<br clear="none"/>
1. Il ressort des pièces du dossier soumis aux juges du fond que l'entreprise unipersonnelle à responsabilité limitée (EURL) Pharmacie LocquetB..., dont Mme B... était l'unique associée, a cédé le 27 décembre 2008 son fonds de commerce en raison du départ à la retraite de Mme B...puis a été dissoute le lendemain. Au titre de ces opérations, M. et Mme B...ont déclaré avoir réalisé une plus-value dont ils ont estimé qu'elle était exonérée d'impôt sur le revenu et de contributions sociales en application des dispositions de l'article 151 septies A du code général des impôts. A la suite d'un contrôle sur pièces, l'administration fiscale a soumis cette plus-value aux contributions sociales au motif que l'exonération prévue à l'article 151 septies A du code général des impôts était limitée à l'impôt sur le revenu. M. et Mme B...n'ont pas contesté l'assujettissement de la plus-value aux contributions sociales mais ont demandé la réduction de son montant et, par suite, celle des contributions mises à leur charge. Par un jugement du 11 décembre 2015, le tribunal administratif de Nice a rejeté leur demande. Par l'arrêt attaqué du 2 mai 2017, la cour administrative d'appel de Marseille a rejeté l'appel qu'ils ont formé contre ce jugement.<br clear="none"/>
<br clear="none"/>
2. D'une part, aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable ou d'après le contenu d'un acte présenté par lui à la formalité de l'enregistrement. ".<br clear="none"/>
<br clear="none"/>
3. D'autre part, dans le cas où une société, une entreprise ou une personne soumise à l'impôt à raison des bénéfices qu'elle tire de son activité professionnelle, cède les parts inscrites à l'actif de son bilan qu'elle détient dans une société ou dans un groupement relevant ou ayant relevé du régime prévu aux articles 8, 8 ter, 239 quater B ou 239 quater C du code général des impôts ou, lorsqu'elle ne dresse pas de bilan, les parts de même nature qu'elle a affectées à l'exercice de sa profession, le résultat de cette opération, imposable dans les conditions prévues à l'article 39 duodecies précité et aux articles suivants du même code, doit être calculé en retenant comme prix de revient de ces parts leur valeur d'acquisition, majorée en premier lieu, d'une part, de la quote-part des bénéfices de cette société revenant à l'associé qui a été ajoutée aux résultats imposés de celui-ci, antérieurement à la cession et pendant la période d'application du régime visé ci-dessus, d'autre part, des pertes afférentes à des entreprises exploitées par la société et ayant donné lieu de la part de l'associé à un versement en vue de les combler, puis minorée en second lieu, d'une part, des déficits que l'associé a déduits pendant cette même période, à l'exclusion de ceux qui trouvent leur origine dans une disposition par laquelle le législateur a entendu conférer aux contribuables un avantage fiscal définitif et, d'autre part, des bénéfices afférents à des entreprises exploitées en France par la société et ayant donné lieu à répartition au profit de l'associé.<br clear="none"/>
<br clear="none"/>
4. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... a acquis en 1997, au prix de 565 192 euros, l'intégralité des parts de la SNC Pharmacie Berthoux-Masson, dont l'objet social était l'exploitation d'un fonds de commerce de pharmacie. Le 27 décembre 2008, la SNC Pharmacie LocquetB..., anciennement " Pharmacie Berthoux-Masson ", a changé de forme sociale pour devenir une entreprise unipersonnelle à responsabilité limitée et cédé son fonds de commerce, au prix de 2 000 000 euros. Le 28 décembre 2008, la société a été dissoute. M. et Mme B...ont déclaré, au titre de leurs revenus de l'année 2008, avoir réalisé, à la suite de ces opérations, une plus-value d'un montant de 1 034 440 euros correspondant à la différence entre, d'une part, le prix de cession du fonds de commerce, soit 2 000 000 euros et, d'autre part, la valeur nette comptable du fonds figurant à l'actif de l'EURL, soit 965 560 euros. A la suite du contrôle sur pièces dont ils ont fait l'objet, ils ont demandé la réduction du montant de cette plus-value à raison de l'omission de la prise en compte, pour sa détermination, du prix d'acquisition des parts de l'EURL, soit 565 192 euros.<br clear="none"/>
<br clear="none"/>
5. Pour demander la réduction des contributions sociales mises à leur charge au titre de l'année 2008 établies conformément à leurs déclarations, M. et Mme B...ont demandé que soit prise en compte, en diminution de la plus-value réalisée par l'EURL sur la cession du fonds de commerce, imposable entre leurs mains, la moins-value que Mme B... aurait réalisée lors du transfert des parts de l'EURL de son patrimoine professionnel vers son patrimoine privé et qu'ils avaient omise de déclarer. En jugeant qu'en se bornant à produire la liasse fiscale de l'EURL établie au 27 décembre 2008, M. et Mme B...ne versaient pas aux débats l'ensemble des éléments nécessaires pour déterminer, compte tenu notamment d'éventuels déficits déduits ou de bénéfices distribués au cours de l'exercice de l'activité professionnelle de MmeB..., le prix de revient de ces parts par application des principes rappelés au point 3, la cour n'a ni dénaturé les faits et les pièces du dossier ni insuffisamment motivé sa décision ni méconnu les règles de dévolution de la charge de la preuve, rappelées par les dispositions de l'article R. 194-1 du livre des procédures fiscales précitées, selon lesquelles il incombe au contribuable de démontrer le caractère exagéré d'une imposition établie d'après les bases indiquées dans sa déclaration.<br clear="none"/>
<br clear="none"/>
6. Il résulte de ce qui précède que M. et Mme B...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas la partie perdante dans la présente instance.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : Le pourvoi de M. et Mme B... est rejeté.<br clear="none"/>
Article 2 : La présente décision sera notifiée à M. et Mme A... B...et au ministre de l'action et des comptes publics.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
