<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032553343</ID>
<ANCIEN_ID>JG_L_2016_05_000000386810</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/55/33/CETATEXT000032553343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 18/05/2016, 386810</TITRE>
<DATE_DEC>2016-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386810</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386810.20160518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 30 décembre 2014, 31 mars 2015, 23 décembre 2015 et 9 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société Direct Energie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 28 octobre 2014 modifiant le décret n° 2009-975 du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de commerce ;<br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2010-1488 du 7 décembre 2010 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 337-1 du code de l'énergie : " Le deuxième alinéa de l'article L. 410-2 du code de commerce s'applique : / (...) 2° Aux tarifs réglementés de vente d'électricité ; / (...) " ; que le deuxième alinéa de l'article L. 410-2 du code de commerce prévoit que " dans les secteurs ou les zones où la concurrence par les prix est limitée en raison soit de situations de monopole ou de difficultés durables d'approvisionnement, soit de dispositions législatives ou réglementaires, un décret en Conseil d'Etat peut réglementer les prix après consultation de l'Autorité de la concurrence. " ; que, sur le fondement de ces dispositions, le décret du 28 octobre 2014 a modifié le décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité, qui fixe les modalités de détermination de ces tarifs ; que la société Direct Energie en demande l'annulation pour excès de pouvoir ;<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Considérant que l'article 13 de la loi du 7 décembre 2010 portant nouvelle organisation du marché de l'électricité a abrogé les dispositions du dernier alinéa du II de l'article 4 de la loi du 10 février 2000 relative à la modernisation et au développement du service public de l'électricité, qui prévoyaient que " les tarifs réglementés de vente d'électricité couvrent l'ensemble des coûts supportés à ce titre par Electricité de France et par les distributeurs non nationalisés mentionnés à l'article 23 de la loi n° 46-628 du 8 avril 1946 " et les a remplacées par les dispositions suivantes, codifiées à l'article L. 337-6 du code de l'énergie : " Dans un délai s'achevant au plus tard le 31 décembre 2015, les tarifs réglementés de vente d'électricité sont progressivement établis en tenant compte de l'addition du prix d'accès régulé à l'électricité nucléaire historique, du coût du complément à la fourniture d'électricité qui inclut la garantie de capacité, des coûts d'acheminement de l'électricité et des coûts de commercialisation ainsi que d'une rémunération normale " ; que, par ces dispositions, le législateur a entendu organiser, sur une période transitoire de cinq ans s'achevant au plus tard le 31 décembre 2015, une convergence tarifaire propre à résorber l'écart structurel qui existait alors, pour des raisons historiques qui tenaient à l'économie générale du marché de l'électricité en France, entre le niveau des tarifs réglementés de l'électricité et les coûts, plus élevés à l'époque, de fourniture de l'électricité distribuée à un tarif de marché ;<br/>
<br/>
              3. Considérant en outre qu'aux termes de l'article L. 337-5 du code de l'énergie, issu des dispositions du premier alinéa du II de l'article 4 de la loi du 10 février 2000 qui ont été maintenues sans changement par la loi du 7 décembre 2010 : " Les tarifs réglementés de vente d'électricité sont définis en fonction de catégories fondées sur les caractéristiques intrinsèques des fournitures, en fonction des coûts liés à ces fournitures " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions combinées qu'en prévoyant la prise en compte des coûts de l'activité de fourniture de l'électricité aux tarifs réglementés par les fournisseurs historiques, le législateur a, dans le but à la fois de ne pas fausser la concurrence sur le marché de détail de l'électricité et de ne pas imposer aux fournisseurs historiques une vente à un tarif inférieur à leur coût de revient, exclu que les tarifs réglementés soient fixés à un niveau artificiellement bas, inférieur aux coûts comptables complets de la fourniture de l'électricité à ces tarifs, incluant les frais financiers ; qu'il n'a pas entendu, en revanche, garantir un niveau de rémunération des capitaux propres engagés ;<br/>
<br/>
              5. Considérant qu'au cours de la période transitoire ouverte par la loi du 7 décembre 2010, l'article 3 du décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité continuait de prévoir que : " La part fixe et la part proportionnelle de chaque option ou version tarifaire (...) sont établies de manière à couvrir les coûts de production, les coûts d'approvisionnement, les coûts d'utilisation des réseaux publics de transport et de distribution et les coûts de commercialisation, que supportent pour fournir leurs clients Electricité de France et les distributeurs non nationalisés mentionnés à l'article 23 de la loi n° 46-628 du 8 avril 1946, ainsi qu'une marge raisonnable " ; que, pendant cette période transitoire, il incombait ainsi aux ministres chargés de l'énergie et de l'économie, lorsqu'ils fixaient les tarifs réglementés, d'une part, conformément aux dispositions de l'article 3 du décret du 12 août 2009, de répercuter les variations des coûts complets de l'électricité fournie par Electricité de France et par les entreprises locales de distribution et, d'autre part, de veiller à ce que ces mêmes tarifs réglementés soient, conformément aux dispositions de l'article L. 337-6 du code de l'énergie, fixés à un niveau de nature à assurer progressivement la convergence voulue par le législateur ;<br/>
<br/>
              6. Considérant que, par son article 2, le décret attaqué du 28 octobre 2014 a fixé à la date de son entrée en vigueur, soit le 31 octobre 2014, l'achèvement de la période transitoire mentionnée au point 2 ci-dessus ;<br/>
<br/>
              7. Considérant qu'aux termes des nouvelles dispositions de l'article 3 du décret du 12 août 2009 introduites par le décret attaqué : " (...) le niveau des tarifs réglementés de vente de l'électricité est déterminé, sous réserve de la prise en compte des coûts de l'activité de fourniture de l'électricité aux tarifs réglementés d'Electricité de France et des entreprises locales de distribution, par l'addition du coût de l'accès régulé à l'électricité nucléaire historique, du coût du complément d'approvisionnement, qui inclut la garantie de capacité, des coûts d'acheminement de l'électricité et des coûts de commercialisation ainsi que d'une rémunération normale de l'activité de fourniture. (...) " ; <br/>
<br/>
              8. Considérant que ce même article 3 du décret du 12 août 2009 détaille, dans sa nouvelle rédaction issue du décret du 28 octobre 2014, les différents éléments de la méthode de détermination des tarifs réglementés de vente de l'électricité, dite " par empilement " des coûts, dans les termes suivants : " Le coût de l'accès régulé à l'électricité nucléaire historique est déterminé en fonction du prix de l'accès régulé à l'électricité nucléaire historique applicable à la date d'entrée en vigueur de l'arrêté mentionné à l'article 5 appliqué au prorata de la quantité de produit théorique calculée en application de l'article 4 du décret du 28 avril 2011 susvisé. / Le coût du complément d'approvisionnement sur le marché est calculé en fonction des caractéristiques moyennes de consommation et des prix de marché à terme constatés. (...) / Les coûts d'acheminement de l'électricité sont déterminés en fonction des tarifs d'utilisation des réseaux publics applicables à la date d'entrée en vigueur de l'arrêté mentionné à l'article 5. / Les coûts de commercialisation correspondent aux coûts de commercialisation d'un fournisseur d'électricité au moins aussi efficace qu'Electricité de France dans son activité de fourniture des clients ayant souscrit aux tarifs réglementés de vente de l'électricité. " ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              9. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              10. Considérant que la société requérante soutient que l'article L. 337-5 du code de l'énergie est contraire au droit de propriété, à la liberté d'entreprendre et à la liberté du commerce et de l'industrie, au principe d'égalité, au droit de demander compte à l'administration posé par l'article 15 de la Déclaration des droits de l'homme et du citoyen ainsi qu'aux articles 3 et 4 de la Charte de l'environnement ;<br/>
<br/>
              11. Considérant qu'en vertu de cet article, dans sa rédaction applicable à la date d'adoption du décret attaqué, les tarifs réglementés de vente d'électricité sont définis " en fonction des coûts liés " à la fourniture de l'électricité à ces tarifs ; que, contrairement à ce que soutient la société requérante, il ne résulte pas de ces dispositions que cette prise en compte implique nécessairement la couverture des coûts complets, constatés en comptabilité, de la fourniture d'électricité par les fournisseurs historiques, y compris une rémunération garantie des capitaux propres engagés ; que, par ces dispositions, le législateur a, ainsi qu'il a été dit au point 4,  seulement imposé que soient pris en compte les coûts de l'activité de fourniture de l'électricité aux tarifs réglementés par les fournisseurs historiques ;<br/>
<br/>
              12. Considérant, en premier lieu, que la loi garantit, à l'article L. 337-6 du code de l'énergie cité au point 2, que les tarifs réglementés sont fixés à un niveau qui couvre l'ensemble des coûts de fourniture de l'électricité fournie à un tarif de marché ; que, par suite, d'une part, la disposition contestée, lue en combinaison avec cet article, ne peut avoir pour effet de faire obstacle au développement de l'activité de fourniture ou de production des opérateurs concurrents des fournisseurs historiques ; que, d'autre part, la prise en compte des coûts de l'opérateur historique, qui a l'obligation de fournir l'électricité aux tarifs réglementés, garantit que ce dernier ne puisse être conduit à procéder à cette fourniture à des conditions de nature à porter atteinte à sa pérennité ; que, dès lors, le moyen tiré de la méconnaissance de la liberté d'entreprendre et de la liberté du commerce et de l'industrie ne présente pas de caractère sérieux ;<br/>
<br/>
              13. Considérant, en deuxième lieu, qu'à supposer même que, comme le soutient la société requérante, la disposition contestée, en ne faisant pas obstacle à la fixation des tarifs à un niveau inférieur aux coûts comptables complets de fourniture supportés par les fournisseurs historiques comprenant une rémunération garantie des capitaux propres engagés tels qu'ils étaient auparavant déterminés, implique une diminution des bénéfices des fournisseurs historiques distribués à leurs associés ou actionnaires, ainsi qu'une baisse de la valeur des titres que ceux-ci détiennent, cet article n'emporte ni privation du droit de propriété, ni limitation de son exercice, dès lors que les associés ou actionnaires conservent la propriété de leurs titres ainsi que la possibilité de bénéficier du partage des résultats sociaux ;<br/>
<br/>
              14. Considérant, en troisième lieu, qu'en disposant que les tarifs réglementés de vente de l'électricité prennent en compte les coûts liés à la fourniture de l'électricité à ces tarifs, le législateur s'est fondé sur un critère objectif et rationnel en rapport avec l'objet de la loi ; que le moyen tiré de la rupture d'égalité entre les marchés de détail de l'électricité, d'une part, et du gaz, d'autre part, pour lesquels, en application de l'article L. 445-3 du code de l'énergie, les tarifs réglementés doivent couvrir l'ensemble des coûts de fourniture, ne présente en tout état de cause aucun caractère sérieux dès lors que cette différence de traitement est justifiée par la différence entre ces deux marchés ;<br/>
<br/>
              15. Considérant, enfin, que la société requérante ne peut utilement se prévaloir d'une méconnaissance du droit de demander compte à l'administration, qui découle de l'article 15 de la Déclaration des droits de l'homme et du citoyen, dès lors que l'obligation pour les fournisseurs historiques de proposer la vente de l'électricité à un tarif réglementé n'induit, quel que soit le niveau de ce tarif, aucune dépense publique ; que, de même, elle ne saurait utilement se prévaloir d'une méconnaissance des principes posés par les articles 3 et 4 de la Charte de l'environnement, aux termes desquels : " Toute personne doit, dans les conditions définies par la loi, prévenir les atteintes qu'elle est susceptible de porter à l'environnement ou, à défaut, en limiter les conséquences " et " Toute personne doit contribuer à la réparation des dommages qu'elle cause à l'environnement, dans les conditions définies par la loi ", dès lors que la disposition contestée, relative aux règles de fixation des tarifs réglementés de vente de l'électricité, est par elle-même sans incidence sur le respect de ces principes ;<br/>
<br/>
              16. Considérant, dès lors, que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 337-5 du code de l'énergie porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur le moyen tiré de  la méconnaissance de l'exigence de couverture des coûts de fourniture de l'électricité par les fournisseurs historiques :<br/>
<br/>
              17. Considérant que la requérante soutient, en premier lieu, que les dispositions précitées de l'article 3 du décret du 12 août 2009, dans sa rédaction issue du décret attaqué, méconnaîtraient un principe de couverture des coûts complets, constatés en comptabilité, de l'électricité fournie aux tarifs réglementés par les fournisseurs historiques, y compris une rémunération garantie des capitaux propres engagés, qui découlerait de l'article L. 337-5 du code de l'énergie ;<br/>
<br/>
              18. Considérant toutefois que si, ainsi qu'il a été dit au point 5 ci-dessus, la fixation des tarifs réglementés de vente de l'électricité restait, jusqu'au terme de la période transitoire prévue par l'article L. 337-6 du code de l'énergie, soumise au respect du principe de couverture des coûts complets de l'électricité fournie à ces tarifs, alors prévu par l'article 3 décret du 12 août 2009, ces tarifs doivent seulement, depuis la modification de cet article par le décret attaqué, être " définis en fonction de catégories fondées sur les caractéristiques intrinsèques des fournitures, en fonction des coûts liés à ces fournitures ", conformément à l'article L. 337-5 de ce code ; que le décret attaqué a fait une correcte application de ces dispositions en prévoyant que les tarifs, déterminés en principe par l'addition des coûts mentionnés par l'article 3 du décret du 12 août 2009, doivent en tout état de cause prendre en compte les " coûts de l'activité de fourniture de l'électricité aux tarifs réglementés d'Electricité de France et des entreprises locales de distribution " ; que, par ces dispositions, le pouvoir réglementaire n'a pas méconnu la règle résultant, comme il a été dit au point 4 ci-dessus, des articles L. 337-5 et L. 337-6 du code, qui exclut que les tarifs soient fixés à un niveau artificiellement bas, inférieur aux coûts comptables complets de la fourniture de l'électricité à ces tarifs, incluant les frais financiers, sans garantir nécessairement un niveau de rémunération des capitaux propres engagé ; qu'il en résulte que, contrairement à ce que soutient la requérante, même si, en application de l'article 3 du décret du 12 août 2009, le coût de la garantie de capacité est regardé comme nul jusqu'à la première année de livraison du mécanisme d'obligation de capacité, ce qui réduit le niveau des tarifs déterminé par l'addition des coûts, les tarifs réglementés ne peuvent être inférieurs aux coûts de fourniture supportés par les fournisseurs historiques ;<br/>
<br/>
              19. Considérant, en second lieu, qu'il résulte de ce qui précède que, contrairement à ce que soutient la requérante, le décret attaqué n'a pas illégalement renvoyé aux arrêtés tarifaires pris par les ministres chargés de l'énergie et de l'économie la définition des modalités de prise en compte des coûts liés à la fourniture d'électricité aux tarifs réglementés par les fournisseurs historiques ; qu'il n'a pas davantage méconnu les objectifs du deuxième paragraphe de l'article 3 de la directive du Parlement européen et du Conseil du 13 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité, qui exigent que les obligations de service public imposées aux entreprises du secteur de l'électricité, portant notamment sur le prix de la fourniture, soient " clairement définies " ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance du " principe de contestabilité des tarifs " :<br/>
<br/>
              20. Considérant que le moyen par lequel la requérante invoque un " principe de contestabilité des tarifs " doit être regardé comme tiré de la méconnaissance de l'article L. 337-6 du code de l'énergie, qui, ainsi qu'il a été dit au point 2 ci-dessus, dispose, dans le but de résorber l'écart entre le niveau des tarifs réglementés et les coûts de fourniture de l'électricité distribuée à un tarif de marché, que " les tarifs réglementés de vente d'électricité sont progressivement établis en tenant compte de l'addition du prix d'accès régulé à l'électricité nucléaire historique, du coût du complément à la fourniture d'électricité qui inclut la garantie de capacité, des coûts d'acheminement de l'électricité et des coûts de commercialisation ainsi que d'une rémunération normale " ;<br/>
<br/>
              21. Considérant que l'article 3 du décret attaqué, cité aux points 7 et 8 ci-dessus, prévoit que le niveau des tarifs réglementés de vente de l'électricité est déterminé par l'addition des différentes catégories de coûts mentionnées par l'article L. 337-6 du code de l'énergie ; que, contrairement à ce qui est soutenu, la détermination du " coût du complément à la fourniture d'électricité " retenue par le décret attaqué, par référence aux prix de marché à terme constatés, ne méconnaît pas ces dispositions dès lors que ce sont les prix auxquels les concurrents des fournisseurs historiques s'approvisionnent pour la part qui n'est pas couverte par l'accès régulé à l'électricité nucléaire historique ; qu'enfin, si, ainsi que le fait valoir la requérante, le décret attaqué ne prévoit aucune compensation des avantages immatériels dont bénéficient les fournisseurs historiques, résultant notamment de l'étendue de leur portefeuille de clientèle et de l'ancienneté de leur marque, la prise en compte de ces avantages n'est prévue ni par l'article L. 337-6 de ce code ni par aucune autre disposition législative ; que, dès lors, le moyen tiré de la méconnaissance de cet article ne peut être accueilli ; <br/>
<br/>
              Sur les moyens relatifs au droit de la concurrence :<br/>
<br/>
              22. Considérant que la société requérante soutient que les dispositions du décret attaqué mettent par elles-mêmes les fournisseurs historiques d'électricité en situation d'abuser de manière automatique de la position dominante qu'ils détiennent sur le marché de détail de l'électricité, en méconnaissance des stipulations combinées des articles 102 et 106, paragraphe 1 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              23. Considérant, en premier lieu, que le décret attaqué, qui prévoit que les tarifs réglementés sont fixés à un niveau qui couvre l'ensemble des coûts de fourniture de l'électricité fournie à un tarif de marché, ne saurait avoir pour effet d'évincer du marché de détail des concurrents regardés comme aussi efficaces que les fournisseurs historiques ;<br/>
<br/>
              24. Considérant, en second lieu, que la société requérante soutient que le décret attaqué conduit à définir les tarifs réglementés à un niveau inférieur au prix d'approvisionnement sur le marché de gros ou, à tout le moins, à un prix insuffisant pour couvrir les coûts de fourniture de fournisseurs alternatifs aussi efficaces que les fournisseurs historiques, ce qui entraînerait un effet de " ciseau tarifaire " pour les concurrents des opérateurs historiques ; que, toutefois, les modalités de détermination des tarifs prévues par le décret attaqué garantissent au contraire un écart positif entre les tarifs fixés et les coûts d'approvisionnement, résultant de l'accès régulé à l'énergie nucléaire historique et du complément de fourniture défini par référence au prix du marché à terme, cet écart étant au moins égal aux coûts hors approvisionnement normalement supportés par un opérateur aussi efficace qu'EDF puisqu'il couvre le coût d'utilisation des réseaux publics d'électricité, les coûts de commercialisation définis comme ceux d'un fournisseur " au moins aussi efficace qu'Electricité de France dans son activité de fourniture des clients ayant souscrit aux tarifs réglementés de vente de l'électricité " ainsi qu'une rémunération normale ; que, par suite, ce moyen ne peut être accueilli ;<br/>
<br/>
              25. Considérant qu'il résulte de tout ce qui précède que la requête doit être rejetée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              26. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Direct Energie.<br/>
Article 2 : La requête de la société Direct Energie est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la société Direct Energie et au ministre de l'économie, de l'industrie et du numérique.<br/>
Copie en sera adressée pour information au Conseil constitutionnel, au Premier ministre, à la ministre de l'environnement, de l'énergie et de la mer, à la Commission de régulation de l'énergie et à la société EDF.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-06-02-005 ENERGIE. MARCHÉ DE L'ÉNERGIE. TARIFICATION. - TARIFS RÉGLEMENTÉS DE VENTE D'ÉLECTRICITÉ - INTERDICTION DE FIXATION À UN NIVEAU ARTIFICIELLEMENT BAS - EXISTENCE - GARANTIE D'UN NIVEAU DE RÉMUNÉRATION DES CAPITAUX PROPRES ENGAGÉS - ABSENCE.
</SCT>
<ANA ID="9A"> 29-06-02-005 Il résulte des dispositions combinées des articles L. 337-5 (issues de l'article 4 de la loi n° 2000-108 du 10 février 2000 et maintenues sans changement par la loi n° 2010-1488 du 7 décembre 2010) et L. 337-6 (issues de l'article 13 de la loi n° 2010-1488 du 7 décembre 2010) du code de l'énergie qu'en prévoyant la prise en compte des coûts de l'activité de fourniture de l'électricité aux tarifs réglementés par les fournisseurs historiques, le législateur a, dans le but à la fois de ne pas fausser la concurrence sur le marché de détail de l'électricité et de ne pas imposer aux fournisseurs historiques une vente à un tarif inférieur à leur coût de revient, exclu que les tarifs réglementés soient fixés à un niveau artificiellement bas, inférieur aux coûts comptables complets de la fourniture de l'électricité à ces tarifs, incluant les frais financiers. Il n'a pas entendu, en revanche, garantir un niveau de rémunération des capitaux propres engagés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
