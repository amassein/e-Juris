<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023604484</ID>
<ANCIEN_ID>JG_L_2011_02_000000337018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/60/44/CETATEXT000023604484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 11/02/2011, 337018</TITRE>
<DATE_DEC>2011-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:337018.20110211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 25 février 2010 au secrétariat du contentieux du Conseil d'État, présentée pour la société ALLO PERMIS dont le siège social est situé 4, avenue Claude Vellefaux à Paris (75010), représentée par ses dirigeants en exercice, la société ACTION CONDUITE PREVENTION SECURITE dont le siège social est situé 14, rue des Trois Territoires à Vincennes (94300), représentée par ses dirigeants en exercice, la société SOS-PERMIS dont le siège social est situé 14, rue Paul Lafargue à Puteaux (92800), représentée par ses dirigeants en exercice, le SYNDICAT NATIONAL DES PROFESSIONNELS DU PERMIS A POINTS dont le siège est situé 3, avenue Paul Doumer à Rueil Malmaison (92508), représenté par son président ; la société ALLO PERMIS et les autres requérants demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2009-1678 du 29 décembre 2009 relatif à l'enseignement de la conduite et à l'animation de stages de sensibilisation à la sécurité routière ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu la loi n° 2007-797 du 5 mars 2007 ; <br/>
<br/>
              Vu le décret n° 2010-272 du 15 mars 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la société ALLO PERMIS et autres, <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la société ALLO PERMIS et autres ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 213-1 du code de la route issu de l'article 23 de la loi du 5 mars 2007 relative à la prévention de la délinquance : " L'enseignement, à titre onéreux, de la conduite des véhicules à moteur d'une catégorie donnée et de la sécurité routière ainsi que l'animation des stages de sensibilisation à la sécurité routière mentionnés à l'article L. 223-6 ne peuvent être organisés que dans le cadre d'un établissement d'enseignement dont l'exploitation est subordonnée à un agrément délivré par l'autorité administrative, après avis d'une commission " ; que le décret du 29 décembre 2009 relatif à l'enseignement de la conduite et à l'animation des stages de sensibilisation à la sécurité routière a été notamment pris pour l'application de ces dispositions ; que les requérants demandent l'annulation pour excès de pouvoir de ce décret en tant qu'il soumet à des exigences accrues l'obtention de l'agrément permettant d'assurer l'exploitation d'un établissement organisant des stages de sensibilisation à la sécurité routière et définit des mesures transitoires pour son application ; <br/>
<br/>
              Sur les moyens tirés de la violation de la liberté du commerce et de l'industrie : <br/>
<br/>
              Considérant, en premier lieu, que, contrairement à ce que soutiennent les requérants, le décret ne limite pas ses effets aux personnes physiques mais s'applique aux dirigeants de personnes morales ; que les dispositions du décret ne font par suite pas obstacle au renouvellement des agréments délivrés à des personnes morales avant l'entrée en vigueur du décret ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article L. 213-3 du code de la route : " Nul ne peut exploiter, à titre individuel, ou être dirigeant ou gérant de droit ou de fait d'un des établissements mentionnés à l'article L. 213-1, s'il ne satisfait aux conditions suivantes : 1° Ne pas avoir fait l'objet d'une condamnation : a) Soit à une peine criminelle ; b) Soit à une peine correctionnelle prononcée pour une infraction figurant sur une liste fixée par décret en Conseil d'État ; (...) " ; que l'article R. 212-4 du code de la route, que l'article 7 du décret attaqué complète et auquel son article 13 renvoie pour l'application de l'article L. 213-3, définit une liste d'infractions dont la nature et la gravité ne sont pas dépourvues de tout lien avec les exigences posées par le législateur ; qu'au demeurant, une liste analogue d'infractions est définie pour la délivrance de l'agrément aux exploitants des établissements d'enseignement, à titre onéreux, de la conduite des véhicules à moteur ainsi que pour la délivrance de l'autorisation d'enseigner la conduite des véhicules à moteur et de l'autorisation d'animer des stages de sensibilisation à la sécurité routière, prévues respectivement aux articles R. 213-1 et R. 212-1 du même code ; <br/>
<br/>
              Considérant, en troisième lieu, que le 1° du II de l'article R. 213-2 du code de la route issu de l'article 13 du décret attaqué, qui subordonne, ainsi qu'il vient d'être dit, pour les organisateurs de stages de sensibilisation à la sécurité routière et les personnes qu'ils désignent nommément pour l'encadrement administratif de ces stages, la délivrance de l'agrément prévu par l'article L. 213-1 du même code à la condition de n'avoir pas été condamné pour l'une des infractions prévues par l'article R. 212-4 du code de la route, sans fixer une limite dans l'ancienneté de la condamnation, se borne à faire application des dispositions précitées de l'article L. 213-3 ; que, compte tenu par ailleurs de l'application des dispositions législatives du code pénal et du code de procédure pénale relatives à la réhabilitation légale, à la réhabilitation judiciaire et au relèvement qui est de nature à limiter dans le temps les effets de l'interdiction résultant de la condamnation pour une des infractions prévues par le décret attaqué, le moyen tiré de ce que ce décret serait illégal faute d'avoir prévu lui-même une limite temporelle doit être écarté ;<br/>
<br/>
              Considérant, en quatrième lieu, qu'en imposant aux personnes souhaitant exploiter un établissement organisant des stages de sensibilisation à la sécurité routière de justifier de garanties minimales relatives aux locaux, l'article 13 du décret attaqué n'emporte pas, par lui-même, une atteinte disproportionnée et injustifiée à la liberté du commerce et de l'industrie ;<br/>
<br/>
              Sur le moyen tiré de la violation du principe de sécurité juridique : <br/>
<br/>
              Considérant que l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes, puissent invoquer un droit au maintien de la réglementation existante ; qu'en principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs ; que, toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle ; qu'il en va ainsi lorsque l'application immédiate des règles nouvelles, de fond ou de procédure, entraînerait, au regard de leur objet ou de leurs effets, une atteinte excessive aux intérêts publics ou privés en cause ; <br/>
<br/>
              Considérant, d'une part, que le décret du 29 décembre 2009, qui a pour objet, conformément à la volonté du législateur, de conforter la qualité et l'efficacité des stages de sensibilisation à la sécurité routière en renforçant les conditions d'agrément des établissements organisant ces stages,  a prévu que les agréments pour l'organisation des stages de sensibilisation à la sécurité routière délivrés avant l'entrée en vigueur du décret demeuraient valides jusqu'au 30 juin 2010 pour permettre aux organisateurs déjà titulaires d'un agrément de se mettre en conformité avec les nouvelles exigences posées par le décret  ; que cette échéance a au demeurant été reportée au 1er janvier 2011 par le décret du 15 mars 2010 modifiant le décret du 29 décembre 2009 relatif à l'enseignement de la conduite et à l'animation de stages de sensibilisation à la sécurité routière ; qu'en prolongeant ainsi la validité des agréments en cours, le pouvoir réglementaire a laissé à leurs bénéficiaires un délai raisonnable pour s'adapter à la modification de la réglementation ; que, d'autre part, l'application immédiate des nouvelles règles et de la nouvelle procédure aux demandes de premier agrément déjà déposées avant l'entrée en vigueur du décret mais non encore traitées n'entraîne pas une atteinte excessive aux intérêts privés en cause ; que le moyen tiré de la méconnaissance du principe de sécurité juridique doit, dès lors, être écarté ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret attaqué ; que les conclusions qu'ils présentent au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de la société ALLO PERMIS, de la société ACTION CONDUITE PREVENTION SECURITE, de la société SOS-PERMIS et du SYNDICAT NATIONAL DES PROFESSIONNELS DU PERMIS A POINTS est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société ALLO PERMIS, premier requérant dénommé, au Premier ministre et au ministre de l'écologie, du développement durable, des transports et du logement. <br/>
Les autres requérants seront informés de la présente décision par la SCP Célice, Blancpain, Soltner, avocat au Conseil d'État et à la Cour de cassation, qui les représente devant le Conseil d'État.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-02 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. - DIRIGEANT OU GÉRANT D'UN ÉTABLISSEMENT ORGANISANT DES STAGES DE SENSIBILISATION À LA SÉCURITÉ ROUTIÈRE - AGRÉMENT (ART. 213-3 DU CODE DE LA ROUTE) - CONDITIONS - ABSENCE DE CONDAMNATION À UNE PEINE POUR UNE SÉRIE D'INFRACTIONS LISTÉES PAR DÉCRET EN CONSEIL D'ETAT - 1) INFRACTIONS DONT LA NATURE ET LA GRAVITÉ NE SONT PAS DÉPOURVUES DE TOUT LIEN AVEC LES EXIGENCES POSÉES PAR LE LÉGISLATEUR - CONSÉQUENCE - MÉCONNAISSANCE DE LA LIBERTÉ DU COMMERCE ET DE L'INDUSTRIE - ABSENCE - 2) ABSENCE DE LIMITE FIXÉE PAR LE DÉCRET TENANT À L'ANCIENNETÉ DES INFRACTIONS EN CAUSE - CONSÉQUENCE - LÉGALITÉ, COMPTE TENU DE L'APPLICATION DES DISPOSITIONS D'ORDRE GÉNÉRAL DE NATURE À LIMITER DANS LE TEMPS LES EFFETS DE L'INTERDICTION.
</SCT>
<ANA ID="9A"> 55-02 Aux termes de l'article L. 213-3 du code de la route : « Nul ne peut exploiter, à titre individuel, ou être dirigeant ou gérant de droit ou de fait d'un des établissements mentionnés à l'article L. 213-1, s'il ne satisfait aux conditions suivantes : 1° Ne pas avoir fait l'objet d'une condamnation : a) Soit à une peine criminelle ; b) Soit à une peine correctionnelle prononcée pour une infraction figurant sur une liste fixée par décret en Conseil d'État ; (&#133;) ». Le décret pris pour l'application de cette loi aux établissements organisant des stages de sensibilisation à la sécurité routière définit à l'article R. 212-4 du code de la route une liste d'infractions. 1) Les infractions listées par ce décret sont d'une nature et d'une gravité qui ne sont pas dépourvues de tout lien avec les exigences posées par le législateur ; au demeurant, une liste analogue d'infractions est définie pour la délivrance de l'agrément aux exploitants des établissements d'enseignement, à titre onéreux, de la conduite des véhicules à moteur ainsi que pour la délivrance de l'autorisation d'enseigner la conduite des véhicules à moteur et de l'autorisation d'animer des stages de sensibilisation à la sécurité routière, prévues respectivement aux articles R. 213-1 et R. 212-1 du même code. 2) La condition tenant à ce que les organisateurs de stages de sensibilisation à la sécurité routière et les personnes qu'ils désignent nommément pour l'encadrement administratif de ces stages n'aient pas été condamnés pour l'une des infractions prévues par l'article R. 212-4 du code de la route, sans qu'une limite tenant à l'ancienneté de la condamnation soit fixée, se borne à faire application des dispositions précitées de l'article L. 213-3 ; compte tenu par ailleurs de l'application des dispositions législatives du code pénal et du code de procédure pénale relatives à la réhabilitation légale, à la réhabilitation judiciaire et au relèvement, qui est de nature à limiter dans le temps les effets de l'interdiction résultant de la condamnation pour une des infractions prévues par le décret attaqué, le moyen tiré de ce que ce décret serait illégal faute d'avoir prévu lui-même une limite temporelle doit être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
