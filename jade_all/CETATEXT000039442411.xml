<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442411</ID>
<ANCIEN_ID>JG_L_2019_12_000000422307</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/24/CETATEXT000039442411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 02/12/2019, 422307</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422307</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422307.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Ysenbaert a demandé au tribunal administratif de Lille de condamner le département du Nord à lui verser la somme de 128 520,19 euros, augmentée des intérêts de droit à compter du 2 août 2012 et de la capitalisation des intérêts échus à compter de cette même date, au titre du paiement direct de ses prestations de sous-traitant dans le cadre d'un marché de fourniture et de pose de signalisation directionnelle passé avec la société Sécurité et Signalisation (SES). Par un jugement n° 1408343 du 18 octobre 2016, le tribunal administratif de Lille a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16DA02390 du 17 mai 2018, la cour administrative d'appel de Douai a, sur appel de la société Ysenbaert, annulé ce jugement et a condamné le département du Nord à verser à cette société la somme de 42 164,83 euros augmentée des intérêts au taux légal à compter du 7 septembre 2012 et de leur capitalisation à compter du 25 novembre 2014. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 juillet, 17 octobre 2018 et 9 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le département du Nord demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Ysenbaert la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - la loi n° 75-1334 du 31 décembre 1975 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département du Nord et à Me Occhipinti, avocat de la société Ysenbaert ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le département du Nord a conclu le 2 mars 2007 avec un groupement constitué notamment de la société SES un marché à bons de commandes, d'une durée d'un an renouvelable trois fois, portant sur la réalisation de travaux de signalisation verticale de jalonnement sur les routes du département. Par un acte spécial notifié le même jour à la société Ysenbaert, le département du Nord a accepté cette dernière comme sous-traitante de la société SES et a agréé ses conditions de paiement direct dans la limite d'un plafond annuel fixé à 107 640 euros TTC. Par un jugement du tribunal de commerce de Tours du 30 juin 2011, la société SES a été placée en liquidation judiciaire. Par un courrier du 2 août 2012, la société Ysenbaert a demandé au département du Nord le versement de la somme de 128 520,19 euros, au titre des prestations réalisées entre le 27 janvier et le 28 avril 2011. Le département a rejeté cette demande par un courrier du 7 septembre 2012. Une décision implicite de refus du département est née à la suite d'une nouvelle demande présentée le 5 mai 2014 par la société Ysenbaert. Par un jugement du 18 octobre 2016, le tribunal administratif de Lille a rejeté la demande de cette société tendant à la condamnation du département du Nord à lui verser la somme de 128 520,19 euros. Le département du Nord se pourvoit en cassation contre l'arrêt du 18 octobre 2016 par lequel la cour administrative d'appel de Douai a annulé ce jugement et l'a condamné à verser à la société Ysenbaert la somme de 42 164,83 euros. Par la voie du pourvoi incident, la société Ysenbaert demande l'annulation de l'arrêt, en tant qu'il n'a fait droit que partiellement à sa requête d'appel.  <br/>
<br/>
              2. En premier lieu, aux termes de l'article 3 de la loi du 31 décembre 1975 relative à la sous-traitance : " L'entrepreneur qui entend exécuter un contrat ou un marché en recourant à un ou plusieurs sous-traitants doit, au moment de la conclusion et pendant toute la durée du contrat ou du marché, faire accepter chaque sous-traitant et agréer les conditions de paiement de chaque contrat de sous-traitance par le maître de l'ouvrage (...) / Lorsque le sous-traitant n'aura pas été accepté ni les conditions de paiement agréées par le maître de l'ouvrage dans les conditions prévues à l'alinéa précédent, l'entrepreneur principal sera néanmoins tenu envers le sous-traitant mais ne pourra invoquer le contrat de sous-traitance à l'encontre du sous-traitant ". Aux termes de l'article 5 de la même loi : " (...) En cours d'exécution du marché, l'entrepreneur principal peut faire appel à de nouveaux sous-traitants, à la condition de les avoir déclarés préalablement au maître de l'ouvrage ". Aux termes de l'article 6 de la même loi : " Le sous-traitant direct du titulaire du marché qui a été accepté et dont les conditions de paiement ont été agréées par le maître de l'ouvrage, est payé directement par lui pour la part du marché dont il assure l'exécution (...) ". Aux termes de l'article 14-1 de la même loi : " (...) Le maître de l'ouvrage doit, s'il a connaissance de la présence sur le chantier d'un sous-traitant n'ayant pas fait l'objet des obligations définies à l'article 3 ou à l'article 6, ainsi que celles définies à l'article 5, mettre l'entrepreneur principal ou le sous-traitant en demeure de s'acquitter de ces obligations ". <br/>
<br/>
              3. Aux termes de l'article 114 du code des marchés publics, dans sa rédaction applicable au litige : " L'acceptation de chaque sous-traitant et l'agrément de ses conditions de paiement sont demandés dans les conditions suivantes : 1° Dans le cas où la demande de sous-traitance intervient au moment du dépôt de l'offre ou de la proposition, le candidat fournit au pouvoir adjudicateur une déclaration mentionnant : (...) c) Le montant maximum des sommes à verser par paiement direct au sous-traitant (...) ; 2° Dans le cas où la demande est présentée après le dépôt de l'offre, le titulaire remet contre récépissé au pouvoir adjudicateur ou lui adresse par lettre recommandée, avec demande d'avis de réception, une déclaration contenant les renseignements mentionnés au 1°./ L'acceptation du sous-traitant et l'agrément des conditions de paiement sont alors constatés par un acte spécial signé des deux parties. Figurent dans l'acte spécial les renseignements ci-dessus mentionnés au 1° ; 3° Si, postérieurement à la notification du marché, le titulaire envisage de confier à des sous-traitants bénéficiant du paiement direct l'exécution de prestations pour un montant supérieur à celui qui a été indiqué dans le marché ou l'acte spécial, il demande la modification de l'exemplaire unique ou du certificat de cessibilité prévus à l'article 106 du présent code (...). Le pouvoir adjudicateur ne peut pas accepter un sous-traitant ni agréer ses conditions de paiement si l'exemplaire unique ou le certificat de cessibilité n'a pas été modifié ou si la justification mentionnée ci-dessus ne lui a pas été remise. Toute modification dans la répartition des prestations entre le titulaire et les sous-traitants payés directement ou entre les sous-traitants eux-mêmes exige également la modification de l'exemplaire unique ou du certificat de cessibilité ou, le cas échéant, la production d'une attestation ou d'une mainlevée du ou des cessionnaires ".<br/>
<br/>
              4. Il résulte des dispositions citées aux points 2 et 3 qu'il incombe au maître d'ouvrage, lorsqu'il a connaissance de l'exécution, par le sous-traitant, de prestations excédant celles prévues par l'acte spécial et conduisant au dépassement du montant maximum des sommes à lui verser par paiement direct, de mettre en demeure le titulaire du marché ou le sous-traitant de prendre toute mesure utile pour mettre fin à cette situation ou pour la régulariser, à charge pour le titulaire du marché, le cas échéant, de solliciter la modification de l'exemplaire unique ou du certificat de cessibilité et celle de l'acte spécial afin de tenir compte d'une nouvelle répartition des prestations avec le sous-traitant. <br/>
<br/>
              5. Il résulte des pièces du dossier soumis aux juges du fond que le département du Nord avait connaissance du fait que le plafond, prévu dans l'acte acte spécial notifié le 2 mars 2007, de 107 640 euros, en-deçà duquel il devait payer directement la société Ysenbaert pour les prestations de sous-traitance réalisées pour le compte de la société SES, devait être dépassé vers le 15 novembre 2010, alors même que le contrat de sous-traitance courait jusqu'à la fin de l'année 2010. Dès lors, en jugeant que le département du Nord avait commis une faute en s'abstenant de mettre en demeure la société SES, titulaire du marché de réalisation de travaux de signalisation verticale de jalonnement sur les routes du département, de régulariser la situation de la société sous-traitante Ysenbaert eu égard aux articles 3 et 6 de la loi du 31 décembre 1975, la cour n'a entaché son arrêt ni d'erreur de droit, ni d'erreur de qualification juridique.<br/>
<br/>
              6. En deuxième lieu, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis en considérant qu'il existait un lien direct entre le comportement fautif du département et le préjudice subi par la société Ysenbaert, le département s'étant abstenu de toute démarche à l'égard de la société SES et ayant versé à cette dernière, ultérieurement placée en redressement judiciaire, les sommes correspondant aux travaux exécutés par la société Ysenbaert au-delà du montant maximal fixé par l'acte spécial.<br/>
<br/>
              7. En troisième lieu, en jugeant que la société Ysenbaert et la société SES avaient chacune commis une faute de nature à atténuer la responsabilité du département du Nord, la première en poursuivant l'exécution des prestations au-delà du montant maximum fixé par l'acte spécial sans s'assurer que sa situation avait été régularisée, la seconde en négligeant de soumettre à l'agrément du département les conditions de paiement du sous-traitant pour les prestations en cause, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              8. Enfin, c'est par une appréciation souveraine, exempte de dénaturation, que la cour a opéré un partage de responsabilité par tiers entre le département du Nord, la société SES et la société Ysenbaert, et a condamné le département à verser à cette dernière la somme de 42 164,83 euros.<br/>
<br/>
              9. Il résulte de tout ce qui précède que ni le département du Nord, par la voie du pourvoi principal, ni la société Ysenbaert, par la voie du pourvoi incident, ne sont fondés à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions des deux parties tendant à l'application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département du Nord est rejeté.<br/>
Article 2 : Le pourvoi incident de la société Ysenbaert et ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 3 : La présente décision sera notifiée au département du Nord et à la société Ysenbaert. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-03-01-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. CONDITIONS D'EXÉCUTION DES ENGAGEMENTS CONTRACTUELS EN L'ABSENCE D'ALÉAS. MARCHÉS. SOUS-TRAITANCE. - DROIT AU PAIEMENT DIRECT - MAÎTRE D'OUVRAGE AYANT CONNAISSANCE DE L'EXÉCUTION DE PRESTATIONS EXCÉDANT CELLES PRÉVUES PAR L'ACTE SPÉCIAL ET DÉPASSANT LE MONTANT MAXIMUM DU PAIEMENT DIRECT - OBLIGATION DE PRENDRE TOUTE MESURE UTILE POUR METTRE FIN À CETTE SITUATION OU LA RÉGULARISER.
</SCT>
<ANA ID="9A"> 39-03-01-02-03 Il résulte des articles 3, 5, 6 et 14-1 de la loi n° 75-1334 du 31 décembre 1975 et de l'article 114 du code des marchés publics (CMP) qu'il incombe au maître d'ouvrage, lorsqu'il a connaissance de l'exécution, par le sous-traitant, de prestations excédant celles prévues par l'acte spécial et conduisant au dépassement du montant maximum des sommes à lui verser par paiement direct, de mettre en demeure le titulaire du marché ou le sous-traitant de prendre toute mesure utile pour mettre fin à cette situation ou pour la régulariser, à charge pour le titulaire du marché, le cas échéant, de solliciter la modification de l'exemplaire unique ou du certificat de cessibilité et celle de l'acte spécial afin de tenir compte d'une nouvelle répartition des prestations avec le sous-traitant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
