<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089180</ID>
<ANCIEN_ID>JG_L_2018_06_000000410596</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089180.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème chambre jugeant seule, 20/06/2018, 410596, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410596</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410596.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet des Alpes-Maritimes a déféré au tribunal administratif de Nice la société civile immobilière (SCI) Cap Azur comme prévenue d'une contravention de grande voirie pour l'occupation sans droit ni titre du domaine public maritime sur le territoire de la commune de Saint-Jean-Cap-Ferrat. Par un jugement n° 1303141 du 16 juillet 2015, le tribunal a, d'une part, jugé que l'action publique était prescrite, d'autre part, condamné la SCI Cap Azur à procéder à la démolition des ouvrages mentionnés dans le procès-verbal du 15 février 2012, à remettre en état le domaine public maritime et à évacuer les gravats vers un centre de traitement agréé.<br/>
<br/>
              Par un arrêt n° 15MA03960 du 16 mars 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la SCI Cap Azur contre ce jugement, en tant qu'il avait statué sur l'action domaniale.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mai et 16 août 2017 et le 13 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la SCI Cap Azur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la SCI Cap Azur.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'un procès-verbal de contravention de grande voirie a été dressé le 15 février 2012 à l'encontre de la SCI Cap Azur à raison de la présence sur le domaine public maritime, sans autorisation d'occupation, de différents ouvrages, notamment un port abri comportant un garage à bateau, situés au droit de sa propriété " Les Rochers " à Saint-Jean-Cap-Ferrat (Alpes-Maritimes) et d'une propriété voisine, édifiés de nombreuses années avant que la SCI Cap Azur n'acquière, en 2004, la villa " Les Rochers ". La SCI Cap Azur se pourvoit en cassation contre l'arrêt du 16 mars 2017 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 16 juillet 2015 par lequel le tribunal administratif de Nice a jugé qu'elle avait maintenu sans autorisation ces ouvrages sur le domaine public maritime et l'a condamnée à les détruire et à remettre dans leur état naturel les dépendances du domaine public maritime en cause.<br/>
<br/>
              2. La personne qui peut être poursuivie pour contravention de grande voirie est soit celle qui a commis ou pour le compte de laquelle a été commise l'action qui est à l'origine de l'infraction, soit celle sous la garde de laquelle se trouvait l'objet qui a été la cause de la contravention.<br/>
<br/>
              3. En premier lieu, en jugeant que le procès-verbal de constat d'huissier produit par la société requérante ne permettait pas d'établir les motifs de la présence de plusieurs personnes sur le versant du port abri situé au-dessous de la propriété voisine, le château Saint-Jean, alors que ce constat mentionnait " la présence de plusieurs personnes s'activant à l'entretien ", la cour a dénaturé les pièces du dossier.<br/>
<br/>
              4. En second lieu, en se fondant sur le fait que la SCI Cap Azur n'aurait pas établi que l'usage du port abri, qu'elle ne contestait pas utiliser elle-même, serait partagé avec la propriété voisine et que cet ouvrage serait fréquenté par diverses autres personnes et sur la circonstance que la SCI n'aurait pas contesté avoir l'usage exclusif des autres ouvrages et installations, pour juger que la SCI avait la garde de ces ouvrages et que, dès lors, elle avait été poursuivie à bon droit pour contravention de grande voirie à raison de l'occupation irrégulière du domaine public maritime, alors que ces seules circonstances ne suffisaient pas à caractériser l'existence d'un pouvoir de direction et de contrôle permettant de regarder la SCI Cap Azur comme ayant la garde de ces ouvrages, la cour administrative d'appel de Marseille a entaché son arrêt d'une erreur de qualification juridique des faits.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SCI Cap Azur est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la SCI Cap Azur de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : L'arrêt du 16 mars 2017 de la cour administrative d'appel de Marseille est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la SCI Cap Azur la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Cap Azur et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
