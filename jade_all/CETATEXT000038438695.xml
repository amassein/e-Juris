<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038438695</ID>
<ANCIEN_ID>JG_L_2019_04_000000419891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/86/CETATEXT000038438695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/04/2019, 419891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419891.20190430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1707764 du 6 avril 2018, enregistrée le 16 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Montreuil a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par Mme A...B....  <br/>
<br/>
              Par cette requête, enregistrée le 28 août 2017 au greffe du tribunal administratif, et par six mémoires en réplique, enregistrés les 6 décembre, 17 décembre 2018, 4 janvier, 8 janvier, 16 janvier et 1er avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 8 juin 2017 rapportant le décret du 14 janvier 2015 qui lui avait accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - le code civil ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 avril 2019, présentée par Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Il ressort des pièces de dossier que MmeB..., ressortissante algérienne, a déposé une demande de naturalisation le 17 juin 2014 par laquelle elle a indiqué être célibataire et s'est engagée sur l'honneur à signaler tout changement dans sa situation personnelle et familiale. Au vu de ses déclarations, elle a été naturalisée par décret du 14 janvier 2015. Toutefois, par bordereau reçu le 19 juin 2015, le ministre des affaires étrangères et du développement international a informé le ministre chargé des naturalisations que Mme B...avait épousé en Algérie, le 17 septembre 2014, un ressortissant algérien résidant en Algérie. Par décret du 8 juin 2017, publié au Journal officiel le 10 juin 2017, sur avis conforme du Conseil d'Etat en date du 16 mai 2017, le Premier ministre a rapporté le décret du 14 janvier 2015 prononçant la naturalisation de Mme B...au motif qu'il avait été pris au vu d'informations mensongères fournies par l'intéressée sur sa situation familiale. Mme B...demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3.	En premier lieu, il ressort des mentions de l'ampliation du décret attaqué, certifiée conforme par le secrétaire général du Gouvernement, que le décret a été signé par le Premier ministre et contresigné par le ministre d'Etat, ministre de l'intérieur. Le moyen tiré de ce que le décret attaqué n'aurait pas été signé par le Premier ministre ne peut qu'être écarté.<br/>
<br/>
              4.	En deuxième lieu, le décret attaqué comporte l'indication des éléments de droit et de fait sur lesquels il se fonde et est ainsi suffisamment motivé. <br/>
<br/>
              5.	En troisième lieu, il ressort des pièces du dossier que Mme B...s'est mariée le 17 septembre 2014 à Mansourah (Algérie) avec un ressortissant algérien résidant en Algérie. Ce mariage a constitué un changement de sa situation personnelle et familiale que l'intéressée aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, comme elle s'y était engagée en déposant sa demande de naturalisation, ce qu'elle n'a pas fait avant que ne lui soit accordée la nationalité française. Si Mme B...soutient qu'elle était de bonne foi et qu'elle a sollicité elle-même que soit indiqué son nom d'épouse sur sa carte d'identité auprès des services de la préfecture de la Seine-Saint-Denis, elle ne fait état d'aucune circonstance qui l'aurait mise dans l'impossibilité de faire part de son changement de situation familiale au service chargé de l'instruction de son dossier avant l'intervention du décret lui accordant la nationalité française. L'intéressée, qui maîtrise la langue française ainsi qu'il ressort du procès-verbal d'assimilation du 18 juin 2014, ne pouvait se méprendre sur la teneur de l'engagement qu'elle avait pris sur l'honneur, en déposant sa demande de naturalisation, de faire connaître toute modification de sa situation familiale survenant au cours de l'instruction de sa demande de naturalisation. Dans ces conditions, Mme B...doit être regardée comme ayant volontairement dissimulé le changement de sa situation familiale. Par suite, en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude, le ministre d'Etat, ministre de l'intérieur, n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil. <br/>
<br/>
              6.	En quatrième lieu, l'article 21-16 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressée ait dissimulé s'être mariée en Algérie avec un ressortissant algérien était de nature à modifier l'appréciation qui avait été portée par l'autorité administrative sur la fixation du centre de ses intérêts. <br/>
<br/>
              7.	En dernier lieu, la définition des conditions d'acquisition et de perte de la nationalité relève de la compétence de chaque Etat membre de l'Union européenne. Toutefois, dans la mesure où la perte de la nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité. Ces dispositions permettaient en l'espèce, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, de rapporter légalement le décret accordant à Mme B...la nationalité française. Au demeurant, un décret qui rapporte pour fraude un décret de naturalisation est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille. En dépit de la bonne intégration de la requérante, il ne ressort pas des pièces du dossier qu'en prenant le décret attaqué le Premier ministre ait commis une erreur manifeste d'appréciation.<br/>
<br/>
              8.	Il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation pour excès de pouvoir du décret qu'elle attaque. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
