<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030296243</ID>
<ANCIEN_ID>JG_L_2015_02_000000350590</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/29/62/CETATEXT000030296243.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/02/2015, 350590, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350590</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:350590.20150227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La Société nationale d'exploitation industrielle des tabacs et allumettes (Seita) a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations de taxe professionnelle auxquelles elle a été assujettie au titre des années 2000 à 2005. Par un jugement n°s 0204539 et 0605088 du 7 juillet 2009, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 09VE03423 du 3 mai 2011, la cour administrative d'appel de Versailles a annulé ce jugement et accordé la réduction des cotisations litigieuses à concurrence des différences, si elles sont positives, entre, d'une part, les cotisations mises en recouvrement et, d'autre part, celles résultant du calcul effectué conformément aux règles prescrites dans son arrêt, au titre de chacune des années en litige. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés respectivement les 4 juillet 2011, 4 octobre 2011, 7 janvier 2013 et 24 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la Seita demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de l'arrêt n° 09VE03423 du 3 mai 2011 de la cour administrative d'appel de Versailles ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 80-495 du 2 juillet 1980 ;<br/>
              - la loi n° 84-603 du 13 juillet 1984 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la Société nationale d'exploitation industrielle des tabacs et allumettes.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par des réclamations contentieuses des 26 décembre 2001, 23 décembre 2003 et 19 décembre 2005, la Société nationale d'exploitation industrielle des tabacs et allumettes (Seita) a sollicité la réduction des cotisations de taxe professionnelle qu'elle a acquittées au titre des années 2000 à 2005, à raison notamment d'un établissement situé à Saint-Denis, dont le caractère industriel n'est plus en litige. Elle contestait, en particulier, les modalités de détermination de la valeur locative des biens passibles d'une taxe foncière entrant dans l'assiette de la taxe professionnelle. Elle a saisi du litige le tribunal administratif de Cergy-Pontoise qui, par un jugement du 7 juillet 2009, a rejeté ses demandes. Elle se pourvoit en cassation contre l'article 2 de l'arrêt du 3 mai 2011 par lequel la cour administrative d'appel de Versailles, après avoir annulé ce jugement, n'a accordé la réduction des cotisations litigieuses qu'à hauteur de la réduction résultant éventuellement de l'application des règles prescrites dans son arrêt. Le ministre des finances et des comptes publics demande, par la voie du pourvoi incident, l'annulation de l'article 3 de cet arrêt par lequel la cour a mis à sa charge la somme de 1 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2. Pour l'établissement de la taxe professionnelle, aux termes de l'article 1469 du code général des impôts, la valeur locative des biens passibles de la taxe foncière est calculée suivant les règles fixées pour l'établissement de cette taxe, qui figurent, s'agissant des immobilisations industrielles, à l'article 1499 de ce code. L'article 324 AE de l'annexe III au même code dispose : " Le prix de revient visé à l'article 1499 du code général des impôts s'entend de la valeur d'origine pour laquelle les immobilisations doivent être inscrites au bilan en conformité de l'article 38 quinquies " et le 1 de l'article 38 quinquies de cette même annexe précise que la valeur d'inscription d'un bien est sa valeur d'origine, laquelle s'entend : " (...) b. Pour les immobilisations acquises à titre gratuit, de la valeur vénale ; (...) ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a regardé comme acquis à titre gratuit, au sens de ces dispositions, les biens de la société requérante. Or, si l'article 1er de la loi du 13 juillet 1984 créant la Société industrielle d'exploitation des tabacs et allumettes a créé, à compter du 1er janvier 1985, une société qui s'est substituée de plein droit à la société précédemment créée par la loi du 2 juillet 1980 portant modification du statut du service d'exploitation industrielle des tabacs et allumettes, il résulte de l'ensemble des dispositions de cette loi, éclairées par les travaux parlementaires, que ces dispositions ont eu pour seul objet d'opérer une substitution entre l'ancienne et la nouvelle société, sans entraîner d'autre modification qu'un changement de dénomination. Ainsi, les immobilisations corporelles de la seconde société ne sauraient être regardées, pour l'application des dispositions des articles 324 AE et 38 quinquies de l'annexe III au code général des impôts, comme ayant été acquises par elle, serait-ce à titre gratuit. Il suit de là que la cour a commis une erreur de droit en se fondant sur la valeur vénale de l'immeuble industriel en litige pour en déterminer la valeur locative destinée à l'établissement des cotisations de taxe professionnelle dont la société requérante demande la réduction. <br/>
<br/>
              4. Au surplus, la cour a appliqué à la valeur locative qu'elle a déterminée, en considération des frais de gestion, d'assurances, d'amortissement, d'entretien et de réparation, l'abattement de 50 % mentionné à l'article 1388 du code général des impôts relatif à la taxe foncière sur les propriétés bâties. Toutefois, si, comme indiqué au point 2, la valeur locative servant de base à la taxe professionnelle est calculée, pour les biens passibles de taxe foncière, suivant les règles fixées pour l'établissement de cette taxe conformément au 1° de l'article 1469 du code général des impôts, la valeur locative à retenir est une valeur cadastrale brute. L'abattement forfaitaire prévu par l'article 1388 n'est, dès lors, pas applicable en matière de taxe professionnelle.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la Seita est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'article 2 de l'arrêt qu'elle attaque. Il y a lieu également d'annuler l'article 3 de cet arrêt, par lequel la cour, en conséquence de la réduction des cotisations de taxe professionnelle qu'elle accordait à la Seita par l'article 2 de son arrêt, avait mis la somme de 1 000 euros à la charge de l'Etat, au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de faire droit à la demande de la société requérante tendant à ce qu'une somme de 3 500 euros soit mise à la charge de l'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les articles 2 et 3 de l'arrêt du 3 mai 2011 de la cour administrative d'appel de Versailles sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Versailles. <br/>
Article 3 : L'Etat versera la somme de 3 500 euros à la Société nationale d'exploitation industrielle des tabacs et allumettes au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Société nationale d'exploitation industrielle des tabacs et allumettes et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
