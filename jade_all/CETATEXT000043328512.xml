<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043328512</ID>
<ANCIEN_ID>JG_L_2021_04_000000437799</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/85/CETATEXT000043328512.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/04/2021, 437799</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437799</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437799.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B... C... et Mme A... C..., agissant en leur nom et en leur qualité de représentants de leur fille, et M. D... C... ont demandé au tribunal administratif de Montreuil, d'une part, de condamner l'Etat à leur verser la somme de 20 000 euros en réparation des préjudices qu'ils estiment avoir subis en raison de leur absence de relogement et, d'autre part, en application de l'article L. 441-2-3-1 du code de la construction et de l'habitation, d'ordonner à l'Etat de les reloger sans délai, sous astreinte de 2 000 euros par mois de retard. <br/>
<br/>
              Par un jugement n° 1809470 du 19 novembre 2019, le tribunal administratif a rejeté leur demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 6 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. C... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la commission de médiation de la Seine-Saint-Denis a, par une décision du 12 novembre 2014, désigné M. C..., marié et père de deux enfants, comme prioritaire et devant être logé en urgence. Par un jugement du 15 septembre 2015, le tribunal administratif de Montreuil, saisi par l'intéressé sur le fondement de l'article L. 441-2-3-1 du code de la construction et de l'habitation, a enjoint au préfet de la Seine-Saint-Denis d'assurer son relogement et celui de sa famille, sous astreinte de 550 euros par mois de retard.<br/>
<br/>
              2. Constatant le défaut d'exécution de ce jugement, M. C..., son épouse et son fils ont, le 2 octobre 2018, demandé au même tribunal administratif de condamner l'Etat à leur verser la somme de 20 000 euros en réparation du préjudice subi du fait de l'absence de relogement, en assortissant ces conclusions de conclusions tendant, à nouveau, à ce qu'il soit ordonné à l'Etat d'assurer leur relogement sous astreinte. Ils se pourvoient en cassation contre le jugement du 19 novembre 2019 par lequel le tribunal administratif a rejeté leurs conclusions. <br/>
<br/>
              Sur le jugement attaqué en tant qu'il statue sur les conclusions à fin d'indemnisation :<br/>
<br/>
              3. Lorsqu'une personne a été reconnue comme prioritaire et devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation. Dans le cas où le demandeur a été reconnu prioritaire au seul motif que sa demande de logement social n'a pas reçu de réponse dans le délai réglementaire, son maintien dans le logement où il réside ne peut être regardé comme entraînant des troubles dans ses conditions d'existence lui ouvrant droit à réparation que si ce logement est inadapté au regard, notamment, de ses capacités financières et de ses besoins.<br/>
<br/>
              4. Il résulte des termes du jugement attaqué que, pour rejeter les conclusions indemnitaires dont il était saisi, le tribunal s'est fondé sur ce que M. C... avait été désigné comme prioritaire au seul motif que sa demande de logement social n'avait pas reçu de réponse dans le délai réglementaire et qu'il n'avait produit aucune pièce de nature à établir que le logement où il résidait n'était pas adapté à ses capacités financières.<br/>
<br/>
              5. En statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis que le loyer mensuel du logement de M. C... s'élevait à 950 euros et que, en réponse à une mesure d'instruction ordonnée par le tribunal, l'intéressé avait produit un avis d'imposition sur les revenus de l'année 2018 mentionnant un revenu imposable annuel de 8 803 euros, le tribunal administratif a dénaturé les pièces du dossier. <br/>
<br/>
              Sur le jugement attaqué en tant qu'il statue sur les conclusions à fin que soit ordonné un relogement : <br/>
<br/>
              6. Aux termes de l'article L. 300-1 du code de la construction et de l'habitation : " Le droit à un logement décent et indépendant (...) est garanti par l'Etat à toute personne qui (...) n'est pas en mesure d'y accéder par ses propres moyens ou de s'y maintenir. / Ce droit s'exerce par un recours amiable puis, le cas échéant, par un recours contentieux dans les conditions et selon les modalités fixées par le présent article et les articles L. 441-2-3 et L. 441-2-3-1 ". Aux termes de l'article L. 441-2-3-1 du même code : " I. -Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu (...) une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement. / (...) Le président du tribunal administratif ou le magistrat qu'il désigne statue en urgence, dans un délai de deux mois à compter de sa saisine. Sauf renvoi à une formation collégiale, l'audience se déroule sans conclusions du commissaire du Gouvernement. / Le président du tribunal administratif ou le magistrat qu'il désigne, lorsqu'il constate que la demande a été reconnue comme prioritaire par la commission de médiation et doit être satisfaite d'urgence et que n'a pas été offert au demandeur un logement tenant compte de ses besoins et de ses capacités, ordonne le logement ou le relogement de celui-ci par l'Etat et peut assortir son injonction d'une astreinte (...) ". <br/>
<br/>
              7. Aux termes de l'article R. 612-1 du code de justice administrative : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser (...). La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 ".<br/>
<br/>
              8. Si, ainsi qu'il a été dit au point 3, le bénéficiaire d'une décision favorable de la commission de médiation peut, en cas de carence de l'administration à exécuter cette décision dans le délai imparti, demander au juge administratif de condamner l'Etat à l'indemniser des troubles dans ses conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, il ne peut présenter dans la même demande des conclusions tendant à ce qu'il soit ordonné à l'Etat d'assurer son logement ou son relogement conformément à la décision de la commission de médiation, de telles conclusions ne pouvant être portées que devant le tribunal administratif statuant dans les conditions prévues par l'article L. 441-2-3-1 du code de la construction et de l'habitation.<br/>
<br/>
              9. Par suite, lorsque le tribunal administratif, saisi comme juge de droit commun du contentieux administratif d'un recours tendant à la mise en cause de la responsabilité de l'Etat, est simultanément saisi de conclusions relevant des dispositions de l'article L. 441-2-3-1 du code de la construction et de l'habitation, il lui appartient, en application de l'article R. 612-1 du code de justice administrative, d'inviter son auteur à les régulariser en les présentant par une requête distincte. Il ne peut en aller autrement que s'il apparaît que ces conclusions peuvent être rejetées par le tribunal comme irrecevables, notamment lorsqu'elles sont présentées au-delà du délai prévu par les articles R. 778-2 du code de justice administrative et R. 441-18-2 du code de la construction et de l'habitation. Toutefois, dans ce dernier cas, s'il appartient au tribunal de relever d'office une telle irrecevabilité, il ne peut le faire qu'après en avoir informé les parties conformément, aux dispositions de l'article R. 611-7 du code de justice administrative.<br/>
<br/>
              10. Il résulte de ce qui précède que M. C... et autres sont fondés à soutenir qu'en rejetant leurs conclusions tendant à ce qu'il soit ordonné à l'Etat de les reloger au motif qu'elles soulevaient un litige distinct de leur demande principale, sans les avoir au préalable invités à les régulariser par la production d'une requête distincte, le tribunal a entaché son jugement d'irrégularité.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. C... et autres sont fondés à demander l'annulation du jugement qu'ils attaquent.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. C... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 19 novembre 2019 du tribunal administratif de Montreuil est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
<br/>
Article 3 : L'Etat versa à M. C... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... C..., premier requérant dénommé et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - REQUÊTE DALO ASSORTIE DE CONCLUSIONS INDEMNITAIRES - 1) RECEVABILITÉ DE CES CONCLUSIONS - ABSENCE [RJ1] - 2) OBLIGATION D'INVITER À LES RÉGULARISER PAR PRÉSENTATION D'UNE REQUÊTE DISTINCTE - EXISTENCE [RJ1] - 3) EXCEPTION - IRRECEVABILITÉ DES CONCLUSIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - REQUÊTE DALO ASSORTIE DE CONCLUSIONS INDEMNITAIRES - 1) RECEVABILITÉ DE CES CONCLUSIONS - ABSENCE [RJ1] - 2) OBLIGATION D'INVITER À LES RÉGULARISER PAR PRÉSENTATION D'UNE REQUÊTE DISTINCTE - EXISTENCE [RJ1] - 3) EXCEPTION - IRRECEVABILITÉ DES CONCLUSIONS.
</SCT>
<ANA ID="9A"> 38-07-01 1) Si une personne été reconnue comme prioritaire et devant être logée ou relogée d'urgence par une commission de médiation peut, en cas de carence de l'administration à exécuter cette décision dans le délai imparti, demander au juge administratif de condamner l'Etat à l'indemniser des troubles dans ses conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, elle ne peut présenter dans la même demande des conclusions tendant à ce qu'il soit ordonné à l'Etat d'assurer son logement ou son relogement conformément à la décision de la commission de médiation, de telles conclusions ne pouvant être portées que devant le tribunal administratif statuant dans les conditions prévues par l'article L. 441-2-3-1 du code de la construction et de l'habitation (CCH).,,2) Par suite, lorsque le tribunal administratif, saisi comme juge de droit commun du contentieux administratif d'un recours tendant à la mise en cause de la responsabilité de l'Etat, est simultanément saisi de conclusions relevant de l'article L. 441-2-3-1 du CCH, il lui appartient, en application de l'article R. 612-1 du code de justice administrative (CJA), d'inviter son auteur à les régulariser en les présentant par une requête distincte.... ...3) Il ne peut en aller autrement que s'il apparaît que ces conclusions peuvent être rejetées par le tribunal comme irrecevables, notamment lorsqu'elles sont présentées au-delà du délai prévu par les articles R. 778-2 du CJA et R. 441-18-2 du CCH. Toutefois, dans ce dernier cas, s'il appartient au tribunal de relever d'office une telle irrecevabilité, il ne peut le faire qu'après en avoir informé les parties conformément, à l'article R. 611-7 du CJA.</ANA>
<ANA ID="9B"> 60-02-012 1) Si une personne été reconnue comme prioritaire et devant être logée ou relogée d'urgence par une commission de médiation peut, en cas de carence de l'administration à exécuter cette décision dans le délai imparti, demander au juge administratif de condamner l'Etat à l'indemniser des troubles dans ses conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, elle ne peut présenter dans la même demande des conclusions tendant à ce qu'il soit ordonné à l'Etat d'assurer son logement ou son relogement conformément à la décision de la commission de médiation, de telles conclusions ne pouvant être portées que devant le tribunal administratif statuant dans les conditions prévues par l'article L. 441-2-3-1 du code de la construction et de l'habitation (CCH).,,2) Par suite, lorsque le tribunal administratif, saisi comme juge de droit commun du contentieux administratif d'un recours tendant à la mise en cause de la responsabilité de l'Etat, est simultanément saisi de conclusions relevant de l'article L. 441-2-3-1 du CCH, il lui appartient, en application de l'article R. 612-1 du code de justice administrative (CJA), d'inviter son auteur à les régulariser en les présentant par une requête distincte.... ...3) Il ne peut en aller autrement que s'il apparaît que ces conclusions peuvent être rejetées par le tribunal comme irrecevables, notamment lorsqu'elles sont présentées au-delà du délai prévu par les articles R. 778-2 du CJA et R. 441-18-2 du CCH. Toutefois, dans ce dernier cas, s'il appartient au tribunal de relever d'office une telle irrecevabilité, il ne peut le faire qu'après en avoir informé les parties conformément, à l'article R. 611-7 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 mars 2013,,, n° 341269, T. p. 686.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
