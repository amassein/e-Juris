<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659639</ID>
<ANCIEN_ID>JG_L_2020_12_000000431093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659639.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 09/12/2020, 431093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431093.20201209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme D... A... C... a demandé à la commission départementale d'aide sociale des Bouches-du-Rhône d'annuler la décision du 15 avril 2016 de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône de récupérer un indu de 2 882,43 euros en raison de prestations indument perçues au titre de la protection complémentaire en matière de santé pour la période comprise entre le 1er mai 2015 et le 30 avril 2016. Par une décision du 8 septembre 2016, la commission départementale d'aide sociale des Bouches-du-Rhône a fait droit à sa demande.<br/>
<br/>
              Par une décision n° 160601 du 23 juillet 2018, la Commission centrale d'aide sociale a, sur l'appel formé par la caisse primaire d'assurance maladie des Bouches-du-Rhône, annulé la décision du 8 septembre 2016 de la commission départementale d'aide sociale des Bouches-du-Rhône.<br/>
<br/>
              1° Sous le n° 431093, par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat de la section du contentieux les 27 mai et 27 août 2019, Mme A... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale du 23 juillet 2018 ;<br/>
<br/>
              2°) de mettre à la charge de la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 431138, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 mai et 27 août 2019, Mme A... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale du 23 juillet 2018 ;<br/>
<br/>
              2°) de mettre à la charge de la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et de familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de Mme A... C... et à la SCP Boutet-Hourdeaux, avocat de la caisse primaire d'assurance maladie de Marseille ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les documents enregistrés sous le n° 431138 constituent en réalité des mémoires faisant suite au pourvoi de Mme A... C... enregistré sous le n° 431093. Par suite, ces documents doivent être rayés des registres du secrétariat du contentieux du Conseil d'Etat et être joints au pourvoi enregistré sous le n° 431093.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... C... a bénéficié de la protection complémentaire en matière de santé pour la période du 1er mai 2015 au 30 avril 2016. Après avoir diligenté un contrôle de sa situation, au cours duquel a été mis en oeuvre le droit de communication, la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône a invité l'intéressée, le 18 janvier 2016, à justifier de virements effectués à son profit sur ses comptes bancaires entre le 18 novembre 2014 et le 3 février 2015. La caisse primaire a ensuite décidé, le 15 avril 2016, la récupération d'un indu de 2 882,43 euros en raison de prestations indûment perçues au titre de la protection complémentaire. Par une décision du 8 septembre 2016, la commission départementale d'aide sociale des Bouches-du-Rhône a annulé cette décision à la demande de Mme A... C.... Cette dernière se pourvoit en cassation contre la décision du 23 juillet 2018 par laquelle la Commission centrale d'aide sociale a, sur l'appel de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône, annulé la décision de la commission départementale.<br/>
<br/>
              Sur la régularité de la décision de la Commission centrale d'aide sociale :<br/>
<br/>
              3 En premier lieu, aux termes de l'article L. 134-9 du code de l'action sociale et des familles, applicable à la procédure devant les juges du fond : " Le demandeur, accompagné de la personne ou de l'organisme de son choix, est entendu lorsqu'il le souhaite, devant la commission départementale et la commission centrale d'aide sociale ". Ces dispositions imposaient à la Commission centrale d'aide sociale de mettre les parties à même d'exercer la faculté qui leur était ainsi reconnue. A cet effet, elle devait soit avertir les parties de la date de la séance, soit les inviter à l'avance à lui faire connaître si elles avaient l'intention de présenter des explications orales pour qu'en cas de réponse affirmative de leur part, elle avertisse ultérieurement de la date de la séance celles des parties qui auraient manifesté une telle intention. En outre, le caractère contradictoire de la procédure lui interdisait de se fonder sur des mémoires ou des pièces si les parties n'avaient pas été effectivement mises à même d'en prendre connaissance et de faire connaître les observations qu'ils appelaient de leur part.  <br/>
<br/>
              4. Il ressort des pièces de la procédure devant la Commission centrale d'aide sociale que celle-ci a informé Mme A... C..., par un courrier du 4 janvier 2017, de l'appel formé par la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône ainsi que de la possibilité qu'elle avait, en faisant connaître son intention dans un délai d'un mois, de consulter le dossier du litige et de demander à être entendue lors de la séance de jugement. Il ressort également de ces pièces que l'intéressée a répondu à ce courrier par un mémoire du 17 janvier 2017, sans demander à prendre connaissance de l'appel de la caisse primaire ni faire connaître son intention de présenter des explications orales. Par suite, Mme A... C... n'est pas fondée à soutenir que la commission centrale aurait méconnu l'article L. 134-9 du code de l'action sociale et des familles et le caractère contradictoire de la procédure et, ainsi, statué au terme d'une procédure irrégulière.<br/>
<br/>
              5. En deuxième lieu, au nombre des règles que la Commission centrale d'aide sociale était tenue de respecter, figure celle d'après laquelle ses décisions doivent contenir notamment l'analyse des conclusions des parties et de leur argumentation. Il ressort des pièces de la procédure devant les juges du fond que Mme A... C... a produit le 17 janvier 2017 un mémoire, enregistré au secrétariat de la Commission centrale d'aide sociale le 6 février 2017, par lequel elle demandait " un nouvel examen de son dossier " en faisant valoir la faiblesse de ses revenus, qui ne lui permettait pas, selon elle, de rembourser l'indu mis à sa charge par la caisse primaire d'assurance maladie des Bouches-du-Rhône. Il ressort des mentions de la décision attaquée que la Commission centrale d'aide sociale n'a pas analysé ce mémoire dans les visas de sa décision. Si ce mémoire ne contenait aucune argumentation relative au bien-fondé de l'indu à laquelle la commission centrale n'aurait pas répondu dans les motifs de sa décision, en revanche, la commission centrale ne s'est pas prononcée sur les conclusions de l'intéressée à fin de remise gracieuse. Mme A... C... est, dès lors, fondée à soutenir, en tant seulement qu'elle omet de se prononcer sur ces conclusions, que la décision du 23 juillet 2018 est entachée d'irrégularité.<br/>
<br/>
              6. En dernier lieu, en jugeant que Mme A... C... avait omis de déclarer des virements bancaires de façon intentionnelle, la commission centrale a suffisamment motivé sa décision sur ce point au regard de l'argumentation dont elle était saisie.  <br/>
<br/>
              Sur le bien-fondé de la décision de la Commission centrale d'aide sociale, en tant qu'elle statue sur le bien-fondé de l'indu :<br/>
<br/>
              7. En vertu de l'article L. 861-1 du code de la sécurité sociale, dans sa rédaction applicable au litige, les personnes résidant en France dans les conditions prévues par l'article L. 380-1 du même code, dont les ressources sont inférieures à un plafond déterminé par décret selon la composition du foyer et le nombre de personnes à charge, ont droit à une protection complémentaire en matière de santé. Aux termes de l'article L. 861-2 du même code, dans sa rédaction alors applicable : " L'ensemble des ressources du foyer est pris en compte pour la détermination du droit à la protection complémentaire en matière de santé, après déduction des charges consécutives aux versements des pensions et obligations alimentaires, à l'exception du revenu de solidarité active, de certaines prestations à objet spécialisé et de tout ou partie des rémunérations de nature professionnelle lorsque celles-ci ont été interrompues (...) ". L'article R. 861-8 de ce code, dans sa rédaction alors en vigueur, précise que : " Les ressources prises en compte sont celles qui ont été effectivement perçues au cours de la période des douze mois civils précédant la demande (...) ", sous réserve des dispositions particulières des articles R. 861-11, R. 861-14 et R. 861-15 applicables aux travailleurs non salariés. Enfin, aux termes de l'article L. 861-10 du même code : " I.- En cas de réticence du bénéficiaire de la protection complémentaire en matière de santé à fournir les informations requises ou de fausse déclaration intentionnelle, la décision attribuant la protection complémentaire est rapportée. (...) / IV.- Les organismes prévus à l'article L. 861-4 peuvent obtenir le remboursement des prestations qu'ils ont versées à tort. (...) ".  <br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône avait invité Mme A... C... à présenter ses explications sur vingt et un virements qui avaient été effectués à son profit entre le 18 novembre 2014 et le 3 février 2015, au cours de la période des douze mois civils précédant sa demande, allant du 1er mars 2014 au 28 février 2015, et qui faisaient apparaître des ressources non déclarées au moment de cette demande. Eu égard aux dates de ces virements et en l'absence de contestation sur ce point, Mme A... C... n'est pas fondée à soutenir que la Commission centrale d'aide sociale aurait commis une erreur de droit en retenant certaines de ces sommes sans vérifier qu'elles avaient été effectivement perçues au cours de la période de douze mois précédant sa demande, conformément aux dispositions de l'article R. 861-8 du code de la sécurité sociale.  <br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme A... C... est fondée à demander l'annulation de la décision de la Commission centrale d'aide sociale en tant seulement qu'elle ne statue pas sur ses conclusions à fin de remise gracieuse. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de remise gracieuse :<br/>
<br/>
              11. Aux termes du IV de l'article L. 861-10 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Les organismes prévus à l'article L. 861-4 peuvent obtenir le remboursement des prestations qu'ils ont versées à tort. En cas de précarité de la situation du demandeur, la dette peut être remise ou réduite sur décision de l'autorité administrative compétente. Les recours contentieux contre les décisions relatives aux demandes de remise ou de réduction de dette et contre les décisions ordonnant le reversement des prestations versées à tort sont portés devant la juridiction mentionnée au troisième alinéa de l'article L. 861-5 ", c'est-à-dire la commission départementale d'aide sociale.<br/>
<br/>
              12. Il résulte des dispositions du IV de l'article L. 861-10 du code de la sécurité sociale que le juge de l'aide sociale ne peut être saisi de conclusions tendant à la remise ou à la réduction d'un indu résultant de prestations versées à tort qu'à la condition qu'une demande gracieuse en ce sens ait été préalablement présentée par le requérant. Par suite, les conclusions de Mme A... C..., qui n'ont pas été précédées d'une telle demande, ne sont pas recevables.  <br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les productions enregistrées sous le n° 431138 seront rayées du registre du secrétariat du contentieux du Conseil d'Etat pour être jointes au pourvoi n° 431093.<br/>
Article 2 : La décision de la Commission centrale d'aide sociale du 23 juillet 2018 est annulée en tant qu'elle omet de statuer sur les conclusions de Mme A... C... à fin de remise gracieuse.<br/>
Article 3 : Les conclusions à fin de remise gracieuse présentées par Mme A... C... devant la Commission centrale d'aide sociale sont rejetées.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de Mme A... C... est rejeté.<br/>
Article 5 : La présente décision sera notifiée à Mme D... A... C... et à la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
