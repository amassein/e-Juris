<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043246431</ID>
<ANCIEN_ID>JG_L_2021_03_000000442602</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/64/CETATEXT000043246431.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 12/03/2021, 442602, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442602</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:442602.20210312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 août et 5 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société des Eaux de Trouville Deauville demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les mots " sans les modifier ", figurant au paragraphe IV-A-1-1-a de la notice C3S 2020 publiée par l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur, et " et reporter strictement ", figurant au 1-b du 1 du A du IV figurant en pages 9 et 10 de cette même notice ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La société des Eaux de Trouville Deauville demande l'annulation pour excès de pouvoir de certains termes de la notice explicative intitulée " C3S 2020 " publiée le 10 mars 2020 sur le site internet de l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur, relative aux modalités d'application de la contribution sociale de solidarité des sociétés au titre de l'année 2020. <br/>
<br/>
              Sur l'intervention :<br/>
<br/>
              2. La société des Eaux de Picardie justifie d'un intérêt suffisant pour intervenir au soutien de la requête de la société des Eaux de Trouville Deauville. Son intervention est ainsi recevable.<br/>
<br/>
              Sur la compétence de la juridiction administrative :<br/>
<br/>
              3. Aux termes de l'article L.137-32 du code de la sécurité sociale : " La contribution sociale de solidarité est annuelle. Son fait générateur est constitué par l'existence de l'entreprise débitrice au 1er janvier de l'année au titre de laquelle elle est due. Son taux est fixé à 0,16 %. Elle est assise sur le chiffre d'affaires défini à l'article L. 137-33 réalisé l'année précédant celle au titre de laquelle elle est due, après application d'un abattement égal à 19 millions d'euros. Elle est recouvrée par une union de recouvrement des cotisations de sécurité sociale et d'allocations familiales désignée par le directeur de l'Agence centrale des organismes de sécurité sociale (...) ". Par une décision du 31 octobre 2018, prise en application de ces dispositions, le directeur de l'Agence centrale des organismes de sécurité sociale a désigné l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur pour assurer, à partir du 1er janvier 2019, le recouvrement, le contrôle et le contentieux de la contribution sociale de solidarité des sociétés sur l'ensemble du territoire national. <br/>
<br/>
              4. La " notice " par laquelle l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur indique, à destination des redevables de l'imposition en cause, l'interprétation qu'il convient de retenir des dispositions législatives qui la régissent a la nature d'un acte administratif, alors même que les contentieux individuels auxquels donne lieu l'assujettissement à cette imposition relèvent de la compétence des juridictions de l'ordre judiciaire. Par suite, l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur n'est pas fondée à soutenir que la juridiction administrative serait incompétente pour connaître du présent recours, qui tend à l'annulation pour excès de pouvoir de cette notice.<br/>
<br/>
              Sur les conclusions de la requête :<br/>
<br/>
              5. Les sociétés et entreprises assujetties à la contribution instituée par les dispositions précitées de l'article L.137-32 précité du code de la sécurité sociale sont tenues, aux termes de l'article L. 137-33 du même code, " d'indiquer annuellement à l'organisme chargé du recouvrement de cette contribution le montant de leur chiffre d'affaires global déclaré à l'administration fiscale, calculé hors taxes sur le chiffre d'affaires et taxes assimilées. De ce montant sont déduits, en outre, les droits ou taxes indirects et les taxes intérieures de consommation, versés par ces sociétés et entreprises, grevant les produits médicamenteux et de parfumerie, les boissons, ainsi que les produits pétroliers (...) ".<br/>
<br/>
              6. Après avoir précisé, dans sa sous-partie intitulée " A - Opérations à déclarer " de sa partie " IV - Assiette de la C3S et taux applicables ", que la déclaration prévue à l'art. L. 137-33 du code de la sécurité sociale prenait en principe la forme d'un formulaire électronique pré-rempli par l'administration fiscale lorsque l'entreprise en cause est assujettie à la TVA, la notice litigieuse énonce que : " Si les montants inscrits sont strictement conformes à votre déclaration auprès de l'administration fiscale, vous avez à les valider, sans les modifier. / Si les montants inscrits sont inexacts ou incomplets par rapport à votre déclaration auprès de l'administration fiscale, vous devez les corriger à la hausse ou à la baisse. Dans ce cas, saisissez les montants corrigés dans les cases prévues à cet effet ". Cette même notice précise ensuite que : " En l'absence d'informations fiscales exploitables, le formulaire déclaratif n'est pas pré-rempli. Vous devez déterminer et reporter strictement sur le formulaire déclaratif le chiffre d'affaires, imposé à la TVA ou exonéré, déclaré à l'administration fiscale au cours de l'année civile précédente ". La société des eaux de Trouville Deauville demande l'annulation pour excès de pouvoir des termes " sans les modifier " et " et reporter strictement " figurant respectivement dans ces deux passages.<br/>
<br/>
              7. L'exploitant du service public de l'eau est chargé, pour le compte de l'agence de l'eau, de recouvrer auprès des usagers la redevance pour pollution de l'eau d'origine domestique prévue à l'article L. 213-10-3 du code de l'environnement ainsi que la redevance pour modernisation des réseaux de collecte prévue à l'article L. 213-10-5 du même code. Par ailleurs, en vertu de l'article L. 1611-7-1 du code général des collectivités territoriales, l'autorité délégante peut, après avis conforme du comptable public et par convention écrite, confier à un organisme privé l'encaissement du revenu tiré des prestations assurées dans le cadre d'un contrat portant notamment sur la gestion du service public de l'eau.<br/>
<br/>
              8. La société des eaux de Trouville Deauville soutient qu'en prévoyant l'inclusion dans l'assiette de la cotisation sociale de solidarité des sociétés du chiffre d'affaires global déclaré à l'administration fiscale au titre de la taxe de la valeur ajoutée, calculé hors taxes sur le chiffre d'affaires et taxes assimilées, sans permettre à la personne assujettie de déduire de cette assiette les redevances et les revenus qu'elle a perçus au nom et pour le compte de tiers en application des dispositions des articles L. 213-10-3 et L. 213-10-5 du code de l'environnement ainsi que de l'article L. 1611-7-1 du code général des collectivités territoriales, la notice qu'elle attaque réitèrerait la règle prévue à l'article L. 137-33 du code de la sécurité sociale qui serait elle-même incompatible avec l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Toutefois, les énonciations dont la société des eaux de Trouville Deauville demande l'annulation pour excès de pouvoir se bornent à indiquer que les montants à déclarer au titre de la contribution sociale de solidarité des sociétés sont en principe identiques à ceux portés sur les déclarations souscrites au titre de la taxe sur la valeur ajoutée, la même notice précisant, au demeurant, qu'il convient d'ajouter ou de retrancher au chiffre d'affaire déclaré à l'administration fiscale certaines sommes, dont la liste est donnée de manière explicitement non exhaustive. Par suite, les énonciations attaquées ne prennent pas directement et explicitement position sur la question de savoir si de tels redevances et revenus doivent être inclus ou non dans l'assiette de la cotisation sociale de solidarité des sociétés due par l'exploitant du service public de l'eau. Par suite, le moyen soulevé par la société des eaux de Trouville Deauville ne peut qu'être écarté. <br/>
<br/>
              9. Il en résulte, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir soulevées par l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur, que les conclusions de la société des eaux de Trouville Deauville tendant à l'annulation partielle de la notice litigieuse doivent être rejetées. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société des eaux de Trouville Deauville une somme de 3 000 euros à verser à l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur en application des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société des Eaux de Picardie, qui n'a pas la qualité de partie dans le présent litige. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société des Eaux de Picardie est admise.<br/>
Article 2 : La requête de la société des eaux de Trouville Deauville est rejetée. <br/>
Article 3 : La société des eaux de Trouville Deauville versera à l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées, au titre de l'article L. 761-1 du code de justice administrative, par l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur à l'encontre de la société des Eaux de Picardie sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société des eaux de Trouville Deauville, à l'union de recouvrement des cotisations de sécurité sociale et d'allocations familiales de Provence-Alpes-Côte d'Azur et à la société des Eaux de Picardie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
