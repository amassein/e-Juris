<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029461625</ID>
<ANCIEN_ID>JG_L_2014_09_000000383721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/46/16/CETATEXT000029461625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/09/2014, 383721, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:383721.20140912</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 14 août 2014 au secrétariat du contentieux du Conseil d'État, présentée pour l'Association nationale des opérateurs détaillants en énergie (ANODE), dont le siège est situé 1, boulevard Malesherbes à Paris (75008), représentée par son président en exercice ; l'association requérante demande au juge des référés du Conseil d'État :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 28 juillet 2014 modifiant l'arrêté du <br/>
26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'écologie, du développement durable et de l'énergie et au ministre de l'économie, de l'industrie et du numérique de se prononcer à nouveau sur la fixation des tarifs réglementés de vente de l'électricité dans un délai de quinze jours ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient que : <br/>
              - la condition d'urgence est remplie ;<br/>
              - l'arrêté contesté porte une atteinte grave et immédiate à la situation économique et financière des entreprises membres en affectant substantiellement leur compétitivité sur le marché de l'électricité et en favorisant une situation de quasi-monopole de la société Electricité de France ;<br/>
              - il porte une atteinte directe et avérée à l'intérêt public que constitue l'objectif d'ouverture à la concurrence du marché de l'électricité ;<br/>
              - il conduira en cas d'annulation à l'assujettissement des consommateurs et des fournisseurs nouveaux entrants à des augmentations rétroactives de tarif particulièrement pénalisantes ; <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté du 28 juillet 2014 ;<br/>
              - l'arrêté litigieux est intervenu au terme d'une procédure irrégulière, dans la mesure où la Commission de régulation de l'énergie, qui doit donner un avis motivé sur les projets de mouvement tarifaire en application des dispositions combinées des articles L. 337-4 et L. 134-11 du code de l'énergie, s'est abstenue de prendre position sur le texte qui lui était soumis ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation du niveau des tarifs réglementés de vente d'électricité en ce que le " gel tarifaire " qu'il introduit conduit à méconnaître les éléments d'évaluation des coûts disponibles à la date de son adoption ainsi que l'estimation de l'évolution de ces coûts sur la période tarifaire à venir en fonction des éléments disponibles à cette même date ; <br/>
              - il est entaché d'une erreur de droit au regard du principe d'annualité de la révision des tarifs réglementés de vente d'électricité, prévu par l'article 5 du décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité, en ce qu'il supprime l'augmentation tarifaire prévue à compter du 1er août 2014 ; <br/>
              - il constitue une violation par la France de ses engagements communautaires dans la mesure où il ne respecte pas l'article 2 de la décision du 12 juin 2012 de la Commission européenne subordonnant la compatibilité des dispositifs français d'aides au secteur de l'électricité avec le droit de l'Union européenne à des conditions précises ; <br/>
              - il est contraire au principe de sécurité juridique en ce qu'il procède à la suppression brutale de l'augmentation de 5% des tarifs réglementés de vente d'électricité dans leur composante " bleu ", qui aurait dû s'appliquer à compter du 1er août 2014, sans l'accompagner de mesures transitoires ;<br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
              Vu la copie de la requête à fin annulation de cet arrêté ; <br/>
              Vu le mémoire en défense, enregistré le 28 août 2014, présenté par le ministre de l'économie, de l'industrie et du numérique, qui conclut au rejet de la requête ; <br/>
              il soutient que : <br/>
              - la condition d'urgence n'est pas remplie ;<br/>
              - l'arrêté contesté ne peut être regardé comme plaçant les fournisseurs alternatifs d'électricité dans une situation économique et financière difficilement soutenable du fait de l'effet dit de " ciseau tarifaire " ;<br/>
              - aucune atteinte portée à l'objectif public d'ouverture à la concurrence du marché de l'électricité n'est avérée ;<br/>
              - l'intérêt des consommateurs finaux ne se trouve pas menacé ;<br/>
              - il n'existe pas de doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté litigieux, qui, tout comme les dispositions de l'article 6 de l'arrêté du 26 juillet 2013 qu'il abroge, n'a pour objet la fixation des tarifs réglementés de l'électricité, constitue une mesure conservatoire destinée à permettre de fixer les nouveaux tarifs réglementés en fonction de la nouvelle méthode de calcul qui est en cours d'adoption et en fonction des données les plus récentes, notamment en ce qui concerne les coûts de production d'EDF, qui n'étaient pas encore toutes disponibles à la date du 1er août 2014 ;<br/>
              - la Commission de régulation de l'énergie a rendu un avis motivé sur le projet d'arrêté ;<br/>
              - les ministres compétents pouvaient, sans commettre d'illégalité, procéder à l'annulation de la hausse envisagée des tarifs réglementés de vente de l'électricité " Bleu " au <br/>
1er août 2014 ; <br/>
              - l'arrêté litigieux ne méconnaît pas la règle selon laquelle les tarifs réglementés de vente de l'électricité doivent faire l'objet d'un examen au moins une fois par année civile, comme le prévoit l'article 5 du décret du 12 août 2009 ;<br/>
              - il n'est pas entaché d'une erreur manifeste d'appréciation ;<br/>
              - il ne constitue pas une violation des engagements pris par la France dans le cadre de l'Union européenne ;<br/>
              - il ne porte aucune atteinte au principe de sécurité juridique ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Association nationale des opérateurs détaillants en énergie et, d'autre part, le ministre de l'économie, de l'industrie et du numérique ainsi que la ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 3 septembre 2014 à 10 heures, au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Association nationale des opérateurs détaillants en énergie ;<br/>
<br/>
              - les représentants de l'Association nationale des opérateurs détaillants en énergie ;<br/>
<br/>
              - les représentants du ministre de l'économie, de l'industrie et du numérique ;<br/>
<br/>
              - les représentants de la ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au jeudi 11 septembre à 10 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 5 septembre 2014, présenté pour l'ANODE, qui conclut aux mêmes fins que sa requête par les mêmes moyens et produit des éléments chiffrés à l'appui du moyen tiré de ce que l'arrêté litigieux porte une atteinte grave et immédiate à la situation financière des opérateurs alternatifs ;<br/>
<br/>
              Vu le nouveau mémoire en défense, enregistré le 9 septembre 2014, présenté par le ministre de l'économie, de l'industrie et du numérique, qui conclut au rejet de la requête par les mêmes moyens ; il soutient, en outre, que les éléments fournis par la requérante ne permettent pas de regarder l'atteinte alléguée comme établie ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 11 septembre 2014, présenté pour l'ANODE, qui conclut aux mêmes fins que sa requête par les mêmes moyens ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu le décret n° 2009-975 du 12 août 2009 ;<br/>
<br/>
              Vu l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du second alinéa de l'article L. 337-4 du code de l'énergie : " Pendant une période transitoire s'achevant le 7 décembre 2015, les tarifs réglementés de vente d'électricité sont arrêtés par les ministres chargés de l'énergie et de l'économie, après avis de la Commission de régulation de l'énergie " ; qu'aux termes de l'article L. 337-5 du même code : " Les tarifs réglementés de vente d'électricité sont définis en fonction de catégories fondées sur les caractéristiques intrinsèques des fournitures, en fonction des coûts liés à ces fournitures " ; qu'aux termes du premier alinéa de l'article L. 337-6 : " Dans un délai s'achevant au plus tard le 31 décembre 2015, les tarifs réglementés de vente d'électricité sont progressivement établis en tenant compte de l'addition du prix d'accès régulé à l'électricité nucléaire historique, du coût du complément à la fourniture d'électricité qui inclut la garantie de capacité, des coûts d'acheminement de l'électricité et des coûts de commercialisation ainsi que d'une rémunération normale " ; qu'enfin, aux termes de l'article 3 du décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité : " La part fixe et la part proportionnelle de chaque option ou version tarifaire sont chacune l'addition d'une part correspondant à l'acheminement et d'une part correspondant à la fourniture qui sont établies de manière à couvrir les coûts de production, les coûts d'approvisionnement, les coûts d'utilisation des réseaux publics de transport et de distribution et les coûts de commercialisation, que supportent pour fournir leurs clients Electricité de France et les distributeurs non nationalisés (...), ainsi qu'une marge raisonnable. / La part correspondant à l'acheminement est déterminée en fonction du tarif d'utilisation des réseaux publics en vigueur applicable à l'option ou à la version concernée. La part correspondant à la fourniture couvre les coûts de production, d'approvisionnement et de commercialisation supportés par Electricité de France et les distributeurs non nationalisés pour fournir les clients ayant souscrit à cette option ou version " ;<br/>
<br/>
              2. Considérant que, sur le fondement de ces dispositions, les ministres chargés de l'économie et de l'énergie ont fixé, par arrêté du 26 juillet 2013, les barèmes des tarifs réglementés " bleu ", " jaune " et " vert " de vente de l'électricité ; que l'article 6 de cet arrêté prévoyait que : " (...) Les barèmes du Tarif Bleu, tels qu'annexés, sont augmentés de 5 % en moyenne à compter du 1er août 2014. Ce niveau sera ajusté en fonction de l'évolution effective des coûts sur la période tarifaire concernée " ; que, par l'arrêté litigieux, entré en vigueur le 1er août 2014, et dont la requérante demande la suspension, les ministres ont abrogé ces dernières dispositions ;<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              4. Considérant que, pour justifier de l'urgence à prononcer la suspension demandée, l'association requérante fait valoir que le " gel " des tarifs réglementés de vente de l'électricité au 1er août 2014 résultant de l'arrêté litigieux a pour effet, d'une part, de rendre négative la marge nette des opérateurs alternatifs, de dégrader leur rentabilité et de compromettre de ce fait leur maintien sur le marché de vente de l'électricité, d'autre part, de porter une atteinte grave et immédiate à l'objectif public d'ouverture à la concurrence de ce marché en renforçant la position dominante de l'opérateur historique et, enfin, de porter une atteinte grave et immédiate à l'intérêt des consommateurs, qui devront supporter une hausse rétroactive des tarifs réglementés à la suite de l'annulation de l'arrêté ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction, et notamment des indications fournies par l'administration dans ses écritures et rappelées lors de l'audience publique, que l'arrêté litigieux, qui se borne à supprimer la prévision de hausse moyenne au 1er août 2014 des seuls tarifs " bleus " retenue lors de l'adoption du précédent arrêté tarifaire du 26 juillet 2013, n'a pas pour objet de fixer les tarifs réglementés de vente de l'électricité pour la prochaine période tarifaire mais de permettre la détermination de ces nouveaux tarifs, au plus tard le 31 décembre 2014, en fonction de la nouvelle méthode de calcul qui est sur le point d'être adoptée par décret, de l'analyse des coûts d'EDF la plus récente faite par la Commission de régulation de l'énergie, dont les résultats seront très prochainement disponibles, ainsi que du bilan au 1er octobre 2014 annoncé publiquement par la ministre chargée de l'énergie ; qu'eu égard aux motifs d'intérêt général et au caractère temporaire des effets de l'arrêté contesté, dont les éléments versés au dossier ne permettent d'établir ni qu'il entraînerait une dégradation de marge nette et de rentabilité de nature à compromettre l'activité des opérateurs alternatifs ni qu'il serait susceptible d'affecter durablement la concurrence sur le marché de vente de l'électricité mais dont la suspension serait, en revanche, susceptible de perturber l'application des tarifs réglementés à venir aux consommateurs, plus particulièrement aux consommateurs résidentiels, et compte tenu des délais dans lesquels le Conseil d'Etat devrait statuer sur la requête au fond, l'urgence de la suspension demandée ne peut, en l'espèce, être regardée comme établie ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la requête de l'ANODE doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association nationale des opérateurs détaillants en énergie est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association nationale des opérateurs détaillants en énergie, au ministre de l'économie, de l'industrie et du numérique et à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
