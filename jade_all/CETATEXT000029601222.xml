<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601222</ID>
<ANCIEN_ID>JG_L_2014_10_000000380778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/12/CETATEXT000029601222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 06/10/2014, 380778</TITRE>
<DATE_DEC>2014-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380778.20141006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 mai et 11 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Auboué, représentée par son maire ; la commune d'Auboué demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 13NC02253 du 13 mai 2014 par laquelle le président de la cour administrative d'appel de Nancy a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance n° 1300342 du 24 octobre 2013 par laquelle le juge des référés du tribunal administratif de Nancy a rejeté sa demande tendant à la condamnation solidaire de l'Etat et de la société Sotrae Batilly au paiement de la somme de 520 000 euros à titre de provision à valoir sur les condamnations définitives qui seront mises à leur charge et, d'autre part, à ce qu'il soit fait droit à sa demande de provision, sinon en totalité, du moins partiellement ; <br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société Sotrae Batilly le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de la commune d'Auboué, et à la SCP Odent, Poulet, avocat de la société Sotrae Batilly ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. ( ...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Nancy que des désordres sont apparus sur la voirie de la commune d'Auboué, à la suite de travaux de réhabilitation et de restructuration réalisés pour le syndicat intercommunal des eaux de Soiron ; que, saisi sur le fondement de l'article R. 541-1 du code de justice administrative par la commune d'Auboué, maître d'ouvrage délégué, d'une demande, tendant à la condamnation de l'Etat, en qualité de maître d'oeuvre, et de la société Sotrae Batilly, chargée de la pose de l'assainissement et de l'eau potable, à lui verser une provision d'un montant de 520 000 euros TTC, le juge des référés du tribunal administratif de Nancy a, par une ordonnance du 24 octobre 2013, rejeté la demande de provision de la commune ; que, par une ordonnance du 13 mai 2014, contre laquelle la commune se pourvoit en cassation, le juge des référés de la cour administrative d'appel a confirmé l'ordonnance du 24 octobre 2013 du juge des référés du tribunal administratif de Nancy ;<br/>
<br/>
              3. Considérant que pour regarder une obligation comme non sérieusement contestable, il appartient au juge des référés de s'assurer que les éléments qui lui sont soumis par les parties sont de nature à en établir l'existence avec un degré suffisant de certitude ; qu'il ressort des pièces de la procédure que le juge des référés a adressé le 8 avril 2014 à la commune d'Auboué une mesure d'instruction par laquelle il lui demandait la transmission de plus d'une dizaine de pièces ou séries de pièces ; qu'en relevant que la commune d'Auboué n'avait pas répondu à cette mesure d'instruction et que, dès lors, il n'était pas en mesure de se prononcer avec un degré suffisant de certitude sur l'existence de l'obligation dont se prévalait la commune, quand bien même deux des pièces demandées figuraient au dossier, le juge des référés a ni entaché son ordonnance de dénaturation, ni commis d'erreur de droit ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 611-8-2 du code de justice administrative : " Les avocats, les avocats au Conseil d'Etat et à la Cour de cassation, les administrations de l'Etat, les personnes morales de droit public et les organismes de droit privé chargés de la gestion d'un service public peuvent s'inscrire dans l'application informatique mentionnée à l'article R. 414-1, dans les conditions fixées par l'arrêté prévu à cet article. Toute juridiction peut adresser par le moyen de cette application, à une partie ou à un mandataire ainsi inscrit, toutes les communications et notifications prévues par le présent livre pour tout dossier et l'inviter à produire ses mémoires et ses pièces par le même moyen. Les parties ou leur mandataire sont réputés avoir reçu la communication ou la notification à la date de première consultation du document qui leur a été ainsi adressé, certifiée par l'accusé de réception délivré par l'application informatique, ou, à défaut de consultation dans un délai de huit jours à compter de la date de mise à disposition du document dans l'application, à l'issue de ce délai. Sauf demande contraire de leur part, les parties ou leur mandataire sont alertés de toute nouvelle communication ou notification par un message électronique envoyé à l'adresse choisie par eux. Lorsque le juge est tenu, en application d'une disposition législative ou réglementaire, de statuer dans un délai inférieur ou égal à un mois, la communication ou la notification est réputée reçue dès sa mise à disposition dans l'application " ; qu'il résulte de ces dispositions que l'application informatique dédiée accessible par le réseau internet, mentionnée à l'article R. 414-1 du même code, permet à toute partie ou tout mandataire inscrit de consulter les communications et notifications relatives aux requêtes qu'il a introduites, quelle que soit la forme sous laquelle il les a introduites et quelle que soit la date à laquelle il s'est inscrit à l'application ; qu'il ressort des pièces de la procédure que l'avocat de la commune était inscrit à l'application informatique dédiée à la juridiction, permettant ainsi à la cour, en vertu de l'article R. 611-8-2 du code de justice administrative, de lui adresser les communications et notifications sous une forme dématérialisée par le réseau internet ; que la cour lui a ainsi adressé le 8 avril 2014 un message électronique l'informant que la mesure d'instruction évoquée au point 3 de la présente décision était consultable sous une forme dématérialisée ; qu'il n'est pas établi par les pièces du dossier que des dysfonctionnements auraient empêché l'avocat de la commune d'accéder à cette information ; que, par suite, le juge des référés de la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en considérant implicitement mais nécessairement que la commune devait être réputée avoir reçu communication de la mesure d'instruction dans un délai de huit jours à compter de sa mise à disposition le 8 avril 2014 dans l'application dédiée à la cour, conformément aux dispositions précitées ; que ni ces dispositions ni le principe du caractère contradictoire de la procédure n'imposaient à la cour, compte tenu de l'inscription de l'avocat de la commune à l'application informatique dédiée, de lui communiquer également sous forme non dématérialisée la mesure d'instruction au motif que la requête avait été introduite sous cette forme ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la commune d'Auboué n'est pas fondée à demander l'annulation de l'ordonnance attaquée ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; qu'il y a lieu, en revanche, de mettre à sa charge le versement à la société Sotrae Batilly d'une somme de 3 000 euros au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune d'Auboué est rejeté.<br/>
Article 2 : La commune d'Auboué versera à la société Sotrae Batilly une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Auboué, à la société Sotrae Batilly et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - COMMUNICATION D'UNE MESURE D'INSTRUCTION À UNE PARTIE PAR TÉLÉRECOURS - PARTIE RÉPUTÉE AVOIR REÇU COMMUNICATION DE LA MESURE DANS LES CONDITIONS PRÉVUES PAR L'ARTICLE R. 611-8-2 DU CJA - INCLUSION - COMMUNICATION À UN REQUÉRANT QUI N'AVAIT PAS INTRODUIT SA REQUÊTE SOUS FORME DÉMATÉRIALISÉE.
</SCT>
<ANA ID="9A"> 54-04-03 Il résulte des dispositions de l'article R. 611-8-2 du code de justice administrative (CJA) que l'application informatique dédiée accessible par le réseau internet (télérecours),  mentionnée à l'article R. 414-1 du même code, permet à toute partie ou tout mandataire inscrit de consulter les communications et notifications relatives aux requêtes qu'il a introduites, quelle que soit la forme sous laquelle il les a introduites et quelle que soit la date à laquelle il s'est inscrit à l'application.... ,,Par suite, une cour ne commet pas d'erreur de droit en jugeant que le requérant doit être réputé avoir reçu communication d'une mesure d'instruction dès lors que son avocat était inscrit à l'application informatique dédiée à la juridiction et qu'aucun dysfonctionnement n'est établi, sans qu'y fasse obstacle le fait que le requérant avait introduit sa requête sous forme non dématérialisée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
