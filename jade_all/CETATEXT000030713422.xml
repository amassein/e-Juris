<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713422</ID>
<ANCIEN_ID>JG_L_2015_06_000000376348</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713422.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 08/06/2015, 376348, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376348</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376348.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Bordeaux de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2006, 2007 et 2008. Par un jugement n° 1002268 du 24 avril 2012, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12BX01596 du 14 janvier 2014, la cour administrative d'appel de Bordeaux, après avoir décidé que les revenus de M. A...au titre des années 2006, 2007 et 2008 seraient diminués respectivement des sommes de 14 396 euros, 14 580 euros et 14 390 euros et qu'il serait déchargé de la différence entre l'impôt sur le revenu auquel il a été assujetti au titre des trois années en litige et celui qui résulte de ces  diminutions et avoir réformé dans cette mesure ce jugement, a rejeté le surplus des conclusions de la requête. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 mars et 13 juin 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 5 de cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a déduit de ses salaires imposables les suppléments de rémunération qu'il a perçus au cours des années 2006, 2007 et 2008 au titre des missions qu'il a effectuées à l'étranger dans le cadre de son activité salariée ; qu'à la suite d'un contrôle sur pièces de ses déclarations de revenus, l'administration fiscale a réintégré dans ses bases d'imposition les montants correspondants à ces suppléments de rémunération au motif qu'ils ne répondaient pas aux conditions fixées au II de l'article 81 A du code général des impôts ; que M. A...se pourvoit en cassation contre l'article 5 de l'arrêt du 14 janvier 2014 par lequel la cour administrative d'appel de Bordeaux, après avoir réformé partiellement le jugement du 24 avril 2012 du tribunal administratif de Bordeaux et prononcé la décharge d'une quote-part des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2006 à 2008, a rejeté le surplus des conclusions de sa requête ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 81 A du code général des impôts, dans sa rédaction applicable aux impositions litigieuses : " I. - Les personnes domiciliées en France au sens de l'article 4 B qui exercent une activité salariée et sont envoyées par un employeur dans un Etat autre que la France et que celui du lieu d'établissement de cet employeur peuvent bénéficier d'une exonération d'impôt sur le revenu à raison des salaires perçus en rémunération de l'activité exercée dans l'Etat où elles sont envoyées. / (...) II. - Lorsque les personnes mentionnées au premier alinéa du I ne remplissent pas les conditions définies aux 1° et 2° du même I, les suppléments de rémunération qui leur sont éventuellement versés au titre de leur séjour dans un autre État sont exonérés d'impôt sur le revenu en France s'ils réunissent les conditions suivantes : / (...) 3° Être déterminés dans leur montant préalablement aux séjours dans un autre État et en rapport, d'une part, avec le nombre, la durée et le lieu de ces séjours et, d'autre part, avec la rémunération versée aux salariés compte non tenu des suppléments mentionnés au premier alinéa. Le montant des suppléments de rémunération ne peut pas excéder 40 % de celui de la rémunération précédemment définie " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions, en particulier des termes de la seconde phrase du 3° du II, rapprochés de ceux de la première phrase du même 3°, que le législateur a entendu, d'une part, subordonner le bénéfice de l'exonération d'impôt sur le revenu des suppléments de rémunération versés à un salarié envoyé par son employeur à l'étranger à des conditions tenant, notamment, à ce que le montant de ces suppléments soit déterminé préalablement et en rapport avec le nombre, la durée et le lieu de ses séjours hors de France, d'autre part, ces conditions étant remplies, limiter le montant du revenu pouvant être exonéré pendant la période d'imposition à 40 % de la rémunération, laquelle doit ainsi s'entendre comme correspondant au montant global de la rémunération hors suppléments versée au salarié pendant cette période, et non à celui de la seule rémunération perçue pendant la durée des séjours hors de France donnant lieu au versement de ces suppléments ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Bordeaux a commis une erreur de droit en jugeant que le montant de la rémunération servant de base au calcul du plafond de 40 % était celui de la rémunération qu'aurait normalement perçue M. A...au cours des périodes pendant lesquelles il avait effectué des déplacements à l'étranger ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi,  l'article 5 de son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que les rémunérations supplémentaires perçues à ce titre atteignaient 27 224 euros en 2006, 30 260 euros en 2007 et 26 442 euros en 2008 ; qu'il  n'est pas contesté par l'administration qu'elles ont été déterminées préalablement aux déplacements en fonction du nombre, de la durée et du lieu des séjours hors de France de M. A... ; qu'elles sont inférieures à 40 % des rémunérations annuelles, hors suppléments de rémunération, versées à M. A...au cours des années 2006, 2007  et 2008 ;  que, par suite, elles bénéficient d'une exonération d'impôt sur le revenu en application des dispositions du II de l'article 81 A du code général des impôts citées au point 1 ; que, dès lors, M. A...est fondé à demander la décharge des impositions supplémentaires restant à sa charge au titre de ces trois années  à concurrence des rehaussements portant sur les traitements et salaires ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, M. A...est  fondé à soutenir que c'est à tort que le tribunal administratif de Bordeaux a rejeté le surplus de ses conclusions ;  <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 4 000 euros à verser à M. A..., en application de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par lui pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                        --------------<br/>
<br/>
Article 1er : L'article 5 de l'arrêt du 14 janvier 2014 de la cour administrative d'appel de Bordeaux et le jugement du 24 avril 2012 du tribunal administratif de Bordeaux sont annulés.<br/>
<br/>
Article 2 : M. A...est déchargé des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2006, 2007 et 2008 à concurrence des rehaussements portant sur les traitements et salaires et des pénalités correspondantes. <br/>
<br/>
Article 3 : L'Etat versera à M. A...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
