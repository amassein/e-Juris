<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783342</ID>
<ANCIEN_ID>JG_L_2018_12_000000412905</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783342.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/12/2018, 412905, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412905</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412905.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir l'arrêté du 24 mars 2014 par lequel le maire de la commune de Lucé (Eure-et-Loir) l'a radiée des cadres de la commune pour abandon de poste. <br/>
<br/>
              Par un jugement n° 1403368 du 10 mars 2015, le tribunal administratif d'Orléans a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15NT02963 du 24 mai 2017, la cour administrative d'appel de Nantes a annulé ce jugement ainsi que l'arrêté du 24 mars 2014.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 31 juillet 2017 et 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Lucé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MmeA... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Lucé et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., assistante d'enseignement artistique employée par la commune de Lucé (Eure-et-Loir) au sein du conservatoire municipal en tant que professeur de piano, a été placée en congé de longue durée jusqu'au 15 mai 2013 puis, après une mise en demeure de reprendre son poste en date du 16 septembre 2013, en congé de maladie ordinaire du 19 septembre 2013 au 20 février 2014. En réponse à la lettre du 5 février 2014 par laquelle la commune de Lucé l'a informée qu'une nouvelle prolongation de son arrêt de travail impliquerait la saisine du comité médical et lui a demandé de lui préciser si son état de santé était susceptible de relever, selon son médecin traitant, d'un congé de longue maladie ou de longue durée, Mme A...a adressé à la commune de Lucé, le 20 février 2014, un pli contenant une demande de congé de longue maladie accompagnée d'un certificat médical attestant que son état de santé justifiait l'attribution d'un tel congé. Toutefois, ce pli a été refusé par la commune de Lucé en raison d'un affranchissement insuffisant et a été réexpédié par les services de La Poste à MmeA..., qui l'a reçu le 22 mars 2014. Par lettre en date du 17 mars 2014, reçue le 20 mars 2014, le maire de Lucé a mis en demeure Mme A...de reprendre ses fonctions le lundi 24 mars 2014 en précisant qu'à défaut d'une telle reprise, elle s'exposait à une radiation des cadres sans qu'il soit nécessaire d'engager une procédure disciplinaire à son encontre. Par un arrêté du 24 mars 2014, Mme A... a été radiée des cadres pour abandon de poste. Par un jugement du 10 mars 2015, le tribunal administratif d'Orléans a rejeté la demande d'annulation de cet arrêté présentée par MmeA.... La cour administrative d'appel de Nantes a annulé ce jugement ainsi que l'arrêté du 24 mars 2014. La commune de Lucé se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. Aux termes de l'article 57 de la loi du 26 janvier 1984 : " Le fonctionnaire en activité a droit :/ (...) 3° A des congés de longue maladie d'une durée maximale de trois ans dans les cas où il est constaté que la maladie met l'intéressé dans l'impossibilité d'exercer ses fonctions, rend nécessaires un traitement et des soins prolongés et présente un caractère invalidant et de gravité confirmée (...) ". Aux termes de l'article 4 du décret du 30 juillet 1987 : " Le comité médical (...)  est consulté obligatoirement pour : (...) b) L'octroi et le renouvellement des congés de longue maladie (...) ".<br/>
<br/>
              3. Une mesure de radiation de cadres pour abandon de poste ne peut être régulièrement prononcée que si l'agent concerné a, préalablement à cette décision, été mis en demeure de rejoindre son poste ou de reprendre son service dans un délai approprié qu'il appartient à l'administration de fixer. Une telle mise en demeure doit prendre la forme d'un document écrit, notifié à l'intéressé, l'informant du risque qu'il court d'une radiation de cadres sans procédure disciplinaire préalable. Lorsque l'agent ne s'est ni présenté ni n'a fait connaître à l'administration aucune intention avant l'expiration du délai fixé par la mise en demeure, et en l'absence de toute justification d'ordre matériel ou médical, présentée par l'agent, de nature à expliquer le retard qu'il aurait eu à manifester un lien avec le service, cette administration est en droit d'estimer que le lien avec le service a été rompu du fait de l'intéressé.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... ne s'est pas présentée et n'a pas fait connaître à la commune de Lucé ses intentions avant l'expiration du délai fixé par la mise en demeure, reçue le 20 mars 2014, l'invitant à reprendre ses fonctions le 24 mars 2014 et lui précisant qu'à défaut d'une telle reprise, elle s'exposait à une mesure de radiation des cadres. La circonstance que l'intéressée a reçu, le samedi 22 mars 2014, la réexpédition par La Poste de son courrier demandant à la commune son placement en congé de longue maladie est sans incidence sur la détermination du point de départ du délai qui lui avait été fixé pour reprendre ses fonctions. Par suite, en se fondant pour annuler l'arrêté du 24 mars 2014 sur le motif inopérant selon lequel, jusqu'au 22 mars 2014, l'intéressée pouvait penser que sa demande de congé de longue maladie était en cours d'examen par l'administration et qu'elle n'avait ainsi disposé que du samedi 22 mars 2014 et du dimanche 23 mars 2014, jours non ouvrables, pour prendre réellement conscience du risque couru en ne se présentant pas à son poste le lundi 24 mars 2014 au matin, la cour a commis une erreur de droit. Par suite, la commune de Lucé est fondée à demander, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Lucé au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Lucé, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 mai 2017 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par la commune de Lucé et par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Lucé et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
