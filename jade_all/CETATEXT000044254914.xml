<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044254914</ID>
<ANCIEN_ID>JG_L_2021_10_000000456917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/25/49/CETATEXT000044254914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/10/2021, 456917, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456917.20211012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              I. Sous le n° 456917, par une requête enregistrée le 21 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société YS Group demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-1059 du 7 août 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable compte tenu de son intérêt à agir en sa qualité de société de restauration commerciale soumise à l'obligation de contrôler le " passe sanitaire " de son personnel et de ses clients ;<br/>
              - la condition d'urgence est satisfaite dès lors que le contrôle des " passes sanitaires " est d'ores et déjà en vigueur dans les restaurants, si bien qu'elle se trouve actuellement dans l'obligation de refuser des clients sous peine de sanctions pénales ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué en ce qu'il exige un " passe sanitaire " dans les restaurants ;<br/>
              - le décret est entaché de détournement de pouvoir en ce qu'il a été édicté, sous couvert de lutte contre l'épidémie, dans le but de contraindre indirectement à la vaccination ; <br/>
              - il méconnaît le principe " non bis in idem " qui découle du principe de nécessité des peines ;<br/>
              - il porte une atteinte grave, disproportionnée et donc manifestement illégale à la liberté d'aller et venir, au droit au respect de la vie privée, au droit à la vie et au droit de disposer de son corps, compte tenu du faible risque de mortalité résultant de l'exposition au virus, de la stabilité du taux de reproduction de celui-ci autour de 1, de la dangerosité des vaccins autorisés, de la possibilité de recourir à des mesures moins restrictives telles que le télétravail, le port de masques FFP2 et la mise en quarantaine aux frontières, enfin de la gravité de la discrimination qu'il induit entre les personnes vaccinées et les personnes non vaccinées, notamment celles ayant déjà été contaminées ou, subsidiairement, celles ayant reçu un vaccin autre que ceux reconnus par les autorités sanitaires françaises.<br/>
<br/>
<br/>
              II. Sous le n° 456918, par une requête enregistrée le 21 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société YS Group demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-1059 du 7 août 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soulève les mêmes moyens que sous le n° 456917.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - la loi n° 2021-1040 du 5 août 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Par deux requêtes, qu'il y a lieu de joindre pour statuer par une seule ordonnance, la société YS Group demande au juge des référés du Conseil d'Etat, statuant sur le fondement des dispositions citées ci-dessus, de suspendre l'exécution du décret du 7 août 2021 modifiant le décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de la crise sanitaire.<br/>
<br/>
              3. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus de la Covid-19 prises dans le cadre de l'état d'urgence sanitaire ont été remplacées, après l'expiration de celui-ci le 1er juin 2021, par celles de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire. Cette loi a notamment permis au Premier ministre, dans l'intérêt de la santé publique et afin de lutter contre la propagation de l'épidémie, de subordonner à la présentation soit du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid-19, soit d'un justificatif de statut vaccinal concernant la Covid-19, soit d'un certificat de rétablissement à la suite d'une contamination par la Covid-19, l'accès à certains lieux, établissements ou évènements impliquant de grands rassemblements de personnes pour des activités de loisirs, des foires ou des salons professionnels. Par un décret du 7 juin 2021, le Premier ministre a défini les règles relatives à l'établissement et au contrôle de ce document dénommé " passe sanitaire ". Mais après une diminution de l'épidémie, la situation sanitaire s'est dégradée du fait de la diffusion croissante du variant Delta qui présente une transmissibilité augmentée de 60 % par rapport au variant Alpha, avec une sévérité au moins aussi importante. Au regard de l'évolution de la situation épidémiologique, la loi du 31 mai 2021 a été modifiée et complétée par la loi du 5 août 2021 relative à la gestion de la crise sanitaire, afin de permettre au Premier ministre d'étendre le champ de l'obligation de présentation d'un " passe sanitaire ". Le décret contesté, en date du 7 août 2021, a été pris pour l'application de cette dernière loi.<br/>
<br/>
              4. En premier lieu, ainsi que l'a d'ailleurs relevé le Conseil constitutionnel dans sa décision n° 2021-824 DC du 5 août 2021, l'obligation de présenter un " passe sanitaire " pour l'accès à certains lieux ou établissements ne saurait être regardée comme constituant une obligation de vaccination ni comme ayant un effet équivalent. Par suite, sont inopérants les moyens tirés de ce qu'en exigeant un tel document, la loi du 31 mai 2021 modifiée par la loi du 5 août 2021 ainsi que, par voie de conséquence, le décret contesté du 7 août 2021 pris pour son application porteraient une atteinte disproportionnée au droit à la vie et au droit de disposer de son corps en exposant les personnes à un risque disproportionné au regard des bénéfices attendus de la vaccination, de la situation épidémiologique et de la possibilité de recourir à d'autres mesures. <br/>
<br/>
              5. En deuxième lieu, d'une part, la loi du 31 mai 2021 modifiée a subordonné à la présentation d'un " passe sanitaire " l'accès à certains lieux mettant en présence un nombre important de personnes pour tenir compte du risque élevé de contamination résultant du variant Delta, sans limiter l'accès aux soins ni aux produits de première nécessité. D'autre part, il résulte de cette même loi ainsi que du décret contesté, pris pour son application, que l'obligation de présenter un " passe sanitaire " pour l'accès aux restaurants peut être satisfaite par la présentation soit du résultat d'un examen de dépistage virologique, soit d'un justificatif de statut vaccinal, soit d'un certificat de rétablissement. Dès lors, les moyens tirés, d'une part, d'une atteinte excessive à la liberté d'aller et venir et au droit au respect de la vie privée et, d'autre part, d'une discrimination injustifiée entre les personnes selon qu'elles sont ou non vaccinées et selon le vaccin qu'elles ont reçu, ne sont pas propres à créer, en l'état de l'instruction, un doute sérieux sur la légalité du décret. <br/>
<br/>
              6. En troisième lieu, les sanctions pénales encourues en cas de méconnaissance de l'obligation de présenter un " passe sanitaire " pour l'accès à des établissements tels que les restaurants ayant été prévues par le législateur à des fins de protection de la santé publique, le moyen tiré de ce que le décret contesté méconnaîtrait le principe de nécessité des peines ne saurait, en l'état de l'instruction, être regardé comme créant un doute sérieux sur la légalité du décret.<br/>
<br/>
              7. Enfin, le détournement de pouvoir allégué, qui consisterait à rendre le " passe sanitaire " obligatoire sous peine de sanction pénale pour l'accès à des établissements tels que les restaurants dans le seul but de contraindre indirectement les personnes à se faire vacciner, ne saurait non plus être regardé comme un moyen propre à créer un doute sérieux sur la légalité du décret.<br/>
<br/>
              8. Il résulte de tout ce qui précède qu'il apparait manifeste, en l'état de l'instruction, qu'aucun des moyens soulevés n'est de nature à caractériser une atteinte manifestement illégale aux libertés fondamentales ni à créer un doute sérieux sur la légalité du décret contesté. Dès lors, sans qu'il soit besoin de se prononcer sur la condition d'urgence prévue par les dispositions citées au point 1, les requêtes de la société YS Group tendant à la suspension de l'exécution de ce décret ne peuvent qu'être rejetées, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Les requêtes de la société YS Group sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à la Sarl YS Group.<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
