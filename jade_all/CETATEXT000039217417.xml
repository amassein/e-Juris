<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217417</ID>
<ANCIEN_ID>JG_L_2019_10_000000416568</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217417.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/10/2019, 416568, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416568</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416568.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Pau d'annuler pour excès de pouvoir la décision du 14 février 2014 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, d'une part, annulé la décision du 31 juillet 2013 de l'inspecteur du travail de la 8ème section de l'unité territoriale des Pyrénées-Atlantiques refusant à la société Triangle Propreté l'autorisation de la licencier et, d'autre part, accordé cette autorisation. Par un jugement n° 1401097 du 13 octobre 2015, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 15BX03959 du 16 octobre 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Triangle Propreté contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 décembre 2017 et 13 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Triangle Propreté demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Triangle Propreté et à la SCP Boulloche, avocat de Mme B... A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 31 juillet 2013, l'inspecteur du travail de la 8e section des Pyrénées-Atlantiques a refusé d'autoriser la société Triangle Propreté à licencier, pour motif disciplinaire, Mme A..., salariée protégée. Par une décision du 14 février 2014, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé cette décision et accordé l'autorisation de licenciement sollicitée. Par un jugement du 13 octobre 2015, le tribunal administratif de Pau a annulé la décision du 14 février 2014. La société Triangle Propreté se pourvoit en cassation contre l'arrêt du 16 octobre 2017 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel contre ce jugement.<br/>
<br/>
              2. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle. Lorsque le licenciement de l'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi.<br/>
<br/>
              3. Pour rejeter la requête de la société Triangle Propreté, la cour s'est fondée sur ce que la décision du ministre du travail était, en ce qu'elle retient que le licenciement de Mme A... était sans rapport avec ses mandats, entachée d'un " défaut d'examen ", le ministre n'ayant pas pris connaissance des lettres de licenciement d'autres salariés, alors qu'il se fondait sur ces autres licenciements pour estimer que le licenciement de Mme A... n'était pas discriminatoire. En statuant ainsi, alors que, dans ses écritures, Mme A... se bornait à soutenir sur ce point, d'une part, que la procédure suivie par le ministre du travail n'avait pas été contradictoire, faute pour le ministre de l'avoir mise à même de prendre connaissance de pièces de nature à établir qu'un tel lien n'était pas établi, et d'autre part, que le ministre avait inexactement apprécié les faits qui lui étaient soumis en estimant que ce lien n'était pas établi, la cour s'est méprise sur la portée des écritures de Mme A.... <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Triangle Propreté est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Triangle Propreté, qui n'est pas la partie perdante dans la présente instance, la somme demandée au titre de ces dispositions. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Triangle Propreté au même titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 16 octobre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 4 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Triangle Propreté et à Mme B... A....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
