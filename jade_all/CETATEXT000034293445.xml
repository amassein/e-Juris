<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034293445</ID>
<ANCIEN_ID>JG_L_2017_03_000000388235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/29/34/CETATEXT000034293445.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 27/03/2017, 388235, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:388235.20170327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris, d'une part, d'annuler la décision du 26 octobre 2012 par laquelle l'Agence pour l'enseignement français à l'étranger (AEFE) a refusé son recrutement et son affectation au lycée franco-libanais Nahr-Ibrahim et, d'autre part, la condamnation de l'AEFE à lui verser la somme de 203 655,48 euros en réparation des préjudices qu'elle estime avoir subis. Par un jugement n° 1220749,1304187 du 23 janvier 2014, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14PA01427 du 31 décembre 2014, la cour administrative d'appel de Paris a, sur appel de MmeA..., annulé ce jugement, condamné l'AEFE à lui verser la somme de 22 949,70 euros avec intérêts au taux légal à compter du 8 mars 2013 et capitalisation de ceux-ci et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par une requête sommaire et deux mémoires complémentaires, enregistrés les 23 février 2015, 26 mai 2015 et 21 avril 2016 au secrétariat du contentieux du Conseil d'Etat, l'AEFE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il fait droit aux conclusions de Mme A...;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A...;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -  le code de l'éducation ;<br/>
              - le décret n° 2002-22 du 4 janvier 2002 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de l'Agence pour l'enseignement français à l'étranger et à la SCP Coutard, Munier-Apaire, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 452-5 du code de l'éducation, l'Agence pour l'enseignement français à l'étranger (AEFE) assure " au bénéfice de l'ensemble des établissements scolaires participant à l'enseignement français à l'étranger : (...)2° Le choix, l'affectation, la gestion des agents titulaires de la fonction publique placés en détachement auprès d'elle, après avis des commissions consultatives paritaires compétentes, et également l'application des régimes de rémunération de ces personnels (...) " ; qu'aux termes de l'article 2 du décret du 4 janvier 2002 relatif à la situation administrative et financière des personnels des établissements d'enseignement français à l'étranger : " Les personnels expatriés sont recrutés par l'agence, après avis de la commission consultative paritaire centrale compétente, hors du pays d'affectation, sur des postes dont la liste limitative est fixée chaque année par le directeur de l'agence. / Les personnels résidents après avis de la commission consultative paritaire locale compétente de l'agence quand elle existe sont recrutés par l'agence sur proposition du chef d'établissement. / Sont considérés comme personnels résidents les fonctionnaires établis dans le pays depuis trois mois au moins à la date d'effet du contrat " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., agent du département de la Seine-Saint-Denis, a présenté sa candidature à un poste de chef des services administratif et financier du lycée franco-libanais Nahr-Ibrahim d'Al Maayssra au Liban qui est un établissement conventionné par l'AEFE et géré par la Mission laïque française ; que sa candidature ayant été retenue, il a été convenu qu'elle serait placée en position de disponibilité personnelle et prendrait ce poste sous couvert d'un contrat local, puis, passé un délai de trois mois, qu'elle serait détachée auprès de l'AEFE en qualité de personnel résident ; que MmeA..., bien qu'elle n'ait pu obtenir auprès de l'AEFE des informations précises sur les conditions financières de cette prise de poste, a néanmoins pris ses fonctions à la date prévue ; que s'étant vue proposer un contrat local qui n'était pas conforme à ses attentes, elle ne l'a pas signé et a quitté le Liban ; que l'AEFE a alors refusé de procéder à son recrutement en qualité de personnel résident ;<br/>
<br/>
              3. Considérant que par un arrêt du 31 décembre 2014, la cour administrative d'appel de Paris a condamné l'AEFE à verser à MmeA..., au titre des préjudices subis par elle du fait des retards dans la production des renseignements qu'elle avait sollicités et des fautes commises par l'agence dans l'exercice de la mission générale qui lui est confiée par l'article L. 452-5 du code de l'éducation, la somme de 22 949,70 euros avec intérêts au taux légal et capitalisation de ceux-ci ; que l'AEFE se pourvoit en cassation contre cet arrêt en tant qu'il l'a condamnée à payer cette somme ; que, par la voie du pourvoi incident, Mme A...en demande l'annulation en tant qu'il a rejeté le surplus de ses conclusions tendant au versement d'une indemnité plus élevée ;<br/>
<br/>
              Sur le pourvoi principal de l'AEFE :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier que le mémoire en réplique présenté par Mme A...le 10 décembre 2014 ne comportait aucun élément nouveau ; que, par suite, l'AEFE n'est pas fondée à soutenir que sa communication par cour administrative d'appel aurait, en raison de la brièveté du délai qui lui était laissé pour y répondre, méconnu le caractère contradictoire de la procédure ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis au juge du fond que, ainsi qu'il a été dit au point 2, l'AEFE s'était engagée à ce que Mme A... prenne le poste vacant au lycée franco-libanais sous couvert d'un contrat local puis que, passé un délai de trois mois, elle serait détachée auprès de l'agence en qualité de personnel résident ; que, dans ces conditions, au regard de la mission globale de recrutement qui incombait en l'espèce à l'AEFE à l'égard de MmeA..., en vertu de l'article L. 452-5 du code de l'éducation, la cour administrative d'appel a pu juger, sans erreur de droit, que la circonstance que Mme A...doive, lors de ses trois premiers mois de fonctions, bénéficier d'un contrat local souscrit avec la Mission laïque française, ne pouvait exonérer l'AEFE de toute responsabilité dans le préjudice subi par l'intéressée ; <br/>
<br/>
              6. Considérant, enfin, qu'en estimant que les demandes, réitérées de Mme A... auprès de l'AEFE tendant, avant sa prise de poste au Liban, à l'obtention d'éléments d'information, notamment financiers, sur les conditions de sa prise en charge, soit étaient restées sans réponse, soit avaient donné lieu à des réponses parcellaires ou erronées, la cour administrative d'appel n'a pas dénaturé les pièces du dossier ; qu'elle a exactement qualifié ces faits en jugeant qu'ils étaient fautifs et de nature à engager la responsabilité de l'AEFE à l'égard de MmeA... ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'AEFE n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque, lequel est suffisamment motivé ; que son pourvoi doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Sur le pourvoi incident de Mme A...:<br/>
<br/>
              En ce qui concerne l'arrêt en tant qu'il a statué sur sa responsabilité :<br/>
<br/>
              8. Considérant que la cour administrative d'appel a pu, sans dénaturer les faits qui lui étaient soumis, estimer que, le choix de Mme A...de prendre tout de même ses fonctions au Liban malgré la persistance d'incertitudes sur les éléments de sa rémunération était constitutif d'une imprudence de sa part ; qu'en jugeant qu'une telle imprudence était constitutive d'une faute de nature à exonérer partiellement l'AEFE de sa responsabilité, la cour administrative d'appel n'a pas inexactement qualifié les faits ; qu'enfin, en estimant à 10 % la part de cette exonération, elle n'a pas davantage entaché son arrêt de dénaturation ; <br/>
<br/>
              En ce qui concerne l'arrêt en tant qu'il a statué sur son préjudice :<br/>
<br/>
              9. Considérant qu'en retenant que les préjudices invoqués par Mme A...relatifs, d'une part, à la perte de la rémunération qu'elle aurait dû percevoir pendant toute la durée de sa présence au Liban telle qu'elle l'envisageait initialement et, d'autre part, à sa perte de rémunération entre son départ du Liban jusqu'à sa nouvelle affectation, ne présentaient pas un caractère suffisamment certain pour le premier et n'étaient pas établis pour le second, la cour administrative d'appel a porté sur les pièces du dossier une appréciation souveraine exempte de dénaturation et n'a pas commis d'erreur de droit ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que le pourvoi incident de Mme A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Agence pour l'enseignement français à l'étranger est rejeté.<br/>
Article 2 : Le pourvoi incident de Mme A...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à l'Agence pour l'enseignement français à l'étranger et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
