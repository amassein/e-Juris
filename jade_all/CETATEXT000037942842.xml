<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037942842</ID>
<ANCIEN_ID>JG_L_2018_12_000000408922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/94/28/CETATEXT000037942842.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 31/12/2018, 408922, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408922.20181231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 mars et 15 juin 2017 et le 3 mai 2018 au secrétariat du contentieux du Conseil d'Etat, l'Union des syndicats de l'immobilier (UNIS), le Syndicat national des professionnels immobiliers (SNPI) et la Fédération nationale de l'immobilier (FNAIM) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, l'arrêté du ministre de l'économie et des finances et de la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire du 10 janvier 2017 relatif à l'information des consommateurs par les professionnels intervenant dans une transaction immobilière ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la consommation ;<br/>
              - la loi n° 70-9 du 2 janvier 1970 ;<br/>
              - le décret n° 72-678 du 20 juillet 1972 ;<br/>
              - le décret n° 2015-819 du 6 juillet 2015 ;<br/>
              - la circulaire du Premier ministre du 23 mai 2011 relative aux dates communes d'entrée en vigueur des normes concernant les entreprises ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Carbonnier, avocat de l'Union des syndicats de l'immobilier et autres. <br/>
<br/>
<br/>
<br/>
<br/>
              1. Les requérants demandent l'annulation pour excès de pouvoir de l'arrêté du ministre de l'économie et des finances et de la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire du 10 janvier 2017 relatif à l'information des consommateurs par les professionnels intervenant dans une transaction immobilière.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 112-1 du code de la consommation, pour l'application duquel est pris l'arrêté attaqué : " Tout vendeur de produit ou tout prestataire de services informe le consommateur, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, sur les prix et les conditions particulières de la vente et de l'exécution des services, selon des modalités fixées par arrêtés du ministre chargé de l'économie, après consultation du Conseil national de la consommation ". Le 3° de l'article 1er du décret du 6 juillet 2015 relatif aux attributions déléguées à la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire prévoit que celle-ci traite notamment, par délégation du ministre de l'économie, de l'industrie et du numérique, les questions relatives " aux droits des consommateurs, en veillant notamment à la loyauté des relations entre les consommateurs et les professionnels (...) ". Par suite, le moyen tiré de ce que l'arrêté attaqué serait entaché d'incompétence au motif que le ministre chargé de l'économie était seul compétent pour l'édicter doit, en tout état de cause, être écarté.<br/>
<br/>
              3. En deuxième lieu, l'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'un texte doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par ce texte. Par suite, dans le cas où, après avoir recueilli son avis, l'autorité compétente pour prendre le texte envisage d'apporter à son projet des modifications, elle ne doit procéder à une nouvelle consultation de cet organisme que si ces modifications posent des questions nouvelles. Il ressort des pièces du dossier que, conformément aux dispositions de l'article 13-1 de la loi du 2 janvier 1970 réglementant les conditions d'exercice des activités relatives à certaines opérations portant sur les immeubles et les fonds de commerce, dans sa version applicable, l'arrêté attaqué a été pris après consultation du Conseil national de la transaction et de la gestion immobilières. Postérieurement à cette consultation, le projet a fait l'objet, d'une part, de modifications rédactionnelles ou de portée mineure, et d'autre part, d'une modification tendant à reporter de trois mois la date d'entrée en vigueur de l'arrêté. Eu égard à la portée de ces modifications, qui ne posaient pas de questions nouvelles, et alors au surplus qu'il ressort des écritures des requérants que le report de l'entrée en vigueur de l'arrêté avait été demandé par les membres du conseil, l'avis de ce dernier ne peut être regardé comme ayant été rendu au terme d'une procédure irrégulière.    <br/>
<br/>
              4. En troisième lieu, eu égard à la portée de l'arrêté litigieux, qui vise à moderniser les dispositions relatives aux modalités d'affichage des annonces immobilières et à préciser ou compléter les informations que les professionnels doivent fournir aux consommateurs, ses auteurs n'ont pas commis d'erreur manifeste d'appréciation en accordant aux professionnels concernés un délai inférieur à trois mois à compter de la publication de l'arrêté pour se conformer aux obligations qu'il prévoit.<br/>
<br/>
              5. En quatrième lieu, aux termes du I de l'article 2 de l'arrêté attaqué : " Les professionnels visés à l'article 1er sont tenus d'afficher les prix effectivement pratiqués des prestations qu'ils assurent, notamment celles liées à la vente, à la location de biens et à la gestion immobilière, en indiquant pour chacune de ces prestations à qui incombe le paiement de cette rémunération ". Ces dispositions qui, au demeurant, se bornent à reprendre celles de l'article 2 de l'arrêté du 29 juin 1990 relatif à la publicité des prix pratiqués par des professionnels intervenant dans les transactions immobilières abrogé par l'arrêté attaqué, visent à informer les consommateurs sur la politique générale du professionnel quant à la détermination de sa rémunération et sur la répartition de la charge qui en résulte entre les parties, et n'ont pas pour effet de lui interdire d'y déroger au cas par cas. Par suite, et en tout état de cause, le moyen tiré de la méconnaissance de la liberté contractuelle et de la liberté du commerce et de l'industrie doit être écarté.<br/>
<br/>
              6. En cinquième lieu, il résulte des dispositions combinées de l'article 1er de l'arrêté attaqué, qui prévoit que ses dispositions " s'appliquent à tout professionnel qui, à quelque titre que ce soit, intervient pour mettre en relation acquéreurs ou locataires et vendeurs ou bailleurs de biens immobiliers " et ne s'appliquent pas " aux personnes physiques ou morales qui interviennent en tant que simples supports des annonces immobilières ", et du I de son article 2, citées au point précédent, que les obligations posées à l'article 2 sont mises à la charge de tous les professionnels visés à l'article 1er pour l'ensemble des prestations qu'ils assurent dans le cadre de la mise en relation des parties qui désirent conclure une transaction immobilière. Par suite, le moyen tiré de l'erreur manifeste d'appréciation et de la méconnaissance de l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme doit être écarté.<br/>
<br/>
              7. En sixième lieu, les dispositions de l'article 3 de l'arrêté attaqué relatives aux mentions obligatoires pour toute publicité relative à la vente d'un bien déterminé, qui imposent de mentionner un prix de vente du bien comprenant, le cas échéant, la part des honoraires du professionnel à la charge de l'acquéreur, exprimé honoraires inclus et exclus, et qui proscrivent l'inclusion dans ce prix des honoraires à la charge du vendeur, ont pour seul objet d'informer l'acquéreur potentiel sur le montant dont il devra s'acquitter auprès du vendeur et du professionnel immobilier pour cette transaction. Par suite, le moyen tiré de ce que ces dispositions seraient entachées d'erreur manifeste d'appréciation au motif qu'elles seraient de nature à induire l'acquéreur en erreur sur le montant des droits de mutation à titre onéreux dont il devra s'acquitter au titre de cette transaction doit être écarté. <br/>
<br/>
              8. En septième lieu, l'article L. 112-1 du code de la consommation, pour l'application duquel est pris l'arrêté attaqué, concerne les relations entre professionnels et consommateurs. Par suite, le moyen tiré de ce que l'arrêté attaqué serait entaché d'erreur manifeste d'appréciation au motif qu'il ne met pas d'obligations à la charge des particuliers qui mettent en location leur bien immobilier sans recourir aux services d'un intermédiaire professionnel doit être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède que l'Union des syndicats de l'immobilier et autres ne sont pas fondés à demander l'annulation de l'arrêté qu'ils attaquent. Leur requête doit, par suite, être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union des syndicats de l'immobilier et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Union des syndicats de l'immobilier, représentante désignée pour l'ensemble des requérants et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
