<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861429</ID>
<ANCIEN_ID>JG_L_2016_01_000000382634</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861429.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 07/01/2016, 382634, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382634</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2016:382634.20160107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...C...et son frère M. B...C...ont demandé au tribunal administratif de Saint-Denis de condamner la commune de Cilaos à les indemniser des préjudices ayant résulté pour chacun de l'accident dont Mme C...et sa mère ont été victimes le 3 avril 2002 sur le territoire de cette commune. Par un jugement n° 1000710 du 4 octobre 2012, le tribunal administratif a partiellement fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 12BX03269 du 13 mai 2014, la cour administrative d'appel de Bordeaux a, à la demande de la commune de Cilaos, annulé ce jugement et rejeté la demande des consortsC.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires,  enregistrés les 15 juillet et 15 octobre 2014, 11 mai et 19 juin 2015 au secrétariat du contentieux du Conseil d'Etat, les consorts C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Cilaos ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Cilaos la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme C...et de M. C...et à la SCP Gatineau, Fattaccini, avocat de la commune de Cilaos ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 13 mars 2002, consécutif au passage d'une tempête tropicale, le préfet de La Réunion a prononcé la fermeture des itinéraires de randonnée de cette île, dont la partie du sentier GR R2 située sur le territoire de la commune de Cilaos ; que le maire de Cilaos a procédé à l'affichage de cet arrêté en mairie ; que l'Office national des forêts (ONF) a procédé à son affichage au point de départ de ce sentier, dont l'entretien lui incombait ; qu'il a toutefois mis fin à cet affichage quelques jours plus tard, après avoir estimé que le parcours ne présentait plus de risques ; que M. E... C..., Mme A...C...et leur fille Mme D... C...ont emprunté cet itinéraire le 2 avril 2002 après que l'ONF ait mis fin à cet affichage ; qu'en cours de randonnée, Mme A...C...et Mme D...C...ont quitté de leur propre chef le sentier GR R2 pour rejoindre un site naturel formé de vasques d'eau chaude, situé sur le territoire de la commune de Cilaos ; qu'elles ont suivi à cette fin un itinéraire non aménagé longeant le lit de la rivière Bras-Rouge ; qu'un éboulement survenu sur ces lieux a provoqué le décès de Mme A...C...et gravement blessé Mme D...C... ; que cette dernière a demandé à être indemnisée des préjudices consécutifs à ses blessures et son frère, M. B... C..., de son propre préjudice moral résultant du décès de leur mère ; que, par un jugement du 4 octobre 2012, le tribunal administratif de Saint-Denis a déclaré la commune de Cilaos responsable à 75 % des conséquences dommageables de cet accident ; que les consorts C...se pourvoient en cassation contre l'arrêt du 13 mai 2014 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté leur demande indemnitaire ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2212-2 du code général des collectivités territoriales : " La police municipale a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques. Elle comprend notamment : (...) Le soin de prévenir, par des précautions convenables, et de faire cesser, par la distribution des secours nécessaires, (...) les éboulements de terre ou de rochers, les avalanches ou autres accidents naturels, (...) de pourvoir d'urgence à toutes les mesures d'assistance et de secours et, s'il y a lieu, de provoquer l'intervention de l'administration supérieure " ; qu'en vertu de ces dispositions, il incombe au maire de la commune d'assurer la sécurité des promeneurs et notamment de signaler les dangers qui excèdent ceux contre lesquels les intéressés doivent normalement se prémunir ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le site des vasques d'eau chaude de la rivière Bras-Rouge est répertorié comme un site pittoresque et digne d'intérêt par les principaux guides touristiques de La Réunion ; qu'il est, de ce fait, fréquenté par de nombreux randonneurs ; que ces visiteurs sont conduits, pour l'atteindre, à quitter le GR R2 et à emprunter un itinéraire dont le caractère dangereux est avéré, y compris en l'absence de conditions météorologiques particulières ; que cet itinéraire présente, en outre, un balisage non officiel dont la mairie de Cilaos ne pouvait ignorer l'existence et qui était de nature à induire les randonneurs en erreur sur son aménagement ; qu'à la date des faits, le caractère dangereux de cet itinéraire était temporairement aggravé par le récent passage du cyclone Harry, qui avait justifié la fermeture des sentiers de randonnée de l'île par un arrêté préfectoral du 13 mars 2002, toujours en vigueur au moment de l'accident ; que, dans ces circonstances, alors même que les sentiers de randonnée de l'île de La Réunion présenteraient un danger supérieur au danger moyen des sentiers de randonnée et justifieraient de ce fait une prudence particulière de la part des promeneurs, et sans préjudice des obligations qui pouvaient également incomber à d'autres personnes morales telles que l'ONF ni des fautes éventuelles des victimes, il incombait au maire de Cilaos, en vertu des dispositions de l'article L. 2212-2 du code général des collectivités territoriales, de prendre toute mesure pour informer les randonneurs du danger ; qu'en jugeant que le maire de la commune de Cilaos n'avait pas commis de faute dans l'exercice de ses pouvoirs de police en s'abstenant de toute mesure autre que l'affichage en mairie de l'arrêté préfectoral du 13 mars 2002 pour informer les randonneurs des dangers du site en cause et du chemin qui y mène au motif que ce parcours ne présentait pas de danger particulier excédant celui auquel ils devaient normalement s'attendre, la cour administrative d'appel de Bordeaux a inexactement qualifié les faits qui lui étaient soumis ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Cilaos la somme de 3 000 euros, à verser aux consorts C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mise à la charge des consorts C...qui ne sont pas, dans la présente instance, la partie perdante, la somme demandée par la commune de Cilaos ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
     --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 13 mai 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : La commune de Cilaos versera aux consorts C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la commune de Cilaos au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à Mme D...C..., à M. B...C..., à la commune de Cilaos, à la caisse primaire d'assurance maladie de Beauvais et à la mutuelle générale de l'éducation nationale. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
