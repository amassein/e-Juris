<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029147032</ID>
<ANCIEN_ID>J0_L_2014_06_000001204324</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/14/70/CETATEXT000029147032.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 5ème chambre, 12/06/2014, 12VE04324, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-12</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>12VE04324</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. LE GARS</PRESIDENT>
<AVOCATS>MARCHAIS</AVOCATS>
<RAPPORTEUR>M. Jean-Edmond  PILVEN</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme BESSON-LEDEY</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 décembre 2012, présentée pour LA POSTE, dont le siège est 44 boulevard de Vaugirard à Paris (75757), par Me Marchais, avocat ; <br/>
<br/>
       LA POSTE demande à la Cour :<br/>
<br/>
       1° d'annuler le jugement nos 1109594-1201095 du 8 novembre 2012 par lequel le Tribunal administratif de Montreuil a annulé les décisions des 19 septembre 2011 et 16 janvier 2012 du directeur de l'enseigne Ile-de-France de LA POSTE portant suspension de fonctions de M. A...C..., directeur d'établissement à Tremblay-en-France ;<br/>
<br/>
       2° de mettre à la charge de M. C...la somme de 2 500 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
       Elle soutient que :<br/>
<br/>
       - les faits reprochés à l'intéressé doivent présenter un caractère de vraisemblance et de gravité suffisante et qu'ils étaient établis ;<br/>
<br/>
       Vu le jugement attaqué ;<br/>
<br/>
       Vu le mémoire en défense, enregistré le 11 février 2013, présenté pour <br/>
M.C..., par le cabinet Cassel, avocat, qui conclut au rejet de la requête et à ce que la somme de 3 000 euros soit mise à la charge de LA POSTE en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
       Il soutient que :<br/>
<br/>
       - l'auteur des décisions attaquées n'avait pas reçu délégation de compétence ;<br/>
       - les décisions contestées ne sont pas motivées et ne prévoient aucune durée de suspension ;<br/>
       - les faits qui lui sont reprochés ne sont pas matériellement établis, et aucune faute grave n'a été relevée dans son comportement ;<br/>
<br/>
       Vu le mémoire, enregistré le 18 avril 2013, présenté pour LA POSTE qui conclut aux mêmes fins que précédemment et soutient que :<br/>
<br/>
       - l'auteur des décisions contestées avait reçu délégation de compétence ;<br/>
       - les décisions de suspension n'ont pas à être motivées ; par ailleurs, ce moyen manque en fait ;<br/>
       - la durée de la suspension n'a pas à être indiquée dans les décisions de suspension ;<br/>
       - les faits sont matériellement établis ;<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires ;<br/>
<br/>
       Vu la loi n° 90-568 du 2 juillet 1990 modifié portant statut de La Poste ;<br/>
<br/>
       Vu le décret n° 90-1111 du 12 décembre 1990 modifié portant statut de La Poste ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 15 mai 2014 :<br/>
<br/>
       - le rapport de M. Pilven, premier conseiller,<br/>
       - les conclusions de Mme Besson-Ledey, rapporteur public,<br/>
       - et les observations de MeB..., substituant Me Marchais, pour LA POSTE ; <br/>
<br/>
<br/>
       1. Considérant que M. A...C..., directeur de l'établissement de <br/>
LA POSTE à Tremblay-en-France a été suspendu de ses fonctions, à la suite de la plainte de sept de ses collaborateurs pour des faits de harcèlement moral, par une décision du directeur exécutif de l'enseigne Ile-de France de LA POSTE en date du 19 septembre 2011 aux motifs d'un " comportement managérial inapproprié envers ses collaborateurs " ; qu'il a été mis fin à cette mesure le 3 novembre 2011 à la suite du placement de l'intéressé en congé de maladie ordinaire et qu'une nouvelle décision de suspension a été prise à son encontre le 16 janvier 2012 ; que, par un jugement du 8 novembre 2012, le Tribunal administratif de Montreuil a annulé les deux décisions de suspension susmentionnées ; que LA POSTE forme régulièrement appel de ce jugement ;<br/>
<br/>
       2. Considérant qu'aux termes de l'article 30 de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires : " En cas de faute grave commise par un fonctionnaire, qu'il s'agisse d'un manquement à ses obligations professionnelles ou d'une infraction de droit commun, l'auteur de cette faute peut être suspendu par l'autorité ayant pouvoir disciplinaire qui saisit, sans délai, le conseil de discipline. Le fonctionnaire suspendu conserve son traitement, l'indemnité de résidence, le supplément familial de traitement et les prestations familiales obligatoires (...) " ; et qu'aux termes de l'article 6 quinquies de la loi n° 83-634 du 13 juillet 1983 : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel. (...) " ;<br/>
<br/>
       3. Considérant que la mesure provisoire de suspension prévue par ces dispositions législatives, qui est uniquement destinée à écarter temporairement un agent du service, en attendant qu'il soit statué disciplinairement ou pénalement sur sa situation, ne présente pas par elle-même un caractère disciplinaire ; qu'elle peut être légalement prise dès lors que l'administration est en mesure d'articuler à l'encontre de l'intéressé des griefs qui ont un caractère de vraisemblance suffisant et qui permettent de présumer que celui-ci a commis une faute grave ; que si LA POSTE soutient que les deux mesures successives de suspension contestées sont justifiées par le fait que sept collaborateurs de M. C...se sont dits victimes de harcèlement moral, elle n'expose ni dans ses écritures, ni dans les pièces produites, absolument aucun élément de fait circonstancié relatif à des agissements dont <br/>
M. C... serait responsable avec pour objet ou pour effet une dégradation des conditions de travail de ses collaborateurs, susceptible de porter atteinte à leur dignité, d'altérer leur santé physique ou mentale ou encore de compromettre leur avenir professionnel ; qu'en l'absence de tout élément circonstancié porté à la connaissance de la Cour, LA POSTE ne l'a pas mise en mesure de porter une appréciation sur la vraisemblance des griefs permettant de présumer la commission d'une faute grave par M. C... ; que, par suite, LA POSTE n'est pas fondée à se plaindre de ce que le Tribunal administratif de Montreuil a, par le jugement attaqué, annulé les décisions de suspension de fonctions prises à l'encontre de M. C... les 19 septembre 2011 et 16 janvier 2012 ; que, dès lors, sa requête ne peut qu'être rejetée ;<br/>
<br/>
       Sur l'application de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
       4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M.C..., qui n'est pas la partie perdante dans la présente instance, la somme que LA POSTE demande au titre des frais exposés et non compris dans les dépens ; qu'il y a lieu, en revanche, de faire application de ces dispositions et de mettre à la charge de LA POSTE une somme de 1 500 euros au titre des frais exposés par M. C...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
DECIDE :<br/>
<br/>
<br/>
<br/>
<br/>
Article 1er : La requête de LA POSTE est rejetée.<br/>
Article 2 : LA POSTE versera à M. C...une somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le présent arrêt sera notifié à LA POSTE et à M. A... C....<br/>
<br/>
<br/>
<br/>
       Délibéré après l'audience du 15 mai 2014, à laquelle siégeaient :<br/>
<br/>
       M. Le Gars, président ;<br/>
       M. Pilven, premier conseiller ;<br/>
       Mme Margerit, premier conseiller ; <br/>
<br/>
<br/>
       Lu en audience publique, le 12 juin 2014.<br/>
<br/>
<br/>
<br/>
Le rapporteur,<br/>
J.-E. PILVENLe président,<br/>
J. LE GARSLe greffier,<br/>
C. YARDE       <br/>
<br/>
       La République mande et ordonne au ministre de l'économie, du redressement productif et du numérique en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun, contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
<br/>
       					Pour expédition conforme<br/>
       					Le greffier,<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 12VE04324		2<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01 Compétence. Compétence à l'intérieur de la juridiction administrative. Compétence en premier ressort des tribunaux administratifs.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
