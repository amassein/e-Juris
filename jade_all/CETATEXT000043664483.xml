<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664483</ID>
<ANCIEN_ID>JG_L_2021_06_000000438113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664483.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/06/2021, 438113, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438113.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 30 janvier 2020 et 8 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'avis de la commission d'avancement du 4 décembre 2019 défavorable à sa demande de détachement dans le corps judiciaire sur le fondement de l'article 41 de l'ordonnance du 22 décembre 1958 ;<br/>
<br/>
              2°) d'enjoindre à la commission d'avancement de réexaminer sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le décret n° 93-21 du 7 janvier 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament - Robillot, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. M. B... demande au Conseil d'Etat d'annuler pour excès de pouvoir l'avis défavorable émis par la commission d'avancement, siégeant du 25 novembre au 4 décembre 2019, à sa demande de détachement dans le corps judiciaire. <br/>
<br/>
              2. Aux termes de l'article 41 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Les membres des corps recrutés par la voie de l'Ecole nationale d'administration et les professeurs et les maîtres de conférences des universités peuvent, dans les conditions prévues aux articles suivants, faire l'objet d'un détachement judiciaire pour exercer les fonctions des premier et second grades. / Le présent article s'applique, dans les conditions prévues par leur statut, aux fonctionnaires de l'Etat, territoriaux et hospitaliers, aux militaires et aux fonctionnaires des assemblées parlementaires appartenant à des corps et cadres d'emplois de même niveau de recrutement ". Aux termes de l'article 31-1 du décret du 7 janvier 1993 pris pour l'application de cette ordonnance : " Lorsqu'elle statue en application des articles 18-1, 25-2, 40 et 41-2 de l'ordonnance du 22 décembre 1958 susvisée, la commission prévue à l'article 34 de cette ordonnance peut, si elle l'estime nécessaire au vu du dossier d'un candidat, procéder à une audition de ce dernier ou désigner à cette fin un ou plusieurs de ses membres ".<br/>
<br/>
              3. En premier lieu, si le requérant fait valoir qu'une partie des membres de la commission d'avancement n'auraient pas siégé lors de l'examen de sa candidature, il ne ressort pas des pièces du dossier que celle-ci aurait délibéré sans que la majorité de ses membres titulaires ou suppléants soient présents. Par suite, le moyen tiré de ce que la commission aurait rendu son avis en siégeant dans une composition irrégulière doit être écarté. <br/>
<br/>
              4. En deuxième lieu, il résulte des dispositions de l'article 31-1 du décret du 7 janvier 1993 mentionnées ci-dessus que s'il est loisible à la commission d'avancement de procéder, lorsqu'elle l'estime nécessaire, à l'audition d'un candidat au détachement dans le corps, celle-ci n'y est pas tenue à peine d'irrégularité de la procédure. La seule circonstance que le rapport d'activité de la commission de l'année 2018-2019 ait mentionné, s'agissant du détachement judiciaire, " l'exigence d'une audition préalable de chaque candidat par deux rapporteurs au stade de l'examen de la demande initiale fondée sur les articles 41-1 à 41-8 de l'ordonnance statutaire " est, par ailleurs et en tout état de cause, sans incidence sur la légalité de l'avis contesté. <br/>
<br/>
              5. En troisième lieu, si M. B..., qui exerçait les fonctions de directeur adjoint de l'établissement public départemental de santé mentale de l'Aisne, fait valoir qu'il justifiait de plusieurs attestations favorables émanant notamment de magistrats du tribunal de grande instance de Laon et du bâtonnier de l'ordre des avocats de Laon, il ressort des pièces du dossier, et notamment des appréciations émises par la première présidente et la procureure générale de la cour d'appel d'Amiens ainsi que par le président et le procureur de la République du tribunal judiciaire de Senlis, que celui-ci, nonobstant ses qualités professionnelles et son intérêt pour le métier de magistrat, était insuffisamment préparé à l'exercice de fonctions juridictionnelle en raison de lacunes dans ses connaissances juridiques et d'une information trop limitée sur l'institution judiciaire. Dès lors, la commission n'a pas commis d'erreur manifeste d'appréciation en estimant que, malgré ses compétences professionnelles avérées, M. B... ne présentait pas les aptitudes requises pour exercer immédiatement des fonctions judiciaires. <br/>
<br/>
              6. Enfin, il ressort des pièces du dossier que les entretiens que M. B... a eus dans le cadre de l'instruction de sa demande de détachement ont été réalisés non pas avec les chefs du tribunal judiciaire de Laon, en raison des liens professionnels qu'il avait noué avec plusieurs magistrats de cette juridiction dans le cadre de ses fonctions, mais avec ceux du tribunal judiciaire de Senlis afin d'assurer le respect du principe d'impartialité. En outre, il ne ressort pas des pièces du dossier que les contacts professionnels très ponctuels que M. B... a pu avoir avec la première présidente de la cour d'appel d'Amiens ainsi qu'avec la procureure générale près la même cour aient été de nature, dans les circonstances de l'espèce, à porter atteinte au principe d'impartialité dans l'instruction de sa candidature. <br/>
<br/>
              7. Compte tenu de tout ce qui précède, M. B... n'est pas fondé à demander l'annulation de l'avis de la commission d'avancement qu'il attaque. Ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par suite, être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au garde des <br/>
sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
