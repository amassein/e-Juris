<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323673</ID>
<ANCIEN_ID>JG_L_2013_12_000000352460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/36/CETATEXT000028323673.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 13/12/2013, 352460, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352460.20131213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 septembre et 5 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01904 du 11 juillet 2011 de la cour administrative d'appel de Marseille en tant que la cour, après avoir annulé pour irrégularité le jugement n° 0601158 du 25 mars 2009 du tribunal administratif de Nice, a rejeté sa demande de première instance tendant à la condamnation de l'Etat à l'indemniser des préjudices résultant de sa vaccination contre l'hépatite B ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 25 novembre 2013, présentée pour Mme A... ;<br/>
	Vu le code de la santé publique ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de Mme A...et à la SCP Célice, Blancpain, Soltner, avocat de l'Hôpital de Cimiez centre hospitalier universitaire ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juge du fond que Mme A...a subi, dans le cadre de l'obligation vaccinale liée à son activité d'infirmière, trois injections d'un vaccin contre l'hépatite B, en mars, avril et mai 1991, puis un rappel le 20 octobre 1992 et, enfin, une dernière injection le 10 octobre 1997 ; qu'ayant développé une sclérose en plaques, elle a recherché, sur le fondement de l'article L. 3111-9 du code de la santé publique, la responsabilité de l'Etat à raison de cette affection qu'elle impute à la vaccination obligatoire ; que le ministre chargé de la santé, après avoir recueilli l'avis de la commission de règlement amiable des accidents vaccinaux, a estimé que les troubles dont Mme A...était atteinte n'étaient pas imputables à la vaccination et a rejeté sa demande d'indemnisation ; que l'intéressée a recherché la responsabilité de l'Etat devant le tribunal administratif de Nice qui a rejeté sa demande par un jugement du 25 mars 2009 ; qu'elle se pourvoit en cassation contre l'arrêt du 11 juillet 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement en raison d'un vice de procédure, a rejeté sa demande de première instance ainsi que les conclusions indemnitaires présentées par la caisse primaire d'assurance maladie des Alpes-Maritimes et le centre hospitalier universitaire de Nice au titre des prestations versées à l'intéressée ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              2. Considérant que le fait qu'une personne ait présenté des symptômes de sclérose en plaques antérieurement à la vaccination contre l'hépatite B qu'elle a reçue n'est pas de nature à faire obstacle à ce que soit recherchée l'imputabilité de l'aggravation de cette affection à la vaccination ; que le lien direct entre la vaccination et l'aggravation de la pathologie doit être regardé comme établi lorsque des signes cliniques caractérisés d'aggravation sont apparus dans un bref délai à la suite d'une injection et que la pathologie s'est, à la suite de la vaccination, développée avec une ampleur et à un rythme qui n'étaient pas normalement prévisibles au vu des atteintes que la personne présentait antérieurement à celle-ci ; <br/>
<br/>
              3. Considérant que, pour juger que l'état de santé de Mme A...n'était pas imputable à la vaccination qu'elle avait subie, la cour administrative d'appel de Marseille s'est fondée sur la circonstance que les premiers symptômes de sclérose en plaques étaient apparus avant toute injection vaccinale ; qu'en se déterminant ainsi, sans rechercher si l'évolution de la maladie à la suite des injections permettait de regarder comme établi un lien direct entre la vaccination et l'aggravation de la pathologie, la cour a commis une erreur de droit ; que son arrêt doit par suite être annulé en tant qu'après avoir annulé pour irrégularité le jugement du tribunal administratif de Nice il a statué par la voie de l'évocation sur la responsabilité de l'Etat ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant, d'une part, qu'il résulte de l'instruction, et notamment des expertises réalisées dans le cadre de la procédure de règlement amiable des accidents vaccinaux, que Mme A... a souffert de troubles neurologiques, notamment des troubles de la sensibilité et trois névrites optiques, avant toute injection vaccinale ; que dans ces conditions, la sclérose en plaques dont elle est atteinte ne peut être regardée comme imputable à la vaccination contre l'hépatite B ; <br/>
<br/>
              6. Considérant, d'autre part, que si Mme A...déclare qu'à la suite de la dernière injection, pratiquée le 10 octobre 1997, elle a ressenti une fatigue importante qui s'est prolongée tout au long de l'année 1998, cet épisode, qui est évoqué par l'expertise du Dr Girard mais n'est pas mentionné par l'expertise du Dr Cohen et n'est retracé par aucune pièce, n'est pas suffisamment établi ; que la névrite optique apparue en décembre 1997 ne révèle pas à elle seule une aggravation de la maladie dès lors que Mme A...avait déjà souffert à plusieurs reprises de troubles de même nature ; que l'hyperesthésie de l'hémicorps gauche ayant justifié une hospitalisation en janvier 1999 est survenue plus d'un an après la dernière injection de vaccin ; qu'eu égard à ce délai, et compte tenu du fait que la pathologie a évolué, sans rupture d'une ampleur imprévisible, le lien direct entre la vaccination et l'aggravation de la sclérose en plaques ne peut être regardé comme établi ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les conclusions indemnitaires de Mme A... doivent être rejetées ; qu'il en va de même, par voie de conséquence, des conclusions qu'elle a présentées devant la cour administrative d'appel et devant le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 11 juillet 2011 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : La demande de Mme A...tendant à ce que l'Etat soit condamné à lui verser une indemnité est rejetée. <br/>
<br/>
Article 3 : Les conclusions présentées par Mme A...devant la cour administrative d'appel et le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A..., à la ministre des affaires sociales et de la santé et à la caisse primaire d'assurance maladie des Alpes-Maritimes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
