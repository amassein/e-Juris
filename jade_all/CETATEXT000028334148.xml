<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028334148</ID>
<ANCIEN_ID>JG_L_2013_12_000000346575</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/33/41/CETATEXT000028334148.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 16/12/2013, 346575, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346575</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:346575.20131216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 février et 10 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant... ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09LY02503 du 9 décembre 2010 de la cour administrative d'appel de Lyon en tant qu'il a, d'une part, réduit à 3 581 euros la somme que le centre hospitalier d'Albertville avait été condamné à lui verser par le jugement n° 9602834 du 25 septembre 2002 du tribunal administratif de Grenoble, rectifié le 28 novembre 2002, et, d'autre part, rejeté les conclusions de son appel incident ; <br/>
<br/>
              2°) de mettre à la charge du centre hospitalier d'Albertville la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, avocat de Mme B...et à Me Le Prado, avocat du centre hospitalier d'Albertville ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un accident du travail survenu le 27 février 1991, Mme B...a présenté à la main gauche une blessure qui a été soignée au centre hospitalier d'Albertville ; qu'elle a dû subir en 1995 dans le même établissement une amputation de deux doigts et d'une partie de la paume de la main ; que, s'estimant victime de fautes imputables au centre hospitalier d'Albertville, elle a recherché la responsabilité de cet établissement public devant le juge administratif ; qu'après que l'intéressée a été indemnisée par son employeur de certains préjudices personnels en exécution d'un jugement du 25 avril 2002 du tribunal des affaires de sécurité sociale de la Savoie, le tribunal administratif de Grenoble, statuant par un jugement du 25 septembre 2002, a retenu l'existence de fautes médicales engageant la responsabilité du centre hospitalier d'Albertville et a fait partiellement droit aux conclusions indemnitaires formées par Mme B... ainsi qu'à la demande de la caisse primaire d'assurance maladie de la Savoie tendant au remboursement de ses débours ; que les appels dirigés contre ce jugement ont fait l'objet d'un arrêt du 13 mars 2007 de la cour administrative d'appel de Lyon, qui a été annulé par une décision du 21 octobre 2009 du Conseil d'Etat statuant au contentieux ; que la cour administrative d'appel de Lyon, à laquelle l'affaire a été renvoyée, a statué à nouveau par un arrêt du 9 décembre 2010 qui accorde à MmeB..., au titre de ses préjudices patrimoniaux, une indemnité de 3 581 euros correspondant à des pertes de revenus et rejette le surplus de ses conclusions, y compris sa demande relative à la réparation de ses préjudices personnels, dont le montant est regardé comme inférieur à la somme versée en application du jugement du tribunal des affaires de sécurité sociale ; que l'intéressée se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              2. Considérant que la cour, après avoir retenu que les périodes d'incapacité temporaire totale subies par Mme B...en raison des fautes imputables au centre hospitalier d'Albertville s'étendaient du 27 février au 1er décembre 1991, du 7 mai au 7 septembre 1992 et du 19 octobre au 19 décembre 1995, a déterminé les salaires qui auraient dû être perçus par l'intéressée durant ces périodes, puis en a déduit les indemnités journalières qui lui avaient été versées avant de lui allouer la différence au titre de ses pertes de revenus, soit une somme de 3 581 euros ; que la cour ayant fait droit à l'intégralité de sa demande au titre des périodes en cause, Mme B...est sans intérêt à demander l'annulation de l'article 1er de l'arrêt attaqué en tant qu'il limite à 3 581 euros la somme qui lui est due par le centre hospitalier d'Albertville au titre de ses pertes de revenus ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel que Mme B...sollicitait par la voie de l'appel incident le remboursement des sommes de 1 525 euros au titre de frais de transport et 105,50 euros au titre de frais d'aide à domicile restés à sa charge ; que la cour a rejeté l'ensemble des conclusions incidentes de Mme B...sans se prononcer dans les motifs de son arrêt sur le bien-fondé de ces demandes ; que l'arrêt est entaché, sur ce point, d'une insuffisance de motivation ; <br/>
<br/>
              4. Considérant que, pour évaluer les préjudices personnels subis par MmeB..., y compris les troubles dans ses conditions d'existence, à la somme totale de 30 000 euros, la cour a notamment relevé que la requérante avait connu des périodes d'incapacité temporaire totale d'une durée cumulée de huit mois et demi ; qu'en se déterminant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que les périodes d'incapacité temporaire totale de l'intéressée atteignaient, ainsi d'ailleurs qu'elle l'a retenu dans une autre partie de son arrêt, une durée cumulée de quinze mois, la cour a entaché son arrêt de dénaturation ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme B...n'est fondée à demander l'annulation de l'arrêt attaqué qu'en tant qu'il rejette ses conclusions tendant au remboursement de frais de transport et d'aide à domicile restés à sa charge et statue sur ses préjudices personnels ; <br/>
<br/>
              6. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la limite de la cassation prononcée ;<br/>
<br/>
              Sur les frais de transport et d'aide à domicile : <br/>
<br/>
              7. Considérant que Mme B...a sollicité dans ses conclusions d'appel le remboursement des sommes de 1 525 euros au titre de frais de transport restés à sa charge et de 105,50 euros au titre de frais d'aide à domicile ; qu'elle établit avoir exposé ces frais d'aide à domicile et a droit au remboursement de la somme correspondante ; qu'en revanche, elle ne justifie pas de l'existence des frais de transport dont elle sollicite le remboursement, lequel ne peut, dès lors, lui être accordé ; <br/>
<br/>
              Sur les préjudices personnels subis par Mme B...:<br/>
<br/>
              8. Considérant que les conclusions présentées par Mme B...au titre de ses préjudices non patrimoniaux peuvent être regardées comme tendant à l'indemnisation, d'une part, s'agissant des préjudices qu'elle a subis jusqu'à la date de la consolidation de son état de santé, acquise le 31 décembre 1995, du déficit fonctionnel temporaire et des souffrances physiques et psychiques et, d'autre part, s'agissant des préjudices permanents qu'elle subit depuis cette date, du déficit fonctionnel permanent, du préjudice esthétique résultant de l'altération de son apparence physique et du préjudice d'agrément lié à l'impossibilité de continuer à pratiquer certaines activités sportives et de loisirs ; <br/>
<br/>
              En ce qui concerne les préjudices temporaires :<br/>
<br/>
              9. Considérant, d'une part, que Mme B...a subi avant la consolidation de son état de santé, en raison des fautes imputables au centre hospitalier d'Albertville, plusieurs périodes d'incapacité temporaire totale d'une durée cumulée de quinze mois ainsi que des périodes d'incapacité temporaire partielle d'une durée cumulée de neuf mois et demi avec un taux d'incapacité évalué à 20 % et de près de trois ans avec un taux d'incapacité évalué à 10 % ; qu'il sera fait une juste appréciation du préjudice ayant résulté pour elle de son déficit fonctionnel temporaire en l'évaluant à 9 000 euros ; <br/>
<br/>
              10. Considérant, d'autre part, que l'intéressée a éprouvé durant la période de quatre ans et dix mois antérieure à la consolidation de son état de santé des souffrances physiques et psychiques dont l'intensité a été évaluée par l'expert à 4/7 ; que ce préjudice peut être évalué à 7 000 euros ;<br/>
<br/>
              En ce qui concerne les préjudices permanents :<br/>
<br/>
              Quant au déficit fonctionnel permanent : <br/>
<br/>
              11. Considérant qu'il résulte de l'instruction que Mme B...demeure atteinte, depuis la consolidation de son état de santé, acquise alors qu'elle était âgée de 32 ans, d'une incapacité permanente partielle de 12 % du fait de l'amputation des quatrième et cinquième doigts de sa main gauche et d'une partie de la paume de cette main ; qu'il sera fait une juste appréciation du préjudice inhérent au déficit fonctionnel permanent qui résulte de cette amputation en lui allouant à ce titre, conformément à ses conclusions, la somme de 18 000 euros ; <br/>
<br/>
              Quant au préjudice esthétique :<br/>
<br/>
              12. Considérant que ce préjudice a été évalué par l'expert à 3/7 ; qu'il y a lieu d'allouer à ce titre à Mme B...la somme de 4 500 euros ; <br/>
<br/>
              Quant au préjudice d'agrément :<br/>
<br/>
              13. Considérant que Mme B...justifie rencontrer depuis son amputation des difficultés dans l'exercice des activités de loisirs et des activités sportives qu'elle pratiquait avant l'accident dont elle a été victime ;  qu'il sera fait une juste appréciation du préjudice qu'elle subit à ce titre en lui allouant une somme de 3 600 euros ; <br/>
<br/>
              Sur l'indemnité due à MmeB... : <br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que Mme B...a subi des préjudices personnels dont le montant total s'élève à 42 100 euros ; qu'en exécution du jugement du 25 avril 2002 du tribunal des affaires de sécurité sociale de la Savoie mentionné ci-dessus, elle a déjà été indemnisée par son employeur de ses préjudices personnels à hauteur de 31 623 euros ; qu'il y a lieu, par suite, de lui allouer la différence, soit 10 477 euros, augmentée de la somme de 105,50 euros mentionnée au point 7, correspondant aux frais d'aide à domicile, soit au total une somme de 10 582,50 euros, à la charge du centre hospitalier d'Albertville ; <br/>
<br/>
              Sur les intérêts et leur capitalisation :<br/>
<br/>
              15. Considérant que Mme B...a droit aux intérêts au taux légal sur la somme qui lui est due à compter du 9 mai 1996, date de réception de sa réclamation préalable ; qu'elle a demandé la capitalisation des intérêts à cette date ; qu'il y a lieu d'ordonner cette capitalisation au 9 mai 1997, date à laquelle a été due une année d'intérêts, puis à chaque échéance annuelle ultérieure ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier d'Albertville la somme de 3 000 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 9 décembre 2010 est annulé en tant qu'il rejette les conclusions de Mme B...tendant au remboursement de frais de transport et d'aide à domicile restés à sa charge et statue sur ses préjudices personnels.<br/>
<br/>
Article 2 : Le centre hospitalier d'Albertville est condamné à verser à Mme B...la somme de 10 582,50 euros s'ajoutant à la somme de 3 581 euros mise à sa charge par l'article 1er de l'arrêt de la cour administrative d'appel de Lyon du 9 décembre 2010. La somme totale allouée à Mme B...sera assortie des intérêts au taux légal à compter du 9 mai 1996. Les intérêts échus à la date du 9 mai 1997 puis à chaque échéance annuelle ultérieure à compter de cette date seront capitalisés à chacune de ces dates pour produire eux-mêmes intérêts. <br/>
<br/>
Article 3 : L'article 1er du jugement du tribunal administratif de Grenoble n° 9602834 du 25 septembre 2002 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 4 : Le centre hospitalier d'Albertville versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions présentées par Mme B...devant la cour administrative d'appel de Lyon et le Conseil d'Etat est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A... B...et au centre hospitalier d'Albertville. <br/>
Copie pour information en sera adressée à la caisse primaire d'assurance maladie de la Savoie.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - APPLICATION DE LA NOMENCLATURE DITE DINTILHAC [RJ1] - IDENTIFICATION DES POSTES DE PRÉJUDICE PERSONNEL EN CAS DE DOMMAGE CORPOREL - 1) DISTINCTION ENTRE LES PRÉJUDICES ANTÉRIEURS ET LES PRÉJUDICES POSTÉRIEURS À LA CONSOLIDATION - EXISTENCE - 2) PRÉJUDICES ANTÉRIEURS À LA CONSOLIDATION - NOTION - DÉFICIT FONCTIONNEL TEMPORAIRE - SOUFFRANCES PHYSIQUES ET PSYCHIQUES - INCLUSION - 3) PRÉJUDICES POSTÉRIEURS À LA CONSOLIDATION - NOTION - DÉFICIT FONCTIONNEL PERMANENT - PRÉJUDICE ESTHÉTIQUE RÉSULTANT DE L'ALTÉRATION DE L'APPARENCE PHYSIQUE - PRÉJUDICE D'AGRÉMENT LIÉ À L'IMPOSSIBILITÉ DE CONTINUER À PRATIQUER CERTAINES ACTIVITÉS SPORTIVES ET DE LOISIRS - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 376-1 (ANCIEN ART. L. 397) DU CODE DE LA SÉCURITÉ SOCIALE. - APPLICATION DE LA NOMENCLATURE DITE DINTILHAC - IDENTIFICATION DES POSTES DE PRÉJUDICE PERSONNEL EN CAS DE DOMMAGE CORPOREL - 1) DISTINCTION ENTRE LES PRÉJUDICES ANTÉRIEURS ET LES PRÉJUDICES POSTÉRIEURS À LA CONSOLIDATION - EXISTENCE - 2) PRÉJUDICES ANTÉRIEURS À LA CONSOLIDATION - NOTION - DÉFICIT FONCTIONNEL TEMPORAIRE - SOUFFRANCES PHYSIQUES ET PSYCHIQUES - INCLUSION - 3) PRÉJUDICES POSTÉRIEURS À LA CONSOLIDATION - NOTION - DÉFICIT FONCTIONNEL PERMANENT - PRÉJUDICE ESTHÉTIQUE RÉSULTANT DE L'ALTÉRATION DE L'APPARENCE PHYSIQUE - PRÉJUDICE D'AGRÉMENT LIÉ À L'IMPOSSIBILITÉ DE CONTINUER À PRATIQUER CERTAINES ACTIVITÉS SPORTIVES ET DE LOISIRS - INCLUSION.
</SCT>
<ANA ID="9A"> 60-04-03 1) Lorsqu'il utilise la nomenclature dite Dintilhac, il appartient au juge administratif de distinguer les préjudices personnels subis avant la consolidation de son état de santé, d'une part, et les préjudices subis après la date de consolidation, d'autre part.,,,2) Au nombre des postes de préjudice personnel antérieurs à la consolidation figurent notamment le déficit fonctionnel temporaire et les souffrances physiques et psychiques subis par la victime jusqu'à cette date.,,,3) Au nombre des postes de préjudice personnel postérieurs à la consolidation figurent, notamment, le déficit fonctionnel permanent, le préjudice esthétique résultant de l'altération de l'apparence physique et le préjudice d'agrément lié à l'impossibilité de continuer à pratiquer certaines activités sportives et de loisirs.</ANA>
<ANA ID="9B"> 60-05-04-01-01 1) Lorsqu'il utilise la nomenclature dite Dintilhac, il appartient au juge administratif de distinguer les préjudices personnels subis avant la consolidation de son état de santé, d'une part, et les préjudices subis après la date de consolidation, d'autre part.,,,2) Au nombre des postes de préjudice personnel antérieurs à la consolidation figurent notamment le déficit fonctionnel temporaire et les souffrances physiques et psychiques subis par la victime jusqu'à cette date.,,,3) Au nombre des postes de préjudice personnel postérieurs à la consolidation figurent, notamment, le déficit fonctionnel permanent, le préjudice esthétique résultant de l'altération de l'apparence physique et le préjudice d'agrément lié à l'impossibilité de continuer à pratiquer certaines activités sportives et de loisirs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, Section, avis, 4 juin 2007, Lagier et Consorts Guignon, n°s 303422 304214, p. 228 ; CE, 7 octobre 2013, Ministre de la défense c/ M. Hamblin, n° 337851, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
