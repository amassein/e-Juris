<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037059323</ID>
<ANCIEN_ID>JG_L_2018_06_000000409437</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/05/93/CETATEXT000037059323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/06/2018, 409437, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409437</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409437.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 25 septembre 2012 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé la décision du 9 mai 2012 de l'inspecteur du travail de Metz refusant d'accorder à la société Rolanfer Matériel Ferroviaire l'autorisation de le licencier et accordé cette autorisation. Par un jugement n° 1205548 du 16 juin 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC01581 du 31 janvier 2017, la cour administrative d'appel de Nancy  a rejeté l'appel formé  par M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 31 mars et 30 juin 2017 et le 23 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Rolanfer Matériel Ferroviaire la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. A...et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Rolanfer Matériel Ferroviaire ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 25 septembre 2012, le ministre chargé du travail a, d'une part, annulé la décision du 9 mai 2012 par laquelle l'inspecteur du travail avait refusé à la société Rolanfer Matériel Ferroviaire l'autorisation de licencier M. A...et, d'autre part, autorisé cette société à le licencier ; que M. A...se pourvoit en cassation contre l'arrêt du 31 janvier 2017 par lequel la cour administrative d'appel de Nancy a rejeté son appel dirigé contre le jugement du 16 juin 2015 par lequel le tribunal administratif de Strasbourg a rejeté sa demande d'annulation de cette décision ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; <br/>
<br/>
              3. Considérant qu'en jugeant que le ministre, à qui il appartenait, en application des dispositions de l'article R. 2421-7 du code du travail, d'examiner si la décision de licenciement était en rapport avec le mandat détenu par M. A...et de l'indiquer dans sa décision, n'était, en revanche, pas tenu de mentionner les raisons pour lesquelles il estimait ne pas devoir retenir les motifs de l'appréciation inverse précédemment portée sur ce point par l'inspecteur du travail, la cour administrative d'appel n'a pas entaché son arrêt d'erreur de droit ; <br/>
<br/>
              4. Considérant que le moyen tiré de ce que l'arrêt attaqué serait entaché d'inexacte qualification juridique des faits et d'erreur de droit en ce qu'il jugerait que la faute reprochée à l'intéressé est de nature à justifier une mesure de licenciement est inopérant, la cour administrative d'appel n'ayant eu à prendre parti, au vu des moyens soulevés devant elle, que sur la matérialité des faits et non sur leur qualification ;<br/>
<br/>
              5. Considérant qu'en jugeant que le ministre avait pu tenir compte, pour statuer sur la légalité de la décision de l'inspecteur du travail, d'éléments qui, bien que présentés postérieurement à cette décision, étaient susceptibles d'éclairer les circonstances de droit et de fait à la date de son édiction, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque, lequel est suffisamment motivé ; que ses conclusions doivent être rejetées, y compris, par voie de conséquence, celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société Rolanfer Matériel Ferroviaire ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Le surplus des conclusions de la société Rolanfer Matériel Ferroviaire présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à la société Rolanfer Matériel Ferroviaire et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
