<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926990</ID>
<ANCIEN_ID>JG_L_2021_08_000000440270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926990.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 06/08/2021, 440270, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:440270.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... D... a demandé au tribunal administratif de Lille d'annuler la décision du 9 février 2017 par laquelle le ministre de l'intérieur a refusé de lui accorder la reconstitution de quatre points de son permis de conduire à la suite d'un stage de sensibilisation à la sécurité routière effectué les 9 et 10 novembre 2012 et d'enjoindre au ministre de valider son stage et de créditer son permis de conduire de douze points. Par un jugement n° 1702443 du 16 novembre 2017, le tribunal administratif a rejeté sa demande.   <br/>
<br/>
              Par une décision n° 417334 du 25 février 2019, le Conseil d'Etat, statuant au contentieux, a annulé ce jugement et a renvoyé l'affaire au tribunal administratif.<br/>
<br/>
              Par un nouveau jugement n° 1902078 du 25 février 2020, le tribunal administratif, statuant sur renvoi du Conseil d'Etat, a rejeté la demande de M. D....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 avril et 20 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. D....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D... a commis entre le 22 avril 2006 et le 14 octobre 2011 diverses infractions au code de la route ayant entraîné un retrait de treize points sur son permis de conduire. Par une décision référencée " 48 SI " du 5 novembre 2012, le ministre de l'intérieur lui a notifié le dernier retrait de points et a constaté, en lui rappelant les précédentes décisions portant retrait de points, la perte de validité de son permis de conduire pour solde de points nul. Par un jugement du 16 novembre 2017, le tribunal administratif de Lille a rejeté la demande de M. D... tendant à l'annulation de cette décision. Par une décision du 25 février 2019, le Conseil d'Etat, statuant au contentieux, a, sur pourvoi de M. D..., annulé ce jugement et renvoyé l'affaire devant le tribunal administratif. Par un jugement du 25 février 2020, le tribunal administratif a de nouveau rejeté la demande de M. D..., lequel se pourvoit en cassation contre ce dernier jugement.<br/>
<br/>
              2. " Aux termes de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne./ Lorsque l'affaire est susceptible d'être dispensée de conclusions du rapporteur public, en application de l'article R. 732-1-1, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, si le rapporteur public prononcera ou non des conclusions et, dans le cas où il n'en est pas dispensé, le sens de ces conclusions ". Aux termes de l'article R. 732-1-1 du même code : " Sans préjudice de l'application des dispositions spécifiques à certains contentieux prévoyant que l'audience se déroule sans conclusions du rapporteur public, le président de la formation de jugement ou le magistrat statuant seul peut dispenser le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience sur tout litige relevant des contentieux suivants : 1° Permis de conduire ; (...) ". Aux termes de l'article R. 741-2 du même code : " La décision mentionne que l'audience a été publique, sauf s'il a été fait application des dispositions de l'article L. 731-1 (...)./ Lorsque, en application de l'article R. 732-1-1, le rapporteur public a été dispensé de prononcer des conclusions, mention en est faite./ (...) ". Il résulte de ces dispositions que le rapporteur public, sur sa proposition, peut être dispensé par le président de la formation de jugement de prononcer des conclusions dans un litige relatif au contentieux du permis de conduire. Les parties à un tel litige doivent être en mesure de connaître si le rapporteur public prononcera ou non des conclusions et, s'il en prononce, le sens de celles-ci.<br/>
<br/>
              3. Il ressort des pièces de la procédure que le 25 janvier 2020, soit trois jours avant l'audience du 28 avril 2020 du tribunal administratif de Lille au cours de laquelle a été appelée l'affaire en litige, une dispense de conclusions a été mentionnée sur l'application " Sagace ". Or il ressort des visas du jugement attaqué que le rapporteur public a néanmoins prononcé des conclusions sur cette affaire lors de l'audience, ces mentions faisant foi en l'absence de preuve contraire. Le requérant, qui n'a ainsi pas été mis en mesure de connaître à l'avance le sens des conclusions du rapporteur public, est, par suite, fondé à demander, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'annulation du jugement qu'il attaque.<br/>
<br/>
              4. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              5. En premier lieu, il résulte de l'instruction que la décision attaquée est revêtue du nom, du prénom, de la qualité et de la signature M. A... C..., chef du bureau national des droits à conduire, lequel avait reçu délégation de signature du ministre de l'intérieur par une décision du délégué à la sécurité et à la circulation routières du 3 mars 2016 parue au Journal officiel du 13 mars 2016. Dès lors, le moyen tiré de l'incompétence de l'auteur de la décision doit être écarté.<br/>
<br/>
              6. En deuxième lieu, il résulte de l'instruction que le courrier du 5 février 2013 du préfet du Pas-de-Calais se borne à indiquer à M. D..., en réponse à son recours gracieux, que l'analyse des services préfectoraux est transmise au fichier national des permis de conduire pour une éventuelle attribution de points après réexamen de sa situation. M. D... n'est, par suite, pas fondé à soutenir que la décision qu'il attaque aurait retiré ce courrier et aurait, de ce fait, violé les dispositions de l'article L. 242-1 du code des relations entre le public et l'administration en retirant une décision créatrice de droit.<br/>
<br/>
              7. En troisième lieu, tant que le retrait de l'ensemble des points du permis de conduire ne lui a pas été rendu opposable par une notification régulière, le titulaire de ce permis peut prétendre au bénéfice des dispositions de l'article L. 223-6 du code de la route qui prévoient des reconstitutions de points, à la suite d'un stage de sensibilisation à la sécurité routière ou lorsqu'il n'a été commis aucune infraction ayant donné lieu à un retrait de points pendant une certaine période.<br/>
<br/>
              8. Il résulte de l'instruction que l'avis de réception du pli recommandé adressé à M. D... contenant la décision référencée " 48 SI " du ministre de l'intérieur et récapitulant les derniers retraits de points, a été retourné à l'administration, revêtu d'une signature et de la mention " distribué le 5/11/12 ". Il ne résulte de l'instruction, ni que l'adresse à laquelle ce pli a été envoyé ne correspondait pas une des résidences effectives de M. D..., ni que le signataire de l'avis de réception n'aurait pas eu qualité pour recevoir ce pli. Dès lors, cette notification d'un solde de points nul le 5 novembre 2012 faisait obstacle à ce que le ministre de l'intérieur fasse droit à la demande d'attribution de quatre points sur le permis de conduire de l'intéressé à la suite du stage de sensibilisation à la sécurité routière qu'il a suivi les 9 et 10 novembre 2012. Par suite, le moyen tiré de ce que le ministre de l'intérieur aurait méconnu les dispositions de l'article L. 223-6 du code de la route doit être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. D... n'est pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance, la partie perdante, la somme que demande, à ce titre, M. D....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lille du 25 février 2020 est annulé.<br/>
<br/>
Article 2 :  La demande présentée par M. D... devant le tribunal administratif de Lille est rejetée.<br/>
<br/>
Article 3 : Les conclusions de M. D... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... D... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
