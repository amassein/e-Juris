<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027113387</ID>
<ANCIEN_ID>JG_L_2013_01_000000352307</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/11/33/CETATEXT000027113387.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 22/01/2013, 352307, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352307</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352307.20130122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 31 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0800917 du 13 juillet 2011 par lequel le tribunal administratif de Saint-Denis a annulé la décision du 2 avril 2008 du recteur de l'académie de la Réunion en tant que celle-ci refuse à M. A... B...le bénéfice de la nouvelle bonification indiciaire (NBI) au titre des services accomplis entre le 1er septembre 2006 et le 2 avril 2008 et a condamné le recteur à verser à M. B... un rappel de bonification indiciaire pour cette période ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B... ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 91-73 du 18 janvier 1991 ;<br/>
<br/>
              Vu le décret n° 91-1229 du 6 décembre 1991 ;<br/>
<br/>
              Vu l'arrêté du 6 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du I de l'article 27 de la loi du 18 janvier 1991 : " La nouvelle bonification indiciaire des fonctionnaires et des militaires, instituée à compter du 1er août 1990, est attribuée pour certains emplois comportant une responsabilité ou une technicité particulières dans des conditions fixées par décret " ; qu'aux termes de l'article 1er du décret du 6 décembre 1991 instituant la nouvelle bonification indiciaire dans les services du ministère de l'éducation nationale : " Une nouvelle bonification indiciaire, prise en compte et soumise à cotisation pour le calcul de la pension de retraite, peut être versée mensuellement, dans la limite des crédits disponibles, aux fonctionnaires du ministère de l'éducation nationale exerçant une des fonctions figurant en annexe au présent décret " ; qu'en vertu de cette annexe, sont notamment susceptibles de bénéficier de la nouvelle bonification indiciaire, les personnels enseignants spécialisés du premier degré chargés de la scolarisation des enfants handicapés ou assurant le secrétariat d'une commission départementale d'éducation spéciale ; que l'arrêté interministériel du 6 décembre 1991 fixant les conditions d'attribution de la nouvelle bonification indiciaire dans les services du ministère de l'éducation nationale, dans sa rédaction applicable au litige, précise que peuvent en bénéficier les personnels enseignants du premier degré titulaires d'un diplôme spécialisé pour l'enseignement des jeunes handicapés affectés soit dans une classe d'intégration scolaire soit dans une classe de perfectionnement créées dans une école maternelle ou élémentaire ;<br/>
<br/>
              2. Considérant que l'administration peut, en première instance comme en appel, faire valoir devant le juge de l'excès de pouvoir que la décision dont l'annulation est demandée est légalement justifiée par un motif, de droit ou de fait, autre que celui initialement indiqué, mais également fondé sur la situation existant à la date de cette décision ; qu'il appartient alors au juge, après avoir mis l'auteur du recours à même de présenter ses observations sur la substitution ainsi sollicitée, de rechercher si un tel motif est de nature à fonder légalement la décision, puis d'apprécier s'il résulte de l'instruction que l'administration aurait pris la même décision si elle s'était fondée initialement sur ce motif ; que, dans l'affirmative, il peut procéder à la substitution demandée, sous réserve toutefois qu'elle ne prive pas le requérant d'une garantie procédurale liée au motif substitué ;<br/>
<br/>
              3. Considérant que, pour annuler la décision du recteur de l'académie de La Réunion du 2 avril 2008 refusant à M. B..., professeur des écoles, l'attribution d'une nouvelle bonification indiciaire au titre de ses fonctions au sein de l'institut médico-éducatif (IME) Raymond Allard de Saint-André, le tribunal administratif de Saint-Denis a estimé que la circonstance que l'intéressé n'était titulaire d'aucun diplôme spécialisé ne pouvait lui être opposée malgré les termes contraires de l'arrêté du 6 décembre 1991, au motif que le bénéfice de la nouvelle bonification indiciaire est exclusivement lié à l'occupation effective et exclusive des fonctions qui y ouvrent droit ; que toutefois, dans son mémoire en défense devant le tribunal administratif, le recteur, tout en soutenant que M. B... ne remplissait pas les conditions de diplômes selon lui requises, s'est également prévalu, pour justifier la légalité de la décision, du fait que l'intéressé n'était affecté ni dans une classe d'intégration scolaire, ni dans une classe de perfectionnement créée dans une école maternelle ou élémentaire, ni au secrétariat d'une commission départementale de l'éducation spéciale ; que le tribunal administratif, qui était ainsi saisi d'une demande de substitution de motifs, s'est abstenu de rechercher si un tel motif était de nature à fonder légalement la décision et ne s'est pas prononcé sur le point de savoir si l'administration aurait pris la même décision si elle avait retenu ce motif ; que, par suite, le ministre est fondé à soutenir que le jugement attaqué est entaché d'une erreur de droit et, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, à en demander l'annulation ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'il résulte des termes mêmes de l'article 27 de la loi du 18 janvier 1991 que le bénéfice de la nouvelle bonification indiciaire, exclusivement attaché à l'exercice effectif des fonctions, ne peut, dès lors, être limité aux fonctionnaires d'un corps ou aux titulaires d'une qualification déterminée ni être soumis à une condition de diplôme ; qu'ainsi, l'arrêté du 6 décembre 1991 fixant la liste des emplois du ministère de l'éducation nationale éligibles à la nouvelle bonification indiciaire ne pouvait légalement en subordonner le bénéfice à la détention d'un diplôme spécialisé pour l'enseignement des jeunes handicapés ; que, par suite, le recteur ne pouvait refuser le bénéfice de la nouvelle bonification indiciaire au motif que M. B... ne détenait pas un diplôme spécialisé pour l'enseignement des jeunes handicapés ; <br/>
<br/>
              6. Considérant, toutefois, que la nouvelle bonification indiciaire prévue par les dispositions du décret et de l'arrêté du 6 décembre 1991 ne s'applique pas aux personnels enseignants affectés dans des établissements médico-éducatifs régis par le code de l'action sociale et des familles et qui comprennent des accueils en classes adaptées aux besoins des personnes accueillies, tels qu'un institut médico-éducatif ; que le motif, invoqué en défense par le recteur devant le tribunal administratif, tiré de ce que l'intéressé ne justifiait pas avoir exercé l'une des fonctions mentionnées par le décret du 6 décembre 1991 susceptibles d'ouvrir droit à la nouvelle bonification indiciaire, était ainsi de nature à fonder légalement sa décision ; qu'il ressort des pièces du dossier que l'administration aurait pris la même décision si elle s'était fondée sur ce seul motif ; que, dès lors, cette substitution de motif ne privant l'intéressée d'aucune garantie procédurale, il y a lieu d'y procéder ; que, par suite, la demande présentée par M. B... doit être rejetée ; <br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Saint-Denis du 13 juillet 2011 est annulé.<br/>
<br/>
Article 2 : La demande de M. B... est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et à M. B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
