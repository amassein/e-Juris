<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044513225</ID>
<ANCIEN_ID>JG_L_2021_11_000000450553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/51/32/CETATEXT000044513225.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/11/2021, 450553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450553.20211130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 mars 2021 et 5 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 23 décembre 2020 l'ayant déchu de sa nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le code civil ; <br/>
              - le code pénal ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 25 du code civil : " L'individu qui a acquis la qualité de Français peut, par décret pris après avis conforme du Conseil d'Etat, être déchu de la nationalité française, sauf si la déchéance a pour résultat de le rendre apatride : / 1° S'il est condamné pour un acte qualifié de crime ou délit constituant une atteinte aux intérêts fondamentaux de la Nation ou pour un crime ou un délit constituant un acte de terrorisme (...) ". L'article 25-1 de ce code ne permet la déchéance de la nationalité dans ce cas qu'à la condition que les faits aient été commis moins de quinze ans auparavant et qu'ils aient été commis soit avant l'acquisition de la nationalité française, soit dans un délai de quinze ans à compter de cette acquisition.<br/>
<br/>
              2. L'article 421-2-1 du code pénal qualifie d'acte de terrorisme " le fait de participer à un groupement formé ou à une entente établie en vue de la préparation, caractérisée par un ou plusieurs faits matériels, d'un des actes de terrorisme " mentionnés aux articles 421-1 et 421-2 du code pénal.<br/>
<br/>
              3. M. A... D... a été déchu de la nationalité française par un décret du 23 décembre 2020 sur le fondement des articles 25 et 25-1 du code civil, au motif qu'il a été condamné par un jugement du tribunal de grande instance de Paris en date du 28 avril 2017 pour avoir participé à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme, faits prévus par l'article 421-2-1 du code pénal. <br/>
<br/>
              4. Aux termes de l'article 61 du décret du 30 décembre 1993 relatif aux déclarations de nationalité, aux décisions de naturalisation, de réintégration, de perte, de déchéance et de retrait de la nationalité française : " Lorsque le Gouvernement décide de faire application des articles 25 et 25-1 du code civil, il notifie les motifs de droit et de fait justifiant la déchéance de la nationalité française, en la forme administrative ou par lettre recommandée avec demande d'avis de réception (...). L'intéressé dispose d'un délai d'un mois à dater de la notification ou de la publication de l'avis au Journal officiel pour faire parvenir au ministre chargé des naturalisations ses observations en défense. A l'expiration de ce délai, le Gouvernement peut déclarer, par décret motivé pris sur avis conforme du Conseil d'Etat, que l'intéressé est déchu de la nationalité française ".<br/>
<br/>
              5. En premier lieu, après avoir cité les textes applicables et énoncé que M. D..., qui a acquis la nationalité française en 2007, a été condamné par un jugement du tribunal de grande instance de Paris du 28 avril 2017 à une peine de sept ans d'emprisonnement assortie d'une période de sûreté de la moitié pour participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme courant 2013, 2014, jusqu'au 2 juin 2014 et en tout cas depuis temps non prescrit, à Paris, en région parisienne, en tout cas sur le territoire national, en Turquie et en Syrie, le décret attaqué indique que les conditions légales permettant de prononcer la déchéance de la nationalité française doivent être regardées comme réunies, sans qu'aucun élément relatif à la situation personnelle du requérant et aux circonstances de l'espèce justifie qu'il y soit fait obstacle. Dans ces conditions, le décret attaqué satisfait à l'exigence de motivation posée par l'article 61 du décret du 30 décembre 1993.<br/>
<br/>
              6. En deuxième lieu, M. D... a été informé qu'une procédure de déchéance de la nationalité française était engagée à son encontre et a été mis en mesure de présenter ses observations en défense en temps utile avant l'intervention du décret attaqué. Par ailleurs, aucun texte ni aucun principe ne faisait obligation à l'administration de répondre aux observations qu'il a produites. Il ressort ainsi des pièces du dossier que le décret attaqué a été précédé de la procédure contradictoire prévue par l'article 61 du décret du 30 décembre 1993. Le moyen tiré de ce que le décret attaqué aurait été pris au terme d'une procédure irrégulière ne peut, dès lors, qu'être écarté.<br/>
<br/>
              7. En troisième lieu, si M. D... soutient que la mesure contestée a pour effet de le rendre apatride, il ressort toutefois des pièces du dossier que le requérant possède la nationalité algérienne depuis sa naissance et qu'en vertu des dispositions des articles 22, 23 et 29 du code de la nationalité algérienne en vigueur à la date du décret attaqué, seule peut être déchue de la nationalité algérienne une personne ayant acquis cette nationalité après sa naissance, la déchéance étant prononcée par décret. Par suite, M. D..., qui n'établit pas être dépourvu de la nationalité algérienne à la date du décret prononçant sa déchéance de la nationalité française, n'est pas fondé à soutenir que ce décret est susceptible de le rendre apatride, en méconnaissance des dispositions précitées de l'article 25 du code civil.  <br/>
<br/>
              8. En quatrième lieu, ainsi qu'il a été dit, le législateur a prévu la possibilité de déchoir de la nationalité française des personnes ayant fait l'objet d'une condamnation pénale pour un crime ou un délit constituant un acte de terrorisme. La conformité des dispositions adoptées sur ce point par le législateur au principe de nécessité des délits et des peines qui découle de l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 ne saurait être contestée devant le Conseil d'Etat statuant au contentieux en dehors de la procédure prévue à l'article 61-1 de la Constitution. <br/>
<br/>
              9. En cinquième lieu, il ressort des pièces du dossier que M. D... a été condamné à une peine de sept ans assortie d'une période de sûreté de la moitié pour des faits qualifiés de participation à une association de malfaiteurs en vue de la préparation d'un acte de terrorisme. Il ressort des constatations de fait auxquelles a procédé le juge pénal que M. D... a rejoint un groupe terroriste, participé à des entrainements et aux opérations armées de ce groupe. <br/>
<br/>
              10. Eu égard à la nature et à la gravité des faits commis par le requérant qui ont conduit à sa condamnation pénale, la sanction de déchéance de la nationalité française n'a pas revêtu, dans les circonstances de l'espèce, un caractère disproportionné. Le comportement ultérieur de l'intéressé ne permet pas de remettre en cause cette appréciation.<br/>
<br/>
              11. En dernier lieu, aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à l'intégrité territoriale ou à la sûreté publique, à la défense de l'ordre et à la prévention du crime, à la protection de la santé ou de la morale, à la protection de la réputation ou des droits d'autrui (...) ".<br/>
<br/>
              12. La sanction de déchéance de la nationalité, prévue par les articles 25 et 25-1 du code civil, a pour objectif de renforcer la lutte contre le terrorisme. Un décret portant déchéance de la nationalité française est par lui-même dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise comme sur ses liens avec les membres de sa famille et n'affecte pas, dès lors, le droit au respect de sa vie familiale. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et porte une atteinte au droit au respect de sa vie privée, dont l'intensité dépend en particulier du mode et de la date d'acquisition de la nationalité. En l'espèce, eu égard à la gravité des faits commis par le requérant, le décret attaqué n'a pas porté une atteinte disproportionnée au droit au respect de sa vie privée garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. D... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque. Ses conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et au ministre de l'intérieur. <br/>
              Délibéré à l'issue de la séance du 4 novembre 2021 où siégeaient : M. Nicolas Boulouis, président de chambre, présidant ; Mme Anne Courrèges, conseillère d'Etat et M. Sébastien Gauthier, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 30 novembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Nicolas Boulouis<br/>
 		Le rapporteur : <br/>
      Signé : M. Sébastien Gauthier<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
