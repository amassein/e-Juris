<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411798</ID>
<ANCIEN_ID>JG_L_2019_04_000000428350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/04/2019, 428350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:428350.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société Bouygues Bâtiment Centre Sud-ouest a demandé au tribunal administratif de Toulouse de condamner l'Etat à lui verser la somme de 7 471 840,08 euros TTC en règlement du solde du marché de travaux portant sur la reconstruction de l'Ecole nationale supérieure d'ingénieurs en arts chimiques et technologiques sur le campus de Toulouse Labège, outre les intérêts moratoires actualisés jusqu'à parfait achèvement et capitalisés. Par un jugement n° 1201114 du 1er juin 2016, le tribunal administratif de Toulouse a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16BX02606 du 31 décembre 2018, la cour administrative d'appel de Bordeaux a, sur appel de la société Bouygues Bâtiment Centre Sud-ouest, réformé ce jugement en fixant le décompte général définitif à la somme de 53 922 079,10 euros TTC, en condamnant l'Etat à lui payer la somme de 5 480 656,58 euros TTC au titre du solde du marché et, au titre des intérêts moratoires, la somme de 172 093,99 euros TTC diminuée des intérêts afférents à la somme de 220 000 euros, mais actualisée à la date du paiement effectif des sommes concernées ou de la notification de l arrêt.  <br/>
<br/>
              1° Sous le n° 428350, par un pourvoi, enregistré le 22 février 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'enseignement supérieur, de la recherche et de l'innovation demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Bouygues Bâtiment Centre Sud-ouest.<br/>
<br/>
              2° Sous le n° 428351, par une requête enregistrée 22 février 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'enseignement supérieur, de la recherche et de l'innovation demande au Conseil d'Etat d'ordonner le sursis à exécution de cet arrêt.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
                               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A...Renault, Auditrice,  <br/>
<br/>
              - les conclusions de Mme B...Le Corre, rapporteur public. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le pourvoi n° 428350 : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'elle attaque, la ministre de l'enseignement supérieur, de la recherche et de l'innovation soutient que la cour a :<br/>
              - commis une erreur de droit et dénaturé les pièces du dossier en jugeant qu'aucune stipulation des cahiers des clauses techniques particulières ne prévoyait l'alimentation des équipements en gaz spéciaux jusqu'aux points d'utilisation, qu'aucun plan ne permettait au titulaire du marché d'effectuer une telle prestation, que les plans d'architecte dont disposait la société ne pouvaient se substituer à des plans de conception, pour en déduire que  les prestations de l'ordre de service n° 49 n'étaient pas comprises dans le marché initial ;<br/>
              - commis une erreur de droit en condamnant l'Etat à rembourser à la société Bouygues Bâtiment Centre Sud-ouest des pénalités de retard d'un montant de 2 983 226,52 euros, en jugeant que la révision du prix devait être majorée de la somme de 195 254,84 euros HT et en condamnant l'Etat à verser également à cette société des intérêts moratoires, alors que l'ordre de service n° 49 correspondait à des travaux qui étaient initialement prévus au marché.<br/>
<br/>
              3. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête n° 428351 à fin de sursis à exécution :<br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi formé par le ministre de l'enseignement supérieur, de la recherche et de l'innovation contre l'arrêt du 31 décembre 2018 de la cour administrative d'appel de Bordeaux n'est pas admis. Par suite, les conclusions à fin de sursis à l'exécution de cet arrêt sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi n° 428350 de la ministre de l'enseignement supérieur, de la recherche et de l'innovation n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 428351 de la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
Article 3 : La présente décision sera notifiée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
Copie en sera adressée à la société Bouygues Bâtiment Centre Sud-ouest.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
