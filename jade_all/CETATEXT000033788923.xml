<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788923</ID>
<ANCIEN_ID>JG_L_2016_12_000000375406</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788923.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/12/2016, 375406</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375406</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP ROCHETEAU, UZAN-SARANO ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:375406.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 23 décembre 2015, le Conseil d'Etat, statuant au contentieux, saisi du pourvoi du centre hospitalier de Chambéry tendant à l'annulation de l'arrêt n°13LY02337 du 12 décembre 2013 de la cour administrative d'appel de Lyon  et à la condamnation de la société Groupe Lépine à le garantir des condamnations prononcées à son encontre par cet arrêt, a sursis à statuer jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel était l'ordre de juridiction compétent pour connaître de ce litige.<br/>
<br/>
              Par une décision du 11 avril 2016, le Tribunal des conflits a déclaré la juridiction administrative seule compétente pour connaître de l'appel en garantie formé par le centre hospitalier de Chambéry contre la société Groupe Lépine.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris les pièces visées par la décision du Conseil d'Etat du 23 décembre 2015 ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la directive 85/374/CEE du 25 juillet 1985 modifiée ;<br/>
<br/>
              - le code civil ;<br/>
<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du Centre Hospitalier de Chambery, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B... et à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Groupe lépine.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la luxation d'une prothèse du genou posée le 25 janvier 2000 au centre hospitalier de Chambéry, M. B...a dû subir, le 27 avril 2000, une intervention chirurgicale de reprise et qu'il a fallu procéder, le 8 février 2001, au remplacement de cette prothèse ; que l'intéressé a exercé un recours indemnitaire contre le centre hospitalier de Chambéry, en invoquant la défectuosité de la première prothèse ; que la cour administrative d'appel de Lyon a, par un arrêt du 12 décembre 2013, retenu que le dommage engageait la responsabilité sans faute du centre hospitalier et a condamné cet établissement à verser la somme de 7 300 euros à M. B... et la somme de 6 660,37 euros à la caisse primaire d'assurance maladie de la Savoie en réparation des préjudices imputables à la pose de la prothèse défectueuse ; que, par le même arrêt, la cour a rejeté les conclusions du centre hospitalier tendant à ce que la société Groupe Lépine, producteur de la prothèse, le garantisse des condamnations prononcées contre lui ; que le centre hospitalier demande l'annulation de cet arrêt en tant qu'il rejette son appel en garantie ; <br/>
<br/>
              2. Considérant que, sur renvoi effectué par la décision visée ci-dessus du Conseil d'Etat, le Tribunal des conflits a déclaré, par une décision du 11 avril 2016, la juridiction administrative compétente pour statuer sur les conclusions du centre hospitalier de Chambéry tendant à ce que la société Groupe Lépine le garantisse des condamnations prononcées contre lui ;<br/>
<br/>
              3. Considérant que le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise, y compris lorsqu'il implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient ; que, par un arrêt du 21 décembre 2011, la Cour de justice de l'Union européenne a notamment jugé qu'un prestataire de services, tel qu'un prestataire de soins, dont la responsabilité est engagée à l'égard du bénéficiaire de la prestation en raison de l'utilisation, dans le cadre de celle-ci, d'un produit défectueux, doit avoir la possibilité de mettre en cause la responsabilité du producteur sur le fondement des règles issues de la directive 85/374/CEE du Conseil du 25 juillet 1985 modifiée, relative au rapprochement des dispositions législatives, réglementaires et administratives des États membres en matière de responsabilité du fait des produits défectueux ; que cette directive a été transposée en droit français par les dispositions des articles 1386-1 à 1386-18  du code civil, repris désormais aux articles 1245-1 à 1245-17 du code ; que, dès lors, un centre hospitalier qui a été condamné à indemniser un patient à raison des dommages résultant de l'implantation d'une prothèse défectueuse doit se voir reconnaître la possibilité de rechercher la responsabilité du producteur de la prothèse sur le fondement des articles 1386-1 à 1386-18 du code civil ; qu'il suit de là qu'en jugeant que le centre hospitalier de Chambéry ne pouvait pas utilement se prévaloir, à l'appui de son recours contre la société Groupe Lépine, des dispositions de la directive 85/374/CEE précitée ni des articles 1386-1 et suivants du code civil, au motif que la prestation de services en cause n'entrait pas dans le champ d'application de ces dispositions, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé en tant qu'il rejette l'appel en garantie formé par le centre hospitalier contre la société Groupe Lépine ; <br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que, contrairement à ce que soutient la société Groupe Lépine, le centre hospitalier de Chambéry n'a pas abandonné en appel les conclusions qu'il a présentées en première instance tendant à ce que cette société soit condamnée, sur le fondement des articles 1386-1 et suivants du code civil précités, à le garantir des condamnations prononcées contre lui ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 1386-1 du code civil, devenu l'article 1245-1 : " Le producteur est responsable du dommage causé par un défaut de son produit... " ; qu'aux termes de l'article 1386-17 du même code, devenu l'article 1245-16 : " L'action en réparation fondée sur les dispositions du présent titre se prescrit dans un délai de trois ans à compter de la date à laquelle le demandeur a eu ou aurait dû avoir connaissance du dommage, du défaut et de l'identité du producteur " ; qu'aux termes de l'article 1386-7 du même code, devenu l'article 1245-6 : " Si le producteur ne peut être identifié, le vendeur, le loueur, à l'exception du crédit-bailleur ou du loueur assimilable au crédit-bailleur, ou tout autre fournisseur professionnel, est responsable du défaut de sécurité du produit, dans les mêmes conditions que le producteur, à moins qu'il ne désigne son propre fournisseur ou le producteur, dans un délai de trois mois à compter de la date à laquelle la demande de la victime lui a été notifiée. / Le recours du fournisseur contre le producteur obéit aux mêmes règles que la demande émanant de la victime directe du défaut. Toutefois, il doit agir dans l'année suivant la date de sa citation en justice " ; <br/>
<br/>
              7. Considérant que le centre hospitalier, qui a utilisé la prothèse défectueuse dans le cadre d'une prestation de soins, n'a pas la qualité de fournisseur de cette prothèse au sens de l'article 1386-7 du code civil ; que la disposition du second alinéa de cet article, qui ne permet au fournisseur de former un recours contre le producteur que dans un délai d'un an à compter de sa citation en justice, ne lui est, par suite, pas applicable ; que, dès lors, le seul délai opposable au centre hospitalier de Chambéry est le délai de prescription de trois ans prévu à l'article 1386-17 du code civil cité ci-dessus ; que ce délai a commencé à courir à compter de la date à laquelle l'établissement a, à la fois, vu sa responsabilité engagée par la victime et eu connaissance du dommage, du défaut de la prothèse et de l'identité du producteur ; que le centre hospitalier, qui n'ignorait pas l'identité du producteur, ayant eu connaissance du défaut de la prothèse à la date de remise du rapport d'expertise le 12 juillet 2002, le délai de trois ans a couru en l'espèce à compter du 27 février 2003, date à laquelle M. B... a saisi le tribunal administratif de Grenoble de son recours indemnitaire dirigé contre l'hôpital ; que, dans ces conditions, l'action en garantie engagée le 11 juillet 2003 devant le tribunal administratif n'est pas prescrite ; <br/>
<br/>
              8. Considérant que le rapport déposé le 12 juillet 2002 par l'expert désigné par le juge des référés du tribunal administratif de Grenoble  conclut que le dommage subi par M. B... est exclusivement imputable à la défectuosité de la prothèse mise en place le 25 janvier 2000 ; que la circonstance que la société Groupe Lépine, producteur de cette prothèse, n'a pas participé à l'expertise ne fait pas obstacle à la prise en compte de ce rapport, dès lors que la société a pu en contester les conclusions devant les juges de première instance et d'appel ; que la société n'apporte pas d'éléments contestant utilement la conclusion de l'expertise sur la défectuosité de la prothèse et le lien de causalité entre cette défectuosité et le dommage ; que, dès lors, la société Groupe Lépine est responsable des dommages causés par la défectuosité de la prothèse en application de l'article 1386-1 du code civil précité et doit être condamnée à rembourser au centre hospitalier de Chambéry les sommes de 7 300 euros et 6 660,37 euros que celui-ci a été condamné à verser à M. B...et à la caisse primaire d'assurance maladie de la Savoie par l'arrêt du 12 décembre 2013 de la cour administrative d'appel de Lyon ; que la société remboursera également au centre hospitalier les intérêts sur la somme de 6 660,37 euros, que cet arrêt l'a condamné à verser à la caisse primaire d'assurance maladie ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier de Chambéry qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 12 décembre 2013 est annulé en tant qu'il rejette l'appel en garantie formé par le centre hospitalier de Chambéry contre la société Groupe Lépine.<br/>
<br/>
Article 2 : La société Groupe Lépine versera au centre hospitalier de Chambéry la somme de 7 300 euros et la somme de 6 660,37 euros assortie des intérêts au taux légal à compter du 19 mars 2003, avec capitalisation des intérêts au 3 février 2010 et à chaque échéance annuelle ultérieure.<br/>
<br/>
Article 3 : Les conclusions de la société Groupe Lépine présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Chambéry, à M. A... B..., à la caisse primaire d'assurance maladie de Savoie et à la société Groupe Lépine. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-21 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. SANTÉ PUBLIQUE. - RESPONSABILITÉ SANS FAUTE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ DU FAIT DES PRODUITS OU APPAREILS DE SANTÉ DÉFECTUEUX [RJ1] - CONSÉQUENCES DE LA DIRECTIVE 85/374/CEE - FACULTÉ DE L'HÔPITAL CONDAMNÉ SUR CE FONDEMENT À SE RETOURNER CONTRE LE PRODUCTEUR DU PRODUIT SUR LE FONDEMENT DE LA RESPONSABILITÉ DU FAIT DES PRODUITS DÉFECTUEUX [RJ2] - EXISTENCE - APPLICATION À L'IMPLANTATION D'UNE PROTHÈSE DÉFECTUEUSE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-005 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. - RESPONSABILITÉ SANS FAUTE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ DU FAIT DES PRODUITS OU APPAREILS DE SANTÉ DÉFECTUEUX [RJ1] - 1) FACULTÉ POUR UN ÉTABLISSEMENT DE SANTÉ CONDAMNÉ DE SE RETOURNER CONTRE LE PRODUCTEUR DU PRODUIT SUR LE FONDEMENT DE LA RESPONSABILITÉ DU FAIT DES PRODUITS DÉFECTUEUX - EXISTENCE  [RJ2] - 2) DÉLAI DE PRESCRIPTION - DÉLAI DE TROIS ANS, L'HÔPITAL N'AYANT PAS LA QUALITÉ DE FOURNISSEUR.
</SCT>
<ANA ID="9A"> 15-05-21 Le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise, y compris lorsqu'il implante, au cours de la prestation de soins, un produit défectueux dans le corps d'un patient.... ,,Par un arrêt du 21 décembre 2011, la Cour de justice de l'Union européenne a jugé qu'un prestataire de services, tel qu'un prestataire de soins, dont la responsabilité est engagée à l'égard du bénéficiaire de la prestation en raison de l'utilisation, dans le cadre de celle-ci, d'un produit défectueux, doit avoir la possibilité de mettre en cause la responsabilité du producteur sur le fondement des règles issues de la directive 85/374/CEE du Conseil du 25 juillet 1985, relative au rapprochement des dispositions législatives, réglementaires et administratives des États membres en matière de responsabilité du fait des produits défectueux. Dès lors, un centre hospitalier qui a été condamné à indemniser un patient à raison des dommages résultant de l'implantation d'une prothèse défectueuse a la possibilité de rechercher la responsabilité du producteur de la prothèse sur le fondement du régime de responsabilité du fait des produits défectueux prévu par les articles 1386-1 à 1386-18 du code civil.</ANA>
<ANA ID="9B"> 60-02-01-01-005 1) Un centre hospitalier qui a été condamné à indemniser un patient à raison des dommages résultant de l'implantation d'une prothèse défectueuse a la possibilité de rechercher la responsabilité du producteur de la prothèse sur le fondement du régime de responsabilité du fait des produits défectueux prévu par les articles 1386-1 à 1386-18 du code civil.,,,2) Le centre hospitalier, qui a utilisé la prothèse défectueuse dans le cadre d'une prestation de soins, n'a pas la qualité de fournisseur de cette prothèse au sens de l'article 1386-7 du code civil. La disposition du second alinéa de cet article, qui ne permet au fournisseur de former un recours contre le producteur que dans un délai d'un an à compter de sa citation en justice, ne lui est, par suite, pas applicable et le seul délai opposable au centre hospitalier est le délai de prescription de trois ans prévu à l'article 1386-17 du code civil.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 juillet 2003, Assistance publique-Hôpitaux de Paris c/ Mme Marzouk, n° 220437, p. 338.,,[RJ2] Rappr. CJUE, 21 décembre 2011, Centre hospitalier universitaire de Besançon, C-495/10. Cf. CE, 12 mars 2012, Centre hospitalier universitaire de Besançon, n° 327449, p. 85 ; CE, Section, 25 juillet 2013, M. Falempin, n° 339922, p. 226.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
