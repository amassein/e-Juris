<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032378003</ID>
<ANCIEN_ID>JG_L_2016_04_000000382652</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/80/CETATEXT000032378003.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 07/04/2016, 382652, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382652</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:382652.20160407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 16 juillet 2014 et le 7 mars 2016 au secrétariat du contentieux du Conseil d'Etat, l'association Entraide universitaire, la Fédération des associations pour adultes et jeunes handicapés (APAJH), l'Association parisienne travail épanouissement (APTE), l'Association pour l'insertion sociale et professionnelle des personnes handicapées (ADAPT), l'Association des paralysés de France (APF), la Fédération des établissements hospitaliers et d'aide à la personne (FEHAP), la Fédération nationale des associations de parents et amis employeurs gestionnaires d'établissements et services pour personnes handicapées mentales (FEGAPEI), l'association &#140;uvre Falret, l'Union nationale des association de parents de personnes handicapées mentales et de leurs amis (UNAPEI) et l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux (UNIOPSS) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé du 30 avril 2014 fixant les tarifs plafonds prévus au deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles applicables aux établissements et services mentionnés au a du 5° du I de l'article L. 312-1 du même code ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - l'arrêté du ministre des affaires sociales et de la santé et du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget du 22 avril 2013 fixant les tarifs plafonds prévus au deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles applicables aux établissements et services mentionnés au a du 5° du I de l'article L. 312-1 du même code ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte du second alinéa de l'article L. 314-4 du code de l'action sociale et des familles que le montant total annuel des dépenses prises en compte pour le calcul des dotations globales de fonctionnement des établissements et services d'aide par le travail mentionnés au a du 5° du I de l'article L. 312-1 du même code " est constitué en dotations régionales limitatives. Le montant de ces dotations régionales est fixé (...) en fonction des besoins de la population, des priorités définies au niveau national en matière de politique médico-sociale, en tenant compte de l'activité et des coûts moyens des établissements et services et d'un objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions. A cet effet, un arrêté interministériel fixe, annuellement, les tarifs plafonds ou les règles de calcul desdits tarifs plafonds (...) ainsi que les règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ". Pour l'application de ces dispositions, un arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé du 30 avril 2014, dont les associations requérantes demandent l'annulation pour excès de pouvoir, a fixé les tarifs plafonds applicables aux établissements et services d'aide par le travail pour l'année 2014.<br/>
<br/>
              Sur les conclusions à fin d'annulation pour excès de pouvoir :<br/>
<br/>
              En ce qui concerne l'article 1er de l'arrêté attaqué :<br/>
<br/>
              2. L'article 1er de l'arrêté attaqué dispose, d'une part, que les tarifs plafonds sont opposables aux établissements et services d'aide par le travail à l'exception de ceux ayant conclu un contrat pluriannuel d'objectifs et de moyens en cours de validité et, d'autre part, que " les contrats pluriannuels d'objectifs et de moyens signés à compter de la date de la publication du présent arrêté comportent un volet financier prévoyant, par groupe fonctionnel et pour la durée du contrat, les modalités de fixation annuelle de la tarification conformes aux règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ".<br/>
<br/>
              3. Aux termes de l'article L. 313-11 du code de l'action sociale et des familles : " (...) des contrats pluriannuels peuvent être conclus entre les personnes physiques et morales gestionnaires d'établissements et services et la ou les autorités chargées de l'autorisation et, le cas échéant, les organismes de protection sociale, afin notamment de permettre la réalisation des objectifs retenus par le schéma d'organisation sociale et médico-sociale dont ils relèvent, la mise en oeuvre du projet d'établissement ou de service ou de la coopération des actions sociales et médico-sociales. / Ces contrats fixent les obligations respectives des parties signataires et prévoient les moyens nécessaires à la réalisation des objectifs poursuivis, sur une durée maximale de cinq ans notamment dans le cadre de la tarification. Dans ce cas, les tarifs annuels ne sont pas soumis à la procédure budgétaire annuelle prévue aux II et III de l'article L. 314-7. (...) ". Aux termes de l'article R. 314-40 du même code : " Les éléments pluriannuels du budget sont fixés dans le cadre (...) du contrat pluriannuel prévu par l'article L. 313-11 (...). / Le contrat (...) comporte (...) alors un volet financier qui fixe, par groupes fonctionnels ou par section tarifaire selon la catégorie d'établissement ou de service, et pour la durée de la convention, les modalités de fixation annuelle de la tarification. / Ces modalités peuvent consister : / 1° Soit en l'application directe à l'établissement ou au service du taux d'évolution des dotations régionales limitatives mentionnées aux articles L. 314-3 et L. 314-4 ; / 2° Soit en l'application d'une formule fixe d'actualisation ou de revalorisation ;/ 3° Soit en la conclusion d'avenants annuels d'actualisation ou de revalorisation ".<br/>
<br/>
              4. Les dispositions précitées de l'article R. 314-40 du code de l'action sociale et des familles énumèrent de manière limitative les modalités selon lesquelles le volet financier des contrats pluriannuels prévus par l'article L. 313-11 du même code détermine, pour la durée de la convention, les modalités de fixation annuelle de la tarification. Au nombre de ces possibilités ne figurent pas celle consistant à imposer à ces contrats de contenir des modalités de fixation annuelle de la tarification " conformes aux règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ". Dès lors, sans qu'il soit besoin d'examiner les autres moyens de la requête dirigés contre ces dispositions, les associations requérantes sont fondées à soutenir que le troisième alinéa de l'article 1er de l'arrêté attaqué, qui précise que les contrats pluriannuels conclus à compter de la date de publication de l'arrêté comportent un volet financier prévoyant de telles modalités, méconnaît les dispositions de l'article R. 314-40 du même code.<br/>
<br/>
              En ce qui concerne les articles 3 et 4 de l'arrêté attaqué :<br/>
<br/>
              5. Par son article 3, l'arrêté attaqué prévoit que les établissements et services d'aide par le travail dont le tarif à la place au 31 décembre 2013 est supérieur aux tarifs plafonds prévus, pour l'année 2014, par les articles 1er et 2 de l'arrêté perçoivent, pour cet exercice, une dotation globale de financement correspondant au montant des charges nettes autorisé par l'autorité compétente de l'Etat au titre de l'exercice 2013. Par son article 4, l'arrêté attaqué dispose que " par dérogation au précédent article, les établissements et services (...) dont le tarif à la place constaté au 31 décembre 2013 est inférieur au tarif à la place constaté au 31 décembre 2011 en raison de l'application du mécanisme des tarifs plafonds (...) bénéficient d'une reconstitution de leur tarif à la place constaté au titre de 2011 et de l'application sur cette nouvelle base du taux d'évolution moyen régional attribué en 2012 et 2013 au titre de ces exercices, dans la limite des tarifs plafonds fixés à l'article 2 (...) ".<br/>
<br/>
              6. En premier lieu, les dispositions de ces articles ne méconnaissent pas l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité du droit.<br/>
<br/>
              7. En second lieu, si ces mêmes dispositions se réfèrent conjointement au " tarif à la place constaté au 31 décembre 2013 ", elles ne font pas application des dispositions de l'arrêté susvisé du 22 avril 2013 du ministre des affaires sociales et de la santé et du ministre chargé du budget. Dès lors, les associations requérantes ne sauraient utilement se prévaloir de l'erreur manifeste d'appréciation dont cet arrêté serait entaché.<br/>
<br/>
              8. En troisième lieu, les associations requérantes ne peuvent utilement se prévaloir des dispositions de l'article L. 351-6 du code de l'action sociale et des familles, qui sont relatives aux décisions du juge du tarif.<br/>
<br/>
              9. En dernier lieu, d'une part, le gel, à un niveau correspondant au montant des charges nettes autorisé au titre de l'année 2013, de la dotation globale de financement pour 2014 des établissements et services dont le tarif au 31 décembre 2013 est supérieur aux tarifs plafonds fixés par l'arrêté attaqué contribue à rapprocher les tarifs pratiqués du niveau des tarifs plafonds et participe ainsi de l'objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions. D'autre part, les dispositions de l'article 4 de l'arrêté attaqué établissent, pour les établissements et services dont le tarif à la place constaté au 31 décembre 2013 est inférieur au tarif à la place constaté au 31 décembre 2011, des modalités particulières de rapprochement des tarifs pratiqués du niveau des tarifs plafonds. Il ressort des pièces du dossier que ces modalités visent à compenser les effets de l'abattement de 2,5 % opéré au titre de l'exercice 2012 par un arrêté qui a fait l'objet d'une annulation pour excès de pouvoir et participent également, contrairement à ce que soutiennent les associations requérantes, de ce même objectif. Par suite, le moyen tiré de ce que les dispositions des articles 3 et 4 de l'arrêté attaqué ne répondraient pas à l'objectif assigné au pouvoir réglementaire par l'article L. 314-4 du code de l'action sociale et des familles doit être écarté.<br/>
<br/>
              10. Il résulte de ce qui précède que l'association Entraide universitaire et les autres associations requérantes sont seulement fondées à demander l'annulation des dispositions du troisième alinéa de l'article 1er de l'arrêté attaqué, qui en sont divisibles.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'association Entraide universitaire et les autres associations requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                          --------------<br/>
Article 1er : Le troisième alinéa de l'article 1er de l'arrêté du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé du 30 avril 2014 est annulé.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de l'association Entraide universitaire, de la Fédération des associations pour adultes et jeunes handicapés, de l'Association parisienne travail épanouissement, de l'Association pour l'insertion sociale et professionnelle des personnes handicapées, de l'Association des paralysés de France, de la Fédération des établissements hospitaliers et d'aide à la personne, de la Fédération nationale des associations de parents et amis employeurs gestionnaires d'établissements et services pour personnes handicapées mentales, de l'association &#140;uvre Falret, de l'Union nationale des association de parents de personnes handicapées mentales et de leurs amis et de l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association Entraide universitaire, à la Fédération des associations pour adultes et jeunes handicapés, à l'Association parisienne travail épanouissement, à l'Association pour l'insertion sociale et professionnelle des personnes handicapées, à l'Association des paralysés de France, à la Fédération des établissements hospitaliers et d'aide à la personne, à la Fédération nationale des associations de parents et amis employeurs gestionnaires d'établissements et services pour personnes handicapées mentales, à l'association &#140;uvre Falret, à l'Union nationale des association de parents de personnes handicapées mentales et de leurs amis, à l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux, au ministre des finances et des comptes publics et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
