<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854756</ID>
<ANCIEN_ID>JG_L_2020_12_000000440814</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854756.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/12/2020, 440814, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440814</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Eric Buge</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440814.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire, un mémoire récapitulatif, un mémoire en réplique et un nouveau mémoire, enregistrés les 24 mai, 3 juin, 23 juillet, 25 août et 14 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat, dans le dernier état de ses écritures :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le IV de l'article 1er de l'ordonnance n° 2020-313 du 25 mars 2020 relative aux adaptations des règles d'organisation et de fonctionnement des établissements sociaux et médico-sociaux ;<br/>
<br/>
              2°) d'enjoindre à l'Etat de verser l'aide sociale aux bénéficiaires et non aux services et d'organiser la concurrence entre services, pour permettre aux personnes âgées dépendantes de bénéficier de remplaçants lorsque les autres intervenants font défaut.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Buge, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme B... D..., rapporteurr publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le I de l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a habilité le Gouvernement, pendant trois mois, à prendre par ordonnances, dans les conditions prévues à l'article 38 de la Constitution, " 5° Afin, face aux conséquences de l'épidémie de covid-19, d'assurer la continuité de l'accompagnement et la protection des personnes en situation de handicap et des personnes âgées vivant à domicile ou dans un établissement ou service social et médico-social, des mineurs et majeurs protégés et des personnes en situation de pauvreté, toute mesure : / a) Dérogeant aux dispositions de l'article L. 312-1 et du chapitre III du titre Ier du livre III du code de l'action sociale et des familles pour permettre aux établissements et services sociaux et médico-sociaux autorisés d'adapter les conditions d'organisation et de fonctionnement de l'établissement ou du service et de dispenser des prestations ou de prendre en charge des publics destinataires figurant en dehors de leur acte d'autorisation (...) ". <br/>
<br/>
              2. Sur le fondement de ces dispositions, le Gouvernement a adopté l'ordonnance du 25 mars 2020 relative aux adaptations des règles d'organisation et de fonctionnement des établissements sociaux et médico-sociaux. M. A... demande l'annulation pour excès de pouvoir du IV de l'article 1er de cette ordonnance, qui prévoit que : " En cas de sous-activité ou de fermeture temporaire résultant de l'épidémie de covid-19, le niveau de financement des établissements et services mentionnés au I de l'article L. 312-1 du code de l'action sociale et des familles n'est pas modifié. Pour la partie de financement des établissements et services sociaux et médico-sociaux mentionnés au I du même article L. 312-1 qui ne relève pas de dotation ou de forfait global, la facturation est établie à terme mensuel échu sur la base de l'activité prévisionnelle, sans tenir compte de la sous-activité ou des fermetures temporaires résultant de l'épidémie de covid-19. (...) ". Au nombre des services mentionnés au I de l'article L. 312-1 du code de l'action sociale et des familles, figurent, à ses 6° et 7°, les services qui apportent aux personnes âgées ou handicapées à domicile une assistance dans les actes quotidiens de la vie ou une aide à l'insertion sociale. En vertu de l'article 2 de la même ordonnance, ces dispositions sont applicables à compter du 12 mars 2020 et jusqu'à la date de cessation de l'état d'urgence sanitaire déclaré par l'article 4 de la loi du 23 mars 2020, le cas échéant prolongé dans les conditions prévues par cet article, et les mesures prises en application de ces mêmes dispositions prennent fin trois mois au plus tard après la même date.<br/>
<br/>
              3. En premier lieu, faute d'avoir été introduite par mémoire distinct dans les formes prescrites par l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel et par l'article R. 77113 du code de justice administrative, la question de la conformité des dispositions critiquées, intervenues dans le domaine de la loi, au principe de sauvegarde de la dignité de la personne humaine, principe à valeur constitutionnelle relevant des droits et libertés garantis par la Constitution, est irrecevable.<br/>
<br/>
              4. En deuxième lieu, il résulte des articles L. 232-6 et L. 232-7 du code de l'action sociale et des familles que le bénéficiaire de l'allocation personnalisée d'autonomie à domicile choisit librement d'utiliser l'allocation à la rémunération d'un ou de salariés ou d'un service d'aide à domicile et l'article L. 232-15 du même code prévoit que la partie de l'allocation destinée à rémunérer un service d'aide à domicile autorisé peut être versée au bénéficiaire de l'allocation sous forme de chèque emploi-service universel, ou bien directement au service choisi par le bénéficiaire, lequel demeure libre de choisir un autre service. Les dispositions critiquées ne modifient pas ces dispositions et n'y apportent aucune dérogation. Par suite, le requérant n'est pas fondé à soutenir qu'elles méconnaîtraient le droit des bénéficiaires de l'allocation personnalisée d'autonomie, découlant de ces dispositions, de recourir au prestataire d'aide à domicile de son choix.  <br/>
<br/>
              5. En troisième lieu, la possibilité pour le département de verser directement au service d'aide à domicile choisi par le bénéficiaire la part de l'allocation personnalisée d'autonomie destinée à cette forme d'aide résulte des dispositions de l'article L. 232-15 du code de l'action sociale et des familles mentionné ci-dessus, de même que, pour la prestation de compensation du handicap, de l'article L. 245-12 du code de l'action sociale et des familles, et non des dispositions de l'ordonnance du 25 mars 2020 que le requérant critique. Par suite, les moyens qu'il tire de l'illégalité du versement direct aux services d'aide à domicile de sommes relevant de l'aide sociale ou, en tout état de cause, de la méconnaissance des dispositions applicables aux délégations de service public ne peuvent qu'être écartés.<br/>
<br/>
              6. En quatrième lieu, les dispositions contestées adaptent les règles de financement des établissements et services sociaux et médico-sociaux au contexte de l'épidémie de covid-19 afin de préserver l'offre de services sociaux et médico-sociaux, en dépit des refus ou suspensions d'intervention de certains de leurs bénéficiaires, et de l'indisponibilité d'une partie du personnel du fait de la maladie ou de la garde d'enfants au domicile, et d'assurer ainsi la pérennité de la prise en charge des personnes bénéficiant de ces services. Dans ces conditions, le Gouvernement n'a pas commis d'erreur manifeste d'appréciation en prévoyant un financement sur la base de l'activité prévisionnelle, alors même qu'une partie des heures n'est pas réalisée.<br/>
<br/>
              7. En cinquième lieu, aux termes du 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne : " Sauf dérogations prévues par les traités, sont incompatibles avec le marché intérieur, dans la mesure où elles affectent les échanges entre États membres, les aides accordées par les États ou au moyen de ressources d'État sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions ". Le soutien aux services d'aide à domicile pouvant résulter de la mise en oeuvre des dispositions critiquées étant, en tout état de cause, insusceptible d'affecter les échanges entre Etats membres, le requérant ne peut utilement invoquer la méconnaissance de ces stipulations.   <br/>
<br/>
              8. Enfin, M. A... n'invoque aucun texte ou aucun principe dont découlerait un " droit à la protection contre les menaces ". Par suite, le moyen tiré de sa méconnaissance ne peut qu'être écarté. <br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non-recevoir soulevée par le ministre des solidarités et de la santé, que M. A... n'est pas fondé à demander l'annulation des dispositions qu'il attaque.<br/>
<br/>
              10. La présente décision n'implique aucune mesure d'exécution. Par suite, les conclusions de M. A... aux fins d'injonction ne peuvent qu'être écartées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
