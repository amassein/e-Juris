<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025918302</ID>
<ANCIEN_ID>JG_L_2012_05_000000347101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/83/CETATEXT000025918302.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/05/2012, 347101</TITRE>
<DATE_DEC>2012-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347101.20120515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 28 février et 30 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION INTERCO CFDT, dont le siège est 47/49, avenue Simon Bolivar à Paris (75950), représentée par sa secrétaire générale ; la FEDERATION INTERCO CFDT demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2010-1733 du 30 décembre 2010 relatif aux comités d'agence, à la représentation syndicale, aux délégués du personnel et aux emplois de direction des agences régionales de santé et modifiant diverses dispositions du code de la santé publique ;<br/>
<br/>
              2°) d'enjoindre aux ministres concernés de modifier les dispositions des articles R. 1432-116 à R. 1432-120 et R. 1432-124 du code de la santé publique afin de les rendre applicables à l'ensemble du personnel de l'agence régionale de santé, les dispositions de l'article R. 1432-121 afin de permettre la prise en compte des résultats des élections au niveau de l'ensemble des suffrages exprimés dans les deux collèges électoraux, et les dispositions de l'article R. 1432-155 afin de ne pas imputer, pour les personnels relevant du droit public, les jours de formation des représentants du personnel au CHSCT sur la formation syndicale prévue par les dispositions 34-7° de la loi du 11 janvier 1984 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la charte des droits fondamentaux de l'Union européenne ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu la loi n° 2010-751 du 5 juillet 2010 ; <br/>
<br/>
              Vu le décret n° 82-450 du 28 mai 1982 ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de la FEDERATION INTERCO CFDT, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de la FEDERATION INTERCO CFDT ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 1432-11 du code de la santé publique, dans sa rédaction issue de l'article 27 de la loi du 5 juillet 2010 relative à la rénovation du dialogue social : " I. - Il est institué dans chaque agence régionale de santé un comité d'agence et un comité d'hygiène, de sécurité et des conditions de travail, compétents pour l'ensemble du personnel de l'agence. 1. Le comité d'agence exerce les compétences prévues au II de l'article 15 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat et celles prévues au chapitre III du titre II du livre III de la deuxième partie du code du travail, sous réserve des adaptations prévues par décret en Conseil d'Etat en application de l'article L. 2321-1 du même code. (...) Les représentants du personnel siégeant au comité d'agence sont élus au scrutin de liste avec représentation proportionnelle. L'élection a lieu par collèges dans des conditions fixées par décret en Conseil d'Etat. Les candidatures sont présentées par les organisations syndicales qui remplissent les conditions suivantes : 1° Pour le collège des agents de droit privé régis par les conventions collectives applicables au personnel des organismes de sécurité sociale, celles prévues par l'article L. 2324-4 du code du travail ; 2° Pour le collège des fonctionnaires, des agents de droit public et des agents contractuels de droit public, celles prévues par l'article 9 bis de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires. 2. Le comité d'hygiène, de sécurité et des conditions de travail est institué dans les conditions prévues par l'article 16 de la loi n° 84-16 du 11 janvier 1984 précitée. Il exerce les compétences du comité institué par ce même article et celles prévues au chapitre II du titre Ier du livre VI de la quatrième partie du code du travail, sous réserve des adaptations fixées par décret en Conseil d'Etat. (...) Pour l'application des deux alinéas précédents et pour l'appréciation de la représentativité prévue à l'article L. 2122-1 du code du travail, les modalités de prise en compte des résultats électoraux sont fixées, par décret en Conseil d'Etat, de façon à garantir la représentation des agents de chacun des deux collèges de personnel mentionnés aux 1° et 2° du 1 du I du présent article. (...) " ; que la FEDERATION INTERCO CFDT demande l'annulation du décret du 30 décembre 2010 relatif aux comités d'agence, à la représentation syndicale et aux délégués du personnel et aux emplois de direction dans les agences régionales de santé, pris pour l'application de ces dispositions ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              Considérant que la FEDERATION INTERCO CFDT ne peut utilement soutenir que les consultations préalables à l'édiction du décret litigieux auraient méconnu les stipulations des articles 12 et 28 de la charte des droits fondamentaux de l'Union européenne, lesquelles ne peuvent être invoquées que pour des dispositions mettant en oeuvre le droit de l'Union européenne ; qu'elle ne saurait par ailleurs soutenir que, de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, découlerait une obligation de consultation préalable des organisations syndicales sur le décret litigieux ; qu'enfin, eu égard à l'objet de ce décret, la FEDERATION INTERCO CFDT ne saurait invoquer la méconnaissance des dispositions de l'article L. 1 du code du travail aux termes desquelles : " Tout projet de réforme envisagé par le Gouvernement qui porte sur les relations individuelles et collectives du travail, (...) et qui relève du champ de la négociation nationale et interprofessionnelle fait l'objet d'une concertation préalable avec les organisations syndicales de salariés et d'employeurs représentatives au niveau national et interprofessionnel (...) " ;<br/>
<br/>
              Considérant que l'article L. 2323-19 du code du travail dispose que " Le comité d'entreprise est informé et consulté sur les modifications de l'organisation économique ou juridique de l'entreprise (...) " ; que la FEDERATION INTERCO CFD n'est pas fondée à soutenir que les comités d'entreprise des organismes locaux d'assurance maladie auraient dû être préalablement consultés, le décret attaqué n'entraînant par lui-même aucune modification de l'organisation juridique ou économique de ces organismes ; qu'elle ne saurait par ailleurs ailleurs se prévaloir, sur ce point, d'un prétendu principe de parallélisme des formes qui imposerait à un texte modificatif d'être soumis aux respect de toutes les consultations ayant précédé le texte qu'il modifie ;<br/>
<br/>
              Considérant que le moyen tiré de ce que le comité technique paritaire ministériel des affaires sociales n'a pas été consulté sur le projet de décret manque en fait ; qu'il ne ressort pas du dossier que la consultation du comité technique paritaire ministériel aurait été entachée d'irrégularité ;<br/>
<br/>
              Considérant que si l'article 2 du décret du 28 mai 1982 relatif au conseil supérieur de la fonction publique de l'Etat alors en vigueur dispose que celui-ci est notamment chargé d'examiner " les questions d'ordre général relatives (...) aux restructurations administratives (...) ", le décret attaqué n'a pas pour objet de mettre en oeuvre une restructuration administrative ; que, par suite, le moyen tiré de ce que le Conseil supérieur de la fonction publique de l'Etat aurait dû être consulté préalablement à son édiction doit, en tout état de cause, être écarté ; <br/>
<br/>
              Considérant, enfin, qu'il ressort de la copie de la minute de la section de l'administration et de la section sociale du Conseil d'Etat, telle qu'elle a été produite au dossier par le ministre du travail, de l'emploi et de la santé, que le texte publié ne contient pas de dispositions qui diffèreraient à la fois du projet initial du Gouvernement et du texte adopté par le Conseil d'Etat ; que la FEDERATION INTERCO CFDT n'est, dès lors, pas fondée à soutenir que le décret litigieux aurait, pour ce motif, été pris selon une procédure irrégulière ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 1432-121, introduit dans le code de la santé publique par le décret attaqué : " Pour l'appréciation de la représentativité des organisations syndicales, 1°) le pourcentage des voix exprimées aux élections aux comités d'agence en faveur des organisations mentionnées aux articles L. 2122-1 à L. 2122-3 du code du travail s'apprécie au niveau de chacun des deux collèges ou sous-collèges pour les organisations syndicales mentionnées à l'article L. 2122-2 du même code, 2°) pour l'application de l'article L. 2143-3 du code du travail, le pourcentage de voix exprimées aux élections aux comités d'agence en faveur du candidat s'apprécie au niveau du collège ou du sous-collège dans lequel il s'est présenté " ; que, contrairement à ce que soutient la FEDERATION INTERCO CFDT, l'appréciation de représentativité effectuée, en vertu de ces dispositions, au niveau de chacun des deux collèges électoraux, se borne à mettre en oeuvre, sans en méconnaître la portée, les dispositions citées ci-dessus de l'article L. 1432-11 du code de la santé publique qui prévoient que la prise en compte des résultats électoraux doit " garantir la représentation des agents de chacun des deux collèges de personnel " ; que ces mêmes dispositions de l'article L. 1432-11 du code de la santé publique ayant expressément pour objet d'adapter aux agences régionales de santé les exigences de l'article L. 2122-1 du code du travail, la fédération requérante ne saurait, par suite, utilement soutenir que le nouvel article R. 1432-121 du code de la santé publique viole les dispositions de l'article L. 2122-1 du code du travail : <br/>
<br/>
              Considérant que le décret attaqué institue à l'article R. 1432-155 le droit pour les représentants du personnel au comité d'hygiène, de sécurité et des conditions de travail de bénéficier d'une formation nécessaire à l'exercice de leur mission ; qu'aux termes du deuxième alinéa de ce nouvel article R. 1432-155 , " Pour les représentants appartenant au premier collège, mentionné à l'article R. 1432-78, les jours de congés obtenus en application de l'alinéa précédent s'imputent sur leurs droits à congés de formation syndicale prévus par le 7° de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat " ; que ces dispositions instaurent au bénéfice des agents publics élus au titre du " premier collège " le droit à des congés de formation distincts des congés de formation syndicale prévus par l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dont peuvent bénéficier ceux de ces agents qui ont la qualité de fonctionnaire de l'Etat ; que, s'il était loisible au décret litigieux de fixer des règles de cumul applicables au nouveau congé qu'il institue, de telles règles ne sauraient en revanche avoir légalement pour effet de diminuer le nombre de jours de congé de formation syndicale dont les représentants au comité d'hygiène, de sécurité et des conditions de travail qui sont fonctionnaires de l'Etat peuvent bénéficier au titre de l'article 34 de la loi du 11 janvier 1984 ; qu'ainsi, sans qu'il soit besoin d'examiner le moyen tiré de la rupture d'égalité entre les agents de droit public et agents de droit privé, la fédération requérante est fondée à demander l'annulation du deuxième alinéa de l'article R. 1432-155 ;<br/>
<br/>
              Considérant que l'article R. 1432-116, inséré dans le code de la santé publique par le décret attaqué prévoit que : " les délégués du personnel représentent les agents de droit privé régis par les conventions collectives applicables au personnel des organismes de sécurité sociale " ; que contrairement à ce que soutient la fédération requérante, le principe de participation n'impose pas que des délégués du personnel soient également désignés par les agents publics des agences régionales de santé, dès lors que la participation de ces derniers à la détermination de leurs conditions de travail est assurée par des dispositions qui leur sont propres ; que la FEDERATION INTERCO CFDT n'est donc pas fondée à soutenir que le décret serait, sur ce point, entaché d'illégalité ;<br/>
<br/>
              Considérant, enfin, que la FEDERATION INTERCO CFDT soutient que l'article R. 1432-124, introduit dans le code de la santé publique par le décret attaqué, est illégal  en ce qu'il subordonne la validité des accords collectifs de travail à leur signature par des organisations syndicales ayant réuni au moins 30 % des suffrages exprimés en fonction des seuls résultats électoraux au " second collège " du comité d'agence, qui ne comprend que les agents de droit privé ; que, toutefois, ces accords collectifs étant destinés à ne régir que les conditions de travail des seuls salariés de droit privé, ces dispositions ne méconnaissent ni le principe d'égalité, ni le droit des agents public à participer à la détermination de leurs conditions de travail ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la FEDERATION INTERCO CFDT n'est fondée à demander l'annulation du décret du 30 décembre 2010 qu'en tant qu'il introduit le deuxième alinéa de l'article R. 1432-155 du code de la santé publique ; qu'il y a lieu, par suite, de rejeter les conclusions à fin d'injonction présentées par cette fédération sur le fondement des dispositions de l'article L. 911-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat  le versement de la somme de 1 500 euros à la FEDERATION INTERCO CFDT au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le deuxième alinéa de l'article R. 1432-155 du code de la santé publique introduit par le décret n° 2010-1733 du 30 décembre 2010 est annulé.<br/>
Article 2 : Le surplus de la requête de la FEDERATION INTERCO CFDT est rejeté.<br/>
Article 3 : L'Etat versera la somme de 1 500 euros à la FEDERATION INTERCO CFDT au titre de l'article L. 761-1 du code de la justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la FEDERATION INTERCO CFDT, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-05 TRAVAIL ET EMPLOI. SYNDICATS. - CONGÉ DE FORMATION SYNDICALE - DÉCRET PRÉVOYANT QUE LES JOURS DE CONGÉ POUR FORMATION À LEUR MISSION DES MEMBRES DU CHSCT S'IMPUTENT SUR LE NOMBRE DE JOURS DE CONGÉ DE FORMATION SYNDICALE - ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 66-05 Illégalité du décret n° 2010-1733 du 30 décembre 2010 pour avoir prévu que les jours de congé reconnus aux représentants du personnel au comité d'hygiène, de sécurité et des conditions de travail (CHSCT) afin de bénéficier d'une formation nécessaire à l'exercice de leur mission s'imputent sur le nombre de jours de congé de formation syndicale dont ces représentants peuvent par ailleurs bénéficier en vertu du 7° de l'article 34 de la loi n° 84-16 du 11 janvier 1984.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
