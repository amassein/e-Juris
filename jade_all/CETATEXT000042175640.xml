<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175640</ID>
<ANCIEN_ID>JG_L_2020_07_000000422674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 29/07/2020, 422674, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:422674.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. et Mme B... et Sybille A... ont demandé au tribunal administratif de Poitiers d'annuler la décision du 20 février 2017 du directeur de la caisse d'allocations familiales de la Vienne de récupérer un indu d'aide exceptionnelle de fin d'année 2015 d'un montant de 508,03 euros, ainsi que la décision du 19 mai 2017 rejetant leur recours gracieux, et de les décharger de l'obligation de payer cette somme. Par un jugement n° 1701605 du 28 juin 2018, le tribunal administratif de Poitiers a rejeté leur demande.<br/>
<br/>
              Sous le n° 422674, par un pourvoi et un nouveau mémoire, enregistrés les 27 juillet 2018 et 9 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, du département de la Vienne et de la caisse d'allocations familiales de la Vienne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Sous le n° 422681, par un pourvoi et deux nouveaux mémoires, enregistrés les 27 juillet 2018, 7 et 9 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, du département de la Vienne et de la caisse d'allocation familiales de la Vienne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2015-1870 du 30 décembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. et Mme A..., et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la caisse d'allocations familiales de la Vienne et du département de la Vienne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de M. et Mme A... présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge du fond qu'un contrôle diligenté par la caisse d'allocations familiales de la Vienne a révélé que M. et Mme A..., allocataires du revenu de solidarité active, avaient omis de déclarer, au titre de leurs ressources, la pension alimentaire mensuelle de 530 euros qui leur était versée en 2015 et 2016 par la mère de M. A.... Le directeur de la caisse d'allocations familiales de la Vienne leur a réclamé, par une décision du 20 février 2017, le reversement d'un indu d'aide exceptionnelle de fin d'année de 508,03 euros au titre de l'année 2015, puis, par une décision du 14 mars 2017, celui d'un indu de revenu de solidarité active, de prime d'activité et d'aide exceptionnelle de fin d'année s'élevant au total à la somme de 6 259,27 euros au titre de la période du 1er avril 2015 au 31 octobre 2016. Le 19 mai 2017, la caisse d'allocations familiales a rejeté le recours gracieux formé par les intéressés le 13 mars 2017 contre la décision du 20 février 2017. Par une décision du 29 juin 2017, le président du conseil départemental de la Vienne a confirmé l'indu de revenu de solidarité active et rejeté la demande de remise gracieuse de leur dette présentée par M. et Mme A.... Ces derniers se pourvoient en cassation contre les jugements du 28 juin 2018 par lesquels le tribunal administratif de Poitiers a rejeté leurs contestations des décisions du directeur de la caisse d'allocation familiale et du président du conseil départemental de la Vienne relatives au revenu de solidarité active et à l'aide exceptionnelle de fin d'année.<br/>
<br/>
              Sur l'indu de revenu de solidarité active :<br/>
<br/>
              En ce qui concerne le droit de communication :<br/>
<br/>
              3. Aux termes de l'article L. 262-16 du code de l'action sociale et des familles : " Le service du revenu de solidarité active est assuré, dans chaque département, par les caisses d'allocations familiales et, pour leurs ressortissants, par les caisses de mutualité sociale agricole ". Aux termes de l'article L. 262-40 de ce code : " (...) Les organismes chargés de son versement réalisent les contrôles relatifs au revenu de solidarité active selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale. (...) ". A ce titre, l'article L. 114-19 du code de la sécurité sociale prévoit que le droit de communication permet à certains agents des organismes de sécurité sociale d'obtenir, auprès de personnes publiques et privées que l'article L. 114-20 du même code désigne par renvoi au livre des procédures fiscales, sans que le secret professionnel ne s'y oppose, les documents et informations nécessaires à l'exercice des missions de contrôle ou de recouvrement de prestations indûment versées qu'il définit. L'article L. 114-21 du code de la sécurité sociale dispose que l'organisme ayant usé de ce droit est tenu d'informer la personne à l'encontre de laquelle est prise la décision de supprimer le service d'une prestation ou de mettre des sommes en recouvrement " de la teneur et de l'origine des informations et documents obtenus auprès de tiers sur lesquels il s'est fondé pour prendre cette décision " et qu'il communique, avant la mise en recouvrement ou la suppression du service de la prestation, une copie de ces documents à la personne qui en fait la demande.<br/>
<br/>
              4. Il résulte des dispositions de l'article L. 262-16 du code de l'action sociale et des familles, rappelées au point précédent, que les caisses d'allocations familiales et les caisses de mutualité sociale agricole, chargées du service du revenu de solidarité active, réalisent les contrôles relatifs à cette prestation d'aide sociale selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale, au nombre desquels figurent le droit de communication instauré par l'article L. 114-19 du code de la sécurité sociale au bénéfice des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations qu'ils servent, ainsi que les garanties procédurales s'attachant, en vertu de l'article L. 114-21 du même code, à l'exercice de ce droit par un organisme de sécurité sociale. <br/>
<br/>
              5. En l'espèce, le tribunal administratif, pour écarter le moyen tiré de la méconnaissance de l'article L. 114-21 du code de la sécurité sociale, s'est fondé sur la circonstance que M. et Mme A... avaient été informés par courrier du 14 novembre 2016, antérieurement à la décision de récupération d'indu, de la transmission par les services fiscaux à la caisse d'allocations familiales de leurs ressources déclarés au titre de l'année 2015 et que les intéressés y avaient répondu en indiquant avoir continué à percevoir au cours de l'année 2016 la pension alimentaire qu'ils s'étaient abstenus de déclarées. En jugeant que les dispositions de l'article L. 114-21 du code de la sécurité sociale n'avaient, dès lors, pas été méconnues, le tribunal administratif n'a, en tout état de cause, pas commis d'erreur de droit. <br/>
<br/>
              En ce qui concerne l'examen du recours administratif préalable dirigé contre la récupération de l'indu de revenu de solidarité active :<br/>
<br/>
              6. En vertu du 1° du I de l'article L. 262-25 du code de l'action sociale et des familles, une convention, conclue entre le département et chacun des organismes payeurs mentionnés à l'article L. 262-16, précise en particulier les conditions dans lesquelles le revenu de solidarité active est servi et contrôlé. Le premier alinéa de l'article L. 262-47 du même code prévoit que : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil départemental. Ce recours est, dans les conditions et limites prévues par la convention mentionnée à l'article L. 262-25, soumis pour avis à la commission de recours amiable qui connaît des réclamations relevant de l'article L. 142-1 du code de la sécurité sociale.(...) ". Enfin, aux termes du premier alinéa de l'article R. 262-89 du même code : " Sauf lorsque la convention mentionnée à l'article L. 262-25 en dispose autrement, ce recours est adressé par le président du conseil départemental pour avis à la commission de recours amiable mentionnée à l'article R. 142-1 du code de la sécurité sociale ". Il résulte de ces dispositions que la convention conclue entre le département et la caisse d'allocations familiales ne peut légalement prévoir qu'aucun recours administratif préalable dirigé contre une décision relative au revenu de solidarité active n'est soumis pour avis à la commission de recours amiable. <br/>
<br/>
              7. M. et Mme A... sont, par suite, fondés à soutenir que le tribunal administratif a commis une erreur de droit en jugeant que la convention conclue entre le département et la caisse d'allocations familiales de la Vienne avait pu légalement exclure la consultation de la commission de recours amiable sur quelque réclamation dirigée contre une décision relative au revenu de solidarité active que ce soit. Toutefois, l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière a été prise pour son application ou y trouve sa base légale. La décision par laquelle le président du conseil départemental confirme la récupération par la caisse d'allocations familiales d'un indu de revenu de solidarité active ne constitue pas un acte pris pour l'application des dispositions de la convention conclue entre cette caisse et le département en application de l'article L. 262-25 du code de l'action sociale et des familles relatives à la saisine de la commission de recours amiable, lesquelles ne constituent pas davantage sa base légale. Par suite, M. et Mme A... ne pouvaient utilement invoquer, par voie d'exception, l'illégalité de la convention conclue entre le département et la caisse d'allocations familiales de la Vienne. Ce motif doit être substitué au motif erroné par lequel le tribunal administratif a écarté cette exception d'illégalité.<br/>
<br/>
              En ce qui concerne la matérialité de l'indu :<br/>
<br/>
              8. M. et Mme A... se bornaient devant le tribunal administratif à soutenir qu'il appartenait au département de justifier de la matérialité de l'indu, sans aucunement contester ni le montant des sommes qu'ils avaient effectivement perçues, ni le calcul de l'indu réclamé, et sans davantage soutenir que la lettre du 14 mars 2017 par laquelle l'indu leur avait été réclamé n'aurait pas comporté la mention du motif, de la nature et du montant des sommes réclamées conformément à l'article R. 133-9-2 du code de la sécurité sociale. Par suite, les requérants ne sont, en tout état de cause, pas fondés à soutenir que le tribunal administratif aurait commis une erreur de droit en écartant leur moyen sans rechercher si le courrier du 14 mars 2017 comportait ces éléments.<br/>
<br/>
              Sur le refus de remise gracieuse de l'indu de revenu de solidarité active :<br/>
<br/>
              9. D'une part, l'article L. 262-17 du code de l'action sociale et des familles dispose que : " Lors du dépôt de sa demande, l'intéressé reçoit, de la part de l'organisme auprès duquel il effectue le dépôt, une information sur les droits et devoirs des bénéficiaires du revenu de solidarité active (...) " et l'article R. 262-37 du même code prévoit que : " Le bénéficiaire de l'allocation de revenu de solidarité active est tenu de faire connaître à l'organisme chargé du service de la prestation toutes informations relatives à sa résidence, à sa situation de famille, aux activités, aux ressources et aux biens des membres du foyer ; il doit faire connaître à cet organisme tout changement intervenu dans l'un ou l'autre de ces éléments ".<br/>
<br/>
              10. D'autre part, aux termes de l'article L. 262-46 du code de l'action sociale et des familles : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / (...) La créance peut être remise ou réduite par le président du conseil départemental en cas de bonne foi ou de précarité de la situation du débiteur, sauf si cette créance résulte d'une manoeuvre frauduleuse ou d'une fausse déclaration (...) ". Il résulte de ces dispositions qu'un allocataire du revenu de solidarité active ne peut bénéficier d'une remise gracieuse de la dette résultant d'un paiement indu d'allocation, quelle que soit la précarité de sa situation, lorsque l'indu trouve sa cause dans une manoeuvre frauduleuse de sa part ou dans une fausse déclaration, laquelle doit s'entendre comme désignant les inexactitudes ou omissions qui procèdent d'une volonté de dissimulation de l'allocataire caractérisant de sa part un manquement à ses obligations déclaratives.<br/>
<br/>
              11. Lorsqu'il statue sur un recours dirigé contre une décision rejetant une demande de remise gracieuse d'un indu de revenu de solidarité active, il appartient au juge administratif d'examiner si une remise gracieuse totale ou partielle est justifiée et de se prononcer lui-même sur la demande en recherchant si, au regard des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision, la situation de précarité du débiteur et sa bonne foi justifient que lui soit accordée une remise. Lorsque l'indu résulte de ce que l'allocataire a omis de déclarer certaines de ses ressources, il y a lieu, pour apprécier la condition de bonne foi de l'intéressé, hors les hypothèses où les omissions déclaratives révèlent une volonté manifeste de dissimulation ou, à l'inverse, portent sur des ressources dépourvues d'incidence sur le droit de l'intéressé au revenu de solidarité active ou sur son montant, de tenir compte de la nature des ressources ainsi omises, de l'information reçue et de la présentation du formulaire de déclaration des ressources, du caractère réitéré ou non de l'omission, des justifications données par l'intéressé ainsi que de toute autre circonstance de nature à établir que l'allocataire pouvait de bonne foi ignorer qu'il était tenu de déclarer les ressources omises. A cet égard, si l'allocataire a pu légitimement, notamment eu égard à la nature du revenu en cause et de l'information reçue, ignorer qu'il était tenu de déclarer les ressources omises, la réitération de l'omission ne saurait alors suffire à caractériser une fausse déclaration. <br/>
<br/>
              12. Pour rejeter le recours de M. et Mme A... contre le refus de remise gracieuse de l'indu dont le remboursement leur était demandé, le tribunal s'est fondé sur la circonstance qu'il paraissait peu vraisemblable qu'ils aient pu légitimement ignorer que la pension alimentaire qui leur était versée par la mère de M. A..., qu'ils n'avaient pas omis de déclarer aux services fiscaux, devait être également déclarée au titre du revenu de solidarité active. Eu égard à la nature, au montant et à la régularité de la ressource en cause, le tribunal n'a ce faisant pas commis d'erreur de droit.<br/>
<br/>
              Sur l'indu d'aide exceptionnelle de fin d'année :<br/>
<br/>
              13. L'article 3 du décret du 30 décembre 2015 portant attribution d'une aide exceptionnelle de fin d'année à certains allocataires du revenu de solidarité active et aux bénéficiaires de l'allocation de solidarité spécifique, de la prime forfaitaire pour reprise d'activité et de l'allocation équivalent retraite dispose que : " Une aide exceptionnelle est attribuée aux allocataires du revenu de solidarité active qui ont droit à cette allocation au titre du mois de novembre 2015 ou, à défaut, du mois de décembre 2015, sous réserve que le montant dû au titre de ces périodes ne soit pas nul et à condition que les ressources du foyer, appréciées selon les dispositions prises en vertu de l'article L. 262-3 du code de l'action sociale et des familles, n'excèdent pas le montant forfaitaire mentionné à l'article L. 262-2 du même code (...) ".<br/>
<br/>
              14. Pour rejeter comme irrecevable le recours de M. et Mme A... contre la décision du 20 février 2017 par laquelle le directeur de la caisse d'allocations familiales de la Vienne a décidé la récupération d'un indu d'aide exceptionnelle de fin d'année 2015 d'un montant de 508,03 euros, ainsi que la décision du 19 mai 2017 rejetant leur recours gracieux, le tribunal administratif de Poitiers s'est fondé sur la circonstance que la décision du 20 février 2017 devait être regardée comme ayant été retirée, avant l'introduction du recours, par celle du 14 mars 2017 de récupérer les sommes indument versées au titre du revenu de solidarité active, de la prime d'activité et de l'aide exceptionnelle d'activité pour la période comprise entre le 1er avril 2015 et le 31 octobre 2016.<br/>
<br/>
              15. En premier lieu, le tribunal n'a pas commis d'erreur de droit en jugeant que la décision du directeur de la caisse d'allocations familiales du 14 mars 2017, modifiant le montant de l'indu d'aide exceptionnelle de fin d'année notifié aux requérants par la décision du 20 février 2017, avait retiré cette dernière décision, sans que le directeur de la caisse d'allocations familiales puisse être regardé comme étant revenu sur ce retrait en se bornant à rejeter le 19 mai 2017 le recours gracieux formé contre elle par M. et Mme A....<br/>
<br/>
              16. En second lieu, si la décision du 14 mars 2017 ne pouvait prendre effet qu'à compter de sa notification à M. et Mme A..., ceux-ci n'ont pas contesté, devant le tribunal administratif, qu'elle leur avait été notifiée avant l'introduction de leur recours contentieux le 6 juillet 2017, ainsi qu'il ressortait des pièces du dossier soumis au juge du fond. Par suite, ils ne sont pas fondés à soutenir que le tribunal administratif, faute d'avoir relevé la date de cette notification dans les motifs de son jugement, aurait commis une erreur de droit.<br/>
<br/>
              17. Il résulte de tout ce qui précède que M. et Mme A... ne sont pas fondés à demander l'annulation des jugements qu'ils attaquent.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              18. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, de la caisse d'allocation familiales et du département de la Vienne les sommes demandées par M. et Mme A.... Ces mêmes dispositions font également obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par la caisse d'allocations familiales, qui n'a été appelée en la cause que pour produire des observations. Enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par le département de la Vienne. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les pourvois de M. et Mme A... sont rejetés. <br/>
Article 2 : Le conclusions présentées par le département et la caisse d'allocations familiales de la Vienne au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. et Mme C... A..., au ministre des solidarités et de la santé, au département de la Vienne et à la caisse d'allocations familiales de la Vienne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
