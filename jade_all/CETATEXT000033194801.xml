<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194801</ID>
<ANCIEN_ID>JG_L_2016_10_000000376599</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 05/10/2016, 376599, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376599</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:376599.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Besançon la condamnation de la commune de Pérouse à leur verser une somme de 22 800 euros au titre du préjudice financier et du préjudice moral subi à raison d'une décision du 27 juillet 2007 portant illégalement retrait du permis de construire dont ils étaient titulaires. Par un jugement n° 1101854 du 28 février 2013, le tribunal administratif de Besançon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13NC00599 du 23 janvier 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. et Mme B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 mars et 27 mai 2014 et le 1er février 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pérouse la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. et Mme B...et à Me Le Prado, avocat de la commune de Pérouse ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 septembre 2016, présentée pour M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. et Mme B...ont demandé à la commune de Pérouse (Territoire de Belfort) de les indemniser de la perte de revenus locatifs et du préjudice moral dont ils s'estimaient victimes du fait de la décision du 27 juillet 2007 portant retrait du permis de construire dont ils étaient titulaires en vue d'aménager un logement de trois pièces, à but locatif, dans un immeuble existant, décision qui a été annulée par un arrêt du 17 décembre 2009 de la cour administrative d'appel de Nancy. Ils se pourvoient en cassation contre l'arrêt du 23 janvier 2014 par lequel cette cour a rejeté leur requête tendant à l'annulation du jugement du 28 février 2013 du tribunal administratif de Besançon rejetant leur demande tendant à la condamnation de la commune à les indemniser.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Si les requérants font valoir que la cour administrative d'appel a, en méconnaissance des prescriptions de l'article R. 741-2 du code de justice administrative, omis de mentionner, dans les visas de l'arrêt attaqué, leur mémoire en réplique du 27 septembre 2013, ce moyen manque en fait.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              3. S'agissant des pertes de revenus locatifs, il ne ressort pas des pièces du dossier soumis aux juges du fond que les requérants avaient justifié des raisons des différents délais observés pour la réalisation des travaux. Par suite, la cour n'a pas entaché son arrêt de dénaturation en relevant cette circonstance dans les motifs de son arrêt. Il ne ressort pas non plus des pièces du dossier soumis aux juges du fond que la cour aurait entaché son arrêt de dénaturation en jugeant que M. et Mme B...ne justifiaient pas la réalité de leur projet locatif, par un motif présenté à juste titre comme surabondant par les juges d'appel, et que les requérants ne peuvent donc, en tout état de cause, utilement contester devant le juge de cassation. Les motifs par lesquels la cour a rejeté leur demande d'indemnisation des pertes de revenus locatifs en se fondant, d'une part, sur l'absence de lien de causalité direct et certain entre le retrait illégal du permis de construire modificatif et la perte de loyers subie et, d'autre part, sur le défaut de caractère certain du préjudice allégué, sont eux aussi surabondants, et ne peuvent être utilement contestés devant le juge de cassation.<br/>
<br/>
              4. S'agissant du préjudice moral, il ressort des pièces du dossier soumis aux juges du fond que les requérants n'ont produit aucun élément susceptible de justifier la réalité de ce préjudice. Par suite, après avoir relevé que les requérants se bornaient à faire état de leur angoisse née de la longueur de la procédure administrative et de l'attitude de la commune de Pérouse, sans produire aucun élément précis de nature à en établir la réalité, la cour a pu juger, sans entacher son arrêt de dénaturation, que les requérants n'établissaient pas l'existence d'un tel préjudice.<br/>
<br/>
              5. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées pour la commune sur le même fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : Les conclusions présentées pour la commune de Pérouse au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Monsieur et Madame A...B...et à la commune de Pérouse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
