<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043387983</ID>
<ANCIEN_ID>JG_L_2021_03_000000450599</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/38/79/CETATEXT000043387983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/03/2021, 450599, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450599</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450599.20210329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B..., épouse C..., a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, d'autre part, d'enjoindre au préfet des Alpes-Maritimes de lui délivrer un titre de séjour ou un récépissé de renouvellement de titre de séjour avec autorisation de travail dès notification de l'ordonnance à intervenir, sous astreinte de 50 euros par jour de retard. Par une ordonnance n° 2101158 du 5 mars 2021, le juge des référés du tribunal administratif de Nice a rejeté sa demande d'injonction.<br/>
<br/>
              Par une requête, enregistrée le 12 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) d'enjoindre au préfet des Alpes-Maritimes de lui délivrer un récépissé de demande de renouvellement de titre de séjour ou de demande de carte de résident dans un délai de 48 heures à compter de l'ordonnance à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - l'ordonnance du juge des référés de première instance est entachée d'erreur de fait dès lors qu'il a retenu qu'elle ne justifiait ni n'alléguait avoir effectué d'autres démarches infructueuses auprès des services de la préfecture pour obtenir le renouvellement de son récépissé ou avoir contesté l'absence de délivrance de ce récépissé depuis le 5 décembre 2020 ou de délivrance d'un titre de séjour depuis deux ans, alors qu'elle a envoyé aux services de la préfecture six courriels de relance restés sans réponse et a justifié de l'impossibilité de prendre rendez-vous sur le site internet de la préfecture ; <br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, qu'elle se trouve démunie de tout récépissé en cours de validité et n'est plus en mesure de justifier de la régularité de sa situation administrative et que, d'autre part, son contrat de travail risque d'être suspendu alors qu'elle élève seule ses deux enfants ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit au travail, à sa liberté d'aller et venir ainsi qu'à sa liberté de circulation. <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 mars 2021, le ministre de l'intérieur conclut au non-lieu à statuer. Il soutient que, par un courrier du 15 mars 2021, le préfet des Alpes-Maritimes a envoyé à la requérante un récépissé de demande de renouvellement de titre de séjour valable jusqu'au 14 juin 2021. À titre subsidiaire, il sollicite la réduction, à de plus justes proportions, du montant mis à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire en réplique, enregistré le 19 juin 2021, Mme Gorlenko soutient qu'elle maintient ses conclusions, notamment celles fondées sur l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le mardi 23 mars 2021 à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Il résulte du mémoire en défense présenté par le ministre de l'intérieur que, postérieurement à l'introduction de la requête, le préfet des Alpes-Maritimes a, par courrier du 15 mars 2021, envoyé à la requérante un récépissé de demande de renouvellement de titre de séjour, valable jusqu'au 14 juin 2021. Dans ces conditions, les conclusions de la requérante tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative afin d'enjoindre au préfet des Alpes-Maritimes de lui octroyer un document de séjour lui permettant notamment de travailler sont devenues sans objet. Il n'y a, par suite, plus lieu d'y statuer. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y pas lieu de statuer sur les conclusions de la requête de Mme Gorlenko.<br/>
Article 2 : L'Etat versera à Mme Gorlenko la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
Article : 3: La présente ordonnance sera notifiée à Mme Olga Gorlenko et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
