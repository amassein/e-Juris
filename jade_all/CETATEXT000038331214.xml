<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038331214</ID>
<ANCIEN_ID>JG_L_2019_03_000000429028</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/33/12/CETATEXT000038331214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/03/2019, 429028, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429028</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429028.20190329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la Ligue des droits de l'homme demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2019-208 du 20 mars 2019 instituant une contravention pour participation à une manifestation interdite sur la voie publique ;   <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - elle a intérêt à agir, dès lors, d'une part, que ses statuts l'autorisent à agir en justice pour défendre les droits de l'homme et, d'autre part, que cet intérêt à agir lui a déjà été reconnu à de nombreuses reprises, notamment par la juridiction administrative ; <br/>
              - la condition d'urgence est remplie, dès lors, d'une part, que le rassemblement visé par le décret attaqué est imminent et, d'autre part, que ce dernier est susceptible d'avoir un impact grave et immédiat sur la liberté de manifestation ;  <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté de manifestation, dès lors que cette atteinte, qui doit être nécessaire, adaptée et proportionnelle, ne respecte pas les dispositions de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, telles qu'interprétées par la Cour européenne des droits de l'homme ; <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'expression, dès lors que cette atteinte, qui doit être nécessaire, adaptée et proportionnelle, ne respecte pas les dispositions de l'article 11 de la même convention, telles qu'interprétées par la Cour européenne des droits de l'homme ;<br/>
              - il est porté une atteinte grave et manifestement illégale au principe constitutionnel de légalité des délits et des peines, dès lors que les sanctions prévues par le décret attaqué sont susceptibles de créer un effet dissuasif sur la libre formation des idées et sur le débat démocratique ; <br/>
              - le décret attaqué crée de façon inédite une infraction spécifique qui permet la répression pénale sans qu'il ne soit tenu compte du comportement individuel des personnes ni du déroulement effectif de la manifestation, ce qui affecte les trois libertés précitées, dès lors, d'une part, qu'elle pourrait avoir pour effet de sanctionner un manifestant pacifique, alors même que les libertés d'expression et de manifestation sont des prérogatives individuelles, d'autre part, que le dispositif n'est pas adapté à la réalité du " mouvement des gilets jaunes " et, enfin, que le décret attaqué est dépourvu de toute nécessité. <br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 mars 2019, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés par la Ligue des droits de l'homme ne sont pas fondés.  <br/>
              Par une intervention, enregistrée le 26 mars 2019, la Confédération générale du travail et le Syndicat des avocats de France demandent au juge des référés du Conseil d'Etat de faire droit aux conclusions de la Ligue des droits de l'homme. Ils soutiennent qu'ils ont intérêt à intervenir et que les moyens de la requête sont fondés.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Ligue des droits de l'homme, la Confédération générale du travail et le Syndicat des avocats de France et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 27 mars 2019 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ligue des droits de l'homme ; <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Confédération générale du travail et du Syndicat des avocats de France ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 28 mars 2019 à 10 heures. <br/>
<br/>
              Vu, enregistré le 27 mars 2019, le mémoire présenté pour la Confédération générale du travail et le Syndicat des avocats de France, qui conclut aux mêmes fins que la requête ;<br/>
              Vu, enregistré le 27 mars 2019, le mémoire présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de la sécurité intérieure ; <br/>
              - le décret n° 2019-208 du 20 mars 2019 ; <br/>
              - le code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Le respect de ces conditions revêt un caractère cumulatif.<br/>
<br/>
              2. Aux termes de l'article L. 211-4 du code de la sécurité intérieure : " Si l'autorité investie des pouvoirs de police estime que la manifestation projetée est de nature à troubler l'ordre public, elle l'interdit par un arrêté qu'elle notifie immédiatement aux signataires de la déclaration au domicile élu. Le maire transmet, dans les vingt-quatre heures, la déclaration au représentant de l'Etat dans le département. Il y joint, le cas échéant, une copie de son arrêté d'interdiction. Si le maire, compétent pour prendre un arrêté d'interdiction, s'est abstenu de le faire, le représentant de l'Etat dans le département peut y pourvoir dans les conditions prévues à l'article L. 2215-1 du code général des collectivités territoriales. ".<br/>
<br/>
              3. Sur le fondement de l'article L. 521-2 précité, la Ligue des droits de l'homme demande la suspension de l'exécution du décret du 20 mars 2019 qui insère un nouvel article R. 644-4 dans le code pénal, au terme duquel " Le fait de participer à une manifestation sur la voie publique interdite sur le fondement des dispositions de l'article L. 211-4 du code de la sécurité intérieure est puni de l'amende prévue pour les contraventions de la quatrième classe " et modifie par voie de conséquence l'article R. 48-1 du code de procédure pénale et la section 5 du chapitre Ier du titre Ier du  livre II du code de la sécurité intérieure. <br/>
              Sur l'intervention de la Confédération générale du travail et du Syndicat des avocats de France :<br/>
<br/>
              4. Eu égard à leur objet, les syndicats mentionnés ont intérêt à la suspension du décret contesté du 20 mars 2019. Leur intervention est admise.<br/>
<br/>
              Sur la demande de suspension du décret du 20 mars 2019 :<br/>
<br/>
              5. Le respect de la liberté de manifestation, qui a le caractère d'une liberté fondamentale, doit être concilié avec le maintien de l'ordre public. Il appartient à l'autorité investie du pouvoir de police d'apprécier le risque de troubles à l'ordre public et de prendre les mesures de nature à prévenir de tels troubles dont, le cas échéant, l'interdiction de manifestation si une telle mesure est seule de nature préserver l'ordre public.<br/>
<br/>
              6. Si les atteintes portées dans ces conditions, pour des raisons de sauvegarde de l'ordre public, à la liberté de manifester doivent être nécessaires, adaptées et proportionnées, il ressort de l'instruction que le décret litigieux, qui se borne à renforcer, en augmentant le montant d'une amende pour contravention, l'effectivité d'une interdiction de manifester sur la voie publique telle que prévue à l'article L. 211-4 du code de la sécurité intérieure, ne porte pas atteinte à la liberté de manifester dès lors qu'il ne concerne que des manifestations interdites et que son application est assorties de garanties suffisantes. <br/>
<br/>
              7. D'une part, la décision d'interdiction de manifester sur la voie publique prise par l'autorité administrative est motivée et soumise au plein contrôle du juge de l'excès de pouvoir.<br/>
<br/>
              8. D'autre part, il appartient à l'autorité administrative ayant prononcé cette interdiction d'assurer, par tout moyen utile, l'information complète du public et donc des personnes qui envisageraient de participer à telle manifestation interdite malgré son caractère illicite. En tant que sanction pénale, la contravention peut avoir pour effet de dissuader de participer à une manifestation malgré l'interdiction mais elle se borne à mettre en oeuvre l'interdiction. Elle ne porte pas atteinte à la liberté de manifester, corollaire de la liberté d'expression garantie par la Constitution et par les articles 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              9. Enfin, si le principe de légalité des délits et des peines garanti par l'article 8 de la déclaration des droits de l'homme et du citoyen commande que toute infraction pénale soit strictement définie, il n'apparaît pas que l'infraction prévue par le décret contesté réprime une infraction insuffisamment précise. En effet, d'une part, l'autorité administrative précise les motifs, la date, les horaires et le périmètre de l'interdiction. D'autre part, le contrevenant peut faire valoir devant le juge judiciaire, compétent pour apprécier l'intention de chaque personne présente dans le périmètre de la manifestation interdite d'y participer effectivement, l'absence d'une telle intention.  <br/>
<br/>
              10. Il résulte de ce qui précède que le décret contesté ne porte pas d'atteinte grave et manifestement illégale à une liberté fondamentale. Par suite, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête doit être rejetée. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Confédération générale du travail et du Syndicat des avocats de France est admise. <br/>
Article 2 : La requête de la Ligue des droits de l'homme est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à la Ligue des droits de l'homme et au ministre de l'intérieur.<br/>
Copie en sera adressée à la Confédération générale du travail et au Syndicat des avocats de France.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
