<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028337137</ID>
<ANCIEN_ID>JG_L_2013_12_000000354590</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/33/71/CETATEXT000028337137.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 17/12/2013, 354590, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354590</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:354590.20131217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 décembre 2011 et 5 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Montpellier, représentée par son maire ; la commune de Montpellier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 10MA0191 du 29 septembre 2011 par lequel la cour administrative d'appel de Marseille lui a réitéré l'injonction de payer à la société d'assurance La Médicale de France l'indemnité (principal et intérêts) mise à sa charge par son précédent arrêt du 2 septembre 2010 et l'a condamnée verser une somme de 15 825 euros, ainsi qu'une somme d'un même montant à l'Etat, au titre de la liquidation de l'astreinte prononcée à son encontre par cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer un non-lieu à statuer sur l'appel de la société d'assurance La Médicale de France ; <br/>
<br/>
              3°) de mettre à la charge de la société d'assurance La Médicale de France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, y compris le coût des timbres fiscaux en application de l'article  R. 761-1 du même code ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Montpellier et à la SCP Richard, avocat de la société d'assurance La Médicale de France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par un arrêt du 2 septembre 2010, la cour administrative d'appel de Marseille a prononcé une astreinte de 150 euros par jour de retard à l'encontre de la commune de Montpellier si elle ne justifiait pas avoir, dans les 2 mois suivant la notification de cet arrêt, versé à la société d'assurance La Médicale de France la somme de 8 265,50 euros, augmentée des intérêts et de la capitalisation des intérêts ; que le 11 mars 2011, la société d'assurance a demandé la liquidation de l'astreinte, soit une somme de 18 000 euros, au motif que la commune n'avait procédé à aucune mesure d'exécution ; que, par un arrêt du 29 septembre 2011, contre lequel la commune de Montpellier se pourvoit en cassation, la cour administrative d'appel de Marseille l'a condamnée à verser à la société d'assurance La Médicale de France, outre le paiement de l'indemnité mise à sa charge par l'arrêt du 2 septembre 2010, la somme de 15 825 euros au titre de la liquidation de l'astreinte ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis à la cour statuant comme juge de l'exécution que la commune de Montpellier a produit devant cette juridiction un mandat de paiement de l'indemnité mise à sa charge par l'arrêt du 2 septembre 2010, mentionnant le nom du bénéficiaire, " Médicale de France SA ", le montant mis en paiement sur deux lignes " C " et " D " ne pouvant signifier autre chose que " Crédit " et " Débit ", ainsi que le mot " Paiement " et la date du 2 mars 2011 sur la même ligne que " D " ; que s'il appartient au juge de l'exécution, devant lequel la partie bénéficiaire d'une condamnation prononcée par une décision de justice soutient que cette décision n'a pas été exécutée, de s'assurer de la réalité et de la plénitude de l'exécution et si ce juge porte une appréciation souveraine sur les faits dont il est saisi, la cour a dénaturé les pièces produites devant elle en estimant que la commune de Montpellier n'avait procédé à aucune exécution entre le 4 novembre 2010 et de la date de lecture de l'arrêt attaqué et a méconnu son office en statuant comme elle l'a fait sans s'assurer, en mettant au besoin en oeuvre ses pouvoirs d'instruction, de la réalité des faits ; que, dès lors, la commune de Montpellier est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société d'assurance La Médicale de France la somme de 3 000 euros à verser à la commune de Montpellier au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Montpellier qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Marseille du 29 septembre 2011 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La société d'assurance La Médicale de France versera à la commune de Montpellier une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de la société d'assurance La Médicale de France au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Montpellier et à la société d'assurance La Médicale de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
