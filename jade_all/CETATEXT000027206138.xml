<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027206138</ID>
<ANCIEN_ID>JG_L_2013_03_000000366248</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/20/61/CETATEXT000027206138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/03/2013, 366248, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366248</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:366248.20130312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 février 2013 au secrétariat du contentieux du Conseil d'Etat présentée pour la société American Medical System France (AMS), dont le siège est 19, avenue de Norvège, les Fjors, bâtiment Nobel - ZA Courtaboeuf à Courtaboeuf (91953), représentée par son représentant légal ; la société requérante demande au juge des référés du Conseil d'Etat d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution des décisions du 10 juillet 2012 refusant d'inscrire le dispositif médical " Advance " sur la liste des produits et prestations remboursables mentionnée à l'article L.165-1 du code de la sécurité sociale et sur la liste des produits susceptibles de faire l'objet d'une prise en charge en sus des prestations d'hospitalisation, mentionnée à l'article L. 162-22-7 du même code et de la décision rejetant son recours gracieux contre cette décision ; <br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que les décisions contestées portent un préjudice grave et immédiat tant à sa situation financière qu'à l'intérêt des patients ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              - les ministres compétents ont commis une erreur de droit en s'estimant liés par la recommandation du conseil de l'hospitalisation du 20 avril 2012 ;<br/>
              - les décisions contestées sont entachées d'une erreur d'appréciation et d'une erreur de droit en ce qu'elles ont été prises conformément à des recommandations du conseil de l'hospitalisation des 14 décembre 2011 et 20 avril 2012 elles-mêmes entachées d'erreurs d'appréciation ; <br/>
              - les décisions contestées sont entachées d'erreurs d'appréciations et d'erreurs de droit en ce qu'elles ont été prises, au vu notamment d'un avis de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé, sur la base d'une amélioration du service attendu du dispositif médical " Advance " qualifiée de " mineure " à partir d'une comparaison avec des traitements non comparables, soit simplement palliatifs, soit curatifs mais d'une efficacité inférieure ; <br/>
              - les décisions contestées sont entachées d'une erreur d'appréciation et d'une erreur de droit en ce qu'elles se fondent sur une possibilité de prise en charge du dispositif médical " Advance " sans inscription sur les listes mentionnées aux articles L. 162-22-7 et L. 165-1 du code de la sécurité sociale alors qu'en l'état actuel du droit, une prise en charge au titre de certains groupes homogènes de soins est financièrement restreinte et juridiquement sujette à caution ; <br/>
              - la décision refusant l'inscription sur la liste mentionnée à l'article L. 165-1 est entachée d'erreur de droit faute de pouvoir être légalement justifiée par le motif tiré de ce que l'inscription serait susceptible d'entrainer des dépenses injustifiées pour l'assurance maladie ;   <br/>
<br/>
<br/>
              Vu les décisions dont la suspension de l'exécution est demandée ;<br/>
              Vu la copie de la requête à fin d'annulation des décisions contestées ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 7 mars 2013, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ; elle soutient qu'aucun des moyens soulevés n'est de nature à créer un doute sérieux quant à la légalité des décisions contestées et que la condition d'urgence n'est pas remplie en l'absence de préjudice grave et immédiat tant pour la société AMS dont le dispositif médical " Advance " fait d'ores et déjà l'objet de prises en charge au titre des forfaits d'hospitalisation mentionnés à l'article L. 162-22-6 du code de la sécurité sociale, n'établit pas que le préjudice dont elle se prévaut, au demeurant mineur par rapport à son chiffre d'affaires serait certain, que pour les malades, qui peuvent bénéficier de ce dispositif ;<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société American Medical System France (AMS), d'autre part, la ministre des affaires sociales et de la santé ainsi que le ministre de l'économie et des finances ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 mars 2013 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Le Prado, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Société American Medical System France (AMS) ;<br/>
<br/>
              - les représentants de la ministre des affaires sociales et de la santé ;<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ; <br/>
<br/>
              2. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 165-1 du code de la sécurité sociale : " Le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel, (...) est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute Autorité de santé mentionnée à l'article L. 161-37. (...) Les conditions d'application du présent article, notamment les conditions d'inscription sur la liste, ainsi que la composition et le fonctionnement de la commission sont fixées par décret en Conseil d'Etat " ; que ces conditions d'application sont fixées aux articles R. 165-1 à R. 165-25 du même code ; qu'aux termes de l'article L. 162-22-7 du même code : " L'Etat fixe (...) les conditions dans lesquelles certains produits et prestations mentionnés à l'article L. 165-1 peuvent faire l'objet d'une prise en charge en sus des prestations d'hospitalisation susmentionnées " ; que l'article R. 162-42-7 du même code dispose : " La liste des spécialités pharmaceutiques et les conditions de prise en charge des produits et prestations mentionnés à l'article L. 162-22-7 sont fixées par arrêté des ministres chargés de la santé et de la sécurité sociale sur recommandation du conseil de l'hospitalisation " ;<br/>
<br/>
              4. Considérant que la société AMS soutient que les décisions contestées, refusant d'inscrire le dispositif médical " Advance "  sur les listes mentionnées aux articles L. 165-1 et  L. 162-22-7 du code de la sécurité sociale, sont source d'un important manque à gagner et portent une atteinte grave à l'intérêt des patients ; que, toutefois, ainsi qu'il ressort des pièces du dossier et des indications fournies à l'audience, le préjudice financier invoqué est au moins en partie incertain en raison de l'absence de données fiables sur le nombre de patients susceptibles d'être intéressés, de l'existence d'une concurrence avec d'autres traitements curatifs ou palliatifs  et de la circonstance que, nonobstant l'absence d'inscription, ce dispositif médical est acquis, à l'heure actuelle, par des établissements de santé dans le cadre des forfaits d'hospitalisation mentionnés à l'article L. 162-22-6 code de la sécurité sociale ; qu'ainsi et en outre, ces refus d'inscription n'interdisent pas aux patients de bénéficier de ce dispositif ; que, par suite, les décisions contestées ne portent pas aux intérêts de la société requérante ou à celui de la santé publique une atteinte de nature à caractériser une situation d'urgence ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la condition d'urgence exigée par l'article L. 521-1 du code de justice administrative n'est pas remplie ; que dès lors, et sans qu'il soit besoin de se prononcer sur les moyens de légalité soulevés par la société requérante, les conclusions aux fins de suspension qu'elle présente doivent être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
      Article 1er : La requête de la société American Medical System France (AMS) est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société American Medical System France (AMS), à la ministre des affaires sociales et de la santé ainsi qu'au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
