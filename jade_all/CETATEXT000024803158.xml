<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024803158</ID>
<ANCIEN_ID>JG_L_2011_11_000000351890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/80/31/CETATEXT000024803158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 09/11/2011, 351890</TITRE>
<DATE_DEC>2011-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:351890.20111109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1101475 QPC du 12 août 2011, enregistrée le 16 août 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la 3e chambre du tribunal administratif de Nîmes, avant qu'il soit statué sur la demande de M. A tendant à l'annulation de l'arrêté du 1er février 2011 par lequel le préfet de Vaucluse a déclaré cessibles les parcelles nécessaires à la réalisation de l'extension de la zone d'activités de Bellecour sur le territoire de la commune de Carpentras, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 11-1, L. 11-2 et L. 11-8 du code de l'expropriation pour cause d'utilité publique ;<br/>
<br/>
              Vu le mémoire, enregistré le 2 mai 2011 au greffe du tribunal administratif de Nîmes, présenté par M. Philippe A, demeurant ..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique, notamment ses articles L. 11-1, L. 11-2 et L. 11-8 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 11-1 du code de l'expropriation pour cause d'utilité publique : " I. L'expropriation d'immeubles, en tout ou partie, ou de droits réels immobiliers ne peut être prononcée qu'autant qu'elle aura été précédée d'une déclaration d'utilité publique intervenue à la suite d'une enquête publique et qu'il aura été procédé contradictoirement à la détermination des parcelles à exproprier ainsi qu'à la recherche des propriétaires, des titulaires de droits réels et autres intéressés (...) " ; qu'aux termes de l'article L. 11-2 du même code : " L'utilité publique est déclarée par arrêté ministériel ou par arrêté préfectoral. / Toutefois, un décret en Conseil d'Etat détermine les catégories de travaux ou d'opérations qui, en raison de leur nature ou de leur importance, ne pourront être déclarées d'utilité publique que par décret en Conseil d'Etat (...) " ; qu'aux termes de l'article L. 11-8 du même code : " Le préfet détermine par arrêté de cessibilité la liste des parcelles ou des droits réels immobiliers à exproprier si cette liste ne résulte pas de la déclaration d'utilité publique (...) " ;<br/>
<br/>
              Considérant, en premier lieu, que le législateur n'a autorisé l'expropriation d'immeubles ou de droits réels immobiliers que pour la réalisation d'opérations dont l'utilité publique est préalablement et formellement constatée ; que cette condition correspond à l'exigence de nécessité publique, légalement constatée, prévue par l'article 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en vertu d'un principe fondamental reconnu par les lois de la République, à l'exception des matières réservées par nature à l'autorité judiciaire, relève en dernier ressort de la compétence de la juridiction administrative l'annulation ou la réformation des décisions prises par les autorités exerçant le pouvoir exécutif dans l'exercice des prérogatives de puissance publique ; que sont au nombre de ces décisions les déclarations d'utilité publique mentionnées aux articles L. 11-1 et L. 11-2 du code de l'expropriation pour cause d'utilité publique ainsi que les arrêtés de cessibilité mentionnés à l'article L. 11-8 du même code ; que le recours ouvert contre ces décisions devant le juge administratif revêt, bien qu'il n'ait pas d'effet suspensif de plein droit, un caractère effectif et ne méconnaît ni l'article 16 de la Déclaration des droits de l'homme et du citoyen ni l'importance des attributions conférées à l'autorité judiciaire en matière de protection de la propriété immobilière par les principes fondamentaux reconnus par les lois de la République ;<br/>
<br/>
              Considérant, en troisième lieu, que M. A n'est pas fondé à soutenir que, faute d'avoir détaillé les éléments constitutifs de la notion d'utilité publique et d'avoir précisé les catégories de bénéficiaires des opérations d'expropriation, le législateur n'aurait pas pleinement exercé la compétence qui lui revient de déterminer les principes fondamentaux du régime de la propriété et aurait de ce fait porté au droit de propriété une atteinte contraire à la Constitution ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu, en conséquence, de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Nîmes.<br/>
Article 2 : La présente décision sera notifiée à M. Philippe A et à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
Copie en sera adressée au Conseil Constitutionnel, au Premier ministre, au garde des sceaux, ministre de la justice et au tribunal administratif de Nîmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-01-01 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. NOTIONS GÉNÉRALES. NOTION D'UTILITÉ PUBLIQUE. - PORTÉE - EXIGENCE DE NÉCESSITÉ PUBLIQUE AU SENS DE L'ARTICLE 17 DE LA DDHC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - EXPROPRIATION - CONDITION D'UTILITÉ PUBLIQUE (ART. L. 11-1  ET L. 11-2 DU CODE DE L'EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE) - NOTION - EXIGENCE DE NÉCESSITÉ PUBLIQUE AU SENS DE L'ARTICLE 17 DE LA DDHC - MÉCONNAISSANCE PAR LE LÉGISLATEUR DE L'ÉTENDUE DE SA COMPÉTENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 34-01-01 Par les dispositions des articles L. 11-1 et L. 11-2 du code de l'expropriation pour cause d'utilité publique, le législateur n'a autorisé l'expropriation d'immeubles ou de droits réels immobiliers que pour la réalisation d'opérations dont l'utilité publique est préalablement et formellement constatée. Cette condition correspond à l'exigence de nécessité publique, légalement constatée, prévue par l'article 17 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789.</ANA>
<ANA ID="9B"> 54-10-05-04-02 Par les dispositions des articles L. 11-1 et L. 11-2 du code de l'expropriation pour cause d'utilité publique, le législateur n'a autorisé l'expropriation d'immeubles ou de droits réels immobiliers que pour la réalisation d'opérations dont l'utilité publique est préalablement et formellement constatée. Cette condition correspond à l'exigence de nécessité publique, légalement constatée, prévue par l'article 17 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789. En ne détaillant pas les éléments constitutifs de la notion d'utilité publique et en ne précisant pas les catégories de bénéficiaires des opérations d'expropriation, le législateur n'a pas méconnu l'étendue de la compétence lui revenant de déterminer les principes fondamentaux du régime de la propriété.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
