<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217420</ID>
<ANCIEN_ID>JG_L_2019_10_000000417682</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 14/10/2019, 417682, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417682</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417682.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Montpellier de condamner l'Etat à lui verser une indemnité de 13 453,62 euros en réparation des préjudices subis du fait du retard de l'Etat à prendre les mesures statutaires permettant sa titularisation. Par un jugement n° 1403408 du 25 mars 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA02083 du 28 novembre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 janvier et 30 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté sa demande d'indemnisation du préjudice résultant de la minoration de son droit à pension ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit dans cette mesure à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 85-1534 du 31 décembre 1985 ;<br/>
              - le décret n° 2000-788 du 24 août 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., après avoir été employé en qualité d'agent non titulaire de catégorie A par le ministère de la coopération et le ministère des affaires étrangères de 1971 au 30 juin 2000, a été mis à disposition du ministère de l'éducation nationale jusqu'au 30 juin 2001 puis recruté en qualité d'agent contractuel par l'université de Perpignan le 1er juillet 2001. Par arrêté du ministre de l'éducation nationale du 6 novembre 2002, M. B... a été titularisé à compter du 26 novembre 2001 dans le corps des ingénieurs d'études régi par le décret du 31 décembre 1985 fixant les dispositions statutaires applicables aux ingénieurs et aux personnels techniques et administratifs de recherche et de formation du ministère de l'éducation nationale, à la suite de sa réussite à l'examen professionnel organisé en application du décret du 24 août 2000 fixant les conditions exceptionnelles d'intégration de certains agents non titulaires mentionnés à l'article 74 (1º) de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dans des corps de fonctionnaires de catégorie A. Après avoir demandé et obtenu devant le juge administratif réparation du préjudice de carrière causé par sa titularisation tardive dans le corps des ingénieurs d'études, M. B..., entretemps admis à faire valoir ses droits à pension de retraite, a demandé à être indemnisé également du préjudice causé par la minoration de sa pension, évalué par lui en fonction de la différence entre l'indice qu'il avait atteint dans le corps des ingénieurs d'études à la date de son départ en retraite, et l'indice qu'il aurait atteint à cette même date s'il avait été intégré dans ce corps dès le 1er janvier 1987. Par l'arrêt attaqué du 28 novembre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B... contre le jugement du tribunal administratif de Montpellier du 25 mars 2016 ayant rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 73 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique : " Les agents non titulaires qui occupent un emploi présentant les caractéristiques définies à l'article 3 du titre 1er du statut général ont vocation à être titularisés, sur leur demande, dans des emplois de même nature qui sont vacants ou qui seront créés par les lois de finances (...) ". Aux termes de l'article 74 de la même loi : " Ont également vocation à être titularisés, sur leur demande, dans les conditions fixées à l'article précédent : /1° Les personnels civils de coopération culturelle, scientifique et technique en fonction auprès d'Etats étrangers ou de l'organisme auprès duquel ils sont placés, qui remplissent les conditions fixées au deuxième alinéa de l'article 8 de la loi n° 72-659 du 13 juillet 1972 relative à la situation du personnel civil de coopération culturelle, scientifique et technique auprès d'Etats étrangers ; /2° Les personnels civils des établissements et organismes de diffusion culturelle ou d'enseignement situés à l'étranger considérés comme des services déconcentrés du ministère des relations extérieures, gérés dans les conditions fixées par l'ordonnance n° 62-952 du 11 août 1962 ou jouissant de l'autonomie financière en application de l'article 66 de la loi de finances n° 73-1150 du 27 décembre 1973 ". L'article 79 de la loi prévoit que des décrets en Conseil d'Etat peuvent organiser pour les agents non titulaires mentionnés à l'article 73 l'accès aux différents corps de fonctionnaires suivant certaines modalités. Enfin, en vertu de l'article 80 de la même loi, les décrets prévus par son article 79 fixent, pour chaque ministère, les corps auxquels les agents non titulaires mentionnés à l'article 73 peuvent accéder et les modalités d'accès à ces corps.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'il est constant que M. B... a été employé, non par le ministère chargé de l'enseignement supérieur et de la recherche, mais par le ministère des affaires étrangères, en qualité d'agent non titulaire de coopération culturelle, scientifique et technique, entre 1971 et le 31 décembre 1999. En conséquence, il n'avait, en tout état de cause, aucune chance sérieuse d'être titularisé, à compter du 1er janvier 1987, dans le corps des ingénieurs d'études relevant du décret du 31 décembre 1985 fixant les dispositions statutaires applicables aux ingénieurs et aux personnels techniques et administratifs de recherche et de formation du ministère de l'éducation nationale. Par suite, M. B... ne pouvait utilement se prévaloir de ces dispositions à l'appui de sa demande, à laquelle elles étaient inapplicables, tendant à la réparation du préjudice de retraite ayant résulté de sa titularisation tardive dans le corps des ingénieurs d'études, celle-ci étant intervenue à compter du 26 novembre 2001 alors qu'il était agent contractuel du ministère chargé de l'éducation nationale et de l'enseignement supérieur. Dès lors, sans qu'il soit besoin d'examiner le moyen d'erreur de droit soulevé par M. B..., il y a lieu de substituer ce motif d'ordre public, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifie le dispositif de l'arrêt attaqué en ce qui concerne le préjudice financier résultant de la liquidation des droits à pension, au motif par lequel la cour a cru devoir écarter le moyen de M. B.... <br/>
<br/>
              4. Il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
