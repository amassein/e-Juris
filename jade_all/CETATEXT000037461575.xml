<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461575</ID>
<ANCIEN_ID>JG_L_2018_10_000000413681</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 01/10/2018, 413681</TITRE>
<DATE_DEC>2018-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413681</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413681.20181001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 24 août 2017 et le 12 mars 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association interprofessionnelle de la banane demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 28 février 2017 portant extension de l'accord interprofessionnel conclu dans le cadre de l'Association interprofessionnelle de la banane (AIB) et relatif à la réalisation d'actions collectives ainsi que la décision implicite de rejet du recours gracieux contre cet arrêté prise par le ministre de l'agriculture, en ce que cet arrêté a substitué pour l'extension une période qui s'étend du 1er janvier 2017 au 31 août 2019 à la période d'application prévue par l'accord, qui débutait au 1er septembre 2016 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles et abrogeant les règlements (CEE) n° 922/72, (CEE) n° 234/79, (CE) n° 1037/2001 et (CE) n° 1234/2007 du Conseil ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 28 février 2017, les ministres chargés de l'économie et de l'agriculture ont étendu l'accord interprofessionnel conclu le 8 novembre 2016 dans le cadre de l'Association interprofessionnelle de la banane (AIB) et relatif à la réalisation d'actions collectives. L'AIB, dont la reconnaissance en qualité d'organisation interprofessionnelle est intervenue le 5 septembre 2016, a demandé le 20 avril 2017 au ministre de l'agriculture, dans le cadre d'un recours gracieux, de donner effet à l'extension à compter du 1er septembre 2016, conformément aux stipulations de l'accord interprofessionnel, et non du 1er janvier 2017, date fixée par l'arrêté du 28 février 2017. L'AIB demande au Conseil d'Etat l'annulation de cet arrêté ainsi que de la décision implicite de rejet de son recours gracieux, en ce que cet arrêté a substitué, pour l'extension de l'accord, une période du 1er janvier 2017 au 31 août 2019 à la période d'application prévue par l'accord, qui débutait le 1er septembre 2016 pour s'achever le 31 août 2019.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En premier lieu, s'il est prévu au dernier alinéa de l'article L. 632-4 du code rural et de la pêche maritime que les décisions de refus d'extension des accords interprofessionnels doivent être motivées, l'AIB a, en tout état de cause, été destinataire d'un courrier du 13 février 2017 exposant les motifs qui ont conduit les ministres à différer l'extension au 1er janvier 2017. Le moyen tiré de la méconnaissance du dernier alinéa de l'article L. 632-4 de ce code ne peut, par suite, qu'être écarté. <br/>
<br/>
              3. En second lieu, il résulte du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que le directeur général adjoint de la performance économique et environnementale des entreprises, M. A...B..., signataire de la lettre du 13 février 2017 au président de l'AIB, avait, par l'effet de sa nomination par arrêté ministériel du 20 mai 2015, qualité pour signer ce courrier au nom du ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              4. L'article 6 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles et abrogeant les règlements (CEE) n° 922/72, (CEE) n° 234/79, (CE) n° 1037/2001 et (CE) n° 1234/2007 du Conseil dispose : " Les campagnes de commercialisation suivantes sont établies : a) du 1er janvier au 31 décembre d'une année donnée pour les secteurs des fruits et légumes, des fruits et légumes transformés et de la banane ; (...) ". En vertu de l'article 164 de ce même règlement : " 1. Dans le cas où (...) une organisation interprofessionnelle reconnue opérant dans une ou plusieurs circonscriptions économiques déterminées d'un État membre est considérée comme représentative de la production ou du commerce ou de la transformation d'un produit donné, l'État membre concerné peut, à la demande de cette organisation, rendre obligatoires, pour une durée limitée, certains accords, certaines décisions ou certaines pratiques concertées arrêtés dans le cadre de cette organisation pour d'autres opérateurs, individuels ou non, opérant dans la ou les circonscriptions économiques en question et non membres de cette organisation ou association. (...) 4. (...) Ces règles ne portent pas préjudice aux autres opérateurs de l'État membre concerné ou de l'Union et n'ont pas les effets énumérés à l'article 210, paragraphe 4, ou ne sont pas contraires à la législation de l'Union ou à la réglementation nationale en vigueur. ". Selon son article 165 : " Dans le cas où les règles (...) d'une organisation interprofessionnelle reconnue sont étendues au titre de l'article 164 et lorsque les activités couvertes par ces règles présentent un intérêt économique général pour les opérateurs économiques dont les activités sont liées aux produits concernés, l'État membre qui a accordé la reconnaissance peut décider, après consultation des acteurs concernés, que les opérateurs économiques individuels ou les groupes d'opérateurs non membres de l'organisation qui bénéficient de ces activités sont redevables à l'organisation de tout ou partie des contributions financières versées par les membres, dans la mesure où ces dernières sont destinées à couvrir les coûts directement liés à la conduite des activités concernées. " . En vertu du premier alinéa de l'article L. 632-6 du code rural et de la pêche maritime : " Les organisations interprofessionnelles reconnues, mentionnées aux articles L. 632-1 à L. 632-2, sont habilitées à prélever, sur tous les membres des professions les constituant, des cotisations résultant des accords étendus selon la procédure fixée aux articles L. 632-3 et L. 632-4 et, s'il y a lieu, à l'article 165 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013, et qui, nonobstant leur caractère obligatoire, demeurent des créances de droit privé". Compte tenu des conditions particulières dans lesquelles s'exercent les activités agricoles, les dispositions réglementaires fixant le régime applicable à un produit agricole pour une campagne déterminée doivent nécessairement produire effet pour l'ensemble de la campagne considérée. L'entrée en vigueur de telles dispositions dès le début d'une campagne n'est légale qu'à la condition que ces dispositions interviennent avant la fin de la campagne. Il en va ainsi notamment des dispositions réglementaires rendant obligatoires le versement de cotisations à une organisation interprofessionnelle dans le cadre de l'extension d'un accord interprofessionnel.<br/>
<br/>
              5. L'article 2 de l'accord interprofessionnel conclu dans le cadre de l'AIB instaure une cotisation sur les bananes de toutes origines commercialisées en France métropolitaine, qui s'élève à un euro hors taxes par tonne de bananes fruits mises en marché sur ce territoire. Alors même que certaines actions prévues ne porteraient pas seulement sur la commercialisation des bananes, l'accord fixe comme fait générateur de la cotisation la commercialisation de celles-ci. L'article 6 du règlement mentionné au point 4 dispose que la campagne de commercialisation de la banane s'étend du 1er janvier au 31 décembre d'une année donnée. En conséquence, les auteurs de l'arrêté n'ont pas commis d'erreur de droit en estimant, à la date à laquelle ils l'ont pris, que l'extension de l'accord ne pouvait légalement prendre effet à partir du 1er septembre 2016, alors que la campagne de l'année 2016 était expirée, et qu'elle ne serait effective qu'à compter du début de la campagne de commercialisation en cours, soit le 1er janvier 2017. En outre, contrairement à ce que soutient l'AIB, les auteurs de l'arrêté ne pouvaient étendre l'accord au-delà du terme que celui-ci avait stipulé, soit le 31 août 2019.<br/>
<br/>
              6. Il résulte de tout ce qui précède que l'AIB n'est pas fondée à demander l'annulation des décisions attaquées. Par suite, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent également être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association interprofessionnelle de la banane est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Association interprofessionnelle de la banane, au ministre de l'économie et des finances et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. RÉTROACTIVITÉ. RÉTROACTIVITÉ LÉGALE. - ACCORDS, CONCLUS DANS LE CADRE DES ORGANISATIONS INTERPROFESSIONNELLES AGRICOLES, PRÉVOYANT LE PRÉLÈVEMENT DE COTISATIONS - EXTENSION PAR ARRÊTÉ MINISTÉRIEL (ART. L. 632-3 ET L. 632-4 DU CODE RURAL ET DE LA PÊCHE MARITIME) - EXTENSION NE POUVANT LÉGALEMENT PRENDRE EFFET QU'À COMPTER DU DÉBUT DE LA CAMPAGNE EN COURS [RJ1], ET NON À COMPTER D'UNE CAMPAGNE ANTÉRIEURE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-01 AGRICULTURE ET FORÊTS. INSTITUTIONS AGRICOLES. - ACCORDS, CONCLUS DANS LE CADRE DES ORGANISATIONS INTERPROFESSIONNELLES, PRÉVOYANT LE PRÉLÈVEMENT DE COTISATIONS - EXTENSION PAR ARRÊTÉ MINISTÉRIEL (ART. L. 632-3 ET L. 632-4 DU CODE RURAL ET DE LA PÊCHE MARITIME) - EXTENSION NE POUVANT LÉGALEMENT PRENDRE EFFET QU'À COMPTER DU DÉBUT DE LA CAMPAGNE EN COURS [RJ1], ET NON À COMPTER D'UNE CAMPAGNE ANTÉRIEURE.
</SCT>
<ANA ID="9A"> 01-08-02-01 Accord interprofessionnel, conclu le 8 novembre 2016, instaurant une cotisation sur les bananes de toutes origines commercialisées en France métropolitaine, et fixant comme fait générateur de la cotisation la commercialisation de celles-ci. La campagne de commercialisation de la banane s'étend du 1er janvier au 31 décembre d'une année donnée. En conséquence, les auteurs de l'arrêté d'extension n'ont pas commis d'erreur de droit en estimant, à la date à laquelle ils l'ont pris, le 28 février 2017, que l'extension de l'accord ne pouvait légalement prendre effet à partir du 1er septembre 2016, alors que la campagne de l'année 2016 était expirée, et qu'elle ne serait effective qu'à compter du début de la campagne de commercialisation en cours, soit le 1er janvier 2017.</ANA>
<ANA ID="9B"> 03-01 Accord interprofessionnel, conclu le 8 novembre 2016, instaurant une cotisation sur les bananes de toutes origines commercialisées en France métropolitaine, et fixant comme fait générateur de la cotisation la commercialisation de celles-ci. La campagne de commercialisation de la banane s'étend du 1er janvier au 31 décembre d'une année donnée. En conséquence, les auteurs de l'arrêté d'extension n'ont pas commis d'erreur de droit en estimant, à la date à laquelle ils l'ont pris, le 28 février 2017, que l'extension de l'accord ne pouvait légalement prendre effet à partir du 1er septembre 2016, alors que la campagne de l'année 2016 était expirée, et qu'elle ne serait effective qu'à compter du début de la campagne de commercialisation en cours, soit le 1er janvier 2017.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 8 juin 1979, Confédération générale des planteurs de betterave, n° 4188, p. 269.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
