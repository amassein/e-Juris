<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986068</ID>
<ANCIEN_ID>JG_L_2014_12_000000375600</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/60/CETATEXT000029986068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 29/12/2014, 375600, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375600</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:375600.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille de condamner la commune de Comines à lui verser une somme 66 920 euros en réparation des préjudices résultant de la décision du 7 décembre 2006 ayant refusé de le réintégrer dans les services municipaux et des arrêtés des 11 mai et 16 novembre 2007 l'ayant maintenu en disponibilité d'office jusqu'au 10 mai 2008. <br/>
<br/>
              Par un jugement n° 1004281 du 16 octobre 2012, le tribunal administratif de Lille a condamné la commune de Comines à verser une somme de 18 000 euros à M. B....<br/>
<br/>
              Par un arrêt n° 12DA01883 du 10 décembre 2013, la cour administrative d'appel de Douai a, à la demande de M.B..., porté la condamnation de la commune de Comines de 18 000 à 32 000 euros et rejeté l'appel incident formé par cette dernière tendant à l'annulation du jugement du tribunal administratif de Lille du 16 octobre 2012.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 février, 19 mai et 26 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Comines demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 12DA01883 du 10 décembre 2013 de la cour administrative d'appel de Douai ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...et de faire droit à son appel incident tendant à l'annulation du jugement du tribunal administratif de Lille du 16 octobre 2012 ;<br/>
<br/>
              3°) de mettre à la charge de M. B...une somme de 2 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Comines et à la SCP Hémery, Thomas-Raquin, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que pour juger que la commune de Comines a commis une faute de nature à engager sa responsabilité à l'égard de M. B...pour la période allant du 8 mai 2006 au 4 janvier 2009, la cour administrative d'appel de Douai s'est bornée à relever qu' " un jugement devenu définitif " a constaté " l'illégalité des décisions par lesquelles la commune de Comines a refusé de réintégrer M. B...dans son poste, puis l'a maintenu en disponibilité d'office jusqu'au mois de janvier 2009 " ;<br/>
<br/>
              2. Considérant toutefois que, d'une part, le jugement du tribunal administratif de Lille du 22 octobre 2008 auquel la cour faisait ainsi référence, a eu pour seul objet d'annuler la décision du 7 décembre 2006 refusant la réintégration de M. B...et les arrêtés des 11 mai et 16 novembre 2007 prononçant sa mise en disponibilité d'office du 11 mai 2007 au 10 novembre 2007 puis du 11 novembre 2007 au 10 mai 2008 ; qu'il ne permettait donc pas d'établir l'existence d'une illégalité fautive avant le 7 décembre 2006 ou après le 10 mai 2008 ; que, d'autre part, la cour ne s'est pas prononcée sur la légalité de l'arrêté du 9 mai 2006 ayant placé M. B...en disponibilité d'office du 11 mai 2006 au 10 mai 2007 ni sur celle de l'arrêté du 5 janvier 2009 l'ayant placé en disponibilité d'office du 11 mai 2007 au 4 janvier 2009, alors que, contrairement à ce que soutient M.B..., la commune de Comines invoquait le bénéfice de l'un et l'autre de ces deux arrêtés ; qu'il résulte de ce qui précède que la cour administrative d'appel de Douai a entaché sa décision d'une erreur de droit en jugeant que le jugement du 22 octobre 2008 du tribunal administratif de Lille suffisait à établir que la commune de Comines avait commis une faute de nature à engager sa responsabilité à l'égard de M. B...pour la période allant du 8 mai 2006 au 4 janvier 2009 ; que la commune de Comines est, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme de 2 000 euros à verser à la commune de Comines au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit, à ce titre, mise à la charge de la commune de Comines, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 10 décembre 2013 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : M. B...versera une somme de 2 000 euros à la commune de Comines au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Comines et à M. A...B....<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
