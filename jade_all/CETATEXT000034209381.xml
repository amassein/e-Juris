<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034209381</ID>
<ANCIEN_ID>JG_L_2017_03_000000397107</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/93/CETATEXT000034209381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 17/03/2017, 397107</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397107</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397107.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Novissen et autres ont demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir l'arrêté du 7 mars 2013 par lequel le préfet de la Somme a accordé à la SA Ramery un permis de construire des bâtiments d'élevage de vaches laitières et un complexe de méthanisation sur le territoire des communes de Buigny Saint-Maclou et de Drucat, ainsi que la décision implicite par laquelle le préfet a rejeté leur recours gracieux. Par un jugement n° 1302391 du 30 juin 2015, le tribunal administratif d'Amiens a rejeté leur demande. <br/>
<br/>
              Par une ordonnance n° 15DA01536, 15DA01598 du 22 décembre 2015, le premier vice-président de la cour administrative d'appel de Douai, président de la première chambre, a, d'une part, donné acte du désistement d'instance de la requête n° 15DA01536 de l'association Novissen et autres tendant à l'annulation de ce jugement, d'autre part, rejeté pour irrecevabilité manifeste la requête n° 15DA01598 des mêmes requérants tendant à l'annulation de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 février et 18 mai 2016 au secrétariat du contentieux du Conseil d'Etat, l'association Novissen et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat, de la SA Ramery et de la SCEA Côte de la Justice la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	le code de l'urbanisme ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de l'association Novissen et autres, et à la SCP Baraduc, Duhamel, Rameix, avocat de la SA Ramery.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 7 mars 2013, le préfet de la Somme a délivré à la société Ramery un permis de construire en vue de l'édification de bâtiments d'élevage de vaches laitières et d'un complexe de méthanisation sur le territoire des communes de Buigny Saint-Maclou et Drucat ; que, par un jugement du 30 juin 2015, le tribunal administratif d'Amiens a rejeté la demande de l'association Novissen et autres tendant à l'annulation pour excès de pouvoir de cet arrêté et de la décision implicite de rejet de leur recours gracieux ; que, par deux requêtes successives identiques, l'association Novissen et autres ont relevé appel de ce jugement ; que par une ordonnance du 22 décembre 2015, contre laquelle l'association Novissen et autres se pourvoient en cassation, le premier vice-président de la cour administrative d'appel de Douai, président de la première chambre, a donné acte d'un désistement d'instance pour la première requête et rejeté la seconde pour irrecevabilité manifeste, en application des dispositions de l'article R. 600-1 du code de l'urbanisme ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 600-1 du code de l'urbanisme, dans sa rédaction alors en vigueur : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un certificat d'urbanisme, d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant un certificat d'urbanisme, une décision de non-opposition à une déclaration préalable ou un permis de construire, d'aménager ou de démolir. L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif. / La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt du déféré ou du recours. / La notification du recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation est réputée accomplie à la date d'envoi de la lettre recommandée avec accusé de réception. Cette date est établie par le certificat de dépôt de la lettre recommandée auprès des services postaux " ; que ces dispositions ne font pas obstacle à ce qu'un requérant qui a omis de notifier dans un délai de quinze jours sa requête à l'auteur de la décision et au titulaire de l'autorisation se désiste de l'instance engagée et présente une nouvelle requête identique, qui sera recevable sous réserve que le délai de recours ne soit pas expiré et que l'obligation de notification prévue par l'article R. 600-1 du code de l'urbanisme soit remplie ;   <br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que l'association Novissen et autres ont relevé appel du jugement du 30 juin 2015, sans justifier du respect de l'obligation de notification prévue par les dispositions de l'article R. 600-1 du code de l'urbanisme ; qu'ils se sont désistés de cette requête puis ont formé une nouvelle requête ayant le même objet ; qu'après avoir donné acte de leur désistement d'instance, le juge d'appel a opposé une irrecevabilité manifeste à la deuxième requête, en se fondant sur le motif tiré de ce que les deux requêtes se rattachant au même litige, le respect de cette obligation de notification devait s'apprécier au regard de la première requête ; qu'il résulte de ce qui a été dit au point 3 qu'en se prononçant ainsi, le premier vice-président de la cour administrative d'appel de Douai, président de la première chambre, a entaché son ordonnance d'une erreur de droit ; <br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes de l'article R. 811-2 du code de justice administrative : " Sauf disposition contraire, le délai d'appel est de deux mois. Il court contre toute partie à l'instance à compter du jour où la notification a été faite à cette partie dans les conditions prévues aux articles R. 751-3 à R. 751-4-1 " ; qu'aux termes de l'article R. 751-3 du code de justice administrative, dans sa rédaction alors en vigueur, antérieure au décret du 2 novembre 2016 portant modification du code de justice administrative : " Sauf disposition contraire, les décisions sont notifiées le même jour à toutes les parties en cause et adressées à leur domicile réel, par lettre recommandée avec demande d'avis de réception, sans préjudice du droit des parties de faire signifier ces décisions par acte d'huissier de justice " ; qu'il résulte de ces dispositions que la notification du jugement doit être adressée à chaque partie, à son domicile réel ; que, par suite, en jugeant que lorsqu'une requête signée par un avocat est présentée au nom d'un grand nombre de personnes physiques ou morales qui n'ont pas désigné l'une d'elles à fin de notification, cet auxiliaire de justice doit être regardé comme le mandataire unique à l'égard duquel les actes de notification sont valablement accomplis, le premier vice-président de la cour administrative d'appel de Douai, président de la première chambre, a entaché son ordonnance d'une autre erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyen du pourvoi, que l'association Novissen et autres sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la société Ramery la somme de 1 500 euros chacun à verser à l'association Novissen et autres au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de la SCEA Côte de la justice à ce titre ; qu'enfin, ces dispositions font obstacle à ce qu'une somme soit mise à la charge l'association Novissen et autres, qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 22 décembre 2015 du premier vice-président de la cour administrative d'appel de Douai, président de la première chambre, est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : L'Etat et la société Ramery verseront chacun à l'association Novissen et autres la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions de la société Ramery présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'association Novissen et à la société Ramery. Les autres requérants seront informés de la présente décision par la SCP Rocheteau, Uzan-Sarano, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, à la ministre du logement et de l'habitat durable et à la SCEA Côte de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - CAS D'UN REQUÉRANT NE JUSTIFIANT PAS DU RESPECT DE L'OBLIGATION DE NOTIFICATION (ART. R. 600-1 DU CODE DE L'URBANISME) - POSSIBILITÉ DE SE DÉSISTER ET DE PRÉSENTER DANS LE DÉLAI DE RECOURS UNE NOUVELLE REQUÊTE AYANT LE MÊME OBJET EN RESPECTANT L'OBLIGATION DE NOTIFICATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-06-01-04 Requérant ayant relevé appel d'un jugement sans justifier du respect de l'obligation de notification prévue par les dispositions de l'article R. 600-1 du code de l'urbanisme. Il pouvait régulièrement se désister et présenter dans le délai de recours une nouvelle requête ayant le même objet en respectant l'obligation de notification.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
