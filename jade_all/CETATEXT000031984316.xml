<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031984316</ID>
<ANCIEN_ID>JG_L_2016_02_000000383149</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/98/43/CETATEXT000031984316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 05/02/2016, 383149, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383149</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2016:383149.20160205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société " Voyages Guirette " a demandé au tribunal administratif de Montpellier, d'une part, l'annulation du marché n° 2008-39 conclu par le syndicat mixte des transports en commun Hérault Transport avec le groupement momentané d'entreprises Pons Laurès et, d'autre part, la condamnation du syndicat mixte à l'indemniser du préjudice né de son éviction irrégulière, à hauteur de 15 000 euros au titre des frais de soumissionnement exposés et à hauteur de 699 976,50 euros au titre du manque à gagner.<br/>
<br/>
              Par un jugement n° 0903521 du 19 novembre 2010, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11MA00297 du 26 mai 2014, la cour administrative d'appel de Marseille a, sur la requête de la société " Voyages Guirette ", annulé ce jugement, résilié le contrat litigieux et rejeté les conclusions indemnitaires de cette société.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 28 juillet et 28 octobre 2014 et le 30 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat mixte des transports en commun Hérault Transport demande au Conseil d'Etat dans le dernier état de ses écritures :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a prononcé la résiliation du contrat conclu entre le syndicat mixte des transports en commun Hérault Transport et le groupement momentané d'entreprises Pons Laurès ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du syndicat mixte des transports en commun Hérault Transport ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le syndicat mixte des transports en commun Hérault Transport a conclu en 2009 avec le groupement d'entreprises Pons Laurès un marché à bons de commande portant sur des services de transports scolaires et réguliers de voyageurs dans le secteur Mont d'Orb - Caroux ; que, la société Voyages Guirette, dont l'offre avait été rejetée par un courrier du 13 mai 2009, a saisi le tribunal administratif de Montpellier d'un recours contestant la validité de ce contrat et de conclusions tendant à ce que le syndicat mixte soit condamné à l'indemniser du préjudice né de son éviction irrégulière ; que, par un arrêt du 26 mai 2014, la cour administrative d'appel de Marseille a, d'une part, annulé le jugement du 19 novembre 2010 par lequel le tribunal administratif de Montpellier avait rejeté cette demande, et, d'autre part, prononcé la résiliation du contrat litigieux et rejeté les conclusions indemnitaires de la société Voyages Guirette ; que le syndicat mixte des transports en commun Hérault Transport se pourvoit en cassation contre cet arrêt en tant qu'il a prononcé la résiliation du contrat ;<br/>
<br/>
              Sur les règles applicables au recours :<br/>
<br/>
              2. Considérant qu'indépendamment des actions dont disposent les parties à un contrat administratif et des actions ouvertes devant le juge de l'excès de pouvoir contre les clauses réglementaires d'un contrat ou devant le juge du référé contractuel sur le fondement des articles L. 551-13 et suivants du code de justice administrative, tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses est recevable à former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles ; que cette action devant le juge du contrat est également ouverte aux membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné ainsi qu'au représentant de l'Etat dans le département dans l'exercice du contrôle de légalité ; que si le représentant de l'Etat dans le département et les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné, compte tenu des intérêts dont ils ont la charge, peuvent invoquer tout moyen à l'appui du recours ainsi défini, les autres tiers ne peuvent invoquer que des vices en rapport direct avec l'intérêt lésé dont ils se prévalent ou ceux d'une gravité telle que le juge devrait les relever d'office ; que le tiers agissant en qualité de concurrent évincé de la conclusion d'un contrat administratif ne peut ainsi, à l'appui d'un recours contestant la validité de ce contrat, utilement invoquer, outre les vices d'ordre public, que les manquements aux règles applicables à la passation de ce contrat qui sont en rapport direct avec son éviction ;<br/>
<br/>
              3. Considérant toutefois que la décision n°358994 du 4 avril 2014 du Conseil d'Etat, statuant au contentieux a jugé que le recours défini ci-dessus ne trouve à s'appliquer, selon les modalités précitées et quelle que soit la qualité dont se prévaut le tiers, qu'à l'encontre des contrats signés à compter de la lecture de cette même décision ; qu'il en résulte que le recours de la société Voyage Guirette, formé le 7 août 2009 devant le tribunal administratif de Montpellier, doit être apprécié au regard des règles applicables avant ladite décision, qui permettaient à tout requérant qui aurait eu intérêt à conclure un contrat administratif d'invoquer tout moyen à l'appui de son recours contre le contrat ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en retenant, pour résilier le marché public contesté, un moyen tiré de l'illégalité de la durée de cet accord-cadre, sans avoir recherché si ce moyen pouvait être utilement invoqué par la société, eu égard à l'intérêt lésé dont elle se prévalait, la cour n'a pas commis d'erreur de droit ni méconnu l'étendue de son office ; <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 77 du code des marchés publics : " (...) II. - La durée des marchés à bons de commande ne peut dépasser quatre ans, sauf dans des cas exceptionnels dûment justifiés, notamment par leur objet ou par le fait que leur exécution nécessite des investissements amortissables sur une durée supérieure à quatre ans. / L'émission des bons de commande ne peut intervenir que pendant la durée de validité du marché. (...) " ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le contrat litigieux a été passé pour une durée de six ans afin, comme le soutenait le pouvoir adjudicateur, " de tenir compte des investissements nécessaires, conséquences des exigences qualitatives en matière de sécurité, d'accessibilité et de normes environnementales, figurant dans le dossier de consultation des entreprises " ; qu'en jugeant, après avoir notamment relevé dans sa décision, d'une part, que la durée d'amortissement des véhicules utilisés, retenue par l'administration fiscale, était de quatre ou cinq ans et, d'autre part, que l'attributaire pouvait utiliser des véhicules d'une ancienneté maximale de dix ans, que les exigences qualitatives prévues par les documents de la consultation ne plaçaient pas le syndicat mixte dans un cas exceptionnel justifiant qu'il fût dérogé à la durée de quatre ans prévue par les dispositions citées ci-dessus du code des marchés publics, la cour n'a pas donné aux faits ainsi énoncés une qualification juridique erronée ; qu'elle n'a, en tout état de cause, pas non plus commis d'erreur de droit en tenant compte des possibilités de réutilisation ou de revente des véhicules nécessaires à l'exécution du contrat litigieux pour apprécier les modalités de leur amortissement ;<br/>
<br/>
              7. Considérant, en second lieu, que si le syndicat mixte des transports en commun Hérault Transport soutient que la cour aurait méconnu l'étendue de son office et commis une erreur de droit en prononçant la résiliation du contrat sans s'assurer que cette mesure n'était pas de nature à porter atteinte à l'intérêt général, il ressort des écritures du syndicat devant la cour administrative d'appel que ce dernier demandait à la cour, dans l'hypothèse où elle estimerait que l'illégalité invoquée par la société appelante était de nature à vicier la procédure, de ne pas annuler le marché, eu égard précisément à la nécessaire protection de l'intérêt général, mais de se limiter à en prononcer la résiliation ; qu'eu égard à l'absence de tout élément de nature à établir que la résiliation du contrat était elle-même de nature à porter une atteinte excessive à l'intérêt général, il s'ensuit que le syndicat mixte des transports en commun Hérault Transport n'est pas fondé à soutenir qu'en faisant droit à cette demande, la cour administrative d'appel de Marseille aurait commis une erreur de droit ou méconnu l'étendue de son office ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi du syndicat mixte des transports en commun Hérault Transport ne peut qu'être rejeté ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du syndicat mixte des transports en commun Hérault Transport est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat mixte des transports en commun Hérault Transport, à la société Voyages Guirette et au groupement momentané d'entreprises Pons Laurès.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - RECOURS DÉFINI PAR LA JURISPRUDENCE TARN-ET-GARONNE [RJ1] - 1) APPLICATION DANS LE TEMPS - CONTRATS SIGNÉS À COMPTER DE LA LECTURE DE LA DÉCISION, Y COMPRIS POUR LES CONCURRENTS ÉVINCÉS  - 2) MOYENS INVOCABLES - CONCURRENT ÉVINCÉ - MANQUEMENTS AUX RÈGLES DE PASSATION DU CONTRAT EN RAPPORT AVEC SON ÉVICTION.
</SCT>
<ANA ID="9A"> 39-08-01-03 1) La décision n° 358994 du 4 avril 2014 du Conseil d'Etat statuant au contentieux a jugé que le recours de plein contentieux des tiers contre le contrat qu'elle a défini ne trouve à s'appliquer, selon les modalités qu'elle précise et quelle que soit la qualité dont se prévaut le tiers, qu'à l'encontre des contrats signés à compter de la lecture de cette même décision.... ,,2) Si le représentant de l'Etat dans le département et les membres de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné, compte tenu des intérêts dont ils ont la charge, peuvent invoquer tout moyen à l'appui d'un recours de plein contentieux contre un contrat, les autres tiers ne peuvent invoquer que des vices en rapport direct avec l'intérêt lésé dont ils se prévalent ou ceux d'une gravité telle que le juge devrait les relever d'office. Le tiers agissant en qualité de concurrent évincé de la conclusion d'un contrat administratif ne peut ainsi, à l'appui d'un recours contestant la validité de ce contrat, utilement invoquer, outre les vices d'ordre public, que les manquements aux règles applicables à la passation de ce contrat qui sont en rapport direct avec son éviction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
