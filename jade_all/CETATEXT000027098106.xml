<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027098106</ID>
<ANCIEN_ID>JG_L_2013_01_000000349806</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/81/CETATEXT000027098106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 22/01/2013, 349806</TITRE>
<DATE_DEC>2013-01-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349806</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:349806.20130122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er juin  et 1er septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. B... A..., demeurant au... ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT02597 du 28 octobre 2010 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 09135 du 10 novembre 2009 du tribunal administratif de Nantes constatant un non-lieu à statuer sur sa demande tendant à l'annulation de la décision implicite du directeur interrégional des services pénitentiaires de Rennes rejetant son recours préalable formé le 10 décembre 2008 contre la sanction disciplinaire, assortie du sursis, qui lui a été infligée le 9 décembre 2008 par le président de la commission de discipline de la maison d'arrêt des hommes de Nantes, d'autre part, à l'annulation, pour excès de pouvoir, de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à son avocat au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Spinosi, avocat de M.A...,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article D. 251-6 du code de procédure pénale, alors en vigueur et dont les dispositions ont été reprises aux articles R. 57-7-54 à  R. 57-7-57 du même code : " Le président de la commission de discipline peut accorder le bénéfice du sursis pour tout ou partie de l'exécution de la sanction disciplinaire soit lors du prononcé de celle-ci, soit au cours de son exécution. / Lorsqu'il octroie le bénéfice du sursis, le président de la commission de discipline fixe un délai de suspension de la sanction sans que celui-ci puisse excéder six mois. (...) / Si, au cours du délai de suspension de la sanction, le détenu n'a commis aucune faute disciplinaire donnant lieu à une sanction, la sanction assortie du sursis est réputée non avenue. Il en est fait mention sur le registre prévu par l'article D. 250-6 " ; qu'aux termes de l'article D. 250-6 du même code, devenu l'article R. 57-7-30 : " Les sanctions disciplinaires prononcées sont inscrites sur un registre tenu sous l'autorité du chef d'établissement. Ce registre est présenté aux autorités administratives et judiciaires lors de leurs visites de contrôle ou d'inspection " ;<br/>
<br/>
              2. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, alors qu'il était détenu à..., ; qu'après avoir relevé qu'au cours de ce délai expirant le 5 juin 2009, le requérant n'avait pas commis de nouvelle faute disciplinaire et jugé que cette sanction était ainsi réputée non avenue depuis cette date, en application des dispositions de l'article D. 251-6 du code de procédure pénale, sans avoir produit d'effet, la cour administrative d'appel de Nantes en a déduit que le recours pour excès de pouvoir, enregistré le 12 janvier 2009 au greffe du tribunal administratif de Nantes, tendant à l'annulation de la décision implicite de rejet née de l'absence de réponse au recours préalable dirigé contre cette sanction et adressé par M. A...le 10 décembre 2008 au directeur interrégional des services pénitentiaires de Rennes, était devenu sans objet lorsque les premiers juges ont statué le 10 novembre 2009 sur cette demande ;<br/>
<br/>
              3. Considérant toutefois que, lorsque la sanction disciplinaire assortie du sursis est réputée non avenue, il en est fait mention, ainsi que l'imposent les dispositions qui figuraient alors à l'article D. 251-6 du code de procédure pénale, sur le registre tenu sous l'autorité du chef d'établissement ; qu'eu égard aux effets que cette mention est susceptible le cas échéant d'emporter, les conclusions dirigées contre une telle sanction ne peuvent être regardées, en l'absence de tout effacement de celle-ci, comme ayant perdu leur objet, alors même que cette sanction n'est plus susceptible de recevoir exécution ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, M. A...est fondé à soutenir que la cour a commis une erreur de droit en confirmant le jugement du tribunal administratif qui avait prononcé un non-lieu et à demander, pour ce motif, l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Spinosi renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cet avocat ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 28 octobre 2010 de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'Etat versera à Me Spinosi, avocat de M.A..., la somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve qu'il renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DISCIPLINE DES DÉTENUS - SANCTION DISCIPLINAIRE ASSORTIE DU SURSIS - HYPOTHÈSE DANS LAQUELLE LA SANCTION EST RÉPUTÉE NON AVENUE - CONSÉQUENCE - PERTE D'OBJET DES CONCLUSIONS DIRIGÉES CONTRE CETTE SANCTION - ABSENCE, EU ÉGARD AUX EFFETS QUE LA MENTION PORTÉE SUR LE REGISTRE TENU SOUS L'AUTORITÉ DU CHEF D'ÉTABLISSEMENT EST SUSCEPTIBLE D'EMPORTER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-05-01 PROCÉDURE. INCIDENTS. NON-LIEU. ABSENCE. - SANCTION DISCIPLINAIRE D'UN DÉTENU ASSORTIE DU SURSIS - HYPOTHÈSE DANS LAQUELLE LA SANCTION EST RÉPUTÉE NON AVENUE - CONSÉQUENCE - PERTE D'OBJET DES CONCLUSIONS DIRIGÉES CONTRE CETTE SANCTION - ABSENCE, EU ÉGARD AUX EFFETS QUE LA MENTION PORTÉE SUR LE REGISTRE TENU SOUS L'AUTORITÉ DU CHEF D'ÉTABLISSEMENT EST SUSCEPTIBLE D'EMPORTER.
</SCT>
<ANA ID="9A"> 37-05-02-01 Lorsque la sanction disciplinaire d'un détenu assortie du sursis est réputée non avenue, il en est fait mention, ainsi que l'imposent les dispositions figurant à l'article D. 251-6 du code de procédure pénale (désormais reprises aux articles R. 57-7-54 à  R. 57-7-57 du même code), sur le registre tenu sous l'autorité du chef d'établissement. Eu égard aux effets que cette mention est susceptible le cas échéant d'emporter, les conclusions dirigées contre une telle sanction ne peuvent être regardées, en l'absence de tout effacement de celle-ci, comme ayant perdu leur objet, alors même que cette sanction n'est plus susceptible de recevoir exécution.</ANA>
<ANA ID="9B"> 54-05-05-01 Lorsque la sanction disciplinaire d'un détenu assortie du sursis est réputée non avenue, il en est fait mention, ainsi que l'imposent les dispositions figurant à l'article D. 251-6 du code de procédure pénale (désormais reprises aux articles R. 57-7-54 à  R. 57-7-57 du même code), sur le registre tenu sous l'autorité du chef d'établissement. Eu égard aux effets que cette mention est susceptible le cas échéant d'emporter, les conclusions dirigées contre une telle sanction ne peuvent être regardées, en l'absence de tout effacement de celle-ci, comme ayant perdu leur objet, alors même que cette sanction n'est plus susceptible de recevoir exécution.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
