<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861450</ID>
<ANCIEN_ID>JG_L_2016_01_000000386186</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861450.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 06/01/2016, 386186, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386186</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:386186.20160106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société anonyme d'HLM Antin Résidences a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2007 et 2008 à raison d'un immeuble situé 17, rue de l'Espérance, à Montigny-lès-Cormeilles (Val-d'Oise). Par un jugement n° 1209283 du 9 octobre 2014, le tribunal administratif a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi, enregistré le 3 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Antin Résidences d'HLM ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la SA d'HLM Antin Résidences est propriétaire d'un immeuble situé 17, rue de l'Espérance, à Montigny-lès-Cormeilles (Val-d'Oise) ; que cet immeuble était, aux 1er janvier 2007 et 1er janvier 2008, loué à l'association Aftam ; que l'association Aftam a occupé l'immeuble afin d'y offrir des prestations hôtelières et para-hôtelières à des personnes défavorisées sans domicile fixe, immigrées ou réfugiées, et que l'immeuble abritait en outre un foyer de travailleurs migrants ; que, pour actualiser la valeur locative retenue pour l'assiette de la taxe foncière sur les propriétés bâties due par la société à raison de cet immeuble au titre des années 2007 et 2008, l'administration a appliqué le coefficient d'actualisation de 2,25 retenu pour les locaux commerciaux ; que la société a saisi le tribunal administratif de Cergy-Pontoise d'une demande tendant à ce que lui soit appliqué le coefficient d'actualisation de 1,70 retenu pour les locaux d'habitation sur le fondement de l'article 1518 II ter du code général des impôts au motif que l'association Aftam est un organisme privé à but non lucratif ; que le ministre des finances et des comptes publics se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Cergy-Pontoise a fait droit à cette demande ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1518 du code général des impôts : " I. Dans l'intervalle de deux révisions générales, les valeurs locatives définies aux I et II de l'article 1496 et aux articles 1497 et 1498 (...) sont actualisées tous les trois ans au moyen de coefficients correspondant à l'évolution de ces valeurs, entre la date de référence de la dernière révision générale et celle retenue pour l'actualisation. (...). II ter. Pour l'application du présent article, la valeur locative des locaux occupés par les organismes privés à but non lucratif est actualisée au moyen du coefficient applicable aux locaux mentionnés à l'article 1496 (...) " ;<br/>
<br/>
              3. Considérant que, pour l'application de ces dispositions, une association doit être regardée comme un organisme privé à but non lucratif si, d'une part, sa gestion présente un caractère désintéressé et si, d'autre part, les services qu'elle rend ne sont pas offerts en concurrence dans la même zone géographique d'attraction avec ceux proposés au même public par des entreprises commerciales exerçant une activité identique ; que, toutefois, même dans le cas où l'association intervient dans un domaine d'activité et dans un secteur géographique où existent des entreprises commerciales, le but non lucratif lui est reconnu si elle exerce son activité dans des conditions différentes de celles des entreprises commerciales, soit en répondant à certains besoins insuffisamment satisfaits par le marché, soit en s'adressant à un public qui ne peut normalement accéder aux services offerts par les entreprises commerciales, notamment en pratiquant des prix inférieurs à ceux du secteur  concurrentiel et à tout le moins des tarifs modulés en fonction de la  situation des bénéficiaires, sous réserve de ne pas recourir à des méthodes commerciales excédant les besoins de l'information du public sur  les services qu'elle offre ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, d'une part, qu'il n'est pas contesté, en l'espèce, que la gestion de l'association Aftam était désintéressée ; que, d'autre part, au regard des éléments qui lui étaient soumis, le tribunal administratif a constaté, par une appréciation souveraine dont il n'est d'ailleurs pas soutenu qu'elle serait entachée de dénaturation, que cette association n'exerçait pas son activité dans les mêmes conditions que celles des entreprises commerciales ; qu'il a pu en déduire, sans erreur de droit ni erreur de qualification juridique, que cette association devait être regardée comme poursuivant un but non lucratif et que les locaux litigieux devaient à ce titre se voir appliquer le coefficient prévu à l'article 1496 du code général des impôts ; qu'à cet égard, le ministre ne saurait utilement invoquer pour la première fois devant le juge de cassation la circonstance que cette association fournirait des prestations hôtelières et para-hôtelières similaires, par le public visé et les conditions dans lesquelles elles sont proposées, à celles offertes par des structures telles que la société d'économie mixte Adoma et l'association Adef, qui seraient soumises aux impôts commerciaux ;  <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
<br/>
Article 2 : Le ministre des finances et des comptes publics versera à la SA d'HLM Antin Résidences une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SA d'HLM Antin Résidences.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
