<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036117322</ID>
<ANCIEN_ID>JG_L_2017_11_000000408389</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/11/73/CETATEXT000036117322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 29/11/2017, 408389, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408389</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Recours en révision</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:408389.20171129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon de prononcer la réduction des cotisations d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2008 et 2009 et de la taxe d'habitation mise à sa charge au titre de l'année 2010. Par un jugement n° 1201764 du 25 mars 2014, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14LY01734 du 27 août 2015, la cour administrative d'appel de Lyon a, d'une part, transmis au Conseil d'Etat les conclusions d'appel de M. B... relatives à la taxe d'habitation et, d'autre part, rejeté celles qui portaient sur l'impôt sur le revenu.<br/>
<br/>
              Par une décision nos 393214 et 394154 du 28 décembre 2016 le Conseil d'Etat statuant au contentieux a rejeté les pourvois formés par M. B...contre ce jugement et cet arrêt.<br/>
<br/>
              Par un recours en révision et en rectification d'erreur matérielle enregistré le 27 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer nulle et non avenue sa décision nos 393214, 394154 du 28 décembre 2016 ;<br/>
<br/>
              2°) statuant à nouveau sur les pourvois, de rouvrir l'instruction et de communiquer la question prioritaire de constitutionnalité qu'il a déposée avec sa note en délibéré. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 834-1 du code de justice administrative : " Le recours en révision contre une décision contradictoire du Conseil d'Etat ne peut être présenté que dans trois cas : 1° Si elle a été rendue sur pièces fausses ; / 2° Si la partie a été condamnée faute d'avoir produit une pièce décisive qui était retenue par son adversaire ; / 3° Si la décision est intervenue sans qu'aient été observées les dispositions du présent code relatives à la composition de la formation de jugement, à la tenue des audiences ainsi qu'à la forme et au prononcé de la décision ". L'article R. 833-1 du même code dispose que : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification. / Ce recours doit être présenté dans les mêmes formes que celles dans lesquelles devait être introduite la requête initiale. Il doit être introduit dans un délai de deux mois qui court du jour de la notification ou de la signification de la décision dont la rectification est demandée. / (...) ". Le recours en rectification d'erreur matérielle ouvert par cet article présente un caractère subsidiaire par rapport au recours en révision, et n'est recevable que si son objet ne peut pas être atteint par l'exercice de ce dernier.<br/>
<br/>
              Sur le recours en révision :<br/>
<br/>
              2. En premier lieu, M. B... soutient que le premier mémoire en défense du 6 décembre 2016 produit par le ministre de l'économie et des finances sous le n° 393214 ne lui a pas été communiqué, en violation de l'article R. 611-1 du code de justice administrative qui prévoit que " la requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties (...) ". Ce moyen, tiré de la méconnaissance du caractère contradictoire de la procédure, ne constitue pas un cas d'ouverture du recours en révision.<br/>
<br/>
              3. En deuxième lieu, M. B... soutient que l'interprétation de la loi proposée par le rapporteur public et retenue par la décision, en substitution des motifs de l'arrêt de la cour administrative d'appel, n'était défendue par aucune des parties et que celles-ci n'en ont pas été informées, en violation de l'article R. 611-7 du code de justice administrative en vertu duquel " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la chambre chargée de l'instruction en informe les parties avant la séance de jugement (...) ". Ce moyen, également tiré de ce que la décision aurait été rendue en méconnaissance du caractère contradictoire de la procédure, ne constitue pas davantage un cas d'ouverture du recours en révision.<br/>
<br/>
              4. En troisième lieu, M. B...soutient que la question prioritaire de constitutionnalité qu'il a présentée, à l'appui de chacun de ses deux pourvois, par un mémoire distinct joint à la note en délibéré déposée le 16 décembre 2016, aurait dû être visée séparément dans la décision et examinée par la formation de jugement dès lors qu'elle se rapportait aux dispositions législatives telles qu'interprétées par le rapporteur public. Toutefois, d'une part, le moyen tiré de l'absence de visa de ces mémoires distincts manque en fait, dès lors que la décision vise, dans chaque affaire, " les notes en délibéré, enregistrées le 16 décembre 2016 ", cette mention faisant nécessairement référence, en l'espèce, à la note en délibéré et au mémoire distinct joint à celle-ci et, d'autre part, le moyen tiré de ce que la formation de jugement aurait dû examiner cette question prioritaire de constitutionnalité ne relève d'aucun des cas d'ouverture du recours en révision, qui sont limitativement énumérés à l'article R. 834-1 du code de justice administrative cité au point 1.<br/>
<br/>
              5. Il résulte de ce qui précède que le recours en révision présenté par <br/>
M. B...ne peut qu'être rejeté.<br/>
<br/>
              Sur le recours en rectification d'erreur matérielle :<br/>
<br/>
              6. Les irrégularités de procédure alléguées, mentionnées aux points 2 et 3 <br/>
ci-dessus, qui se rapportent au caractère contradictoire de la procédure, ne constituent pas, en tout état de cause, des erreurs matérielles au sens des dispositions de l'article R. 833-1 du code de justice administrative cité au point 1. Par ailleurs, il résulte de ce qui est dit au point 4 que le moyen tiré de l'absence de visa des mémoires soulevant une question prioritaire de constitutionnalité manque en fait. <br/>
<br/>
              		    7. Il résulte de ce qui précède que le recours en rectification d'erreur matérielle formé par M. B...ne peut qu'être rejeté.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
