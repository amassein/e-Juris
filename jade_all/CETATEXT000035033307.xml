<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035033307</ID>
<ANCIEN_ID>JG_L_2017_06_000000403763</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/03/33/CETATEXT000035033307.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 23/06/2017, 403763, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403763</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Marie-Laure Denis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet De Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403763.20170623</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...a demandé au juge des référés du tribunal administratif de Châlons-en-Champagne, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du garde des sceaux, ministre de la justice du 29 mars 2016 décidant sa réintégration au centre pénitentiaire de Villenauxe-la-Grande à compter du 1er avril 2016, après son détachement puis son placement en position de disponibilité pour une période de trois mois. Par une ordonnance n° 1601187 du 30 juin 2016, le juge des référés du tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 26 septembre 2016, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761 1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2009-972 du 3 août 2009 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Laure Denis, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. A...a soulevé le moyen tiré de la violation de l'article 4 et de l'article 5 de la loi du 3 août 2009 portant sur la mobilité et les parcours professionnels dans la fonction publique en précisant que ce dernier article a modifié l'article 45 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat relatif à la réintégration des fonctionnaires au terme de leur détachement ; qu'eu égard à la teneur de ses écritures et à l'office du juge des référés, en se bornant à relever que n'était pas de nature à faire naître un doute sérieux sur la légalité de la décision attaquée le moyen tiré de la méconnaissance des articles 4 et 5 de la loi du 3 août 2009, le juge des référés a suffisamment motivé son ordonnance ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 45 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " A l'expiration de son détachement, le fonctionnaire est, sauf intégration dans le corps ou cadre d'emplois de détachement, réintégré dans son corps d'origine. Il est tenu compte, lors de sa réintégration, du grade et de l'échelon qu'il a atteints ou auxquels il peut prétendre à la suite de la réussite à un concours ou à un examen professionnel ou de l'inscription sur un tableau d'avancement au titre de la promotion au choix dans le corps ou cadre d'emplois de détachement sous réserve qu'ils lui soient plus favorables. " ; que, contrairement à ce que soutient M.A..., il ne résulte de ces dispositions ni que le fonctionnaire réintégré dans son corps d'origine ait droit à ce que cette réintégration se fasse sur l'emploi qu'il occupait avant son détachement ni même dans le service où il était affecté, ni que l'autorité compétente soit tenue, pour décider de sa nouvelle affectation, de tenir compte de sa situation familiale ; qu'il ressort, en tout état de cause, de ses écritures produites devant le juge des référés que M. A...n'a pas soulevé le moyen, qui n'est pas d'ordre public, tiré de la méconnaissance de l'article 60 de la même loi qui prévoit que dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille ; que, en outre, il ressort des pièces du dossier soumis au juge des référés qu'avant la fin de sa période de détachement M. A...avait sollicité sa réintégration au centre pénitentiaire de Rémire-Montjoly, où il exerçait auparavant les fonctions de surveillant, centre situé en Guyane où réside sa famille ; que, faute d'emploi vacant dans ce centre, cette demande n'a pu être satisfaite ; qu'afin de régulariser la situation administrative de l'intéressé, qui avait informé son administration de sa décision de la réintégrer moins de trois mois avant la fin de son détachement, en méconnaissance de la réglementation en vigueur, il a été procédé à sa mise en disponibilité pour convenances personnelles, jusqu'au 31 mars 2016 ; que, par un courrier du 29 février 2016, M. A...a accepté son affectation sur un emploi vacant au centre de détention de Villenauxe-la-Grande, qui est situé en métropole ; que, par un arrêté du 23 mars 2016, l'intéressé a été réintégré à compter du 1er avril suivant dans ce centre ; que M.A...  a été informé de ce qu'à la suite de sa réintégration les voeux qu'il serait susceptible de formuler pour obtenir un rapprochement familial seraient examinés lors de la prochaine commission administrative paritaire de mobilité des surveillants ; qu'il résulte de tout ce qui précède que, eu égard à son office, en estimant que n'étaient pas de nature à faire naître, en l'état de l'instruction, un doute sérieux sur la légalité de la décision attaquée, les moyens tirés de ce que l'arrêté du 29 mars 2016 portait une atteinte disproportionnée au droit de l'intéressé au respect de sa vie privée et familiale, en méconnaissance de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 3-1 de la convention internationale relative aux droits de l'enfant, et méconnaissait l'article 45 de la loi du 11 janvier 1984 modifié par la loi du 3 août 2009, le juge des référés, n'a entaché son ordonnance ni de dénaturation ni d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. C...A...et à la garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
