<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030742890</ID>
<ANCIEN_ID>JG_L_2015_06_000000384448</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/74/28/CETATEXT000030742890.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 15/06/2015, 384448, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384448</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384448.20150615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté en date du 15 mai 2013 par lequel le préfet de la Haute-Savoie a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français et a fixé le pays de destination. Par un jugement n° 1302835 du 20 septembre 2013, le tribunal administratif a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 13LY02796 du 8 juillet 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par le préfet de la Haute-Savoie contre ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 11 septembre 2014 et 26 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., né en Arménie, a été confié au service de l'aide sociale à l'enfance depuis son arrivée sur le territoire français en janvier 2010 à l'âge de quinze ans et jusqu'à sa majorité ; qu'il a fait l'objet, par arrêté du 15 mai 2013 du préfet de la Haute-Savoie, d'un refus d'admission au séjour et d'une obligation de quitter le territoire français dans le délai de trente jours à destination du pays dont il a la nationalité ou de tout pays pour lequel il établit qu'il est légalement admissible ; que le tribunal administratif de Grenoble, par jugement du 20 septembre 2013, a annulé cet arrêté pour le motif que le préfet avait inexactement appliqué les dispositions du 2° bis de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que si, sur appel du préfet, la cour administrative de Lyon, par un arrêt du 8 juillet 2014, a censuré le motif sur lequel s'était fondé le tribunal administratif, elle a cependant jugé que, dans les circonstances particulières de l'espèce, le préfet avait commis une erreur manifeste dans l'appréciation des conséquences de sa décision sur la situation personnelle de l'intéressé ; que le ministre de l'intérieur se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2.	Considérant que, pour demander l'annulation de l'arrêt qu'il attaque, le ministre fait valoir que la cour s'est fondée à tort sur la volonté de l'intéressé de s'insérer en France, qu'elle n'a pas tenu compte d'un courrier électronique des services du département de la Haute-Savoie du 25 octobre 2012 relevant l'absence de projet éducatif et qu'aucune pièce du dossier produite ne révèle que l'intéressé n'entretient plus de rapport avec sa mère qui vit en Russie ; que, toutefois, la cour administrative d'appel, en estimant que le préfet avait commis une erreur manifeste dans l'appréciation des conséquences de sa décision sur la situation personnelle de l'intéressé, s'est livrée à une appréciation souveraine des faits de l'espèce, laquelle est exempte de dénaturation ; qu'il s'ensuit que le pourvoi du ministre de l'intérieur doit être rejeté ; <br/>
<br/>
              3. Considérant que M. A... a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc-Duhamel-Rameix, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Baraduc-Duhamel-Rameix ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Baraduc-Duhamel-Rameix, avocat de M.A..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
