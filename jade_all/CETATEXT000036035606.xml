<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036035606</ID>
<ANCIEN_ID>JG_L_2017_10_000000393456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/03/56/CETATEXT000036035606.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/10/2017, 393456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BOULLOCHE ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:393456.20171026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E...I...et M. C...I..., agissant tant en leur nom personnel qu'en qualité de représentants légaux de leurs fils Luca et Tanguy, M. et Mme B...I...et M. et Mme D...J...ont demandé au tribunal administratif de Grenoble de condamner le centre hospitalier de Montélimar à les indemniser des préjudices qu'ils estiment avoir subis du fait des fautes commises lors de l'accouchement de MmeI..., le 27 août 1991. Par un jugement n° 0604299 du 14 octobre 2010, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n°10LY02764 du 6 octobre 2011, la cour administrative d'appel de Lyon a rejeté l'appel formé par les requérants contre ce jugement.<br/>
<br/>
              Par une décision n°354713 du 30 avril 2014, le Conseil d'Etat a annulé cet arrêt.<br/>
<br/>
              Par un arrêt n° 14LY01483 du 9 juillet 2015, la cour administrative d'appel de Lyon  a rejeté l'appel formé par les requérants contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 10 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme E...J...divorcéeI..., agissant tant en son nom personnel qu'en qualité de représentante de son fils Luca, M. C...I..., Mme E...G..., M. et Mme D...J...et M. F...I...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Montélimar la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de la sécurité sociale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat des consortsJ..., à la SCP Célice, Soltner, Texidor, Perier, avocat du centre hospitalier de Montélimar et à la SCP Foussard, Froger, avocat de la caisse primaire d'assurance maladie de l'Ardèche ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme E...J..., alors mariée à M. C...I..., a donné naissance, le 27 mars 1991, au centre hospitalier de Montélimar, à un enfant, Luca, en état de mort apparente ; que, placé sous respirateur artificiel, celui-ci a conservé de graves séquelles neurologiques ; que M. et Mme I...et plusieurs membres de leurs familles ont saisi le tribunal administratif de Grenoble d'une requête tendant à la condamnation du centre hospitalier de Montélimar à les indemniser des préjudices subis ; que la caisse primaire d'assurance maladie (CPAM) de l'Ardèche a présenté des conclusions tendant au remboursement de ses débours ; que le tribunal, après avoir prescrit deux expertises médicales, a rejeté leur demande indemnitaire ; que par un arrêt du 6 octobre 2011, la cour administrative d'appel de Lyon a rejeté l'appel formé par les consorts I...et J...contre ce jugement ; que ceux-ci s'étant pourvus en cassation contre cet arrêt, le Conseil d'Etat, statuant au contentieux, par une décision du 30 avril 2014, l'a annulé et a renvoyé l'affaire à la cour ; que les appelants se pourvoient en cassation contre l'arrêt du 9 juillet 2015 par lequel cette cour a de nouveau rejeté leur appel ;<br/>
<br/>
              2. Considérant qu'en estimant, au vu des rapports d'expertise, que le choix d'un accouchement par voie basse était, dans les circonstances de l'espèce, conforme aux données médicales de l'époque, la cour administrative d'appel de Lyon s'est livrée à une appréciation souveraine des faits, exempte de dénaturation ; <br/>
<br/>
              3. Considérant qu'en estimant, au vu du rapport établi par les seconds experts judiciaires, qu'il ne résultait pas de l'instruction que les manoeuvres d'extraction de l'enfant réalisées successivement par l'interne puis par le chef de service n'auraient pas été conformes aux bonnes pratiques, la cour s'est livrée à une appréciation souveraine exempte de dénaturation et n'a pas entaché son arrêt de contradiction de motifs ; qu'en en déduisant que les requérants n'étaient pas fondés à soutenir que les médecins du centre hospitalier auraient commis une faute de nature à engager la responsabilité de l'établissement, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              4. Considérant que la cour a relevé que selon les experts judiciaires, l'obstétricien et chef de service aurait dû rester aux côtés de l'interne tout au long de l'accouchement et qu'il aurait en particulier dû être présent lorsque ce dernier a tenté en vain de procéder aux manoeuvres d'extraction ; qu'elle a cependant estimé, en se livrant à une appréciation souveraine exempte de dénaturation, qu'il ne pouvait être considéré de façon suffisamment certaine que son absence momentanée aurait contribué, même partiellement, à la survenue du dommage dont les requérants demandaient réparation, dès lors qu'il ne résultait de l'instruction ni que l'interne n'aurait pas disposé des compétences requises pour assurer initialement la prise en charge, ni que les manoeuvres réalisées par ce dernier sans succès auraient été contraires aux bonnes pratiques, ni que le délai d'intervention aurait été excessivement long ;<br/>
<br/>
              5. Considérant que la cour a pu, sans entacher son arrêt d'erreur de droit ou de contradiction de motifs, juger tout à la fois qu'il ne résultait pas de l'instruction que les lésions constatées auraient pour origine une faute commise par les médecins dans la réalisation des manoeuvres d'extraction de l'enfant et que les conditions posées, en l'état du droit alors applicable, pour la mise en jeu de la responsabilité sans faute du centre hospitalier n'étaient pas réunies ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les consorts J...et I...et la CPAM de l'Ardèche ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du centre hospitalier, qui n'est pas la partie perdante dans la présente instance, les sommes que les consorts J...et I...et la CPAM de l'Ardèche demandent sur leur fondement ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des requérants la somme demandée par le centre hospitalier au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des consorts J...et des consorts I...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par la caisse primaire d'assurance maladie de l'Ardèche sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées par le centre hospitalier de Montélimar sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...I..., à Mme E...J..., à M. C... I..., à M. et Mme D...J..., à Mme E...G...veuveI..., à M. F...I...au centre hospitalier de Montélimar et à la caisse primaire d'assurance maladie de l'Ardèche. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
