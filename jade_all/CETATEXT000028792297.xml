<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028792297</ID>
<ANCIEN_ID>JG_L_2014_03_000000355620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/79/22/CETATEXT000028792297.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 28/03/2014, 355620, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355620.20140328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 6 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement ; la ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er, 2 et 3 de l'arrêt n° 10PA03659 du 25 octobre 2011 par lequel la cour administrative d'appel de Paris a annulé le jugement n° 0604829/2-2 du 21 mai 2010 par lequel le tribunal administratif de Paris a rejeté la demande de M. et Mme A...B...tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu mis à leur charge au titre de l'année 2000 et a prononcé la décharge de ces cotisations supplémentaires d'impôt sur le revenu ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par M. et Mme B...; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, Auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 80 ter du code général des impôts, dans sa rédaction applicable à l'année d'imposition en litige : " a) Les indemnités, remboursements et allocations forfaitaires pour frais versés aux dirigeants de sociétés sont, quel que soit leur objet, soumis à l'impôt sur le revenu. / b) Ces dispositions sont applicables : / 1° Dans les sociétés anonymes : au président du conseil d'administration ; au directeur général ; à l'administrateur provisoirement délégué; aux membres du directoire ; à tout administrateur ou membre du conseil de surveillance chargé de fonctions spéciales ; / (...) 3° Dans les autres entreprises ou établissements passibles de l'impôt sur les sociétés : aux dirigeants soumis au régime fiscal des salariés (...) " ; qu'en vertu du 1 de l'article 80 duodecies du même code, dans sa rédaction applicable à l'année d'imposition en litige, les indemnités versées à un salarié à l'occasion de la rupture de son contrat de travail constituent une rémunération imposable, sous réserve des exceptions prévues par ce texte ; qu'aux termes du 2 de ce même article : " Constitue également une rémunération imposable toute indemnité versée, à l'occasion de la cessation de leurs fonctions, aux mandataires sociaux, dirigeants et personnes visés à l'article 80 ter. (...)" ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., qui a effectué toute sa carrière au sein de la mutualité sociale agricole, a exercé les fonctions de directeur général adjoint de la Caisse centrale des assurances mutuelles agricoles (CCAMA) à compter de 1993, ainsi que celles de directeur général délégué de la société Groupama SA, filiale à 100% de la CCAMA à compter de 1995 ; qu'il a, par ailleurs, été nommé vice-président du directoire de la société Groupama SA à compter du 26 avril 2000 ; qu'il a été mis fin au mois de juin 2000 à l'ensemble de ses fonctions et mandats au sein de la CCAMA et de la société Groupama SA, sociétés auxquelles il était lié par des contrats de travail distincts mais indissociables ; qu'en application d'un protocole transactionnel conclu le 7 août 2000, M. B... a perçu une indemnité globale ;<br/>
<br/>
              3. Considérant qu'en jugeant que, en raison de la nature des fonctions qu'il avait exercées respectivement comme directeur général adjoint de la CCAMA durant sept ans et directeur général délégué de la société Groupama SA durant cinq ans, fonctions dont elle a relevé, y compris pour celles exercées au sein de la société Groupama SA, qu'elles se caractérisaient par l'existence d'un lien de subordination par rapport au président du conseil d'administration de la CCAMA et à son directeur général, et, par suite, ne correspondaient pas aux fonctions énumérées à l'article 80 ter, M. B...ne pouvait être regardé comme un " dirigeant " au sens de ces dispositions en dépit de la circonstance que, sept semaines avant son licenciement, il avait été nommé vice-président du directoire de la société Groupama SA, fonction visée au 1° de ce même article 80 ter, la cour n'a pas entaché son arrêt de dénaturation ni inexactement qualifié les faits de l'espèce ; <br/>
<br/>
              4. Considérant qu'il suit de là qu'en jugeant que, dans les circonstances de l'espèce, l'indemnité perçue par M. B...devait être regardée comme obéissant, pour le tout, au régime défini au 1 de l'article 80 duodecies du code général des impôts, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la ministre doit être rejeté ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement est rejeté. <br/>
Article 2 : L'Etat versera à M. et Mme B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
