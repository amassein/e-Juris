<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033194848</ID>
<ANCIEN_ID>JG_L_2016_10_000000401229</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/19/48/CETATEXT000033194848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 05/10/2016, 401229, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401229</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401229.20161005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2008 à 2010, ainsi que des pénalités correspondantes. Au soutien de cette demande, il a demandé à ce tribunal, par un mémoire distinct enregistré le 25 mai 2016, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions combinées du b du 1 de l'article 1728 du code général des impôts, dans sa rédaction issue de l'ordonnance n° 2005-1512 du 7 décembre 2005, et de l'article 1741 du même code, dans sa rédaction issue de l'ordonnance n° 2000-916 du 19 septembre 2000. <br/>
<br/>
              Par une ordonnance n° 1502492 du 1er juillet 2016, enregistrée le 6 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la présidente de la 7ème chambre du tribunal administratif de Montreuil a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre cette question au Conseil d'Etat.<br/>
<br/>
              Dans la question ainsi transmise et par un mémoire en réplique, enregistré le 9 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...soutient que ces dispositions, qui sont applicables au litige et n'ont pas déjà été déclarées conformes à la Constitution au regard du principe constitutionnel d'égalité devant la loi pénale, méconnaissent ce principe, dès lors que les faits qu'elles visent sont également susceptibles de donner lieu à l'application des sanctions pénales instituées par l'article 1741 du code général des impôts qui, cependant, les répriment très différemment, sans que cette différence ne soit justifiée par une différence de situation en rapport direct avec l'objet de la loi.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 61-1;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'ordonnance n° 2000-916 du 19 septembre 2000 ;<br/>
              - l'ordonnance n° 2005-1512 du 7 décembre 2005 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
              2. D'une part, aux termes du 1 de l'article 1728 du code général des impôts, dans sa rédaction issue de l'ordonnance du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités : " Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de :/ (...) b. 40 % lorsque la déclaration ou l'acte n'a pas été déposé dans les trente jours suivant la réception d'une mise en demeure, notifiée par pli recommandé, d'avoir à le produire dans ce délai ;/ (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 1741 du même code, dans sa rédaction issue de l'ordonnance du 19 septembre 2000 portant adaptation de la valeur en euros de certains montants exprimés en francs dans les textes législatifs : " Sans préjudice des dispositions particulières relatées dans la présente codification, quiconque s'est frauduleusement soustrait ou a tenté de se soustraire frauduleusement à l'établissement ou au paiement total ou partiel des impôts visés dans la présente codification, soit qu'il ait volontairement omis de faire sa déclaration dans les délais prescrits, (...), est passible, indépendamment des sanctions fiscales applicables, d'une amende de 37 500 &#128; et d'un emprisonnement de cinq ans./(...) ".<br/>
<br/>
              4. M. B...soutient que l'application combinée des dispositions citées aux points 2 et 3 ci-dessus méconnaît le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen, dès lors que les faits visés par le b du 1 de l'article 1728 du code général des impôts sont également susceptibles de donner lieu à l'application des sanctions pénales instituées par l'article 1741 du même code qui, cependant, les répriment très différemment, sans que cette différence de traitement soit justifiée par une différence de situation en rapport direct avec l'objet de la loi.<br/>
<br/>
              5. Toutefois, l'objet même de la répression administrative est d'instituer des sanctions d'une nature différente de celles prévues par la répression pénale. Par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux au regard du principe d'égalité devant la loi. <br/>
<br/>
              6. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Montreuil.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Montreuil.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'économie et des finances.<br/>
Copie en sera adressée pour information au Premier ministre, au Conseil constitutionnel et au tribunal administratif de Montreuil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
