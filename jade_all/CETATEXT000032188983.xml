<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188983</ID>
<ANCIEN_ID>JG_L_2016_03_000000384092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 09/03/2016, 384092, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE ; SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:384092.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er septembre 2014, 28 novembre 2014 et 6 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, l'association Vent de Colère ! Fédération nationale, l'association Fédération environnement durable, l'association Contribuables associés, ainsi que Mme M...I...-Q..., MM. F...C..., K...D..., L...J..., A...P..., O...B..., H...G...et E...N...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 17 juin 2014 de la ministre de l'écologie, du développement durable et de l'énergie et du ministre de l'économie, du redressement productif et du numérique fixant les conditions d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent implantées à terre ;<br/>
<br/>
              2°) à titre subsidiaire, de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à l'appréciation de la validité de la décision de la Commission européenne du 27 mars 2014 et de surseoir à statuer dans l'attente de son arrêt ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement n° 659/1999 du Conseil du 22 mars 1999 ;<br/>
              - le règlement n° 784/2004 de la Commission du 21 avril 2004 ;<br/>
              - la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - la loi organique n° 2001-692 du 1er août 2001 ;<br/>
              - le décret n° 2001-410 du 10 mai 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de l'Association Vent de Colère ! Fédération nationale, de l'association Fédération environnement durable, de l'association Contribuables associés, de M. C..., de M.D..., de M.J..., de M.P..., de M.B..., de Mme I...-Q..., de M. G...et de M. N...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 février 2016, présentée par l'association Vent de Colère ! Fédération nationale et les autres requérants ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 314-1 du code de l'énergie : " Sous réserve de la nécessité de préserver le fonctionnement des réseaux, Electricité de France et (...) les entreprises locales de distribution chargées de la fourniture sont tenues de conclure, lorsque les producteurs intéressés en font la demande, un contrat pour l'achat de l'électricité produite sur le territoire national par : / (...) 3° Les installations de production d'électricité utilisant l'énergie mécanique du vent qui sont implantées à terre (...) " ; qu'aux termes de l'article L. 314-4 du même code : " Les conditions dans lesquelles les ministres chargés de l'économie et de l'énergie arrêtent, après avis de la Commission de régulation de l'énergie, les conditions d'achat de l'électricité produite par les installations mentionnées à l'article L. 314-1, sont précisées par voie réglementaire. " ; qu'aux termes de l'article 8 du décret du 10 mai 2001 relatif aux conditions d'achat de l'électricité produite par des producteurs bénéficiant de l'obligation d'achat, pris pour l'application de ces dispositions et codifié depuis le 1er janvier 2016 à l'article R. 314-18 du code de l'énergie : " Des arrêtés des ministres chargés de l'économie et de l'énergie, pris après avis du Conseil supérieur de l'énergie et après avis de la Commission de régulation de l'énergie, fixent les conditions d'achat de l'électricité produite par les installations bénéficiant de l'obligation d'achat (...) " ;<br/>
<br/>
              2. Considérant que, par un arrêté du 17 juin 2014 pris pour l'application de ces dispositions, la ministre de l'écologie, du développement durable et de l'énergie et le ministre de l'économie, du redressement productif et du numérique ont fixé les conditions d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent implantées à terre ; que l'association Vent de colère ! Fédération nationale et dix autres requérants en demandent l'annulation pour excès de pouvoir ;<br/>
<br/>
              Sur les interventions du Syndicat des énergies renouvelables et de l'association France Energie Eolienne :<br/>
<br/>
              3. Considérant que le Syndicat des énergies renouvelables et l'association France Energie Eolienne ont intérêt au maintien de l'arrêté attaqué ; qu'ainsi, leurs interventions sont recevables ;<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aucune règle ni aucun principe n'exige que l'arrêté attaqué, qui constitue un acte réglementaire, soit motivé ; que, contrairement à ce que soutiennent les requérants, la directive du Parlement européen et du Conseil du 13 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité n'impose pas davantage une telle obligation ; que le moyen tiré de l'irrégularité de l'arrêté attaqué résultant de l'absence d'indication des motifs qui ont conduit ses auteurs à ne pas suivre l'avis de la Commission de régulation de l'énergie ne peut, par suite, qu'être écarté ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article 107 du traité sur le fonctionnement de l'Union européenne : " 1. Sauf dérogations prévues par les traités, sont incompatibles avec le marché intérieur, dans la mesure où elles affectent les échanges entre Etats membres, les aides accordées par les Etats ou au moyen de ressources d'Etat sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions. (...) " ; que selon le paragraphe 3 de l'article 108 du traité sur le fonctionnement de l'Union européenne : " La Commission est informée, en temps utile pour présenter ses observations, des projets tendant à instituer ou à modifier des aides. Si elle estime qu'un projet n'est pas compatible avec le marché intérieur, aux termes de l'article 107, elle ouvre sans délai la procédure prévue au paragraphe précédent. L'État membre intéressé ne peut mettre à exécution les mesures projetées, avant que cette procédure ait abouti à une décision finale. " ; qu'en application de ces stipulations, le régime d'aide d'Etat à la production d'électricité à partir d'installations éoliennes terrestres a été notifié le 11 octobre 2013 à la Commission européenne qui, par une décision du 27 mars 2014, l'a déclaré compatible avec le marché intérieur ; que les requérants soutiennent que l'arrêté attaqué devait à nouveau lui être notifié dès lors qu'il apporte des modifications à ce régime d'aide ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article 2 du règlement du Conseil du 22 mars 1999 portant modalités d'application de l'article 108 du traité sur le fonctionnement de l'Union européenne : " 1. Sauf indication contraire dans tout règlement pris en application de l'article 94 du traité ou de toute autre disposition pertinente de ce dernier, tout projet d'octroi d'une aide nouvelle est notifié en temps utile à la Commission par l'État membre concerné. (...) " ; qu'aux termes de l'article 1er de ce règlement : " Aux fins du présent règlement, on entend par : / (...) c) " aide nouvelle ": toute aide, c'est-à-dire tout régime d'aides ou toute aide individuelle, qui n'est pas une aide existante, y compris toute modification d'une aide existante ; (...) " ; que l'article 4 du règlement 784/2004 de la Commission du 21 avril 2004 concernant la mise en oeuvre de ce règlement précise en son paragraphe 1 qu'" Aux fins de l'article 1er, point c), du règlement (CE) no 659/1999, on entend par modification d'une aide existante tout changement autre que les modifications de caractère purement formel ou administratif qui ne sont pas de nature à influencer l'évaluation de la compatibilité de la mesure d'aide avec le marché commun. Toutefois, une augmentation du budget initial d'un régime d'aides existant n'excédant pas 20 % n'est pas considérée comme une modification de l'aide existante. " ; qu'il résulte de l'interprétation de ces dispositions donnée par le Tribunal de l'Union européenne, notamment dans un arrêt du 16 décembre 2010 Royaume des Pays-Bas et Nederlandse Omroep Stichting c. Commission européenne (aff. T-231/06 et T-237/06), que " ce n'est pas " toute aide existante modifiée " qui doit être considérée comme une aide nouvelle, mais c'est seulement la modification en tant que telle qui est susceptible d'être qualifiée d'aide nouvelle " et que " c'est donc seulement dans l'hypothèse où la modification affecte le régime initial dans sa substance même que ce régime se trouve transformé en un régime d'aides nouveau " ;<br/>
<br/>
              7. Considérant que les conditions tarifaires fixées par l'arrêté attaqué sont identiques à celles que prévoyait l'arrêté du 17 novembre 2008 fixant les conditions d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent, complété par un arrêté modificatif du 23 décembre 2008, sur la base desquels la Commission européenne a fondé son appréciation de la compatibilité avec le marché intérieur du régime d'aides qui lui a été notifié ; que, si les requérants font valoir que l'arrêté attaqué a remplacé les indices du coût du travail et des prix à la production entrant dans le calcul du coefficient K mentionné au point 4 par de nouveaux indices, cette substitution, qui résulte seulement de ce que les anciens indices mentionnés par l'arrêté du 17 novembre 2008 ne sont plus publiés par l'Institut national de la statistique et des études économiques, n'est pas de nature à influencer l'évaluation de la compatibilité de la mesure d'aide avec le marché intérieur ; que, dès lors, le moyen tiré de ce que les modifications apportées au régime d'aides imposaient la notification de l'arrêté attaqué à la Commission européenne ne peut être accueilli ;<br/>
<br/>
              8. Considérant, enfin, que contrairement à ce qui est soutenu, ni l'adoption par la Commission européenne, le 28 juin 2014, de nouvelles lignes directrices concernant les aides d'Etat à la protection de l'environnement et à l'énergie pour la période 2014-2020, ni l'adoption par la Commission de régulation de l'énergie en avril 2014 d'un rapport sur le coût et la rentabilité des énergies renouvelables, ni, enfin, l'adoption par les ministres chargés de l'énergie et des finances de l'arrêté du 18 septembre 2014 relatif à la compensation des charges de service public de l'électricité et pris en application de l'article 59 de la loi n° 2013-1279 du 29 décembre 2013, postérieures à l'arrêté attaqué, n'imposaient au Gouvernement de notifier l'arrêté attaqué à la Commission européenne ; que ce moyen ne peut par suite qu'être écarté ;<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              En ce qui concerne l'exigence d'accessibilité et d'intelligibilité de la norme :<br/>
<br/>
              9. Considérant que les tarifs d'achat qui figurent dans le tableau annexé à l'arrêté attaqué s'appliquent aux installations pour lesquelles la demande complète de contrat d'achat a été effectuée en 2007 ; qu'en application de l'article 3 de l'arrêté attaqué, les tarifs applicables aux installations dont la demande complète de contrat d'achat a été effectuée après cette date sont déterminés par l'application d'un coefficient K, dont il précise le calcul, aux tarifs figurant dans l'annexe à l'arrêté ; que, contrairement à ce que soutiennent les requérants, la circonstance que les tarifs applicables aux installations pour lesquelles la demande complète de contrat d'achat a été effectuée en 2014 ne puissent être déterminés que par l'application de ce coefficient ne méconnaît pas l'objectif de valeur constitutionnelle d'accessibilité et d'intelligibilité de la norme ;<br/>
<br/>
              En ce qui concerne le respect de la loi organique du 1er août 2001 relative aux lois de finances :<br/>
<br/>
              10. Considérant qu'à supposer même que la compensation octroyée aux opérateurs supportant les charges de service public résultant notamment de l'obligation d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent implantées à terre constitue une subvention pour charges de service public accordée par l'Etat, qu'il appartient, comme le fait valoir la requérante, à la loi de finances de prévoir chaque année avant son ordonnancement en application des articles 5, 6, 7 et 9 de cette loi organique, l'arrêté attaqué n'a ni pour objet ni pour effet de déterminer cette compensation ; que ses dispositions n'avaient donc pas à figurer dans une loi de finances ;<br/>
<br/>
              En ce qui concerne les tarifs fixés :<br/>
<br/>
              11. Considérant qu'aux termes de l'article L. 314-7 du code de l'énergie : " Les contrats conclus en application de la présente section par Electricité de France et les entreprises locales de distribution (...) prévoient des conditions d'achat prenant en compte les coûts d'investissement et d'exploitation évités par ces acheteurs, auxquels peut s'ajouter une prime prenant en compte la contribution de la production livrée ou des filières à la réalisation des objectifs définis au deuxième alinéa de l'article L. 121-1. Le niveau de cette prime ne peut conduire à ce que la rémunération des capitaux immobilisés dans les installations bénéficiant de ces conditions d'achat excède une rémunération normale des capitaux, compte tenu des risques inhérents à ces activités et de la garantie dont bénéficient ces installations d'écouler l'intégralité de leur production à un tarif déterminé. / (...) " ; <br/>
<br/>
              12. Considérant que les requérants soutiennent que les tarifs fixés par l'arrêté attaqué induisent une rémunération excessive des capitaux immobilisés dans les installations bénéficiant de ces conditions d'achat, en méconnaissance des dispositions citées au point 11 ; que ces tarifs ont été fixés, sous réserve de l'application du coefficient d'indexation K pour les années postérieures à 2007, à 8,2 centimes d'euro par kWh pour les dix premières années de production, puis sont dégressifs en fonction de la durée annuelle de fonctionnement de l'installation pour les cinq années suivantes ; qu'il ressort des pièces du dossier que ces tarifs induisent une rentabilité du projet après impôt qui varie, selon les différentes études disponibles et selon la durée annuelle de fonctionnement de l'installation, entre 3,9 % et 10 % pour la grande majorité des projets ; que, compte tenu, d'une part, des aléas qui s'attachent aux hypothèses de rentabilité des investissements en cause, calculée sur une durée allant de quinze à vingt ans et dépendant notamment de la durée annuelle de fonctionnement des installations utilisant l'énergie mécanique du vent, et d'autre part, de la diversité des caractéristiques du financement des projets, selon les choix opérés par les investisseurs, portant notamment sur l'arbitrage entre recours à l'emprunt et financement sur capitaux propres, il ne ressort pas des pièces du dossier qu'une erreur manifeste aurait été commise dans l'évaluation de la rémunération normale des capitaux immobilisés dans les installations utilisant l'énergie mécanique du vent ; que les dispositions précitées de l'article L. 314-7 du code de l'énergie n'ont, dès lors, pas été méconnues ;<br/>
<br/>
              En ce qui concerne la validité de la décision de la Commission européenne du 27 mars 2014 :<br/>
<br/>
              13. Considérant qu'aux termes de l'article 267 du traité sur le fonctionnement de l'Union européenne : " La Cour de justice de l'Union européenne est compétente pour statuer, à titre préjudiciel: / (...) b) sur la validité et l'interprétation des actes pris par les institutions, organes ou organismes de l'Union. / Lorsqu'une telle question est soulevée devant une juridiction d'un des États membres, cette juridiction peut, si elle estime qu'une décision sur ce point est nécessaire pour rendre son jugement, demander à la Cour de statuer sur cette question. / Lorsqu'une telle question est soulevée dans une affaire pendante devant une juridiction nationale dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne, cette juridiction est tenue de saisir la Cour. / (...) " ; que, d'une part, ainsi que l'a jugé la Cour de justice de l'Union européenne dans son arrêt du 22 octobre 1987 Foto-Frost contre Hauptzollamt Lübeck-Ost (aff. 314/85), si les juridictions nationales peuvent examiner la validité d'un acte des institutions de l'Union et, si elles n'estiment pas fondés les moyens d'invalidité que les parties invoquent devant elles, rejeter ces moyens en concluant que l'acte est pleinement valide, elles ne sont en revanche pas compétentes pour constater elles-mêmes l'invalidité de ces actes ; que, d'autre part, ainsi qu'elle l'a jugé dans son arrêt du 25 juillet 2002 Unión de Pequeños Agricultores (aff. C-50/00), le traité a établi un système complet de voies de recours et de procédures destiné à assurer le contrôle de la légalité des actes des institutions de l'Union, en le confiant au juge de l'Union et, dans ce système, des personnes physiques ou morales ne pouvant pas, en raison des conditions de recevabilité prévues par l'article 263, quatrième alinéa, du traité, attaquer directement des actes de droit de l'Union de portée générale, ont la possibilité, selon les cas, de faire valoir l'invalidité de tels actes soit, de manière incidente en vertu de l'article 277 du traité, devant le juge de l'Union, soit devant les juridictions nationales et d'amener celles-ci à interroger à cet égard la Cour de justice par la voie de questions préjudicielles ; <br/>
<br/>
              14. Considérant qu'il suit de là qu'il appartient en tout état de cause au juge administratif, saisi d'un moyen mettant en cause la validité d'un acte des institutions de l'Union de portée générale, d'écarter un tel moyen s'il ne présente pas de difficulté sérieuse ou lorsque la partie qui l'invoque avait sans aucun doute la possibilité d'introduire un recours en annulation, sur le fondement de l'article 263 du traité, contre l'acte prétendument invalide ; <br/>
<br/>
              15. Considérant, en premier lieu, que la décision litigieuse fait apparaître, d'une façon claire et non équivoque, le raisonnement de la Commission européenne et permet aux intéressés de connaître les justifications de la mesure prise et au juge d'exercer son contrôle ; que le moyen tiré de ce que cette décision serait insuffisamment motivée doit être écarté ;<br/>
<br/>
              16. Considérant, en deuxième lieu, qu'aux termes du paragraphe 3 de l'article 4 du règlement du Conseil du 22 mars 1999 portant modalités d'application de l'article 108 du traité sur le fonctionnement de l'Union européenne : " Si la Commission constate, après un examen préliminaire, que la mesure notifiée, pour autant qu'elle entre dans le champ de l'article 92, paragraphe 1, du traité, ne suscite pas de doutes quant à sa compatibilité avec le marché commun, elle décide que cette mesure est compatible avec le marché commun (...) " ; que les requérants soutiennent que la Commission ne pouvait déclarer l'aide en cause compatible avec le marché intérieur dès l'issue de l'examen préliminaire dès lors que cet examen présentait des difficultés sérieuses ; que toutefois, les délais d'adoption de la décision litigieuse n'ont pas excédé les exigences normales d'un premier examen ; que l'appréciation de la Commission sur l'aide en cause, qui n'est pas nouvelle dès lors que, ainsi qu'il ressort de sa décision, huit décisions avaient déjà été adoptées concernant les aides à la production d'électricité par les installations utilisant l'énergie mécanique du vent instituées par d'autres Etats membres, résulte directement de l'écart constaté entre les coûts de production de cette énergie et le prix de marché de l'électricité ainsi que de l'absence de surcompensation révélée par les analyses de taux de rentabilité dont elle fait état ; qu'au demeurant, l'association Vent de Colère ! Fédération nationale a pu faire valoir ses arguments dans sa plainte adressée à la Commission européenne ; que, dans ces conditions, la Commission a pu conclure à la compatibilité de l'aide en cause avec le marché intérieur dès l'issue de l'examen préliminaire sans méconnaître les dispositions précitées du règlement du 22 mars 1999 ;<br/>
<br/>
              17. Considérant, en troisième lieu, s'agissant de la méconnaissance alléguée des orientations que la Commission s'est imposée pour l'exercice de ses pouvoirs d'appréciation de la compatibilité des aides avec le marché intérieur, que c'est à bon droit que la Commission s'est fondée sur les lignes directrices concernant les aides d'Etat à la protection de l'environnement adoptées le 1er avril 2008, et non sur l'encadrement communautaire des aides d'Etat pour la protection de l'environnement qu'elle a adopté le 3 février 2001, dès lors que ces lignes directrices, publiées avant la décision litigieuse, précisent en leur paragraphe 204 que " La Commission appliquera les présentes lignes directrices à toutes les mesures d'aide notifiées sur lesquelles elle sera appelée à se prononcer après la publication des lignes directrices au Journal officiel de l'Union européenne " ; que, contrairement à ce que soutiennent les requérants, la circonstance que le montant de l'aide en cause ne soit pas fonction de la différence entre le coût de production de l'énergie produite à partir d'installations utilisant l'énergie mécanique du vent et le prix de marché de l'électricité n'est pas contraire à ces lignes directrices dès lors que leur paragraphe 109, qui permet l'octroi d'aides compensant cette différence, ajoute que " l'aide peut aussi couvrir la rentabilité normale de l'installation " ; que la Commission, dont la décision relève que la durée de l'aide en cause, accordée pour 15 ans, correspond à la durée d'amortissement normale selon le droit comptable français, n'a pas davantage méconnu l'exigence, également formulée au paragraphe 109 des lignes directrices, selon laquelle une telle aide ne doit être accordée que " jusqu'à ce que l'installation ait été complètement amortie selon les règles comptables ordinaires " ; qu'il ressort des points 45 à 52 de la décision litigieuse que la Commission n'a pas omis de prendre en compte les règles relatives au cumul d'aides fixées aux paragraphes 189 à 191 des lignes directrices ; qu'enfin, la circonstance que la Commission n'impose pas, dans sa décision, la publication sur internet des textes régissant l'aide, comme le prévoient les paragraphes 196 et 197 des lignes directrices, est sans incidence sur le respect de l'article 107 du traité ; qu'il en résulte que le moyen tiré de ce que la Commission ne se serait pas conformée aux lignes directrices qu'elle a adoptées doit être écarté ;<br/>
<br/>
              18. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est manifestement pas établi ;<br/>
<br/>
              19. Considérant qu'il résulte de tout ce qui précède, sans qu'il y ait lieu de se prononcer sur la recevabilité de la contestation de la validité de la décision de la Commission européenne du 27 mars 2014, que ce moyen doit être écarté ;<br/>
<br/>
              20. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêté qu'ils attaquent ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu' une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; que ces dispositions font également obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par le Syndicat des énergies renouvelables, qui n'a pas la qualité de partie à la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les interventions du Syndicat des énergies renouvelables et de l'association France Energie Eolienne sont admises.<br/>
Article 2 : La requête présentée par l'association Vent de Colère ! Fédération nationale, l'association Fédération environnement durable, l'association Contribuables associés, Mme M...I...-Q..., MM. F...C..., K...D..., L...J..., A...P..., O...B..., H...G...et E...N...ainsi que les conclusions présentées par le Syndicat des énergies renouvelables tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association Vent de Colère ! Fédération nationale, premier requérant dénommé, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, au ministre de l'économie, de l'industrie et du numérique, au Syndicat des énergies renouvelables, à l'association France Energie Eolienne ainsi qu'à la Commission de régulation de l'énergie. Les autres requérants seront informés de la présente décision par la SCP Tiffreau-Marlange-de la Burgade, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
