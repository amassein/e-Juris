<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044200811</ID>
<ANCIEN_ID>JG_L_2021_10_000000443426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/08/CETATEXT000044200811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 12/10/2021, 443426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443426.20211012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'association Amicale laïque de Mions (ALM) a demandé au tribunal administratif de Lyon d'annuler la délibération du 30 juin 2016 par laquelle le conseil municipal de Mions a abrogé la délibération du 11 février 2016 approuvant le versement d'une subvention de 28 000 euros à son profit et autorisant le maire à conclure avec elle la convention d'objectifs et de moyens définissant les modalités d'attribution de cette subvention, ainsi que la décision implicite par laquelle le maire a refusé de signer cette convention, de condamner en conséquence la commune à lui verser la somme de 28 000 euros et d'enjoindre à son maire de signer la convention d'objectifs et de moyens. Par un jugement n° 1605695 du 22 novembre 2018, le tribunal administratif de Lyon a partiellement fait droit à sa demande en annulant la délibération contestée et en condamnant la commune de Mions à lui verser la somme de 28 000 euros.<br/>
<br/>
              Par un arrêt n° 19LY00231 du 2 juillet 2020, la cour administrative d'appel de Lyon a, sur appel de la commune de Mions, annulé ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 août et 25 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association ALM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Mions ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Mions la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de l'association Amicale laïque de Mions et à Me Le Prado, avocat de la commune de Mions ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que l'association Amicale laïque de Mions (ALM) a demandé au tribunal administratif de Lyon, d'une part, d'annuler la délibération du 30 juin 2016 par laquelle le conseil municipal de Mions a abrogé la délibération du 11 février 2016 approuvant le versement d'une subvention d'un montant total de 28 000 euros à son profit et autorisant le maire à signer la convention d'objectifs et de moyens fixant les modalités d'attribution de cette subvention, ainsi que la décision implicite par laquelle le maire a refusé de signer la convention et, d'autre part, de condamner en conséquence la commune à lui verser la somme de 28 000 euros et d'enjoindre au maire de signer la convention d'objectifs et de moyens. Par un jugement du 22 novembre 2018, le tribunal administratif de Lyon a fait droit à la demande de l'association ALM en annulant la délibération contestée et en condamnant la commune de Mions à lui verser une subvention de 28 000 euros. L'association ALM se pourvoit en cassation contre l'arrêt du 2 juillet 2020 par lequel la cour administrative d'appel de Lyon a, sur appel de la commune de Mions, annulé le jugement du tribunal administratif de Lyon.<br/>
<br/>
              2. Aux termes de l'article L. 242-1 du code des relations entre le public et l'administration : " L'administration ne peut abroger ou retirer une décision créatrice de droits de sa propre initiative ou sur la demande d'un tiers que si elle est illégale et si l'abrogation ou le retrait intervient dans le délai de quatre mois suivant la prise de cette décision. ". Aux termes de l'article L. 242-2 du même code : " Par dérogation à l'article L. 242-1, l'administration peut, sans condition de délai : / (...) 2° Retirer une décision attribuant une subvention lorsque les conditions mises à son octroi n'ont pas été respectées. ". Aux termes de l'article 10 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration : " (...) L'autorité administrative ou l'organisme chargé de la gestion d'un service public industriel et commercial mentionné au premier alinéa de l'article 9-1 qui attribue une subvention doit, lorsque cette subvention dépasse un seuil défini par décret, conclure une convention avec l'organisme de droit privé qui en bénéficie, définissant l'objet, le montant, les modalités de versement et les conditions d'utilisation de la subvention attribuée. (...) ".<br/>
<br/>
              3. Il résulte des termes mêmes de l'arrêt attaqué que, pour annuler le jugement du tribunal administratif de Lyon qui faisait droit à la requête de l'association ALM tendant à l'annulation de la délibération du 30 juin 2016 abrogeant la délibération du 11 février 2016 lui accordant des subventions d'un montant de 28 000 euros, la cour administrative d'appel de Lyon a jugé que la délibération du 11 février 2016 n'avait pas créé de droits au profit de l'ALM au motif que la signature du maire de Mions ne figurait pas sur la convention d'objectifs et de moyens prévue à l'article 10 de la loi du 12 avril 2000. En se fondant sur cette seule circonstance, alors qu'il lui incombait de vérifier si l'ALM avait respecté tout ou partie des conditions d'octroi de la subvention, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              4. Il résulte ce qui précède que l'association ALM est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Mions une somme de 3 000 euros à verser à l'association ALM au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association ALM qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 2 juillet 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : La commune de Mions versera à l'association ALM une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Mions au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'association Amicale laïque de Mions et à la commune de Mions.<br/>
              Délibéré à l'issue de la séance du 9 septembre 2021 où siégeaient : M. Guillaume Goulard, président de chambre, présidant ; M. Christian Fournier, conseiller d'Etat et M. Martin Guesdon, auditeur-rapporteur. <br/>
<br/>
<br/>
<br/>
              Rendu le 12 octobre 2021.<br/>
                 Le Président : <br/>
                 Signé : M. Guillaume Goulard<br/>
 		Le rapporteur : <br/>
      Signé : M. Martin Guesdon<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
