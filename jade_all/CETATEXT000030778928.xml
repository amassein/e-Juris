<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030778928</ID>
<ANCIEN_ID>J0_L_2015_06_000001403200</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/77/89/CETATEXT000030778928.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 3ème chambre, 23/06/2015, 14VE03200, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-23</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>14VE03200</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme SIGNERIN-ICRE</PRESIDENT>
<AVOCATS>CABINET FIDAL</AVOCATS>
<RAPPORTEUR>M. Franck  LOCATELLI</RAPPORTEUR>
<COMMISSAIRE_GVT>M. COUDERT</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 novembre 2014, présentée pour la société ARKEA CREDIT BAIL, dont le siège est 255 rue de Saint-Malo CS 2117 à Rennes cedex (35011), par Mes Orhan et Rouxel, avocats ;  la société ARKEA CREDIT BAIL demande à la Cour : <br/>
<br/>
       1° d'annuler le jugement n° 1312071 en date du 22 septembre 2014 par lequel le Tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge des rappels de cotisation sur la valeur ajoutée des entreprises auxquels elle a été assujettie au titre des années 2010 et 2011, ainsi que des intérêts de retard, à concurrence des sommes respectives de <br/>
114 166 euros et 69 769 euros ;<br/>
<br/>
       2° de prononcer la décharge des impositions supplémentaires en litige ;<br/>
<br/>
       3° de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
       Elle soutient que :<br/>
<br/>
       - le jugement attaqué est irrégulier en ce qu'il ne répond pas aux moyens tirés de la rupture d'égalité devant l'impôt, pour le calcul de la valeur ajoutée, entre les crédits-bailleurs immobiliers selon qu'ils comptabilisent ou non les taxes foncières en diminution du montant des produits refacturés à ce titre, et, d'autre part, entre ces mêmes crédits-bailleurs et les autres acteurs économiques ;<br/>
       - compte tenu de ce que l'activité de crédit-bail constitue, pour le crédit-preneur, un moyen de financement de son acquisition immobilière, dont l'option lui est ouverte de façon irrévocable, les particularités de cette opération de financement justifient l'application de règles comptables adaptées ; <br/>
       - l'instruction fiscale 4 A-6-95 du 12 décembre 1995 relative au " nouveau régime " du crédit-bail immobilier confirme expressément que le crédit-bail immobilier constitue avant tout " un moyen de financement " pour le crédit-preneur ;<br/>
       - le règlement n° 91-01 du 11 janvier 1991 dispose expressément que la refacturation de la taxe foncière peut être présentée en déduction de la charge foncière elle-même et, ainsi, être comptabilisée en tant que produit net d'exploitation bancaire ; par suite, cette refacturation doit rester sans incidence sur le calcul de la valeur ajoutée du crédit-bailleur ; le jugement attaqué, en ce qu'il rejette la neutralisation du produit bancaire par sa charge, s'écarte de cette règle alors que les refacturations en cause ne créent pas un euro supplémentaire de valeur ajoutée ; l'imputation des charges refacturées à l'euro près sur les charges supportées est validée par le poste 12 et l'article 3.5 du règlement CRB n° 91 en tant que le produit de la refacturation a nécessairement la même nature que la charge, objet de la refacturation ; le jugement doit ainsi être annulé en ce qu'il dispose que la taxe foncière ne peut être déduite du calcul de la valeur ajoutée produite par l'entreprise, alors qu'une comptabilisation de cette taxe en minoration du produit de la refacturation aboutirait à prendre en compte cette même taxe en minoration de la valeur ajoutée ; dès lors que les taxes foncières sont liées à l'activité de crédit-bail, elles sont à prendre en compte dans la catégorie des " charges se rapportant à des biens immobiliers loués en crédit-bail " définie au poste 4 du règlement du 16 janvier 1991, seul poste susceptible de refléter la réalité de l'activité économique et du produit net bancaire ; <br/>
       - cette analyse, au demeurant validée par les commissaires aux comptes, est conforme au contenu du courrier du 24 janvier 2013 adressé par l'Autorité des normes comptables à l'Association française des sociétés financières ; en effet, le poste 15 intitulé " autres impôts et taxes " concerne les autres impôts non directement rattachables à l'activité de crédit-bail, tels que la taxe sur les salaires ou la taxe sur la valeur ajoutée non récupérables, qui ont la nature de charges générales d'exploitation ; <br/>
       - à supposer qu'il existe deux possibilités de comptabilisation de la taxe foncière, comme l'a fait valoir l'administration dans sa défense de première instance, à savoir le poste 15 et le poste 4, l'inscription en charges sur opérations de crédit-bail s'imposerait, conformément à la jurisprudence du Conseil d'Etat qui fait primer l'écriture comptable la plus respectueuse de la réalité économique et fiscale de l'opération ; intégrer les refacturations de taxe foncière dans le calcul de la valeur ajoutée sans, symétriquement, permettre leur déduction en tant que charge sur opérations de crédit-bail reviendrait en effet à faire primer une qualification comptable majorant artificiellement, c'est-à-dire sans justification économique, la valeur ajoutée ; en outre, une telle option aboutit à interdire la déduction de la charge refacturée chez le crédit-preneur ; cette absence de traitement symétrique entre les produits et les charges refacturées est contraire à la règle comptable applicable, ne se justifie pas d'un point de vue économique et contrevient de surcroît aux stipulations du contrat de bail ; la spécificité de l'activité de crédit-bail au titre de laquelle le crédit-bailleur n'est propriétaire de l'immeuble que pendant la durée de financement de l'opération l'autorise ainsi à déduire les dotations aux amortissements relatifs aux immeubles donnés en crédit-bail ; il doit en aller de même, et pour le même motif, de la taxe foncière refacturée qui doit être prise en compte au titre des charges contribuant au calcul de la valeur ajoutée ; <br/>
       - la logique défendue par l'administration fiscale, et validée par le tribunal, conduit enfin à une double rupture du principe constitutionnel d'égalité devant l'impôt, consacré par les articles 6 et 13 de la Déclaration des droits de l'homme et des citoyens ; en effet, cette rupture naît d'un traitement différent entre personnes placées dans une situation similaire, qu'il s'agisse des crédits-bailleurs entre eux selon qu'ils appliquent ou non les dispositions de l'article 3.5 du règlement CRB n°91-01, mais aussi des crédits-bailleurs vis-à-vis des autres opérateurs économiques ;<br/>
<br/>
       ...............................................................................................<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu le code monétaire et financier ;<br/>
<br/>
       Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 9 juin 2015 :<br/>
<br/>
       - le rapport de M. Locatelli, premier conseiller, <br/>
       - et les conclusions de M. Coudert, rapporteur public ;<br/>
<br/>
<br/>
       1. Considérant qu'à la suite d'une vérification de comptabilité des exercices clos en 2010 et 2011 de la société ARKEA CREDIT-BAIL, l'administration a remis en cause le calcul de la cotisation sur la valeur ajoutée des entreprises effectué par la société, au motif que celle-ci avait, à tort, déduit de la valeur ajoutée les cotisations de taxe foncière et de taxe annuelle sur les locaux à usage de bureaux dont elle s'était acquittée à raison des immeubles donnés en <br/>
crédit-bail à ses clients ; que la société ARKEA CREDIT-BAIL relève appel du jugement <br/>
du 22 septembre 2014 par lequel le Tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge, en droits et intérêts de retard, des rappels de cotisation sur la valeur ajoutée des entreprises auxquels elle a été assujettie au titre des années 2010 et 2011 à la suite de la remise en cause de ce calcul ;<br/>
<br/>
       Sur la régularité du jugement attaqué :<br/>
<br/>
       2. Considérant qu'il ressort des pièces du dossier de première instance que la société ARKEA CREDIT-BAIL a soutenu devant le tribunal administratif que les différences existant entre les modes de calcul de la valeur ajoutée des crédits-bailleurs selon que ces derniers comptabilisent ou non les taxes foncières en diminution du montant des produits qu'ils refacturent, d'une part, et les différences existant entre le mode de calcul de la valeur ajoutée des crédits-bailleurs et celui de la valeur ajoutée des autres acteurs économiques, d'autre part, méconnaissaient les principes d'égalité devant la loi et devant les charges publiques tels qu'ils sont garantis aux articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen ; qu'en ne répondant à ce moyen qu'en sa première branche, alors qu'en sa seconde branche, le moyen n'était pas inopérant, le Tribunal administratif de Montreuil a entaché son jugement d'irrégularité ; qu'il y a par suite lieu de l'annuler ;<br/>
       3. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par la société ARKEA CREDIT-BAIL devant le Tribunal administratif de Montreuil ; <br/>
<br/>
       Sur le bien-fondé des impositions :<br/>
<br/>
       4. Considérant qu'aux termes de l'article 1447 du code général des impôts, dans sa rédaction applicable à l'espèce : " I. La cotisation foncière des entreprises est due chaque année par les personnes physiques ou morales (...) qui exercent à titre habituel une activité professionnelle non salariée. (...) " ; qu'aux termes de l'article 1586 ter du même code : " I. - Les personnes physiques ou morales (...) qui exercent une activité dans les conditions fixées aux articles 1447 et 1447 bis et dont le chiffre d'affaires est supérieur à 152 500 &#128; sont soumises à la cotisation sur la valeur ajoutée des entreprises. / II. - 1. La cotisation sur la valeur ajoutée des entreprises est égale à une fraction de la valeur ajoutée produite par l'entreprise, telle que définie à l'article 1586 sexies. (...) " et qu'aux termes de l'article 1586 sexies de ce code : " (...) III.-Pour les établissements de crédit et, lorsqu'elles sont agréées par l'Autorité de contrôle prudentiel, les entreprises mentionnées à l'article L. 531-4 du code monétaire et financier : 1. Le chiffre d'affaires comprend l'ensemble des produits d'exploitation bancaires et des produits divers d'exploitation autres que les produits suivants : a) 95 % des dividendes sur titres de participation et parts dans les entreprises liées ; b) Plus-values de cession sur immobilisations figurant dans les produits divers d'exploitation autres que celles portant sur les autres titres détenus à long terme ; c) Reprises de provisions spéciales et de provisions sur immobilisations ; d) Quotes-parts de subventions d'investissement ; e) Quotes-parts de résultat sur opérations faites en commun. / 2. La valeur ajoutée est égale à la différence entre : a) D'une part, le chiffre d'affaires tel qu'il est défini au 1, majoré des reprises de provisions spéciales et des récupérations sur créances amorties lorsqu'elles se rapportent aux produits d'exploitation bancaire ; b) Et, d'autre part : - les charges d'exploitation bancaires autres que les dotations aux provisions sur immobilisations données en crédit-bail ou en location simple ; - les services extérieurs, à l'exception des loyers ou redevances afférents aux biens corporels pris en location ou en sous-location pour une durée de plus de six mois ou en crédit-bail ainsi que les redevances afférentes à ces biens lorsqu'elles résultent d'une convention de location-gérance ; toutefois, lorsque les biens pris en location par le redevable sont donnés en sous-location pour une durée de plus de six mois, les loyers sont retenus à concurrence du produit de cette sous-location ; - les charges diverses d'exploitation, à l'exception des moins-values de cession sur immobilisations autres que celles portant sur les autres titres détenus à long terme et des quotes-parts de résultat sur opérations faites en commun " et, à compter de l'année 2011 " - les pertes sur créances irrécouvrables lorsqu'elles se rapportent aux produits d'exploitation bancaire (...)  " ; que ces dispositions fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation sur la valeur ajoutée des entreprises et qu'il y a lieu, pour déterminer si une charge ou un produit se rattache à l'une de ces catégories, de se reporter aux normes comptables, dans leur rédaction en vigueur lors de l'année d'imposition concernée, sous réserve qu'elles ne soient pas incompatibles avec les règles applicables pour l'assiette de l'impôt ;<br/>
<br/>
       5. Considérant qu'il est constant que la société requérante a le statut d'établissement de crédit et se trouve ainsi soumise au règlement du comité de la réglementation bancaire <br/>
n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels des établissements de crédit ; <br/>
       6. Considérant, en premier lieu, que la société ARKEA CREDIT-BAIL soutient que l'activité de crédit-bail qu'elle exerce a une nature essentiellement financière qui ne peut la faire regarder comme un simple propriétaire de biens donnés à bail au crédit-preneur, pour le compte duquel elle règle la taxe foncière et la taxe sur les locaux à usage de bureaux, qu'elle lui refacture ensuite au franc le franc conformément à la règle comptable applicable et à laquelle la loi fiscale ne déroge pas ; qu'elle en déduit que ces taxes entrent dans le poste 4 " charges sur opérations de crédit-bail et assimilées " et doivent, par suite, venir en déduction de la valeur ajoutée à prendre en compte pour le calcul de la cotisation sur la valeur ajoutée des entreprises ; <br/>
<br/>
       7. Considérant, toutefois, que la circonstance que la taxe foncière ou la taxe annuelle sur les locaux à usage de bureaux soient acquittées à raison d'un immeuble donné en crédit-bail n'a pas pour effet de leur conférer le caractère d'une charge sur opération de crédit-bail relevant des postes 4 ou 9 de la réglementation comptable applicable, compte tenu, d'une part, que le fait générateur de ces taxes n'est pas lié à l'opération en cause et, d'autre part, que le crédit-bailleur, auquel la taxe foncière et la taxe annuelle sur les locaux à usage de bureaux incombent en sa qualité de propriétaire au 1er janvier de l'année d'imposition, en vertu, respectivement, des articles 1400 et 231 ter du code général des impôts, n'acquitte pas ces taxes pour le compte du crédit-preneur qui peut choisir ou non d'acquérir le bien à l'expiration du contrat ; que l'exigence du réalisme fiscal implique, au contraire, qu'eu égard à leur nature, ces taxes locales soient rattachées au poste 15 " charges générales d'exploitation " qui comprend les charges salariales et sociales, les impôts et taxes afférents aux frais de personnels ainsi que les autres frais administratifs " dont les impôts et taxes (...) ", sans que leur refacturation puisse venir en diminution de ce compte ; qu'elles ne sont donc pas au nombre des charges diverses d'exploitation dont il y aurait lieu de tenir compte pour le calcul de la valeur ajoutée ; qu'est sans incidence à cet égard le fait que l'article 3.5 du règlement du 16 janvier 1991 prévoit, ce qui n'est d'ailleurs qu'une faculté, que les charges refacturées et les produits rétrocédés au franc le franc puissent être déduits des produits et des charges auxquels ils se rapportent ; qu'en conséquence, la taxe foncière et la taxe sur les locaux à usage de bureaux ne peuvent être regardées comme des charges d'exploitation bancaire déductibles de la valeur ajoutée en application des dispositions précitées de l'article 1586 sexies du code général des impôts ;<br/>
       8. Considérant, en deuxième lieu, que la refacturation de ces impôts locaux, dont elle est le redevable légal, par la société ARKEA CREDIT-BAIL, aux crédit-preneurs, est créatrice de recettes qui, en tant qu'elles ont la nature de produits divers d'exploitation au sens du 1 <br/>
du III de l'article 1586 sexies, doivent être prises en compte dans le chiffre d'affaires servant de détermination à la valeur ajoutée des établissements de crédit ; que, par suite, le moyen tiré de que ces produits devraient être, par symétrie, exclus du calcul de la valeur ajoutée au seul motif que les charges foncières auxquelles ils se rapportent n'y sont pas incluses pour les raisons précédemment exposés, ne peut qu'être écarté ;<br/>
       9. Considérant, en troisième lieu, que la société ARKEA CREDIT-BAIL ne peut utilement se prévaloir de la lettre adressée, le 24 janvier 2013, par l'Autorité des normes comptables à l'Association des sociétés financières en ce que ce courrier, au surplus postérieur à l'année d'imposition en litige, ne constitue pas une doctrine émanant de l'administration fiscale qui lui serait opposable sur le fondement de l'article L. 80 A du livre des procédures fiscales, pas plus qu'il ne constitue une norme comptable à laquelle il y aurait lieu de se reporter pour l'application de la loi fiscale ; que, de même, est sans influence la circonstance que les commissaires aux comptes ont validé les options comptables que la requérante a exercées conformément au règlement du 16 janvier 1991 ; <br/>
<br/>
      10. Considérant, en quatrième lieu, que la société requérante ne peut rien tirer d'utile de l'instruction 4 A-6-95 du 12 décembre 1995 du service de la législation fiscale portant réforme du régime fiscal applicable aux opérations de crédit-bail immobilier en ce que cette instruction ne contient aucune méthode de calcul de la valeur ajoutée différente de celle dont il est présentement fait application sur le fondement de la loi fiscale ;<br/>
      11. Considérant, enfin, que si la requérante soutient que les modalités de calcul de la valeur ajoutée, qui sont fixées par la loi, seraient contraires aux principes constitutionnels d'égalité devant l'impôt et devant les charges publiques protégés aux articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, il n'appartient pas au juge administratif, hors saisine, par mémoire distinct, d'une question prioritaire de constitutionnalité, d'apprécier la conformité des lois à la Constitution ; qu'ainsi, le moyen soulevé est irrecevable ; <br/>
      12. Considérant qu'il résulte de ce qui précède que la société ARKEA CREDIT-BAIL n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Montreuil a rejeté sa demande ;<br/>
<br/>
      Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
      13. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que la société ARKEA CREDIT-BAIL demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
DECIDE :<br/>
Article 1er : Le jugement 1312071 du Tribunal administratif de Montreuil du 22 septembre 2014 est annulé.<br/>
Article 2 : La demande présentée par la société ARKEA CREDIT-BAIL devant le Tribunal administratif de Montreuil et le surplus des conclusions de sa requête devant la Cour sont rejetés. <br/>
''<br/>
''<br/>
''<br/>
''<br/>
7<br/>
2<br/>
N° 14VE03200<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-03-04-05 Contributions et taxes. Impositions locales ainsi que taxes assimilées et redevances. Taxe professionnelle. Questions relatives au plafonnement.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
