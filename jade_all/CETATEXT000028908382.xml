<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028908382</ID>
<ANCIEN_ID>JG_L_2014_05_000000368530</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/83/CETATEXT000028908382.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 07/05/2014, 368530, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368530</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368530.20140507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 mai et 9 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Philips France, dont le siège est 33 rue de Verdun, à Suresnes (92156), représentée par son président directeur général en exercice ; la société Philips France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NT00950 du 14 mars 2013 par lequel la cour administrative d'appel de Nantes a, d'une part, annulé le jugement n° 1100578 du 9 février 2012 par lequel le tribunal administratif d'Orléans a rejeté la demande de Mme B...A...tendant à l'annulation de la décision du 29 décembre 2010 par laquelle le ministre du travail, de l'emploi et de la santé a autorisé son licenciement, d'autre part, annulé cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A...; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Philips France et à la SCP Lyon-Caen, Thiriez, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 1233-4 du code du travail : " Le licenciement pour motif économique d'un salarié ne peut intervenir que lorsque tous les efforts de formation et d'adaptation ont été réalisés et que le reclassement de l'intéressé ne peut être opéré dans l'entreprise ou dans les entreprises du groupe auquel l'entreprise appartient. / Le reclassement du salarié s'effectue sur un emploi relevant de la même catégorie que celui qu'il occupe ou sur un emploi équivalent assorti d'une rémunération équivalente. A défaut, et sous réserve de l'accord exprès du salarié, le reclassement s'effectue sur un emploi d'une catégorie inférieure. / Les offres de reclassement proposées au salarié sont écrites et précises. " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, pour apprécier si l'employeur a satisfait à l'obligation de reclassement qui lui incombe avant de procéder à un licenciement économique, il appartient à l'autorité administrative, sous le contrôle du juge de l'excès de pouvoir, de vérifier que la société a cherché à reclasser le salarié sur un autre emploi équivalent ; qu'à défaut d'emploi équivalent disponible dans la société ou, le cas échéant, le groupe, il appartient à l'employeur, en application des dispositions législatives précitées, de rechercher à le reclasser sur un emploi d'une catégorie inférieure ; <br/>
<br/>
              3. Considérant que la cour administrative d'appel de Nantes, faisant droit à l'appel de Mme A...contre le jugement du tribunal administratif d'Orléans, a annulé la décision autorisant son licenciement au motif que les postes qui lui avaient été proposés ne constituaient pas des emplois équivalents à celui qu'elle occupait et que, par suite, la société Philips France n'avait pas satisfait à son obligation de reclassement ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui vient d'être dit qu'en se fondant sur la seule circonstance que les postes qui avaient été proposés à Mme A...ne concernaient pas des emplois équivalents à celui qu'elle occupait pour en déduire que la société Philips France n'avait pas satisfait à son obligation de reclassement, sans rechercher si, dans le contexte de la fermeture du site et de la pénurie, alléguée par la société requérante, d'emplois correspondant aux qualifications de l'intéressée, de telles offres n'étaient pas les seules disponibles et si les propositions de reclassement faites par la société n'étaient pas, dans ces conditions, suffisamment sérieuses, la cour administrative d'appel de Nantes a commis une erreur de droit ; que, dès lors, la société Philips France est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société requérante qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 14 mars 2013 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : Les conclusions présentées par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Philips France et à Mme B...A....<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
