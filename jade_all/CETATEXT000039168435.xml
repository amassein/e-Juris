<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168435</ID>
<ANCIEN_ID>JG_L_2019_10_000000419807</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 02/10/2019, 419807, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419807</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419807.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>1° Sous le n° 419807, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 avril et 5 juillet 2018 et le 28 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la chambre de commerce et d'industrie de région Paris - Ile-de-France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 février 2018  par laquelle le ministre de l'action et des comptes publics a refusé d'adopter des mesures réglementaires permettant de compenser la perte de rémunération résultant, pour les agents de la chambre de commerce et d'industrie de région Paris Ile-de-France relevant avant 2013 d'un régime spécial d'assurance-maladie, de la hausse du taux de la contribution sociale généralisée prévue par l'article 8 de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018 ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'action et des comptes publics, à titre principal, d'adopter les mesures réglementaires sollicitées dans un délai de trois mois à compter de la notification de la décision à intervenir ou, à titre subsidiaire, de réexaminer sa demande dans un délai de deux mois à compter de cette notification ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 421869, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 juin et 7 septembre 2018 et le 28 janvier 2019, la chambre de commerce et d'industrie de région Paris - Ile-de-France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet de son recours gracieux, formé le 28 février 2018, tendant au retrait de la décision du 12 février 2018  par laquelle le ministre de l'action et des comptes publics a refusé d'adopter des mesures réglementaires permettant de compenser la perte de rémunération résultant, pour les agents de la chambre de commerce et d'industrie de région Paris - Ile-de-France relevant avant 2013 d'un régime spécial d'assurance-maladie, de la hausse du taux de la contribution sociale généralisée prévue par l'article 8 de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018 ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'action et des comptes publics, à titre principal, d'adopter les mesures réglementaires sollicitées dans un délai de trois mois à compter de la notification de la décision à intervenir, ou, à titre subsidiaire, de réexaminer sa demande dans un délai de deux mois à compter de cette notification ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 52-1311 du 10 décembre 1952 ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 2017- 1836 du 30 décembre 2017 ;<br/>
              - la loi n° 2017-1837 du 30 décembre 2017 ;<br/>
              - le décret n° 2012-1486 du 27 décembre 2012 ;<br/>
              - le décret n° 2017-1889 du 30 décembre 2017 ;<br/>
              - le décret n° 2017-1890 du 30 décembre 2017 ;<br/>
              - le décret 2017-1891 du 30 décembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie de région Paris - Ile-de-France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 113 de la loi du 30 décembre 2017 de finances pour 2018 : " A compter du 1er janvier 2018, les agents publics civils et les militaires perçoivent une indemnité compensatrice tenant compte de la hausse du taux de la contribution sociale généralisée, prévue à l'article 8 de la loi n° 2017-1836 du 30 décembre 2017 de financement de la sécurité sociale pour 2018, de la suppression de la contribution exceptionnelle de solidarité et de la suppression de la cotisation salariale d'assurance maladie ainsi que de la baisse ou de la suppression de la contribution salariale d'assurance chômage, en application du même article 8 ". Le décret n° 2017-1889 du 30 décembre 2017 pris en application de l'article 113 de la loi n° 2017-1837 du 30 décembre 2017 de finances pour 2018 a institué une indemnité compensatrice de la hausse de la contribution sociale généralisée dans la fonction publique. Le même jour, afin notamment de tenir compte du coût, pour les employeurs publics autres que l'Etat, des mesures destinées à compenser l'effet pour les assurés de la hausse de la contribution sociale généralisée à compter du 1er janvier 2018, le décret n° 2017-1890 a réduit le taux de la cotisation d'assurance maladie applicable aux employeurs des fonctions publiques territoriale et hospitalière et le décret n° 2017-1891, par lequel a notamment été modifié le taux des cotisations d'assurance maladie du régime général, a abaissé le taux de cotisation patronale de plusieurs autres employeurs publics, en particulier d'employeurs dont les agents relèvent de régimes spéciaux. Toutefois, le VII de l'article 1er de ce décret rétablit au code de la sécurité sociale un article D. 711-1, dont le II précise que, pour les assurés de l'ancien régime spécial de la chambre de commerce et d'industrie de Paris, le taux applicable demeure celui qui résulte de l'article 1er du décret du 27 décembre 2012 relatif au taux de la cotisation d'assurance maladie, maternité, invalidité et décès due au titre des salariés qui relevaient antérieurement au 1er janvier 2013 du régime spécial d'assurance maladie de la chambre de commerce et d'industrie de Paris par la chambre de commerce et d'industrie de région Paris - Ile-de-France. Par un courrier du 9 janvier 2018, cette chambre a demandé au ministre de l'action et des comptes publics d'adopter des mesures réglementaires propres à lui permettre de compenser la perte de rémunération résultant, pour ceux de ses agents relevant avant 2013 d'un régime spécial d'assurance maladie, de la hausse du taux de la contribution sociale généralisée prévue par l'article 8 de la loi du 30 décembre 2017 de financement pour la sécurité sociale pour 2018, telles que celles, prévues par le décret n° 2017-1891 du 30 décembre 2017 pour d'autres employeurs publics, consistant en une baisse du taux de la cotisation d'assurance maladie. Le ministre de l'action et des comptes publics a rejeté cette demande par un courrier du 12 février 2018, puis a implicitement rejeté le recours gracieux formé le 28 février 2018 par la chambre de commerce et d'industrie de région Paris - Ile-de-France contre cette décision. <br/>
<br/>
              2. Par deux requêtes, qu'il y a lieu de joindre, la chambre de commerce et d'industrie de région Paris - Ile-de-France demande l'annulation pour excès de pouvoir de la décision du 12 février 2018 et du rejet implicite de son recours gracieux contre cette décision.<br/>
<br/>
              Sur la fin de non-recevoir soulevée par la ministre des solidarités et de la santé :<br/>
<br/>
              3. Par le courrier litigieux du 12 février 2018, le ministre de l'action et des comptes publics a rejeté la demande qui lui avait été présentée par la chambre de commerce et d'industrie de région Paris - Ile-de-France le 9 janvier 2018, par laquelle celle-ci sollicitait " une nouvelle disposition réglementaire " rendant " possible à la chambre de commerce et d'industrie de région Paris Ile-de-France de compenser l'impact négatif de la hausse de la contribution sociale généralisée pour les collaborateurs de l'ex-régime spécial d'assurance maladie ", en indiquant à cette chambre " qu'aucune mesure supplémentaire ne peut être envisagée afin de compenser le coût pour la chambre de commerce et d'industrie du maintien de la rémunération nette " de ces agents, qui relèvent désormais du régime général. Il a, ainsi, refusé l'adoption de mesures réglementaires. La ministre des solidarités et de la santé n'est, par suite, pas fondée à soutenir que le courrier du 12 février 2018 ne constituerait qu'une réponse à une demande d'information et ne ferait pas grief.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              4. Le refus de prendre un acte réglementaire présente un caractère réglementaire et, par suite, ne relève pas des dispositions de l'article L. 211-2 du code des relations entre le public et l'administration, qui sont relatives à la motivation des décisions individuelles défavorables. Dès lors, et en tout état de cause, le moyen tiré du défaut de motivation qui entacherait la décision de la ministre du 12 février 2018 ne peut qu'être écarté. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5. En premier lieu, il résulte des termes mêmes de l'article 113 de la loi du 30 décembre 2017 de finances pour 2018, cités au point 1, que, contrairement à ce que soutient la ministre des solidarités et de la santé, cet article est applicable à l'ensemble des agents publics civils et des militaires, dont font partie les agents administratifs de la chambre de commerce et d'industrie de région Paris - Ile-de-France, établissement public administratif. Si le deuxième alinéa de cet article dispose qu'" un décret, pris après avis du Conseil commun de la fonction publique et du Conseil supérieur de la fonction militaire, fixe les conditions d'application du présent article ", il résulte de ces dispositions que le législateur a seulement renvoyé à un décret la détermination des modalités d'attribution de l'indemnité compensatrice dont bénéficient les agents des fonctions publiques de l'Etat, territoriale et hospitalière ainsi que les militaires et les magistrats et qu'il n'a pas entendu déroger aux dispositions de l'article 1er de la loi du 10 décembre 1952 relative à l'établissement obligatoire d'un statut du personnel administratif des chambres d'agriculture, des chambres de commerce et des chambres de métiers, en vertu duquel le statut de ces agents est établi par une commission paritaire nommée par le ministre de tutelle. Au demeurant, la commission paritaire nationale des chambres de commerce et d'industrie a, par une décision du 29 janvier 2018, ajouté au statut du personnel administratif des chambres de commerce et d'industrie une disposition imposant à chaque chambre de commerce et d'industrie employeur d'accorder à ses agents publics recrutés avant le 1er janvier 2018 le bénéfice de l'indemnité compensatrice prévue par l'article 113 de la loi de finances pour 2018. Par suite, la chambre de commerce et d'industrie de région Paris - Ile-de-France n'est pas fondée à soutenir que le refus d'adopter les mesures réglementaires qu'elle sollicite serait contraire aux dispositions de l'article 113 de la loi de de finances pour 2018 ni, en tout état de cause, qu'il serait constitutif d'une rupture d'égalité illégale entre ses agents et les autres agents publics civils et militaires bénéficiant de l'indemnité compensatrice prévue par les mêmes dispositions.<br/>
<br/>
              6. En second lieu, d'une part, ni l'article 113 de la loi de finances pour 2018 ni aucun autre texte n'imposent la compensation par l'Etat du coût résultant pour les établissements publics, employeurs d'agents publics, du versement de l'indemnité compensatrice prévue par ce même article. Dès lors, la requérante n'est pas fondée à soutenir qu'en refusant d'adopter les mesures propres à compenser le coût résultant pour elle de la mise en oeuvre de l'indemnité compensatrice prévue par l'article 113 de la loi de finances pour 2018, le ministre de l'action et des comptes publics en aurait méconnu les dispositions.<br/>
<br/>
              7. D'autre part, si le décret n° 2017-1891 du 30 décembre 2017 abaisse le taux de cotisation d'assurance maladie à la charge de plusieurs établissements publics, au nombre desquels se trouvent notamment la Régie autonome des transports parisiens et la Société nationale des chemins de fer français, en vue de compenser, pour ces organismes, le coût du maintien de la rémunération nette des assurés qu'ils emploient, ces derniers, dont les agents relèvent de régimes spéciaux de sécurité sociale, ne peuvent être regardés, s'agissant de leur situation vis-à-vis des régimes d'assurance maladie, maternité, invalidité et décès, dans une situation identique à celle de la chambre de commerce et d'industrie de région Paris - Ile-de-France, dont les agents relèvent, depuis le 1er janvier 2013, du régime général pour ces risques. Par suite, la requérante ne saurait utilement se prévaloir d'une méconnaissance du principe d'égalité. <br/>
<br/>
              8. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre des solidarités et de la santé tirée de la tardiveté de la requête enregistrée sous le n° 421869, que la requérante n'est pas fondée à demander l'annulation des décisions qu'elle attaque. Ses conclusions à fin d'injonction ne peuvent, par suite, qu'être également rejetées.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les requêtes de la chambre de commerce et d'industrie de région Paris - Ile-de-France sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la chambre de commerce et d'industrie de région Paris Ile-de-France et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
