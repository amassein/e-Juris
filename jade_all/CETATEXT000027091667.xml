<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027091667</ID>
<ANCIEN_ID>JG_L_2013_02_000000364025</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/16/CETATEXT000027091667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 20/02/2013, 364025</TITRE>
<DATE_DEC>2013-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364025</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364025.20130220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 novembre et 6 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté d'agglomération de Saint-Quentin-en-Yvelines, dont le siège est 1 rue Eugène Hénaff ZA du Buisson à Trappes (78192) ; la communauté d'agglomération demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1205667-1205670-1205671 du 5 novembre 2012 par laquelle le juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, a rejeté ses demandes tendant, en premier lieu, à ce qu'il soit enjoint à la société France-Télécom de procéder à l'enfouissement de ses lignes aériennes de télécommunications en utilisant les infrastructures aménagées à cet effet dans l'ouvrage souterrain commun, premièrement, sur la section courant du n° 6 de l'impasse de la Marre Jarry jusqu'à l'angle qu'elle forme avec le chemin de la Petite Minière à Guyancourt (78280), et ce dans un délai de deux mois à compter de la notification de l'ordonnance à intervenir, deuxièmement, sur la section courant du n° 12 de l'allée des Roses jusqu'à l'angle qu'elle forme avec la route de Versailles, et tout au long de l'allée des Capucines, de l'angle qu'elle forme avec le chemin de la Chapelle (n° 18 de l'allée) jusqu'au n° 46 bis route de Versailles à Magny-les-Hameaux (78114), et ce dans un délai de deux mois à compter de la notification de l'ordonnance à intervenir, troisièmement, sur la section courant du n° 33 au n° 65 de l'avenue Paul Vaillant-Couturier, à Trappes (78190) et ce dans un délai de deux mois à compter de la notification de l'ordonnance à intervenir, en deuxième lieu, de condamner la société France-Télécom à lui verser, pour chacune des demandes, une astreinte de 1 000 euros par jour de retard à compter de l'expiration dudit délai de deux mois à compter de la notification des ordonnances à intervenir, et ce pendant un délai d'un mois, en dernier lieu, de l'autoriser, pour chacune des demandes, en cas de carence de la société France-Télécom au terme d'un délai de trois mois à compter de la notification des ordonnances à intervenir, à faire réaliser par un prestataire professionnel de son choix disposant de la compétence technique nécessaire les enfouissements en question, aux frais et risques de la société France-Télécom ; <br/>
<br/>
              2°) de mettre à la charge de la société France-Télécom le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 février 2013, présentée pour la société France-Télécom ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de la communauté d'agglomération de Saint-Quentin-en-Yvelines, et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société France-Télécom,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat de la communauté d'agglomération de Saint-Quentin-en-Yvelines, et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société France-Télécom ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative. " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la communauté d'agglomération de Saint-Quentin-en-Yvelines a décidé de procéder à l'enfouissement de lignes électriques aériennes sur le territoire des communes de <br/>
Magny-les-Hameaux, Guyancourt et Trappes ; que, sur le fondement des dispositions de l'article L. 2224-35 du code général des collectivités territoriales, elle a demandé à la société <br/>
France-Télécom de procéder également à l'enfouissement des lignes de communications circulant sur des appuis partagés avec les lignes électriques ; qu'en raison de désaccords entre les parties, d'abord sur le régime de propriété des installations d'accueil des lignes de communications électroniques puis sur la redevance d'utilisation susceptible d'être demandée à l'opérateur par la collectivité publique, aucune convention n'a pu être signée pour la réalisation de chacune des opérations programmées ; que la communauté d'agglomération de <br/>
Saint-Quentin-en-Yvelines, qui a procédé à l'enfouissement des lignes électriques dans les trois zones concernées, se pourvoit en cassation contre l'ordonnance par laquelle le juge des référés du tribunal administratif de Versailles, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, a refusé de faire droit à sa demande tendant à ce qu'il soit enjoint à la société France-Télécom de procéder à l'enfouissement de ses lignes aériennes ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 2224-35 du code général des collectivités territoriales dans sa rédaction issue de la loi du 21 juin 2004 pour la confiance dans l'économie numérique, modifiée par la loi du 9 juillet 2004 relative aux communications électroniques et aux services de communication audiovisuelle et par la loi du 7 décembre 2006 relative au secteur de l'énergie : " Tout opérateur de communications électroniques autorisé par une collectivité territoriale, par un établissement public de coopération compétent pour la distribution publique d'électricité, ou par un gestionnaire de réseau public de distribution d'électricité à installer un ouvrage aérien non radioélectrique sur un support de ligne aérienne d'un réseau public de distribution d'électricité procède, en cas de remplacement de cette ligne aérienne par une ligne souterraine à l'initiative de la collectivité ou de l'établissement précité, au remplacement de sa ligne aérienne en utilisant la partie aménagée à cet effet dans l'ouvrage souterrain construit en remplacement de l'ouvrage aérien commun. Les infrastructures communes de génie civil créées par la collectivité territoriale ou l'établissement public de coopération lui appartiennent. / L'opérateur de communications électroniques prend à sa charge les coûts de dépose, de réinstallation en souterrain et de remplacement des équipements de communications électroniques incluant  les câbles, les fourreaux et les chambres de tirage, y compris les coûts d'études et d'ingénierie correspondants. Il prend à sa charge l'entretien de ses équipements. Un arrêté des ministres chargés des communications électroniques et de l'énergie détermine la proportion des coûts de terrassement pris en charge par l'opérateur de communications électroniques. / Une convention conclue entre la collectivité ou l'établissement public de coopération et l'opérateur de communications électroniques fixe la participation financière de celui-ci, sur la base des principes énoncés ci-dessus, ainsi que le montant de la redevance qu'il doit éventuellement verser au titre de l'occupation du domaine public. " ; <br/>
<br/>
              4. Considérant que ces dispositions ont été modifiées et complétées par la loi du 17 décembre 2009 relative à la lutte contre la fracture numérique ; qu'en particulier, le dernier alinéa a été complété pour prévoir désormais que la convention passée entre la collectivité publique et l'opérateur de télécommunications " fixe les modalités de réalisation et, le cas échéant, d'occupation de l'ouvrage partagé, notamment les responsabilités et la participation financière de chaque partie (...) et indique le montant de la redevance qu'il doit éventuellement verser au titre de l'occupation du domaine public " ; qu'en outre, un nouvel alinéa a été inséré pour prévoir que les infrastructures d'accueil d'équipement de communications électroniques peuvent faire l'objet d'une prise en charge financière partielle ou complète par la collectivité ou par l'établissement public de coopération, qui dispose alors d'un droit d'usage ou de la propriété de ces infrastructures dans des conditions fixées par la convention prévue au dernier alinéa ; <br/>
<br/>
              5. Considérant que les dispositions issues de la loi du 17 décembre 2009 n'ont pas modifié l'étendue de l'obligation pour l'opérateur de déposer et d'enfouir son réseau câblé aérien en cas de remplacement du réseau public aérien de distribution d'électricité ; qu'en particulier, elles n'ont pas subordonné l'exécution de cette obligation à la conclusion préalable de l'engagement contractuel qu'elles prévoient entre la collectivité publique et l'opérateur ;<br/>
<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en énonçant que l'obligation prévue par l'article L. 2224-35 ne peut être mise en oeuvre qu'après conclusion d'une convention entre la collectivité ou l'établissement public de coopération et l'opérateur de communications électroniques, le juge des référés a commis une erreur de droit ; que, par suite, son ordonnance doit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulée ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la communauté d'agglomération de <br/>
Saint-Quentin-en-Yvelines, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant, en premier lieu, qu'il résulte de l'instruction que le refus, par France-Télécom, de procéder à la dépose et à l'enfouissement de ses câbles fait obstacle à l'achèvement des travaux d'enfouissement des lignes électriques, engagés dès la fin de l'année 2009 sur l'une des sections concernées et à la fin de l'année 2011 sur les deux autres sections, ainsi qu'au démontage des points d'appui communs qui était l'objectif de l'opération décidée par la communauté d'agglomération ; que cette situation est source de gêne pour les usagers et empêche la collectivité publique de réceptionner et de solder certains marchés en cours ; que la mesure demandée présente en conséquence un caractère d'urgence et d'utilité ; <br/>
<br/>
              9. Considérant, en second lieu, qu'ainsi qu'il a été dit, les dispositions de l'article L. 2224-35 du code général des collectivités territoriales mettent à la charge des opérateurs de communications électroniques l'obligation de déposer et d'enfouir leur réseau câblé aérien en cas de remplacement par la personne publique du réseau public aérien de distribution d'électricité par une ligne souterraine sans que l'absence d'engagement contractuel puisse exonérer l'opérateur de son obligation ; que, la société France-Télécom faisant seulement valoir que l'opération d'enfouissement ne peut intervenir en l'absence de signature préalable d'une convention, la mesure demandée au juge des référés par la communauté d'agglomération de Saint-Quentin-en-Yvelines ne se heurte pas à une contestation sérieuse ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède qu'il y a lieu d'ordonner à la société France-Télécom de procéder à l'enfouissement de ses lignes câblées aériennes sur les sections des communes de Magny-les-Hameaux, de Guyancourt et de Trappes dans un délai de trois mois à compter de la notification de la présente décision ; qu'il y a lieu d'assortir cette injonction d'une astreinte de 1 000 euros par jour de retard ; qu'il n'y a pas lieu, en revanche, de faire droit aux conclusions de la communauté d'agglomération tendant à l'autoriser à faire réaliser les travaux aux frais et risques de la société France-Télécom ; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la communauté d'agglomération de Saint-Quentin-en-Yvelines, qui n'est pas la partie perdante dans la présente instance, la somme demandée par la société France-Télécom au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de la société France-Télécom, le versement d'une somme de 4 500 euros au titre des mêmes dispositions pour la procédure suivie devant le Conseil d'Etat et le juge des référés du tribunal administratif de Versailles ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Versailles du 5 novembre 2012 est annulée. <br/>
Article 2 : Il est enjoint à la société France-Télécom de procéder, dans un délai de trois mois à compter de la présente décision, à l'enfouissement de ses lignes aériennes de télécommunications en utilisant les infrastructures aménagées à cet effet dans l'ouvrage souterrain construit par la communauté d'agglomération de Saint-Quentin-en-Yvelines, premièrement, sur la section courant du n° 6 de l'impasse de la Marre Jarry jusqu'à l'angle qu'elle forme avec le chemin de la Petite Minière à Guyancourt (78280), deuxièmement, sur la section courant du n° 12 de l'allée des Roses jusqu'à l'angle qu'elle forme avec la route de Versailles, et tout au long de l'allée des Capucines, de l'angle qu'elle forme avec le chemin de la Chapelle <br/>
(n° 18 de l'allée) jusqu'au n° 46 bis route de Versailles à Magny-les-Hameaux (78114), troisièmement, sur la section courant du n° 33 au n° 65 de l'avenue Paul Vaillant-Couturier, à Trappes (78190).<br/>
Article 3 : A défaut pour la société France-Télécom de justifier de l'exécution de l'article 2 dans les délais prescrits, est prononcée à son encontre une astreinte de 1 000 euros par jour jusqu'à la date à laquelle la présente décision aura reçu exécution. <br/>
Article 4 : La société France-Télécom portera à la connaissance du secrétariat du contentieux du Conseil d'Etat les mesures prises pour assurer l'exécution de l'article 2 de la présente décision.<br/>
Article 5 : La société France-Télécom versera à la communauté d'agglomération de <br/>
Saint-Quentin-en-Yvelines une somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Le surplus des conclusions de la demande de la communauté d'agglomération de Saint-Quentin-en-Yvelines est rejeté.<br/>
Article 7 : La présente décision sera notifiée à la communauté d'agglomération de <br/>
Saint-Quentin-en-Yvelines et à la société France-Télécom.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-02-004 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. - OBLIGATION POUR L'OPÉRATEUR DE DÉPOSER ET D'ENFOUIR SON RÉSEAU CÂBLÉ AÉRIEN EN CAS DE REMPLACEMENT DU RÉSEAU PUBLIC AÉRIEN DE DISTRIBUTION D'ÉLECTRICITÉ (ART. L. 2224-35 DU CGCT) [RJ1] - INCIDENCE DES MODIFICATIONS APPORTÉES PAR LA LOI DU 17 DÉCEMBRE 2009 SUR CETTE OBLIGATION - ABSENCE - CONSÉQUENCE - EXONÉRATION DE L'OBLIGATION D'ENFOUISSEMENT PESANT SUR L'OPÉRATEUR EN CAS D'ABSENCE DE L'ENGAGEMENT CONTRACTUEL PRÉALABLE ENTRE LES PARTIES PRÉVU PAR CE TEXTE - ABSENCE.
</SCT>
<ANA ID="9A"> 51-02-004 Les modifications apportées par la loi n° 2009-1572 du 17 décembre 2009 aux dispositions de l'article L. 2224-35 du code général des collectivités territoriales (CGCT) n'ont pas remis en cause l'obligation pour l'opérateur de déposer et d'enfouir son réseau câblé aérien en cas de remplacement du réseau public aérien de distribution d'électricité. En particulier, l'absence de l'engagement contractuel préalable entre les deux parties prévu depuis la loi du 17 décembre 2009 ne peut l'exonérer de cette obligation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 juillet 2008, Société Rhône Vision Cable, n° 309878, T. p. 863.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
