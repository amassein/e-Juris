<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034076440</ID>
<ANCIEN_ID>JG_L_2017_02_000000395876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/64/CETATEXT000034076440.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 22/02/2017, 395876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:395876.20170222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir l'arrêté du 25 novembre 2014 par lequel le préfet de la Vienne a rejeté sa demande de renouvellement de son titre de séjour en qualité d'étranger malade, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination. Par un jugement n° 1403484 du 26 mars 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15BX01442 du 10 novembre 2015, la cour administrative d'appel de Bordeaux a annulé le jugement du tribunal administratif de Poitiers et a enjoint au préfet de la Vienne de délivrer à M. A...une carte de séjour portant la mention "vie privée et familiale" dans un délai de deux mois.<br/>
<br/>
              Par un pourvoi, enregistré le 5 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°)  réglant l'affaire au fond, de rejeter l'appel de M.A....<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -le code de la santé publique ;<br/>
              -la loi n° 91-647 du 10 juillet 1991 ;<br/>
              -le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. A..., de nationalité géorgienne, né le 14 janvier 1992, est entré régulièrement en France le 20 mai 2011 ; qu'il a bénéficié à compter du 25 octobre 2012 au titre de la prise en charge médicale nécessitée par son état de santé d'un titre de séjour régulièrement renouvelé jusqu'au 22 avril 2014 ; qu'il a, le 13 mai 2014, sollicité le renouvellement de son titre de séjour au même titre ; que, malgré l'avis favorable sur sa demande émis le 11 juin 2014 par le médecin désigné de l'Agence régionale de santé, le préfet de la Vienne a, par un arrêté du 25 novembre 2014, rejeté sa demande d'admission au séjour et lui a enjoint de quitter le territoire dans un délai de trente jours à destination de son pays d'origine ; que le ministre de l'intérieur se pourvoit en cassation contre l'arrêt du 10 novembre 2015 par lequel la cour administrative d'appel de Bordeaux a annulé cet arrêté ainsi que le jugement du tribunal administratif de Poitiers rejetant la demande de M. A...;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile dans sa version alors en vigueur : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit :(...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays dont il est originaire, sauf circonstance humanitaire exceptionnelle appréciée par l'autorité administrative après avis du directeur général de l'agence régionale de santé, sans que la condition prévue à l'article L. 311-7 soit exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin de l'agence régionale de santé de la région de résidence de l'intéressé, désigné par le directeur général de l'agence, ou, à Paris, du médecin, chef du service médical de la préfecture de police. Le médecin de l'agence régionale de santé ou, à Paris, le chef du service médical de la préfecture de police peut convoquer le demandeur pour une consultation médicale devant une commission médicale régionale dont la composition est fixée par décret en Conseil d'Etat (...) " ; qu'il résulte de ces dispositions qu'il appartient à l'autorité administrative, lorsqu'elle envisage d'éloigner un étranger du territoire national, de vérifier que cette décision ne peut avoir de conséquences exceptionnelles sur l'état de santé de l'intéressé et, en particulier, d'apprécier, sous le contrôle du juge de l'excès de pouvoir, la nature et la gravité des risques qu'entraînerait une éventuelle interruption des traitements suivis en France ; que, lorsque cette interruption risque d'avoir des conséquences exceptionnelles sur la santé de l'intéressé, il appartient alors à cette autorité de démontrer qu'il existe des possibilités de traitement approprié de l'affection en cause dans le pays de renvoi ;<br/>
<br/>
              3. Considérant que la cour a relevé que M. A...a bénéficié à partir de janvier 2013 d'un traitement de substitution aux opiacés par la prescription de Subutex ; que, pour annuler l'arrêté litigieux, la cour a notamment relevé que cette spécialité n'était pas disponible en Géorgie et qu'il était porté sur les ordonnances délivrées à l'intéressé la mention " non substituable " ; qu'il résulte des dispositions pertinentes du code de la santé publique que cette indication a pour seul objet d'interdire au pharmacien, pour des raisons médicales, de substituer à la spécialité prescrite une autre spécialité du même groupe générique ; qu'en s'estimant liée par cette mention, pour juger que M. A...devait être regardé comme ne pouvant bénéficier dans son pays d'origine de soins appropriés à son état, alors qu'elle relevait par ailleurs que le préfet avait produit divers documents attestant l'existence en Géorgie de traitements adaptés de substitution aux opiacés, la cour a entaché son arrêt d'une erreur de droit ; que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre de l'intérieur est, par suite, fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 novembre 2015 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
