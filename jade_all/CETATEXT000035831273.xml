<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035831273</ID>
<ANCIEN_ID>JG_L_2017_10_000000398697</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/83/12/CETATEXT000035831273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 18/10/2017, 398697, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398697</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398697.20171018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 11 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des cadres de la sécurité intérieure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction du 17 février 2016 du directeur général de la police nationale, en tant qu'elle dispose que, pour les membres des corps de conception et de direction et de commandement de la police nationale, la restitution horaire d'un report de repos est égale à 100 % du temps effectué ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - le décret n° 82-452 du 28 mai 1982 ; <br/>
<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
<br/>
              - le décret n° 95-654 du 9 mai 1995 ;<br/>
<br/>
              - l'arrêté du 7 mai 1974 portant règlement intérieur d'emploi des gradés et gardiens de la paix ;<br/>
<br/>
              - l'arrêté du 6 juin 2006 portant règlement général d'emploi de la police nationale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 21 du décret du 9 mai 1995 fixant les dispositions communes applicables aux fonctionnaires actifs des services de la police nationale : " Quelles que soient les spécificités de leur cycle de travail, les fonctionnaires actifs de la police nationale ont droit à une journée de repos légal hebdomadaire. Toutefois, ce repos peut exceptionnellement être reporté si l'intérêt du service l'exige. Il ne peut être procédé à plus de deux reports consécutifs que sur décision ministérielle " ; qu'aux termes de l'article 113-21 de l'arrêté du 6 juin 2006 portant règlement général d'emploi de la police nationale : " Les fonctionnaires actifs de la police nationale peuvent prétendre à deux jours de repos hebdomadaires consécutifs, incluant la journée de repos légal hebdomadaire, qui est de droit sous réserve des contraintes liées au respect des cycles de travail et dans les limites qui résultent des nécessités du service. / Ce repos peut être exceptionnellement reporté si l'intérêt du service l'exige (...) " ; que, par une instruction du 17 février 2016, le directeur général de la police nationale a précisé les modalités de recours aux reports de repos et de leur compensation ; que le Syndicat des cadres de la sécurité intérieure demande l'annulation pour excès de pouvoir de cette instruction, en tant qu'elle prévoit que : " Pour les agents du corps de conception et de direction, pour ceux du corps de commandement (...) la restitution horaire opérée lors d'un report de repos est égale à 100 % du temps effectué (...) " ; <br/>
<br/>
              2. Considérant que ces dispositions à caractère général, rédigées dans des termes impératifs, sont susceptibles de faire l'objet d'un recours pour excès de pouvoir ; qu'elles sont divisibles des autres dispositions du point 2.2.1. de l'instruction ; que, par suite, les fins de non-recevoir soulevées par le ministre de l'intérieur doivent être écartées ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 15 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Les comités techniques connaissent des questions relatives à l'organisation et au fonctionnement des services, des questions relatives aux effectifs, aux emplois et aux compétences, des projets de statuts particuliers ainsi que des questions prévues par un décret en Conseil d'Etat. (...) " ; qu'aux termes de l'article 31 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat, qui a abrogé le décret n° 82-452 du 28 mai 1982 ayant le même objet : " Les comités techniques sont consultés, dans les conditions et les limites précisées pour chaque catégorie de comité par les articles 35 et 36 sur les questions et projets de textes relatifs : / (...) 1° A l'organisation et au fonctionnement des administrations, établissements ou services ; (...) " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que l'instruction attaquée prévoit des modalités de compensation des reports de repos différentes selon les corps auxquels appartiennent les fonctionnaires de police concernés ; que l'administration a déduit des dispositions de l'arrêté du 7 mai 1974 portant règlement intérieur d'emploi des gradés et gardiens de la paix de la police nationale que les agents du corps d'application et d'encadrement bénéficiaient en cas de report de repos, quelle que soit la durée effective du service accompli à l'occasion du report jusqu'à concurrence de la durée d'une vacation, d'une restitution égale au double d'une vacation en cas de report du repos légal et à une vacation en cas de report du repos compensateur ; que, s'agissant en revanche des agents des corps de conception et de direction et de commandement, l'administration a déduit des dispositions du décret du 9 mai 1995 et de l'arrêté du 6 juin 2006 visés ci-dessus, qui se bornent à prévoir la possibilité d'un report de repos des personnels de police si l'intérêt du service l'exige, que ces agents ne bénéficiaient que de la restitution du temps effectivement travaillé à l'occasion de ce report ; <br/>
<br/>
              5. Considérant qu'en énonçant ainsi des règles applicables au report de repos des agents des corps de conception et de direction et de commandement, alors que ces règles ne résultaient pas directement des dispositions du décret du 9 mai 1995 et de l'arrêté du 6 juin 2006, l'instruction litigieuse a édicté une mesure qui, compte tenu de l'importance qu'elle revêt, soulevait une question relative à l'organisation et au fonctionnement des services au sens et pour l'application de l'article 31 du décret du 15 février 2011 précité et qui, par suite, devait faire l'objet d'un examen préalable par le comité technique compétent ; que l'omission de cette consultation, qui a privé les fonctionnaires concernés d'une garantie, a constitué une irrégularité de nature à entacher d'illégalité l'instruction attaquée ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, le syndicat requérant est fondé à demander l'annulation des dispositions attaquées ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Syndicat des cadres de la sécurité intérieure, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'instruction du directeur général de la police nationale du 17 février 2016 est annulée en tant qu'elle prévoit que, pour les agents des corps de conception et de direction et de commandement de la police nationale, la restitution horaire opérée lors d'un report de repos est égale à 100 % du temps effectué.<br/>
<br/>
Article 2 : L'Etat versera au Syndicat des cadres de la sécurité intérieure une somme de 3  000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat des cadres de la sécurité intérieure et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
