<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036626534</ID>
<ANCIEN_ID>JG_L_2018_02_000000399161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/62/65/CETATEXT000036626534.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 20/02/2018, 399161, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:399161.20180220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              M. D...B...a demandé au tribunal administratif de Montreuil de prononcer la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période du 1er  janvier 2006 au 31 décembre 2008 ainsi que des pénalités correspondantes. Par un jugement n° 1304196 du 30 septembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14VE03298 du 1er  mars 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 avril et 19 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la SCP Jacques Moyrand, en qualité de liquidateur judiciaire de M.B..., demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
              - la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. D...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui exerçait à titre individuel une activité de " liaison qualité " au profit de fabricants espagnols de pièces pour automobiles, a fait l'objet d'une vérification de comptabilité portant notamment sur la taxe sur la valeur ajoutée pour la période du 1er janvier 2006 au 31 décembre 2008. A la suite de ce contrôle, l'administration fiscale a estimé que l'activité de M. B...réalisée en France sur des véhicules assemblés en France et destinés au marché français était imposable à la taxe sur la valeur ajoutée en France sur le fondement du 4° bis de l'article 259 A du code général des impôts. <br/>
<br/>
              2. Par un jugement du 30 septembre 2014, le tribunal administratif de Montreuil a rejeté la demande de M. B...tendant à la décharge des rappels de taxe litigieux. La SCP Jacques Moyrand, en qualité de liquidateur judiciaire de M.B..., demande l'annulation de l'arrêt du 1er mars 2016 par lequel la cour administrative d'appel de Versailles a rejeté l'appel que M. B...avait formé contre ce jugement. <br/>
<br/>
              3. Aux termes de l'article 259 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige : " Le lieu des prestations de services est réputé se situer en France lorsque le prestataire a en France le siège de son activité ou un établissement stable à partir duquel le service est rendu ou, à défaut, son domicile ou sa résidence habituelle. " Aux termes de l'article 259 A du même code, dans sa rédaction applicable à la période d'imposition en litige : " Par dérogation aux dispositions de l'article 259, le lieu des prestations suivantes est réputé se situer en France :(...) 4° bis Travaux et expertises portant sur des biens meubles corporels : a. lorsque ces prestations sont matériellement exécutées en France, sauf si le preneur a fourni au prestataire son numéro d'identification à la taxe sur la valeur ajoutée dans un autre Etat membre de la Communauté européenne et si les biens sont expédiés ou transportés hors de France (...) ". Pour l'application de ces dispositions, il convient de déterminer si la prestation en cause relève du 4° bis de l'article 259 A du code général des impôts ou des autres cas spécifiques mentionnés à cet article et aux articles 259 B à 259 D du même code et, à défaut, si elle entre dans le champ d'application de la règle générale posée à l'article 259 de ce code. <br/>
<br/>
              4. La Cour de justice des communautés européennes a dit pour droit, au point 13 de son arrêt du 6 mars 1997 Maatschap M.J.M. C...A..., Pouwels en J. Scheres CS   (C-167/95), que la notion d' " expertise de biens meubles corporels ", au sens des 1 et 2 de l'article 9 de la sixième directive, transposée au 4° bis de l'article 259 A du code général des impôts, correspondait " à l'examen de l'état physique ou à l'étude de l'authenticité d'un bien, en vue de procéder à une estimation de sa valeur ou à une évaluation de travaux à effectuer ou de l'étendue d'un dommage subi ". <br/>
<br/>
              5. Il ressort des pièces du dossier soumis à la cour que l'activité de " liaison qualité " exercée par M. B...au profit d'équipementiers automobiles espagnols se bornait à la vérification, lors de déplacements sur les sites de production français des véhicules de la marque PSA, de la qualité des pièces utilisées sur les lignes de montage des véhicules et à la validation de la mise à l'écart de celles présentant des défauts, à l'analyse des messages d'alerte envoyés par les sites de montage et à la transmission des rapports de visite à ses donneurs d'ordres. Ainsi, alors même que le compte-rendu de l'entretien qui a été réalisé par le vérificateur avec M. B...le 25 novembre 2009 et contresigné par celui-ci, mentionnait que l'intéressé exerçait une activité " d'expertise ", aucun élément du dossier soumis à la cour n'établissait que le travail d'examen auquel se livrait M. B...était réalisé en vue de procéder à une estimation de la valeur des pièces ou à une évaluation de travaux à effectuer sur celles-ci ou d'un éventuel dommage subi par ces dernières. Par suite, en jugeant que les prestations effectuées par M. B...devaient être regardées comme des expertises portant sur des biens meubles corporels, imposables à la taxe sur la valeur ajoutée en vertu du 4° bis de l'article 259 A du code général des impôts, la cour a inexactement qualifié les faits qui lui étaient soumis. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SCP Jacques Moyrand est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Jacques Moyrand au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 1er mars 2016 de la cour administrative d'appel de Versailles est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : L'Etat versera à la SCP Jacques Moyrand la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SCP Jacques Moyrand, en qualité de liquidateur judiciaire de M.B..., et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
