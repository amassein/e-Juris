<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042120826</ID>
<ANCIEN_ID>JG_L_2020_07_000000432325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/12/08/CETATEXT000042120826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 15/07/2020, 432325</TITRE>
<DATE_DEC>2020-07-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432325.20200715</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C... et Mme D... A... épouse C... ont demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la décision du 20 septembre 2011 par laquelle le maire d'Echirolles a exercé le droit de préemption urbain sur la parcelle AY 331 et la décision du 8 décembre 2011 rejetant leur recours gracieux. Par un jugement n° 1106752 du 6 février 2014, le tribunal administratif de Grenoble a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14LY00996 du 31 mai 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme C... contre ce jugement.<br/>
<br/>
              Par une décision n° 401464, le Conseil d'Etat, statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Lyon.<br/>
<br/>
              Par un nouvel arrêt n° 17LY03851, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme C... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 juillet et 2 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Echirolles la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jehannin, avocat de M. et Mme C... et à la SCP Piwnica, Molinié, avocat de la commune d'Echirolles ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 20 septembre 2011, le maire d'Echirolles a exercé le droit de préemption urbain sur la parcelle cadastrée AY 331, appartenant à Electricité de France. M. et Mme C..., acquéreurs évincés, ont demandé l'annulation pour excès de pouvoir de cette décision au tribunal administratif de Grenoble, qui a rejeté leur demande par un jugement du 6 février 2014. Ils se pourvoient en cassation contre l'arrêt par lequel, statuant sur renvoi du Conseil d'Etat après annulation d'un premier arrêt, la cour administrative d'appel de Lyon a rejeté leur appel. <br/>
<br/>
              2. Aux termes du dernier alinéa de l'article R. 741-2 du code de justice administrative : " La décision fait apparaître la date de l'audience et la date à laquelle elle a été prononcée ". L'arrêt attaqué mentionnant, dans son en-tête et en dernière page, des dates de lecture contradictoires, M. et Mme C... sont fondés à soutenir que ces mentions ne permettent pas d'établir la date à laquelle sa lecture est effectivement intervenue et l'entache d'irrégularité. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de leur pourvoi, ils sont fondés à en demander, pour ce motif, l'annulation. <br/>
<br/>
              3. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              4. Aux termes de l'article L. 210-1 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision attaquée : " Les droits de préemption institués par le présent titre sont exercés en vue de la réalisation, dans l'intérêt général, des actions ou opérations répondant aux objets définis à l'article L. 300-1, à l'exception de ceux visant à sauvegarder ou à mettre en valeur les espaces naturels, ou pour constituer des réserves foncières en vue de permettre la réalisation desdites actions ou opérations d'aménagement. / (...) / Toute décision de préemption doit mentionner l'objet pour lequel ce droit est exercé. (...) ". Aux termes du premier alinéa de l'article L. 300-1 du même code, dans sa rédaction applicable à la même date : " Les actions ou opérations d'aménagement ont pour objets de mettre en oeuvre un projet urbain, une politique locale de l'habitat, d'organiser le maintien, l'extension ou l'accueil des activités économiques, de favoriser le développement des loisirs et du tourisme, de réaliser des équipements collectifs ou des locaux de recherche ou d'enseignement supérieur, de lutter contre l'insalubrité, de permettre le renouvellement urbain, de sauvegarder ou de mettre en valeur le patrimoine bâti ou non bâti et les espaces naturels ". Il résulte de ces dispositions que, pour exercer légalement ce droit, les collectivités titulaires du droit de préemption urbain doivent, d'une part, justifier, à la date à laquelle elles l'exercent, de la réalité d'un projet d'action ou d'opération d'aménagement répondant aux objets mentionnés à l'article L. 300-1 du code de l'urbanisme, alors même que les caractéristiques précises de ce projet n'auraient pas été définies à cette date, et, d'autre part, faire apparaître la nature de ce projet dans la décision de préemption.<br/>
<br/>
              5. Il ressort des pièces du dossier que la décision litigieuse est motivée par la volonté de la commune de construire des logements sur la parcelle préemptée, en vue de répondre à l'objectif du programme local de l'habitat de proposer une offre de logement suffisante et aux objectifs de livraison de logements fixés par ce programme pour la période allant de 2010 à 2015. Si elle fait ainsi apparaître la nature du projet d'action ou d'opération d'aménagement poursuivi, il ne ressort pas du programme local de l'habitat pour la période considérée qu'il envisagerait, dans le secteur de la parcelle préemptée, la construction de logements pour en accroître l'offre dans l'agglomération. Il ressort en outre des pièces du dossier que le " schéma de faisabilité " établi en août 2011 en vue de la construction de deux lots de logements sur la parcelle et sur la parcelle voisine appartenant toujours à Electricité de France était particulièrement succinct et que de fortes contraintes s'opposent à la réalisation d'un tel projet sur cette parcelle, qui est enclavée sur trois côtés, située dans la zone de dangers d'une centrale hydroélectrique et à proximité d'une plateforme chimique et classée par le plan local d'urbanisme  en zone UA indice "ru" ne permettant la construction d'habitations que sous réserve de mesures de confinement vis-à-vis de ces aléas technologiques. Dans ces conditions, la réalité, à la date de la décision de préemption, du projet d'action ou d'opération d'aménagement l'ayant justifiée ne peut être regardée comme établie pour cette parcelle qui, au surplus, a été revendue par la commune à l'établissement public foncier local de la région grenobloise dans un but de réserve foncière en vertu d'un acte authentique du 20 janvier 2012 pris, après une délibération en ce sens du conseil municipal intervenue dès le 25 octobre 2011.<br/>
<br/>
              6. Pour l'application de l'article L. 600-4-1 du code de l'urbanisme, aucun autre moyen n'est susceptible de fonder l'annulation de la décision attaquée. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. et Mme C... sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a rejeté leur demande tendant à l'annulation de la décision du 20 septembre 2011 du maire d'Echirolles, ainsi que de la décision du 8 décembre 2011 rejetant leur recours gracieux. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Echirolles une somme de 5 000 euros à verser à M. et Mme C... sur le fondement de l'article L. 761-1 du code de justice administrative, au titre des frais qu'ils ont exposés tant devant les juges du fond que devant le Conseil d'Etat. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme C..., qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt n° 17LY03851 de la cour administrative d'appel de Lyon, le jugement du tribunal administratif de Grenoble du 6 février 2014 et les décisions des 20 septembre et 8 décembre 2011 du maire d'Echirolles sont annulés.<br/>
Article 2 : La commune d'Echirolles versera une somme de 5 000 euros à M. et Mme C... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la commune d'Echirolles présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme B... C... et à la commune d'Echirolles.<br/>
Copie en sera adressée à Electricité de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. DROIT DE PRÉEMPTION URBAIN. - JUSTIFICATION, À LA DATE DE LA PRÉEMPTION, DE LA RÉALITÉ D'UN PROJET RÉPONDANT AUX OBJETS MENTIONNÉS À L'ARTICLE L. 300-1 DU CODE DE L'URBANISME [RJ1] - ILLUSTRATION - PROJET DE CONSTRUCTION DE LOGEMENTS NON ENVISAGÉ DANS LE PLH, SUR UNE PARCELLE SOUMISE À DE FORTES CONTRAINTES S'OPPOSANT À SA RÉALISATION ET REVENDUE PAR LA COMMUNE 3 MOIS PLUS TARD - JUSTIFICATION - ABSENCE.
</SCT>
<ANA ID="9A"> 68-02-01-01-01 La décision de préemption litigieuse est motivée par la volonté de la commune de construire des logements sur la parcelle préemptée, en vue de répondre à l'objectif du programme local de l'habitat (PLH) de proposer une offre de logement suffisante et aux objectifs de livraison de logements fixés par ce programme pour la période allant de 2010 à 2015. Si elle fait ainsi apparaître la nature du projet d'action ou d'opération d'aménagement poursuivi, il ne ressort pas du PLH pour la période considérée qu'il envisagerait, dans le secteur de la parcelle préemptée, la construction de logements pour en accroître l'offre dans l'agglomération. Il ressort en outre des pièces du dossier que le schéma de faisabilité établi en août 2011 en vue de la construction de deux lots de logements sur la parcelle et sur la parcelle voisine appartenant toujours à Electricité de France était particulièrement succinct et que de fortes contraintes s'opposent à la réalisation d'un tel projet sur cette parcelle, qui est enclavée sur trois côtés, située dans la zone de dangers d'une centrale hydroélectrique et à proximité d'une plateforme chimique et classée par le plan local d'urbanisme en zone UA indice ru ne permettant la construction d'habitations que sous réserve de mesures de confinement vis-à-vis de ces aléas technologiques. Dans ces conditions, la réalité, à la date de la décision de préemption, du projet d'action ou d'opération d'aménagement l'ayant justifié ne peut être regardée comme établie pour cette parcelle qui, au surplus, a été revendue par la commune à un établissement public foncier local dans un but de réserve foncière en vertu d'un acte authentique du 20 janvier 2012 pris après une délibération en ce sens du conseil municipal intervenue dès le 25 octobre 2011. Annulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 mars 2008, Commune de Meung-sur-Loire, n° 288371, p. 97.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
