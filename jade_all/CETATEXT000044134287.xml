<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044134287</ID>
<ANCIEN_ID>JG_L_2021_09_000000451652</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/13/42/CETATEXT000044134287.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/09/2021, 451652, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451652</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451652.20210929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SNCF Réseau a demandé au juge des référés du tribunal administratif de Marseille, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre à la commune de Veynes, à titre principal, de réaliser, à ses frais, les travaux permettant de mettre fin au risque d'effondrement de la canalisation d'eaux usées passant sous les voies ferrées en gare de Veynes, et à titre subsidiaire, de neutraliser, à ses frais, cette canalisation, en la comblant par des matériaux solides et incompressibles et dans les conditions compatibles avec les exigences de sécurité du service public ferroviaire et d'assortir la mesure à intervenir d'une astreinte d'un montant de 500 euros par jour de retard à l'expiration d'un délai qui ne saurait excéder un mois à compter de la notification de l'ordonnance, déduction faite des délais de validation des études et de délivrance des autorisations par SNCF Réseau. <br/>
<br/>
              Par une ordonnance n° 2101745 du 29 mars 2021, le juge des référés du tribunal administratif de Marseille a enjoint à la commune de Veynes de prendre, à ses frais, toutes mesures nécessaires pour neutraliser la canalisation existante située sous la voie ferrée en gare de Veynes, dans les conditions compatibles avec les exigences sécuritaires du service public ferroviaire, afin de parer à tout risque de tassement ou de compression du sol, et ce dans le délai d'un mois à compter de la notification de son ordonnance, déduction faite des délais de validation des études et délivrance des autorisations par SNCF Réseau.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 et 28 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de Veynes demande au Conseil d'Etat :<br/>
<br/>
<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par SNCF Réseau ;<br/>
<br/>
              3°) de mettre à la charge de SNCF Réseau la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la Scp Matuchansky, Poupot, Valdelièvre, avocat de la commune de Veynes et à la Scp Piwnica, Molinié, avocat de la société SNCF Réseau ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 septembre 2021, présentée par la commune de Veynes. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. (...) ". Aux termes de l'article L.521-3 même code : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". Il résulte des dispositions combinées de ces articles que, saisi sur le fondement de cette dernière disposition d'une demande qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Marseille que SNCF Réseau a saisi celui-ci d'une demande tendant à ce qu'il soit enjoint à la commune de Veynes de réaliser les travaux, compatibles avec les exigences de sécurité du service public ferroviaire, permettant de mettre fin au risque d'effondrement de la canalisation d'eaux usées passant sous les voies ferrées de la gare de Veynes et, à titre subsidiaire, de neutraliser, à ses frais, cette canalisation. Un rapport établi par un expert désigné par le juge des référés du même tribunal et déposé le 23 décembre 2020 a constaté des anomalies structurelles de la canalisation en cause présentant un niveau de gravité très élevé, et a conclu qu'en raison du risque très important d'évolution de la situation, la canalisation devait être remplacée d'urgence. <br/>
<br/>
              3. En enjoignant à la commune de " neutraliser " la canalisation alors que cette mesure n'était pas la seule permettant de mettre un terme au danger grave et immédiat d'effondrement du sol et ne pouvait être regardée comme présentant un caractère provisoire ou conservatoire, le juge des référés a commis une erreur de droit et méconnu son office. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              5. Il résulte de l'instruction que la demande de SNCF Réseau, qui a la qualité de tiers à l'égard de la canalisation en litige, n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative, qu'elle ne fait obstacle à aucune décision administrative, que la commune de Veynes a la qualité de maître d'ouvrage de la canalisation défectueuse et qu'ainsi qu'il est dit au point 2, l'état dégradé et le risque d'effondrement de cette canalisation du réseau d'eaux usées passant sous les voies ferrées nécessitent que des travaux soient réalisés d'urgence afin de prévenir celui-ci.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu d'ordonner à la commune de Veynes de réaliser, dans des conditions compatibles avec les exigences de sécurité du service public ferroviaire, les travaux nécessaires afin de prévenir tout risque d'effondrement de cette canalisation, dans le délai de deux mois à compter de la notification de la présente décision, déduction faite des délais de validation des études et de délivrance des autorisations nécessaires par SNCF Réseau, sans qu'il y ait lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de SNCF Réseau qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Veynes, au titre de l'ensemble de la procédure, la somme de 3 000 euros à verser à SNCF Réseau au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 29 mars 2021 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 2 : Il est enjoint à la commune de Veynes de réaliser, à ses frais, dans des conditions compatibles avec les exigences de sécurité du service public ferroviaire, les travaux nécessaires sur la canalisation existante située sous la voie ferrée en gare de Veynes afin de parer à tout risque de tassement ou de compression du sol, et ce dans le délai deux mois à compter de la notification de la présente décision, déduction faite des délais de validation des études et de délivrance des autorisations nécessaires par SNCF Réseau. <br/>
Article 3 : La commune de Veynes versera à SNCF Réseau la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées sur le même fondement sont rejetées.<br/>
Article 4 : Le surplus des conclusions de la demande présentée par SNCF Réseau devant le juge des référés du tribunal administratif de Marseille est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la commune de Veynes et à SNCF Réseau. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
