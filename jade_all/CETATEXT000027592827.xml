<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592827</ID>
<ANCIEN_ID>JG_L_2013_06_000000345825</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592827.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 19/06/2013, 345825, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345825</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:345825.20130619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 janvier et 18 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA01821 du 15 novembre 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête dirigée contre le jugement n° 0505194/6-3 et n° 0509102/6-3 du 2 février 2009 par lequel le tribunal administratif de Paris a rejeté ses demandes tendant, d'une part, à la condamnation du conseil régional de l'ordre des chirurgiens-dentistes à lui verser la somme de 150 000 euros en réparation du préjudice subi en raison de la poursuite illégale des procédures de recouvrement des frais d'instance mis à sa charge par les décisions prononçant à son encontre deux sanctions disciplinaires les 7 décembre 2000 et le 6 septembre 2001, ainsi que la publication du jugement dans divers quotidiens nationaux et revues spécialisées, sous astreinte, d'autre part, à la condamnation de l'Etat à lui verser la somme de 150 000 euros en réparation du préjudice subi du fait de l'irrégularité de la procédure de saisie diligentée par un huissier de justice et un commissaire de police sur son domicile professionnel et ses comptes bancaires ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et d'ordonner la capitalisation des intérêts ; <br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et du conseil régional de l'ordre la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...et à la SCP Hémery, Thomas-Raquin, avocat du conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le requérant a fait l'objet de deux décisions rendues par la section disciplinaire du conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France les 7 décembre 2000 et 6 septembre 2001, confirmées par une décision de la section disciplinaire du conseil national de l'ordre des chirurgiens-dentistes du 3 mai 2002, contre laquelle il s'est pourvu en cassation ; que le conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France a diligenté le recouvrement forcé des frais d'instance auxquels le requérant était condamné par les deux décisions précitées de la section disciplinaire du conseil régional de l'ordre ; qu'il se pourvoit contre l'arrêt par lequel la cour administrative d'appel de Paris a rejeté sa requête dirigée contre le jugement du 2 février 2009 par lequel le tribunal administratif de Paris a rejeté ses demandes tendant à la condamnation du conseil régional de l'ordre des chirurgiens-dentistes et de l'Etat en réparation du préjudice subi en raison de la poursuite des procédures de recouvrement des frais d'instance mis à sa charge ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une lettre du 10 mai 2005 adressée au requérant, le président de la section disciplinaire de l'ordre des chirurgiens-dentistes lui faisait savoir, en réponse à sa demande, que " les différentes autorités qui avaient reçu notification des décisions de la section disciplinaire [le] concernant ont été informées, le 9 juillet 2002, du caractère suspensif des pourvois formés devant le Conseil d'Etat " ; qu'au nombre des autorités destinataires de cette notification, et par suite de l'information délivrée par la lettre du président de la section disciplinaire, figurait le conseil régional d'Ile-de-France de l'ordre ; que, dès lors, la cour administrative d'appel ne pouvait, sans dénaturer les pièces soumises à son examen, juger que, " nonobstant la lettre du 10 mai 2005 ", il ne résultait pas de l'instruction que le conseil régional d'Ile-de-France ait été effectivement destinataire de cette information ;  que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions à ce titre du conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 15 novembre 2010 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions de M. A...et du conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., au conseil régional de l'ordre des chirurgiens-dentistes d'Ile-de-France et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
