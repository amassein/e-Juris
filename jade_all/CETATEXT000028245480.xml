<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028245480</ID>
<ANCIEN_ID>JG_L_2013_11_000000359032</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/24/54/CETATEXT000028245480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 27/11/2013, 359032, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359032</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359032.20131127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Carrefour Hypermarchés, dont le siège est situé 1, rue Jean Mermoz, à Evry (91000) ; la société Carrefour Hypermarchés demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1190 T du 1er février 2012 par laquelle la Commission nationale d'aménagement commercial a autorisé la SCI Des Charmes à procéder à l'extension de 900 m² d'un magasin Super U d'une surface de vente de 2 475 m², portant sa surface de vente totale à 3 375 m², à Devecey (Doubs) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la SCI Des Charmes ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              1. Considérant en premier lieu que, contrairement à ce que soutient la requérante, il ne résulte d'aucune disposition législative ou réglementaire ni d'aucun principe que les décisions de la Commission nationale d'aménagement commercial doivent comporter des mentions attestant du fait que ses membres ont pu prendre connaissance en temps utile de l'ensemble des documents nécessaires à l'examen du dossier ; <br/>
<br/>
              2. Considérant en second lieu qu'il résulte de la combinaison des dispositions du quatrième alinéa de l'article R. 752-51 du code de commerce et du deuxième alinéa de l'article R. 752-16 du même code que les ministres intéressés dont les avis doivent être recueillis par le commissaire du gouvernement et présentés à la commission conformément à l'article R. 752-51 précité, sont ceux qui ont autorité sur les services chargés d'instruire les demandes, soit les ministres en charge du commerce, de l'urbanisme et de l'environnement ; qu'ainsi, contrairement à ce que soutient la requérante, la commission n'était pas tenue de recueillir l'avis des ministres chargés de l'emploi et de l'aménagement du territoire ; qu'il ressort par ailleurs des pièces du dossier que les avis des ministres intéressés au sens de l'article R. 752-51 du code de commerce présentés à la commission ont été signés par des personnes dûment habilitées à le faire ;<br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              3. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              4. Considérant, d'une part, qu'il ressort des pièces du dossier que l'extension du magasin qu'autorise la décision attaquée permettra d'améliorer le confort des consommateurs et de diversifier l'offre commerciale dans la commune de Devecey, contribuant ainsi à limiter les déplacements des consommateurs vers des pôles commerciaux plus éloignés sans nuire à l'animation de la vie de la commune ; que ses effets sur les flux de transport apparaissent limités et que l'accès à ce magasin ne pose pas de difficulté particulière en termes de sécurité eu égard à la fréquentation de l'axe routier en cause et au dégagement existant ; <br/>
<br/>
              5. Considérant, d'autre part, qu'il ressort des pièces du dossier que le site est accessible par les modes de transport doux et desservi par les transports en commun, la fréquence de cette desserte, à la supposer insuffisante, n'étant pas de nature à compromettre, à elle seule, la réalisation des objectifs fixés par le législateur ; que, par ailleurs, le projet comporte des dispositions permettant un traitement adéquat des eaux ainsi que des déchets ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la Commission nationale d'aménagement commercial n'a pas, en prenant la décision attaquée, fait une inexacte application des dispositions rappelées plus haut du code de commerce ; <br/>
<br/>
              En ce qui concerne la compatibilité du projet avec le schéma de cohérence territoriale :<br/>
<br/>
              7. Considérant qu'en vertu de l'article L. 122 1 du code de l'urbanisme, les autorisations délivrées par la Commission nationale d'aménagement commercial doivent être compatibles avec les schémas de cohérence territoriale ; que si les orientations du schéma de cohérence territoriale applicable prévoient le " développement du commerce de proximité " et précise que le schéma de cohérence territoriale se " dote de principes d'implantations des surfaces à dominante alimentaire " il n'en résulte aucune incompatibilité de la décision de la commission nationale autorisant l'extension, au demeurant limitée, de la surface commerciale concernée, qui permettra d'améliorer l'offre de proximité dans la commune de Devecey ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que les conclusions présentées par la société Carrefour Hypermarchés contre la décision de la commission nationale doivent être rejetées ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu de mettre à la charge de la société requérante le versement de la somme de 5 000 euros à la SCI Des Charmes au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société Carrefour Hypermarchés est rejetée. <br/>
<br/>
Article 2 : La société Carrefour Hypermarchés versera à la SCI Des Charmes la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Carrefour Hypermarchés, à la SCI Des Charmes et à la Commission nationale d'aménagement commercial. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
