<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043486349</ID>
<ANCIEN_ID>JG_L_2021_05_000000451915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/63/CETATEXT000043486349.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/05/2021, 451915, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451915.20210504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B..., épouse A..., a demandé au juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de la confiscation de son titre de séjour et, d'autre part, d'enjoindre à la police des frontières, le cas échéant au préfet de la Seine-Saint-Denis, de lui restituer son titre de séjour dans un délai de 48 heures sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 2104321 du 21 avril 2021, le juge des référés du tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 21 avril 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance et d'ordonner, par voie de conséquence, la suspension de l'exécution de la décision de retrait de son titre de séjour ;<br/>
<br/>
              2°) d'enjoindre à la police aux frontières et le cas échéant au préfet de Seine-Saint-Denis de lui restituer son titre de séjour dans le délai de 48 heures sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - l'absence d'un motif impérieux pour voyager ne pouvait lui être légalement opposée alors que cette exigence porte une atteinte disproportionnée à son droit fondamental d'accéder à son pays, la Côte d'Ivoire ;<br/>
              - la condition d'urgence doit être présumée dans le cas d'un retrait ou d'un refus de renouvellement du titre de séjour ;<br/>
              - la confiscation de son titre de séjour est constitutive d'une situation d'urgence, ce titre lui permettant de bénéficier d'une prise d'une prise en charge médicale sur le territoire français, cette urgence étant renforcée par le contexte de crise sanitaire liée à l'épidémie de covid-19 que traverse le territoire français ;<br/>
              - il existe un doute sérieux quant à la légalité de l'acte attaqué ;<br/>
              - la confiscation de son titre de séjour porte une atteinte grave et manifestement illégale à son droit à la vie en raison des conséquences qu'elle peut sur son état de santé.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte de l'instruction que Mme B..., ressortissante ivoirienne née le 12 décembre 1945, s'est vu délivrer une carte de séjour mention " vie privée et familiale " pour raisons de santé, valable du 28 décembre 2020 jusqu'au 24 septembre 2021. Le 7 mars 2021, Mme B... s'est présentée à l'aéroport de Roissy-Charles-de-Gaulle pour embarquer sur un vol à destination de la Côte d'Ivoire. Ne justifiant pas d'un motif impérieux d'ordre personnel, familial, professionnel ou sanitaire permettant de se déplacer entre le territoire métropolitain et un pays étranger, elle a souhaité maintenir son départ et a indiqué renoncer à son droit de résidence en France en remettant son titre de séjour à la police de l'air et des frontières. Le 30 mars 2021, elle a saisi le juge des référés du tribunal administratif de Montreuil, sur le fondement de l'article L. 521-2 du code de justice administrative, pour qu'il enjoigne à l'administration de lui restituer le titre de séjour. Par une ordonnance du 1er avril 2021, le juge des référés a rejeté sa demande en estimant que la condition d'urgence nécessaire à l'intervention du juge des référés dans un délai de quarante-huit heures ne pouvait être regardée comme remplie.<br/>
<br/>
              3. En premier lieu, en distinguant les deux procédures prévues par les articles L. 521-1 et L. 521-2 mentionnés au point 1, le législateur a entendu répondre à des situations différentes. Les conditions auxquelles est subordonnée l'application de ces dispositions ne sont pas les mêmes, non plus que les pouvoirs dont dispose le juge des référés. En particulier, le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article. <br/>
<br/>
              4. En deuxième lieu, pour rejeter la demande de Mme B..., le juge des référés du tribunal administratif a estimé qu'en restituant son titre de séjour pour effectuer son voyage alors qu'elle ne justifiait pas d'un motif impérieux, l'intéressée devait être regardée comme s'étant elle-même placée dans la situation d'urgence qu'elle invoque. En appel, Mme B... soutient de manière générale que ce titre de séjour lui permettrait de bénéficier d'une prise d'une prise en charge médicale sur le territoire français et que l'urgence serait renforcée par le contexte de crise sanitaire liée à l'épidémie de covid-19 que traverse le territoire français. Ces éléments ne permettent pas de justifier de circonstances particulières de nature à infirmer l'appréciation portée par le juge des référés du tribunal administratif de Montreuil.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête de Mme B... doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C... B... épouse A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
