<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038633903</ID>
<ANCIEN_ID>JG_L_2019_06_000000427921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/63/39/CETATEXT000038633903.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 17/06/2019, 427921</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:427921.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Smoke House a demandé au juge des référés du tribunal administratif de Melun d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 14 décembre 2018 par lequel le préfet du Val-de-Marne a prononcé sa fermeture administrative pour une durée de trois mois à compter de sa notification et d'enjoindre au préfet de réexaminer sa situation et de prendre une nouvelle décision dans un délai de huit jours à compter de la présente ordonnance, sous astreinte de 150 euros par jour de retard. Par une ordonnance n° 1900292 du 28 janvier 2019 le juge des référés a suspendu l'exécution de l'arrêté du 14 décembre 2018 et enjoint au préfet du Val-de-Marne de procéder au réexamen de la situation de l'établissement Smoke House dans un délai de quinze jours à compter de la notification de son ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 et 15 février 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Smoke House ;<br/>
<br/>
              3°) de mettre à la charge de la société Smoke House la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code général des impôts ;<br/>
<br/>
              - le code des relations entre le public et l'administration ;<br/>
<br/>
              - le décret n° 2010-720 du 28 juin 2010 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du ministre de l'action et des comptes publics et à la SCP Spinosi, Sureau, avocat de la société Smoke House ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Melun que, par un arrêté du 14 décembre 2018, notifié le 28 décembre 2018, le préfet du Val-de-Marne a, sur le fondement de l'article 1825 du code général des impôts, décidé, à la suite d'un contrôle du respect de la réglementation en matière de vente de tabac, la fermeture de l'établissement Smoke House, situé à Alfortville, pour une durée de trois mois à compter de la notification de l'arrêté. Par une ordonnance du 28 janvier 2019, le juge des référés du tribunal administratif, saisi d'une demande de suspension fondée sur l'article L. 521-1 du code de justice administrative par la société Smoke House qui exploite l'établissement, a suspendu l'exécution de cet arrêté et enjoint au préfet du Val-de-Marne de procéder au réexamen de la situation de la société dans un délai de quinze jours. Le ministre de l'action et des comptes publics se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              2. Aux termes de l'article 1825 du code général des impôts : " La fermeture de tout établissement dans lequel aura été constatée l'une des infractions mentionnées à l'article 1817 peut être ordonnée, pour une durée ne pouvant excéder trois mois, par arrêté préfectoral pris sur proposition de l'autorité administrative désignée par décret. Cet arrêté est affiché sur la porte de l'établissement pendant la durée de la fermeture ". Aux termes de l'article 1817 de ce code : " Les dispositions de l'article 1750 sont applicables aux infractions prévues aux articles 1810, 1811 et 1812 ". Enfin, aux termes de l'article 1810 du même code : " Indépendamment des pénalités prévues aux articles 1791 à 1794, les infractions visées ci-après sont punies d'une peine d'un an d'emprisonnement, et les moyens de transport sont saisis et confisqués, ainsi que les récipients, emballages, ustensiles, mécaniques, machines ou appareil : (...) / 10° Quelles que soient l'espèce et la provenance de ces tabacs : fabrication de tabacs ; détention frauduleuse en vue de la vente de tabacs fabriqués ; vente, y compris à distance, de tabacs fabriqués ; transport en fraude de tabacs fabriqués ; acquisition à distance, introduction en provenance d'un autre Etat membre de l'Union européenne ou importation en provenance de pays tiers de produits du tabac manufacturé acquis dans le cadre d'une vente à distance ".<br/>
<br/>
              3. Il résulte des dispositions citées au point 2 que la fermeture temporaire d'un établissement décidée sur le fondement de l'article 1825 du code général des impôts, si elle est subordonnée au constat des infractions mentionnées à l'article 1817, a pour objet de prévenir le risque d'atteinte à l'ordre public que constituerait la réitération des manquements constatés et présente, par suite, le caractère non d'une sanction mais d'une mesure de police. Alors même qu'elle a été suspendue par le juge des référés, cette mesure, décidée en fonction de la situation existant à la date à laquelle l'autorité compétente a statué, n'est pas susceptible de produire des effets juridiques au-delà de la période d'exécution déterminée par la décision. <br/>
<br/>
              4. Il résulte de ce qui précède que l'arrêté du 14 décembre 2018 ordonnant la fermeture de l'établissement concerné pour une durée de trois mois à compter de sa notification, intervenue le 28 décembre 2018, n'est, alors même que son exécution a été suspendue le 28 janvier 2019 par le juge des référés du tribunal administratif de Melun, plus susceptible de produire des effets à la date de la présente décision. Par suite, eu égard à la nature de la procédure de référé, le pourvoi du ministre de l'action et des comptes publics est désormais privé d'objet. Il n'y a, dès lors, plus lieu d'y statuer. <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la société Smoke House et du ministre de l'action et des comptes publics au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi du ministre de l'action et des comptes publics tendant à l'annulation de l'ordonnance du 28 janvier 2019 du juge des référés du tribunal administratif de Melun.<br/>
Article 2 : Les conclusions présentées par la société Smoke House et par le ministre de l'action et des comptes publics au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SAS Smoke House et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05 POLICE. POLICES SPÉCIALES. - FERMETURE TEMPORAIRE D'UN ÉTABLISSEMENT DANS LEQUEL A ÉTÉ CONSTATÉE UNE INFRACTION RELATIVE À LA FABRICATION, À LA DÉTENTION, AU TRANSPORT OU À LA VENTE D'ALCOOL OU DE TABAC (ART. 1825 DU CGI) - 1) MESURE DE POLICE - EXISTENCE - CONSÉQUENCE - ABSENCE DE SUSPENSION DE LA PÉRIODE D'EXÉCUTION EN CAS DE SUSPENSION DE LA MESURE PAR LE JUGE DES RÉFÉRÉS - 2) CONSÉQUENCE - NON-LIEU EN CASSATION CONTRE L'ORDONNANCE DE RÉFÉRÉ LORSQUE LA PÉRIODE D'EXÉCUTION A PRIS FIN [RJ1] - ILLUSTRATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-05 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). VOIES DE RECOURS. - CASSATION - NON-LIEU - EXISTENCE - POURVOI CONTRE UNE ORDONNANCE SUSPENDANT L'EXÉCUTION D'UNE DÉCISION DE FERMETURE TEMPORAIRE D'UN ÉTABLISSEMENT (ART. 1825 DU CGI) DONT LA PÉRIODE D'EXÉCUTION A PRIS FIN À LA DATE À LAQUELLE LE CONSEIL D'ETAT STATUE [RJ1].
</SCT>
<ANA ID="9A"> 49-05 1) La fermeture temporaire d'un établissement décidée sur le fondement de l'article 1825 du code général des impôts (CGI), si elle est subordonnée au constat des infractions mentionnées à l'article 1817, a pour objet de prévenir le risque d'atteinte à l'ordre public que constituerait la réitération des manquements constatés et présente, par suite, le caractère non d'une sanction mais d'une mesure de police.... ,,Alors même qu'elle a été suspendue par le juge des référés, cette mesure, décidée en fonction de la situation existant à la date à laquelle l'autorité compétente a statué, n'est pas susceptible de produire des effets juridiques au-delà de la période d'exécution déterminée par la décision.,,,2) Il résulte de ce qui précède que l'arrêté du 14 décembre 2018 ordonnant la fermeture d'un établissement pour une durée de trois mois à compter de sa notification, intervenue le 28 décembre 2018, n'est, alors même que son exécution a été suspendue le 28 janvier 2019 par le juge des référés du tribunal administratif, plus susceptible de produire des effets à la date de la décision du Conseil d'Etat. Par suite, eu égard à la nature de la procédure de référé, le pourvoi du ministre de l'action et des comptes publics est désormais privé d'objet. Il n'y a, dès lors, plus lieu d'y statuer.</ANA>
<ANA ID="9B"> 54-035-02-05 La fermeture temporaire d'un établissement décidée sur le fondement de l'article 1825 du code général des impôts (CGI), si elle est subordonnée au constat des infractions mentionnées à l'article 1817, a pour objet de prévenir le risque d'atteinte à l'ordre public que constituerait la réitération des manquements constatés et présente, par suite, le caractère non d'une sanction mais d'une mesure de police.... ,,Alors même qu'elle a été suspendue par le juge des référés, cette mesure, décidée en fonction de la situation existant à la date à laquelle l'autorité compétente a statué, n'est pas susceptible de produire des effets juridiques au-delà de la période d'exécution déterminée par la décision.,,,Il résulte de ce qui précède que l'arrêté du 14 décembre 2018 ordonnant la fermeture d'un établissement pour une durée de trois mois à compter de sa notification, intervenue le 28 décembre 2018, n'est, alors même que son exécution a été suspendue le 28 janvier 2019 par le juge des référés du tribunal administratif, plus susceptible de produire des effets à la date de la décision du Conseil d'Etat. Par suite, eu égard à la nature de la procédure de référé, le pourvoi du ministre de l'action et des comptes publics est désormais privé d'objet. Il n'y a, dès lors, plus lieu d'y statuer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la suspension d'un fonctionnaire ne constituant pas une sanction disciplinaire, CE, 12 février 2003, Commune de Sainte-Maxime, n° 249498, T. p. 925.  Comp., s'agissant d'une sanction disciplinaire, décision du même jour, CE, Centre hospitalier de Valenciennes c/,, n° 426558, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
