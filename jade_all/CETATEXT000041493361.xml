<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041493361</ID>
<ANCIEN_ID>JG_L_2020_01_000000415314</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/49/33/CETATEXT000041493361.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 27/01/2020, 415314</TITRE>
<DATE_DEC>2020-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415314</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:415314.20200127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 415314, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 27 octobre 2017, le 20 juillet 2018 et le 4 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les délibérations prises par le comité de sélection statuant sur sa candidature au concours de recrutement d'un professeur des universités en psychopathologie et psychologie clinique ouvert en 2017 par l'université de Picardie Jules Verne, ainsi que les délibérations du conseil d'administration et du conseil académique relatives à ce concours, la décision par laquelle le président de l'université a rejeté sa demande tendant au réexamen de sa candidature et la nomination de M. A... C... en qualité de professeur des universités ; <br/>
<br/>
              2°) d'enjoindre à l'université de Picardie Jules Verne, à son président, à son conseil d'administration et à son conseil académique de statuer à nouveau sur ce recrutement et de transmettre au ministre en charge de l'enseignement supérieur leurs délibérations en vue d'une nomination par le Président de la République ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Picardie Jules Verne la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 416560, par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés le 14 décembre 2017, le 6 juillet et 20 juillet 2018 et le 4 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les délibérations et décisions analysées sous le numéro 415314 ci-dessus ainsi que le décret du Président de la République du 25 octobre 2017 en tant qu'il nomme M. A... C... en qualité de professeur des universités, et l'affecte à l'université d'Amiens ; <br/>
<br/>
              2°) d'enjoindre à l'université, à son président, à son conseil d'administration et à son conseil académique de statuer à nouveau sur ce recrutement et de transmettre au ministre chargé de l'enseignement supérieur leurs délibérations en vue d'une nomination par le Président de la République ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Picardie Jules Verne la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;  <br/>
              - le décret n° 84-431 du 6 juin 1984 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de l'université de Picardie Jules Verne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers que le conseil académique et le conseil d'administration de l'université de Picardie Jules Verne ont, par des délibérations en date, respectivement, des 1er et 6 juin 2016, décidé de ne pas donner suite au concours, ouvert par cette université, de recrutement d'un professeur des universités en psychologie clinique et psychopathologie sur le poste " 16PR0984 " relevant de la 16ème section du Conseil national des universités. Suite à ces décisions, l'université de Picardie Jules Verne a organisé un nouveau concours de recrutement au cours du printemps 2017 en vue de pourvoir le même poste. Mme D..., maître de conférences, candidate aux concours de recrutement d'un professeur des universités en psychopathologie et psychologie clinique ouverts en 2016 puis en 2017 par l'université, demande, par deux requêtes distinctes, l'annulation des délibérations du 9 mai et du 23 mai 2017 du comité de sélection statuant sur les candidatures au concours de recrutement ouvert en 2017, des délibérations du 1er juin 2017 du conseil académique et du 9 juin 2017 du conseil d'administration de l'université de Picardie Jules Verne se prononçant sur ce recrutement, de la décision implicite née le 3 septembre 2017 par laquelle le président de l'université de Picardie Jules Verne a rejeté sa demande tendant au réexamen de sa candidature et du décret du Président de la République du 25 octobre 2017 en tant qu'il nomme M. A... C... en qualité de professeur des universités et l'affecte à l'université de Picardie Jules Verne. Ces deux requêtes présentant à juger les mêmes questions, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil académique ou, pour les établissements qui n'en disposent pas, du conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / (...) / Au vu de son avis motivé, le conseil académique ou, pour les établissements qui n'en disposent pas, le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) ". Aux termes du premier et du huitième alinéas de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences, dans sa rédaction applicable au litige : " Le comité de sélection examine les dossiers des maîtres de conférences ou professeurs postulant à la nomination dans l'emploi par mutation et des candidats à cette nomination par détachement et par recrutement au concours parmi les personnes inscrites sur la liste de qualification aux fonctions, selon le cas, de maître de conférences ou de professeur des universités. Au vu de rapports pour chaque candidat présenté par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre. Les motifs pour lesquels leur candidature n'a pas été retenue sont communiqués aux candidats qui en font la demande. (...) Le comité de sélection émet un avis motivé unique portant sur l'ensemble des candidats ainsi qu'un avis motivé sur chaque candidature. Ces deux avis sont communiqués aux candidats sur leur demande. (...) ". Si le juge de l'excès de pouvoir ne contrôle pas l'appréciation que le comité de sélection porte sur les mérites notamment scientifiques d'un candidat, il contrôle, en revanche, l'erreur manifeste susceptible d'entacher son appréciation de l'adéquation de la candidature au profil du poste ouvert. A ce titre, il appartient au comité de sélection d'énoncer, dans son avis motivé, les raisons pour lesquelles il estime qu'une candidature n'est pas en adéquation avec le profil du poste ouvert au recrutement. <br/>
<br/>
<br/>
              3. Il ressort des pièces des dossiers que, pour motiver sa décision du 9 mai 2017 défavorable à l'audition de Mme D..., le comité de sélection de l'université de Picardie Jules Verne s'est fondé, non pas sur les mérites scientifiques de la candidate mais sur l'adéquation de la candidature de l'intéressée au profil du poste, en se bornant à mentionner : " Peu d'adéquation aux profils d'enseignement et de recherche ", sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressée correspondait peu à ce profil. Par suite, Mme D... est fondée à soutenir que cette décision est insuffisamment motivée et à en demander l'annulation. <br/>
<br/>
              4. Il résulte de l'annulation prononcée au point 2 que les décisions prises, pour le recrutement sur le poste litigieux, à la suite de la décision écartant illégalement la candidature de Mme D... sont, par voie de conséquence, également entachées d'illégalité. Mme D... est, par suite, fondée, sans qu'il soit besoin d'examiner les autres moyens de ses requêtes, à demander également l'annulation de la délibération du comité de sélection du 23 mai 2017, de la délibération du conseil académique restreint du 1er juin 2017, de la délibération du conseil d'administration restreint du 9 juin 2017, de la décision implicite par laquelle le président de l'université a rejeté son recours gracieux tendant au retrait de l'ensemble des décisions défavorables à son recrutement, ainsi que du décret du 25 octobre 2017 en tant qu'il nomme M. C... en qualité de professeur des universités affecté à l'université d'Amiens. <br/>
<br/>
              5. L'annulation des décisions attaquées implique la reprise des opérations du concours pour le poste " 16PR0984 " relevant de la 16ème section du Conseil national des universités. Elle n'implique pas, en revanche, que la candidature de Mme D... soit nécessairement retenue. Il y a donc seulement lieu d'enjoindre au comité de sélection de se prononcer à nouveau sur les candidatures, dans un délai de trois mois à compter de la notification de la présente décision.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme D..., qui n'est pas la partie perdante dans la présente instance, la somme que demande l'université de Picardie Jules Verne au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université de Picardie Jules Verne la somme que Mme D... demande au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les délibérations du comité de sélection de l'université Picardie Jules Verne des 9 et 23 mai 2017, la délibération du conseil académique du 1er juin 2017, la délibération du conseil d'administration du 9 juin 2017, en tant que le conseil académique et le conseil d'administration se prononcent sur le recrutement sur le poste " 16PR0984 ", la décision implicite par laquelle le président de l'université a rejeté le recours gracieux de Mme D... tendant au retrait de l'ensemble des décisions défavorables à son recrutement, ainsi le décret du Président de la République du 25 octobre 2017 en tant qu'il nomme M. C... en qualité de professeur des universités affecté à l'université de Picardie Jules Verne, sont annulés.<br/>
Article 2 : Il est enjoint au comité de sélection de se prononcer à nouveau sur les candidatures au poste de professeur des universités en psychologie clinique et psychopathologie " 16PR0984 " relevant de la 16ème section du Conseil national des universités dans un délai de trois mois à compter de la notification de la présente décision. <br/>
Article 3 : L'université de Picardie Jules Verne versera à Mme D... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de l'université de Picardie Jules Verne tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... D..., à l'université de Picardie Jules Verne, à M. A... C..., à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. ABSENCE. - AVIS DÉFAVORABLE RENDU PAR UN COMITÉ DE SÉLECTION SUR LA CANDIDATURE D'UN ENSEIGNANT-CHERCHEUR (ART. 9-2 DU DÉCRET DU 6 JUIN 1984) - AVIS N'INDIQUANT PAS, MÊME SOMMAIREMENT, LES RAISONS POUR LESQUELLES LA CANDIDATURE DE L'INTÉRESSÉ NE CORRESPOND PAS AU PROFIL DU POSTE (1).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05-01-06-01-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. GESTION DES UNIVERSITÉS. GESTION DU PERSONNEL. RECRUTEMENT. - AVIS DU COMITÉ DE SÉLECTION SUR LA CANDIDATURE D'UN ENSEIGNANT-CHERCHEUR (ART. 9-2 DU DÉCRET DU 6 JUIN 1984) - 1) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'APPRÉCIATION DE L'ADÉQUATION DE LA CANDIDATURE AU PROFIL DU POSTE - CONTRÔLE DE L'ERREUR MANIFESTE D'APPRÉCIATION [RJ2] - 2) CONSÉQUENCE - MOTIVATION - AVIS DEVANT COMPORTER LES RAISONS POUR LESQUELLES LE COMITÉ ESTIME QU'UNE CANDIDATURE N'EST PAS EN ADÉQUATION AVEC LE PROFIL DU POSTE [RJ1] -ESPÈCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-02-01 Pour motiver sa décision défavorable, le comité de sélection s'est fondé, non pas sur les mérites scientifiques du candidat mais sur l'adéquation de la candidature de l'intéressé au profil du poste, en se bornant à mentionner : Peu d'adéquation aux profils d'enseignement et de recherche , sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressé correspondait peu à ce profil. Par suite, le requérant est fondé à soutenir que cette décision est insuffisamment motivée et à en demander l'annulation.</ANA>
<ANA ID="9B"> 30-02-05-01-06-01-02 1) Si le juge de l'excès de pouvoir ne contrôle pas l'appréciation que le comité de sélection porte sur les mérites notamment scientifiques d'un candidat, il contrôle, en revanche, l'erreur manifeste susceptible d'entacher son appréciation de l'adéquation de la candidature au profil du poste ouvert.... ,,2) A ce titre, il appartient au comité de sélection d'énoncer, dans son avis motivé, les raisons pour lesquelles il estime qu'une candidature n'est pas en adéquation avec le profil du poste ouvert au recrutement.,,,Pour motiver sa décision défavorable, le comité de sélection s'est fondé, non pas sur les mérites scientifiques du candidat mais sur l'adéquation de la candidature de l'intéressé au profil du poste, en se bornant à mentionner : Peu d'adéquation aux profils d'enseignement et de recherche , sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressé correspondait peu à ce profil. Par suite, le requérant est fondé à soutenir que cette décision est insuffisamment motivée et à en demander l'annulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 14 octobre 2011, M.,, n°s 333712, 334692, T. pp. 736-956. Rappr., s'agissant des avis défavorables rendus par le conseil académique sur le fondement de l'article 9-2 du décret du 6 juin 1984, CE, 30 janvier 2019, Mme,, n° 412159, à mentionner aux Tables.,,[RJ2] Cf. CE, 9 février 2011, M.,, n° 317314, T. pp. 956-1062-1100.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
