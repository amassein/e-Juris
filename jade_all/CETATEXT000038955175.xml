<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038955175</ID>
<ANCIEN_ID>JG_L_2019_08_000000430906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/95/51/CETATEXT000038955175.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 21/08/2019, 430906, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-08-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430906.20190821</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 21 mai et 3 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le Collectif des citoyens exposés au trafic aérien (COCETA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions de la Commission nationale du débat public n° 2018/87 du 7 novembre 2018 et n° 2019/92 du 7 mai 2019 relatives au projet de réaménagement de l'aéroport de Nantes-Atlantique ; <br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 juillet 2019, présentée par le Collectif des citoyens exposés au trafic aérien ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article R. 351-4 du code de justice administrative : " Lorsque tout ou partie des conclusions dont est saisi (...) le Conseil d'Etat relève de la compétence d'une juridiction administrative, (...) le Conseil d'Etat (...) est compétent, nonobstant les règles de répartition des compétences entre juridictions administratives, pour rejeter les conclusions entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance ou pour constater qu'il n'y a pas lieu de statuer sur tout ou partie des conclusions ". <br/>
<br/>
              2.	Par sa décision n° 2018/87 du 7 novembre 2018, la Commission nationale du débat public a refusé d'organiser un débat public pour le projet de réaménagement de l'aéroport de Nantes-Atlantique, décidé de l'organisation d'une concertation préalable et désigné un garant du processus de concertation. Par sa décision n° 2019/92 du 7 mai 2019, la Commission a constaté la complétude du dossier de concertation et fixé les modalités de la concertation, notamment le calendrier et le périmètre du projet.  <br/>
<br/>
              3.	D'une part, aux termes de l'article R. 421-1 du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. " Le recours contentieux tendant à l'annulation pour excès de pouvoir de la décision de la Commission nationale du débat public du 7 novembre 2018, publiée au Journal officiel de la République française le 13 novembre 2018, a été formé par le requérant le 20 mai 2019, soit après l'expiration du délai de recours contentieux. Ainsi, les conclusions tendant à l'annulation de cette décision sont tardives et, par suite, entachées d'une irrecevabilité manifeste insusceptible d'être couverte au cours de l'instance.<br/>
<br/>
              4.	D'autre part, si les actes par lesquels la Commission nationale du débat public décide ou refuse d'organiser un débat public ont le caractère de décisions faisant grief, en revanche les actes que la commission peut être appelée à prendre sur les modalités du débat ou de la concertation préalable, notamment le calendrier et les conditions de son déroulement, n'ont pas le caractère d'actes faisant grief. Par suite, les conclusions présentées par le requérant tendant à l'annulation de la décision du 7 mai 2019 par laquelle la Commission nationale du débat public s'est bornée à constater la complétude du dossier de concertation et à fixer les modalités de cette concertation, notamment le calendrier et le périmètre du projet, sont entachées d'une irrecevabilité manifeste insusceptible d'être couverte au cours de l'instance.  <br/>
<br/>
              5.	Il résulte de tout ce qui précède qu'il y a lieu de rejeter la requête du Collectif des citoyens exposés au trafic aérien, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.  Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la Commission nationale du débat public au titre des mêmes dispositions.  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Collectif des citoyens exposés au trafic aérien est rejetée.<br/>
Article 2 : Les conclusions présentées par la Commission nationale du débat public sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au Collectif des citoyens exposés au trafic aérien et à la Commission nationale du débat public.<br/>
Copie en sera adressée à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
