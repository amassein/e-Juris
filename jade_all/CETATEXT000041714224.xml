<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041714224</ID>
<ANCIEN_ID>JG_L_2020_03_000000421445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/71/42/CETATEXT000041714224.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 11/03/2020, 421445</TITRE>
<DATE_DEC>2020-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421445.20200311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (S.C.I.) 200 Quai de Jemmapes, venant aux droits de la S.C.I. Epinvest, a demandé au tribunal administratif de Versailles d'ordonner à la commune d'Arpajon la restitution de la somme de 110 466,72 euros que la SCI Epinvest a versée au titre de la participation pour non réalisation des aires de stationnement, majorée des intérêts au taux légal. Par un jugement n° 1203578 du 31 décembre 2015, le tribunal administratif a fait droit à la demande de restitution de la somme de 110 466,72 euros, majorée des intérêts au taux légal à compter du 2 mars 2012.<br/>
<br/>
              Par un arrêt n° 16VE00679 du 12 avril 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par la commune d'Arpajon et fait droit à la demande de la S.C.I. 200 Quai de Jemmapes tendant à la capitalisation des intérêts à compter du 2 mars 2013.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 juin et 12 septembre 2018 au secrétariat du contentieux du Conseil d'État, la commune d'Arpajon demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter les conclusions de la S.C.I. 200 Quai de Jemmapes ;<br/>
<br/>
<br/>
<br/>
              3°) de mettre à la charge de la S.C.I. 200 Quai de Jemmapes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune d'Arpajon et à Me Le Prado, avocat de la SCI 200 quai de Jemmapes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 421-3 du code de l'urbanisme, dans sa version alors applicable, issue de l'ordonnance du 19 septembre 2000 portant adaptation de la valeur en euros de certains montants exprimés en francs dans les textes législatifs : " Le permis de construire ne peut être accordé que si les constructions projetées sont conformes aux dispositions législatives et réglementaires concernant l'implantation des constructions, leur destination, leur nature, leur architecture, leurs dimensions, leur assainissement et l'aménagement de leurs abords et si le demandeur s'engage à respecter les règles générales de construction prises en application du chapitre Ier du titre Ier du livre Ier du code de la construction et de l'habitation. / (...) / Lorsque le pétitionnaire ne peut satisfaire lui-même aux obligations imposées par un document d'urbanisme en matière de réalisation d'aires de stationnement, il peut être tenu quitte de ces obligations en justifiant, pour les places qu'il ne peut réaliser lui-même sur le terrain d'assiette ou dans son environnement immédiat, soit de l'obtention d'une concession à long terme dans un parc public de stationnement existant ou en cours de réalisation, soit de l'acquisition de places dans un parc privé de stationnement existant ou en cours de réalisation. / (...) / A défaut de pouvoir réaliser l'obligation prévue au quatrième alinéa, le pétitionnaire peut être tenu de verser à la commune une participation fixée par le conseil municipal, en vue de la réalisation de parcs publics de stationnement. Le montant de cette participation ne peut excéder 12 195 euros par place de stationnement. Cette valeur, fixée à la date de promulgation de la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, est modifiée au 1er novembre de chaque année en fonction de l'indice du coût de la construction publié par l'Institut national de la statistique et des études économiques. / Un décret en Conseil d'Etat détermine les conditions d'application des quatrième et cinquième alinéas du présent article et précise notamment les modalités d'établissement, de liquidation et de recouvrement de la participation prévue au quatrième alinéa, ainsi que les sanctions et garanties y afférentes. (...) ". L'article R. 332-22 du même code, dans sa version alors en vigueur, dispose que : " Le redevable de la participation en obtient, sur sa demande, le dégrèvement ou la restitution : / (...) / d) Si, dans le délai de cinq ans à compter du paiement, la commune ou l'établissement public compétent n'a pas affecté le montant de la participation à la réalisation d'un parc public de stationnement ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le maire d'Arpajon a délivré à la S.C.I. Epinvest, aux droits de laquelle vient la S.C.I. 200 Quai de Jemmapes, le 30 septembre 2005, un permis de construire, modifié les 15 février 2007 et 1er octobre 2008, en vue d'édifier des logements et un local d'activités. La société bénéficiaire du permis de construire ayant été dans l'impossibilité technique de réaliser les douze places de stationnement de la construction autorisée, le permis de construire modificatif du 15 février 2007 a mis à sa charge, en application de l'article L. 421-3 du code de l'urbanisme, une participation d'un montant de 166 860 euros. Le 19 octobre 2007, la société a acquitté ladite participation à hauteur de la somme de 110 466,72 euros correspondant à la participation demandée pour huit places de stationnement exigées par le permis de construire initial. Par lettre du 22 juin 2010, le maire d'Arpajon a informé la société de l'affectation de la participation acquittée par elle à la réalisation du parc de stationnement public dit du Jeu de Paume et lui a rappelé qu'elle restait redevable de la somme de 56 393,28 euros au titre de la participation à raison de quatre places de stationnement. Contestant l'affectation de la participation qu'elle avait versée à la création d'un parc public de stationnement, la société en a demandé la restitution par lettre du 21 février 2012 adressée au maire, qui a rejeté sa demande par une décision notifiée le 2 avril 2012. Par un jugement du 31 décembre 2015, le tribunal administratif de Versailles a ordonné à la commune d'Arpajon la restitution de la somme de 110 466,72 euros, majorée des intérêts au taux légal. Par un arrêt du 12 avril 2018, contre lequel la commune d'Arpajon se pourvoit en cassation, la cour administrative d'appel de Versailles a rejeté son appel formé contre le jugement du tribunal administratif.<br/>
<br/>
              3. Il résulte des dispositions citées au point 1 que la participation pour non-réalisation d'aires de stationnement doit être regardée, non comme une imposition, mais comme une participation que la loi, dans les limites qu'elle définit, autorise la commune à percevoir sur le bénéficiaire du permis de construire à raison des équipements publics dont la réalisation est rendue nécessaire par la construction. Une telle participation doit être affectée au financement de la réalisation d'un parc public de stationnement dans le délai de cinq ans à compter de son paiement. Cette affectation implique le financement, par la commune, dans le délai imparti, d'un parc public de stationnement pour un montant égal ou supérieur à celui des participations perçues pour non-réalisation d'aires de stationnement. Elle doit être en principe établie par les documents budgétaires de la commune, dans le respect du cadre budgétaire et comptable applicable ; la commune peut cependant en justifier par tout moyen.<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que la cour a jugé que si la commune justifiait avoir exposé des dépenses pour l'aménagement du parking du Jeu de Paume pour un montant supérieur au montant global de différentes participations pour non-réalisation d'aires de stationnement, elle n'établissait pas que la participation versée par la S.C.I. Epinvest avait été effectivement affectée à cette opération. En excluant ainsi que la commune puisse faire état de tous éléments de nature à établir l'affectation de la participation dès lors qu'elle n'était pas précisément retracée dans les documents budgétaires de la commune, la cour a entaché son arrêt d'erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la commune d'Arpajon est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la S.C.I. 200 Quai de Jemmapes la somme de 3 000 euros à verser à la commune d'Arpajon au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune d'Arpajon qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 12 avril 2018 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : La S.C.I. 200 Quai de Jemmapes versera à la commune d'Arpajon une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4: Les conclusions présentées par la S.C.I. 200 Quai de Jemmapes au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune d'Arpajon et à la S.C.I. 200 Quai de Jemmapes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-04-02-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. DÉPENSES. DÉPENSES OBLIGATOIRES. - PARTICIPATION POUR NON-RÉALISATION D'AIRES DE STATIONNEMENT [RJ1] - AFFECTATION OBLIGATOIRE, PAR LA COMMUNE, AU FINANCEMENT DE LA RÉALISATION D'UN PARC PUBLIC DE STATIONNEMENT - PREUVE DE L'AFFECTATION - PREUVE LIBRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-02-03 COMPTABILITÉ PUBLIQUE ET BUDGET. BUDGETS. BUDGET DES COMMUNES (VOIR : COLLECTIVITÉS TERRITORIALES). - PARTICIPATION POUR NON-RÉALISATION D'AIRES DE STATIONNEMENT [RJ1] - AFFECTATION OBLIGATOIRE, PAR LA COMMUNE, AU FINANCEMENT DE LA RÉALISATION D'UN PARC PUBLIC DE STATIONNEMENT - PREUVE DE L'AFFECTATION - PREUVE LIBRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-024 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CONTRIBUTIONS DES CONSTRUCTEURS AUX DÉPENSES D'ÉQUIPEMENT PUBLIC. - PARTICIPATION POUR NON-RÉALISATION D'AIRES DE STATIONNEMENT [RJ1] - AFFECTATION OBLIGATOIRE, PAR LA COMMUNE, AU FINANCEMENT DE LA RÉALISATION D'UN PARC PUBLIC DE STATIONNEMENT - PREUVE DE L'AFFECTATION - PREUVE LIBRE.
</SCT>
<ANA ID="9A"> 135-02-04-02-01 La participation pour non-réalisation d'aires de stationnement doit être affectée au financement de la réalisation d'un parc public de stationnement dans le délai de cinq ans à compter de son paiement. Une telle affectation implique le financement, par la commune, dans le délai imparti, d'un parc public de stationnement pour un montant égal ou supérieur à celui des participations perçues pour non-réalisation d'aires de stationnement. Elle doit être en principe établie par les documents budgétaires de la commune, dans le respect du cadre budgétaire et comptable applicable ; la commune peut cependant en justifier par tout moyen.</ANA>
<ANA ID="9B"> 18-02-03 La participation pour non-réalisation d'aires de stationnement doit être affectée au financement de la réalisation d'un parc public de stationnement dans le délai de cinq ans à compter de son paiement. Une telle affectation implique le financement, par la commune, dans le délai imparti, d'un parc public de stationnement pour un montant égal ou supérieur à celui des participations perçues pour non-réalisation d'aires de stationnement. Elle doit être en principe établie par les documents budgétaires de la commune, dans le respect du cadre budgétaire et comptable applicable ; la commune peut cependant en justifier par tout moyen.</ANA>
<ANA ID="9C"> 68-024 La participation pour non-réalisation d'aires de stationnement doit être affectée au financement de la réalisation d'un parc public de stationnement dans le délai de cinq ans à compter de son paiement. Une telle affectation implique le financement, par la commune, dans le délai imparti, d'un parc public de stationnement pour un montant égal ou supérieur à celui des participations perçues pour non-réalisation d'aires de stationnement. Elle doit être en principe établie par les documents budgétaires de la commune, dans le respect du cadre budgétaire et comptable applicable ; la commune peut cependant en justifier par tout moyen.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'absence de caractère d'imposition de cette participation, CE, Section, 26 mars 1999,,, n° 189805, p. 109 ; CE, 10 octobre 2014, Min. c/ Commune de Cavalaire-sur-Mer, n° 356722, p. 308.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
