<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875952</ID>
<ANCIEN_ID>JG_L_2021_07_000000453722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/07/2021, 453722, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453722.20210705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... B... et M. A... C... ont demandé au juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Morbihan de les orienter vers un dispositif d'hébergement susceptible de les accueillir avec leurs enfants, adapté à leur situation, dans un délai de 24h à compter de la notification de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 2102746 du 2 juin 2021, le juge des référés du tribunal administratif de Rennes a rejeté leurs demandes.<br/>
<br/>
              Par une requête, enregistrée le 17 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... et M. C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de les admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Rennes du 2 juin 2021 ;<br/>
<br/>
              3°) d'enjoindre au préfet du Morbihan de les orienter vers une structure adaptée à leur situation et susceptible de les accueillir, dans un délai de 24h à compter de la notification de l'ordonnance à intervenir, sous astreinte de 200 euros par jours de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article 37, alinéa 2, de la loi n° 91-647 du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est remplie eu égard à l'atteinte grave portée au droit au logement d'urgence dès lors que, privés de ressources et d'hébergement stable, ils sont placés dans une situation d'extrême vulnérabilité du fait de la fin de leur prise en charge depuis le 1er juin 2021 ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit à l'hébergement d'urgence et au droit à ne pas subir des traitements inhumains et dégradants ;<br/>
              - la carence de l'Etat porte une atteinte grave et manifestement illégale à ces libertés fondamentales dès lors que les graves problèmes médicaux nécessitant un suivi régulier dont souffrent Mme B... et son fils ajoutés à la détresse sociale qui découle de l'absence de solution d'hébergement caractérisent des circonstances exceptionnelles justifiant qu'ils bénéficient d'un dispositif d'hébergement d'urgence.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure qu'il a diligentée.<br/>
<br/>
              2. Mme D... B... et M. A... C..., de nationalité kosovare, ont rejoint la France, où deux enfants leur sont nés, et ont vu leur demande d'asile rejetée par l'Office français de protection des réfugiés et apatrides (OFPRA) et leur recours contre cette décision par la Cour nationale du droit d'asile (CNDA). Leur demande de titre de séjour a également été rejetée. Logés au titre du dispositif d'hébergement d'urgence, pourtant particulièrement saturé en dépit d'efforts significatifs de renforcement des capacités dans le département du Morbihan où ils résident, ils ont été avisés au terme du rejet de leurs recours successifs que l'obligation de quitter le territoire français qui leur a été signifiée les privait du droit à continuer à bénéficier de cet hébergement. Ils ont en conséquence demandé au juge des référés du tribunal administratif de Rennes, sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner au préfet du Morbihan de procéder à leur hébergement. Par une ordonnance du 2 juin 2021, le juge des référés du tribunal administratif, après avoir admis Mme B... au bénéfice de l'aide juridictionnelle provisoire, a rejeté leurs conclusions. Après avoir rappelé qu'au regard de leur situation qu'il a rappelée en détails, seules des circonstances exceptionnelles pouvaient conduire à regarder le refus de poursuivre leur hébergement comme une atteinte à une liberté fondamentale, il a relevé que n'ayant pas cherché à bénéficier des aides au retour, les intéressés ne faisaient valoir aucune circonstance particulière qui aurait pu faire regarder la terminaison de leur hébergement comme une atteinte à une liberté fondamentale au sens de l'article L. 521-2. <br/>
<br/>
              Sur la demande d'admission au bénéfice provisoire de l'aide juridictionnelle :<br/>
<br/>
              3. Aux termes de l'article 20 de la loi du 10 juillet 1991 relative à l'aide juridique : " Dans les cas d'urgence [...], l'admission provisoire à l'aide juridictionnelle peut être prononcée soit par le président du bureau ou de la section compétente du bureau d'aide juridictionnelle, soit par la juridiction compétente ou son président. "<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de prononcer l'admission provisoire de Mme D... B... et M. A... C... au bénéfice de l'aide juridictionnelle.<br/>
<br/>
              Sur les autres conclusions d'appel :<br/>
<br/>
              5. Au soutien de leur appel, Mme D... B... et M. A... C... se bornent à réitérer leurs arguments de première instance et font valoir à nouveau que l'urgence serait ainsi établie et que les circonstances exceptionnelles résulteraient du seul fait que l'état de santé de Mme B... et d'un de ses enfants serait grave, alors que le premier juge a relevé le collège des médecins de l'OFII a estimé l'état médical de l'intéressée comme compatible avec son voyage de retour vers son pays d'origine, tandis que les troubles visuels de son enfant ne s'opposaient pas à un tel voyage. Ces circonstances ont été analysées par l'ordonnance attaquée, et écartées, faute de précisions, qui ne sont du reste pas apportées non plus en appel, pour exclure que cette situation put être constitutive de telles circonstances exceptionnelles. Il est manifeste que ces arguments ne sont pas de nature à remettre en cause l'ordonnance attaquée ni à établir une atteinte manifestement illégale à une liberté fondamentale. Les conclusions de la requête ne peuvent donc qu'être rejetées, y compris en tant qu'elles demandent que l'Etat soit condamné à verser à Mme D... B... et M. A... C... une somme d'argent sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, qui, l'Etat n'étant pas la partie perdante, y font obstacle.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Mme D... B... et M. A... C... sont admis au bénéfice provisoire de l'aide juridictionnelle.<br/>
Article 2 : Le surplus des conclusions de la requête de Mme D... B... et M. A... C... est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme D... B..., première requérante dénommée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
