<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076973</ID>
<ANCIEN_ID>JG_L_2016_02_000000388173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076973.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 17/02/2016, 388173, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:388173.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 février, 20 mai et 28 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des professions de l'automobile (CNPA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 29 décembre 2014 relatif aux modalités d'information de l'assuré au moment du sinistre sur la faculté de choisir le réparateur professionnel auquel il souhaite recourir prévue à l'article L. 211-5-1 du code des assurances ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ;<br/>
              - la loi n° 2014-344 du 17 mars 2014 ;<br/>
              - le décret n° 2014-435 du 29 avril 2014 ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat du Conseil national des professions de l'automobile ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 211-1 du code des assurances : " Toute personne physique ou toute personne morale autre que l'Etat, dont la responsabilité civile peut être engagée en raison de dommages subis par des tiers résultant d'atteintes aux personnes ou aux biens dans la réalisation desquels un véhicule est impliqué, doit, pour faire circuler celui-ci, être couverte par une assurance garantissant cette responsabilité, dans les conditions fixées par décret en Conseil d'Etat. (...) / Les contrats d'assurance couvrant la responsabilité mentionnée au premier alinéa du présent article doivent également couvrir la responsabilité civile de toute personne ayant la garde ou la conduite (...) du véhicule, à l'exception des professionnels de la réparation, de la vente et du contrôle de l'automobile, ainsi que la responsabilité civile des passagers du véhicule (...). Toutefois, en cas de vol d'un véhicule, ces contrats ne couvrent pas la réparation des dommages subis par les auteurs, coauteurs ou complices du vol. / L'assureur est subrogé dans les droits que possède le créancier de l'indemnité contre la personne responsable de l'accident lorsque la garde ou la conduite du véhicule a été obtenue contre le gré du propriétaire. / Ces contrats doivent être souscrits auprès d'une entreprise d'assurance agréée pour pratiquer les opérations d'assurance contre les accidents résultant de l'emploi de véhicules automobiles. (...) " ; qu'aux termes de l'article L. 211-5-1 du même code, créé par l'article 63 de la loi du 17 mars 2014 visée ci-dessus relative à la consommation : " Tout contrat d'assurance souscrit au titre de l'article L. 211-1 mentionne la faculté pour l'assuré, en cas de dommage garanti par le contrat, de choisir le réparateur professionnel auquel il souhaite recourir. Cette information est également délivrée, dans des conditions définies par arrêté, lors de la déclaration du sinistre " ;  <br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article 1er de l'arrêté du 29 décembre 2014 dont le Conseil national des professions de l'automobile (CNPA) demande l'annulation pour excès de pouvoir : " La faculté pour l'assuré, prévue à l'article L. 211-5-1, de choisir le réparateur professionnel auquel il souhaite recourir lui est rappelée de manière claire et objective par tout professionnel, y compris l'assureur, dès la survenance du sinistre, notamment au moyen d'une mention visible et lisible dans le constat européen d'accident. / Si le moyen de communication est oral, un écrit, notamment un message électronique ou un message textuel interpersonnel (SMS) spécifique, confirme dans les plus brefs délais cette information " ; qu'il résulte des termes et de l'objet des dispositions de l'article L. 211-5-1 du code des assurances citées au point 1 que l'obligation d'information définie par celles-ci pèse, au moment de la déclaration de sinistre, sur l'assureur ; que, dès lors, d'une part, en prévoyant qu'une obligation d'information pesait sur " tout professionnel ", d'autre part, en indiquant que cette obligation devait être accomplie dès la survenance du sinistre, les dispositions contestées de l'arrêté attaqué ont méconnu l'article L. 211-5-1 du code des assurances et excédé, par suite, la portée de l'habilitation législative ; qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner l'autre moyen de la requête, que le Conseil national des professions de l'automobile est fondé à demander l'annulation de l'arrêté qu'il attaque ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Conseil national des professions de l'automobile au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 29 décembre 2014 relatif aux modalités d'information de l'assuré au moment du sinistre sur la faculté de choisir le réparateur professionnel auquel il souhaite recourir prévue à l'article L. 211-5-1 du code des assurances est annulé.<br/>
<br/>
Article 2 : L'Etat versera au Conseil national des professions de l'automobile une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée au Conseil national des professions de l'automobile, au ministre des finances et des comptes publics, au ministre de l'économie, de l'industrie et du numérique et à la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
