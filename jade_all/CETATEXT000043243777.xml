<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243777</ID>
<ANCIEN_ID>JG_L_2021_03_000000433584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 10/03/2021, 433584, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; BALAT</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433584.20210310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme I... F..., Mme A... H..., Mme B... E..., M. D... J... et M. G... C... ont demandé au tribunal administratif de Rouen d'annuler l'arrêté du préfet de l'Eure du 17 décembre 2015 portant création de la commune nouvelle du Val d'Hazey. Par un jugement n° 1600486 du 17 octobre 2017, le tribunal administratif de Rouen a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 17DA02305 du 27 juin 2019, la cour administrative d'appel de Douai a, sur appel de Mme F..., de Mme H..., de Mme E..., de M. J... et de M. C..., annulé ce jugement ainsi que l'arrêté du préfet de l'Eure du 17 décembre 2015.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 13 août 2019 et 24 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel Mme F..., de Mme H..., de Mme E..., de M. J... et de M. C....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Balat, avocat de M. C..., de M. J..., de Mme E..., de Mme H... et de Mme F... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par trois délibérations concordantes des 2, 3 et 4 décembre 2015, les conseils municipaux des communes de Vieux-Villez, Aubevoye et Sainte-Barbe-sur-Gaillon ont demandé la création d'une commune nouvelle résultant de la fusion des trois communes. Par arrêté du 17 décembre 2015, le préfet de l'Eure a procédé à la création de cette commune nouvelle, dénommée Le Val d'Hazey, à compter du 1er janvier 2016. La ministre de la cohésion des territoires et des relations avec les collectivités territoriales se pourvoit en cassation contre l'arrêt du 21 juin 2019 par lequel la cour administrative d'appel de Douai, sur appel de Mme F..., de Mme H..., de Mme E..., de M. J... et de M. C..., conseillers municipaux de la commune de Vieux-Villez, a prononcé l'annulation de cet arrêté.<br/>
<br/>
              Sur les conclusions présentées par la commune du Val d'Hazey et les moyens soulevés à leur appui :<br/>
<br/>
              2. La commune du Val d'Hazey, qui était partie dans l'instance ouverte devant la cour administrative d'appel, avait qualité pour se pourvoir en cassation contre l'arrêt rendu au terme de cette instance. Le mémoire produit par cette commune après l'expiration du délai pour se pourvoir en cassation, à la suite de la communication du pourvoi de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales qui lui a été faite par le Conseil d'Etat, doit être regardé comme de simples observations, par lesquelles la commune ne peut soulever ni conclusions, ni moyens propres.<br/>
<br/>
              Sur les conclusions du pourvoi :<br/>
<br/>
              3. En premier lieu, aux termes de l'article 33 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable au litige : " Les comités techniques sont consultés pour avis sur les questions relatives : / 1° A l'organisation et au fonctionnement des services ; / 2° Aux évolutions des administrations ayant un impact sur les personnels ; / 3° Aux grandes orientations relatives aux effectifs, emplois et compétences (...) ". Aux termes de l'article L. 2113-2 du code général des collectivités territoriales, dans sa rédaction applicable au litige : " Une commune nouvelle peut être créée en lieu et place de communes contiguës (...) à la demande de tous les conseils municipaux ". L'article L. 2113-10 du même code précise que la commune nouvelle a seule la qualité de collectivité territoriale.<br/>
<br/>
              4. La consultation du comité technique dans les conditions prévues à l'article 33 de la loi du 26 janvier 1984 a pour objet, en associant les personnels à l'organisation et au fonctionnement du service, d'éclairer les organes compétents de la collectivité auprès desquels est institué le comité technique. Un projet de création d'une commune nouvelle en application des dispositions de l'article L. 2113-2 du code général des collectivités territoriales soulève des questions relatives à l'organisation et au fonctionnement des services de chacune des communes concernées. Par suite, et alors que les dispositions de l'article L. 2113-2 ne peuvent être interprétées, contrairement à ce que soutient la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, comme ayant implicitement écarté l'application de l'article 33 de la loi du 26 janvier 1984, la consultation du comité technique compétent doit intervenir avant que le conseil municipal ne prenne parti, en application de ces dispositions, sur un tel projet. En statuant ainsi, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En second lieu, si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou s'il a privé les intéressés d'une garantie.<br/>
<br/>
              6. La cour administrative d'appel, après avoir constaté que le comité technique de la commune de Vieux-Villez n'avait pas été consulté préalablement à la délibération par laquelle le conseil municipal de cette commune a demandé la création de la commune nouvelle, a relevé que cette consultation constituait, pour le personnel de cette commune, une garantie. Elle n'a, dès lors, pas commis d'erreur de droit faute de rechercher si les personnes intéressées avaient été privées d'une garantie. En retenant qu'aucune circonstance particulière ne permettait en l'espèce de remédier au défaut de consultation préalable du comité technique, notamment pas la circonstance que le comité technique de la commune nouvelle a été consulté à plusieurs reprises depuis sa création, la cour s'est livrée à une appréciation souveraine des faits de l'espèce exempte de dénaturation.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales doit être rejeté. <br/>
<br/>
              Sur les conclusions présentées par les défendeurs au pourvoi au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 1 500 euros à verser à Mme F..., à Mme H..., à Mme E..., à M. J... et à M. C....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales est rejeté.<br/>
Article 2 : Les conclusions présentées par la commune du Val d'Hazey sont rejetées.<br/>
Article 3 : L'Etat versera une somme globale de 1 500 euros à Mme F..., à Mme H..., à Mme E..., à M. J... et à M. C....<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à Mme I... F... pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à la commune du Val d'Hazey.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
