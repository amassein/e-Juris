<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254075</ID>
<ANCIEN_ID>JG_L_2018_07_000000421638</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 421638, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421638</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:421638.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...C...et M. B...D...ont, à l'appui de leur appel du jugement du 26 mai 2016 par lequel le tribunal administratif de Paris a rejeté leur protestation contre les élections des représentants du personnel relevant du collège B, siégeant au sein des conseils centraux de l'université Pierre et Marie Curie - Paris IV, qui se sont tenues le 16 février 2016, ont produit deux mémoires, enregistrés 2 mars et 31 octobre 2017 au greffe de la cour administrative d'appel de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel ils soulèvent une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un arrêt n° 16PA02503 du 12 juin 2018, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Paris, avant qu'il soit statué sur la demande de Mme C...et M.D..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 1° du I de l'article L.712-3 et des deux premiers alinéas de l'article L. 719-2 du code de l'éducation.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise, Mme C...et M. D... soutiennent que ces dispositions, applicables au litige, méconnaissent le principe constitutionnel d'indépendance des maîtres de conférence ainsi que son corollaire, le droit de ces derniers à une représentation propre et authentique, et que les dispositions de l'article L. 719-2 du code de l'éducation sont, en outre, entachées d'incompétence négative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 2 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Sorbonne Université, venant aux droits de l'université Pierre et Marie Curie - Paris IV, soutient qu'il n'y a pas lieu de statuer sur la question prioritaire de constitutionnalité transmise, dès lors que la requête au soutien de laquelle cette question a été soulevée a perdu tout objet, et, subsidiairement, que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, en particulier que la question ne présente pas un caractère sérieux.<br/>
<br/>
              Par un mémoire en défense, enregistré le 9 juillet 2018, la ministre de l'enseignement supérieur, de la recherche et de l'innovation soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, en particulier que la question ne présente pas un caractère sérieux.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation, notamment ses articles L. 712-3 et L. 719-2 ; <br/>
              - la décision n° 83-165 DC du 20 janvier 1984 du Conseil constitutionnel ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par un jugement du 26 mai 2016, le tribunal administratif de Paris a rejeté la protestation que Mme A...C...et M. B...D...avaient formée contre les élections des représentants du personnel relevant du collège B, siégeant au sein des conseils centraux de l'université Pierre et Marie Curie - Paris IV, qui se sont tenues le 16 février 2016. Les intéressés ont soulevé, à l'appui de l'appel qu'ils ont formé contre ce jugement devant la cour administrative d'appel de Paris, en application de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, le moyen tiré de ce que les dispositions du 1° du I de l'article L.712-3 et des deux premiers alinéas de l'article L.719-2 du code de l'éducation méconnaîtraient les droits et libertés garantis par la Constitution. La cour administrative d'appel de Paris a, par un arrêt du 12 juin 2018, transmis au Conseil d'Etat la question prioritaire de constitutionnalité soulevée.<br/>
<br/>
              2. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil d'Etat se prononce sur le renvoi de la question au Conseil constitutionnel dans un délai de trois mois, sans qu'il lui appartienne, dans ce cadre, de statuer sur la régularité de la décision juridictionnelle qui lui a transmis la question. Il en résulte que Sorbonne Université ne peut utilement soutenir, à l'appui de ses conclusions tendant au non-renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Paris, que celle-ci aurait à tort écarté une exception de non-lieu à statuer soulevée devant elle.<br/>
<br/>
              3. En vertu de l'article 23-4, le Conseil constitutionnel est saisi de la question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              Sur l'article L. 712-3 du code de l'éducation :<br/>
<br/>
               4. D'une part, aux termes de l'article L. 712-3 du code de l'éducation : " I.-Le conseil d'administration comprend de vingt-quatre à trente-six membres ainsi répartis : 1° De huit à seize représentants des enseignants-chercheurs et des personnels assimilés, des enseignants et des chercheurs, en exercice dans l'établissement, dont la moitié de professeurs des universités et personnels assimilés ; ... ". <br/>
<br/>
              5. D'autre part, aux termes de l'article L. 711-4 du même code : " I.-Les établissements publics à caractère scientifique, culturel et professionnel sont créés par décret après avis du Conseil national de l'enseignement supérieur et de la recherche. (...) ". L'article L. 711-7 de ce code dispose : " Les établissements déterminent, par délibérations statutaires du conseil d'administration prises à la majorité absolue des membres en exercice, leurs statuts et leurs structures internes, conformément aux dispositions du présent code et des décrets pris pour son application. (...) ".<br/>
<br/>
              6. Il résulte de ces dispositions que si le législateur a fixé le nombre minimal et le nombre maximal de membres du conseil d'administration des universités et, au sein de ce conseil, le nombre minimal et le nombre maximal des représentants des enseignants-chercheurs et personnels assimilés, enseignants et chercheurs, et qu'il a précisé que la moitié de ces représentants devaient être des professeurs des universités et personnels assimilés, il a renvoyé au pouvoir réglementaire le soin de déterminer la composition des conseils d'administration et notamment de fixer les conditions de représentation, au sein de ce conseil, des enseignants-chercheurs autres que les professeurs d'université, le pouvoir réglementaire étant tenu, dans l'exercice de cette compétence, de respecter le principe à valeur constitutionnelle d'indépendance des professeurs et des enseignants-chercheurs ayant une autre qualité, lequel suppose, pour chacun de ces deux ensembles, une représentation propre et authentique dans les conseils de la communauté universitaire. Par suite, les requérants, qui ne contestent pas la conformité à la Constitution des dispositions par lesquelles le législateur a confié cette compétence au pouvoir règlementaire, ne sont pas fondé à soutenir que l'article L. 712-3 du code de l'éducation méconnaîtrait, par lui-même, l'exigence de représentation propre et authentique dans les conseils de la communauté universitaire des enseignants-chercheurs autres que les professeurs d'université. Par suite, la question prioritaire de constitutionnalité ne revêt pas, s'agissant des dispositions du 1° du I de l'article L.712-3 du code de l'éducation, un caractère sérieux.<br/>
<br/>
<br/>
              Sur l'article L. 719-2 du code de l'éducation :<br/>
<br/>
              7. Aux termes de l'article L. 719-2 du code de l'éducation, issues de l'article 39 de la loi du 26 janvier 1984 sur l'enseignement supérieur : " Un décret fixe les conditions d'exercice du droit de suffrage, la composition des collèges électoraux et les modalités d'assimilation et d'équivalence de niveau pour la représentation des personnels et des étudiants aux conseils ainsi que les modalités de recours contre les élections. Il précise dans quelles conditions sont représentés, directement ou indirectement, les personnels non titulaires qui ne seraient pas assimilés aux titulaires et les usagers qui ne seraient pas assimilés aux étudiants. / Au sein de la représentation des enseignants-chercheurs et personnels assimilés de chaque conseil, le nombre des professeurs et personnels de niveau équivalent doit être égal à celui des autres personnels ".<br/>
<br/>
              8. Par une décision n° 83-165 DC du 20 janvier 1984, le Conseil constitutionnel a, dans les motifs et le dispositif de cette décision, déclaré l'article 39 de la loi du 26 janvier 1984 conforme à la Constitution. Il en résulte que, s'agissant de ces dispositions, les conditions auxquelles l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel subordonne le renvoi de la question prioritaire de constitutionalité ne sont pas remplies.<br/>
<br/>
               9. Il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Paris.<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée à l'encontre du 1° du I de l'article L. 712-3 et des deux premiers alinéas de l'article L. 719-2 du code de l'éducation. <br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...C..., à M. B...D..., à Sorbonne Université, à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre ainsi qu'à la cour administrative d'appel de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
