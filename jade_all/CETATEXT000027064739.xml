<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064739</ID>
<ANCIEN_ID>JG_L_2013_02_000000350962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064739.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 13/02/2013, 350962, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350962.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi et le mémoire complémentaire, enregistrés les 15 juillet et 17 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C... B..., demeurant..., ; il demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY01548 du 12 mai 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0801143 du 30 mars 2010 du tribunal administratif de Dijon rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des revenus fonciers perçus en 2004, d'autre part, à la décharge de ces impositions et des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur, <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. C... B...,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de M. C... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumises aux juges du fond que M. et Mme A...B...et leurs enfantsC..., Christophe et Isabelle, associés de la société civile immobilière (SCI) du Chateloy, ont été assujettis à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre de l'année 2004 dans la catégorie des revenus fonciers, à raison du retour gratuit des améliorations apportées à l'ensemble immobilier situé à Levernois (Côte-d'Or) que la société civile immobilière a cédé, le 1er mai 2004, à la société Hostellerie de Levernois, qui en était auparavant le locataire ; que M. C... B...se pourvoit en cassation contre l'arrêt du 12 mai 2011 de la cour administrative d'appel de Lyon qui a rejeté son appel formé contre le jugement du tribunal administratif de Dijon du 30 mars 2010 rejetant sa demande tendant à la décharge de ces impositions et pénalités ; <br/>
<br/>
              2. Considérant, en premier lieu, que, devant la cour, le requérant a fait valoir que les propositions de rectification étaient insuffisamment motivées aux motifs que l'administration faisait référence à la valeur nette comptable des constructions et aménagements réalisés par le locataire sans préciser leur valeur réelle et que l'absence de liste précise de ces éléments ne lui donnait pas les moyens de vérifier si ces derniers avaient été réalisés sur la propriété de la société civile immobilière et si certains d'entre eux ne constituaient pas des biens meubles exclus d'un retour gratuit au bailleur ; que la cour n'a pas commis d'erreur de droit en relevant par une appréciation souveraine non arguée de dénaturation, d'une part, que l'administration n'était pas tenue de donner dans la proposition de rectification des éléments de comparaison dès lors qu'elle avait retenu la valeur nette comptable des biens, et, d'autre part, que les informations fournies, qui indiquaient le rattachement des six types de travaux à l'hôtel, au restaurant ou au terrain et qui mentionnaient les évaluations retenues, étaient suffisamment précises pour permettre aux contribuables d'identifier les éléments concernés ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que, contrairement à ce que soutient le requérant, la cour, qui n'était d'ailleurs pas saisie d'un tel moyen, n'a pas jugé que l'administration pouvait se référer exclusivement à la valeur nette comptable des constructions inscrites au bilan de la société Hostellerie de Levernois pour évaluer la valeur de l'avantage résultant de leur retour gratuit au bailleur mais s'est bornée, ainsi qu'il a été dit au point 2, à répondre au moyen relatif à la régularité de la procédure d'imposition, tiré de ce que la proposition de rectification aurait dû mentionner non seulement les valeurs nettes comptables mais aussi des termes de comparaison de nature à établir que ces valeurs étaient égales à la valeur vénale des biens ; que, par suite, le moyen tiré de l'erreur de droit qu'aurait commise la cour en admettant la méthode d'évaluation retenue par l'administration pour évaluer les biens faisant retour à la société civile immobilière ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, que la cour a expressément écarté le moyen tiré de ce que M. D..., signataire d'un compromis de vente le 3 avril 2004 avec la société civile immobilière, aurait été le véritable redevable des revenus fonciers après levée des clauses suspensives en se fondant sur les circonstances que le contrat de vente du 1er mai 2004 avait été signé entre la SCI du Chateloy et la société Hostellerie de Levernois et qu'aucune justification de la levée de ces clauses n'était apportée ; que, par suite, l'arrêt est suffisamment motivé sur ce point ;<br/>
<br/>
              5. Considérant, enfin, qu'aux termes du premier alinéa de l'article 29 du code général des impôts : " Sous réserve des dispositions des articles 33 ter et 33 quater, le revenu brut des immeubles ou parties d'immeubles donnés en location, est constitué par le montant des recettes brutes perçues par le propriétaire, augmenté du montant des dépenses incombant normalement à ce dernier et mises par les conventions à la charge des locataires et diminué du montant des dépenses supportées par le propriétaire pour le compte des locataires (...) " ; que lorsqu'un contrat de bail prévoit, en faveur du bailleur, la remise gratuite en fin de bail des aménagements ou constructions réalisés par le preneur, la valeur de cet avantage constitue, pour le bailleur, un complément de loyer ayant le caractère d'un revenu foncier imposable au titre de l'année au cours de laquelle le bail arrive à expiration ou fait l'objet avant l'arrivée du terme d'une résiliation ;<br/>
<br/>
              6. Considérant qu'après avoir relevé que, par acte du 2 juin 1988, la SCI du Chateloy avait consenti à la société Hostellerie de Lavernois, un bail commercial portant sur un ensemble immobilier à usage d'hôtel restaurant prévoyant que le bailleur, en fin de bail, conserverait sans indemnité " tous embellissements, améliorations et installations faits par le preneur ", la cour a pu, sans commettre d'erreur de droit ni méconnaître le principe de l'annualité de l'impôt, estimer que la vente de cet ensemble immobilier au même preneur, par acte du 1er mai 2004, avait des effets identiques à une résiliation amiable du bail et faisait naître au profit du bailleur un complément de loyer, imposable au titre de l'année de cession, correspondant à la valeur des constructions et aménagements édifiés par le preneur lui revenant gratuitement ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que, par suite, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
