<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023248145</ID>
<ANCIEN_ID>JG_L_2010_12_000000332363</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/24/81/CETATEXT000023248145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 15/12/2010, 332363</TITRE>
<DATE_DEC>2010-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332363</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>Mme Constance  Rivière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Bourgeois-Machureau Béatrice</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:332363.20101215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés le 28 septembre 2009 et le 13 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés par l'ASSOCIATION NATIONALE D'ASSISTANCE AUX FRONTIERES POUR LES ETRANGERS (ANAFÉ), dont le siège est 21 ter, rue Voltaire à Paris (75011) ; l'ASSOCIATION NATIONALE D'ASSISTANCE AUX FRONTIERES POUR LES ETRANGERS demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la circulaire du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire, en date du 21 septembre 2009, relative aux conditions d'entrée dans l'espace Schengen des ressortissants d'Etats tiers détenteurs d'autorisations provisoires de séjour et de récépissés de demande de titre de séjour délivrés par les autorités françaises, en tant qu'elle prescrit de refuser le retour des titulaires de récépissés de première demande de titre de séjour et de demande d'asile ;<br/>
<br/>
              2°) d'ordonner la production de la note DCPAF/SDAITS/BCRT n° 07-859 du 16 janvier 2007 et du procès-verbal ou compte-rendu du groupe "frontières" du 7 mars 2007 réalisé par le service juridique du Conseil de l'Union européenne ; <br/>
<br/>
              3°) à titre subsidiaire, de saisir, avant-dire droit, la Cour de justice de l'union Européenne d'une question préjudicielle tendant à savoir si les dispositions du " code frontières Schengen " sont applicables aux séjours de plus de trois mois d'étrangers déjà autorisés au séjour sur le territoire d'un Etat membre ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu le pacte international relatif aux droits civils et politiques ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la convention d'application de l'accord de Schengen, du 19 juin 1990 ;<br/>
<br/>
              Vu le règlement (CE) n° 1030/2002 du Conseil du 13 juin 2002 ;<br/>
<br/>
              Vu le règlement (CE) n° 562/2006 du Parlement européen et du Conseil, du 15 mars 2006 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Constance Rivière, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir opposée par  le ministre de l'intérieur, de l'outre-mer et des collectivités territoriales et par le ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire :<br/>
<br/>
              Considérant que l'association requérante a produit, dans le délai de recours contentieux, un mémoire contenant l'exposé des faits et des moyens soulevés à l'appui de son recours, conformément à l'article R. 411-1 du code de justice administrative ; qu'ainsi, la fin de non-recevoir opposée par les ministres et tirée de ce que la requête ne serait pas motivée doit être écartée ;<br/>
<br/>
              Sur la légalité de la circulaire : <br/>
<br/>
              En ce qui concerne le moyen tiré de l'incompétence du signataire de la circulaire attaquée :<br/>
<br/>
              Considérant que la circulaire attaquée a été signée par M. Decharrière, directeur du cabinet du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire ; que M. Decharrière a reçu délégation du ministre pour signer notamment les circulaires telles que celle qui est attaquée en vertu d'un arrêté du 20 janvier 2009, publié au Journal officiel de la République française le 30 janvier 2009  ; que, par suite, le moyen tiré de l'incompétence du signataire de la circulaire en date du 21 septembre 2009 ne peut qu'être écarté ; <br/>
<br/>
              En ce qui concerne les autres moyens de la requête :<br/>
<br/>
              Considérant que ni la liberté d'aller et de venir, ni le droit d'asile ni aucun principe général du droit communautaire ne confère aux étrangers titulaires d'une autorisation provisoire de séjour qui ont quitté le territoire national le droit d'y revenir, même s'ils y séjournent régulièrement ;<br/>
<br/>
              Considérant qu'aux termes de l'article 5 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) : " 1. Pour un séjour n'excédant pas trois mois sur une période de six mois, les conditions d'entrée pour les ressortissants de pays tiers sont les suivantes :/ a) être en possession d'un document ou de documents de voyage en cours de validité permettant le franchissement de la frontière ;/ b) être en possession d'un visa en cours de validité si celui-ci est requis en vertu du règlement (CE) n° 539/2001 du Conseil du 15 mars 2001 fixant la liste des pays tiers dont les ressortissants sont soumis à l'obligation de visa pour franchir les frontières extérieures des États membres et la liste de ceux dont les ressortissants sont exemptés de cette obligation, sauf s'ils sont titulaires d'un titre de séjour en cours de validité ;/ c) justifier l'objet et les conditions du séjour envisagé, et disposer des moyens de subsistance suffisants, tant pour la durée du séjour envisagé que pour le retour dans le pays d'origine ou le transit vers un pays tiers dans lequel leur admission est garantie, ou être en mesure d'acquérir légalement ces moyens ;/ d) ne pas être signalé aux fins de non-admission dans le SIS ;/ e) ne pas être considéré comme constituant une menace pour l'ordre public, la sécurité intérieure, la santé publique ou les relations internationales de l'un des États membres et, en particulier, ne pas avoir fait l'objet d'un signalement aux fins de non-admission dans les bases de données nationales des États membres pour ces mêmes motifs (...)/ 4. Par dérogation au paragraphe 1,/ a) les ressortissants de pays tiers qui ne remplissent pas toutes les conditions visées au paragraphe 1, mais qui sont titulaires d'un titre de séjour ou d'un visa de retour délivré par l'un des États membres ou, lorsque cela est requis, de ces deux documents, se voient autorisés à entrer aux fins de transit sur le territoire des autres États membres afin de pouvoir atteindre le territoire de l'État membre qui a délivré le titre de séjour ou le visa de retour, sauf s'ils figurent sur la liste nationale de signalements de l'État membre aux frontières extérieures duquel ils se présentent et si ce signalement est assorti d'instructions quant à l'interdiction d'entrée ou de transit (...) " ; qu'aux termes de l'article 2 (" Définitions ") du même règlement : " Aux fins du présent règlement on entend par : (...) 15) "titre de séjour":/ a) tous les titres de séjour délivrés par les États membres selon le format uniforme prévu par le règlement (CE) no 1030/2002 du Conseil du 13 juin 2002 établissant un modèle uniforme de titre de séjour pour les ressortissants de pays tiers ;/ b) tous les autres documents délivrés par un État membre aux ressortissants de pays tiers et leur autorisant le séjour ou le retour sur son territoire, à l'exception des titres temporaires délivrés au cours de l'examen d'une première demande de titre de séjour tel que visé au point a) ou au cours de l'examen d'une demande d'asile " ; qu'enfin, aux termes de l'article 13 de ce même règlement : " 1. L'entrée sur le territoire des États membres est refusée au ressortissant de pays tiers qui ne remplit pas l'ensemble des conditions d'entrée, telles qu'énoncées à l'article 5, paragraphe 1, et qui n'appartient pas à l'une des catégories de personnes visées à l'article 5, paragraphe 4. Cette disposition est sans préjudice de l'application des dispositions particulières relatives au droit d'asile et à la protection internationale ou à la délivrance de visas de long séjour " ; <br/>
<br/>
              Considérant qu'aux termes de l'article L. 311-4 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La détention d'un récépissé d'une demande de délivrance ou de renouvellement d'un titre de séjour, d'un récépissé d'une demande d'asile ou d'une autorisation provisoire de séjour autorise la présence de l'étranger en France, sans préjuger de la décision définitive qui sera prise au regard de son droit au séjour (...) " ; qu'un récépissé ou une autorisation provisoire de séjour, quel qu'il soit, qui n'est délivré qu'après dépôt d'un dossier complet, est souvent renouvelé plusieurs fois avant que n'intervienne une décision définitive relative au droit au séjour ; que ces dispositions de l'article L. 311-4 précité ont été comprises en ce sens qu'un étranger titulaire d'un titre l'autorisant à séjourner en France, fût-ce provisoirement, peut quitter le territoire national et y revenir, tant que ce titre n'est pas expiré, et sans avoir à solliciter de visa ; <br/>
<br/>
              Considérant que la circulaire attaquée, relative aux " conditions d'entrée dans l'espace Schengen des ressortissants d'Etats tiers détenteurs d'autorisations provisoires de séjour (APS) et de récépissés de demande de titre de séjour délivrés par les autorités françaises ", retient, toutefois, qu'en vertu des dispositions du code frontières Schengen, notamment celles de son article 2, point 15, sous b), les autorisations provisoires de séjour délivrées dans le cadre de l'examen d'une demande d'asile ainsi que les récépissés de première demande de titre de séjour ne permettent désormais plus à leur titulaire de revenir librement dans l'espace Schengen  ; que cette circulaire en déduit que les ressortissants de pays tiers soumis à visa, qui auraient quitté le territoire français munis de ces seuls autorisations ou récépissés, ne peuvent revenir dans l'espace Schengen que munis d'un visa de retour délivré par les autorités consulaires ou, à titre exceptionnel, par les autorités préfectorales, ce visa de retour n'autorisant le franchissement des frontières extérieures à l'espace Schengen que par un point d'entrée du territoire français ;<br/>
<br/>
              Considérant que l'association requérante soutient que la circulaire ne se borne pas à tirer les conséquences du code frontières Schengen mais ajoute à ses dispositions, les détenteurs d'autorisations provisoires de séjour ou de récépissés de demandes de titre de séjour se trouvant hors du champ d'application de ce code dès lors qu'ils ont vocation à résider plus de trois mois durant une même période de six mois sur le territoire de l'Etat membre qui leur a délivré cette autorisation ou ce récépissé ; qu'en outre, la circulaire méconnaîtrait le principe de sécurité juridique et le principe de confiance légitime dans la mesure où elle est immédiatement applicable et où elle prive les étrangers qui ont quitté le territoire du droit de revenir en France sans avoir à solliciter de visa, comme ils pouvaient légitimement s'y attendre en vertu des circulaires antérieurement applicables ; <br/>
<br/>
              Considérant qu'en vertu de l'article 5, paragraphe 1, du règlement du 15 mars 2006, les ressortissants de pays tiers titulaires d'un visa, lorsque celui-ci est requis, ou d'un titre de séjour en cours de validité ont le droit d'entrer sur le territoire des Etats membres de l'Union européenne pour y effectuer un séjour d'une durée n'excédant pas trois mois sur une même période de six mois, lorsqu'ils remplissent les autres conditions fixées par ce paragraphe ; que, s'ils ne remplissent pas ces conditions, les ressortissants de pays tiers titulaires d'un titre de séjour ou d'un " visa de retour " sont autorisés en vertu du paragraphe 4, sous a), du même article, à entrer uniquement aux fins de transit sur le territoire des autres Etats membres afin de gagner le territoire de l'Etat membre qui a délivré le titre de séjour ou le " visa de retour " ; que le b) du point 15 de l'article 2 du règlement, relatif aux " définitions ", exclut des " titres de séjour " visés par le règlement, les " titres temporaires délivrés au cours de l'examen d'une première demande de titre de séjour tel que visé au point a) ou au cours d'une demande d'asile " ; qu'en vertu de l'article 13, paragraphe 1, du règlement, les Etats membres sont tenus de refuser l'entrée aux ressortissants de pays tiers lorsqu'ils ne remplissent pas les conditions mentionnées à l'article 5 ; <br/>
<br/>
              Considérant toutefois que, dans la mesure où les conditions figurant à l'article 5 ne sont expressément prévues que pour l'entrée sur l'ensemble du territoire des Etats membres, et non pour le retour sur le seul territoire de l'Etat membre ayant délivré l'autorisation provisoire de séjour, en vue d'un séjour n'excédant pas trois mois, où le b) du point 15 de l'article 2 ne mentionne que les titres temporaires de séjour délivrés à l'occasion d'une première demande de titre de séjour selon le modèle uniforme ou à l'occasion d'une demande d'asile, où les raisons de l'exclusion de ces deux titres en particulier ne sont pas précisées et où plusieurs dispositions du règlement, notamment l'article 5, paragraphe 4, sous c), autorisent la délivrance d'autorisations d'entrée limitées au territoire de l'Etat membre qui les octroie, une première question se pose de savoir si l'interdiction prévue à l'article 13 du règlement s'applique également au retour sur le territoire de l'Etat membre qui a délivré le titre temporaire de séjour lorsque le retour sur son territoire ne nécessite ni entrée, ni transit, ni séjour sur le territoire des autres Etats membres ;<br/>
<br/>
              Considérant que, dans l'hypothèse d'une réponse positive à la première question, dès lors que le règlement ne définit pas ce qu'est un " visa de retour ", la question se pose également de savoir dans quelles conditions un tel visa peut être délivré par un Etat membre à un ressortissant de pays tiers, et notamment s'il peut limiter l'entrée aux seuls points du territoire national ; <br/>
<br/>
              Considérant enfin que, dans la mesure où le règlement du 15 mars 2006 exclurait toute possibilité d'entrée sur le territoire des Etats membres aux ressortissants de pays tiers qui ne sont titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile, contrairement à ce que permettaient les stipulations de la convention d'application de l'accord de Schengen du 19 juin 1990, dans sa rédaction antérieure à sa modification par ce règlement, la question se pose de savoir si les principes de sécurité juridique et de confiance légitime n'imposaient pas que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté leur territoire alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006 ; <br/>
<br/>
              Considérant que ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elles présentent une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur la requête de l'ANAFÉ ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur la requête présentée par l'ANAFÉ  jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              1.	L'article 13 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) s'applique-t-il au retour d'un ressortissant de pays tiers sur le territoire d'un Etat membre qui a délivré à ce dernier un titre temporaire de séjour lorsque le retour sur son territoire ne nécessite ni entrée, ni transit, ni séjour sur le territoire des autres Etats membres '<br/>
              2.	Dans quelles conditions un Etat membre peut-il délivrer à des ressortissants de pays tiers un " visa de retour " au sens de l'article 5, paragraphe 4, sous a), du même règlement ' En particulier, un tel visa peut-il limiter l'entrée aux seuls points du territoire national '<br/>
              3.	Dans la mesure où le règlement du 15 mars 2006 exclurait toute possibilité d'entrée sur le territoire des Etats membres aux ressortissants de pays tiers qui ne sont titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile, contrairement à ce que permettaient les stipulations de la convention d'application de l'accord de Schengen, du 19 juin 1990, dans sa rédaction antérieure à sa modification par le règlement, les principes de sécurité juridique et de confiance légitime imposaient-ils que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté leur territoire alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006 '<br/>
Article 2 : La présente décision sera notifiée à l'ASSOCIATION NATIONALE D'ASSISTANCE AUX FRONTIERES POUR LES ETRANGERS (ANAFÉ) et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et à la Cour de justice de l'Union européenne.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03-02-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT COMMUNAUTAIRE PAR LE JUGE ADMINISTRATIF FRANÇAIS. RENVOI PRÉJUDICIEL À LA COUR DE JUSTICE DES COMMUNAUTÉS EUROPÉENNES. - RÈGLEMENT (CE) N° 562/2006 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 15 MARS 2006 (CODE FRONTIÈRES SCHENGEN) - QUESTIONS PRÉSENTANT UNE DIFFICULTÉ SÉRIEUSE JUSTIFIANT UN RENVOI À LA CJUE - 1) INTERDICTION DU RETOUR, SANS ENTRÉE, NI TRANSIT, NI SÉJOUR SUR LE TERRITOIRE DES AUTRES ETATS MEMBRES, D'UN RESSORTISSANT DE PAYS TIERS SUR LE TERRITOIRE D'UN ETAT MEMBRE LUI AYANT DÉLIVRÉ UN TITRE TEMPORAIRE DE SÉJOUR (ART. 13) - 2) CONDITIONS DE DÉLIVRANCE D'UN VISA DE RETOUR - 3) NÉCESSITÉ ÉVENTUELLE DE MESURES TRANSITOIRES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-002 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - RÈGLEMENT (CE) N° 562/2006 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 15 MARS 2006 (CODE FRONTIÈRES SCHENGEN) - QUESTIONS PRÉSENTANT UNE DIFFICULTÉ SÉRIEUSE JUSTIFIANT UN RENVOI À LA CJUE - NÉCESSITÉ ÉVENTUELLE DE MESURES TRANSITOIRES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-045-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - RÈGLEMENT (CE) N° 562/2006 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 15 MARS 2006 (CODE FRONTIÈRES SCHENGEN) - QUESTIONS PRÉSENTANT UNE DIFFICULTÉ SÉRIEUSE JUSTIFIANT UN RENVOI À LA CJUE - 1) INTERDICTION DU RETOUR, SANS ENTRÉE, NI TRANSIT, NI SÉJOUR SUR LE TERRITOIRE DES AUTRES ETATS MEMBRES, D'UN RESSORTISSANT DE PAYS TIERS SUR LE TERRITOIRE D'UN ETAT MEMBRE LUI AYANT DÉLIVRÉ UN TITRE TEMPORAIRE DE SÉJOUR (ART. 13) - 2) CONDITIONS DE DÉLIVRANCE D'UN VISA DE RETOUR - 3) NÉCESSITÉ ÉVENTUELLE DE MESURES TRANSITOIRES.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">335-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. - RÈGLEMENT (CE) N° 562/2006 DU PARLEMENT EUROPÉEN ET DU CONSEIL DU 15 MARS 2006 (CODE FRONTIÈRES SCHENGEN) - QUESTIONS PRÉSENTANT UNE DIFFICULTÉ SÉRIEUSE JUSTIFIANT UN RENVOI À LA CJUE - 1) INTERDICTION DU RETOUR, SANS ENTRÉE, NI TRANSIT, NI SÉJOUR SUR LE TERRITOIRE DES AUTRES ETATS MEMBRES, D'UN RESSORTISSANT DE PAYS TIERS SUR LE TERRITOIRE D'UN ETAT MEMBRE LUI AYANT DÉLIVRÉ UN TITRE TEMPORAIRE DE SÉJOUR (ART. 13) - 2) CONDITIONS DE DÉLIVRANCE D'UN VISA DE RETOUR - 3) NÉCESSITÉ ÉVENTUELLE DE MESURES TRANSITOIRES.
</SCT>
<ANA ID="9A"> 15-03-02-01 Les questions de savoir, 1) si l'article 13 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) interdit le retour d'un ressortissant de pays tiers sur le territoire d'un Etat membre qui a délivré à ce dernier un titre temporaire de séjour lorsque le retour sur son territoire ne nécessite ni entrée, ni transit, ni séjour sur le territoire des autres Etats membres, 2) dans quelles conditions un Etat membre peut délivrer à des ressortissants de pays tiers un « visa de retour » au sens de l'article 5, paragraphe 4, sous a), du même règlement et en particulier,  si un tel visa peut limiter l'entrée aux seuls points du territoire national, et 3) si les principes de sécurité juridique et de confiance légitime imposent que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté le territoire d'un Etat membre alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006, présentent des difficultés sérieuses justifiant un renvoi à titre préjudiciel à la Cour de justice de l'Union européenne (CJUE), en application de l'article 267 du traité sur le fonctionnement de l'Union européenne.</ANA>
<ANA ID="9B"> 15-05-002 La question de savoir si les principes de sécurité juridique et de confiance légitime imposent que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté le territoire d'un Etat membre alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006, présente une difficulté sérieuse justifiant un renvoi à titre préjudiciel à la Cour de justice de l'Union européenne (CJUE), en application de l'article 267 du traité sur le fonctionnement de l'Union européenne.</ANA>
<ANA ID="9C"> 15-05-045-01 Les questions de savoir, 1) si l'article 13 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) interdit le retour d'un ressortissant de pays tiers sur le territoire d'un Etat membre qui a délivré à ce dernier un titre temporaire de séjour lorsque le retour sur son territoire ne nécessite ni entrée, ni transit, ni séjour sur le territoire des autres Etats membres, 2) dans quelles conditions un Etat membre peut délivrer à des ressortissants de pays tiers un « visa de retour » au sens de l'article 5, paragraphe 4, sous a), du même règlement et en particulier,  si un tel visa peut limiter l'entrée aux seuls points du territoire national, et 3) si les principes de sécurité juridique et de confiance légitime imposent que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté le territoire d'un Etat membre alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006, présentent des difficultés sérieuses justifiant un renvoi à titre préjudiciel à la Cour de justice de l'Union européenne (CJUE), en application de l'article 267 du traité sur le fonctionnement de l'Union européenne.</ANA>
<ANA ID="9D"> 335-01-02 Les questions de savoir, 1) si l'article 13 du règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes (code frontières Schengen) interdit le retour d'un ressortissant de pays tiers sur le territoire d'un Etat membre qui a délivré à ce dernier un titre temporaire de séjour lorsque le retour sur son territoire ne nécessite ni entrée, ni transit, ni séjour sur le territoire des autres Etats membres, 2) dans quelles conditions un Etat membre peut délivrer à des ressortissants de pays tiers un « visa de retour » au sens de l'article 5, paragraphe 4, sous a), du même règlement et en particulier,  si un tel visa peut limiter l'entrée aux seuls points du territoire national, et 3) si les principes de sécurité juridique et de confiance légitime imposent que soient prévues des mesures transitoires pour les ressortissants de pays tiers ayant quitté le territoire d'un Etat membre alors qu'ils n'étaient titulaires que d'un titre temporaire de séjour délivré au cours de l'examen d'une première demande de titre de séjour ou d'une demande d'asile et souhaitant y revenir après l'entrée en vigueur du règlement du 15 mars 2006, présentent des difficultés sérieuses justifiant un renvoi à titre préjudiciel à la Cour de justice de l'Union européenne (CJUE), en application de l'article 267 du traité sur le fonctionnement de l'Union européenne.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
