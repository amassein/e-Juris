<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035841786</ID>
<ANCIEN_ID>JG_L_2017_10_000000412262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/84/17/CETATEXT000035841786.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 20/10/2017, 412262, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:412262.20171020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par deux mémoires, enregistrés les 28 juillet et 26 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Lafonta santé demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir du décret n° 2017-809 du 5 mai 2017 relatif aux dispositifs médicaux remboursables utilisés dans le cadre de certains traitements d'affections chroniques, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 165-1-3 du code de la sécurité sociale.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'article L. 165-1-3 du code de la sécurité sociale ;<br/>
              - la loi n° 2016-1827 du 23 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Lafonta santé.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 octobre 2017, présentée pour la société Lafonta santé ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article L. 165-1-3 inséré dans le code de la sécurité sociale par la loi du 23 décembre 2016 de financement de la sécurité sociale pour 2017 prévoit que : " Dans le cadre de la mise en oeuvre de certains traitements d'affections chroniques, dont la liste est fixée par arrêté des ministres chargés de la santé et de la sécurité sociale après avis de la Haute Autorité de santé, les prestataires mentionnés à l'article L. 5232-3 du code de la santé publique peuvent recueillir, avec l'accord du patient, les données issues d'un dispositif médical inscrit sur la liste prévue à l'article L. 165-1 du présent code qu'ils ont mis à la disposition du patient et qui est nécessaire à son traitement. Pour l'application du présent article, le recueil des données s'entend des seules données résultant de l'utilisation par le patient du dispositif médical concerné. / Ces données peuvent, avec l'accord du patient, être télétransmises au médecin prescripteur, au prestataire et au service du contrôle médical mentionné à l'article L. 315-1. Au regard de ces données, le prestataire peut conduire, en lien avec le prescripteur qui réévalue, le cas échéant, sa prescription, des actions ayant pour objet de favoriser une bonne utilisation du dispositif médical inscrit ainsi que ses prestations de services et d'adaptation associées, sur la liste mentionnée à l'article L. 165-1. / (...) / Les tarifs de responsabilité ou les prix mentionnés, respectivement, aux articles L. 165-2 et L. 165-3 peuvent être modulés, sans préjudice des autres critères d'appréciation prévus aux mêmes articles L. 165-2 et L. 165-3, en fonction de certaines données collectées, notamment celles relatives aux modalités d'utilisation du dispositif médical mis à disposition. (...) Cette modulation du tarif de responsabilité ou du prix des produits et prestations mentionnés audit article L. 165-1 ne peut avoir d'incidence sur la qualité de la prise en charge du patient par les prestataires. Une moindre utilisation du dispositif médical ne peut en aucun cas conduire à une augmentation de la participation de l'assuré mentionnée au I de l'article L. 160-13 aux frais afférents à ce dispositif et à ses prestations associées (...) ".<br/>
<br/>
              3. Selon l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, la loi " doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité devant la loi ne s'oppose ni à ce que législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit.<br/>
<br/>
              4. L'article 13 de la Déclaration de 1789 dispose que : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". Si cet article n'interdit pas de faire supporter, pour un motif d'intérêt général, à certaines catégories de personnes des charges particulières, il ne doit pas en résulter de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              5. En premier lieu, les dispositions de l'article L. 165-1-3 du code de la sécurité sociale ont pour objet, dans un double objectif d'incitation des prestataires à conduire des actions destinées à favoriser une bonne utilisation de certains dispositifs médicaux et de meilleure allocation des ressources publiques, de permettre la modulation du tarif de responsabilité ou du prix des produits et prestations en fonction de certaines données résultant de l'utilisation du dispositif médical mis à disposition du patient, relatives notamment aux modalités de cette utilisation. D'une part, il appartiendra aux ministres chargés de la santé et de la sécurité sociale, au vu notamment de l'avis de la Haute Autorité de santé, de déterminer les traitements d'affections chroniques pour lesquels, des actions pouvant être conduites afin de favoriser la bonne utilisation du dispositif médical, la modulation du tarif de responsabilité ou du prix des produits et prestations répond à l'objectif poursuivi par le législateur d'implication des prestataires dans l'amélioration de l'observance des traitements. D'autre part, la différence de traitement entre prestataires mettant à disposition des patients les mêmes dispositifs médicaux selon les données collectées, susceptible de résulter de la modulation prévue par les dispositions critiquées, est en rapport direct avec l'objet de la loi qui est, ainsi qu'il a été dit, d'inciter les prestataires à participer à une meilleure utilisation de ces dispositifs médicaux. Dès lors, la société requérante n'est pas fondée à soutenir que ces dispositions méconnaîtraient le principe d'égalité devant la loi.<br/>
<br/>
              6. En deuxième lieu, si les dispositions critiquées sont susceptibles d'entraîner une diminution de la rémunération des prestataires, alors que la modulation prévue ne peut conduire à une augmentation de la participation de l'assuré, elles répondent aux objectifs de protection de la santé publique et d'équilibre financier de la sécurité sociale. En outre, cette modulation, qui s'effectue sans préjudice des autres critères d'appréciation des tarifs de responsabilité ou des prix fixés par les articles L. 165-2 et L. 165-3 du code de la sécurité sociale, est nécessairement associée à une télétransmission des données au médecin prescripteur, amené le cas échéant à réévaluer sa prescription, et au service du contrôle médical, amené le cas échéant à faire usage des pouvoirs qu'il tient des articles L. 315-1 et L. 315-2-1 du même code. Dans ces conditions, et alors qu'il appartiendra au pouvoir réglementaire de prévoir une amplitude dans la modulation du tarif de responsabilité ou du prix des produits et prestations qui n'entraîne pas de rupture caractérisée de l'égalité devant les charges publiques, les dispositions de l'article L. 165-1-3, qui répondent à un motif d'intérêt général, satisfont aux exigences découlant de l'article 13 de la Déclaration de 1789. <br/>
<br/>
              7. En troisième lieu, pour les mêmes raisons, les dispositions de l'article L. 165-1-3 du code de la sécurité sociale n'excèdent pas, en tout état de cause, les limitations que le législateur peut apporter aux conditions d'exercice du droit de propriété garanti notamment par l'article 2 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              8. En dernier lieu, ni la détermination des traitements pour lesquels une modulation des tarifs et des prix des produits et prestations peut être prévue ni celle de l'amplitude de cette modulation ne relèvent des principes fondamentaux de la sécurité sociale qu'il incombe au législateur de déterminer en vertu de l'article 34 de la Constitution. Ainsi, la société requérante n'est, en tout état de cause, pas fondée à invoquer la méconnaissance par le législateur de sa propre compétence.<br/>
<br/>
              9. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée à l'encontre de l'article L. 165-1-3 du code de la sécurité sociale, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de la renvoyer au Conseil constitutionnel, le moyen tiré de que les dispositions de cet article portent atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Lafonta santé.<br/>
Article 2 : La présente décision sera notifiée à la société Lafonta santé, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
