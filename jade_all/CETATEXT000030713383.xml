<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713383</ID>
<ANCIEN_ID>JG_L_2015_06_000000366161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/33/CETATEXT000030713383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 08/06/2015, 366161</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:366161.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. A...F..., demeurant..., Mme D...C..., demeurant..., et M. E... B..., demeurant ... ; M. F...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social sur leur demande tendant à l'abrogation de la décision du 5 août 2010 par laquelle le ministre du travail a fixé les règles relatives à la rémunération annuelle des médecins inspecteurs régionaux du travail, en tant qu'elle crée la tranche exceptionnelle de rémunération ouverte aux médecins inspecteurs et en fixe les modalités d'accès ; <br/>
<br/>
              2°) d'enjoindre au ministre du travail de procéder à l'abrogation, dans cette mesure, de la décision du 5 août 2010 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que, au titre des dépens, la contribution pour l'aide juridique prévue à l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. F...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 8123-1 du code du travail : " Les médecins inspecteurs du travail exercent une action permanente en vue de la protection de la santé physique et mentale des travailleurs sur leur lieu de travail et participent à la veille sanitaire au bénéfice des travailleurs. Leur action porte en particulier sur l'organisation et le fonctionnement des services de santé au travail prévus au titre II du livre VI de la quatrième partie " ; qu'aux termes de l'article R. 8123-1 du même code : " Le médecin inspecteur du travail concourt à l'ensemble des missions des services déconcentrés relevant des ministres chargés du travail, de l'emploi et de la formation professionnelle. A ce titre, il formule les avis et prend les décisions prévues par les dispositions légales. Il est notamment chargé de l'étude des risques professionnels et de leur prévention. Il exerce une mission d'information au bénéfice des médecins du travail et des médecins de main-d'oeuvre, qu'il associe aux études entreprises (...) " ;<br/>
<br/>
              2. Considérant que par une décision du 5 août 2010 relative à la rémunération annuelle des médecins inspecteurs régionaux du travail, le ministre du travail, de la solidarité et de la fonction publique a fixé, pour les médecins inspecteurs régionaux du travail qui ont la qualité d'agents contractuels de l'Etat, les règles de rémunération qui leur sont applicables ; que M. F...et autres demandent l'annulation de la décision implicite par laquelle le ministre a refusé d'abroger cette décision du 5 août 2010 en tant qu'elle crée une tranche exceptionnelle de rémunération et en fixe les modalités d'accès ; <br/>
<br/>
              3. Considérant que la décision du 5 août 2010 ne fixe aucune règle de déontologie médicale ; que, par suite, le moyen tiré de ce qu'elle aurait dû, par application des dispositions de l'article L. 4127-1 du code de la santé publique relatives au code de déontologie des médecins, faire l'objet d'un décret en Conseil d'Etat préparé par le Conseil national de l'ordre des médecins ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant que la décision de refus d'abrogation dont M. F...et autres demandent l'annulation revêt, en raison du caractère réglementaire de l'acte qu'elle refuse d'abroger, un caractère réglementaire ; que les requérants ne sauraient, par suite, utilement soutenir qu'elle méconnaît les dispositions des articles 1er et 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, qui ne sont pas applicables aux actes réglementaires ; <br/>
<br/>
              5. Considérant que l'article 4 de la décision du 5 août 2010 dispose : " Il est créé une tranche exceptionnelle ouverte aux médecins inspecteurs, accessible par la voie de la nomination au choix sous les conditions suivantes : / avoir une ancienneté dans le service de 6 ans / être proposé par le directeur régional / avoir manifesté des qualités particulières dans la manière de servir / être retenu par une commission DGT-DAGEMO " ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article R. 4127-95 du code de la santé publique : " Le fait pour un médecin d'être lié dans son exercice professionnel par un contrat ou un statut à un autre médecin, une administration, une collectivité ou tout autre organisme public ou privé n'enlève rien à ses devoirs professionnels et l'indépendance de ses décisions. / En aucune circonstance, le médecin ne peut accepter de limitation à son indépendance dans son exercice médical de la part du médecin, de l'entreprise ou de l'organisme qui l'emploie. (...) " ;<br/>
<br/>
              7. Considérant qu'en prévoyant, par sa décision attaquée du 5 août 2010 qui a créé la tranche exceptionnelle de rémunération, la prise en compte des " qualités particulières dans la manière de servir " des médecins inspecteurs régionaux, le ministre chargé du travail doit être regardé comme ayant entendu subordonner l'accès à cette tranche exceptionnelle à une évaluation globale des qualités professionnelles des médecins inspecteurs susceptibles d'y prétendre ; qu'une telle disposition n'a pas pour objet et ne saurait avoir légalement pour effet de conduire les autorités chargées de proposer ou d'accorder le bénéfice de cette tranche exceptionnelle à porter un jugement sur les décisions médicales particulières prises par les médecins inspecteurs dans l'exercice de leurs compétences ; qu'au demeurant, contrairement à ce que soutiennent les requérants, l'instruction adressée le 25 janvier 2012 par le ministre chargé du travail aux directeurs et directeurs régionaux des entreprises, de la concurrence, de la consommation, du travail et de l'emploi, chargés de proposer les noms des bénéficiaires de cette tranche exceptionnelle, leur fixe des critères de choix qui ne font intervenir aucune appréciation de leurs analyses médicales ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à soutenir que la décision du 5 août 2010 et, par suite, le refus de l'abroger violeraient le principe d'indépendance professionnelle des médecins rappelé par l'article R. 4127-95 du code de la santé publique cité ci-dessus ; <br/>
<br/>
              9. Considérant que l'absence, dans la décision du 5 août 2010, de précisions apportées au critère des " qualités particulières dans la manière de servir " n'est pas de nature à entraîner nécessairement la méconnaissance, par les décisions individuelles prises sur son fondement, du principe d'égalité de traitement entre les médecins inspecteurs, ni celle du principe d'impartialité ; que les requérants ne sont, par suite, pas fondés à soutenir que le refus d'abroger la décision du 5 août 2010 méconnaît ces mêmes principes ;<br/>
<br/>
              10. Considérant, enfin, que l'appréciation de la manière de servir, qui comporte l'évaluation des qualités générales manifestées par les médecins inspecteurs dans l'exercice de l'ensemble de leurs fonctions, ne peut par suite être regardée, même si elle exclut toute appréciation portée sur la part médicale de leur activité, comme ne touchant qu'une part mineure de leur activité ; que les requérants ne sont, ainsi, pas fondés à soutenir que la décision du 5 août 2010 ainsi que le refus de l'abroger seraient, pour ce motif, entachés d'une erreur manifeste d'appréciation ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que M. F...et autres ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent ; que leurs conclusions tendant à ce qu'il soit enjoint au ministre chargé du travail d'abroger la décision du 5 août 2010 doivent, par suite, être rejetées ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de laisser la contribution pour l'aide juridique à la charge des requérants ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. F...et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...F..., à Mme D...C..., à M. E...B...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - MÉDECIN INSPECTEUR - INDÉPENDANCE - POSSIBILITÉ POUR L'AUTORITÉ GESTIONNAIRE D'ÉVALUER LA MANIÈRE DE SERVIR  - EXISTENCE - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03-04-01-03 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. MÉDECINE DU TRAVAIL. STATUT DES MÉDECINS DU TRAVAIL DANS L'ENTREPRISE. CONTRÔLE TECHNIQUE PAR LES MÉDECINS-INSPECTEURS (DÉCRET DU 24 NOVEMBRE 1977). - INDÉPENDANCE DES MÉDECINS INSPECTEURS - POSSIBILITÉ POUR L'AUTORITÉ GESTIONNAIRE D'ÉVALUER LA MANIÈRE DE SERVIR - EXISTENCE, DÈS LORS QUE NE SONT PAS ÉVALUÉES LES DÉCISIONS MÉDICALES [RJ1].
</SCT>
<ANA ID="9A"> 55-03-01-02 En prévoyant, par sa décision du 5 août 2010 qui a créé la tranche exceptionnelle de rémunération, la prise en compte des  qualités particulières dans la manière de servir  des médecins inspecteurs régionaux, le ministre chargé du travail doit être regardé comme ayant entendu subordonner l'accès à cette tranche exceptionnelle à une évaluation globale des qualités professionnelles des médecins inspecteurs susceptibles d'y prétendre. Une telle disposition n'a pas pour objet et ne saurait avoir légalement pour effet de conduire les autorités chargées de proposer ou d'accorder le bénéfice de cette tranche exceptionnelle à porter un jugement sur les décisions médicales particulières prises par les médecins inspecteurs dans l'exercice de leurs compétences.</ANA>
<ANA ID="9B"> 66-03-04-01-03 En prévoyant, par sa décision du 5 août 2010 qui a créé la tranche exceptionnelle de rémunération, la prise en compte des  qualités particulières dans la manière de servir  des médecins inspecteurs régionaux, le ministre chargé du travail doit être regardé comme ayant entendu subordonner l'accès à cette tranche exceptionnelle à une évaluation globale des qualités professionnelles des médecins inspecteurs susceptibles d'y prétendre. Une telle disposition n'a pas pour objet et ne saurait avoir légalement pour effet de conduire les autorités chargées de proposer ou d'accorder le bénéfice de cette tranche exceptionnelle à porter un jugement sur les décisions médicales particulières prises par les médecins inspecteurs dans l'exercice de leurs compétences.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 30 juin 1993, Villes de Champigny-sur-Marne et de Kermadec, n° 101887 102179, T. p. 591.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
