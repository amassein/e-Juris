<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033970242</ID>
<ANCIEN_ID>JG_L_2017_02_000000396854</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/97/02/CETATEXT000033970242.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 01/02/2017, 396854, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396854</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:396854.20170201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 9 février et 6 mai 2016, le syndicat UNSA-Ecologie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 10 décembre 2015 par laquelle le directeur général de l'Office national de la chasse et de la faune sauvage (ONCFS) a refusé de faire droit à sa demande tendant à l'abrogation de l'article 21 du règlement intérieur de l'ONCFS relatif à l'aménagement et la réduction du temps de travail ;<br/>
<br/>
              2°) d'enjoindre à l'ONCFS, dans un délai de deux mois à compter de la notification de la décision du Conseil d'Etat, d'abroger le règlement intérieur en tant qu'il méconnait l'arrêté du 4 février 2002 du ministre chargé de l'environnement portant application du décret n° 2000-815 du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat dans certains établissements publics du ministère de l'écologie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat et de mettre le règlement intérieur de l'ONFCS en conformité avec les dispositions de l'article 1er de l'arrêté du 4 février 2002, et ce, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) subsidiairement, d'enjoindre à l'ONCFS de réexaminer sa demande d'abrogation partielle et de modification du règlement intérieur dans un délai de deux mois à compter de la notification de la décision du Conseil d'Etat sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'ONCFS et de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - l'arrêté 4 février 2002 du ministre en charge de l'environnement portant application du décret n° 2000-815 du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat dans certains établissements publics du ministère de l'écologie, de l'énergie, du développement durable et de la mer ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du syndicat UNSA-Ecologie et à la SCP Waquet, Farge, Hazan, avocat de l'Office national de la chasse et de la faune sauvage. <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er du décret du 25 août 2000 : " La durée du travail effectif est fixée à trente-cinq heures par semaine dans les services et établissements publics administratifs de l'Etat ainsi que dans les établissements publics locaux d'enseignements. / Le décompte du temps de travail est réalisé sur la base d'une durée annuelle de travail effectif de 1607 heures maximum, sans préjudice des heures supplémentaires susceptibles d'être effectuées " ; qu'aux termes de l'article 1er de l'arrêté du 4 février 2002 : " Les personnels des établissements publics administratifs visés en annexe qui, dans le cadre de leurs obligations de service normal, sont appelés à travailler la nuit, le dimanche ou les jours fériés peuvent bénéficier, au choix de l'agent, d'une indemnisation ou d'une compensation, sous réserve des nécessités de service. / Lorsque ce travail ne fait pas l'objet de l'indemnisation prévue à l'alinéa précédent, la durée de la compensation est calculée sur la base d'un coefficient de majoration fixé à 2. Ces majorations ne sont pas cumulables " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 21 du règlement intérieur de l'Office national de la chasse et de la faune sauvage adopté le 21 février 2002 : " Le service le dimanche, les jours fériés et la nuit sont, soit indemnisés financièrement, soit compensés en temps. Lorsqu'il n'est pas indemnisé, le travail le dimanche ou les jours fériés donne droit à un jour de récupération et un jour de compensation (...) " ; qu'il ressort des pièces du dossier que, par un courrier du 1er décembre 2015, le syndicat UNSA-Ecologie a demandé à l'Office d'abroger l'article 21 du règlement intérieur au motif qu'il ne serait pas conforme à l'article 1er de l'arrêté du 4 février 2002 ; que, par une lettre du 10 décembre 2015, le directeur général de l'Office a informé le syndicat UNSA-Ecologie qu'il ne serait pas donné suite à sa démarche ; que, par la présente requête, le syndicat UNSA-Ecologie demande au Conseil d'Etat l'annulation pour excès de pouvoir du refus d'abroger l'article 21 du règlement intérieur qui lui a ainsi été opposé ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Considérant que si, aux termes du II l'article R. 421-13 du code de l'environnement, le conseil d'administration de l'Office national de la chasse et de la faune sauvage " délibère notamment sur : (...) 12° Le règlement intérieur ; (...) ", ces dispositions ne font toutefois pas obstacle à ce que le rejet d'une demande d'abrogation d'un article du règlement intérieur soit prononcé par simple décision de son directeur général ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              4. Considérant qu'il résulte des dispositions de l'article 1er de l'arrêté du 4 février 2002, cité au point 1, qu'un agent qui, dans le cadre de ses obligations de service normal, est appelé à travailler le dimanche, bénéficie, s'il n'est pas indemnisé, d'une compensation en temps de deux journées pour un dimanche travaillé ; qu'ainsi, un agent travaillant cinq jours par semaine qui est appelé à travailler un dimanche bénéficie, en sus de ses deux journées de repos hebdomadaire, dont l'une compense le travail le dimanche, d'une journée supplémentaire de repos ; qu'il suit de là que l'article 21 du règlement intérieur de l'Office cité au point 2, qui accorde " un jour de récupération et un jour de compensation " lorsqu'un agent de l'Office a travaillé un dimanche sans être indemnisé, ne méconnaît pas les dispositions de l'article 1er de l'arrêté du 4 février 2002 ; qu'en rejetant, pour ce motif, la demande du syndicat UNSA-Ecologie tendant à l'abrogation de cet article 21, le directeur général de l'Office n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que, par suite, ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par l'Office national de la chasse et de la faune sauvage ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat UNSA-Ecologie est rejetée. <br/>
Article 2 : Les conclusions de l'Office national de la chasse et de la faune sauvage tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat UNSA-Ecologie et à l'Office national de la chasse et de la faune sauvage.<br/>
Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et à la ministre de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
