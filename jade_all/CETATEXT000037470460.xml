<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037470460</ID>
<ANCIEN_ID>JG_L_2018_10_000000417261</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/47/04/CETATEXT000037470460.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 05/10/2018, 417261, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417261</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417261.20181005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A...B...a demandé au juge des référés du tribunal administratif de Cergy-Pontoise que soit ordonnée une expertise, sur le fondement de l'article R. 532-1 du code de justice administrative, afin de décrire les séquelles de la chute dont elle a été victime le 16 novembre 2016 à Nanterre (Hauts-de-Seine), d'indiquer si toutes les précautions ont été prises par la commune pour éviter cet accident et d'évaluer l'ensemble des préjudices qu'elle a subis.<br/>
<br/>
              Par une ordonnance n° 1703348 du 30 août 2017, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17VE02921 du 28 décembre 2017, le juge des référés de la cour administrative d'appel de Versailles a rejeté l'appel qu'elle a formé contre cette ordonnance. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 et 29 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande tendant à ce que soit ordonnée une expertise ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Nanterre la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de Mme A...B..., et à Me Haas, avocat de la commune de Nanterre ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article R. 532-1 du code de justice administrative : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction " ;<br/>
<br/>
              2.	Considérant que l'utilité d'une mesure d'instruction ou d'expertise qu'il est demandé au juge des référés d'ordonner sur le fondement de l'article R. 532-1 du code de justice administrative doit être appréciée, d'une part, au regard des éléments dont le demandeur dispose ou peut disposer par d'autres moyens et, d'autre part, bien que ce juge ne soit pas saisi du principal, au regard de l'intérêt que la mesure présente dans la perspective d'un litige principal, actuel ou éventuel, auquel elle est susceptible de se rattacher ; qu'à ce dernier titre, il ne peut faire droit à une demande d'expertise permettant d'évaluer un préjudice, en vue d'engager la responsabilité d'une personne publique, en l'absence manifeste de lien de causalité entre le préjudice à évaluer et la faute alléguée de cette personne ;<br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges des référés que Mme A...B...a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, sur le fondement de l'article R. 532-1 du code de justice administrative, d'ordonner une expertise aux fins d'évaluer les préjudices qu'elle estime avoir subis du fait de la chute dont elle a été victime le 16 novembre 2016 à Nanterre, en raison d'une bouche d'égout qui n'était ni refermée ni signalée, et qui lui a occasionné une fracture du col de l'humérus associée à une fracture du tubercule majeur, et d'indiquer si des précautions avaient été prises par la commune pour éviter cet accident ; que, par une ordonnance du 30 août 2017, le juge des référés du tribunal administratif a rejeté cette demande d'expertise ; que, par une ordonnance du 28 décembre 2017, contre laquelle Mme A...B...se pourvoit en cassation, le juge des référés de la cour administrative d'appel de Versailles a rejeté l'appel qu'elle avait formé contre l'ordonnance rendue par le juge des référés du tribunal administratif ;<br/>
<br/>
              4.	Considérant que, pour rejeter l'appel dont il était saisi, le juge des référés de la cour administrative d'appel a retenu que les pièces produites par Mme A...B...ne comportaient pas de mentions suffisamment précises et circonstanciées pour identifier avec précision l'ouvrage public mis en cause et l'origine de la chute ; qu'en statuant ainsi, alors qu'avaient été versées au dossier une lettre adressée par le maire de Nanterre à Mme A...B...le 24 novembre 2016 faisant état de la chute de l'intéressée dans un regard d'assainissement situé sur le trottoir de la rue Lamartine et une attestation d'un employé municipal établie le 21 novembre 2016 indiquant qu'il avait porté secours à l'intéressée et portant mention de la date et de l'heure approximative de l'accident, le juge des référés a dénaturé les pièces du dossier ; qu'il s'ensuit que Mme A...B...est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance qu'elle attaque ; <br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Nanterre une somme de 3 000 euros à verser à Mme A...B...au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mis à ce titre à la charge de Mme A...B...qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 28 décembre 2017 du juge des référés de la cour administrative d'appel de Versailles est annulée. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : La commune de Nanterre versera une somme de 3 000 euros à Mme A...B...au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions de la commune de Nanterre présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme C...A...B...et à la commune de Nanterre. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
