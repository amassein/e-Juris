<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037534157</ID>
<ANCIEN_ID>JG_L_2018_10_000000411819</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/41/CETATEXT000037534157.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/10/2018, 411819, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411819</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411819.20181026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n ° RG 2016050502 du 6 juin 2017, enregistré au secrétariat du contentieux du Conseil d'Etat le 23 juin 2017, le tribunal de commerce de Paris a, en application de l'article 49 alinéa 2 du code de procédure civile, sursis à statuer dans le litige opposant la Fédération française de football et la Ligue de football professionnel à la société Gaumont Pathé archives au sujet de la commercialisation d'images d'archives de matchs de football et saisi le Conseil d'Etat d'une question préjudicielle relative à la conformité de l'article L. 333-1 du code du sport aux articles 2, 11, 16 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789, de l'article 34 de la Constitution et de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              Par un mémoire, enregistré le 2 août 2017, la Fédération française de football et la Ligue de football professionnel demandent au Conseil d'État de déclarer que cette disposition n'est pas entachée d'illégalité et de mettre à la charge de la société Gaumont Pathé archives la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure civile ;<br/>
              - le code de la propriété intellectuelle ;<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de la société Gaumont Pathe Archives  et à la SCP Meier-Bourdeau, Lecuyer, avocat de la Fédération française de football et de la société Ligue de football professionnel ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que la Fédération française de football et la Ligue  de football professionnel, estimant que l'exploitation commerciale d'images d'archives prises à l'occasion de matchs du championnat de France de football par la société Gaumont Pathé archives portait atteinte aux droits exclusifs qu'elles détiennent sur le fondement de l'article L. 333-1 du code du sport, ont saisi le tribunal de grande instance de Paris d'une demande visant à faire cesser cette exploitation et à ce que la société Gaumont Pathé archives soit condamnée à leur verser environ 1,2 million d'euros en réparation du préjudice subi. Par une ordonnance du 23 juin 2016, le tribunal de grande instance de Paris a décliné sa compétence au profit du tribunal de commerce de Paris. Par un jugement du 6 juin 2017, le tribunal de commerce de Paris a, en application des dispositions du second alinéa de l'article 49 du code de procédure civile, décidé de surseoir à statuer et demandé au Conseil d'Etat de se prononcer sur la question suivante : " L'article L. 333-1 du code du sport méconnaît-il les articles 2, 11, 16 et 17 de la Déclaration des droits de l'homme de 1789 et 34 de la Constitution, ainsi que l'article 1 du premier protocole additionnel à  la convention européenne des droits de l'homme, en  tant qu'il viendrait subordonner à l'autorisation d'un tiers l'exercice par le producteur de vidéogrammes de son droit déjà constitué sur des séquences d'images de manifestations sportives antérieures à son entrée en vigueur, et en tant qu'il ne précise ni l'assiette du droit des organisateurs de manifestations sportives, ni la nature de ce droit et, par voie de conséquence, sa durée ' ".<br/>
<br/>
              3. Le code du sport, issu de l'ordonnance du 23 mai 2006 relative à la partie législative du code du sport, qui n'a pas été ratifiée, comporte un article L. 333-1 aux termes duquel : " Les fédérations sportives, ainsi que les organisateurs de manifestations sportives mentionnés à l'article L. 331-5, sont propriétaires du droit d'exploitation des manifestations ou compétitions sportives qu'ils organisent./ Toute fédération sportive peut céder aux sociétés sportives, à titre gratuit, la propriété de tout ou partie des droits d'exploitation audiovisuelle des compétitions ou manifestations sportives organisées chaque saison sportive par la ligue professionnelle qu'elle a créée, dès lors que ces sociétés participent à ces compétitions ou manifestations sportives. La cession bénéficie alors à chacune de ces sociétés ". Ces dispositions reprennent celles de l'article 18-1 de la loi du 16 juillet 1984 relative à l'organisation et à la promotion des activités physiques et sportives créé par la loi du 13 juillet 1992. En l'absence de disposition contraire, cette loi ne dispose que pour l'avenir et ne saurait saisir des situations définitivement constituées avant son intervention. La règle attribuant le droit d'exploitation d'une manifestation ou d'une compétition sportive à son organisateur ne s'applique donc qu'aux événements postérieurs à l'entrée en vigueur de la loi du 13 juillet 1992.<br/>
<br/>
              4. Il s'ensuit, sans préjudice de la détermination du détenteur du droit d'exploitation d'une manifestation ou d'une compétition sportive organisée avant l'entrée en vigueur de la loi du 13 juillet 1992, que les dispositions issues de cette loi et reprises aujourd'hui à article L. 333-1 du code du sport ne règlent en aucune manière les situations constituées antérieurement à cette entrée en vigueur. Elles ne sauraient, dès lors, méconnaître les dispositions et stipulations citées au point 1 à raison de leur prétendu caractère rétroactif.  <br/>
<br/>
              5. Il résulte de ce qui précède que la société Gaumont Pathé archives n'est pas fondée à soutenir que l'article L. 331-1 du code du sport serait, dans cette mesure, entaché d'illégalité.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la Fédération française de football, la Ligue de football professionnel et la société Gaumont Pathé archives au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est déclaré que l'exception d'illégalité de l'article L. 333-1 du code du sport, soulevée par la société Gaumont Pathé archives devant le tribunal de commerce de Paris n'est pas fondée.<br/>
Article 2 : Les conclusions de la Fédération française de football, de la Ligue de football professionnel et de la société Gaumont Pathé archives présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au tribunal de commerce de Paris, à la Fédération française de football, à la Ligue de football professionnel et à la société Gaumont Pathé archives.   <br/>
Copie en sera adressée pour information à la ministre des sports.  <br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
