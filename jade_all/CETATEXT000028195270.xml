<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028195270</ID>
<ANCIEN_ID>JG_L_2013_11_000000360444</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/19/52/CETATEXT000028195270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 13/11/2013, 360444, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360444</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:360444.20131113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juin et 21 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Caisse des dépôts et consignations, dont le siège est centre de gestion des pensions rue du Vergne à Bordeaux cedex (33059) ; la Caisse des dépôts et consignations demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA00561 du 17 avril 2012 par lequel la cour administrative d'appel de Marseille a, à la demande de Mme A...B..., en premier lieu, annulé le jugement n° 0705003 du 10 décembre 2009 par lequel le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation de la décision de la Caisse nationale de retraites des agents des collectivités locales du 15 juin 2007 en tant qu'elle a refusé d'admettre l'intéressée à la retraite avec pension d'invalidité, en deuxième lieu, annulé la décision du 15 juin 2007 de la Caisse nationale de retraites des agents des collectivités locales en tant qu'elle a refusé d'admettre Mme B...à la retraite pour invalidité à compter du 15 juin 2007 et, en dernier lieu, enjoint à la Caisse des dépôts et consignations, gestionnaire de la Caisse nationale de retraites des agents des collectivités locales, d'admettre Mme B...à la retraite pour invalidité à compter du 15 juin 2007 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de MmeB... ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 7 du décret n° 2003-1306 du 26 décembre 2003 : " Le droit à pension est acquis : ... 2° Sans condition de durée des services aux fonctionnaires rayés des cadres pour invalidité résultant ou non de l'exercice des fonctions. " ; qu'aux termes de l'article 30 : " Le fonctionnaire qui se trouve dans l'impossibilité définitive et absolue de continuer ses fonctions par suite de maladie, blessure ou infirmité grave dûment établie peut être admis à la retraite soit d'office, soit sur demande. " ; qu'aux termes de l'article 31 : " Une commission de réforme est constituée dans chaque département pour apprécier la réalité des infirmités invoquées, la preuve de leur imputabilité au service, les conséquences et le taux d'invalidité qu'elles entraînent, l'incapacité permanente à l'exercice des fonctions (...) / Le pouvoir de décision appartient dans tous les cas à l'autorité qui a qualité pour procéder à la nomination, sous réserve de l'avis conforme de la Caisse nationale de retraites des agents des collectivités locales. " ; qu'aux termes de l'article 39 : " Le fonctionnaire qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'une invalidité ne résultant pas du service peut être mis à la retraite par anticipation soit sur demande soit d'office (...). L'intéressé a droit à la pension rémunérant les services prévue au 2° de l'article 7 et au 2° du I de l'article L. 24 du code des pensions civiles et militaires de retraite sous réserve que ses blessures ou maladies aient été contractées ou aggravées au cours d'une période durant laquelle il acquérait des droits à pension. " ; et qu'aux termes du I de l'article 24 du code des pensions civiles et militaires de retraite : " La liquidation de la pension intervient : ... 2° Lorsque le fonctionnaire est mis à la retraite pour invalidité et qu'il n'a pas pu être reclassé dans un emploi compatible avec son état de santé. " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions qu'il appartient à la Caisse nationale de retraites des agents des collectivités locales, lorsqu'elle est saisie d'une demande tendant à la mise à la retraite d'un fonctionnaire pour invalidité assortie du bénéfice du droit à pension, d'une part, d'émettre un avis sur le bien-fondé de la demande de mise à la retraite pour invalidité, d'autre part, de décider si l'intéressé a droit à une pension ; que l'intervention de la décision de mise à la retraite pour invalidité d'un fonctionnaire, prise par l'autorité ayant qualité pour procéder à sa nomination, étant subordonnée à l'avis conforme de la caisse, cet avis est susceptible de faire l'objet d'un recours pour excès de pouvoir de la part du fonctionnaire concerné lorsqu'il est défavorable ; qu'enfin, lorsque l'invalidité ne résulte pas de l'exercice des fonctions, la Caisse nationale de retraites des agents des collectivités locales est tenue de vérifier, d'une part, si le fonctionnaire se trouve dans l'incapacité permanente de continuer ses fonctions au sens des articles 30 et 39 et, d'autre part, s'il a droit au bénéfice d'une pension sans condition de durée de services, conformément à l'article 39, dans le cas où ses blessures ou maladies ont été contractées ou aggravées au cours d'une période durant laquelle il acquérait des droits à pension ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que MmeB..., fonctionnaire territorial, a sollicité sa mise à la retraite pour invalidité avec droit à pension ; que, par un courrier du 15 juin 2007, la Caisse nationale de retraites des agents des collectivités locales a rejeté cette demande ; que, pour annuler partiellement cette décision en tant que la caisse avait refusé " d'admettre Mme B...à la retraite pour invalidité à compter du 15 juin 2007 ", la cour administrative d'appel de Marseille s'est fondée sur le fait que dès lors qu'il ressortait des pièces du dossier que l'intéressée se trouvait dans l'incapacité totale et définitive d'exercer ses fonctions, elle remplissait les conditions posées par l'article 30 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales, lesquelles sont rappelées, en substance, à la première phrase du premier alinéa de l'article 39 ;<br/>
<br/>
              4. Considérant que contrairement à ce que soutient la Caisse des dépôts et consignations, la cour administrative d'appel n'a pas commis d'erreur de droit en estimant que la circonstance que Mme B...ne remplirait pas, par ailleurs, les conditions posées par la deuxième phrase du premier alinéa de l'article 39 pour bénéficier d'une pension d'invalidité, lesquelles imposent que les blessures ou maladies soient contractées ou aggravées au cours d'une période durant laquelle le fonctionnaire acquiert des droits à pension, était sans incidence sur le droit de l'intéressée à être mise à la retraite pour invalidité ; qu'ainsi, en ne se prononçant pas sur le bien-fondé de ce moyen de défense qui était inopérant en tant qu'il était soulevé au soutien de la décision de la caisse d'émettre un avis défavorable à la mise à la retraite pour invalidité de MmeB..., la cour n'a pas entaché son arrêt d'insuffisante motivation ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la Caisse des dépôts et consignations n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Caisse des dépôts et  consignations est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la Caisse des dépôts et consignations et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
