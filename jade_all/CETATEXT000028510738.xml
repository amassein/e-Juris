<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028510738</ID>
<ANCIEN_ID>JG_L_2014_01_000000346787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/51/07/CETATEXT000028510738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 24/01/2014, 346787</TITRE>
<DATE_DEC>2014-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:346787.20140124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 février 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense et des anciens combattants ; le ministre de la défense et des anciens combattants demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0903805 du 30 décembre 2010 par lequel le tribunal administratif de Bordeaux, à la demande de M. B... A..., a annulé les décisions du 22 juillet 2009 refusant à l'intéressé le bénéfice des dispositions de l'article L. 25 bis du code des pensions civiles et militaires de retraite et du 21 septembre 2009 rejetant le recours gracieux de l'intéressé formé le 17 août 2009 contre cette décision ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M.A... ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... a été engagé aux Etablissements de subsistance de Dijon le 1er juin 1972 ; que, sur sa demande, il a été rayé des cadres à compter du 17 avril 1998 et admis, par une décision du 27 avril 1998, à faire valoir ses droits à la retraite avec jouissance différée au 25 décembre 2011, soit au jour de ses 60 ans ; que, par une lettre du 12 juin 2009, M. A...a demandé à l'administration de lui appliquer l'article L. 25 bis du code des pensions civiles et militaires de retraite relatif aux carrières longues et de bénéficier du versement de sa pension à compter du 1er janvier 2010 ; que, par un jugement du 30 décembre 2010, le tribunal administratif de Bordeaux a annulé les décisions du 22 juillet et du 21 septembre 2009 par lesquelles le ministre de la défense a rejeté cette demande ainsi que le recours gracieux qu'il avait formé contre ce rejet ; <br/>
<br/>
              2. Considérant que l'article 119 de la loi du 30 décembre 2004 de finances pour 2005, qui inséré l'article L. 25 bis dans le code des pensions civiles et militaires de retraite, abaisse l'âge légal de l'ouverture du droit à la retraite au terme d'une carrière longue pour les assurés ayant commencé à travailler très jeunes ; que cette loi prévoit une entrée en vigueur progressive de ce dispositif à compter du 1er janvier 2005 ; <br/>
<br/>
              3. Considérant qu'en principe, les droits à pension s'apprécient au regard de la législation en vigueur à la date de radiation des cadres ; que toutefois, en l'absence de disposition législative contraire, le droit à l'abaissement de l'âge de soixante ans pour la liquidation de la pension de retraite, prévu par l'article L. 25 bis du code des pensions civiles et militaires, entré en vigueur le 1er janvier 2005, est applicable aux fonctionnaires qui demandent la liquidation de leur pension de retraite à compter de cette date ou d'une date postérieure, quelle que soit la date de leur radiation des cadres ; <br/>
<br/>
              4. Considérant qu'ainsi qu'il a été dit au point 1, M. A...a demandé et obtenu la liquidation de sa retraite en 1998 ; que, dès lors, et quand bien même l'entrée en jouissance de sa pension a été différée, l'article 119 de la loi du 30 décembre 2004, qui a inséré l'article L. 25 bis dans le code des pensions civiles et militaires de retraite, ne pouvait s'appliquer à la situation de M.A... ; qu'en jugeant que ce dernier avait droit, sur le fondement de ces dispositions, à la jouissance de sa pension à compter du 1er janvier 2010, le tribunal administratif a commis une erreur de droit ; que, par suite,  son jugement doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit ci-dessus que la demande présentée par M. A...devant le tribunal administratif de Bordeaux ne peut qu'être rejetée ; que, par voie de conséquence, ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent également être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Bordeaux du 30 décembre 2010 est annulé.<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Bordeaux est rejetée.<br/>
Article 3 : Les conclusions de M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de la défense et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. - DROITS À PENSION - 1) PRINCIPE - APPRÉCIATION AU REGARD DE LA LÉGISLATION EN VIGUEUR À LA DATE DE RADIATION DES CADRES - EXCEPTION - DROIT À L'ABAISSEMENT DE L'ÂGE DE SOIXANTE ANS POUR LA LIQUIDATION DE LA PENSION PRÉVU PAR L'ARTICLE L. 25 BIS DU CPCMR - APPLICATION AUX FONCTIONNAIRES DEMANDANT LA LIQUIDATION DE LEUR PENSION À COMPTER DU 1ER JANVIER 2005 OU À UNE DATE POSTÉRIEURE - EXISTENCE, QUELLE QUE SOIT LA DATE DE RADIATION [RJ1] - 2) ESPÈCE - FONCTIONNAIRE AYANT DEMANDÉ ET OBTENU LA LIQUIDATION DE SA PENSION EN 1998 - APPLICATION DE L'ARTICLE L. 25 BIS DU CPCMR - ABSENCE, QUAND BIEN MÊME L'ENTRÉE EN VIGUEUR DE SA PENSION A ÉTÉ DIFFÉRÉE.
</SCT>
<ANA ID="9A"> 48-02-01 1) En principe, les droits à pension s'apprécient au regard de la législation en vigueur à la date de radiation des cadres. Toutefois, en l'absence de disposition législative contraire, le droit à l'abaissement de l'âge de soixante ans pour la liquidation de la pension de retraite, prévu par l'article L. 25 bis du code des pensions civiles et militaires de retraite (CPCMR), entré en vigueur le 1er janvier 2005, est applicable aux fonctionnaires qui demandent la liquidation de leur pension de retraite à compter de cette date ou d'une date postérieure, quelle que soit la date de leur radiation des cadres.,,,2) Fonctionnaire ayant demandé et obtenu la liquidation de sa pension de retraite en 1998. Dès lors, et quand bien même l'entrée en jouissance de sa pension a été différée, l'article 119 de la loi n° 2004-1484 du 30 décembre 2004, qui a inséré l'article L. 25 bis dans le CPCMR, ne pouvait s'appliquer à sa situation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 12 mars 2012, Caisse des dépôts et consignations c/ M. Delbal, n° 329967, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
