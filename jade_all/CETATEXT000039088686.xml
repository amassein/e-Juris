<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039088686</ID>
<ANCIEN_ID>JG_L_2019_09_000000431862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/08/86/CETATEXT000039088686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème chambre jugeant seule, 12/09/2019, 431862, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème chambre jugeant seule</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:431862.20190912</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 26 avril 2019, M. B... C... et Mme A... C... ont demandé au tribunal administratif de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur demande tendant à la réduction de la contribution sociale généralisée à laquelle ils ont été assujettis à raison de la plus-value retirée de la cession de valeurs mobilières réalisées en 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 3° du V-A de l'article 8 de la loi n° 2017-1836 du 30 décembre 2017 de financement de la sécurité sociale pour 2018. <br/>
<br/>
              Par une ordonnance n° 1903153 du 18 juin 2019, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 5ème chambre du tribunal administratif de Versailles a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre cette question au Conseil d'Etat.<br/>
<br/>
              Par la question prioritaire transmise et un nouveau mémoire, enregistrés le 19 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C... soutiennent que ces dispositions, applicables au litige, méconnaissent l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 en ce qu'elles s'appliquent rétroactivement à compter de l'imposition des revenus 2017. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 2017-1836 du 30 décembre 2017 ;<br/>
              - l'ordonnance du président de la section du contentieux du Conseil d'Etat n° 427813 du 6 mars 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de M. et Mme C... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 juin 2019, présentée par M. et Mme C....  <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 8 de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018 : " I.-Le code de la sécurité sociale est ainsi modifié : / (...) 6° L'article L. 136-8 est ainsi modifié : / (...) b) Au 2° du même I, le taux : " 8,2 % " est remplacé par le taux : " 9,9 % " ; / (...) V-A.-Les I et II du présent article s'appliquent : / (...) 3° A compter de l'imposition des revenus de l'année 2017, en ce qu'ils concernent la contribution mentionnée à l'article L. 136-6 du code de la sécurité sociale, sous réserve du II de l'article 34 de la loi n° 2016-1918 du 29 décembre 2016 de finances rectificative pour 2016 ; / (...) ". Aux termes de l'article L. 136-6 du code de la sécurité sociale : " I. Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à une contribution sur les revenus du patrimoine assise sur le montant net retenu pour l'établissement de l'impôt sur le revenu, à l'exception de ceux ayant déjà supporté la contribution au titre des articles L. 136-3, L. 136-4 et L. 136-7 : / (...) e) Des plus-values, gains en capital et profits soumis à l'impôt sur le revenu (...) ". <br/>
<br/>
              3. M. et Mme C... soutiennent, à l'appui de leur demande tendant à la réduction de la contribution sociale généralisée à laquelle ils ont été assujettis à raison de la plus-value retirée de la cession de valeurs mobilières réalisées en 2017, que les dispositions du 3° du V-A de l'article 8 de la loi du 30 décembre 2017 portent atteinte aux exigences de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 dès lors qu'elles rendent applicable à des cessions réalisées au cours de l'année 2017, antérieurement à leur entrée en vigueur, la hausse de contribution sociale généralisée de 1,7 point  prévue par le b du 6° du I de l'article 8 de cette loi. <br/>
<br/>
              4. Aux termes de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ". Il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou d'abroger ceux-ci en leur substituant, le cas échéant, d'autres dispositions. Ce faisant, il ne saurait toutefois priver de garanties légales des exigences constitutionnelles. En particulier, il ne saurait, sans motif d'intérêt général suffisant, ni porter atteinte aux situations légalement acquises ni remettre en cause les effets qui peuvent légitimement être attendus de telles situations.<br/>
<br/>
              5. Les dispositions contestées, qui sont applicables aux impositions dues en 2018 au titre de l'année 2017, modifient le taux de la contribution sociale généralisée sur les revenus du patrimoine antérieurement applicable. D'une part, aucune règle constitutionnelle n'en imposait le maintien et les requérants ne pouvaient légitimement s'attendre à ce que leur soit appliqué le taux en vigueur à la date de la cession, alors que si le transfert de propriété constitue le fait générateur de la plus-value, le fait générateur de l'imposition de cette dernière se situe au 31 décembre de l'année de la réalisation du revenu. D'autre part, contrairement à ce qui est soutenu, la loi de financement de la sécurité sociale pour 2018 est entrée en vigueur le 31 décembre 2017, conformément aux dispositions du dernier alinéa de son article 78. Par suite, le moyen tiré de ce que ces dispositions seraient contraires à la garantie des droits résultant de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 est dépourvu de caractère sérieux.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et Mme C....<br/>
Article 2 : La présente décision sera notifiée à M. et Mme C... et au ministre de l'action et des comptes publics.  <br/>
Copie en sera adressée au Premier ministre, au tribunal administratif de Versailles, au tribunal administratif de Cergy-Pontoise et au Conseil Constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
