<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044393191</ID>
<ANCIEN_ID>J4_L_2021_11_00020NT00305</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/39/31/CETATEXT000044393191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANTES, 5ème chambre, 30/11/2021, 20NT00305, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-30</DATE_DEC>
<JURIDICTION>CAA de NANTES</JURIDICTION>
<NUMERO>20NT00305</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. FRANCFORT</PRESIDENT>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Jérôme  FRANCFORT</RAPPORTEUR>
<COMMISSAIRE_GVT>M. MAS</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Par une ordonnance du 4 janvier 2020, le président de la section du contentieux du Conseil d'Etat a transmis pour attribution à la cour administrative d'appel de Nantes la requête de M. B... A..., enregistrée le 21 mars 2019 au secrétariat du contentieux du Conseil d'Etat, dirigée contre le jugement n° 1601142 du 21 janvier 2019 du tribunal administratif de Rennes.<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. B... A... a demandé au tribunal administratif de Rennes de condamner l'Etat à lui verser la somme de 3 806 986 euros en réparation du préjudice résultant, d'une part, de la faute commise par le commissaire du gouvernement exerçant la tutelle de la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile en s'abstenant de demander à cette caisse d'appliquer aux pensions déjà liquidées le décret n° 95-825 du 30 juin 1995 et, d'autre part, de la faute commise par le Premier ministre en procédant à une codification erronée du code de l'aviation civile. <br/>
<br/>
       Par un jugement n° 1601142 du 21 janvier 2019, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête et des mémoires, enregistrés le 27 janvier 2020 et le 9 juin 2021,             M.  A..., représenté par le cabinet Briard, demande à la cour :<br/>
<br/>
       1°) d'annuler ce jugement du tribunal administratif de Rennes du 21 janvier 2019 ;<br/>
<br/>
       2°) à titre principal, de faire droit à ses conclusions de première instance ;<br/>
<br/>
       3°) à titre subsidiaire, de surseoir à statuer et de saisir la Cour européenne des droits de l'homme, pour avis, de la question de savoir si la suppression des intitulés des sections IV à VI du chapitre VI figurant au livre IV du titre II du code de l'aviation civile dans l'édition papier de ce code à compter de 1987 et jusqu'au 20 février 2010 méconnaît l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er du premier protocole additionnel à cette convention en induisant la privation d'un bien et en instaurant une discrimination entre les affiliés de la caisse de retraite du personnel navigant professionnel de l'aéronautique civile ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat une somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
       Il soutient que :<br/>
<br/>
       Sur la régularité du jugement attaqué :<br/>
       - le jugement attaqué est insuffisamment motivé, eu égard à l'ampleur des développements de la requête, en ce qu'il écarte toute carence fautive du commissaire du gouvernement dans l'exercice de son pouvoir de tutelle de la Caisse ; <br/>
       - ce jugement souffre également d'une insuffisance de motivation en ce qu'il écarte tout lien de causalité entre la faute commise dans la rédaction erronée du code de l'aviation civile et le préjudice subi de manière insuffisamment argumentée, eu égard à l'ampleur des développements de la requête. <br/>
<br/>
       Sur le bien-fondé du jugement attaqué : <br/>
       - sa demande, qui tend à obtenir réparation d'une faute commise par l'Etat, diffère de celle présentée devant le juge judiciaire qui tend à la révision de sa pension de retraite et est, par suite, recevable ;<br/>
       - la faute commise par les services du Premier ministre, en omettant de reporter, dans l'édition papier de ce code à compter de 1987 et jusqu'au 20 février 2010, l'intitulé des sections IV à VI du chapitre VI figurant au livre IV du titre II du code de l'aviation civile, qui n'avaient pas été supprimées par le décret du 18 juin 1984 contrairement à ce qu'a jugé le tribunal administratif, a provoqué une confusion entre les règles relatives à la constitution du droit à pension et celles relatives au calcul de la pension, confusion qui a conduit au refus de lui appliquer les règles plus favorables, issues du décret du 30 juin 1995, pour le calcul de sa pension et a induit en erreur la Cour de Cassation qui a rejeté la demande des personnels concernés de se voir appliquer ces règles au motif, erroné, qu'elles avaient trait à la liquidation des droits à pension et ne pouvaient donc avoir d'effet rétroactif ;<br/>
       - en s'abstenant de demander à la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile d'appliquer les dispositions du décret du 30 juin 1995 et celles du décret du 27 mai 2005 aux retraités ayant liquidé leur droit à pension antérieurement à son entrée en vigueur, le commissaire du gouvernement placé auprès de la caisse a méconnu les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, interdisant toute discrimination, et celles de l'article 1er du protocole additionnel à cette convention, protégeant le droit de propriété, et a, ainsi, commis une faute lourde ayant conduit à le priver d'un complément de pension et à le traiter de manière moins favorable que les personnels ayant fait valoir leurs droits à la retraite postérieurement à l'entrée en vigueur du décret du 30 juin 1995 ; <br/>
       - les autorités de tutelle de la caisse de retraite du personnel navigant professionnel de l'aéronautique civile ont également commis une faute lourde en ne remettant pas en cause l'application par celle-ci de la minoration de pension instituée par le décret du 18 juin 1984 alors, d'une part, que ce mécanisme avait été implicitement mais nécessairement abrogé par la loi du 13 septembre 1984 supprimant la décote à la limite d'âge, alors que la décote instituée par le décret du 18 juin 1984, ayant trait à la définition de la nature des conditions exigées pour l'attribution de la retraite, étant intervenue dans un domaine réservé à la loi et pour cette raison illégale et, d'autre part, que la minoration des années prises en compte au-delà des vingt-cinq dernières années est également contraire à la loi n° 72-1223 du 29 décembre 1972, abrogée et reprise à l'article L. 921-3 du code de la sécurité sociale en ce qu'elle prévoit la validation de toutes les périodes travaillées, cotisées ou non.<br/>
<br/>
       Par des mémoires en défense, enregistrés le 17 décembre 2020 et le 2 juillet 2021, la ministre de la transition écologique, représentée par la SCP Lyon-Caen et Thiriez, conclut au rejet de la requête et à ce que la somme de 3 000 euros soit mise à la charge de M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Il soutient que :<br/>
- les conclusions indemnitaires présentées par M. A... sont irrecevables ;<br/>
- les moyens soulevés par M. A... ne sont pas fondés.<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - la décision n° 84-136 L du 28 février 1984 du Conseil constitutionnel ;<br/>
       - le code de l'aviation civile ;<br/>
       - la loi n° 72-1223 du 29 décembre 1972 ;<br/>
       - la loi n° 84-834 du 13 septembre 1984 ;<br/>
       - le décret n° 67-334 du 30 mars 1967 ;<br/>
       - le décret n° 84-469 du 18 juin 1984 ;<br/>
       - le décret n° 95-825 du 30 juin 1995 ;<br/>
       - le décret n° 2005-609 du 27 mai 2005 ;<br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Francfort, <br/>
       - et les conclusions de M. Mas, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. M. A... relève appel du jugement du 21 janvier 2019 par lequel le tribunal administratif de Rennes a rejeté sa demande, qui tendait à la condamnation de l'Etat à lui verser la somme de 3 806 986 euros en réparation du préjudice qu'il estime avoir subi à raison des fautes lourdes commises, d'une part par le Premier ministre en procédant à une codification erronée du code de l'aviation civile, d'autre part par le commissaire du gouvernement exerçant la tutelle de la caisse de retraite du personnel navigant professionnel de l'aéronautique civile en s'abstenant de demander à la caisse d'appliquer aux pensions déjà liquidées le décret n° 95-825 du 30 juin 1995, et enfin par les représentants des administrations exerçant la tutelle de la caisse en s'abstenant de remettre en cause la minoration de pension introduite par le décret du 18 juin 1984. <br/>
Sur la régularité du jugement attaqué :<br/>
       2. D'une part M. A... soutient que les premiers juges n'ont pas suffisamment motivé leur jugement en se fondant, pour écarter l'application du décret du 30 juin 1995 aux pensions liquidées antérieurement à son entrée en vigueur, sur le caractère suffisamment clair des dispositions de ce décret, sans toutefois les citer, et sur l'absence d'une disposition législative habilitant le pouvoir réglementaire à donner un effet rétroactif aux dispositions de ce décret, sans préciser l'incidence d'une telle absence. Toutefois, il ressort du jugement attaqué que les motifs ainsi critiqués ne sont pas ceux retenus par le tribunal administratif de Rennes dans son jugement du 21 janvier 2019. <br/>
       3. D'autre part, en indiquant qu'aucune faute lourde ne pouvait être retenue à l'encontre du commissaire du gouvernement exerçant la tutelle sur la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile, au regard de son abstention à inviter cette dernière à mettre en œuvre les dispositions plus favorables du décret du 30 juin 1995 aux pensions de retraite liquidées avant son entrée en vigueur, dès lors que la cour de cassation avait jugé que ces dispositions n'avaient aucune portée rétroactive, les premiers juges ont suffisamment répondu à l'argumentation que M. A... faisait valoir sur ce point.<br/>
       4. Il résulte de ce qui précède que le moyen tiré de ce que le jugement attaqué serait insuffisamment motivé, en contradiction avec les exigences résultant de l'article L. 9 du code de justice administrative ne peut qu'être écarté.<br/>
Sur le bien-fondé du jugement attaqué :<br/>
       En ce qui concerne la faute commise par le Premier ministre en ayant laissé subsister une version erronée du code de l'aviation civile au Journal officiel de la République française :<br/>
       5. M. A... soutient que, contrairement à ce qu'a jugé le tribunal administratif, les sections IV à VI du chapitre VI figurant au livre IV du titre II du code de l'aviation civile n'avaient pas été supprimées par le décret du 18 juin 1984, et que leur omission dans l'édition papier de ce code à compter de 1987 et jusqu'au 20 février 2010 a induit en erreur la Cour de Cassation en la conduisant à juger que les dispositions du décret du 30 juin 1995 étaient dépourvues de portée rétroactive. Toutefois, les dispositions du décret du 30 juin 1995 ne prévoyant en tout état de cause pas expressément leur application aux pensions déjà liquidées, ce dernier motif suffit à justifier légalement l'absence d'application rétroactive des dispositions dont M. A... se prévaut, si bien que le requérant ne peut utilement soutenir que l'omission de certains intitulés de section dans les versions papier du code de l'aviation civile aurait pu avoir une incidence sur l'interprétation des dispositions concernées.<br/>
       En ce qui concerne la faute lourde commise dans l'exercice de son pouvoir de tutelle  par le commissaire du gouvernement auprès de la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile :<br/>
<br/>
       6. En premier lieu, aux termes de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " La jouissance des droits et libertés reconnus dans la présente convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation ". Aux termes de l'article premier du premier protocole additionnel à cette convention : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international ". Une distinction entre des personnes placées dans une situation analogue est discriminatoire, si elle n'est pas assortie de justifications objectives et raisonnables, c'est-à-dire si elle ne poursuit pas un objectif d'utilité publique ou si elle n'est pas fondée sur des critères objectifs et rationnels en rapport avec les buts de la loi.<br/>
       7. Si le requérant soutient que le refus du commissaire du gouvernement de demander à la Caisse d'appliquer les dispositions du décret du 30 juin 1995 puis celles du décret du 27 mai 2005 aux pensions déjà liquidées serait constitutif d'une discrimination prohibée par l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et d'une privation des biens protégés par l'article 1er du premier protocole additionnel à cette convention, il est constant que les dispositions invoquées par le requérant ne prévoyaient pas expressément une application rétroactive. Ce seul motif suffit à justifier légalement le refus opposé par le commissaire du gouvernement, lequel n'a ainsi pas commis la faute qui lui est reprochée. A supposer que le moyen soulevé par les requérants soit dirigé contre le décret lui-même, il y a lieu de l'écarter dès lors qu'il existe, en tout état de cause, une différence objective de situation entre les personnels navigants selon qu'ils ont demandé la liquidation de leur pension avant ou après l'intervention de ce décret, tenant notamment aux contreparties, au titre desquelles un possible allongement de la durée de cotisation, dont était assortie la prise en compte progressive, à compter de l'entrée en vigueur du décret, de la totalité des annuités au-delà des vingt-cinq meilleures années.<br/>
      En ce qui concerne la carence fautive de l'organe de tutelle en s'abstenant de remettre en cause la minoration de pension introduite par le décret du 18 juin 1984 :<br/>
       8. En premier lieu la loi du 13 septembre 1984 relative à la limite d'âge dans la fonction publique et le secteur public n'est pas applicable au personnel navigant de l'aviation civile et, au demeurant, ne prévoit pas, dans sa version applicable à la date d'entrée en vigueur du décret du 30 juin 1995, la suppression de toute décote à la limite d'âge. Il suit de là que cette loi ne peut en aucun cas être regardée comme ayant implicitement abrogé le décret n° 84-469 du 18 juin 1984 en ce que ce dernier prévoit une décote et, par voie de conséquence, le décret n° 95-825 en tant qu'il modifie le décret du 18 juin 1984. Dès lors, et en tout état de cause, M. A... n'est pas fondé à soutenir qu'en s'abstenant de demander à la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile de ne pas faire application du décret du 18 juin 1984, les représentants de l'Etat et les commissaires du gouvernement successifs auprès de la caisse auraient commis une faute lourde dans l'exercice de leurs pouvoirs de tutelle de nature à engager la responsabilité de l'Etat.<br/>
       9. En deuxième lieu, par sa décision n° 84-136 L du 28 février 1984, le Conseil constitutionnel, sollicité par le Premier ministre pour se prononcer sur la nature juridique des dispositions de l'article L. 426-1 du code de l'aviation civile telles qu'elles résultent de la loi n° 72-1090 du 8 décembre 1972, a considéré que si, dans le régime complémentaire de retraite du personnel navigant de l'aéronautique civile, la définition de la nature des conditions exigées pour l'attribution de la retraite est au nombre des principes fondamentaux de la sécurité sociale qui relèvent, en vertu de l'article 34 de la Constitution, du domaine de la loi, il appartient au pouvoir réglementaire, sauf à dénaturer ces conditions, d'en préciser les éléments, tels que l'âge. Il a ainsi jugé que, si les dispositions en cause relèvent du domaine de la loi en tant qu'elles subordonnent l'acquisition du droit à la retraite à l'existence d'une condition d'âge ou qu'elles dispensent de cette condition les personnels devant cesser leur activité de navigant à la suite d'un accident ou d'une maladie consécutifs à l'exercice de la profession, elles ont au contraire un caractère réglementaire dans la mesure où elles se bornent à fixer l'âge de la retraite, et que, si la détermination des personnes assujetties à l'obligation de cotiser ainsi que le partage de cette obligation entre employeur et salarié constituent des principes fondamentaux réservés au législateur, le soin de fixer le taux de la part qui incombe à chacune de ces catégories de personnes entre dans la compétence du pouvoir réglementaire.<br/>
       10. Eu égard à ce qu'a ainsi jugé le Conseil constitutionnel, M. A... n'est pas fondé à soutenir que le principe d'une décote introduit par le décret du 18 juin 1984 tiendrait de la définition de la nature des conditions exigées pour l'attribution de la retraite et serait donc au nombre des principes fondamentaux de la sécurité sociale qui relèvent, en vertu de l'article 34           de la Constitution, du domaine de la loi. Il s'ensuit que le moyen tiré de ce que le décret du              18 juin 1984 serait entaché d'incompétence doit être écarté.<br/>
       11. En troisième lieu, ni la loi n° 72-1223 du 29 décembre 1972 portant généralisation de la retraite complémentaire au profit des salariés et anciens salariés, ni les dispositions de l'article L. 921-3 du code de la sécurité sociale reprenant ses dispositions, ne traitent de la validation de périodes travaillées. Ces textes n'ont pas davantage vocation à régir les modalités de calcul des pensions, ni à interdire l'institution d'une décote. Dans ces conditions, et en tout état de cause, il ne saurait être reproché au commissaire du gouvernement auprès de la caisse d'avoir méconnu ces textes en s'abstenant de demander à cette caisse d'appliquer aux pensions déjà liquidées les dispositions du décret du 30 juin 1995.<br/>
       12. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la ministre de la transition écologique, ni d'adresser une demande d'avis à la Cour européenne des droits de l'homme, que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a rejeté sa demande.<br/>
       Sur les frais liés au litige :<br/>
<br/>
       13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. A... demande au titre des frais exposés et non compris dans les dépens. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... une somme au titre des frais exposés par l'Etat et non compris dans les dépens. <br/>
<br/>
<br/>
DÉCIDE :<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Les conclusions de la ministre de la transition écologique présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Le présent arrêt sera notifié à M. B... A... et à la ministre de la transition écologique. <br/>
 	  Copie en sera adressée, pour information, au Premier ministre et à la Caisse de retraite du personnel navigant professionnel de l'aéronautique civile. <br/>
       Délibéré après l'audience du 15 novembre 2021, à laquelle siégeaient :<br/>
<br/>
       - M. Francfort, président de chambre,<br/>
       - Mme Buffet, présidente-assesseure,<br/>
       - M. Frank, premier conseiller.<br/>
<br/>
<br/>
       Rendu public par mise à disposition au greffe le 30 novembre 2021.<br/>
<br/>
<br/>
Le président-rapporteur,<br/>
J. FRANCFORTL'assesseure la plus ancienne, <br/>
 C. BUFFET        <br/>
<br/>
Le greffier,<br/>
C. GOY <br/>
<br/>
       La République mande et ordonne à la ministre de la transition écologique en ce qui la concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
3<br/>
N° 20NT00305<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
