<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464470</ID>
<ANCIEN_ID>JG_L_2015_11_000000384728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464470.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 09/11/2015, 384728, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Sophie-Justine Lieber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384728.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 24 septembre 2014 et 17 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 29 juillet 2014 par laquelle le Conseil national des barreaux a rejeté sa demande tendant à l'abrogation des alinéas 2 et 3 de l'article 10.6 du règlement intérieur national de la profession d'avocat ;<br/>
<br/>
              2°) à titre subsidiaire, de saisir la Cour de justice de l'Union européenne d'une question préjudicielle visant à savoir si, d'une part, l'article 24 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 doit être interprété comme s'opposant à une réglementation nationale apportant des restrictions aux communications des professions réglementées n'ayant pas le caractère de communications commerciales et notamment à celles permettant l'accès direct à l'entreprise, telles que le choix d'un nom de domaine ou d'une adresse de courrier électronique, et si, d'autre part, dans l'hypothèse d'une réponse négative à la première question, cette directive doit néanmoins être interprétée comme s'opposant à une réglementation nationale qui interdit aux membres d'une profession de services réglementée d'exploiter un nom de domaine autre que celui correspondant à leur nom ou leur dénomination exacte, éventuellement complété de l'intitulé de leur profession ; <br/>
<br/>
              3°) d'enjoindre au Conseil national des barreaux de procéder à l'abrogation de ces dispositions dans un délai de deux mois à compter de la notification de la décision à intervenir ; <br/>
<br/>
              4°) de mettre à la charge du Conseil national des barreaux la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2006/123/CE du 12 décembre 2006 du Parlement européen et du Conseil ;<br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - le décret n° 91-1197 du 37 novembre 1991 ;<br/>
              - le décret n° 2005-790 du 12 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Justine Lieber, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Conseil national des barreaux ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 53 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques dispose : " Dans le respect de l'indépendance de l'avocat, de l'autonomie des conseils de l'ordre et du caractère libéral de la profession, des décrets en Conseil d'Etat fixent les conditions d'application du présent titre. / Ils précisent notamment : (...) / 2° Les règles de déontologie (...) " ; que l'article 21-1 de la même loi, dans sa rédaction applicable au litige, dispose : " Dans le respect des dispositions législatives et réglementaires en vigueur, le Conseil national des barreaux unifie par voie de dispositions générales les règles et usages de la profession d'avocat " ; qu'aux termes de l'article 17 de la même loi, le conseil de l'ordre de chaque barreau a pour attribution : " (...) de traiter toutes questions intéressant l'exercice de la profession et de veiller à l'observation des devoirs des avocats ainsi qu'à la protection de leurs droits (...) " et a pour tâches, notamment : " 1° D'arrêter et, s'il y a lieu, de modifier les dispositions du règlement intérieur (...) / ; 10° D'assurer dans son ressort l'exécution des décisions prises par le Conseil national des barreaux " ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions citées au point 1 que le Conseil national des barreaux est investi par la loi d'un pouvoir réglementaire, qui s'exerce en vue d'unifier les règles et usages des barreaux et dans le cadre des lois et règlements qui régissent la profession ; que ce pouvoir trouve cependant sa limite dans les droits et libertés qui appartiennent aux avocats et dans les règles essentielles de l'exercice de la profession ; que le Conseil national des barreaux ne peut légalement fixer des prescriptions nouvelles qui mettraient en cause la liberté d'exercice de la profession d'avocat ou les règles essentielles qui la régissent et qui n'auraient aucun fondement dans les règles législatives ou dans celles fixées par les décrets en Conseil d'Etat prévus par l'article 53 de la loi du 31 décembre 1971, ou ne seraient pas une conséquence nécessaire d'une règle figurant au nombre des traditions de la profession ; <br/>
<br/>
              3. Considérant que l'article 3 du décret du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat dispose : " L'avocat exerce ses fonctions avec dignité, conscience, indépendance, probité et humanité, dans le respect des termes de son serment. / Il respecte en outre, dans cet exercice, les principes d'honneur, de loyauté, de désintéressement, de confraternité, de délicatesse, de modération et de courtoisie. / Il fait preuve, à l'égard de ses clients, de compétence, de dévouement, de diligence et de prudence " ; qu'aux termes du premier alinéa de l'article 15 du même décret : " La publicité et la sollicitation personnalisée sont permises à l'avocat si elles procurent une information sincère sur la nature des prestations de services proposées et si leur mise en oeuvre respecte les principes essentiels de la profession (...) " ;  <br/>
<br/>
              4. Considérant que les dispositions de l'article 10-6 du règlement intérieur national de la profession d'avocat, contestées par le requérant, prévoient, dans leur version issue de la décision du Conseil national des barreaux du 8 mai 2010, applicable au litige, que : " L'avocat qui ouvre ou modifie substantiellement un site Internet doit en informer le Conseil de l'Ordre sans délai et lui communiquer les noms de domaine qui permettent d'y accéder. / Le nom de domaine doit comporter le nom de l'avocat ou la dénomination du cabinet en totalité ou en abrégé, qui peut être suivi ou précédé du mot " avocat ". / L'utilisation de noms de domaine évoquant de façon générique le titre d'avocat ou un titre pouvant prêter à confusion, un domaine du droit ou une activité relevant de celles de l'avocat est interdite " ; que ces dispositions n'ont ni pour objet, ni pour effet de subordonner à des conditions nouvelles l'exercice de la profession d'avocat ; que, d'une part, l'intérêt général de la profession d'avocat, dont l'expression est confiée au Conseil national des barreaux, d'autre part, le respect des principes essentiels de la profession et des exigences déontologiques, et enfin le respect des règles relatives à la publicité permettent que celui-ci, au titre de sa mission d'harmonisation des usages et règles de la profession avec les lois et décrets en vigueur, précise, dans les conditions et limites énoncées au point 2, les conditions selon lesquelles un avocat peut choisir un nom de domaine pour son site Internet, de manière à éviter l'appropriation directe ou indirecte, via un nom de domaine générique, d'un domaine du droit ou d'un domaine d'activité que se partage la profession ; que le Conseil national des barreaux était, dès lors, compétent, sur le fondement des dispositions citées au point 1, pour édicter les règles contestées, qui ne méconnaissent pas la liberté d'exercice de la profession d'avocat ni les règles essentielles qui la régissent ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article 24 de la directive 2006/123/CE du 12 décembre 2006 du Parlement européen et du Conseil relative aux services dans le marché intérieur : " 1. Les Etats membres suppriment toutes les interdictions totales visant les communications commerciales des professions réglementées. / 2. Les Etats membres veillent à ce que les communications commerciales faites par les professions réglementées respectent les règles professionnelles conformes au droit communautaire, qui visent notamment l'indépendance, la dignité et l'intégrité de la profession ainsi que le secret professionnel, en fonction de la spécificité de chaque profession. Les règles professionnelles en matière de communications commerciales doivent être non discriminatoires, justifiées par une raison impérieuse d'intérêt général et proportionnées " ; que le paragraphe 12 de l'article 4 de la même directive définit la communication commerciale comme " toute forme de communication destinée à promouvoir directement ou indirectement, les biens, les services ou l'image d'une (...) personne exerçant une profession réglementée " mais précise, dans son a), que " les informations permettant l'accès direct à l'activité de l'entreprise, de l'organisation ou de la personne, notamment un nom de domaine ou une adresse de courrier électronique ne constituent pas en tant que telles des communications commerciales " ; qu'il résulte de ces dispositions que les informations relatives aux noms de domaine ne constituent pas une communication commerciale au sens du paragraphe 12 de l'article 4 de la directive précitée ; que, dès lors, les règles encadrant la dénomination des sites Internet des personnes ou des entreprises relevant de professions réglementées ne relèvent pas du champ des dispositions de la directive du 12 décembre 2006 s'imposant aux Etats membres ; qu'il suit de là que, sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, le moyen tiré de ce que les dispositions de l'article 10.6 du règlement intérieur national de la profession d'avocat encadrant les possibilités de dénomination des noms de domaine par les avocats seraient contraires à l'article 24 précité de la directive, ne peut être utilement soulevé ; <br/>
<br/>
              6. Considérant que les dispositions de l'article 10.6 du règlement intérieur national de la profession d'avocat, en assurant le respect des exigences déontologiques de la profession, fixées notamment aux articles 3 et 15 du décret du 12 juillet 2005, cités au point 3, poursuivent les objectifs d'intérêt général de protection de l'intégrité de la profession d'avocat, d'une part, et de bonne information du client, d'autre part ; qu'eu égard à ces objectifs, ces dispositions, en prohibant l'utilisation d'un nom de domaine générique par les avocats, ne portent pas d'atteinte disproportionnée ni au droit de propriété des avocats, ni à leur liberté de communication, ni, en tout état de cause, à la liberté d'entreprendre ; <br/>
<br/>
              7. Considérant que l'article 83 de la loi du 31 décembre 1971 dispose : " Tout ressortissant de l'un des Etats membres de la Communauté européenne peut exercer en France la profession d'avocat à titre permanent sous son titre professionnel d'origine, à l'exclusion de tout autre, si ce titre professionnel figure sur une liste fixée par décret " ; que l'article 201 du décret du 27 novembre 1991 organisant la profession d'avocat fixe, en application de ces dispositions, la liste des titres professionnels sous lesquels sont reconnus en France comme avocats les ressortissants des Etats membres ; qu'ainsi, si les dispositions du deuxième alinéa de l'article 10.6 du règlement intérieur national prévoient que le nom de domaine doit comporter le nom de l'avocat ou de son cabinet et peut être suivi ou précédé de la mention " avocat ", elles ne font pas obstacle à ce que les avocats ressortissants de l'Union européenne fassent mention, dans le nom de domaine qu'ils choisissent, de leur titre professionnel d'origine ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision du 29 juillet 2014 par laquelle le Conseil national des Barreaux a rejeté sa demande tendant à l'abrogation des alinéas 2 et 3 de l'article 10.6 du règlement intérieur national de la profession d'avocat ; que, par suite, les conclusions qu'il présente aux fins d'injonction et ses conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être  rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au Conseil national des barreaux. Copie en sera adressée à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
