<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587835</ID>
<ANCIEN_ID>JG_L_2021_05_000000450341</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587835.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/05/2021, 450341, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450341</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2021:450341.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1914101 du 4 mars 2021, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Cergy-Pontoise, avant de statuer sur la requête présentée par M. A... B... tendant à l'annulation de la décision par laquelle le préfet du Val-d'Oise a prolongé le délai d'exécution de la décision de son transfert aux autorités italiennes de six à dix-huit mois et à ce qu'il soit enjoint au préfet d'enregistrer sa demande d'asile en procédure normale, a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen la question suivante : la prorogation à dix-huit mois du délai de transfert d'un demandeur de protection internationale vers l'Etat membre responsable de l'examen de sa demande, sur la base d'un constat de fuite de l'intéressé, doit-elle être regardée comme une décision susceptible de faire l'objet d'un recours autonome en annulation pour excès de pouvoir ou cette prorogation ne peut-elle être contestée qu'à l'occasion d'un recours pour excès de pouvoir contre une décision ultérieure et dont elle conditionne la légalité '<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003, modifié par le règlement d'exécution (UE) n° 118/2014 de la Commission du 30 janvier 2014 ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Fabio Gennari, auditeur ;<br/>
<br/>
              - Les conclusions de M. Guillaume Odinet, rapporteur public ; <br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes de l'article 29 du règlement (UE) n° 604/2013 du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers : " 1. Le transfert du demandeur ou d'une autre personne visée à l'article 18, paragraphe 1, point c) ou d), de l'État membre requérant vers l'État membre responsable s'effectue conformément au droit national de l'État membre requérant, après concertation entre les États membres concernés, dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation par un autre État membre de la requête aux fins de prise en charge ou de reprise en charge de la personne concernée ou de la décision définitive sur le recours ou la révision lorsque l'effet suspensif est accordé conformément à l'article 27, paragraphe 3. (...) ; / 2. Si le transfert n'est pas exécuté dans le délai de six mois, l'État membre responsable est libéré de son obligation de prendre en charge ou de reprendre en charge la personne concernée et la responsabilité est alors transférée à l'État membre requérant. Ce délai peut être porté (...) à dix-huit-mois au maximum si la personne concernée prend la fuite. (...) ". Aux termes du paragraphe 2 de l'article 9 du règlement (CE) n° 1560/2003 du 2 septembre 2003, modifié par le règlement d'exécution (UE) n° 118/2014 de la Commission du 30 janvier 2014  : " Il incombe à l'État membre qui, pour un des motifs visés à l'article 29, paragraphe 2, du règlement (UE) n° 604/2013, ne peut procéder au transfert dans le délai normal de six mois à compter de la date de l'acceptation de la requête aux fins de prise en charge ou de reprise en charge de la personne concernée, ou de la décision finale sur le recours ou le réexamen en cas d'effet suspensif, d'informer l'État responsable avant l'expiration de ce délai. À défaut, la responsabilité du traitement de la demande de protection internationale et les autres obligations découlant du règlement (UE) n° 604/2013 incombent à cet État membre conformément aux dispositions de l'article 29, paragraphe 2, dudit règlement ".<br/>
<br/>
              2. II résulte des dispositions citées au point 1 du paragraphe 2 de l'article 29 du règlement n° 604/2013, combinées avec celles du règlement n° 1560/2003 modifié qui en porte modalités d'application, que si l'Etat membre sur le territoire duquel séjourne le demandeur d'asile a informé l'Etat membre responsable de l'examen de la demande, avant l'expiration du délai de six mois dont il dispose pour procéder au transfert de ce demandeur, qu'il n'a pu y être procédé du fait de la fuite de l'intéressé, l'Etat membre requis reste responsable de l'instruction de la demande d'asile pendant un délai de dix-huit mois, courant à compter de l'acceptation de la reprise en charge, dont dispose l'Etat membre sur le territoire duquel séjourne le demandeur pour procéder à son transfert.<br/>
<br/>
              3. La prolongation du délai de transfert, qui résulte du seul constat de fuite du demandeur et qui ne donne lieu qu'à une information de l'Etat responsable de la demande d'asile par l'État membre qui ne peut procéder au transfert du fait de cette fuite, a pour effet de maintenir en vigueur la décision de transfert aux autorités de l'Etat responsable et ne suppose pas l'adoption d'une nouvelle décision. Cette prolongation n'est ainsi qu'une des modalités d'exécution de la décision initiale de transfert et ne peut être regardée comme révélant une décision susceptible de recours. <br/>
<br/>
              4. Au demeurant, dans le cadre d'un recours contre une décision de transfert, l'expiration du délai de transfert, qui a pour conséquence que l'Etat requérant devient responsable de l'examen de la demande de protection internationale, prive d'objet le litige. Il appartient au juge saisi de le constater en prononçant un non-lieu à statuer. L'étranger peut en outre demander à l'administration de reconnaître la compétence de la France pour examiner sa demande d'asile et saisir le juge d'un éventuel refus fondé sur l'absence d'expiration du délai de transfert, le cas échéant dans le cadre d'une instance de référé. Il lui est également loisible de contester l'existence d'une cause de prolongation à l'appui d'un recours dirigé contre une mesure prise en vue de l'exécution du transfert, telle qu'une assignation à résidence, ou d'une mesure tirant les conséquences du constat de la fuite, telle que la limitation ou la suspension des conditions matérielles d'accueil. Dans ces différentes hypothèses, l'étranger peut ainsi se prévaloir de l'expiration du délai de transfert.<br/>
<br/>
<br/>
<br/>
<br/>
              5. Le présent avis sera notifié au tribunal administratif de Cergy-Pontoise, à M. A... B... et au ministre de l'intérieur. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - 1) PROLONGATION DU DÉLAI DE TRANSFERT EN CAS DE FUITE DU DEMANDEUR - DÉCISION RÉVÉLÉE SUSCEPTIBLE DE RECOURS - ABSENCE [RJ1] - 2) VOIES DE RECOURS PERMETTANT À L'INTÉRESSÉ DE SE PRÉVALOIR DU DÉLAI DE TRANSFERT - A) RECOURS CONTRE LA DÉCISION DE TRANSFERT [RJ2] - B) RECOURS CONTRE UN REFUS DE RECONNAÎTRE LA FRANCE RESPONSABLE DE L'EXAMEN - C) RECOURS CONTRE UNE MESURE PRISE EN VUE DE L'EXÉCUTION DU TRANSFERT [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - 1) PROLONGATION DU DÉLAI DE TRANSFERT EN CAS DE FUITE DU DEMANDEUR - DÉCISION RÉVÉLÉE SUSCEPTIBLE DE RECOURS - ABSENCE [RJ1] - 2) VOIES DE RECOURS PERMETTANT À L'INTÉRESSÉ DE SE PRÉVALOIR DU DÉLAI DE TRANSFERT - A) RECOURS CONTRE LA DÉCISION DE TRANSFERT [RJ2] - B) RECOURS CONTRE UN REFUS DE RECONNAÎTRE LA FRANCE RESPONSABLE DE L'EXAMEN - C) RECOURS CONTRE UNE MESURE PRISE EN VUE DE L'EXÉCUTION DU TRANSFERT [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - PROLONGATION DU DÉLAI DE TRANSFERT VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE LA DEMANDE D'ASILE EN CAS DE FUITE DU DEMANDEUR [RJ1].
</SCT>
<ANA ID="9A"> 095-02-03 II résulte du paragraphe 2 de l'article 29 du règlement (UE) n° 604/2013 du 26 juin 2013 (dit Dublin III), combiné avec le règlement (CE) n° 1560/2003 du 2 septembre 2003, que si l'Etat membre sur le territoire duquel séjourne le demandeur d'asile a informé l'Etat membre responsable de l'examen de la demande, avant l'expiration du délai de six mois dont il dispose pour procéder au transfert de ce demandeur, qu'il n'a pu y être procédé du fait de la fuite de l'intéressé, l'Etat membre requis reste responsable de l'instruction de la demande d'asile pendant un délai de dix-huit mois courant à compter de l'acceptation de la reprise en charge, dont dispose l'Etat membre sur le territoire duquel séjourne le demandeur pour procéder à son transfert.,,,1) La prolongation du délai de transfert, qui résulte du seul constat de fuite du demandeur et qui ne donne lieu qu'à une information de l'Etat responsable de la demande d'asile par l'État membre qui ne peut procéder au transfert, a pour effet de maintenir en vigueur la décision de transfert aux autorités de l'Etat responsable et ne suppose pas l'adoption d'une nouvelle décision. Cette prolongation n'est ainsi qu'une des modalités d'exécution de la décision initiale de transfert et ne peut être regardée comme révélant une décision susceptible de recours.... ,,2) a) Au demeurant, dans le cadre d'un recours contre une décision de transfert, l'expiration du délai de transfert, qui a pour conséquence que l'Etat requérant devient responsable de l'examen de la demande de protection internationale, prive d'objet le litige. Il appartient au juge saisi de le constater en prononçant un non-lieu à statuer.... ,,b) L'étranger peut en outre demander à l'administration de reconnaître la compétence de la France pour examiner sa demande d'asile et saisir le juge d'un éventuel refus fondé sur l'absence d'expiration du délai de transfert, le cas échéant dans le cadre d'une instance de référé.... ,,c) Il lui est également loisible de contester l'existence d'une cause de prolongation à l'appui d'un recours dirigé contre une mesure prise en vue de l'exécution du transfert, telle qu'une assignation à résidence, ou d'une mesure tirant les conséquences du constat de la fuite, telle que la limitation ou la suspension des conditions matérielles d'accueil. Dans ces différentes hypothèses, l'étranger peut se prévaloir de l'expiration du délai de transfert.</ANA>
<ANA ID="9B"> 15-05-045-05 II résulte du paragraphe 2 de l'article 29 du règlement (UE) n° 604/2013 du 26 juin 2013 (dit Dublin III), combiné avec le règlement (CE) n° 1560/2003 du 2 septembre 2003, que si l'Etat membre sur le territoire duquel séjourne le demandeur d'asile a informé l'Etat membre responsable de l'examen de la demande, avant l'expiration du délai de six mois dont il dispose pour procéder au transfert de ce demandeur, qu'il n'a pu y être procédé du fait de la fuite de l'intéressé, l'Etat membre requis reste responsable de l'instruction de la demande d'asile pendant un délai de dix-huit mois courant à compter de l'acceptation de la reprise en charge, dont dispose l'Etat membre sur le territoire duquel séjourne le demandeur pour procéder à son transfert.,,,1) La prolongation du délai de transfert, qui résulte du seul constat de fuite du demandeur et qui ne donne lieu qu'à une information de l'Etat responsable de la demande d'asile par l'État membre qui ne peut procéder au transfert, a pour effet de maintenir en vigueur la décision de transfert aux autorités de l'Etat responsable et ne suppose pas l'adoption d'une nouvelle décision. Cette prolongation n'est ainsi qu'une des modalités d'exécution de la décision initiale de transfert et ne peut être regardée comme révélant une décision susceptible de recours.... ,,2) a) Au demeurant, dans le cadre d'un recours contre une décision de transfert, l'expiration du délai de transfert, qui a pour conséquence que l'Etat requérant devient responsable de l'examen de la demande de protection internationale, prive d'objet le litige. Il appartient au juge saisi de le constater en prononçant un non-lieu à statuer.... ,,b) L'étranger peut en outre demander à l'administration de reconnaître la compétence de la France pour examiner sa demande d'asile et saisir le juge d'un éventuel refus fondé sur l'absence d'expiration du délai de transfert, le cas échéant dans le cadre d'une instance de référé.... ,,c) Il lui est également loisible de contester l'existence d'une cause de prolongation à l'appui d'un recours dirigé contre une mesure prise en vue de l'exécution du transfert, telle qu'une assignation à résidence, ou d'une mesure tirant les conséquences du constat de la fuite, telle que la limitation ou la suspension des conditions matérielles d'accueil. Dans ces différentes hypothèses, l'étranger peut se prévaloir de l'expiration du délai de transfert.</ANA>
<ANA ID="9C"> 54-01-01-02 II résulte du paragraphe 2 de l'article 29 du règlement (UE) n° 604/2013 du 26 juin 2013 (dit Dublin III), combiné avec le règlement (CE) n° 1560/2003 du 2 septembre 2003, que si l'Etat membre sur le territoire duquel séjourne le demandeur d'asile a informé l'Etat membre responsable de l'examen de la demande, avant l'expiration du délai de six mois dont il dispose pour procéder au transfert de ce demandeur, qu'il n'a pu y être procédé du fait de la fuite de l'intéressé, l'Etat membre requis reste responsable de l'instruction de la demande d'asile pendant un délai de dix-huit mois courant à compter de l'acceptation de la reprise en charge, dont dispose l'Etat membre sur le territoire duquel séjourne le demandeur pour procéder à son transfert.,,,La prolongation du délai de transfert, qui résulte du seul constat de fuite du demandeur et qui ne donne lieu qu'à une information de l'Etat responsable de la demande d'asile par l'État membre qui ne peut procéder au transfert, a pour effet de maintenir en vigueur la décision de transfert aux autorités de l'Etat responsable et ne suppose pas l'adoption d'une nouvelle décision. Cette prolongation n'est ainsi qu'une des modalités d'exécution de la décision initiale de transfert et ne peut être regardée comme révélant une décision susceptible de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de l'absence de naissance d'une nouvelle décision de transfert, CE, 21 octobre 2015, Ministre de l'Intérieur c/ Mme,, n° 391375, T. pp. 557-582.,,[RJ2] Cf., sur le non-lieu dans un tel cas, CE, 27 mai 2019, Ministre de l'intérieur c/ M. et Mme,, n° 421276, T. pp. 574-622-927.,,[RJ3] Cf. CE, 26 juillet 2018, Mme,, n° 417441, p. 324.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
