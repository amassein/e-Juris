<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032960361</ID>
<ANCIEN_ID>JG_L_2016_07_000000401380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/96/03/CETATEXT000032960361.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/07/2016, 401380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:401380.20160726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 11 juillet 2016 au secrétariat du Conseil d'Etat, l'association des musulmans de Lagny-sur-Marne demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du 6 mai 2016 portant dissolution de l'" Association des musulmans de Lagny-sur-Marne " ;<br/>
<br/>
              		     2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - sa requête est recevable, dès lors que, d'une part, elle a qualité et intérêt pour agir et que, d'autre part, le Conseil d'Etat est compétent pour connaître de cette demande ;<br/>
              - la condition d'urgence est remplie eu égard, d'une part, à l'obstacle que constitue la dissolution litigieuse pour l'exercice du culte musulman dans la commune de Lagny-sur-Marne et, d'autre part, aux conséquences matérielles et financières qu'elle entraîne ;<br/>
              - le décret contesté porte une atteinte grave et manifestement illégale aux libertés fondamentales d'association, de conscience et de religion ;<br/>
              - il est entaché d'une erreur de droit en ce qu'il méconnaît les principes de liberté d'association, de conscience et de religion ;<br/>
              - il est entaché de plusieurs erreurs de fait et d'une erreur manifeste d'appréciation en ce que la dissolution de l'association requérante n'entre dans aucun des cas prévus par l'article L. 212-1 du code de la sécurité intérieure.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 juillet 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi du 1er juin 1901 relative au contrat d'association ;<br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ;<br/>
              - le code de la sécurité intérieure ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association des musulmans de Lagny-sur-Marne, d'autre part, le Premier ministre et le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 juillet 2016 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association des musulmans de Lagny-sur-Marne ;<br/>
<br/>
              - les représentants de l'association des musulmans de Lagny-sur-Marne ;<br/>
              - la représentante du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 521-2 du code de justice administrative, le juge des référés,  saisi d'une demande en ce sens justifiée par l'urgence, peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté  une atteinte grave et manifestement illégale.<br/>
<br/>
              2. Aux termes de l'article L. 212-1 du code de la sécurité intérieure : " Sont dissous, par décret en conseil des ministres, toutes les associations ou groupements de fait  : / 6° (...) qui, soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, une nation, une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence ; / 7° Ou qui se livrent, sur le territoire français ou à partir de ce territoire, à des agissements en vue de provoquer des actes de terrorisme en France ou à l'étranger (...) ". <br/>
<br/>
              3. Par décret du 14 janvier 2016, publié au Journal officiel le 15 janvier 2016, le Président de la République a, sur le fondement des dispositions du 6° de l'article L. 212-1 du code de la sécurité intérieure, prononcé la dissolution des associations " Retour aux sources ", " Le retour aux sources musulmanes " et " Association des musulmans de Lagny-sur-Marne ". Par une ordonnance du 30 mars 2016, le juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de ce décret en tant qu'il a dissout l'" Association des musulmans de Lagny-sur-Marne " au terme d'une procédure irrégulière. Par un décret du 6 mai 2016, publié au Journal officiel du 7 mai 2016, le Président de la République a retiré le décret du 14 janvier 2016 en tant qu'il porte dissolution de l'" Association des musulmans de Lagny-sur-Marne " et a prononcé à nouveau la dissolution de cette association sur le fondement des dispositions précitées du code de la sécurité intérieure. L'association a saisi le juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à la suspension de l'exécution de ce décret.<br/>
<br/>
              4. Pour prononcer la dissolution contestée, le Président de la République s'est fondé sur ce que l' " Association des musulmans de Lagny-sur-Marne "  participait, en lien étroit avec deux autres associations qui ont également fait l'objet d'une mesure de dissolution, à des activités d'endoctrinement, de recrutement et d'acheminement de candidats vers le jihad armé, entretenait des liens avec des personnes mises en cause dans des opérations de terrorisme et avait ainsi le caractère d'un groupement provoquant à la discrimination, à la haine ou à la violence envers un groupe de personnes en raison de leur non-appartenance à une religion au sens du 6° de l'article L. 212-1 du code de la sécurité intérieure et pouvait être regardée comme se livrant sur le territoire français à des agissements en vue de provoquer des actes de terrorisme en France ou à l'étranger au sens du 7° de cet article.<br/>
<br/>
              5. Il résulte de l'instruction écrite et des débats à l'audience que l' " Association des musulmans de Lagny " a contribué, avec deux autres associations, " Retour aux sources " et " Retour aux sources musulmanes ", auxquelles elle était étroitement liée, à propager l'idéologie de l'ancien imam de la mosquée de Lagny, lui-même parti en Egypte à la fin de 2014,  qui prônait un islamisme radical, appelant au rejet des valeurs de la République et faisant l'apologie du djihad armé ainsi que de la mort en martyr.  Des " notes blanches " précises et circonstanciées versées au débat contradictoire montrent que des membres de l'association ont activement participé à des filières de recrutement et d'acheminement vers la zone irako-syrienne. Même s'il n'est pas contesté que les perquisitions administratives effectuées le 2 décembre 2015 au domicile du président de " l'Association des musulmans de Lagny-sur-Marne " n'ont pas permis de découvrir des éléments susceptibles de révéler des activités à caractère terroriste, ces mêmes perquisitions ont révélé qu'un des dirigeants de fait de l'association  avait installé à son domicile une école coranique clandestine qui diffusait des messages appelant au jihad. Eu égard à l'ensemble de ces éléments, la dissolution contestée n'a pas porté aux libertés fondamentales que sont les libertés de religion, de conscience et d'association d'atteinte grave et manifestement illégale.  <br/>
<br/>
              6. Il  résulte de ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de l' " Association des musulmans de Lagny "  doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association des musulmans de Lagny-sur-Marne est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association des musulmans de Lagny-sur-Marne et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre et à la commune de Lagny-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
