<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240952</ID>
<ANCIEN_ID>JG_L_2021_03_000000433889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 05/03/2021, 433889, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Vaullerin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433889.20210305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 433889, par une requête et un mémoire en réplique, enregistrés les 23 août 2019 et 31 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le Comité interprofessionnel des huiles essentielles françaises, la société Florame, la société Hyteck Aroma-Zone, la société Laboratoires Gilbert, la société Laboratoire Léa Nature, la société Laboratoires Oméga Pharma France, la société Pierre Fabre médicaments, la société Pranarom France et la société Puressentiel France demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-642 du 26 juin 2019 relatif aux pratiques commerciales prohibées pour certaines catégories de produits biocides ;<br/>
<br/>
              2°) de saisir, le cas échéant, la Cour de justice de l'Union européenne d'une question préjudicielle relative à l'harmonisation exhaustive réalisée par le règlement européen n° 528/2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 433890, par une requête et un mémoire en réplique, enregistrés les 23 août 2019 et 31 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le Comité interprofessionnel des huiles essentielles françaises (CIHEF), la société Florame, la société Hyteck Aroma-Zone, la société Laboratoires Gilbert, la société Laboratoire Léa Nature, la société Laboratoires Oméga Pharma France, la société Pierre Fabre médicaments, la société Pranarom France et la société Puressentiel France demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-643 du 26 juin 2019 relatif à la publicité commerciale pour certaines catégories de produits biocides ;<br/>
<br/>
              2°) de saisir, le cas échéant, la Cour de justice de l'Union européenne d'une question préjudicielle relative à l'harmonisation exhaustive réalisée par le règlement européen n° 528/2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 267 ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - le règlement UE n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 ;<br/>
              - la directive 2000/31/CE du Parlement européen et du Conseil du 8 juin 2000 ;<br/>
              - le code de l'environnement ; <br/>
              - la loi n° 2018-938 du 30 octobre 2018 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 10 février 2021, présentées par la ministre de la transition écologique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes visées ci-dessus tendent à l'annulation de deux décrets pris en application de la même loi et présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Le règlement (UE) n°528/2012 du Parlement européen et du Conseil du 22 mai 2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides vise, aux termes du 1 de son article 1er, " à améliorer le fonctionnement du marché intérieur par l'harmonisation des règles concernant la mise à disposition sur le marché et l'utilisation des produits biocides, tout en assurant un niveau élevé de protection de la santé humaine et animale et de l'environnement. Ses dispositions se fondent sur le principe de précaution dont le but est la préservation de la santé humaine, de la santé animale et de l'environnement. Il convient d'accorder une attention particulière à la protection des groupes vulnérables ". Aux termes du 2 du même article : " Le présent règlement établit les règles régissant: a) l'établissement, au niveau de l'Union, d'une liste de substances actives pouvant être utilisées dans les produits biocides; / b) l'autorisation des produits biocides; / c) la reconnaissance mutuelle des autorisations à l'intérieur de l'Union; / d) la mise à disposition sur le marché et l'utilisation des produits biocides dans un ou plusieurs États membres ou dans l'Union ; / e) la mise sur le marché des articles traités". En vertu de l'article 72 du même règlement : " 1. Toute publicité pour des produits biocides, outre le respect des dispositions du règlement (CE) n° 1272/2008 comporte les phrases " Utilisez les produits biocides avec précaution. Avant toute utilisation, lisez l'étiquette et les informations concernant le produit. ". Ces phrases ressortent clairement dans la publicité et sont facilement lisibles. / 2. Les annonceurs peuvent remplacer le mot " biocides " dans les phrases obligatoires par une référence claire au type de produit visé par la publicité. / 3. Les publicités pour des produits biocides ne font pas référence au produit d'une manière susceptible de tromper l'utilisateur quant aux risques qu'il peut présenter pour la santé humaine, pour la santé animale ou pour l'environnement ou quant à son efficacité. En tout état de cause, la publicité pour un produit biocide ne comporte pas les mentions " produit biocide à faible risque ", " non toxique ", " ne nuit pas à la santé ", " naturel ", " respectueux de l'environnement ", " respectueux des animaux " ou toute autre indication similaire ". <br/>
<br/>
              3. Aux termes du nouvel article L. 522-18 du code de l'environnement, créé par l'article 76 de la loi du 30 octobre 2018 pour l'équilibre des relations commerciales dans le secteur agricole et alimentaire et une alimentation saine, durable et accessible à tous : " A l'occasion de la vente de produits biocides définis à l'article L. 522-1, les remises, les rabais, les ristournes, la différenciation des conditions générales et particulières de vente au sens de l'article L. 441-1 du code de commerce ou la remise d'unités gratuites et toutes pratiques équivalentes sont interdits. Toute pratique commerciale visant à contourner, directement ou indirectement, cette interdiction par l'attribution de remises, de rabais ou de ristournes sur une autre gamme de produits qui serait liée à l'achat de ces produits est prohibée. / Un décret en Conseil d'Etat précise les catégories de produits concernés en fonction des risques pour la santé humaine et pour l'environnement ". Aux termes du nouvel article L. 522-5-3 du code de l'environnement, créé par la loi du 30 octobre 2018 : " Toute publicité commerciale est interdite pour certaines catégories de produits biocides définies par le règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 précité. / Par dérogation au premier alinéa du présent article, la publicité destinée aux utilisateurs professionnels est autorisée dans les points de distribution de produits à ces utilisateurs et dans les publications qui leur sont destinées. / Un décret en Conseil d'Etat définit les catégories de produits concernés en fonction des risques pour la santé humaine et pour l'environnement ainsi que les conditions dans lesquelles les insertions publicitaires sont présentées. Ces insertions publicitaires mettent en avant les bonnes pratiques dans l'usage et l'application des produits pour la protection de la santé humaine et animale et pour l'environnement ainsi que les dangers potentiels pour la santé humaine et animale et pour l'environnement ".<br/>
<br/>
              4. Le décret attaqué n° 2019-642 du 26 juin 2019, pris en application du nouvel article L 522-18 du code de l'environnement, crée dans ce code un article R. 522-16-1, qui dispose que : " Les catégories de produits mentionnées à l'article L. 522-18, pour lesquels certaines pratiques commerciales sont prohibées, sont les produits relevant des types 14 et 18 définis par le règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides. / Ces dispositions ne s'appliquent pas aux produits biocides admissibles à la procédure d'autorisation simplifiée conformément à l'article 25 du même règlement ". Le décret attaqué n° 2019-643 du 26 juin 2019, pris en application de l'article L. 522-5-3 du code de l'environnement, insère dans ce code un nouvel article R. 522-16-2 ainsi rédigé : " I.- Les catégories de produits biocides mentionnées à l'article L. 522-5-3, pour lesquels il est interdit de faire de la publicité commerciale à destination du grand public, sont les suivantes : 1° Les produits relevant des types 14 et 18 définis par le règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides ; / 2° Les produits appartenant aux types 2 et 4 définis par ce même règlement et classés, selon les dispositions du règlement (CE) n° 1272/2008 du Parlement européen et du Conseil du 16 décembre 2008 relatif à la classification, à l'étiquetage et à l'emballage des substances et des mélanges, comme dangereux pour le milieu aquatique de catégorie 1 : toxicité aiguë de catégorie 1 (H 400) et toxicité chronique de catégorie 1 (H 410) . / II.- Pour les produits mentionnés au I, toute publicité à destination des professionnels est rédigée dans le respect des dispositions de l'article 72 du règlement (UE) n° 528/2012 mentionné au 1° du I. Elle fait, en outre, apparaître, de manière claire et lisible, les éléments suivants : 1° Deux phrases ainsi rédigées : " Avant toute utilisation, assurez-vous que celle-ci est indispensable, notamment dans les lieux fréquentés par le grand public. Privilégiez chaque fois que possible les méthodes alternatives et les produits présentant le risque le plus faible pour la santé humaine et animale et pour l'environnement. "/ 2° La mention du type de produits biocides associé au produit, tel que défini par l'annexe V du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 mentionné précédemment. / III.- Les dispositions du présent article ne s'appliquent pas aux produits biocides admissibles à la procédure d'autorisation simplifiée conformément à l'article 25 du règlement (UE) n° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides ".<br/>
<br/>
              5. Il résulte des dispositions législatives et réglementaires citées aux points 3 et 4 que certaines pratiques commerciales sont interdites, comme les remises, rabais, ristournes, différenciations des conditions générales et particulières de vente au sens de l'article L. 441-1 du code de commerce, remises d'unités gratuites et toute pratique équivalente, de même que la publicité commerciale à destination du grand public pour les produits biocides luttant contre les rongeurs et les arthropodes relevant des types 14 et 18 de l'annexe V du règlement du 22 mai 2012, à l'exclusion des produits biocides admissibles à la procédure d'autorisation simplifiée conformément à l'article 25 du même règlement.<br/>
<br/>
              6. En premier lieu, s'il n'est pas contesté que les deux décrets attaqués ont pour effet d'interdire certaines pratiques commerciales et la publicité vers le grand public pour certains produits biocides que les sociétés requérantes commercialisent, le moyen tiré de ce qu'ils seraient de nature à porter atteinte à leur droit de propriété protégé par l'article 17 de la Charte des droits fondamentaux de l'Union européenne n'est pas assorti de précisions suffisantes permettant d'en apprécier le bien-fondé et ne peut, dès lors, qu'être écarté.<br/>
<br/>
              7. En deuxième lieu, si les requérants soutiennent que les deux décrets attaqués sont susceptibles d'avoir pour effet une baisse des ventes de leurs produits et une perte de chiffre d'affaires, l'interdiction des pratiques commerciales et de la publicité pour le grand public qu'ils prévoient est justifiée par des objectifs de protection de la santé publique et de préservation de l'environnement. Les seules circonstances invoquées par les requérantes, à les supposer établies, ne sauraient caractériser une méconnaissance, par les décrets attaqués, des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8. En troisième lieu, si les requérants soutiennent que le décret du 26 juin 2019 relatif à la publicité commerciale pour certaines catégories de produits biocides  porte une atteinte excessive au droit à la liberté d'expression garanti par l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la limitation, prévue par l'article L. 522-5-3 du code de l'environnement précité, de la publicité commerciale à destination du grand public pour les produits biocides luttant contre les rongeurs et les arthropodes ne prive en tout état de cause pas les consommateurs d'accès à l'information et constitue, dans l'objectif de protection de la santé publique, une mesure nécessaire et proportionnée. Par suite, ce moyen doit être écarté. <br/>
<br/>
              9. En quatrième lieu, la directive 2000/31 du 8 juin 2000 relative à certains aspects juridiques des services de la société de l'information, et notamment du commerce électronique, dans le marché intérieur fixe le régime de la liberté de circulation des services et prévoit les conditions dans lesquels un Etat membre peut apporter à cette liberté des restrictions proportionnées dans un objectif d'intérêt général. En prévoyant des mesures nécessaires et proportionnées à l'objectif de protection de la santé publique qu'il poursuit, le décret du 26 juin 2019 relatif aux pratiques commerciales prohibées pour certaines catégories de produits biocides ne méconnaît pas la directive. <br/>
<br/>
              10. En cinquième lieu, il résulte des dispositions des articles L. 522-5-3 et L. 522-18 du code de l'environnement citées au point 3 que l'interdiction de certaines pratiques commerciales et de la publicité au grand public n'est pas générale et absolue mais vise des catégories de produits biocides en fonction des risques pour la santé humaine et pour l'environnement qu'il revient au pouvoir réglementaire de préciser. Contrairement à ce qui est soutenu, il ressort des pièces des dossiers que, pour déterminer les produits concernés par les mesures d'interdiction, notamment pour exclure du régime d'interdiction les produits biocides admissibles à la procédure d'autorisation simplifiée prévue à l'article 25 du règlement cité au point 2, le pouvoir réglementaire a pris en compte les risques pour la santé humaine et pour l'environnement propres à chaque produit. Dès lors, le comité interprofessionnel des huiles essentielles françaises et les sociétés requérantes ne sont pas fondés à soutenir que les décrets attaqués méconnaîtraient sur ce point les articles L. 522-18 et L. 522-5-3 du code de l'environnement. <br/>
<br/>
              11. En sixième lieu, si les requérants soutiennent que les décrets attaqués ont introduit une discrimination injustifiée entre les produits soumis aux mesures d'interdiction qu'ils prévoient et ceux qui, appartenant également au groupe 3 " produits de lutte contre les nuisibles " mentionné à l'annexe 5 du règlement du 22 mai 2012, en sont exclus, ils ne ressort pas des pièces des dossiers que les produits exclus contiendraient les mêmes substances actives ni que leurs usages seraient comparables. Dès lors, en prévoyant que les catégories 14 et 18 du groupe 3 seraient seules concernées par les interdictions qu'ils prévoient, les décrets attaqués ne méconnaissent pas, en tout état de cause, le principe d'égalité.<br/>
<br/>
              12. En dernier lieu, dès lors que le règlement cité au point 2 ne comporte aucune disposition autorisant un Etat membre à prévoir des mesures restrictives du type de celles qui figurent aux articles L. 522-18 et L. 522-5-3 du code de l'environnement ni le lui interdisant, la question se pose de savoir si de telles mesures, non prévues par le règlement, peuvent être prises sans déroger ou porter atteinte à ce règlement et sans faire obstacle à son bon fonctionnement. Les dispositions législatives en application desquelles ont été prises les dispositions réglementaires attaquées ont pour objectif de prévenir, pour certains produits biocides, les inconvénients que leur usage excessif présente pour la santé publique et l'environnement. Si cet objectif n'est pas en contradiction avec ceux du règlement européen précité, les interdictions que prévoient ces dispositions législatives interviennent dans le champ de la mise sur le marché des produits biocides, que le règlement a pour objet d'harmoniser au niveau européen, sans renvoyer à l'adoption de textes d'application par les Etats membres et sans que de tels textes d'application soient rendus nécessaires pour sa pleine efficacité. La réponse au moyen tiré de ce que les décrets attaqués auraient été pris en application de dispositions législatives adoptées en méconnaissance du règlement (UE) n ° 528/2012 du Parlement européen et du Conseil du 22 mai 2012 dépend de la réponse à la question de savoir si ce règlement fait obstacle à ce que le législateur national adopte, dans l'intérêt de la santé publique et de l'environnement, des règles restrictives en matière de pratiques commerciales et de publicité telles que celles que prévoient les articles L. 522-18 et L. 522-5-3 du code de l'environnement. <br/>
<br/>
              13. Cette question est déterminante pour la solution du litige que doit trancher le Conseil d'Etat et elle présente une difficulté sérieuse. Il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur les requêtes du comité interprofessionnel des huiles essentielles françaises et des sociétés Florame et autres.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur les requêtes nos 433889 et 433890 jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante : Le règlement du 22 mai 2012 concernant la mise à disposition sur le marché et l'utilisation des produits biocides s'oppose-t-il à ce qu'un Etat membre adopte, dans l'intérêt de la santé publique et de l'environnement, des règles restrictives en matière de pratiques commerciales et de publicité telles que celles que prévoient les articles L. 522-18 et L. 522-5-3 du code de l'environnement ' Le cas échéant, sous quelles conditions un Etat membre peut-il adopter de telles mesures ' <br/>
Article 2 : La présente décision sera notifiée au comité interprofessionnel des huiles essentielles françaises, premier requérant dénommé pour l'ensemble des requérants, à la ministre de la transition écologique et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
