<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044099032</ID>
<ANCIEN_ID>JG_L_2021_09_000000456650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/09/90/CETATEXT000044099032.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/09/2021, 456650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456650.20210921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 10 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... et les autres requérants dont le nom figure sur cette requête demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-1059 du 7 août 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - l'urgence n'est pas douteuse ;<br/>
              - ils justifient d'un intérêt à agir rendant leur demande recevable ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté, en ce qu'il est entaché de détournement de pouvoir, qu'il porte une atteinte disproportionnée au droit de disposer de son corps garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qu'il institue une interdiction disproportionnée au regard de l'évolution de l'épidémie de covid-19, du taux de mortalité, des effets de la vaccination et des autres mesures susceptibles d'être mises en place, que l'institution du passe sanitaire est disproportionnée pour le personnel des transports aériens, qu'elle y est dangereuse pour la sécurité, que, subsidiairement, il porte une atteinte disproportionnée au droit au respect de la vie privée et à l'interdiction des discriminations à raison de la santé.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes du premier alinéa de l'article R. 522-1 du code de justice administrative : " La requête visant au prononcé de mesures d'urgence doit (...) justifier de l'urgence de l'affaire ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il appartient au juge des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative d'une demande tendant à la suspension d'une décision administrative, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de cette décision sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence, qui doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'espèce, justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. L'office du juge des référés, saisi de conclusions à fin de suspension, le conduit à porter sur l'urgence une appréciation objective, concrète et globale, au vu de l'ensemble des intérêts en présence, afin de déterminer si, dans les circonstances particulières de chaque affaire, il y a lieu d'ordonner une mesure conservatoire à effet provisoire dans l'attente du jugement au fond de la requête à fin d'annulation de la décision contestée.<br/>
<br/>
              3. Contrairement aux prescriptions de l'article R. 522-1 du code de justice administrative, la requête, qui se borne à affirmer que l'urgence n'est pas douteuse du fait de l'entrée en vigueur des dispositions critiquées, ne justifie pas de l'urgence qu'il y aurait à suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution des dispositions du décret contesté. Il s'ensuit que la requête, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut qu'être rejetée par application de l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A..., première requérante dénommée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
