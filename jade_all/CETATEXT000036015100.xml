<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036015100</ID>
<ANCIEN_ID>JG_L_2017_11_000000402951</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/01/51/CETATEXT000036015100.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 08/11/2017, 402951</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402951</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402951.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 27 mars 2012 du président du conseil d'administration du service départemental d'incendie et de secours (SDIS) du Finistère le plaçant en congé pour raison opérationnelle sans constitution de droits à pension. Par un jugement n° 1202194 du 11 juin 2014, le tribunal administratif de Rennes a fait droit à sa demande et a annulé cet arrêté du 27 mars 2012. Par un arrêt n° 14NT02279 du 30 juin 2016, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la demande de M. A... présentée devant le tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 août et 28 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du service départemental d'incendie et de secours du Finistère une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 2000-628 du 7 juillet 2000 ;<br/>
              - le décret n° 2005-372 du 20 avril 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. A...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du service départemental d'incendie et de secours du Finistère.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., sapeur-pompier professionnel affecté au service départemental d'incendie et de secours (SDIS) du Finistère, a, sur sa demande, été déclaré inapte aux fonctions opérationnelles par des avis de la commission médicale et de la commission départementale de réforme. Il a sollicité auprès de son employeur l'octroi d'un congé pour raison opérationnelle en raison de son état de santé. Après avoir fait à M. A... une proposition d'affectation non-opérationnelle en qualité de sapeur-pompier stationnaire au centre de secours à Brest, que celui-ci a refusée, le président du conseil d'administration du SDIS du Finistère a, par un arrêté du 27 mars 2012, placé l'intéressé, à compter du 1er avril 2012, en congé pour raison opérationnelle sans constitution de droits à pension. M. A...a saisi le tribunal administratif de Rennes pour demander l'annulation de cet arrêté en tant qu'il ne lui accordait pas la constitution de droits à pension durant le temps de ce congé. Par un jugement du 11 juin 2014, le tribunal administratif de Rennes a annulé l'arrêté du 27 mars 2012 et a enjoint au SDIS de réexaminer la situation de M. A... et de prendre une nouvelle décision. M. A... se pourvoit en cassation contre l'arrêt du 30 juin 2016 par lequel la cour administrative d'appel de Nantes a annulé ce jugement et rejeté sa demande d'annulation de l'arrêté du 27 mars 2012.<br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 7 juillet 2000 relative à la prolongation du mandat et à la date de renouvellement des conseils d'administration des services départementaux d'incendie et de secours ainsi qu'au reclassement et à la cessation anticipée d'activité des sapeurs-pompiers professionnels : " Le sapeur-pompier professionnel âgé d'au moins cinquante ans peut demander qu'une commission médicale constituée à cet effet constate qu'il rencontre des difficultés incompatibles avec l'exercice des fonctions opérationnelles relevant des missions confiées aux services d'incendie et de secours. Lorsque c'est le cas, il bénéficie d'un projet de fin de carrière qui peut consister dans l'affectation à des fonctions non opérationnelles au sein du service d'incendie et de secours, en un reclassement dans un autre corps, cadre d'emplois ou emploi de la fonction publique ou en un congé pour raison opérationnelle, dans les conditions prévues aux articles suivants. (...) La décision accordant à un sapeur-pompier professionnel le bénéfice d'une affectation non opérationnelle, d'un reclassement ou d'un congé pour raison opérationnelle ne peut être prise qu'après acceptation écrite de l'intéressé. (...) ". Aux termes de l'article 4 de la même loi : " Le reclassement pour raison opérationnelle intervient, sur demande de l'intéressé, dans les conditions prévues aux articles 81 à 85 de la loi n° 84-53 du 26 janvier 1984 (...) ". Aux termes de l'article 5 de la même loi : " Le bénéfice du congé pour raison opérationnelle est ouvert au sapeur-pompier professionnel en position d'activité auprès d'un service départemental d'incendie et de secours et ayant accompli une durée de vingt-cinq années de services effectifs en tant que sapeur-pompier ou de services militaires. ". Aux termes de l'article 6 de la même loi : " (...). Le sapeur-pompier professionnel admis au bénéfice d'un congé pour raison opérationnelle doit opter : a) Soit pour un congé avec faculté d'exercer une activité privée, dans les conditions déterminées à l'article 7 ; b) Soit pour un congé avec constitution de droits à pension, dans les conditions déterminées à l'article 8. ". Aux termes de l'article 8 de la même loi : " Le sapeur-pompier professionnel qui n'aura fait l'objet d'aucune proposition de reclassement dans un délai de deux mois à compter de sa demande de congé pour raison opérationnelle peut bénéficier, à sa demande, d'un congé avec constitution de droits à pension. / Le sapeur-pompier professionnel ayant refusé les propositions de reclassement formulées dans le même délai de deux mois, dans un emploi de niveau équivalent et situé dans un lieu d'affectation proche de celui qu'il occupait au moment de sa demande, ne peut bénéficier d'un congé avec constitution de droits à pension. (...) ". Enfin, aux termes de l'article 5 du décret du 20 avril 2005 relatif au projet de fin de carrière des sapeurs-pompiers professionnels : " En cas de reclassement prévu au 2° de l'article 4, le sapeur-pompier professionnel présente à l'autorité territoriale, à compter de la réception de la proposition, une demande de détachement dans un autre corps, cadre d'emplois ou emploi de la fonction publique (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions que le sapeur-pompier professionnel âgé d'au moins cinquante ans, dont la difficulté à exercer des fonctions opérationnelles est reconnue médicalement, peut bénéficier d'un projet de fin de carrière dans lequel il peut se voir proposer par l'autorité territoriale, soit une affectation non opérationnelle au sein du SDIS, selon les possibilités du service, soit un reclassement pour raison opérationnelle dans un autre corps, cadre d'emplois ou emploi de la fonction publique sous la forme d'un détachement, soit un congé pour raison opérationnelle. Lorsque le sapeur-pompier professionnel demande à bénéficier d'un congé pour raison opérationnelle, l'autorité territoriale ne peut lui refuser l'octroi d'un congé pour raison opérationnelle avec constitution de droits à pension que s'il a rejeté la ou les propositions de détachement dans un autre emploi, de niveau équivalent et situé dans un lieu d'affectation proche de celui qu'il occupait qu'elle lui a adressées dans le délai de deux mois à compter de sa demande de congé. En l'absence de proposition de détachement dans ce délai de la part de l'autorité territoriale, qui n'a pas à être saisie préalablement par le sapeur-pompier professionnel d'une demande spéciale en ce sens, ce dernier peut bénéficier d'un congé avec constitution de droits à pension. Par suite, en jugeant que M. A... ne pouvait pas bénéficier, en application des dispositions de l'article 8 de la loi du 7 juillet 2000, du congé pour raison opérationnelle avec constitution de droits à pension au motif qu'il n'avait pas manifesté sa volonté de bénéficier d'un reclassement par voie de détachement alors qu'il avait été informé de cette possibilité, la cour administrative d'appel a commis une erreur de droit. Dès lors, M. A... est fondé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du service départemental d'incendie et de secours du Finistère le versement d'une somme de 3 000 euros à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. A..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 juin 2016 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Le service départemental d'incendie et de secours du Finistère versera la somme de 3 000 euros à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le service départemental d'incendie et de secours du Finistère au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B... A...et au service départemental d'incendie et de secours du Finistère.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-04-02-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. DISPOSITIONS PARTICULIÈRES. SERVICES D'INCENDIE ET SECOURS. - SAPEUR-POMPIER PROFESSIONNEL ÂGÉ D'AU MOINS CINQUANTE ANS AYANT DES DIFFICULTÉS À EXERCER DES FONCTIONS OPÉRATIONNELLES (ART. 3 À 8 DE LA LOI DU 7 JUILLET 2000) - BÉNÉFICE D'UN CONGÉ POUR RAISON OPÉRATIONNELLE AVEC CONSTITUTION DE DROITS À PENSION - CONDITION - REJET PAR L'INTÉRESSÉ DES PROPOSITIONS DE RECLASSEMENT PAR LA VOIE DU DÉTACHEMENT DEVANT ÊTRE PROPOSÉES D'OFFICE PAR L'AUTORITÉ TERRITORIALE DANS LE DÉLAI DE DEUX MOIS OU ABSENCE DE PROPOSITION DE DÉTACHEMENT DANS CE DÉLAI.
</SCT>
<ANA ID="9A"> 135-01-04-02-03 Il résulte de l'article 3 de la loi n° 2000-628 du 7 juillet 2000 relative notamment au reclassement et à la cessation anticipée d'activité des sapeurs-pompiers professionnels que le sapeur-pompier professionnel âgé d'au moins cinquante ans, dont la difficulté à exercer des fonctions opérationnelles est reconnue médicalement, peut bénéficier d'un projet de fin de carrière dans lequel il peut se voir proposer par l'autorité territoriale, soit une affectation non opérationnelle au sein du service d'incendie et de secours (SDIS), selon les possibilités du service, soit un reclassement pour raison opérationnelle dans un autre corps, cadre d'emplois ou emploi de la fonction publique sous la forme d'un détachement, soit un congé pour raison opérationnelle.... ,,Lorsque le sapeur-pompier professionnel demande à bénéficier d'un congé pour raison opérationnelle, l'autorité territoriale ne peut lui refuser l'octroi d'un congé pour raison opérationnelle avec constitution de droits à pension que s'il a rejeté la ou les propositions de détachement dans un autre emploi, de niveau équivalent et situé dans un lieu d'affectation proche de celui qu'il occupait qu'elle lui a adressées dans le délai de deux mois à compter de sa demande de congé. En l'absence de proposition de détachement dans ce délai de la part de l'autorité territoriale, qui n'a pas à être saisie préalablement par le sapeur-pompier professionnel d'une demande spéciale en ce sens, ce dernier peut bénéficier d'un congé avec constitution de droits à pension.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
