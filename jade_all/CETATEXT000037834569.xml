<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834569</ID>
<ANCIEN_ID>JG_L_2018_12_000000405068</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834569.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 19/12/2018, 405068, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405068</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405068.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 29 avril 2014 par laquelle l'inspecteur du travail de la 7ème section de l'unité territoriale du Calvados a autorisé MeA..., en qualité de liquidateur judiciaire de la société Fan Technology, à la licencier. Par un jugement n° 1401341 du 18 juin 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT02515 du 14 septembre 2016, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 novembre 2016 et 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Me A...la somme de 600 euros au titre de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme de 1 000 euros au même titre.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme B...et à la SCP Lyon-Caen, Thiriez, avocat de MeA..., liquidateur judiciaire de la société Fan Technology ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 29 avril 2014, l'inspecteur du travail de la 7ème section de l'unité territoriale du Calvados a autorisé MeA..., liquidateur judiciaire de la société Fan Technology, à licencier MmeB..., salariée protégée en qualité de déléguée syndicale et de représentante du personnel au comité d'hygiène, de sécurité et des conditions de travail ; que, par un jugement du 18 juin 2015, le tribunal administratif de Caen a rejeté la demande de Mme B...tendant à l'annulation de cette décision ; qu'elle se pourvoit en cassation contre l'arrêt du 14 septembre 2016 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes des dispositions alors applicables de l'article L. 2421-3 du code du travail : " Le licenciement envisagé par l'employeur d'un délégué du personnel ou d'un membre élu du comité d'entreprise titulaire ou suppléant, d'un représentant syndical au comité d'entreprise ou d'un représentant des salariés au comité d'hygiène de sécurité et des conditions de travail est soumis au comité d'entreprise, qui donne un avis sur le projet de licenciement. / Lorsqu'il n'existe pas de comité d'entreprise dans l'établissement, l'inspecteur du travail est saisi directement (...) " ; qu'il ressort des termes de l'arrêt attaqué que, pour écarter le moyen soulevé par MmeB..., tiré de ce que le comité d'entreprise de la société Fan Technology n'avait pas été consulté sur son licenciement, la cour administrative d'appel s'est fondée sur ce que le comité d'entreprise était régulièrement supprimé à la date à laquelle son avis aurait été requis ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 2322-1 du code du travail, alors en vigueur : " Un comité d'entreprise est constitué dans toutes les entreprises employant au moins cinquante salariés " ; qu'aux termes des dispositions, alors applicables, de l'article L. 2322-7 du même code : " La suppression d'un comité d'entreprise est subordonnée à un accord entre l'employeur et l'ensemble des organisations syndicales représentatives. / A défaut d'accord, l'autorité administrative peut autoriser la suppression du comité d'entreprise en cas de réduction importante et durable du personnel ramenant l'effectif au-dessous de cinquante salariés " ; <br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que les mandats des membres du comité d'entreprise de la société Fan Technology avaient expiré le 5 janvier 2014 ; que la société n'employait plus, alors, que 28 salariés ; que, le tribunal de commerce de Caen ayant, par un jugement du 5 mars 2014, prononcé la liquidation judiciaire de la société et autorisé la poursuite provisoire de son activité " uniquement dans le but de la mise en place des institutions représentatives du personnel indispensables à l'engagement des mesures de licenciement pour motif économique ", l'ensemble des organisations syndicales représentatives du personnel avaient conclu, avec l'employeur, un protocole d'accord préélectoral ayant pour objet, non pas, ainsi que le prévoyait l'article L. 2324-3 du code du travail alors applicable, l'élection simultanée de représentants du personnel au comité d'entreprise et l'élection de délégués du personnel mais, exclusivement, l'élection de délégués du personnel ; qu'en se fondant sur l'ensemble de ces circonstances pour estimer qu'il existait, en l'espèce, un accord entre l'employeur et l'ensemble des organisations syndicales représentatives pour la suppression du comité d'entreprise la cour a porté sur les pièces du dossier une appréciation souveraine qui n'est pas entachée de dénaturation ; qu'elle a pu, sans commettre d'erreur de droit, en déduire que le comité d'entreprise avait été régulièrement supprimé et que, par suite, l'absence de sa consultation n'entachait pas d'irrégularité la demande d'autorisation de licenciement de MmeB... ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de Me A... et de l'Etat, qui ne sont pas les parties perdantes dans la présente instance, les sommes qu'elle demande à ce titre ; que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de Mme B...la somme demandée par Me A... au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Les conclusions présentées par Me A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme C...B..., à Me A..., en qualité de liquidateur judiciaire de la société Fan Technology et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
