<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035099000</ID>
<ANCIEN_ID>J7_L_2017_05_000001601882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/09/90/CETATEXT000035099000.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de DOUAI, 1ère chambre - formation à 3, 18/05/2017, 16DA01882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-18</DATE_DEC>
<JURIDICTION>CAA de DOUAI</JURIDICTION>
<NUMERO>16DA01882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Yeznikian</PRESIDENT>
<AVOCATS>QUEVREMONT</AVOCATS>
<RAPPORTEUR>Mme Amélie  Fort-Besnard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Riou</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :               Procédure contentieuse antérieure :               M. A...C...a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir l'arrêté de la préfète du Pas-de-Calais du 23 octobre 2015 l'obligeant à quitter le territoire français sans délai, fixant le pays de renvoi et le plaçant en rétention.               Par un jugement n° 1503369 du 30 août 2016, le tribunal administratif de Rouen a annulé la décision fixant le pays de renvoi en tant qu'elle n'exclut pas l'Irak ainsi que la décision de placement en rétention et a rejeté le surplus des conclusions de sa demande.              Procédure devant la cour :               Par une requête, enregistrée le 26 octobre 2016, M.C..., représenté par Me B...D..., demande à la cour :               1°) d'annuler ce jugement en tant qu'il a rejeté ses conclusions tendant à l'annulation de la décision l'obligeant à quitter le territoire français ;              2°) d'annuler pour excès de pouvoir cette décision ;              3°) de mettre à la charge de l'Etat, au profit de son avocate, la somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 alinéa 2 de la loi du 10 juillet 1991.       <br/>
Il soutient que :        - l'obligation de quitter le territoire français méconnaît les stipulations de l'article 4 du protocole 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;       - elle est entachée de détournement de pouvoir ;        - elle méconnaît les stipulations du 1) de l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.              La requête a été communiquée à la préfète du Pas-de-Calais qui n'a pas produit d'observations.               M. C...a été admis au bénéfice de l'aide juridictionnelle totale par une décision du 26 septembre 2016 du bureau d'aide juridictionnelle près le tribunal de grande instance de Douai.              Vu les autres pièces du dossier.              Vu :       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;       - la loi n° 91-647 du 10 juillet 1991 ;       - le code de justice administrative.              Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.              Les parties ont été régulièrement averties du jour de l'audience.              Le rapport de Mme Amélie Fort-Besnard, premier conseiller, a été entendu au cours de l'audience publique.                     1. Considérant que M.C..., ressortissant irakien, est entré irrégulièrement sur le territoire français et n'a pas sollicité de titre de séjour ; qu'il pouvait, dès lors, faire l'objet d'une obligation de quitter le territoire français sur le fondement du 1°) du I de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que si la contrôleuse générale des lieux de privation de liberté a déploré, dans ses recommandations en urgence du 13 novembre 2015, publiées au Journal officiel de la République française du 2 décembre 2015, un usage détourné de la procédure de placement en rétention administrative dans le cadre de l'évacuation des étrangers présents dans la " Jungle " de Calais, elle n'a, en tout état de cause, pas remis en cause le caractère irrégulier de leur présence ; que, par suite, ni la circonstance que l'administration n'aurait pas accompli de diligences en vue de son éloignement effectif, ni celle tirée de ce qu'il a été interpellé avec d'autres ressortissants placés dans la même situation ne permettent de considérer que la préfète aurait poursuivi un autre but que celui assigné à la police spéciale des étrangers en obligeant M. C... à quitter le territoire français ; que, par suite, le détournement de pouvoir allégué n'est pas établi ;        <br/>
2. Considérant que l'obligation de quitter le territoire français n'implique en elle-même aucune privation de liberté ; que, dès lors, le moyen tiré de la méconnaissance de l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui garantit le droit à la liberté et à la sûreté doit être écarté comme inopérant ;               3. Considérant que M. C...n'était présent sur le territoire français que depuis un mois et n'avait pas formulé de demande de titre de séjour lorsqu'il a été interpelé, en même temps que quatre autres personnes, alors qu'il avait pénétré de manière illicite sur une partie non ouverte au public du site d'Eurotunnel ; qu'il ressort des pièces du dossier et notamment du procès-verbal d'audition signé par M.C..., que celui-ci a été entendu, assisté d'un interprète, le 23 octobre 2015, par un officier de police judiciaire sur ses conditions de séjour et de vie en France, sur les conditions de son arrivée sur le territoire et son désir de s'y maintenir ; qu'il lui a également été demandé s'il avait des observations à formuler au cas où la préfète du Pas-de-Calais prendrait à son encontre une mesure de reconduite à la frontière, éventuellement assortie d'une assignation à résidence ou d'un placement en rétention, et s'il avait d'autres éléments relatifs à sa situation qu'il souhaitait porter à la connaissance de la préfète ; qu'ainsi, l'intéressé a été personnellement entendu et a eu la possibilité, au cours de cette audition, de faire connaître ses observations sur la décision attaquée ; que si la décision attaquée mentionne seulement son identité, sa nationalité, son âge et les circonstances de son interpellation, ses éléments de personnalisation apparaissent suffisants eu égard à la très courte durée de son séjour en France ainsi qu'à la précarité de ses conditions de séjour ; que, dans ces conditions, M. C...n'est pas fondé à soutenir qu'il a fait l'objet d'une mesure d'expulsion collective qui le visait de façon indiscriminée ; que, dès lors et en tout état de cause, le moyen tiré de la méconnaissance des stipulations de l'article 4 du protocole n° 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui prohibe les expulsions collectives d'étrangers doit être écarté ;              4. Considérant qu'il résulte de tout ce qui précède que M. C...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rouen a rejeté ses conclusions tendant à l'annulation de l'obligation de quitter le territoire français dont il a fait l'objet le 23 octobre 2015 ; que, par voie de conséquence, ses conclusions présentées sur le fondement des articles L. 761-1 du code de justice administrative et 37 alinéa 2 de la loi du 10 juillet 1991 doivent être rejetées ;                                                 DÉCIDE :                                                 Article 1er : La requête de M. C...est rejetée.       <br/>
       Article 2 : Le présent arrêt sera notifié à M. A...C..., au ministre de l'intérieur et à Me B...D....               Copie en sera transmise pour information au préfet du Pas-de-Calais.                                           Délibéré après l'audience publique du 4 mai 2017 à laquelle siégeaient :                     - M. Olivier Yeznikian, président de chambre,       - M. Christian Bernier, président-assesseur,       - Mme Amélie Fort-Besnard, premier conseiller.                     Lu en audience publique le 18 mai 2017.              Le rapporteur,Signé : A. FORT-BESNARDLe premier vice-président de la cour,Président de chambre,Signé : O. YEZNIKIANLe greffier,Signé : C. SIRE       <br/>
La République mande et ordonne au ministre de l'intérieur en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun, contre les parties privées, de pourvoir à l'exécution du présent arrêt.              Pour expédition conforme,Le greffier en chef,Par délégation,Le greffier,Christine SireN°16DA01882	2<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03 Étrangers. Obligation de quitter le territoire français (OQTF) et reconduite à la frontière.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
