<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027069255</ID>
<ANCIEN_ID>JG_L_2013_02_000000361263</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/92/CETATEXT000027069255.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 15/02/2013, 361263, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361263</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361263.20130215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense ; le ministre de la défense demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1005576 du 3 juillet 2012 par lequel le tribunal administratif de Marseille a, à la demande de M. A...B..., d'une part, annulé la décision du 18 juin 2010 par laquelle le ministre de la défense et des anciens combattants a refusé de lui accorder le bénéfice de l'indemnité spéciale de sécurité aérienne (ISSA) à compter du 1er septembre 2009 et a confirmé l'obligation de reverser un trop-perçu d'ISSA pour la période allant de septembre à décembre 2009 et, d'autre part, enjoint au ministre de la défense d'allouer à M. B...l'indemnité spéciale de sécurité aérienne à compter du mois de septembre 2009 assortie des intérêts de retard au taux en vigueur ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'injonction prononcée par le tribunal administratif de Marseille sur le fondement de l'article L. 911-1 du code de justice administrative ;<br/>
<br/>
              3°) réglant l'affaire au fond, de rejeter les conclusions présentées par M. B...devant le tribunal administratif de Marseille ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu le code de la défense ;<br/>
<br/>
              Vu le décret n° 69-448 du 20 mai 1969 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Blanc, Rousseau, avocat de M.B...,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Blanc, Rousseau, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er du décret du 20 mai 1969 portant création d'une indemnité spéciale de sécurité aérienne : " Une indemnité spéciale de sécurité aérienne est allouée aux officiers et aux militaires non officiers à solde mensuelle contrôleurs d'opérations et de sécurité aériennes assumant dans des organisations militaires ou mixtes et sur les bâtiments de guerre une responsabilité directe dans la conduite des aéronefs (...) " ; qu'aux termes de l'article 2 : " L'indemnité spéciale de sécurité aérienne est une indemnité accessoire de la solde perçue dans les mêmes conditions que celle-ci pendant le temps où la fonction de contrôleur d'opérations et de sécurité aériennes est exercée (...). " ;<br/>
<br/>
              Sur les conclusions dirigées contre le jugement en tant qu'il a annulé la décision ministérielle du 18 juin 2010 :<br/>
<br/>
              2. Considérant, en premier lieu, que pour annuler la décision du 18 juin 2010 par laquelle le ministre de la défense et des anciens combattants a, après avis de la commission de recours des militaires, confirmé le refus d'accorder à M.B..., adjudant-chef de l'armée de l'air, l'indemnité spéciale de sécurité aérienne créée par le décret du 20 mai 1969, le tribunal administratif de Marseille ne s'est pas fondé sur l'illégalité de l'instruction ministérielle du 19 septembre 2006 mais sur l'illégalité commise par le ministre de la défense à ne pas s'être interrogé sur la responsabilité que lui donnait ou non ses fonctions dans la conduite des aéronefs ; que, par suite, le ministre ne peut, en tout état de cause, utilement invoquer les moyens tirés de ce qu'en estimant cette instruction ministérielle illégale, le tribunal administratif aurait commis des erreurs de droit ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ne ressort pas des énonciations du jugement attaqué que le tribunal administratif aurait considéré que le ministre avait omis d'indiquer, dans son mémoire en défense, en quoi les nouvelles fonctions de M. B...à l'école du personnel navigant d'essais et de réception ne permettaient pas d'assumer une responsabilité directe dans la conduite des aéronefs ; qu'en précisant qu'il ne ressortait " ni de la décision contestée ni des écritures en défense que le retrait de l'indemnité de M. B... soit en lien avec les fonctions exercées au sein de l'école du personnel navigant d'essais et de réception ", le tribunal administratif, qui n'était pas saisi d'une demande de substitution de motif de la décision ministérielle contestée, s'est borné à estimer que les écritures du ministre n'étaient pas de nature à établir qu'à la date à laquelle la décision de refus avait été prise, l'administration aurait vérifié si les fonctions de M. B...lui permettaient d'assumer une responsabilité directe dans la conduite des aéronefs ; que, par suite, les moyens tirés de ce que le tribunal administratif aurait inexactement interprété les écritures du ministre de la défense et entaché son jugement d'insuffisante motivation sur ce point doivent être écartés ;<br/>
<br/>
              Sur les conclusions dirigées contre le jugement en tant qu'il a enjoint au ministre d'accorder l'indemnité spéciale de sécurité aérienne :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              5. Considérant qu'eu égard au motif d'annulation de la décision ministérielle rappelé ci-dessus et retenu par le tribunal administratif de Marseille, son jugement n'impliquait pas que le ministre de la défense prenne une mesure d'exécution dans un sens déterminé mais seulement qu'il statue, après avoir examiné à nouveau la situation de M. B..., sur le recours administratif présenté par celui-ci ; que, par suite, en enjoignant sous astreinte au ministre d'allouer à M. B...l'indemnité spéciale de sécurité aérienne, le tribunal administratif a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre de la défense est seulement fondé à demander l'annulation de l'article 2 du jugement attaqué ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 821-2 du code de justice administrative, le Conseil d'Etat, s'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, peut " régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie " ; que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond dans cette mesure ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 911-2 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé " ;<br/>
<br/>
              9. Considérant qu'eu égard à ses motifs, le jugement du tribunal administratif de Marseille implique nécessairement que le ministre de la défense se prononce à nouveau sur le recours de M. B...; qu'il y a lieu, par suite, d'enjoindre au ministre de procéder à cet examen dans un délai de deux mois à compter de la notification de la présente décision ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que demande M. B...au titre des frais exposés par lui et non compris dans les dépens, soit mise à la charge de l'Etat, qui n'est pas, dans la présente affaire, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Marseille du 3 juillet 2012 est annulé.<br/>
Article 2 : Il est enjoint au ministre de la défense de se prononcer à nouveau sur le recours de M. B... dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus du pourvoi du ministre de la défense et les conclusions de M. B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
