<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043080053</ID>
<ANCIEN_ID>JG_L_2021_01_000000441751</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/08/00/CETATEXT000043080053.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/01/2021, 441751</TITRE>
<DATE_DEC>2021-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441751</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441751.20210128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 10 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat des médecins d'Aix et Région (SMAER) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-630 du 26 mai 2020 modifiant le décret n° 2020-548 du 11 mai 2020 en tant qu'il met fin à la possibilité pour les médecins hospitaliers de prescrire l'hydroxychloroquine pour les malades atteints de covid-19 ;<br/>
<br/>
              2°) d'enjoindre, sous astreinte, au Premier ministre d'adopter un décret autorisant de nouveau la prescription, la dispensation et l'administration de l'hydroxychloroquine aux patients atteints de covid-19, tant par les médecins dans les établissements de santé que par les praticiens libéraux ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le décret n° 2020-260 du 16 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-337du 26 mars 2020<br/>
              - le décret n° 2020-545 du 11 mai 2020 ;<br/>
              - le décret n° 2020-548 du 11 mai 2020 ;<br/>
              - le décret n° 2020-630 du 26 mai 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              1. En premier lieu, l'article L. 5121-8 du code de la santé publique dispose que : " Toute spécialité pharmaceutique (...) doit faire l'objet, avant sa mise sur le marché ou sa distribution à titre gratuit, d'une autorisation de mise sur le marché délivrée par l'Agence française de sécurité sanitaire des produits de santé. L'autorisation peut être assortie de conditions appropriées (...) ". L'article L. 5121-12-1 du même code prévoit que : " I.- Une spécialité pharmaceutique peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, sous réserve qu'une recommandation temporaire d'utilisation établie par l'Agence nationale de sécurité du médicament et des produits de santé sécurise l'utilisation de cette spécialité dans cette indication ou ces conditions d'utilisation. (...) / En l'absence de recommandation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, une spécialité pharmaceutique ne peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché qu'en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation et sous réserve que le prescripteur juge indispensable, au regard des données acquises de la science, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient. / (...) ". Aux termes de l'article R. 4127-8 de ce code : " Dans les limites fixées par la loi et compte tenu des données acquises de la science, le médecin est libre de ses prescriptions qui seront celles qu'il estime les plus appropriées en la circonstance. / Il doit, sans négliger son devoir d'assistance morale, limiter ses prescriptions et ses actes à ce qui est nécessaire à la qualité, à la sécurité et à l'efficacité des soins. / Il doit tenir compte des avantages, des inconvénients et des conséquences des différentes investigations et thérapeutiques possibles ".<br/>
<br/>
              2. En deuxième lieu, l'article L. 3131-12 inséré dans le code de la santé publique par la loi du 23 mars 2020 prévoit que : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire (...) en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". D'une part, aux termes du I de l'article L. 3131-15 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire (...) ". Aux termes du III du même article : " Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". D'autre part, aux termes du premier alinéa de l'article L. 3131-16 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. ". Aux termes du troisième alinéa du même article : " Les mesures prescrites en application du présent article sont strictement nécessaires et proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". Ces dispositions étaient applicables à la date d'édiction du décret attaqué par l'effet de l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et de l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, qui ont déclaré puis prorogé l'état d'urgence sanitaire.<br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Au vu de l'évolution de la situation sanitaire, de nouvelles mesures générales ont été adoptées par deux décrets du 11 mai 2020 pour assouplir progressivement les sujétions imposées afin de faire face à l'épidémie.<br/>
<br/>
              4. Le sulfate d'hydroxychloroquine est commercialisé par le laboratoire Sanofi sous le nom de marque de Plaquenil, en vertu d'une autorisation de mise sur le marché initialement délivrée le 27 mai 2004, avec pour indications thérapeutiques le traitement symptomatique d'action lente de la polyarthrite rhumatoïde, le lupus érythémateux discoïde, le lupus érythémateux subaigu, le traitement d'appoint ou prévention des rechutes des lupus systémiques et la prévention des lucites. En application de l'article L. 5121-12-1 du code de la santé publique, et en l'absence de toute recommandation temporaire d'utilisation, cette spécialité ne pouvait être prescrite pour une autre indication, en l'absence d'alternative médicamenteuse appropriée disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation, qu'à la condition qu'en l'état des données acquises de la science, le prescripteur juge indispensable, le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient.<br/>
<br/>
              5. A la suite d'un avis sur les recommandations thérapeutiques dans la prise en charge du covid-19 du 23 mars 2020 du Haut Conseil de la santé publique, le Premier ministre, par un décret du 25 mars 2020, modifié par un décret du 26 mars, a complété d'un article 12-2 le décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, pour prévoir notamment les conditions dans lesquelles l'hydroxychloroquine peut être prescrite, dispensée et administrée aux patients atteints de covid-19, en dehors des indications de l'autorisation de mise sur le marché du Plaquenil. A ce titre, d'une part, par dérogation aux dispositions du code de la santé publique relatives aux autorisations de mise sur le marché, il a autorisé la prescription, la dispensation et l'administration sous la responsabilité d'un médecin, de l'hydroxychloroquine aux patients atteints de covid-19, dans les établissements de santé qui les prennent en charge, ainsi que, pour la poursuite de leur traitement si leur état le permet et sur autorisation du prescripteur initial, à domicile, en précisant que ces prescriptions interviennent, après décision collégiale, dans le respect des recommandations du Haut Conseil de la santé publique et, en particulier, de l'indication pour les patients atteints de pneumonie oxygéno-requérante ou d'une défaillance d'organe. D'autre part, il a prévu, au cinquième alinéa de cet article 12-2, que : " La spécialité pharmaceutique Plaquenil (c), dans le respect des indications de son autorisation de mise sur le marché, et les préparations à base d'hydroxychloroquine ne peuvent être dispensées par les pharmacies d'officine que dans le cadre d'une prescription initiale émanant exclusivement de spécialistes en rhumatologie, médecine interne, dermatologie, néphrologie, neurologie ou pédiatrie ou dans le cadre d'un renouvellement de prescription émanant de tout médecin ".<br/>
<br/>
              6. Ces dispositions ont été reprises à l'identique à l'article 17 du décret n° 2020-545 du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, qui abroge notamment l'article 12-2 du décret du 23 mars 2020, puis à l'article 19 du décret n° 2020-548 du même jour ayant le même objet, qui abroge le précédent et est entré en vigueur dès sa publication au Journal officiel de la République française le 12 mai 2020. <br/>
<br/>
              7. A la suite d'un nouvel avis du Haut Conseil de la santé publique relatif à l'utilisation de l'hydroxychloroquine dans le covid-19 du 24 mai 2020, le Premier ministre a abrogé, par décret du 26 mai 2020, l'article 19 du décret précité et le ministre des solidarités et de la santé a, par un arrêté du même jour pris sur le fondement de l'article L. 3131-16 du code de la santé publique, complété l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire d'un article 6-2 reprenant les dispositions du cinquième alinéa de l'article 12-2 du décret du 23 mars 2020 cité au point 5. Le syndicat requérant demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 26 mai 2020 en tant qu'il met fin à la possibilité pour les médecins hospitaliers de prescrire l'hydroxychloroquine pour les malades atteints du covid-19.<br/>
<br/>
              Sur la légalité du décret attaqué : <br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              8. Il ressort des pièces du dossier que le décret attaqué comporte, contrairement à ce que soutient le syndicat requérant, la signature du Premier ministre et du ministre des solidarités et de la santé. Par suite, le moyen tiré de ce que ce décret méconnaîtrait les dispositions de l'article L. 212-1 du code des relations entre le public et l'administration, en vertu desquelles toute décision prise par une administration comporte la signature de son auteur, ne peut qu'être écarté. <br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              9. Le décret attaqué a été pris sur l'avis du Haut conseil de la santé publique du 24 mai 2020 qui s'appuie sur des recommandations internationales et nationales, dont celles de l'Organisation mondiale de la Santé, de l'Infectious Diseases Society of America, des agences gouvernementales de santé des Etats-Unis d'Amérique et du Royaume-Uni et du gouvernement du Canada, mais également sur les données des centres régionaux de pharmacovigilance relatives aux médicaments utilisés chez des patients pris en charge pour une infection au covid-19, sur des données de bibliographie, sur la recommandation temporaire du 23 mai 2020 du groupe exécutif de l'essai clinique Solidarity lancé par l'Organisation mondiale de la Santé et ses partenaires dans le but de trouver un traitement efficace du covid-19 et, enfin, sur les dispositions prises pour l'essai Recovery au Royaume-Uni. Au regard de l'ensemble de ces éléments, le Haut Conseil de la santé publique estime dans cet avis que les données actuelles disponibles ne sont pas favorables à l'utilisation de l'hydroxychloroquine en dehors du cadre d'essais cliniques, aux motifs qu'elles n'apportent pas la preuve d'un bénéfice de l'utilisation de cette spécialité isolément ou en association à un macrolide, comme l'azithromycine, sur l'évolution du covid-19, qu'il existe une toxicité cardiaque de l'hydroxychloroquine, particulièrement en association avec l'azithromycine, et que la balance bénéfice/risque, seule et en association à un macrolide, apparaît défavorable. Le Haut Conseil recommande dès lors, dans l'attente des données issues d'études cliniques prospectives comparatives randomisées, de ne pas utiliser l'hydroxychloroquine, isolément ou en association à un macrolide pour le traitement du covid-19 chez les patients, ambulatoires ou hospitalisés, quel que soit le niveau de gravité. <br/>
<br/>
              10. Si les requérants critiquent la fiabilité de l'article publié dans le journal scientifique " The Lancet " le 22 mai 2020, il résulte de ce qui a été dit ci-dessus que cet article n'a constitué qu'un des nombreux éléments pris en compte par le Haut Conseil de la santé publique pour élaborer son avis. <br/>
<br/>
              11. Il résulte de ce qui précède, qu'à la date du 26 mai 2020, date d'édiction des dispositions contestées, les données acquises de la science ne permettaient pas de conclure, au-delà des essais cliniques, au caractère indispensable du recours à l'utilisation de l'hydroxychloroquine, en dehors des indications de son autorisation de mise sur le marché et en l'absence d'une autorisation temporaire d'utilisation, pour améliorer ou stabiliser l'état clinique des patients atteints par le covid-19. Par suite, le syndicat requérant n'est pas fondé à soutenir que le décret du 26 mai 2020, en tant qu'il met fin à la possibilité pour les médecins hospitaliers de prescrire l'hydroxychloroquine pour les malades atteints de covid-19, porterait atteinte au droit à la vie, au droit à la santé, au droit de recevoir les traitements et soins les plus appropriés à son état de santé, à la liberté de prescription et à l'indépendance d'exercice des médecins ou serait entaché d'une erreur manifeste d'appréciation.<br/>
<br/>
              12. Il résulte de tout ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation des dispositions du décret qu'il attaque. Ses conclusions à fin d'injonction et celles qu'il présente au titre de l'article L. 761-1 du code de justice administratives ne peuvent, par suite, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête du Syndicat des médecins d'Aix et Région est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat des médecins d'Aix et Région et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-01-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. LUTTE CONTRE LES ÉPIDÉMIES. - ETAT D'URGENCE SANITAIRE (ART. L. 3131-15 DU CSP) - DÉCRET METTANT FIN À LA POSSIBILITÉ DE PRESCRIRE D'HYDROXYCHLOROQUINE HORS AMM POUR LES MALADES ATTEINTS DE COVID-19 [RJ1] - ILLÉGALITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-04-01-01 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. AUTORISATIONS DE MISE SUR LE MARCHÉ. - PRESCRIPTION HORS AMM - DÉCRET METTANT FIN À LA POSSIBILITÉ DE PRESCRIRE D'HYDROXYCHLOROQUINE HORS AMM POUR LES MALADES ATTEINTS DE COVID-19 [RJ1] - ILLÉGALITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 61-01-01-02 Décret n° 2020-630 du 26 mai 2020 modifiant le décret n° 2020-548 du 11 mai 2020 pour mettre fin à la possibilité pour les médecins hospitaliers de prescrire l'hydroxychloroquine pour les malades atteints de covid-19.... ,,Il ressort des pièces du dossier qu'à la date d'édiction des dispositions contestées, les données acquises de la science ne permettaient pas de conclure, au-delà des essais cliniques, au caractère indispensable du recours à l'utilisation de l'hydroxychloroquine, en dehors des indications de son autorisation de mise sur le marché (AMM) et en l'absence d'une autorisation temporaire d'utilisation, pour améliorer ou stabiliser l'état clinique des patients atteints par le covid-19.... ,,Par suite, ces dispositions ne portent pas atteinte au droit à la vie, au droit à la santé, au droit de recevoir les traitements et soins les plus appropriés à son état de santé, à la liberté de prescription et à l'indépendance d'exercice des médecins ni ne sont entachées d'une erreur manifeste d'appréciation.</ANA>
<ANA ID="9B"> 61-04-01-01 Décret n° 2020-630 du 26 mai 2020 modifiant le décret n° 2020-548 du 11 mai 2020 pour mettre fin à la possibilité pour les médecins hospitaliers de prescrire l'hydroxychloroquine pour les malades atteints de covid-19.... ,,Il ressort des pièces du dossier qu'à la date d'édiction des dispositions contestées, les données acquises de la science ne permettaient pas de conclure, au-delà des essais cliniques, au caractère indispensable du recours à l'utilisation de l'hydroxychloroquine, en dehors des indications de son autorisation de mise sur le marché (AMM) et en l'absence d'une autorisation temporaire d'utilisation, pour améliorer ou stabiliser l'état clinique des patients atteints par le covid-19.... ,,Par suite, ces dispositions ne portent pas atteinte au droit à la vie, au droit à la santé, au droit de recevoir les traitements et soins les plus appropriés à son état de santé, à la liberté de prescription et à l'indépendance d'exercice des médecins ni ne sont entachées d'une erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la contestation des mesures encadrant la prescription d'hydroxychloroquine dans la prise en charge du covid-19, en référé-liberté, CE, juge des référés, 28 mars 2020, M.,et autres, n° 439765, à mentionner aux Tables ; en excès de pouvoir, CE, décisions du même jour, M.,et autres, n° 439764, à publier au Recueil, Syndicat des médecins d'Aix et région et autres, n° 439936, à mentionner aux Tables et M.,, n° 400129, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
