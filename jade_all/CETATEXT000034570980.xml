<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034570980</ID>
<ANCIEN_ID>JG_L_2017_05_000000407796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/57/09/CETATEXT000034570980.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 03/05/2017, 407796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407796.20170503</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a saisi le tribunal administratif de Bastia d'une demande tendant à la suspension de la décision du 8 décembre 2016 par laquelle le ministre de la défense a dénoncé son contrat d'engagement souscrit le 30 décembre 2015. <br/>
<br/>
              Par une ordonnance n° 170079 du 27 janvier 2017, le président du tribunal administratif de Bastia a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 9 février et 5 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le décret n° 2008-956 du 12 septembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B....<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de la décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bastia que M.B..., militaire du rang, a saisi la commission de recours des militaires le 20 janvier 2017, en application de l'article R. 4125-1 du code de la défense, d'un recours tendant à l'annulation de la décision du 8 décembre 2016 dénonçant son contrat d'engagement souscrit le 30 décembre 2015 ; qu'il a saisi le 26 janvier 2017 le tribunal administratif de Bastia d'une demande tendant à la suspension de l'exécution de cette décision ; que, par l'ordonnance attaquée, le président du tribunal administratif de Bastia a rejeté sa demande comme irrecevable ;<br/>
<br/>
              Sur l'ordonnance de référé :<br/>
<br/>
              3. Considérant que l'objet même du référé organisé par les dispositions précitées de l'article L. 521-1 du code de justice administrative est de permettre, dans tous les cas où l'urgence le justifie, la suspension dans les meilleurs délais d'une décision administrative contestée par le demandeur ; qu'une telle possibilité est ouverte y compris dans le cas où un texte législatif ou règlementaire impose l'exercice d'un recours administratif préalable avant de saisir le juge de l'excès de pouvoir, sans donner un caractère suspensif à ce recours obligatoire ; que, dans une telle hypothèse la suspension peut être demandée au juge des référés sans attendre que l'administration ait statué sur le recours préalable, dès lors que l'intéressé a justifié, en produisant une copie de ce recours, qu'il a engagé les démarches nécessaires auprès de l'administration pour obtenir l'annulation ou la réformation de la décision contestée ; que, saisi d'une telle demande de suspension, le juge des référés peut y faire droit si l'urgence justifie la suspension avant même que l'administration ait statué sur le recours préalable et s'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ; que, sauf s'il en décide autrement, la mesure qu'il ordonne en ce sens vaut, au plus tard, jusqu'à l'intervention de la décision administrative prise sur le recours présenté par l'intéressé ;<br/>
<br/>
              4. Considérant que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Bastia a jugé que la demande de M. B...était irrecevable au motif qu'elle n'était pas accompagnée de la copie d'une requête en annulation de la décision dont la suspension de l'exécution était demandée ; qu'en statuant ainsi, alors qu'il ressort des pièces du dossier soumis au juge des référés que le requérant avait joint à sa demande de suspension copie de son recours administratif préalable devant la commission de recours des militaires, exigé par les dispositions de l'article R. 4125-1 du code de la défense, tout en indiquant expressément dans sa demande de suspension au tribunal avoir formé un tel recours pour justifier de sa recevabilité, le juge des référés a commis une erreur de droit ; que, par suite, l'ordonnance contestée doit être annulée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              6. Considérant que la demande de M. B...est accompagnée d'une copie du recours administratif préalable obligatoire formé contre la décision contestée devant la commission de recours des militaires ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 4139-12 du code de la défense : " L'état militaire cesse, pour le militaire de carrière, lorsque l'intéressé est radié des cadres, pour le militaire servant en vertu d'un contrat, lorsque l'intéressé est rayé des contrôles " ;<br/>
<br/>
              8. Considérant que la décision de dénonciation du contrat de M. B...a, en l'espèce, pour effet de priver l'intéressé de son emploi et de sa rémunération ; que, par suite, son exécution est susceptible de porter à la situation du requérant une atteinte suffisamment grave et immédiate pour caractériser une situation d'urgence ;<br/>
<br/>
              9. Considérant qu'en l'état de l'instruction, le moyen tiré de ce que l'autorité militaire a méconnu la loi en prenant une mesure de dénonciation du contrat qui ne rentre dans aucun des cas énoncés par l'article 15 du décret du 12 septembre 2008 relatif aux militaires à titre étranger et qui ne relève pas non plus de l'article 12 de ce décret, est de nature à faire naître un doute sérieux quant à la légalité de la décision attaquée ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander la suspension de l'exécution de la décision du 8 décembre 2016 par laquelle le ministre de la défense a dénoncé son contrat d'engagement ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              11. Considérant que la présente décision implique nécessairement que le ministre de la défense procède à la réintégration de M. B...dans les effectifs de la Légion étrangère dans un délai de quinze jours à compter de la notification de la présente décision dans l'attente de l'intervention d'une nouvelle décision de l'autorité administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              12. Considérant qu'il y a lieu de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros à M. B...au titre des frais exposés par lui tant en première instance que devant le Conseil d'Etat et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Bastia du 27 janvier 2017 est annulée.<br/>
Article 2 : L'exécution de la décision du 8 décembre 2016 du ministre de la défense dénonçant le contrat d'engagement de M. B...est suspendue.<br/>
Article 3 : Il est enjoint au ministre de la défense de réintégrer M. B...dans les effectifs de la Légion étrangère, dans un délai de quinze jours à compter de la notification de la présente décision.<br/>
Article 4 : L'Etat versera à M. B...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de M. B...est rejeté.<br/>
Article 6 : La présente décision sera notifiée à M. A...B...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
