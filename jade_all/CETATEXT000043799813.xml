<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799813</ID>
<ANCIEN_ID>JG_L_2021_07_000000450883</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/98/CETATEXT000043799813.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/07/2021, 450883, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450883</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450883.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. E... G... a demandé au tribunal administratif de Grenoble d'annuler l'élection de M. B... F... à l'issue des élections organisées les 15 mars et 28 juin 2020 dans la commune de Grenoble (Isère) en vue de désigner les conseillers municipaux et communautaire, de rejeter le compte de campagne de l'intéressé, de le déclarer inéligible pour une durée d'un an et de le déclarer démissionnaire d'office de ses fonctions de conseiller municipal.<br/>
<br/>
              Par jugement n° 2003533 du 25 février 2021, ce tribunal a rejeté cette protestation.<br/>
<br/>
              Par une requête enregistrée le 20 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. G... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des élections municipales qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Grenoble (Isère), la liste conduite par M. A... D..., maire sortant, a recueilli 16 169 voix, soit 53,13 % des suffrages exprimés, et obtenu quarante-six sièges au conseil municipal et vingt-huit sièges au conseil communautaire, la liste conduite par M. B... F... a recueilli 7 133 voix, soit 23,44 % des suffrages exprimés, et obtenu sept sièges au conseil municipal et quatre sièges au conseil communautaire, la liste conduite par Mme C... a recueilli 3 803 voix, soit 12,49 % des suffrages exprimés, et obtenu trois sièges au conseil municipal et deux sièges au conseil communautaire et la liste conduite par M. H... a obtenu 3 325 voix, soit 10,92 % des suffrages exprimés, et trois sièges au conseil municipal et deux sièges au conseil communautaire. M. G... relève appel du jugement du 25 février 2021 par lequel le tribunal administratif de Grenoble a rejeté sa protestation tendant à l'annulation de l'élection de M. F... à l'issue de ces opérations électorales, au rejet du compte de campagne de l'intéressé et à ce que celui-ci soit déclaré inéligible pour une durée d'un an et démissionnaire d'office de ses fonctions de conseiller municipal. <br/>
<br/>
              2. A l'occasion d'une protestation relative à l'élection des conseillers municipaux et communautaire dans une commune de plus de mille habitants, qui se déroule au scrutin de liste à la représentation proportionnelle à la plus forte moyenne, l'annulation partielle de cette élection, dans laquelle l'attribution des sièges constitue une opération indivisible, peut être prononcée par le juge si les griefs présentés à l'appui de la protestation soit portent sur l'inéligibilité d'un ou de plusieurs candidats ou sur l'incompatibilité de leurs fonctions avec le mandat de conseiller municipal ou communautaire, soit permettent au juge de reconstituer avec certitude la répartition exacte des voix. <br/>
<br/>
              3. Il ressort des termes de la protestation de M. G... enregistrée au greffe du tribunal administratif de Grenoble le 2 juillet 2020 que celui-ci a demandé au juge de l'élection de prononcer l'annulation de l'élection de M. F... en qualité de conseiller municipal en invoquant notamment des griefs ayant trait à la régularité du financement de la campagne de l'intéressé. Par suite, c'est à tort que, par le jugement attaqué, le tribunal administratif a regardé comme irrecevables les conclusions présentées par le requérant tendant à l'annulation de l'élection de M. F... et à ce que soit prononcée son inéligibilité, et rejeté pour ce motif sa protestation. Son jugement doit, dès lors, être annulé, sans qu'il soit besoin de se prononcer sur les autres moyens de la requête.<br/>
<br/>
              4. Il y a lieu de statuer immédiatement sur la protestation présentée par M. G..., le tribunal administratif se trouvant dessaisi par l'expiration du délai résultant de l'article R. 120 du code électoral, de l'article 19 de la loi du 23 mars 2020 et de l'article 17 de l'ordonnance du 25 mars 2020.<br/>
<br/>
              5. En premier lieu, les griefs invoqués par M. G..., tirés de l'irrégularité, au regard des dispositions des articles L. 51 et L. 90 du code électoral, de la publicité par voie d'affichage à laquelle aurait procédé la liste conduite par M. F... ainsi que de l'absence de mention du directeur de publication sur le site internet de campagne de M. F..., ne portent pas sur l'inéligibilité ou l'incompatibilité de M. F... et ne sont pas de nature, à les supposer établis, à permettre au juge de reconstituer avec certitude la répartition exacte des voix et donc de prononcer l'annulation partielle de l'élection. <br/>
<br/>
              6. En deuxième lieu, la seule mention sur les bulletins de vote de la liste conduite par M. F..., à côté du nom des candidat, de leur profession, sans qu'apparaisse le nom d'aucune entreprise ou personne morale de droit privé ne méconnaît pas, contrairement à ce qui est soutenu, les dispositions de l'article R. 30 du code électoral qui, dans leur version alors applicable, disposaient que " Les bulletins ne peuvent pas comporter d'autres noms de personne que celui du ou des candidats ou de leurs remplaçants éventuels ".<br/>
<br/>
              7. En troisième lieu, aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) ".<br/>
<br/>
              8. Il ne résulte pas de l'instruction que la publication, sur le site internet, la page Facebook et les comptes Youtube et Twitter de l'association " Grenoble le Changement " de messages favorables à la liste conduite par M. F... serait constitutive d'un financement en nature de cette liste, en méconnaissance des dispositions de l'article L. 52-8 du code électoral. Ne saurait davantage être regardé comme un tel concours la mention, sur les bulletins de vote, de la profession des candidats sans au demeurant, ainsi qu'il a été dit, que soit mentionné aucun nom d'entreprise ou personne morale de droit privé. M. G... n'est par suite pas fondé à soutenir que l'absence de prise en compte de ces dépenses dans le compte de campagne de la liste conduite par M. F... méconnaîtrait les dispositions des articles L. 52-4, L. 52-6 et L. 113-1 du même code. De même, le grief tiré de ce que M. F... aurait omis de faire figurer dans son compte de campagne les coûts d'acquisition d'un fichier des personnes âgées de la commune et d'envoi à ces dernières d'un courrier de campagne n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              9. En outre, si la réalisation par une entreprise privée, sans que cette prestation ait donné lieu à facturation, de " clips vidéos " mettant en avant la création d'un monorail à propulsion solaire et un projet en faveur de la biodiversité traduit l'existence d'une contribution d'une personne morale à la campagne électorale de la liste conduite par M. F... en méconnaissance des dispositions de l'article L. 52-8 du code électoral, cette irrégularité, qui porte sur une somme que la commission nationale de contrôle des comptes de campagne et des financements politiques a évaluée à 2 500 euros, ne saurait justifier le rejet du compte de campagne de cette liste. <br/>
<br/>
              10. Enfin, si M. G... soutient que le prêt de 6 000 euros consenti par une personne physique à la liste conduite par M. F... et retracé comme tel dans son compte de campagne aurait en réalité la nature d'un don excédant le montant maximal de 4 600 euros autorisé par l'article L. 52-8 du code électoral, une telle allégation n'est assortie d'aucun élément de justification.<br/>
<br/>
              11. Il résulte de ce qui précède que les griefs invoqués par M. G..., relatifs au financement de la compagne de la liste de M. F..., ne sont pas de nature à entraîner le rejet de son compte de campagne ni, par suite, le prononcé de son inéligibilité en application des dispositions de l'article L. 118-3 du code électoral.<br/>
<br/>
              12. En quatrième lieu, dès lors qu'il n'apparaît pas que M. F... se serait livré à des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin, M. G... n'est pas davantage fondé à demander le prononcé de son inéligibilité sur le fondement de l'article L. 118-4 du code électoral.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la protestation de M. G... doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 25 février 2021 est annulé.<br/>
Article 2 : La protestation de M. G... est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. E... G..., à M. B... F... et au ministre de l'Intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
