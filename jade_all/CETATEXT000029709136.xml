<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709136</ID>
<ANCIEN_ID>JG_L_2014_11_000000356797</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709136.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 05/11/2014, 356797, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356797</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356797.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 février et 16 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02318 du 14 décembre 2011 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation de l'article 2 du jugement n° 0506568, 0506573 du tribunal administratif de Montpellier du 6 mars 2008, qui après l'avoir, par son article 1er, déchargé d'une fraction des pénalités qu'il contestait, a rejeté le surplus de sa demande de décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2001, 2002 et 2003 et des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite du contrôle fiscal de la SCI Agathe, dont M. A...est le gérant et l'associé à hauteur de 50 % des parts, l'administration l'a assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre des années 2001, 2002 et 2003, ainsi qu'à la pénalité pour opposition à contrôle fiscal alors prévue à l'article 1730 du code général des impôts ; que, par un jugement du 6 mars 2008, le tribunal administratif de Montpellier l'a déchargé d'une fraction de cette pénalité mais a rejeté le surplus des conclusions de sa demande de décharge ; qu'il se pourvoit en cassation contre l'arrêt du 14 décembre 2011 par lequel la cour administrative d'appel de Marseille a rejeté son appel dirigé contre l'article 2 de ce jugement ;<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              En ce qui concerne la nature du contrôle de la SCI Agathe :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 172 bis du code général des impôts : " Un décret précise la nature et la teneur des documents qui doivent être produits ou présentés à l'administration par les sociétés immobilières non soumises à l'impôt sur les sociétés qui donnent leurs immeubles en location ou en confèrent la jouissance à leurs associés (...) " ; que les articles 46 B et 46 C de l'annexe III au même code soumettent les sociétés immobilières visées à l'article 172 bis du code général des impôts à des obligations déclaratives particulières ; qu'aux termes de l'article 46 D de l'annexe III au même code : "Les sociétés visées aux articles 46 B et 46 C sont tenues de présenter à toute réquisition du service des impôts tous documents comptables ou sociaux, inventaires, copies de lettres, pièces de recettes et de dépenses de nature à justifier l'exactitude des renseignements portés sur les déclarations prévues auxdits articles 46 B et 46 C. " ; qu'il résulte de ces dispositions qu'afin d'examiner les documents comptables et autres pièces justificatives que l'article 46 D de l'annexe III au code général des impôts impose de tenir aux sociétés civiles immobilières qui donnent leurs immeubles en location ou en confèrent la jouissance à leurs associés, l'administration peut légalement procéder à un contrôle sur place de ces documents, dans le respect des garanties bénéficiant à l'ensemble des contribuables vérifiés ;<br/>
<br/>
              3. Considérant que, pour écarter le moyen invoqué par le requérant tiré de ce que l'administration ne pouvait mettre en oeuvre une vérification de comptabilité à l'encontre de la SCI Agathe dès lors que celle-ci n'était pas astreinte à la tenue d'une comptabilité, la cour a jugé que la circonstance qu'une société civile immobilière ne serait pas contrainte de tenir une véritable comptabilité ne saurait faire obstacle à la mise en oeuvre du contrôle sur place mentionné au point 2 qui, s'il doit se dérouler dans le respect des garanties générales conférées aux contribuables faisant l'objet d'une vérification de comptabilité, ne saurait néanmoins être regardé comme une vérification de comptabilité ; qu'elle a, ensuite, relevé qu'il n'était pas allégué par le requérant que l'administration aurait exigé la production de documents ou de pièces autres que ceux que la société était tenue de conserver en vertu des dispositions précitées de l'article 46 D de l'annexe III au code général des impôts ; qu'enfin, elle a jugé que la circonstance que l'administration ait adressé à la société un imprimé mentionnant "avis de vérification de comptabilité " au lieu d'un imprimé mentionnant "avis de contrôle sur place ", ne pouvait être regardée comme étant de nature à induire celle-ci en erreur sur la nature des documents à produire au cours du contrôle et ne l'avait privée d'aucune garantie ;<br/>
<br/>
              4. Considérant que, par ces motifs, la cour n'a pas, contrairement à ce que soutient le requérant, jugé que la SCI Agathe pouvait faire l'objet d'une vérification de comptabilité ; qu'elle a seulement estimé que le contrôle que l'administration fiscale avait entendu mettre en oeuvre à son égard consistait en un contrôle sur place des documents comptables et autres pièces justificatives qu'elle avait l'obligation de tenir en application de l'article 46 D de l'annexe III au code général des impôts ; que, par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en jugeant que l'administration était en droit de diligenter une vérification de comptabilité à l'encontre de la SCI Agathe en matière de taxe sur la valeur ajoutée, sans rechercher préalablement si cette société était assujettie cette taxe au cours de la période litigieuse doit être écarté comme inopérant ; que le requérant n'est, par ailleurs, pas fondé à soutenir que la cour, qui statuait sur la procédure ayant conduit à des redressements en matière d'impôt sur le revenu et non de taxe sur la valeur ajoutée, aurait entaché son arrêt d'insuffisance de motivation et de contradiction de motifs en jugeant qu'il n'était pas allégué que le service aurait exigé la production de documents ou de pièces autres que ceux que la société était tenue de conserver en vertu des dispositions de l'article 46 D de l'annexe III au code général des impôts, alors que la société avait soutenu que l'administration ne pouvait exiger de documents se rapportant à la taxe sur la valeur ajoutée ;<br/>
<br/>
              En ce qui concerne la mise en oeuvre de la procédure d'opposition à contrôle fiscal :<br/>
<br/>
              5. Considérant qu'aux termes du premier alinéa de l'article L. 74 du livre des procédures fiscales : " Les bases d'imposition sont évaluées d'office lorsque le contrôle fiscal ne peut avoir lieu du fait du contribuable ou de tiers " ; que le contrôle fiscal mentionné par ces dispositions ne saurait être restreint à la seule vérification de comptabilité prévue par l'article L. 13 du livre des procédures fiscales mais vise l'ensemble des contrôles sur place auxquels l'administration des impôts est en droit de procéder  ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que la procédure d'évaluation d'office mentionnée à l'article L. 74 précité du livre des procédures fiscales était applicable à la SCI Agathe dans le cadre du contrôle sur place dont elle pouvait légalement faire l'objet ;<br/>
<br/>
              6. Considérant que la cour a relevé qu'il résultait de l'instruction, d'une part, que l'administration avait envoyé, le 21 septembre puis le 6 octobre 2004, tant à l'adresse communiquée par la société qu'à l'adresse de son siège social, des avis de contrôle fiscal, qui avaient été retournés au service avec la mention " non réclamé, retour à l'envoyeur ", d'autre part, qu'un autre avis, envoyé le 6 octobre 2004, à l'adresse du gérant était, pour sa part, parvenu à son destinataire, qui n'avait toutefois pris aucune disposition pour recevoir le vérificateur, enfin, que ce dernier, s'étant présenté au siège social de la société le 3 novembre 2004, avait constaté l'absence de tout représentant de cette dernière et avait établi un procès-verbal d'opposition à contrôle fiscal ; qu'elle a estimé que, compte tenu des diligences nombreuses et vaines effectuées par le vérificateur, le contrôle fiscal de la société devait être regardé comme n'ayant pu avoir lieu de son fait et que l'envoi d'un avis de vérification de comptabilité au lieu d'un avis de contrôle sur place, qui ne privait la société, d'aucune garantie, ne l'autorisait pas à se soustraire aux opérations de contrôle ; qu'en déduisant de l'ensemble de ces circonstances que l'opposition à contrôle fiscal devait être regardée comme constituée, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              En ce qui concerne la motivation de la proposition de rectification : <br/>
<br/>
              7. Considérant que si l'administration ne peut légalement mettre de suppléments d'imposition à la charge personnelle des associés d'une société de personne non soumise à l'impôt sur les sociétés sans leur avoir notifié, dans les conditions prévues à l'article L. 57 du livre des procédures fiscales, les corrections apportées aux déclarations qu'ils ont eux-mêmes souscrites, cette obligation doit être regardée comme satisfaite dès lors que la proposition de rectification adressée aux associés comporte une référence aux rehaussements apportés aux bénéfices sociaux de la société ainsi que l'indication de la quote-part de ces bénéfices à raison de laquelle les intéressés seront imposés ;<br/>
<br/>
              8. Considérant qu'il en résulte que la cour n'a, en tout état de cause, pas commis d'erreur de droit en écartant le moyen tiré de l'insuffisante motivation la proposition de rectification adressée à M. A...en ce qui concerne les redressements opérés, au titre des années 2001 et 2002, dans la catégorie des revenus de capitaux mobiliers, après avoir relevé, d'une part, que cette proposition indiquait que le vérificateur envisageait d'imposer l'intéressé, à proportion de sa quote-part dans les droits de la SCI Agathe, à raison des revenus distribués à cette société par la SARL l'Amnesia et précisait le montant et la cause des rehaussements et, d'autre part, qu'était jointe à cette proposition la proposition de rectification adressée à la SCI Agathe ; <br/>
<br/>
              Sur les motifs de l'arrêt relatifs au bien-fondé des impositions : <br/>
<br/>
              9. Considérant, d'une part, qu'aux termes de l'article L. 193 du livre des procédures fiscales : " Dans tous les cas où une imposition a été établie d'office, la charge de la preuve incombe au contribuable qui demande la décharge ou la réduction de l'imposition " ; qu'il résulte de ce qui a été dit au point 6 ci-dessus que c'est sans erreur de droit que la cour, après avoir relevé que  les impositions mises à la charge de M. A...procédaient, à proportion de ses droits dans la SCI Agathe, des rehaussements apportés aux résultats de cette société dans le cadre de la procédure d'évaluation d'office régulièrement suivie par l'administration, en a déduit qu'il lui incombait, en conséquence,  de rapporter la preuve de l'exagération des impositions contestées ;<br/>
<br/>
              10. Considérant, d'autre part, que la cour, après avoir relevé que les constatations comptables de l'administration n'étaient pas utilement contestées et que l'administration ajoutait, sans être contredite, que l'analyse du compte courant de la SCI Agathe dans la comptabilité de la SARL l'Amnesia avait permis de mettre en évidence que cette dernière gérait les opérations financières de la société civile immobilière à travers sa propre comptabilité dès lors que la SARL l'Amnesia avait réglé des remboursements d'emprunt pour le compte de la société civile immobilière ou des rappels de taxe sur la valeur ajoutée réclamés à cette dernière lors d'un précédent contrôle ou encore avait consenti à la société civile immobilière, en 2002, une avance de loyer et remboursé pour son compte un prêt de 31 405 euros, en a déduit que l'administration démontrait ainsi que la SCI Agathe n'avait, en réalité, pas de véritable activité et n'avait fait que s'interposer entre la SARL l'Amnesia et M. A... ; que, contrairement à ce que soutient ce dernier, l'arrêt attaqué est suffisamment motivé sur ce point ;<br/>
<br/>
              Sur les motifs de l'arrêt relatifs aux pénalités :<br/>
<br/>
              11. Considérant que, pour écarter le moyen invoqué par le requérant tiré de ce que la pénalité pour opposition à contrôle fiscal, alors prévue à l'article 1730 du code général des impôts, dont les impositions mises à sa charge ont été assorties aurait été insuffisamment motivée, la cour, après avoir relevé que la procédure d'évaluation d'office prévue à l'article L. 74 du livre des procédures fiscales en cas d'opposition du contribuable au contrôle fiscal avait été régulièrement appliquée à la SCI Agathe, a jugé que la notification de redressement du 20 décembre 2004 adressée à M.A..., qui était accompagnée de la notification adressée à la SCI Agathe, apportait toutes informations utiles quant à la nature de l'infraction, au texte fondant celle-ci, au taux de la pénalité et au calcul de celle-ci et que, dans ces conditions, le vérificateur n'avait nulle obligation de faire référence à un " document extérieur " ; qu'en statuant ainsi, la cour, qui a porté sur les faits une appréciation souveraine non arguée de dénaturation, a suffisamment motivé son arrêt ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat en application des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
