<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031315642</ID>
<ANCIEN_ID>JG_L_2015_10_000000387249</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/31/56/CETATEXT000031315642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 14/10/2015, 387249, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387249</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:387249.20151014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 20 janvier et 19 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société d'assurance mutuelle L'Auxiliaire demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe 70 de l'instruction fiscale BOI-IS-BASE-20-20-10-30-20131231 intitulée " IS - Base d'imposition - Plus-value de cession de titres de société à prépondérance immobilière " publiée au bulletin officiel des finances publiques-impôts le 31 décembre 2013 ;<br/>
<br/>
              2°) d'enjoindre au ministre des finances et des comptes publics d'édicter de nouvelles instructions conformes à la loi. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'aux termes de l'article 219 du code général des impôts : " I. (...) Le taux normal de l'impôt est fixé à 33,1/3 %. / Toutefois: / a. Le montant net des plus- values à long terme fait l'objet d'une imposition séparée au taux de 19 %, dans les conditions prévues au 1 du I de l'article 39 quindecies et à l'article 209 quater. / Pour les exercices ouverts à compter du 1er janvier 2005, le taux fixé au premier alinéa est fixé à 15 %. / Pour les exercices ouverts à compter du 1er décembre 2007, le montant net des plus-values à long terme afférentes aux titres des sociétés à prépondérance immobilière définies au a sexies-0 bis cotées est imposé au taux prévu au IV. (...) / a sexies-0 bis Le régime des plus et moins-values à long terme cesse de s'appliquer à la plus ou moins-value provenant des cessions de titres de sociétés à prépondérance immobilière non cotées réalisées à compter du 26 septembre 2007. Sont considérées comme des sociétés à prépondérance immobilière les sociétés dont l'actif est, à la date de la cession de ces titres ou a été à la clôture du dernier exercice précédant cette cession, constitué pour plus de 50 % de sa valeur réelle par des immeubles, des droits portant sur des immeubles, des droits afférents à un contrat de crédit-bail conclu dans les conditions prévues au 2 de l'article L. 313-7 du code monétaire et financier ou par des titres d'autres sociétés à prépondérance immobilière. Pour l'application de ces dispositions, ne sont pas pris en considération les immeubles ou les droits mentionnés à la phrase précédente lorsque ces biens ou droits sont affectés par l'entreprise à sa propre exploitation industrielle, commerciale ou agricole ou à l'exercice d'une profession non commerciale. / Les provisions pour dépréciation afférentes aux titres exclus du régime des plus et moins-values à long terme en application du premier alinéa cessent d'être soumises à ce même régime (...) " ; qu'aux termes du dix-neuvième alinéa du 5° du 1 de l'article 39 du code général des impôts, pour la détermination des résultats des exercices ouverts à compter du 1er janvier 2007, les dotations aux provisions pour dépréciation comptabilisées au titre de l'exercice sur l'ensemble des titres de participation de sociétés à prépondérance immobilière définies au a sexies-0 bis du I de l'article 219 " ne sont pas déductibles à hauteur du montant des plus-values latentes existant à la clôture du même exercice sur les titres appartenant à cet ensemble "  ; qu'en vertu du VI de l'article 209 du code général des impôts, les dispositions du dix-neuvième alinéa du 5° du 1 de l'article 39 s'appliquent distinctement aux titres de sociétés à prépondérance immobilière cotées et aux titres de sociétés à prépondérance immobilière non cotées ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que la détermination des dotations aux provisions pour dépréciation des titres de participation de sociétés à prépondérance immobilière, qu'elles soient cotées ou non, exige de valoriser  ces dépréciations éventuelles ainsi que les plus-values latentes à la clôture de l'exercice sur l'ensemble des titres de participation de même nature détenues par l'entreprise ; qu'en prévoyant, au paragraphe 70 de l'instruction BOI-IS-BASE-20-20-10-30 du 31 décembre 2013, que lorsque les titres des sociétés à prépondérance immobilière, qu'ils fassent l'objet d'une cession ou non, font l'objet d'une dotation ou d'une reprise au compte de provisions pour dépréciation, le caractère immobilier prépondérant de ces sociétés s'apprécie à la date de clôture de l'exercice de l'entreprise qui détient les titres, alors que la loi prévoit que, pour les titres qui font l'objet d'une cession, le caractère immobilier prépondérant des sociétés concernées s'apprécie à la date de la cession des titres ou à la clôture du dernier exercice précédant cette cession et qu'aucune disposition de la loi ne précise comment s'apprécie le caractère immobilier prépondérant des sociétés en l'absence de cession des titres, le ministre ne s'est pas borné à expliciter la loi mais y a ajouté des dispositions nouvelles ; que, par suite, la société L'Auxiliaire est fondée à demander l'annulation de ce paragraphe, qui est entaché d'incompétence ; <br/>
<br/>
              3. Considérant que l'exécution de la présente décision n'implique pas, comme le réclame la société requérante, que le ministre des finances et des comptes publics remplace la disposition entachée d'illégalité par une nouvelle disposition ; qu'il y a donc lieu de rejeter ses conclusions aux fins d'injonction ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : Le paragraphe 70 de l'instruction fiscale BOI-IS-BASE-20-20-10-30 du 31 décembre 2013 est annulé.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de la société L'Auxiliaire est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société d'assurance mutuelle l'Auxiliaire et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
