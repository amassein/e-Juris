<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035608409</ID>
<ANCIEN_ID>JG_L_2017_09_000000401364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/60/84/CETATEXT000035608409.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/09/2017, 401364</TITRE>
<DATE_DEC>2017-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401364.20170922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise, en premier lieu, sous le n° 1202501, d'annuler la décision du 9 janvier 2009 par laquelle le directeur des ressources humaines de la direction générale de l'armement a refusé son reclassement, de condamner l'Etat à lui payer une indemnité correspondant à la perte de rémunération et de droits à pension consécutive à ce refus et d'enjoindre au ministre de la défense de procéder à la reconstitution de sa carrière à compter du 1er mars 2003, en deuxième lieu, sous le n° 1205670, d'annuler la décision implicite par laquelle le directeur des ressources humaines de la direction générale de l'armement a rejeté sa demande tendant à être licencié et d'enjoindre à l'Etat de procéder à son licenciement dans le délai de quinze jours suivant la notification du jugement à intervenir et de lui allouer une indemnité de licenciement et, en dernier lieu, sous le n° 1210533, d'annuler la décision du 12 novembre 2012 par laquelle le directeur des ressources humaines de la direction générale de l'armement l'a licencié à titre disciplinaire et d'enjoindre à l'Etat de le réintégrer et de procéder à la reconstitution de sa carrière et de ses droits à pension. Par un jugement n°s 1202501, 1205670, 1210533, 1205665 du 24 juillet 2014, le tribunal administratif de Cergy-Pontoise a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 14VE02806 du 10 mai 2016, la cour administrative d'appel de Versailles a rejeté l'appel de M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 juillet et 11 octobre 2016 et le 23 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le décret n° 88-451 du 4 mai 1988 ;<br/>
              - l'arrêté du 4 mai 1988 fixant les modalités de recrutement, le régime de rémunération et de déroulement de carrière des agents régis par le décret n° 88-451 du 4 mai 1988 ;<br/>
              - l'arrêté du 4 mai 1988 relatif aux modalités de recrutement et de rémunération des agents sur contrat du ministère de la défense dans les services de la délégation générale pour l'armement qui n'ont pas un caractère industriel ou commercial ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              1. Considérant, en premier lieu, que, dans l'intérêt d'une bonne administration de la justice, le juge administratif dispose, sans jamais y être tenu, de la faculté de joindre deux ou plusieurs affaires ; que l'absence de jonction est, par elle-même, insusceptible d'avoir un effet sur la régularité de la décision rendue et ne peut, par suite, être contestée en tant que telle devant le juge de cassation ; que, par suite, le moyen tiré de ce que la cour administrative d'appel de Versailles aurait entaché la procédure juridictionnelle d'irrégularité en ne procédant pas à la jonction des deux requêtes de M. A...dirigées contre le jugement du 24 juillet 2014 doit être écarté ; <br/>
<br/>
              2. Considérant, en second lieu, que s'il est loisible au requérant de solliciter la communication auprès du rapporteur public qui a porté la parole à l'audience de ses conclusions, celui-ci reste cependant libre d'apprécier la suite à donner à une telle demande ; que les conclusions du rapporteur public peuvent au demeurant ne pas être écrites ; que, par suite, le moyen tiré de ce que la procédure devant la cour serait entachée d'irrégularité du fait du refus de communication au requérant des conclusions du rapporteur public postérieurement à leur lecture en séance publique ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. A...a été recruté au sein de la direction des constructions navales du ministère de la défense en qualité d'agent contractuel pour occuper un poste d'ingénieur du corps technique à compter du 2 novembre 1992 ; qu'il a été affecté le 1er mars 2003 au service de la maintenance aéronautique de la direction générale de l'armement en qualité de directeur des systèmes d'information puis, à compter du 4 juillet 2007, auprès de l'unité de management " opérations d'armement navales " de la direction des systèmes d'armes en qualité de chef du département achats ; qu'il a sollicité le 9 décembre 2008 son reclassement dans la position III C à compter du 1er mars 2003 ; que cette demande a été rejetée par une décision du 9 janvier 2009 ; que, estimant que son contrat conclu en 1992 était entaché d'irrégularité, M. A...en a demandé la régularisation par un courrier du 13 décembre 2011 ; que, par courrier du 17 février 2012, il a refusé de signer le nouveau contrat de travail qui lui était proposé et a demandé à l'administration, par un courrier du 8 mars 2012, de prononcer son licenciement ; que l'administration a implicitement rejeté cette demande ; que, par une décision du 12 novembre 2012, le directeur des ressources humaines de la direction générale de l'armement a prononcé le licenciement disciplinaire de M. A...à compter du 1er décembre 2012 ;<br/>
<br/>
              4. Considérant, en premier lieu, que, sauf s'il présente un caractère fictif ou frauduleux, le contrat de recrutement d'un agent contractuel de droit public crée des droits au profit de celui-ci ; que, lorsque le contrat est entaché d'une irrégularité, notamment parce qu'il méconnaît une disposition législative ou réglementaire applicable à la catégorie d'agents dont relève l'agent contractuel en cause, l'administration est tenue de proposer à celui-ci une régularisation de son contrat afin que son exécution puisse se poursuive régulièrement ; que si le contrat ne peut être régularisé, il appartient à l'administration, dans la limite des droits résultant du contrat initial, de proposer à l'agent un emploi de niveau équivalent, ou, à défaut d'un tel emploi et si l'intéressé le demande, tout autre emploi, afin de régulariser sa situation ; que si l'intéressé refuse la régularisation de son contrat ou si la régularisation de sa situation, dans les conditions précisées ci-dessus, est impossible, l'administration est tenue de le licencier ; <br/>
<br/>
              5. Considérant que, lorsqu'elle n'implique la modification d'aucun de ses éléments substantiels, l'administration procède à la  régularisation du contrat de l'agent,  sans être tenue d'obtenir son accord ; que, dès lors, si l'agent déclare refuser la régularisation à laquelle a procédé l'administration, ce refus n'y fait pas obstacle et l'administration n'est pas tenue de licencier l'agent ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces soumis à l'examen des juges du fond que M. A...a été muté à sa demande, à compter du 4 juillet 2007, au sein de la direction des systèmes d'armes, service à caractère administratif de la direction générale de l'armement ; que son contrat se référait, en vertu de son article 1er, aux dispositions de l'arrêté du 4 mai 1988 fixant les modalités de recrutement ainsi que le régime de rémunération et de déroulement de carrière des agents régis par le décret du 4 mai 1988 relatif à certains agents sur contrat des services à caractère industriel ou commercial du ministère de la défense, qui était applicable jusque là à sa situation ; qu'il résulte de ce qui a été dit au point précédent, qu'après avoir relevé, par des motifs non contestés en cassation, que l'administration, en régularisant le contrat litigieux pour qu'il se réfère désormais à l'arrêté du 4 mai 1988 relatif aux modalités de recrutement et de rémunération des agents sur contrat du ministère de la défense dans les services de la délégation générale pour l'armement qui n'ont pas un caractère industriel ou commercial, n'avait modifié aucun de ses éléments substantiels, la cour a pu, sans erreur de droit, en déduire que, malgré le refus qu'il avait opposé, l'administration n'était pas tenue de licencier M. A...; que, dès lors,  M. A...ne peut utilement soutenir, en tout état de cause, que la cour aurait dénaturé les faits en estimant que le refus qu'il a opposé à la régularisation qu'elle lui proposait devait être interprété comme une manifestation de son intention de démissionner ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que, contrairement à ce qui est soutenu, la cour administrative d'appel de Versailles n'a pas dénaturé les pièces du dossier qui lui était soumis en estimant que les missions confiées à M. A...en tant que directeur des systèmes d'information du service de la maintenance aéronautique de la direction générale de l'armement entre le 1er mars 2003 et le 4 juillet 2007 n'impliquaient pas l'exercice de fonctions d'encadrement et que le poste de chef du département achats de l'unité de management " opérations d'armement navales " de la direction des systèmes d'armes qu'il occupait depuis cette dernière date comportait des fonctions de direction d'un niveau de responsabilité correspondant à la position III C ;<br/>
<br/>
              8. Considérant, en dernier lieu, que si l'article 44 du décret du 17 janvier 1986 relatif aux dispositions générales applicables aux agents contractuels de l'Etat prévoit le droit de l'agent non titulaire à l'encontre duquel une sanction disciplinaire est envisagée à la communication de l'intégralité de son dossier individuel et de tous documents annexes et à l'assistance par les défenseurs de son choix, il ne mentionne pas, parmi les formalités applicables à une sanction disciplinaire, l'exigence d'un entretien préalable ; que celle-ci n'est prévue par l'article 47 du même décret que pour les mesures de licenciement prononcées à un titre autre que disciplinaire ; qu'il ressort des pièces du dossier soumis aux juges du fond que le licenciement de M.A..., prononcé le 12 novembre 2012 par le directeur des ressources humaines de la direction générale de l'armement, revêt un caractère disciplinaire ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que M. A...ne pouvait utilement faire valoir qu'il n'avait pas bénéficié de l'entretien préalable prévu par l'article 47 du décret précité ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la ministre des armées.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12-02 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. EXÉCUTION DU CONTRAT. - CONTRAT DE RECRUTEMENT D'UN AGENT CONTRACTUEL DE DROIT PUBLIC IRRÉGULIER - OBLIGATION DE LICENCIER L'INTÉRESSÉ S'IL REFUSE LA RÉGULARISATION [RJ1] - CAS OÙ LA RÉGULARISATION N'IMPLIQUE LA MODIFICATION D'AUCUN DES ÉLÉMENTS SUBSTANTIELS DU CONTRAT - NÉCESSITÉ POUR L'ADMINISTRATION D'OBTENIR L'ACCORD DE L'AGENT - ABSENCE - CONSÉQUENCE [RJ2].
</SCT>
<ANA ID="9A"> 36-12-02 Obligation pour l'administration de proposer la régularisation du contrat de recrutement d'un agent contractuel de droit public en cas d'irrégularité de ce dernier et, si la régularisation n'est pas possible, un emploi de niveau équivalent ou tout autre emploi, et enfin de licencier l'intéressé s'il refuse la régularisation.... ,,Lorsqu'elle n'implique la modification d'aucun de ses éléments substantiels, l'administration procède à la  régularisation du contrat de l'agent, sans être tenue d'obtenir son accord. Dès lors, si l'agent déclare refuser la régularisation à laquelle a procédé l'administration, ce refus n'y fait pas obstacle et l'administration n'est pas tenue de licencier l'agent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 31 décembre 2008,,, n° 283256, p. 481.,,[RJ2] Comp. CE, Section, 31 décembre 2008,,, n° 283256, p. 481.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
