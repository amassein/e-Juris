<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245512</ID>
<ANCIEN_ID>JG_L_2017_07_000000393408</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245512.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 19/07/2017, 393408</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393408</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393408.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du 16 octobre 2014 du préfet du Puy-de-Dôme suspendant la validité de son permis de conduire à compter de cette date et la décision du 24 décembre 2014 par laquelle ce dernier a rejeté son recours gracieux contre cette décision. Par un jugement n° 1500279 du 10 juillet 2015, le tribunal administratif a annulé ces décisions.<br/>
<br/>
              Par un pourvoi, enregistré le 10 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              - l'arrêté du 21 décembre 2005 fixant la liste des affections médicales incompatibles avec l'obtention ou le maintien du permis de conduire ou pouvant donner lieu à la délivrance de permis de conduire de durée de validité limitée ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond qu'après la suspension du permis de conduire de M. A...pour six mois du fait de la conduite sous usage de stupéfiants, le préfet du Puy-de-Dôme a décidé de soumettre l'intéressé à un contrôle médical ; qu'à la suite de l'avis émis par la commission médicale, le préfet a prononcé une nouvelle suspension de la validité de son permis de conduire, par une décision du 16 octobre 2014, puis rejeté son recours gracieux, par une décision du 24 décembre 2014 ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 10 juillet 2015 par lequel le tribunal administratif de Clermont-Ferrand a annulé ces décisions ;<br/>
<br/>
              Sur le pourvoi du ministre de l'intérieur :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 11 juillet1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, en vigueur à la date de la décision préfectorale litigieuse et aujourd'hui repris à l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui :/ 1° Restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police (...) " ; qu'aux termes de l'article 3 de la même loi, aujourd'hui repris à l'article L. 211-5 du même code : " La motivation exigée par la présente loi doit être écrite et comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision " ; qu'aux termes du second alinéa de son article 4, repris à l'article L. 211-6 du code : " Les dispositions de la présente loi ne dérogent pas aux textes législatifs interdisant la divulgation ou la publication de faits couverts par le secret " ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1111-7 du code de la santé publique : " Toute personne a accès à l'ensemble des informations concernant sa santé détenues, à quelque titre que ce soit, par des professionnels et établissements de santé, qui sont formalisées ou ont fait l'objet d'échanges écrits entre professionnels de santé, notamment des résultats d'examen, comptes rendus de consultation, d'intervention, d'exploration ou d'hospitalisation, des protocoles et prescriptions thérapeutiques mis en oeuvre, feuilles de surveillance, correspondances entre professionnels de santé, à l'exception des informations mentionnant qu'elles ont été recueillies auprès de tiers n'intervenant pas dans la prise en charge thérapeutique ou concernant un tel tiers. / Elle peut accéder à ces informations directement ou par l'intermédiaire d'un médecin qu'elle désigne et en obtenir communication, dans des conditions définies par voie réglementaire au plus tard dans les huit jours suivant sa demande et au plus tôt après qu'un délai de réflexion de quarante-huit heures aura été observé. (...). "<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 221-14 du code de la route : " I. -  Postérieurement à la délivrance du permis, le préfet peut enjoindre à un conducteur de se soumettre à un contrôle médical :/ 1° Dans le cas où les informations en sa possession lui permettent d'estimer que l'état physique du titulaire du permis peut être incompatible avec le maintien de ce permis de conduire. Cet examen médical est réalisé par un médecin agréé consultant hors commission médicale ; au vu de l'avis médical émis, le préfet prononce, s'il y a lieu, soit la restriction de validité, la suspension ou l'annulation du permis de conduire, soit le changement de catégorie de ce titre " ; que, par l'arrêté visé ci-dessus du 21 décembre 2005, les ministres chargés de la sécurité routière et de la santé, habilités à cet effet par les dispositions du 2° de l'article R. 226-2 du code de la route, ont fixé la liste des affections médicales incompatibles avec l'obtention ou le maintien du permis de conduire, en précisant, pour la plupart d'entre elles, les critères d'appréciation de l'incompatibilité et l'étendue de celle-ci ; que les médecins chargés du contrôle médical sont tenus au secret médical dans les conditions rappelées au premier alinéa de l'article R. 4127-104 du code de la santé publique relatifs aux devoirs des médecins exerçant la médecine de contrôle, aux termes duquel : " Le médecin chargé du contrôle est tenu au secret envers l'administration ou l'organisme qui fait appel à ses services. Il ne peut et ne doit lui fournir que ses conclusions sur le plan administratif, sans indiquer les raisons d'ordre médical qui les motivent " ; qu'il appartient, en revanche, au médecin chargé du contrôle, lorsqu'il estime que le titulaire du permis de conduire est inapte à la conduite, de porter à sa connaissance le motif d'inaptitude qu'il retient parmi ceux que mentionne l'arrêté du 21 décembre 2005 ; qu'il est, par ailleurs, loisible à l'intéressé de demander communication, sur le fondement des dispositions de l'article L. 1111-7 précité du code de la santé publique, des documents énonçant ces motifs conservés par le médecin ;<br/>
<br/>
              5. Considérant que la décision par laquelle le préfet suspend ou annule un permis de conduire, ou restreint sa validité, au motif que son titulaire est atteint d'une affection médicale incompatible avec la conduite d'un véhicule présente le caractère d'une mesure de police et doit, par suite, être motivée  ; qu'il ressort des pièces du dossier soumis aux juges du fond que la décision du préfet du préfet du Puy-de-Dôme du 16 octobre 2014, qui mentionne les dispositions du code de la route dont elle fait application, vise l'avis rendu par la commission médicale des permis de conduire le 29 août 2014, concluant à l'inaptitude de M. A...à la conduite d'un véhicule à moteur, et mentionne que l'intéressé a pris connaissance des motifs d'ordre médical justifiant cette conclusion ; qu'en jugeant que le préfet avait méconnu les dispositions de la loi du 11 juillet 1979 en s'abstenant de faire figurer ces motifs dans sa décision, alors que le secret médical interdit aux médecins chargés du contrôle de préciser dans leur avis destiné à l'administration l'affection qu'ils ont constatée, le tribunal administratif a commis une erreur de droit ; qu'en ajoutant qu'il n'était pas établi que M. A...avait eu connaissance de ces motifs médicaux, alors que le préfet avait joint à son mémoire en défense le formulaire rempli à l'issue du contrôle médical, sur lequel l'intéressé avait apposé sa signature sous la mention selon laquelle il reconnaissait avoir pris connaissance des motifs médicaux justifiant l'avis d'inaptitude, le tribunal a entaché son jugement de dénaturation ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le jugement du tribunal administratif de Clermont-Ferrand du 10 juillet 2015 doit être annulé ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Clermont-Ferrand du 10 juillet 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Clermont-Ferrand.<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur, et à M. B... A....  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION RESTREIGNANT L'EXERCICE DES LIBERTÉS PUBLIQUES OU, DE MANIÈRE GÉNÉRALE, CONSTITUANT UNE MESURE DE POLICE. - DÉCISION DU PRÉFET DE SUSPENDRE OU D'ANNULER LE PERMIS DE CONDUIRE OU DE RESTREINDRE SA VALIDITÉ AU MOTIF QUE SON TITULAIRE EST ATTEINT D'UNE AFFECTION MÉDICALE INCOMPATIBLE AVEC LA CONDUITE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - DÉCISION DU PRÉFET DE SUSPENDRE OU D'ANNULER LE PERMIS DE CONDUIRE OU DE RESTREINDRE SA VALIDITÉ AU MOTIF QUE SON TITULAIRE EST ATTEINT D'UNE AFFECTION MÉDICALE INCOMPATIBLE AVEC LA CONDUITE, ALORS MÊME QU'ELLE NE FAIT PAS FIGURER LES MOTIFS D'ORDRE MÉDICAL [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - CAS OÙ LE TITULAIRE D'UN PERMIS DE CONDUIRE EST DÉCLARÉ INAPTE À LA CONDUITE À L'ISSUE D'UN CONTRÔLE MÉDICAL (ART. R. 221-14 DU CODE DE LA ROUTE) - 1) OBLIGATION DU MÉDECIN CHARGÉ DU CONTRÔLE MÉDICAL D'INFORMER L'INTÉRESSÉ DU MOTIF D'INAPTITUDE - EXISTENCE - 2) DÉCISION DU PRÉFET DE SUSPENDRE OU D'ANNULER LE PERMIS DE CONDUIRE OU DE RESTREINDRE SA VALIDITÉ [RJ1] - A) OBLIGATION DE MOTIVATION - EXISTENCE - B) MODALITÉS DE MOTIVATION [RJ2].
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-01 La décision par laquelle le préfet suspend ou annule un permis de conduire, ou restreint sa validité, au motif que son titulaire est atteint d'une affection médicale incompatible avec la conduite d'un véhicule, présente le caractère d'une mesure de police et doit, par suite, être motivée.</ANA>
<ANA ID="9B"> 01-03-01-02-02-02 Décision du préfet de suspendre ou annuler un permis de conduire, ou de restreindre sa validité, au motif que son titulaire est atteint d'une affection médicale incompatible avec la conduite d'un véhicule.... ,,La décision mentionne les dispositions du code de la route dont elle fait application, vise l'avis rendu par la commission médicale des permis de conduire concluant à l'inaptitude de l'intéressé à la conduite d'un véhicule à moteur et mentionne que l'intéressé a pris connaissance des motifs d'ordre médical justifiant cette conclusion. Le préfet ne méconnaît pas les dispositions de la loi n° 79-587 du 11 juillet 1979 en s'abstenant de faire figurer ces motifs dans sa décision, dès lors que le secret médical interdit aux médecins chargés du contrôle de préciser dans leur avis destiné à l'administration l'affection qu'ils ont constatée.</ANA>
<ANA ID="9C"> 49-04-01-04 1) Il appartient au médecin chargé du contrôle prévu par l'article R. 221-14 du code de la route, lorsqu'il estime que le titulaire du permis de conduire est inapte à la conduite, de porter à sa connaissance le motif d'inaptitude qu'il retient parmi ceux que mentionne l'arrêté du 21 décembre 2005. Il est, par ailleurs, loisible à l'intéressé de demander communication, sur le fondement des dispositions de l'article L. 1111-7 du code de la santé publique, des documents énonçant ces motifs conservés par le médecin.,,,2) a) La décision par laquelle le préfet suspend ou annule un permis de conduire, ou restreint sa validité, au motif que son titulaire est atteint d'une affection médicale incompatible avec la conduite d'un véhicule présente le caractère d'une mesure de police et doit, par suite, être motivée.,,,b) Décision du préfet mentionnant les dispositions du code de la route dont elle fait application, visant l'avis rendu par la commission médicale des permis de conduire concluant à l'inaptitude de l'intéressé à la conduite d'un véhicule à moteur et mentionnant que l'intéressé a pris connaissance des motifs d'ordre médical justifiant cette conclusion. Le préfet ne méconnaît pas les dispositions de la loi n° 79-587 du 11 juillet 1979 en s'abstenant de faire figurer ces motifs dans sa décision, dès lors que le secret médical interdit aux médecins chargés du contrôle de préciser dans leur avis destiné à l'administration l'affection qu'ils ont constatée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., en ce qui concerne l'obligation de motivation, CE, 10 mai 1995, Ministre de l'équipement, du logement, des transports et de l'espace c/ Gravey, n° 127339, p. 202.,,[RJ2]Ab. jur., en ce qui concerne le contenu de la motivation, CE, 10 mai 1995, Ministre de l'équipement, du logement, des transports et de l'espace c/ Gravey, n° 127339, p. 202.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
