<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041555229</ID>
<ANCIEN_ID>JG_L_2020_02_000000433738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/55/52/CETATEXT000041555229.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 10/02/2020, 433738, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Marie Walazyc</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433738.20200210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina " a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 31 août 2016 par lequel le maire d'Antibes a accordé à la société Kaufman et Broad Côte d'Azur un permis de construire, valant permis de démolir, pour la réalisation d'un immeuble d'habitation comprenant 57 logements sur un terrain situé chemin des Plateaux Fleuris, ainsi que la décision implicite rejetant son recours gracieux. Par un jugement n° 1700861 du 20 juin 2019, le tribunal administratif de Nice a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 août et 20 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune d'Antibes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande du syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina " ; <br/>
<br/>
              3°) de mettre à la charge du syndicat des coproriétaires de l'immeuble " Le Parc d'Elvina " la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
Vu : <br/>
- le code de l'urbanisme ;<br/>
- le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Walazyc, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune d'Antibes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation du jugement du tribunal administratif de Nice qu'elle attaque, la commune d'Antibes soutient que : <br/>
              - le tribunal a dénaturé les faits et les pièces du dossier, d'une part, en retenant que l'avis du service d'incendie et de secours prescrivant la création d'une voie engins d'un rayon intérieur minimum de onze mètres portait sur la connexion entre le chemin privé et la voie interne du projet de construction et, d'autre part, que cette prescription reprise par le permis de construire était manifestement irréalisable  ;<br/>
              - il a commis une erreur de droit en faisant application des dispositions de l'article UC 3 du règlement du plan local d'urbanisme au croisement de la voie interne du projet et du chemin des jardins d'Elvina ;<br/>
              - il a dénaturé les faits et pièces du dossier en estimant que l'accès au terrain d'assiette prévu pour les engins d'incendie et de secours n'était pas adapté à l'opération et ne satisfaisait pas aux exigences de sécurité et de défense contre l'incendie et était de nature à créer un risque pour la sécurité publique ;  <br/>
              - il a commis une erreur de droit en ne se bornant pas, eu égard aux motifs qu'il retenait, à annuler l'arrêté du 31 août 2016 en tant qu'il portait permis de construire et en l'annulant également en tant qu'il valait permis de démolir ;<br/>
              - il a commis une erreur de droit en ne recherchant pas d'office si les vices entachant le permis de construire qu'il avait relevés étaient susceptibles de régularisation sur le fondement des articles L. 600-5 ou L. 600-5-1 du code de l'urbanisme.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre le jugement attaqué en tant qu'il annule l'arrêté du maire d'Antibes du 31 août 2016 en ce qu'il vaut permis de démolir et la décision implicite rejetant le recours gracieux contre ce permis. En revanche, s'agissant des conclusions dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur le surplus des conclusions de la demande, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de la commune d'Antibes qui sont dirigées contre le jugement attaqué en tant qu'il annule l'arrêté du maire d'Antibes du 31 août 2016 en ce qu'il vaut permis de démolir et la décision implicite rejetant le recours gracieux contre ce permis sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la commune d'Antibes n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Antibes.<br/>
Copie en sera adressée au syndicat des copropriétaires de l'immeuble " Le Parc d'Elvina " et à la société Kaufman et Broad Côte d'Azur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
