<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998447</ID>
<ANCIEN_ID>JG_L_2014_12_000000379615</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998447.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 30/12/2014, 379615, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379615</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Henri Loyrette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:379615.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 22 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-262 du 26 février 2014 portant délimitation des cantons dans le département des Pyrénées-Orientales ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu la loi n° 86-825 du 11 juillet 1986 ; <br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Henri Loyrette, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la même loi du 17 mai 2013, applicable à la date du décret attaqué : " I.- Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / II. - La qualité de chef-lieu de canton est maintenue aux communes qui la perdent dans le cadre d'une modification des limites territoriales des cantons, prévue au I, jusqu'au prochain renouvellement général des conseils généraux. / III. La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants. / IV. Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département des Pyrénées-Orientales, compte tenu de l'exigence de réduction du nombre des cantons de ce département de 31 à 17 résultant de l'article L. 191-1 du code électoral ; <br/>
<br/>
              3. Considérant, en premier lieu, que, pour mettre en oeuvre les critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales, le décret attaqué a procédé à la délimitation des dix-sept nouveaux cantons du département des Pyrénées-Orientales en se fondant sur une population moyenne de 26 385 habitants et en rapprochant la population de chaque canton de cette moyenne ; que la circonstance que la population du nouveau canton de la Côte Sableuse pourrait, compte tenu de son fort potentiel démographique, dépasser à terme de plus de 20 % la population moyenne des cantons du département est par elle-même sans incidence sur la légalité du décret attaqué ; que cette nouvelle délimitation ne méconnaît pas le principe posée au III de l'article L. 3113-2 du code général des collectivités territoriales selon lequel elle doit être faite sur des bases essentiellement démographiques ; que, par suite, le moyen  tiré de ce que la délimitation porterait atteinte au principe d'égalité des citoyens devant le suffrage ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, d'une part, prennent en compte la précédente délimitation cantonale, ou les prévisions d'évolution des populations, et, d'autre part, s'agissant de circonscriptions électorales, coïncident avec les périmètres des établissements publics de coopération intercommunale ; <br/>
<br/>
              5. Considérant, en troisième lieu, que si le requérant soutient que la délimitation du canton de la Côte Sableuse revêtirait un caractère arbitraire, il n'assortit ses allégations d'aucun élément de nature à les conforter ; que, par suite, le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : La requête de M. B...est rejetée. <br/>
<br/>
 Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'Intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
