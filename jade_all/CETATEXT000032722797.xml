<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032722797</ID>
<ANCIEN_ID>JG_L_2016_06_000000375951</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/27/CETATEXT000032722797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 13/06/2016, 375951, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375951</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:375951.20160613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Banque de Vizille a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations minimales de taxe professionnelle auxquelles elle a été assujettie au titre des années 2005 à 2007, ainsi que des pénalités correspondantes. Par un jugement n° 1008080 du 24 janvier 2012, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE01175 du 30 décembre 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement par la SA CM CIC Investissement, venant aux droits et obligations de la société Banque de Vizille.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 mars et 3 juin 2014 au secrétariat du contentieux du Conseil d'Etat, la SA CM CIC Investissement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État une somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 85-695 du 11 juillet 1985 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la SA CM CIC Investissement ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières ". En vertu de l'article 1er de cette loi, elles doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés. Il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle.<br/>
<br/>
              2. D'autre part, aux termes du I de l'article 1647 E du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies (...) ". Selon le II de l'article 1647 B sexies du même code, dans sa rédaction alors en vigueur : " 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes, les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion de valeurs mobilières est égale à la différence entre : / d'une part, les produits d'exploitation bancaires et produits accessoires ; / et, d'autre part, les charges d'exploitation bancaires. (...) ". Eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des seules entreprises qui exercent cette activité pour leur propre compte. Les sociétés de capital-risque, qui gèrent pour leur propre compte des participations financières, sont, dès lors, soumises aux modalités de calcul de la valeur ajoutée prévues au 3 du II de l'article 1647 B sexies du code général des impôts pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières.<br/>
<br/>
              3. Les dispositions de l'article 1647 B sexies du code général des impôts fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation minimale de taxe professionnelle. Pour l'application de ces dispositions, la production d'une entreprise ayant pour activité exclusive la gestion de valeurs mobilières doit être calculée, comme celle des établissements de crédit, en fonction des règles comptables fixés par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit. L'annexe à ce règlement contient un modèle de compte de résultat et des commentaires de chacun des postes de ce compte. En vertu de ces dispositions, les produits d'exploitation bancaires et produits accessoires incluent les " gains et pertes sur opérations des portefeuilles de négociation " et les " gains et pertes sur opérations des portefeuilles de placement et assimilés ", mais non les " gains et pertes sur actifs immobilisés ", ce dernier poste étant défini comme " le solde en bénéfice ou perte des opérations sur titres de participation, sur autres titres détenus à long terme et sur parts dans les entreprises liées ".<br/>
<br/>
              4. Pour rejeter la demande de la SA CM CIC Investissement tendant à la décharge des cotisations minimales de taxe professionnelle auxquelles la société Banque de Vizille avait été assujettie au titre des années 2005 à 2007, la cour administrative d'appel de Versailles a jugé que l'administration était fondée à prendre en compte, pour la détermination de la valeur ajoutée servant au calcul de la cotisation minimale de taxe professionnelle de cette société de capital-risque, l'ensemble des plus-values de cessions de valeurs mobilières, sans exclure les plus-values sur titres de participation. En statuant ainsi, la cour a commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 1 500 euros à verser à la SA CM CIC Investissement au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 30 décembre 2013 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'État versera à la SA CM CIC Investissement une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SA CM CIC Investissement et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
