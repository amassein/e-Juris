<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033284725</ID>
<ANCIEN_ID>J0_L_2016_10_000001402361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/47/CETATEXT000033284725.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour administrative d'appel de Versailles, 6ème chambre, 20/10/2016, 14VE02361, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Cour administrative d'appel de Versailles</JURIDICTION>
<NUMERO>14VE02361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. DEMOUVEAUX</PRESIDENT>
<AVOCATS>CABINET FIDAL DIRECTION PARIS</AVOCATS>
<RAPPORTEUR>M. Jean-Eric  SOYEZ</RAPPORTEUR>
<COMMISSAIRE_GVT>M. DELAGE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société UCAR DEVELOPPEMENT a demandé au Tribunal administratif de Versailles qui a transmis sa demande au Tribunal administratif de Cergy-Pontoise, de la décharger des rappels de taxe différentielle sur les véhicules à moteur, en droits et intérêts de retard, qui lui ont été réclamés pour la période du 1er mars 2005 au 14 août 2006.<br/>
<br/>
       Par un jugement n° 1101301 du 30 juin 2014, le Tribunal administratif de <br/>
Cergy-Pontoise a rejeté cette demande. <br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête et un mémoire, enregistrés respectivement le 1er août 2014 et le 1er juin 2015, la société UCAR DEVELOPPEMENT, représentée par Me Cordier-Deltour, avocat, demande à la Cour : <br/>
<br/>
       1° d'annuler le jugement n° 1101301 du 30 juin 2014 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande ;<br/>
<br/>
       2° de prononcer une décharge d'un montant de 220 641 euros ;<br/>
<br/>
       3° de mettre à la charge de l'Etat la somme de 8 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - elle a été imposée à l'issue d'une procédure irrégulière, faute que la proposition de rectification l'ait informée de la faculté de proroger le délai de présentation de ses observations ;<br/>
       - les avis de mise en recouvrement sont entachés d'une erreur substantielle sur la nature de la taxe due, au regard de l'article R* 256-1 du livre des procédures fiscales et de la doctrine administrative référencée 12C122 du 1er décembre 1984 ; <br/>
       - à titre subsidiaire, l'administration a méconnu les articles 1599 C du code général des impôts et R. 322-1 du code de la route ; <br/>
       - la société requérante, qui a acquitté dans d'autres départements que l'Oise, à raison des véhicules dont elle était propriétaire, les droits rappelés, a été expressément autorisée par le ministère de l'intérieur à recourir à la procédure de télétransmission de données pour immatriculer dans le département de l'Oise des véhicules destinés à la location de courte durée ; <br/>
       - les rappels litigieux sont contraires à la chose jugée le 24 mai 2012 par le Tribunal de Grande Instance de Nanterre ;<br/>
<br/>
       ----------------------------------------------------------------------------------------------------------<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Soyez,<br/>
       - les conclusions de M. Delage, rapporteur public,<br/>
       - et les observations de MeA..., pour la société UCAR DEVELOPPEMENT.<br/>
<br/>
<br/>
       1. Considérant que la société U TOP LCD, devenue UCAR DEVELOPPEMENT, qui a pour activité la location de véhicules utilitaires et de tourisme, a fait l'objet d'une vérification de comptabilité au titre des exercices clos en 2005 et 2006 ; qu'à l'issue des opérations de contrôle, l'administration lui a notifié des rappels de droits de taxe différentielle sur les véhicules à moteur par deux propositions de rectification en date du 9 décembre 2008, pour la période du 1er janvier 2005 au 14 août 2006, au motif qu'elle aurait à tort bénéficié de l'exonération de cette taxe en immatriculant dans le département de l'Oise certains des véhicules dont elle disposait ; que sa réclamation ayant été rejetée le 13 décembre 2010, la contribuable a demandé au juge administratif la décharge des rappels de cette taxe pour la période du 1er mars 2005 au <br/>
14 août 2006 ; qu'elle relève appel du jugement du 30 juin 2014 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande ; <br/>
       Sans qu'il soit besoin d'examiner les autres moyens de la requête ; <br/>
<br/>
       2. Considérant qu'aux termes de l'article R* 256-1 du livre des procédures fiscales, dans sa rédaction alors applicable : " L'avis de mise en recouvrement prévu à l'article L. 256 indique pour chaque impôt ou taxe le montant global des droits, des pénalités et des intérêts de retard qui font l'objet de cet avis. Lorsque l'avis de mise en recouvrement est consécutif à une procédure de rectification, il fait référence à la proposition prévue à l'article L. 57 ou à la notification prévue à l'article L. 76 et, le cas échéant, au document adressé au contribuable l'informant d'une modification des droits, taxes et pénalités résultant des rectifications " ;<br/>
<br/>
       3. Considérant qu'il est constant que les deux avis de mise en recouvrement adressés à la société U TOP LCD le 17 août 2010 mentionnent non la taxe différentielle sur les véhicules à moteur, mais la " taxe sur les véhicules de sociétés - véhicules taxés sur les émissions de CO2 " ; qu'alors même qu'ils font expressément référence aux propositions de redressement du 9 décembre 2008, qui ne portent que sur la taxe différentielle sur les véhicules à moteur, et que leurs autres mentions sont exactes, ces avis sont ainsi entachés d'une erreur sur la nature même des droits mis en recouvrement ; qu'au surplus, ils font application, pour la détermination du montant des droits et intérêts de retard, de deux régimes juridiques distincts en raison de l'intervention de l'article 42 de la loi de finances du 30 décembre 2004 pour 2005, circonstance qui rendait ainsi difficile la comparaison entre les impositions notifiées dans les propositions de rectification et celles mises en recouvrement ; que, par suite, ces avis de mise en recouvrement n'étaient pas conformes aux prescriptions de l'article R* 256-1 précité du livre des procédures fiscales ; <br/>
<br/>
       4. Considérant qu'il résulte de ce qui précède que la société UCAR DEVELOPPEMENT est fondée à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la décharge des impositions contestées ;  <br/>
<br/>
       5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 5 000 euros à verser à la société UCAR DEVELOPPEMENT sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
DECIDE :<br/>
Article 1er : Le jugement n° 1101301 du Tribunal administratif de Cergy-Pontoise du 30 juin 2014 est annulé. <br/>
Article 2 : La société UCAR DEVELOPPEMENT est déchargée des rappels de taxe différentielle sur les véhicules à moteur, en droits et intérêts de retard, qui lui ont été réclamés pour la période du 1er mars 2005 au 14 août 2006.<br/>
Article 3 : L'Etat versera à la société UCAR DEVELOPPEMENT une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de la société UCAR DEVELOPPEMENT est rejeté.<br/>
       ----------------------------------------------------------------------------------------------------------<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 14VE02361<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-01-05 Contributions et taxes. Généralités. Recouvrement.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
