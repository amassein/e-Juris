<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036386815</ID>
<ANCIEN_ID>JG_L_2017_12_000000416530</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/38/68/CETATEXT000036386815.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/12/2017, 416530, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416530</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI, RIDOUX</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:416530.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre à la métropole de Lyon de procéder à son accueil en foyer ou en famille d'accueil, dans un délai de 48 heures et sous astreinte de 200 euros par jour de retard et, d'autre part, d'enjoindre à la métropole de Lyon de procéder à son inscription dans un établissement scolaire, dans un délai de huit jours, sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 1708304 du 29 novembre 2017 le juge des référés du tribunal administratif de Lyon a, d'une part, enjoint au préfet du Rhône de confier M. A...à une structure susceptible de le prendre en charge, de l'évaluer et de procéder à son inscription dans un établissement scolaire, jusqu'à ce que l'autorité judiciaire se soit prononcée sur sa situation dans un délai de trois jours à compter de la notification de l'ordonnance et, d'autre part, décidé que la prise en charge se fera sous la responsabilité des services de la direction de la protection judiciaire de la jeunesse, jusqu'à ce que l'autorité judiciaire ait statué sur sa situation. <br/>
              Par une requête, enregistrée le 13 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la requête présentée en première instance par M.A....<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - il est recevable à relever appel contre l'ordonnance du juge des référés du tribunal administratif de Lyon dès lors qu'elle ne lui a pas été notifiée, alors qu'il est le ministre de l'administration partie au litige et que, par voie de conséquence, son recours ne peut être considéré comme tardif ; <br/>
              - l'ordonnance contestée est entachée d'une erreur de droit dès lors qu'elle méconnaît la compétence de l'autorité judiciaire ainsi que la compétence de principe du département du Rhône en ce qu'elle enjoint au préfet du Rhône de confier M. A...à une structure susceptible de le prendre en charge, de l'évaluer et de procéder à son inscription dans un établissement scolaire, et ce en l'absence de toute décision judiciaire ; <br/>
              - la condition d'urgence n'est pas satisfaite dès lors que, d'une part, M.A..., a bénéficié d'une évaluation le 6 octobre 2017 et, d'autre part, la métropole du Grand Lyon a assuré, à compter de cette date, sa prise en charge provisoire dans l'attente d'une décision du juge judiciaire ; <br/>
              - l'ordonnance contestée est insuffisamment motivée dès lors que le juge des référés s'est fondé sur des considérations d'ordre général et des prévisions sur les capacités d'accueil de la métropole et du service sans apporter aucun élément spécifique à la situation de M. A...ou de nature à caractériser une carence dans la prise en charge des mineurs isolés étrangers ;<br/>
              - l'ordonnance contestée est entachée d'irrégularité dès lors qu'en enjoignant au préfet du Rhône d'accueillir en foyer ou en famille d'accueil et de procéder à la scolarisation de M. A...sans toutefois constater l'existence d'une carence caractérisée de la métropole de Lyon dans l'exécution de ses obligations, le juge des référés a excédé son office ; <br/>
              - l'ordonnance contestée est entachée d'une erreur de droit dès lors qu'elle considère les mineurs non encore admis par le juge des enfants à l'aide sociale à l'enfance sous la responsabilité juridique de l'Etat ; <br/>
              - l'ordonnance contestée est entachée d'une erreur de droit dès lors que la substitution de l'Etat au département doit être exceptionnelle, justifiée par un nombre important de mineurs et une urgence de prise en charge incompatible avec les capacités du département ;<br/>
              - l'ordonnance contestée est entachée d'une inexacte appréciation des faits dès lors qu'elle n'établit pas l'existence d'une carence caractérisée de la métropole de Lyon dans la prise en charge de M. A...ni l'existence d'une situation sans précédent exigeant l'adoption de mesures de sauvegarde excédant les capacités d'action du département de nature a entraîner une obligation d'agir pour le préfet.<br/>
              Par un mémoire en défense et des observations complémentaires, enregistrées les 18 et 20 décembre 2017, M. A...conclut au rejet de la requête. Il fait valoir que les moyens soulevés par le ministre d'Etat, ministre de l'intérieur, ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
              - le jugement en assistance éducative placement du 13 novembre 2017 du juge des enfants du tribunal pour enfant de Lyon ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre d'Etat, ministre de l'intérieur, d'autre part, M. A...et le département du Rhône ;<br/>
              Vu le procès-verbal de l'audience publique du 21 décembre 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentantes du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              - Me Gouz-Fitoussi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A...;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale ". <br/>
<br/>
              2. Aux termes de l'article L. 223-2 du code de l'action sociale et des familles dispose que " sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. (...) Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil. (...) ". L'article 375-5 du code civil dispose que dans cette situation, le procureur de la République ou le juge des enfants auquel la situation d'un mineur isolé a été signalée, décide de l'orientation du mineur concerné, laquelle peut consister en application de l'article 375-3 du même code en son admission à l'aide sociale à l'enfance. Si, en revanche, le département qui a recueilli la personne refuse de saisir l'autorité judiciaire, notamment parce qu'il estime que cette personne a atteint la majorité, cette personne peut saisir elle-même le juge des enfants en application de l'article 375 du code civil afin qu'il soit décidé de son orientation. <br/>
<br/>
              3. L'article R. 221-11 du code de l'action sociale et des familles définit la procédure applicable pour la mise en oeuvre de l'article L. 223-2 cité ci-dessus. Il dispose que " I.-Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II.-Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) IV.-Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". <br/>
<br/>
              4. Il résulte de l'instruction que M.A..., ressortissant guinéen, déclarant être né le 12 août 2012, est entré en France le 16 septembre 2017, et a été pris en charge par la métropole de Lyon à partir du début du mois d'octobre. Par une ordonnance de placement en assistance éducative du 13 novembre 2017, que la métropole de Lyon est tenue d'exécuter, le juge des enfants du tribunal pour enfant de Lyon a confié M. A...au service de l'aide sociale à l'enfance de la métropole jusqu'au 12 août 2020. <br/>
<br/>
              5. Dès lors, il y a lieu d'annuler l'ordonnance du 29 novembre 2017 du juge des référés du tribunal administratif de Lyon qui enjoint au préfet du Rhône de confier M. A...à un organisme susceptible de le prendre en charge, de l'évaluer et de procéder à son inscription dans un établissement scolaire, jusqu'à ce que l'autorité judiciaire se soit prononcée sur sa situation, dans un délai de trois jours à compter de la notification de l'ordonnance et de rejeter la demande de M. A...dirigée contre la métropole de Lyon.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance n° 1708304 du 29 novembre 2017 est annulée.<br/>
Article 2 : La présente ordonnance sera notifiée au ministre d'Etat, ministre de l'intérieur, à la métropole de Lyon, à M. B... A...et au département du Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
