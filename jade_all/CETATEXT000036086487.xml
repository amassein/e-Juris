<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036086487</ID>
<ANCIEN_ID>JG_L_2017_11_000000396294</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/08/64/CETATEXT000036086487.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 24/11/2017, 396294</TITRE>
<DATE_DEC>2017-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396294</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396294.20171124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1505616, 1505618, 1505624, 1505625 du 12 janvier 2016, enregistrée le 21 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Montreuil a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 24 juin 2015 au greffe de ce tribunal, présentée par M. B... A.... <br/>
<br/>
              Par cette requête et par un nouveau mémoire, enregistré le 8 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le e) du paragraphe 2.1 de l'article 2 du chapitre 5 du statut des relations collectives entre la Société nationale des chemins de fer français et son personnel ;<br/>
<br/>
              2°) de mettre à la charge de la Société nationale des chemins de fer français (SNCF) le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la décision du 5 novembre 2014 de la ministre de l'écologie, du développement durable et de l'énergie portant approbation des modifications du statut des relations collectives entre la Société nationale des chemins de fer français et son personnel examinées lors de la commission mixte du statut du 27 juin 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A...et à la SCP Spinosi, Sureau, avocat de la Société nationale des chemins de fer français ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. A...demande l'annulation pour excès de pouvoir des dispositions du e) du paragraphe 2.1 de l'article 2 du chapitre 5 du statut des relations collectives entre la Société nationale des chemins de fer français et son personnel, approuvées par le ministre chargé des transports, aux termes desquelles, pour pouvoir être admis dans un emploi du cadre permanent de l'établissement public, tout candidat doit : " être âgé de 18 ans au moins et de 30 ans au plus au jour de son admission " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative dans sa version applicable à la requête de M. A...: " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que l'intégralité du texte du statut des relations collectives entre la Société nationale des chemins de fer français (SNCF) et son personnel, mis à jour après chacune de ses modifications, est tenue à la disposition de l'ensemble des agents de la SNCF, par sa diffusion au sein des collections librement consultables de chacun des établissements ainsi que, depuis 1999, par sa publication sur l'intranet de la SNCF ; que chacune de ces modalités de publicité a le caractère d'une publication de nature à faire courir les délais de recours contentieux à l'égard des agents intéressés ;<br/>
<br/>
              4. Considérant que les dispositions attaquées figurent dans le statut du personnel de la SNCF depuis plus de quarante ans sans avoir jamais été modifiées ; qu'il ressort des pièces du dossier qu'elles ont ainsi été publiées, au moins depuis janvier 1970, dans les conditions rappelées au point précédent ; qu'en l'absence de lien indivisible entre ces dispositions et les autres dispositions du statut, aucune des modifications apportées, depuis 1970, aux autres dispositions du statut, notamment la modification approuvée par la décision du 5 novembre 2014 de la ministre de l'écologie, du développement durable et de l'énergie, n'ont été, contrairement à ce que soutient M.A..., de nature à rouvrir un délai de recours contre les dispositions qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la requête de M.A..., enregistrée le 24 juin 2015 au greffe du tribunal administratif de Montreuil, est tardive et, par suite, irrecevable ; qu'il y a lieu de la rejeter pour ce motif sans qu'il soit besoin de statuer sur les autres fins de non-recevoir soulevées en défense ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SNCF qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., à la Société nationale des chemins de fer français et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. PUBLICATION. - MODALITÉS DE PUBLICATION DE NATURE À FAIRE COURIR LES DÉLAIS DE RECOURS À L'ÉGARD DES AGENTS INTÉRESSÉS - DOCUMENT TENU À LA DISPOSITION DES AGENTS DE LA SNCF AU SEIN DES COLLECTIONS LIBREMENT CONSULTABLES DE CHAQUE ÉTABLISSEMENT - EXISTENCE [RJ1] - PUBLICATION SUR L'INTRANET DE LA SNCF - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-01-02-05-01 TRANSPORTS. TRANSPORTS FERROVIAIRES. OPÉRATEURS DE TRANSPORTS FERROVIAIRES. - CONTESTATION DU STATUT DES RELATIONS COLLECTIVES ENTRE LA SNCF ET SON PERSONNEL - MODALITÉS DE PUBLICATION DE NATURE À FAIRE COURIR LE DÉLAI DE RECOURS À L'ÉGARD DES AGENTS INTÉRESSÉS - DOCUMENT TENU À LA DISPOSITION DES AGENTS DE LA SNCF AU SEIN DES COLLECTIONS LIBREMENT CONSULTABLES DE CHAQUE ÉTABLISSEMENT - EXISTENCE [RJ1] - PUBLICATION SUR L'INTRANET DE LA SNCF - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 54-01-07-02-02 L'intégralité du texte du statut des relations collectives entre la Société nationale des chemins de fer français (SNCF) et son personnel, mis à jour après chacune de ses modifications, est tenue à la disposition de l'ensemble des agents de la SNCF, par sa diffusion au sein des collections librement consultables de chacun des établissements ainsi que, depuis 1999, par sa publication sur l'intranet de la SNCF. Chacune de ces modalités de publicité a le caractère d'une publication de nature à faire courir les délais de recours contentieux à l'égard des agents intéressés.</ANA>
<ANA ID="9B"> 65-01-02-05-01 L'intégralité du texte du statut des relations collectives entre la Société nationale des chemins de fer français (SNCF) et son personnel, mis à jour après chacune de ses modifications, est tenue à la disposition de l'ensemble des agents de la SNCF, par sa diffusion au sein des collections librement consultables de chacun des établissements ainsi que, depuis 1999, par sa publication sur l'intranet de la SNCF. Chacune de ces modalités de publicité a le caractère d'une publication de nature à faire courir les délais de recours contentieux à l'égard des agents intéressés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 février 1999,,, n° 102208, T. pp. 941-1047., ,[RJ2] Rappr., s'agissant de la publication d'une décision réglementaire régissant la situation des personnels d'un établissement public par une mise en ligne sur le site de l'établissement, CE, 11 janvier 2006, Syndicat national CGT-ANPE, n° 273665, T. pp. 713-892 ; CE, 24 avril 2012, Etablissement public Voies navigables de France, n° 339669, p. 166.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
