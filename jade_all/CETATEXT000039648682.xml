<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648682</ID>
<ANCIEN_ID>JG_L_2019_12_000000434071</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 19/12/2019, 434071, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434071</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:434071.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 30 août, 14 octobre et 25 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 21 août 2019 le révoquant de ses fonctions de maire de la commune d'Hesdin (Pas-de-Calais) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ; <br/>
              - le code général des collectivités territoriales, notamment son article L. 2122-16 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 2122-16 du code général des collectivités territoriales : " Le maire et les adjoints, après avoir été entendus ou invités à fournir des explications écrites sur les faits qui leur sont reprochés, peuvent être suspendus par arrêté ministériel motivé pour une durée qui n'excède pas un mois. Ils ne peuvent être révoqués que par décret motivé pris en conseil des ministres. / (...) / La révocation emporte de plein droit l'inéligibilité aux fonctions de maire et à celles d'adjoint pendant une durée d'un an à compter du décret de révocation à moins qu'il ne soit procédé auparavant au renouvellement général des conseils municipaux ". Ces dispositions ont pour objet de réprimer les manquements graves et répétés aux obligations qui s'attachent aux fonctions de maire et de mettre ainsi fin à des comportements dont la particulière gravité est avérée.<br/>
<br/>
              2. M. B... a été révoqué de ses fonctions de maire de la commune d'Hesdin (Pas-de-Calais) par un décret du 21 août 2019, pris sur le fondement des dispositions citées au point 1, au motif que ses agissements dans le cadre de l'exercice de ses fonctions municipales l'ont privé de l'autorité morale nécessaire à l'exercice des fonctions de maire. Il ressort des termes de ce décret que les faits qu'il vise sont, en premier lieu, l'utilisation par M. B... de moyens de la commune dans le cadre de sa campagne en vue des élections législatives des 11 et 18 juin 2017 et, en deuxième lieu, les refus réitérés de l'intéressé de tenir compte des règles applicables en matière de gestion des deniers publics de la commune. Si le décret vise aussi les " mises en examen dont fait l'objet M. A... B... pour prise illégale d'intérêts et complicité de faux et usage de faux en écriture, pour détournement de fonds publics et pour irrégularités manifestes dans la gestion de la commune assorties de délits de favoritisme dans la passation de marchés publics ", il doit être regardé comme visant les agissements qu'il mentionne, dont, au demeurant, seuls les premiers ont, ainsi que le relève M. B..., donné lieu à sa mise en examen. <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. En premier lieu, le décret attaqué fait mention de la lettre du 5 juillet 2019 par laquelle M. B... a fait part de ses observations sur la mesure de révocation qu'il était envisagé de prendre à son encontre. Il ne ressort ni de la motivation du décret attaqué ni d'aucune des pièces du dossier que l'autorité administrative se serait abstenue de prendre en considération ces observations avant de prononcer la sanction de révocation.<br/>
<br/>
              4. En second lieu, le décret attaqué comporte l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision. Il est, ainsi, suffisamment motivé, sans qu'ait d'incidence à cet égard  la circonstance qu'il ne fait pas état du contenu de la lettre du 5 juillet 2019 par laquelle M. B... a fait part de ses observations sur la mesure de révocation envisagée, ni que le requérant soit fondé à soutenir que la formule selon laquelle les agissements retenus à son encontre, qui sont précisément énoncés, " le privent de l'autorité morale nécessaire à l'exercice de ses fonctions de maire " serait trop vague.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5. En premier lieu, la procédure prévue à l'article L. 2122-16 du code général des collectivités territoriales est indépendante de la procédure pénale. Par suite, l'autorité administrative ne méconnaît pas le principe de la présomption d'innocence en prononçant une sanction sur le fondement de ces dispositions sans attendre que les juridictions répressives aient définitivement statué, y compris dans l'hypothèse où c'est à raison des mêmes faits que sont engagées parallèlement les deux procédures.<br/>
<br/>
              6. En deuxième lieu, la circonstance qu'une des sanctions prévues par les dispositions de l'article L. 2122-16 du code général des collectivités territoriales soit prononcée peu de temps avant le prochain renouvellement général des conseils municipaux n'est pas de nature, eu égard à leur objet, rappelé au point 1, à l'entacher d'illégalité au regard du principe de nécessité des peines.<br/>
<br/>
              7. En troisième lieu, aucun texte ni aucun principe général du droit n'enferment dans un délai déterminé le prononcé de la révocation prévue par les dispositions de l'article L. 2122-16 du code général des collectivités territoriales. Le moyen tiré de ce que le décret attaqué serait entaché d'une erreur de droit au motif qu'il se fonde pour partie sur des faits remontant à plus d'un an ne peut, dès lors, qu'être écarté. <br/>
<br/>
              8. En quatrième lieu, d'une part, il ressort des pièces du dossier que, ainsi que le Conseil constitutionnel l'a relevé dans sa décision n° 2018-5581 AN du 18 mai 2018 visée par le décret attaqué, M. B... a utilisé une manifestation financée par la commune d'Hesdin et destinée à la présentation du bilan à mi-mandat de son action en qualité de maire, qui s'est tenue le 14 avril 2017, pour procéder à l'annonce de sa candidature aux élections législatives. D'autre part, il ressort également des pièces du dossier, notamment du rapport d'observations définitives sur les comptes et la gestion de la commune au titre des exercices 2013 et suivants, délibéré par la chambre régionale des comptes des Hauts-de-France le 30 janvier 2019, auquel le préfet du Pas-de-Calais s'est expressément référé dans sa lettre du 27 juin 2019 invitant M. B... à présenter ses observations sur la mesure de révocation envisagée, que des irrégularités nombreuses et répétées au regard des règles budgétaires et comptables ainsi que des règles de la commande publique ont entaché la gestion de M. B... en sa qualité de maire d'Hesdin. Certaines des irrégularités en cause revêtent un caractère d'une particulière gravité, notamment les irrégularités en matière de commande publique, commises à toutes les étapes de la passation des marchés publics, qui ont porté atteinte, comme l'ont relevé les magistrats de la chambre régionale des comptes, aux principes fondamentaux de liberté d'accès, d'égalité de traitement et de transparence des procédures. M. B..., qui n'apporte au soutien de sa requête aucun élément permettant de douter de l'exactitude de cet ensemble de faits, n'est pas fondé à soutenir que les agissements qui lui sont reprochés ne seraient, sur ces points, ni définis ni avérés. Contrairement à ce qu'il soutient, il ressort des pièces du dossier que de tels agissements ont été de nature à le priver de l'autorité morale nécessaire à l'exercice de ses fonctions de maire.<br/>
<br/>
              9. En cinquième lieu, si M. B..., qui fait valoir qu'aucune des procédures pénales mentionnées au point 2 n'avait donné lieu, à la date du décret attaqué, à un jugement de condamnation, conteste que soient réunis les éléments constitutifs des infractions du chef desquelles elles ont été ouvertes, il résulte de l'instruction que l'autorité administrative aurait pris la même décision si elle ne s'était fondée que sur les faits mentionnés au point 8, qui eu égard à leur nature, à leur gravité et à leur caractère répété et au fait que, ainsi qu'il a été dit, ils ont été de nature à priver l'intéressé de l'autorité morale nécessaire à l'exercice de ses fonctions de maire sont, à eux seuls, de nature à justifier la mesure de révocation prise. Par suite, le décret attaqué n'a pas fait une inexacte application des dispositions de l'article L. 2122-16 du code général des collectivités territoriales en prononçant la révocation de M. B....<br/>
<br/>
              10. En dernier lieu, M. B... soutient que le décret attaqué méconnaît les droits de la défense, les dispositions de l'article 3 de la Constitution, le droit à un procès équitable protégé par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ainsi que les stipulations de l'article 3 du premier protocole additionnel à cette convention. Toutefois, il ne présente, à l'appui de ces moyens, aucune argumentation distincte de celle qui a déjà été écartée aux points 3 à 9 de la présente décision. Dès lors, ces moyens ne peuvent eux-mêmes qu'être écartés.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la requête de M. B... doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au Premier ministre, au ministre de l'intérieur et au ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
