<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253540</ID>
<ANCIEN_ID>JG_L_2017_12_000000411906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/35/CETATEXT000036253540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 20/12/2017, 411906, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:411906.20171220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a saisi le tribunal administratif de Nantes d'une demande tendant, en premier lieu, à l'annulation du titre de pension n° B 14 020325 U du 14 avril 2014 en tant qu'il retient un nombre de 156 trimestres à prendre en compte pour bénéficier d'une retraite à taux plein et non 150 trimestres, en deuxième lieu, à l'annulation de la décision du ministre de l'intérieur du 2 juin 2014 rejetant son recours administratif, en troisième lieu, à l'annulation de la décision du ministre des finances et des comptes publics du 4 juin 2014 rejetant son recours administratif, enfin, à ce qu'il soit enjoint à l'Etat de régulariser sa situation administrative. Par un jugement n° 1406693 du 3 mai 2017, le tribunal administratif de Nantes a, d'une part, annulé le titre de pension en litige, ainsi que les décisions ministérielles attaquées, et, d'autre part, enjoint au ministre des finances et des comptes publics et au ministre de l'intérieur, chacun pour ce qui le concerne, de régulariser la situation de M. A...en lui concédant une retraite à taux plein dans un délai de deux mois à compter de la notification du jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 27 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2003-775 du 21 août 2003 ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M.A....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...est un ancien brigadier-chef de la police nationale, né le 22 novembre 1953 ; qu'il a été admis à faire valoir ses droits à la retraite le 1er mai 2014 ; qu'une pension de retraite lui a été concédée par un titre du 14 avril 2014 ; que M. A...a constaté que celui-ci précisait qu'une durée de 156 trimestres était nécessaire pour pouvoir bénéficier d'un taux de retraite à 75% et qu'il ne bénéficiait, sur la base des 150 trimestres réalisés, que d'un taux de retraite de 72,115 % ; que les recours administratifs de l'intéressé ont été rejetés respectivement par le ministre de l'intérieur et le ministre des finances et des comptes publics les 2 et 4 juin 2014 ; que, par un jugement du 3 mai 2017, le tribunal administratif de Nantes a, d'une part, annulé le titre de pension en litige, ainsi que les décisions ministérielles attaquées, et, d'autre part, enjoint au ministre des finances et des comptes publics et au ministre de l'intérieur, chacun pour ce qui le concerne, de régulariser la situation de M. A...en lui concédant une retraite à taux plein dans un délai de deux mois à compter de la notification du jugement ;<br/>
<br/>
              2. Considérant qu'aux termes du VI de l'article 5 de la loi du 21 août 2003 portant réforme des retraites, dans sa rédaction initiale : " La durée des services et bonifications exigée des fonctionnaires de l'Etat et des militaires pour obtenir le pourcentage maximum d'une pension civile ou militaire de retraite est celle qui est en vigueur lorsqu'ils atteignent l'âge auquel ou l'année au cours de laquelle ils remplissent les conditions de liquidation d'une pension en application des articles L. 24 et L. 25 du code des pensions civiles et militaires de retraite dans leur rédaction issue de la présente loi. " ; qu'aux termes des mêmes dispositions, dans leur version issue de la loi du 9 novembre 2010 portant réforme des retraites : " La durée des services et bonifications exigée des fonctionnaires de l'Etat et des militaires pour obtenir le pourcentage maximum d'une pension civile ou militaire de retraite est celle qui est en vigueur lorsqu'ils atteignent l'âge mentionné au troisième alinéa du I./ Par dérogation au premier alinéa du présent VI, la durée des services et bonifications exigée des fonctionnaires de l'Etat et des militaires qui remplissent les conditions de liquidation d'une pension avant l'âge mentionné au troisième alinéa du I est celle exigée des fonctionnaires atteignant l'âge mentionné au même troisième alinéa l'année à compter de laquelle la liquidation peut intervenir. " ; que les modalités de liquidation d'une pension sont appréciées à la date de l'admission à la retraite et sur la base de la législation en vigueur à cette date ; qu'ainsi, en se fondant sur la rédaction initiale des dispositions du VI de l'article 5 de la loi du 21 août 2003 et non sur leur rédaction issue de la loi précitée du 9 novembre 2010, alors que M. A...a été autorisé à faire valoir ses droits à la retraite à compter du 1er mai 2014 par décision du 14 avril 2014, le tribunal administratif a commis une erreur de droit ; que, par suite, son jugement doit être annulé ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 3 mai 2017 du tribunal administratif de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Nantes.<br/>
Article 3 : Les conclusions présentées par M. A...devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
