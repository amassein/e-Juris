<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074275</ID>
<ANCIEN_ID>JG_L_2021_01_000000433429</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074275.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 26/01/2021, 433429</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433429</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433429.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Melun, d'une part, d'annuler la décision du 8 janvier 2015 prise par le ministre des finances et des comptes publics et le ministre de l'économie, de l'industrie et du numérique, en tant qu'elle lui refuse un recul de la limite d'âge de départ à la retraite au-delà d'un an, d'autre part, d'annuler la décision du 10 juin 2015 par laquelle les mêmes ministres ont rejeté son recours gracieux formé, dans la même mesure, contre cette décision. Par un jugement n° 1504660-1506418 du 27 juin 2017, le tribunal administratif de Melun a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 17PA02944 du 26 juin 2019, la cour administrative d'appel de Paris a, sur appel de M. A..., annulé ce jugement ainsi que la décision du 8 janvier 2015 et celle du 10 juin 2015 rejetant son recours gracieux contre cette décision.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - la loi du 18 août 1936 concernant les mises à la retraite par ancienneté ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 4 de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté : " Les limites d'âge sont reculées d'une année par enfant à charge, sans que la prolongation d'activité puisse être supérieure à trois ans. Les enfants pris en compte pour l'application de la présente disposition sont ceux qui sont définis par les lois et règlements régissant l'attribution des prestations familiales, ainsi que ceux qui ouvrent droit au versement de l'allocation aux adultes handicapés ". Il résulte de ces dispositions que les enfants qu'elles mentionnent sont ceux qui sont susceptibles d'être pris en compte pour l'attribution de l'une quelconque des prestations familiales.<br/>
<br/>
              2. Aux termes de l'article L. 511-1 du code de la sécurité sociale : " Les prestations familiales comprennent : / (...) 2°) les allocations familiales ; / 3°) le complément familial ; / 4°) l'allocation de logement (...) ". Selon l'article L. 512-3 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Sous réserve des règles particulières à chaque prestation, ouvre droit aux prestations familiales : / 1°) tout enfant jusqu'à la fin de l'obligation scolaire ; / 2°) après la fin de l'obligation scolaire, et jusqu'à un âge limite, tout enfant dont la rémunération éventuelle n'excède pas un plafond. / Toutefois, pour l'attribution du complément familial et de l'allocation de logement mentionnés aux 3° et 4° de l'article L. 511-1 et à l'article L. 755-21, l'âge limite peut être différent de celui mentionné au 2° du présent article ". Cet âge limite est fixé à vingt ans par les dispositions réglementaires codifiées au premier alinéa de l'article R. 512-2 du code de la sécurité sociale. Toutefois, pour l'attribution du complément familial et de l'allocation de logement, sont susceptibles d'être pris en compte, en vertu, respectivement, de l'article R. 522-1 du code de la sécurité sociale et de son article D. 542-4, dans sa rédaction applicable au litige, les enfants âgés de moins de vingt et un ans.<br/>
<br/>
              3. Par suite, en jugeant que, pour l'application des dispositions de l'article 4 de la loi du 18 août 1936 relatives au droit au recul de la limite d'âge de départ en retraite applicable aux fonctionnaires, un enfant âgé de plus de vingt ans mais de moins de vingt et un ans peut être regardé comme un enfant à charge, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que le ministre de l'économie et des finances et le ministre de l'action et des comptes publics ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances et du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à M. B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-01 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. MISE À LA RETRAITE POUR ANCIENNETÉ ; LIMITES D'ÂGE. - ENFANT À CHARGE - NOTION - ENFANT DE MOINS DE VINGT-ET-UN ANS [RJ1].
</SCT>
<ANA ID="9A"> 36-10-01 Il résulte du premier alinéa de l'article 4 de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté que les enfants qu'il mentionne sont ceux qui sont susceptibles d'être pris en compte pour l'attribution de l'une quelconque des prestations familiales.,,,Pour l'attribution du complément familial et de l'allocation de logement, qui font partie des prestations familiales mentionnées à l'article L. 511-1 du code de la sécurité sociale (CSS), sont susceptibles d'être pris en compte, en vertu, respectivement, de l'article R. 522-1 du CSS et de son article D. 542-4, dans sa rédaction applicable au litige, les enfants âgés de moins de vingt et un ans.,,,Par suite, pour l'application des dispositions de l'article 4 de la loi du 18 août 1936 relatives au droit au recul de la limite d'âge de départ en retraite applicable aux fonctionnaires, un enfant âgé de moins de vingt et un ans peut être regardé comme un enfant à charge.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour l'application des dispositions relatives au supplément familial de traitement, CE, décision du même jour, M.,, n° 433426, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
