<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038366653</ID>
<ANCIEN_ID>JG_L_2019_03_000000428122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/36/66/CETATEXT000038366653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/03/2019, 428122, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:428122.20190315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 18 février 2019, MM. P...H..., B...T...I..., O...F..., A...G..., N...J..., S..., K...E...,<br/>
M. L...D..., Mme M...Q...et M. B...C...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'ordonnance n° 2019-76 du 6 février 2019 portant diverses mesures relatives à l'entrée, au séjour, aux droits sociaux et à l'activité professionnelle, applicables en cas d'absence d'accord sur le retrait du Royaume-Uni de l'Union européenne ;<br/>
<br/>
              2°) de saisir la cour de justice de l'Union européenne de trois questions préjudicielles relatives, pour la première, aux articles 20 et 21 du traité sur le fonctionnement de l'Union européenne, à l'article 50 du traité sur l'Union européenne et à la directive n° 2004/38 du parlement européen et du conseil du 29 avril 2004 relative au droit des citoyens de l'Union et des membres de leurs familles de circuler et de séjourner librement sur le territoire des Etats membres, pour la deuxième, au paragraphe 3 du préambule du traité sur l'Union européenne, aux articles 2, 9 et 21 du traité sur l'Union européenne, à l'article 21 du traité sur le fonctionnement de l'Union européenne, au paragraphe 2 du préambule et aux articles 7, 20, 21 et 41 de la charte des droits fondamentaux de l'Union européenne, à la recommandation 2014/53/UE et à la communication de la commission COM(2014) 33 final, la troisième aux articles 20 et 21 du traité sur le fonctionnement de l'Union européenne, à la directive n° 2004/38 du parlement européen et du conseil du 29 avril 2004 relative au droit des citoyens de l'Union et des membres de leurs familles de circuler et de séjourner librement sur le territoire des Etats membres, à l'article 50 du traité sur l'Union européenne et à la directive n° 2003/109/CE du conseil du 25 novembre 2003 relative au statut des ressortissants des pays tiers résidents de longue durée ;<br/>
<br/>
<br/>
<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - l'ordonnance n° 2019-76 du 6 février 2019 a valeur réglementaire, n'ayant pas encore été ratifiée ;<br/>
              - l'urgence se déduit de l'obligation, pour les citoyens britanniques ayant acquis après cinq ans de résidence un droit permanent au séjour en France, de demander un titre de séjour dans un délai compris entre trois mois et un an après la sortie du Royaume-Uni de l'Union européenne, en méconnaissance de la directive n° 2004/38 du parlement européen et du conseil du 29 avril 2004 relative au droit des citoyens de l'Union et des membres de leurs familles de circuler et de séjourner librement sur le territoire des Etats membres ;<br/>
              - l'urgence est également constituée du fait de l'impossibilité pour les ressortissants britanniques de voter aux élections européennes du 26 mai 2019 et aux élections municipales anticipées de Bordeaux ;<br/>
              - la condition de réciprocité figurant à l'article 19 de l'ordonnance relève de la compétence de l'Union européenne, ou à tout le moins, ne peut résulter que d'une loi et non d'une ordonnance, de sorte qu'un doute sérieux existe quant à la légalité de l'ordonnance ;<br/>
              - il existe un doute sérieux quant à la légalité de l'ordonnance au regard de la directive n° 2004/38/CE du 29 avril 2004, en ce qu'elle ne règle pas la situation des citoyens britanniques ayant acquis un droit permanent au séjour ;<br/>
              - en conditionnant le droit de séjour des Britanniques à la réciprocité, l'ordonnance offre un niveau insuffisant de sécurité juridique au regard des exigences de la directive n° 2003/109/CE du 25 novembre 2003, s'analysant en doute sérieux quant à sa légalité ;<br/>
              - l'ingérence de l'ordonnance dans la vie privée et familiale des ressortissants britanniques, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'Homme, est insuffisamment prévisible, ce qui constitue un doute sérieux quant à sa légalité ;<br/>
              - un doute sérieux quant à la légalité de l'ordonnance résulte d'une discrimination non justifiée à l'égard des citoyens britanniques ;<br/>
              - le moyen tiré de la méconnaissance du droit des personnes âgées à mener une vie digne et indépendante, et à participer à la vie sociale et culturelle crée un doute sérieux quant à la légalité de l'ordonnance ;<br/>
              - la privation, par l'ordonnance, de la liberté de circulation et de séjourner, la méconnaissance du principe de bonne administration ainsi que de l'égalité devant le vote, est de nature à créer un doute sérieux ;<br/>
              - l'ordonnance méconnaît les règles relatives à la citoyenneté européenne, en privant les ressortissants britanniques de son bénéfice, alors que le droit de l'Union européenne ne l'impose pas, cette circonstance étant de nature à créer un doute sérieux quant à la légalité de l'ordonnance.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes de l'article 1 de l'ordonnance n° 2019-76 du 6 février 2019 : " Pour une période d'au moins trois mois à compter de la date du retrait du Royaume-Uni de l'Union européenne, sur le fondement de l'article 50 du traité sur le fonctionnement de l'Union européenne (TFUE) et jusqu'à une date fixée par décret et qui ne peut être postérieure à un an après cette date, le ressortissant britannique qui, à la date de ce retrait, résidait régulièrement en France dans les conditions prévues par les articles L. 121-1 et L. 122-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et continue à y résider, n'est pas tenu de détenir un titre de séjour. Pendant cette période, il conserve son droit de séjour, y compris au titre du droit d'exercice d'une activité professionnelle ainsi que les droits sociaux qui en résultent et dont il bénéficie à la date du retrait du Royaume-Uni de l'Union européenne. Le décret fixe également la date avant laquelle les demandes de titres de séjour des ressortissants britanniques qui résidaient régulièrement en France à la date de retrait du Royaume-Uni de l'Union européenne doivent être déposées dans les conditions fixées par les articles 2 et 3 ".<br/>
<br/>
              3. Les requérants demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'ordonnance n° 2019-76 du 6 février 2019.<br/>
<br/>
              4. Les requérants soutiennent, en premier lieu, que l'urgence se déduirait du risque pour eux d'être privés de leur droit de séjour avant le jugement du recours en excès de pouvoir contre l'ordonnance n° 2019-76 du 6 février 2019. Toutefois, dans l'hypothèse d'une sortie du Royaume-Uni de l'Union européenne, les ressortissants britanniques seront dispensés, pour une période de trois mois au moins et pouvant durer un an, de détenir un titre de séjour. Par suite, la condition d'urgence requise par les dispositions de l'article L. 521-1 du code de justice administrative n'est pas remplie. En second lieu, ils soutiennent que l'urgence résulterait de l'impossibilité pour les citoyens britanniques de voter aux élections européennes du 26 mai 2019 ainsi qu'aux élections municipales anticipées devant se tenir à Bordeaux. L'ordonnance<br/>
n° 2017-76 du 6 février 2019 ne réglemente toutefois pas les conditions d'éligibilité ou de vote des citoyens britanniques aux élections municipales ou européennes. La proximité de ces échéances n'est donc pas de nature à caractériser la situation d'urgence au sens de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui précède que la requête doit être rejetée, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de MM.H..., I..., F..., G..., J..., R..., E..., M.D..., Mme Q...et M. C...est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à MM. P...H..., B...T...I..., O...F..., A...G..., N...J..., S..., K...E..., M. L... D..., Mme M... Q...et M. B... C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
