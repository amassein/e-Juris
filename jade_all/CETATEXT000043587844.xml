<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587844</ID>
<ANCIEN_ID>JG_L_2021_06_000000438619</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587844.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/06/2021, 438619, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438619</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438619.20210601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Le président de l'université de Bordeaux a engagé contre M. D... C... des poursuites disciplinaires devant la section disciplinaire du conseil académique de l'université. Par une décision du 18 juillet 2019, la section disciplinaire a infligé à M. C... la sanction de la révocation. <br/>
<br/>
              Par une décision du 27 novembre 2019, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, a, sur la requête de M. C..., ordonné qu'il soit sursis à l'exécution de cette sanction.  <br/>
<br/>
              1° Sous le n° 438619, par un pourvoi et un mémoire en réplique, enregistrés le 13 février 2020 et le 15 février 2021 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'enseignement supérieur, de la recherche et de l'innovation demande au Conseil d'Etat d'annuler cette décision. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 438719, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 février et 2 mars 2020 au secrétariat du contentieux du Conseil d'Etat, l'université de Bordeaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la même décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M. C... aux fins de sursis à exécution ; <br/>
<br/>
              3°) de mettre à la charge de M. C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Sevaux, Mathonnet, avocat de M. C... et à la SCP Boulloche, avocat de l'université de Bordeaux ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 mai 2021, présentée par M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le président de l'université de Bordeaux a engagé le 11 avril 2019 des poursuites disciplinaires contre M. C..., maître de conférences, en raison notamment de ses agissements à l'égard d'une collègue, avec laquelle il avait eu antérieurement une relation sentimentale. Par une décision du 18 juillet 2019, la section disciplinaire de l'université a estimé que M. C... s'était rendu coupable de faits de harcèlement moral à l'égard de celle-ci, que son comportement avait compromis le bon fonctionnement du service, qu'il avait manqué à ses obligations de service et porté atteinte à l'image de l'université, et lui a infligé la sanction de la révocation. Par une décision du 27 novembre 2019, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, a, sur la requête de M. C..., prononcé le sursis à exécution de cette décision. La ministre de l'enseignement supérieur, de la recherche et de l'innovation d'une part, l'université de Bordeaux d'autre part, se pourvoient en cassation contre cette décision. <br/>
<br/>
              2. Le pourvoi de la ministre de l'enseignement supérieur, de la recherche et de l'innovation et celui de l'université de Bordeaux sont dirigés contre la même décision. Il y a lieu de les joindre pour statuer par une même décision.<br/>
<br/>
              3. Aux termes de l'article R. 232-34 du code de l'éducation : " La demande de sursis à exécution est, à peine d'irrecevabilité, présentée par requête distincte jointe à l'appel. / (...) Le sursis peut être prononcé si les moyens présentés dans la requête paraissent sérieux et de nature à justifier l'annulation ou la réformation de la décision attaquée (...) ".<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier de la procédure suivie devant le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, que, par une requête en date du 22 septembre 2019, M. C..., qui n'avait pas d'avocat, a indiqué interjeter appel de la décision de sanction lui ayant été infligée le 18 juillet 2019 et que, par une nouvelle requête, précisant qu'elle se substituait à la précédente, en date du 23 septembre 2019, l'expiration du délai de recours contentieux intervenant le 30 septembre suivant, a de nouveau mentionné qu'il interjetait appel de cette décision. Si cette requête, comme au demeurant la précédente, ne contenait aucun moyen, elle comportait, en annexe, une demande aux fins de sursis à exécution de la même décision, qui exposait les moyens par lesquels l'intéressé contestait la sanction dont il avait été frappé. En estimant, dans ces conditions, que la requête d'appel de M. C... devait, contrairement à ce que soutenait l'université de Bordeaux, être regardée comme motivée, et qu'elle était, par suite, recevable, le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, n'a pas entaché sa décision, qui est suffisamment motivée, d'erreur de droit. <br/>
<br/>
              5. En second lieu, à l'appui de sa demande de sursis à exécution, M. C... invoquait plusieurs irrégularités procédurales et soutenait que de nombreuses pièces qu'il avait fournies n'avaient pas été prises en compte par la juridiction de première instance dont l'appréciation était, par suite, erronée. En relevant, pour faire droit à la demande tendant au sursis à exécution de la décision litigieuse, que M. C... soutenait que " la section disciplinaire de première instance n'a pas tenu compte de la production de témoignages en sa faveur " et " que les explications du déféré ont convaincu les juges d'appel et que dès lors, il existe un moyen sérieux de nature à justifier l'annulation ou la réformation de la décision de première instance ", le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire, a, d'une part, contrairement à ce qui est soutenu, désigné le moyen de la requête qui lui paraissait sérieux et mis à même le juge de cassation d'exercer son contrôle, d'autre part, s'est livré à une appréciation souveraine exempte de dénaturation. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la ministre de l'enseignement supérieur, de la recherche et de l'innovation et l'université de Bordeaux ne sont pas fondées à demander l'annulation de la décision qu'elles attaquent. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de l'université de Bordeaux une somme de 1 500 euros chacun à verser à M. C... au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par l'université de Bordeaux à l'encontre de M. C... qui, dans la présente instance, n'est pas la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de l'enseignement supérieur, de la recherche et de l'innovation et le pourvoi de l'université de Bordeaux sont rejetés.<br/>
Article 2 : L'Etat et l'université de Bordeaux verseront chacun une somme de 1 500 euros à M. C... au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, à l'université de Bordeaux et à M. D... C.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
