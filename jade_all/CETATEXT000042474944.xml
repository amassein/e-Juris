<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042474944</ID>
<ANCIEN_ID>JG_L_2020_10_000000445273</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/49/CETATEXT000042474944.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 20/10/2020, 445273, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445273</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445273.20201020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête et un nouveau mémoire, enregistrés les 12 et 16 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat Action et Démocratie - CFE-CGC - Syndicat national de l'enseignement demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-4 du code de justice administrative : <br/>
<br/>
              1°) de suspendre l'exécution de la circulaire du 14 septembre 2020 du ministre de l'éducation nationale, de la jeunesse et des sports relative à la gestion des personnels et aux modalités d'application au sein du ministère de l'éducation nationale, de la jeunesse et des sports des dispositions prises pour la fonction publique en raison de l'évolution de l'épidémie de covid-19 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il a recueilli des éléments nouveaux, au sens de l'article L. 521-4 du code de justice administrative, depuis le rejet de ses conclusions présentées au titre de l'article L. 521-1 du code de justice administrative par une ordonnance n° 445054 du 7 octobre 2020 du juge des référés du Conseil d'Etat ;<br/>
              - de très nombreux enseignants présentent une contre-indication médicale au port du masque sans pouvoir se voir prescrire un arrêt maladie pour ce motif ;<br/>
              - il existe un doute sérieux quant à la légalité de la circulaire contestée ; <br/>
              - la circulaire contestée est illégale en ce qu'elle prévoit que, dans le cas d'une contre-indication médicale au port du masque, les personnes ne pouvant exercer en télétravail doivent produire un arrêt de travail délivré par un médecin afin d'être placées en congé ordinaire de maladie au sens de l'article 34 de la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la circulaire contestée aurait dû prévoir la mise en oeuvre de l'article 1er du décret n° 84-1051 du 30 novembre 1984, qui permet à l'administration d'affecter un fonctionnaire qui n'est plus en mesure d'exercer ses fonctions mais ne bénéficie pas d'un congé de maladie dans un emploi de son grade dans lequel les conditions de service sont de nature à permettre à l'intéressé d'assurer les fonctions correspondantes ;<br/>
              - elle aurait dû prévoir un aménagement du poste de travail adapté à une contre-indication au port du masque en application de l'article 63 de la loi du 11 janvier 1984.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 2020-473 du 25 avril 2020 ;<br/>
              - la loi n° 2020-856 du 9 juillet 2020 ;<br/>
              - le décret n° 84-1051 du 30 novembre 1984 ;<br/>
              - le décret n° 2020-860 du 10 juillet 2020 ; <br/>
              - le décret n° 2020-1096 du 28 août 2020 ;<br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 521-4 de ce code prévoit que : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Sur le fondement de l'article 1er de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire, le décret du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé a, dans sa rédaction issue du décret du 28 août 2020 ci-dessus visé, s'agissant des établissements et activités, à son article 27, prévu que toute personne de onze ans ou plus porte un masque de protection dans les établissements recevant du public dont il a dressé la liste, dont les administrations, et, à son article 36, défini les conditions d'accueil des usagers dans les établissements d'enseignement, parmi lesquelles figure l'obligation de port du masque par les personnes qu'il a mentionnées, en particulier par le personnel de ces structures. Ces dispositions ont été reprises aux articles 27 et 36 du décret du 16 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, qui a abrogé le décret du 10 juillet 2020.<br/>
<br/>
              3. Le syndicat Action et Démocratie - CFE-CGC - Syndicat national de l'enseignement demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-4 du code de justice administrative, de suspendre l'exécution de la circulaire du 14 septembre 2020 par laquelle le ministre de l'éducation nationale, de la jeunesse et des sports a précisé les modalités d'application au sein de ce ministère des dispositions prises pour la fonction publique de l'Etat en raison de l'évolution de l'épidémie de covid-19. En premier lieu, eu égard aux moyens qu'il soulève, le syndicat requérant doit être regardé comme en demandant la suspension en tant que cette circulaire prévoit que, dans le cas d'une contre-indication médicale au port du masque, les personnes ne pouvant exercer en télétravail doivent produire un arrêt de travail délivré par un médecin afin d'être placées en congé de maladie ordinaire. En deuxième lieu, appelé à statuer sur le fondement de l'article L. 521-1 du code de justice administrative sur une requête du même syndicat tendant à ce que soit ordonnée la suspension de l'exécution de la même circulaire, le juge des référés du Conseil d'Etat en a prononcé le rejet par une ordonnance du 7 octobre 2020. Si l'article L. 521-4 du code de justice administrative permet au juge des référés, saisi par toute personne intéressée, de modifier les mesures qu'il avait ordonnées ou d'y mettre fin, au vu d'un élément nouveau, ces dispositions ne sauraient être utilement invoquées lorsque le juge des référés a rejeté purement et simplement une requête aux fins de suspension d'une décision administrative dont il était saisi. La présente requête doit ainsi s'analyser comme une nouvelle demande par le même requérant de suspension de la même circulaire sur le fondement de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              3. Le syndicat requérant soutient que :<br/>
              - la circulaire contestée est illégale en ce qu'elle prévoit que, dans le cas d'une contre-indication médicale au port du masque, les personnes ne pouvant exercer en télétravail doivent produire un arrêt de travail délivré par un médecin afin d'être placées en congé ordinaire de maladie au sens de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat ;<br/>
              - la circulaire contestée aurait dû prévoir la mise en oeuvre de l'article 1er du décret n° 84-1051 du 30 novembre 1984 pris en application de l'article 63 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat en vue de faciliter le reclassement des fonctionnaires de l'Etat reconnus inaptes à l'exercice de leurs fonctions, qui permet à l'administration d'affecter un fonctionnaire qui n'est plus en mesure d'exercer ses fonctions mais ne bénéficie pas d'un congé de maladie dans un emploi de son grade dans lequel les conditions de service sont de nature à permettre à l'intéressé d'assurer les fonctions correspondantes ;<br/>
              - elle aurait dû prévoir un aménagement du poste de travail adapté à une contre-indication du port du masque en application de l'article 63 de la loi du 11 janvier 1984.<br/>
<br/>
              4. Il est manifeste qu'aucun de ces moyens n'est, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la circulaire du 14 septembre 2020. En particulier, le syndicat requérant ne soutient pas sérieusement et n'établit aucunement, par la seule production des certificats médicaux délivrés à un enseignant, qu'une contre-indication médicale au port du masque ne serait pas de nature à permettre à un agent ne pouvant exercer en télétravail, dont la situation n'est, contrairement à ce qu'il soutient, en rien assimilable à celle des salariés dits " vulnérables " dont le I de l'article 20 de la loi du 25 avril 2020 de finances rectificative pour 2020 a permis le placement en position d'activité partielle, de se voir délivrer un arrêt de travail au titre du 2° de l'article 34 de la loi du 11 janvier 1984 en vertu duquel " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie (...) en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions (...) ". Par suite, sans qu'il soit besoin de se prononcer sur l'urgence, il y a lieu de rejeter la requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du syndicat Action et Démocratie - CFE-CGC - Syndicat national de l'enseignement est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat Action et Démocratie - CFE-CGC - Syndicat national de l'enseignement.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
