<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704088</ID>
<ANCIEN_ID>JG_L_2019_06_000000420417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/40/CETATEXT000038704088.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 28/06/2019, 420417, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420417.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Prima a demandé à la cour administrative d'appel de Marseille d'annuler pour excès de pouvoir l'arrêté du 25 mai 2016 par lequel le maire de la commune de Peyriac-Minervois (Aude) a accordé à la société K Peyriac Invest un permis de construire valant autorisation d'exploitation commerciale. Par un arrêt n° 16MA02991 du 5 mars 2018, la cour administrative d'appel a rejeté sa requête.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 mai et 7 août 2018 au secrétariat du contentieux du Conseil d'Etat, la société Prima demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge de la société K Peyriac Invest, de la commune de Peyriac-Minervois et de l'Etat la somme de 2 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Prima et à la SCP Piwnica, Molinié, avocat de la société K Peyriac Invest.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société K Peyriac Invest a déposé une demande de permis de construire valant autorisation d'exploitation commerciale auprès du maire de la commune de Peyriac-Minervois le 31 juillet 2015 en vue de la création d'un ensemble commercial d'une surface de vente totale de 2 294 m². Saisie dans le cadre de l'instruction de cette demande par la commune de Peyricac-Minervois, la commission départementale d'aménagement commercial de l'Aude a, par un acte du 21 octobre 2015, " accordé " une autorisation d'exploitation commerciale à la société K Peyriac Invest. Sur recours de la société Prima, la Commission nationale d'aménagement commercial a émis un avis favorable au projet, le 6 avril 2016. Au vu de cet avis, un permis de construire valant autorisation d'exploitation commerciale a été délivré à la société K Peyriac Invest par le maire de la commune de Peyriac-Minervois, le 25 mai 2016. La société Prima se pourvoit en cassation contre l'arrêt du 5 mars 2018 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation de ce permis, en tant qu'il vaut autorisation d'exploitation commerciale.<br/>
<br/>
              2. L'article L. 425-4 du code de l'urbanisme dispose que : " lorsque le projet est soumis à autorisation d'exploitation commerciale au sens de l'article L. 752-1 du code de commerce, le permis de construire tient lieu d'autorisation dès lors que la demande de permis a fait l'objet d'un avis favorable de la commission départementale d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial (...) / A peine d'irrecevabilité, la saisine de la commission nationale par les personnes mentionnées à l'article L. 752-17 du même code est un préalable obligatoire au recours contentieux dirigé contre la décision de l'autorité administrative compétente pour délivrer le permis de construire ". Il résulte des termes mêmes de ces dispositions que le recours formé devant la Commission nationale d'aménagement commercial contre un avis d'une commission départementale d'aménagement commercial constitue un recours préalable obligatoire. En conséquence, il appartient à la Commission nationale de se prononcer sur le projet d'aménagement commercial qui lui est soumis en fonction des circonstances de droit et de fait à la date de son avis.<br/>
<br/>
              3. Il s'ensuit que, sans que puissent y faire obstacle les dispositions, d'effet transitoire, de l'article 6 du décret du 24 novembre 2008 relatif à l'aménagement commercial, la cour administrative d'appel a commis une erreur de droit en jugeant que l'avis de la Commission nationale d'aménagement commercial devait se fonder sur les dispositions législatives et réglementaires en vigueur à la date à laquelle la commission départementale s'était prononcée. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Prima est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Prima, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre la société K Peyriac Invest. Par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société Prima.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 5 mars 2018 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Le surplus des conclusions de la société Prima et les conclusions de la société K Peyriac Invest, présentés au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la société Prima, à la société K Peyriac Invest et à la commune de Peyriac-Minervois.<br/>
Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
