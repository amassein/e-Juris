<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020220329</ID>
<ANCIEN_ID>JG_L_2009_02_000000297823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/22/03/CETATEXT000020220329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 06/02/2009, 297823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>297823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Daël</PRESIDENT>
<AVOCATS>JACOUPY</AVOCATS>
<RAPPORTEUR>M. Francis  Girault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 septembre 2006 et 29 décembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A, demeurant ..., M. Stéphane A, demeurant ..., Mlle Caroline A, demeurant ... ; Mme A et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 25 juillet 2006 par lequel la cour administrative d'appel de Bordeaux a rejeté la demande présentée par M. Christian A devant le tribunal administratif de Cayenne, demande tendant à l'annulation de la décision implicite du ministre de la défense rejetant sa demande d'assimilation au grade de commandant et de versement des rappels de traitement et autres indemnités s'y rattachant ; <br/>
<br/>
              2°) réglant l'affaire au fond, de condamner l'Etat à verser aux requérants le rappel des traitements et indemnités dus au titre de l'assimilation de M. Christian A au grade de commandant et le rappel des sommes dues au titre de l'indemnité pour charges militaires, le tout avec intérêts et capitalisation des intérêts ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la défense ;<br/>
<br/>
              Vu la loi du 13 juillet 1927 ;<br/>
<br/>
              Vu le décret n° 47-1142 du 23 juin 1947 modifié ;<br/>
<br/>
              Vu le décret n° 53-155 du 23 février 1953 ;<br/>
<br/>
              Vu l'arrêté du 25 mars 1953 portant organisation de la poste aux armées ;<br/>
<br/>
              Vu l'arrêté du 12 septembre 1973 portant création du service de la poste aux armées ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Francis Girault, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Jacoupy, avocat de Mme A et autres, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que M. A,  inspecteur principal de la Poste, détaché au service de la poste des armées de 1980 à 1997 avec, à compter du 1er janvier 1989, le grade de capitaine, s'est vu refuser l'assimilation au grade de commandant pour la période 1995-1997 ; que le tribunal administratif de Cayenne a rejeté sa demande tendant à l'annulation de ce refus d'assimilation et à la condamnation de l'Etat à lui verser les rappels de traitement et indemnités afférents au grade de commandant ; que, par arrêt du 25 juillet 2006, la cour administrative d'appel de Bordeaux, après avoir annulé le jugement du tribunal administratif pour irrégularité, a rejeté cette demande ; que les consorts A, qui ne contestent pas l'annulation du jugement pour irrégularité, se pourvoient en cassation contre cet arrêt en tant qu'après l'avoir évoquée, il a à nouveau rejeté leur demande ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article 40 de la loi du 13 juillet 1927 sur l'organisation générale de l'armée : «Des corps spéciaux peuvent être formés avec les personnels ..., soit en raison de leur profession, soit comme appartenant à des services régulièrement organisés en temps de paix. La formation de ces corps est organisée par décret dès le temps de paix ou seulement au moment du besoin ...»; qu'il résulte de cet article, qui est inséré au chapitre II « Composition de l'armée du temps de guerre » du titre III « Organisation du temps de guerre » de la loi, que les corps spéciaux, s'ils peuvent être organisés en temps de paix, ne sont effectivement dotés en personnel que dans le cas de mobilisation générale ou partielle ; qu'il s'ensuit que le décret du 25 février 1953, portant règlement d'administration publique pour l'organisation du service de la poste aux armées, pris sur le fondement des dispositions de cet article, ne trouvait à  s'appliquer qu'en temps de mobilisation générale ou partielle, de même que l'arrêté du 25 mars 1953, pris pour l'application de ce décret, qui déterminait les règles d'assimilation des grades de la Poste aux grades de la hiérarchie militaire ; que, par suite, la cour a commis une erreur de droit en jugeant que M. A relevait des dispositions de cet arrêté ; que les requérants sont fondés à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté leur demande ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans la limite de cette annulation, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que la situation des agents de la Poste détachés en temps de paix dans la poste aux armées était régie, pour la période où M. A se trouvait dans cette position, par le décret du 23 juin 1947 relatif à la situation du personnel de l'administration des postes, télégraphes et téléphones détaché dans le service de la poste aux armées en dehors du cas de mobilisation générale ou partielle ; que ni ce décret, ni aucune autre disposition réglementaire ne prévoyait d'assimilation des grades de la Poste à des grades de la hiérarchie militaire ; que par suite M. A n'avait aucun droit à être assimilé au grade de commandant ;<br/>
<br/>
              Considérant que le décret du 23 juin 1947 prévoyait, à son article 3, que les agents détachés dans la poste aux armées percevaient une rémunération composée, d'une part, d'une solde égale à leur traitement de base à la Poste, d'autre part, d'une indemnité mensuelle de service fonction du grade détenu dans le service de la poste aux armées, enfin des « indemnités et prestations allouées aux militaires de l'armée active dans les conditions où elles sont accordées à ceux de ces militaires auxquels ils sont assimilés et avec lesquels ils sont en service » ; qui si les requérants demandent que soit reconnu à M. A, sur le fondement de cet article, un droit aux indemnités allouées aux commandants, ce dernier ne tenait, ainsi qu'il vient d'être dit, aucun droit à être assimilé à ce grade ; que cette demande ne peut par suite qu'être rejetée ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à demander l'annulation de la décision implicite du ministre de la défense rejetant la demande d'assimilation de M. A au grade de commandant et du rappel pécuniaire qui s'y rattache ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761 1 du code de justice administrative font obstacle à ce que l'Etat qui n'est pas, dans la présente instance, la partie perdante, soit condamné à payer aux consorts A la somme qu'ils demandent au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux en date du 25 juillet 2006 est annulé en tant qu'il a rejeté la demande d'annulation de la décision implicite du ministre de la défense rejetant la demande de M. A d'assimilation au grade de commandant dans la hiérarchie militaire et du rappel pécuniaire qui s'y rattache.<br/>
Article 2 : La demande présentée par M. A devant le tribunal de Cayenne et les conclusions présentées par les consorts A devant le Conseil d'Etat sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A, à M. Stéphane A, à Mlle Caroline A et au ministre de la défense.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
