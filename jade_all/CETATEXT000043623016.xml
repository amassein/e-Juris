<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043623016</ID>
<ANCIEN_ID>JG_L_2021_06_000000446694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/62/30/CETATEXT000043623016.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 07/06/2021, 446694, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446694.20210607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. I... D... a demandé au tribunal administratif de Mayotte d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Bouéni (Mayotte) et de déclarer inéligible M. E... H... en application de l'article L. 118-4 du code électoral. <br/>
<br/>
              Par un jugement n° 2000652 du 21 octobre 2020, ce tribunal a rejeté sa protestation. <br/>
<br/>
              Par une requête enregistrée le 20 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ; <br/>
<br/>
              3°) de déclarer M. H... inéligible ;<br/>
<br/>
              4°) de mettre à la charge de M. H... une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François-René Burnod, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Bouéni (Mayotte), la liste "Union pour la continuité et le développement de la commune de Bouéni" conduite par M. H..., maire sortant, a obtenu 1949 voix soit 55,17% des suffrages exprimés, 23 sièges de conseillers municipaux et 6 sièges de conseillers communautaires, tandis que la liste "Mouvement du renouveau du Grand Sud", conduite par M. D..., a obtenu 1584 voix, soit 44,83% des suffrages exprimés, 6 sièges au conseil municipal et le dernier siège de conseiller communautaire. M. D... relève appel du jugement du 21 octobre 2020 par lequel le tribunal administratif de Mayotte a rejeté sa protestation dirigée contre ces opérations électorales. <br/>
<br/>
              2. En premier lieu, aux termes du deuxième alinéa de l'article L.52-8 du code électoral, " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués". M. D... soutient que la commune de Bouéni et plusieurs entreprises auraient, en méconnaissance de ces dispositions, participé au financement de la campagne de M. H..., notamment en mettant des véhicules gratuitement à sa disposition. Toutefois, ces allégations ne sauraient être regardées comme établies par la seule production de photographies de trois véhicules, dont un véhicule utilitaire d'une entreprise du secteur du bâtiment, portant une affiche de campagne de M. H....  <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 52-1 du code électoral, " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin ". M. D... soutient que plusieurs publications mises en ligne sur le site Facebook de la commune, entre le 5 mars et le 14 juin, ont constitué une campagne de promotion publicitaire en faveur du maire sortant, méconnaissant ces dispositions. Il résulte toutefois de l'instruction que le message publié le 5 mars 2020 sur ce site portait sur la création d'un centre social par le conseil départemental, que les publications mises en ligne les 2, 7 et 20 avril 2020 avaient pour objet de donner des informations pratiques sur la collecte de déchets par les services de la commune, que les publications intervenues les 22 avril, 11 mai et 20 mai 2020 concernaient, respectivement, la distribution de bons d'achat, de masques en tissu, et de colis alimentaires et, enfin, que la publication du 11 juin 2020 portait sur une réunion du maire avec des agriculteurs de la commune. Ces publications, qui revêtaient un caractère purement informatif et étaient relatives à des actions du département ou de la commune, ne peuvent ainsi être regardées comme constituant les éléments d'une campagne promotion publicitaire au sens de l'article 52-1 du code électoral. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article L. 19 du code électoral fixant la composition de la commission de contrôle des listes électorales: " Dans les communes de 1 000 habitants et plus dans lesquelles deux listes ont obtenu des sièges au conseil municipal lors de son dernier renouvellement, la commission est composée : 1° De trois conseillers municipaux appartenant à la liste ayant obtenu le plus grand nombre de sièges, pris dans l'ordre du tableau parmi les membres prêts à participer aux travaux de la commission, à l'exception du maire, des adjoints titulaires d'une délégation et des conseillers municipaux titulaires d'une délégation en matière d'inscription sur la liste électorale ; 2° De deux conseillers municipaux appartenant à la deuxième liste ayant obtenu le plus grand nombre de sièges, pris dans l'ordre du tableau parmi les membres prêts à participer aux travaux de la commission, à l'exception du maire, des adjoints titulaires d'une délégation et des conseillers municipaux titulaires d'une délégation en matière d'inscription sur la liste électorale ". S'il résulte de l'instruction que Mme C..., désignée par le préfet de Mayotte, par un arrêté du 12 décembre 2018, comme membre de cette commission au titre des conseillers municipaux appartenant à la deuxième liste ayant obtenu le plus grand nombre de sièges, ne s'est pas représentée à l'occasion du scrutin de 2020 et que M. A..., désigné au même titre, a rejoint la liste de M. H... à l'occasion de ce même scrutin, de telles circonstances ne sont pas de nature à entacher d'irrégularité la composition de la commission. <br/>
<br/>
              5. En quatrième lieu, si M. D... soutient que 151 procurations régulièrement établies auraient été irrégulièrement rejetées par les services de la commune, au motif inexact qu'elles n'auraient pas été reçues à temps, l'intéressé se borne à produire à l'appui de ses allégations un tableau comportant 107 noms de mandants ainsi qu'une copie d'une centaine de récépissés de dépôt de procuration qui ne sont de nature à établir ni que ces procurations ont été données à des mandataires effectivement inscrits sur les listes électorales de la commune, ni qu'elles sont parvenues dans les délais requis aux services de la commune de Bouéni, ni qu'elles ont été rejetées par ces services. <br/>
<br/>
              6. En cinquième lieu, s'agissant des votes par procuration, s'il résulte de l'instruction que le récépissé remis à Mme B... indique qu'elle avait donné procuration à Mme G... alors que le registre des procurations mentionne que son mandataire était M. F..., de sorte que le vote correspondant ne saurait être regardé comme régulièrement exprimé, M. D... n'apporte en revanche aucun élément de preuve au soutien de ses allégations relatives à l'existence d'autres discordances entre les récépissés de demande de procuration et les listes d'émargement ou à des procurations qui auraient été établies par les agents municipaux. Il y a seulement lieu, par suite, de retrancher un suffrage du nombre total des suffrages exprimés et du nombre de voix obtenues par la liste arrivée en tête, ce qui, eu égard à l'écart de voix, demeure sans incidence sur le résultat du scrutin. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. D... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Mayotte a rejeté sa protestation. <br/>
<br/>
              8. Aux termes de l'article L. 118-4 du code électoral : " Saisi d'une contestation formée contre l'élection, le juge de l'élection peut déclarer inéligible, pour une durée maximale de trois ans, le candidat qui a accompli des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin ". Dès lors qu'il ne résulte pas de l'instruction que des irrégularités imputables à M. H... auraient entaché la sincérité du scrutin, les conclusions de M. D... tendant à ce que celui-ci soit déclaré inéligible en application de ces dispositions ne peuvent qu'être rejetées. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. H..., qui n'est pas dans la présente instance la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de M. D... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée. <br/>
Article 2 : les conclusions de M. H... sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la M. I... D..., à M. E... H... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
