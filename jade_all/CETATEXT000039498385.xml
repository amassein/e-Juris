<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039498385</ID>
<ANCIEN_ID>JG_L_2019_12_000000419760</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/49/83/CETATEXT000039498385.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 11/12/2019, 419760</TITRE>
<DATE_DEC>2019-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419760</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:419760.20191211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif d'Orléans d'annuler l'arrêté du 21 janvier 2015 par lequel le préfet d'Eure-et-Loir a déclaré d'utilité publique les travaux d'aménagement de la rue de Bruxelles, sur le territoire de la commune de Vernouillet. <br/>
<br/>
              Par un jugement n° 1500972 du 29 novembre 2016, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT00345 du 12 février 2018, la cour administrative d'appel de Nantes a rejeté l'appel de Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 avril et 3 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de Mme A... et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la commune de Vernouillet ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 janvier 2015, le préfet d'Eure-et-Loir a déclaré d'utilité publique les travaux d'aménagement de la rue de Bruxelles, à Vernouillet (Eure-et-Loir). Mme A..., propriétaire du terrain de 7066 m² constituant le périmètre de la déclaration, comprenant sa maison d'habitation ainsi qu'un bâtiment destiné à une activité de commerce, en a demandé l'annulation pour excès de pouvoir au tribunal administratif d'Orléans. Par un jugement du 29 novembre 2016, ce dernier a rejeté sa demande. Par un arrêt du 12 février 2018 contre lequel Mme A... se pourvoit en cassation, la cour administrative d'appel de Nantes a rejeté son appel contre le jugement du 29 novembre 2016. <br/>
<br/>
              2. Il appartient au juge, lorsqu'il doit se prononcer sur le caractère d'utilité publique d'une opération nécessitant l'expropriation d'immeubles ou de droits réels immobiliers, de contrôler successivement qu'elle répond à une finalité d'intérêt général, que l'expropriant n'était pas en mesure de réaliser l'opération dans des conditions équivalentes sans recourir à l'expropriation, notamment en utilisant des biens se trouvant dans son patrimoine et, enfin, que les atteintes à la propriété privée, le coût financier et, le cas échéant, les inconvénients d'ordre social ou économique que comporte l'opération ne sont pas excessifs eu égard à l'intérêt qu'elle présente.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'opération projetée, qui s'inscrit dans le cadre du projet de restructuration de l'entrée Sud de l'agglomération de Dreux, consiste à réaliser, à Vernouillet, sur la surface de l'actuelle rue de Bruxelles et des parcelles expropriées, une nouvelle voie d'accès à la zone d'activités commerciales dite " Plein Sud ", deux giratoires, ainsi qu'un espace de stationnement de quatre-vingt-dix places, des cheminements piétons et des aménagements paysagers. Cette opération, dite de " requalification du paysage urbain ", est justifiée par l'objectif de renforcer l'attractivité du secteur ouest de la zone d'activités commerciales dite " Plein Sud " par l'amélioration de l'accès à ce secteur et de sa visibilité. Or, si une telle opération peut être regardée comme répondant à une finalité d'intérêt général et ne peut être réalisée sans procéder aux expropriations litigieuses, il ressort également des pièces du dossier soumis aux juges du fond, notamment du rapport du commissaire enquêteur, que son apport à l'amélioration de l'accessibilité à ce secteur de la zone commerciale est limité et que la justification de l'expropriation prévue réside essentiellement dans l'objectif d'une amélioration de la visibilité de ce secteur, quand bien des places de stationnement supplémentaires seraient réalisées. Dans ces conditions, en jugeant que l'atteinte aux droits de propriété de Mme A..., qui habite l'un des deux bâtiments concernés par l'expropriation envisagée, ainsi que le coût de l'opération, évalué à près de 1,2 millions d'euros, n'étaient pas excessifs eu égard à l'intérêt que celle-ci présente, la cour a commis une erreur de qualification juridique. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui a été dit ci-dessus que l'opération en cause ne présente pas un caractère d'utilité publique. Par suite, et sans qu'il besoin d'examiner les autres moyens présentés par Mme A..., il y a lieu d'annuler le jugement du tribunal administratif d'Orléans du 29 novembre 2016 et l'arrêté attaqué du préfet d'Eure-et-Loir du 21 janvier 2015.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 6 000 euros, à verser à Mme A..., au titre des instances d'appel et de cassation.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 12 février 2018 est annulé. <br/>
Article 2 : Le jugement du tribunal administratif d'Orléans du 29 novembre 2016 et l'arrêté du préfet d'Eure-et-Loir du 21 janvier 2015 déclarant d'utilité publique les travaux d'aménagement de la rue de Bruxelles sur le territoire de la commune de Vernouillet sont annulés.<br/>
Article 3 : L'Etat versera à Mme A... la somme de 6 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B... A..., à la commune de Vernouillet et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-01-01-01 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. NOTIONS GÉNÉRALES. NOTION D'UTILITÉ PUBLIQUE. ABSENCE. - OPÉRATION DE RÉAMÉNAGEMENT URBAIN AUTOUR D'UNE ZONE COMMERCIALE - BILAN NÉGATIF.
</SCT>
<ANA ID="9A"> 34-01-01-01 Opération s'inscrivant dans le cadre du projet de restructuration d'une entrée d'agglomération et consistant à réaliser, sur la surface de l'actuelle rue et des parcelles expropriées, une nouvelle voie d'accès à une zone d'activités commerciales (ZAC), deux giratoires, ainsi qu'un espace de stationnement de 90 places, des cheminements piétons et des aménagements paysagers. Cette opération, dite de requalification du paysage urbain, est justifiée par l'objectif de renforcer l'attractivité du secteur ouest de la ZAC par l'amélioration de l'accès à ce secteur et de sa visibilité.... ,,Or, si une telle opération peut être regardée comme répondant à une finalité d'intérêt général et ne peut être réalisée sans procéder aux expropriations litigieuses, il ressort également des pièces du dossier soumis aux juges du fond, notamment du rapport du commissaire enquêteur, que son apport à l'amélioration de l'accessibilité à ce secteur de la zone commerciale est limité et que la justification de l'expropriation prévue réside essentiellement dans l'objectif d'une amélioration de la visibilité de ce secteur, quand bien des places de stationnement supplémentaires seraient réalisées.... ,,Dans ces conditions, en jugeant que l'atteinte aux droits de propriété de la requérante, qui habite l'un des deux bâtiments concernés par l'expropriation envisagée, ainsi que le coût de l'opération, évalué à près de 1,2 millions d'euros, n'étaient pas excessifs eu égard à l'intérêt que celle-ci présente, la cour a commis une erreur de qualification juridique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
