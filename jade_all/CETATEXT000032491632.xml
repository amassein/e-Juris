<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491632</ID>
<ANCIEN_ID>JG_L_2016_05_000000399040</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/16/CETATEXT000032491632.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 02/05/2016, 399040, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399040</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:399040.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 21 avril 2016 par laquelle l'Agence française de lutte contre le dopage (AFLD), à la suite de la décision n° 394199 du Conseil d'Etat du 15 avril 2016 ramenant à une interdiction de participer pendant un an la sanction prise le 10 septembre 2015 à son encontre, a décidé d'établir le reliquat de la sanction à exécuter à 6 mois et 13 jours, déduction faite de la période effectivement purgée de 5 mois et 17 jours ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie, dès lors que la décision litigieuse menace sa carrière de sportif professionnel et fait obstacle à sa participation aux tournois qualifiants pour la sélection aux jeux olympiques de Rio organisés au mois d'août 2016, notamment au tournoi du Grand Slam de Bakou, au Grand Prix du Kazakhstan ainsi qu'aux masters de Guadalajara, qui se déroulent sur la période courant du 6 au 29 mai 2016 ;<br/>
              - il existe un doute sérieux sur la légalité de la décision contestée ;<br/>
              - aucune disposition législative ou règlementaire ne confère à l'Agence la compétence pour décider des conditions d'exécution d'une décision de justice, ni pour l'interpréter ; <br/>
              - le quorum requis par les dispositions de l'article L. 232-7 du code du sport n'était pas atteint ;<br/>
              - les membres du collège n'ont pas été convoqués dans le respect des prescriptions de l'article 7 de la délibération n° 2 du 5 octobre 2006 portant règlement intérieur du collège de l'Agence ;<br/>
              - l'Agence n'a respecté ni les garanties inhérentes à une procédure disciplinaire ni les droits de la défense ;<br/>
              - la décision litigieuse est entachée d'une erreur de droit au regard des principes de non rétroactivité des décisions d'annulation et de non cumul des sanctions.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
              - la décision du Conseil d'Etat n° 394199 statuant au contentieux du 15 avril 2016 ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 232-21 du code du sport : " Toute personne qui a contrevenu aux dispositions des articles L. 232-9, L. 232-9-1, L. 232-10, L. 232-14-5, L. 232-15, L. 232-15-1 ou L. 232-17 encourt des sanctions disciplinaires de la part de la fédération dont elle est licenciée. Il en est de même pour les licenciés complices de ces manquements. (...) Ces sanctions sont prononcées par les fédérations sportives mentionnées à l'article L. 131-8. A cet effet, les fédérations adoptent dans leur règlement des dispositions définies par décret en Conseil d'Etat et relatives aux contrôles organisés en application du présent titre, ainsi qu'aux procédures disciplinaires et aux sanctions applicables, dans le respect des droits de la défense. Ce règlement dispose que l'organe disciplinaire de première instance de ces fédérations se prononce, après que l'intéressé a été mis en mesure de présenter ses observations, dans un délai de dix semaines à compter de la date à laquelle l'infraction a été constatée(...) " ; que l'article L. 232-22 du même code prévoit qu'en cas d'infraction aux dispositions rappelées ci-dessus : " L'Agence française de lutte contre le dopage exerce un pouvoir de sanction disciplinaire dans les conditions suivantes :/ 1° Elle est compétente pour infliger des sanctions disciplinaires aux personnes non licenciées (...) ;/ 2° Elle est compétente pour infliger des sanctions disciplinaires aux personnes relevant du pouvoir disciplinaire d'une fédération sportive lorsque celle-ci n'a pas statué dans les délais prévus à l'article L. 232-21. Dans ce cas, l'agence se saisit d'office dès l'expiration de ces délais ;/ 3°) Elle peut réformer les décisions prises en application de l'article L. 232-21. Dans ces cas, l'agence se saisit, dans un délai de deux mois à compter de la réception du dossier complet, des décisions prises par les fédérations agréées " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que l'Agence française de lutte contre le dopage a inscrit M.B..., en sa qualité de judoka de haut niveau, dans le "groupe cible " des sportifs en application de l'article L. 232-15 du code du sport ; que, par une décision du 5 février 2015, l'organe disciplinaire de première instance de la Fédération française de judo, jujitsu, kendo et disciplines associées a prononcé à l'encontre de M. B...une mesure de suspension de compétition d'une durée de dix mois ; que statuant en matière disciplinaire, le collège de l'AFLD a, par une décision du 10 septembre 2015, en premier lieu, prononcé à l'encontre de M. B...une sanction d'interdiction de participer pendant deux ans aux manifestations sportives organisées ou autorisées par la Fédération française de judo, jujitsu, kendo et disciplines associées, en déduisant de cette sanction les périodes déjà purgées en application, d'une part, de la sanction de suspension provisoire prononcée le 11 décembre 2014 et, d'autre part, de la sanction prise à son encontre le 5 février 2015 par la fédération, en deuxième lieu, annulé l'ordonnance " de rétraction ", retirant la décision du 5 février 2015, prise par le président de l'organe disciplinaire de première instance de la fédération et, enfin, réformé la décision de la fédération du 5 février 2015 ; que, par une ordonnance n°394200 du 24 novembre 2015, le juge des référés du Conseil d'Etat, statuant sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative a suspendu l'exécution de cette décision ; <br/>
<br/>
	4. Considérant que, par une décision n° 394199 du 15 avril 2016, le Conseil d'Etat statuant au contentieux a ramené la sanction prise à l'encontre de M. B... à une interdiction de participer pendant un an aux manifestations sportives organisées ou autorisées par la Fédération française de judo, jujitsu, kendo et disciplines associées ; qu'il a en outre précisé que " la présente décision a pour effet de mettre fin à la suspension prononcée par le juge des référés du Conseil d'Etat par une ordonnance du 24 novembre 2015 et de redonner application, dans la limite d'un an, à la sanction d'interdiction de participer aux manifestations sportives organisées ou autorisées par la Fédération française de judo, jujitsu, kendo et disciplines associées infligée le 10 septembre 2015 par l'Agence française de lutte contre le dopage à M.C..., avec déduction de la période déjà purgée en raison tant de la décision de suspension temporaire que de la sanction de la fédération ; qu'il y a lieu, en outre, de déduire la durée qui s'est écoulée entre la notification à M. C...de la sanction infligée par l'Agence et la suspension de celle-ci par l'effet de l'ordonnance du juge des référés du Conseil d'Etat en date du 24 novembre 2015 ; " ; que pour l'exécution de cette décision, l'Agence française de lutte contre le dopage, par une décision D. n° 2016-49 du 21 avril 2016, a indiqué que la durée de sanction purgée des périodes d'interdiction effectivement supportées par M. B...est de 5 mois et 17 jours et que le reliquat de la sanction à exécuter s'établit à 6 mois et 15 jours ;<br/>
<br/>
              5. Considérant que, pour justifier de l'existence d'un doute sérieux quant à la légalité de la décision attaquée, M. B...soutient en premier lieu, qu'aucune disposition législative et règlementaire ne confère à l'Agence de compétence pour décider des conditions particulières d'exécution d'une décision de suspension prononcée par le Conseil d'Etat en matière disciplinaire ni pour interpréter le sens d'une telle décision, l'Agence ayant épuisé sa saisine en rendant sa décision du 10 septembre 2015; en deuxième lieu, que l'Agence n'a pas assorti la décision litigieuse des garanties inhérentes à la procédure de sanction disciplinaire, alors que, nonobstant le fait que les modalités de calcul retenues présentent un caractère défavorable pour l'intéressé, celui-ci n'a pas été informé qu'une telle mesure était susceptible d'être prise ni convoqué à la séance au cours de laquelle l'agence s'est prononcée, que les membres du collège n'ont pas tous été convoqués à cette séance et que le quorum requis par les dispositions de l'article L. 232-7 du code du sport n'était pas atteint ; en dernier lieu, que la décision de l'AFLD méconnaît les principes de non rétroactivité des décisions d'annulation et de non-cumul des sanctions en ce qu'elle retient que l'annulation de l'ordonnance de retrait du 7 avril 2015 du président de l'organe disciplinaire de première instance par la décision du 10 septembre 2015 de l'AFLD n'a pas produit d'effets pour le passé ;<br/>
<br/>
              6. Considérant, toutefois, que, par sa décision du 15 avril 2016 dont la portée est rappelée au point 4, le Conseil d'Etat a indiqué que devait être déduite de la durée d'un an à laquelle était ramenée la sanction d'interdiction de participer à des manifestations sportives organisées ou autorisées par la Fédération française de judo, jujitsu, kendo et disciplines associées prise à l'encontre de M. B... la période déjà purgée par son auteur, en raison tant de la décision de suspension provisoire que de la sanction de la fédération, ainsi que la durée écoulée entre la sanction prononcée par l'AFLD le 10 septembre 2015 et la suspension de cette sanction par l'ordonnance du juge des référés du Conseil d'Etat du 24 novembre 2015 ; que par la décision litigieuse, l'AFLD s'est bornée à préciser, conformément aux indications données par le Conseil d'Etat, la durée de la période déjà purgée, devant s'imputer sur la durée totale de la sanction, et la durée du reliquat de la sanction, et n'a pas pris une nouvelle décision de sanction ; qu'en fixant ces durées à respectivement 5 mois et 17 jours et 6 mois et 13 jours, l'AFLD s'est en tout point conformé à la chose jugée par le Conseil d'Etat dans sa décision du 15 avril 2016 ; qu'ainsi aucun des moyens soulevés par M. B...n'est, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de la décision contestée ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, qu'il est manifeste que la demande de M. B...ne peut être accueillie ; que la requête doit, en conséquence, être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B....<br/>
Copie en sera adressée pour information à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
