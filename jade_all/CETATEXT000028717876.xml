<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717876</ID>
<ANCIEN_ID>JG_L_2014_03_000000365877</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 12/03/2014, 365877, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365877</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365877.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 février 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre délégué, chargé du budget ; le ministre demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'article 2 de l'arrêt n° 12NT01642 du 20 décembre 2012 par lequel la cour administrative d'appel de Nantes, statuant sur sa requête contre le jugement n° 0904641 du 19 avril 2012 du tribunal administratif de Rennes déchargeant la société Diana Ingrédients des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre de l'année 2004 à hauteur de 21 856 euros, après avoir remis à la charge de la société les sommes correspondant, pour la détermination du montant des crédits d'impôt recherche, à celles versées au profit du comité d'entreprise, a rejeté le surplus de ses conclusions tendant à exclure des dépenses éligibles au crédit d'impôt recherche les sommes versées au titre de l'intéressement et de la participation versées au personnel de recherche ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire intégralement droit à son appel ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat des sociétés Diana et Diana naturals ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Diana Naturals, filiale fiscalement intégrée de la société Diana, anciennement société Diana Ingrédients, a fait l'objet d'une vérification de comptabilité portant sur le crédit d'impôt recherche pour les années 2004, 2005 et 2006 ; qu'à l'issue de ce contrôle, l'administration a remis en cause une fraction des crédits d'impôt dont la société a bénéficié correspondant aux sommes versées au titre de l'intéressement des salariés et de leur participation aux résultats de l'entreprise et aux versements effectués au bénéfice du comité d'entreprise ; que par un jugement du 19 avril 2012, le tribunal administratif de Rennes a prononcé la décharge des suppléments de cotisations à l'impôt sur les sociétés mis à la charge de la société Diana Ingrédients au titre de l'année 2004 ; que le ministre se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Nantes du 20 décembre 2012 qui n'a annulé le jugement qu'en tant qu'il avait prononcé la décharge des cotisations supplémentaires d'impôt sur les sociétés correspondant aux sommes versées au comité d'entreprise pour l'année 2004 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 244 quater B du code général des impôts, dans sa rédaction applicable au litige : " I. - Les entreprises industrielles et commerciales ou agricoles imposées d'après leur bénéfice réel ou exonérées en application des articles 44 sexies, 44 sexies A, 44 septies, 44 octies, 44 decies et 44 undecies qui exposent des dépenses de recherche peuvent bénéficier d'un crédit d'impôt (...) II. Les dépenses de recherche ouvrant droit au crédit d'impôt sont (...) b) Les dépenses de personnel afférentes aux chercheurs et techniciens de recherche directement et exclusivement affectés à ces opérations  " ; qu'aux termes de l' article 49 septies I de l'annexe III au même code : " Pour la détermination des dépenses de recherche visées aux a, b, f et au 2° du h du II de l'article 244 quater B du code général des impôts, il y a lieu de retenir : / (...) b. Au titre des dépenses de personnel, les rémunérations et leurs accessoires ainsi que les charges sociales, dans la mesure où celles-ci correspondent à des cotisations sociales obligatoires " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'alors même que les montants des versements effectués par une société à ses salariés en application d'un accord d'intéressement ou en application du régime légal de participation sont déterminés, en application des dispositions du code du travail, en fonction du résultat ou des performances et, le cas échéant, du bénéfice de l'entreprise, ces versements n'ont pas, contrairement à ce que soutient le ministre, le caractère d'une affectation du résultat de la société, mais donnent lieu à la comptabilisation de charges déductibles du résultat de l'exercice au titre duquel ils sont effectués ; qu'ils constituent pour les chercheurs et techniciens qui en bénéficient un accessoire de leur rémunération ; qu'en règle générale, ces sommes sont imposées à l'impôt sur le revenu entre les mains des salariés bénéficiaires, dans la catégorie des traitements et salaires ; que, dans ces conditions, sans qu'y fasse obstacle la qualification que donnent à ces versements des dispositions du code du travail et du code de la sécurité sociale, ces versements constituent, pour les salariés, un accessoire de leur rémunération, au sens des dispositions de l'article 49 septies I de l'annexe III au code général des impôts et, pour la société, des dépenses de personnel pouvant être comprises dans l'assiette du crédit d'impôt recherche, sur le fondement des dispositions précitées du II de l'article 244 quater B du code général des impôts ;<br/>
<br/>
              4. Considérant que, dès lors, en statuant comme elle l'a fait, la cour administrative d'appel de Nantes, qui a répondu à tous les moyens soulevés devant elle, n'a pas commis d'erreur de droit et n'a pas inexactement qualifié les faits ; que, par suite, le ministre n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 1 500 euros à verser à la société Diana Naturals et à la société Diana au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : Le pourvoi du ministre délégué, chargé du budget est rejeté.<br/>
<br/>
 Article 2 : L'Etat versera à la société Diana Naturals et à la société Diana une somme globale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances, à la société Diana Naturals et à la société Diana.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
