<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926978</ID>
<ANCIEN_ID>JG_L_2021_08_000000434456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 06/08/2021, 434456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434456.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 10 septembre 2019 et 12 mai 2021 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 2 septembre 2019 de la ministre de la transition écologique et solidaire relatif à la capture des vanneaux et des pluviers dorés dans le département des Ardennes pour la campagne 2019-2020 ;<br/>
<br/>
              2°) d'enjoindre à la ministre de la transition écologique et solidaire, sur le fondement de l'article L. 911-1 du code de justice administrative, de procéder à l'abrogation de l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes ;<br/>
<br/>
              3°) à titre subsidiaire, de transmettre à la Cour de justice de l'Union européenne une question préjudicielle relative à la conformité des méthodes de chasse traditionnelles autorisées sur le fondement de l'article L. 424-4 du code de l'environnement avec les articles 8 et 9 de la directive 2009/147/CE du Parlement et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la directive 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2019-773 du 24 juillet 2019 ;<br/>
              - l'arrêté du 26 juin 1987 fixant la liste des espèces de gibier dont la chasse est autorisée ;<br/>
              - l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes ;<br/>
              - l'arrêt C-900/19 du 17 mars 2021 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'association One Voice ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 juillet 2021, présentée par la ministre de la transition écologique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 2 septembre 2019 relatif à la capture des vanneaux et des pluviers dorés dans le département des Ardennes pour la campagne 2019-2020, dont l'association One Voice demande l'annulation pour excès de pouvoir, la ministre de la transition écologique et solidaire a fixé à respectivement 1 200 et 30 le nombre de vanneaux huppés et de pluviers dorés pouvant être chassés par tenderie dans le département des Ardennes pour la campagne 2019-2020. <br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêté du 2 septembre 2019 :<br/>
<br/>
              2. Aux termes du paragraphe 1 de l'article 8 de la directive du 30 novembre 2009 concernant la conservation des oiseaux sauvages, dite directive oiseaux : " 1. En ce qui concerne la chasse, la capture ou la mise à mort d'oiseaux dans le cadre de la présente directive, les États membres interdisent le recours à tous moyens, installations ou méthodes de capture ou de mise à mort massive ou non sélective ou pouvant entraîner localement la disparition d'une espèce, et en particulier à ceux énumérés à l'annexe IV, point a). / (...) ". Parmi les moyens, installations ou méthode de capture ou de mise à mort prohibés par le a) de l'annexe IV de la directive figure notamment les " collet (...), gluaux, hameçons, oiseaux vivants utilisés comme appelants aveuglés ou mutilés, enregistreurs, appareils électrocutants " ou encore les " filets, pièges-trappes, appâts empoisonnés ou tranquillisants (...) ". Toutefois, l'article 9 de la directive prévoit en son paragraphe 1 que " Les États membres peuvent déroger aux articles 5 à 8 s'il n'existe pas d'autre solution satisfaisante, pour les motifs ci-après : / (...) c) pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux en petites quantités ". Par ailleurs, le paragraphe 2 de cet article 9 prévoit que les dérogations doivent mentionner les espèces concernées, les moyens, installations ou méthodes de capture ou de mise à mort autorisés, les conditions de risque et les circonstances de temps et de lieu dans lesquelles ces dérogations peuvent être prises, l'autorité habilitée à déclarer que les conditions exigées sont réunies, à décider quels moyens, installations ou méthodes peuvent être mis en oeuvre, dans quelles limites et par quelles personnes, enfin les contrôles qui seront opérés.<br/>
<br/>
              3. Il résulte de ces dispositions de la directive, telles qu'interprétées par la Cour de justice de l'Union européenne, dans son arrêt du 17 mars 2021 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, qu'une réglementation nationale faisant usage des possibilités de dérogation prévues à l'article 9 de la directive ne remplit pas les conditions relatives à l'obligation de motivation découlant du paragraphe 2 de cet article, lorsqu'elle contient la seule indication selon laquelle il n'existe pas d'autre solution satisfaisante, sans que cette indication soit étayée par une motivation circonstanciée, fondée sur les meilleures connaissances scientifiques pertinentes et exposant les motifs ayant conduit l'autorité compétente à la conclusion que l'ensemble des conditions susceptibles de permettre une dérogation, parmi lesquelles celle relative à l'inexistence d'une autre solution satisfaisante, étaient réunies. <br/>
<br/>
              4. Il résulte également de ces dispositions, telles qu'interprétées par la Cour de justice, que les motifs de dérogation prévus à l'article 9 de la directive sont d'interprétation stricte et, à cet égard, que si les méthodes traditionnelles de chasse sont susceptibles de constituer une exploitation judicieuse de certains oiseaux au sens de la directive, l'objectif de préserver ces méthodes ne constitue pas un motif autonome de dérogation au sens de cet article. Par suite, le caractère traditionnel d'une méthode de chasse ne suffit pas, en soi, à établir qu'une autre solution satisfaisante, au sens des dispositions du paragraphe 1 de cet article 9, ne peut être substituée à cette méthode, de même que le simple fait qu'une autre méthode requerrait une adaptation et, par conséquent, exigerait de s'écarter de certaines caractéristiques d'une tradition, ne saurait suffire pour considérer qu'il n'existe pas une telle autre solution satisfaisante.<br/>
<br/>
              5. Les dispositions de la directive du 30 novembre 2009 citées au point 2 ont pour partie été transposées à l'article L. 424-2 du code de l'environnement relatif notamment aux temps de chasse et à l'interdiction de chasse des oiseaux migrateurs pendant leur trajet de retour vers leur lieu de nidification, sous réserve de dérogation et à l'article L. 424-4 du même code, relatif aux modes et moyens de chasse autorisés aux détenteurs d'un permis de chasse valide. Selon l'article L. 424-2 du code de l'environnement, dans sa rédaction applicable en l'espèce issue de la loi du 24 juillet 2019 portant création de l'Office français de la biodiversité, modifiant les missions des fédérations des chasseurs et renforçant la police de l'environnement : " (...) Les oiseaux ne peuvent être chassés ni pendant la période nidicole ni pendant les différents stades de reproduction et de dépendance. Les oiseaux migrateurs ne peuvent en outre être chassés pendant leur trajet de retour vers leur lieu de nidification. / Des dérogations peuvent être accordées, s'il n'existe pas d'autre solution satisfaisante et à la condition de maintenir dans un bon état de conservation les populations migratrices concernées : / (...) 2° Pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux en petites quantités ; / (...) ". En vertu de l'article L. 424-4 du même code : " Dans le temps où la chasse est ouverte, le permis donne à celui qui l'a obtenu le droit de chasser de jour, soit à tir, soit à courre, à cor et à cri, soit au vol, suivant les distinctions établies par des arrêtés du ministre chargé de la chasse. (...) / (...) / Pour permettre, dans des conditions strictement contrôlées et de manière sélective, la chasse de certains oiseaux de passage en petites quantités, le ministre chargé de la chasse autorise, dans les conditions qu'il détermine, l'utilisation des modes et moyens de chasse consacrés par les usages traditionnels, dérogatoires à ceux autorisés par le premier alinéa. / Tous les moyens d'assistance électronique à l'exercice de la chasse, autres que ceux autorisés par arrêté ministériel, sont prohibés. / Les gluaux sont posés une heure avant le lever du soleil et enlevés avant onze heures. / Tous les autres moyens de chasse, y compris l'avion et l'automobile, même comme moyens de rabat, sont prohibés. / (...) ".<br/>
<br/>
              6. Sur le fondement des dispositions codifiées au code de l'environnement, l'article 1er de l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes prévoit que : " La capture des vanneaux huppés et des pluviers dorés à l'aide de filets à nappes fixés à terre, dénommée tenderie aux vanneaux, est autorisée " dans dix-sept communes limitativement énumérées du département " dans les conditions strictement contrôlées définies ci-après afin de permettre la capture sélective et en petites quantités de ces oiseaux, puisqu'il n'existe pas d'autre solution satisfaisante ". Aux termes de l'article 5 de cet arrêté : " Le nombre maximum d'oiseaux pouvant être capturés pendant la campagne est fixé chaque année par le ministre chargé de la chasse. "<br/>
<br/>
              7. En premier lieu, il résulte des termes mêmes de l'article 1er de l'arrêté du 17 août 1989 qu'il autorise la tenderie aux vanneaux dans les conditions qu'il détermine au seul motif qu'il " n'existe pas d'autre solution satisfaisante " sans autre précision. Aucune autre mention ni aucune disposition de l'arrêté du 2 septembre 2019 attaqué ne vient davantage expliciter, par une motivation circonstanciée, les motifs ayant conduit le ministre chargé de la chasse à retenir que la condition relative à l'inexistence d'une autre solution satisfaisante, posée à l'article 9 de la directive du 30 novembre 2009, était caractérisée. Par suite, cet arrêté doit être regardé comme méconnaissant l'obligation de motivation de l'absence d'autre solution satisfaisante découlant du paragraphe 2 de cet article.<br/>
<br/>
              8. En second lieu, il ressort des pièces du dossier que le motif de la dérogation prévue par l'arrêté du 17 août 1989 réside uniquement dans l'objectif de préserver l'utilisation des modes et moyens de chasse consacrés par les usages traditionnels qui, ainsi que l'a jugé la Cour de justice de l'Union européenne, ne saurait à lui seul constituer une démonstration suffisante de l'absence d'autre solution satisfaisante au sens de l'article 9 de la directive du 30 novembre 2009.<br/>
<br/>
              9. Il suit de là que les dispositions de l'arrêté du 17 août 1989 relatif à la tenderie aux vanneaux dans le département des Ardennes, sur le fondement duquel a été pris l'arrêté attaqué, doivent être regardées dans leur ensemble comme méconnaissant les objectifs de l'article 9 de la directive du 30 novembre 2009. <br/>
<br/>
              10. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens de la requête et sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne à titre préjudiciel, que l'association requérante est fondée à demander l'annulation de l'arrêté du 2 septembre 2019 qu'elle attaque.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 911-1 du code de justice administrative : <br/>
<br/>
              11. L'annulation de l'arrêté du 2 septembre 2019 n'implique, par elle-même, pas nécessairement l'abrogation de l'arrêté du 17 août 1989. Par suite, les conclusions tendant à ce qu'il soit enjoint au ministre d'abroger ce dernier arrêté ne peuvent qu'être rejetées.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              12. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par la requérante sur le fondement de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêté du 2 septembre 2019 de la ministre de la transition écologique et solidaire relatifs à la capture des vanneaux et des pluviers dorés dans le département des Ardennes pour la campagne 2019-2020 est annulé.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente décision sera notifiée à l'association One Voice et à la ministre de la transition écologique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
