<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335891</ID>
<ANCIEN_ID>JG_L_2019_11_000000428820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335891.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 06/11/2019, 428820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:428820.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Lille, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision de 375,46 euros, correspondant à sa rémunération entre les 23 et 26 janvier 2018 qui a été retenue sur son traitement par décision de la garde des sceaux, ministre de la justice. Par une ordonnance n° 1807162 du 26 février 2019, le juge des référés du tribunal administratif de Lille a accédé à sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 13 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la garde des sceaux, ministre de la justice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A....<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2017-1837 du 30 décembre 2017 ;<br/>
              - l'ordonnance n° 58-696 du 6 août 1958 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la garde des sceaux, ministre de la justice, retenant que M. A..., surveillant au centre pénitentiaire de Maubeuge, n'avait pas effectué son service du 23 au 26 janvier 2018 en raison de sa participation à un mouvement social ayant touché l'ensemble de l'administration pénitentiaire pendant cette période, a procédé à une retenue sur son traitement pour service non fait d'un montant de 375,46 euros, en dépit du certificat médical, produit par l'intéressé, indiquant qu'il était dans l'impossibilité d'exercer ses fonctions pour la période en cause. La garde des sceaux, ministre de la justice se pourvoit en cassation contre l'ordonnance du juge des référés du tribunal administratif de Lille qui a fait droit à la demande présentée par M. A... tendant à ce que l'Etat soit condamné à lui verser une provision de 375,46 euros sur le fondement de l'article R. 541-1 du code de justice administrative.<br/>
<br/>
              2. Aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie. "<br/>
<br/>
              3. D'une part, aux termes de l'article 3 de l'ordonnance du 6 août 1958 relative au statut spécial des fonctionnaires des services déconcentrés de l'administration pénitentiaire : " Toute cessation concertée du service, tout acte collectif d'indiscipline caractérisée de la part des personnels des services extérieurs de l'administration pénitentiaire est interdit. "<br/>
<br/>
              4. D'autre part, aux termes de l'article 34 de la loi du 11 janvier 1984 : " le fonctionnaire a droit (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; (...). Le bénéfice de ces dispositions est subordonné à la transmission par le fonctionnaire, à son administration, de l'avis d'arrêt de travail justifiant du bien-fondé du congé de maladie, dans un délai et selon les sanctions prévues en application de l'article 35. (...) ". Aux termes de l'article 25 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires : " Pour obtenir un congé de maladie (...), le fonctionnaire adresse à l'administration dont il relève, dans un délai de quarante-huit heures suivant son établissement, un avis d'interruption de travail. (...). L'administration peut faire procéder à tout moment à la contre-visite du demandeur par un médecin agréé ; le fonctionnaire doit se soumettre, sous peine d'interruption du versement de sa rémunération, à cette contre-visite. (...) ". <br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés, que, au cours de la période du 23 au 26 janvier 2018, un mouvement social de grande ampleur a affecté l'administration pénitentiaire à l'occasion duquel, à l'appel d'organisations syndicales nationales, de nombreux agents n'ont pas effectué leur service. Un grand nombre d'agents ayant été absents au cours de cette période a toutefois produit un avis médical pour justifier leur absence. Ainsi, au centre pénitentiaire de Maubeuge où était affecté M. A..., 98 agents du corps concerné sur 122 ont cessé le travail pendant la période en cause, et 89 d'entre eux ont produit un certificat médical portant sur les trois jours en cause. Dans ces circonstances particulières, marquées par un mouvement social de grande ampleur dans une administration où la cessation concertée du service est interdite et alors que l'administration soutient avoir été dans l'impossibilité pratique de faire procéder de manière utile aux contre-visites prévues par l'article 25 du décret du 14 mars 1986, le juge des référés a inexactement qualifié les faits de l'espèce en jugeant que l'obligation dont se prévalait M. A... n'était pas sérieusement contestable. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la garde des sceaux, ministre de la justice est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur la demande de provision présentée par M. A... devant le juge des référés du tribunal administratif de Lille.<br/>
<br/>
              7. Il résulte de l'instruction que, eu égard à la situation telle qu'elle a été exposée au point 5 ci-dessus, l'existence de l'obligation dont se prévaut M. A... ne peut être regardée comme n'étant pas sérieusement contestable. Par suite, sa demande de provision doit être rejetée, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Lille du 26 février 2019 est annulée. <br/>
<br/>
Article 2 : La requête présentée par M. A... devant le juge des référés du tribunal administratif de Lille est rejetée. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice et à M. B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
