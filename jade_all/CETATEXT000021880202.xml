<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021880202</ID>
<ANCIEN_ID>JG_L_2009_10_000000309247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/88/02/CETATEXT000021880202.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 02/10/2009, 309247</TITRE>
<DATE_DEC>2009-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>309247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP VIER, BARTHELEMY, MATUCHANSKY ; SCP ROGER, SEVAUX</AVOCATS>
<RAPPORTEUR>Mme Jeannette  Bougrab</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 10 septembre 2007 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Pierre A, demeurant ... ; M. A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 1er du jugement du 10 juillet 2007 par lequel le tribunal administratif de Dijon a rejeté sa demande d'annulation de la décision du directeur du centre hospitalier de Sens, révélée par un courrier de ce directeur en date du 21 février 2006, en tant qu'elle prévoit la validation de son activité opératoire par le chef du service d'oto-rhino-laryngologie de cet hôpital ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler cette décision ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Sens le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Jeannette Bougrab, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Roger, Sevaux, avocat de M. A et de la SCP Vier, Barthélemy, Matuchansky, avocat du centre hospitalier de Sens, <br/>
<br/>
              - les conclusions de M. Luc Derepas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Roger, Sevaux, avocat de M. A et à la SCP Vier, Barthélemy, Matuchansky, avocat du centre hospitalier de Sens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que si, en vertu de l'article L. 6143-7 du code de la santé publique, le directeur d'un établissement de santé publique assure la gestion et la conduite générale de l'établissement et dispose à cet effet d'un pouvoir hiérarchique sur l'ensemble de son personnel, il résulte du même article que l'autorité du directeur s'exerce  dans le respect des règles déontologiques ou professionnelles qui s'imposent aux professions de santé, des responsabilités qui sont les leurs dans l'administration des soins et de l'indépendance professionnelle du praticien dans l'exercice de son art  ; que l'article L. 6146-5-1 du même code, relatif aux pouvoirs des praticiens chefs de service, dispose par ailleurs que ceux-ci  assurent la mise en oeuvre des missions assignées à la structure dont ils ont la responsabilité et la coordination de l'équipe médicale qui s'y trouve affectée  ; qu'il résulte de ces dispositions que les pouvoirs des directeurs d'établissements et des chefs de service à l'égard des praticiens hospitaliers placés sous leur autorité ne peuvent s'exercer que dans le respect du principe de l'indépendance professionnelle des médecins, rappelé à l'article R. 4127-5 du code de la santé publique ; <br/>
<br/>
              Considérant qu'il résulte des pièces du dossier soumis au juge du fond que le directeur du centre hospitalier de Sens a subordonné l'ensemble des décisions pré-opératoires relatives notamment à l'indication opératoire, au degré d'urgence et aux moyens nécessaires, prises à l'égard de ses patients par M. A, praticien hospitalier du service d'oto-rhino-laryngologie, à une validation préalable par le chef de ce service ; qu'une telle décision, nonobstant son caractère provisoire ou la circonstance qu'elle serait intervenue pour mettre un terme à des tensions nées entre différents services, méconnaît l'indépendance professionnelle de M. A dans l'exercice de son art médical ; que, dès lors, en jugeant qu'elle pouvait trouver son fondement légal dans les dispositions rappelées ci-dessus des articles L. 6143-7 et L. 6146-5-1 du code de la santé publique, le tribunal administratif de Dijon a entaché sa décision d'une erreur de droit ;<br/>
<br/>
              Considérant que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, ce jugement doit en conséquence être annulé en tant que, par son article 1er, il rejette la demande de M. A ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que, s'il incombe au directeur d'un centre hospitalier de prendre les mesures que les impératifs de santé publique exigent dans son établissement, au besoin en saisissant les autorités administratives ou ordinales compétentes pour prononcer des mesures d'interdiction professionnelle, il résulte de ce qui a été dit ci-dessus que le principe de l'indépendance professionnelle des médecins fait obstacle à ce que les décisions prises par un praticien dans l'exercice de son art médical soient soumises à l'approbation d'un autre médecin ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens soulevés par lui devant le tribunal administratif de Dijon, M. A est fondé à demander l'annulation de la décision révélée par le courrier du 21 février 2006 du directeur du centre hospitalier en tant qu'elle soumet son activité pré-opératoire à une validation préalable par le chef du service d'oto-rhino-laryngologie de l'établissement ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que M. A, qui n'est pas la partie perdante dans la présente instance, soit condamné à payer au centre hospitalier de Sens la somme que ce dernier demande au titre des frais exposés par lui et non compris dans les dépens ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de faire application de ces mêmes dispositions et de mettre à la charge du centre hospitalier de Sens le versement d'une somme de 3 000 euros à M. A ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du 10 juillet 2007 du tribunal administratif de Dijon est annulé.<br/>
Article 2 : La décision du directeur du centre hospitalier de Sens, révélée par son courrier du 21 février 2006, est annulée en tant qu'elle soumet l'activité pré-opératoire de M. A à la validation préalable du chef de service d'oto-rhino-laryngologie.<br/>
Article 3 : Le centre hospitalier de Sens versera à M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du centre hospitalier de Sens tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. Pierre A et au centre hospitalier de Sens.<br/>
Copie en sera adressée pour information à la ministre de la santé et des sports.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-11-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. PERSONNEL MÉDICAL. PRATICIENS À TEMPS PLEIN. - PRINCIPE D'INDÉPENDANCE PROFESSIONNELLE DES MÉDECINS - CONSÉQUENCE - IMPOSSIBILITÉ DE SUBORDONNER DES ACTES MÉDICAUX À LA VALIDATION D'UN AUTRE PRATICIEN, FÛT-IL DIRECTEUR D'ÉTABLISSEMENT OU CHEF DE SERVICE.
</SCT>
<ANA ID="9A"> 36-11-01-03 Il résulte des dispositions des articles L. 6143-7 et L. 6146-5-1 du code de la santé publique que les pouvoirs des directeurs d'établissements et des chefs de service à l'égard des praticiens hospitaliers placés sous leur autorité ne peuvent s'exercer que dans le respect du principe de l'indépendance professionnelle des médecins, rappelé à l'article R. 4127-5 du code de la santé publique. En conséquence, un directeur d'établissement ne pouvait légalement subordonner l'accomplissement d'actes médicaux par un praticien oeuvrant au sein de son établissement à leur validation préalable par un chef de service.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
