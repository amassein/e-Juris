<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599959</ID>
<ANCIEN_ID>JG_L_2018_11_000000411010</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 09/11/2018, 411010</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411010</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411010.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 30 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. D...A...et Mme B...C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a refusé de retirer, modifier ou abroger les articles R. 111-42 et R. 111-49 du code de l'urbanisme ; <br/>
<br/>
              2°) d'ordonner au Premier ministre, dans le délai d'un mois à compter de la décision à intervenir et sous astreinte de 300 euros par jour de retard à l'expiration de ce délai, de modifier les articles R. 111-42 et R. 111-49 du code de l'urbanisme afin d'autoriser les gens du voyage qui le souhaitent à vivre dans des caravanes et résidences mobiles sur les terrains leur appartenant ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le pacte international relatif aux droits civils et politiques ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;   <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2000-614 du 5 juillet 2000 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 111-25 du code de l'urbanisme prévoit, au titre du règlement national d'urbanisme, qu' " un décret en Conseil d'Etat précise les conditions dans lesquelles peuvent être installées ou implantées des caravanes, résidences mobiles de loisirs et habitations légères de loisirs " et que ce décret détermine notamment les catégories de terrains aménagés sur lesquels les résidences mobiles de loisirs et les habitations légères de loisirs peuvent être installées ou implantées. En application de ces dispositions, d'une part, l'article R. 111-41 de ce code précise que " sont regardés comme des résidences mobiles de loisirs les véhicules terrestres habitables qui sont destinés à une occupation temporaire ou saisonnière à usage de loisirs, qui conservent des moyens de mobilité leur permettant d'être déplacés par traction mais que le code de la route interdit de faire circuler " et l'article R. 111-42 du même code dispose que : " Les résidences mobiles de loisirs ne peuvent être installées que : / 1° Dans les parcs résidentiels de loisirs spécialement aménagés à cet effet (...) ; / 2° Dans les villages de vacances classés en hébergement léger en application du code du tourisme ; / 3° Dans les terrains de camping régulièrement créés (...) ". D'autre part, l'article R. 111-47 du code de l'urbanisme précise que : " Sont regardés comme des caravanes les véhicules terrestres habitables qui sont destinés à une occupation temporaire ou saisonnière à usage de loisirs, qui conservent en permanence des moyens de mobilité leur permettant de se déplacer par eux-mêmes ou d'être déplacés par traction et que le code de la route n'interdit pas de faire circuler ". Aux termes de l'article R. 111-49 du code de l'urbanisme : " L'installation des caravanes, quelle qu'en soit la durée, est interdite dans les secteurs où la pratique du camping a été interdite dans les conditions prévues à l'article R. 111-34. (...) ". L'article R. 111-34 du même code prévoit que " la pratique du camping en dehors des terrains aménagés à cet effet peut (...) être interdite dans certaines zones par le plan local d'urbanisme ou le document d'urbanisme en tenant lieu. Lorsque cette pratique est de nature à porter atteinte à la salubrité, à la sécurité ou à la tranquillité publiques, aux paysages naturels ou urbains, à la conservation des perspectives monumentales, à la conservation des milieux naturels ou à l'exercice des activités agricoles et forestières, l'interdiction peut également être prononcée par arrêté du maire (...) ".<br/>
<br/>
              2. M. A...et MmeC..., qui sont propriétaires d'une parcelle, située en zone A du règlement du plan local d'urbanisme de la commune de Saint-André (Pyrénées-Orientales), sur laquelle l'installation des résidences mobiles de loisirs et des caravanes est interdite en vertu de ces dispositions, demandent l'annulation pour excès de pouvoir du refus implicite opposé par le Premier ministre à leur demande du 9 février 2017 tendant à ce que les articles R. 111-42 et R. 111-49 du code de l'urbanisme soient retirés, modifiés ou abrogés afin de permettre aux gens du voyage qui le souhaitent de vivre dans des caravanes et résidences mobiles sur les terrains leur appartenant.<br/>
<br/>
              3. Toutefois, le I de l'article 1er de la loi du 5 juillet 2000 relative à l'accueil et à l'habitat des gens du voyage dispose que : " Les communes participent à l'accueil des personnes dites gens du voyage et dont l'habitat traditionnel est constitué de résidences mobiles installées sur des aires d'accueil ou des terrains prévus à cet effet. / Ce mode d'habitat est pris en compte par les politiques et les dispositifs d'urbanisme, d'habitat et de logement adoptés par l'Etat et par les collectivités territoriales ". A ce titre, d'une part, le schéma départemental mentionné au II de cet article prévoit les secteurs géographiques d'implantation et les communes où doivent être réalisés, outre des aires permanentes d'accueil et des aires de grand passage, " 2° Des terrains familiaux locatifs aménagés et implantés dans les conditions prévues à l'article L. 444-1 du code de l'urbanisme et destinés à l'installation prolongée de résidences mobiles, le cas échéant dans le cadre des mesures définies par le plan départemental d'action pour le logement et l'hébergement des personnes défavorisées, ainsi que le nombre et la capacité des terrains". L'article L. 444-1 du code de l'urbanisme régit " l'aménagement de terrains bâtis ou non bâtis, pour permettre l'installation de résidences démontables constituant l'habitat permanent de leurs utilisateurs définies par décret en Conseil d'Etat ou de résidences mobiles au sens de l'article 1er de la loi n° 2000-614 du 5 juillet 2000 relative à l'accueil et à l'habitat des gens du voyage ", qu'il soumet à permis d'aménager ou à déclaration préalable, et, s'il impose en principe que ces terrains soient situés dans des secteurs constructibles, il permet leur aménagement dans des secteurs de taille et de capacité d'accueil limitées définis à cette fin dans les zones naturelles, agricoles ou forestières par le règlement du plan local d'urbanisme, en application de l'article L. 151-13 du code de l'urbanisme. D'autre part, si le I de l'article 9 de la loi du 5 juillet 2000 permet que, dans une commune qui remplit les obligations qui lui incombent en application de l'article 2, le stationnement des résidences mobiles mentionnées à l'article 1er soit interdit sur le territoire de la commune en dehors des aires d'accueil aménagées et le II du même article qu'en cas de stationnement effectué en violation d'une telle interdiction, les occupants soient mis en demeure de quitter les lieux, ces dispositions ne sont, en vertu du III du même article, " pas applicables au stationnement des résidences mobiles appartenant aux personnes mentionnées à l'article 1er de la présente loi : / 1° Lorsque ces personnes sont propriétaires du terrain sur lequel elles stationnent (...) ". Enfin, l'article R. 421-23 du code de l'urbanisme soumet à déclaration préalable : " d) L'installation, pour une durée supérieure à trois mois par an, d'une caravane autre qu'une résidence mobile mentionnée au j ci-dessous (...) j) L'installation d'une résidence mobile visée par l'article 1er de la loi n° 2000-614 de la loi n° 2000-614 du 5 juillet 2000 relative à l'accueil et à l'habitat des gens du voyage, constituant l'habitat permanent des gens du voyage, lorsque cette installation dure plus de trois mois consécutifs ".<br/>
<br/>
              4. Il résulte de l'ensemble des dispositions citées au point précédent que l'installation des résidences mobiles qui, au sens de l'article 1er de la loi du 5 juillet 2000, constituent l'habitat permanent de gens du voyage, est entièrement régie par des dispositions particulières qui, notamment, précisent les conditions dans lesquelles ces résidences peuvent faire l'objet d'une installation sur le terrain de leur propriétaire ou en zone non constructible, de même que pour une durée supérieure à trois mois. Les articles R. 111-42 du code de l'urbanisme, réglementant l'installation des résidences mobiles de loisirs, et R. 111-49 du même code, réglementant l'installation des caravanes, qui figurent d'ailleurs au sein d'une section dont l'article R. 111-31 précise que ses dispositions " ne sont applicables ni sur les foires, marchés, voies et places publiques, ni sur les aires de stationnement créées en application de la loi n° 2000-614 du 5 juillet 2000 relative à l'accueil et à l'habitat des gens du voyage ", ne sont, ainsi, pas applicables à l'installation des résidences mobiles qui, au sens de l'article 1er de la loi du 5 juillet 2000, constituent l'habitat permanent de gens du voyage. Par suite, les requérants ne sauraient utilement soutenir que ces dispositions réglementaires seraient contraires à l'articles 17 du pacte international relatif aux droits civils et politiques ainsi qu'aux articles 8 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et à l'article 1er de son premier protocole additionnel, faute de ménager la prise en considération des traditions spécifiques au mode de vie des gens du voyage en leur permettant notamment de résider à titre permanent dans des résidences mobiles sur un terrain dont ils sont propriétaires. <br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par le ministre, que M. A...et Mme C...ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent. Leur requête doit ainsi être rejetée, y compris leurs conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
				--------------<br/>
Article 1er : La requête de M. A...et de Mme C...est rejetée.<br/>
Article 2 : La présente décision sera notifiée M. D...A..., premier dénommé, et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-03 POLICE. POLICES SPÉCIALES. POLICE DES GENS DU VOYAGE. - RÉSIDENCES MOBILES CONSTITUANT L'HABITAT PERMANENT DE GENS DU VOYAGE (1ER AL. DU I DE L'ART. 1ER DE LA LOI DU 5 JUILLET 2000) - INSTALLATION DE CES RÉSIDENCES ENTIÈREMENT RÉGIE PAR DES DISPOSITIONS PARTICULIÈRES - EXISTENCE - CONSÉQUENCE - INAPPLICABILITÉ DES DISPOSITIONS RÉGISSANT L'INSTALLATION DES RÉSIDENCES MOBILES DE LOISIRS (ART. R. 111-42 DU CODE DE L'URBANISME) ET DES CARAVANES (ART. R. 111-49 DE CE CODE) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. AUTORISATIONS RELATIVES AU CAMPING, AU CARAVANING ET À L'HABITAT LÉGER DE LOISIR. - DISPOSITIONS RÉGISSANT L'INSTALLATION DES RÉSIDENCES MOBILES DE LOISIRS (ART. R. 111-42 DU CODE DE L'URBANISME) ET DES CARAVANES (ART. R. 111-49 DE CE CODE) - APPLICABILITÉ À L'INSTALLATION DES RÉSIDENCES MOBILES CONSTITUANT L'HABITAT PERMANENT DE GENS DU VOYAGE (1ER AL. DU I DE L'ART. 1ER DE LA LOI DU 5 JUILLET 2000) - ABSENCE, DÈS LORS QUE LEUR INSTALLATION EST ENTIÈREMENT RÉGIE PAR DES DISPOSITIONS PARTICULIÈRES.
</SCT>
<ANA ID="9A"> 49-05-03 Il résulte des articles L. 444-1 et R. 421-23 du code de l'urbanisme et 1er et 9 de la loi n° 2000-614 du 5 juillet 2000 que l'installation des résidences mobiles qui, au sens de l'article 1er de cette loi, constituent l'habitat permanent de gens du voyage, est entièrement régie par des dispositions particulières qui, notamment, précisent les conditions dans lesquelles ces résidences peuvent faire l'objet d'une installation sur le terrain de leur propriétaire ou en zone non constructible, de même que pour une durée supérieure à trois mois.... ...Les articles R. 111-42 du code de l'urbanisme, réglementant l'installation des résidences mobiles de loisirs, et R. 111-49 du même code, réglementant l'installation des caravanes, qui figurent d'ailleurs au sein d'une section dont l'article R. 111-31 précise que ses dispositions ne sont pas applicables sur les aires de stationnement créées en application de la loi du 5 juillet 2000, ne sont, ainsi, pas applicables à l'installation des résidences mobiles qui, au sens de l'article 1er de la loi du 5 juillet 2000, constituent l'habitat permanent de gens du voyage.</ANA>
<ANA ID="9B"> 68-04-04 Il résulte des articles L. 444-1 et R. 421-23 du code de l'urbanisme et 1er et 9 de la loi n° 2000-614 du 5 juillet 2000 que l'installation des résidences mobiles qui, au sens de l'article 1er de cette loi, constituent l'habitat permanent de gens du voyage, est entièrement régie par des dispositions particulières qui, notamment, précisent les conditions dans lesquelles ces résidences peuvent faire l'objet d'une installation sur le terrain de leur propriétaire ou en zone non constructible, de même que pour une durée supérieure à trois mois.... ...Les articles R. 111-42 du code de l'urbanisme, réglementant l'installation des résidences mobiles de loisirs, et R. 111-49 du même code, réglementant l'installation des caravanes, qui figurent d'ailleurs au sein d'une section dont l'article R. 111-31 précise que ses dispositions ne sont pas applicables sur les aires de stationnement créées en application de la loi du 5 juillet 2000, ne sont, ainsi, pas applicables à l'installation des résidences mobiles qui, au sens de l'article 1er de la loi du 5 juillet 2000, constituent l'habitat permanent de gens du voyage.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
