<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713428</ID>
<ANCIEN_ID>JG_L_2015_06_000000380627</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713428.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 08/06/2015, 380627, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380627</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380627.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              L'office public de l'habitat " Valophis Habitat - OPH du Val-de-Marne " a demandé au tribunal administratif de Melun de condamner l'Etat à lui verser la somme de 9 881,79 euros en réparation du préjudice ayant résulté pour lui du refus du préfet du Val-de-Marne de lui accorder le concours de la force publique pour l'exécution d'une décision de justice. Par un jugement n° 1106719/4 du 26 mars 2014, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mai et 21 août 2014 au secrétariat du contentieux du Conseil d'Etat, la société d'habitations à loyer modéré " Valophis Habitat - OPH du Val-de-Marne " demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code des procédures civiles d'exécution ; <br/>
<br/>
              - la loi n° 91-650 du 9 juillet 1991 ;<br/>
<br/>
              - le décret n° 92-755 du 31 juillet 1992;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à la SCP Piwnica, Molinié, avocat de l'office public de l'habitat " Valophis Habitat - OPH du Val-de-Marne " ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes des premier et quatrième alinéas de l'article 62 de la loi du 9 juillet 1991 portant réforme des procédures civiles d'exécution, applicables au litige porté devant le tribunal administratif de Melun et dont les dispositions sont désormais reprises aux articles L. 412-1 et L. 412-5 du code des procédures civiles d'exécution : " Si l'expulsion porte sur un local affecté à l'habitation principale de la personne expulsée ou de tout occupant de son chef, elle ne peut avoir lieu, sans préjudice des dispositions des articles L. 613-1 à L. 613-5 du code de la construction et de l'habitation, qu'à l'expiration d'un délai de deux mois qui suit le commandement (...) / Dès le commandement d'avoir à libérer les locaux à peine de suspension du délai avant l'expiration duquel l'expulsion ne peut avoir lieu, l'huissier de justice chargé de l'exécution de la mesure d'expulsion doit en informer le représentant de l'État dans le département en vue de la prise en compte de la demande de relogement de l'occupant dans le cadre du plan départemental d'action pour le relogement des personnes défavorisées prévu par la loi n° 90-449 du 31 mai 1990 visant à la mise en oeuvre du droit au logement " ; qu'aux termes du premier alinéa de l'article 197 du décret du 31 juillet 1992, alors en vigueur, dont les dispositions sont désormais reprises au deuxième alinéa de l'article R. 412-2 du code des procédures civiles d'exécution : " L'huissier de justice envoie au préfet du département du lieu de situation de l'immeuble, par lettre recommandée avec demande d'avis de réception, copie du commandement d'avoir à libérer les locaux " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le concours de la force publique ne peut être légalement accordé avant l'expiration d'un délai de deux mois à compter de la réception par le préfet du commandement d'avoir à quitter les lieux antérieurement signifié à l'occupant ; que le préfet saisi d'une demande de concours avant l'expiration de ce délai, qu'il doit mettre à profit pour tenter de trouver une solution de relogement de l'occupant, est légalement fondé à la rejeter, par une décision qui ne saurait engager la responsabilité de l'Etat, en raison de son caractère prématuré ; que, toutefois, lorsque, à la date d'expiration du délai, la demande n'a pas été rejetée pour ce motif par une décision expresse notifiée à l'huissier, le préfet doit être regardé comme valablement saisi à cette date ; qu'il dispose alors d'un délai de deux mois pour se prononcer sur la demande ; que son refus exprès, ou le refus implicite né à l'expiration de ce délai, est de nature à engager la responsabilité de l'Etat ; <br/>
<br/>
              3. Considérant qu'en l'absence de contestation sur ce point le juge saisi d'une demande d'indemnisation au titre d'un refus de concours de la force publique n'est pas tenu de procéder à une mesure d'instruction pour vérifier que le bénéficiaire du jugement d'expulsion a notifié au préfet le commandement de quitter les lieux et la date à laquelle il a accompli cette formalité ; que si le juge décide néanmoins de procéder à une telle mesure d'instruction, il lui appartient de lui donner un caractère contradictoire en l'adressant tant au requérant qu'au représentant de l'Etat ; que c'est seulement si ce dernier affirme que le commandement ne lui a pas été notifié ou qu'il l'a été moins de deux mois avant la réquisition de la force publique que l'absence de production par le propriétaire d'un justificatif apportant la preuve contraire permet au juge de retenir que la réquisition a été irrégulière ou prématurée et d'en tirer les conséquences en ce qui concerne l'engagement de la responsabilité de l'Etat ;<br/>
<br/>
              4. Considérant que, pour rejeter les conclusions présentées devant lui par l'office public de l'habitat " Valophis Habitat - OPH du Val-de-Marne ", tendant à ce que l'Etat soit condamné à réparer les conséquences dommageables du rejet par le préfet du Val-de-Marne de sa demande de concours de la force publique pour l'exécution d'un jugement ordonnant l'expulsion d'occupants d'un logement lui appartenant, sis 5 rue de Picardie à Chevilly-Larue, le tribunal administratif de Melun a estimé que cette demande ne pouvait être regardée comme ayant été régulièrement présentée dès lors qu'en dépit d'une mesure d'instruction qu'il avait diligentée la société requérante n'avaient pas versé au dossier le justificatif de la notification au préfet, préalablement à la réquisition de la force publique, d'une copie du commandement de quitter les lieux délivré aux occupants de son bien ; qu'il résulte de ce qui a été dit au point 3 qu'en fondant ainsi sa décision sur l'absence de production d'un justificatif de la notification de cette pièce, alors que le préfet, qui n'avait pas produit d'observations en défense, ne contestait pas que cette formalité avait été régulièrement accomplie, le tribunal administratif a entaché son jugement d'une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société d'habitations à loyer modéré " Valophis Habitat - OPH du Val-de-Marne ", au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 26 mars 2014 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Melun. <br/>
<br/>
Article 3 : L'Etat versera à l'office public de l'habitat " Valophis Habitat - OPH du Val-de-Marne " la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à l'office public de l'habitat " Valophis Habitat - OPH du Val-de-Marne " et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
