<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659284</ID>
<ANCIEN_ID>JG_L_2018_11_000000414541</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/92/CETATEXT000037659284.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/11/2018, 414541, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414541</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414541.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 septembre 2017 et 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, l'Instance de gestion spécifique du régime local agricole d'assurance-maladie d'Alsace et de Moselle demande au Conseil d'Etat d'annuler pour excès de pouvoir, d'une part, l'instruction adressée le 7 novembre 2016 par le directeur de la sécurité sociale au directeur général de la Caisse centrale de la mutualité sociale agricole (MSA) relative aux modalités de facturation du ticket modérateur applicables aux prestations d'hospitalisation des patients affiliés au régime local agricole et, d'autre part, la décision implicite de rejet de son recours gracieux du 30 novembre 2016.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2003-1199 du 18 décembre 2003;<br/>
              - le décret n° 2009-213 du 23 février 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'Instance de gestion spécifique du régime local agricole d'assurance maladie d'Alsace et de Moselle demande l'annulation pour excès de pouvoir de la note du 7 novembre 2016 par laquelle le directeur de la sécurité sociale a donné au directeur général de la Caisse centrale de la mutualité sociale agricole (MSA), par des dispositions impératives à caractère général, l'interprétation qu'il prescrivait d'adopter des dispositions relatives à la facturation du ticket modérateur applicable aux prestations d'hospitalisation des patients affiliés au régime local d'assurance maladie d'Alsace et de Moselle admis dans des établissements de santé publics ou à but non lucratif, en médecine, chirurgie ou obstétrique, en lui demandant de la diffuser auprès des organismes de la MSA. <br/>
<br/>
              2. D'une part, les articles L. 325-1 du code de la sécurité sociale et L. 761-3 du code rural et de la pêche maritime prévoient que, respectivement, le régime local d'assurance maladie complémentaire obligatoire des départements du Haut-Rhin, du Bas-Rhin et de la Moselle et le régime local agricole d'assurance maladie complémentaire obligatoire des départements du Bas-Rhin, du Haut-Rhin et de la Moselle servent à leurs bénéficiaires des prestations, en complément de celles du régime général ou du régime des assurances sociales agricoles, " pour couvrir tout ou partie de la participation laissée à la charge de l'assuré en application de l'article L. 160-13 " du code de la sécurité sociale et peuvent " prendre en charge tout ou partie du forfait journalier prévu à l'article L. 174-4 " du même code, ces prestations étant déterminées par le conseil d'administration de l'instance de gestion de ces régimes dans des conditions définies par décret. Ainsi que le prévoient l'article D. 325-6, le II de l'article D. 325-7 et l'article D. 325-8 du code de la sécurité sociale pour le premier de ces régimes, l'article D. 761-8 du code rural et de la pêche maritime dispose, s'agissant du régime local agricole, que : " L'instance de gestion spécifique peut prendre en charge, selon les taux qu'elle détermine et sous réserve des dispositions prévues aux quatrième et cinquième alinéas ci-dessous : / 1° La participation laissée à la charge de l'assuré en application de l'article L. 322-2 du code de la sécurité sociale ; / 2° Tout ou partie du forfait journalier mentionné à l'article L. 174-4 du code de la sécurité sociale pour tout ou partie du séjour hospitalier, selon des modalités qu'elle détermine. / (...) / Le conseil d'administration de l'instance de gestion spécifique peut instituer une participation de l'assuré aux frais d'hospitalisation. Cette participation est acquittée par l'assuré directement auprès de l'établissement. / En cas d'hospitalisation dans un établissement public ou privé conventionné, le montant des frais d'hospitalisation à la charge de l'instance de gestion spécifique est réglé directement à l'établissement de soins ". <br/>
<br/>
              3. D'autre part, le II de l'article 33 de la loi du 18 décembre 2003 de financement de la sécurité sociale pour 2004  prévoit que, jusqu'au 31 décembre 2019, dans les établissements de santé publics ou à but non lucratif,  " par exception aux 1° à 3° du I de l'article L. 162-22-10 du [code de la sécurité sociale], les tarifs nationaux des prestations des séjours ne servent pas de base au calcul de la participation du patient. Les conditions et modalités de la participation du patient aux tarifs des prestations mentionnées à l'article L. 162-22-6 du même code sont fixées par voie réglementaire. (...) ". Le décret du 23 février 2009 relatif aux objectifs des dépenses d'assurance maladie et portant diverses dispositions financières relatives aux établissements de santé prévoit, à ses articles 4 à 5-2, au titre des dispositions transitoires relatives aux établissements de santé soumis à la tarification à l'activité et antérieurement financés par dotation globale, une tarification de la participation du patient sur la base de tarifs journaliers de prestations fixés par le directeur général de chaque agence régionale de santé. <br/>
<br/>
              4. Il résulte des dispositions citées au point 2 que le régime local agricole d'assurance maladie d'Alsace et de Moselle, comme le régime local d'assurance maladie, prend en charge, selon les taux et la part déterminés par l'instance de gestion spécifique, la participation aux tarifs d'hospitalisation en principe laissée, dans le régime général et le régime des assurances sociales agricoles, à la charge des assurés sociaux, dès lors que ceux-ci n'en sont pas exonérés par les dispositions de l'article L. 160-14 du code de la sécurité sociale. Par suite, le montant de la prestation ainsi servie par le régime local agricole, comme par le régime local, doit nécessairement être calculé sur la base du montant de cette participation. En application des dispositions transitoires prévues par l'article 33 de la loi du 18 décembre 2003, qui dérogent sur ce point aux dispositions de l'article L. 162-22-10 du code de la sécurité sociale, cette participation est calculée sur la base de tarifs journaliers de prestations, propres à chaque établissement, et non sur la base des tarifs fixés au niveau national sous forme de forfaits de séjour et de soins, dénommés " groupes homogènes de séjour " (GHS), qui servent habituellement, dans les conditions alors prévues par les dispositions combinées des articles L. 162-22, L. 162-22-6 et  R. 162-29 du code de la sécurité sociale, à la prise en charge des activités de médecine, de chirurgie et d'obstétrique par les régimes obligatoires de sécurité sociale. Par suite, le directeur de la sécurité sociale a pu légalement, sans empiéter sur les compétences du conseil d'administration de l'instance de gestion spécifique prévues aux articles L. 761-10 et D. 761-7 du code rural et de la pêche maritime, rappeler au directeur général de la Caisse centrale de la mutualité sociale agricole que, conformément aux dispositions législatives et règlementaires applicables, les prestations servies par le régime local et par le régime local agricole d'assurance-maladie d'Alsace et de Moselle à leurs affiliés qui ne sont pas exonérés de toute participation aux tarifs d'hospitalisation, au titre de la participation à la charge de l'assuré, doivent être calculées sur la base des tarifs journaliers de prestations. Les dispositions du 2°) de l'article L. 174-3 du code de la sécurité sociale, relatives au calcul de la participation des assurés dans le cas où le régime d'assurance maladie dont ils relèvent comporte une disposition de cet ordre, concernent la fixation de la participation des assurés sociaux pour des activités de soins financées par une dotation annuelle de financement et non les activités de médecine, chirurgie ou obstétrique et, dès lors, la requérante ne peut utilement s'en prévaloir. Elle n'est, par voie de conséquence, pas fondée à soutenir que l'instruction attaquée aurait été prise par une autorité incompétente, serait dépourvue de base légale ou ferait une interprétation erronée des dispositions applicables du code de la sécurité sociale.<br/>
<br/>
              5. Dans ces conditions, la requérante n'est pas davantage fondée à soutenir que le directeur de la sécurité sociale aurait entaché son instruction d'une rétroactivité illégale, quand bien même il a entendu mettre un terme aux pratiques antérieures de facturation non conformes aux dispositions législatives et règlementaires applicables, en fixant au 1er juillet 2016 la date à compter de laquelle il demandait aux caisses de mutualité sociale agricole de se conformer aux règles applicables.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la requérante n'est pas fondée à demander l'annulation de l'instruction qu'elle attaque ni de la décision implicite par laquelle son recours gracieux a été rejeté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Instance de gestion spécifique du régime local agricole d'assurance maladie d'Alsace et de Moselle est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Instance de gestion spécifique du régime local agricole d'assurance maladie d'Alsace et de Moselle et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la Caisse centrale de la mutualité sociale agricole.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
