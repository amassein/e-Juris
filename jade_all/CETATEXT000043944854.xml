<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043944854</ID>
<ANCIEN_ID>JG_L_2021_08_000000455176</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/94/48/CETATEXT000043944854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/08/2021, 455176, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455176</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455176.20210813</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B... a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de l'arrêté du 25 juillet 2021 par lequel le préfet de Mayotte l'a obligée à quitter le territoire français sans délai, a fixé le pays de destination et a interdit son retour sur le territoire français et, d'autre part, d'enjoindre au préfet de Mayotte de lui délivrer un récépissé l'autorisant à travailler dans l'attente de l'instruction de sa demande de titre de séjour sous astreinte de 500 euros par jour de retard. Par une ordonnance n° 2102687 du 29 juillet 2021, le juge des référés du tribunal administratif de Mayotte a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 et 9 août 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de l'arrêté du 25 juillet 2021 en tant qu'il interdit son retour sur le territoire français ;<br/>
<br/>
              3°) d'enjoindre au préfet de Mayotte de la ramener à Mayotte aux frais de l'Etat sous astreinte de 1 000 euros par jour de retard et de lui délivrer un récépissé l'autorisant à travailler sous astreinte de 500 euros par jour de retard, ces astreintes devant être liquidées tous les sept jours sans autre formalité ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête n'est pas devenue sans objet du fait de l'exécution de l'arrêté litigieux dès lors que cet arrêté est assorti d'une interdiction de retour sur le territoire français pendant un an ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, la décision litigieuse est un arrêté de reconduite à la frontière qui a donné lieu à un placement en rétention d'une durée de cinq jours et est assortie d'une interdiction de retour sur le territoire français pendant un an, d'autre part, le préfet n'a pas statué sur la demande de titre de séjour qu'elle a introduite et dont il a accusé réception le 15 juin 2021 et, enfin, elle est éloignée de sa mère vivant sur Mayotte qui est handicapée et a besoin d'assistance ; <br/>
              - l'ordonnance attaquée est irrégulière dès lors que, d'une part, alors que son avocat avait fait savoir qu'il ne se rendrait pas à l'audience et qu'il s'en remettait à ses écritures , qu'il a reçu le mémoire en défense de l'administration après le début de l'audience et a néanmoins produit un mémoire, le juge des référés n'a pas rouvert l'instruction mais a pris en compte ses observations à l'audience qui venaient en contradiction avec les écritures de son avocat et, d'autre part, que la décision n'indique pas que la requérante ait bénéficié de l'assistance d'un interprète en méconnaissance de l'article R. 776-23 du code de justice administrative et, enfin, qu'elle est insuffisamment motivée, le juge des référés installé à la Réunion n'ayant pas pris en compte les réalités locales ;<br/>
              - l'arrêté litigieux porte une atteinte grave et manifestement illégale au droit au respect de sa vie privée et familiale dès lors, d'une part, qu'elle justifie d'attaches à Mayotte et, d'autre part, qu'elle est éloignée de sa mère handicapée avec laquelle elle vit depuis sept ans. <br/>
<br/>
              Par un mémoire en défense, enregistré le 9 août 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Ont été entendus lors de l'audience publique du 10 août 2021, à 10 heures 30 : <br/>
<br/>
              - Me Ricard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme A... B... ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ;  <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ".<br/>
<br/>
              2. Il résulte de l'instruction que Mme A... B..., ressortissante comorienne née le 13 juillet 1976, a fait l'objet d'un arrêté du préfet de Mayotte du 25 juillet 2021 lui faisant obligation de quitter le territoire français sans délai, fixant le pays de destination et prévoyant une interdiction de retour sur le territoire français pendant une durée d'un an. Elle a été placée en centre de rétention administrative par un arrêté du même jour. Mme A... B... a demandé au juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de cet arrêté et d'enjoindre au préfet de lui délivrer un récépissé l'autorisant à travailler dans l'attente de l'instruction de sa demande de titre de séjour. Par une ordonnance du 29 juillet 2021, le juge des référés a rejeté sa demande. Mme A... B..., qui a été éloignée en destination de l'Union des Comores, relève appel de cette ordonnance.<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              3. Aux termes de l'article L. 5 du code de justice administrative : " L'instruction des affaires est contradictoire. Les exigences de la contradiction sont adaptées à celles de l'urgence (...) ". Aux termes de l'article L. 522-1 du même code : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale / Lorsqu'il lui est demandé de prononcer les mesures visées aux articles L. 521-1 et L. 521-2, de les modifier ou d'y mettre fin, il informe sans délai les parties de la date et de l'heure de l'audience publique ".<br/>
<br/>
              4. En premier lieu, compte tenu des impératifs de l'urgence, la requérante n'est pas fondée à soutenir que le juge des référés aurait méconnu le caractère contradictoire de la procédure en communiquant à son avocat le mémoire en défense produit par le préfet juste avant le début de l'audience de référé.<br/>
<br/>
              5. En deuxième lieu, il résulte de l'instruction que l'avocat de Mme A... B..., avocat au barreau de Marseille, n'a pas assisté à l'audience publique qui s'est tenue à Mamoudzou le 29 juillet 2021 à 9 heures. Cette absence ne privait pas le juge des référés de la faculté de solliciter les observations de Mme A... B... lors de l'audience publique dans le cadre de la procédure contradictoire orale prévue à l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              6. En troisième lieu, aux termes de l'article R. 776-23 du code de justice administrative : " Dans le cas où l'étranger, qui ne parle pas suffisamment la langue française, le demande, le président nomme un interprète qui doit prêter serment d'apporter son concours à la justice en son honneur et en sa conscience. Cette demande peut être formulée dès le dépôt de la requête introductive d'instance. Lors de l'enregistrement de la requête, le greffe informe au besoin l'intéressé de la possibilité de présenter une telle demande (...) "<br/>
<br/>
              7. La requérante ne saurait utilement soutenir que le juge des référés aurait méconnu les dispositions de l'article R. 776-23 du code de justice administrative relatives à l'assistance d'un interprète, ces dispositions n'étant pas applicables à une demande introduite sur le fondement de l'article L. 521-2 du code de justice administrative. Au demeurant, il résulte de l'ordonnance attaquée que Mme A... B... a pu répondre aux sollicitations du juge des référés et donner des indications sur sa situation familiale. <br/>
<br/>
              8. En quatrième lieu, le juge des référés a pu valablement siégé au tribunal administratif de Saint-Denis de la Réunion en étant relié à la salle d'audience de Mamoudzou par un moyen de communication audiovisuelle, en application des dispositions L. 781-1 et R. 781-1 et suivants du code de justice administrative.<br/>
<br/>
              9. En dernier lieu, le juge des référés a suffisamment motivé son ordonnance au regard de l'argumentation dont il était saisi.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              10. Pour rejeter la demande de Mme A... B..., le juge des référés a estimé que les éléments produits par la requérante n'établissaient pas l'ancienneté, la continuité et la stabilité de son séjour à Mayotte et qu'elle ne justifiait pas que l'état de santé de sa mère lourdement handicapée exigerait sa présence à ses côtés ou qu'elle serait la seule à même de lui porter assistance alors qu'elle avait indiqué à l'audience que sa mère n'était pas isolée et qu'elle vivait avec son beau-père. <br/>
<br/>
              11. En appel, Mme A... B... reprend les éléments produits en première instance pour établir l'ancienneté de son séjour à Mayotte et fait valoir qu'elle est l'unique soutien de sa mère lourdement handicapée en produisant une attestation de son beau-père indiquant qu'il n'a plus de vie commune avec la mère de Mme A... B.... Ces éléments ne sont toutefois pas de nature à infirmer l'appréciation portée par le juge des référés du tribunal administratif de Mayotte au vu des indications apportées par Mme A... B... à l'audience publique devant le juge des référés.<br/>
<br/>
              12. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur le surplus des conclusions, que la requête de doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
