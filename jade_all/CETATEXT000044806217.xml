<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806217</ID>
<ANCIEN_ID>JG_L_2021_12_000000448694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/12/2021, 448694, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448694.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F... D... a demandé au tribunal administratif de Montpellier d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Canet-en-Roussillon (Pyrénées orientales) pour l'élection des conseillers municipaux et communautaires. Par un jugement n° 2001455 du 15 décembre 2020, le tribunal administratif a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 14 janvier et 17 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour la désignation des conseillers municipaux et communautaires de la commune de Canet-en-Roussillon ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-760 du 22 juin 2020 ;<br/>
              - la décision du Conseil constitutionnel n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Carine Chevrier, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 pour le renouvellement des conseillers municipaux et communautaires de la commune de Canet-en-Roussillon (Pyrénées-Orientales), commune de plus de 1 000 habitants, les trente-trois sièges de conseillers municipaux et les quatre sièges de conseillers communautaires ont été pourvus. Vingt-neuf des sièges de conseillers municipaux et les quatre sièges de conseillers communautaires ont été attribués à des candidats de la liste " Canet ensemble 2020 ", conduite par M. A..., qui a obtenu la majorité absolue avec 3 562 voix, soit 67,37 % des suffrages exprimés, deux autres sièges de conseillers municipaux ont été attribués à des candidats de la liste " Canet plus en Roussillon ", conduite par M. D..., qui a obtenu 812 voix ; soit 15,35 % des suffrages exprimés, un siège de conseiller municipal ayant été attribué à chacune des deux autres listes présentes ayant obtenu, respectivement, 470 voix, soit 8,88 % des suffrages exprimés, et 443 voix, soit 8,37 % des suffrages exprimés. M. D... fait appel du jugement du 15 décembre 2020 par lequel le tribunal administratif de Montpelier a rejeté la protestation qu'il a formée contre ces opérations électorales. <br/>
<br/>
              Sur la régularité des opérations électorales :<br/>
<br/>
              2. En premier lieu, l'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              3. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              4. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ".<br/>
<br/>
              5. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est, ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              6. En l'espèce, M. D... fait valoir que le taux d'abstention a augmenté fortement par rapport aux précédentes élections municipales et que les annonces du Gouvernement le samedi soir précédant le scrutin imposant la fermeture des établissements scolaires et l'arrêt de certaines activités économiques en vue de limiter la propagation du virus SARS-CoV-2 au sein de la population, ont conduit certains électeurs à ne pas aller voter, alors qu'ils le souhaitaient, en particulier les plus âgés et les plus vulnérables d'entre eux et ont méconnu l'universalité du vote et rompu l'égalité entre les électeurs. Toutefois, il ne résulte pas de l'instruction ni que l'abstention aurait été plus forte chez les électeurs les plus âgés de la commune, ni, en tout état de cause, que cette abstention aurait été particulièrement préjudiciable aux candidats de la liste conduite par M. D.... Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin. <br/>
<br/>
              7. En second lieu, le grief tiré d'une atteinte à la liberté d'expression des candidats, garantie par les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce que M. D... n'avait pu s'exprimer après les annonces du Premier ministre en date du 14 mars 2020, alors même qu'aux termes de l'article L. 49 du code électoral, il n'était plus autorisé à le faire, n'est pas assorti des précisions permettant d'en apprécier le bien-fondé et doit, dès lors, être écarté.<br/>
<br/>
              Sur l'inéligibilité de M. A... :  <br/>
<br/>
              8. Aux termes de l'article L. 231 du code électoral : " Les agents salariés communaux ne peuvent être élus au conseil municipal de la commune qui les emploie ".<br/>
<br/>
              9. Il résulte de l'instruction que, par un arrêté du 18 décembre 2019, qui a été transmis en préfecture le 29 janvier suivant, M. A..., qui occupait alors la fonction de directeur de cabinet du maire de Canet-en-Roussillon, a été muté à sa demande dans le grade d'attaché principal territorial au sein de la commune de Pollestres à compter du 1er février 2020, et que, par un arrêté du 9 janvier 2020, transmis au centre de gestion de la fonction publique territoriale et au comptable public le 29 janvier suivant, le maire de Pollestres a prononcé sa mise en disponibilité pour convenances personnelles et pour une période de quatre mois à compter de cette même date. Dès lors qu'il ne résulte pas de l'instruction que M. A... aurait continué à exercer de fait ses fonctions de directeur de cabinet du maire de la commune de Canet-en-Roussillon au-delà de cette date, M. A... ne peut être regardé comme ayant, à la date du scrutin, la qualité d'agent salarié de la commune de Canet-en-Roussillon. Il ne résulte pas davantage de l'instruction que la mutation de M. A... auprès de la commune de Pollestres et sa mise en disponibilité dans la foulée serait constitutif d'une manœuvre.<br/>
<br/>
              Sur la campagne et la propagande électorales :<br/>
<br/>
              10. En premier lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". Aux termes de l'article L. 48-1 du code électoral : " Les interdictions et restrictions prévues par le présent code en matière de propagande électorale sont applicables à tout message ayant le caractère de propagande électorale diffusé par tout moyen de communication au public par voie électronique. "<br/>
<br/>
              11. D'une part, M. D... soutient que M. A... s'est livré à de la propagande électorale au sens des dispositions précitées en utilisant le bilan, le programme politique et le nom du maire sortant, en apparaissant à ses côtés notamment sur la page " Facebook " de la commune de Canet-en-Roussillon sur laquelle une vidéo relative au bilan des réalisations de l'équipe municipale sortante au cours de l'année 2019 a été diffusée, en distribuant un document intitulé " Canet 2020 ensemble. Stéphane A... pour l'avenir " dans lequel il fait la promotion des actions réalisées par le maire sortant, dont il avait été le directeur de cabinet, sur la période 2014-2020 sur 11 des 23 pages du programme. Il résulte de l'instruction que la vidéo relative au bilan de la commune postée sur le site Facebook, qui constitue un bilan du mandat établi par l'équipe sortante, n'établit pas de lien avec la candidature de M. A... et que le document intitulé " Canet 2020 ensemble. Stéphane A... pour l'avenir " est un document électoral réalisé par la liste de M. A... pour les besoins de sa propre campagne et dont le financement a été imputé sur son compte de campagne. Dès lors, outre que le grief tiré de l'apparition du candidat sur le site Facebook de la commune n'est pas assorti d'éléments permettant d'en apprécier le bien-fondé, ni la vidéo relative au bilan présente sur la page " Facebook " de la commune, ni le programme électoral en litige ne constituent une campagne de promotion publicitaire des réalisations de la collectivité prohibée par les dispositions précitées du 2èmealinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              12. D'autre part, s'il est soutenu que la consultation de la page " Facebook " de la commune de Canet-en-Roussillon conduisait à ce que soit affiché un lien vers la page de campagne de M. A..., lien dont il est soutenu qu'aucune démarche n'a été engagée pour l'interrompre, il ne résulte en tout état de cause pas de l'instruction que ce lien, dont il est constant qu'il était automatiquement généré par l'algorithme de l'application de ce réseau social, serait imputable au candidat ou à ses soutiens. <br/>
<br/>
              13. En deuxième lieu, aux termes du 3ème alinéa de l'article L. 52-8 du même code : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. ". La seule circonstance que M. A..., alors directeur de cabinet du maire, ait assisté au repas des anciens organisé par le centre communal d'action sociale le 10 janvier 2020 et qu'il ait également participé aux cérémonies de commémoration de l'appel du 18 juin aux côtés des élus de la majorité municipale sortante en 2019, période pendant laquelle il était encore directeur de cabinet du maire ne peut être regardée comme un avantage consenti par une personne morale au sens des dispositions précitées de l'article L. 52-8 du code électoral.<br/>
<br/>
              14. En troisième lieu, aux termes de l'article L. 50 du code électoral : " Il est interdit à tout agent de l'autorité publique ou municipale de distribuer des bulletins de vote, professions de foi et circulaires des candidats. " Si M. D... fait valoir que des agents communaux ont distribué des documents de propagande en faveur de M. A..., il ne produit aucun élément à l'appui de ses allégations, de sorte que le grief tiré de la méconnaissance de l'article L. 50 du code électoral ne peut qu'être écarté. <br/>
<br/>
              15. En dernier lieu, il résulte en tout état de cause de l'instruction que l'occupation temporaire du domaine public pour la tenue d'un buffet organisé par M. A... devant le siège de sa campagne avait été autorisée.<br/>
<br/>
              16. Il résulte de tout ce qui précède que M. D... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a rejeté sa protestation. Par suite, sa requête doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A... au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 :  Les conclusions présentées par M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. F... D..., à M. E... A..., premier dénommé, pour l'ensemble des autres défendeurs, et au ministre de l'intérieur.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 20 décembre 2021 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; Mme Suzanne von Coester, conseillère d'Etat et Mme Carine Chevrier, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
 		La rapporteure : <br/>
      Signé : Mme Carine Chevrier<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
