<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042573975</ID>
<ANCIEN_ID>JG_L_2020_11_000000446440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/39/CETATEXT000042573975.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 20/11/2020, 446440, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446440.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'article 37 du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la requête est recevable ; <br/>
              -  la condition d'urgence est remplie dès lors que la disposition contestée le prive de ses revenus et le met dans une situation financière difficile ; <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'entreprendre, à la libre concurrence et à la liberté de commerce et de l'industrie ; <br/>
              - la disposition contestée est entachée d'incompétence dès lors qu'elle méconnaît l'article 3-1.b) du traité de fonctionnement de l'Union européenne ; <br/>
              - elle est entachée d'un défaut de motivation dès lors que, d'une part, les motifs ayant conduit à la liste des activités autorisées ne sont pas précisés et, d'autre part, le conseil scientifique, dans une note du 22 septembre 2020, envisageait seulement un confinement territorial ; <br/>
              - elle est entachée d'une erreur manifeste d'appréciation dès lors qu'aucune contamination n'est à recenser dans les salons de tatouage eu égard au respect de règles d'hygiène ; <br/>
              - elle méconnaît le principe d'égalité de traitement dès lors que d'autres structures commerciales, dans lesquelles les risques sanitaires sont supérieurs à ceux présents dans les salons de tatouage, restent ouvertes.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment, son préambule ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique " prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, activités et réunions, notamment " Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé (...) " à condition d'être " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ".<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence. <br/>
<br/>
              4. Une nouvelle progression de l'épidémie au cours des mois de septembre et d'octobre, dont le rythme n'a cessé de s'accélérer au cours de cette période, a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret contesté, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              5. Il résulte des données scientifiques publiées que la circulation du virus sur le territoire métropolitain s'est fortement amplifiée au cours des dernières semaines, malgré les mesures prises, conduisant à une situation particulièrement dangereuse pour la santé de l'ensemble de la population française. Ainsi, à la date du 11 novembre 2020, plus de 1 860 000 cas ont été confirmés positifs à la covid-19, en augmentation de près de 35 000 dans les dernières vingt-quatre heures, le taux d'incidence national étant de 428 cas pour 100 000 habitants contre 246 au 20 octobre et 118 au 28 septembre, le taux de positivité des tests réalisés étant de 19,5 % au 11 novembre contre 13,2 % au 18 octobre et 9 % au 28 septembre, 42 435 décès de la covid-19 sont à déplorer au 11 novembre 2020, en hausse de 441 cas en vingt-quatre heures. Enfin, le taux d'occupation des lits en réanimation par des patients atteints de la covid-19 est passé de 43 % au 20 octobre à près de 70 % au 1er novembre et à près de 95 % au 11 novembre, mettant sous tension l'ensemble du système de santé et rendant nécessaire, au cours des derniers jours, des transferts de patients entre régions et avec des pays voisins ainsi que des déprogrammations d'hospitalisations non urgentes.<br/>
<br/>
              6. Ainsi, pour faire face à cette situation d'urgence sanitaire, le gouvernement, en prenant les mesures détaillées par le décret du 29 octobre 2020, a fait le choix d'une politique qui cherche à casser la dynamique actuelle de progression du virus par la stricte limitation des déplacements de personnes hors de leur domicile. A cette fin, il a, à l'article 4 du décret, interdit tout déplacement des personnes hors de leur lieu de résidence et fixé une liste limitative des exceptions à cette interdiction. De même, par les articles 37 et suivants, il a procédé à la fermeture générale des restaurants et débits de boisson et a autorisé, s'agissant des magasins de vente, l'ouverture au public pour la vente de produits de première nécessité, tout en maintenant la possibilité, pour les autres produits, de recourir à la vente à distance avec livraison à domicile ou retrait de commandes.<br/>
<br/>
              7. En premier lieu, le législateur a par la loi n° 2020-290 du 23 mars 2020, créant l'article L. 3131-15 du code de la santé publique, donné compétence au Premier ministre, en période d'état d'urgence sanitaire, pour adopter par décret des mesures nécessaires à la préservation de la santé publique et à la lutte contre l'épidémie de covid-19. Il en résulte que les moyens tirés de l'incompétence du Premier ministre et du défaut de motivation dont serait entaché le décret attaqué doivent être écartés.<br/>
<br/>
              8. En deuxième lieu, M. B... soutient que la fermeture des salons de tatouage est disproportionnée. Toutefois, il résulte de ce qui a été dit aux points 5 et 6 que cette fermeture procède du choix de limiter la propagation du virus par le maintien aussi strict que possible des personnes à leur domicile. Cette mesure, qui devra faire l'objet d'une évaluation, s'accompagne en outre, à destination des entreprises concernées dont l'activité sera fortement réduite, d'un dispositif d'aides visant à réduire les charges qu'elles supportent. Dans ces conditions, eu égard à l'aggravation rapide au cours des dernières semaines de la propagation de l'épidémie sur l'ensemble du territoire, à la nécessité de casser cette propagation afin de préserver les structures hospitalières et globalement le système santé ainsi qu'aux exceptions à l'interdiction générale prévue par le décret du 29 octobre 2020, les moyens tirés de ce qu'il serait porté une atteinte grave et manifestement illégale à la liberté d'entreprendre, à la libre concurrence et à la liberté de commerce et d'industrie doivent être écartés.<br/>
<br/>
              8. En troisième lieu, il ne résulte pas de l'instruction, eu égard aux éléments qui viennent d'être rappelés, que la mesure en cause serait constitutive d'une discrimination portant une atteinte grave et manifestement illégale à une liberté fondamentale dès lors qu'elle s'applique aux activités de même nature que celles exercées par la société requérante et que les commerces demeurant ouverts ne proposent pas les mêmes prestations.<br/>
<br/>
              10. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de M. B... doit être rejetée par application de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
