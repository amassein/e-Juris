<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043456930</ID>
<ANCIEN_ID>JG_L_2021_04_000000431369</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/45/69/CETATEXT000043456930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/04/2021, 431369, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431369</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:431369.20210427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Strasbourg d'annuler la décision référencée " 48 M " du 27 juillet 2018 par laquelle le ministre de l'intérieur a retiré six points de son permis de conduire à la suite d'une infraction commise le 2 août 2016, ainsi que la décision implicite rejetant son recours gracieux. Par un jugement n° 1900266 du 22 mars 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 19NC01545 du 4 juin 2019, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 21 mai 2019 au greffe de cette cour, présentée par M. B.... Par ce pourvoi, et par des observations complémentaires enregistrées le 25 novembre 2019, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats appartenant à l'Union européenne et à l'Espace économique européen ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision référencée " 48 M " du 27 juillet 2018, le ministre de l'intérieur a, en application de l'article 234-1 du code de la route, procédé au retrait de six points du permis de conduire de M. B..., titulaire d'un permis de conduire allemand, à la suite de sa condamnation par un jugement du 5 janvier 2017 du tribunal de grande instance de Strasbourg, devenu définitif le 19 février 2017, pour avoir conduit le 2 août 2016 sous l'empire d'un état alcoolique. M. B... a demandé au tribunal administratif de Strasbourg d'annuler cette décision et la décision implicite de rejet née du silence gardé par le ministre de l'intérieur sur le recours gracieux qu'il lui a adressé le 19 septembre 2018. Il se pourvoit en cassation contre le jugement du 22 mars 2019 par lequel le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 222-1 du code de la route dans sa rédaction applicable à la date de la décision litigieuse : " Tout permis de conduire national régulièrement délivré par un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, est reconnu en France sous réserve d'être en cours de validité./ (...) Tout titulaire d'un des permis de conduire considérés aux deux alinéas précédents, qui établit sa résidence normale en France, peut le faire enregistrer par le préfet du département de sa résidence selon les modalités définies par arrêté du ministre chargé de la sécurité routière, après avis du ministre chargé des affaires étrangères ". Aux termes de l'article R. 222-2 du même code : " Toute personne ayant sa résidence normale en France, titulaire d'un permis de conduire national délivré par un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, en cours de validité dans cet Etat, peut, sans qu'elle soit tenue de subir les examens prévus au premier alinéa de l'article D. 221-3, l'échanger contre le permis de conduire français selon les modalités définies par arrêté du ministre chargé de la sécurité routière, après avis du ministre de la justice et du ministre chargé des affaires étrangères./ L'échange d'un tel permis de conduire contre le permis français est obligatoire lorsque son titulaire a commis, sur le territoire français, une infraction au présent code ayant entraîné une mesure de restriction, de suspension, de retrait du droit de conduire ou de retrait de points. Cet échange doit être effectué selon les modalités définies par l'arrêté prévu à l'alinéa précédent, aux fins d'appliquer les mesures précitées./ Le fait de ne pas effectuer l'échange de son permis de conduire dans le cas prévu à l'alinéa précédent est puni de l'amende prévue pour les contraventions de la quatrième classe ". Aux termes de l'article 4 de l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats appartenant à l'Union européenne et à l'Espace économique européen : " 4.1. Les titulaires d'un permis de conduire obtenu dans un Etat membre de l'Union européenne ou dans un autre Etat partie à l'accord sur l'Espace économique européen, ayant fixé leur résidence normale sur le territoire français, peuvent demander l'échange de leur permis de conduire contre un permis français équivalent./ (...) 4.2. L'échange d'un tel permis contre un permis de conduire français est obligatoirement effectué si le conducteur a commis, sur le territoire français, une infraction ayant entraîné une mesure de restriction, de suspension, de retrait, d'annulation du droit de conduire, ou une infraction devenue définitive au sens de l'article L. 223-1 et entraînant de plein droit le retrait de points. (...) ".  Il résulte de la combinaison de ces dispositions que si le titulaire d'un permis de conduire délivré par l'un des Etats membres de l'Union européenne ou de l'Espace économique européen n'est, en principe, pas tenu de procéder à l'échange de ce permis pour conduire en France, cet échange devient, en revanche, obligatoire si, ayant sa résidence normale en France, il a commis sur le territoire national une infraction ayant entraîné une mesure de restriction, de suspension, de retrait ou d'annulation du droit de conduire ou de retrait de points. Lorsque le titulaire d'un tel permis n'a pas procédé à l'échange auquel il était tenu, l'administration est fondée à le regarder comme étant exclusivement titulaire d'un permis français et à appliquer sur ce permis les mesures qu'appelle l'infraction commise et, le cas échéant, les mesures ultérieurement applicables.<br/>
<br/>
              3. D'autre part, aux termes des troisième et quatrième alinéas de l'article R. 221-1 du code de route : II. - Toute personne sollicitant un permis de conduire, national ou international, doit justifier de sa résidence normale ainsi que, le cas échéant, de son droit au séjour en France ou, pour les élèves et étudiants étrangers titulaires d'un titre de séjour ou d'un visa long séjour valant titre de séjour validé par l'office français de l'immigration et de l'intégration correspondant à leur statut, de la poursuite de leurs études en France depuis au moins six mois en France à la date de leur demande de permis de conduire./ III.-On entend par résidence normale le lieu où une personne demeure habituellement, c'est-à-dire pendant au moins 185 jours par année civile, en raison d'attaches personnelles et professionnelles, ou, dans le cas d'une personne sans attaches professionnelles, en raison d'attaches personnelles révélant des liens étroits entre elle-même et l'endroit où elle demeure./ (...) ".<br/>
<br/>
              4. En premier lieu, en estimant que M. B... devait être regardé, en application des dispositions citées ci-dessus, comme ayant sa résidence normale en France, au motif notamment qu'il avait lors de l'interception de son véhicule le 2 août 2016 par la gendarmerie nationale, déclaré résider dans la commune de Lobsann (Bas-Rhin), qu'il avait accusé réception de la décision contestée à l'adresse déclarée dans cette commune et qu'il ne justifiait pas de manière probante la résidence normale qu'il soutenait avoir en Allemagne, le tribunal administratif de Strasbourg, qui s'est livré à une appréciation souveraine exempte de dénaturation, n'a entaché son jugement d'aucune erreur de droit.<br/>
<br/>
              5. En deuxième lieu, en regardant M. B..., en vertu des règles énoncées ci-dessus, comme exclusivement titulaire d'un permis français auquel pouvait être appliquées les mesures qu'appelait l'infraction délictuelle commise, le tribunal administratif, qui n'avait dès lors pas à rechercher si l'intéressé avait été préalablement mis en mesure de procéder à l'échange de son permis de conduire allemand, n'a pas davantage entaché son jugement d'erreur de droit.<br/>
<br/>
              6. Enfin, il résulte des termes mêmes de l'article 32 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, applicable à l'enregistrement des informations relatives aux permis de conduire prévu par l'article L. 225-1 du code de la route, que ses dispositions régissent la communication des informations qu'elles énumèrent à la personne auprès de laquelle sont recueillies des données à caractère personnel. En jugeant que l'obligation de communication à la personne concernée des informations prévues par l'article 32 de la loi du 6 janvier 1978 s'applique lors de la collecte de ces informations et en écartant pour ce motif comme inopérant le moyen tiré de ce que la décision " 48 M " du 27 juillet 2018 ne comportait pas l'ensemble de ces informations, le tribunal administratif de Strasbourg n'a, dès lors, pas commis d'erreur de droit. Au demeurant il ressortait du verso de l'avis de rétention du permis de conduire de M. B... établi le 2 août 2016 par la gendarmerie nationale, et figurant au dossier qui était soumis au juge du fond, que l'intéressé avait été informé de l'enregistrement de ses données à caractère personnel dans le traitement automatisé des informations relatives au permis de conduire et de la possibilité, en application des dispositions des articles L. 225-3 et R. 225-6 du code de la route, de consulter le relevé d'information intégral de son permis de conduire auprès de l'autorité préfectorale,.<br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
