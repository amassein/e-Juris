<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528090</ID>
<ANCIEN_ID>JG_L_2016_05_000000391157</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528090.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 11/05/2016, 391157, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391157</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391157.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              - de suspendre l'exécution de la décision du 30 décembre 2014 par laquelle le président du conseil de Paris a suspendu le versement de son allocation de revenu de solidarité active ;<br/>
              - d'enjoindre au président du conseil de Paris de rétablir son droit au revenu de solidarité active à compter du 1er janvier 2015, sous astreinte de 50 euros par jour.<br/>
<br/>
              Par une ordonnance n° 1507531 du 3 juin 2015, le juge des référés du tribunal administratif de Paris a suspendu l'exécution de la décision du 30 décembre 2014 jusqu'à l'intervention d'une décision explicite statuant sur le recours préalable formé par Mme A...le 23 janvier 2015, enjoint au président du conseil de Paris de lui payer à titre provisoire le revenu de solidarité active au titre des mois de janvier, février et mars 2015 dans un délai d'une semaine à compter de la notification de son ordonnance et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juin et 3 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le département de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de Paris du 3 juin 2015 ;<br/>
<br/>
              2°) statuant en référé, de dire n'y avoir plus lieu de statuer sur la demande de suspension formée par Mme A...ou, subsidiairement, de la rejeter.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2000-221 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par une décision du 30 décembre 2014, le président du conseil de Paris a suspendu le versement de l'allocation de revenu de solidarité active dont bénéficiait MmeA.... La reprise de ce versement a été décidée le 3 avril 2015 à compter du 1er avril 2015 seulement, à la suite de la validation du contrat d'engagements réciproques signé par l'intéressée. Par une requête enregistrée au greffe du tribunal administratif de Paris le 7 mai 2015, Mme A...a, dans le but d'obtenir le versement de cette allocation au titre des mois de janvier, février et mars 2015, demandé au juge des référés de ce tribunal de suspendre l'exécution de la décision du 30 décembre 2014 et d'enjoindre au président du conseil de Paris de rétablir son droit au revenu de solidarité active à compter du 1er janvier 2015. Par une ordonnance du 3 juin 2015, contre laquelle le département de Paris se pourvoit en cassation, le juge des référés a, d'une part, prononcé la suspension demandée jusqu'à l'intervention d'une décision explicite du président du conseil de Paris statuant sur le recours administratif préalable obligatoire formé par Mme A... le 23 janvier 2015 et, d'autre part, enjoint au président du conseil de Paris de verser à titre provisoire à Mme A...le revenu de solidarité active au titre des trois mois en question.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 262-47 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général (...) ".<br/>
<br/>
              3. L'objet même du référé organisé par les dispositions de l'article L. 521-1 du code de justice administrative est de permettre, dans tous les cas où l'urgence le justifie, la suspension dans les meilleurs délais d'une décision administrative contestée par le demandeur. Une telle possibilité est ouverte y compris dans le cas où un texte législatif ou réglementaire impose l'exercice d'un recours administratif préalable avant de saisir le juge, sans donner un caractère suspensif à ce recours obligatoire. Dans une telle hypothèse, la suspension peut être demandée au juge des référés sans attendre que l'administration ait statué sur le recours préalable, dès lors que l'intéressé a justifié, en produisant une copie de ce recours, qu'il a engagé les démarches nécessaires auprès de l'administration pour obtenir l'annulation ou la réformation de la décision contestée. En revanche, lorsqu'intervient une décision implicite ou explicite de rejet du recours administratif préalable obligatoire, qui se substitue à la décision initiale, il appartient au requérant de présenter de nouvelles conclusions tendant à sa suspension et d'introduire une requête tendant à son annulation ou à sa réformation. A défaut, sa demande de suspension doit être rejetée comme irrecevable.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés que, pour établir avoir engagé les démarches nécessaires pour obtenir l'annulation de la décision du 30 décembre 2014, Mme A...a produit la copie du recours préalable qu'elle a formé le 23 janvier 2015 auprès du président du conseil de Paris. Dès lors, en l'absence de décision explicite intervenue avant cette date, une décision implicite de rejet est née, conformément aux dispositions du second alinéa de l'article R. 262-89 du code de l'action sociale et des familles, le 23 mars 2015, qui s'est substituée à celle du 30 décembre 2014.  Mme A...ayant demandé, par une requête enregistrée au greffe du tribunal le 7 mai 2015, la suspension de la seule décision du 30 décembre 2014, au surplus sans avoir formé aucune requête en annulation ou en réformation de la décision qui s'y était substituée, sa demande était ainsi irrecevable. Dès lors, le juge des référés a commis une erreur de droit en y faisant droit. <br/>
<br/>
              5. Il résulte de ce qui précède que le département de Paris est fondé à demander l'annulation de l'ordonnance qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Le département de Paris ne peut utilement invoquer la décision qu'il a prise le 6 mai 2015 pour soutenir que la demande de Mme A...aurait perdu son objet postérieurement à son introduction devant le juge de référés du tribunal administratif de Paris, le 7 mai suivant.<br/>
<br/>
              8. En revanche, il résulte de ce qui a été dit ci-dessus que sa demande de suspension est irrecevable. Elle doit, par suite, être rejetée. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 3 juin 2015 est annulée.<br/>
Article 2 : La demande présentée par Mme A...devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : La présente décision sera notifiée au département de Paris et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
