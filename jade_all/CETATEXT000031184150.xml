<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031184150</ID>
<ANCIEN_ID>JG_L_2015_09_000000384523</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/18/41/CETATEXT000031184150.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 18/09/2015, 384523, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384523</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384523.20150918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Avena BTP a notamment demandé au tribunal administratif de Nice de condamner le centre hospitalier de Grasse à lui verser la somme de 87 350,84 euros TTC au titre du solde d'un marché public de travaux. <br/>
<br/>
              Par un jugement n° 1001527 du 25 mai 2012, le tribunal administratif de Nice a rejeté sa demande, l'a condamnée à verser au centre hospitalier la somme de 4 043 euros TTC au titre du solde de ce marché, assortie des intérêts moratoires à compter du 25 mars 2009 avec capitalisation des intérêts à compter du 27 octobre 2010, et a mis à sa charge les frais d'expertise. <br/>
<br/>
              Par un arrêt n° 12MA02581 du 21 juillet 2014, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Avena BTP contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 septembre et 4 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Avena BTP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de renvoyer le jugement de l'affaire devant la cour administrative d'appel de Marseille ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Grasse la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Avena BTP, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier de Grasse ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Avena BTP et le centre hospitalier de Grasse ont conclu, en 2006, un marché public de travaux pour la construction d'un bâtiment de ce centre ; que la société Avena BTP a refusé d'accepter le décompte général présenté par le centre hospitalier et, après le rejet d'une partie de ses réclamations en février 2009, saisi le juge des référés du tribunal administratif de Nice le 7 juillet 2009, lequel a ordonné une expertise ; qu'elle se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a confirmé le jugement du tribunal administratif de Nice qui, saisi le 19 avril 2010, a notamment rejeté ses demandes relatives au solde de ce marché ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 50.32 du cahier des clauses administratives générales - Travaux publics (CCAG-Travaux) en vigueur à la date du marché en cause et applicable à celui-ci ainsi qu'il ressort des pièces du dossier soumis au juge du fond : " Si, dans le délai de six mois à partir de la notification à l'entrepreneur de la décision prise conformément au 23 du présent article sur les réclamations auxquelles a donné lieu le décompte général du marché, l'entrepreneur n'a pas porté ses réclamations devant le tribunal administratif compétent, il est considéré comme ayant accepté ladite décision et toute réclamation est irrecevable. Toutefois, le délai de six mois est suspendu en cas de saisine du comité consultatif de règlement amiable dans les conditions du 4 du présent article. " ; <br/>
<br/>
              3. Considérant que la procédure de réclamation préalable prévue par l'article 50.32 du cahier des clauses administratives générales - Travaux publics, notamment le respect du délai de six mois pour saisir le juge du contrat à compter de la notification à l'entrepreneur de la décision prise sur les réclamations auxquelles a donné lieu le décompte général, résulte des clauses contractuelles auxquelles les parties ont souscrit ; qu'elles ont organisé de la sorte, ainsi qu'elles le pouvaient, des règles particulières de saisine du juge du contrat ; que ces stipulations ne prévoient aucune cause d'interruption de ce délai ni d'autres cas de suspension que la saisine du comité consultatif de règlement amiable ; que, par suite, en jugeant que les dispositions du code civil qui étaient invoquées devant elle n'étaient pas applicables au litige entre les parties relatif au décompte, exclusivement régi sur ce point par les stipulations citées ci-dessus du CCAG, la cour n'a, en tout état de cause, pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant que la cour n'a pas davantage commis d'erreur de droit en jugeant que ni la saisine du juge des référés du tribunal administratif aux fins de voir ordonner une expertise, ni le dépôt du rapport d'expertise n'avaient eu pour effet de suspendre le délai de six mois prévu à l'article 50.32 du CCAG-Travaux et que le juge des référés saisi d'une demande d'expertise n'était pas le tribunal administratif compétent pour régler le litige mentionné par l'article 50.32 du CCAG-Travaux ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société Avena BTP n'est pas fondée à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Avena BTP la somme de 3 000 euros à verser au centre hospitalier de Grasse, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Avena BTP est rejeté. <br/>
Article 2 : La société Avena BTP versera au centre hospitalier de Grasse la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Avena BTP et au centre hospitalier de Grasse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
