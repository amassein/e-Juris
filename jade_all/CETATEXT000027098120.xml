<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027098120</ID>
<ANCIEN_ID>JG_L_2013_02_000000332701</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/81/CETATEXT000027098120.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 22/02/2013, 332701, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332701</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mme Anne Berriat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:332701.20130222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 13 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'Office français de protection des réfugiés et apatrides, dont le siège est 201, rue Carnot à Fontenay-sous-Bois (94136 cedex) ; l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat d'annuler la décision n° 634930/08015393 du 31 juillet 2009 par laquelle la Cour nationale du droit d'asile, statuant sur le recours de M. C..., a, d'une part, annulé la décision du 5 septembre 2008 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'admission au statut de réfugié, d'autre part, accordé à l'intéressé le bénéfice de la protection subsidiaire ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Berriat, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides et de la SCP de Chaisemartin, Courjon, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides et à la SCP de Chaisemartin, Courjon, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du paragraphe 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, modifié par le paragraphe 2 de l'article 1er du protocole signé le 31 janvier 1967 à New-York, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité ou de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " ; qu'aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié mentionnées à l'article L. 711-1 et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : / b) La torture ou des peines ou traitements inhumains et dégradants " ; qu'aux termes de l'article L. 713-2 du même code : " Les persécutions prises en compte dans l'octroi de la qualité de réfugié et les menaces graves pouvant donner lieu au bénéfice de la protection subsidiaire peuvent être le fait des autorités de l'Etat, de partis ou d'organisations qui contrôlent l'Etat ou une partie substantielle du territoire de l'Etat, ou d'acteurs non étatiques dans les cas où les autorités définies à l'alinéa suivant refusent ou ne sont pas en mesure d'offrir une protection. / Les autorités susceptibles d'offrir une protection peuvent être les autorités de l'Etat et des organisations internationales et régionales " ;<br/>
<br/>
              2. Considérant que, pour rejeter le recours de M. A...contre la décision du directeur de l'Office français de protection des réfugiés et apatrides en date du 5 septembre 2008 refusant de lui reconnaître la qualité de réfugié, la Cour nationale du droit d'asile a estimé qu'en l'absence de faits établissant qu'un " soutien à la cause tamoule " ait pu lui être imputé par les autorités de son pays, il n'existait aucun élément permettant de penser que le requérant serait exposé à des persécutions au sens des stipulations de la convention de Genève, mais qu'en revanche, dans le contexte de guerre civile ayant prévalu jusqu'à une date récente et des multiples exactions commises dans la région d'origine de M.A..., ce dernier pouvait, en raison du fait que les autorités sri-lankaises le soupçonnaient de " sympathie pour le mouvement de libération des tigres tamouls " et de son emploi d'auxiliaire pour une organisation non gouvernementale venant en aide à la population tamoule, prétendre à l'octroi de la protection subsidiaire sur le fondement des dispositions du b) de l'article L. 712-1 précité ;<br/>
<br/>
              3. Considérant que, dans le cas où les agissements en raison desquels un étranger craint d'être persécuté dans le pays dont il a la nationalité, même dépourvus de mobile politique, sont regardés par les autorités du pays comme une manifestation de soutien ou l'expression d'une proximité avec des adversaires qu'elles persécutent pour des motifs politiques, ces agissements ne peuvent, lorsqu'ils sont établis, ouvrir droit qu'à la reconnaissance de la qualité de réfugié ; que ce n'est que dans l'hypothèse où ces agissements, réels ou imputés, ne relèvent d'aucune des catégories permettant d'accorder la protection conventionnelle que le juge peut rechercher si sont réunies les conditions d'octroi de la protection subsidiaire ; qu'à cet égard, la menace grave de subir des tortures ou des peines ou traitements inhumains et dégradants pour des motifs autres que ceux qui sont mentionnés par les stipulations conventionnelles ne peut entraîner l'octroi de la protection subsidiaire que s'il est établi que l'intéressé l'encourt personnellement ;<br/>
<br/>
              4. Considérant qu'en jugeant tout à la fois, ainsi qu'il a été dit, que la sympathie pour la cause tamoule imputée à M. A...n'était pas établie et qu'elle justifiait l'octroi de la protection subsidiaire, la Cour a entaché sa décision de contradiction de motifs ; qu'il en résulte que l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              5. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP de Chaisemartin, Courjon, avocat de M. A...;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de Cour nationale du droit d'asile est annulé.<br/>
Article 2 : Les conclusions de M. A...tendant à l'application des dispositions de l'article 37 alinéa 2 de la loi n° 91-467 du 10 juillet 1991 modifiée relative à l'aide juridique et de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
