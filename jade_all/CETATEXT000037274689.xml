<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037274689</ID>
<ANCIEN_ID>JG_L_2018_07_000000421870</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/46/CETATEXT000037274689.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 20/07/2018, 421870, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421870</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421870.20180720</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mlle A...B...a demandé au juge des référés du tribunal administratif de Grenoble, sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'ordonner au président du conseil départemental de l'Isère, dans un délai de vingt-quatre heures à compter de la notification de l'ordonnance à intervenir, de l'admettre à l'aide sociale à l'enfance et en conséquence, en exécution de l'ordonnance de placement du procureur de la République de Grenoble du 26 avril 2018, de procéder à son accueil en foyer ou en famille d'accueil et à son inscription dans un établissement scolaire et, à titre subsidiaire, d'ordonner au département de l'Isère son hébergement d'urgence, dans une structure adaptée à sa situation et, en cas de carence du département, d'ordonner au préfet de l'Isère de lui proposer un lieu d'hébergement susceptible de l'accueillir, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1803723 du 15 juin 2018, le juge des référés du tribunal administratif de Grenoble a rejeté cette demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 29 juin et 10 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mlle B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge solidaire du département de l'Isère et de l'Etat la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que son exclusion du centre d'hébergement la place dans une situation d'extrême précarité et de particulière vulnérabilité ;<br/>
<br/>
              - la décision du département de l'Isère est manifestement illégale dès lors qu'elle est signée par une autorité incompétente, que le département était tenu d'exécuter l'ordonnance de placement provisoire du procureur de la République de Grenoble tant qu'elle n'était pas levée par le juge des enfants au terme d'une procédure contradictoire et que ni le département ni l'Etat ne pouvaient remettre en cause la minorité de l'intéressée sans méconnaître les articles 47 et 388 du code civil ;<br/>
              - elle porte une atteinte grave et manifestement illégale à la dignité de la personne humaine, à l'intérêt supérieur de l'enfant et au droit à un recours effectif ;<br/>
              - le refus du préfet de l'Isère de pourvoir à son hébergement porte une atteinte grave et manifestement illégale à la dignité de la personne humaine.<br/>
              Par un mémoire en défense, enregistré le 10 juillet 2018, le département de l'Isère conclut au rejet de la requête. Il soutient que les moyens soulevés par la requérante ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 10 juillet 2018, la ministre des solidarités et de la santé conclut au rejet de la requête. Elle soutient que les moyens soulevés par la requérante ne sont pas fondés.   <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MlleB..., et, d'autre part, le président du conseil départemental de l'Isère et la ministre des solidarités et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 juillet 2018 à 11 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mlle B... ;<br/>
<br/>
              - la représentante de MlleB... ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat à la Cour de cassation, avocat du département de l'Isère ;<br/>
<br/>
              - les représentants de la ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au vendredi 13 juillet à 10 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 12 juillet 2018, présenté par la ministre des solidarités et de la santé, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 12 juillet 2018, présenté par le département de l'Isère, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 13 juillet 2018 avant la clôture de l'instruction, présenté par MlleB..., qui conclut aux mêmes fins que la requête ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que Mlle B...a été prise en charge à compter du 26 mars 2018 par le département de l'Isère, auprès duquel elle s'était présentée munie d'un acte de naissance dont il ressortait qu'elle était née le 13 février 2002 en République démocratique du Congo. Saisi le 4 mai 2018 par le département, le procureur de la République près le tribunal de grande instance de Grenoble a, le 7 mai 2018, ordonné en urgence le placement provisoire de l'intéressée et engagé une enquête sur sa minorité, dont il est résulté qu'elle avait formulé en 2014 une demande de visa sous l'identité d'une autre personne, née en 1998 et de nationalité angolaise. Le 17 mai 2018, le département de l'Isère a indiqué à Mlle B... que, le procureur de la République ayant classé son dossier sans suite, sans saisir le juge des enfants, son placement provisoire avait cessé et qu'eu égard aux éléments révélés par l'enquête judiciaire sur son âge, il était mis fin à son hébergement et à sa mise à l'abri. Mlle B...relève appel de l'ordonnance par laquelle le juge des référés du tribunal administratif de Grenoble, statuant sur le fondement de l'article L.521-2 du code de la justice administrative, a rejeté sa demande tendant à ce qu'il soit ordonné au président du conseil départemental de l'Isère, à titre principal de l'admettre à l'aide sociale à l'enfance et d'exécuter l'ordonnance de placement du procureur de la République et, à titre subsidiaire, de pourvoir à son hébergement d'urgence dans une structure adaptée à sa situation et, en cas de carence du département, à ce qu'il soit ordonné au préfet de l'Isère de lui proposer un lieu d'hébergement susceptible de l'accueillir.<br/>
<br/>
              3. D'une part, l'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". L'article L. 223-2 du même code dispose que " sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. (...) /  Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit (...) l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil. (...) ". L'article R. 221-11 du code de l'action sociale et des familles définit la procédure applicable pour la mise en oeuvre de l'article L. 223-2 cité ci-dessus. Il dispose que " I.-Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II.-Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) IV.-Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". <br/>
<br/>
              4. D'autre part, l'article 375 du code civil dispose que : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public. (...) ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ". L'article 375-5 du code civil prévoit qu' " A titre provisoire, mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4./ En cas d'urgence, le procureur de la République du lieu où le mineur a été trouvé a le même pouvoir, à charge de saisir dans les huit jours le juge compétent, qui maintiendra, modifiera ou rapportera la mesure (...) ". L'article 1181 du code de procédure civile dispose que : " Les mesures d'assistance éducative sont prises par le juge des enfants du lieu où demeure, selon le cas l'un des parents, le tuteur du mineur ou la personne, ou le service à qui l'enfant a été confié ; à défaut, par le juge du lieu où demeure le mineur. " et l'article 1184 du même code que : " Lorsque le juge est saisi, conformément aux dispositions du second alinéa de l'article 375-5 du code civil, par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire, il convoque les parties et statue dans un délai qui ne peut excéder quinze jours à compter de sa saisine, faute de quoi le mineur est remis, sur leur demande, à ses parents ou tuteur, ou à la personne ou au service à qui il était confié. ".<br/>
<br/>
              5. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants ou par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire conformément aux dispositions du second alinéa de l'article 375-5 du code civil, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. A cet égard, une obligation particulière pèse sur ces autorités lorsqu'un mineur privé de la protection de sa famille est sans abri et que sa santé, sa sécurité ou sa moralité est en danger. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, en tenant compte des moyens dont l'administration départementale dispose ainsi que de la situation du mineur intéressé, quelles sont les mesures qui peuvent être utilement ordonnées sur le fondement de l'article L. 521-2. <br/>
<br/>
              6. Il résulte de l'instruction que MlleB..., célibataire, sans enfants et dépourvue de documents d'identité, s'étant présentée comme mineure, en produisant un acte de naissance, établi en République démocratique du Congo, selon lequel elle serait née le 13 février 2002 à Kinshasa, a été prise en charge le 26 mars 2018 à titre provisoire par le service de l'aide sociale à l'enfance au sein du dispositif d'accueil des mineurs non accompagnés. Au cours de cette prise en charge, d'une durée de plusieurs semaines, il a été procédé à l'évaluation prévue par l'article R. 221-11 du code de l'action sociale et des familles, à l'issue de laquelle le président du conseil départemental a, au vu des résultats de cette évaluation laissant subsister un doute sur sa minorité, poursuivi l'accueil d'urgence de l'intéressée et saisi le 4 mai 2018 le procureur de la République. Si ce dernier a ordonné son placement provisoire le 7 mai 2018, l'enquête qu'il a alors diligentée a révélé que l'intéressée avait formulé une demande de visa sous l'identité d'une autre personne, majeure, et il n'est pas contesté qu'il n'a pas estimé devoir saisir le juge des enfants dans les conditions prévues au deuxième alinéa de l'article 375-5 du code civil aux fins qu'elle puisse rester confiée au département de l'Isère au-delà du délai de huit jours prévu par ces dispositions. En outre, si Mlle B...indique avoir saisi le juge des enfants par une requête du 29 juin 2018 aux fins d'une mesure de protection, cette requête n'a pas, à la date de la présente décision, abouti à l'intervention d'une telle mesure.<br/>
<br/>
              7. Ainsi, le département de l'Isère a satisfait en l'espère aux obligations qui lui incombaient au titre des dispositions mentionnées au point 3 en pourvoyant à l'accueil provisoire et à l'évaluation de l'intéressée, qu'il ne pouvait admettre à l'aide sociale à l'enfance au-delà de cette période sans que l'autorité judiciaire ne l'ait ordonné. Il a, compte tenu du doute qui subsistait sur la minorité de l'intéressé, saisi le procureur de la République en vue qu'une telle mesure puisse être le cas échéant prononcée, sans cependant que l'ordonnance de placement provisoire prise alors par le parquet ait été suivie d'une saisine par celui-ci du juge des enfants. Enfin, la saisine de ce dernier par l'intéressée elle-même, parallèlement à la présente instance, n'a pas conduit, à la date de la présente ordonnance, à une telle mesure, même prise à titre provisoire comme l'article 375-5 du code civil le permet au juge des enfants pendant l'instance devant lui. Dans ces conditions, la décision du département de l'Isère de mettre fin à l'hébergement et à la mise à l'abri de Mlle B...ne révèle, en l'état de l'instruction, aucune atteinte grave et manifestement illégale à une liberté fondamentale d'une personne mineure.<br/>
<br/>
              8. Dès lors qu'il résulte de ce qui précède qu'aucune carence portant une atteinte grave et manifestement illégale à une liberté fondamentale ne peut, en l'espèce, être caractérisée de la part du département de l'Isère à l'égard de MlleB..., les conclusions de celle-ci tendant à ce que le juge des référés prononce à ce titre une injonction à l'égard de l'Etat ne peuvent qu'être également écartées.<br/>
<br/>
              9. Il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur l'urgence, que Mlle B...n'est pas fondée à se plaindre de ce que le juge des référés du tribunal administratif de Grenoble a rejeté sa demande tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à ce que des mesures soient ordonnées au département de l'Isère ou, en cas de carence, à l'Etat. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise sur leur fondement à la charge du département de l'Isère et de l'Etat qui ne sont pas la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mlle B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mlle A...B..., au département de l'Isère et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
