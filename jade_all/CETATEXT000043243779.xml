<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243779</ID>
<ANCIEN_ID>JG_L_2021_03_000000434132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 11/03/2021, 434132, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434132.20210311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 23 mars 2020, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la région Provence-Alpes-Côte-d'Azur dirigées contre l'arrêt n° 17MA03422 du 1er juillet 2019 de la cour administrative d'appel de Marseille en tant qu'il s'est prononcé sur les salaires versés postérieurement au 30 septembre 2009 et au 30 septembre 2010 mentionnés aux points 14 et 19 de son arrêt et en tant qu'à son point 25 et à ses articles 2 et 3, il a condamné la région à verser des sommes qui devaient seulement abonder l'assiette subventionnable.<br/>
<br/>
              Par un mémoire en défense, enregistré le 5 octobre 2020, l'association pour la Fondation Internet Nouvelle Génération conclut au rejet du pourvoi. Elle soutient qu'aucun de ses moyens n'est fondé. Elle demande en outre que soit mise à la charge de la région Provence-Alpes-Côte-d'Azur la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la région Provence-Alpes-Côte-d'Azur et à la SCP Thouvenin, Coudray, Grevy, avocat de l'association pour la Fondation Internet Nouvelle Génération ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. La région Provence-Alpes-Côte d'Azur a conclu avec l'association pour la Fondation Internet Nouvelle Génération, les 16 mars 2010 et 20 mai 2011, deux conventions attributives de subvention pour la tenue d'une conférence annuelle " Lift France " sur l'innovation, la créativité et la technologie en 2009 et 2010. Les subventions allouées par la région, au titre du Fonds européen de développement régional, ont été fixées, au titre de l'année 2009, au taux maximum de 44,01 % du coût prévisionnel de cette manifestation et, au titre de l'année 2010, au taux maximum de 45,07 % de ce coût prévisionnel. En 2014, la région Provence-Alpes-Côte-d'Azur a estimé que diverses dépenses dont l'association demandait le remboursement au titre de cette subvention n'y étaient pas éligibles, et a réduit à due proportion le versement de celle-ci. L'association a réclamé le paiement des soldes non versés par demandes du 17 novembre 2014, auxquelles la région a opposé un refus par courriers du 18 décembre 2014. Par jugement du 30 mai 2017, le tribunal administratif de Marseille a partiellement fait droit à la demande de l'association tendant à ce que la région soit condamnée à lui verser les sommes réclamées. La cour administrative d'appel de Marseille, après avoir annulé ce jugement et évoqué l'affaire, a condamné la région à verser à l'association, pour l'année 2009, la somme de 11 861,05 euros ainsi que les sommes réclamées au titre des salaires versés pour la journée du 20 juin 2009 et postérieurement au 30 septembre 2009, et pour l'année 2010, la somme de 77 360,55 euros ainsi que les sommes réclamées au titre des salaires versés pour la journée du 3 juillet 2010 et postérieurement au 30 septembre 2010.<br/>
<br/>
              2. En se bornant, pour écarter le moyen tiré de ce que les rémunérations versées par l'association postérieurement au 30 septembre 2009 et au 30 septembre 2010 ne constituaient pas des dépenses éligibles à la subvention, à relever que l'échéancier prévu par les conventions de financement incluait les dépenses engagées, pour 2009, jusqu'au 1er février 2010 et, pour 2010, jusqu'au 28 février 2011, sans se prononcer sur le moyen  de la région selon lequel, dès lors qu'il n'était pas établi que les salariés concernés avaient effectivement été affectés à l'organisation de ces manifestations, ces dépenses étaient sans lien avec l'opération concernée, la cour a commis une erreur de droit.<br/>
<br/>
              3. La cour a également commis une erreur de droit en condamnant la région à verser à l'association des sommes correspondant à l'ensemble des dépenses qu'elle avait jugé éligibles, au lieu de limiter cette somme au montant résultant de l'application à ces dépenses du taux de subventionnement pertinent.<br/>
<br/>
              4. Il résulte de ce qui précède que la région est fondée à demander, dans cette mesure, l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association pour la Fondation Internet Nouvelle Génération une somme de 3 000 euros à verser à la région Provence-Alpes-Côte d'Azur au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font en revanche obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la région Provence-Alpes-Côte d'Azur, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 2 et 3 de l'arrêt de la cour administrative d'appel de Marseille du 1er juillet 2019 sont annulés, d'une part, en tant qu'ils condamnent la région Provence-Alpes-Côte d'Azur à verser à l'association pour la Fondation Internet Nouvelle Génération les sommes correspondant aux salaires versés postérieurement au 30 septembre 2009 et au 30 septembre 2010 mentionnés à ses points 14 et 19 et, d'autre part, en tant qu'ils condamnent la région à verser à l'association des sommes correspondant aux dépenses que la cour a jugé éligibles au lieu de limiter ces sommes au montant résultant de l'application à ces dépenses du taux de subventionnement pertinent. L'article 4 du même arrêt est annulé en tant qu'il rejette les conclusions correspondantes.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille. <br/>
Article 3 : L'association pour la Fondation Internet Nouvelle Génération versera à la région Provence-Alpes-Côte d'Azur une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par l'association pour la Fondation Internet Nouvelle Génération sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5: La présente décision sera notifiée à la région Provence-Alpes-Côte d'Azur et à l'association pour la Fondation Internet Nouvelle Génération.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
