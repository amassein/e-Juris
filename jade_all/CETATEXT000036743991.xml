<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036743991</ID>
<ANCIEN_ID>JG_L_2018_03_000000406356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/39/CETATEXT000036743991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 26/03/2018, 406356</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406356.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 406356, par une requête et un mémoire en réplique, enregistrés les 27 décembre 2016 et 6 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, l'Union syndicale Solidaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation du décret n° 2015-1237 du 7 octobre 2015 modifiant le décret n° 84-558 du 4 juillet 1984 fixant les conditions de désignation des membres du Conseil économique, social et environnemental, en tant qu'il ne lui a pas attribué un siège supplémentaire au titre des représentants des salariés ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de l'autoriser à désigner un représentant des salariés supplémentaire dans un délai de deux mois à compter de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 406357, par une requête et un mémoire en réplique, enregistrés les 27 décembre 2016 et 6 septembre 2017, l'Union syndicale Solidaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation du décret du 19 novembre 2015 portant désignation de personnalités associées au Conseil économique, social et environnemental, en tant qu'il ne prévoit pas la désignation d'une personnalité associée désignée par elle ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de l'autoriser à désigner une personnalité associée dans un délai de deux mois à compter de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1360 du 29 décembre 1958 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - l'ordonnance n° 2015-1341 du 23 octobre 2015 ;<br/>
              - le décret n° 84-558 du 4 juillet 1984 ;<br/>
              - le décret n° 84-822 du 6 septembre 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes de l'Union syndicale Solidaires présentent à juger des questions semblables. Par suite, il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur le refus d'abroger l'article 1er du décret du 7 octobre 2015 :<br/>
<br/>
              2. L'article 7 de l'ordonnance du 29 décembre 1958 portant loi organique relative au Conseil économique et social dispose que : " I. - Le Conseil économique, social et environnemental comprend : / 1° Cent quarante membres au titre de la vie économique et du dialogue social, répartis ainsi qu'il suit : / - soixante-neuf représentants des salariés ; / (...) II. - Les membres représentant les salariés, les entreprises, les artisans, les professions libérales et les exploitants agricoles sont désignés, pour chaque catégorie, par les organisations professionnelles les plus représentatives. (...) ". Aux termes de l'article 2 du décret du 4 juillet 1984 fixant les conditions de désignation des membres du Conseil économique et social et environnemental, dans sa rédaction issue de l'article 1er du décret du 7 octobre 2015 : " Les soixante-neuf représentants des salariés sont désignés ainsi qu'il suit. / Dix-huit représentants désignés par la confédération française démocratique du travail, dont au moins un sur proposition de l'union confédérale des ingénieurs et cadres ; / Six représentants désignés par la confédération française des travailleurs chrétiens ; / Dix-huit représentants désignés par la confédération générale du travail dont au moins un sur proposition de l'union générale des ingénieurs, cadres et techniciens ; / Quatorze représentants désignés par la confédération générale du travail Force ouvrière, dont au moins un sur proposition de l'union des cadres et ingénieurs ; / Six représentants désignés par la confédération française de l'encadrement - C. G. C. ; / Quatre représentants désignés par l'Union nationale des syndicats autonomes ; / Un représentant désigné par la Fédération syndicale unitaire ; / Deux représentants désigné par l'Union syndicale Solidaires ". <br/>
<br/>
              3. L'Union syndicale Solidaires demande l'annulation pour excès de pouvoir de la décision implicite par laquelle le Premier ministre a refusé d'abroger l'article 1er du décret du 7 octobre 2015 modifiant les règles de répartition des sièges entre organisations représentant les salariés au sein du Conseil économique, social et environnemental, au motif qu'il ne lui attribue la désignation que de deux représentants.<br/>
<br/>
              4. Aux termes de l'article L. 243-2 du code des relations entre le public et l'administration, entré en vigueur le 1er juin 2016 : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé (...) ".<br/>
<br/>
              5. Pour l'application des dispositions de l'article 7 de l'ordonnance du 29 décembre 1958 citées au point 2, la représentativité des organisations syndicales appelées à désigner des représentants des salariés au Conseil économique, social et environnemental doit être appréciée au regard de l'ensemble des critères de représentativité, et notamment de l'ancienneté, des effectifs et de l'audience. Il incombe au pouvoir réglementaire, conformément au principe général de représentativité et sous le contrôle du juge de l'excès de pouvoir, de répartir les sièges entre les organisations syndicales les plus représentatives, en tenant compte de leurs résultats aux diverses élections professionnelles au niveau national.<br/>
<br/>
              6. Il ressort des pièces du dossier que l'union requérante a obtenu 3,47 % des voix lors des élections professionnelles qui se sont tenues entre janvier 2009 et décembre 2012 au sein des entreprises de plus de onze salariés, auprès des salariés des très petites entreprises et des employés à domicile et lors des élections aux chambres départementales d'agriculture, et 6,85 % des voix lors des élections professionnelles pour les comités techniques dans la fonction publique en 2014, soit une audience moyenne de 4,4 % auprès de l'ensemble des salariés du secteur privé et du secteur public à la date de la décision attaquée. Si le pouvoir réglementaire, qui a regardé l'union requérante comme l'une des organisations professionnelles de salariés les plus représentatives au sens de l'article 7 de l'ordonnance du 29 décembre 1958, devait, ainsi qu'il a été dit, pour répartir les sièges de représentants des salariés au Conseil économique, social et environnemental entre les organisations syndicales les plus représentatives, tenir compte de leurs résultats aux diverses élections professionnelles au niveau national, il n'était pas tenu, contrairement à ce que soutient l'union requérante, de les répartir selon la règle de la représentation proportionnelle. Par suite, le décret litigieux, qui tient compte des résultats obtenus aux élections professionnelles, a pu sans erreur de droit ni erreur manifeste d'appréciation allouer à l'union requérante deux des soixante-neuf sièges attribués aux représentants des salariés. <br/>
<br/>
              7. Il résulte de ce qui précède que l'Union syndicale Solidaires n'est pas fondée à demander l'annulation du refus par le Premier ministre d'abroger l'article 1er du décret du 7 octobre 2015. Par suite, ses conclusions à fin d'injonction doivent également être rejetées. <br/>
<br/>
              Sur le refus d'abroger le décret du 19 novembre 2015 :<br/>
<br/>
              8. L'article 12 de l'ordonnance du 29 décembre 1958 dispose que : " Les sections sont composées de membres du Conseil économique, social et environnemental. / Des personnalités associées désignées par le Gouvernement à raison de leur qualité, de leur compétence ou de leur expérience peuvent, en outre, dans des conditions fixées par décret en Conseil d'Etat, être appelées à y apporter leur expertise pour une mission et une durée déterminées. Le nombre de ces personnalités associées ne peut excéder huit par section (...) ". Aux termes de l'article 5 du décret du 6 septembre 1984 relatif à l'organisation du Conseil économique, social et environnemental : " Les personnalités associées, mentionnées à l'article 12 de l'ordonnance du 29 décembre 1958, sont nommées pour une durée maximale de cinq ans par décret. Cet acte précise, outre la durée et l'objet de la mission qui leur est confiée, la section à laquelle elles sont rattachées ". <br/>
<br/>
              9. D'une part, selon le principe codifié à l'article L. 242-1 du code des relations entre le public et l'administration entré en vigueur le 1er juin 2016, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits, hors le cas où elle satisfait à une demande du bénéficiaire, que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale. Il résulte des dispositions citées au point 8 que le décret par lequel le Gouvernement désigne une personnalité associée, qui est pris en vue d'apporter à la section du Conseil économique, social et environnemental de rattachement de cette personnalité une expertise tenant à la qualité, à la compétence ou à l'expérience de celle-ci et fixe la durée et l'objet de la mission qui lui est confiée, revêt le caractère d'une décision individuelle créatrice de droits qui ne peut, dès lors, être abrogée, à l'initiative de l'autorité de nomination ou sur la demande d'un tiers, que dans le délai de quatre mois suivant son édiction et à la condition qu'elle soit illégale. Par suite, le Premier ministre, qui l'a reçue le 2 juin 2016, ne pouvait que rejeter la demande de l'Union syndicale Solidaires en tant qu'elle tendait à l'abrogation du décret du 19 novembre 2015 portant désignation de personnalités associées au Conseil économique, social et environnemental. <br/>
<br/>
              10. D'autre part, l'Union syndicale Solidaires ne peut se prévaloir d'aucun droit à obtenir la nomination, en qualité de personnalité associée, d'une personne choisie en son sein ou sur sa proposition. Par suite, elle n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision implicite née du silence gardé par le Premier ministre sur sa demande en tant qu'elle tendait à ce que le décret critiqué soit complété par la nomination d'une personnalité associée supplémentaire choisie en son sein.<br/>
<br/>
              11. Il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le Premier ministre, que les conclusions à fin d'annulation de l'Union syndicale Solidaires et, par suite, ses conclusions à fin d'injonction, doivent être rejetées.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
<br/>
Article 1er : Les requêtes de l'Union syndicale Solidaires sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à l'Union syndicale Solidaires et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES NON RÉGLEMENTAIRES. - DÉCRET PORTANT DÉSIGNATION DE PERSONNALITÉS ASSOCIÉES AUX SECTIONS DU CESE - 1) DEMANDE D'ABROGATION PRÉSENTÉE PAR UNE ORGANISATION SYNDICALE AYANT POUR EFFET DE REMETTRE EN CAUSE LE MANDAT D'UNE PERSONNALITÉ DÉJÀ NOMMÉE - DEMANDE TENDANT À L'ABROGATION D'UNE DÉCISION EXPRESSE INDIVIDUELLE CRÉATRICE DE DROIT - EXISTENCE - CONSÉQUENCE - ABROGATION À L'INITIATIVE DE L'AUTORITÉ DE NOMINATION OU À LA DEMANDE D'UN TIERS DANS UN DÉLAI DE QUATRE MOIS, À CONDITION QU'ELLE SOIT ILLÉGALE - 2) DEMANDE PRÉSENTÉE PAR UNE ORGANISATION SYNDICALE TENDANT À CE QUE LE DÉCRET SOIT COMPLÉTÉ PAR LA NOMINATION D'UNE PERSONNALITÉ ASSOCIÉE SUPPLÉMENTAIRE CHOISIE EN SON SEIN - DROIT À LA NOMINATION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">52-04-01 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. CONSEIL ÉCONOMIQUE, SOCIAL ET ENVIRONNEMENTAL. - DÉCRET PORTANT DÉSIGNATION DE PERSONNALITÉS ASSOCIÉES AUX SECTIONS DU CESE - 1) DEMANDE D'ABROGATION PRÉSENTÉE PAR UNE ORGANISATION SYNDICALE AYANT POUR EFFET DE REMETTRE EN CAUSE LE MANDAT D'UNE PERSONNALITÉ DÉJÀ NOMMÉE - DEMANDE TENDANT À L'ABROGATION D'UNE DÉCISION EXPRESSE INDIVIDUELLE CRÉATRICE DE DROIT - EXISTENCE - CONSÉQUENCE - ABROGATION À L'INITIATIVE DE L'AUTORITÉ DE NOMINATION OU À LA DEMANDE D'UN TIERS DANS UN DÉLAI DE QUATRE MOIS, À CONDITION QU'ELLE SOIT ILLÉGALE - 2) DEMANDE PRÉSENTÉE PAR UNE ORGANISATION SYNDICALE TENDANT À CE QUE LE DÉCRET SOIT COMPLÉTÉ PAR LA NOMINATION D'UNE PERSONNALITÉ ASSOCIÉE SUPPLÉMENTAIRE CHOISIE EN SON SEIN - DROIT À LA NOMINATION - ABSENCE.
</SCT>
<ANA ID="9A"> 01-09-02-02 Décret portant nomination de personnalités associées désignées par le Gouvernement à raison de leur qualité, de leur compétence ou de leur expérience pour siéger dans les sections du Conseil économique, social et environnemental (CESE), mentionnées à l'article 11 de l'ordonnance n° 58-1360 du 29 décembre 1958.... ,,1) Selon le principe codifié à l'article L. 242-1 du code des relations entre le public et l'administration (CRPA) entré en vigueur le 1er juin 2016, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits, hors le cas où elle satisfait à une demande du bénéficiaire, que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale. Il résulte de l'article 12 de l'ordonnance n° 58-1360 du 29 décembre 1958 et de l'article 5 du décret n° 84-558 du 4 juillet 1984 que le décret par lequel le Gouvernement désigne une personnalité associée, qui est pris en vue d'apporter à la section du CESE de rattachement de cette personnalité une expertise tenant à la qualité, à la compétence ou à l'expérience de celle-ci et fixe la durée et l'objet de la mission qui lui est confiée, revêt le caractère d'une décision individuelle créatrice de droits qui ne peut, dès lors, être abrogée, à l'initiative de l'autorité de nomination ou sur la demande d'un tiers, que dans le délai de quatre mois suivant son édiction et à la condition qu'elle soit illégale. Par suite, le Premier ministre ne peut que rejeter la demande, présentée par une organisation syndicale après l'expiration de ce délai de quatre mois, tendant à l'abrogation d'un décret portant désignation de personnalités associées au CESE.... ,,2) Une organisation syndicale ne peut se prévaloir d'aucun droit à obtenir la nomination, en qualité de personnalité associée, d'une personne choisie en son sein ou sur sa proposition. Par suite, le Premier ministre peut légalement rejeter la demande d'une organisation syndicale tendant à ce que le décret soit complété par la nomination d'une personnalité qualifiée supplémentaire choisie en son sein.</ANA>
<ANA ID="9B"> 52-04-01 Décret portant nomination de personnalités associées désignées par le Gouvernement à raison de leur qualité, de leur compétence ou de leur expérience pour siéger dans les sections du Conseil économique, social et environnemental (CESE), mentionnées à l'article 11 de l'ordonnance n° 58-1360 du 29 décembre 1958.... ,,1) Selon le principe codifié à l'article L. 242-1 du code des relations entre le public et l'administration (CRPA) entré en vigueur le 1er juin 2016, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits, hors le cas où elle satisfait à une demande du bénéficiaire, que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale. Il résulte de l'article 12 de l'ordonnance n° 58-1360 du 29 décembre 1958 et de l'article 5 du décret n° 84-558 du 4 juillet 1984 que le décret par lequel le Gouvernement désigne une personnalité associée, qui est pris en vue d'apporter à la section du CESE de rattachement de cette personnalité une expertise tenant à la qualité, à la compétence ou à l'expérience de celle-ci et fixe la durée et l'objet de la mission qui lui est confiée, revêt le caractère d'une décision individuelle créatrice de droits qui ne peut, dès lors, être abrogée, à l'initiative de l'autorité de nomination ou sur la demande d'un tiers, que dans le délai de quatre mois suivant son édiction et à la condition qu'elle soit illégale. Par suite, le Premier ministre ne peut que rejeter la demande, présentée par une organisation syndicale après l'expiration de ce délai de quatre mois, tendant à l'abrogation d'un décret portant désignation de personnalités associées au CESE.... ,,2) Une organisation syndicale ne peut se prévaloir d'aucun droit à obtenir la nomination, en qualité de personnalité associée, d'une personne choisie en son sein ou sur sa proposition. Par suite, le Premier ministre peut légalement rejeter la demande d'une organisation syndicale tendant à ce que le décret soit complété par la nomination d'une personnalité qualifiée supplémentaire choisie en son sein.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
