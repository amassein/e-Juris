<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022203485</ID>
<ANCIEN_ID>JG_L_2009_12_000000304802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/20/34/CETATEXT000022203485.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 28/12/2009, 304802, Publié au recueil Lebon</TITRE>
<DATE_DEC>2009-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>304802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; ODENT</AVOCATS>
<RAPPORTEUR>M. Xavier  Domino</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEASS:2009:304802.20091228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 avril et 13 juin 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE BEZIERS, représentée par son maire ; la COMMUNE DE BEZIERS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 12 février 2007 de la cour administrative d'appel de Marseille, en tant qu'après avoir annulé le jugement du tribunal administratif de Montpellier du 25 mars 2005, il rejette sa demande tendant à ce que la commune de Villeneuve-lès-Béziers soit condamnée à lui verser une indemnité de 591 103,78 euros, au titre des sommes que cette commune aurait dû lui verser en application des clauses d'une convention signée le 10 octobre 1986 ainsi que 45 374,70 euros au titre des dommages et intérêts ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Villeneuve-lès-Béziers la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Xavier Domino, Auditeur,<br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE BEZIERS et de Me Odent, avocat de la commune de Villeneuve-les-Béziers,<br/>
<br/>
              - les conclusions de M. Emmanuel Glaser, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE BEZIERS et à Me Odent, avocat de la commune de Villeneuve-les-Béziers.<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un syndicat intercommunal à vocation multiple qu'elles avaient créé à cette fin, les communes de BEZIERS et de Villeneuve-lès-Béziers ont mené à bien une opération d'extension d'une zone industrielle intégralement située sur le territoire de la commune de Villeneuve-lès-Béziers ; que, par une convention signée par leurs deux maires le 10 octobre 1986, ces collectivités sont convenues que la commune de Villeneuve-lès-Béziers verserait à la COMMUNE DE BEZIERS une fraction des sommes qu'elle percevrait au titre de la taxe professionnelle, afin de tenir compte de la diminution de recettes entraînée par la relocalisation, dans la zone industrielle ainsi créée, d'entreprises jusqu'ici implantées sur le territoire de la COMMUNE DE BEZIERS ; que, par lettre du 22 mars 1996, le maire de Villeneuve-lès-Béziers a informé le maire de BEZIERS de son intention de résilier cette convention à compter du 1er septembre 1996 ; que, par un jugement du 25 mars 2005, le tribunal administratif de Montpellier, saisi par la COMMUNE DE BEZIERS, a rejeté sa demande tendant à ce que la commune de Villeneuve-lès-Béziers soit condamnée à lui verser une indemnité de 591 103,78 euros au titre des sommes non versées depuis la résiliation de la convention, ainsi qu'une somme de 45 374,70 euros au titre des dommages et intérêts ; que, par un arrêt du 13 juin 2007, contre lequel la COMMUNE DE BEZIERS se pourvoit en cassation, la cour administrative d'appel de Marseille a, après avoir annulé pour irrégularité le jugement du tribunal administratif de Montpellier, jugé que la convention du 10 octobre 1986 devait être " déclarée nulle " et rejeté la demande de la COMMUNE DE BEZIERS ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant, en premier lieu, que les parties à un contrat administratif peuvent saisir le juge d'un recours de plein contentieux contestant la validité du contrat qui les lie ; qu'il appartient alors au juge, lorsqu'il constate l'existence d'irrégularités, d'en apprécier l'importance et les conséquences, après avoir vérifié que les irrégularités dont se prévalent les parties sont de celles qu'elles peuvent, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui ; qu'il lui revient, après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, soit de décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation ;<br/>
<br/>
              Considérant, en second lieu, que, lorsque les parties soumettent au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l' exigence de loyauté des relations contractuelles, de faire application du contrat ; que, toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel ;<br/>
<br/>
              Considérant qu'en vertu des dispositions de l'article 2-I de la loi du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions, désormais codifiées à l'article L. 2131-1 du code général des collectivités territoriales : " Les actes pris par les autorités communales sont exécutoires de plein droit dès lors qu'il a été procédé à leur publication ou à leur notification aux intéressés ainsi qu'à leur transmission au représentant de l'Etat dans le département ou à son délégué dans le département " ; que l'absence de transmission de la délibération autorisant le maire à signer un contrat avant la date à laquelle le maire procède à sa signature constitue un vice affectant les conditions dans lesquelles les parties ont donné leur consentement ; que, toutefois, eu égard à l'exigence de loyauté des relations contractuelles, ce seul vice ne saurait être regardé comme d'une gravité telle que le juge doive écarter le contrat et que le litige qui oppose les parties ne doive pas être tranché sur le terrain contractuel ;<br/>
<br/>
              Considérant, dès lors, qu'en jugeant que la convention conclue le 10 octobre 1986 entre les communes de Villeneuve-lès-Béziers et de Béziers devait être " déclarée nulle " au seul motif que les délibérations du 29 septembre 1986 et du 3 octobre 1986 autorisant les maires de ces communes à la signer n'avaient été transmises à la sous-préfecture que le 16 octobre 1986 et qu'une telle circonstance faisait obstacle à ce que les stipulations du contrat soient invoquées dans le cadre du litige dont elle était saisie, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, par suite, la COMMUNE DE BEZIERS est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la COMMUNE DE BEZIERS, qui n'est pas la partie perdante dans la présente instance, la somme que la commune de Villeneuve-lès-Béziers demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, sur le fondement des mêmes dispositions, de mettre à la charge de Villeneuve-lès-Béziers une somme de 3 000 euros à verser à la COMMUNE DE BEZIERS ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 février 2007 est annulé en tant qu'il rejette la demande de la COMMUNE DE BEZIERS.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure devant la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La commune de Villeneuve-lès-Béziers versera à la COMMUNE DE BEZIERS la somme de 3 000 au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Villeneuve-lès-Béziers au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE BEZIERS et à la commune de Villeneuve-lès-Béziers.<br/>
Copie en sera transmise pour information au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-015 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. CONTRÔLE DE LA LÉGALITÉ DES ACTES DES AUTORITÉS LOCALES. - DÉLIBÉRATION AUTORISANT LE MAIRE À SIGNER UN CONTRAT - MAIRE CONCLUANT CE CONTRAT AVANT TRANSMISSION DE LA DÉLIBÉRATION AU PRÉFET - VICE AFFECTANT LES CONDITIONS DANS LESQUELLES LES PARTIES ONT DONNÉ LEUR CONSENTEMENT - EXISTENCE - PORTÉE - ILLÉGALITÉ QUI N'EST PAS DE NATURE À ENTRAÎNER L'ANNULATION DU CONTRAT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. - DÉLIBÉRATION AUTORISANT LE MAIRE À SIGNER UN CONTRAT - MAIRE CONCLUANT CE CONTRAT AVANT TRANSMISSION DE LA DÉLIBÉRATION AU PRÉFET - VICE AFFECTANT LES CONDITIONS DANS LESQUELLES LES PARTIES ONT DONNÉ LEUR CONSENTEMENT - EXISTENCE - PORTÉE - ILLÉGALITÉ QUI N'EST PAS DE NATURE À ENTRAÎNER L'ANNULATION DU CONTRAT [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. - OFFICE DU JUGE DE PLEIN CONTENTIEUX SAISI PAR UNE PARTIE À UN CONTRAT - 1) CAS OÙ LE JUGE EST SAISI D'UN RECOURS EN VALIDITÉ DU CONTRAT - A) RECEVABILITÉ À SE PRÉVALOIR D'UNE IRRÉGULARITÉ - CONDITION - IRRÉGULARITÉ POUVANT ÊTRE INVOQUÉE EU ÉGARD À L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES - B) POUVOIRS ET DEVOIRS DU JUGE FACE À UNE TELLE IRRÉGULARITÉ [RJ2] - 2) CAS OÙ LE JUGE EST SAISI D'UN LITIGE D'EXÉCUTION DU CONTRAT - OBLIGATION DE FAIRE APPLICATION, EN PRINCIPE, DU CONTRAT - EXCEPTIONS - CARACTÈRE ILLICITE DU CONTRAT OU VICE D'UNE PARTICULIÈRE GRAVITÉ.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - ANNULATION DU CONTRAT DANS LE CAS OÙ LE MAIRE L'A CONCLU AVANT TRANSMISSION DE LA DÉLIBÉRATION AU PRÉFET - ABSENCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - IRRÉGULARITÉS SOULEVÉES D'OFFICE PAR LE JUGE - IRRÉGULARITÉS TENANT SEULEMENT AU CARACTÈRE ILLICITE DU CONTENU DU CONTRAT OU À UN VICE D'UNE PARTICULIÈRE GRAVITÉ RELATIF NOTAMMENT AUX CONDITIONS DANS LESQUELLES LES PARTIES ONT DONNÉ LEUR CONSENTEMENT - CONSÉQUENCES - 1) ANNULATION EN CAS DE CONTESTATION DE LA VALIDITÉ DU CONTRAT - 2) LITIGE NE POUVANT ÊTRE RÉGLÉ SUR LE TERRAIN CONTRACTUEL EN CAS DE LITIGE EN EXÉCUTION DU CONTRAT.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - IRRÉGULARITÉS DONT UNE PARTIE PEUT SE PRÉVALOIR DANS LE CADRE D'UN RECOURS EN VALIDITÉ DU CONTRAT - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES [RJ3].
</SCT>
<SCT ID="8G" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - JUGE DE PLEIN CONTENTIEUX SAISI PAR UNE PARTIE À UN CONTRAT - 1) CAS OÙ LE JUGE EST SAISI D'UN RECOURS EN VALIDITÉ DU CONTRAT - A) RECEVABILITÉ À SE PRÉVALOIR D'UNE IRRÉGULARITÉ - CONDITION - IRRÉGULARITÉ POUVANT ÊTRE INVOQUÉE EU ÉGARD À L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES - B) POUVOIRS ET DEVOIRS DU JUGE FACE À UNE IRRÉGULARITÉ [RJ2] - 2) CAS OÙ LE JUGE EST SAISI D'UN LITIGE D'EXÉCUTION DU CONTRAT - OBLIGATION DE FAIRE APPLICATION, EN PRINCIPE DU CONTRAT - EXCEPTIONS - CARACTÈRE ILLICITE DU CONTRAT OU VICE D'UNE PARTICULIÈRE GRAVITÉ.
</SCT>
<ANA ID="9A"> 135-01-015 L'absence de transmission de la délibération autorisant le maire à signer un contrat avant la date à laquelle le maire procède à sa signature constitue un vice affectant les conditions dans lesquelles les parties ont donné leur consentement. Toutefois, eu égard à l'exigence de loyauté des relations contractuelles, ce seul vice ne saurait être regardé comme d'une gravité telle que le juge saisi par une partie au contrat doive, soit l'annuler s'il est saisi de sa validité, soit l'écarter pour régler un litige d'exécution sur un terrain non contractuel.</ANA>
<ANA ID="9B"> 39-02 L'absence de transmission de la délibération autorisant le maire à signer un contrat avant la date à laquelle le maire procède à sa signature constitue un vice affectant les conditions dans lesquelles les parties ont donné leur consentement. Toutefois, eu égard à l'exigence de loyauté des relations contractuelles, ce seul vice ne saurait être regardé comme d'une gravité telle que le juge saisi par une partie au contrat doive, soit l'annuler s'il est saisi de sa validité, soit l'écarter pour régler un litige d'exécution sur un terrain non contractuel.</ANA>
<ANA ID="9C"> 39-04 1) Une partie à un contrat administratif peut saisir le juge du contrat d'un recours de plein contentieux pour en contester la validité. a) Il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. b) S'il constate une irrégularité, il doit en apprécier l'importance et les conséquences. Après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, il peut soit décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation.,,2) Lorsqu'une partie à un contrat administratif soumet au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l'exigence de loyauté des relations contractuelles, de faire application du contrat. Toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel.</ANA>
<ANA ID="9D"> 39-04-01 L'absence de transmission de la délibération autorisant le maire à signer un contrat avant la date à laquelle le maire procède à sa signature constitue un vice affectant les conditions dans lesquelles les parties ont donné leur consentement. Toutefois, eu égard à l'exigence de loyauté des relations contractuelles, ce seul vice ne saurait être regardé comme d'une gravité telle que le juge saisi par une partie au contrat doive, soit l'annuler s'il est saisi de sa validité, soit l'écarter pour régler un litige d'exécution sur un terrain non contractuel.</ANA>
<ANA ID="9E"> 39-04-01 Le juge du contrat, juge de plein contentieux saisi par une partie, peut relever d'office une irrégularité tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement. Dans ce cas : 1) si le juge est saisi d'un recours en validité du contrat, il doit l'annuler si l'irrégularité est avérée ; 2) si le juge est saisi d'un litige d'exécution du contrat, il doit l'écarter et ne peut pas régler le litige sur le terrain contractuel.</ANA>
<ANA ID="9F"> 39-08-01 Le juge, saisi par une partie à un contrat administratif d'un recours de plein contentieux pour en contester la validité, doit vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui.</ANA>
<ANA ID="9G"> 39-08-03-02 1) Une partie à un contrat administratif peut saisir le juge du contrat d'un recours de plein contentieux pour en contester la validité. a) Il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. b) S'il constate une irrégularité, il doit en apprécier l'importance et les conséquences. Après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, il peut soit décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation.,,2) Lorsqu'une partie à un contrat administratif soumet au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l'exigence de loyauté des relations contractuelles, de faire application du contrat. Toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. Section, avis, 10 juin 1996, Préfet de la Côte d'Or, n°s 176873 176874 176875, p. 198., ,[RJ2] Rappr., s'agissant de la prise en compte de l'intérêt général avant de décider d'une résiliation du contrat litigieux, 10 décembre 2003, Institut de recherche pour le développement, n° 248950, p. 501 ; s'agissant des différents pouvoirs dont le juge du contrat dispose, Assemblée, 16 juillet 2007, Société Tropic Travaux Signalisation, n° 291545, p. 360., ,[RJ3] Rappr., pour l'obligation similaire incombant au juge des référés précontractuels, 3 octobre 2008, Syndicat mixte intercommunal de réalisation et de gestion pour l'élimination des ordures ménagères du secteur Est de la Sarthe (Smirgeomes), n° 305420, p. 324.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
