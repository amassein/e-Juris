<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799805</ID>
<ANCIEN_ID>JG_L_2021_07_000000450041</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/98/CETATEXT000043799805.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 16/07/2021, 450041, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450041</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450041.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B... A... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution des décisions verbales des 12 octobre et 10 novembre 2020 par lesquelles le maire de la commune de Clichy-la-Garenne a refusé d'assurer son hébergement ou son relogement après l'évacuation de l'immeuble qu'elle occupait, d'autre part, d'enjoindre au même maire de lui présenter des offres d'hébergement ou de relogement dans un délai de 24 heures sous astreinte de 100 euros par jour de retard, de lui permettre de récupérer ses effets personnels et de lui verser une indemnité de relogement. Par une ordonnance n° 2012720 du 7 janvier 2021, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 février et 10 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Clichy-la-Garenne la somme de 1 500 euros à verser à la SCP Thouvenin-Coudray-Grévy, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de Mme B... A... et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Clichy-la-Garenne.<br/>
<br/>
              Vu la note en délibéré, présentée par Mme B... A..., enregistrée le 9 juillet 2021 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par deux arrêtés des 20 et 28 octobre 2020, le maire de Clichy-la-Garenne a ordonné l'évacuation immédiate et l'interdiction d'accès et d'occupation d'un immeuble situé au 93 rue Henri Barbusse. Par plusieurs décisions verbales, notamment du 10 novembre 2020, le même maire a rejeté la demande de Mme B... A..., ancienne occupante de cet immeuble, tendant à ce que la commune assure son hébergement ou son relogement. Mme B... A... se pourvoit en cassation contre l'ordonnance du 7 janvier 2021 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande de suspension de l'exécution de ces refus d'hébergement.  <br/>
<br/>
              2. D'une part, aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. D'autre part, aux termes du premier alinéa de l'article L. 521-1 du code de la construction et de l'habitation : " Pour l'application du présent chapitre, l'occupant est le titulaire d'un droit réel conférant l'usage, le locataire, le sous-locataire ou l'occupant de bonne foi des locaux à usage d'habitation et de locaux d'hébergement constituant son habitation principale ". Aux termes du I de l'article L. 521-3-1 du même code, dans sa rédaction applicable à la date des décisions contestées : " I.-Lorsqu'un immeuble fait l'objet d'une interdiction temporaire d'habiter ou d'utiliser ou que son évacuation est ordonnée en application de l'article L. 511-3 ou de l'article L. 129-3, le propriétaire ou l'exploitant est tenu d'assurer aux occupants un hébergement décent correspondant à leurs besoins. / A défaut, l'hébergement est assuré dans les conditions prévues à l'article L. 521-3-2. Son coût est mis à la charge du propriétaire ou de l'exploitant. / (...) ". Aux termes de l'article L. 521-3-2 du même code, dans sa rédaction applicable : " I.-Lorsqu'un arrêté de péril pris en application de l'article L. 511-1 ou des prescriptions édictées en application de l'article L. 123-3 ou de l'article L. 129-3 sont accompagnés d'une interdiction temporaire ou définitive d'habiter et que le propriétaire ou l'exploitant n'a pas assuré l'hébergement ou le relogement des occupants, le maire ou, le cas échéant, le président de l'établissement public de coopération intercommunale prend les dispositions nécessaires pour les héberger ou les reloger. / (...) / VII.-Si l'occupant a refusé trois offres de relogement qui lui ont été faites au titre des I, II ou III, le juge peut être saisi d'une demande tendant à la résiliation du bail ou du droit d'occupation et à l'autorisation d'expulser l'occupant ". Il résulte de ces dispositions que l'obligation d'hébergement qu'elles prévoient incombe au maire de la commune ou, le cas échéant, au président de l'établissement public de coopération intercommunale, dès lors qu'il est établi que le propriétaire ou l'exploitant n'assure pas sa propre obligation.<br/>
<br/>
              4. Il résulte des termes mêmes de l'ordonnance attaquée que, pour juger, d'une part, que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative n'était pas remplie et, d'autre part, que le moyen tiré de la méconnaissance des dispositions de l'article L. 521-3-2 du code de la construction et de l'habitation n'était pas propre à créer un doute sérieux quant à la légalité des refus d'hébergement contestés, le juge des référés s'est fondé sur la circonstance que Mme B... A... n'avait effectué de démarche expresse auprès du propriétaire de l'immeuble que peu de temps avant de saisir le tribunal administratif et n'établissait donc pas la défaillance de ce dernier. En statuant ainsi, alors qu'une telle défaillance du propriétaire peut résulter du seul comportement de ce dernier, même sans démarche formelle du locataire et qu'il ressortait des pièces du dossier qui lui était soumis que le propriétaire de l'immeuble en cause avait renoncé à proposer un hébergement à ses anciens occupants, le juge des référés a dénaturé ces mêmes pièces du dossier.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi et sans qu'il y ait lieu de faire droit à la substitution de motifs demandée par la commune de Clichy-la-Garenne, tirée de ce que la requérante n'était pas une occupante de bonne foi, laquelle requiert une appréciation des faits qui ne saurait relever du juge de cassation, que Mme B... A... est fondée à demander l'annulation de l'ordonnance qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Clichy-la-Garenne une somme de 500 euros à verser à la SCP Thouvenin-Coudray-Grévy, avocat de Mme B... A..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance de la juge des référés du tribunal administratif de Cergy-Pontoise du 7 janvier 2021 est annulée. <br/>
<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Cergy-Pontoise. <br/>
<br/>
Article 3 : La commune de Clichy-la-Garenne versera à la SCP Thouvenin-Coudray-Grévy la somme de 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme C... B... A... et à la commune de Clichy-la-Garenne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
