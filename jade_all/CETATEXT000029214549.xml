<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214549</ID>
<ANCIEN_ID>JG_L_2014_07_000000373381</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214549.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 09/07/2014, 373381, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373381</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DELVOLVE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373381.20140709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 novembre 2013 et 20 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat mixte interdépartemental d'aménagement du Vidourle (SIAV), dont le siège est 1000, rue d'Alco à Montpellier (34087) ; le SIAV demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 13MA00831 du 19 septembre 2013 par lequel la cour administrative d'appel de Marseille, statuant sur l'appel de M. et MmeA..., a annulé pour irrégularité le jugement n° 0630055 du tribunal administratif de Nîmes du 11 mars 2008 et, évoquant la demande indemnitaire présentée par les époux A...devant le tribunal administratif, l'a condamné à leur verser la somme de 270 048,34 euros, augmentée des intérêts au taux légal à compter du 31 juillet 2006, et a mis à sa charge les frais d'expertise taxés et liquidés à la somme de 4 202,30 euros ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des époux A...; <br/>
<br/>
              3°) de mettre à la charge des époux A...les frais d'expertise et le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat mixte interdépartemental d'aménagement du Vidourle ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; <br/>
<br/>
              2. Considérant que, pour demander l'annulation de l'arrêt qu'il attaque, le Syndicat mixte interdépartemental d'aménagement du Vidourle soutient que la cour administrative d'appel de Marseille a omis de répondre au moyen tiré de ce que les époux A...ayant acheté en toute connaissance de cause une propriété régulièrement inondée, il existait un risque accepté de nature à l'exonérer de sa responsabilité ; qu'elle a inexactement qualifié les faits de l'espèce ou commis une erreur de droit en retenant sa pleine responsabilité, alors que le droit à réparation des époux A...ne pouvait porter que sur la part du dommage excédant les risques qu'ils avaient acceptés ; qu'elle a entaché son arrêt d'insuffisance de motivation, de dénaturation et d'erreur de droit en indemnisant les époux A...au titre de la perte de valeur vénale de leur propriété, sans répondre à l'argumentation tirée de ce que leur bâtiment, situé en amont de la zone de déversement, n'avait subi aucune inondation après 2003 et alors que les intéressés n'avaient pas cherché à vendre leur bien, en sorte que le préjudice résultant de la perte de valeur vénale ne présentait pas un caractère certain et que leurs conclusions relatives à la perte de la valeur vénale étaient nouvelles en appel et, par suite, irrecevables ; qu'elle a dénaturé les faits soumis à son appréciation ou commis une erreur de droit en retenant un préjudice pour les troubles dans les conditions d'existence de 11 000 euros résultant de la crainte d'être surexposés au risque d'inondation, alors que les requérants n'étayaient pas leurs prétentions sur ce point ; qu'elle a commis une erreur de droit en indemnisant deux fois les frais d'expertise, à hauteur de 5 830,59 euros puis de 4 202,30 euros ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt en tant qu'il fixe le montant de l'indemnité due aux épouxA... ; qu'en revanche, s'agissant des conclusions dirigées contre l'arrêt attaqué en tant qu'il annule pour irrégularité le jugement du tribunal administratif de Nîmes du 11 mars 2008, admet le principe de la responsabilité du Syndicat mixte interdépartemental d'aménagement du Vidourle, met à sa charge les frais d'expertise et statue sur les conclusions des parties présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi du Syndicat mixte interdépartemental d'aménagement du Vidourle qui sont dirigées contre l'arrêt attaqué en tant qu'après avoir annulé pour irrégularité le jugement du tribunal administratif de Nîmes du 11 mars 2008 et admis le principe de sa responsabilité il fixe le montant de l'indemnité due aux époux A...sont admises.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi du Syndicat mixte interdépartemental d'aménagement du Vidourle n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat mixte interdépartemental d'aménagement du Vidourle et aux épouxA....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
