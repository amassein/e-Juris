<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657410</ID>
<ANCIEN_ID>JG_L_2016_12_000000391452</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657410.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 16/12/2016, 391452, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391452</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP BORE, SALVE DE BRUNETON ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Marie-Françoise Guilhemsans</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391452.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Mardié et l'association Mardiéval ont demandé au tribunal administratif d'Orléans d'annuler la décision du préfet du Loiret du 28 mars 2011 autorisant la société Ligérienne Granulats SA à exploiter une carrière de matériaux alluvionnaires et une installation de traitement de ces matériaux au lieu-dit " L'Etang " sur la commune de Mardié. Par un jugement n° 1103069-1104130 du 19 mars 2013, ce tribunal a rejeté leurs demandes. <br/>
<br/>
              Par un arrêt n° 13NT01425-13NT01426 du 11 mai 2015, la cour administrative d'appel de Nantes a, sur les appels de la commune de Mardié et de l'association Mardiéval, annulé ce jugement et la décision attaqués.  <br/>
<br/>
              1° Sous le n° 391452, par un pourvoi et un mémoire en réplique, enregistrés les 2 juillet 2015 et 3 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Ligérienne Granulats SA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes d'appel de la commune de Mardié et de l'association Mardiéval ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Mardié et de l'association Mardiéval la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 391688, par un pourvoi enregistré le 10 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes d'appel de la commune de Mardié et de l'association Mardiéval ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Françoise Guilhemsans, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Ligérienne Granulats SA, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Mardié et à la SCP Boré, Salve de Bruneton, avocat de l'association Mardiéval.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société Ligérienne Granulats SA et du ministre de l'écologie, du développement durable et de l'énergie, sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 28 mars 2011, le préfet du Loiret a autorisé la société Ligérienne Granulats SA à exploiter une carrière de matériaux alluvionnaires et une installation de traitement sur le territoire de la commune de Mardié ; que, par deux jugements du 19 mars 2013, le tribunal administratif d'Orléans a rejeté les demandes de cette commune et de l'association Mardiéval tendant à l'annulation de cet arrêté ; que, par un arrêt du 11 mai 2015, contre lequel la société Ligérienne Granulats SA et la ministre de l'écologie, du développement durable et de l'énergie se pourvoient en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et cet arrêté ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en vertu du premier alinéa de l'article L. 123-5 du code de l'urbanisme, devenu son article L. 152-1, le règlement et les documents graphiques du plan d'occupation des sols ou du plan local d'urbanisme qui lui a succédé sont opposables à l'ouverture des installations classées appartenant aux catégories déterminées dans le plan ; qu'il en résulte que les prescriptions de celui-ci qui déterminent les conditions d'utilisation et d'occupation des sols et les natures d'activités interdites ou limitées s'imposent aux autorisations d'exploiter délivrées au titre de la législation des installations classées ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il appartient au juge du plein contentieux des installations classées de se prononcer sur la légalité de l'autorisation au regard des règles d'urbanisme légalement applicables à la date de sa délivrance ; que, toutefois, eu égard à son office, la méconnaissance par l'autorisation des règles d'urbanisme en vigueur à cette date ne fait pas obstacle à ce qu'il constate que, à la date à laquelle il statue, la décision a été régularisée par une modification ultérieure de ces règles ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il résulte de l'article L. 600-12 du même code que la déclaration d'illégalité d'un document d'urbanisme a, au même titre que son annulation pour excès de pouvoir, pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur et, le cas échéant, en l'absence d'un tel document, les règles générales d'urbanisme rendues alors applicables, en particulier celles de l'article L. 111-1-2 du code de l'urbanisme ; que, dès lors, il peut être utilement soutenu devant le juge qu'une autorisation d'exploiter une installation classée a été délivrée sous l'empire d'un document d'urbanisme illégal - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que l'autorisation méconnaît les dispositions d'urbanisme pertinentes remises en vigueur du fait de la constatation de cette illégalité et, le cas échéant, de celle du document remis en vigueur ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, qu'en ayant apprécié la légalité de l'autorisation litigieuse au regard des seules dispositions du plan local d'urbanisme en vigueur à la date de son arrêt, alors qu'il lui appartenait d'abord de l'apprécier au regard des règles d'urbanisme légalement applicables à la date à laquelle l'autorisation a été délivrée, la cour a entaché son arrêt d'une erreur de droit ; qu'en outre, elle a entaché son arrêt d'une seconde erreur de droit en ayant déduit de la seule circonstance que les règles d'urbanisme au regard desquelles elle appréciait la légalité de l'autorisation contestée méconnaissaient les exigences de l'article L. 122-1-15 du code de l'urbanisme que cette autorisation devait être annulée, alors qu'il lui appartenait, dans ces conditions, et sous les réserves rappelées au point 5, d'examiner la légalité de cette autorisation au regard des dispositions pertinentes du document d'urbanisme remises en vigueur de ce fait ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que société Ligérienne Granulats SA et la ministre de l'écologie, du développement durable et de l'énergie sont fondés, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Mardié et de l'association Mardiéval la somme de 1 500 euros chacune à verser à la société Ligérienne Granulats SA, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de cette société qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 mai 2015 de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
<br/>
Article 3 : La commune de Mardié et l'association Mardiéval verseront à la société Ligérienne Granulats SA une somme de 1 500 euros chacune, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Mardié et l'association Mardiéval au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Ligérienne Granulats SA, à la commune de Mardié, à l'association Mardiéval et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-04 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - OFFICE DU JUGE DU PLEIN CONTENTIEUX DES INSTALLATIONS CLASSÉES - MODALITÉS D'APPRÉCIATION DE LA LÉGALITÉ DE L'AUTORISATION AU REGARD DES RÈGLES D'URBANISME [RJ1] - 1) APPRÉCIATION À LA DATE DE LA DÉLIVRANCE DE L'AUTORISATION - 2) CAS OÙ L'AUTORISATION MÉCONNAÎT CES RÈGLES MAIS A ÉTÉ RÉGULARISÉE PAR LEUR MODIFICATION ULTÉRIEURE - CONSTAT PAR LE JUGE DE CETTE RÉGULARISATION - EXISTENCE - 3) OPÉRANCE DE L'EXCEPTION D'ILLÉGALITÉ D'UN DOCUMENT D'URBANISME - CONDITION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - EXCEPTION D'ILLÉGALITÉ D'UN DOCUMENT D'URBANISME INVOQUÉE À L'ENCONTRE D'UNE AUTORISATION D'EXPLOITER UNE INSTALLATION CLASSÉE - OPÉRANCE - CONDITION - REQUÉRANT INVOQUANT ÉGALEMENT L'ILLÉGALITÉ DE L'AUTORISATION AU REGARD DES DOCUMENTS D'URBANISME ANTÉRIEURS REMIS EN VIGUEUR DU FAIT DE L'ILLÉGALITÉ DU DOCUMENT SOUS L'EMPIRE DUQUEL LE PERMIS A ÉTÉ DÉLIVRÉ [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - OFFICE DU JUGE DU PLEIN CONTENTIEUX DES INSTALLATIONS CLASSÉES - MODALITÉS D'APPRÉCIATION DE LA LÉGALITÉ DE L'AUTORISATION AU REGARD DES RÈGLES D'URBANISME [RJ1] - 1) APPRÉCIATION À LA DATE DE LA DÉLIVRANCE DE L'AUTORISATION - 2) CAS OÙ L'AUTORISATION MÉCONNAÎT CES RÈGLES MAIS A ÉTÉ RÉGULARISÉE PAR LEUR MODIFICATION ULTÉRIEURE - CONSTAT PAR LE JUGE DE CETTE RÉGULARISATION - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. - EXCEPTION D'ILLÉGALITÉ D'UN DOCUMENT D'URBANISME INVOQUÉE À L'ENCONTRE D'UNE AUTORISATION D'EXPLOITER UNE INSTALLATION CLASSÉE - OPÉRANCE - CONDITION - REQUÉRANT INVOQUANT ÉGALEMENT L'ILLÉGALITÉ DE L'AUTORISATION AU REGARD DES DOCUMENTS D'URBANISME ANTÉRIEURS REMIS EN VIGUEUR DU FAIT DE L'ILLÉGALITÉ DU DOCUMENT SOUS L'EMPIRE DUQUEL LE PERMIS A ÉTÉ DÉLIVRÉ [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">68-06-04-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. MOYENS. - EXCEPTION D'ILLÉGALITÉ D'UN DOCUMENT D'URBANISME INVOQUÉE À L'ENCONTRE D'UNE AUTORISATION D'EXPLOITER UNE INSTALLATION CLASSÉE - OPÉRANCE - CONDITION - REQUÉRANT INVOQUANT ÉGALEMENT L'ILLÉGALITÉ DE L'AUTORISATION AU REGARD DES DOCUMENTS D'URBANISME ANTÉRIEURS REMIS EN VIGUEUR DU FAIT DE L'ILLÉGALITÉ DU DOCUMENT SOUS L'EMPIRE DUQUEL LE PERMIS A ÉTÉ DÉLIVRÉ [RJ2].
</SCT>
<ANA ID="9A"> 44-02-04 1) Il appartient au juge du plein contentieux des installations classées de se prononcer sur la légalité de l'autorisation au regard des règles d'urbanisme légalement applicables à la date de sa délivrance.,,,2) Toutefois, eu égard à son office, la méconnaissance par l'autorisation des règles d'urbanisme en vigueur à cette date ne fait pas obstacle à ce qu'il constate que, à la date à laquelle il statue, la décision a été régularisée par une modification ultérieure de ces règles.,,,3) Il résulte de l'article  L. 600-12 du code de l'urbanisme que la déclaration d'illégalité d'un document d'urbanisme a, au même titre que son annulation pour excès de pouvoir, pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur et, le cas échéant, en l'absence d'un tel document, les règles générales d'urbanisme rendues alors applicables, en particulier celles de l'article L. 111-1-2 du code de l'urbanisme. Dès lors, il peut être utilement soutenu devant le juge qu'une autorisation d'exploiter une installation classée a été délivrée sous l'empire d'un document d'urbanisme illégal - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que l'autorisation méconnaît les dispositions d'urbanisme pertinentes remises en vigueur du fait de la constatation de cette illégalité et, le cas échéant, de celle du document remis en vigueur.</ANA>
<ANA ID="9B"> 54-07-01-04-04 Il résulte de l'article  L. 600-12 du code de l'urbanisme que la déclaration d'illégalité d'un document d'urbanisme a, au même titre que son annulation pour excès de pouvoir, pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur et, le cas échéant, en l'absence d'un tel document, les règles générales d'urbanisme rendues alors applicables, en particulier celles de l'article L. 111-1-2 du code de l'urbanisme. Dès lors, il peut être utilement soutenu devant le juge qu'une autorisation d'exploiter une installation classée a été délivrée sous l'empire d'un document d'urbanisme illégal - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que l'autorisation méconnaît les dispositions d'urbanisme pertinentes remises en vigueur du fait de la constatation de cette illégalité et, le cas échéant, de celle du document remis en vigueur.</ANA>
<ANA ID="9C"> 54-07-03 1) Il appartient au juge du plein contentieux des installations classées de se prononcer sur la légalité de l'autorisation au regard des règles d'urbanisme légalement applicables à la date de sa délivrance.,,,2) Toutefois, eu égard à son office, la méconnaissance par l'autorisation des règles d'urbanisme en vigueur à cette date ne fait pas obstacle à ce qu'il constate que, à la date à laquelle il statue, la décision a été régularisée par une modification ultérieure de ces règles.</ANA>
<ANA ID="9D"> 68-01-01-01 Il résulte de l'article  L. 600-12 du code de l'urbanisme que la déclaration d'illégalité d'un document d'urbanisme a, au même titre que son annulation pour excès de pouvoir, pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur et, le cas échéant, en l'absence d'un tel document, les règles générales d'urbanisme rendues alors applicables, en particulier celles de l'article L. 111-1-2 du code de l'urbanisme. Dès lors, il peut être utilement soutenu devant le juge qu'une autorisation d'exploiter une installation classée a été délivrée sous l'empire d'un document d'urbanisme illégal - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que l'autorisation méconnaît les dispositions d'urbanisme pertinentes remises en vigueur du fait de la constatation de cette illégalité et, le cas échéant, de celle du document remis en vigueur.</ANA>
<ANA ID="9E"> 68-06-04-01 Il résulte de l'article  L. 600-12 du code de l'urbanisme que la déclaration d'illégalité d'un document d'urbanisme a, au même titre que son annulation pour excès de pouvoir, pour effet de remettre en vigueur le document d'urbanisme immédiatement antérieur et, le cas échéant, en l'absence d'un tel document, les règles générales d'urbanisme rendues alors applicables, en particulier celles de l'article L. 111-1-2 du code de l'urbanisme. Dès lors, il peut être utilement soutenu devant le juge qu'une autorisation d'exploiter une installation classée a été délivrée sous l'empire d'un document d'urbanisme illégal - sous réserve, en ce qui concerne les vices de forme ou de procédure, des dispositions de l'article L. 600-1 du même code -, à la condition que le requérant fasse en outre valoir que l'autorisation méconnaît les dispositions d'urbanisme pertinentes remises en vigueur du fait de la constatation de cette illégalité et, le cas échéant, de celle du document remis en vigueur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 22 février 2016, Société Entreprise routière du Grand Sud et autres, n° 367901, aux Tables sur d'autres points.,,[RJ2]Cf. CE, Section, 7 février 2008, Commune de Courbevoie, n°s 297227 e. a., p. 41. Rappr., sur l'office de l'autorité administrative délivrant une autorisation d'urbanisme, CE, 9 mai 2005,  Marangio, n° 277280, p. 195.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
