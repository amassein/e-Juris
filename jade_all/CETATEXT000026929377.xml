<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026929377</ID>
<ANCIEN_ID>JG_L_2013_01_000000351393</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/92/93/CETATEXT000026929377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 11/01/2013, 351393, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351393</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:351393.20130111</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 juillet et 28 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement public du musée du Quai Branly, dont le siège est 222 rue de l'Université à Paris cedex 07 (75343) ; l'établissement public demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05139-09PA05140 du 26 avril 2011 par lequel la cour administrative d'appel de Paris a rejeté ses requêtes tendant à l'annulation des jugements n° 0616041/3-2 et n° 0616040/3-2 du 3 juin 2009, par lesquels le tribunal administratif de Paris a rejeté ses demandes tendant à la condamnation de la société Axa France Iard à lui verser la somme de 593 677,22 euros HT au titre de la mise en oeuvre de la garantie " tous risques chantier " pour les dommages relatifs à deux sinistres subis en 2005 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la société Axa France Iard le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des assurances ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'établissement public du musée du Quai Branly, et de la SCP Boutet, avocat de la société Axa France Iard,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'établissement public du musée du Quai Branly, et à la SCP Boutet, avocat de la société Axa France Iard ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'établissement public du musée du Quai Branly a souscrit auprès de la société Axa France Iard une assurance " tous risques chantier " visant à garantir, durant les travaux de construction du musée, " les frais de réparation consécutifs à toutes pertes y compris vols ou dommages " susceptibles d'affecter ses biens ; que, d'une part, des dommages ont été causés à la façade vitrée du musée par des travaux réalisés au mois de mars 2005 ; que, d'autre part, le chantier a subi d'importants dommages en raison d'un violent orage survenu au mois de juin 2005 ; que la société Axa France Iard a refusé d'indemniser l'établissement public au motif que ces deux sinistres trouvaient leur origine dans une négligence du musée et étaient de ce fait dépourvus de caractère aléatoire ; que par l'arrêt attaqué, la cour administrative d'appel de Paris a rejeté les conclusions de l'établissement public tendant à l'annulation des jugements du tribunal administratif de Paris rejetant ses demandes tendant à la condamnation de la société Axa France Iard à l'indemniser des dommages causés par les sinistres subis en mars et juin 2005, au motif que l'établissement public n'établissait pas l'étendue de son préjudice ; <br/>
<br/>
              2. Considérant, d'une part, que la cour administrative d'appel de Paris, en écartant le caractère probant de documents produits par l'établissement public du musée du Quai Branly pour établir son préjudice au seul motif qu'ils étaient " partiellement " mis en cause par un rapport d'expertise, sans même tenir compte des éléments qu'elle estimait ne pas être remis en cause, a commis une erreur de droit ; que, d'autre part, en rejetant " en l'état " la demande de l'établissement public sans procéder à une quelconque mesure d'instruction et en relevant qu'il appartenait à cette personne publique de mettre la juridiction de première instance en situation de disposer d'éléments de preuve " incontestés ", la cour a méconnu son office dès lors qu'il lui appartenait d'apprécier le bien fondé des prétentions du requérant au vu des pièces produites et de diligenter les éventuelles mesures d'instruction qui lui semblaient nécessaires ; que, par ailleurs, la cour a également commis une erreur de droit en exigeant de l'établissement public la production de factures dès lors que l'assuré n'est pas, sauf clause particulière, tenu de procéder aux réparations pour percevoir l'indemnité due par l'assureur ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'établissement public du musée du Quai Branly qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la société Axa France Iard ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 26 avril 2011 de la cour administrative d'appel de Paris est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris y compris les conclusions de l'établissement public du musée du Quai Branly présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par la société Axa France Iard au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'établissement public du musée du Quai Branly et à la société Axa France Iard.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
