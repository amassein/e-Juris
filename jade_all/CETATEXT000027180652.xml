<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027180652</ID>
<ANCIEN_ID>JG_L_2013_03_000000360277</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/18/06/CETATEXT000027180652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 15/03/2013, 360277, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360277</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP ROGER, SEVAUX</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360277.20130315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société à responsabilité limitée (SARL) Quickdream, dont le siège est Oyster Pond, lot 28 à Saint-Martin (97150), et le syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria, dont le siège est Oyster Pond, lot 29 à Saint-Martin (97150) ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0900085 du 22 mars 2012 par lequel le tribunal administratif de Saint-Martin a déclaré qu'il n'y avait pas lieu de statuer sur le recours en appréciation de la légalité du permis de construire délivré le 12 décembre 1997 par la commune de Saint-Martin à la société Ajusara ; <br/>
<br/>
              2°) de déclarer que ce permis de construire est entaché d'illégalité ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Martin et de la société Ajusara le versement de la somme de 6 000 euros à chacun des requérants au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 février 2013, présentée par la SARL Quickdream et le syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Roger, Sevaux, avocat de la collectivité d'outre-mer de Saint-Martin et de la SCP Célice, Blancpain, Soltner, avocat de la société Ajusara,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Roger, Sevaux, avocat de la collectivité d'outre-mer de Saint-Martin et à la SCP Célice, Blancpain, Soltner, avocat de la société Ajusara ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article R. 611-7 du code de justice administrative : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement (...) en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué. (...) " ;<br/>
<br/>
              2. Considérant que la circonstance que le courrier du 23 février 2012 par lequel le président de la formation de jugement a informé les parties de ce que la décision était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce qu'il n'y avait plus lieu de statuer sur l'appréciation de la légalité du permis de construire délivré à la société Ajusara mentionnait à tort un permis du 6 novembre 1997 et que les requérants n'auraient pas reçu la lettre rectifiant cette erreur de date est sans incidence sur la régularité du jugement attaqué, dès lors qu'ils ne pouvaient se méprendre sur la portée du moyen ainsi mentionné ;<br/>
<br/>
              3. Considérant que le mémoire produit par la société Ajusara le 29 février 2012 devant le tribunal administratif de Saint-Martin répondait au courrier du 23 février 2012 par lequel le président de ce tribunal avait informé les parties de ce que la décision du tribunal était susceptible d'être fondée sur un moyen relevé d'office ; que, par suite, et en application des termes mêmes de l'article R. 611-7 cité ci-dessus, le tribunal devait, ainsi qu'il l'a fait, viser et analyser ce mémoire alors même que celui-ci lui était parvenu après la clôture de l'instruction ; que les requérants ne sont, par suite, pas fondés à soutenir que ces mentions entachent le jugement attaqué d'irrégularité ;<br/>
<br/>
              4. Considérant que le tribunal a, de la même manière, visé et analysé les observations produites par la SARL Quickdream et le syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria, le 27 février 2012, en réponse au même courrier du 23 février 2012 du président du tribunal administratif de Saint-Martin ; que le moyen des requérants tiré de ce que le jugement attaqué serait irrégulier faute d'avoir tenu compte des observations qu'ils ont présentées le 27 février 2012 ne peut qu'être écarté ; <br/>
<br/>
              5. Considérant que si le jugement attaqué du tribunal administratif de Saint-Martin se fonde sur le moyen relevé d'office par ce tribunal, cette seule circonstance ne permet pas de regarder ce jugement comme s'étant fondé sur la réponse apportée le 29 février 2012 par la société Ajusara, celle-ci s'étant d'ailleurs bornée à répondre au moyen relevé d'office par le tribunal en le reprenant à son compte, sans ajouter d'élément nouveau ; que, dès lors, la circonstance que ce mémoire, communiqué à la collectivité d'outre-mer de Saint-Martin, n'aurait pas été communiqué aux autres parties, n'est pas, en tout état de cause, de nature à entacher d'irrégularité le jugement ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que, par un arrêt avant-dire-droit du 5 novembre 2007, la cour d'appel de Basse-Terre, saisie par la SARL Quickdream et le syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria d'une action en démolition d'un bâtiment appartenant à la SARL Ajusara, a sursis à statuer et renvoyé au juge administratif l'appréciation de la légalité du permis de construire du bâtiment en litige ; que toutefois, par un arrêt ultérieur du 7 septembre 2009, confirmé par la Cour de cassation le 11 janvier 2011 et devenu irrévocable, la même cour a déclaré irrecevable l'action en démolition de la société Quickdream et du syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria, et dit n'y avoir plus lieu à question préjudicielle devant le juge administratif ; que l'obligation de statuer à laquelle est soumise le juge administratif lorsqu'il est saisi sur renvoi de l'autorité judiciaire cesse dans le cas où le jugement de renvoi a été définitivement infirmé ; que, par suite, c'est sans méconnaître son office ni commettre d'erreur de droit que le tribunal administratif de Saint-Martin a prononcé un non-lieu à statuer sur la demande en appréciation de légalité dont il était saisi ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de la SARL Quickdream et du syndicat des copropriétaires de l'ensemble immobilier Santa Maria doit être rejetée, y compris leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Ajusara et par la collectivité d'outre-mer de Saint-Martin au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SARL Quickdream et du syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria est rejetée.<br/>
Article 2 : Les conclusions présentées par la société Ajusara et par la collectivité de Saint-Martin au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la SARL Quickdream, au syndicat des copropriétaires de l'ensemble immobilier dénommé Santa Maria, à la collectivité d'outre-mer de Saint-Martin et à la société Ajusara.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
