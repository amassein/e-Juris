<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036912404</ID>
<ANCIEN_ID>JG_L_2018_05_000000410790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/91/24/CETATEXT000036912404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 04/05/2018, 410790</TITRE>
<DATE_DEC>2018-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410790.20180504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...C...et Mme D...B...ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir les arrêtés du 1er octobre 2013 et du 3 mars 2014 par lesquels le maire de Bouc Bel Air a refusé de leur délivrer un permis de construire pour un projet de construction de logements collectifs. Par un jugement nos 1307206, 1403160 du 22 décembre 2014, le tribunal administratif de Marseille a prononcé un non-lieu à statuer sur les conclusions tendant à l'annulation de l'arrêté du 1er octobre 2013 et annulé l'arrêté du 3 mars 2014.<br/>
<br/>
              Par un arrêt n° 15MA00964 du 23 mars 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Bouc Bel Air contre ce jugement.  <br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 23 mai et 31 août 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Bouc Bel Air demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme C...et Mme B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du patrimoine ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 2001-492 du 6 juin 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Bouc Bel Air et à la SCP Célice, Soltner, Texidor, Perier, avocat de Mme C...et autre.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, saisi par Mme C...et Mme B...d'une demande de permis de construire portant sur un projet de réalisation de 7 bâtiments et 91 logements situé dans le champ de visibilité du jardin d'Albertas dit " jardin d'en haut ", classé au titre des monuments historiques, le maire de Bouc Bel Air (Bouches-du-Rhône) a, après l'avis défavorable émis par l'architecte des Bâtiments de France le 29 août 2013, refusé le permis sollicité par un arrêté du 1er octobre 2013 ; que les intéressées ont alors saisi, le 12 novembre 2013, le préfet de la région Provence-Alpes-Côte d'Azur d'un recours contre l'avis défavorable de l'architecte des Bâtiments de France ; que le préfet leur a demandé, par une lettre du 5 décembre 2013, de lui transmettre le dossier complet de la demande de permis de construire afin de pouvoir se prononcer ce recours ; que ce dossier a été reçu à la préfecture le 30 décembre 2013 ; que le préfet a confirmé l'avis défavorable de l'architecte des Bâtiments de France le 28 février 2014 ; que, par un arrêté du 3 mars 2014, le maire a confirmé son refus de délivrer le permis sollicité ; que, par un jugement du 22 décembre 2014, le tribunal administratif de Marseille a prononcé un non-lieu à statuer sur les conclusions dirigées contre l'arrêté du 1er octobre 2013 du maire de Bouc Bel Air et a, en revanche, annulé son arrêté du 3 mars 2014 ; que par un arrêt du 23 mars 2017, contre lequel la commune se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement et lui a enjoint de délivrer aux pétitionnaires un certificat de permis de construire tacite ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 621-31 du code du patrimoine, dans sa rédaction alors en vigueur : " Lorsqu'un immeuble est adossé à un immeuble classé ou situé dans le champ de visibilité d'un édifice classé ou inscrit au titre des monuments historiques, il ne peut faire l'objet, tant de la part des propriétaires privés que des collectivités et établissements publics, d'aucune construction nouvelle, d'aucune démolition, d'aucun déboisement, d'aucune transformation ou modification de nature à en affecter l'aspect, sans une autorisation préalable. / (...) / La même autorisation est nécessaire lorsque l'immeuble est situé dans le champ de visibilité d'un parc ou d'un jardin classé ou inscrit ne comportant pas d'édifice, si le périmètre de protection de ce parc ou de ce jardin a été délimité dans les conditions fixées aux cinquième ou sixième alinéas de l'article L. 621-30. / (...) " ; que l'article L. 621-32 du code du patrimoine dispose que : " I.- Le permis de construire (...) tient lieu de l'autorisation prévue au premier alinéa de l'article L. 621-31 si l'architecte des Bâtiments de France a donné son accord. / En cas de désaccord soit du maire ou de l'autorité administrative compétente pour délivrer le permis de construire (...), soit du pétitionnaire avec l'avis émis par l'architecte des Bâtiments de France, le représentant de l'Etat dans la région émet, après consultation de la section de la commission régionale du patrimoine et des sites, un avis qui se substitue à celui de l'architecte des Bâtiments de France. Le recours du pétitionnaire s'exerce à l'occasion du refus d'autorisation (...). Si le représentant de l'Etat dans la région exprime son désaccord à l'encontre de l'avis de l'architecte des Bâtiments de France, le maire ou l'autorité administrative compétente peut délivrer le permis de construire (...) initialement refusé (...). En l'absence de décision expresse du représentant de l'Etat dans la région dans le délai de deux mois à compter de sa saisine par le maire, l'autorité administrative compétente ou le pétitionnaire, le recours est réputé admis. / Le délai de saisine du représentant de l'Etat dans la région ainsi que les délais impartis au maire ou à l'autorité administrative compétente pour statuer sont fixés par décret " ; que l'article  R. 424-14 du code de l'urbanisme dispose que : " (...) le demandeur peut, en cas ( ...) de refus de permis fondé sur une opposition de l'architecte des Bâtiments de France, saisir le préfet de région, par lettre recommandée avec demande d'avis de réception, d'un recours contre cette décision dans le délai de deux mois à compter de la notification de l'opposition ou du refus. / Le préfet de région adresse notification de la demande dont il est saisi au maire et à l'autorité compétente en matière de permis. / Les dispositions des premier à cinquième et huitième à douzième alinéas de l'article R. * 423-68 et celles de l'article R. * 423-68-1 sont applicables au recours du demandeur. / Si le préfet de région (...) infirme l'avis de l'architecte des Bâtiments de France, le maire ou l'autorité compétente doit statuer à nouveau dans le délai d'un mois suivant la réception du nouvel avis ou suivant la date à laquelle est intervenue l'admission tacite du recours " ; que selon l'article R. 423-68 du même code, dans sa rédaction alors en vigueur : " Le délai à l'issue duquel le préfet de région doit se prononcer sur un recours (...) contre l'avis émis par l'architecte des Bâtiments de France est : / (...) c) De deux mois lorsque l'avis porte sur des travaux situés (...) dans le champ de visibilité d'un monument historique défini à l'article L. 621-30-1 du code du patrimoine. / En l'absence de décision expresse du préfet de région à l'issue du délai mentionné aux alinéas précédents, le recours est réputé admis (...) " ; <br/>
<br/>
              3. Considérant, d'une part, qu'il résulte des dispositions qui viennent d'être citées que le pétitionnaire doit, avant de former un recours pour excès de pouvoir contre un refus de permis de construire portant sur un immeuble situé dans le champ de visibilité d'un édifice classé ou inscrit et faisant suite à un avis négatif de l'architecte des Bâtiments de France, saisir le préfet de région d'une contestation de cet avis ; que l'avis émis par le préfet, qu'il soit exprès ou tacite, se substitue à celui de l'architecte des Bâtiments de France ; que, lorsque le préfet infirme l'avis défavorable de l'architecte des Bâtiments de France, l'autorité compétente doit statuer à nouveau sur la demande de permis de construire dans un délai d'un mois à compter de la réception du nouvel avis, cette nouvelle décision se substituant alors au refus de permis de construire précédemment opposé ; que, lorsque le préfet confirme l'avis défavorable de l'architecte des Bâtiments de France, l'autorité compétente n'a pas à se prononcer à nouveau sur la demande de permis de construire et le délai de recours contentieux contre le refus de permis de construire court à compter de la notification de la décision du préfet confirmant l'avis défavorable de l'architecte des Bâtiments de France ; que si l'autorité compétente prend néanmoins une nouvelle décision de refus, cette dernière est purement confirmative du refus initialement opposé ;<br/>
<br/>
              4. Considérant, d'autre part, que lorsqu'un recours formé en application des dispositions qui viennent d'être rappelées contre l'avis défavorable de l'architecte des Bâtiments de France ne comporte pas le dossier complet de la demande de permis de construire, qui est seul de nature à mettre le préfet de région à même de se prononcer sur le recours dont il est saisi, il appartient au préfet d'inviter le pétitionnaire à compléter ce dossier, dans le délai qu'il fixe, et d'en informer l'autorité d'urbanisme compétente pour statuer sur la demande de permis de construire ; que le délai au terme duquel le recours est réputé admis, en vertu de l'article R. 423-68 du code de l'urbanisme, est alors interrompu et ne recommence à courir qu'à compter de la réception des pièces requises, conformément à l'article 2 du décret du 6 juin 2001, repris à l'article L. 114-5 du code des relations entre le public et les administrations ; <br/>
<br/>
              5. Considérant que la cour a jugé que l'invitation faite par le préfet aux intéressées de compléter le dossier du recours dont elle l'avait saisi n'avait pu avoir pour effet d'interrompre le délai prévu à l'article R. 423-68 du code de l'urbanisme et qu'ainsi, un avis favorable tacite du préfet de région sur le projet était né et s'était substitué à l'avis défavorable de l'architecte des Bâtiments de France ; qu'elle en a déduit que, l'avis tacite du préfet ayant infirmé celui de l'architecte des Bâtiments de France, l'autorité compétente pour délivrer le permis de construire était tenue de se prononcer à nouveau sur la demande et que, faute de l'avoir fait dans le délai d'un mois imparti par l'article R. 424-14 du code de l'urbanisme, un permis de construire tacite était né ; que, par voie de conséquence, elle a jugé qu'il n'y avait plus lieu de statuer sur les conclusions dirigées contre le refus initial opposé à la demande, le permis tacite s'y étant substitué ; qu'en se prononçant ainsi, la cour a entaché son arrêt d'erreur de droit au regard des règles rappelées aux points 3 et 4 de la présente décision ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Bouc Bel Air est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Bouc Bel Air, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C...et Mme B...la somme globale de 3 000 euros à verser à la commune de Bouc Bel Air au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 mars 2017 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Mmes C...et B...verseront une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de Mmes C...et B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Bouc Bel Air, à Mme A...C...et à Mme D...B....<br/>
Copie en sera adressée au ministre de la cohésion des territoires et à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-05-03 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. MESURES APPLICABLES AUX IMMEUBLES SITUÉS DANS LE CHAMP DE VISIBILITÉ D'UN ÉDIFICE CLASSÉ OU INSCRIT. PERMIS DE CONSTRUIRE. - CONTESTATION DANS LE CADRE D'UN RAPO DEVANT LE PRÉFET DE RÉGION DE L'AVIS DÉFAVORABLE DE L'ABF - 1) OFFICE DE L'AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE DE PERMIS DE CONSTRUIRE EN FONCTION DE L'ISSUE DU RAPO - A) INFIRMATION DE L'AVIS DE L'ABF - OBLIGATION DE STATUER À NOUVEAU - EXISTENCE - B) CONFIRMATION DE L'AVIS DE L'ABF - OBLIGATION DE STATUER À NOUVEAU - ABSENCE - CONSÉQUENCE - POINT DE DÉPART DU DÉLAI DE RECOURS CONTRE LE REFUS DE PERMIS - DATE DE NOTIFICATION DE LA DÉCISION DU PRÉFET CONFIRMANT L'AVIS DE L'ABF - 2) INSTRUCTION DU RAPO PAR LE PRÉFET DE RÉGION - FACULTÉ DE DEMANDER DES PIÈCES COMPLÉMENTAIRES - EXISTENCE, SI LE DOSSIER N'EST PAS COMPLET ET SOUS RÉSERVE D'EN INFORMER L'AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE DE PERMIS - CONSÉQUENCE SUR LE DÉLAI AU TERME DUQUEL LE RAPO EST RÉPUTÉ ADMIS - INTERRUPTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - PERMIS DE CONSTRUIRE SOUMIS À AVIS FAVORABLE DE L'ABF - CONTESTATION DANS LE CADRE D'UN RAPO DEVANT LE PRÉFET DE RÉGION DE L'AVIS DÉFAVORABLE DE L'ABF - 1) OFFICE DE L'AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE DE PERMIS DE CONSTRUIRE EN FONCTION DE L'ISSUE DU RAPO - A) INFIRMATION DE L'AVIS DE L'ABF - OBLIGATION DE STATUER À NOUVEAU - EXISTENCE - B) CONFIRMATION DE L'AVIS DE L'ABF - OBLIGATION DE STATUER À NOUVEAU - ABSENCE - CONSÉQUENCE - POINT DE DÉPART DU DÉLAI DE RECOURS CONTRE LE REFUS DE PERMIS - DATE DE NOTIFICATION DE LA DÉCISION DU PRÉFET CONFIRMANT L'AVIS DE L'ABF - 2) INSTRUCTION DU RAPO PAR LE PRÉFET DE RÉGION - FACULTÉ DE DEMANDER DES PIÈCES COMPLÉMENTAIRES - EXISTENCE, SI LE DOSSIER N'EST PAS COMPLET ET SOUS RÉSERVE D'EN INFORMER L'AUTORITÉ COMPÉTENTE POUR STATUER SUR LA DEMANDE DE PERMIS - CONSÉQUENCE SUR LE DÉLAI AU TERME DUQUEL LE RAPO EST RÉPUTÉ ADMIS - INTERRUPTION.
</SCT>
<ANA ID="9A"> 41-01-05-03 1) Il résulte des articles L. 621-31, L. 621-32 (alors en vigueur) du code du patrimoine et R. 424-14 et R. 423-68 du code de l'urbanisme que le pétitionnaire doit, avant de former un recours pour excès de pouvoir contre un refus de permis de construire portant sur un immeuble situé dans le champ de visibilité d'un édifice classé ou inscrit et faisant suite à un avis négatif de l'architecte des Bâtiments de France (ABF), saisir le préfet de région d'une contestation de cet avis. L'avis émis par le préfet, qu'il soit exprès ou tacite, se substitue à celui de l'ABF.... ,,a) Lorsque le préfet infirme l'avis défavorable de l'ABF, l'autorité compétente doit statuer à nouveau sur la demande de permis de construire dans un délai d'un mois à compter de la réception du nouvel avis, cette nouvelle décision se substituant alors au refus de permis de construire précédemment opposé....  ,,b) Lorsque le préfet confirme l'avis défavorable de l'ABF, l'autorité compétente n'a pas à se prononcer à nouveau sur la demande de permis de construire et le délai de recours contentieux contre le refus de permis de construire court à compter de la notification de la décision du préfet confirmant l'avis défavorable de l'ABF. Si l'autorité compétente prend néanmoins une nouvelle décision de refus, cette dernière est purement confirmative du refus initialement opposé.... ,,2) Lorsqu'un recours formé contre l'avis défavorable de l'ABF ne comporte pas le dossier complet de la demande de permis de construire, qui est seul de nature à mettre le préfet de région à même de se prononcer sur le recours dont il est saisi, il appartient au préfet d'inviter le pétitionnaire à compléter ce dossier, dans le délai qu'il fixe, et d'en informer l'autorité d'urbanisme compétente pour statuer sur la demande de permis de construire. Le délai au terme duquel le recours est réputé admis, en vertu de l'article R. 423-68 du code de l'urbanisme, est alors interrompu et ne recommence à courir qu'à compter de la réception des pièces requises, conformément à l'article 2 du décret n° 2001-492 du 6 juin 2001, repris à l'article L. 114-5 du code des relations entre le public et les administrations (CRPA).</ANA>
<ANA ID="9B"> 54-01-02-01 1) Il résulte des articles L. 621-31, L. 621-32 (alors en vigueur) du code du patrimoine et R. 424-14 et R. 423-68 du code de l'urbanisme que le pétitionnaire doit, avant de former un recours pour excès de pouvoir contre un refus de permis de construire portant sur un immeuble situé dans le champ de visibilité d'un édifice classé ou inscrit et faisant suite à un avis négatif de l'architecte des Bâtiments de France (ABF), saisir le préfet de région d'une contestation de cet avis. L'avis émis par le préfet, qu'il soit exprès ou tacite, se substitue à celui de l'ABF.... ,,a) Lorsque le préfet infirme l'avis défavorable de l'ABF, l'autorité compétente doit statuer à nouveau sur la demande de permis de construire dans un délai d'un mois à compter de la réception du nouvel avis, cette nouvelle décision se substituant alors au refus de permis de construire précédemment opposé....  ,,b) Lorsque le préfet confirme l'avis défavorable de l'ABF, l'autorité compétente n'a pas à se prononcer à nouveau sur la demande de permis de construire et le délai de recours contentieux contre le refus de permis de construire court à compter de la notification de la décision du préfet confirmant l'avis défavorable de l'ABF. Si l'autorité compétente prend néanmoins une nouvelle décision de refus, cette dernière est purement confirmative du refus initialement opposé.... ,,2) Lorsqu'un recours formé contre l'avis défavorable de l'ABF ne comporte pas le dossier complet de la demande de permis de construire, qui est seul de nature à mettre le préfet de région à même de se prononcer sur le recours dont il est saisi, il appartient au préfet d'inviter le pétitionnaire à compléter ce dossier, dans le délai qu'il fixe, et d'en informer l'autorité d'urbanisme compétente pour statuer sur la demande de permis de construire. Le délai au terme duquel le recours est réputé admis, en vertu de l'article R. 423-68 du code de l'urbanisme, est alors interrompu et ne recommence à courir qu'à compter de la réception des pièces requises, conformément à l'article 2 du décret n° 2001-492 du 6 juin 2001, repris à l'article L. 114-5 du code des relations entre le public et les administrations (CRPA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
