<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043243772</ID>
<ANCIEN_ID>JG_L_2021_03_000000430623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/37/CETATEXT000043243772.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/03/2021, 430623, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET ; SCP CELICE, TEXIDOR, PERIER ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:430623.20210310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... E..., Mme I... E..., M. A... B..., Mme N... G... L..., Mme M... F... D..., M. K... H... et Mme J... H... ont demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir l'arrêté du 10 novembre 2016 par lequel le maire de Chanteloup-en-Brie a délivré à la société Severini Pierres et Loisirs un permis de construire, ainsi que les décisions rejetant leurs recours gracieux. Par un jugement n° 1702814, 1702816, 1702817 du 31 décembre 2018, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 mai et 12 août au secrétariat du contentieux du Conseil d'Etat, M. E... et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Chanteloup-en-Brie la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Colin - Stoclet, avocat de M. et Mme E... et autres, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la commune de Chanteloup-en-Brie et à la SCP Célice, Texidor, Périer, avocat de la société Severini Pierres et Loisirs ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 février 2021, présentée par la société Severini Pierres et Loisirs ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 10 novembre 2016, le maire de Chanteloup-en-Brie (Seine-et-Marne) a accordé à la société Severini Pierres et Loisirs un permis de construire pour 36 logements. M. E... et autres se pourvoient en cassation contre le jugement du 31 décembre 2018 par lequel le tribunal administratif de Melun a rejeté leur demande d'annulation de ce permis de construire. <br/>
<br/>
              2. Les désistements de M. B..., Mme G... L... et Mme F... D... sont purs et simples. Rien ne s'oppose à ce qu'il en soit donné acte. <br/>
<br/>
              3. L'article U 2.11 du règlement du plan local d'urbanisme de la commune de Chanteloup-en-Brie dispose que : " 2. Eclairement des combles. L'éclairement éventuel des combles doit être assuré par des lucarnes à deux pentes ou à capucines. (...) 3. Hauteurs des façades, nombre de niveaux habitables. (...) Les constructions n'auront pas plus de 3 niveaux habitables (RdC, 1er étage, combles aménagés ".<br/>
<br/>
              4. Il résulte des termes du jugement attaqué que, pour écarter le moyen tiré de ce que le projet autorisé par le permis en litige méconnaissait ces dispositions, le tribunal administratif a jugé que l'existence d'un éventuel préjudice de vue sur la parcelle appartenant à M. et Mme E... était sans incidence sur la légalité du permis de construire. En statuant ainsi, alors qu'il était également soutenu devant lui que le troisième niveau des constructions projetées comportait, pour certains lots, des balcons et fenêtres dont la présence méconnaissait ces mêmes dispositions, le tribunal administratif a insuffisamment motivé son jugement.<br/>
<br/>
              5. M. E... et autres sont, par suite, fondés à demander l'annulation du jugement qu'ils attaquent, sans qu'il soit besoin de se prononcer sur les autres moyens de leur pourvoi. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de M. E... et autres, qui ne sont pas, dans la présente instance, les parties perdantes, les sommes que demandent, à ce titre, la commune de Chanteloup-en-Brie et la société Severini Pierres et Loisirs. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Chanteloup-en-Brie la somme de 800 euros chacun à verser, au même titre, à M. et Mme E... et à M. et Mme H.... <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte des désistements d'instance de M. B..., Mme N... G... L... et Mme F... D....<br/>
<br/>
Article 2 : Le jugement du 31 décembre 2018 du tribunal administratif de Melun est annulé.<br/>
<br/>
		Article 3 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
<br/>
Article 4 : La commune de Chanteloup-en-Brie versera la somme de 800 euros chacun à M. E..., à Mme E..., à M. H... et à Mme H... au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par la commune de Chanteloup-en-Brie et par la société Severini Pierre et Loisirs sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. C... E..., premier requérant dénommé, à la commune de Chanteloup-en-Brie et à la société Severini Pierre et Loisirs.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
