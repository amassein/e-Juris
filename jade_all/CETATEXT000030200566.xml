<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200566</ID>
<ANCIEN_ID>JG_L_2015_01_000000374070</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200566.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 16/01/2015, 374070</TITRE>
<DATE_DEC>2015-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374070</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374070.20150116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 18 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1308311 du 30 octobre 2013 par laquelle le juge des référés du tribunal administratif de Nantes a rejeté, en application de l'article L. 522-3 du code de justice administrative, sa demande tendant à ce que soient ordonnées toutes mesures nécessaires à la sauvegarde de sa liberté d'expression, à laquelle l'arrêté du 19 septembre 2013 du maire de la commune de Batz-sur-Mer la mettant en demeure de supprimer les dispositifs apposés sur les murs de sa propriété porte atteinte ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Batz-sur-Mer le versement à la SCP Boulloche d'une somme de 3 000 euros sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de Mme A...et à la SCP Odent, Poulet, avocat de la commune de Batz-sur-Mer ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ; qu'aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat (...) le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est (...) présenté dans un écrit distinct et motivé " ; que l'article 23-3 de cette ordonnance prévoit qu'une juridiction saisie d'une question prioritaire de constitutionnalité " peut prendre les mesures provisoires ou conservatoires nécessaires " ;<br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions organiques avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge des référés d'un tribunal administratif statuant sur des conclusions qui lui sont présentées sur le fondement de l'article L. 521-2 de ce code ; que le juge des référés peut en toute hypothèse, par une ordonnance prise sur le fondement de l'article L. 522-3, rejeter les conclusions présentées sur le fondement de l'article L. 521-2 pour incompétence de la juridiction administrative, irrecevabilité ou pour défaut d'urgence, sans être alors tenu d'examiner la question prioritaire de constitutionnalité soulevée devant lui ; qu'en revanche, si le juge des référés envisage de rejeter la demande en vertu de l'article L. 522-3 au motif qu'il apparaît manifeste qu'elle est mal fondée, il lui appartient dans cette hypothèse de se prononcer sur l'ensemble des moyens soulevés devant lui, y compris celui tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces de la procédure devant le tribunal administratif de Nantes qu'à l'appui de sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative, Mme A...avait présenté, par un mémoire distinct enregistré le 25 octobre 2013 au greffe du tribunal administratif, une question prioritaire de constitutionnalité portant sur les dispositions de l'article 3 de la loi du 29 décembre 1979 relative à la publicité ; que, par l'ordonnance attaquée du 30 octobre 2013, le juge des référés du tribunal administratif de Nantes a rejeté cette demande, sur le fondement des dispositions de l'article L. 522-3 du même code, pour absence d'atteinte grave et manifestement illégale à une liberté fondamentale : que ce faisant, il a nécessairement jugé, non que la demande était irrecevable, mais qu'elle était mal fondée ; qu'en statuant ainsi, sans se prononcer sur la question prioritaire de constitutionnalité dont il était saisi, le juge des référés a méconnu les devoirs de son office ; qu'il en résulte que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, cette ordonnance doit être annulée ;<br/>
<br/>
              5. Considérant  qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant que l'usage, par le juge des référés, des pouvoirs qu'il tient des dispositions de l'article L. 521-2 du code de justice administrative est subordonné à la condition qu'une urgence particulière rende nécessaire l'intervention dans les quarante-huit heures d'une décision destinée à la sauvegarde d'une liberté fondamentale ;<br/>
<br/>
              7. Considérant qu'à l'appui de sa demande dirigée contre l'arrêté municipal litigieux du 24 septembre 2013 la mettant en demeure d'enlever les dispositifs publicitaires apposés sur les murs de sa propriété, Mme A...ne justifie pas d'une situation d'urgence caractérisée, impliquant qu'une mesure visant à sauvegarder une liberté fondamentale soit prise dans les quarante-huit heures ; que, par suite, et sans qu'il y ait lieu de statuer sur la demande de renvoi au Conseil constitutionnel d'une question prioritaire de constitutionnalité, la demande de Mme A...doit être rejetée ; qu'il en va de même, par voie de conséquence, de ses conclusions présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1308311 du juge des référés du tribunal administratif de Nantes du 30 octobre 2013 est annulée.<br/>
<br/>
Article 2 : La demande de Mme A...devant le juge des référés du tribunal administratif de Nantes et le surplus des conclusions de son pourvoi sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Madame B...A...et à la commune de Batz-sur-Mer.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-01-05 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. QUESTIONS COMMUNES. REJET DE LA DEMANDE SANS PROCÉDURE CONTRADICTOIRE (ART. L. 522-3 DU CODE DE JUSTICE ADMINISTRATIVE). - QPC POSÉE DEVANT LE JUGE DU RÉFÉRÉ-LIBERTÉ (ART. L. 521-2 DU CJA) [RJ1] - RECOURS À LA PROCÉDURE DE L'ARTICLE L. 522-3 - OBLIGATION D'EXAMINER LA QPC, EN FONCTION DU MOTIF DE REJET - INCOMPÉTENCE DE L'ORDRE ADMINISTRATIF, IRRECEVABILITÉ OU DÉFAUT D'URGENCE - ABSENCE - REQUÊTE MANIFESTEMENT MAL FONDÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - QPC POSÉE DEVANT LE JUGE DU RÉFÉRÉ-LIBERTÉ (ART. L. 521-2 DU CJA) [RJ1] - RECOURS À LA PROCÉDURE DE REJET SANS INSTRUCTION CONTRADICTOIRE (ART. L. 522-3 DU CJA) - OBLIGATION D'EXAMINER LA QPC, EN FONCTION DU MOTIF DE REJET - INCOMPÉTENCE DE L'ORDRE ADMINISTRATIF, IRRECEVABILITÉ OU DÉFAUT D'URGENCE - ABSENCE - REQUÊTE MANIFESTEMENT MAL FONDÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-035-01-05 Il résulte de la combinaison des dispositions des articles 23-1 et 23-3 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel avec celles du livre V du code de justice administrative (CJA) qu'une question prioritaire de constitutionnalité (QPC) peut être soulevée devant le juge des référés d'un tribunal administratif statuant sur des conclusions qui lui sont présentées sur le fondement de l'article L. 521-2 de ce code.,,,Le juge des référés peut en toute hypothèse, par une ordonnance prise sur le fondement de l'article L. 522-3, rejeter les conclusions présentées sur le fondement de l'article L. 521-2 pour incompétence de la juridiction administrative, irrecevabilité ou pour défaut d'urgence, sans être alors tenu d'examiner la question prioritaire de constitutionnalité soulevée devant lui,,,En revanche, si le juge des référés envisage de rejeter la demande en vertu de l'article L. 522-3 au motif qu'il apparaît manifeste qu'elle est mal fondée, il lui appartient dans cette hypothèse de se prononcer sur l'ensemble des moyens soulevés devant lui, y compris celui tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution.</ANA>
<ANA ID="9B"> 54-10 Il résulte de la combinaison des dispositions des articles 23-1 et 23-3 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel avec celles du livre V du code de justice administrative (CJA) qu'une question prioritaire de constitutionnalité (QPC) peut être soulevée devant le juge des référés d'un tribunal administratif statuant sur des conclusions qui lui sont présentées sur le fondement de l'article L. 521-2 de ce code.,,,Le juge des référés peut en toute hypothèse, par une ordonnance prise sur le fondement de l'article L. 522-3, rejeter les conclusions présentées sur le fondement de l'article L. 521-2 pour incompétence de la juridiction administrative, irrecevabilité ou pour défaut d'urgence, sans être alors tenu d'examiner la question prioritaire de constitutionnalité soulevée devant lui,,,En revanche, si le juge des référés envisage de rejeter la demande en vertu de l'article L. 522-3 au motif qu'il apparaît manifeste qu'elle est mal fondée, il lui appartient dans cette hypothèse de se prononcer sur l'ensemble des moyens soulevés devant lui, y compris celui tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, juge des référés, 16 juin 2010,,, n° 340250, p. 205.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
