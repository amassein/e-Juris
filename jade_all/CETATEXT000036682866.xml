<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682866</ID>
<ANCIEN_ID>JG_L_2018_03_000000417370</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682866.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 07/03/2018, 417370, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417370</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417370.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...D...a demandé au juge des référés du tribunal administratif de Toulouse d'ordonner, sur le fondement de l'article L. 521-2 du code de justice administrative, la suspension de l'exécution de la décision du 2 octobre 2017 par laquelle le chef d'établissement du centre de détention de Muret a suspendu, pour une durée de six mois, à compter du 24 septembre 2017, le permis de visite accordé à Mme A...C..., ensemble la décision du 30 novembre 2017 par laquelle le directeur interrégional des services pénitentiaires de Toulouse a confirmé, sur recours hiérarchique, la décision du chef d'établissement. <br/>
<br/>
              Par une ordonnance n° 1705702 du 13 décembre 2017, le juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 522-3 du code de justice administrative, a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 16 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2500 euros à verser à la SCP Capron, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de M. B...D...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que par une décision du 30 novembre 2017, le directeur interrégional des services pénitentiaires de Toulouse a confirmé, sur recours hiérarchique, la décision du 2 octobre 2017 par laquelle le chef d'établissement du centre de détention de Muret a suspendu, pour une durée de six mois, le permis délivré à Mme A...C...pour rendre visite à M. B... D... à raison de la découverte d'un billet de 50 euros sur ce dernier, à l'issue d'un parloir avec sa compagne. Par une ordonnance du 13 décembre 2017, le juge des référés du tribunal administratif de Toulouse, statuant sur le fondement de l'article L. 522-3 du code de justice administrative, a rejeté la demande de M. D...tendant à la suspension de l'exécution de ces deux décisions. M. D... se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              3. Pour rejeter comme manifestement mal fondée la demande de M. D...sur le fondement de l'article L. 522-3 du code de justice administrative, le juge des référés du tribunal administratif de Toulouse a jugé, après avoir relevé que la mesure de suspension litigieuse constituait une mesure de police administrative appropriée pour remédier au trouble à l'ordre public au sein de l'établissement pénitentiaire suscité par le comportement du requérant et de sa compagne, que cette mesure ne portait aucune atteinte grave et manifestement illégale au droit au respect de sa vie familiale. En statuant ainsi, alors que, dans les circonstances de l'espèce, la mesure de suspension litigieuse, qui caractérise une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative, a pour effet de priver, pendant une durée de six mois, le requérant de tout contact direct avec sa compagne ainsi que de la possibilité de voir, en présence de celle-ci, leur enfant commun, le juge des référés a entaché son ordonnance de dénaturation. <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. D...est fondé à demander l'annulation de l'ordonnance qu'il attaque. <br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              6. Aux termes des deux premiers alinéas de l'article 35 de la loi pénitentiaire du 24 novembre 2009 : "  Le droit des personnes détenues au maintien des relations avec les membres de leur famille s'exerce soit par les visites que ceux-ci leur rendent, soit, pour les condamnés et si leur situation pénale l'autorise, par les permissions de sortir des établissements pénitentiaires. Les prévenus peuvent être visités par les membres de leur famille ou d'autres personnes, au moins trois fois par semaine, et les condamnés au moins une fois par semaine. / L'autorité administrative ne peut refuser de délivrer un permis de visite aux membres de la famille d'un condamné, suspendre ou retirer ce permis que pour des motifs liés au maintien du bon ordre et de la sécurité ou à la prévention des infractions. " L'article R. 57-8-10 du code de procédure pénale désigne le chef d'établissement comme l'autorité responsable de la délivrance, la suspension ou du retrait d'un permis de visiter une personne condamnée et le dernier alinéa de l'article R. 57-8-15 du même code dispose : " Les incidents mettant en cause les visiteurs sont signalés à l'autorité ayant délivré le permis qui apprécie si le permis doit être suspendu ou retiré. " Aux termes de l'article R. 57-8-11 du même code : " Le chef d'établissement fait droit à tout permis de visite qui lui est présenté, sauf à surseoir si des circonstances exceptionnelles l'obligent à en référer à l'autorité qui a délivré le permis, ou si les personnes détenues sont matériellement empêchées, ou si, placées en cellule disciplinaire, elles ont épuisé leur droit à un parloir hebdomadaire. "<br/>
<br/>
              7. Il résulte des dispositions citées au point précédent que les décisions tendant à restreindre, supprimer ou retirer les permis de visite relèvent du pouvoir de police des chefs d'établissements pénitentiaires. Ces décisions affectant directement le maintien des liens des détenus avec leurs proches, elles sont susceptibles de porter atteinte à leur droit au respect de leur vie privée et familiale protégé par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Il appartient en conséquence à l'autorité compétente de prendre les mesures nécessaires, adaptées et proportionnées à assurer le maintien du bon ordre et de la sécurité de l'établissement pénitentiaire ou, le cas échéant, la prévention des infractions sans porter d'atteinte excessive au droit des détenus. <br/>
<br/>
              8. Il résulte de ce qui a été dit au point 3 que la suspension du permis de visite de MmeC..., notamment eu égard à sa durée, caractérise une situation d'urgence. Eu égard à son objet et à la gravité ses effets, cette décision de suspension porte une atteinte grave et manifestement illégale au droit au respect de la vie familiale de M.D.... Il y a lieu, par suite, de suspendre l'exécution de cette décision. <br/>
<br/>
              9. M. D...a obtenu le bénéfice de l'aide juridictionnelle. Son avocat peut, dès lors, se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, sous réserve que Me Capron, avocat de M.D..., renonce à percevoir la somme correspondante à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2500 euros à verser à cet avocat.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Toulouse du 13 décembre 2017 est annulée.<br/>
<br/>
Article 2 : L'exécution de la décision du 2 octobre 2017 du chef d'établissement du centre de détention de Muret suspendant le permis de visite de Mme A...C...de la décision du 30 novembre 2017 du directeur interrégional des services pénitentiaires de Toulouse est suspendue.<br/>
Article 3 : L'Etat versera à Me Capron, avocat de M.D..., une somme de 2 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondante à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...D...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
