<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025685559</ID>
<ANCIEN_ID>JG_L_2012_04_000000357322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/68/55/CETATEXT000025685559.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 02/04/2012, 357322, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle de Silva</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 5 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA), représentée par son président en exercice, dont le siège social est situé au tribunal administratif de Paris, 7 rue de Jouy à Paris (75004) ; l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA) demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution du décret n° 2011-1950 du 23 décembre 2011 modifiant le code de justice administrative, en tant qu'il porte application, dans ses articles 2 à 11, des dispositions de l'article L. 732-1 du code de justice administrative relatives à la dispense de conclusions du rapporteur public ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement des sommes de 35 euros en remboursement des droits acquittés au titre de l'article 1635 bis Q du code général des impôts et de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que la condition d'urgence est remplie dès lors que le décret contesté, entré en vigueur le 1er janvier 2012, prive les justiciables de la garantie essentielle que constitue l'intervention du rapporteur public et porte par suite une atteinte grave aux intérêts des justiciables et aux intérêts que le syndicat entend défendre, tenant notamment à la qualité de la justice administrative rendue ; qu'aucun intérêt public légitime ne saurait venir contrebalancer l'urgence ainsi caractérisée, la suspension ne portant pas atteinte à l'organisation des juridictions administratives, à la tenue des audiences ou à l'effectivité des garanties accordées aux justiciables ; qu'il existe un doute sérieux quant à la légalité du décret contesté ; que l'auteur du décret a méconnu l'étendue de sa compétence, faute d'avoir précisé les conditions dans lesquelles la dispense de conclusion pourrait être accordée, notamment les cas dans lesquels la solution de l'affaire paraît s'imposer ou ne soulève aucune question de droit nouvelle ; que les dispositions de l'article L. 732-1 du code de justice administrative, en application desquelles le décret a été pris, méconnaissent l'article 6, paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors que, lorsque la dispense de conclusions est accordée, l'ensemble des observations adressées à la formation de jugement n'est pas porté à la connaissance des parties ; qu'en outre, lorsque le rapporteur public envisage de faire application des dispositions contestées, l'information donnée au président de la formation de jugement constitue une communication à l'un des membres de la formation de jugement de l'avis du rapporteur public sur le dossier contentieux, sans que les parties puissent prendre connaissance de cette observation susceptible d'influencer la décision des juges ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 26 mars 2012, présenté par le garde des sceaux, ministre de la justice et des libertés, qui tend au rejet des conclusions soumises par la requête ; il soutient que la condition d'urgence n'est pas remplie ; qu'en effet, le décret contesté ne porte aucune atteinte grave et immédiate aux intérêts que le syndicat requérant représente ni à ceux des justiciables, pas plus qu'à l'intérêt de la justice ; que le fait que des affaires soient jugées avec dispense des conclusions du rapporteur public ne caractérise aucune situation d'urgence ; que l'incertitude relative à la portée et la validité juridiques d'un acte réglementaire ne peut fonder une situation d'urgence à suspendre un tel acte ; qu'aucun doute sérieux quant à la légalité du décret litigieux ne peut être relevé ; qu'en appliquant des critères objectifs pour déterminer les matières susceptibles d'être dispensées de conclusions du rapporteur public, le pouvoir réglementaire n'a commis aucune erreur de droit ; qu'il ne peut être valablement soutenu devant le juge des référés qu'une loi, dont l'acte réglementaire contesté fait application, méconnaîtrait des dispositions conventionnelles alors que la question posée est inédite ; qu'en prévoyant simplement une information des parties quant à la dispense de prononcé de telles conclusions, le décret contesté ne méconnaît pas l'article 6 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 28 mars 2012, présenté par l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA) qui reprend les conclusions de sa requête et les mêmes moyens ; le requérant soutient en outre que le décret attaqué méconnaît par lui-même les exigences constitutionnelles, et notamment le principe d'égalité devant la justice, dès lors que le décret a déterminé la liste des matières concernées par la dispense de conclusions sans se fonder sur des critères objectifs ; que le contentieux des étrangers ne saurait être considéré comme ne posant que des questions de droit déjà tranchées ou comme posant, de façon récurrente, des questions de fait analogues ; que, par ailleurs, plusieurs contentieux inclus dans le périmètre de la dispense de concluions ne présentent pas un caractère statistiquement significatif pour la juridiction administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code général des impôts ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA) et, d'autre part, le Premier ministre ainsi que le garde des sceaux, ministre de la justice et des libertés ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 29 mars 2012 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - le représentant de l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA) ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice et des libertés ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              Considérant que, d'une part, aux termes de l'article L. 7 du code de justice administrative : " Un membre de la juridiction, chargé des fonctions de rapporteur public, expose publiquement, et en toute indépendance, son opinion sur les questions que présente à juger les requêtes et sur les solutions qu'elles appellent " ; que, d'autre part, aux termes de l'article L. 732-1 du même code : " Dans des matières énumérées par décret en Conseil d'Etat, le président de la formation de jugement peut dispenser le rapporteur public, sur sa proposition, d'exposer à l'audience ses conclusions sur une requête, eu égard à la nature des questions à juger " ; que le décret du 23 décembre 2011 modifiant le code de justice administrative a notamment pour objet de préciser les conditions d'application des dispositions de l'article L. 732-1 ; que l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS demande au juge des référés de suspendre les articles 2 à 11 du décret en tant qu'ils portent application des dispositions de l'article L. 732-1 ;<br/>
<br/>
              Considérant qu'en l'état de l'instruction, les moyens soulevés par L'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS à l'appui de sa demande de suspension ne sont pas de nature à créer un doute sérieux quant à la légalité du décret contesté ; qu'il y a lieu, par suite, et sans qu'il soit besoin de statuer sur l'urgence, de rejeter les conclusions de la requête à fin de suspension, ainsi que, par voie de conséquence, les conclusions tendant au remboursement de la contribution prévue par l'article 1635 bis Q du code général des impôts et celles présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA) est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'UNION SYNDICALE DES MAGISTRATS ADMINISTRATIFS (USMA), au Premier ministre ainsi qu'au garde des sceaux, ministre de la justice et des libertés. <br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
