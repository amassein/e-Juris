<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006491</ID>
<ANCIEN_ID>JG_L_2007_06_000000285022</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/64/CETATEXT000018006491.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 18/06/2007, 285022, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>285022</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin Laprade</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Eric  Berti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 12 et 23 septembre 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour le CONSEIL NATIONAL DE L'ORDRE DES MEDECINS, dont le siège est 180, boulevard Haussmann à Paris cedex 08 (75389) ; le CONSEIL NATIONAL DE L'ORDRE DES MEDECINS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la « lettre-réseau » de la Caisse nationale d'assurance maladie des travailleurs salariés LR-MPS-22/2005 du 1er août 2005 relative à la mise en oeuvre des dispositions de l'avenant n° 4 de la convention nationale des médecins libéraux ;<br/>
<br/>
              2°) de mettre à la charge de la Caisse nationale d'assurance maladie des travailleurs salariés le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Berti, chargé des fonctions de Maître des requêtes,<br/>
<br/>
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat du CONSEIL NATIONAL DE L'ORDRE DES MEDECINS et de la SCP Baraduc, Duhamel, avocat de la Caisse nationale d'assurance maladie des travailleurs salariés, <br/>
<br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que pour assurer la mise en oeuvre de la permanence des soins prévue par l'article L. 6315-1 devenu L. 6314-1 du code de la santé publique et par l'avenant n° 4 du 22 avril 2005 à la convention nationale des médecins généralistes et des médecins spécialistes, approuvé par un arrêté conjoint du ministre des solidarités, de la santé et de la famille et du secrétaire d'Etat à l'assurance maladie en date du 26 mai 2005, le directeur général de la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS) a adressé le 1er août 2005 à l'ensemble des directeurs des caisses primaires d'assurance maladie, la « lettre-réseau » LR-MPS-22/2005, qui constitue la décision attaquée ; <br/>
<br/>
              Considérant, en premier lieu, d'une part, que l'article L. 161-28 du code de la sécurité sociale dispose que : « Les caisses nationales des régimes d'assurance maladie ont pour mission de participer à la maîtrise de l'évolution des dépenses. A cette fin, elles prennent toutes mesures d'organisation et de coordination internes à ces régimes, notamment de collecte, de vérification et de sécurité des informations relatives à leurs bénéficiaires et aux prestations qui leur sont servies » ; que l'article L. 221-3-1 du même code dispose que le directeur général de la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS) « est notamment chargé, pour ce qui concerne la gestion de la caisse nationale et du réseau des caisses locales et de leurs groupements : (...) 3° De prendre les mesures nécessaires à l'organisation et au pilotage du réseau des caisses du régime général ; (...)/ Le directeur général prend les décisions nécessaires au respect des objectifs de dépenses fixés par le Parlement » ; qu'il appartenait au directeur général de la Caisse nationale d'assurance maladie des travailleurs salariés, dans le cadre des pouvoirs qu'il tient des dispositions combinées de ces deux articles, d'instaurer un dispositif de contrôle de la cotation des majorations spécifiques liées à la mise en oeuvre du système de permanence des soins ;<br/>
<br/>
              Considérant, d'autre part, que la « lettre-réseau » attaquée dispose, dans son paragraphe relatif aux modalités de contrôle, que « les caisses devront procéder à des contrôles a posteriori de la cotation des majorations spécifiques. Pour cela, il conviendra de croiser les informations provenant de deux documents : le tableau de permanence transmis par l'Ordre et la liste des appels reçus par le centre de régulation et ayant donné lieu à l'intervention du médecin de permanence (ou exceptionnellement, en cas d'indisponibilité de ce dernier, d'un autre médecin) » ; qu'il ressort des pièces du dossier que l'objectif de ce contrôle est de vérifier si le médecin de permanence est intervenu à la demande du médecin chargé de la régulation ou du centre d'appel de l'association de permanence des soins ; que cette procédure, qui conduit à rapprocher le nombre d'appels téléphoniques reçus au centre de régulation, du nombre de patients ayant contacté le centre de régulation avant de consulter le médecin de permanence, n'a pas pour effet de contraindre ce centre à communiquer aux agents des caisses primaires d'assurance maladie des données comportant le nom des patients concernés, et n'entraîne donc par elle-même aucune violation du secret médical ni du respect de la vie privée protégés par l'article L. 1110-4 du code de la santé publique ; que le CONSEIL NATIONAL DE L'ORDRE DES MEDECINS n'est, dès lors, pas fondé à soutenir que la disposition litigieuse relevait de la compétence du législateur ;<br/>
<br/>
              Considérant, enfin, que la circonstance que les centres de régulation des appels ne sont pas parties à la convention nationale des médecins libéraux ne faisait pas obstacle à l'obligation qui leur est faite de transmettre à la Caisse nationale d'assurance maladie des travailleurs salariés les indications susmentionnées relatives aux appels téléphoniques à des fins de contrôle ;<br/>
<br/>
              Considérant, en second lieu, que la « lettre-réseau » attaquée prévoit que « les éléments relatifs à l'astreinte (...) seront quant à eux effectifs dès lors que la sectorisation départementale (...) demandée à tous les  préfets par le ministre des solidarités, de la santé et de la famille et le secrétaire d'Etat à l'assurance maladie par lettre du 12 avril 2005 sera intervenue. (...)/ Les nouveaux montants d'astreinte pourront être versés dès le lendemain de la publication de l'arrêté préfectoral de sectorisation » ; <br/>
<br/>
              Considérant, d'une part, que l'article L. 6315-1 devenu L. 6314-1 du code de la santé publique dispose que : « Sous réserve des missions dévolues aux établissements de santé, les médecins (...) participent, dans un but d'intérêt général, à la permanence des soins dans des conditions et selon des modalités d'organisation définies par un décret en Conseil d'Etat » ; qu'aux termes de l'article R. 730, devenu R. 635-1 du code de la sécurité sociale, dans sa rédaction issue du décret du 7 avril 2005 : « Cette permanence est organisée dans le cadre départemental en liaison avec les établissements de santé publics et privés et en fonction des besoins évalués par le comité départemental mentionné à l'article L. 6313-1. / A cette fin, le département est divisé en secteurs dont le nombre et les limites sont fixés en fonction de données géographiques et démographiques ainsi que de l'offre de soins existante. Ces limites peuvent varier selon les périodes de l'année et être adaptées, pour tout ou partie de la période de permanence de soins, aux besoins de la population./ La détermination du nombre et des limites des secteurs est arrêtée par le préfet du département (...) » ; que, d'autre part, dans le cadre des compétences qu'ils tirent de l'article L. 162-5 du code de la sécurité sociale, selon lequel  « Les rapports entre les organismes d'assurance maladie et les médecins sont définis par des conventions nationales (...)/ La ou les conventions déterminent notamment :/ (...) 16° Les modes de rémunération par l'assurance maladie (...) de la participation des médecins au dispositif de permanence des soins en application des dispositions prévues à l'article L. 6325-1 du code de la santé publique », les partenaires conventionnels ont, à l'article 4 de l'avenant n° 4 à la convention médicale des médecins généralistes et des médecins spécialistes, fixé les nouveaux taux de rémunération des astreintes, sans indiquer expressément la date à laquelle ces taux seraient applicables ;<br/>
<br/>
              Considérant toutefois que cet avenant n° 4 stipule à son article 1er que : « La mise en oeuvre de la nouvelle organisation de la permanence des soins ambulatoire, fondée sur le volontariat du médecin, repose notamment sur : (...) La sectorisation, pour le dimanche, les jours fériés et la nuit./ L'organisation, sur proposition des partenaires régionaux (missions régionales de santé), de cette sectorisation, notamment la mutualisation de secteurs entre 20 heures et 8 heures, doit faire l'objet d'arrêtés préfectoraux, pris après consultation des conseils départementaux de l'ordre des médecins et avis des CODAMUPS » ; qu'ainsi, les partenaires conventionnels ont entendu subordonner la date d'entrée en vigueur du nouveau régime de rémunération des astreintes dans chaque département à l'intervention dans celui-ci de l'arrêté préfectoral redéfinissant la sectorisation, ainsi d'ailleurs que les partenaires conventionnels l'ont expressément mentionné dans leur « relevé de décision » pris au cours de leur réunion du 17 juin 2005 ; que, par suite, le moyen tiré de ce que les dispositions litigieuses de la « lettre-réseau » seraient contraires aux stipulations de l'article 4 de l'avenant n° 4 à la convention nationale des médecins généralistes et des médecins spécialistes doit être écarté ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le CONSEIL NATIONAL DE L'ORDRE DES MEDECINS n'est pas fondé à demander l'annulation de la « lettre-réseau » du directeur général de la Caisse nationale d'assurance maladie des travailleurs salariés du 1er août 2005 ; que, par voie de conséquence, doivent être rejetées ses conclusions tendant au bénéfice de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du CONSEIL NATIONAL DE L'ORDRE DES MEDECINS est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au CONSEIL NATIONAL DE L'ORDRE DES MEDECINS, à  la Caisse nationale d'assurance maladie des travailleurs salariés et au ministre de la santé, de la jeunesse et des sports.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
