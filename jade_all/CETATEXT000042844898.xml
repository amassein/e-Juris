<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844898</ID>
<ANCIEN_ID>JG_L_2020_12_000000438056</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/12/2020, 438056, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438056</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438056.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Versailles de condamner la commune d'Angervilliers à lui verser une somme de 175 759 euros au titre de primes non perçues,  une somme de 171 759 euros au titre de la perte de son indemnité de secrétaire du syndicat intercommunal d'adduction d'eau potable de la région d'Angervilliers (SIAEP), une somme de 36 910 euros en réparation du préjudice financier résultant de la perte de son indemnité de secrétaire du centre communal d'action sociale et une somme de 75 000 euros au titre de la perte de chance et du préjudice professionnel. Par un jugement n°1302424 du 14 juin 2016, le tribunal administratif de Versailles a rejeté la demande de Mme B... et l'a condamnée à verser au Trésor public une amende de 1 200 euros en application des dispositions de l'article L. 741-12 du code de justice administrative.<br/>
<br/>
              Par un arrêt n° 16VE02677 du 28 novembre 2019, la cour administrative d'appel de Versailles, sur appel de Mme B..., a réformé ce jugement en ramenant le montant de l'amende pour recours abusif à 600 euros et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 15 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté le surplus des conclusions de son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Angervilliers la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n°83-634 du 13 juillet 1983 ;<br/>
              - la loi n°84-53 du 26 janvier 1984<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, Mme B... soutient que la cour administrative de Versailles :<br/>
<br/>
              - a commis une erreur de droit et dénaturé les faits de l'espèce en jugeant que la requérante ne démontre pas que la commune aurait eu des agissements fautifs de nature à engager sa responsabilité pour la période postérieure au 4 octobre 2007;<br/>
              - a commis une erreur de droit en jugeant que les conclusions tendant à la condamnation de la commune d'Angervilliers à l'indemniser des préjudices résultant des décisions prises par les organes dirigeants du CCAS d'Angervilliers et du SIAEP de la région d'Angervilliers étaient mal dirigées ;<br/>
              - a dénaturé les faits de l'espèce et les a inexactement qualifiés en jugeant que le recours de le Mme B... était abusif ou, à tout le moins, en fixant le montant de l'amende.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les indemnités accessoires et l'amende pour recours abusif. En revanche, aucun des moyens soulevés n'est de nature à justifier l'admission du surplus des conclusions des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de Mme B... qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les indemnités accessoires et l'amende pour recours abusif sont admises. <br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme B... n'est pas admis.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B....<br/>
Copie en sera adressée à la commune d'Angervilliers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
