<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029498123</ID>
<ANCIEN_ID>JG_L_2014_09_000000348214</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/81/CETATEXT000029498123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 24/09/2014, 348214, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348214</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:348214.20140924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
      Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	La société Beauté Créateurs a demandé au tribunal administratif de Paris de la décharger des cotisations supplémentaires à l'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 1992 et 1993, ainsi que des pénalités correspondantes. Par un jugement n° 706997/1 du 17 décembre 2003, le tribunal administratif de Paris a rejeté sa demande. <br/>
	Par un arrêt n° 04PA00659 du 5 mars 2007, la cour administrative d'appel de Paris a rejeté son appel formé contre ce jugement. <br/>
	Par une décision n° 305449 du 30 novembre 2009, le Conseil d'Etat statuant au contentieux a, d'une part, annulé cet arrêt en tant qu'il a statué sur les chefs de redressements correspondant à la réintégration, dans les résultats des exercices clos en 1992 et 1993, des frais de procédures et de surveillance ainsi que des redevances versées au titre des droits concédés à la société Beauté Créateurs et, d'autre part, renvoyé l'affaire, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Paris. <br/>
<br/>
	Par les articles 1er et 2 d'un arrêt n° 10PA00278 du 3 février 2011, la cour administrative d'appel de Paris a, d'une part, déchargé la société Beauté Créateurs des suppléments d'impôt sur les sociétés mis à sa charge au titre des exercices clos en 1992 et 1993, correspondant à la réintégration dans les résultats de ces deux exercices des redevances versées en contrepartie des droits qui lui ont été concédés et, d'autre part, réformé dans cette mesure le jugement n° 9706997/1 du 17 décembre 2003 du tribunal administratif de Paris. La cour a rejeté le surplus des conclusions d'appel de la société.<br/>
Procédure devant le Conseil d'Etat<br/>
	Par un pourvoi et des mémoires complémentaires, enregistrés le 7 avril 2011, le 20 septembre 2013 et le 20 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre chargé du budget demande au Conseil d'Etat :<br/>
	1°) d'annuler les articles 1er et 2 de l'arrêt n° 10PA00278 de la cour administrative d'appel de Paris du 3 février 2011 ;<br/>
	2°) réglant l'affaire au fond, de rejeter la requête d'appel de la société.<br/>
<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative. <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société Beauté Créateurs ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er septembre 2014, présentée pour la société Beauté Créateurs ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur les exercices clos en 1992, 1993 et 1994, la société par actions simplifiée Beauté Créateurs, qui exerçait depuis 1986 une activité de vente à distance de produits cosmétiques, s'est vu notifier divers redressements en matière d'impôt sur les sociétés ; que le vérificateur a notamment remis en cause la déduction de frais de dépôt, d'acquisition et de surveillance de marques, au motif qu'il s'agissait de dépenses à immobiliser, ainsi que la déduction de redevances versées en rémunération de l'utilisation d'autres marques, au motif que les droits concédés avaient le caractère d'éléments incorporels de l'actif immobilisé ; que la société Beauté Créateurs s'est pourvue en cassation contre l'arrêt du 5 mars 2007 par lequel la cour administrative d'appel de Paris, confirmant le jugement du tribunal administratif de Paris du 17 décembre 2003, a rejeté sa requête tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 1992 et 1993 en conséquence de ces redressements ; que, par une précédente décision n° 305449 du 30 novembre 2009, le Conseil d'Etat statuant au contentieux a, d'une part, annulé cet arrêt en tant qu'il a statué sur les chefs de redressements correspondant à la réintégration, dans les résultats des exercices clos en 1992 et 1993, des frais de procédures et de surveillance ainsi que des redevances versées au titre des droits concédés à la société Beauté Créateurs et, d'autre part, renvoyé l'affaire, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Paris ;<br/>
<br/>
              2. Considérant que, par l'arrêt du 3 février 2011, la cour a notamment déchargé la société Beauté Créateurs des compléments d'impôt sur les sociétés mis à sa charge au titre des exercices clos en 1992 et 1993, correspondant à la réintégration dans les résultats de ces deux exercices des redevances versés au titre des droits qui lui ont été sous-concédés par la société L'Oréal ; que le ministre chargé du budget demande l'annulation des articles 1er et 2 de cet arrêt, qui ont prononcé cette décharge et réformé en conséquence le jugement du 17 décembre 2003 du tribunal administratif de Paris ;<br/>
<br/>
              3. Considérant qu'aux termes du 2 de l'article 38 du code général des impôts : " Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés " ; que, s'agissant des droits tirés d'une concession de distribution exclusive et d'utilisation de marques, ne doivent suivre le régime fiscal des éléments incorporels de l'actif immobilisé de l'entreprise que les droits constituant une source régulière de profits, dotés d'une pérennité suffisante et susceptibles de faire l'objet d'une cession ; que ces droits ne peuvent être regardés comme dotés d'une pérennité suffisante si le concédant peut à tout moment résilier ce contrat sans indemnité, avec un préavis d'une faible durée ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société L'Oréal, dont la société par actions simplifiée Beauté Créateurs était une filiale à 100 %, a conclu quatre contrats de concession de licence exclusive avec diverses sociétés et personnes physiques, afin que sa filiale commercialise ses produits sous les marques constituées par leurs noms, et un contrat avec un professeur des universités par lequel celui-ci cédait à L'Oréal l'ensemble des procédés et techniques élaborés par lui et relatifs à un ensemble de produits cosmétiques à commercialiser par sa filiale ; <br/>
<br/>
              5. Considérant que la cour a examiné, pour chaque contrat, si les droits qui y étaient attachés étaient dotés d'une pérennité suffisante ; que, toutefois, elle s'est fondée sur les seules facultés de résiliation offertes à la société L'Oréal dans les contrats en cause, relevant ainsi que cette société, licenciée des marques et procédés concédés dont elle avait confié l'exploitation à sa filiale Beauté Créateurs, avait entendu se ménager une liberté de résiliation sans grande contrainte, alors que, pour vérifier si les droits qui en découlaient étaient ou non dotés d'une pérennité suffisante pour constituer des éléments incorporels de l'actif immobilisé de la société Beauté Créateurs, elle aurait dû rechercher exclusivement quelles étaient les facultés de résiliation offertes, contrat par contrat, aux concédants ; qu'en statuant ainsi, la cour a commis une erreur de droit ; <br/>
<br/>
              6 Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre chargé du budget est fondé à demander l'annulation des articles 1er et 2 de l'arrêt qu'il attaque ; <br/>
<br/>
              7. Considérant que l'affaire faisant l'objet d'un second pourvoi en cassation, il y a lieu de la régler dans cette mesure au fond, par application des dispositions du second alinéa de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, contrairement à ce que soutient la société Beauté Créateurs, celle-ci a été régulièrement convoquée à l'audience du 19 novembre 2003 du tribunal administratif de Paris ; que la minute du jugement du 17 décembre 2003, qui est suffisamment motivé, comporte le visa des mémoires complémentaires qu'elle a produits les 13 novembre et 5 décembre 1997 ; que, par suite, les moyens tirés de l'irrégularité de ce jugement doivent être écartés ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que la notification de redressements adressée le 29 juin 1995 à la société Beauté Créateurs comporte des éléments suffisants pour lui permettre de formuler ses observations, conformément aux dispositions de l'article L. 57 du livre des procédures fiscales ;<br/>
<br/>
              10. Considérant, en troisième lieu, d'une part, qu'il résulte de l'instruction que les contrats signés avec Tan Giudicelli, Marc Pajot et Michel Klein portent sur des durées comprises entre cinq et six ans, renouvelables par tacite reconduction pour une période de cinq ans ; qu'ils peuvent être dénoncés à l'échéance du terme avec un préavis de six mois ; que, toutefois, ces contrats prévoient que les deux premiers renouvellements sont automatiques, sous réserve que les redevances versées au titre des deux dernières années de la période considérée dépassent le montant minimum de redevances garanti par le licencié au concédant pour chacune de ces deux années ; que si d'autres hypothèses de résiliation sont prévues dans des délais très courts, au cas où l'une des parties ne respecte pas ses obligations et en l'absence de réparation suffisante acceptable et dans certains cas exceptionnels, tels que le décès ou l'incapacité d'un cocontractant ou la survenue de son insolvabilité, dans des délais brefs et sans indemnités, ces modalités correspondent aux pratiques usuelles et sont inhérentes aux relations contractuelles entre parties ; <br/>
<br/>
              11. Considérant, d'autre part, qu'il résulte de l'instruction que les contrats signés avec le professeur Cotte et Michel A...portent sur des durées comprises entre dix et onze ans ; que leur renouvellement, pour une période de cinq ans, est subordonné à un accord écrit entre les parties six mois avant la fin de chaque période ; que les deux premiers renouvellements mentionnés dans le contrat de M. A...sont automatiques dès lors que le chiffre d'affaires réalisé au cours de l'année précédente place la marque parmi les trois premières marques de produits capillaires vendus par correspondance aux Etats-Unis et dans trois des pays mentionnés dans le contrat ; <br/>
<br/>
              12. Considérant qu'il en résulte que les droits qui découlaient de ces contrats étaient d'une durée initiale suffisante et assortis de conditions de renouvellement dépendant des modalités de réalisation des prévisions de chiffre d'affaires par le licencié, sans que des clauses autres que celles de droit commun permissent une résiliation anticipée ; que, dans ces conditions, ces droits doivent être regardés comme remplissant la condition de pérennité suffisante à laquelle est subordonnée leur qualification d'éléments incorporels d'actif immobilisé ; que si la société fait valoir que la cessibilité des contrats en cause était limitée, elle n'apporte pas, en tout état de cause, de précisions suffisantes à l'appui de ses allégations, alors même que sa société mère L'Oréal lui avait sous-concédé ces licences pour une durée indéterminée ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la SA Beauté Créateurs n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande tendant à la réduction des cotisations supplémentaires à l'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices 1992 et 1993, correspondant à la réintégration dans les résultats de ces deux exercices des redevances versées au titre des droits qui lui ont été concédés ; que doivent être rejetées, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 3 février 2011 de la cour administrative d'appel de Paris sont annulés. <br/>
Article 2 : Les conclusions de la SA Beauté Créateurs tendant à la décharge des cotisations supplémentaires d'impôts sur les sociétés mises à sa charge au titre des exercices 1992 et 1993 présentées devant la cour administrative d'appel de Paris sont rejetées.  <br/>
Article 3 : Les conclusions présentées par la SA Beauté Créateurs au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société par actions simplifiée Beauté Créateurs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
