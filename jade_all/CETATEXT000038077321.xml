<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077321</ID>
<ANCIEN_ID>JG_L_2019_01_000000412159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 30/01/2019, 412159</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412159.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 5 juillet et 6 octobre 2017 et les 8 février et 6 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme D...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 7 avril 2017 par laquelle le conseil académique de l'université Lumière Lyon 2 a refusé sa demande de mutation prioritaire sur le poste n° 4298 " sociologie du genre, sociologie de l'égalité " ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir le courrier électronique du 9 mai 2017 l'informant de son classement par le comité de sélection constitué pour le recrutement sur ce même poste ;<br/>
<br/>
              3°) d'annuler pour excès de pouvoir la délibération du 29 mai 2017 du conseil académique de la même université, relative à ce recrutement ; <br/>
<br/>
              4°) d'annuler pour excès de pouvoir la délibération du 2 juin 2017 du conseil d'administration de la même université, relative à ce recrutement ;<br/>
<br/>
              5°) d'annuler pour excès de pouvoir la décision de nomination éventuellement prise sur ce poste à l'issue de la procédure de recrutement ;<br/>
<br/>
              6°) d'enjoindre à l'université Lumière Lyon 2 de reprendre la procédure de recrutement au stade de l'examen des demandes de mutation prévu par les dispositions de l'article 9-3 du décret n° 84-431 du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences, à compter de la notification de l'arrêt, sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              7°) de mettre à la charge de l'université Lumière Lyon 2 une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-430 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,<br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article 9-3 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences dispose que, par dérogation à la procédure de recrutement des enseignants-chercheurs prévue par son article 9-2 : " (...) le conseil académique ou l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1, en formation restreinte, examine les candidatures à la mutation et au détachement des personnes qui remplissent les conditions prévues aux articles 60 et 62 de la loi du 11 janvier 1984 susvisée, sans examen par le comité de sélection. Si le conseil académique retient une candidature, il transmet le nom du candidat sélectionné au conseil d'administration. Lorsque l'examen de la candidature ainsi transmise conduit le conseil d'administration à émettre un avis favorable sur cette candidature, le nom du candidat retenu est communiqué au ministre chargé de l'enseignement supérieur. L'avis défavorable du conseil d'administration est motivé. / Lorsque la procédure prévue au premier alinéa n'a pas permis de communiquer un nom au ministre chargé de l'enseignement supérieur, les candidatures qui n'ont pas été retenues par le conseil académique ou qui ont fait l'objet d'un avis défavorable du conseil d'administration sont examinées avec les autres candidatures par le comité de sélection selon la procédure prévue à l'article 9-2 ".<br/>
<br/>
              2. Il ressort des pièces du dossier que MmeA..., professeur des universités à l'université Toulouse 3, a fait acte de candidature sur le poste de professeur des universités intitulé " sociologie du genre, sociologie de l'égalité " ouvert à l'université Lumière Lyon 2 en demandant à bénéficier des dispositions, citées ci-dessus, relatives aux personnes dont la candidature est dispensée de l'examen par le comité de sélection, au titre de la mutation pour rapprochement de conjoints. Par une délibération du 7 avril 2017, le conseil académique de l'université Lumière Lyon 2 a toutefois refusé de transmettre sa candidature au conseil d'administration et l'a transmise au comité de sélection, pour qu'elle soit examinée avec l'ensemble des autres candidatures. A l'issue de la procédure de recrutement, le Président de la République a, par un décret du 29 janvier 2018, nommé Mme C...sur le poste ouvert à ce concours.<br/>
<br/>
              Sur les conclusions dirigées contre le courrier électronique du 9 mai 2017 :<br/>
<br/>
              3. Le courrier électronique du 9 mai 2017 informant Mme A... de son rang de classement par le comité de sélection ne présente pas le caractère d'une décision faisant grief. L'université Lumière Lyon 2 est, ainsi, fondée à soutenir que les conclusions d'annulation dirigées contre ce courrier par Mme A...sont irrecevables.<br/>
<br/>
              Sur les conclusions dirigées contre la délibération du conseil académique du 7 avril 2017 :<br/>
<br/>
              4. Pour écarter, par sa délibération du 7 avril 2017, la demande de recrutement par voie de mutation de MmeA..., le conseil académique de l'université Lumière Lyon 2 s'est borné à indiquer que la " pleine adéquation " de la candidature de l'intéressée avec le profil du poste n'était pas " avérée ", sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressée ne correspondait pas au profil de poste. Mme A...est, par suite, fondée à soutenir que cette délibération est insuffisamment motivée et à en demander l'annulation.<br/>
<br/>
              Sur les conclusions dirigées contre la délibération du conseil académique du 29 mai 2017 et la délibération du 2 juin 2017 du conseil d'administration :<br/>
<br/>
              5. Il résulte de l'annulation prononcée au point 4 que les décisions prises, pour le recrutement sur le poste litigieux, à la suite de la transmission illégale de la candidature de Mme A...au comité de sélection, sont, par voie de conséquence, également entachées d'illégalité. Mme A...est, par suite, fondée à demander l'annulation des délibérations du conseil académique de l'université Lumière Lyon 2 du 29 mai 2017 et du conseil d'administration de la même université du 2 juin 2017, relatives à ce recrutement.<br/>
<br/>
              Sur les conclusions relatives à la nomination sur le poste " sociologie du genre, sociologie de l'égalité " ouvert au sein de l'université Lumière Lyon 2 :<br/>
<br/>
              6. Mme A...a présenté, dans quatre mémoires enregistrés entre le 5 juillet 2017 et le 6 juillet 2018, des conclusions qu'elle se bornait à diriger contre " la décision de nomination éventuellement prise " sur le poste de professeur pour lequel elle s'était portée candidate. De telles conclusions, dépourvues de toute précision, sont irrecevables et doivent, par suite, être rejetées.<br/>
<br/>
              7. Si, dans un mémoire enregistré le 8 octobre 2018, Mme A... conclut à l'annulation du décret du 29 janvier 2018 du Président de la République en tant qu'il nomme Mme C...sur le poste litigieux, ces conclusions, dirigées contre un décret publié au Journal officiel de la République française le 31 janvier 2018, sont tardives et, par suite, irrecevables.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              8. Il résulte de l'instruction et de ce qui a été dit au point 7 que la procédure de recrutement sur le poste de professeur des universités en " sociologie du genre, sociologie de l'égalité " à l'université Lumière Lyon 2 a fait l'objet d'une décision de nomination devenue définitive. Les conclusions de Mme A...tendant à ce qu'il soit enjoint à cette université de reprendre la procédure de recrutement au stade de l'examen des demandes de mutation ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeA..., qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université Lumière Lyon 2 une somme de 2 000 euros à verser à Mme A...au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La délibération du 7 avril 2017 du conseil académique restreint de l'université Lumière Lyon 2, la délibération du conseil académique restreint du 29 mai 2017 et la délibération du conseil d'administration restreint du 2 juin 2017 sont annulées en tant qu'elles statuent sur le poste de professeur des universités en " sociologie du genre, sociologie de l'égalité ".<br/>
<br/>
Article 2 : L'université Lumière Lyon 2 versera à Mme A...une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 3 : Le surplus des conclusions de la requête de Mme A...est rejeté.<br/>
<br/>
Article 4 : Les conclusions de l'université Lumière Lyon 2 présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme D...A..., à l'université Lumière Lyon 2, à Mme B...C...et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. ABSENCE. - AVIS DÉFAVORABLE RENDU PAR UN CONSEIL ACADÉMIQUE SUR LA CANDIDATURE D'UN ENSEIGNANT CHERCHEUR (ART. 9-3 DU DÉCRET DU 6 JUIN 1984) - CAS D'UN CANDIDAT DONT LA SPÉCIALITÉ CORRESPOND À CELLE DU POSTE - AVIS N'INDIQUANT PAS, MÊME SOMMAIREMENT, LES RAISONS POUR LESQUELLES LA CANDIDATURE DE L'INTÉRESSÉ NE CORRESPOND PAS AU PROFIL DE POSTE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05-01-04 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. CONSEILS D'UNIVERSITÉ. - AVIS DÉFAVORABLE RENDU PAR UN CONSEIL ACADÉMIQUE SUR LA CANDIDATURE D'UN ENSEIGNANT CHERCHEUR (ART. 9-3 DU DÉCRET DU 6 JUIN 1984) - CAS D'UN CANDIDAT DONT LA SPÉCIALITÉ CORRESPOND À CELLE DU POSTE - AVIS N'INDIQUANT PAS, MÊME SOMMAIREMENT, LES RAISONS POUR LESQUELLES LA CANDIDATURE DE L'INTÉRESSÉ NE CORRESPOND PAS AU PROFIL DE POSTE - MOTIVATION INSUFFISANTE [RJ1].
</SCT>
<ANA ID="9A"> 01-03-01-02-02-01 Est insuffisamment motivé l'avis défavorable rendu par un conseil académique, sur le fondement de l'article 9-3 du décret n° 84-430 du 6 juin 1984, sur la candidature d'un professeur de sociologie à un poste de professeur d'université intitulé sociologie du genre, sociologie de l'égalité, qui se borne à indiquer que la pleine adéquation de la candidature de l'intéressé avec le profil du poste n'était pas avérée, sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressée ne correspondait pas au profil de poste.</ANA>
<ANA ID="9B"> 30-02-05-01-04 Est insuffisamment motivé l'avis défavorable rendu par un conseil académique, sur le fondement de l'article 9-3 du décret n° 84-430 du 6 juin 1984, sur la candidature d'un professeur de sociologie à un poste de professeur d'université intitulé sociologie du genre, sociologie de l'égalité, qui se borne à indiquer que la pleine adéquation de la candidature de l'intéressé avec le profil du poste n'était pas avérée, sans indiquer, même sommairement, les raisons pour lesquelles il estimait que la candidature de l'intéressée ne correspondait pas au profil de poste.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des avis défavorables rendus par le comité de sélection sur le fondement de l'article 9.2 du décret du 6 juin 1984, CE, 14 octobre 2011, Thepaut, n°s 333712, 334692, T. pp. 736-956.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
