<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042506235</ID>
<ANCIEN_ID>JG_L_2020_11_000000432405</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/50/62/CETATEXT000042506235.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 06/11/2020, 432405, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432405</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432405.20201106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Montenotte a demandé au tribunal administratif de Paris de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 24 mai 2019 par lequel la Ville de Paris a abrogé les autorisations des 1er septembre et 3 novembre 2009 l'autorisant à installer deux contre-terrasses à proximité de l'établissement qu'elle exploite au 13 rue Montenotte à Paris XVIIème, jusqu'à ce qu'il soit statué au fond sur la légalité de cet arrêté. Par une ordonnance n° 1913043 du 3 juillet 2019, le juge des référés de ce tribunal a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 et 17 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la société Montenotte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Montenotte et à la SCP Foussard, Froger, avocat de la Ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la société Montenotte bénéficie de deux autorisations d'occupation temporaire du domaine public délivrées, l'une, le 1er septembre 2009 pour exploiter une contre-terrasse ouverte d'une longueur de 6 mètres et d'une largeur de 5 mètres sur la place face au 13, rue Montenotte à Paris (75017) et, l'autre, le 3 novembre 2009 pour exploiter une contre-terrasse d'une longueur de 2 mètres sur une largeur de 1 mètre sur le côté gauche de la devanture située à cette même adresse. Par un arrêté du 24 mai 2019, la Ville de Paris a abrogé ces autorisations. La société Montenotte se pourvoit en cassation contre l'ordonnance du 3 juillet 2019 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa requête tendant à ce que soit ordonnée, en application des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cet arrêté. <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis au juge des référés que par courrier du 25 avril 2019, la Ville de Paris a informé la société Montenotte qu'elle envisageait de mettre fin aux autorisations d'occupation du domaine public dont elle bénéficiait, au motif que l'occupation excédait les superficies autorisées, ainsi qu'en attestaient un procès-verbal d'infraction dressé le 7 mars 2019 et des constatations opérées lors d'un contrôle sur place le 9 avril 2019. La Ville de Paris a invité la société à présenter ses observations, ce que celle-ci a fait par courrier du 9 mai 2019. Le juge des référés du tribunal administratif, qui n'a pas entaché son ordonnance de dénaturation des faits et des pièces du dossier, n'a pas commis d'erreur de droit manifeste en jugeant, dans les circonstances de l'espèce, eu égard notamment au caractère objectif et à l'ampleur des empiètements non autorisés sur la voirie publique, que n'était pas de nature à faire naître un doute sérieux sur la légalité de la décision attaquée le moyen tiré de ce que la société aurait été privée, du fait du refus de la Ville de Paris de faire droit à sa demande tendant à présenter des observations orales, des garanties attachées au caractère contradictoire de la procédure suivie à son égard.<br/>
<br/>
              4. En second lieu, la société requérante n'est pas fondée à soutenir que le juge des référés aurait dénaturé les faits et les pièces du dossier en jugeant que n'était pas de nature à faire naître un doute sérieux sur la légalité de la décision attaquée le moyen tiré de que cette décision était disproportionnée par rapport aux objectifs qu'elle poursuivait.<br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de la société Montenotte ne peut qu'être rejeté.<br/>
<br/>
              6. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Ville de Paris, qui n'est pas la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Montenotte la somme de 3 000 euros à verser à la Ville de Paris au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Montenotte est rejeté.<br/>
Article 2 : La société Montenotte versera à la Ville de Paris une somme de 3 000 euros à au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Montenotte et à la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
