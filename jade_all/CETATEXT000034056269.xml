<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034056269</ID>
<ANCIEN_ID>JG_L_2017_02_000000402102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/05/62/CETATEXT000034056269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 14/02/2017, 402102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:402102.20170214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de la Guyane d'ordonner la suspension de l'exécution de la décision du 27 juin 2016 par laquelle le directeur général de la caisse générale de sécurité sociale de la Guyane a prononcé à son encontre la sanction de suspension de la possibilité d'exercer dans le cadre conventionnel pour une durée de trois ans à compter du 1er août 2016. Par une ordonnance n° 1600445 du 19 juillet 2016, rectifiée par une ordonnance n° 1600445/1 du 21 juillet 2016, le juge des référés du tribunal administratif de la Guyane a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 et 17 août 2016 et 20 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de la Guyane du 19 juillet 2016, rectifiée le 21 juillet 2016 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de la caisse générale de sécurité sociale la somme de 4 300 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - l'arrêté du 14 juin 2006 portant approbation de la convention nationale des chirurgiens-dentistes destinée à régir les rapports entre les chirurgiens-dentistes et les caisses d'assurance maladie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.B..., et à la SCP Baraduc, Duhamel, Rameix, avocat de la caisse générale de sécurité sociale de la Guyane.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de la Guyane que, par une décision du 27 juin 2016, le directeur général de la caisse générale de sécurité sociale de la Guyane a, en application de l'article 7.3.2 de la convention nationale destinée à régir les rapports entre les chirurgiens-dentistes et les caisses d'assurance maladie, prononcé à l'encontre de M. B...la sanction de suspension de la possibilité d'exercer dans le cadre conventionnel pour une durée de trois ans à compter du 1er août 2016. M. B...demande l'annulation de l'ordonnance du 19 juillet 2016, rectifiée le 21 juillet 2016, par laquelle le juge des référés a rejeté sa demande de suspension de l'exécution de cette décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative (...) fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Aux termes de l'article L. 5 du même code : " L'instruction des affaires est contradictoire. Les exigences de la contradiction sont adaptées à celles de l'urgence ". Le premier alinéa de l'article L. 522-1 du même code précise que : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale ".<br/>
<br/>
              4. Il résulte des énonciations de l'ordonnance attaquée, telles que rectifiées par l'ordonnance prise sur le fondement de l'article R. 741-11 du code de justice administrative, qu'elle a été rendue le 19 juillet 2016, alors que la clôture de l'instruction avait été fixée au 20 juillet 2016 à midi. Si la caisse générale de sécurité sociale de la Guyane soutient que cette dernière date serait elle-même entachée d'une erreur purement matérielle, l'incohérence qui résulte de ces énonciations, alors que l'ordonnance rectificative avait été prise dans le but de corriger l'omission de l'ordonnance initiale quant au différé de la clôture de l'instruction ordonné à l'issue de l'audience, en application de l'article R. 522-8 du code de justice administrative, ne permet pas au Conseil d'Etat, juge de cassation, d'exercer son contrôle sur la régularité de l'ordonnance attaquée. M. B...est donc fondé à en demander l'annulation pour ce motif. Le moyen d'irrégularité retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. En premier lieu, l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'espèce. <br/>
<br/>
              7. La décision attaquée a pour effet d'exclure les patients de M. B..., pendant une durée de trois ans, du bénéfice de la prise en charge de leurs soins par l'assurance maladie. Ainsi, et alors que sa patientèle est constituée de personnes disposant de faibles ressources qui ne peuvent recourir aux soins d'un chirurgien-dentiste non conventionné, M. B...est fondé à soutenir que cette décision est susceptible de lui faire perdre l'essentiel de sa patientèle et de préjudicier ainsi de manière grave et immédiate à ses intérêts. Ni la circonstance invoquée par la caisse générale de sécurité sociale de la Guyane selon laquelle il serait à  l'origine, du fait de son propre comportement, de la sanction qui lui a été ainsi infligée, ni le préjudice financier qui en serait résulté pour la caisse ne s'opposent, dans les circonstances de l'espèce, à ce que l'existence d'une situation d'urgence, au sens de l'article L. 521-1 du code de justice administrative, soit regardée comme établie.<br/>
<br/>
              8. En second lieu, selon l'article 7.3.2 de la convention nationale destinée à régir les rapports entre les chirurgiens-dentistes et les caisses d'assurance maladie, signée les 11 et 19 mai 2006 par l'Union nationale des caisses d'assurance maladie, la Confédération nationale des syndicats dentaires et l'Union des jeunes chirurgiens-dentistes - Union dentaire, approuvée par arrêté des ministres chargés de la santé et de la sécurité sociale du 14 juin 2006 et reconduite tacitement les 19 juin 2011 et 19 juin 2016, en vertu de l'article L. 162-15-2 du code de la sécurité sociale, lorsqu'un chirurgien-dentiste ne respecte pas les stipulations de la convention, il encourt une sanction qui peut être la suspension de la possibilité d'exercer dans le cadre conventionnel. Ce même article précise que : " Cette suspension peut être temporaire (une semaine, un, trois, six, neuf, douze mois) ou prononcée pour la durée d'application de la convention, selon l'importance des griefs ". Ainsi, le moyen soulevé par M. B...tiré de ce que la décision du 27 juin 2016, en prononçant à son encontre une suspension pour une durée de trois ans, non prévue par la convention nationale, serait entachée d'une erreur de droit est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cette décision.<br/>
<br/>
              9. Il résulte de ce qui précède que M. B...est fondé à demander la suspension de l'exécution de la décision du 27 juin 2016.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la caisse générale de sécurité sociale de la Guyane une somme globale de 1 500 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de la Guyane du 19 juillet 2016, rectifiée le 21 juillet 2016, est annulée.<br/>
Article 2 : L'exécution de la décision du directeur général de la caisse générale de sécurité sociale de la Guyane du 27 juin 2016 est suspendue.<br/>
Article 3 : La caisse générale de sécurité sociale de la Guyane versera à M. B...une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la caisse générale de sécurité sociale de la Guyane présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la caisse générale de sécurité sociale de la Guyane.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
