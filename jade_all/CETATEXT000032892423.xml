<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892423</ID>
<ANCIEN_ID>JG_L_2016_07_000000394331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892423.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 13/07/2016, 394331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394331.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. E...D...et Mme G...H...ont demandé au tribunal administratif de Marseille d'annuler les opérations électorales qui se sont déroulées le 29 mars 2015 dans le canton de Briançon 2 en vue des élections au conseil départemental, de rejeter le compte de campagne de M. C...A...et de Mme F...B...et de prononcer leur inéligibilité pour une durée d'un an.<br/>
<br/>
              Par un jugement n° 1502564 du 2 octobre 2015, le tribunal administratif a annulé l'élection de M. A...et Mme B...et rejeté le surplus des conclusions de la protestation.  <br/>
<br/>
              Par une requête, enregistrée le 30 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a annulé les opérations électorales ;<br/>
<br/>
              2°) de rejeter la protestation de M. D...et Mme H...;<br/>
<br/>
              3°) de mettre à la charge de M. D...et Mme H...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code électoral ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à l'issue du second tour des opérations électorales qui ont eu lieu les 22 et 29 mars 2015 en vue de la désignation des conseillers départementaux dans le canton de Briançon 2, M. C...A...et Mme F...B...ont été proclamés élus par 1 892 voix contre 1 882 pour le binôme composé de M. E...D...et de Mme G...H... ; que M. A...et Mme B...relèvent appel du jugement du 2 octobre 2015 du tribunal administratif de Marseille en tant qu'il annule ces opérations électorales ; que M. D...et Mme H...forment appel contre ce jugement en tant qu'il a écarté leurs conclusions tendant au rejet du compte de campagne de M. A...et de Mme B...et à ce que ceux-ci soient déclarés inéligibles ;<br/>
<br/>
              Sur le bien fondé du jugement du tribunal administratif :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 62-1 du code électoral : " Pendant toute la durée des opérations électorales, une copie de la liste électorale certifiée par le maire et comportant les mentions prescrites par les articles L. 18 et L. 19 ainsi que le numéro d'ordre attribué à chaque électeur, reste déposée sur la table à laquelle siège le bureau. Cette copie constitue la liste d'émargement. Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement " ; que le second alinéa de l'article L. 64 du code électoral dispose que : " Lorsqu'un électeur se trouve dans l'impossibilité de signer, l'émargement prévu par le troisième alinéa de l'article L. 62-1 est apposé par un électeur de son choix qui fait suivre sa signature de la mention suivante : " l'électeur ne peut signer lui-même " "; qu'il résulte de ces dispositions, destinées à assurer la sincérité des opérations électorales, que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf mise en oeuvre de la faculté de signature par un tiers dans les conditions définies au second alinéa de l'article L. 64 ;<br/>
<br/>
              3. Considérant que les attestations et pièces produites par les électeurs ayant voté dans la commune de Briançon sous le n° 364 dans le bureau n° 1, sous le n° 1314 dans le bureau n° 3 et sous le n° 150 dans le bureau n° 4 permettent de démontrer que ces électeurs ont signé eux-mêmes la liste d'émargement, de leur nom complet pour l'un des deux tours et de leurs initiales ou paraphe pour l'autre ; que les signatures figurant sur la liste d'émargement en face des noms des électeurs ayant voté sous le n° 126 dans le bureau n° 3, sous le n° 1689 dans le bureau n° 4 et sous les n° 964 et 1175 dans le bureau n° 5 sont similaires à celles figurant sur leur carte d'identité, dont la copie a été fournie à l'appui des attestations produites ; que si chacun des électeurs ayant voté sous les n° 140 et 351 dans le bureau de Montgenèvre chef-lieu, qui portent le même nom de famille, a signé accidentellement soit au premier, soit au second tour sur la ligne correspondant au nom de l'autre, cette erreur matérielle n'est pas suffisante pour regarder leurs votes comme ayant été irrégulièrement exprimés ; <br/>
<br/>
              4. Considérant en revanche que s'agissant des électeurs ayant voté à Briançon sous le n° 1242 dans le bureau n° 3 et sous les n° 695, 780 et 1704 dans le bureau n° 5 ainsi que sous les n° 245 et 45 dans le bureau de Montgenèvre n° 1, la signature apposée pour le second tour de scrutin est différente de celle apposée pour le premier tour de scrutin sans qu'aucun élément, y compris les signatures figurant sur les cartes d'identité produites, ne permette de justifier de telles différences ; que ces votes ont par suite été irrégulièrement émis ; qu'il convient également d'écarter le vote de l'électeur ayant voté sous le n° 1253 dans le bureau n° 4 de Briançon dès lors que la croix qui figure dans la liste d'émargement ne peut être regardée comme permettant d'authentifier son vote, les électeurs ayant la faculté en cas d'impossibilité de signer de faire signer la personne de leur choix avec mention sur la liste d'émargement ; qu'il y a lieu également d'écarter le suffrage de l'électeur ayant voté sous le n° 144 dans le bureau n° 1 dès lors que si les signatures des deux tours de scrutin sont rigoureusement identiques, celle du premier tour est accompagnée de la mention selon laquelle l'électeur n'a pu signer lui-même sans que cette mention figure pour le second tour, alors qu'aucune circonstance ne permet de regarder la mention portée sur la liste d'émargement au premier tour comme valant également pour le second ;<br/>
<br/>
              5. Considérant par ailleurs que les signatures des électeurs ayant voté sous les n° 88, 91 et 96 dans le bureau de Montgenèvre n° 2 ne sauraient être regardées comme permettant d'authentifier les suffrages que ces électeurs ont pu exprimer au second tour, dès lors que ces signatures figurent dans la case réservée au premier tour et que les flèches manuscrites dirigées vers les cases correspondant au second tour ne sont pas accompagnées de mentions figurant au procès-verbal permettant d'authentifier que ces électeurs auraient signé dans un emplacement erroné  ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que onze suffrages doivent être regardés comme ayant été irrégulièrement exprimés et être hypothétiquement déduits du nombre de voix obtenu par le binôme élu ; que, ce chiffre étant supérieur à l'écart de dix voix constaté entre les binômes de candidats, ces irrégularités doivent être regardées comme ayant pu altérer la sincérité du scrutin ; qu'il suit de là que M. A...et Mme B...ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Marseille a annulé les opérations électorales qui ont eu lieu dans le canton de Briançon 2 les 22 et 29 mars 2015 ; <br/>
<br/>
              Sur les griefs relatifs à la régularité des comptes de campagne des candidats élus :<br/>
<br/>
              7. Considérant que M. D...et Mme H...présentent à nouveau en cause d'appel des conclusions tendant au rejet du compte de campagne des candidats élus et au prononcé de leur inéligibilité pour une durée d'un an ; que ces conclusions ont été présentées dans un mémoire enregistré après le délai d'appel défini par l'article R. 116 du code électoral ; que dès lors que la voie du recours incident n'est pas ouverte en matière électorale, ces conclusions ne peuvent qu'être rejetées comme irrecevables ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D...et de Mme H...qui ne sont pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...et de Mme B...les sommes que M. D...et de Mme H...demandent sur le même fondement ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. A...et de Mme B...est rejetée. <br/>
<br/>
Article 2 : Les conclusions présentées par M. D...et Mme H...sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C...A..., Mme F...B..., M. E... D..., Mme G...H...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
