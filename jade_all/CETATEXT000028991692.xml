<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028991692</ID>
<ANCIEN_ID>JG_L_2014_05_000000354903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/99/16/CETATEXT000028991692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 26/05/2014, 354903</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354903.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société IMS Health, dont le siège est, 91 rue Jean Jaurès à Puteaux (92800), représentée par sa présidente en exercice ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération n° 2011-246 du 8 septembre 2011 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a autorisé la mise en oeuvre par la société Celtipharm d'un traitement de données à caractère personnel ayant pour finalité la réalisation d'études épidémiologiques à partir de données issues des feuilles de soins électroniques anonymisées à bref délai ; <br/>
<br/>
              2°) de mettre à la charge de la CNIL la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 avril 2014, présentée pour la société IMS Health ; <br/>
<br/>
              Vu le code pénal ; <br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978, modifiée par la loi n° 2004-801 du 6 août 2004 ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, avocat de la société Ims Health  et à la SCP Odent, Poulet, avocat de la société Celtipharm ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par une délibération du 8 septembre 2011, dont la société IMS Health demande l'annulation pour excès de pouvoir, la Commission nationale de l'informatique et des libertés (CNIL) a autorisé la société Celtipharm à mettre en oeuvre un traitement de données issues des feuilles de soins anonymisées à bref délai, sur le fondement du III de l'article 8 et de l'article 25 de la loi du 6 janvier 1978 relative à l'informatique et aux libertés, ayant pour but la réalisation d'études statistiques relatives à la consommation de produits de santé ; qu'en vertu de cette délibération, les organismes concentrateurs techniques, qui assurent le routage des feuilles de soins électroniques vers les caisses d'assurance-maladie pour le compte et sur instruction des pharmaciens d'officine, transmettent également ces données à la société Celtipharm, après les avoir anonymisées par un processus de " hachage " irréversible, s'agissant des données identifiant les patients et sous forme cryptée, s'agissant de celles qui identifient les professionnels de santé ; que la société Celtipharm procède ensuite à une seconde anonymisation des données relatives aux patients, qui demeurent toutefois rattachables à un même individu anonyme via un identifiant unique; qu'elle procède par ailleurs au décryptage puis à l'anonymisation des données identifiant les professionnels de santé ; que ce n'est qu'à l'issue de ce processus que la société Celtipharm procède au traitement des données contenues dans les feuilles de soins électroniques à des fins statistiques et de recherche scientifique ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              En ce qui concerne les moyens tirés de l'irrégularité de la procédure d'adoption de la délibération attaquée : <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ne résulte d'aucune disposition législative ou réglementaire ni d'aucun principe que les délibérations de la CNIL doivent comporter des mentions relatives à la formation dans laquelle elle s'est réunie ; que, par suite, le moyen tiré de ce que la délibération attaquée serait entachée d'irrégularité au motif qu'elle ne comporterait aucune mention permettant de s'assurer qu'elle a été adoptée par la formation plénière de la CNIL ne peut qu'être écarté ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que dès lors que la procédure d'édiction de la délibération attaquée est exclusivement régie par les dispositions de l'article 3 du décret du 20 octobre 2005, aux termes desquelles : " Les délibérations de la commission sont prises à la majorité absolue des membres présents. / Toutefois, sont prises à la majorité absolue des membres composant la commission, réunie en formation plénière, les délibérations suivantes : (...) 3° Les autorisations délivrées par la commission lorsqu'elle est saisie de la création de traitements mentionnés à l'article 25 de la loi du 6 janvier 1978 susvisée ; (...) ", le moyen tiré de la méconnaissance des dispositions de l'article 5 du règlement intérieur de la CNIL, dans sa version applicable à la date de la décision attaquée, ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'il  ressort des pièces du dossier que les membres de la formation plénière de la CNIL ont été informés que la délibération attaquée devait faire l'objet d'un vote sans débat plus de deux jours ouvrés avant la séance du 8 septembre 2011 au cours de laquelle elle a été adoptée ; qu'il suit de là que le moyen tiré de ce que la délibération attaquée aurait été adoptée en méconnaissance des dispositions des articles 13 et 14 du règlement intérieur de la CNIL, dans sa version applicable à la date de la délibération attaquée, manque en fait ; <br/>
<br/>
              En ce qui concerne le moyen tiré de l'insuffisance de motivation de la délibération attaquée : <br/>
<br/>
              5. Considérant qu'il résulte de l'article 2 de la loi du 11 juillet 1979 que " doivent (...) être motivées les décisions administratives qui dérogent aux règles générales fixées par la loi ou le règlement " ; qu'aux termes du I de l'article 8 de la loi du 6 janvier 1978 : " Il est interdit de collecter ou de traiter des données à caractère personnel, qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses et l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci. " ; que toutefois, aux termes du III de ce même article : " Si les données à caractère personnel visées au I sont appelées à faire l'objet à bref délai d'un processus d'anonymisation préalablement reconnu conforme aux dispositions de la présente loi par la Commission nationale de l'informatique et des libertés, celle-ci peut autoriser, compte tenu de leur finalité, certaines catégories de traitements selon les modalités prévues à l'article 25 " ; qu'il résulte de ces dispositions combinées que le III de l'article 8 définit les possibilités de dérogation à l'interdiction de principe posée au I du même article ; qu'il suit de là que la délibération attaquée, dès lors qu'elle a été prise sur le fondement des dispositions du III de l'article 8 de la loi du 6 janvier 1978, est, contrairement à ce qui est soutenu en défense, au nombre des actes devant obligatoirement être motivés en vertu de l'article 2 de la loi du 11 juillet 1979 ; <br/>
<br/>
              6. Considérant que la délibération litigieuse vise l'ensemble des dispositions législatives et réglementaires applicables au traitement qu'elle autorise et énumère de manière détaillée les finalités de ce traitement, les données traitées ainsi que les caractéristiques particulières du traitement, au nombre desquelles figurent les modalités d'anonymisation des données contenues dans les feuilles de soins électroniques ; qu'ainsi, contrairement à ce que soutient la société requérante, elle comporte l'énoncé de l'ensemble des considérations de droit et de fait qui constituent son fondement ; qu'il suit de là que le moyen tiré de la méconnaissance de l'article 2 de la loi du 11 juillet 1979 doit être écarté ;  <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance, par la Commission nationale de l'informatique et des libertés, de sa propre interprétation des dispositions législatives et réglementaire dont elle a pour mission d'assurer l'application : <br/>
<br/>
              7. Considérant que la société requérante ne saurait utilement opposer à la CNIL l'interprétation que celle-ci aurait faite par le passé des dispositions législatives et réglementaires dont elle a pour mission d'assurer l'application, qui ne portait au demeurant pas sur les dispositions de la loi du 6 janvier 1978 dans sa version issue de la loi du 6 août 2004 ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance, par la délibération attaquée, des dispositions du III de l'article 8 de la loi du 6 janvier 1978 :<br/>
<br/>
              8. Considérant que, pour la mise en oeuvre du traitement autorisé par la délibération attaquée, les données cryptées contenues dans les feuilles de soins électroniques sont appelées à être décryptées par la société Celtipharm au moyen de clés de déchiffrement fournies par le GIE Sésame-Vitale et insérées dans une " boîte noire " ; que, s'agissant des donnés identifiant les professionnels de santé, la délibération attaquée prévoit qu'il sera procédé à leur anonymisation par la société Celtipharm ; que si la société requérante soutient que la délibération attaquée serait illégale au motif que la CNIL ne pouvait autoriser le traitement en litige sans s'assurer au préalable des conditions dans lesquelles ces clés de déchiffrement seraient fournies à la société requérante, les conditions d'obtention de ces clés et les éventuelles difficultés rencontrées par la société Celtipharm pour ce faire sont sans incidence sur le respect, par la délibération attaquée, des dispositions de la loi du 6 janvier 1978 ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la violation du secret professionnel et de la vie privée des patients : <br/>
<br/>
              9. Considérant qu'aux termes de l'article 226-13 du code pénal : " La révélation d'une information à caractère secret par une personne qui en est dépositaire soit par état ou par profession, soit en raison d'une fonction ou d'une mission temporaire, est punie d'un an d'emprisonnement et de 15000 euros d'amende. " ; que, par ailleurs, les articles L. 161-36-1-A du code de la sécurité sociale et 1er de la loi du 6 janvier 1978 garantissent le droit au respect de la vie privée, qui implique le secret des données contenues dans les feuilles de soins électroniques ; que toutefois, dès lors que ces données font l'objet d'une anonymisation irréversible, le traitement autorisé par la délibération attaquée ne saurait avoir pour effet de porter atteinte au secret professionnel et au respect de la vie privée des patients ; <br/>
<br/>
              En ce qui concerne le moyen tiré du détournement du traitement des données issues des feuilles de soins électroniques à des fins purement commerciales :  <br/>
<br/>
              10. Considérant qu'aux termes de l'article 226-21 du code pénal : " Le fait, par toute personne détentrice de données à caractère personnel à l'occasion de leur enregistrement, de leur classement, de leur transmission ou de toute autre forme de traitement, de détourner ces informations de leur finalité telle que définie par la disposition législative, l'acte réglementaire ou la décision de la Commission nationale de l'informatique et des libertés autorisant le traitement automatisé, ou par les déclarations préalables à la mise en oeuvre de ce traitement, est puni de cinq ans d'emprisonnement et de 300 000 euros d'amende. " ; que, selon le 2° de l'article 6 de la loi du 6 janvier 1978, les données à caractère personnel " sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. Toutefois, un traitement ultérieur de données à des fins statistiques ou à des fins de recherche scientifique ou historique est considéré comme compatible avec les finalités initiales de la collecte des données, s'il est réalisé dans le respect des principes et des procédures prévus au présent chapitre, au chapitre IV et à la section 1 du chapitre V ainsi qu'aux chapitres IX et X et s'il n'est pas utilisé pour prendre des décisions à l'égard des personnes concernées " ; qu'il résulte de ces dispositions que les traitements de données initialement collectées pour d'autre finalités à des fins statistiques ou de recherche scientifique sont réputés compatibles avec ces finalités initiales, sous les réserves énoncées au 2° de l'article 6 de la loi du 6 janvier 1978 ;  <br/>
<br/>
              11. Considérant que le traitement autorisé par la délibération attaquée a pour but, ainsi qu'il a été dit au point 1, le traitement de données issues des feuilles de soins électroniques à des fins statistiques et de recherche scientifique en vue de la réalisation d'études relatives à la consommation de médicaments ; que, dès lors, un tel traitement doit être regardé comme compatible avec les finalités initiales de la collecte des données figurant dans les feuilles de soins électroniques en vertu des dispositions du 2° de l'article 6 de la loi du 6 janvier 1978 ; qu'au demeurant, dès lors que le traitement litigieux a été autorisé par délibération de la CNIL, l'utilisation, dans ce cadre, de données issues des feuilles des soins électroniques ne saurait relever du champ des infractions réprimées par l'article 226-21 du code pénal ; que, dans ces conditions, il ne saurait être reproché au traitement autorisé par la délibération attaquée d'organiser le détournement de leur finalité des informations figurant dans les feuilles de soins électroniques ; qu'il suit de là que le moyen tiré de la méconnaissance des dispositions de l'article 226-21 du code pénal doit être écarté ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance, par la délibération attaquée, des dispositions de l'article 6 de la loi du 6 janvier 1978 : <br/>
<br/>
              12. Considérant qu'il résulte des motifs énoncés au point précédent que les finalités du traitement autorisé par la délibération attaquée, au demeurant conformes à la volonté du législateur, doivent être regardées comme légitimes eu égard à leur objet, qui tend à l'amélioration de la connaissance relative à la consommation de produits de santé ; que les données collectées, dès lors qu'elle font l'objet d'un processus d'anonymisation, sont également adéquates, pertinentes et non excessives au regard des finalités poursuivies ; qu'il suit de là que le moyen tiré de la méconnaissance de l'article 6 de la loi de 1978 ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance de l'article 35 de la loi du 6 janvier 1978 : <br/>
<br/>
              13. Considérant qu'aux termes de l'article 35 de la loi du 6 janvier 1978 : " les données à caractère personnel ne peuvent faire l'objet d'une opération de traitement de la part d'un sous-traitant, d'une personne agissant sous l'autorité du responsable de traitement ou de celle du sous-traitant que sur instruction du responsable de traitement. / Toute personne traitant des données à caractère personnel pour le compte du responsable de traitement est considérée comme un sous-traitant au sens de la présente loi. " ; que, selon l'article 45-3 de la convention nationale organisant les rapports entre les pharmaciens titulaires d'officine et l'assurance-maladie, approuvée par arrêté du 11 juillet 2006 et adopté sur le fondement de l'article L. 161-34 du code de la sécurité sociale, " le pharmacien peut recourir à un service informatique notamment par un contrat avec un organisme concentrateur technique (OCT). Cet organisme tiers agit pour le compte et sous la responsabilité du pharmacien dans le respect : / - des dispositions légales et réglementaires ayant trait plus particulièrement à l'informatique et aux libertés, ainsi qu'à la confidentialité et à l'intégrité des feuilles de soins électroniques ; / - des dispositions du cahier des charges OCT publié par le GIE SESAM-Vitale " ; <br/>
<br/>
              14. Considérant que si la société requérante soutient que le traitement autorisé par la délibération qu'elle attaque est illégal en ce qu'il permettrait aux organismes concentrateurs techniques d'agir pour le compte de la société Celtipharm, l'anonymisation des données identifiant les patients et la transmission de ces données anonymisées ne peuvent être faites que sur instruction des pharmaciens d'officine participants, de même que la transmission des données cryptées identifiant les professionnels de santé aux fins d'anonymisation à bref délai par la société Celtipharm ; qu'il suit de là que le moyen tiré de la méconnaissance de l'article 35 de la loi de 1978 ne peut qu'être écarté ;  <br/>
<br/>
              En ce qui concerne le moyen tiré du caractère illégal de la collecte et du traitement du numéro d'inscription au répertoire des personnes physiques par une entreprise privée : <br/>
<br/>
              15. Considérant que la délibération attaquée précise que les organismes concentrateurs techniques, qui collectent les données issues des feuilles de soins électroniques, procèdent ensuite à une première anonymisation de celles de ces données qui sont susceptibles d'identifier les patients, au nombre desquelles figure le numéro d'inscription au répertoire national des personnes physiques, avant de les transmettre à la société Celtipharm ; <br/>
<br/>
              16. Considérant que l'article R. 115-1 du code de la sécurité sociale énumère les personnes et organismes autorisés à utiliser le numéro d'inscription au répertoire national des personnes physiques ; qu'en vertu de l'article R. 115-2 de ce même code, cette autorisation vaut exclusivement pour les traitements énumérés à cet article, au nombre desquels ne figurent pas les traitements à des fins statistiques ou de recherche scientifique ; <br/>
<br/>
              17. Considérant, toutefois, que la loi du 6 janvier 1978, dans sa version issue de la loi du 6 août 2004, ne subordonne plus systématiquement la mise en oeuvre de traitements de données à caractère personnel incluant le numéro d'inscription au répertoire national des personnes physiques à une autorisation par décret en Conseil d'Etat ; qu'en effet, certains d'entre eux peuvent désormais être autorisés par délibération de la CNIL, y compris en dehors des cas prévus par les dispositions du code de la sécurité sociale mentionnées au point 16 ; que tel est notamment le cas du traitement de données à caractère personnel à des fins statistiques et de recherche scientifique en litige, dont les finalités doivent, ainsi qu'il a été dit au point 10, être regardées comme compatibles avec les finalités initiales de la collecte des données qu'il contient, qui a été autorisé par la CNIL sur le fondement des dispositions combinées du III. de l'article 8 de la loi du 6 janvier 1978 et de l'article de 25 de cette même loi, auquel il renvoie ; qu'il suit de là que le moyen tiré de ce que la délibération attaquée autoriserait la collecte et le traitement du numéro d'inscription au répertoire national des personnes physiques dans des conditions illégales doit être écarté ; <br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la recevabilité de la requête, la société IMS Health n'est pas fondée à demander l'annulation de la délibération qu'elle attaque ; <br/>
<br/>
              19. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société IMS Health la somme de 3 000 euros à verser à la société Celtipharm au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société IMS Health est rejetée.<br/>
Article 2 : La société IMS Health versera à la société Celtipharm la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société IMS Health, à la Commission nationale de l'informatique et des libertés et à la société Celtipharm.<br/>
Copie pour information en sera adressée à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-05 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION DÉROGEANT AUX RÈGLES GÉNÉRALES FIXÉES PAR LA LOI OU LE RÈGLEMENT. - DÉLIBÉRATIONS DE LA CNIL AUTORISANT, PAR DÉROGATION À L'INTERDICTION POSÉE AU I DE L'ARTICLE 8 DE LA LOI DU 6 JANVIER 1978, CERTAINES CATÉGORIES DE TRAITEMENT DE DONNÉES SENSIBLES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-02-01 DROITS CIVILS ET INDIVIDUELS. - PRINCIPE - INTERDICTION DE COLLECTER ET DE TRAITER CES DONNÉES - EXCEPTION - DÉROGATIONS AUTORISÉES PAR LA CNIL SUR LE FONDEMENT DU III DE L'ARTICLE 8 DE LA LOI DU 6 JANVIER 1978 - MOTIVATION OBLIGATOIRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-05 Les délibérations par lesquelles la Commission nationale de l'informatique et des libertés (CNIL), sur le fondement des dispositions du III de l'article 8 de la loi n° 78-17 du 6 janvier 1978 qui définissent les possibilités de dérogation à l'interdiction de principe posée au I du même article, autorise, compte tenu de leurs finalités, certaines catégories de traitement de données sensibles, sont au nombre des actes devant obligatoirement être motivés en vertu de l'article 2 de la loi n° 79-587 du 11 juillet 1979.</ANA>
<ANA ID="9B"> 26-07-02-01 Les délibérations par lesquelles la Commission nationale de l'informatique et des libertés (CNIL), sur le fondement des dispositions du III de l'article 8 de la loi n° 78-17 du 6 janvier 1978 qui définissent les possibilités de dérogation à l'interdiction de principe posée au I du même article, autorise, compte tenu de leurs finalités, certaines catégories de traitement de données sensibles, sont au nombre des actes devant obligatoirement être motivés en vertu de l'article 2 de la loi n° 79-587 du 11 juillet 1979.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
