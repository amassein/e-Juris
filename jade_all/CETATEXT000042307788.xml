<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042307788</ID>
<ANCIEN_ID>J2_L_2020_07_000001904088</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/30/77/CETATEXT000042307788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de LYON, 1ère chambre, 07/07/2020, 19LY04088, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-07</DATE_DEC>
<JURIDICTION>CAA de LYON</JURIDICTION>
<NUMERO>19LY04088</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme MARGINEAN-FAURE</PRESIDENT>
<AVOCATS>MOREL</AVOCATS>
<RAPPORTEUR>Mme Christine  PSILAKIS</RAPPORTEUR>
<COMMISSAIRE_GVT>M. LAVAL</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
       M. C... B... a demandé au tribunal administratif de Lyon d'annuler l'arrêté du 26 août 2019 par lequel le préfet du Rhône l'a obligé à quitter le territoire français sans délai, a fixé le pays à destination duquel il pourra être éloigné d'office et l'a assigné à résidence.<br/>
<br/>
       Par un jugement n° 1906722 du 2 septembre 2019, la magistrate désignée par la présidente du tribunal administratif de Lyon a rejeté cette demande.<br/>
<br/>
<br/>
Procédure devant la cour<br/>
<br/>
       Par une requête enregistrée le 12 novembre 2019, M. B..., représenté par Me A..., demande à la cour :<br/>
       1°) d'annuler ce jugement du tribunal administratif de Lyon du 2 septembre 2019 ainsi que les décisions du préfet du Rhône du 26 août 2019 ; <br/>
       2°) d'enjoindre au préfet du Rhône de lui délivrer une carte de séjour temporaire ou à défaut, réexaminer sa situation et dans l'attente de lui délivrer une attestation provisoire de séjour l'autorisant à travailler, dans le délai d'un mois à compter de la notification du présent arrêt ;<br/>
       3°) de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros à verser à son conseil en application des dispositions combinées de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
       Il soutient que :  <br/>
       - la décision lui faisant obligation de quitter le territoire méconnaît l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et est entachée d'erreur manifeste d'appréciation quant à ses conséquences sur sa situation personnelle ; <br/>
       - la décision lui refusant un délai de départ volontaire ne pouvait valablement être fondée sur le 1° de l'article L. 511-1 II du code de l'entrée et du séjour des étrangers et du droit d'asile au seul motif qu'il a été mis en cause pénalement pour des faits de conduite sans permis ; <br/>
       - c'est à tort que le préfet lui a refusé un délai de départ volontaire au motif qu'il ne dispose pas de garanties de représentation suffisantes ;<br/>
       - la décision fixant le pays de destination est illégale du fait de l'illégalité de la décision lui faisant obligation de quitter le territoire qui la fonde ; <br/>
       - la décision l'assignant à résidence est illégale du fait de l'illégalité du refus de titre de séjour qui la fonde.<br/>
<br/>
<br/>
       Le préfet du Rhône n'a pas produit de mémoire en défense. <br/>
<br/>
       M. B... a été admis à l'aide juridictionnelle totale par une décision du bureau d'aide juridictionnelle du 16 octobre 2019.<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
       Vu :<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
       - le code de justice administrative ;<br/>
<br/>
<br/>
       Le président de la formation de jugement ayant dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience ;<br/>
<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
<br/>
       Après avoir entendu au cours de l'audience publique le rapport de Mme D..., première conseillère ;<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
       1. M. C... B..., ressortissant sénégalais né en 1983, est entré en France au mois de novembre 2017 sous couvert d'un visa long séjour après son mariage avec une ressortissante française au Sénégal le 31 août 2017. Il relève appel du jugement du 2 septembre 2019 par lequel la magistrate désignée par la présidente du tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de l'arrêté du 26 août 2019 par lequel le préfet du Rhône l'a obligé à quitter le territoire français sans délai, a fixé le pays à destination duquel il pourra être éloigné d'office et l'a assigné à résidence.<br/>
<br/>
       Sur la décision portant obligation de quitter le territoire :<br/>
<br/>
       2. Pour demander l'annulation de l'obligation de quitter le territoire dont il fait l'objet, M. B... se prévaut de son intégration professionnelle et de ses attaches familiales en France en dépit de la  procédure de divorce en cours et réitère les moyens soulevés devant le tribunal administratif tirés de l'erreur manifeste d'appréciation quant à ses conséquences sur sa situation personnelle commise par le préfet et de la violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Alors que M. B... n'apporte aucun élément supplémentaire sur sa situation personnelle et professionnelle, il y a lieu d'écarter ces moyens par adoption des motifs retenus à bon droit par le premier juge.<br/>
<br/>
       Sur la décision lui refusant un délai de départ volontaire :<br/>
<br/>
       3. L'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose : " II. (...) Toutefois, l'autorité administrative peut, par une décision motivée, décider que l'étranger est obligé de quitter sans délai le territoire français : (...) 3° S'il existe un risque que l'étranger se soustraie à cette obligation. Ce risque peut être regardé comme établi, sauf circonstance particulière, dans les cas suivants : / b) Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa ou, s'il n'est pas soumis à l'obligation du visa, à l'expiration d'un délai de trois mois à compter de son entrée en France, sans avoir sollicité la délivrance d'un titre de séjour ;/ (...) f) Si l'étranger ne présente pas de garanties de représentation suffisantes, notamment parce qu'il ne peut présenter des documents d'identité ou de voyage en cours de validité, qu'il a refusé de communiquer les renseignements permettant d'établir son identité ou sa situation au regard du droit de circulation et de séjour ou a communiqué des renseignements inexacts, qu'il a refusé de se soumettre aux opérations de relevé d'empreintes digitales ou de prise de photographie prévues au deuxième alinéa de l'article L. 611-3, qu'il ne justifie pas d'une résidence effective et permanente dans un local affecté à son habitation principale ou qu'il s'est précédemment soustrait aux obligations prévues aux articles L. 513-4, L. 513-5, L. 552-4, L. 561-1, L. 561-2 et L. 742-2 ; (...). ".<br/>
<br/>
       4. Il ressort des pièces du dossier que le préfet du Rhône a refusé le bénéfice d'un délai de départ volontaire à M. B... aux motifs qu'il se maintenait irrégulièrement sur le territoire français depuis plus de neuf mois après l'expiration de son visa sans avoir entrepris de démarches pour régulariser sa situation, qu'il avait été interpellé et placé en garde à vue pour des faits, traités en flagrant délit, de conduite sans permis de conduire et qu'il ne justifiait pas de la réalité de moyens d'existence effectifs. Si le comportement de M. B..., qui n'a d'ailleurs pas donné lieu à des poursuites pénales, ne constitue pas, dans les circonstances de l'espèce, une menace pour l'ordre public, il n'est pas contesté que le requérant, dépourvu de passeport ou de tout autre document de voyage, se maintenait irrégulièrement sur le territoire français. Dès lors, pour ce seul et dernier motif, le préfet du Rhône pouvait légalement prendre la décision lui refusant l'octroi d'un délai de départ volontaire pour l'exécution de l'obligation de quitter le territoire français. Dans ces conditions, le préfet du Rhône n'a pas inexactement appliqué le II de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ni entaché sa décision d'une erreur manifeste d'appréciation.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       Sur la décision fixant le pays de renvoi :<br/>
<br/>
       5. Il résulte de ce qui est dit ci-dessus aux points 2 à 4 que M. B... n'est pas fondé à invoquer l'illégalité de l'obligation de quitter le territoire français pour demander l'annulation, par voie de conséquence, de la décision fixant le pays de renvoi.<br/>
<br/>
       Sur la décision l'assignant à résidence : <br/>
<br/>
       6. Il résulte de ce qui est dit ci-dessus aux points 2 à 4 que M. B... n'est pas fondé à invoquer l'illégalité de l'obligation de quitter le territoire français pour demander l'annulation, par voie de conséquence, de la décision l'assignant à résidence.<br/>
<br/>
       7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, la magistrate désignée par la présidente du tribunal administratif de Lyon a rejeté sa demande. Ses conclusions à fin d'injonction ainsi que celles tendant à l'application, au bénéfice de son avocat, des dispositions de l'article L. 761-1 du code de justice administrative, doivent être rejetées par voie de conséquence.<br/>
<br/>
<br/>
DÉCIDE :<br/>
Article 1er :	La requête de M. B... est rejetée.<br/>
Article 2 :	Le présent arrêt sera notifié à M. C... B... et au ministre de l'intérieur. <br/>
Copie en sera adressée au préfet du Rhône.<br/>
Délibéré après l'audience du 23 juin 2020 à laquelle siégeaient :<br/>
Mme F..., présidente de chambre ;<br/>
M. Thierry Besse, président-assesseur ;<br/>
Mme E... D..., première conseillère.<br/>
Lu en audience publique, le 7 juillet 2020.<br/>
2<br/>
N° 19LY04088<br/>
md<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03 Étrangers. Obligation de quitter le territoire français (OQTF) et reconduite à la frontière.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
