<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018838990</ID>
<ANCIEN_ID>JG_L_2008_05_000000290241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/83/89/CETATEXT000018838990.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 21/05/2008, 290241</TITRE>
<DATE_DEC>2008-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>290241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bruno  Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Aguila Yann</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 février et 12 juin 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE DU DOMAINE DE SAINTE-MARCELLE, dont le siège est La Chapelle sur Usson au Vernet la Varenne (63580) ; la SOCIETE DU DOMAINE DE SAINTE-MARCELLE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt en date du 15 décembre 2005 par lequel la cour administrative d'appel de Lyon a, d'une part, annulé, sur le recours du ministre de l'aménagement du territoire et de l'environnement, le jugement du 26 juin 2001 du tribunal administratif de Clermont-Ferrand annulant l'arrêté du 19 mars 1998 par lequel le préfet du Puy-de-Dôme a refusé de délivrer à la société requérante une autorisation d'exploiter une carrière de basaltes avec unité de concassage sur le territoire de la commune de Vertaizon, et, d'autre part, rejeté la demande de la société devant le tribunal administratif de Clermont-Ferrand ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le recours du préfet contre le jugement du tribunal administratif de Clermont-Ferrand ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SOCIÉTÉ DU DOMAINE DE SAINTE-MARCELLE, <br/>
<br/>
              - les conclusions de M. Yann Aguila, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par un jugement en date du 26 juin 2001, le tribunal administratif de Clermont-Ferrand a annulé l'arrêté du 19 mars 1998 par lequel le préfet du Puy-de-Dôme a refusé de délivrer à la SOCIETE DU DOMAINE DE SAINTE-MARCELLE l'autorisation d'exploiter une carrière de basalte dans le secteur dit du « Puy-de-Mur », situé sur le territoire de la commune de Vertaizon et classé en zone ND par le plan d'occupation des sols ; que la société se pourvoit contre l'arrêt du 15 décembre 2005 par lequel la cour administrative d'appel de Lyon a, sur le recours du ministre de l'aménagement du territoire et de l'environnement, annulé le jugement du tribunal administratif et rejeté sa demande ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant que les dispositions du troisième alinéa de l'article L. 123-5 du code de l'urbanisme, dans leur rédaction alors en vigueur, selon lesquelles « lorsqu'un plan a été rendu public avant le classement des carrières dans la nomenclature des installations classées, seules sont opposables à l'ouverture des carrières les dispositions du plan les visant expressément », ont eu pour objet d'éviter que ce classement ne rende applicable de plein droit aux carrières l'ensemble des prescriptions du plan d'occupation des sols relatives aux installations classées, sans que l'autorité compétente ait été en mesure de se prononcer sur une telle extension ; qu'en revanche, dans l'hypothèse où le plan interdit dans une zone toute occupation ou utilisation des sols, à l'exception de celles qu'il prévoit expressément et dans le champ desquelles ne rentrent pas les carrières, les dispositions en cause de l'article L. 123-5 ne sauraient être interprétées comme permettant l'ouverture de carrières dans cette zone du seul fait que le plan ne s'y oppose pas expressément ; que, par suite, et ainsi que l'a jugé la cour, le règlement de la zone ND du plan d'occupation des sols de la commune de Vertaizon, qui définit cette zone comme « une zone naturelle à vocation agricole, forestière ou touristique » où, pour des raisons de protection des sites et des paysages, « sont interdits toute construction nouvelle et tout aménagement à quelque usage que ce soit », à l'exception des « travaux destinés à faciliter la mise en valeur foncière, agricole, forestière ou touristique », était opposable à la demande d'autorisation d'exploiter une carrière présentée par la SOCIETE DU DOMAINE DE SAINTE-MARCELLE ;<br/>
<br/>
              Considérant, toutefois, que la société a invoqué devant les juges du fond le moyen tiré de l'illégalité du plan d'occupation des sols de la commune, en raison de son incompatibilité avec le schéma directeur de l'agglomération clermontoise ; qu'il ressort des pièces du dossier soumis aux juges du fond que ce schéma énumère six sites susceptibles d'être retenus pour l'implantation d'une nouvelle carrière, rendue nécessaire par la cessation prévisible de l'exploitation de la carrière dite des « Côtes de Clermont » ; qu'il relève que le site du Puy-de-Mur, seul site mentionné par le schéma directeur implanté sur la commune de Vertaizon, est, au vu des études qui ont été menées, l'un des deux sites les plus intéressants ; que, dès lors, en écartant le moyen tiré de l'incompatibilité avec le schéma directeur du plan d'occupation des sols de la commune de Vertaizon, en tant qu'il s'oppose à l'ouverture d'une carrière dans le secteur de Puy-de-Mur, la cour administrative d'appel a donné aux faits de l'espèce une qualification juridique erronée ; que la SOCIETE DU DOMAINE DE SAINTE-MARCELLE est fondée à demander, pour ce motif, l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement à la SOCIETE DU DOMAINE DE SAINTE-MARCELLE de la somme de 3 000 euros au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon en date du 15 décembre 2005 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'Etat versera à la SOCIETE DU DOMAINE DE SAINTE-MARCELLE une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SOCIETE DU DOMAINE DE SAINTE-MARCELLE et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">40-02-01-01-01 MINES ET CARRIÈRES. CARRIÈRES. QUESTIONS GÉNÉRALES. LÉGISLATION SUR LES CARRIÈRES ET AUTRES LÉGISLATIONS. RÈGLES D'URBANISME. - A) ARTICLE L. 123-5 DU CODE DE L'URBANISME - PORTÉE - B) OBLIGATION DE COMPATIBILITÉ ENTRE UN PLAN D'OCCUPATION DES SOLS ET UN SCHÉMA DIRECTEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-005-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. SCHÉMAS DIRECTEURS D'AMÉNAGEMENT ET D'URBANISME. EFFETS. - OBLIGATION DE COMPATIBILITÉ ENTRE UN PLAN D'OCCUPATION DES SOLS ET UN SCHÉMA DIRECTEUR.
</SCT>
<ANA ID="9A"> 40-02-01-01-01 a) Les dispositions du troisième alinéa de l'article L. 123-5 du code de l'urbanisme ont eu pour objet, lorsqu'un plan d'occupation des sols (POS) a été rendu public avant le classement des carrières dans la nomenclature des installations classées, d'éviter que ce classement ne rende applicable de plein droit aux carrières l'ensemble des prescriptions du POS relatives aux installations classées, sans que l'autorité compétente ait été en mesure de se prononcer sur une telle extension. En revanche, lorsqu'un plan d'occupation des sols interdit dans une zone toute occupation ou utilisation des sols, à l'exception de celles qu'il prévoit expressément et dans le champ desquelles ne rentrent pas les carrières, ces dispositions de l'article L. 123-5 du code de l'urbanisme ne sauraient être interprétées comme permettant l'ouverture de carrières dans cette zone du seul fait que le POS ne s'y oppose pas expressément. b) Dès lors que le schéma d'aménagement indiquait un site susceptible d'être retenu pour l'implantation d'une nouvelle carrière et situé sur une zone du territoire de la commune, la cour administrative d'appel, en écartant le moyen tiré de l'incompatibilité du POS interdisant dans la même zone toute occupation ou utilisation des sols, a commis une erreur de qualification juridique des faits.</ANA>
<ANA ID="9B"> 68-01-005-02 Dès lors que le schéma d'aménagement indiquait un site susceptible d'être retenu pour l'implantation d'une nouvelle carrière et situé sur une zone du territoire de la commune, la cour administrative d'appel, en écartant le moyen tiré de l'incompatibilité du POS interdisant dans la même zone toute occupation ou utilisation des sols, a commis une erreur de qualification juridique des faits.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
