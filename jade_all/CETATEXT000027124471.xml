<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027124471</ID>
<ANCIEN_ID>JG_L_2013_03_000000344213</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/44/CETATEXT000027124471.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 01/03/2013, 344213, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344213</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:344213.20130301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 novembre 2010 et 22 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant...,; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 10VE00512 du 2 septembre 2010 par laquelle le président de la 1ère chambre de la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0908930 du 5 janvier 2010 par lequel le tribunal administratif de Montreuil a rejeté sa demande tendant à l'annulation de la décision du 29 juillet 2009 par laquelle le préfet de la Seine-Saint-Denis a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français et a fixé le pays de destination ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de M.A...,<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : (...) / 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens " ; que l'article R. 811-7 du même code dispose que : " Les appels ainsi que les mémoires déposés devant la cour administrative d'appel doivent être présentés, à peine d'irrecevabilité, par l'un des mandataires mentionnés à l'article R. 431-2. / Lorsque la notification de la décision soumise à la cour administrative d'appel ne comporte pas la mention prévue au troisième alinéa de l'article R. 751-5, le requérant est invité par la cour à régulariser sa requête dans les conditions fixées aux articles R. 612-1 et R. 612-2 (...) " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 441-1 du code de justice administrative : " Les parties peuvent, le cas échéant, réclamer le bénéfice de l'aide juridictionnelle prévue par la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique " ; que cette loi prévoit, en son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle et, en son article 25, que le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure d'appel que M. A...avait introduit sa requête sous le ministère d'un avocat, par ailleurs désigné ultérieurement pour le représenter par le bureau d'aide juridictionnelle près le tribunal de grande instance de Versailles ; que, dans ces conditions et alors même que cet avocat n'a pas produit de nouveau mémoire après sa désignation au titre de l'aide juridictionnelle, le président de la 1ère chambre de la cour administrative d'appel a commis une erreur de droit en opposant au requérant une irrecevabilité tirée du défaut de ministère d'avocat ; que, dès lors, son ordonnance doit être annulée ;<br/>
<br/>
              4. Considérant que M. A...a obtenu devant le Conseil d'Etat, le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Monod-Colin, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat, qui succombe dans la présente instance, la somme de 3 000 euros à verser à la SCP Monod-Colin ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 1ère chambre de la cour administrative d'appel de Versailles du 2 septembre 2010 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la SCP Monod-Colin, avocat de M.A..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
