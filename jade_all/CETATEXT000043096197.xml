<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043096197</ID>
<ANCIEN_ID>JG_L_2021_01_000000445855</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/61/CETATEXT000043096197.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/01/2021, 445855, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445855</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:445855.20210129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F... B..., Mme G... D... née B... et Mme C... E... née B... ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de la décision du 12 octobre 2020 de ne pas poursuivre les traitements autres que de sédation, et notamment de mettre fin à la ventilation mécanique, de M. A... B... et, d'autre part, d'ordonner le transfert de M. A... B... dans un autre hôpital relevant de l'Assistance publique-Hôpitaux de Paris (AP-HP), à titre subsidiaire, d'ordonner qu'il soit procédé à une expertise en vue de déterminer la situation médicale de M. A... B.... Par une ordonnance n° 2016803/9 du 20 octobre 2020, le juge des référés du tribunal administratif de Paris a rejeté leur demande.<br/>
<br/>
              Par une requête, enregistrée le 31 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B..., Mme D... et Mme E... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler l'ordonnance du 20 octobre 2020 ; <br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la décision du 12 octobre 2020 du médecin de l'hôpital Lariboisière de ne pas poursuivre les traitements autre que de sédation, et notamment de mettre fin à la ventilation mécanique de M. A... B... ; <br/>
<br/>
              3°) d'ordonner le transfert de M. A... B... dans un autre hôpital relevant de l'AP-HP ;<br/>
<br/>
              4°) à titre subsidiaire, d'ordonner qu'il soit procédé à une expertise en vue de déterminer la situation médicale de M. A... B... ; <br/>
<br/>
              5°) de mettre à la charge de l'AP-HP la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est satisfaite ; <br/>
              - la décision de mettre fin à la ventilation mécanique dont fait l'objet M. B... porte une atteinte grave et manifestement illégale au droit au respect de la vie ; <br/>
              - le maintien des soins et traitements de M. B... ne constitue pas une obstination déraisonnable, dès lors que le syndrome d'enfermement dont il est atteint n'est pas nécessairement irréversible, que les patients atteints de ce syndrome ne sont pas dans un état végétatif ni dans un état pauci-relationnel, mais conservent une conscience, et que la volonté du patient doit être prise en compte.<br/>
              - la décision de mettre fin à la ventilation mécanique dont il fait l'objet présente un caractère manifestement illégal en ce que, d'une part, elle a été prise sur la base d'éléments médicaux couvrant une période trop brève, sans qu'il ait été procédé à l'IRM de réévaluation envisagée le 22 septembre 2020 et sans que le compte rendu de l'IRM à laquelle il a été procédé lors de l'hospitalisation soit produite aux débats, d'autre part le dossier médical établit que M. B... est toujours conscient et dans un état susceptible d'évolution, et, enfin, elle ne prend pas en compte la volonté du patient de continuer de vivre , quel que soit son état de santé, telle qu'elle ressort des indications données par son entourage et de ses convictions religieuses.<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'Assistance publique - hôpitaux de Paris conclut au rejet de la requête. Elle soutient que les moyens soulevés par les requérants ne sont pas fondés. <br/>
<br/>
              Par une ordonnance du 12 novembre 2020, le juge des référés du Conseil d'Etat a, d'une part, ordonné une expertise, diligentée de manière contradictoire, aux fins, en premier lieu, de décrire l'état clinique actuel de M. A... B..., son évolution ainsi que le niveau de souffrance de l'intéressé, en deuxième lieu, de se prononcer sur le caractère irréversible de ses lésions cérébrales et sur le pronostic clinique, en troisième lieu, de déterminer si M. B... est conscient, s'il est susceptible de percevoir la douleur, et dans quelle mesure un code de communication est susceptible d'être mis en place avec lui au stade présent ou dans l'avenir et, d'autre part, ordonné la suspension de l'exécution de la décision du 12 octobre 2020 de mettre fin aux traitements autres que de sédation apportés à M. B..., jusqu'à ce qu'il soit statué sur la requête.<br/>
<br/>
              Par une ordonnance du 23 novembre 2020, le président de la section du contentieux du Conseil d'Etat a désigné le professeur Gérard Audibert, responsable du département d'anesthésie réanimation chirurgicale du centre hospitalier régional universitaire de Nancy, comme expert.<br/>
<br/>
              Le rapport d'expertise a été déposé le 30 décembre 2020.<br/>
<br/>
              Par deux nouveaux mémoires, enregistrés les 6 et 19 janvier 2021, l'Assistance publique - Hôpitaux de Paris persiste dans ses précédentes conclusions et demande que les frais d'expertise ne soient pas mis à sa charge.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code civil ; <br/>
              - le code de la santé publique ; <br/>
              - la décision du Conseil constitutionnel n° 2017-632 QPC du 2 juin 2017 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., Mme D... et Mme E..., d'autre part, l'Assistance publique - hôpitaux de Paris ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 20 janvier 2021 à 11 heures : <br/>
<br/>
              - Me Prigent, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - les représentants des requérants ; <br/>
<br/>
              - M. F... B...<br/>
<br/>
              - les représentants de l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction puis l'a prolongée jusqu'au 28 janvier à 13 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. F... B..., Mme G... D... née B... et Mme C... E... née B... ont relevé appel de l'ordonnance du 20 octobre 2020 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté leur demande tendant à la suspension de l'exécution de la décision du 12 octobre 2020 du médecin de l'Assistance publique - Hôpitaux de Paris en charge de M. A... B..., leur frère, de ne pas poursuivre les traitements qui sont prodigués à celui-ci autres que de sédation, et notamment de mettre fin à sa ventilation mécanique, et, d'autre part, d'ordonner le transfert de M. A... B... dans un autre hôpital relevant de l'Assistance publique Hôpitaux de Paris.<br/>
<br/>
              2. Par une ordonnance du 12 novembre 2020, le juge des référés du Conseil d'Etat a, avant de se prononcer sur la requête, ordonné qu'il soit procédé à une expertise en vue notamment de déterminer l'état clinique de M. A... B... et de se prononcer sur son état de conscience ainsi que sur la possibilité d'établir une communication avec lui et sur le caractère irréversible des lésions cérébrales dont il est atteint et suspendu, dans cette attente, l'exécution de la décision litigieuse.<br/>
<br/>
              3. Par une ordonnance du 23 novembre 2020, le président de la section du contentieux du Conseil d'Etat, a confié cette expertise au professeur Gérard Audibert, responsable du département d'anesthésie réanimation chirurgicale du centre hospitalier régional universitaire de Nancy, qui, après avoir examiné M. B... et rencontré sa famille ainsi que l'équipe paramédicale et médicale, a rendu son rapport le 30 décembre 2020.<br/>
<br/>
              4. Il appartient au juge des référés d'exercer ses pouvoirs de manière particulière, lorsqu'il est saisi, comme en l'espèce, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une décision, prise par un médecin, dans le cadre défini par le code de la santé publique, et conduisant à arrêter ou ne pas mettre en oeuvre, au titre du refus de l'obstination déraisonnable, un traitement qui apparaît inutile ou disproportionné ou sans autre effet que le seul maintien artificiel de la vie, dans la mesure où l'exécution de cette décision porterait de manière irréversible une atteinte à la vie. Il doit alors prendre les mesures de sauvegarde nécessaires pour faire obstacle à son exécution lorsque cette décision pourrait ne pas relever des hypothèses prévues par la loi, en procédant à la conciliation des libertés fondamentales en cause, que sont le droit au respect de la vie et le droit du patient de consentir à un traitement médical et de ne pas subir un traitement qui serait le résultat d'une obstination déraisonnable.<br/>
<br/>
              Sur le cadre juridique applicable au litige :<br/>
<br/>
              5. Il résulte des dispositions des articles L. 1110-1, L. 1110-2, L. 1110-5, L. 1110-5-1, L. 1110-5-2 et L. 1111-4 du code de la santé publique, ainsi que de l'interprétation que le Conseil constitutionnel en a donnée dans sa décision n° 2017-632 QPC du 2 juin 2017, qu'il appartient au médecin en charge d'un patient hors d'état d'exprimer sa volonté d'arrêter ou de ne pas mettre en oeuvre, au titre du refus de l'obstination déraisonnable, les traitements qui apparaissent inutiles, disproportionnés ou sans autre effet que le seul maintien artificiel de la vie. En pareille hypothèse, le médecin ne peut prendre une telle décision qu'à l'issue d'une procédure collégiale, destinée à l'éclairer sur le respect des conditions légales et médicales d'un arrêt du traitement et, sauf dans les cas mentionnés au troisième alinéa de l'article L. 1111-11 du code de la santé publique, dans le respect des directives anticipées du patient ou, à défaut de telles directives, après consultation de la personne de confiance désignée par le patient ou, à défaut, de sa famille ou de ses proches, ainsi que, le cas échéant, de son ou ses tuteurs.<br/>
<br/>
              6. Pour apprécier si les conditions d'un arrêt des traitements de suppléance des fonctions vitales sont réunies s'agissant d'un patient victime de lésions cérébrales graves, quelle qu'en soit l'origine, qui se trouve dans un état végétatif ou dans un état de conscience minimale le mettant hors d'état d'exprimer sa volonté, le médecin en charge doit se fonder sur un ensemble d'éléments, médicaux et non médicaux, dont le poids respectif ne peut être prédéterminé et dépend des circonstances particulières à chaque patient, le conduisant à appréhender chaque situation dans sa singularité. Les éléments médicaux doivent couvrir une période suffisamment longue, être analysés collégialement et porter notamment sur l'état actuel du patient, sur l'évolution de son état depuis la survenance de l'accident ou de la maladie, sur sa souffrance et sur le pronostic clinique. Une attention particulière doit être accordée à la volonté que le patient peut avoir exprimée, par des directives anticipées ou sous une autre forme.<br/>
<br/>
              7. Enfin, si l'alimentation et l'hydratation artificielles ainsi que la ventilation mécanique sont au nombre des traitements susceptibles d'être arrêtés lorsque leur poursuite traduirait une obstination déraisonnable, la seule circonstance qu'une personne soit dans un état irréversible d'inconscience ou, à plus forte raison, de perte d'autonomie la rendant tributaire d'un tel mode d'alimentation, d'hydratation et de ventilation ne saurait caractériser, par elle-même, une situation dans laquelle la poursuite de ce traitement apparaîtrait injustifiée au nom du refus de l'obstination déraisonnable.<br/>
<br/>
              Sur le litige en référé : <br/>
<br/>
              8. Ainsi qu'il a été dit dans les motifs de l'ordonnance du 12 novembre 2020, il résulte de l'instruction que M. A... B..., âgé de 54 ans, a été victime le 15 septembre 2020 d'un accident vasculaire cérébral qui, après l'échec des tentatives successives de revascularisation, a conduit à l'occlusion du tronc basilaire. Tétraplégique, il fait l'objet depuis le 16 septembre 2020 de soins de supports, y compris une ventilation mécanique. Après des dégradations de son état neurologique constatées les 17 et 20 septembre, l'équipe médicale a constaté le 20 septembre qu'il se trouvait dans un coma avec un score de Glasgow de 3, aréactif sans ouverture des yeux, y compris à la stimulation. Si, à partir du 30 septembre, l'ouverture de ses yeux a été constatée, la mise en place d'un code de communication avec lui n'a pas été possible. Le 12 octobre 2020, après information préalable de sa famille et recueil des avis des services de réanimation, neuroradiologie et neurologie, une procédure collégiale a été conduite intégrant l'équipe de réanimation, ainsi que le neuroradiologue ayant pris en charge le patient, en présence d'un médecin extérieur, intervenant à titre de consultant. A la suite de cette procédure, et eu égard aux très graves lésions neurologiques, regardées comme irréversibles, avec, contrairement au syndrome d'enfermement classique, impact sur la possibilité de ventilation et sur l'état de conscience, sans possibilité de mise en place d'un code de communication, mais sans que puisse être exclue la possibilité de douleur ou de stress intense, le médecin chargé du suivi de M. A... B... a pris, le 12 octobre 2020, la décision de mettre fin aux traitements, notamment à la ventilation mécanique, avec mise en place d'une sédation profonde et continue et maintien des soins de confort pour garantir l'absence de souffrance. <br/>
<br/>
              9. Saisi notamment par M. F... B..., son frère, le juge des référés du tribunal administratif de Paris a, par une ordonnance dont il est relevé appel, rejeté la demande de suspension de l'exécution de la décision du 12 octobre 2020.<br/>
<br/>
              10. Il ressort des conclusions de l'expertise réalisée par le professeur Audibert, ordonnée, ainsi qu'il a été dit au point 2, par le juge des référés du Conseil d'Etat, dans le cadre de l'instruction de la présente requête d'appel, que l'état clinique de M. A... B... a été examiné le 16 décembre 2020, plus de 90 jours après son très grave accident vasculaire cérébral, soit " le délai au terme duquel le pronostic clinique est considéré comme fixé en pathologie vasculaire cérébrale ". L'expert précise que M. A... B... a subi une ischémie complète du pont, partie médiane du tronc cérébral, ce qui caractérise un locked-in syndrome, dans une forme non pas classique mais aggravée, sans conscience ou avec une conscience minimale. Il indique que M. A... B..., tétraplégique, ventilé par une machine, présente " un degré de conscience minimale, mais vraiment faible ", en mentionnant l'élévation du regard à la demande tout en notant l'absence de clignement des paupières sur quelque sollicitation que ce soit et l'absence de réaction aux bruits, ainsi que l'absence de toute motricité volontaire des membres. Il relève que le patient présente une perception de la douleur certaine, même si l'évaluation de son intensité n'est pas possible. Il précise qu'" aucun code de communication ne peut être établi et que la nature des lésions visualisées par imagerie IRM ainsi que le délai écoulé depuis l'AVC ne permettent pas de croire à une amélioration éventuelle ". Il conclut que l'état de M. A... B... correspond à une forme aggravée de locked-in syndrome avec un degré de conscience minimale et sans aucune communication ni possible, ni actuelle, ni future. <br/>
<br/>
              11. Il résulte de ce qui précède ainsi que des échanges lors de l'audience qui s'est tenue après la remise du rapport d'expertise que l'état clinique de M. A... B... à la suite des lésions neurologiques très graves et irréversibles qu'il a subies, se caractérise, outre une tétraplégie et la nécessité d'une ventilation mécanique, par un état de conscience minimale attesté par la seule ouverture de ses yeux, sans possibilité d'établir avec lui quelque communication que ce soit, et la perception certaine, quoique impossible à évaluer, d'une douleur, sans qu'aucune perspective d'évolution ne soit relevée, alors qu'un délai permettant un tel pronostic s'est écoulé depuis l'accident qu'il a subi. Si les auteurs de la requête, tout en prenant acte des conclusions de l'expertise, persistent dans leurs conclusions, ils n'apportent aucun élément de nature à remettre en cause l'ensemble des constations rappelées ci-dessus ni le pronostic selon lequel aucune évolution favorable n'est possible. Eu égard à l'ensemble de ces éléments, et en raison tant de l'état clinique de M B... que des perspectives de pronostic, il apparaît que, en l'état de la science médicale et dans les circonstances particulières qui ont été décrites, les conditions mises par la loi pour que puisse être prise, par le médecin en charge du patient, une décision mettant fin à des traitements n'ayant d'autre effet que le maintien artificiel de la vie et dont la poursuite traduirait ainsi une obstination déraisonnable au sens des dispositions de l'article L. 1110-5-1 du code de la santé publique peuvent être regardées comme réunies.<br/>
<br/>
              12. Il s'ensuit que la décision du 12 octobre 2020 de ne pas poursuivre les traitements qui sont prodigués à M. A... B... autres que de sédation, et notamment de mettre fin à la ventilation mécanique, ne peut être tenue pour illégale. M. B..., Mme D... et Mme E... ne sont, dès lors, pas fondés à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté leur demande. Les conclusions tendant à l'annulation de cette ordonnance, de même que celles tendant au transfert de M. A... B... dans un autre établissement, au soutien desquelles il n'est apporté d'ailleurs aucune précision ni élément nouveau par rapport à la demande de première instance, ne peuvent dès lors qu'être rejetées.<br/>
<br/>
              Sur les frais d'expertise : <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre les frais et honoraires de l'expertise à la charge de M. B..., Mme D... et Mme E..., qui sont, dans la présente instance, la partie perdante.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Assistance publique - Hôpitaux de Paris, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B..., Mme D... et Mme E... est rejetée.<br/>
Article 2 : Les frais de l'expertise ordonnée par le Conseil d'Etat sont mis à la charge de M. B..., Mme D... et Mme E....<br/>
Article 3 : La présente ordonnance sera notifiée à M. F... B..., premier requérant dénommé, ainsi qu'à l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
