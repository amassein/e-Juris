<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713924</ID>
<ANCIEN_ID>JG_L_2019_07_000000410714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713924.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 01/07/2019, 410714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:410714.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. F...D..., Mme J...D..., M. et Mme E...H..., M. et Mme A...I..., Mme C...N..., M. K...B...ainsi que M. et Mme M... G...ont demandé au tribunal administratif de Clermont-Ferrand d'annuler, en premier lieu, la délibération par laquelle le conseil municipal de Saint-Victor-sur-Arlanc a donné pouvoir au maire de la commune pour consulter les électeurs de la section de commune du Bourg afin qu'ils se prononcent sur la vente des parcelles cadastrées section A n° 1349, 1350, 1351 et 1353 appartenant à ladite section, en deuxième lieu, l'arrêté du 7 mai 2015 par lequel le maire de Saint-Victor-sur-Arlanc a convoqué les électeurs de ladite section afin qu'ils se prononcent sur cette vente, en troisième lieu, les opérations de vote du 7 juin 2015 et, en quatrième et dernier lieu, la délibération du 17 juin 2015 par laquelle le conseil municipal de Saint-Victor-sur-Arlanc a accepté de procéder à la vente desdites parcelles et a chargé le maire de signer toutes pièces afférentes à ce dossier.<br/>
<br/>
              Par un jugement n° 1501306 du 5 novembre 2015, le tribunal administratif de Clermont-Ferrand a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16LY00012 du 21 mars 2017, la cour administrative d'appel de Lyon a rejeté la requête d'appel de M. et Mme D...et autres.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 21 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme F...D..., M. et Mme E...H..., Mme L...I..., M. A...I..., Mme C...N...ainsi que M. et Mme M...G...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Victor-sur-Arlanc une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi n° 2013-428 du 27 mai 2013 ;<br/>
              - la décision n° 410714 du 8 février 2019 par laquelle le Conseil d'Etat statuant au contentieux a sursis à statuer sur le pourvoi jusqu'à ce que le Conseil constitutionnel ait tranché la question de la conformité à la Constitution de l'article L. 2411-16 du code général des collectivités territoriales, dans sa rédaction issue de la loi du 27 mai 2013 modernisant le régime des sections de commune, en tant que ces dispositions ne prévoient, avant que les biens d'une section de commune puissent être vendus, que la consultation des membres de la section inscrits sur les listes électorales de cette commune, à l'exclusion des autres membres ;<br/>
              - la décision n° 2019-778 QPC du 10 mai 2019 du Conseil constitutionnel ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. et MmeD..., de M. et Mme E...H..., de Mme L...I..., de M. A...I..., de Mme C...N...et de M. et Mme M... G...et à Me Le Prado, avocat de la commune de Saint-Victor-sur-Arlanc ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une délibération du 17 juin 2015, le conseil municipal de la commune de Saint-Victor sur-Arlanc a accepté l'acquisition pour 200 euros, en application de la procédure de vente prévue par l'article L. 2411-16 du code général des collectivités territoriales, de quatre parcelles d'une superficie d'environ 3 000 m² appartenant à la section de commune du Bourg après que les électeurs de ladite section, consultés le 7 juin 2015, eurent donné, en l'absence de commission syndicale, leur accord pour cette vente par huit voix pour et six voix contre. M. et Mme D...et autres, qui sont membres de cette section, ont demandé au tribunal administratif de Clermont-Ferrand l'annulation de cette délibération ainsi que de la délibération du 3 mai 2015 ayant donné pouvoir au maire de consulter les électeurs de la section, de l'arrêté du 7 mai 2015 par lequel le maire a convoqué ces électeurs ainsi que les opérations de vote du 7 juin 2015. Par un jugement du 5 novembre 2015, le tribunal administratif de Clermont-Ferrand a rejeté leurs demandes. M. et Mme D...et autres se pourvoient en cassation contre l'arrêt du 21 mars 2017 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'ils avaient formé contre ce jugement. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 2411-16 du code général des collectivités territoriales applicable au litige : " Lorsque la commission syndicale n'est pas constituée, le changement d'usage ou la vente de tout ou partie des biens de la section est décidé par le conseil municipal statuant à la majorité absolue des suffrages exprimés, après accord de la majorité des électeurs de la section convoqués par le maire dans les six mois de la transmission de la délibération du conseil municipal. / En l'absence d'accord de la majorité des électeurs de la section, le représentant de l'Etat dans le département statue, par arrêté motivé, sur le changement d'usage ou la vente " ;<br/>
<br/>
              3. Par sa décision n° 2019-778 QPC du 10 mai 2019, le Conseil constitutionnel a déclaré conforme à la Constitution les mots " des électeurs " figurant aux premier et second alinéas des dispositions précitée de l'article L. 2411-16 du code général des collectivités territoriales. Par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en refusant d'annuler les délibérations et l'arrêté litigieux dès lors que les dispositions notamment de L. 2411-16 du code général des collectivités territoriales seraient contraires à la Constitution en tant qu'elles réservent le droit de vote des membres de la section aux seuls membres inscrits sur les listes électorales de la commune, ne peut qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 2131-11 du code général des collectivités territoriales : " Sont illégales les délibérations auxquelles ont pris part un ou plusieurs membres du conseil intéressés à l'affaire qui en fait l'objet, soit en leur nom personnel, soit comme mandataires ". Il résulte de ces dispositions que la participation au vote permettant l'adoption d'une délibération d'un conseiller municipal intéressé à l'affaire qui fait l'objet de cette délibération, c'est-à-dire y ayant un intérêt qui ne se confond pas avec ceux de la généralité des habitants de la commune, est de nature à en entraîner l'illégalité. La circonstance qu'un conseiller municipal, membre d'une section de commune, puisse bénéficier d'une indemnité en cas de vente de biens de cette section, au même titre que tous les autres membres de ladite section, ne saurait, toutefois, suffire à le faire regarder comme " intéressé à l'affaire " qui a fait l'objet d'une délibération prise en application de l'article L. 2411-16 du code général des collectivités territoriales. <br/>
<br/>
              5. Par suite, la cour administrative d'appel n'a entaché son arrêt ni d'erreur de qualification juridique ni de dénaturation des pièces du dossiers en jugeant que la délibération du 17 juin 2015 approuvant la vente des biens sectionaux n'était pas irrégulière du fait de la participation au vote d'un conseiller municipal, membre de la section, dont il était constant que l'intérêt ne différait pas de celui des autres membres. Le moyen tiré, en outre, de ce qu'elle aurait commis une erreur de droit en se fondant sur la circonstance que la délibération avait été adoptée à l'unanimité pour estimer que la participation au vote de ce conseiller municipal n'avait pas exercé d'influence ne peut qu'être écarté comme inopérant dès lors qu'il est dirigé contre un motif surabondant de l'arrêt attaqué. <br/>
<br/>
              6. En troisième lieu, il ne résulte pas des termes des dispositions précitées de l'article L. 2411-16 du code général des collectivités territoriales qu'elles ne seraient pas applicables en cas de vente de tout ou partie des biens d'une section à sa commune de rattachement. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que la commune pouvait recourir, en l'espèce, à cette procédure. La cour n'avait pas, par ailleurs, à rechercher, contrairement à ce que soutiennent les requérants au demeurant pour la première fois en cassation, si la vente avait été décidée en vue de l'exécution d'un service public, de l'implantation d'un lotissement ou de l'exécution d'une opération d'intérêt public, au sens et pour l'application des dispositions du II de l'article L. 2411-6 du même code qui sont relatives à une procédure distincte ou si les parcelles en cause faisaient l'objet d'un projet d'aménagement.<br/>
<br/>
              7. En quatrième et dernier lieu, si les requérants soutenaient en appel que le recours à la procédure de l'article L. 2411-16 du code général des collectivités territoriales plutôt qu'à la procédure de l'article L. 2411-11 du même code présentait, pour la commune, un avantage financier dès lors que la seconde prévoit le versement d'une indemnité contrairement, selon eux, à la première, la cour ne s'est pas prononcée sur le droit à indemnité au cas d'espèce mais s'est bornée à juger que l'absence d'un tel droit n'affecterait pas la régularité du recours à la procédure prévue par l'article L. 2411-16. Par suite, le moyen de M. et Mme D...et autres tiré de ce que la cour aurait entaché son arrêt d'une contradiction de motifs et d'une erreur de droit en écartant, en l'espèce, tout droit à indemnité, doit être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M. et Mme D...et autres ne sont pas fondés à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Saint-Victor-sur-Arlanc qui n'est pas, dans la présente instance, la partie perdante. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de M. et MmeD..., M. et MmeH..., MmeI..., M. I..., Mme N...ainsi que M. et Mme G...le versement d'une somme de 500 euros chacun à cette commune. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme D...et autres contre l'arrêt de la cour administrative d'appel de Lyon du 21 mars 2017 est rejeté.<br/>
Article 2 : M. et Mme F...D..., M. et Mme E...H..., Mme L...I..., M. A... I..., Mme C...N..., ainsi que M. et Mme M...G...verseront chacun à la commune de Saint-Victor-sur-Arlanc la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. et Mme F...D..., M. et Mme E...H..., Mme L...I..., M. A...I..., Mme C...N..., M. et Mme M...G..., à la commune de Saint-Victor-sur-Arlanc et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
