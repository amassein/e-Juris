<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555852</ID>
<ANCIEN_ID>JG_L_2012_10_000000341283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555852.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 29/10/2012, 341283, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS>JACOUPY</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:341283.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Mahmoud B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA00035 du 18 mai 2009 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0305047 du 9 novembre 2007 du tribunal administratif de Nice rejetant sa demande d'annulation de la décision du 22 septembre 2003 du préfet des Alpes-Maritimes refusant de lui délivrer un titre de séjour, d'autre part, à l'annulation pour excès de pouvoir de cette décision et de celle du 8 février 2007 par laquelle le préfet des Alpes-Maritimes a de nouveau refusé de lui délivrer un titre de séjour et lui a notifié une obligation de quitter le territoire français et, enfin, à ce qu'il soit enjoint au préfet de lui délivrer le titre de séjour sollicité ou de procéder à un nouvel examen de sa demande dans le délai d'un mois à compter de la notification de l'arrêt, sous peine d'astreinte à hauteur de 150 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à payer à Me Jacoupy, son avocat, sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu l'ordonnance n° 45-2658 du 2 novembre 1945 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Jacoupy, avocat de M. B,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Jacoupy, avocat de M. B ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. B, ressortissant tunisien, est entré en France le 6 mai 2002 sous couvert d'un passeport revêtu d'un visa de court séjour valable du 25 avril au 24 juillet 2002 ; qu'après s'être maintenu sur le territoire français à l'expiration de la durée de validité de ce visa, il a présenté aux services de la préfecture des Alpes-Maritimes une demande d'admission au séjour en qualité d'étranger malade ; qu'au vu notamment de l'avis émis le 25 août 2003 par le médecin-inspecteur de santé publique, selon lequel la pathologie dont l'intéressé est atteint pouvait faire l'objet d'un suivi dans son pays d'origine, le préfet des Alpes-Maritimes a, par une décision du 22 septembre 2003, rejeté cette demande ; que, par un jugement du 9 novembre 2007, le tribunal administratif de Nice a rejeté la demande présentée par M. B contre cette décision et déclaré irrecevables ses conclusions, présentées dans une note en délibéré, tendant à l'annulation pour excès de pouvoir d'une nouvelle décision du 8 février 2007 par laquelle le préfet a de nouveau refusé de lui délivrer un titre de séjour et lui a notifié une obligation de quitter le territoire français ; que M. B se pourvoit en cassation contre l'arrêt du 18 mai 2009 par lequel la cour administrative d'appel de Marseille a confirmé ce jugement ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions de la requête de M. B dirigées contre la décision du 8 février 2007 du préfet des Alpes-Maritimes :<br/>
<br/>
              2. Considérant que M. B ne formule aucun moyen de cassation contre l'arrêt attaqué en tant qu'il a déclaré irrecevables ses conclusions tendant à l'annulation pour excès de pouvoir de la décision du 8 février 2007 du préfet des Alpes-Maritimes ; que, par suite, le pourvoi ne peut qu'être rejeté en tant qu'il demande l'annulation de cette disposition de l'arrêt attaqué ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur l'appel formé par M. B contre le jugement du 9 novembre 2007 du tribunal administratif de Nice en tant qu'il rejette sa demande d'annulation de la décision du 22 septembre 2003 du préfet des Alpes-Maritimes :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 12 bis de l'ordonnance du 2 novembre 1945, désormais codifié à  l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction en vigueur à la date de la décision litigieuse : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : / (...) / 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse effectivement bénéficier d'un traitement approprié dans le pays dont il est originaire (...). " ;<br/>
<br/>
              4. Considérant que, pour rejeter la requête de M. B dirigée contre le jugement du tribunal administratif de Nice du 9 novembre 2007 en tant qu'il a rejeté sa demande d'annulation pour excès de pouvoir de la décision du préfet des Alpes-Maritimes du 22 septembre 2003 lui refusant la délivrance d'un titre de séjour au titre des dispositions rappelées ci-dessus, la cour administrative d'appel de Marseille a retenu que la circonstance que le requérant ne bénéficierait pas d'une couverture sociale en Tunisie et qu'il ne pourrait ainsi accéder aux traitements requis par son état de santé était sans incidence sur l'existence, dans ce pays, de soins appropriés à sa pathologie ; qu'en se bornant, ce faisant, à constater la disponibilité du traitement approprié à l'état de santé de l'intéressé dans son pays d'origine sans examiner, comme l'y invitait le requérant, s'il pourrait bénéficier effectivement d'un tel traitement eu égard à ses ressources, la cour a entaché son arrêt d'une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. B est fondé à demander l'annulation de cet arrêt en tant qu'il rejette ses conclusions à fin d'annulation de la disposition du jugement attaqué rejetant sa demande d'annulation pour excès de pouvoir de la décision du 22 septembre 2003 du préfet des Alpes-Maritimes ;<br/>
<br/>
              Sur les conclusions de M. B présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 500 euros à verser Me Jacoupy, avocat de M. B, au titre de ces dispositions, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat au titre de l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 18 mai 2009 de la cour administrative d'appel de Marseille est annulé en tant qu'il rejette les conclusions de M. B dirigées contre la disposition du jugement du tribunal administratif de Nice du 9 novembre 2007 rejetant ses demandes relatives à la décision du 22 septembre 2003 du préfet des Alpes-Maritimes.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à Me Jacoupy, avocat de M. B , une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat au titre de l'aide juridictionnelle ;<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. B est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. Mahmoud B et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
