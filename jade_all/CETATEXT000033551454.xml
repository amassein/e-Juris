<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551454</ID>
<ANCIEN_ID>JG_L_2016_12_000000390062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551454.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/12/2016, 390062, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:390062.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et des mémoires en réplique, enregistrés les 11 mai, 11 septembre et 6 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société civile immobilière Nefertari demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 9 avril 2015 de l'Autorité de contrôle prudentiel et de résolution refusant d'instruire sa demande de sanction à l'encontre de la société d'assurance Axa à la suite de la décision du bureau central de tarification demandant à cette société de garantir l'immeuble de la SCI au titre de l'assurance dommage ouvrage ; <br/>
<br/>
              2°) d'enjoindre à l'Autorité de contrôle prudentiel et de résolution de réexaminer sa demande dans le délai d'un mois à compter de la notification de la décision à intervenir sous astreinte de 200 euros par jour de retard ; <br/>
<br/>
              3°) de condamner l'Autorité de contrôle prudentiel et de résolution à lui verser la somme de 70 000 euros au titre du préjudice subi ;<br/>
<br/>
              4°) de mettre à la charge de l'Autorité de contrôle prudentiel et de résolution la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ;<br/>
              - le code monétaire et financier ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 243-6 du code des assurances : " Toute entreprise d'assurance qui maintient son refus de garantir un risque dont la prime a été fixée par le bureau central de tarification est considérée comme ne fonctionnant plus conformément à la réglementation en vigueur et encourt le retrait de l'agrément administratif prévu par l'article L.321-1 du présent code. ". En vertu des dispositions de l'article L. 321-1 du même code, les entreprises d'assurance " ne peuvent commencer leurs opérations qu'après avoir obtenu un agrément administratif délivré par l'Autorité de contrôle prudentiel et de résolution mentionnée à l'article L. 612-1 du code monétaire et financier ".<br/>
<br/>
              2. D'autre part, aux termes du I de l'article L. 612-1 du code monétaire et financier : " L'Autorité de contrôle prudentiel et de résolution, autorité administrative indépendante, veille à la préservation de la stabilité du système financier et à la protection des clients, assurés, adhérents et bénéficiaires des personnes soumises à son contrôle ". En vertu du IV du même article, elle dispose, pour l'accomplissement de ses missions " d'un pouvoir de contrôle, du pouvoir de prendre des mesures de police administrative et d'un pouvoir de sanction ". Enfin, l'article L. 612-39 du même code prévoit que la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution peut prononcer le retrait partiel ou total de l'agrément délivré aux entreprises soumises à son contrôle.<br/>
<br/>
              3. Il ressort des pièces du dossier que la SCI Nefertari a acquis, en 2011, un immeuble de logements situé à la Réunion, partiellement achevé et destiné à la vente. A la suite du refus de la société Axa de lui fournir une assurance dite " constructeur non réalisateur et dommages ouvrage ", elle a saisi le bureau central de tarification (BCT), en application de l'article L. 243-4 du code des assurances qui prévoit que cet organisme peut être saisi en cas de refus de souscription d'un contrat d'assurances et qu'il a pour mission " de fixer le montant de la prime moyennant laquelle l'entreprise d'assurance intéressée est tenue de garantir le risque qui lui a été proposé ". Par une décision du 20 février 2014, le BCT a fixé les conditions et les modalités selon lesquelles la société Axa devait garantir la SCI Nefertari. La société Axa ayant opposé un nouveau refus de garantie à la SCI Nefertari, celle-ci a saisi, le 2 août 2014, l'Autorité de contrôle prudentiel et de résolution (ACPR) d'une demande tendant à ce que cette dernière procède au retrait de l'agrément de la société Axa en application des dispositions, citées au point 1, de l'article L. 243-6 du code des assurances. Par un courrier du 9 avril 2015, intervenu après plusieurs échanges avec la SCI, l'ACPR a rejeté cette demande au motif que la demande adressée par la SCI Nefertari à la société Axa le 14 novembre 2014 était tardive faute d'avoir été formulée dans le délai de trois mois à compter de la notification de la décision du BCT du 20 février 2014, fixé par cette décision.<br/>
<br/>
              4. La SCI Nefertari demande l'annulation, pour excès de pouvoir, de cette décision du 9 avril 2015. Dans son mémoire en réplique, enregistré le 6 octobre 2015, elle demande, en outre, la condamnation de l'Etat (ACPR) à lui verser une somme de 70 000 euros en réparation du préjudice qu'elle prétend avoir subi du fait de cette décision.<br/>
<br/>
              Sur les conclusions indemnitaires : <br/>
<br/>
              5. Si, aux termes du 4° de l'article R. 311-1 du code de justice administrative, le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des " recours dirigés contre les décisions prises par les organes des autorités " mentionnées à cet alinéa " au titre de leur mission de contrôle ou de régulation ", il n'est, en revanche, pas compétent pour connaître en premier et dernier ressort des recours dirigés contre les décisions prises par ces autorités à un autre titre, ni pour connaître des autres litiges, notamment indemnitaires, les concernant.<br/>
<br/>
              6. Les conclusions de la requête de la SCI Nefertari tendant à la condamnation de l'Autorité de contrôle prudentiel et de résolution, laquelle est au nombre des autorités mentionnées au 4° de l'article R. 311-1 du code de justice administrative, à lui verser la somme de 70 000 euros au titre du préjudice qu'elle estime avoir subi du fait de la décision du 9 avril 2015 ne sont pas dirigées contre une décision prise par les organes de l'Autorité au titre des missions de contrôle ou de régulation confiées à cette autorité mais soulèvent un litige d'une autre nature concernant cette autorité. Aucune autre disposition législative ou réglementaire ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort de ces conclusions. Dès lors, il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu du 3° de l'article R. 312-14 du même code.<br/>
<br/>
              Sur les conclusions à fin d'annulation et d'injonction :<br/>
<br/>
              Sur la fin de non-recevoir opposée par l'Autorité de contrôle prudentiel et de résolution :<br/>
<br/>
              7. Il appartient à une autorité administrative indépendante qui dispose, en vertu de la loi, de pouvoirs de contrôle et de sanction qu'elle exerce de sa propre initiative de procéder, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ses pouvoirs, à l'examen des faits qui sont à l'origine de cette demande et de décider des suites à lui donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge. La décision qu'elle prend, lorsqu'elle refuse de donner suite à la demande, a le caractère d'une décision administrative qui peut être déférée au juge de l'excès de pouvoir. Il en résulte que la fin de non-recevoir opposée par l'ACPR aux conclusions de la requête tendant à l'annulation de sa décision du 9 avril 2015 doit être écartée. <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              8. Il résulte des dispositions des articles L. 612-38 et L. 612-39 du code monétaire et financier que si la commission des sanctions de l'ACPR est seule compétente pour prononcer les sanctions énumérées à l'article L. 612-39, la décision d'engager une procédure disciplinaire relève de la compétence de la formation du collège de supervision ou du collège de résolution, selon les cas. Par suite, la décision attaquée, qui a été prise pour le chef du service " Informations et réclamations " de la direction du contrôle des pratiques commerciales de l'APCR, lequel ne justifie pas signer au nom du collège de supervision ou du collège de résolution, a été prise par une autorité incompétente. Si l'ACPR soutient que la demande de la SCI Nefertari ayant été adressée à la société Axa postérieurement à l'expiration du délai de trois mois dans lequel elle pouvait se prévaloir, aux termes de la décision du BCT du 20 février 2014, de cette décision, elle ne pouvait que rejeter sa demande de sanction, elle était, cependant, nécessairement conduite, pour retenir cette tardiveté, à porter en l'espèce une appréciation sur la succession de courriers dont a été saisie cet assureur par la SCI Nefertari. Elle ne se trouvait donc pas en situation de compétence liée pour rejeter cette demande. Il en résulte que, contrairement à ce qu'elle soutient, le moyen tiré de l'incompétence de l'auteur de la décision attaquée n'est pas inopérant et qu'il peut, par suite, fonder l'annulation de la décision attaquée. <br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la SCI Nefertari est fondée à demander l'annulation de la décision attaquée. <br/>
<br/>
              10. L'exécution de la présente décision implique nécessairement que la demande de la SCI Nefertari soit réexaminée. Il y a lieu, par suite, d'enjoindre à l'ACPR, en application de l'article L. 911-2 du code de justice administrative, de procéder à ce réexamen dans un délai de trois mois à compter de la notification de la présente décision. En revanche, il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction de l'astreinte demandée par la société requérante.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 1 500 euros à la charge de l'Etat (ACPR) au titre  des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de la SCI Nefertari qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la requête de la SCI Nefertari tendant à la condamnation de l'Etat (Autorité de contrôle prudentiel et de résolution) à lui verser une indemnité de 70 000 euros est attribué au tribunal administratif de Paris.<br/>
Article 2 : La décision de l'Autorité de contrôle prudentiel et de résolution du 9 avril 2015 est annulée.<br/>
Article 3 : Il est enjoint à l'Autorité de contrôle prudentiel et de résolution de procéder au réexamen de la demande de la SCI Nefertari dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 4 : l'Etat (Autorité de contrôle prudentiel et de résolution) versera une somme de 1 500 euros à la SCI Nefertari au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête de la SCI Nefertari est rejeté.<br/>
Article 6: Les conclusions de l'Autorité de contrôle prudentiel et de résolution présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à la société civile immobilière Nefertari, à l'Autorité de contrôle prudentiel et de résolution et à la société Axa France IARD.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
