<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043871547</ID>
<ANCIEN_ID>JG_L_2021_06_000000452502</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/15/CETATEXT000043871547.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/06/2021, 452502, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452502</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452502.20210601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 mai 2021 au secrétariat du contentieux du Conseil d'Etat, Mme R... J..., Mme K... B..., M. M... G..., Mme I... P..., Mme F... N..., Mme Q... S..., Mme H... L..., Mme T... E..., Mme A... O... et Mme C... D... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'article 36 du décret n° 2020-1310 du 29 octobre 2020 dans sa rédaction issue du décret n° 2021-541 du 1er mai 2021 ainsi que celle du guide relatif au fonctionnement des écoles et établissements scolaires dans le contexte covid-19 pour l'année scolaire dans sa version de février 2021 ;  <br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre un nouveau décret modifiant l'article 36 du décret du 29 octobre 2020, dans un délai de 24 heures à compter de l'ordonnance à intervenir, de diligenter une étude d'impact officielle, de prévoir des conditions de dérogation au port du masque pour les enfants, de prévoir les conséquences d'un refus du port du masque pour les enfants notamment à l'école primaire ainsi que de prévoir une date de fin de cette obligation généralisée, sous astreinte de 50 euros par jours de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - ils ont déposé un recours tendant à l'annulation pour excès de pouvoir des décisions contestées devant le Conseil d'Etat <br/>
              - ils justifient d'un intérêt pour agir contre ces décisions ; <br/>
              - le guide relatif au fonctionnement des écoles et établissements scolaires dans le contexte covid-19 pour l'année scolaire dans sa version de février 2021 est un acte faisant grief aux usagers du service public de l'éducation nationale ;<br/>
              - la condition d'urgence est satisfaite compte tenu du caractère grave et immédiat du préjudice subi par les enfants ; <br/>
              - il existe un doute sérieux quant à la légalité de l'article 36 du décret contesté ; <br/>
              - le premier ministre a entaché l'article 36 d'incompétence négative en tant qu'il n'a pas fixé les conditions d'application concrètes des obligations qu'il édicte ;<br/>
              - les dispositions contestées n'ont pas été précédées d'une étude d'impact ;<br/>
              - le décret n'a pas été pris sur le rapport du ministre chargé de la santé ;<br/>
              - l'article L. 3131-12 du code de la santé publique méconnaît l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'article 36 contesté qui ne prévoit pas de dérogation à l'obligation de port du masque, instaure une mesure qui n'est pas proportionnée ;<br/>
              - les termes " groupes différents " sont trop imprécis et source de discrimination et de rupture d'égalité ;<br/>
              - ces dispositions ne sont ni nécessaires, ni adaptées, ni proportionnées ;<br/>
              - le décret ne prévoit pas de clause de revoyure ;<br/>
              - il existe également un doute sérieux quant à la légalité du guide relatif au fonctionnement des écoles et établissements scolaires dans le contexte covid-19, portant protocole sanitaire ; <br/>
              - le ministre de l'éducation nationale était incompétent pour édicter ce " protocole sanitaire " ; <br/>
              - le guide contesté n'est pas signé par son auteur en méconnaissance de l'article L. 212-1 du code des relations entre le public et l'administration ;<br/>
              - le guide donne de manière illégale aux médecins référents en place dans les établissements scolaires un pouvoir d'appréciation sur les certificats médicaux produits par les élèves et à l'administration scolaire un pouvoir d'appréciation non justifié sur les pathologies rendant les enfants vulnérables ;<br/>
              - il existe enfin un doute sérieux quant à la légalité des deux mesures contestées compte tenu de la violation des droits fondamentaux ;<br/>
              - ces mesures ne sont pas nécessaires, adaptées et proportionnées à la situation et à la liberté des enfants et portent ainsi atteinte à l'obligation d'accorder une primauté aux droits de l'enfant ;<br/>
              - elles méconnaissent le droit à l'autodétermination personnelle et à une vie normale et à la santé, au secret médical et au droit à l'éducation ;<br/>
              - elles portent une atteinte manifestement grave et illégale à la liberté d'aller et venir, à la liberté individuelle, au droit réunion ainsi qu'au principe de fraternité ;<br/>
              - elles portent atteinte de manière disproportionnée à la vie privée et familiale protégée par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              Par un mémoire en défense, enregistré le 25 mai 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à la légalité des actes attaqués.   <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'éducation nationale, de la jeunesse et des sports, qui n'ont pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'éducation ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-1379 du 14 novembre 2020 ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
              - le décret n° 2021 296 du 19 mars 2021 ; <br/>
              - le décret n° 2021 384 du 2 avril 2021 ;<br/>
              - le décret n° 2021-541 du 1er mai 2021 ;<br/>
              - le décret n° 2021-606 du 18 mai 2021 ;<br/>
              - le décret n°91-1195 du 27 novembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 27 mai 2021 à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              Sur le cadre du litige :<br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution de Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes du I de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 5° Ordonner la fermeture provisoire et réglementer l'ouverture, y compris les conditions d'accès et de présence, d'une ou plusieurs catégories d'établissements recevant du public. " Aux termes du III du même article : " Les mesures prescrites en application du présent article strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délais lorsqu'elles ne sont plus nécessaires. "<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence. <br/>
<br/>
              4. La situation épidémiologique au cours des mois de septembre et d'octobre, caractérisée par une accélération du rythme de l'épidémie, a conduit le Président de la République à prendre le 14 octobre 2020, sur le fondement de l'article L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. Le 16 octobre 2020 puis le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, les décrets prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. L'état d'urgence sanitaire a été prorogé jusqu'au 16 février 2021 inclus puis jusqu'au 1er juin 2021 inclus respectivement par la loi n° 2020-1379 du 14 novembre 2020 et la loi n° 2021-160 du 15 février 2021. <br/>
<br/>
              5. La progression de l'épidémie au cours des mois de février, mars et avril 2021 a conduit le Premier ministre à édicter les décrets n° 2021-296 du 19 mars 2021 et n° 2021-384 du 2 avril 2021, modifiant les décrets n° 2020-1262 du 16 octobre 2020 et n° 2020-1310 du 29 octobre 2020 et mettant progressivement en place un nouveau confinement généralisé de la population. Puis l'amélioration de la situation sanitaire a permis un allègement des mesures sanitaires et la mise en application d'un plan dit de déconfinement progressif. Le décret n°2021-541 du 1er mai 2021 a ainsi instauré un couvre-feu de 19 heures à 6 heures du matin, puis le décret n° 2021-606 du 18 mai 2021 a retardé la mise en place de ce couvre-feu à 21 heures et autorisé l'ouverture au public de certaines catégories d'établissements, sous conditions. <br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. Mme J... et autres demandent au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative de suspendre l'exécution, d'une part, du guide relatif au fonctionnement des écoles et établissements scolaires dans le contexte de Covid-19 pour l'année 2020-2021 et, d'autre part, de l'article 36 du décret précité du 29 octobre 2020 dans sa rédaction issue du décret du 1er mai 2021 notamment en ce qu'il impose le port du masque des enfants de 6 à 11 ans dans les établissements scolaires. Ils contestent ces mesures en se prévalant de moyens concernant spécifiquement le protocole sanitaire ou l'article 36 contesté ainsi que de moyens communs à ceux mesures. <br/>
<br/>
              En ce qui concerne la légalité du protocole sanitaire :<br/>
<br/>
              7. Le ministère de l'éducation nationale, de la jeunesse et des sports a publié, en février 2021, un protocole sanitaire pour l'année 2020-2021 applicable à compter du 1er février 2021, sous la forme d'un " guide relatif au fonctionnement des écoles et établissements scolaires dans le contexte de Covid-19 ". Ce document rappelle le contexte pandémique dans lequel le protocole sanitaire s'inscrit au regard notamment de l'évolution de la circulation du virus constatée en janvier 2021 et de l'émergence de variants. Il précise qu'il repose sur les avis successifs du Haut conseil de la santé publique (HCSP), notamment celui du 20 janvier 2021, sur les prescriptions émises par le ministère des solidarités et de la santé au vu des avis rendus par le HCSP ainsi que sur les dispositions législatives et réglementaires en vigueur. Il énonce que le principe est celui d'un accueil de tous les élèves, à tous les niveaux et sur l'ensemble du temps scolaire, dans le respect des prescriptions émises par les autorités sanitaires et renvoie également au plan de continuité pédagogique. Il rappelle que les mesures à prendre nécessitent de tenir compte du contexte propre à chaque école ou établissement et que les mesures spécifiques à certaines thématiques (restauration, éducation physique et sportive, éducation musicale, récréation et internats) sont précisées dans des fiches repères disponibles sur le site du ministère. Il est destiné aux collectivités territoriales, aux services déconcentrés de l'Etat, aux personnels de direction ainsi qu'à l'ensemble des membres de la communauté éducative. Le guide précise ensuite les recommandations en matière de distanciation physique, de gestes barrières en milieu scolaire, de port du masque " grand public " de catégorie 1 par les personnels et les élèves, de ventilation des locaux par une aération renforcée, de limitation du brassage des élèves, de nettoyage et de désinfection des locaux et des matériels, ainsi que de formation, d'information et de communication en faveur des personnels, des parents et des élèves. En ce qui concerne spécifiquement le port du masque, il énonce notamment que, pour les élèves des écoles maternelles, le port du masque est à proscrire et que, pour les élèves des écoles élémentaires, des collèges et des lycées, le port du masque " grand public " de catégorie 1 est obligatoire dans les espaces clos ainsi que dans les espaces extérieurs.<br/>
<br/>
              8. En premier lieu, le protocole sanitaire établi par le ministère de l'éducation nationale, de la jeunesse et des sports sous la forme d'un guide mentionné au point précédent ne constitue pas une mesure d'exécution du décret n° 2020-1310 du 29 octobre 2020. Aussi, la circonstance que le ministre de l'éducation nationale, de la jeunesse et des sports ne soit pas au nombre des ministres chargés de l'exécution de ce décret, ne fait pas obstacle à ce que le ministère dont il a la charge édite, sous son autorité, le document contesté, qui n'a pas, au surplus, le caractère d'une décision, notamment au regard des dispositions de l'article L. 212-1 du code des relations entre le public et l'administration, mais est destiné à rassembler des règles de bonne conduite et à fournir des recommandations pour favoriser, notamment auprès des personnels, des élèves et de leurs parents, l'application, au niveau de chaque établissement accueillant un public scolaire, des " prescriptions émises par le ministère des solidarités et de la santé au vu des avis rendus par le Haut conseil de la santé publique ainsi que sur les dispositions législatives et réglementaires en vigueur ". Par suite, les moyens tirés de ce que le ministre de l'éducation nationale était incompétent pour édicter ce " protocole sanitaire " et que le guide contesté n'est pas signé par son auteur en méconnaissance de l'article L. 212-1 du code des relations entre le public et l'administration, ne sont pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité du document en cause, lequel est susceptible de produire notamment vis-à-vis des enfants scolarisés et des parents d'élèves des effets notables sur leurs droits ou leur situation.<br/>
<br/>
              9. En second lieu, en ce qui concerne le port du masque, ce document énonce que " Pour les élèves présentant des pathologies les rendant vulnérables au risque de développer une forme grave d'infection à la COVID-19, le médecin référent détermine les conditions de leur maintien en présence dans l'école ou l'établissement scolaire ". Il ne résulte pas de ces termes qu'un médecin de l'éducation nationale agissant dans le cadre des missions qui lui confiées par l'article 2 du décret du 27 novembre 1991 portant dispositions statutaires applicables au corps des médecins de l'éducation nationale et à l'emploi de médecin de l'éducation nationale - conseiller technique, auquel renvoient les dispositions de l'article D. 541-2 du code de l'éducation, soit habilité à remettre en cause les constatations ou indications à caractère médical portées dans un certificat médical. Il en va de même notamment des constatations ou indications médicales en lien avec une pathologie rendant l'enfant vulnérable au risque de développer une forme grave d'infection à la covid-19 et susceptibles de déterminer les conditions du maintien de l'enfant en présence dans l'école ou l'établissement scolaire. Par suite, le moyen tiré de ce que le guide contesté donne de manière illégale aux médecins de l'éducation nationale ou à l'administration scolaire un pouvoir d'appréciation des certificats médicaux n'est pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité du guide sur ce point. <br/>
<br/>
              En ce qui concerne la légalité de l'article 36 du décret contesté :<br/>
<br/>
              10. Le 7° de l'article 1er du décret du 1er mai 2021 modifiant le décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire a remplacé les dispositions de l'article 36 du décret du 29 octobre 2020 par les dispositions suivantes : " Art. 36.-I.- L'accueil des usagers dans les établissements mentionnés au présent chapitre est organisé dans des conditions de nature à permettre le respect des règles d'hygiène et de distanciation mentionnées à l'article 1er. L'accueil est organisé dans des conditions permettant de limiter au maximum le brassage des enfants et élèves appartenant à des groupes différents. / Toutefois, dans les établissements et services mentionnés au I de l'article 32, dans les écoles maternelles ainsi que pour les assistants maternels, dès lors que le maintien de la distanciation physique entre le professionnel et l'enfant et entre enfants n'est par nature pas possible, l'établissement ou le professionnel concerné met en oeuvre les mesures sanitaires de nature à prévenir la propagation du virus. Pour chaque groupe d'enfants qu'accueille un établissement ou service mentionné au I de l'article 32, celui-ci est soumis aux dispositions du premier alinéa de l'article R. 2324-43-1 du code de la santé publique dès lors qu'il accueille quatre enfants ou plus. / Dans les établissements mentionnés au II de l'article 32, l'observation d'une distanciation physique d'au moins un mètre s'applique dans la mesure du possible. Les activités sportives proposées dans les accueils mentionnés au III de l'article 32 ne peuvent être organisées qu'en plein air. / Dans les établissements d'enseignement relevant des livres IV et VII du code de l'éducation, à l'exception de ceux mentionnés au deuxième alinéa, l'observation d'une distanciation physique d'au moins un mètre ou d'un siège s'applique, entre deux personnes lorsqu'elles sont côte à côte ou qu'elles se font face, uniquement dans les salles de cours et les espaces clos et dans la mesure où elle n'affecte pas la capacité d'accueil de l'établissement. / II. -Portent un masque de protection : / 1° Les personnels des établissements et structures mentionnés aux articles 32 à 35 ; / 2° Les assistants maternels, y compris à domicile ; / 3° Les élèves des écoles élémentaires ; / 4° Les collégiens, les lycéens et les usagers des établissements mentionnés aux articles 34 et 35 ; / 5° Les enfants de six ans ou plus accueillis en application du II de l'article 32 ; / 6° Les représentants légaux des élèves et des enfants accueillis par des assistants maternels ou dans les établissements mentionnés à l'article 32. / Les dispositions du 2° ne s'appliquent pas lorsque l'assistant maternel n'est en présence d'aucun autre adulte. " <br/>
<br/>
              S'agissant des moyens de légalité externe :<br/>
<br/>
              11. En premier lieu, il ressort des termes mêmes des dispositions citées au point 2 de l'article L. 3131-15 du code de la santé publique, que le Premier ministre était compétent pour prendre, par décret et aux seules fins de garantir la santé publique, les dispositions contenues à l'article 36 du décret contesté. Aucune disposition n'exige du Premier ministre qu'il prévoit une habilitation expresse des ministres pour qu'ils mettent en oeuvre leur pouvoir réglementaire d'organisation des services. Par suite, le Premier ministre n'a pas, en tout état de cause, entaché le décret contesté d'incompétence négative.<br/>
<br/>
              12. En deuxième lieu, si l'article L. 3131-15 du code de la santé publique prévoit que le Premier ministre prend le décret réglementaire " sur le rapport du ministre chargé de la santé ", la circonstance que ce rapport ne soit pas directement accessible ou communicable ne signifie pas qu'il n'existe pas. Il est seulement allégué par les requérants que le décret du 1e mai 2021 qui a notamment modifié la rédaction de l'article 36 du décret du 29 octobre 2020, n'aurait pas été pris sur le rapport du ministre chargé de la santé du seul fait qu'ils y auraient pas eu accès. <br/>
<br/>
              13. En troisième lieu, aucune disposition n'exige du Premier ministre que l'adoption du décret prévu à l'article L. 3131-15 du code de la santé publique soit précédé d'une étude d'impact.<br/>
<br/>
              14. Il résulte des trois points précédents que les moyens tirés des vices d'incompétence ou de procédure soulevés ne sont pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité de l'article 36 contesté. <br/>
<br/>
              S'agissant des moyens de légalité interne :<br/>
<br/>
              15. En premier lieu, l'article L. 3131-12 du code de la santé publique en vertu duquel l'état d'urgence sanitaire peut être déclaré en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population, ne méconnaît pas par lui-même les stipulations de l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales relatives aux cas dans lesquels une personne peut être privée de sa liberté. Sont inopérantes à cet égard les observations faites par les requérants sur la façon dont la juridiction administrative, et en particulier le juge des référés du Conseil d'Etat, apprécie, en réponse aux requêtes dont il est saisi, la réalité de la catastrophe sanitaire liée au covid-19 et de son évolution, au regard des indicateurs et des études produits. Le moyen tiré de la violation de l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'apparaît dès lors pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité de l'article 36 du décret qui a été pris sur le fondement de l'article L. 3131-12 du code de la santé publique.<br/>
<br/>
              16. En deuxième lieu, en vertu du III de l'article L. 3131-15 du code de la santé publique, les mesures prescrites en application de cet article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. <br/>
<br/>
              17. Il résulte, d'une part, de l'instruction qu'à la date à laquelle l'article 36 du décret du 29 octobre 2020 a été maintenu dans la nouvelle rédaction que lui a donnée le 7° de l'article 1er du décret du 1er mai 2021, - date à laquelle il convient de se placer dans le cadre du présent référé présenté sur le fondement de l'article L. 521-1 du code de justice administrative -, les indicateurs de la pandémie, qui ne sont pas, à cette date, sérieusement contestés, présentaient une évolution préoccupante manifestant l'existence d'un risque élevé de propagation du virus, en particulier de ses nouveaux variants. Il n'est pas davantage sérieusement contesté que ce virus et ses variants étaient susceptibles d'être diffusés par l'intermédiaire des enfants, en particulier de manière asymptomatique. D'autre part, il est constant que les établissements accueillant un public scolaire sont des lieux de fort brassage, le plus souvent clos, dans lesquels les élèves et leurs maîtres sont en présence les uns des autres pendant plusieurs heures. Dans ce contexte et compte tenu enfin de la volonté du gouvernement de favoriser la scolarisation au sein de leurs établissements scolaires des élèves des classes élémentaires et secondaires en dépit de la permanence de la pandémie et de ses évolutions défavorables, l'obligation de port du masque par les élèves de six ans ou plus accueillis dans ces établissements, retenue par les dispositions de l'article 36 contesté, présente un caractère nécessaire. Compte tenu des avis favorables des instances scientifiques compétentes concernant le port du masque pour les élèves à partir de l'âge de 6 ans, cette obligation, qui tient compte de l'âge et de la maturité des enfants, dont la mise en oeuvre est placée sous le contrôle des adultes et des maîtres, et qui est associée aux mesures de distanciation physique et sociale ainsi qu'à l'apprentissage des divers gestes barrières, présente également en milieu scolaire un caractère adapté. S'il est vrai que cette mesure constitue une contrainte qui peut être plus ou moins bien tolérée par les enfants ou leurs parents et s'avérer contre-indiquée à la situation médicale d'enfants souffrant de certaines pathologies, les dispositions contestées, qui ne concernent que la scolarisation en présentiel, ne font pas obstacle à ce que ces situations particulières soient prises en compte et fassent, le cas échéant, l'objet de solutions alternatives dans le cadre de l'éducation nationale. Le protocole sanitaire élaboré par le ministère de l'éducation nationale a, au demeurant ainsi qu'il a été dit, prévu la prise en compte de la situation des " élèves présentant des pathologies les rendant vulnérables au risque de développer une forme grave d'infection à la COVID-19 ". Il s'ensuit que l'obligation de port du masque par les élèves de 6 à 11ans n'apparaît pas disproportionnée aux buts de santé publique et de scolarisation des enfants au sein des établissements, qu'elle poursuit. Par suite, le moyen tiré de ce que les dispositions de l'article 36 relatives au port obligatoire du masque par les élèves à partir de l'âge de 6 ans ne seraient ni nécessaires, ni adaptées ni proportionnées n'est pas, en l'état de l'instruction, de nature faire naître un doute sérieux.<br/>
<br/>
              18. En troisième lieu, selon le premier alinéa de l'article 36 contesté : " L'accueil est organisé dans des conditions permettant de limiter au maximum le brassage des enfants et élèves appartenant à des groupes différents ". Si le terme de " groupes différents " ne renvoie pas à une notion juridique préexistante, mais aux pratiques pédagogiques et aux modes de fonctionnement des établissements, il est seulement allégué qu'une telle notion serait, du fait de son imprécision, de nature à favoriser des discriminations ou une rupture d'égalité entre les enfants. Par suite, le moyen tiré de l'illégalité de cette partie de l'article 36 n'est pas, en l'état de l'instruction, de nature faire naître un doute sérieux.<br/>
<br/>
              19. En quatrième lieu, le III de l'article L. 3131-15 du code de la santé publique prévoit qu'il est mis fin aux mesures prescrites par cet article, sans délai lorsqu'elles ne sont plus nécessaires. La circonstance que le décret contesté ne prévoit pas de " clause de revoyure " pour évaluer le dispositif de l'article 36 n'est pas, en tout état de cause, de nature à faire naître un doute sérieux quant à la légalité du dispositif.<br/>
<br/>
              Sur les moyens communs aux deux mesures contestées : <br/>
<br/>
              20. En premier lieu, les requérants font valoir que les mesures contestées portent une atteinte grave et manifestement illégale au droit à l'éducation dès lors que, selon eux, le port du masque tout au long de la journée ne permet pas l'épanouissement des enfants, empêche des apprentissages telles la lecture et l'écriture qui nécessitent de voir la bouche des enfants et de les entendre prononcer distinctement les sons. Toutefois, ni l'article 36 contesté, ni le protocole sanitaire ne font, par eux-mêmes, obstacle à ce que des mesures pédagogiques soient prises notamment à l'attention des élèves, dont il ne résulte pas au demeurant de l'instruction qu'ils seraient majoritaires, pour lesquels l'obligation du port du masque constitue un obstacle réel aux apprentissages. Il résulte d'ailleurs de l'instruction que ces enfants peuvent être équipés, par exemple, de masques dits inclusifs, disponibles dans les établissements scolaires, intégrant un dispositif transparent permettant de conserver la visibilité de la bouche des personnes qui le portent. Par suite, le moyen tiré de l'atteinte au droit à l'éducation des enfants n'est pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité des mesures contestées.<br/>
<br/>
              21. En deuxième lieu, les requérants allèguent que les mesures contestées sont contraires à la prise en compte de l'intérêt supérieur de l'enfant protégé par la convention relative aux droits de l'enfant du 20 janvier 1990. Toutefois, l'obligation du port de masque est destinée à favoriser la scolarisation des élèves en présence de leurs maîtres. Comme il a été dit, les dispositions contestées ne font pas, par elles-mêmes, obstacle à la prise en compte de la situation particulière des élèves en difficulté du fait de l'usage du masque. Le ministère de l'éducation nationale a mis à disposition des enseignants les recommandations de la société française de phoniatrie et de laryngologie permettant d'améliorer la communication avec un masque. Des masques dits inclusifs sont également mis à disposition en cas de besoin. Les activités physiques et sportives réalisées par les enfants sur le temps scolaire et périscolaire, sous le contrôle de leur professeur, sont en outre dispensées du port du masque quel qu'en soit le lieu. Dans ces conditions, le moyen tiré de ce que les mesures contestées portent atteinte à l'intérêt supérieur de l'enfant n'est pas, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à leur légalité.<br/>
<br/>
              22. En dernier lieu, les requérants se bornent à alléguer que les mesures contestées sont contraires au droit à l'autodétermination personnelle, à la liberté d'aller et venir, à la liberté individuelle, au droit de réunion, au droit à une vie privée et familiale normale protégée par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit à la santé, au secret médical ainsi qu'au principe de fraternité. Aucun de ces moyens n'est de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité des mesures contestées.<br/>
<br/>
              23. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de Mme J... et autres doit être rejetée, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme J... et des autres requérants est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme R... J..., première dénommée, pour l'ensemble des requérants et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'éducation nationale, de la jeunesse et des sports. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
