<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709201</ID>
<ANCIEN_ID>JG_L_2014_11_000000383586</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/92/CETATEXT000029709201.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 05/11/2014, 383586</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383586</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:383586.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 août 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet du Premier ministre de sa demande tendant à l'abrogation des termes " ou payant " figurant à l'article R. 417-6 du code de la route ou, subsidiairement, à l'abrogation intégrale de cet article ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de procéder à l'abrogation demandée dans un délai de trois jours ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
- les autres pièces du dossier ;<br/>
<br/>
              - la Constitution ; <br/>
<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le pacte international des droits civils et politiques ;<br/>
<br/>
              - le code pénal ;<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - la loi n° 2014-58 du 27 janvier 2014 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 octobre 2014, présentée par Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le b) du 3° du I de l'article 63 de la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles donne à l'article L. 2333-87 du code général des collectivités territoriales la rédaction suivante : " Sans préjudice de l'application des articles L. 2213-2 et L. 2512-14, le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale ou du syndicat mixte compétent pour l'organisation des transports urbains (...) peut instituer une redevance de stationnement, compatible avec les dispositions du plan de déplacements urbains, s'il existe. (...) / La délibération institutive établit : (...) / 2° Le tarif du forfait de post-stationnement, applicable lorsque la redevance correspondant à la totalité de la période de stationnement n'est pas réglée dès le début du stationnement ou est insuffisamment réglée. (...) " ; qu'aux termes du V du même article : " A l'exception des II et VIII, le présent article entre en vigueur à compter du premier jour du vingt-quatrième mois suivant la promulgation de la présente loi. A compter de cette même date, aucune sanction, de quelque nature que ce soit, ne peut être établie ou maintenue en raison de l'absence ou de l'insuffisance de paiement de la redevance de stationnement des véhicules établie dans les conditions prévues à l'article L. 2333-87 du code général des collectivités territoriales. La deuxième phrase du présent V n'est applicable ni aux infractions liées à l'absence ou à l'insuffisance de paiement d'une redevance de stationnement constatées avant la date d'entrée en vigueur du présent article, ni aux procédures en cours à cette même date. " ;<br/>
<br/>
              2. Considérant que l'article R. 417-6 du code de la route dispose que : " Tout arrêt ou stationnement gratuit ou payant contraire à une disposition réglementaire autre que celles prévues au présent chapitre est puni de l'amende prévue pour les contraventions de la première classe " ; que Mme A...a demandé au Premier ministre d'abroger cet article en tant qu'il concerne le stationnement payant ; qu'à l'appui d'un recours pour excès de pouvoir contre la décision implicite de refus née du silence gardé sur cette demande, elle soutient que les dispositions précitées du V de l'article 63 de la loi du 27 janvier 2014 qui suppriment les sanctions pour absence ou insuffisance de paiement de la redevance de stationnement des véhicules imposent au Premier ministre d'abroger les dispositions réglementaires prévoyant des sanctions pénales à raison de tels faits ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées du V de l'article 63 de la loi du 27 janvier 2014 que la suppression des sanctions pour absence ou insuffisance de paiement de la redevance de stationnement des véhicules ne prendra effet que le 1er janvier 2016, pour les faits constatés à compter de cette date ; que Mme A...soutient qu'en différant ainsi l'entrée en vigueur de la suppression des sanctions et en permettant le maintien des sanctions pour des faits antérieurs à cette entrée en vigueur, le législateur a méconnu le principe de l'application rétroactive de la loi pénale moins sévère, découlant de l'article 8 de la Déclaration des droits de l'homme et du citoyen ; qu'elle demande au Conseil d'Etat de transmettre au Conseil constitutionnel la question de la conformité à la Constitution des termes " A compter de cette date " figurant dans la deuxième phrase du V de l'article 63 de la loi, ainsi que de la troisième phrase du même V ; qu'elle soutient également que ces dispositions doivent être écartées comme contraires aux stipulations de l'article 7, paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et à celles de l'article 15, paragraphe 1 du pacte international relatif aux droits civils et politiques, ainsi qu'aux dispositions de l'article 112-1 du code pénal ; <br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article 8 de la Déclaration de 1789 : " La loi ne doit établir que des peines strictement et évidemment nécessaires " ; que le fait de ne pas appliquer aux infractions commises sous l'empire de la loi ancienne la loi pénale nouvelle, plus douce, revient à permettre au juge de prononcer les peines prévues par la loi ancienne et qui, selon l'appréciation même du législateur, ne sont plus nécessaires ; <br/>
<br/>
              6. Considérant, toutefois, que l'article 63 de la loi du 27 janvier 2014 a modifié l'article L. 2333-87 du code général des collectivités territoriales afin d'instituer un nouveau régime de redevances de stationnement, dans lequel le conseil municipal arrête un barème tarifaire applicable en cas de paiement immédiat de la redevance et un forfait de post-stationnement, applicable lorsque la redevance correspondant à la totalité de la période de stationnement n'est pas réglée dès le début du stationnement ou est insuffisamment réglée, en prévoyant que ce forfait de post-stationnement est perçu et recouvré conformément aux dispositions du titre II du livre III de la deuxième partie du code général de la propriété des personnes publiques relatives aux produits et aux redevances du domaine des collectivités territoriales, de leurs groupements et de leurs établissements publics  ; que, par le V de l'article 63, il n'a pas mis en place une loi pénale plus douce, mais supprimé les sanctions pénales qui étaient indissociables de la réglementation à laquelle il a mis un terme et que le nouveau régime mis en place rend inutiles ; que, dès lors, il n'a pas méconnu les exigences découlant du principe de nécessité des peines en prévoyant que cette suppression ne prendrait effet qu'à la date d'entrée en vigueur du nouveau régime, pour les faits constatés à compter de cette date ; que, par suite, la question posée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du V de l'article 63 de la loi du 27 janvier 2014 qui diffèrent l'entrée en vigueur de la suppression des sanctions portent atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              7. Considérant que les moyens tirés d'une incompatibilité avec le principe de rétroactivité de la loi pénale plus douce tel qu'il résulte des stipulations de l'article 7, paragraphe I de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de celles de l'article 15, paragraphe 1 du pacte international relatif aux droits civils et politiques doivent être écartés pour les mêmes motifs que ceux qui ont été énoncés au point 6 ; que les dispositions de l'article 112-1 du code pénal qui fixent les conditions d'application dans le temps des dispositions pénales plus douces ne peuvent être utilement invoquées pour écarter l'application des dispositions du V de l'article 63 de la loi du 27 janvier 2014 qui fixent les conditions d'entrée en vigueur de la règle posée par sa deuxième phrase ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; <br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que les conclusions tendant à ce qu'il soit enjoint au Premier ministre d'abroger les termes " ou payant " figurant à l'article R. 417-6 du code de la route doivent être rejetées ; <br/>
<br/>
              Sur les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
-----------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeA.... <br/>
<br/>
		Article 2 : La requête de Mme A...est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au Premier ministre.<br/>
Copie en sera adressée pour information au Conseil constitutionnel, au ministre de l'intérieur et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
