<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042702025</ID>
<ANCIEN_ID>JG_L_2020_12_000000441137</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/20/CETATEXT000042702025.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 18/12/2020, 441137, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441137</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Bertinotti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:441137.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 11 juin, 24 juillet et 27 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 26 décembre 2019 rapportant le décret du 12 décembre 2013 en ce qu'il lui avait accordé la nationalité française ;<br/>
<br/>
              2°) d'enjoindre le ministre de l'intérieur de réexaminer sa situation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de Mme Dominique Bertinotti, conseiller d'Etat, <br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 décembre 2020, présentée par le ministre de l'intérieur ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ". <br/>
<br/>
              2.	M. A..., ressortissant algérien, a déposé une demande de naturalisation, le 17 mai 2013, dans laquelle il a indiqué être en concubinage et être père d'un enfant issu d'une précédente union. Au vu de ses déclarations, il a été naturalisé par décret du 12 décembre 2013. Toutefois, par bordereaux reçus les 17 janvier, 19 juillet et 21 décembre 2018, le ministre des affaires étrangères et du développement international a informé le ministre chargé des naturalisations que M. A... avait contracté un mariage le 20 août 1991 à Sidi Ali (Algérie) avec Mme D... B..., ressortissante algérienne, et que de leur union étaient issus cinq enfants, nés à Sidi Ali, entre 1992 et 2007, tous résidant habituellement en Algérie avec leur mère. Par décret du 26 décembre 2019, le Premier ministre a rapporté le décret de naturalisation de M. A... au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation personnelle et familiale. M. A... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3.	En premier lieu, selon l'article 59 du décret du 30 décembre 1993 relatif aux déclarations de nationalité, aux décisions de naturalisation, de réintégration, de perte, de déchéance et de retrait de la nationalité française, applicable en vertu de l'article 62 du même décret en cas de retrait de décret de naturalisation ou de réintégration décidé en application de l'article 272 du code civil, lorsque le Gouvernement a l'intention de retirer un tel décret, il notifie, en la forme administrative ou par lettre recommandée avec demande d'avis de réception, les motifs de droit et de fait motivant le retrait à l'intéressé, qui dispose d'un délai d'un mois à compter de la notification pour faire parvenir ses observations en défense. <br/>
<br/>
              4.	Il ressort des pièces du dossier que le ministre chargé des naturalisations a notifié à M. A... les motifs justifiant le retrait du décret lui accordant la nationalité française par une lettre datée du 29 août 2019. La lettre a été expédiée au nom et à l'adresse de l'intéressé avec demande d'avis de réception sans que ce dernier n'établisse avoir informé l'administration chargé des naturalisations de son changement d'adresse. Elle a été présentée à son domicile le 2 septembre 2019 mais n'a pas été réclamée par l'intéressé aux services postaux, qui ont retourné le pli au ministre le 19 septembre 2019, après l'expiration du délai de mise en instance postal. Cette notification doit être regardée, faute pour l'intéressé d'avoir pris toutes les dispositions utiles pour retirer le pli qui lui avait été régulièrement adressé, comme étant intervenue à la date de première présentation du pli par les services postaux, soit le 2 septembre 2019. Par suite, le moyen tiré de ce que le décret attaqué serait illégal faute pour l'intéressé d'avoir pu présenter ses observations en défense dans le délai d'un mois prévu par le décret du 30 décembre 1993 ne peut qu'être écarté.<br/>
<br/>
              5.	En second lieu, le décret attaqué, qui a été compétemment signé, comporte l'indication des éléments de droit et de fait sur lesquels il se fonde et est ainsi suffisamment motivé. <br/>
<br/>
              6.	En troisième lieu, l'article 21-16 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation personnelle et familiale en France de l'intéressé à la date du décret lui accordant la nationalité française.<br/>
<br/>
              7.	Il ressort des pièces du dossier que M. A... s'est marié le 21 août 1991 avec une ressortissante algérienne résidant habituellement à l'étranger et que cinq enfants sont nés de cette union. M. A... soutient que son union coutumière avec Mme B... ne pouvait être regardée comme un mariage avant son enregistrement par l'état civil algérien. Toutefois, la circonstance que cette union ne pourrait être qualifiée de mariage en vertu de la loi qui lui est applicable n'interdit pas à l'autorité compétente de prendre en compte son existence pour apprécier si la condition de résidence posée par l'article 21-16 du code civil est remplie. Il en résulte qu'alors même qu'il remplirait les autres conditions requises à l'obtention de la nationalité française, la circonstance que l'intéressé ait conclu une union coutumière à l'étranger avec une ressortissante algérienne, comme l'existence des enfants nés de cette union, était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts. L'intéressé, qui maîtrise la langue française, ainsi qu'il ressort du procès-verbal d'assimilation du 17 mai 2013, ne pouvait se méprendre ni sur la teneur des indications devant être portées à la connaissance de l'administration chargée d'instruire sa demande, ni sur la portée de la déclaration sur l'honneur qu'il a signée. Dans ces conditions, M. A... doit être regardée comme ayant volontairement dissimulé sa situation familiale. Par suite, en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude, le ministre de l'intérieur n'a pas méconnu les dispositions de l'article 27-2 du code civil.<br/>
<br/>
              8.	 En dernier lieu, un décret qui rapporte pour fraude un décret de naturalisation est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille et n'affecte pas, dès lors, le droit au respect de la vie familiale. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M. A... garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              9.	Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 26 décembre 2019 par lequel le ministre de l'intérieur a rapporté le décret du 12 décembre 2013 qui lui avait accordé la nationalité française. Ses conclusions à fin d'injonction, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
