<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042228777</ID>
<ANCIEN_ID>JG_L_2020_07_000000441692</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/22/87/CETATEXT000042228777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/07/2020, 441692, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441692</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441692.20200728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 441692, par une requête et un mémoire en réplique, enregistrés les 8 et 24 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération des experts comptables et des commissaires aux comptes de France (Fédération ECF) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2020-667 du 2 juin 2020 relatif à la Compagnie nationale et aux compagnies régionales des commissaires aux comptes ;<br/>
<br/>
              2°) de préciser qu'il en résulte que les prochaines élections devront se dérouler conformément aux modalités antérieures ou, à défaut, d'enjoindre à l'Etat d'adopter des modalités provisoires d'organisation des élections conformes aux motifs de l'ordonnance à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir ;<br/>
              - la condition de l'urgence est remplie eu égard, en premier lieu, à l'imminence des élections et aux risques attachés à leur organisation dans des conditions irrégulières, en deuxième lieu, à la difficulté de constituer des listes électorales conformément au décret attaqué, à trois mois de l'échéance électorale et, en troisième lieu, aux atteintes substantielles portées par le décret à ses intérêts privés, en ce qu'il rompt l'égalité entre les candidats et entre les électeurs, prive la Fédération de la possibilité d'établir, à titre définitif, une liste électorale stabilisée et nuit à son poids au sein du Conseil national des commissaires aux comptes (CNCC), sans qu'un quelconque intérêt public ne justifie que le décret soit exécuté ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - il est entaché d'incompétence dès lors qu'il revient au pouvoir législatif de prévoir, d'une part, l'existence d'une entité, telle que le CNCC, chargée de représenter la profession et ses intérêts auprès des pouvoirs publics, notamment eu égard au rôle de représentation syndicale qu'elle remplit, et, d'autre part, les règles générales applicables aux élections des organes d'une personne morale ;<br/>
              - il est entaché d'irrégularité dès lors qu'il n'est pas établi qu'il est conforme au projet soumis au Conseil d'Etat et à l'avis que celui-ci a rendu ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il prévoit, de manière injustifiée, que le CNCC est composé de deux collèges de commissaires aux comptes distincts représentés à hauteur de 50 % chacun, ce qui conduit à une surreprésentation des commissaires aux comptes exerçant une mission de certification EIP qui n'est pas justifiée par un motif d'intérêt général et qui porte ainsi atteinte à l'égalité entre les syndicats et au principe de représentativité ;<br/>
              - il porte atteinte au principe d'égalité en instaurant une discrimination entre les candidats potentiels, selon qu'ils exercent ou non une mission de certification au 30 juin de l'année en cause, sans que cette différence de traitement ne soit justifiée par une différence objective de situations, un motif d'intérêt général ou les objectifs poursuivis par le décret ;<br/>
              - il est entaché d'un défaut de clarté dès lors, en premier lieu, qu'il rattache les commissaires aux comptes exerçant en société à un collège ne correspondant pas forcément à leur activité propre, en deuxième lieu, qu'il apprécie le rattachement à l'un ou l'autre des collèges en fonction de l'activité exercée au 30 juin de l'année de l'élection, sans prendre en considération la durée d'exercice des missions, l'objectif de composition paritaire du CNCC et la rotation des missions de certification des commissaires aux comptes tous les six ans et, en troisième lieu, qu'il existe une incompatibilité entre la désignation d'office des présidents des compagnies régionales des commissaires aux comptes (CRCC) et de leurs suppléants avec le caractère paritaire du CNCC, notamment lorsqu'un président de CRCC change de collège en cours de mandat ou lorsqu'il est amené à être remplacé en cours de mandat, le décret portant, sur ces deux derniers points, une atteinte au principe de sécurité juridique et étant entaché d'incompétence négative faute de prévoir les conséquences pratiques de ces situations ;  <br/>
              - il porte atteinte au principe de sécurité juridique du fait de la brièveté du délai entre sa publication et les élections, de son entrée en vigueur immédiate et de l'absence de dispositions transitoires.<br/>
<br/>
<br/>
              2° Sous le n° 441721, par une requête et un mémoire en réplique, enregistrés les 9 et 24 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2020-667 du 2 juin 2020 relatif à la Compagnie nationale et aux compagnies régionales des commissaires aux comptes ;<br/>
<br/>
              2°) de préciser qu'il en résulte que les prochaines élections devront se tenir conformément aux modalités antérieures ou, à défaut, d'enjoindre à l'Etat d'adopter des modalités provisoires d'organisation des élections conformes aux motifs de l'ordonnance à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il justifie d'un intérêt à agir;<br/>
              - la condition de l'urgence est remplie eu égard, en premier lieu, à l'imminence des élections et aux risques attachés à leur organisation dans des conditions irrégulières, en deuxième lieu, à la difficulté de constituer des listes électorales conformément au décret attaqué, à trois mois de l'échéance électorale et, en troisième lieu, aux atteintes substantielles portées par le décret à ses intérêts privés, en ce qu'il rompt l'égalité entre les candidats et entre les électeurs, prive la Fédération de la possibilité d'établir, à titre définitif, une liste électorale stabilisée et nuit à son poids au sein du Conseil national des commissaires aux comptes (CNCC), sans qu'un quelconque intérêt public ne justifie que le décret soit exécuté ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - il est entaché d'incompétence dès lors qu'il revient au pouvoir législatif de prévoir, d'une part, l'existence d'une entité, telle que le CNCC, chargée de représenter la profession et ses intérêts auprès des pouvoirs publics, notamment eu égard au rôle de représentation syndicale qu'elle remplit, et, d'autre part, les règles générales applicables aux élections des organes d'une personne morale ;<br/>
              - il est entaché d'irrégularité dès lors qu'il n'est pas établi qu'il est conforme au projet soumis au Conseil d'Etat et à l'avis que celui-ci a rendu ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il prévoit, de manière injustifiée, que le CNCC est composé de deux collèges de commissaires aux comptes distincts représentés à hauteur de 50 % chacun, ce qui conduit à une surreprésentation des commissaires aux comptes exerçant une mission de certification EIP qui n'est pas justifiée par un motif d'intérêt général et qui porte ainsi atteinte à l'égalité entre les syndicats et au principe de représentativité ;<br/>
              - il porte atteinte au principe d'égalité en instaurant une discrimination entre les candidats potentiels, selon qu'ils exercent ou non une mission de certification au 30 juin de l'année en cause, sans que cette différence de traitement ne soit justifiée par une différence objective de situations, un motif d'intérêt général ou les objectifs poursuivis par le décret ;<br/>
              - il est entaché d'un défaut de clarté dès lors, en premier lieu, qu'il rattache les commissaires aux comptes exerçant en société à un collège ne correspondant pas forcément à leur activité propre, en deuxième lieu, qu'il apprécie le rattachement à l'un ou l'autre des collèges en fonction de l'activité exercée au 30 juin de l'année de l'élection, sans prendre en considération la durée d'exercice des missions, l'objectif de composition paritaire du CNCC et la rotation des missions de certification des commissaires aux comptes tous les six ans et, en troisième lieu, qu'il existe une incompatibilité entre la désignation d'office des présidents des compagnies régionales des commissaires aux comptes (CRCC) et de leurs suppléants avec le caractère paritaire du CNCC, notamment lorsqu'un président de CRCC change de collège en cours de mandat ou lorsqu'il est amené à être remplacé en cours de mandat, le décret portant, sur ces deux derniers points, une atteinte au principe de sécurité juridique et étant entaché d'incompétence négative faute de prévoir les conséquences pratiques de ces situations ;  <br/>
              - il porte atteinte au principe de sécurité juridique du fait de la brièveté du délai entre sa publication et les élections, de son entrée en vigueur immédiate et de l'absence de dispositions transitoires.<br/>
<br/>
<br/>
<br/>
              3° Sous le n° 441723, par une requête et un mémoire en réplique, enregistrés les 9 et 24 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2020-667 du 2 juin 2020 relatif à la Compagnie nationale et aux compagnies régionales des commissaires aux comptes ;<br/>
<br/>
              2°) de préciser qu'il en résulte que les prochaines élections devront se tenir conformément aux modalités antérieures ou, à défaut, d'enjoindre à l'Etat d'adopter des modalités provisoires d'organisation des élections conformes aux motifs de l'ordonnance à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il justifie d'un intérêt à agir;<br/>
              - la condition de l'urgence est remplie eu égard, en premier lieu, à l'imminence des élections et aux risques attachés à leur organisation dans des conditions irrégulières, en deuxième lieu, à la difficulté de constituer des listes électorales conformément au décret attaqué, à trois mois de l'échéance électorale et, en troisième lieu, aux atteintes substantielles portées par le décret à ses intérêts privés, en ce qu'il rompt l'égalité entre les candidats et entre les électeurs, prive la Fédération de la possibilité d'établir, à titre définitif, une liste électorale stabilisée et nuit à son poids au sein du Conseil national des commissaires aux comptes (CNCC), sans qu'un quelconque intérêt public ne justifie que le décret soit exécuté ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - il est entaché d'incompétence dès lors qu'il revient au pouvoir législatif de prévoir, d'une part, l'existence d'une entité, telle que le CNCC, chargée de représenter la profession et ses intérêts auprès des pouvoirs publics, notamment eu égard au rôle de représentation syndicale qu'elle remplit, et, d'autre part, les règles générales applicables aux élections des organes d'une personne morale ;<br/>
              - il est entaché d'irrégularité dès lors qu'il n'est pas établi qu'il est conforme au projet soumis au Conseil d'Etat et à l'avis que celui-ci a rendu ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il prévoit, de manière injustifiée, que le CNCC est composé de deux collèges de commissaires aux comptes distincts représentés à hauteur de 50 % chacun, ce qui conduit à une surreprésentation des commissaires aux comptes exerçant une mission de certification EIP qui n'est pas justifiée par un motif d'intérêt général et qui porte ainsi atteinte à l'égalité entre les syndicats et au principe de représentativité ;<br/>
              - il porte atteinte au principe d'égalité en instaurant une discrimination entre les candidats potentiels, selon qu'ils exercent ou non une mission de certification au 30 juin de l'année en cause, sans que cette différence de traitement ne soit justifiée par une différence objective de situations, un motif d'intérêt général ou les objectifs poursuivis par le décret ;<br/>
              - il est entaché d'un défaut de clarté dès lors, en premier lieu, qu'il rattache les commissaires aux comptes exerçant en société à un collège ne correspondant pas forcément à leur activité propre, en deuxième lieu, qu'il apprécie le rattachement à l'un ou l'autre des collèges en fonction de l'activité exercée au 30 juin de l'année de l'élection, sans prendre en considération la durée d'exercice des missions, l'objectif de composition paritaire du CNCC et la rotation des missions de certification des commissaires aux comptes tous les six ans et, en troisième lieu, qu'il existe une incompatibilité entre la désignation d'office des présidents des compagnies régionales des commissaires aux comptes (CRCC) et de leurs suppléants avec le caractère paritaire du CNCC, notamment lorsqu'un président de CRCC change de collège en cours de mandat ou lorsqu'il est amené à être remplacé en cours de mandat, le décret portant, sur ces deux derniers points, une atteinte au principe de sécurité juridique et étant entaché d'incompétence négative faute de prévoir les conséquences pratiques de ces situations ;  <br/>
              - il porte atteinte au principe de sécurité juridique du fait de la brièveté du délai entre sa publication et les élections, de son entrée en vigueur immédiate et de l'absence de dispositions transitoires.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré sous ces trois numéros le 21 juillet 2020, le garde des sceaux, ministre de la justice conclut au rejet des requêtes. Il soutient que la situation d'urgence n'est pas constituée et que les moyens soulevés ne sont pas propres à créer un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
              Les requêtes ont été communiquées au Premier ministre et au ministre des Outre-mer, qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Fédération des experts comptables et des commissaires aux comptes de France, M. C... B... et M. D... A... et, d'autre part, le Premier ministre, le garde des sceaux, ministre de la justice et le ministre des outre-mer ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 27 juillet 2020, à 9 heures 30 : <br/>
<br/>
              - Me Melka, avocat au Conseil d'Etat et à la Cour de cassation, avocate de la Fédération des experts comptables et des commissaires aux comptes de France, M. B... et M. A... ;<br/>
<br/>
              - M. B... ;<br/>
<br/>
              - M. A... ;<br/>
<br/>
              - les représentants des requérants ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - La Constitution ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Les trois requêtes visées ci-dessus tendent à la suspension de l'exécution du même décret. Il y a lieu de les joindre pour statuer par une seule ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              3. Aux termes de l'article L. 821-6 du code de commerce : " Il est institué auprès du garde des sceaux, ministre de la justice, une Compagnie nationale des commissaires aux comptes, établissement d'utilité publique doté de la personnalité morale, chargée de représenter la profession de commissaire aux comptes auprès des pouvoirs publics. / Elle concourt au bon exercice de la profession, à sa surveillance ainsi qu'à la défense de l'honneur et de l'indépendance de ses membres. / Il est institué une compagnie régionale des commissaires aux comptes, dotée de la personnalité morale, par ressort de cour d'appel. Toutefois, le garde des sceaux, ministre de la justice, peut procéder à des regroupements, après avis de la compagnie nationale et après consultation, par cette dernière, des compagnies régionales intéressées ". Aux termes de l'article L. 821-15 du même code : " Les conditions d'application du présent chapitre ", au sein duquel est inséré l'article L. 821-6, " sont fixées par décret en Conseil d'Etat ". <br/>
<br/>
              4. Le décret du 2 juin 2020, dont les requérants demandent que soit ordonnée la suspension de l'exécution sur le fondement de l'article L. 521-1 du code de justice administrative, a introduit au code de commerce des dispositions modifiant notamment la composition du Conseil national et les conditions d'éligibilité à ce conseil. Ainsi, l'article R. 821-37 de ce code dispose désormais : " I- Le Conseil national est composé de soixante membres désignés pour une durée de quatre ans, qui comprennent l'ensemble des présidents de compagnies régionales et des commissaires aux comptes élus. / Il comprend pour moitié des commissaires aux comptes exerçant une ou plusieurs missions de certification auprès d'entités d'intérêt public et pour moitié des commissaires aux comptes n'exerçant pas de mission de certification auprès d'entités d'intérêt public. / Un premier collège d'électeurs est composé des commissaires aux comptes personnes physiques à jour de leurs cotisations professionnelles, exerçant une ou plusieurs missions de certification auprès d'entités d'intérêt public. Un second collège est composé des commissaires aux comptes personnes physiques à jour de leurs cotisations professionnelles, n'exerçant pas de mission de certification auprès d'entités d'intérêt public. / Lorsqu'il exerce en société, chaque commissaire aux comptes relève du collège auquel appartient la société. / La Compagnie nationale répartit les commissaires aux comptes entre les deux collèges en fonction de leur activité au 30 juin de l'année d'expiration des mandats. / Seules sont éligibles les personnes physiques exerçant une ou plusieurs missions de certification au 30 juin de l'année d'expiration des mandats. / Le nombre de commissaires aux comptes élus au sein de chacun des collèges est déterminé en soustrayant le nombre de présidents de compagnies régionales relevant de sa catégorie des trente sièges qui lui sont attribués ". <br/>
<br/>
              5. Premièrement, le moyen tiré de ce que le Premier ministre n'était pas compétent pour prendre le décret contesté, notamment en ce qu'il crée le Conseil national et fixe sa composition et les règles d'éligibilité de ses membres, et particulièrement en ce qu'il serait intervenu en matière de droit syndical, alors que l'article 34 de la Constitution réserve à la loi la détermination des principes fondamentaux de ce droit, n'est pas propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de ce décret, dès lors, et en tout état de cause, que le pouvoir législatif a renvoyé, par l'article L. 821-15 du code de commerce cité au point 3, à des décrets en Conseil d'Etat les conditions d'application du chapitre 1er du titre II du Livre VIII de ce code, et donc notamment l'organisation de la Compagnie nationale des commissaires aux comptes qu'il a instituée par l'article L. 821-6 également cité au point 3.<br/>
<br/>
              6. Deuxièmement, il ressort de la copie de la minute de la section des finances du Conseil d'Etat, telle qu'elle a été versée au dossier par le garde des sceaux, ministre de la justice, que le texte publié ne contient pas de dispositions qui différeraient à la fois du projet initial du gouvernement et du texte adopté par le Conseil d'Etat. Le moyen tiré de ce que le décret contesté aurait, pour ce motif, été pris à l'issue d'une procédure irrégulière, n'est donc pas propre à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              7. Troisièmement, les requérants soutiennent que le décret contesté serait entaché d'erreur manifeste d'appréciation, premièrement, en ce qu'il prévoit une composition du Conseil national paritaire entre les commissaires aux comptes exerçant une ou plusieurs missions de certification auprès d'entités d'intérêt public et les commissaires aux comptes n'exerçant pas de telles missions, cette parité n'étant pas représentative de la répartition numérique des commissaires entre ces deux catégories et le critère retenu n'étant pas pertinent, de sorte qu'il serait porté atteinte au principe de représentativité et, en outre, à l'égalité entre les syndicats de cette profession, deuxièmement, en ce que le rattachement à un collège électoral des commissaires aux comptes exerçant en société dépend de l'exercice par cette société de missions de certification auprès d'entités d'intérêt public, indépendamment de leur activité propre et conduit à rendre inéligibles certains commissaires aux comptes, troisièmement, en ce que le choix de la date du 30 juin de l'année d'expiration des mandats des membres du Conseil national pour la détermination du rattachement des commissaires aux comptes à l'un des deux collèges d'électeurs, selon qu'ils exercent ou non à cette date une mission de certification auprès d'entités d'intérêt public, date dont le ministre de la justice fait valoir en défense qu'elle correspond à la date de clôture des comptes annuels et permet donc une exacte prise en compte de l'activité des commissaires aux comptes, outre qu'elle introduit une rupture d'égalité entre les candidats potentiels, ne permet pas d'apprécier exactement l'activité effective des commissaires aux comptes, alors que le choix d'une période de référence aurait été préférable. Ce moyen n'est cependant pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité des dispositions contestées, dès lors, notamment, que le pouvoir réglementaire a entendu tenir compte, non seulement de la démographie de la profession, mais aussi du poids économique respectif de ces deux catégories, lesquelles correspondent aux deux modes d'exercice de la profession, ce que les requérants ne contestent pas en principe, mais pour les modalités de mise en oeuvre retenues par le gouvernement, dans l'objectif d'assurer une représentation équilibrée de la profession auprès des pouvoirs publics, conformément aux prescriptions du premier alinéa de l'article L. 821-6 du code de commerce. <br/>
<br/>
              8. Quatrièmement, il en est de même du moyen tiré de la méconnaissance de l'objectif constitutionnel de clarté et d'intelligibilité de la norme, de l'atteinte au principe de sécurité juridique et de ce que le décret serait entaché " d'incompétence négative ", en ce qu'il prévoit qu'un commissaire aux comptes exerçant en société relève du collège auquel appartient sa société, en ce qu'il a fixé la date du 30 juin de l'année d'expiration des mandats en cours pour la détermination du rattachement aux collèges, en ce que le décret ne prévoit pas l'hypothèse du changement d'activité en cours de mandat d'un président de compagnie régionale, membre de droit du Conseil national, non plus que l'hypothèse où il se révélerait impossible de désigner au sein du même collège un suppléant d'un président de compagnie régionale absent ou empêché, et en ce que le changement en cours de mandat de l'activité d'un commissaire aux comptes membre du Conseil national peut conduire à rompre la parité initiale de la composition de ce conseil, dès lors qu'aucune obscurité n'entache les dispositions contestées. <br/>
<br/>
              9. Cinquièmement, si les requérants soutiennent que le délai d'un peu plus de trois mois entre la publication du décret et l'organisation des prochaines élections au Conseil national selon les modalités nouvelles prévues par ce texte, porte atteinte au principe de sécurité juridique et que le pouvoir réglementaire aurait dû prévoir des mesures transitoires à même de ménager un temps de préparation plus long pour ces élections, le ministre de la justice faisant valoir en défense que ce délai n'est pas trop bref et qu'en toute hypothèse le dispositif retenu par le décret a été longuement soumis à concertation et était connu des acteurs concernés avant même sa publication, ce moyen n'apparaît pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité du décret contesté. <br/>
<br/>
              10. Il résulte de ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que les requêtes doivent être rejetées, y compris leurs conclusions à fin d'injonction et leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er: Les requêtes de la Fédération des experts comptables et des commissaires aux comptes de France, de M. B... et de M. A... sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à la Fédération des experts comptables et des commissaires aux comptes de France, à M. C... B..., à M. D... A... et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre et au ministre des Outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
