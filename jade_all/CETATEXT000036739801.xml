<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739801</ID>
<ANCIEN_ID>JG_L_2018_03_000000416569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/98/CETATEXT000036739801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 23/03/2018, 416569, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416569.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 15 décembre 2017, 23 février 2018 et 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des producteurs d'alcool agricole (SNPAA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du ministre de l'action et des compte publics du 16 octobre 2017 publiée sous la référence CPAD1727521C, relative au prélèvement supplémentaire de taxe générale sur les activités polluantes (TGAP) applicable aux carburants, en tant qu'elle prévoit, notamment à ses paragraphes 17 et 87, pour la filière essence, que la bio-essence obtenue par hydrotraitement à partir de plantes oléagineuses peut être prise en compte pour la minoration du taux de la TGAP carburants et que les plafonds de 7 % et 0,6 % définis au paragraphe 6 du III de l'article 266 quindecies du code des douanes ne sont pas applicables à ces produits ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des douanes ; <br/>
              - le code de l'énergie ;<br/>
              - le code général des impôts ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Le Syndicat national des producteurs d'alcool agricole (SNPAA) demande l'annulation pour excès de pouvoir de la circulaire du 16 octobre 2017 par laquelle le ministre de l'action et des comptes publics précise aux opérateurs concernés les modalités de mise en oeuvre du prélèvement supplémentaire de taxe générale sur les activités polluantes (TGAP) applicable aux carburants et de calcul de la minoration de son taux au titre de l'année 2017.<br/>
              2. Aux termes de l'article 266 quindecies du code des douanes dans sa version commentée par la circulaire litigieuse : " I.-Les personnes qui mettent à la consommation en France des essences reprises aux indices 11 et 11 bis et 11 ter du tableau B du 1 de l'article 265, du gazole repris à l'indice 20 et à l'indice 22, du superéthanol E85 repris à l'indice 55 et du carburant ED 95 repris à l'indice 56 de ce même tableau sont redevables d'un prélèvement supplémentaire de la taxe générale sur les activités polluantes./ II. -Son assiette est déterminée conformément aux dispositions du 1° du 2 de l'article 298 du code général des impôts, pour chaque carburant concerné. Pour le gazole non routier repris à l'indice 20, ce prélèvement supplémentaire s'applique à 75 % des mises à la consommation en France en 2017. / III.-Son taux est fixé à 7,5 % dans la filière essence et à 7,7 % dans la filière gazole. / Il est diminué à proportion de la quantité d'énergie renouvelable des biocarburants contenus dans les carburants soumis au prélèvement mis à la consommation en France, sous réserve que ces biocarburants respectent les critères de durabilité prévus aux articles L. 661-3 à L. 661-6 du code de l'énergie. / Pour la filière essence, le taux est diminué de la part d'énergie renouvelable résultant du rapport entre l'énergie renouvelable des biocarburants contenus dans les produits repris aux indices d'identification 11, 11 bis, 11 ter, 55 et 56 du tableau B du 1 de l'article 265 du présent code mis à la consommation en France à usage de carburants et l'énergie de ces mêmes carburants soumis au prélèvement, exprimés en pouvoir calorifique inférieur. (...) La part d'énergie renouvelable, prise en compte pour cette minoration, ne peut être supérieure aux valeurs suivantes : 1° Dans la filière essence, la part d'énergie renouvelable maximale des biocarburants produits à partir de céréales et d'autres plantes riches en amidon ou sucrières est de 7 %. Cette part est de 0,6 %, pour les biocarburants mentionnés au e du 4 de l'article 3 de la directive 2009/28/ CE modifiée par la directive (UE) 2015/1513 du Parlement européen et du Conseil du 9 septembre 2015 modifiant la directive 98/70/ CE concernant la qualité de l'essence et des carburants diesel et modifiant la directive 2009/28/ CE relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables ; (...) La liste des biocarburants éligibles à cette minoration de taux est définie par arrêté conjoint des ministres chargés des douanes, de l'écologie, de l'énergie et de l'agriculture. (...) Un arrêté conjoint des ministres chargés des douanes, de l'écologie, de l'énergie et de l'agriculture fixe la liste des matières premières permettant de produire des biocarburants, qui peuvent être pris en compte pour le double de leur valeur réelle exprimée en quantité d'énergie renouvelable, ainsi que les conditions et modalités de cette prise en compte, notamment en matière d'exigence de traçabilité. ". Il résulte de ces dispositions qu'elles instituent un supplément de taxe générale sur les activités polluantes (TGAP) à la charge des personnes qui mettent à la consommation certains types d'essence. Afin d'encourager les opérateurs qui en sont redevables à incorporer des biocarburants dans l'essence qu'ils commercialisent, elles prévoient dans le même temps un mécanisme de réduction du taux à proportion de la part d'énergie renouvelable des biocarburants que ces derniers y intègrent. <br/>
              3. En premier lieu, si le syndicat requérant soutient que la circulaire litigieuse n'a pas été signée par une personne disposant d'une délégation de signature régulière du ministre en charge des douanes, il résulte de l'article 43 de l'arrêté du 10 juillet 2017 portant délégation de signature au sein de la direction générale des douanes et droits indirects que l'administrateur civil, chef du bureau F2, disposait d'une telle délégation. Le moyen manque, par suite, en fait.<br/>
              4. En second lieu, le syndicat requérant soutient qu'en ce qu'elle indique que, pour la filière essence, la bio-essence obtenue par hydrotraitement à partir de plantes oléagineuses peut être prise en compte pour la minoration du taux de la TGAP carburants et que les plafonds de 7 % et 0,6 % définis au paragraphe 6 du III de l'article 266 quindecies du code des douanes ne sont pas applicables à ces produits, la circulaire attaquée aurait ajouté à la loi et méconnu sa portée, les dispositions législatives citées au point 2 faisant au contraire obstacle, selon lui, à ce que de telles bio-essences ouvrent droit au mécanisme de réduction de taux, a fortiori sans plafonnement.<br/>
              5. D'une part, il résulte des dispositions du deuxième alinéa du III de l'article 266 quindecies du code des douanes, éclairées par les travaux parlementaires préalables à leur adoption et à leurs modifications successives, que tous les biocarburants respectant les critères de durabilité prévus aux articles L. 661-3 à L. 661-6 du code de l'énergie ouvrent droit au mécanisme de réduction de taux de TGAP. Il en va notamment ainsi de la bio-essence obtenue par hydrotraitement à partir de plantes oléagineuses. Il suit de là qu'en les mentionnant parmi les biocarburants pris en compte pour la réduction du taux prévu à l'article 266 quindecies du code des douanes, la circulaire litigieuse n'ajoute pas à la loi qu'elle a pour objet de commenter.<br/>
              6. D'autre part, si au sein des biocarburants satisfaisant aux conditions de durabilité prévues par le code de l'énergie, le législateur a entendu limiter le recours à ceux qui sont produits à partir de céréales et d'autres plantes riches en amidon ou sucrières et donner une place particulière à ceux qui sont considérés comme " avancés ", en fixant au 1° du III de l'article 266 quindecies, à respectivement 7 % et 0,6 % le niveau maximal de prise en compte de la part d'énergie renouvelable qui leur est imputable pour le calcul de la minoration du taux de la taxe, il n'a en revanche pas fait figurer, parmi ces biocarburants limitativement énumérés, les bio-essences obtenues par hydrotraitement à partir de plantes oléagineuses, ce qui fait obstacle à ce qu'il leur soit fait application de ces plafonds. La circulaire précise par conséquent à bon droit que cette catégorie de biocarburants n'est pas soumise à cette disposition.<br/>
              7. Il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur  la  fin de non-recevoir soulevée par le ministre, le SNPAA n'est pas fondé à demander l'annulation de la circulaire qu'il attaque.<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administratives font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du SNPAA est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat national des producteurs d'alcool agricole et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
