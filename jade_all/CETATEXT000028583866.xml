<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028583866</ID>
<ANCIEN_ID>JG_L_2014_02_000000357117</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/38/CETATEXT000028583866.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 10/02/2014, 357117, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357117</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357117.20140210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 février 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la réforme de l'Etat, porte-parole du Gouvernement ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 10NT00776 du 29 décembre 2011 de la cour administrative d'appel de Nantes en tant que, par cet arrêt, la cour, a déchargé la société Bayi Finances de l'amende infligée à celle-ci sur le fondement de l'article 1734 bis du code général des impôts ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité de la société Alençon Poids Lourd Socal, appartenant au groupe fiscalement intégré dont la tête est sa société mère, la SAS Bayi Finances, l'administration fiscale a estimé que cette dernière, en cédant à la société Poids Lourd Socal les parts qu'elle détenait dans le capital d'une  autre société du groupe à un prix inférieur à sa valeur vénale, devait être regardée comme ayant commis un acte anormal de gestion au bénéfice de la société cessionnaire ; qu'ayant constaté que la SAS Bayi Finances n'avait pas porté cette subvention intra-groupe sur l'état joint à la déclaration du résultat d'ensemble de l'exercice 2003, l'administration a, sur le fondement de l'article 1734 bis du code général des impôts, mis à sa charge, au titre de cet exercice, une amende égale à 5 % des sommes omises ; que le ministre chargé du budget se pourvoit en cassation contre l'arrêt du 29 décembre 2011 de la cour administrative d'appel de Nantes en tant qu'il a déchargé la société Bayi Finances de l'amende qui lui a été infligée sur le fondement de l'article 1734 bis du code général des impôts et réformé en ce sens le jugement du  17 décembre 2009 du tribunal administratif de Caen ;  <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 223 B du code général des impôts, dans sa rédaction applicable à l'année d'imposition en litige : " Le résultat d'ensemble est déterminé par la société mère en faisant la somme algébrique des résultats de chacune des sociétés du groupe, déterminés dans les conditions de droit commun ou selon les modalités prévues à l'article 217 bis " ; que le sixième alinéa de ce même article prévoit : " L'abandon de créance ou la subvention directe ou indirecte consenti entre des sociétés du groupe n'est pas pris en compte pour la détermination du résultat d'ensemble. La société mère est tenue de joindre à la déclaration du résultat d'ensemble de chaque exercice un état des abandons de créance ou subventions consentis à compter du 1er janvier 1992. Un décret fixe le contenu de ces obligations déclaratives. " ; qu'aux termes de l'article 1734 bis du même code, alors en vigueur et dont les dispositions ont été reprises au I de l'article 1763 de ce code :  " Les contribuables qui n'ont pas produit à l'appui de leur déclaration de résultats de l'exercice (...) l'état des abandons de créances et subventions prévu au sixième alinéa de l'article 223 B (...) sont punis d'une amende égale à 5 % des sommes ne figurant pas sur (...) l'état du seul exercice au titre duquel l'infraction est mise en évidence . / Ce taux est ramené à 1 % lorsque les sommes correspondantes sont réellement déductibles. " ; qu'aux termes de l'article 46 quater-0 ZL de l'annexe III à ce code : " La déclaration du résultat d'ensemble visée à l'article 223 Q du code général des impôts comprend les éléments nécessaires à la détermination et au contrôle de ce résultat. La société mère doit joindre à cette déclaration : 1. Un état des subventions directes ou indirectes et des abandons de créances consentis ou reçus par chacune des sociétés membres du groupe, à compter du 1er janvier 1992, indiquant la dénomination des sociétés concernées ainsi que la nature et le montant de ces subventions ou abandons (...) " ; <br/>
<br/>
              3. Considérant que l'option pour le régime dit de " l'intégration fiscale " ne dispense pas chacune des sociétés du groupe fiscal intégré de déterminer son résultat dans les conditions de droit commun, ainsi que le prévoit le premier alinéa de l'article 223 B du code général des impôts, sous la seule réserve des dérogations expressément autorisées par les dispositions propres à ce régime d'exception ; qu'aucune de ces dispositions n'autorise une société membre du groupe à déclarer selon des règles différentes des règles de droit commun un abandon de créance ou une subvention qu'elle a consenti ou dont elle a bénéficié ; que la neutralisation d'un tel abandon de créance ou d'une subvention consenti entre sociétés du même groupe est effectuée, conformément aux dispositions du sixième alinéa de ce même article 223 B cité ci-dessus, pour la détermination du résultat d'ensemble, après l'établissement des résultats individuels des sociétés membres du groupe ; que cette même disposition prescrit de joindre à la déclaration du résultat d'ensemble un état des abandons de créances ou subventions consenties entre sociétés du groupe ; que cette obligation déclarative a pour objet de permettre à l'administration fiscale de suivre les mouvements financiers à l'intérieur du groupe quand bien même ces mouvements seraient sans incidence tant sur le résultat des sociétés du groupe déterminé dans les conditions de droit commun que sur le résultat d'ensemble du groupe ; <br/>
<br/>
              4. Considérant, dès lors, qu'en jugeant que l'application à la société Bayi Finances de l'amende de 5 % prévue à l'article 1734 bis du code général des impôts était la conséquence directe de la rectification des résultats de la société cessionnaire bénéficiaire de la subvention indirecte, non déclarée, correspondant à la minoration du prix des titres et que, par conséquent, l'irrégularité de la procédure de la rectification des résultats de la société cessionnaire entraînait la décharge non seulement du supplément d'impôt sur les sociétés à laquelle celle-ci avait été assujettie mais aussi de l'amende litigieuse qui en constituait l'accessoire, la cour a commis une erreur de droit ; que, par suite, le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il décharge la société Bayi Finances de l'amende qui lui a été infligée sur le fondement de l'article 1734 bis du code général des impôts ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 de l'arrêt de la cour administrative d'appel de Nantes du 29 décembre 2011 sont annulés. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société Bayi finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
