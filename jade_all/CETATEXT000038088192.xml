<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088192</ID>
<ANCIEN_ID>JG_L_2019_01_000000410544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/81/CETATEXT000038088192.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/01/2019, 410544, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:410544.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               La société de droit portugais Trust Paving Unipessoal LDA a demandé au tribunal administratif de Grenoble la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période du 1er janvier 2007 au 30 septembre 2010, des cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2007, 2008 et 2009 ainsi que des pénalités correspondantes. Par un jugement n° 1204266 du 19 mars 2015, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15LY01693 du 28 mars 2017, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 mai et 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, la société Trust Paving Unipessoal LDA demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
               - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de la société Trust Paving Unipessoal LDA ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société de droit portugais Trust Paving Unipessoal LDA, dont le capital est détenu par M. A..., fournit de la main d'oeuvre à la société A...Bâtiment, établie en France dont M. A...est également l'associé unique. Sur le fondement d'éléments recueillis dans le cadre de l'assistance internationale et de son droit de communication, l'administration fiscale a estimé que la société Trust Paving Unipessoal LDA disposait en France d'un établissement stable à partir duquel elle réalisait des opérations de manière occulte et, à la suite d'une vérification de comptabilité, elle a procédé à la taxation d'office des chiffres d'affaires et des bénéfices que cette société n'avait pas déclarés. La société a demandé au tribunal administratif de Grenoble la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période du 1er janvier 2007 au 30 septembre 2010, des cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos de 2007 à 2009 ainsi que de la pénalité de 80 % prévue par le c du 1 de l'article 1728 du code général des impôts en cas de découverte d'une activité occulte. Elle demande l'annulation de l'arrêt du 28 mars 2017 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle a formé contre le jugement ayant rejeté sa demande. <br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne la procédure d'imposition :<br/>
<br/>
              2. D'une part, l'administration fiscale est en droit à tout moment de la procédure de se prévaloir de la situation de taxation d'office ou d'évaluation d'office du contribuable. Lorsque la situation d'imposition d'office d'un contribuable n'a pas été révélée par la vérification de comptabilité dont celui-ci a fait l'objet, les irrégularités qui ont pu entacher cette vérification sont sans influence sur la régularité de la procédure d'imposition. D'autre part, si l'administration a recouru à la procédure d'évaluation d'office, elle est en droit, à tout moment de la procédure, de substituer une base légale à celle qui a été primitivement retenue dès lors que cette substitution ne prive pas le contribuable de garanties attachées à la procédure d'imposition.<br/>
<br/>
              3. En premier lieu, après avoir relevé que l'administration fiscale avait, au stade de la réclamation contentieuse, substitué la procédure de taxation d'office prévue aux 2° et 3° de l'article L. 66 du livre des procédures fiscales, faute pour la société d'avoir souscrit en France dans le délai prévu les déclarations fiscales lui incombant malgré l'envoi préalable de mises en demeure en application de l'article L. 68 de ce livre, à la procédure d'évaluation d'office prévue par les dispositions de l'article L. 74 du même livre, la cour n'a pas commis d'erreur de droit ni dénaturé les faits qui lui étaient soumis en jugeant que la situation de taxation d'office de la société Trust Paving Unipessoal LDA n'avait pas été révélée par la vérification de comptabilité dont elle avait fait l'objet.  <br/>
<br/>
              4. En second lieu, la cour n'a pas commis d'erreur de droit ni dénaturé les faits qui lui étaient soumis en se fondant sur la situation de taxation d'office de la société exposée au point précédent, pour juger que la circonstance que l'administration fiscale, qui n'était pas tenue de recourir à la procédure contradictoire, avait pris en compte les observations de la société Trust Paving Unipessoal LDA, ne signifiait pas qu'elle avait renoncé à la procédure d'imposition d'office et que, par suite, les irrégularités qui avaient pu entacher la vérification de comptabilité dont elle avait fait l'objet étaient sans influence sur la régularité de la procédure. <br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne les bases de l'impôt sur les sociétés :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que, pour déterminer le bénéfice imposable à l'impôt sur les sociétés de la société Trust Paving Unipessoal LDA, l'administration fiscale a reconstitué le chiffre d'affaires réalisé pour chacun des exercices vérifiés à partir des relevés des factures de prestations de sous-traitance comptabilisées par la société A...Bâtiment. Estimant que la première de ces sociétés n'avait pas facturé de marge à la seconde, elle a majoré le montant de ces factures d'un taux de marge calculé à partir de celui de la société A...Bâtiment. Ce chiffre d'affaires reconstitué a, ensuite, été diminué des charges d'exploitation estimées au montant de ces mêmes factures ainsi que des dépenses salariales estimées sur la base du ratio entre le chiffre d'affaires reconstitué et le coefficient de rendement retenu. <br/>
<br/>
              6. Pour écarter le moyen invoqué devant elle par la société Trust Paving Unipessoal LDA, tiré de ce que cette méthode de reconstitution était radicalement viciée faute d'avoir tenu compte de ses éléments réels d'exploitation tels qu'ils apparaissaient dans ses déclarations de résultats effectuées au Portugal et dans les comptes de résultat correspondants, la cour s'est bornée à relever, en premier lieu, que la vérification de comptabilité concernait uniquement l'établissement français de la société pour lequel aucune pièce comptable n'avait été produite au cours du contrôle, en deuxième lieu, qu'il résultait de l'instruction et n'était pas contesté que les deux sociétés avaient le même objet social et exerçaient dans le même domaine d'activité et, en troisième lieu, que la production, postérieurement aux opérations de contrôle, des liasses fiscales, du journal de comptes et du détail des comptes des bilans des trois exercices vérifiés ne suffisait pas à démontrer que le montant des résultats imposables en France à retenir au titre des trois exercices vérifiés devrait être ramené aux montants déclarés par la société aux autorités fiscales portugaises, qui n'étaient pas justifiés.<br/>
<br/>
              7. Toutefois, il ressort des pièces du dossier soumis aux juges du fond, en premier lieu, que la société Trust Paving Unipessoal LDA réalisait la totalité de son activité avec la société A...Bâtiment, en deuxième lieu, que l'activité de cette dernière société ne consistait pas en la mise à disposition de personnel mais en la réalisation, pour le compte de ses clients, et grâce au personnel mis à sa disposition ainsi qu'à ses propres matériaux et équipements, des chantiers de pose de pierre et de dalles extérieures, en troisième lieu, que l'administration n'a pas contesté devant les juges du fond le caractère probant des éléments de comptabilité, qui ont été produits après les opérations de contrôle mais qui correspondent à ceux qui ont fait l'objet de déclarations de résultats réalisées au Portugal. Ainsi que le soutenait la société requérante, ces éléments de comptabilité faisaient apparaître une légère marge entre, d'une part, les montants facturés pour la mise à disposition de personnel et, d'autre part, les salaires et charges sociales correspondants, pour un bénéfice de 11 225 et de 16 146 euros au titre des exercices clos en 2007 et 2008 et une perte de 9 503 euros au titre de 2009, à comparer avec les bénéfices de 55 792, de 106 303 et de 51 113 euros calculés par l'administration. Dès lors, eu égard aux éléments réels d'exploitation produits par la société requérante, celle-ci est fondée à soutenir qu'en estimant que la méthode retenue par l'administration n'avait pas un caractère radicalement vicié, la cour a commis une erreur de qualification juridique.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne les pénalités en matière de taxe sur la valeur ajoutée :<br/>
<br/>
              8. Si l'administration fiscale est en droit, à tout moment de la procédure contentieuse, de justifier d'une pénalité en en modifiant le fondement juridique, c'est à la double condition que la substitution de base légale ainsi opérée ne prive le contribuable d'aucune des garanties de procédure prévues par la loi et que l'administration invoque, au soutien de la demande de substitution de base légale, des faits qu'elle avait retenus pour motiver la pénalité initialement appliquée.  <br/>
<br/>
              9. Il ressort des pièces du dossier soumis aux juges du fond que, si l'administration a substitué, en réponse à la réclamation préalable de la société requérante, la pénalité prévue par le c du 1 de l'article 1728 du code général des impôts pour découverte d'une activité occulte à la pénalité pour opposition à contrôle fiscal prévue par le a de l'article 1732 du même code, qui avait été retenue dans la proposition de rectification, cette dernière faisait déjà référence aux dissimulations de recettes et de bénéfices qui pouvaient légalement justifier la pénalité finalement retenue. Par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en ne relevant pas d'office le caractère irrégulier de la substitution de base légale opérée par l'administration compte tenu des faits initialement retenus ne peut qu'être écarté.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la société Trust Paving Unipessoal LDA est seulement fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il statue sur les cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos de 2007 à 2009 ainsi sur les pénalités correspondantes.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la société Trust Paving Unipessoal LDA au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 mars 2017 de la cour administrative d'appel de Lyon est annulé en tant qu'il statue sur les cotisations d'impôt sur les sociétés auxquelles la société Trust Paving Unipessoal LDA a été assujettie au titre des exercices clos en 2007, 2008 et 2009 ainsi que sur les pénalités correspondantes.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera à la société Trust Paving Unipessoal LDA la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions du pourvoi de la société Trust Paving Unipessoal LDA est rejeté. <br/>
Article 5 : La présente décision sera notifiée à la société Trust Paving Unipessoal LDA et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
