<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037509300</ID>
<ANCIEN_ID>JG_L_2018_10_000000418023</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/93/CETATEXT000037509300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 19/10/2018, 418023, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418023</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Laure Denis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418023.20181019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une ordonnance n° 1719207 du 1er février 2018, la présidente du tribunal administratif de Paris a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire complémentaire de Mme B...A...enregistrés au greffe de ce tribunal les 12 décembre 2017 et 23 janvier 2018. Par cette requête et ce mémoire, ainsi que par un mémoire en réplique enregistré au secrétariat du contentieux du Conseil d'État le 19 août 2018, Mme A...demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 octobre 2017 par laquelle la garde des sceaux, ministre de la justice a rejeté sa candidature aux fonctions de magistrats exerçant à titre temporaire ;<br/>
<br/>
              2°) d'enjoindre à la garde des sceaux, ministre de la justice de la nommer aux fonctions de magistrats exerçant à titre temporaire et de lui faire suivre la formation probatoire destinée à ces magistrats. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le décret n° 93-21 du 7 janvier 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Laure Denis, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              - Et après en avoir délibéré hors de la présence du rapporteur public. <br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 septembre 2018, présentée par Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'article 41-10 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature dispose que : " Peuvent être nommées magistrats exerçant à titre temporaire, pour exercer des fonctions de juge d'instance, d'assesseur dans les formations collégiales des tribunaux de grande instance, de juge du tribunal de police ou de juge chargé de valider les compositions pénales, les personnes âgées d'au moins trente-cinq ans que leur compétence et leur expérience qualifient particulièrement pour exercer ces fonctions. / Elles doivent soit remplir les conditions prévues au 1°, 2° ou 3° de l'article 22, soit être membre ou ancien membre des professions libérales juridiques et judiciaires soumises à un statut législatif ou réglementaire ou dont le titre est protégé et justifier de cinq années au moins d'exercice professionnel(...) ". Aux termes de l'article 22 de la même ordonnance : " Peuvent être nommés directement aux fonctions du second grade de la hiérarchie judiciaire, à condition d'être âgés de trente-cinq ans au moins : / 1° Les personnes remplissant les conditions prévues à l'article 16 et justifiant de sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires (...) ". L'article 16 de la même ordonnance dispose enfin que : " Les candidats à l'auditorat doivent : / 1° Etre titulaires d'un diplôme sanctionnant une formation d'une durée au moins égale à quatre années d'études après le baccalauréat ou justifiant d'une qualification reconnue au moins équivalente dans des conditions fixées par décret en Conseil d'Etat. (...) ". <br/>
<br/>
              2. Par une décision du 12 octobre 2017, la garde des sceaux, ministre de la justice, a rejeté la candidature présentée par Mme A...sur le fondement des dispositions rappelées ci-dessus au motif que l'intéressée ne remplissait pas la condition d'expérience requise pour l'exercice des fonctions judiciaires. Mme A...demande l'annulation pour excès de pouvoir de cette décision. <br/>
<br/>
              3. En premier lieu, si, en vertu de l'article L. 211-2 du code des relations entre le public et l'administration doivent être motivées les décisions qui refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir, tel n'est pas le cas de la décision attaquée. Aucune autre disposition et aucun principe n'imposaient une telle motivation. Le moyen, d'ailleurs non étayé, tiré de ce que cette décision ne serait pas régulièrement motivée ne peut, par suite, qu'être écarté. <br/>
<br/>
              4. En deuxième lieu, il ne ressort pas des pièces du dossier que les magistrats qui ont procédé à l'audition de l'intéressée auraient adopté une attitude partiale à son encontre.  <br/>
<br/>
              5. En troisième lieu, la décision attaquée est fondée sur le motif tiré de ce que Mme A...ne remplit pas la condition d'expérience requise pour l'exercice des fonctions judiciaires. Cette dernière ne peut, dès lors, utilement exciper de ce que l'administration ne pouvait légalement exiger des candidats aux fonctions de magistrat exerçant à titre temporaire qu'ils produisent une liste de personnalités attestant de la qualité de leur candidature.<br/>
<br/>
              7. En dernier lieu, il résulte des dispositions rappelées au point 1 que le recrutement de magistrats exerçant à titre temporaire est subordonné à la condition que, outre les diplômes requis, les intéressés justifient de sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires, ce qui implique nécessairement qu'une partie substantielle de cette expérience relève du domaine juridique. Si Mme A...soutient qu'elle est titulaire d'une maîtrise en droit mention " carrières judiciaires ", d'un diplôme d'études approfondies de droit des affaires et d'une licence d'anglais, qu'elle a suivi pendant un an un enseignement à l'école de formation du barreau de Paris et qu'elle justifie de plus de dix ans d'expérience dans le domaine social, en particulier en centre d'appel et en qualité d'aide familiale, il ne ressort pas des pièces du dossier que la garde des sceaux, ministre de la justice, aurait commis une erreur manifeste d'appréciation en estimant que l'intéressée, qui n'a fait état que de courtes périodes d'activités relevant du domaine juridique, ne remplissait pas la condition d'expérience requise pour l'exercice des fonctions judiciaires. En se prononçant ainsi, la garde des sceaux, ministre de la justice n'a pas fait une inexacte interprétation des dispositions de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature. <br/>
<br/>
              8. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Ses conclusions à fin d'injonction ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
