<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028627647</ID>
<ANCIEN_ID>JG_L_2014_02_000000366707</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/62/76/CETATEXT000028627647.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 19/02/2014, 366707</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366707</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Véronique Rigal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366707.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le numéro n° 366707, le pourvoi du ministre de l'économie et des finances, enregistré le 11 mars 2013 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat d'annuler le jugement n° 1115974/6-2 du 8 janvier 2013 par lequel le tribunal administratif de Paris a annulé sa décision implicite de refus de communiquer les documents et les rapports d'enquête demandés par la société Speed Rabbit Pizza et lui a enjoint de réexaminer cette demande, eu égard aux motifs de sa décision, dans un délai de deux mois à compter de sa notification ;<br/>
<br/>
<br/>
              Vu 2°, sous le numéro n° 366708, le recours du ministre de l'économie et des finances, enregistré le 11 mars 2013 ; le ministre demande au Conseil d'Etat d'ordonner le sursis à exécution du jugement n° 1115974/6-2 du 8 janvier 2013 du tribunal administratif de Paris ; <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 3°, sous le numéro n° 371215, le pourvoi du ministre de l'économie et des finances, enregistré le 13 août 2013 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat  d'annuler le jugement n° 1104765 du 12 juillet 2013 par lequel le tribunal administratif de Toulouse a annulé la décision implicite de refus du directeur des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Midi-Pyrénées de communiquer les documents relatifs à l'enquête menée sur les pratiques relatives au respect des délais de paiement entre franchiseurs et franchisés dans le secteur de la restauration livrée et lui a enjoint de réexaminer cette demande dans un délai de deux mois à compter de sa notification ;<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 4°, sous le numéro n° 371216, le recours du ministre de l'économie et des finances, enregistré le 13 août 2013 ; le ministre  demande au Conseil d'Etat d'ordonner le sursis à exécution du jugement n° 1104765 du 12 juillet 2013 du tribunal administratif de Toulouse par les mêmes moyens que ceux qui ont été visés sous le n° 366708 ;<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Véronique Rigal, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société Speed Rabbit Pizza ;<br/>
<br/>
<br/>
<br/>Sur les pourvois en cassation :<br/>
<br/>
              1. Considérant que les pourvois du ministre de l'économie et des finances visés ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que la direction générale de la concurrence, de la consommation et de la répression des fraudes (DGCCRF) a programmé une enquête nationale en vue de contrôler les pratiques en matière de délais de paiement dans le secteur de la restauration livrée ; que la société Speed Rabbit Pizza a demandé la communication des pièces émises ou rassemblées au cours de l'enquête au préfet du Haut-Rhin, le 13 avril 2011, qui a transmis sa demande aux services centraux de cette direction, ainsi qu'au directeur départemental des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Midi-Pyrénées, les 5 et 28 avril 2011 ; qu'après que la Commission d'accès aux documents administratifs s'est déclarée incompétente, la société a demandé l'annulation pour excès de pouvoir des décisions implicites de rejet nées du silence gardé par l'administration devant les tribunaux administratifs de Paris et de Toulouse, qui les ont annulées et ont enjoint aux autorités administratives de réexaminer les demandes de communication dans un délai de deux mois ; que le ministre de l'économie et des finances se pourvoit en cassation contre ces jugements ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal : " Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, directives, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions et décisions. " ; <br/>
<br/>
              4. Considérant que, alors même qu'ils ont été élaborés par des services administratifs en vertu des pouvoirs d'investigation qui leur sont conférés, des procès verbaux et rapports d'enquête établis en application de l'article L. 450-2 du code de commerce ne constituent pas, dans la mesure où ils constatent des pratiques qui ne sont susceptibles d'être sanctionnées que par une décision juridictionnelle, des documents administratifs ; qu'ainsi, en jugeant que la circonstance que les documents litigieux constataient des infractions aux règles relatives aux délais de paiement prévues par l'article L. 443-1 du code de commerce, susceptibles seulement de sanctions prononcées par le juge pénal, ne faisait pas obstacle à leur qualification, dans cette mesure, de documents administratifs, les tribunaux administratifs de Paris et de Toulouse ont commis une erreur de droit et inexactement qualifié les faits qui leur étaient soumis ; qu'il y a donc lieu, sans qu'il soit besoin d'examiner les autres moyens des pourvois, d'annuler les jugements attaqués ;  <br/>
<br/>
              Sur les recours du ministre aux fins de sursis à exécution :<br/>
<br/>
              5. Considérant qu'aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond " ; qu'il résulte de ce qui précède que les jugements du 8 janvier 2013 du tribunal administratif de Paris et du 12 juillet 2013 du tribunal administratif de Toulouse sont annulés ; que, par suite, les conclusions aux fin de sursis de ces jugements sont devenues sans objet ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant ces dispositions font obstacle à ce que soit mise à la charge de l'Etat qui n'est pas, dans les présentes instances, la partie perdante, les sommes que demande la société Speed Rabbit Pizza au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 8 janvier 2013 et le jugement du tribunal administratif de Toulouse du 12 juillet 2013 sont annulés.<br/>
Article 2 : Les affaires enregistrées sous les n° 366707 et n° 371215 sont renvoyées respectivement devant le tribunal administratif de Paris et devant le tribunal administratif de Toulouse.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les recours du ministre de l'économie et des finances aux fins de sursis à exécution des jugements attaqués.<br/>
Article 4 : Les conclusions présentées par la société Speed Rabbit Pizza  au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société Speed Rabbit Pizza.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-01 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. NOTION DE DOCUMENT ADMINISTRATIF. - EXCLUSION - PROCÈS-VERBAUX ET RAPPORTS D'ENQUÊTE ÉTABLIS EN APPLICATION DE L'ARTICLE L. 450-2 DU CODE DE COMMERCE.
</SCT>
<ANA ID="9A"> 26-06-01-02-01 Alors même qu'ils ont été élaborés par des services administratifs en vertu des pouvoirs d'investigation qui leur sont conférés, des procès-verbaux et rapports d'enquête établis en application de l'article L. 450-2 du code de commerce ne constituent pas, dans la mesure où ils constatent des pratiques qui ne sont susceptibles d'être sanctionnées que par une décision juridictionnelle, des documents administratifs.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
