<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151221</ID>
<ANCIEN_ID>JG_L_2019_02_000000425521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151221.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 20/02/2019, 425521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425521.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 1er juin 2018, M. A...B...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir l'arrêté qui lui a été notifié le 3 avril 2018 par lequel la garde des sceaux, ministre de la justice, l'a exclu temporairement de ses fonctions pour une durée de trois mois et d'enjoindre au garde des sceaux, ministre de la justice, de le réintégrer dans ses fonctions et d'effacer la sanction litigieuse de son dossier administratif ainsi que de tout autre fichier, sous astreinte de 200 euros par jour de retard. <br/>
<br/>
              Par un mémoire, enregistré le 4 juin 2018 au greffe du tribunal administratif de Melun, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B... a soulevé une question prioritaire de constitutionnalité à l'appui de cette demande. <br/>
<br/>
              Par une ordonnance n° 1804476 du 16 novembre 2018, enregistrée le 20 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la présidente de la 6ème chambre du tribunal administratif de Melun, avant qu'il soit statué sur la demande de M. B..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 3 de l'ordonnance n° 58-696 du 6 août 1958.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - la loi n° 58-520 du 3 juin 1958 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 92-125 du 6 février 1992 ;<br/>
              - l'ordonnance n° 58-696 du 6 août 1958<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 février 2019, présentée par la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 3 de l'ordonnance du 6 août 1958 relative au statut spécial des fonctionnaires des services déconcentrés de l'administration pénitentiaire, prise en vertu de la loi du 3 juin 1958 relative aux pleins pouvoirs, " maintenue en vigueur " par l'article 90 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat et modifiée par la loi du 6 février 1992 relative à l'administration territoriale de la République : " Toute cessation concertée du service, tout acte collectif d'indiscipline caractérisée de la part des personnels des services extérieurs de l'administration pénitentiaire est interdit. Ces faits, lorsqu'ils sont susceptibles de porter atteinte à l'ordre public, pourront être sanctionnés en dehors des garanties disciplinaires. "<br/>
<br/>
              3. Les dispositions de l'article 3 de l'ordonnance du 6 août 1958, relatives à la procédure à mettre en oeuvre pour sanctionner un fonctionnaire des services pénitentiaires pour cessation concertée du service, sont applicables au litige par lequel M. B...demande l'annulation de la sanction prononcée à son encontre par l'arrêté du garde des sceaux, ministre de la justice, qui lui a été notifiée le 3 avril 2018, et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce que ces dispositions portent atteinte au principe des droits de la défense, principe garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, soulève une question sérieuse. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions de l'article 3 de l'ordonnance du 6 août 1958 est renvoyée au Conseil constitutionnel. <br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Melun.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
