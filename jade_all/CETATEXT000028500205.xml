<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028500205</ID>
<ANCIEN_ID>JG_L_2014_01_000000373220</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/50/02/CETATEXT000028500205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/01/2014, 373220</TITRE>
<DATE_DEC>2014-01-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373220</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373220.20140120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12BX03156 du 5 novembre 2013, enregistrée le 8 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Bordeaux, avant qu'il soit statué sur l'appel de M. A...B...contre le jugement n° 1000302 du 14 juin 2012 par lequel le tribunal administratif de Poitiers a rejeté sa demande tendant à l'annulation, d'une part, de l'arrêté du 11 décembre 2009 par lequel le préfet de la région Poitou-Charente, préfet de la Vienne, ne s'est pas opposé aux travaux déclarés au titre de l'article L. 214-3 du code de l'environnement par la SCI " les Genêts " en vue de la réalisation de remblai dans le lit majeur du cours d'eau de l'Oure, au lieu-dit "Les Genêts" sur le territoire de la commune de Saint-Genest-d'Ambière, et, d'autre part, de l'arrêté du 22 janvier 2004 par lequel le maire de Saint-Genest-d'Ambière a autorisé des travaux de remblaiement, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance      n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du II de l'article L. 214-3 du  code de l'environnement ;<br/>
<br/>
              Vu le mémoire, enregistré le 4 octobre 2013 au greffe de la cour administrative d'appel de Bordeaux, présenté pour M. A...B..., demeurant..., en application de l'article 23-1 de la même ordonnance ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, Auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 214-1 du code de l'environnement : " Sont soumis aux dispositions des articles L. 214-2 à L. 214-6 les installations ne figurant pas à la nomenclature des installations classées, les ouvrages, travaux et activités réalisés à des fins non domestiques par toute personne physique ou morale, publique ou privée, et entraînant des prélèvements sur les eaux superficielles ou souterraines, restitués ou non, une modification du niveau ou du mode d'écoulement des eaux, la destruction de frayères, de zones de croissance ou d'alimentation de la faune piscicole ou des déversements, écoulements, rejets ou dépôts directs ou indirects, chroniques ou épisodiques, même non polluants " ; qu'aux termes du premier alinéa de l'article L. 214-2 du même code : " Les installations, ouvrages travaux et activités visés à l'article L. 214-1 sont définis dans une nomenclature, établie par décret en Conseil d'Etat après avis du Comité national de l'eau, et soumis à autorisation ou à déclaration suivant les dangers qu'ils présentent et la gravité de leurs effets sur la ressource en eau et les écosystèmes aquatiques compte tenu notamment de l'existence des zones et périmètres institués pour la protection de l'eau et des milieux aquatiques " ; qu'aux termes de l'article L. 214-3 de ce code : " I. Sont soumis à autorisation (...) les installations, ouvrages, travaux et activités susceptibles de présenter des dangers pour la santé et la sécurité publique, de nuire au libre écoulement des eaux, de réduire la ressource en eau, d'accroître notablement le risque d'inondation, de porter gravement atteinte à la qualité ou à la diversité du milieu aquatique, notamment aux peuplements piscicoles. (...). II. Sont soumis à déclaration les installations, ouvrages, travaux et activités qui, n'étant pas susceptibles de présenter de tels dangers, doivent néanmoins respecter les prescriptions édictées en application des articles L. 211-2 et L. 211-3. / Dans un délai fixé par décret en Conseil d'Etat, l'autorité administrative peut s'opposer à l'opération projetée s'il apparaît qu'elle est incompatible avec les dispositions du schéma directeur d'aménagement et de gestion des eaux ou du schéma d'aménagement et de gestion des eaux, ou porte aux intérêts mentionnés à l'article L. 211-1 une atteinte d'une gravité telle qu'aucune prescription ne permettrait d'y remédier. Les travaux ne peuvent commencer avant l'expiration de ce délai. / Si le respect des intérêts mentionnés à l'article L. 211-1 n'est pas assuré par l'exécution des prescriptions édictées en application des articles L. 211-2 et L. 211-3, l'autorité administrative peut, à tout moment, imposer par arrêté toutes prescriptions particulières nécessaires " ; <br/>
<br/>
              3. Considérant que M. B...soutient que les dispositions du II de l'article          L. 214-3 du code de l'environnement méconnaissent l'article 7 de la Charte de l'environnement et l'article 34 de la Constitution, faute de prévoir une procédure d'information et de participation du public au stade de l'instruction des dossiers soumis au régime de la déclaration ; que, toutefois, il résulte des dispositions contestées que les installations, ouvrages, travaux et activités en cause ne peuvent être soumis à déclaration en application du II de l'article L. 214-3 que s'ils ne présentent pas les dangers énumérés au I de cet article ; que, dans l'hypothèse où les installations, ouvrages, travaux et activités déclarés seraient incompatibles avec les dispositions du schéma directeur d'aménagement et de gestion des eaux ou du schéma d'aménagement et de gestion des eaux ou porteraient aux intérêts mentionnés à l'article L. 211-1 du code de l'environnement une atteinte telle qu'aucune prescription ne permettrait d'y remédier, il appartiendrait à l'autorité administrative compétente de s'y opposer ; que, dès lors, la décision de non opposition à une déclaration présentée au titre du II de l'article L. 214-3 ne constitue pas une décision ayant une incidence significative sur l'environnement et n'est pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la question de la conformité des dispositions contestées aux droits et libertés garantis par la Constitution, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu, par suite, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à la commune de Saint-Gesnest-d'Ambière, à la SCI " les Genêts " et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre des affaires sociales et de la santé et à la cour administrative d'appel de Bordeaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-02-01 EAUX. OUVRAGES. ÉTABLISSEMENT DES OUVRAGES. - RÉGIME DE DÉCLARATION - OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE S'OPPOSER AUX INSTALLATIONS ET OUVRAGES INCOMPATIBLES AVEC LE SDAGE OU LE SAGE OU PORTANT ATTEINTE AUX INTÉRÊTS MENTIONNÉS À L'ARTICLE L. 211-1 DU CODE DE L'ENVIRONNEMENT - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">27-03 EAUX. TRAVAUX. - RÉGIME DE DÉCLARATION - OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE S'OPPOSER AUX TRAVAUX ET ACTIVITÉS INCOMPATIBLES AVEC LE SDAGE OU LE SAGE OU PORTANT ATTEINTE AUX INTÉRÊTS MENTIONNÉS À L'ARTICLE L. 211-1 DU CODE DE L'ENVIRONNEMENT - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">27-05 EAUX. GESTION DE LA RESSOURCE EN EAU. - RÉGIME DE DÉCLARATION - OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE S'OPPOSER AUX INSTALLATIONS, OUVRAGES, TRAVAUX ET ACTIVITÉS INCOMPATIBLES AVEC LE SDAGE OU LE SAGE OU PORTANT ATTEINTE AUX INTÉRÊTS MENTIONNÉS À L'ARTICLE L. 211-1 DU CODE DE L'ENVIRONNEMENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 27-02-01 L'autorité administrative doit s'opposer aux installations, ouvrages, travaux et activités déclarés au titre du II de l'article L. 214-3 du code de l'environnement s'ils sont incompatibles avec les dispositions du schéma directeur d'aménagement et de gestion des eaux (SDAGE) ou du schéma d'aménagement et de gestion des eaux (SAGE) ou porteraient aux intérêts mentionnés à l'article L. 211-1 du même code une atteinte telle qu'aucune prescription ne permettrait d'y remédier.</ANA>
<ANA ID="9B"> 27-03 L'autorité administrative doit s'opposer aux installations, ouvrages, travaux et activités déclarés au titre du II de l'article L. 214-3 du code de l'environnement s'ils sont incompatibles avec les dispositions du schéma directeur d'aménagement et de gestion des eaux (SDAGE) ou du schéma d'aménagement et de gestion des eaux (SAGE) ou porteraient aux intérêts mentionnés à l'article L. 211-1 du même code une atteinte telle qu'aucune prescription ne permettrait d'y remédier.</ANA>
<ANA ID="9C"> 27-05 L'autorité administrative doit s'opposer aux installations, ouvrages, travaux et activités déclarés au titre du II de l'article L. 214-3 du code de l'environnement s'ils sont incompatibles avec les dispositions du schéma directeur d'aménagement et de gestion des eaux (SDAGE) ou du schéma d'aménagement et de gestion des eaux (SAGE) ou porteraient aux intérêts mentionnés à l'article L. 211-1 du même code une atteinte telle qu'aucune prescription ne permettrait d'y remédier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
