<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030322695</ID>
<ANCIEN_ID>JG_L_2015_03_000000359020</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/32/26/CETATEXT000030322695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 06/03/2015, 359020, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359020</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:359020.20150306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 30 avril 2012 et 26 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération nationale des producteurs et élaborateurs de Crémant ; la fédération demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 26 octobre 2011 relatif à l'indication géographique protégée " Sable de Camargue " ainsi que la décision implicite de rejet du recours administratif dirigé contre cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 février 2015, présentée pour le ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement, et pour l'INAO, qui reprennent les conclusions de leur précédent mémoire et les mêmes moyens ; <br/>
<br/>
              Vu le règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques pour certains produits de ce secteur (règlement "OCM unique") ;<br/>
<br/>
              Vu le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Fédération nationale des producteurs et élaborateurs de Crémant  et à la SCP Didier, Pinet, avocat du ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du second alinéa de l'article R. 641-17 du code rural et de la pêche maritime : " L'arrêté homologuant le cahier des charges d'une indication géographique protégée relevant du règlement (CE) n° 1234/ 2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement "OCM unique ") est pris par les ministres chargés, respectivement, de l'agriculture, de la consommation et du budget. Il est fait mention de ces arrêtés au Journal officiel de la République française " ;<br/>
<br/>
              2. Considérant que pour le secteur vitivinicole, le b) du paragraphe 1 de l'article 118 ter du règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 portant organisation commune des marchés dans le secteur agricole et dispositions spécifiques en ce qui concerne certains produits de ce secteur (règlement " OCM unique ") définit l'indication géographique protégée comme : "  une indication renvoyant à une région, à un lieu déterminé ou, dans des cas exceptionnels, à un pays, qui sert à désigner un produit (...) :/ i) possédant une qualité, une réputation ou d'autres caractéristiques particulières attribuables à cette origine géographique (...) " ; que selon le 2 de l'article 118 quater du même règlement : " Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique./ Il comporte au minimum les éléments suivants:/ (...) g) les éléments qui corroborent le lien visé (...) à l'article 118 ter, paragraphe 1, point b) i) " ; qu'enfin, l'article 7 du règlement (CE) n°607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole dispose que : " 1. Les éléments qui corroborent le lien géographique (...) expliquent dans quelle mesure les caractéristiques de la zone géographique délimitée influent sur le produit final (...) 3. Pour une indication géographique, le cahier des charges contient : a) des informations détaillées sur la zone géographique contribuant au lien ; / b) des informations détaillées sur la qualité, la réputation ou d'autres caractéristiques spécifiques du produit découlant de son origine géographique ; / c) une description de l'interaction causale entre les éléments visés au point a) et ceux visés au point b) ./ 4. Pour une indication géographique, le cahier des charges précise si l'indication se fonde sur une qualité ou une réputation spécifique ou sur d'autres caractéristiques liées à l'origine géographique " ;<br/>
<br/>
              3. Considérant qu'il résulte clairement de ces dispositions que l'homologation d'un cahier des charges d'une indication géographique protégée, qui n'est pas une simple indication de provenance géographique, ne peut légalement intervenir que si ce cahier précise les éléments qui permettent d'attribuer à une origine géographique déterminée une qualité, une réputation ou d'autres caractéristiques particulières du produit qui fait l'objet de l'indication et met en lumière de manière circonstanciée le lien géographique et l'interaction causale entre la zone géographique et la qualité, la réputation ou d'autres caractéristiques du produit ; qu'il découle en outre nécessairement de ces mêmes dispositions qu'elles ne permettent de reconnaître un lien avec une origine géographique que pour une production existante, attestée dans la zone géographique à la date de l'homologation et depuis un temps suffisant pour établir ce lien ; qu'enfin, celui-ci doit être établi pour un produit déterminé et ne peut donc procéder d'une analogie avec un autre produit, même voisin ;<br/>
<br/>
              4. Considérant que, par l'arrêté attaqué du 26 octobre 2011, les ministres compétents ont homologué le nouveau cahier des charges de l'indication géographique protégée " Sable de Camargue ", proposé par l'Institut national de l'origine et de la qualité ; que si la Fédération nationale des producteurs et élaborateurs de Crémant demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté, il ressort des termes de sa requête et notamment des moyens développés qu'elle entend attaquer cet arrêté en tant seulement qu'il élargit aux " vins mousseux de qualité rouges, rosés et blancs" la possibilité de se prévaloir de l'indication géographique protégée " Sable de Camargue ", auparavant réservée aux seuls vins tranquilles par la précédente dénomination " Vin de pays des Sables du Golfe du Lion " à laquelle s'est substituée l'indication géographique litigieuse ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que l'antériorité de la production de vins mousseux de qualité, blancs et rosés, dans la zone géographique de l'indication géographique protégée " Sable de Camargue", qui correspond à celle de l'ancienne dénomination " Vin de pays des Sables du Golfe du Lion ", est établie, de manière continue, depuis le milieu des années 1970 ; que, toutefois, le cahier des charges litigieux ne comporte pas d'éléments attestant de l'existence d'une interaction causale entre la zone géographique concernée et la qualité, la réputation ou d'autres caractéristiques des vins mousseux en cause ; que, dans ces conditions, les ministres ont entaché leur arrêté d'une erreur manifeste d'appréciation en estimant que l'existence d'un lien géographique pouvait être établie entre l'aire géographique de l'indication " Sable de Camargue" et la production de " vins mousseux de qualité, rouges, rosés et blancs " ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, la Fédération nationale des producteurs et élaborateurs de Crémant est fondée à demander l'annulation de l'arrêté attaqué en tant qu'il homologue les dispositions du cahier des charges litigieux qui autorisent les vins mousseux de qualité issus des zones qu'il définit à se prévaloir de l'indication géographique protégée <br/>
" Sable de Camargue " ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de différer dans le temps les effets de cette annulation ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la Fédération nationale des producteurs et élaborateurs de Crémant, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les conclusions du ministre et de l'INAO tendant à ce qu'une somme de 1 000 euros soit mise à la charge de la Fédération nationale des producteurs et élaborateurs de Crémant qui n'est pas, dans la présente instance, la partie perdante, ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté interministériel du 26 octobre 2011 relatif à l'indication géographique protégée " Sable de Camargue " est annulé en tant qu'il homologue celles des dispositions du cahier des charges de cette indication géographique protégée relatives aux " vins mousseux de qualité, rouges, rosés et blancs ".<br/>
Article 2 : L'Etat versera une somme de 1 000 euros à la Fédération nationale des producteurs et élaborateurs de Crémant au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de l'Etat et de l'INAO au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la Fédération nationale des producteurs et élaborateurs de Crémant, au ministre de l'économie, du redressement productif et du numérique, au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement, et à l'Institut national de l'origine et de la qualité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
