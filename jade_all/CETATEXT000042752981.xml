<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042752981</ID>
<ANCIEN_ID>JG_L_2020_12_000000427890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/75/29/CETATEXT000042752981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 24/12/2020, 427890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP DE CHAISEMARTIN, DOUMIC-SEILLER ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:427890.20201224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              MM. Hippolyte Edouard A... et Joseph Robert A... ainsi que la société La Savane ont demandé au tribunal administratif de Saint-Barthélemy d'annuler pour excès de pouvoir la délibération du 30 octobre 2014 par laquelle le conseil territorial de la collectivité de Saint-Barthélemy a délivré à la société Jusama Holding et à la société Sobadis un permis de construire une surface commerciale. Par un jugement n° 1500001, 1500005 du 21 juillet 2016, le tribunal administratif de Saint-Barthélemy a rejeté leur demande. <br/>
<br/>
              Par un arrêt avant-dire droit n° 16BX03434 du 27 décembre 2018, la cour administrative d'appel de Bordeaux a, sur appel de MM. A... et de la société La Savane, sursis à statuer sur la légalité du permis de construire du 30 octobre 2014 pour permettre aux sociétés Jusama Holding et Sobadis de notifier à la cour un permis de construire modificatif régularisant les vices tirés de la largeur insuffisante de la voie d'accès au projet et du défaut d'exutoire des eaux pluviales.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 février et 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, MM. A... et la société La Savane demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              3°) de mettre à la charge des sociétés Jusama Holding et Sobadis la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de l'urbanisme de Saint-Barthélemy ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;		<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de M. A... et autres, à la SCP de Chaisemartin, Doumic-Seiller, avocat de la société Jusama holding et de la société Sobadis et à la SCP Lyon-Caen, Thiriez, avocat du Conseil territorial de la collectivité de Saint-Barthélemy ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 décembre 2020, présentée par la collectivité de Saint-Barthélemy ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération n° 2014-1159 CT du 30 octobre 2014, le conseil territorial de la collectivité de Saint-Barthélemy a délivré à la société Jusama Holding et la société Sobadis un permis de construire une surface commerciale présentant 995 m² de surface de vente. Par un jugement du 21 juillet 2016, le tribunal administratif de Saint-Barthélemy a rejeté la demande de MM. A... et de la société La Savane tendant à l'annulation pour excès de pouvoir de ce permis de construire. Saisie de l'appel formé par MM. A... et la société La Savane contre ce jugement, la cour administrative d'appel de Bordeaux a sursis à statuer sur la légalité de ce permis de construire par un arrêt avant-dire droit du 27 décembre 2018, pour permettre aux sociétés Jusama Holding et Sobadis d'obtenir un permis de construire modificatif régularisant deux vices tirés, d'une part, du caractère insuffisant de la voie d'accès au projet et, d'autre part, du défaut d'évacuation des eaux pluviales. Par un arrêt du 21 novembre 2019 mettant fin à l'instance, la cour a rejeté la requête de MM. A... et de la société La Savane, qui se pourvoient en cassation contre l'arrêt avant dire droit du 27 décembre 2018.<br/>
<br/>
              2.	Lorsque les juges du fond, après avoir écarté comme non fondés des moyens de la requête, ont cependant retenu l'existence d'un ou de plusieurs vices entachant la légalité du permis de construire, de démolir ou d'aménager dont l'annulation leur était demandée et ont alors décidé de surseoir à statuer en faisant usage des pouvoirs qu'ils tiennent de l'article L. 600-5-1 du code de l'urbanisme pour inviter l'administration à régulariser ce ou ces vices, l'auteur du recours formé contre le jugement ou l'arrêt avant dire droit peut contester ce jugement ou cet arrêt en tant qu'il a écarté comme non fondés les moyens dirigés contre l'autorisation initiale d'urbanisme et également en tant qu'il a fait application des dispositions de l'article L. 600-5-1. Toutefois, à compter de la délivrance du permis modificatif en vue de régulariser le ou les vices relevés, les conclusions dirigées contre le jugement ou l'arrêt avant-dire droit, en tant qu'il met en oeuvre les pouvoirs que le juge tient de l'article L. 600-5-1 du code de l'urbanisme, sont privées d'objet.<br/>
<br/>
              3.	Dès lors que, postérieurement à l'introduction du présent pourvoi, un permis modificatif a été délivré, le 28 mars 2019, pour la régularisation des vices relevés dans l'arrêt avant dire droit attaqué, il résulte de ce qui a été dit au point 2 qu'il n'y a plus lieu de statuer sur les conclusions de ce pourvoi dirigées contre l'arrêt avant-dire droit en tant qu'il met en oeuvre les pouvoirs que le juge tient de l'article L. 600-5-1 du code de l'urbanisme. En revanche, il y a lieu de se prononcer sur les moyens du pourvoi dirigés contre l'arrêt avant dire droit en tant qu'il a écarté des moyens de la requête dirigés contre le permis de construire initial.<br/>
<br/>
              4.	L'article R. 111-5 du code de l'urbanisme, applicable à Saint-Barthélemy en l'absence de carte d'urbanisme, dispose que : " Le projet peut être refusé sur des terrains qui ne seraient pas desservis par des voies publiques ou privées dans des conditions répondant à son importance ou à la destination des constructions ou des aménagements envisagés, et notamment si les caractéristiques de ces voies rendent difficile la circulation ou l'utilisation des engins de lutte contre l'incendie. / Il peut également être refusé ou n'être accepté que sous réserve de prescriptions spéciales si les accès présentent un risque pour la sécurité des usagers des voies publiques ou pour celle des personnes utilisant ces accès. Cette sécurité doit être appréciée compte tenu, notamment, de la position des accès, de leur configuration ainsi que de la nature et de l'intensité du trafic ".<br/>
<br/>
              5.	Il résulte de ces dispositions que les conditions de desserte d'un projet de construction doivent être appréciées, d'une part, au regard de l'importance de ce dernier, de sa destination ou des aménagements envisagés, mais aussi, d'autre part, au regard des risques que présentent les accès pour la sécurité des usagers des voies publiques ou des personnes qui les utilisent, compte tenu notamment, de la position des accès, de leur configuration ainsi que de la nature et de la densité du trafic. Par suite, en relevant que les requérants ne pouvaient utilement se prévaloir des conditions de la circulation générale dans le secteur pour écarter le moyen tiré de ce que, eu égard au trafic additionnel attendu du projet de centre commercial, le débouché de la voie d'accès du projet sur la route départementale, qui traverse l'île de Saint-Barthélemy, constituerait un danger pour la sécurité publique compte tenu de l'importance du trafic qu'elle supporte déjà, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              6.	Il résulte de ce qui précède que les requérants sont fondés à soutenir que c'est à tort que, par l'arrêt avant-dire droit attaqué, la cour administrative d'appel de Bordeaux a écarté le moyen tiré de la méconnaissance des dispositions du second alinéa de l'article R. 111-5 du code de l'urbanisme. Dès lors, son arrêt doit être annulé. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge conjointe de la société Jusama Holding et de la société Sobadis la somme de 2 000 euros à verser globalement à MM. A... et à la société La Savane au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de MM. A... et de la société La Savane, qui ne sont pas, dans la présente instance, les parties perdantes<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt avant-dire droit du 27 décembre 2018 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : La société Jusama Holding et à la société Sobadis verseront conjointement à MM. A... et à la SCI La Savane la somme globale de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. C... A..., à M. B... A..., à la société La Savane, à la société Jusama Holding et à la société Sobadis.<br/>
Copie en sera adressée à la collectivité de Saint-Barthélemy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
