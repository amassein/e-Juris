<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032529619</ID>
<ANCIEN_ID>JG_L_2016_05_000000381148</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/96/CETATEXT000032529619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 13/05/2016, 381148, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381148</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:381148.20160513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 juin 2014, 28 juillet 2014 et 12 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la société Teofarma demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 6 mars 2014 par laquelle le comité économique des produits de santé a refusé de modifier le prix de la spécialité Laroxyl 40 mg/ml (chlorhydrate d'amitriptyline), solution buvable en gouttes, 20 ml en flacon avec compte-gouttes, ainsi que la décision du 27 mars 2014 par laquelle le comité a rejeté son recours gracieux contre la décision du 6 mars 2014 ; <br/>
<br/>
              2°) d'enjoindre au comité économique des produits de santé de faire droit à sa demande de modification du prix du médicament en litige ou à tout le moins de statuer à nouveau sur celle-ci dans un délai d'un mois au plus tard à compter de la notification de la décision à intervenir, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Teofarma ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 162-16-4 du code de la sécurité sociale : " Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité. La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu apportée par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament (...) ". Aux termes du I de l'article R. 163-11 du même code : " Le prix d'un médicament inscrit sur la liste prévue au premier alinéa de l'article L. 162-17 peut être modifié par convention conclue entre l'entreprise qui l'exploite et le comité économique des produits de santé ou, à défaut, par décision du comité ". Aux termes de l'article R. 163-14 de ce code : " Les décisions portant (...) refus de modification du prix (...) sont communiquées à l'entreprise avec la mention des motifs de ces décisions (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier que, par avenant du 18 mars 2013 à la convention de prix du 11 mai 2010 conclue entre le comité économique des produits de santé et la société Teofarma, le prix fabricant hors taxes de la spécialité Laroxyl 40 mg/ml (chlorhydrate d'amitriptyline) solution buvable en gouttes, 20 ml en flacon avec compte-gouttes, a été porté de 2,11 à 2,94 euros. Par une lettre du 6 décembre 2013, la société Teofarma a demandé au comité économique des produits de santé de porter ce prix à 3,98 euros. Par un courrier du 7 mars 2014, le président du comité économique des produits de santé lui a notifié la décision de ce comité du 6 mars 2014 de ne pas modifier le prix de la spécialité. Par un nouveau courrier du 3 avril 2014, le président du comité lui a notifié la décision du comité, prise le 27 mars 2014 à la suite du recours gracieux formé par la société, de confirmer ce refus. <br/>
<br/>
              3. Il ressort également des pièces du dossier que pour fixer, en 2013, le prix de la spécialité en litige, le comité économique des produits de santé s'est fondé sur le prix de revient unitaire de celle-ci, lequel tenait notamment compte de l'amortissement du coût d'achat des droits et marques afférents à cette spécialité calculé sur un mode linéaire sur une durée de cinq ans, augmenté d'une marge de 50 %. Toutefois, il a retenu comme base de calcul de ces amortissements une quote-part, correspondant au chiffre d'affaires réalisé par la société en France, non pas du coût total d'achat des droits et marques en cause, mais de la moitié seulement de ce coût. Pour refuser, par les décisions attaquées, l'augmentation de prix sollicitée, le comité économique des produits de santé a estimé que ce choix était justifié, seule la moitié du coût d'achat des droits et marques devant être amortie.<br/>
<br/>
              4. Les dispositions précitées de l'article L. 162-16-4 du code de la sécurité sociale ne prévoient pas, même lorsque le prix de vente au public d'un médicament est fixé par voie de convention, que celui-ci doive nécessairement être déterminé en fonction de son prix de revient pour le fabricant. Toutefois, le comité économique des produits de santé ne pouvait, sans entacher sa décision d'erreur de droit, faire le choix de se fonder sur un tel mode de détermination du prix tout en retenant, pour les besoins de son calcul, comme base d'amortissement des droits et marques, la moitié seulement du prix de revient de cet actif. Contrairement à ce que soutient le comité économique des produits de santé, la société Teofarma peut utilement se prévaloir de l'erreur de droit ainsi commise, eu égard au motif qui fonde les décisions attaquées. <br/>
<br/>
              5. Par suite, la société Teofarma est fondée à demander l'annulation des décisions attaquées. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête.<br/>
<br/>
              6. L'exécution de la présente décision implique que la demande de la société Teofarma soit réexaminée. Il y a lieu, par suite, d'enjoindre au comité économique des produits de santé de procéder à ce réexamen dans un délai de trois mois à compter de la notification de la présente décision. Dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la société requérante.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Teofarma au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les décisions du comité économique des produits de santé des 6 et 27 mars 2014 sont annulées. <br/>
Article 2 : Il est enjoint au comité économique des produits de santé de réexaminer la demande de la société Teofarma dans un délai de 3 mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à la société Teofarma une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Teofarma et au comité économique des produits de santé.<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
