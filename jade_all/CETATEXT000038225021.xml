<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038225021</ID>
<ANCIEN_ID>JG_L_2019_03_000000421051</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/50/CETATEXT000038225021.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 12/03/2019, 421051, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421051</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP L. POULET, ODENT ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421051.20190312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Châlons-en-Champagne de condamner le centre hospitalier universitaire de Reims ou l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à réparer les conséquences dommageables de sa contamination par le virus de l'hépatite C lors de la transplantation rénale dont il a bénéficié au CHU de Reims. Par un jugement n° 0801674 du 10 janvier 2013, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC00344 du 26 février 2015, la cour administrative d'appel de Nancy a annulé ce jugement en tant qu'il rejetait les conclusions de M. B...dirigées contre l'ONIAM et a invité M. B...à chiffrer ses prétentions. Par un second arrêt du 24 septembre 2015, la cour administrative d'appel de Nancy a mis à la charge de l'ONIAM le versement de la somme de 353 028 euros au titre des préjudices subis.<br/>
<br/>
              Par une décision n° 394801 du 7 février 2017, le Conseil d'Etat statuant au contentieux  a annulé l'arrêt de la cour administrative d'appel de Nancy du 24 septembre 2015 en tant qu'il statue sur les conclusions de M. B...tendant à l'indemnisation de ses pertes de revenus à compter du 25 septembre 2015 et a lui renvoyé l'affaire dans la limite de la cassation prononcée.<br/>
<br/>
              Par un arrêt n° 17NC00324 du 29 mars 2018, la cour administrative d'appel de Nancy a condamné l'ONIAM à verser à M. B...la somme totale de 231 927,92 euros au titre de ses pertes de revenus à compter du 25 septembre 2015.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 mai et 29 août 2018 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de la sécurité sociale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; <br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Nancy qu'il attaque, l'ONIAM soutient que cette cour :<br/>
              - a insuffisamment motivé son arrêt en s'abstenant de préciser pourquoi M. B... aurait pu escompter des droits à pension supérieurs de 40 % à ceux évalués par Info retraite ;<br/>
              - a commis une erreur de droit et dénaturé les pièces du dossier en ne tenant pas compte de la pension d'invalidité et de la majoration d'invalidité perçue ou susceptible d'être perçue par M. B...pour évaluer sa perte de gains professionnels et l'incidence professionnelle de son dommage.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'évaluation des préjudices de perte de droits à pension de M.B.... En revanche, ces moyens ne sont pas de nature à permettre l'admission des conclusions dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'évaluation des pertes de gains professionnels.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de l'ONIAM qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'évaluation des préjudices de perte de droits à pension de M. B... sont admises.<br/>
		Article 2 : Le surplus des conclusions du pourvoi de l'ONIAM n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
