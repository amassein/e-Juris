<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029413487</ID>
<ANCIEN_ID>JG_L_2014_08_000000376186</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/41/34/CETATEXT000029413487.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 27/08/2014, 376186, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376186</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:376186.20140827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 mars et 10 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Vinci construction grands projets, dont le siège est 5 cours Ferdinand de Lesseps à Rueil-Malmaison Cedex (92851), la société GTM génie civil et services, dont le siège est 61 avenue Jules Quentin à Nanterre (92000) et la société Baudin Châteauneuf, dont le siège est 60 rue de la Brosse à Châteauneuf-sur-Loire (45110) ; les sociétés demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX00160 du 7 janvier 2014 par lequel la cour administrative d'appel de Bordeaux a, d'une part, rejeté leur requête tendant, en premier lieu, à la réformation du jugement n° 0300969-0303192-0402735-0404407-0501199-0501200-0502818 du 17 novembre 2009 du tribunal administratif de Bordeaux statuant sur leur contestation du décompte général du marché conclu avec l'Etat le 30 mars 2000 pour les travaux de remplacement de la suspension du pont d'Aquitaine et d'élargissement du tablier, en deuxième lieu, à ce que soit prononcé la nullité du rapport d'expertise du 12 décembre 2006, à ce que soit ordonné une nouvelle expertise, à la décharge de la totalité des pénalités de retard et à la condamnation de l'Etat à payer au groupement une indemnité provisionnelle de 4 904 472,43 euros assortie des intérêts moratoires à compter du 2 août 2004, capitalisés à compter du 22 septembre 2005, en troisième lieu, si  aucune nouvelle expertise n'est ordonnée, à la condamnation de l'Etat à payer au groupement une somme de 41 810 172,95 euros HT assortie des intérêts moratoires à compter du 2 août 2004, capitalisés à compter du 25 octobre 2005, en quatrième lieu, à la réduction des pénalités infligées pour les retards dans l'exécution des travaux et la levée des réserves, à l'annulation de l'ordre de service du 17 septembre 2004 et à ce que les frais de l'expertise ordonnée le 24 mars 2005 soient mis à la charge de l'Etat, d'autre part, sur l'appel incident de l'Etat, réintégré la somme de 155 426,62 euros HT dans le solde en faveur de l'Etat du marché conclu le 30 mars 2000, cette somme portant intérêts de droit à compter du 30 avril 2009, et réformé en ce sens le jugement du 17 novembre 2009 du tribunal administratif de Bordeaux ; <br/>
<br/>
              2°) de renvoyer le jugement de l'affaire à la cour administrative d'appel de Bordeaux ou subsidiairement, réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 7 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 juillet 2014, présentée par les sociétés Vinci constructions grand projets, GTM Génie civil et services et Baudin Châteauneuf ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu la loi n° 75-1334 du 31 décembre 1975 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Vinci construction grands projets, de la société GTM génie civil et services et de la société Baudin Châteauneuf ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt attaqué, la société Vinci construction grands projets et autres soutiennent que la cour administrative d'appel de Bordeaux a méconnu les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en refusant d'écarter le rapport d'expertise ou, à tout le moins, d'ordonner une nouvelle expertise ; qu'elle a entaché son arrêt de contradiction de motifs en jugeant régulière l'expertise tout en relevant qu'elle pouvait la prendre en compte " à titre d'élément d'information ", de telle sorte qu'il n'est pas possible de savoir quelle force lui a été accordée ; qu'elle a insuffisamment motivé sa décision en jugeant que la notification tardive du plan d'implantation des ouvrages avait été sans incidence sur les retards ; qu'elle a insuffisamment motivé son arrêt en considérant que l'expert avait retenu une prolongation des délais d'exécution de cent vingt et un jours, sans répondre au moyen tiré de ce que ce délai devait être corrigé en cent cinquante et un jours ; qu'elle a commis une erreur de droit en privant de portée l'engagement pris par le maître d'ouvrage de prolonger le délai d'exécution de cent cinquante deux jours à raison des travaux portant sur la poutre avant rive droite ; qu'elle a commis une erreur de droit en considérant que la circonstance que l'Etat n'aurait subi aucun préjudice était sans incidence sur le caractère manifestement excessif du montant des pénalités de retard mises à la charge du groupement ; qu'elle a commis une erreur de droit en examinant le caractère manifestement excessif du montant des pénalités de retard non globalement, mais en distinguant, d'une part, celles liées à l'exécution des travaux, d'autre part, celles liées à la levée des réserves ; qu'elle a dénaturé les pièces du dossier en jugeant que le montant des pénalités de retard, fixé à 9 669 242 euros, soit 24 % du montant du marché, n'était pas manifestement excessif ; qu'elle a inexactement qualifié les faits et dénaturé les pièces du dossier en jugeant que les stipulations de l'article 3.0.0 du cahier des clauses administratives particulières ne dérogeaient pas à celles de l'article 10.11 du cahier des clauses administratives générales alors applicable aux marchés de travaux ; qu'elle a commis une erreur de droit et inexactement qualifié les faits en subordonnant l'indemnisation des travaux supplémentaires proposés par l'entrepreneur et acceptés par ordre de service à leur caractère indispensable ; qu'elle a dénaturé les pièces du dossier en rejetant les conclusions du groupement sollicitant la somme de 1 883 548,46 euros au titre des surcoûts intervenus lors de la deuxième phase du soutènement rive droite ; qu'elle a commis une erreur de droit en retenant, pour la computation du délai de quinze jours ouvert à l'entrepreneur principal pour signifier son refus motivé d'acceptation à la demande de paiement direct d'un sous-traitant en application de la loi du 31 décembre 1975 relative à la sous-traitance, la date de réception de la lettre portant refus et non celle de son envoi ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt en tant qu'il a rejeté les conclusions tendant à l'indemnisation des travaux supplémentaires relatifs, d'une part, aux appuis " Freyssinet " et, d'autre part, aux tiges de serrages des colliers ; qu'en revanche, s'agissant des autres conclusions du pourvoi, aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société Vinci construction grands projets et autres dirigées contre l'arrêt en tant qu'il a rejeté les conclusions tendant à l'indemnisation des travaux supplémentaires relatifs, d'une part, aux appuis " Freyssinet " et, d'autre part, aux tiges de serrages des colliers, sont admises.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Vinci construction grands projets et autres n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Vinci construction grands projets, à la société GTM génie civil et services, à la société Baudin Châteauneuf et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
