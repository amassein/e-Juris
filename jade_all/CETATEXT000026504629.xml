<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504629</ID>
<ANCIEN_ID>JG_L_2012_10_000000348440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 17/10/2012, 348440, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:348440.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 avril et 13 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mlle D...C..., demeurant..., à Nice (06100), M. A... C..., demeurant..., à Nice (06100) et Mme E...C..., demeurant..., à Nice (06100) ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01294 du 17 février 2011 par lequel la cour administrative d'appel de Marseille a rejeté leur appel contre le jugement n° 0506174 du 27 janvier 2009 par lequel le tribunal administratif de Nice a rejeté leur demande tendant à la condamnation du département des Alpes-Maritimes à verser la somme de 8 000 euros à Mlle D... C...et la somme de 5 000 euros chacun à M. et MmeC..., ses parents, en réparation du préjudice moral qu'ils ont subi du chef de la divulgation d'informations confidentielles relatives à l'adoption de Mlle C...; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête d'appel ; <br/>
<br/>
              3°) de mettre à la charge du département des Alpes-Maritimes la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 96-604 du 6 juillet 1996 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de Mlle D...C...et de M. et MmeC..., et de la SCP Gaschignard, avocat du département des Alpes-Maritimes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Rocheteau, Uzan-Sarano, avocat de Mlle D...C...et de M. et MmeC..., et à la SCP Gaschignard, avocat du département des Alpes-Maritimes ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a donné naissance dans l'anonymat à une fille le 7 novembre 1987 ; que celle-ci a été ultérieurement adoptée par M. et Mme C...qui l'ont prénommée Sophie ; que Mme B...a, courant 2001, obtenu des informations relatives au nouvel état civil de sa fille biologique et au nom de ses parents adoptifs ; qu'elle a dès lors pris contact avec Mlle D...C..., alors que celle-ci était âgée de quatorze ans, et s'est manifestée de façon insistante et répétée, au cours de plusieurs années, tant auprès de cette dernière que des membres de sa famille et de son entourage et s'est à plusieurs reprises exprimée dans la presse sur l'enquête personnelle qu'elle avait menée pour retrouver l'enfant ; que les requérants ont recherché devant le tribunal administratif de Nice la responsabilité du département des Alpes-Maritimes à raison de la faute résultant de la divulgation par ses services à Mme B...d'informations confidentielles relatives à la famille adoptive de sa fille biologique ; que le tribunal administratif a rejeté leur demande ; qu'ils se pourvoient en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a rejeté l'appel formé contre le jugement du tribunal administratif ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 133-4 du code de l'action sociale et des familles : " Les informations nominatives à caractère sanitaire et social détenues par les services des affaires sanitaires et sociales sont protégées par le secret professionnel " ; que, par ailleurs, il résulte de l'article 46 du code de la famille et de l'aide sociale, en vigueur à la naissance de Mlle D...C...et devenu l'article L. 222-5 du code de l'action sociale et des familles, que les pupilles de l'Etat, dont font partie les enfants dont la mère a souhaité préserver le secret de son identité lors de son accouchement, sont confiés, sur décision du président du conseil général, au service de l'aide sociale à l'enfance du département ; qu'aux termes de l'article 348-3 du code civil, le consentement à l'adoption donné par le conseil des familles des pupilles de l'Etat peut être reçu par le service de l'aide sociale à l'enfance lorsque l'enfant lui a été confié ; qu'enfin, l'article 62-1 du code de la famille et de l'aide sociale, inséré dans ce code par la loi du 6 juillet 1996 relative à l'adoption et devenu en 2002 l'article L. 224-7 du code de l'action sociale et des familles, dispose que sont conservés sous la responsabilité du président du conseil général les renseignements figurant dans le procès-verbal établi lors du recueil d'un enfant par le service de l'aide sociale à l'enfance et relatifs à l'identité des père et mère de cet enfant et à la volonté des intéressés de conserver le secret de leur identité ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, sous réserve de la mise en oeuvre des dispositions autorisant les autorités ou les services du département à communiquer les informations dont ils sont dépositaires, et en particulier de celles de l'article L. 224-7 du code de l'action sociale et des familles qui imposent au président du conseil général de transmettre au Conseil national pour l'accès aux origines personnelles, sur la demande de ce dernier, les renseignements dont il dispose sur les pupilles de l'Etat qu'il a recueillis, il est interdit au service de l'aide sociale à l'enfance de divulguer de telles informations ; <br/>
<br/>
              4. Considérant que la circonstance que la mère biologique d'un enfant confié à sa naissance au service de l'aide sociale à l'enfance, puis adopté, ait eu connaissance des informations relatives à la nouvelle identité de cet enfant et à celle de ses parents adoptifs révèle une faute dans le fonctionnement du service de l'aide sociale à l'enfance du département de nature à engager la responsabilité de ce dernier, sauf à ce qu'il établisse que la divulgation  de ces informations est imputable à un tiers ou à une faute de la victime ; que par, suite, en estimant que les requérants n'apportaient pas la preuve qui leur incombait que les services du département des Alpes-Maritimes auraient commis une faute de nature à engager la responsabilité de ce département, la cour administrative d'appel de Marseille a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'arrêt de la cour administrative d'appel de Marseille doit être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département des Alpes-Maritimes la somme globale de 3 500 euros à verser à Mlle, M. et MmeC..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des requérants qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 17 février 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Le département des Alpes-Maritimes versera une somme globale de 3 500 euros à Mlle, M. et Mme C...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le département des Alpes-Maritimes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mlle D...C..., à M. A...C..., à Mme E... C... et au département des Alpes-Maritimes. Copie en sera adressée pour information à la ministre des affaires sociales et de la santé.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02-01 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. PUPILLES DE L'ETAT. - ENFANT DONT LA MÈRE A SOUHAITÉ PRÉSERVER LE SECRET DE SON IDENTITÉ LORS DE SON ACCOUCHEMENT - CIRCONSTANCE QUE LA MÈRE BIOLOGIQUE AIT EU CONNAISSANCE DES INFORMATIONS RELATIVES À LA NOUVELLE IDENTITÉ DE L'ENFANT ET À CELLE DES PARENTS ADOPTIFS - RÉGIME DE RESPONSABILITÉ - PRÉSOMPTION DE FAUTE DU SERVICE DE L'AIDE SOCIALE À L'ENFANCE DU DÉPARTEMENT - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-03-02-01-01 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. ACTION SOCIALE. - AIDE SOCIALE À L'ENFANCE - PUPILLES DE L'ETAT - ENFANT DONT LA MÈRE A SOUHAITÉ PRÉSERVER LE SECRET DE SON IDENTITÉ LORS DE SON ACCOUCHEMENT - CIRCONSTANCE QUE LA MÈRE BIOLOGIQUE AIT EU CONNAISSANCE DES INFORMATIONS RELATIVES À LA NOUVELLE IDENTITÉ DE L'ENFANT ET À CELLE DES PARENTS ADOPTIFS - RÉGIME DE RESPONSABILITÉ - PRÉSOMPTION DE FAUTE DU SERVICE DE L'AIDE SOCIALE À L'ENFANCE DU DÉPARTEMENT - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. APPLICATION D'UN RÉGIME DE FAUTE SIMPLE. - AIDE SOCIALE À L'ENFANCE - PUPILLES DE L'ETAT - ENFANT DONT LA MÈRE A SOUHAITÉ PRÉSERVER LE SECRET DE SON IDENTITÉ LORS DE SON ACCOUCHEMENT - CIRCONSTANCE QUE LA MÈRE BIOLOGIQUE AIT EU CONNAISSANCE DES INFORMATIONS RELATIVES À LA NOUVELLE IDENTITÉ DE L'ENFANT ET À CELLE DES PARENTS ADOPTIFS - PRÉSOMPTION DE FAUTE DU SERVICE DE L'AIDE SOCIALE À L'ENFANCE DU DÉPARTEMENT - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - PUPILLES DE L'ETAT - ENFANT DONT LA MÈRE A SOUHAITÉ PRÉSERVER LE SECRET DE SON IDENTITÉ LORS DE SON ACCOUCHEMENT - CIRCONSTANCE QUE LA MÈRE BIOLOGIQUE AIT EU CONNAISSANCE DES INFORMATIONS RELATIVES À LA NOUVELLE IDENTITÉ DE L'ENFANT ET À CELLE DES PARENTS ADOPTIFS - RÉGIME DE RESPONSABILITÉ - PRÉSOMPTION DE FAUTE DU SERVICE DE L'AIDE SOCIALE À L'ENFANCE DU DÉPARTEMENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-02-02-01 Cas des pupilles de l'Etat dont la mère a souhaité préserver le secret de son identité lors de son accouchement. Sous réserve de la mise en oeuvre des dispositions autorisant les autorités ou les services du département à communiquer les informations dont ils sont dépositaires, et en particulier de celles de l'article L. 224-7 du code de l'action sociale et des familles qui imposent au président du conseil général de transmettre au Conseil national pour l'accès aux origines personnelles, sur la demande de ce dernier, les renseignements dont il dispose sur les pupilles de l'Etat qu'il a recueillis, il est interdit au service de l'aide sociale à l'enfance de divulguer de telles informations. La circonstance que la mère biologique d'un enfant confié à sa naissance au service de l'aide sociale à l'enfance, puis adopté, ait eu connaissance des informations relatives à la nouvelle identité de cet enfant et à celle de ses parents adoptifs révèle une faute dans le fonctionnement du service de l'aide sociale à l'enfance du département de nature à engager la responsabilité de ce dernier, sauf à ce qu'il établisse que la divulgation de ces informations est imputable à un tiers ou à une faute de la victime.</ANA>
<ANA ID="9B"> 135-03-02-01-01 Cas des pupilles de l'Etat dont la mère a souhaité préserver le secret de son identité lors de son accouchement. Sous réserve de la mise en oeuvre des dispositions autorisant les autorités ou les services du département à communiquer les informations dont ils sont dépositaires, et en particulier de celles de l'article L. 224-7 du code de l'action sociale et des familles qui imposent au président du conseil général de transmettre au Conseil national pour l'accès aux origines personnelles, sur la demande de ce dernier, les renseignements dont il dispose sur les pupilles de l'Etat qu'il a recueillis, il est interdit au service de l'aide sociale à l'enfance de divulguer de telles informations. La circonstance que la mère biologique d'un enfant confié à sa naissance au service de l'aide sociale à l'enfance, puis adopté, ait eu connaissance des informations relatives à la nouvelle identité de cet enfant et à celle de ses parents adoptifs révèle une faute dans le fonctionnement du service de l'aide sociale à l'enfance du département de nature à engager la responsabilité de ce dernier, sauf à ce qu'il établisse que la divulgation de ces informations est imputable à un tiers ou à une faute de la victime.</ANA>
<ANA ID="9C"> 60-01-02-02-02 Cas des pupilles de l'Etat dont la mère a souhaité préserver le secret de son identité lors de son accouchement. Sous réserve de la mise en oeuvre des dispositions autorisant les autorités ou les services du département à communiquer les informations dont ils sont dépositaires, et en particulier de celles de l'article L. 224-7 du code de l'action sociale et des familles qui imposent au président du conseil général de transmettre au Conseil national pour l'accès aux origines personnelles, sur la demande de ce dernier, les renseignements dont il dispose sur les pupilles de l'Etat qu'il a recueillis, il est interdit au service de l'aide sociale à l'enfance de divulguer de telles informations. La circonstance que la mère biologique d'un enfant confié à sa naissance au service de l'aide sociale à l'enfance, puis adopté, ait eu connaissance des informations relatives à la nouvelle identité de cet enfant et à celle de ses parents adoptifs révèle une faute dans le fonctionnement du service de l'aide sociale à l'enfance du département de nature à engager la responsabilité de ce dernier, sauf à ce qu'il établisse que la divulgation de ces informations est imputable à un tiers ou à une faute de la victime.</ANA>
<ANA ID="9D"> 60-02-012 Cas des pupilles de l'Etat dont la mère a souhaité préserver le secret de son identité lors de son accouchement. Sous réserve de la mise en oeuvre des dispositions autorisant les autorités ou les services du département à communiquer les informations dont ils sont dépositaires, et en particulier de celles de l'article L. 224-7 du code de l'action sociale et des familles qui imposent au président du conseil général de transmettre au Conseil national pour l'accès aux origines personnelles, sur la demande de ce dernier, les renseignements dont il dispose sur les pupilles de l'Etat qu'il a recueillis, il est interdit au service de l'aide sociale à l'enfance de divulguer de telles informations. La circonstance que la mère biologique d'un enfant confié à sa naissance au service de l'aide sociale à l'enfance, puis adopté, ait eu connaissance des informations relatives à la nouvelle identité de cet enfant et à celle de ses parents adoptifs révèle une faute dans le fonctionnement du service de l'aide sociale à l'enfance du département de nature à engager la responsabilité de ce dernier, sauf à ce qu'il établisse que la divulgation de ces informations est imputable à un tiers ou à une faute de la victime.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
