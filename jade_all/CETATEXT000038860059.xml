<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860059</ID>
<ANCIEN_ID>JG_L_2019_07_000000418739</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/07/2019, 418739, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418739</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418739.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Photosol a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir l'arrêté du 10 juin 2012 par lequel le préfet d'Eure-et-Loir a refusé de lui délivrer le permis de construire un parc photovoltaïque d'une puissance de 12 mégawatts crête sur le territoire de la commune de Viabon, ainsi que la décision implicite de rejet de son recours gracieux. Par un jugement n° 1203789 du 31 décembre 2013, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NT00587 du 23 octobre 2015, la cour administrative d'appel de Nantes a, sur l'appel de la société Photosol, annulé ce jugement et l'arrêté du préfet d'Eure-et-Loir et enjoint à ce préfet de procéder à un nouvel examen de la demande de permis de construire de la société Photosol dans un délai de deux mois.<br/>
<br/>
              Par une décision n° 395464 du 8 février 2017, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 17NT00513 du 29 décembre 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Photosol contre le jugement du tribunal administratif d'Orléans. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 mars et 4 juin 2018 et le 3 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la société Photosol demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 29 décembre 2017 ;  <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société Photosol ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Photosol a sollicité un permis de construire portant sur la réalisation d'un parc photovoltaïque sur un terrain situé sur le territoire de la commune de Viabon. Par un arrêté du 10 juin 2012, le préfet d'Eure-et-Loir a rejeté cette demande, au motif que le projet était incompatible avec une activité agricole au sens de l'article L. 123-1 du code de l'urbanisme, l'activité apicole envisagée n'étant pas de nature à compenser la réduction des espaces agricoles entraînée par le projet. Par un jugement du 31 décembre 2013, le tribunal administratif d'Orléans a rejeté la demande de la société Photosol tendant à l'annulation de cet arrêté et de la décision implicite de rejet de son recours gracieux. La société Photosol se pourvoit en cassation contre l'arrêt du 29 décembre 2017 par lequel la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat, a rejeté son appel formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 741-2 du code de justice administrative : " La décision mentionne que l'audience a été publique (...). / Elle contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application. / (...) Mention est également faite de la production d'une note en délibéré (...) ". Il résulte de ces dispositions que, lorsqu'il est régulièrement saisi, à l'issue de l'audience, d'une note en délibéré émanant de l'une des parties, il appartient dans tous les cas au juge administratif d'en prendre connaissance avant de rendre sa décision ainsi que de la viser, sans toutefois l'analyser dès lors qu'il n'est pas amené à rouvrir l'instruction et à la soumettre au débat contradictoire pour tenir compte des éléments nouveaux qu'elle contient.<br/>
<br/>
              3. Il ressort des pièces de la procédure devant la cour administrative d'appel de Nantes qu'après l'audience publique qui s'est tenue le 12 décembre 2017, la société Photosol a produit une note en délibéré, enregistrée au greffe de la cour le 15 décembre 2017, soit avant la lecture de l'arrêt. L'arrêt attaqué, qui ne vise pas cette note, est ainsi entaché d'irrégularité. <br/>
<br/>
              4. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Photosol est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond. <br/>
<br/>
              Sur la régularité du jugement du tribunal administratif d'Orléans :<br/>
<br/>
              6. Aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions, prévue par ces dispositions, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. En conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative. Cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public.<br/>
<br/>
              7. Il ressort des pièces de la procédure devant le tribunal administratif d'Orléans que le sens des conclusions du rapporteur public, tendant au rejet au fond de la demande,  a été mis en ligne le 16 décembre 2013 à 10 h 30, en vue d'une audience se tenant le lendemain à 9 h 30. Le rapporteur public ayant ainsi indiqué aux parties le sens de ses conclusions, en indiquant les éléments du dispositif de la décision qu'il comptait proposer à la formation de jugement d'adopter, dans un délai raisonnable avant l'audience, la société Photosol n'est pas fondée à soutenir que le jugement aurait été rendu au terme d'une procédure irrégulière. <br/>
<br/>
              Sur la légalité de l'arrêté du préfet d'Eure-et-Loir du 10 juin 2012 :<br/>
<br/>
              8. D'une part, le premier alinéa de l'article L. 421-6 du code de l'urbanisme dispose que : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords et s'ils ne sont pas incompatibles avec une déclaration d'utilité publique ". Aux termes de l'avant-dernier alinéa de l'article L. 123-1 du même code, dans sa rédaction applicable à la date du refus de permis de construire en litige : " Les constructions et installations nécessaires à des équipements collectifs peuvent être autorisées dans les zones naturelles, agricoles ou forestières dès lors qu'elles ne sont pas incompatibles avec l'exercice d'une activité agricole, pastorale ou forestière du terrain sur lequel elles sont implantées et qu'elles ne portent pas atteinte à la sauvegarde des espaces naturels et des paysages ". L'article R. 123-7 de ce code, dans sa rédaction applicable à la même date, précise que : " Les zones agricoles sont dites " zones A ". (...) / En zone A peuvent seules être autorisées : / - les constructions et installations nécessaires à l'exploitation agricole ; / - les constructions et installations nécessaires à des équipements collectifs ou à des services publics, dès lors qu'elles ne sont pas incompatibles avec l'exercice d'une activité agricole, pastorale ou forestière dans l'unité foncière où elles sont implantées et qu'elles ne portent pas atteinte à la sauvegarde des espaces naturels et des paysages (...) ". <br/>
<br/>
              9. Les dispositions de l'avant-dernier alinéa de l'article L. 123-1 du code de l'urbanisme, éclairées par les travaux préparatoires de la loi du 27 juillet 2010 de modernisation de l'agriculture et de la pêche dont elles sont issues, ont pour objet de conditionner l'implantation de constructions et installations nécessaires à des équipements collectifs dans des zones agricoles à la possibilité d'exercer des activités agricoles, pastorales ou forestières sur le terrain où elles doivent être implantées et à l'absence d'atteinte à la sauvegarde des espaces naturels et des paysages. Pour vérifier si la première de ces exigences est satisfaite, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, d'apprécier si le projet permet l'exercice d'une activité agricole, pastorale ou forestière significative sur le terrain d'implantation du projet, au regard des activités qui sont effectivement exercées dans la zone concernée du plan local d'urbanisme ou, le cas échéant, auraient vocation à s'y développer, en tenant compte notamment de la superficie de la parcelle, de l'emprise du projet, de la nature des sols et des usages locaux. <br/>
<br/>
              10. D'autre part, en vertu du règlement de la zone A du plan local d'urbanisme de la commune de Viabon, la zone A correspond aux " zones naturelles à protéger en raison du potentiel agronomique des terres agricoles ". Son article A2 prévoit que : " Les constructions, installations et travaux divers sont autorisés s'ils sont nécessaires aux services publics ou d'intérêts collectifs ".<br/>
<br/>
              11. En premier lieu, d'une part, il incombait au préfet d'Eure-et-Loir de faire application, pour statuer sur la demande dont il était saisi, des dispositions de l'article L. 123-1 du code de l'urbanisme citées au point 8, entrées en vigueur six mois après la publication de la loi du 27 juillet 2010 de modernisation de l'agriculture et de la pêche, conformément au IV de son article 51, alors même que le règlement du plan local d'urbanisme de la commune ne soumettait pas les installations nécessaires à des équipements collectifs à la même exigence. D'autre part, contrairement à ce que soutient la société requérante, le préfet ne s'est pas borné à relever que le projet objet de la demande de permis allait être implanté sur des terres cultivées. Par suite, la société requérante n'est pas fondée à soutenir que la décision de refus attaquée serait entachée d'erreur de droit. <br/>
<br/>
              12. En second lieu, il ressort des pièces du dossier que le permis de construire sollicité par la société Photosol vise la réalisation d'un parc photovoltaïque au sol, comportant environ 45 000 panneaux photovoltaïques et les infrastructures associées, d'une emprise de 26,6 hectares sur des parcelles d'une surface totale de 73 hectares, propriétés de plusieurs exploitants agricoles et classées en zone A par le plan local d'urbanisme de la commune de Viabon, située dans la plaine de la Beauce. Il entraînera la réduction de 26,6 hectares de surface agricole effectivement consacrée à la culture céréalière, tandis que l'activité de substitution prévue par le projet prendra la forme d'une jachère mellifère destinée à l'apiculture, qui ne peut être regardée comme correspondant aux activités ayant vocation à se développer dans la zone considérée. Dans ces conditions, même en tenant compte du fait que les terres agricoles considérées seraient de qualité médiocre par rapport à d'autres terres de la commune, le préfet d'Eure-et-Loir, qui aurait pris la même décision s'il n'avait pas retenu la présence de jeunes chênes truffiers dans la zone 2 d'implantation du projet, a pu légalement estimer que le projet ne permettrait pas le maintien d'une activité agricole significative sur le terrain d'implantation de l'équipement collectif envisagé et refuser d'accorder le permis sollicité pour ce motif.  <br/>
<br/>
              13. Il résulte de ce qui précède que la société Photosol n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Orléans a rejeté sa demande tendant à l'annulation de l'arrêté par lequel le préfet d'Eure-et-Loire a rejeté sa demande de permis de construire, ainsi que la décision implicite de rejet de son recours gracieux.<br/>
<br/>
              14. Par suite, ses conclusions à fin d'injonction et les conclusions qu'elle a présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 29 décembre 2017 est annulé. <br/>
Article 2 : La requête présentée par la société Photosol devant la cour administrative d'appel de Nantes et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Photosol et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
