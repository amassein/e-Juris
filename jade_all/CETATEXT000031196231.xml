<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031196231</ID>
<ANCIEN_ID>JG_L_2015_09_000000371205</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/19/62/CETATEXT000031196231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 21/09/2015, 371205, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371205</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371205.20150921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Total Caraïbes a demandé au tribunal administratif de Fort-de-France de l'indemniser du préjudice qu'elle a subi à raison du retrait illégal par le maire du Lamentin (Martinique) du permis de construire qu'elle avait obtenu en vue de la construction dans cette commune d'une station de distribution de carburant. Par un jugement n° 0900101 du 30 mars 2011, le tribunal administratif de Fort-de-France a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11BX01823 du 16 mai 2013, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Total Caraïbes contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 13 août et 13 novembre 2013, le 26 septembre 2014 et le 23 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société Total Caraïbes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes présentées devant le tribunal administratif ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Lamentin la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Total Caraibes et à la SCP Waquet, Farge, Hazan, avocat de la commune du Lamentin ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 7 décembre 2008, le maire du Lamentin a retiré le permis de construire qu'il avait accordé le 7 mars 2007 à la société Total Caraïbes en vue de la construction d'une station de distribution de carburant sur un terrain loué par cette société en vertu d'un bail à construction et sur lequel, à la date du retrait, elle avait déjà engagé des travaux d'aménagement ; que la société Total Caraïbes s'est bornée à adresser au maire du Lamentin un recours gracieux contre cette décision sans contester sa légalité par voie d'action devant le juge de l'excès de pouvoir ni solliciter sa suspension ; qu'elle n'a saisi, le 11 mars 2009, le tribunal administratif de Fort-de-France que d'un recours indemnitaire contre la commune en invoquant l'illégalité, à raison de sa tardiveté, du retrait opéré par le maire du Lamentin ; <br/>
<br/>
              2. Considérant que, pour rejeter l'appel qui lui était présenté contre le jugement du tribunal administratif ayant rejeté la demande d'indemnisation, la cour administrative d'appel de Bordeaux, après avoir estimé le retrait illégal à raison de sa tardiveté, a jugé qu'en s'abstenant de mettre en oeuvre les recours juridictionnels dont elle disposait pour obtenir la suspension et l'annulation du retrait de permis de construire, et en choisissant de s'engager dans la négociation d'un échange de parcelles avec la commune ou d'une transaction, la société Total Caraïbes devait être regardée comme ayant abandonné son projet initial ; qu'elle en a déduit que ces circonstances faisaient obstacle à ce que l'ensemble des préjudices allégués par la société puissent être regardés comme étant en lien direct avec l'illégalité de la décision portant retrait du permis de construire ; <br/>
<br/>
              3. Considérant qu'en statuant ainsi, alors que les faits relevés n'étaient pas de nature à faire apparaître que l'abandon du projet de construction ne résultait pas du retrait illégal du permis de construire et, qu'en tout état de cause, l'absence d'exercice de voies de recours contre ce retrait ne peut avoir pour effet de rendre indirect le lien de causalité entre les préjudices allégués et l'illégalité fautive qu'elle avait constatée, la cour administrative d'appel a procédé à une inexacte qualification juridique des faits ; qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune du Lamentin une somme de 3 000 euros à verser à la société Total Caraïbes, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Total Caraïbes qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 16 mai 2013 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune du Lamentin versera à la société Total Caraïbes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune du Lamentin au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Total Caraïbes et à la commune du Lamentin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
