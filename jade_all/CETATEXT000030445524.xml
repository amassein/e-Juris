<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445524</ID>
<ANCIEN_ID>JG_L_2015_03_000000353717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445524.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 23/03/2015, 353717</TITRE>
<DATE_DEC>2015-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:353717.20150323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 353717, par une requête sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 28 octobre 2011, 30 janvier 2012, 24 septembre 2012 et 2 août 2013 au secrétariat du contentieux du Conseil d'Etat, l'association Lexeek pour l'accès au droit demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la délibération n° 2011-238 du 30 août 2011 de la formation restreinte de la commission nationale de l'informatique et des libertés (CNIL) prononçant à son encontre une sanction pécuniaire et une injonction de cessation de traitement ;<br/>
<br/>
              2°) d'enjoindre à la CNIL de publier la présente décision, accompagnée de commentaires rectificatifs, sur son site internet et dans plusieurs journaux, dans un délai d'un mois à compter de la présente décision sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) d'enjoindre à la CNIL de cesser la diffusion de la délibération litigieuse sur son site internet ;<br/>
<br/>
              4°) de condamner la CNIL à lui verser la somme de 10 000 euros en réparation du préjudice subi ; <br/>
<br/>
              5°) de mettre à sa charge le versement de la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 362944, par une requête et un mémoire en réplique, enregistrés les 21 septembre 2012 et 17 mars 2014 au secrétariat du contentieux du Conseil d'Etat, l'association Lexeek pour l'accès au droit demande au Conseil d'Etat : <br/>
<br/>
              1°) de condamner la CNIL à lui verser la somme de 10 000 euros, avec les intérêts, en réparation du préjudice subi du fait de la délibération contestée sous le n° 353717 et de sa publication ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de l'association Lexeek pour l'accès au droit ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 30 août 2011, la Commission nationale de l'informatique et des libertés (CNIL) a enjoint à  l'association Lexeek pour l'accès au droit, responsable d'un site internet d'information juridique comportant une base de données de jurisprudence, de cesser la mise en oeuvre de ce traitement de données, a prononcé à son encontre une sanction pécuniaire de 10 000 euros et ordonné la publication de cette décision sur le site internet de la CNIL ainsi que dans trois quotidiens ; que, sous le n° 353717, l'association en demande l'annulation ; qu'elle a par ailleurs demandé à la CNIL de retirer de son site internet la délibération litigieuse jusqu'à ce que le Conseil d'Etat ait statué sur la requête dont il avait été saisi et de l'indemniser du préjudice qu'elle estimait avoir subi du fait de la délibération litigieuse et de sa publication prématurée illégale ; que, sous le n° 362944, l'association demande que la CNIL soit condamnée à réparer son préjudice ; qu'il y a lieu de joindre ces requêtes pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la délibération de la CNIL du 30 août 2011 :<br/>
<br/>
              En ce qui concerne la procédure ayant précédé cette délibération : <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes l'article 45 de la loi du 6 janvier 1978, dans sa rédaction applicable en l'espèce, issue de la loi n° 2004-801 du 6 août 2004 : " La Commission nationale de l'informatique et des libertés peut prononcer un avertissement à l'égard du responsable d'un traitement qui ne respecte pas les obligations découlant de la présente loi. Elle peut également mettre en demeure ce responsable de faire cesser le manquement constaté dans un délai qu'elle fixe. / Si le responsable d'un traitement ne se conforme pas à la mise en demeure qui lui est adressée, la commission peut prononcer à son encontre, après une procédure contradictoire, les sanctions suivantes : /1° Une sanction pécuniaire, dans les conditions prévues par l'article 47, à l'exception des cas où le traitement est mis en oeuvre par l'Etat ; / 2° Une injonction de cesser le traitement, lorsque celui-ci relève des dispositions de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25 (...) " ;<br/>
<br/>
              3. Considérant que si la loi n° 2011-334 du 29 mars 2011 a modifié les dispositions de l'article 45 de la loi du 6 janvier 1978 pour confier désormais au président de la CNIL, et non plus à la commission, le pouvoir de mettre en demeure le responsable d'un traitement de respecter les obligations découlant de la loi avant que la formation restreinte de la CNIL puisse, le cas échéant, prononcer une sanction à son encontre, la légalité de cette mise en demeure, qui est une décision susceptible de recours, s'apprécie à la date à laquelle elle est prononcée ; qu'ainsi, la circonstance que l'état du droit ait été ultérieurement modifié est, contrairement à ce que soutient l'association requérante, sans incidence sur sa régularité ;<br/>
<br/>
              4. Considérant, en second lieu, qu'aux termes de l'article 46 de la même loi : " Les sanctions prévues au I (...) de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés (...). Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la formation restreinte (...) " ; qu'aux termes de l'article 75 du décret du 20 octobre 2005 pris pour l'application la loi du 6 janvier 1978 : " Le rapport prévu par l'article 46 de la loi du 6 janvier 1978 (...) est notifié au responsable du traitement par lettre remise contre signature, ou remise en main propre contre récépissé ou acte d'huissier. / Le responsable du traitement dispose d'un délai d'un mois pour transmettre à la commission ses observations écrites (...) " ; qu'aux termes de l'article 76 du même décret : " Le responsable du traitement est informé de la date de la séance de la commission à l'ordre du jour de laquelle est inscrite l'affaire le concernant et de la faculté qui lui est offerte d'y être entendu, lui-même ou son représentant, par lettre remise contre signature, ou remise en main propre contre récépissé ou acte d'huissier. Cette lettre doit lui parvenir au moins un mois avant cette date " ; <br/>
<br/>
              5. Considérant qu'il ne résulte pas de ces dispositions que le délai ouvert au responsable du traitement pour formuler des observations écrites en réponse au rapport qui lui a été notifié par la CNIL doive être cumulé avec celui dans lequel il est informé de la date de la séance de la commission à l'ordre du jour de laquelle est inscrite l'affaire qui le concerne ; <br/>
<br/>
              6. Considérant que la CNIL soutient, sans être contredite par l'association requérante, que cette dernière a été informée par notification d'huissier le 6 juin 2011, à l'adresse de son siège social mentionnée dans ses statuts publiés sur son site internet, de ce que l'affaire la concernant serait examinée au cours d'une séance de la commission le 12 juillet suivant ; que, dès lors, le moyen tiré de la méconnaissance des dispositions de l'article 76 du décret du 20 octobre 2005 manque en fait ; que la circonstance que cette notification est intervenue de manière concomitante avec la transmission du rapport prévu par l'article 46 de la loi du 6 janvier 1978 est sans incidence sur sa régularité ;<br/>
<br/>
              En ce qui concerne le bien-fondé de la sanction : <br/>
<br/>
              7. Considérant qu'aux termes de l'article 2 de la loi du 6 janvier 1978 : " (...) Constitue une donnée à caractère personnel toute information relative à une personne physique identifiée ou qui peut être identifiée, directement ou indirectement, par référence à un numéro d'identification ou à un ou plusieurs éléments qui lui sont propres (...)./ Constitue un traitement de données à caractère personnel toute opération ou tout ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, (...) l'organisation, (...) la communication par transmission, diffusion (...)/ Constitue un fichier de données à caractère personnel tout ensemble structuré et stable de données à caractère personnel accessibles selon des critères déterminés (...) " ; qu'aux termes de l'article 38 de la même loi : " Toute personne physique a le droit de s'opposer, pour des motifs légitimes, à ce que des données à caractère personnel la concernant fassent l'objet d'un traitement " ;<br/>
<br/>
              8. Considérant qu'il résulte de ces dispositions que la mise en ligne sur le réseau internet d'une base de données de jurisprudence non totalement anonymisée, telle que celle gérée par l'association requérante, doit être regardée comme un traitement automatisé de données à caractère personnel au sens de la loi du 6 janvier 1978, auquel s'applique le droit d'opposition qu'elle ouvre aux personnes concernées ;<br/>
<br/>
              9. Considérant, en premier lieu, que dans la mise en demeure adressée à l'association requérante, la CNIL lui demandait de respecter ce droit d'opposition et faisait état des plaintes de quatre personnes demandant que soient retirées de la base de données litigieuse, ou anonymisées, des décisions de justice dans lesquelles leur nom était mentionné, deux de ces personnes ayant en outre indiqué à la commission qu'elles avaient essayé en vain d'obtenir ces mesures auprès de l'association requérante ; que si cette dernière soutient avoir procédé à l'anonymisation des décisions litigieuses avant la réception de cette mise en demeure, elle n'apporte aucun élément à l'appui de cette affirmation et ne conteste pas, en outre, les difficultés d'exercice du droit d'opposition signalées par deux des plaignants dans leurs réclamations ;<br/>
<br/>
              10. Considérant, en deuxième lieu, que, contrairement à ce que soutient la requérante, les manquements au droit d'opposition relevés par la commission dans les motifs de la décision litigieuse sont ceux qui avaient été ainsi mentionnés dans la mise en demeure, laquelle était suffisamment motivée, et exposés à nouveau dans le rapport, communiqué préalablement à l'association et proposant une sanction à son encontre ; qu'il suit de là que, d'une part, le moyen tiré de ce que la décision ne serait pas fondée sur des cas précis de non-respect du droit d'opposition doit être écarté et que, d'autre part, la commission pouvait valablement estimer, au vu des éléments ainsi rassemblés, que le traitement mis en oeuvre par l'association ne permettait pas le respect effectif du droit d'opposition prévu à l'article 38 de la loi du 6 janvier 1978 ; que la commission pouvait légalement mentionner dans sa délibération qu'elle avait, en outre, reçu après la mise en demeure, d'autres plaintes de particuliers faisant état de leur impossibilité d'exercer leur droit d'opposition, dès lors que la mention de ces faits, sur lesquels la sanction n'est pas fondée, se bornait à en corroborer le motif ;<br/>
<br/>
              11. Considérant, en troisième lieu, qu'aux termes de l'article 92 du décret du 20 octobre 2005 : " Les demandes tendant à la mise en oeuvre des droits prévus aux articles 38 à 40 de la loi du 6 janvier 1978 susvisée, lorsqu'elles sont présentées par écrit au responsable du traitement, sont signées et accompagnées de la photocopie d'un titre d'identité portant la signature du titulaire (...) " ; que l'article 94 du même décret dispose que cette procédure est régularisable ; que, si l'association requérante soutient que la CNIL ne pouvait fonder sa décision sur des plaintes de particuliers invoquant la méconnaissance de leur droit d'opposition sans avoir vérifié, au préalable, si ces personnes avaient présenté une demande dans les conditions prévues par ces dispositions, il résulte de l'instruction que certaines des plaintes reçues par la CNIL étaient fondées sur l'impossibilité même, pour les demandeurs, de contacter le gestionnaire du site pour lui faire part de leur demande de retrait ou d'anonymisation, l'adresse postale mentionnée sur le site internet de l'association, à cet effet, n'étant pas utilisable, ce qui n'est pas contesté par l'association requérante ;<br/>
<br/>
              12. Considérant, en quatrième lieu, que l'article 21 de la loi du 6 janvier 1978 dispose que " (...) les détenteurs ou utilisateurs de traitements ou de fichiers de données à caractère personnel ne peuvent s'opposer à l'action de la commission ou de ses membres et doivent au contraire prendre toutes mesures utiles afin de faciliter sa tâche (...) " ; que, contrairement à ce que soutient la requérante, la CNIL pouvait se fonder sur la méconnaissance de ces dispositions dès lors qu'elle s'est abstenue de répondre à plusieurs courriers et convocations envoyés par la commission, caractérisant ainsi une volonté délibérée de ne pas se soumettre aux demandes de cette dernière ; <br/>
<br/>
              13. Considérant, en cinquième lieu, qu'il résulte du texte même de la délibération attaquée que le moyen tiré de ce que la sanction pécuniaire prononcée serait exclusivement fondée sur la mauvaise foi de la requérante manque en fait ;<br/>
<br/>
              14. Considérant, en sixième lieu, qu'eu égard aux conséquences que peuvent avoir, pour les personnes concernées, la mise et le maintien en ligne de décisions de justice non anonymisées, la sanction pécuniaire de 10 000 euros, l'injonction de cesser le traitement litigieux et la publication de la décision attaquée sur le site internet de la commission ainsi que dans trois quotidiens sont proportionnées à la gravité des manquements relevés ; que la circonstance, à la supposer établie, que des décisions anonymisées tardivement par l'association requérante soient restées accessibles sur internet via des moteurs de recherche n'est pas, en tout état de cause, de nature à l'exonérer de sa responsabilité résultant des manquements sanctionnés par la délibération attaquée ;<br/>
<br/>
              15. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que les conclusions de l'association Lexeek pour l'accès au droit tendant à l'annulation de la délibération du 30 août 2011 doivent être rejetées, ainsi que, par suite, ses conclusions à fin d'injonction ;<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              17. Considérant, d'une part, que, dès lors que la délibération contestée n'est pas entachée d'illégalité, elle ne saurait constituer une faute de nature à ouvrir droit à réparation ; <br/>
<br/>
              18. Considérant, d'autre part, qu'aux termes du deuxième alinéa de l'article 78 du décret du 20 octobre 2005, dans sa rédaction applicable en l'espèce, avant l'entrée en vigueur du décret n° 2011-2023 du 29 décembre 2011 : " La publication de la décision de sanction, lorsqu'elle est décidée le cas échéant par la commission, intervient dans le délai d'un mois à compter du jour où la sanction est devenue définitive. " ;<br/>
<br/>
              19. Considérant qu'il résulte de l'instruction que la CNIL, en méconnaissance de ces dispositions, a publié sur son site internet la délibération litigieuse du 30 août 2011 dès le mois de septembre 2011, alors que la sanction prononcée à l'encontre de l'association Lexeek pour l'accès au droit n'avait pas encore revêtu de caractère définitif ; que l'illégalité ainsi commise est constitutive d'une faute de nature à engager la responsabilité de l'administration ; <br/>
<br/>
              20. Considérant, toutefois, que pour justifier du préjudice qu'elle soutient avoir subi, l'association Lexeek pour l'accès au droit se borne à affirmer que la publication prématurée de cette décision, abondamment commentée par les médias, lui aurait fait perdre une chance sérieuse de développer de nouveaux partenariats, alors que son projet était jusqu'alors soutenu par la commune d'Aix-en-Provence, le département des Bouches-du-Rhône et le ministère de la jeunesse et des sports ; que, ce faisant, elle n'apporte pas d'élément permettant d'établir le caractère certain du préjudice qu'elle allègue ; <br/>
<br/>
              21. Considérant qu'il résulte de ce qui précède que les conclusions indemnitaires de l'association Lexeek ne peuvent être accueillies ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              22. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que l'association Lexeek pour l'accès au droit demande au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association Lexeek pour l'accès au droit sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'association Lexeek pour l'accès au droit, à la Commission nationale de l'informatique et des libertés et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-01-01-02 DROITS CIVILS ET INDIVIDUELS. - INCLUSION - MISE EN LIGNE SUR INTERNET D'UNE BASE DE DONNÉES DE JURISPRUDENCE NON TOTALEMENT ANONYMISÉE - CONSÉQUENCE - APPLICABILITÉ DU DROIT D'OPPOSITION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-05-01 DROITS CIVILS ET INDIVIDUELS. - APPLICABILITÉ - MISE EN LIGNE SUR INTERNET D'UNE BASE DE DONNÉES DE JURISPRUDENCE NON TOTALEMENT ANONYMISÉE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-07-10-03 DROITS CIVILS ET INDIVIDUELS. - PROCÉDURE DE SANCTION - CUMUL ENTRE LE DÉLAI DE RÉPONSE AU RAPPORT ET LE DÉLAI DE CONVOCATION À LA SÉANCE DE LA COMMISSION - ABSENCE.
</SCT>
<ANA ID="9A"> 26-07-01-01-02 Il résulte des dispositions des articles 2 et 38 de la loi n° 78-17 du 6 janvier 1978 que la mise en ligne sur le réseau internet d'une base de données de jurisprudence non totalement anonymisée doit être regardée comme un traitement automatisé de données à caractère personnel au sens de la loi du 6 janvier 1978, auquel s'applique le droit d'opposition qu'elle ouvre aux personnes concernées.</ANA>
<ANA ID="9B"> 26-07-05-01 Il résulte des dispositions des articles 2 et 38 de la loi n° 78-17 du 6 janvier 1978 que la mise en ligne sur le réseau internet d'une base de données de jurisprudence non totalement anonymisée doit être regardée comme un traitement automatisé de données à caractère personnel au sens de la loi du 6 janvier 1978, auquel s'applique le droit d'opposition qu'elle ouvre aux personnes concernées.</ANA>
<ANA ID="9C"> 26-07-10-03 Il ne résulte ni des dispositions de l'article 46 de la loi n° 78-17 du 6 janvier 1978 ni de celles des articles 75 et 76 du décret n° 2005-1309 du 20 octobre 2005 que le délai ouvert au responsable du traitement pour formuler des observations écrites en réponse au rapport qui lui a été notifié par la CNIL doive être cumulé avec celui dans lequel il est informé de la date de la séance de la commission à l'ordre du jour de laquelle est inscrite l'affaire qui le concerne.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
