<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037612947</ID>
<ANCIEN_ID>JG_L_2018_11_000000418788</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/29/CETATEXT000037612947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 14/11/2018, 418788, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418788</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418788.20181114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1602997 du 1er mars 2018, enregistrée le 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Dijon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 27 octobre 2016 au greffe de ce tribunal, présentée par M. David de Jésus. Par cette requête, deux mémoires, un mémoire en réplique et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 4 mai, 5 juin, 14 septembre et 24 octobre 2018, M. de Jésus demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du 30 juillet 2016 par laquelle le directeur du centre de détention de Joux-la-Ville a refusé d'abroger la tarification des services téléphoniques imposée aux détenus de l'établissement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code de la consommation, notamment son article L. 113-4 ;<br/>
              - le code des postes et communications électroniques ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. David de Jésus;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 novembre 2018, présentée par la garde des sceaux, ministre de la justice ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.de Jésus, détenu au centre de détention de Joux-la-Ville, a demandé l'abrogation de la tarification applicable aux communications téléphoniques dans les établissements pénitentiaires telle qu'elle résulte du contrat de délégation de service public conclu le 11 mai 2007 et prolongé depuis lors par trois avenants des 10 février 2009, 17 avril 2015 et 7 juillet 2017. M. de Jésus demande l'annulation pour excès de pouvoir du refus qui a été opposé à sa demande.<br/>
<br/>
              2. S'agissant d'une concession de service public, revêtent un caractère réglementaire les clauses qui en définissent l'objet ainsi que celles qui fixent les tarifs applicables aux usagers de ce service.<br/>
<br/>
              3. L'article 39 de la loi du 24 novembre 2009 pénitentiaire dispose que : " Les personnes détenues ont le droit de téléphoner aux membres de leur famille. Elles peuvent être autorisées à téléphoner à d'autres personnes pour préparer leur réinsertion. Dans tous les cas, les prévenus doivent obtenir l'autorisation de l'autorité judiciaire. L'accès au téléphone peut être refusé, suspendu ou retiré, pour des motifs liés au maintien du bon ordre et de la sécurité ou à la prévention des infractions et, en ce qui concerne les prévenus, aux nécessités de l'information. Le contrôle des communications téléphoniques est effectué conformément à l'article 727-1 du code de procédure pénale ". <br/>
<br/>
              4. M. de Jésus soutient que les clauses litigieuses du contrat du 11 mai 2007 fixent des tarifs manifestement disproportionnés au regard du service rendu et méconnaissent ainsi la règle d'équivalence entre le tarif d'une redevance et la valeur de la prestation ou du service. <br/>
<br/>
              Sur le tarif des communications téléphoniques :<br/>
<br/>
              5. En premier lieu, le droit de téléphoner des personnes détenues est consacré par l'article 39 de la loi du 24 novembre 2009 cité au point 3. Il s'exerce dans les limites inhérentes à la détention et dans les conditions particulières en résultant, notamment l'absence de libre choix de l'opérateur de téléphonie. Eu égard à la différence de situation objective existant entre les personnes détenues qui souhaitent téléphoner et les autres usagers d'un service de téléphonie, la circonstance que le tarif des communications téléphoniques, tel qu'il est fixé par les clauses réglementaires du contrat litigieux, est établi à un niveau plus élevé que celui dont bénéficient, en moyenne, les autres usagers du téléphone ne caractérise pas une rupture du principe d'égalité dès lors qu'il ne ressort pas des pièces du dossier que cette différence de tarif soit manifestement disproportionnée. Il ne ressort pas davantage des pièces du dossier que les modalités spécifiques retenues pour le calcul de ce tarif caractérisent par elles-mêmes, une rupture du principe d'égalité, les structures de coût du réseau exploité dans le cadre de la concession litigieuse n'étant pas comparables à celles des autres opérateurs de téléphonie.<br/>
<br/>
              6. En deuxième lieu, il résulte de la différence de situation dans laquelle sont placés les détenus que le requérant ne saurait utilement invoquer la méconnaissance de l'article 35-1 du code des postes et communications électroniques relatif à  l'accès au service universel des communications électroniques, en tant qu'il garantit à chacun " un droit au raccordement " à un service téléphonique, ni celle des dispositions de l'article L. 113-4 du code de la consommation fixant les obligations contractuelles des opérateurs de téléphonie vocale à l'égard de leurs clients souscrivant à un service de communications électroniques.<br/>
<br/>
              7. En troisième lieu, il résulte des motifs énoncés aux points précédents qu'eu égard à leur montant, d'une part, et aux dispositifs mis en place par l'administration pénitentiaire pour garantir aux détenus dépourvus des ressources suffisantes un accès effectif au téléphone, d'autre part, les tarifs des communications téléphoniques, tels qu'ils sont fixés par les clauses litigieuses du contrat du 11 mai 2007, ne méconnaissent, par eux-mêmes, ni le principe de dignité de la personne humaine, ni le droit au respect de la vie privée et familiale ni la liberté d'information et de communication.<br/>
<br/>
              Sur l'étendue des prestations financées par le tarif des communications téléphoniques : <br/>
<br/>
              8. D'une part, il ressort de l'objet et des termes mêmes du contrat litigieux que celui-ci confie au délégataire, la société SAGI, deux missions distinctes respectivement relatives à " l'exploitation d'équipements de réseau de téléphonie fixe " dans les établissements pénitentiaires et " au contrôle des communications téléphoniques ". <br/>
<br/>
              9. D'autre part, l'article 3 du contrat litigieux prévoit que " le délégataire se rémunère sur le prix des communications téléphoniques ". Il résulte de l'annexe financière au contrat que l'amortissement des fournitures et des prestations est réalisé à travers les ventes des communications téléphoniques effectuées par les détenus. Or figurent parmi ces prestations, des " spécifications fonctionnelles " énumérées à l'article 2.2.1 du contrat, permettant d'assurer l'écoute, l'enregistrement et l'archivage des conversations téléphoniques. Ces prestations qui permettent d'assurer le contrôle des communications téléphoniques conformément aux dispositions de l'article 727-1 du code de procédure pénale se rattachent aux missions générales de police qui, par nature, incombent à l'Etat. Les dépenses auxquelles elles donnent lieu, qui ne sont pas exposées dans l'intérêt direct des détenus, ne sauraient dès lors être financées par le tarif des communications téléphoniques perçu auprès des usagers en contrepartie du service qui leur est rendu. <br/>
<br/>
              10. Il résulte de tout ce qui précède que M. de Jésus n'est fondé à demander l'annulation du refus d'abroger les clauses réglementaires du contrat litigieux qu'en tant qu'elles prévoient que les dépenses relatives aux prestations fournies par le délégataire afin de procéder au contrôle des communications téléphoniques des détenus sont financées au moyen du tarif de ces communications.<br/>
<br/>
              11. M. de Jésus ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Didier Le Prado, avocat de M.de Jésus, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat une somme de 1 500 euros, à verser à Me Didier Le Prado, avocat de M.de Jésus.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le refus d'abroger les clauses réglementaires du contrat du 11 mai 2007 est annulé en tant qu'elles prévoient le financement par le tarif des communications téléphoniques des dépenses relatives aux prestations qui permettent d'en assurer le contrôle.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : L'Etat versera à Me Didier Le Prado, avocat de M.de Jésus, une somme de 1 500 euros au titre des articles L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Monsieur David de Jésus et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DES USAGERS DEVANT LE SERVICE PUBLIC. - DSP RELATIVE AUX COMMUNICATIONS TÉLÉPHONIQUES DANS LES ÉTABLISSEMENTS PÉNITENTIAIRES - DIFFÉRENCE DE SITUATION ENTRE LES PERSONNES DÉTENUES ET LES AUTRES USAGERS D'UN SERVICE DE TÉLÉPHONIE - EXISTENCE - CONSÉQUENCE - CLAUSES RÉGLEMENTAIRES FIXANT DES TARIFS DE COMMUNICATION PLUS ÉLEVÉS EN DÉTENTION - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE, SAUF EN CAS DE DIFFÉRENCE DE TARIF MANIFESTEMENT DISPROPORTIONNÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DSP RELATIVE AUX COMMUNICATIONS TÉLÉPHONIQUES DANS LES ÉTABLISSEMENTS PÉNITENTIAIRES - CLAUSES RÉGLEMENTAIRES - 1) CLAUSES FIXANT DES TARIFS DE COMMUNICATION POUR LES PERSONNES DÉTENUES PLUS ÉLEVÉS QUE CEUX DONT BÉNÉFICIENT LES AUTRES USAGERS D'UN SERVICE DE TÉLÉPHONIE - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE, COMPTE TENU DE LA DIFFÉRENCE DE SITUATION OBJECTIVE, SAUF EN CAS DE DIFFÉRENCE DE TARIF MANIFESTEMENT DISPROPORTIONNÉE - 2) PRESTATIONS DONT LE FINANCEMENT PEUT ÊTRE ASSURÉ PAR LES USAGERS DU SERVICE - NOTION - DÉPENSES EXPOSÉES DANS L'INTÉRÊT DIRECT DES USAGERS [RJ1] - CAS DES CONTRÔLES DES COMMUNICATIONS ÉLECTRONIQUES (ART. 727-1 DU CPP) - EXCLUSION, CES PRESTATIONS SE RATTACHANT AUX MISSIONS GÉNÉRALES DE POLICE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - DSP RELATIVE AUX COMMUNICATIONS TÉLÉPHONIQUES DANS LES ÉTABLISSEMENTS PÉNITENTIAIRES - CLAUSES RÉGLEMENTAIRES - 1) CLAUSES FIXANT DES TARIFS DE COMMUNICATION POUR LES PERSONNES DÉTENUES PLUS ÉLEVÉS QUE CEUX DONT BÉNÉFICIENT LES AUTRES USAGERS D'UN SERVICE DE TÉLÉPHONIE - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE, COMPTE TENU DE LA DIFFÉRENCE DE SITUATION OBJECTIVE, SAUF EN CAS DE DIFFÉRENCE DE TARIF MANIFESTEMENT DISPROPORTIONNÉE - 2) PRESTATIONS DONT LE FINANCEMENT PEUT ÊTRE ASSURÉ PAR LES USAGERS DU SERVICE - NOTION - DÉPENSES EXPOSÉES DANS L'INTÉRÊT DIRECT DES USAGERS [RJ1] - CAS DES CONTRÔLES DES COMMUNICATIONS ÉLECTRONIQUES (ART. 727-1 DU CPP) - EXCLUSION, CES PRESTATIONS SE RATTACHANT AUX MISSIONS GÉNÉRALES DE POLICE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">51-02-01-005 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. TÉLÉPHONE. QUESTIONS GÉNÉRALES RELATIVES AU FONCTIONNEMENT DU SERVICE TÉLÉPHONIQUE. - DSP RELATIVE AUX COMMUNICATIONS TÉLÉPHONIQUES DANS LES ÉTABLISSEMENTS PÉNITENTIAIRES - 1) CLAUSES FIXANT DES TARIFS DE COMMUNICATION POUR LES PERSONNES DÉTENUES PLUS ÉLEVÉS QUE CEUX DONT BÉNÉFICIENT LES AUTRES USAGERS D'UN SERVICE DE TÉLÉPHONIE - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE, COMPTE TENU DE LA DIFFÉRENCE DE SITUATION OBJECTIVE, SAUF EN CAS DE DIFFÉRENCE DE TARIF MANIFESTEMENT DISPROPORTIONNÉE - 2) PRESTATIONS DONT LE FINANCEMENT PEUT ÊTRE ASSURÉ PAR LES USAGERS DU SERVICE - NOTION - DÉPENSES EXPOSÉES DANS L'INTÉRÊT DIRECT DES USAGERS [RJ1] - CAS DES CONTRÔLES DES COMMUNICATIONS ÉLECTRONIQUES (ART. 727-1 DU CPP) - EXCLUSION, CES PRESTATIONS SE RATTACHANT AUX MISSIONS GÉNÉRALES DE POLICE.
</SCT>
<ANA ID="9A"> 01-04-03-03-03 Le droit de téléphoner des personnes détenues est consacré par l'article 39 de la loi n° 2009-1436 du 24 novembre 2009. Il s'exerce dans les limites inhérentes à la détention et dans les conditions particulières en résultant, notamment l'absence de libre choix de l'opérateur de téléphonie. Eu égard à la différence de situation objective existant entre les personnes détenues qui souhaitent téléphoner et les autres usagers d'un service de téléphonie, la circonstance que le tarif des communications téléphoniques, tel qu'il est fixé par les clauses réglementaires du contrat litigieux, est établi à un niveau plus élevé que celui dont bénéficient, en moyenne, les autres usagers du téléphone ne caractérise pas une rupture du principe d'égalité dès lors qu'il ne ressort pas des pièces du dossier que cette différence de tarif soit manifestement disproportionnée. Il ne ressort pas davantage des pièces du dossier que les modalités spécifiques retenues pour le calcul de ce tarif caractérisent par elles-mêmes, une rupture du principe d'égalité, les structures de coût du réseau exploité dans le cadre de la concession litigieuse n'étant pas comparables à celles des autres opérateurs de téléphonie.</ANA>
<ANA ID="9B"> 37-05-02-01 1) Le droit de téléphoner des personnes détenues est consacré par l'article 39 de la loi n° 2009-1436 du 24 novembre 2009. Il s'exerce dans les limites inhérentes à la détention et dans les conditions particulières en résultant, notamment l'absence de libre choix de l'opérateur de téléphonie. Eu égard à la différence de situation objective existant entre les personnes détenues qui souhaitent téléphoner et les autres usagers d'un service de téléphonie, la circonstance que le tarif des communications téléphoniques, tel qu'il est fixé par les clauses réglementaires du contrat litigieux, est établi à un niveau plus élevé que celui dont bénéficient, en moyenne, les autres usagers du téléphone ne caractérise pas une rupture du principe d'égalité dès lors qu'il ne ressort pas des pièces du dossier que cette différence de tarif soit manifestement disproportionnée. Il ne ressort pas davantage des pièces du dossier que les modalités spécifiques retenues pour le calcul de ce tarif caractérisent par elles-mêmes, une rupture du principe d'égalité, les structures de coût du réseau exploité dans le cadre de la concession litigieuse n'étant pas comparables à celles des autres opérateurs de téléphonie.,,2) Contrat prévoyant le financement de certaines prestations à travers les ventes des communications téléphoniques effectuées par les détenus, parmi lesquelles les spécifications fonctionnelles permettant d'assurer l'écoute, l'enregistrement et l'archivage des communications électroniques. De telles prestations qui permettent d'assurer le contrôle des communications téléphoniques conformément aux dispositions de l'article 727-1 du code de procédure pénale (CPP) se rattachent aux missions générales de police qui, par nature, incombent à l'Etat. Les dépenses auxquelles elles donnent lieu, qui ne sont pas exposées dans l'intérêt direct des détenus, ne sauraient dès lors être financées par le tarif des communications téléphoniques perçu auprès des usagers en contrepartie du service qui leur est rendu.</ANA>
<ANA ID="9C"> 39-01-03-03 1) Le droit de téléphoner des personnes détenues est consacré par l'article 39 de la loi n° 2009-1436 du 24 novembre 2009. Il s'exerce dans les limites inhérentes à la détention et dans les conditions particulières en résultant, notamment l'absence de libre choix de l'opérateur de téléphonie. Eu égard à la différence de situation objective existant entre les personnes détenues qui souhaitent téléphoner et les autres usagers d'un service de téléphonie, la circonstance que le tarif des communications téléphoniques, tel qu'il est fixé par les clauses réglementaires du contrat litigieux, est établi à un niveau plus élevé que celui dont bénéficient, en moyenne, les autres usagers du téléphone ne caractérise pas une rupture du principe d'égalité dès lors qu'il ne ressort pas des pièces du dossier que cette différence de tarif soit manifestement disproportionnée. Il ne ressort pas davantage des pièces du dossier que les modalités spécifiques retenues pour le calcul de ce tarif caractérisent par elles-mêmes, une rupture du principe d'égalité, les structures de coût du réseau exploité dans le cadre de la concession litigieuse n'étant pas comparables à celles des autres opérateurs de téléphonie.,,2) Contrat prévoyant le financement de certaines prestations à travers les ventes des communications téléphoniques effectuées par les détenus, parmi lesquelles les spécifications fonctionnelles permettant d'assurer l'écoute, l'enregistrement et l'archivage des communications électroniques. De telles prestations qui permettent d'assurer le contrôle des communications téléphoniques conformément aux dispositions de l'article 727-1 du code de procédure pénale (CPP) se rattachent aux missions générales de police qui, par nature, incombent à l'Etat. Les dépenses auxquelles elles donnent lieu, qui ne sont pas exposées dans l'intérêt direct des détenus, ne sauraient dès lors être financées par le tarif des communications téléphoniques perçu auprès des usagers en contrepartie du service qui leur est rendu.</ANA>
<ANA ID="9D"> 51-02-01-005 1) Le droit de téléphoner des personnes détenues est consacré par l'article 39 de la loi n° 2009-1436 du 24 novembre 2009. Il s'exerce dans les limites inhérentes à la détention et dans les conditions particulières en résultant, notamment l'absence de libre choix de l'opérateur de téléphonie. Eu égard à la différence de situation objective existant entre les personnes détenues qui souhaitent téléphoner et les autres usagers d'un service de téléphonie, la circonstance que le tarif des communications téléphoniques, tel qu'il est fixé par les clauses réglementaires du contrat litigieux, est établi à un niveau plus élevé que celui dont bénéficient, en moyenne, les autres usagers du téléphone ne caractérise pas une rupture du principe d'égalité dès lors qu'il ne ressort pas des pièces du dossier que cette différence de tarif soit manifestement disproportionnée. Il ne ressort pas davantage des pièces du dossier que les modalités spécifiques retenues pour le calcul de ce tarif caractérisent par elles-mêmes, une rupture du principe d'égalité, les structures de coût du réseau exploité dans le cadre de la concession litigieuse n'étant pas comparables à celles des autres opérateurs de téléphonie.,,2) Contrat prévoyant le financement de certaines prestations à travers les ventes des communications téléphoniques effectuées par les détenus, parmi lesquelles les spécifications fonctionnelles permettant d'assurer l'écoute, l'enregistrement et l'archivage des communications électroniques. De telles prestations qui permettent d'assurer le contrôle des communications téléphoniques conformément aux dispositions de l'article 727-1 du code de procédure pénale (CPP) se rattachent aux missions générales de police qui, par nature, incombent à l'Etat. Les dépenses auxquelles elles donnent lieu, qui ne sont pas exposées dans l'intérêt direct des détenus, ne sauraient dès lors être financées par le tarif des communications téléphoniques perçu auprès des usagers en contrepartie du service qui leur est rendu.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur cette notion, CE, 30 octobre 1996,,, n°s 136071-142688, p. 387.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
