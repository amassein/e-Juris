<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230335</ID>
<ANCIEN_ID>JG_L_2017_03_000000392296</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230335.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 20/03/2017, 392296, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392296</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392296.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédures contentieuses antérieures :<br/>
<br/>
              M. C...B...et M. A... D...ont, chacun en ce qui le concerne, demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir les décisions du 21 mars 2007 par lesquelles le ministre de l'emploi, de la cohésion sociale et du logement a autorisé la société Total Petrochemicals France à les mettre à la retraite. Par deux jugements n° 0702546 et n° 0702547 du 10 mai 2011, le tribunal administratif a annulé ces deux décisions. <br/>
<br/>
              Par deux arrêts n° 11NC01119 et n° 11NC01116 du 3 mai 2012, la cour administrative d'appel de Nancy a, sur les appels de la société Total Petrochemicals France, annulé ces deux jugements.<br/>
<br/>
              Par une décision n° 360685, 360686 du 8 octobre 2014, le Conseil d'Etat, statuant au contentieux a annulé ces deux arrêts et a renvoyé les affaires devant la cour administrative d'appel de Nancy.<br/>
<br/>
              Par deux arrêts n° 14NC01908, 15NC00605 et n° 14NC01907, 15NC00606 du 2 juin 2015, la cour administrative d'appel de Nancy a rejeté les appels formés par la société Total Petrochemicals France contre les jugements du tribunal administratif de Nancy du 10 mai 2011.<br/>
<br/>
<br/>
              Procédures devant le Conseil d'Etat :<br/>
<br/>
              1° Sous le no 392296, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 août et 3 novembre 2015 et le 21 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Total Petrochemicals France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nancy n° 14NC01908, 15NC00605 du 2 juin 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel formé contre le jugement du tribunal administratif de Strasbourg n° 0702546 du 10 mai 2011 ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 392301, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 août et 3 novembre 2015 et le 21 juin 2016, la société Total Petrochemicals France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nancy n° 14NC01907, 15NC00606 du 2 juin 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel formé contre le jugement du tribunal administratif de Strasbourg n° 0702547 du 10 mai 2011 ;<br/>
<br/>
              3°) de mettre à la charge de M. D... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le décret n° 46-2769 du 27 novembre 1946, modifié notamment par le décret n° 67-1228 du 22 décembre 1967 ;<br/>
              - le décret n° 54-51 du 16 janvier 1954 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Total Petrochemicals France, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et de M. D...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société Total Petrochemicals France présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              2.  Considérant que les moyens tirés de ce que les minutes des arrêts attaqués ne comportent pas toutes les signatures exigées par l'article R. 741-7 du code de justice administrative manquent en fait ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que M. D... et M. B..., salariés protégés, ont été recrutés respectivement en 1960 et 1961 par les Houillères du bassin de Lorraine, établissement public industriel et commercial ; que leurs contrats ont ensuite été repris, à compter du 1er janvier 1968, par la société chimique des charbonnages, société anonyme de droit privé ; qu'à cette époque, en application des dispositions du décret du 22 décembre 1967 relatif au régime spécial de la sécurité sociale dans les mines, ils ont demandé à rester affiliés à ce régime au titre des risques vieillesse et invalidité ; qu'enfin, le 16 août 2006, leur employeur, devenu la société Total Petrochemicals France, a demandé à l'inspecteur du travail l'autorisation de les mettre à la retraite, sur le fondement des dispositions de l'article L. 122-14-13 du code du travail ; que, par deux décisions du 13 octobre 2006, l'inspecteur du travail de la deuxième section de la Moselle a accordé les autorisations demandées ; que, toutefois, par deux décisions du 21 mars 2007, le ministre chargé du travail a annulé les décisions de l'inspecteur du travail au motif que, à ses yeux, ces deux salariés ne relevaient pas des dispositions de l'article L. 122-14-13 du code du travail mais d'un statut particulier applicable aux agents affiliés à la caisse de retraite des mines ; que le ministre a, sur ce nouveau fondement, autorisé les mises à la retraite en cause ; que, par deux jugements du 10 mai 2011, le tribunal administratif de Strasbourg a annulé les décisions du ministre ; que, par deux arrêts du 2 juin 2015, contre lesquels la société Total Petrochemicals France se pourvoit en cassation, la cour administrative d'appel de Nancy a rejeté ses appels contre ces deux jugements ;<br/>
<br/>
              4. Considérant que, lorsque l'autorité administrative estime que le motif pour lequel un employeur lui demande l'autorisation de rompre le contrat de travail d'un salarié protégé n'est pas fondé, elle ne peut légalement accorder l'autorisation demandée en lui substituant un autre motif de rupture de ce contrat de travail, alors même que cet autre motif aurait été de nature, s'il avait été présenté par l'employeur, à justifier un telle rupture ; <br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article L. 122-14-13 du code du travail, sur le fondement duquel, ainsi qu'il a été dit ci-dessus, la société Total Petrochemicals France a demandé les autorisations de mise à la retraite de M. D... et de M.B..., devenu l'article L. 1237-5 du même code : " La mise à la retraite s'entend de la possibilité donnée à l'employeur de rompre le contrat de travail d'un salarié ayant atteint l'âge visé au 1° de l'article L. 351-8 du code de la sécurité sociale (...) " ; que, dans le cas où une demande d'autorisation de rupture du contrat de travail est ainsi motivée par la survenance de l'âge à partir duquel un salarié peut être mis à la retraite par décision de l'employeur, il appartient à l'autorité administrative de vérifier, sous le contrôle du juge de l'excès de pouvoir, d'une part que les conditions légales de mise à la retraite sont remplies et, d'autre part, que la mesure envisagée n'est pas en rapport avec les fonctions représentatives exercées ou l'appartenance syndicale de l'intéressé ; que l'autorité administrative a également la faculté de refuser l'autorisation sollicitée pour des motifs d'intérêt général, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence ;<br/>
<br/>
              6. Considérant, d'autre part, que l'article 2 du décret du 16 janvier 1954 portant application à certains personnels des entreprises minières et assimilées des dispositions du décret du 9 août 1953 relatif au régime des retraites des personnels de l'Etat et des services publics, sur lequel le ministre chargé du travail a, ainsi qu'il a également déjà été dit, entendu se fonder pour autoriser la mise à la retraite de M. D... et de M.B..., dispose que : " L'âge limite de maintien en activité des personnels désignés à l'article 1er du présent décret est l'âge fixé pour l'ouverture du droit à rente ou pension de retraite par les dispositions du premier alinéa de l'article 146 du décret n° 46-2769 du 27 novembre 1946 portant organisation de la sécurité sociale dans les mines " ; que, dans le cas où l'autorité administrative statue sur une demande d'autorisation de rupture du contrat de travail au regard d'un texte qui, comme ces dispositions, fixe un âge limite de maintien en activité, il lui appartient de s'assurer que les conditions légales de cette cessation d'activité sont remplies ; qu'il lui appartient également, le cas échéant, de s'assurer que le salarié n'a pas manifesté le souhait de bénéficier de dispositions permettant son maintien en activité au-delà de cette limite d'âge ; qu'à défaut de telles dispositions ou si le salarié n'en a pas demandé le bénéfice, l'administration ne peut légalement s'opposer à l'autorisation demandée ; qu'en revanche, s'il apparaît que l'employeur a refusé le bénéfice d'un maintien en activité au-delà de la limite d'âge, il appartient à l'autorité administrative de vérifier que la mesure envisagée n'est pas en rapport avec les fonctions représentatives exercées ou l'appartenance syndicale de l'intéressé et, en outre, qu'il n'y a pas lieu de refuser l'autorisation sollicitée pour des motifs d'intérêt général ;<br/>
<br/>
              7. Considérant qu'eu égard à la différence de nature entre une demande d'autorisation de cessation d'activité présentée sur le fondement de l'article L. 122-14-13 du code du travail,  qui traduit un choix de l'employeur et appelle, de la part de l'administration, les contrôles mentionnés au point 5, et une demande d'autorisation de cessation d'activité présentée sur le fondement du décret du 16 janvier 1954, pour laquelle s'impose le respect d'une limite d'âge et qui appelle, de la part de l'administration, les contrôles mentionnés au point 6, ces deux demandes doivent être regardées comme fondées sur des motifs distincts de rupture du contrat de travail ; que la cour administrative d'appel de Nancy a, par suite, exactement qualifié les faits dont elle était saisie et n'a pas commis d'erreur de droit en jugeant que le ministre avait accordé les autorisations demandées en se fondant sur un autre motif que celui pour lequel l'employeur avait formulé ses demandes ;<br/>
<br/>
              8. Considérant, par suite, qu'il résulte de ce qui a été dit au point 4 que la cour n'a pas commis d'erreur de droit en jugeant que cette substitution opérée par le ministre était, quel que fût le régime juridique applicable aux salariés en cause, entachée d'illégalité ; qu'elle n'a pas davantage entaché son arrêt d'erreur de droit en en déduisant, sans examiner si l'erreur ainsi commise avait ou non privé les intéressés d'une garantie, que les décisions attaquées devaient être annulées ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Total Petrochemicals France n'est pas fondée à demander l'annulation des arrêts qu'elle attaque ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Total Petrochemicals France une somme de 1 500 euros à verser à M. B... et une même somme de 1 500 euros à verser à M. D...au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... et de M. D..., qui ne sont pas les parties perdantes ; <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les pourvois de la société Total Petrochemicals France sont rejetés.<br/>
Article 2 : La société Total Petrochemicals France versera à M. B... et à M. D... la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Total Petrochemicals France, à M. C... B...et à M. A... D....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-03-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. POUVOIRS DE L'AUTORITÉ ADMINISTRATIVE. - 1) POSSIBILITÉ DE SUBSTITUER UN AUTRE MOTIF DE RUPTURE DU CONTRAT DE TRAVAIL À CELUI PRÉSENTÉ PAR L'EMPLOYEUR DANS SA DEMANDE - ABSENCE - 2) ESPÈCE - DEMANDE D'AUTORISATION DE MISE À LA RETRAITE D'UN SALARIÉ PROTÉGÉ - MOTIF DISTINCT DE LA DEMANDE FONDÉE SUR LE FAIT QUE LE SALARIÉ A ATTEINT UNE LIMITE D'ÂGE.
</SCT>
<ANA ID="9A"> 66-07-01-03-03 1) Lorsque l'autorité administrative estime que le motif pour lequel un employeur lui demande l'autorisation de rompre le contrat de travail d'un salarié protégé n'est pas fondé, elle ne peut légalement accorder l'autorisation demandée en lui substituant un autre motif de rupture de ce contrat de travail, alors même que cet autre motif aurait été de nature, s'il avait été présenté par l'employeur, à justifier une telle rupture.... ,,2) Eu égard à la différence de nature entre une demande d'autorisation de cessation d'activité présentée sur le fondement de l'article L. 122-14-13 du code du travail, qui traduit un choix de l'employeur et appelle, de la part de l'administration, les contrôles spécifiques, et une demande d'autorisation de cessation d'activité présentée sur le fondement du décret n° 54-51 du 16 janvier 1954, pour laquelle s'impose le respect d'une limite d'âge et qui appelle, de la part de l'administration, des contrôles plus limités, ces deux demandes doivent être regardées comme fondées sur des motifs distincts de rupture du contrat de travail.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
