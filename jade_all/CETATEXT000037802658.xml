<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802658</ID>
<ANCIEN_ID>JG_L_2018_12_000000417309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802658.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 12/12/2018, 417309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417309.20181212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 24 janvier 2014 par laquelle l'inspectrice du travail de la deuxième section de l'unité territoriale du Calvados a autorisé la société Trucks and Stores à le licencier. Par un jugement n° 1400557 du 21 mai 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT02245 du 14 novembre 2017, la cour administrative d'appel de Nantes a, sur appel de M. A..., annulé ce jugement et cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 janvier et 16 avril 2018 au secrétariat du contentieux du Conseil d'Etat, Me D...B..., liquidateur de la société Trucks and Stores, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A....<br/>
<br/>
              3°) de mettre à la charge de M. A... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Périer, avocat de MeB..., liquidateur de la société Trucks and Stores et à la SCP Didier, Pinet, avocat de M. C...A...;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un licenciement collectif pour motif économique, l'inspecteur du travail de la 2ème section de l'unité territoriale du Calvados a, le 24 janvier 2014, autorisé la société Trucks and Stores à licencier M. A... ; que, par un jugement du 21 mai 2015, le tribunal administratif de Caen a rejeté la demande de M. A... tendant à l'annulation de cette décision ; que MeB..., liquidateur de la société Trucks and Stores, se pourvoit en cassation contre l'arrêt du 14 novembre 2017 par lequel la cour administrative d'appel de Nantes a, à la demande de M. A..., annulé ce jugement et cette décision ;<br/>
<br/>
              2.  Considérant qu'aux termes de l'article L. 1233-28 du code du travail dans sa rédaction alors applicable : " L'employeur qui envisage de procéder à un licenciement collectif pour motif économique d'au moins dix salariés dans une même période de trente jours réunit et consulte, selon le cas, le comité d'entreprise ou les délégués du personnel (...) " ; qu'aux termes de son article L. 1233-36 : " Dans les entreprises dotées d'un comité central d'entreprise, l'employeur consulte le comité central et le ou les comités d'établissement intéressés dès lors que les mesures envisagées excèdent le pouvoir du ou des chefs d'établissement concernés ou portent sur plusieurs établissements simultanément (...) " ; que l'article L. 1233-31 dispose : " L'employeur adresse aux représentants du personnel, avec la convocation à la réunion, tous renseignements utiles sur le projet de licenciement collectif. / Il indique : / 1° La ou les raisons économiques, financières ou techniques du projet de licenciement ; (...) " ; qu'enfin, aux termes de l'article L. 2323-15 du même code : " Le comité d'entreprise est saisi en temps utile des projets de restructuration et de compression des effectifs. / Il émet un avis sur l'opération projetée et ses modalités d'application (...) " ; qu'en vertu de ces dispositions, il appartenait à l'administration, saisie d'une demande d'autorisation de licenciement d'un salarié protégé dans le cadre d'un licenciement collectif pour motif économique, de contrôler la régularité de la procédure de consultation du comité d'entreprise sur le projet de licenciement collectif et, notamment, de s'assurer que le comité avait disposé des éléments lui permettant de se prononcer en toute connaissance de cause sur le motif économique invoqué par l'employeur ; qu'à défaut, elle ne pouvait légalement accorder l'autorisation demandée ;<br/>
<br/>
              3.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Trucks and Stores, qui exerce en France une activité de vente au détail d'articles de coutellerie, de quincaillerie et d'outillage, détient, dans ce même secteur d'activité, une société dédiée à la distribution en métropole de la marque Outiror et deux sociétés implantées dans des collectivités d'outre-mer et représentant une faible partie du chiffre d'affaire global du groupe ; qu'il ressort des termes mêmes de la note de présentation du projet de réorganisation présenté par l'employeur et jointe à la convocation des membres du comité central d'entreprise de la société Trucks and Stores ainsi que des comités de ses établissements de Ouezy et Saint-Cyr-sur-Loire, consultés les 6 et 7 juin 2013 sur le projet de réorganisation, que cette note avait pour objet le " projet de réorganisation du groupe " ; qu'elle rappelait l'historique des réorganisations précédentes des sociétés du groupe dans le secteur intéressé, exposait la dégradation de la situation économique des deux sociétés exerçant en métropole et présentait de manière globale, dans un tableau, la baisse de l'activité des marques distribuées par les sociétés du groupe, tant en métropole qu'outre-mer ; que, par suite, en estimant que les comités d'entreprise et d'établissement n'avaient pas disposé des éléments de nature économique les mettant à même de se prononcer en toute connaissance de cause sur le projet de réorganisation, la cour administrative d'appel de Nantes a dénaturé les pièces du dossier qui lui étaient soumis ; que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, MeB..., liquidateur de la société Trucks and Stores, est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4.  Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de MeB..., liquidateur de la société Trucks and Stores ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme que Me B... demande au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par Me B..., liquidateur de la société Trucks and Stores, et par M. A..., au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Me B..., liquidateur de la société Trucks and Stores et à M. C... A....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
