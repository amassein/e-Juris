<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030559654</ID>
<ANCIEN_ID>JG_L_2015_05_000000371915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/96/CETATEXT000030559654.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 07/05/2015, 371915, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; BALAT</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371915.20150507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. C...et Mme B...A...ont demandé au tribunal administratif d'Orléans de condamner la commune de Dreux (Eure-et-Loir) à leur verser la somme de 61 000 euros en réparation du préjudice causé par l'illégalité de la décision de préemption de leur maison d'habitation prise par la commune le 2 septembre 2008, augmentée des intérêts au taux légal à compter du 1er octobre 2008, avec capitalisation des intérêts, ainsi que la somme de 959,86 euros correspondant aux indemnités contractuelles de remboursement anticipé de leur prêt immobilier et la somme de 10 000 euros à titre de dommages et intérêts. Par un jugement n° 1000258 du 4 octobre 2011, le tribunal administratif d'Orléans a condamné la commune de Dreux à verser à M. et Mme A...une somme de 31 000 euros, une somme correspondant aux intérêts au taux légal sur la somme de 131 000 euros pour la période du 18 septembre au 18 décembre 2008 et une somme correspondant aux intérêts au taux légal sur la somme de 30 000 euros à compter du 21 août 2009, et a rejeté le surplus des conclusions de la demande. <br/>
<br/>
              Par un arrêt n° 11NT03083 du 28 juin 2013, la cour administrative d'appel de Nantes, saisie d'un appel de M. et Mme A...et d'un appel incident de la commune de Dreux, a annulé le jugement du tribunal administratif d'Orléans et rejeté la demande présentée devant ce tribunal par M. et MmeA....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 septembre 2013, 4 décembre 2013 et 4 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nantes du 28 juin 2013 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel et de rejeter l'appel incident de la commune de Dreux ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Dreux la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. et MmeA..., et à la SCP Monod, Colin, Stoclet, avocat de la commune de Dreux ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A...ont, le 18 juin 2008, signé une promesse de vente portant sur leur pavillon, situé dans une zone soumise au droit de préemption urbain ; que leur notaire a, le 16 juillet 2008, adressé à la commune de Dreux une déclaration d'intention d'aliéner mentionnant un prix de vente de 131 000 euros, qui a été reçue le 21 juillet 2008 ; que, par une décision du 2 septembre 2008, le maire de la commune de Dreux a décidé d'exercer le droit de préemption sur ce bien au prix de 70 000 euros, alors qu'un avis du service des domaines l'évaluait à 138 000 euros ; que la décision de préemption a été suspendue par une ordonnance du 7 octobre 2008 du juge des référés du tribunal administratif d'Orléans ; que, toutefois, M. et Mme A... n'ont pu conclure la vente avec les acquéreurs initiaux, ces derniers y ayant renoncé par courrier du 22 octobre 2008, " suite à la préemption par la mairie de Dreux " ; que, " placés dans une situation financière difficile ", selon leurs propres termes, relevés par la cour, du fait de leur déménagement à Toulouse, ils ont adressé au maire un courrier le 7 novembre 2008 par lequel ils acceptaient néanmoins la proposition d'achat de la commune au prix de 70 000 euros indiqué dans la décision de préemption ; que la décision de préemption a été ensuite annulée par un jugement du 24 mars 2009 du tribunal administratif d'Orléans, au motif, notamment, que la commune ne justifiait pas de la réalité d'un projet d'action ou d'opération d'aménagement entrant dans le champ de l'article L. 300-1 du code de l'urbanisme ;  <br/>
<br/>
              2. Considérant que la cour a relevé, d'une part, que la suspension de la décision de préemption ne faisait pas obstacle à la vente en dehors de la procédure de préemption, d'autre part, que M. et Mme A...n'avaient pas usé de la faculté de notifier à la commune le maintien du prix figurant dans leur déclaration et leur acceptation que le prix soit fixé par le juge de l'expropriation ou de renoncer à aliéner leur bien ; que, toutefois, eu égard aux circonstances particulières qu'elle avait elle-même relevées, notamment au fait que la décision de préemption illégale avait seule été à l'origine de l'échec de la transaction qui devait être conclue le 12 septembre 2008 en vertu d'une promesse valable jusqu'au 18 septembre et des difficultés particulières auxquelles M. et Mme A...avaient dû faire face du fait de cet échec, elle a inexactement qualifié les faits qui lui étaient soumis en jugeant qu'il n'existait pas de lien de causalité direct et certain entre la décision de préemption illégale du 2 septembre 2008 et le préjudice dont les intéressés se prévalaient ;  <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. et Mme A...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme A..., qui ne sont pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Dreux une somme globale de 3 000 euros à verser à M. et Mme A...au titre de ces dispositions et de celles de l'article R. 761-1 du même code relatives au remboursement de la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 28 juin 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune de Dreux versera une somme globale de 3 000 euros à M. et Mme A...au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Dreux présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme C...et Edith A...et à la commune de Dreux.<br/>
Copie en sera adressée pour information à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
