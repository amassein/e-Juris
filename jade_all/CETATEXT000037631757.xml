<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631757</ID>
<ANCIEN_ID>JG_L_2018_11_000000409937</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 21/11/2018, 409937, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409937</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409937.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 409937, par une requête enregistrée le 20 avril 2017 au secrétariat du contentieux du Conseil d'Etat, l'Association pour la protection des animaux sauvages (ASPAS) et l'association One Voice demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 10 avril 2017 de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et du ministre de l'agriculture, de l'agroalimentaire et de la forêt fixant un nombre supplémentaire de spécimens de loups (Canis lupus) dont la destruction pourra être autorisée pour la période 2016-2017 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à chacune des associations requérantes, de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 411635, par une requête enregistrée le 19 juin 2017 au secrétariat du contentieux du Conseil d'Etat, l'association France Nature Environnement (FNE) et la Ligue française pour la protection des oiseaux (LPO) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même arrêté du 10 avril 2017 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros à chacune des associations requérantes en application des dispositions de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens.<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la directive 92/43/CEE du Conseil du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvage ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 30 juin 2015 fixant les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets concernant le loup (Canis lupus) ;<br/>
              - l'arrêté du 5 juillet 2016 fixant le nombre maximum de spécimens de loups (Canis lupus) dont la destruction pourra être autorisée pour la période 2016-2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Les requêtes nos 409937 et 411635, introduites respectivement par l'Association pour la protection des animaux sauvages (ASPAS) et autre, et l'association France nature environnement (FNE) et autre, tendent à l'annulation pour excès de pouvoir de l'arrêté du 10 avril 2017 fixant un nombre supplémentaire de spécimens de loups (Canis lupus) dont la destruction pourra être autorisée pour la période 2016-2017, en application du 4° de l'article L. 411-2 du code de l'environnement. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
<br/>
              Sur le cadre juridique applicable au litige :<br/>
<br/>
              2.	En premier lieu, aux termes de l'article 12 de la directive 92/43/CEE du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvage, dite " directive habitats " : " 1. Les Etats membres prennent les mesures nécessaires pour instaurer un système de protection stricte des espèces animales figurant à l'annexe IV point a), dans leur aire de répartition naturelle, interdisant : a) toute forme de capture ou de mort intentionnelle de spécimens de ces espèces dans la nature ; b) la perturbation intentionnelle de ces espèces, notamment durant la période de reproduction et de dépendance (...) ". Le loup est au nombre des espèces figurant à l'annexe IV point a) de la directive. L'article 16 de la même directive prévoit toutefois que : " 1. A condition qu'il n'existe pas une autre solution satisfaisante et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle, les Etats membres peuvent déroger aux dispositions des article 12, 13, 14 et de l'article 15 points a) et b) : (...) b) pour prévenir des dommages importants notamment aux cultures, à l'élevage, aux forêts, aux pêcheries, aux eaux et à d'autres formes de propriété ".<br/>
<br/>
              3.	En deuxième lieu, aux termes du I de l'article L. 411-1 du code de l'environnement, pris pour la transposition de la directive habitats précitée : " Lorsqu'un intérêt scientifique particulier, le rôle essentiel dans l'écosystème ou les nécessités de la préservation du patrimoine naturel justifient la conservation (...) d'espèces animales non domestiques (...) et de leurs habitats, sont interdits : 1° (...) la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces (...) ". Aux termes de l'article L. 411-2 du même code, pris pour la transposition de l'article 16 de la même directive : " Un décret en Conseil d'Etat détermine les conditions dans lesquelles sont fixées : 1° La liste limitative des habitats naturels, des espèces animales non domestiques (...) ainsi protégés ; 2° La durée et les modalités de mise en oeuvre des interdictions prises en application du I de l'article L. 411-1 ; 3° La partie du territoire sur laquelle elles s'appliquent (...) ; 4° La délivrance de dérogations aux interdictions mentionnées aux 1°, 2° et 3° de l'article L. 411-1, à condition qu'il n'existe pas d'autre solution satisfaisante, pouvant être évaluée par une tierce expertise menée, à la demande de l'autorité compétente, par un organisme extérieur choisi en accord avec elle, aux frais du pétitionnaire, et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle : a) Dans l'intérêt de la protection de la faune et de la flore sauvages et de la conservation des habitats naturels ; / b) Pour prévenir des dommages importants notamment aux cultures, à l'élevage (...) et à d'autres formes de propriété ".<br/>
<br/>
              4. En troisième lieu, pour l'application de ces dernières dispositions, l'article R. 411-1 du code de l'environnement prévoit que la liste des espèces animales non domestiques faisant l'objet des interdictions définies à l'article L. 411-1 est établie par arrêté conjoint du ministre chargé de la protection de la nature et du ministre chargé de l'agriculture. L'article R. 411-6 du même code précise que : " Les dérogations définies au 4° de l'article L. 411-2 sont accordées par le préfet, sauf dans les cas prévus aux articles R. 411-7 et R. 411-8. / (...) ". Son article R. 411-13 prévoit que les ministres chargés de la protection de la nature et de l'agriculture fixent par arrêté conjoint pris après avis du Conseil national de la protection de la nature " (...) / 2° Si nécessaire, pour certaines espèces dont l'aire de répartition excède le territoire d'un département, les conditions et limites dans lesquelles les dérogations sont accordées afin de garantir le respect des dispositions du 4° de l'article L. 411-2 du code de l'environnement. ". <br/>
<br/>
              5. En quatrième et dernier lieu, l'article 2 de l'arrêté du 30 juin 2015 fixant les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets concernant le loup (Canis lupus), qui est pris pour l'application du 2° de l'article R. 411-13 du code de l'environnement précité, dispose que " Le nombre maximum de spécimens de loups (mâles ou femelles, jeunes ou adultes) dont la destruction est autorisée, en application de l'ensemble des dérogations qui pourront être accordées par les préfets, est fixé chaque année par arrêté ministériel. Cet arrêté ne peut couvrir une période excédant le 30 juin de l'année suivante. / Ce maximum annuel sera diminué du nombre des animaux ayant fait l'objet d'actes de destruction volontaire constatés par les agents mentionnés à l'article L. 415-1 du code de l'environnement durant toute la période de validité de l'arrêté visé au premier alinéa du présent article. ". En application de ces dispositions, un arrêté du 5 juillet 2016 a fixé à trente-six le nombre maximum de spécimens de loups dont la destruction pourra être autorisée pour l'ensemble de la période 2016-2017. Son article 3 a toutefois prévu la possibilité de réviser le quota en cours de campagne en fonction des données disponibles à l'issue de l'hiver 2015-2016. En application de ce dernier article, l'article 1er de l'arrêté du 10 avril 2017 attaqué prévoit la possibilité de détruire deux loups supplémentaires, uniquement par des tirs de défense et des tirs de défense renforcée.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              6. Aux termes de l'article L. 123-19-1 du code de l'environnement applicable en l'espèce : " I. - Le présent article définit les conditions et limites dans lesquelles le principe de participation du public, prévu à l'article 7 de la Charte de l'environnement, est applicable aux décisions, autres que les décisions individuelles, des autorités publiques ayant une incidence sur l'environnement lorsque celles-ci ne sont pas soumises, par les dispositions législatives qui leur sont applicables, à une procédure particulière organisant la participation du public à leur élaboration. (...) / II. - Sous réserve des dispositions de l'article L. 123-19-6, le projet d'une décision mentionnée au I, accompagné d'une note de présentation précisant notamment le contexte et les objectifs de ce projet, est mis à disposition du public par voie électronique (...). / Les observations et propositions du public, déposées par voie électronique ou postale, doivent parvenir à l'autorité administrative concernée dans un délai qui ne peut être inférieur à vingt et un jours à compter de la mise à disposition prévue au même premier alinéa. / Le projet de décision ne peut être définitivement adopté avant l'expiration d'un délai permettant la prise en considération des observations et propositions déposées par le public et la rédaction d'une synthèse de ces observations et propositions. Sauf en cas d'absence d'observations et propositions, ce délai ne peut être inférieur à quatre jours à compter de la date de la clôture de la consultation. / Dans le cas où la consultation d'un organisme consultatif comportant des représentants des catégories de personnes concernées par la décision en cause est obligatoire et lorsque celle-ci intervient après la consultation du public, la synthèse des observations et propositions du public lui est transmise préalablement à son avis. / Au plus tard à la date de la publication de la décision et pendant une durée minimale de trois mois, l'autorité administrative qui a pris la décision rend publics, par voie électronique, la synthèse des observations et propositions du public avec l'indication de celles dont il a été tenu compte, les observations et propositions déposées par voie électronique ainsi que, dans un document séparé, les motifs de la décision. / (...) ". <br/>
<br/>
              7. En premier lieu, le défaut de publication de la synthèse des observations du public ainsi que des motifs de l'arrêté attaqué est, en tout état de cause, sans incidence sur la légalité de celui-ci. Les moyens tirés sur ces points d'une méconnaissance de l'article L. 123-19-1 du code de l'environnement ne peuvent, dès lors, qu'être écartés. <br/>
<br/>
              8. En second lieu, ni les dispositions de l'article L. 123-19-1 du code de l'environnement précité ni aucune autre disposition ou principe n'imposent d'organiser une nouvelle consultation du public sur le projet d'arrêté litigieux à la suite de la publication, le 7 mars 2017, d'un rapport d'expertise scientifique collective comportant de nouvelles informations sur l'évolution de la population des loups en France. Le moyen tiré sur ce point d'une irrégularité de la procédure d'élaboration de l'arrêté ne peut, par suite, être accueilli.  <br/>
<br/>
<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              9. L'arrêté attaqué conduit à augmenter le plafond des destructions d'animaux pouvant être autorisées pour la période 2016-2017, fixé initialement par l'arrêté du 5 juillet 2016, de deux spécimens supplémentaires, uniquement par des tirs de défense et des tirs de défense renforcée. Toutefois, la portée comme la légalité de cet arrêté complémentaire doivent être appréciées au regard de l'ensemble du dispositif réglementaire prévu notamment par l'arrêté du 30 juin 2015 dans le cadre duquel il appartient au préfet d'apprécier, en fonction des circonstances locales, si des dérogations peuvent être autorisées, à charge pour lui de vérifier qu'il n'existe pas d'autre solution satisfaisante et que les mesures envisagées, qui doivent être proportionnées à l'objectif de protection des élevages, ne nuisent pas au maintien de la population des loups, au sein de son aire de répartition naturelle, dans un état de conservation favorable. Il incombe également au pouvoir réglementaire d'adapter ce dispositif au regard de cet objectif et en particulier d'ajuster chaque année, ou même, conformément à l'article 3 de l'arrêté du 5 juillet 2016, en cours de campagne, le plafond de destruction des loups, au vu, d'une part, des données techniques disponibles les plus récentes relatives à la population des loups et à sa répartition ainsi qu'aux dommages occasionnés aux élevages, d'autre part, des résultats des méthodologies scientifiques pertinentes destinées à modéliser sur une période pluriannuelle l'évolution de cette population et de ces dommages.<br/>
<br/>
              10. Les associations requérantes soutiennent qu'en permettant un prélèvement maximum de 38 loups sur la période 2016-2017, alors que la population de cette espèce était estimée à 292 spécimens à la fin de la campagne 2015-2016, l'arrêté attaqué pourrait conduire à des destructions de loups représentant environ 13 % de son effectif total alors que l'expertise scientifique sur le devenir de la population de loups en France publiée en mars 2017 recommande de ne pas dépasser un taux de prélèvement de 10 %, de telles destructions n'étant dès lors, selon elles, ni nécessaires ni proportionnées et de nature à compromettre la conservation de l'espèce. Il ressort toutefois des pièces des dossiers que, pour prendre l'arrêté attaqué, qui ne prévoit au demeurant que la destruction de deux loups supplémentaires, sans que celle-ci ne revête d'ailleurs un caractère d'automaticité, les ministres se sont fondés sur les données les plus récentes relatives à la population des loups et à sa répartition ainsi qu'aux dommages occasionnés aux élevages, en s'appuyant sur des méthodologies scientifiques reconnues, qui mettent notamment en évidence que, à la sortie de l'hiver 2016-2017, d'une part, les loups étaient présents dans six zones de présence permanente (ZPP) supplémentaires par rapport à la fin de la période 2015-2016, soit 55 ZPP contre 49 au terme de la période 2015-2016, et, d'autre part, que leurs attaques de troupeaux étaient en hausse, 2738 constats ayant été dressés en 2016 contre 2447 en 2015, avec près d'un millier de victimes supplémentaires indemnisées en 2016 contre moins de 900 en 2015. Ces études révèlent également une tendance à l'augmentation du nombre de meutes, qui est passé de la sortie d'hiver 2014-2015 au début d'hiver 2015-2016, de trente à trente quatre. En outre, les travaux de modélisation qui ont été réalisés, au vu de l'ensemble des données recueillies notamment par l'Office national de la chasse et de la faune sauvage, indiquent que la dynamique de l'évolution de la population des loups, dans le prolongement de la tendance constatée depuis une quinzaine d'années, demeure favorable en France, comme le confirment d'ailleurs les informations statistiques les plus récentes. Enfin, l'administration aurait l'obligation, ainsi qu'il a été dit ci-dessus, de prendre rapidement des mesures correctrices, éventuellement en réduisant le quota maximum de destructions de loups, si de nouvelles données faisaient apparaître que l'objectif de maintien de l'espèce dans un état de conservation favorable dans son aire de répartition naturelle n'est plus satisfait. Par suite, compte tenu de l'ensemble des éléments qui précèdent, les moyens tirés de la méconnaissance par l'arrêté attaqué des dispositions de l'article 16 de la directive 92/43/CEE du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvage  et du 4° de l'article L. 411-2 du code de l'environnement doivent être écartés.<br/>
<br/>
              11. Il résulte de tout ce qui précède que les requêtes nos 409337 et 411635 doivent être rejetées y compris les conclusions tendant à la prise en charge par l'Etat des frais exposés au cours de l'instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes nos 409937 et 411635 sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'Association pour la protection des animaux sauvages et à l'association France nature environnement, premières dénommées dans chaque requête, pour l'ensemble des requérants, au ministre d'Etat, ministre de la transition écologique et solidaire et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
