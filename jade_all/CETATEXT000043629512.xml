<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043629512</ID>
<ANCIEN_ID>JG_L_2021_06_000000434425</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/62/95/CETATEXT000043629512.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 08/06/2021, 434425, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434425</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434425.20210608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... B... a demandé au tribunal administratif de Nîmes de condamner solidairement le centre hospitalier d'Avignon, le centre hospitalier du pays d'Apt et le groupement de coopération sanitaire Apt-Avignon à lui payer la somme totale de 54 171,56 euros, le centre hospitalier d'Avignon à lui payer la somme de 10 932 euros, le centre hospitalier du pays d'Apt à lui payer la somme de 7 934,86 euros ainsi que le groupement de coopération sanitaire Apt-Avignon à lui payer la somme de 41 490,40 euros, en réparation des préjudices qu'il estime avoir subis dans le cadre de sa mise à disposition auprès du centre hospitalier du pays d'Apt puis du groupement de coopération sanitaire Apt-Avignon Chirurgie. Par un jugement n° 1402729 du 19 décembre 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA00672 du 9 juillet 2019, la cour administrative d'appel de Marseille a, sur appel de M. A... B..., annulé ce jugement, condamné le centre hospitalier du pays d'Apt à lui verser la somme de 6 770 euros, le groupement de coopération sanitaire Apt-Avignon à lui verser la somme de 37 000 euros et, solidairement, le centre hospitalier du pays d'Apt et le centre hospitalier d'Avignon à lui verser la somme de 15 000 euros.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 6 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier d'Avignon, le centre hospitalier du pays d'Apt et le groupement de coopération sanitaire Apt-Avignon demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A... B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier-Pinet, avocat du centre hospitalier d'avignon, du centre hospitalier du Pays d'Apt et du groupement de coopération sanitaire Apt-Avignon et à la SCP Baraduc, Duhamel, Rameix, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., chirurgien spécialisé en orthopédie et traumatologie, a été recruté par le centre hospitalier d'Avignon en tant que praticien contractuel et mis à disposition auprès du centre hospitalier du pays d'Apt puis, par avenant à son contrat, auprès du groupement de coopération sanitaire Apt-Avignon. Il a demandé au tribunal administratif de Nîmes de condamner ces trois établissements de santé à l'indemniser de divers préjudices subis en raison de fautes commises, selon lui, dans la gestion des conditions de son exercice professionnel. Ces établissements se pourvoient en cassation contre l'arrêt du 9 juillet 2019 par lequel la cour administrative d'appel de Marseille les a, solidairement ou à titre individuel, condamnés à verser à l'intéressé diverses sommes à titre de dommages-intérêts.<br/>
<br/>
              2. Aux termes de l'article R. 611-3 du code de justice administrative : " Les décisions prises pour l'instruction des affaires sont notifiées aux parties, en même temps que les copies, produites en exécution des articles R. 411-3 et suivants et de l'article R. 412-2, des requêtes, mémoires et pièces déposés au greffe. La notification peut être effectuée au moyen de lettres simples. / Toutefois, il est procédé aux notifications de la requête, des demandes de régularisation, des mises en demeure, des ordonnances de clôture, des décisions de recourir à l'une des mesures d'instruction prévues aux articles R. 621-1 à R. 626-3 ainsi qu'à l'information prévue à l'article R. 611-7 au moyen de lettres remises contre signature ou de tout autre dispositif permettant d'attester la date de réception. "<br/>
<br/>
              3. S'il est constant que les pièces de la procédure d'appel ont été communiquées par la cour administrative d'appel au centre hospitalier d'Avignon, il ne résulte en revanche d'aucune de ces pièces qu'elles auraient également été communiquées par la cour au centre hospitalier du pays d'Apt et au groupement de coopération sanitaire Apt-Avignon, qui n'ont d'ailleurs pas produit d'observations devant la cour et n'étaient pas représentés à l'audience d'appel. Ces deux établissements ne pouvant dès lors être regardés comme ayant été régulièrement mis en cause par le juge d'appel, les requérants sont fondés à soutenir qu'en ne mettant en cause que le seul centre hospitalier d'Avignon alors qu'elle était saisie de conclusions tendant à la condamnation solidaire de cet établissement avec le centre hospitalier du pays d'Apt et le groupement de coopération sanitaire Apt-Avignon, la cour a entaché son arrêt d'irrégularité. Ils sont par suite fondés, sans qu'il soit besoin de se prononcer sur les autres moyens de leur pourvoi, à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du centre hospitalier d'Avignon, du centre hospitalier du pays d'Apt ou du groupement de coopération sanitaire Apt-Avignon la somme que demande, à ce titre, M. A... B.... Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... B... la somme que demandent les requérants au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 9 juillet 2019 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier d'Avignon, premier requérant dénommé et à M. C... A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
