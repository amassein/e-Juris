<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045099892</ID>
<ANCIEN_ID>JG_L_2022_01_000000437458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/09/98/CETATEXT000045099892.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 31/01/2022, 437458, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-01-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SAS BOULLOCHE, COLIN, STOCLET ET ASSOCIÉS ; SCP LYON-CAEN, THIRIEZ ; SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:437458.20220131</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association le comité de quartier les Aubes, M. K... P..., Mme G... R..., M. I... H..., Mme O... L..., épouse H..., M. N... D..., Mme Q... J..., Mme C... E... et M. F... M... ont demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir, d'une part, l'arrêté du maire de Montpellier du 27 août 2018 délivrant un permis de construire à la SCI Boulevard Ernest Renan MTP pour la réalisation d'une école et d'un collectif de 43 logements, d'autre part, la décision implicite de rejet du recours gracieux formé contre ce permis. Par un jugement n° 1901103 du 7 novembre 2019, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 janvier, 26 mai 2020 et le 6 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, l'association le comité de quartier les Aubes et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Montpellier et de la SCI Boulevard Ernest Renan MTP la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Colin - Stoclet, avocat de l'association le comité de quartier les Aubes et autres, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Montpellier et à la SCP L. Poulet, Odent, avocat de la SCI Boulevard Ernest Renan MTP ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 janvier 2022, présentée par la commune de Montpellier. <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 27 août 2018, le maire de Montpellier (Hérault) a délivré à la société Boulevard Ernest Renan MTP un permis de construire portant sur la réalisation d'une école et d'un collectif de quarante-trois logements sur un terrain situé 51 bis boulevard Ernest Renan. Par un jugement du 7 novembre 2019 contre lequel l'association " le comité de quartier les Aubes " et autres se pourvoient en cassation, le tribunal administratif de Montpellier a rejeté leur demande tendant à l'annulation de ce permis de construire et de la décision implicite rejetant leur recours gracieux.<br/>
<br/>
              2. En premier lieu, l'article 3 du règlement du plan local d'urbanisme de Montpellier, applicable à la zone dans laquelle se situe le terrain d'assiette du projet, dispose que les accès au terrain " doivent être adaptés à l'opération de façon à apporter la moindre gêne à la circulation publique ", que leurs caractéristiques " doivent permettre de satisfaire aux règles minimales de desserte : défense contre l'incendie, protection civile, brancardage, ordures ménagères " et, enfin, qu'ils " doivent présenter au débouché sur la rue et en retrait de l'alignement sur une distance d'au moins 5 mètres une pente n'excédant pas 5% ". <br/>
<br/>
              3. D'une part, après avoir relevé par une appréciation souveraine exempte de dénaturation que l'accès au terrain d'assiette du projet s'effectuait à partir du boulevard Ernest Renan, d'une largeur d'environ 9 mètres et sur lequel la circulation automobile est à double sens, le débouché sur cette voie étant en retrait de 5 mètres suivant une pente de 5 % avant la rampe d'accès aux aires de stationnement situées en sous-sol, pour estimer que, au vu de ces aménagements et eu égard à ces circonstances, les conditions d'accès prévues étaient conformes aux prescriptions de l'article 3 du règlement du plan local d'urbanisme et ne pouvaient être regardées comme étant de nature à gêner la circulation publique, le tribunal administratif de Montpellier n'a pas entaché son jugement d'erreur de droit. <br/>
<br/>
              4. D'autre part, le tribunal administratif n'a pas davantage commis d'erreur de droit en jugeant que les conditions générales de la circulation dans le secteur concerné, et notamment les difficultés liées aux flux aux heures d'entrée et de sortie d'école dans ce boulevard, ne pouvaient, par elles-mêmes, être utilement invoquées pour établir l'insuffisance de l'accès au terrain d'assiette.  <br/>
<br/>
              5. En deuxième lieu, l'article 4 du règlement du plan local d'urbanisme de Montpellier relatif à la desserte par les réseaux dispose, s'agissant de la desserte par le réseau d'eau potable, que " toute construction nouvelle doit être raccordée au réseau public de distribution existant " et que " les raccordements aux réseaux devront se conformer aux prescriptions définies par le règlement du service d'eau potable applicable à la Ville de Montpellier et par le code de la santé publique ".<br/>
<br/>
              6. Après avoir relevé, par une appréciation souveraine non arguée de dénaturation, que, le terrain d'assiette du projet litigieux n'étant pas desservi par un réseau d'eau potable conforme à la réglementation applicable, Montpellier Méditerranée Métropole et la société Boulevard Ernest Renan MTP avaient conclu, le 30 juillet 2018, une convention de projet urbain partenarial prévoyant le renforcement du réseau d'adduction d'eau potable existant du côté du boulevard Ernest Renan, et que la direction eau et assainissement de Montpellier Méditerranée Métropole avait indiqué, par un avis du 19 juin 2018, que ces travaux de renforcement du réseau d'eau potable devraient être réalisés dans un délai de douze mois à compter la réception du premier permis de construire, le tribunal administratif a pu, sans erreur de droit, juger que, eu égard à l'absence de tout élément au dossier remettant en cause la crédibilité de ce délai de douze mois et alors même que la convention de projet urbain partenarial prévoyait un délai maximal de dix ans pour leur réalisation, le projet n'était pas contraire aux prescriptions précitées de l'article 4 du règlement du plan local d'urbanisme.   <br/>
<br/>
              7. En troisième lieu, l'article 12 du règlement du plan local d'urbanisme de Montpellier, relatif au stationnement des véhicules, impose, pour les constructions autres que celles destinées à l'habitation, situées, comme le projet litigieux, dans les périmètres desservis par le tramway, un quota minimum d'une place de stationnement pour 300 m2 de surface de plancher. <br/>
<br/>
              8. En relevant, pour écarter le moyen tiré de l'insuffisance des places de stationnement au regard des prescriptions de cet article 12 du règlement du plan local d'urbanisme, qu'alors que cet article prévoit une règle particulière en matière de stationnement des deux roues pour les seuls établissements d'enseignement public ou privé du 2ème degré dont ne relève pas l'école privée objet du permis de construire litigieux, il ressortait également des pièces du dossier qui lui était soumis que le projet prévoit seize places supplémentaires au-delà des quarante places de stationnement dédiées aux logements collectifs, le tribunal doit être regardé comme ayant implicitement mais nécessairement fait application de la règle précitée relative aux constructions autres que celles destinées à l'habitation situées dans les périmètres desservis par le tramway et n'a, par suite, pas entaché son jugement d'une erreur de droit ni dénaturé les pièces du dossier qui lui était soumis. En jugeant que, en l'espèce, le nombre de places de stationnement prévu répondait aux besoins résultant du projet, le tribunal a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et n'a pas entaché son arrêt d'erreur de droit. <br/>
<br/>
              9. Enfin, aux termes de l'article R. 111-2 du code de l'urbanisme : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations ".<br/>
<br/>
              10. En vertu de ces dispositions, lorsqu'un projet de construction est de nature à porter atteinte à la salubrité ou à la sécurité publique, le permis de construire ne peut être refusé que si l'autorité compétente estime, sous le contrôle du juge, qu'il n'est pas légalement possible, au vu du dossier et de l'instruction de la demande de permis, d'accorder le permis en l'assortissant de prescriptions spéciales qui, sans apporter au projet de modification nécessitant la présentation d'une nouvelle demande, permettraient d'assurer la conformité de la construction aux dispositions législatives et réglementaires dont l'administration est chargée d'assurer le respect. <br/>
<br/>
              11. D'une part, en jugeant que le projet litigieux, au vu de sa situation et de l'accès qu'il réserve aux occupants pour user du parc de stationnement en sous-sol dont le nombre de places répond aux exigences du plan local d'urbanisme, n'est pas de nature à porter atteinte à la sécurité publique, le tribunal, qui a nécessairement tenu compte de l'effet induit sur la circulation et n'était pas tenu de répondre au détail de l'argumentation des requérants, a suffisamment motivé son jugement. <br/>
<br/>
              12. D'autre part, compte tenu de ce qui a été dit au point 6 sur le renforcement du réseau d'eau potable dans un délai rapproché, c'est par une appréciation souveraine exempte de dénaturation que le tribunal administratif a estimé que le projet n'était pas davantage de nature à porter atteinte à la salubrité ou à la sécurité publique. <br/>
<br/>
              13. Il résulte de tout ce qui précède que l'association le comité de quartier les Aubes et autres ne sont pas fondés à demander l'annulation du jugement qu'ils attaquent. <br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Montpellier et de la société Boulevard Ernest Renan MTP qui ne sont pas, dans la présente instance, les parties perdantes. A l'inverse, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association le comité de quartier les Aubes et autres la somme globale de 3 000 euros à verser à la SCI Boulevard Ernest Renan MTP et à la commune de Montpellier au titre de ces mêmes dispositions.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association le comité de quartier les Aubes et autres est rejeté.<br/>
Article 2 : L'association le comité de quartier les Aubes et autres verseront la somme globale de 3 000 euros à la société Boulevard Ernest Renan MTP et à la commune de Montpellier au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association le comité de quartier les Aubes, premier requérant cité, à la commune de Montpellier et à la SCI Boulevard Ernest Renan MTP.<br/>
              Délibéré à l'issue de la séance du 7 janvier 2022 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; M. Cyril Roger-Lacan, conseiller d'Etat et Mme Catherine Moreau, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 31 janvier 2022.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Moreau<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
