<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033123496</ID>
<ANCIEN_ID>JG_L_2016_09_000000403253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/12/34/CETATEXT000033123496.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/09/2016, 403253, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:403253.20160909</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 6 septembre 2016, au secrétariat du contentieux du Conseil d'Etat, l'association " Traditions, Terroirs et Ruralité " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision implicite du ministre de l'agriculture, de l'agroalimentaire et de la forêt rejetant sa demande tendant à ce que diverses mesures soient prises par les pouvoirs publics pour limiter le risque d'augmentation des cas de fièvre catarrhale ovine, notamment en encadrant, sur le plan sanitaire, la circulation et l'abattage d'ovins dans le cadre de la fête de l'Aïd-el-kebir qui aura lieu le 12 septembre 2016 ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'agriculture, de l'agroalimentaire et de la forêt de prendre diverses mesures en ce sens, et en particulier de n'autoriser l'abattage, y compris dans le cadre de l'abattage rituel, que des seuls agneaux certifiés nés en France, transportés par un transporteur agréé désinsectisant son véhicule à chaque transport, abattus dans l'abattoir public le plus proche de l'éleveur, et non dans un abattoir temporaire, choisis par le client et vendus sans intermédiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              L'association requérante soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que la décision contestée porte atteinte de manière suffisamment grave et immédiate à la protection de la santé publique et à l'intérêt économique de la filière ovine, du fait du risque d'augmentation des cas de fièvre catarrhale ovine lié à l'organisation de la fête d'Aïd-el-kébir qui doit débuter le 12 septembre 2016 ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision litigieuse, au regard des principes constitutionnels de protection de la santé publique et de précaution, ainsi que des dispositions du code rural et de la pêche maritime relatives à la prévention des dangers sanitaires, dès lors que l'absence d'intervention des mesures sollicitées risque d'augmenter le nombre de cas de fièvre catarrhale ovine, dont la transmissibilité à l'homme ne saurait être exclue.<br/>
<br/>
              Par un mémoire en défense, enregistré le 9 septembre 2016, le ministre de l'agriculture, de l'agroalimentaire et de la forêt conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'association ne sont pas fondés.<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la directive 2000/75/CE du Conseil du 20 novembre 2000 ;<br/>
              - le règlement (CE) n° 1266/2007 de la Commission ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale ;<br/>
              - l'arrêté du 22 juillet 2011 fixant les mesures techniques et administratives relatives à la lutte contre la fièvre catarrhale du mouton sur le territoire métropolitain ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " Traditions, Terroirs et Ruralité ", d'autre part, le ministre de l'agriculture, de l'agroalimentaire et de la forêt ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 9 septembre 2016 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Monod, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association ;<br/>
<br/>
              - les représentants de l'association " Traditions, Terroirs et Ruralité " ;<br/>
<br/>
              - les représentants du ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a décidé que l'instruction serait close le même jour à 17 h ; <br/>
<br/>
              - Vu les éléments chiffrés transmis par le ministre ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que l'association " Traditions, Terroirs et Ruralité " a demandé au ministre de l'agriculture, de l'agro-alimentaire et de la forêt, le 5 juillet 2016, d'adopter diverses mesures de nature, selon elle, à prévenir le risque d'une augmentation du nombre de cas de fièvre catarrhale ovine, ainsi que le risque de transmission de la maladie à d'autres espèces, voire d'une mutation du virus susceptible de contaminer l'être humain ; que, parallèlement au recours pour excès de pouvoir qu'elle a formé contre la décision implicite de rejet née du silence gardé durant deux mois par le ministre sur sa demande, elle demande au Conseil d'Etat de suspendre l'exécution de cette décision et d'enjoindre au ministre de prendre une partie des mesures en cause ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que la fièvre catarrhale ovine, dont la France était déclarée indemne depuis le 14 décembre 2012, est réapparue sur le territoire en septembre 2015 et que l'on en dénombre actuellement 292 foyers, majoritairement dans le centre du pays ; que cette maladie virale, en l'état des connaissances techniques, concerne les ruminants domestiques et sauvages et est transmise d'un animal à l'autre par la piqûre d'un moucheron ; que si tout risque de transmission à d'autres espèces ne peut être écarté, les animaux ainsi contaminés ne transmettent pas eux-mêmes la maladie ; que, selon l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail, il s'agit d'une maladie virale strictement animale, qui n'affecte pas l'homme et n'a aucune incidence sur la qualité des denrées ;<br/>
<br/>
              4. Considérant que les mesures à mettre en oeuvre pour lutter contre cette maladie ont fait l'objet d'une directive du 20 novembre 2000 arrêtant des dispositions spécifiques relatives aux mesures de lutte et d'éradication de la fièvre catarrhale du mouton ainsi que d'un règlement de la Commission du 26 octobre 2007 portant modalités d'application de la directive ; que ces dispositions ont été transposées et précisées, notamment, par un arrêté du 22 juillet 2011 fixant les mesures techniques et administratives relatives à la lutte contre la fièvre catarrhale du mouton sur le territoire métropolitain, pris sur le fondement de l'article L. 221-1 du code rural et de la pêche maritime, aux termes duquel " le ministre chargé de l'agriculture peut prendre toute mesures destinées à prévenir l'apparition, à enrayer le développement et à poursuivre l'extinction des maladies classées parmi les dangers sanitaires de première et deuxième catégories, en vertu du présent titre " ; <br/>
<br/>
              Sur la décision attaquée en tant qu'elle refuse de prendre les mesures de portée générale sollicitées par l'association :<br/>
<br/>
              5. Considérant, en premier lieu, que l'association " Traditions, Terroirs et Ruralités a demandé au ministre de l'agriculture, de l'agro-alimentaire et de la forêt d'imposer la vaccination des chiens et chats des éleveurs ainsi que de ceux de ces animaux présents aux alentours des foyers de la maladie ; que, toutefois, l'utilité d'une telle vaccination n'est pas avérée, compte tenu des éléments figurant au point 3 ; qu'au demeurant, il résulte de l'instruction qu'un tel vaccin n'est pas disponible ;<br/>
<br/>
              6. Considérant, en second lieu, qu'est déjà assurée, grâce à un réseau épidémiologique organisé par l'Office nationale de la chasse et de la faune sauvage et par les fédérations de chasseurs, la surveillance des animaux sauvages dans les zones touchées par la maladie, mesure également demandée par l'association ;<br/>
<br/>
              Sur la décision attaquée en tant qu'elle refuse de prendre des mesures relatives à l'abattage des ovins, en particulier à l'occasion de la fête de l'Aid-el-kebir :<br/>
<br/>
              7. Considérant que l'association a demandé au ministre de n'autoriser l'abattage, y compris dans le cadre de l'abattage rituel, que des seuls agneaux certifiés nés en France, transportés par un transporteur agréé désinsectisant son véhicule à chaque transport, abattus dans l'abattoir public le plus proche de l'éleveur, et non dans un abattoir temporaire, choisis par le client et vendus sans intermédiaire ;<br/>
<br/>
              8. Considérant, en premier lieu, qu'il ressort des textes applicables qu'à chaque fois qu'un cas de fièvre catarrhale ovine est confirmé dans un Etat membre de l'Union européenne, l'autorité compétente - en France le ministre chargé de l'agriculture - doit délimiter une zone de protection, se composant d'un espace d'au moins cent kilomètres autour de toute exploitation infectée, ainsi qu'une zone de surveillance, d'une profondeur d'au moins cinquante kilomètres au-delà des limites de la zone de protection, l'ensemble constituant une zone dite réglementée ; que  les ovins présents dans une zone réglementée font l'objet d'une interdiction de transport en-dehors de la zone, à laquelle il ne peut être dérogé que dans des hypothèses où il est possible de s'assurer qu'ils ne présentent pas de risque de contamination ; qu'en revanche, les ovins présents dans une zone non réglementée ne sont soumis à aucune restriction et peuvent donc être importés librement d'un autre Etat membre ; que, par suite, l'interdiction générale de l'abattage des agneaux qui ne proviendraient pas du territoire français, outre qu'elle n'offrirait pas nécessairement, par elle-même, une garantie supplémentaire, s'exposerait à la critique au regard du principe de libre circulation des biens issu du droit de l'Union européenne ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que l'obligation de désinsectiser les véhicules utilisés pour le transport des animaux quittant ou traversant une zone de protection est déjà prévue par les dispositions du 4° de l'article 14 de l'arrêté du 22 juillet 2011 ;<br/>
<br/>
              10. Considérant, en troisième lieu, que la réglementation sanitaire applicable aux personnes faisant le commerce des ovins s'applique également aux intermédiaires ;<br/>
<br/>
              11. Considérant, en quatrième lieu, que, selon les données transmises par le ministre de l'agriculture, de l'agro-alimentaire et de la forêt, environ un tiers des ovins abattus à l'occasion de la fête de l'Aid-èl-kebir l'a été dans des abattoirs temporaires ; que, cependant, ceux-ci, de même que les abattoirs permanents, sont soumis à une procédure d'agrément préfectoral en application de l'arrêté du 8 juin 2006 relatif à l'agrément sanitaire des établissements mettant sur le marché des produits d'origine animale ou des denrées contenant des produits d'origine animale, ainsi qu'au respect de la législation relative aux installations classées pour la protection de l'environnement ; qu'au demeurant, l'interdiction, sollicitée par l'association, des abattoirs temporaires, alors qu'il n'est pas établi que les abattoirs permanents pourraient satisfaire les besoins exprimés, présenterait le risque d'un recours accru à des modes d'abattage non encadrés par la puissance publique ;<br/>
<br/>
              12. Considérant, enfin, que la circonstance, au demeurant non établie, que les services de l'Etat éprouveraient, dans le cadre de la réglementation applicable, des difficultés à effectuer des contrôles suffisants sur la filière ovine est, par elle-même, sans incidence sur la légalité de cette réglementation ;<br/>
<br/>
              13. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner la condition d'urgence, que les moyens invoqués ne sont pas propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée, tant au regard de l'objectif constitutionnel de protection de la santé publique et du principe de précaution, que des dispositions du code rural et de la pêche maritime relatives à la prévention des dangers sanitaires ; que, par suite, la requête de l'association " Tradition, Terroirs et Ruralités ", y compris ses conclusions aux fins d'injonction et celles tendant à l'application de l'article L. 761-1 du code de justice administrative, doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête  de l'association " Tradition, Terroirs et Ruralités " est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association " Traditions, Terroirs et Ruralité " et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
Fait à Paris, le 9 septembre 2016<br/>
Signé : Bertrand Dacosta<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
