<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911886</ID>
<ANCIEN_ID>JG_L_2017_10_000000400018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911886.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/10/2017, 400018, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400018.20171026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
Mme B...A...a demandé au tribunal administratif de Bastia d'annuler la décision du 18 avril 2014 par laquelle le président du conseil général de la Haute-Corse a refusé de lui accorder une remise de sa dette de revenu de solidarité active d'un montant de 5 532,82 euros. Par un jugement n° 1400457 du 3 décembre 2015, le tribunal administratif de Bastia a rejeté sa demande.<br clear="none"/>
<br clear="none"/>
Par un pourvoi et un mémoire rectificatif, enregistrés les 24 et 26 mai 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler le jugement du tribunal administratif de Bastia ;<br clear="none"/>
<br clear="none"/>
2°) de mettre à la charge du département de la Haute-Corse la somme de 3 000 euros à verser à son avocat, la SCP Matuchansky, Vexliard, Poupot, sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- le code de l'action sociale et des familles ;<br clear="none"/>
- la loi n° 91-647 du 10 juillet 1991 ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Charles Touboul, rapporteur public.<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeA....<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Considérant ce qui suit :<br clear="none"/>
<br clear="none"/>
1. Il ressort des pièces du dossier soumis au juge du fond que Mme A...a demandé au tribunal administratif de Bastia d'annuler la décision du 18 avril 2014 par laquelle le président du conseil général de la Haute-Corse a refusé de lui accorder une remise de sa dette de revenu de solidarité active d'un montant de 5 532,82 euros. Elle se pourvoit contre le jugement du 3 décembre 2015 par lequel ce tribunal a rejeté sa demande.<br clear="none"/>
<br clear="none"/>
2. Aux termes de l'article R. 772-5 du code de justice administrative : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi (...) ". Aux termes de l'article R. 772-9 du même code : " La procédure contradictoire peut être poursuivie à l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête. / L'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Toutefois, afin de permettre aux parties de verser des pièces complémentaires, le juge peut décider de différer la clôture de l'instruction à une date postérieure dont il les avise par tous moyens. / L'instruction fait l'objet d'une réouverture en cas de renvoi à une autre audience ".<br clear="none"/>
<br clear="none"/>
3. L'article R. 772-9 du code de justice administrative, qui déroge aux règles de droit commun de la procédure administrative contentieuse, tend, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, à assouplir les contraintes de la procédure écrite en ouvrant la possibilité à ce juge de poursuivre à l'audience la procédure contradictoire sur des éléments de fait et en décalant la clôture de l'instruction, laquelle est entièrement régie par les dispositions de son deuxième alinéa. Dès lors, les règles fixées par l'article R. 613-2 du même code, selon lesquelles l'instruction est close à la date fixée par une ordonnance de clôture ou, à défaut, trois jours francs avant la date de l'audience, ne sont pas applicables aux contentieux sociaux régis par les articles R. 772-5 et suivants du code de justice administrative. Il suit de là que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience.<br clear="none"/>
<br clear="none"/>
4. Il ressort des mentions du jugement attaqué que le premier juge a visé le mémoire en réplique présenté par Mme A...le 16 novembre 2015, trois jours avant l'audience, sans l'analyser, en précisant qu'il avait été enregistré " postérieurement à la clôture de l'instruction et non communiqué ". Par suite, la requérante est fondée à soutenir qu'en refusant de prendre en compte ce mémoire qui, seul présenté par l'avocat désigné au titre de l'aide juridictionnelle, comportait des éléments nouveaux, le premier juge a méconnu les dispositions de l'article R. 772-9 du code de justice administrative et a, ainsi, rendu sa décision à l'issue d'une procédure irrégulière.<br clear="none"/>
<br clear="none"/>
5. Il résulte de ce qui précède que Mme A...est fondée à demander l'annulation du jugement qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br clear="none"/>
<br clear="none"/>
6. Mme A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de la Haute-Corse une somme de 1 500 euros à verser à cette SCP.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : Le jugement du tribunal administratif de Bastia du 3 décembre 2015 est annulé.<br clear="none"/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Bastia.<br clear="none"/>
Article 3 : Le département de la Haute-Corse versera à la SCP Matuchansky, Poupot, Valdelièvre une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br clear="none"/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au département de la Haute-Corse.<br clear="none"/>
Copie en sera adressée à la caisse d'allocation familiales de la Haute-Corse.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
