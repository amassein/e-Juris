<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038670472</ID>
<ANCIEN_ID>JG_L_2019_06_000000412144</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/67/04/CETATEXT000038670472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 21/06/2019, 412144, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412144</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Sylvain Humbert</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412144.20190621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société CM-CIC Investissement, devenue la société CM-CIC Investissement SCR et venant aux droits de la société CIC Investissement, a demandé au tribunal administratif de Montreuil de prononcer la décharge de la cotisation minimale de taxe professionnelle en fonction de la valeur ajoutée à laquelle cette dernière a été assujettie au titre des années 2005, 2006 et 2007 ainsi que des majorations et intérêts de retard correspondants. Par un jugement n° 1302639 du 7 avril 2014, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14VE01729 du 5 mars 2015, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société CM-CIC Investissement contre ce jugement. <br/>
<br/>
              Par une décision nos 390084, 390085, 390086 du 16 novembre 2016, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à cette même cour.<br/>
<br/>
              Par un second arrêt n° 16VE03525 du 6 juin 2017, la cour administrative d'appel de Versailles a annulé le jugement du 7 avril 2014 et déchargé la société CM-CIC Investissement de l'imposition en litige.  <br/>
<br/>
              Par un pourvoi et deux nouveaux mémoires, enregistrés le 5 juillet 2017 et les 2 janvier et 24 mai 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt et de remettre à la charge de la société un montant de droits de 85 934 euros au titre de la cotisation minimale de taxe professionnelle de l'année 2006 et de 131 296 euros au titre de la cotisation minimale de taxe professionnelle de l'année 2007, ainsi que les pénalités correspondantes.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 85-695 du 11 juillet 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Humbert, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la société CM-CIC Investissement SCR ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par les moyens qu'il invoque, le ministre doit être regardé comme demandant l'annulation de l'arrêt qu'il attaque en tant seulement qu'il porte sur la cotisation minimale de taxe professionnelle due au titre des années 2006 et 2007. Son pourvoi n'a pas perdu son objet du seul fait qu'un dégrèvement a été accordé à la société en exécution de l'arrêt attaqué.<br/>
<br/>
              2. D'une part, aux termes de l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, dans sa rédaction alors applicable, les sociétés de capital-risque sont des sociétés par actions ayant pour objet social " la gestion d'un portefeuille de valeurs mobilières ". En vertu de l'article 1er de cette loi, elles doivent procéder à des investissements dans des sociétés non cotées pour pouvoir bénéficier d'un régime de faveur au regard de l'imposition des sociétés. Il en résulte que ces sociétés doivent être regardées comme exerçant à titre habituel une activité professionnelle au sens des dispositions, alors en vigueur, du I de l'article 1447 du code général des impôts relatives à l'assujettissement à la taxe professionnelle.<br/>
<br/>
              3. D'autre part, aux termes du I de l'article 1647 E du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La cotisation de taxe professionnelle des entreprises dont le chiffre d'affaires est supérieur à 7 600 000 euros est au moins égale à 1,5 % de la valeur ajoutée produite par l'entreprise, telle que définie au II de l'article 1647 B sexies (...) ". Selon le II de l'article 1647 B sexies du même code, dans sa rédaction alors en vigueur : " 1. La valeur ajoutée (...) est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers (...). / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes, les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. (...) / 3. La production des établissements de crédit, des entreprises ayant pour activité exclusive la gestion de valeurs mobilières est égale à la différence entre : / d'une part, les produits d'exploitation bancaires et produits accessoires ; / et, d'autre part, les charges d'exploitation bancaires. (...) ". Eu égard à l'objet de ces dispositions, qui est de tenir compte de la capacité contributive des entreprises en fonction de leur activité, les entreprises ayant pour activité exclusive la gestion de valeurs mobilières ne s'entendent, pour leur application, que des entreprises qui exercent cette activité pour leur propre compte. Les sociétés de capital-risque, qui gèrent pour leur propre compte des participations financières, sont, dès lors, soumises aux modalités de calcul de la valeur ajoutée prévues au 3 du II de l'article 1647 B sexies du code général des impôts pour les entreprises ayant pour activité exclusive la gestion de valeurs mobilières.<br/>
<br/>
              4. Les dispositions de l'article 1647 B sexies du code général des impôts fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation minimale de taxe professionnelle. Pour l'application de ces dispositions, la production d'une entreprise ayant pour activité exclusive la gestion de valeurs mobilières doit être calculée, comme celle des établissements de crédit, en fonction des règles comptables fixés par le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit. L'annexe à ce règlement contient un modèle de compte de résultat et des commentaires de chacun des postes de ce compte. En vertu de ces dispositions, les produits d'exploitation bancaires et produits accessoires incluent les " gains et pertes sur opérations des portefeuilles de négociation " et les " gains et pertes sur opérations des portefeuilles de placement et assimilés ", mais non les " gains et pertes sur actifs immobilisés ", ce dernier poste étant défini comme " le solde en bénéfice ou perte des opérations sur titres de participation, sur autres titres détenus à long terme et sur parts dans les entreprises liées ".<br/>
<br/>
              5. Pour l'application de l'article 1647 E du code général des impôts à une société de capital-risque, le seuil d'assujettissement à la cotisation minimale de taxe professionnelle doit ainsi être calculé en ne tenant compte que des produits d'exploitation bancaires et des produits accessoires, lesquels ne comprennent pas, selon le règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991, les plus-values sur titres de participation et autres titres détenus à long terme. Il en va de même pour l'application du II de l'article 1647 B sexies du même code relatif à la détermination de la valeur ajoutée.<br/>
<br/>
              6. L'article 9 bis du règlement du comité de la réglementation bancaire n° 90-01 du 23 février 1990 relatif à la comptabilisation des opérations sur titres des établissements de crédit définit les titres de l'activité de portefeuille comme ceux qui ont été acquis dans le cadre d'investissements " réalisés de façon régulière avec pour seul objectif d'en retirer un gain en capital à moyen terme sans intention d'investir durablement dans le développement du fonds de commerce de l'entreprise émettrice, ni de participer activement à sa gestion opérationnelle ", en ajoutant que " des titres ne peuvent être affectés à ce portefeuille que si cette activité, exercée de manière significative et permanente dans un cadre structuré, procure à l'établissement une rentabilité récurrente, provenant principalement des plus-values de cession réalisées " et en citant comme exemple " les titres détenus dans le cadre d'une activité de capital-risque ". Le même article dispose que relèvent de la catégorie des autres titres détenus à long terme " les investissements réalisés sous forme de titres dans l'intention de favoriser le développement de relations professionnelles durables en créant un lien privilégié avec l'entreprise émettrice, mais sans influence dans la gestion des entreprises dont les titres sont détenus en raison du faible pourcentage des droits de vote qu'ils représentent ". Enfin, selon les dispositions du même article, les titres de participation sont ceux " dont la possession durable est estimée utile à l'activité de l'entreprise, notamment parce qu'elle permet d'exercer une influence sur la société émettrice des titres, ou d'en assurer le contrôle ". Une telle utilité peut notamment être caractérisée si les conditions d'achat des titres en cause révèlent l'intention de l'acquéreur d'exercer une influence sur la société émettrice et lui donnent les moyens d'exercer une telle influence.<br/>
<br/>
              7. Les dispositions citées au point 6, élaborées pour la comptabilisation des titres détenus en propre par les établissements de crédit, sociétés de financement et organismes assimilés, doivent être appliquées, pour l'application des articles 1647 E et 1647 B sexies du code général des impôts, à la répartition, entre les catégories qu'elles définissent, des titres détenus par les sociétés de capital-risque. Compte tenu de l'objet spécifique des sociétés de capital-risque, défini à l'article 1-1 de la loi du 11 juillet 1985 portant diverses dispositions d'ordre économique et financier, l'exemple des " titres détenus dans le cadre d'une activité de capital-risque " donné à la fin de l'alinéa définissant les titres de portefeuille, ne doit pas être compris comme excluant que les titres détenus par de telles sociétés puissent être, s'ils en remplissent les conditions, placés dans les catégories des titres de participation ou des autres titres détenus à long terme.  <br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond, en particulier du compte de résultat des exercices clos en 2006 et 2007, comme des écritures de l'administration non contestées sur ce point par la société CM-CIC Investissement SCR, que la société CIC Investissement, qui exerçait une activité de capital-risque, a notamment enregistré, au titre de l'année 2006, des produits nets sur cessions de valeurs mobilières de placement d'un montant supérieur à 8 millions d'euros et, au titre de l'année 2007, des produits nets sur cessions de valeurs mobilières de placement d'un montant supérieur à 6 millions d'euros et des revenus des titres immobilisés de l'activité de portefeuille d'un montant supérieur à 7 millions d'euros, ainsi que, pour chacune des deux années en cause, une plus-value de cession des titres immobilisés de l'activité de portefeuille d'environ 48 millions et 28 millions d'euros respectivement.<br/>
<br/>
              9. Par l'arrêt attaqué, la cour administrative d'appel de Versailles, après avoir rappelé l'ensemble des règles rappelées aux points 3 à 7 ci-dessus, en a déduit que les plus-values résultant de la cession des titres de sociétés comptabilisés en tant qu'immobilisations financières à l'actif du bilan de la société devaient être exclues du chiffre d'affaires pris en compte pour la détermination du seuil d'assujettissement de 7 600 000 euros à la cotisation minimale de taxe professionnelle. Toutefois, en jugeant qu'il n'était pas contesté que le chiffre d'affaires au titre des années 2006 et 2007 n'atteignait pas ce seuil compte tenu de l'exclusion des plus-values litigieuses, alors que les produits bancaires résultant des seuls produits nets sur cessions de valeurs mobilières de placement et revenus des titres immobilisés de l'activité de portefeuille étaient supérieurs à ce montant, la cour a dénaturé les pièces du dossier. Sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre est fondé, par ce moyen qui est né de l'arrêt attaqué, à demander l'annulation de celui-ci, en tant seulement qu'il porte sur la cotisation minimale de taxe professionnelle due au titre des années 2006 et 2007.<br/>
<br/>
              10. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond dans la même mesure.<br/>
<br/>
              11. En premier lieu, il résulte de ce qui est dit au point 2 ci-dessus que la société requérante n'est pas fondée à soutenir qu'elle ne serait pas, du fait de sa qualité de société de capital-risque, passible de la taxe professionnelle.<br/>
<br/>
              12. En deuxième lieu, il résulte de ce qui est dit au point 5 ci-dessus que, dans le cas d'une société de capital-risque, le chiffre d'affaires à prendre en compte pour l'appréciation du seuil d'assujettissement à la cotisation minimale de taxe professionnelle correspond aux produits d'exploitation bancaires et aux produits accessoires, lesquels ne comprennent pas les plus-values sur titres de participation et autres titres détenus à long terme. Si la société requérante soutient dès lors à bon droit que l'administration ne pouvait tenir compte, pour ce calcul, des plus-values de cession des titres immobilisés de l'activité de portefeuille, il résulte toutefois de l'instruction, en particulier du compte de résultat produit au dossier, qu'au titre des années 2006 et 2007, la somme des produits d'exploitation bancaires et des produits accessoires, notamment ceux issus des produits nets sur cessions de valeurs mobilières de placement et des revenus des titres immobilisés de l'activité de portefeuille, atteignait un montant supérieur au seuil de 7 600 000 euros. En outre, la société requérante ne peut utilement se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, ni du rescrit du 6 septembre 2005 n° 2005/43 IDL de la direction générale des impôts, qui, en excluant de la production stockée les produits financiers sauf dans les cas où la réglementation particulière propre à certains secteurs d'activité le prévoit, ne comporte pas une interprétation de la loi différente de celle dont il a été fait application, dès lors qu'il résulte de ce qui est dit au point 4 ci-dessus que sa production doit être calculée comme celle des établissements de crédit, ni de la lettre du 10 avril 2000 de la secrétaire d'Etat au budget, relative à la détermination du chiffre d'affaires des sociétés d'investissement à capital variable, dans les prévisions de laquelle elle n'entre pas. La société requérante n'est donc pas fondée à soutenir que son chiffre d'affaire au titre des années 2006 et 2007 était inférieur au seuil d'assujettissement à la cotisation minimale de taxe professionnelle.<br/>
<br/>
              13. En troisième lieu, il résulte de ce qui est dit au 5 ci-dessus que, dans le cas d'une société de capital-risque, la valeur ajoutée retenue pour déterminer la cotisation minimale de taxe professionnelle est calculée à partir des produits d'exploitation bancaires et des produits accessoires, lesquels ne comprennent pas les plus-values sur titres de participation et autres titres détenus à long terme. La société requérante ne peut utilement faire valoir qu'elle n'est pas une banque, qu'elle n'a pas le statut d'établissement de crédit et qu'elle n'est pas soumise au règlement du comité de la réglementation bancaire n° 91-01 du 16 janvier 1991 relatif à l'établissement et à la publication des comptes individuels annuels des établissements de crédit, dès lors qu'il résulte de ce qui est dit au point 4 ci-dessus que sa production doit être calculée comme celle des établissements de crédit, en fonction des règles comptables fixés par ce règlement. Si la société requérante soutient à bon droit que l'administration ne pouvait tenir compte, pour ce calcul, des plus-values de cession des titres immobilisés de l'activité de portefeuille, il résulte de l'instruction, en particulier des dernières écritures du ministre non contestées par la société requérante, que sa valeur ajoutée doit être évaluée en tenant compte de l'ensemble des produits d'exploitation bancaires et des produits accessoires enregistrés au compte de résultat. Toutefois, il convient de déduire de ce montant, ainsi que le demandait la société requérante, les charges nettes sur cessions de valeurs mobilières de placement dont le ministre n'établit pas qu'elles ne se rattacheraient pas aux charges d'exploitation bancaire dès lors que les produits nets sur cessions de valeurs mobilières de placement sont eux-mêmes intégralement pris en compte, pour un montant de 2 938 428 euros en 2006 et 2 846 071 euros en 2007. La valeur ajoutée doit par suite être évaluée à 2 793 866 euros au titre de l'année 2006 et à 5 910 325 euros au titre de l'année 2007. Le montant de la cotisation minimale de taxe professionnelle devant rester à la charge de la société doit être fixé à 1,5 % de la valeur ajoutée ainsi déterminée, déduction faite de la cotisation de référence, soit un total de 41 858 euros au titre de l'année 2006 et de 88 604 euros au titre de l'année 2007.<br/>
<br/>
              14. Il résulte de tout ce qui précède que la société CM-CIC Investissement SCR est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge de la cotisation minimale de taxe professionnelle à laquelle la société CIC Investissement a été assujettie au titre des années 2006 et 2007, ainsi que des majorations et intérêts de retard correspondants, dans la limite définie au point précédent. <br/>
<br/>
              15. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la SA CM-CIC Investissement SCR de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 juin 2017 de la cour administrative d'appel de Versailles est annulé en tant qu'il porte sur la cotisation minimale de taxe professionnelle due au titre des années 2006 et 2007.<br/>
Article 2 : Le montant de la cotisation minimale de taxe professionnelle à laquelle la société CIC Investissement est assujettie au titre des années 2006 et 2007 est fixé aux sommes respectives de 41 858 euros et de 88 604 euros, assortie des majorations et intérêts de retard correspondants.<br/>
Article 3 : La société CM-CIC Investissements SCR est déchargée de la cotisation minimale de taxe professionnelle au titre des années 2006 et 2007 à hauteur de la différence entre les montants initialement réclamés et les montant mentionnés à l'article 2.<br/>
Article 4 : Le jugement du tribunal administratif de Montreuil en date du 7 avril 2014 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 5 : L'Etat versera la somme de 3 000 euros à la société CM-CIC Investissement SCR au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 6 : Le surplus des conclusions présentées par la société CM-CIC Investissement SCR devant la cour administrative d'appel de Versailles et le surplus des conclusions du pourvoi du ministre de l'action et des comptes publics sont rejetés.<br/>
Article 7 : La présente décision sera notifiée à la société CM-CIC Investissement SCR et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
