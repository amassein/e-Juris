<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672600</ID>
<ANCIEN_ID>JG_L_2021_06_000000435315</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/06/2021, 435315</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435315</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435315.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire rectificatif, un mémoire complémentaire et un mémoire en réplique, enregistrés le 14 octobre 2019 et les 7 janvier et 9 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'office public de l'habitat (OPH) du Territoire de Belfort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 16 mai 2019 par laquelle la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et le ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement, ont prononcé à son encontre une sanction pécuniaire d'un montant de 96 320 euros ainsi que la décision rejetant son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'Office public de l'habitat du Territoire de Belfort.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction qu'à la suite d'un contrôle de l'Agence nationale de contrôle du logement social (ANCOLS) portant sur l'activité de l'office public de l'habitat (OPH) du Territoire de Belfort sur les années 2012 à 2016, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et le ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement, ont, par une décision du 16 mai 2019, infligé à cet office une sanction pécuniaire de 96 320 euros au motif que, au cours de la période considérée, vingt-neuf logements avaient été attribués à des personnes dont les ressources excédaient le plafond applicable et qu'un logement avait été attribué à une personne dont le dossier ne permettait pas de connaître les ressources. L'OPH du Territoire de Belfort demande l'annulation de cette sanction, ainsi que celle du rejet implicite, par les ministres auteurs de la sanction, de son recours gracieux contre celle-ci. <br/>
<br/>
              Sur la régularité de la sanction attaquée :<br/>
<br/>
              2. En premier lieu, aux termes du I de l'article L. 342-14 du code de la construction et de l'habitation, relatif aux suites que l'ANCOLS est susceptible de donner aux contrôles des organismes de logement social : " Après que la personne ou l'organisme a été mis en mesure de présenter ses observations en application de l'article L. 342-12 ou, en cas de mise en demeure, à l'issue du délai mentionné à ce même article, l'agence peut proposer au ministre chargé du logement de prononcer les sanctions suivantes (...) / II- Par dérogation au I, lorsque la sanction concerne un office public de l'habitat ou une société d'économie mixte, elle est prise conjointement par les ministres chargés du logement et des collectivités territoriales, dans les mêmes conditions ". Il ne résulte ni de ces dispositions ni d'aucune autre disposition ni d'aucun principe que l'ANCOLS soit tenue de communiquer à l'organisme les éléments lui permettant de s'assurer du respect des règles, définies aux articles R. 342-1 à R. 342-4 du code de la construction et de l'habitation, qui s'imposent au conseil d'administration de l'ANCOLS lorsqu'il adopte une délibération. Par suite, le moyen tiré de ce que la procédure de sanction serait irrégulière, faute que de tels éléments aient été communiqués au requérant, ne peut qu'être écarté. <br/>
<br/>
              3. En second lieu, l'OPH du Territoire de Belfort n'est pas fondé à soutenir que, faute de faire état des engagements qu'il a pris à la suite du contrôle de son activité, la sanction litigieuse serait insuffisamment motivée ni qu'elle serait inintelligible.<br/>
<br/>
              Sur le bien-fondé de la sanction attaquée :<br/>
<br/>
              4. D'une part, aux termes de l'article L. 441 du code de la construction et de l'habitation : " L'attribution des logements locatifs sociaux participe à la mise en oeuvre du droit au logement, afin de satisfaire les besoins des personnes de ressources modestes et des personnes défavorisées. / L'attribution des logements locatifs sociaux doit notamment prendre en compte la diversité de la demande constatée localement ; elle doit favoriser l'égalité des chances des demandeurs et la mixité sociale des villes et des quartiers, en permettant l'accès à l'ensemble des secteurs d'un territoire de toutes les catégories de publics éligibles au parc social, en facilitant l'accès des personnes handicapées à des logements adaptés et en favorisant l'accès des ménages dont les revenus sont les plus faibles aux secteurs situés en dehors des quartiers prioritaires de la politique de la ville. (...) ". L'article L.441-1 du même code prévoit qu'un décret en Conseil d'Etat : " détermine les conditions dans lesquelles les logements construits, améliorés ou acquis et améliorés avec le concours financier de l'Etat ou ouvrant droit à l'aide personnalisée au logement et appartenant aux organismes d'habitations à loyer modéré ou gérés par ceux-ci sont attribués par ces organismes. Pour l'attribution des logements, ce décret prévoit qu'il est tenu compte notamment du patrimoine, de la composition, du niveau de ressources et des conditions de logement actuelles du ménage, de l'éloignement des lieux de travail, de la mobilité géographique liée à l'emploi et de la proximité des équipements répondant aux besoins des demandeurs (...) ". A ce titre, l'article R. 441-1 du même code dispose que : " Les organismes d'habitations à loyer modéré attribuent les logements visés à l'article L.441-1 aux bénéficiaires suivants : / 1° Les personnes physiques séjournant régulièrement sur le territoire français (...) dont les ressources n'excèdent pas des limites fixées (...) par arrêté conjoint du ministre chargé du logement, du ministre chargé de l'économie et des finances et du ministre chargé de la santé (...) " et les articles R. 441-1-1 et R. 441-1-2 prévoient les conditions dans lesquelles l'autorité administrative peut autoriser des plafonds de ressources dérogatoires pour répondre à certains des objectifs mentionnés à l'article L. 441. Enfin, le III de l'article L. 441-2 dispose que la commission d'attribution des logements et d'examen de l'occupation des logements, notamment chargée, dans chaque organisme d'habitation à loyer modéré, d'attribuer nominativement chaque logement locatif " (...) exerce sa mission d'attribution des logements locatifs dans le respect des article L. 441-1 et L.441-2-3, en prenant en compte les objectifs fixés à l'article L. 441 (...) ".<br/>
<br/>
              5. D'autre part, le I de l'article L. 342-14 du code de la construction et de l'habitation dispose que : " (...) En cas de non-respect, pour un ou plusieurs logements, des règles d'attribution et d'affectation de logements prévues au présent code, sans préjudice de la restitution, le cas échéant, de l'aide publique, [la sanction] ne peut excéder dix-huit mois du loyer en principal du ou des logements concernés ; (...) " et l'article L. 342-16 du même code dispose : " Les sanctions mentionnées au I de l'article L. 342-14 sont fixées en fonction de la gravité des faits reprochés, de la situation financière et de la taille de l'organisme (...) ".<br/>
<br/>
              6. Il résulte de l'ensemble des dispositions citées aux points 4 et 5 que si toute attribution d'un logement locatif social à une personne dont les ressources excèdent le plafond applicable à ce logement constitue une méconnaissance, par la commission d'attribution de l'organisme d'habitation à loyer modéré, des règles d'attribution des logements sociaux de nature à justifier une sanction à l'encontre de cet organisme, la gravité de la faute ainsi commise doit néanmoins s'apprécier au regard, notamment, des objectifs fixés par les articles L. 441 et L. 441-1 du code de la construction et de l'habitation. Par suite, lorsqu'une sanction pécuniaire est prononcée contre un organisme d'habitation à loyer modéré, sur le fondement du I de l'article L. 342-14 du même code, en raison de ce que des logements ont été attribués à des personnes dont les ressources dépassaient les plafonds applicables à ces logements, le montant de cette sanction pécuniaire doit être fixé en tenant compte, non seulement de l'ampleur des dépassements, mais aussi, notamment, de leur fréquence, des raisons pour lesquelles ils sont intervenus, des conséquences de ces attributions irrégulières sur les objectifs fixés par les articles L. 441 et L. 441-1 du code de la construction et de l'habitation, de la taille de l'organisme ou de sa situation financière et, le cas échéant, des mesures qu'il a prises pour les faire cesser.<br/>
<br/>
              En ce qui concerne le caractère fautif des attributions de logement litigieuses :<br/>
<br/>
              7. Ainsi qu'il a été dit, la sanction litigieuse est fondée sur l'attribution irrégulière de trente logements par la commission d'attribution des logements, devenue commission d'attribution des logements et d'examen de l'occupation des logements, de l'OPH du Territoire de Belfort au cours des années 2012 à 2016, dont vingt-neuf logements irrégulièrement attribués en raison d'un dépassement du plafond de ressources applicable et un logement attribué alors que le dossier de demande ne permettait pas de s'assurer des ressources du demandeur.<br/>
<br/>
              8. En premier lieu, il résulte de ce qui a été dit au point 6 que la circonstance que certains des vingt-neuf logements attribués par l'OPH du Territoire de Belfort en méconnaissance du plafond de ressources qui leur était applicable auraient rempli les conditions leur permettant de bénéficier, en application de l'article R. 441-1-1 du code de la construction et de l'habitation, d'une autorisation préfectorale permettant de déroger temporairement à ce plafond, ne pouvait légalement justifier, en l'absence d'une telle autorisation préalable, l'attribution dérogatoire de ces logements. L'office requérant ne saurait, par suite, utilement soutenir que certaines attributions litigieuses ne sont pas entachées d'irrégularité au motif qu'elles concerneraient des logements susceptibles de bénéficier d'une telle dérogation ou que, plus généralement, elles auraient eu pour but de résoudre des situations individuelles conformément aux objectifs fixés par les articles L. 441 et L. 441-1 du même code. Il ne saurait davantage utilement invoquer la circonstance que l'Etat était, ainsi que le prévoit l'article L. 441-2 de ce code, représenté au sein de la commission d'attribution lorsqu'ont été décidées les attributions en question.<br/>
<br/>
              9. En second lieu, aux termes de l'article D. 331-12 du code de la construction et de l'habitation, relatif au prêt locatif à usage social (PLUS), dans sa rédaction applicable: " (...) Pour les opérations financées dans les conditions de l'article R. 331-14 autres que celles prévues au II de l'article D. 331-1 et qui bénéficient de subventions prévues aux 2° et 3° de l'article R.331-15 : / I - 30% au moins des logements sont obligatoirement attribués à des personnes dont l'ensemble des ressources est inférieur ou égal à 60% du montant déterminé par l'arrêté précité (...) II.-10 % au plus des logements des opérations ainsi financées par un même maître d'ouvrage peuvent être attribués à des personnes dont l'ensemble des ressources est supérieur de 20 % au plus au montant déterminé par l'arrêté précité ; (...) / Les bailleurs doivent être en mesure de justifier du respect des règles découlant du présent article (...) ". <br/>
<br/>
              10. Si l'OPH du Territoire de Belfort soutient que certaines attributions qui lui sont reprochées concernaient des logements qui, ayant bénéficié du prêt locatif à usage social mentionné ci-dessus, étaient au nombre de ceux qui pouvaient être attribués à des personnes dont les ressources excédaient, dans la limite de 20 %, le plafond applicable, il n'établit pas, faute de justifier de ce qu'était, à l'époque, la situation d'occupation des autres logements de l'opération financés par ce prêt, que cette dérogation restait dans la limite de 10 % des logements fixée par l'article D. 331-12 du code de la construction et de l'habitation. Le moyen tiré de ce que la sanction litigieuse ne pouvait, pour ce motif, se fonder sur l'irrégularité de ces attributions doit, par suite, être écarté.<br/>
<br/>
              En ce qui concerne le montant de la sanction :<br/>
<br/>
              11. Il résulte de l'instruction que, pour fixer à 96 320 euros le montant de la sanction pécuniaire infligée à l'OPH du Territoire de Belfort, les ministres compétents se sont bornés à faire la somme de montants fixés, pour chaque logement irrégulièrement attribué, à neuf ou dix-huit mois de loyer, selon que le dépassement du plafond de ressources pour le logement en question se situait, respectivement, entre 10 % et 100 % de ce plafond, ou au-dessus de 100 % de ce plafond. <br/>
<br/>
              12. L'OPH requérant est, par suite, fondé à soutenir qu'en se fondant exclusivement sur l'ampleur des dépassements du plafond de ressources constatés dans l'attribution irrégulière de trente logements, sans tenir compte, ni de ce que les attributions irrégulières représentaient moins de 1 % des attributions effectuées au cours des cinq années couvertes par le contrôle, ni de ce qu'un tiers d'entre elles faisaient suite à des vacances locatives particulièrement longues et correspondaient à des dépassements modestes, ni de ce que certaines attributions résultaient d'une simple erreur sans caractère systématique, ni enfin prendre en compte la taille et la situation financière de l'organisme, les ministres ont méconnu les principes mentionnés au point 6.<br/>
<br/>
              13. Il y a lieu, par suite, au regard de la gravité des faits reprochés telle qu'elle résulte de l'ensemble des considérations qui viennent d'être mentionnées et compte tenu de la situation financière et de la taille de l'organisme, de ramener à la somme de 60 000 euros le montant de la sanction pécuniaire de 96 320 euros infligée à l'OPH du Territoire de Belfort et d'annuler, par voie de conséquence, la décision rejetant le recours gracieux formé par l'office contre la sanction du 16 mai 2019.<br/>
<br/>
              14. Enfin il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'OPH du Territoire de Belfort au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La sanction prononcée le 16 mai 2019 à l'encontre de l'office public de l'habitat du Territoire de Belfort est réformée pour être ramenée à un montant de 60 000 euros.<br/>
Article 2 : La décision de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et du ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement, rejetant le recours gracieux dirigé contre la sanction du 16 mai 2019 est annulée.<br/>
Article 3 : L'Etat versera à l'office public de l'habitat du Territoire de Belfort la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté. <br/>
Article 5 : La présente décision sera notifiée à l'office public de l'habitat du Territoire de Belfort, à la ministre de la transition écologique et à la ministre déléguée auprès de la ministre de la transition écologique, chargé du logement.<br/>
Copie en sera adressée à l'Agence nationale de contrôle du logement social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-04 LOGEMENT. HABITATIONS À LOYER MODÉRÉ. - ATTRIBUTION DE LOGEMENTS À DES PERSONNES DÉPASSANT LES PLAFONDS DE RESSOURCES - 1) FAIT DE NATURE À JUSTIFIER UNE SANCTION DE L'ORGANISME D'HLM - EXISTENCE - 2) ELÉMENTS À PRENDRE EN COMPTE POUR FIXER LE QUANTUM DE LA SANCTION - 3) ESPÈCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - SANCTION D'UN ORGANISME D'HLM EN RAISON DE L'ATTRIBUTION DE LOGEMENTS À DES PERSONNES DÉPASSANT LES PLAFONDS DE RESSOURCES - 1) FAIT DE NATURE À JUSTIFIER UNE SANCTION - EXISTENCE - 2) ELÉMENTS À PRENDRE EN COMPTE POUR FIXER LE QUANTUM DE LA SANCTION.
</SCT>
<ANA ID="9A"> 38-04 1) Il résulte de l'ensemble des articles L. 342-14, L. 342-16, L. 441, L. 441-1, L. 441-2, R. 441-1, R. 441-1-1 et R. 441-1-2 du code de la construction et de l'habitation (CCH) que si toute attribution d'un logement locatif social à une personne dont les ressources excèdent le plafond applicable à ce logement constitue une méconnaissance, par la commission d'attribution de l'organisme d'habitation à loyer modéré (HLM), des règles d'attribution des logements sociaux de nature à justifier une sanction à l'encontre de cet organisme, la gravité de la faute ainsi commise doit néanmoins s'apprécier au regard, notamment, des objectifs fixés par les articles L. 441 et L. 441-1 du CCH.... ,,2) Par suite, lorsqu'une sanction pécuniaire est prononcée contre un organisme d'HLM, sur le fondement du I de l'article L. 342-14 du même code, en raison de ce que des logements ont été attribués à des personnes dont les ressources dépassaient les plafonds applicables à ces logements, le montant de cette sanction pécuniaire doit être fixé en tenant compte, non seulement de l'ampleur des dépassements, mais aussi, notamment, de leur fréquence, des raisons pour lesquelles ils sont intervenus, des conséquences de ces attributions irrégulières sur les objectifs fixés par les articles L. 441 et L. 441-1 du CCH, de la taille de l'organisme ou de sa situation financière et, le cas échéant, des mesures qu'il a prises pour les faire cesser.,,,3) Ministres compétents s'étant bornés, pour déterminer le quantum de la sanction, à faire la somme de montants fixés, pour chaque logement irrégulièrement attribué, à neuf ou dix-huit mois de loyer, selon que le dépassement du plafond de ressources pour le logement en question se situait, respectivement, entre 10 % et 100 % de ce plafond, ou au-dessus de 100 % de ce plafond,,,En se fondant exclusivement sur l'ampleur des dépassements du plafond de ressources constatés dans l'attribution irrégulière de trente logements, sans tenir compte, ni de ce que les attributions irrégulières représentaient moins de 1 % des attributions effectuées au cours des cinq années couvertes par le contrôle, ni de ce qu'un tiers d'entre elles faisaient suite à des vacances locatives particulièrement longues et correspondaient à des dépassements modestes, ni de ce que certaines attributions résultaient d'une simple erreur sans caractère systématique, ni enfin prendre en compte la taille et la situation financière de l'organisme, les ministres ont méconnu les principes mentionnés aux points précédents.</ANA>
<ANA ID="9B"> 59-02-02-03 1) Il résulte de l'ensemble des articles L. 342-14, L. 342-16, L. 441, L. 441-1, L. 441-2, R. 441-1, R. 441-1-1 et R. 441-1-2 du code de la construction et de l'habitation (CCH) que si toute attribution d'un logement locatif social à une personne dont les ressources excèdent le plafond applicable à ce logement constitue une méconnaissance, par la commission d'attribution de l'organisme d'habitation à loyer modéré (HLM), des règles d'attribution des logements sociaux de nature à justifier une sanction à l'encontre de cet organisme, la gravité de la faute ainsi commise doit néanmoins s'apprécier au regard, notamment, des objectifs fixés par les articles L. 441 et L. 441-1 du CCH.... ,,2) Par suite, lorsqu'une sanction pécuniaire est prononcée contre un organisme d'HLM, sur le fondement du I de l'article L. 342-14 du même code, en raison de ce que des logements ont été attribués à des personnes dont les ressources dépassaient les plafonds applicables à ces logements, le montant de cette sanction pécuniaire doit être fixé en tenant compte, non seulement de l'ampleur des dépassements, mais aussi, notamment, de leur fréquence, des raisons pour lesquelles ils sont intervenus, des conséquences de ces attributions irrégulières sur les objectifs fixés par les articles L. 441 et L. 441-1 du CCH, de la taille de l'organisme ou de sa situation financière et, le cas échéant, des mesures qu'il a prises pour les faire cesser.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour une espèce similaire, CE, décision du même jour, OPH Drôme aménagement habitat, n°s 432682 436311, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
