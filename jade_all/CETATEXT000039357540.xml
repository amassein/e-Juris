<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039357540</ID>
<ANCIEN_ID>JG_L_2019_11_000000412566</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/75/CETATEXT000039357540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 08/11/2019, 412566</TITRE>
<DATE_DEC>2019-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412566</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412566.20191108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Mme A... B... a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la décision de l'inspecteur du travail de l'unité de contrôle 2 des Alpes-Maritimes du 21 septembre 2015 accordant à la société Hôtel Negresco l'autorisation de la licencier. Par un jugement n° 1504611 du 28 juin 2016, le tribunal administratif a annulé cette décision. <br/>
<br/>
               Par un arrêt n° 16MA03001 du 18 mai 2017, la cour administrative d'appel de Marseille a, sur appel de la société Hôtel Negresco, annulé ce jugement et rejeté la demande de Mme B....<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cet arrêt ;<br/>
<br/>
               2°) réglant l'affaire au fond, de rejeter l'appel de la société Hôtel Negresco ;<br/>
<br/>
               3°) de mettre à la charge de la société Hôtel Negresco la somme 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu :<br/>
               - le code du travail ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
               - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
               La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme B... et à la SCP Gatineau, Fattaccini, avocat de la société Hôtel Negresco.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 21 septembre 2015, l'inspecteur du travail de l'unité de contrôle 2 des Alpes-Maritimes a autorisé la société Hôtel Negresco à licencier pour motif disciplinaire Mme B..., employée en qualité de femme de chambre et exerçant les mandats de déléguée syndicale ainsi que de représentante syndicale au comité d'entreprise. Par un arrêt du 18 mai 2017 contre lequel Mme B... se pourvoit en cassation, la cour administrative d'appel de Marseille a, sur appel de la société, annulé le jugement du tribunal administratif de Nice du 28 juin 2016 annulant la décision d'autorisation de l'inspecteur du travail et rejeté la demande de Mme B....<br/>
<br/>
               2. En premier lieu, la cour administrative d'appel de Marseille, pour écarter le moyen tiré de ce qu'un membre du comité d'entreprise appelé à se prononcer sur le projet de licenciement de la requérante n'avait pas été régulièrement convoqué, a estimé que l'intéressée devait être regardée, dans les circonstances de l'espèce, comme ayant reçu notification de la convocation en temps utile. Ce faisant, la cour a souverainement apprécié les pièces du dossier dont elle était saisie sans entacher son arrêt de dénaturation. Elle n'a pas davantage commis d'erreur de droit en se fondant à ce titre, notamment, sur le procès-verbal de la réunion du comité d'entreprise du 22 juillet 2015 indiquant que l'ensemble des membres présents avaient déclaré avoir bénéficié du temps nécessaire à l'étude du dossier qui leur était soumis.<br/>
<br/>
               3. En deuxième lieu, aux termes du premier alinéa de l'article R. 2421-11 du code du travail : " L'inspecteur du travail procède à une enquête contradictoire au cours de laquelle le salarié peut, sur sa demande, se faire assister d'un représentant de son syndicat ". Cette disposition implique, pour le salarié dont le licenciement est envisagé, le droit d'être entendu personnellement et individuellement par l'inspecteur du travail, sauf s'il s'abstient, sans motif légitime, de donner suite à la convocation. Ce droit ne saurait être exercé collectivement, même si le salarié protégé demande à être entendu en même temps qu'un autre salarié protégé faisant également l'objet d'une procédure d'autorisation administrative de licenciement.<br/>
<br/>
               4. Il résulte de ce qui précède qu'en jugeant que l'inspecteur du travail avait pu, sans entacher d'illégalité sa décision, entendre Mme B... en même temps qu'un autre salarié mis en cause pour les mêmes faits au motif qu'elle n'avait pas demandé à être entendue seule, la cour administrative d'appel de Marseille a entaché son arrêt d'erreur de droit. <br/>
<br/>
               5. Toutefois, si Mme B... soutenait devant la cour administrative d'appel que son audition conjointe avec un autre salarié, le 3 juillet 2015, était irrégulière, il ressort des pièces du dossier soumis au juge du fond que cette audition avait eu lieu dans le cadre d'une procédure administrative antérieure. D'ailleurs, la décision contestée, en date du 17 septembre 2015, est intervenue au terme d'une procédure au cours de laquelle a eu lieu, le 14 septembre 2015, une nouvelle audition, individuelle, de Mme B.... Ainsi le moyen invoqué devant les juges du fond était inopérant. Il y a lieu de substituer ce motif de pur droit au motif erroné retenu par la cour. <br/>
<br/>
               6. En troisième lieu, le caractère contradictoire de l'enquête menée conformément aux articles R. 2421-4 et R. 2421-11 du code du travail implique que le salarié protégé soit mis à même de prendre connaissance de l'ensemble des pièces produites par l'employeur à l'appui de sa demande, dans des conditions et des délais lui permettant de présenter utilement sa défense. En jugeant que Mme B... n'était pas fondée à soutenir qu'elle n'avait pu présenter utilement sa défense au seul motif qu'elle ne s'était pas vue communiquer le rapport du cabinet Revest la mettant en cause alors que, d'une part, l'employeur lui avait donné lecture des extraits de ce document la concernant lors de l'entretien préalable tandis que l'inspecteur du travail en avait fait état lors de l'entretien mené au titre de l'enquête, et que, d'autre part, elle n'établissait pas ni même n'alléguait en avoir expressément demandé la communication, la cour administrative d'appel n'a pas entaché son arrêt d'erreur de droit ni dénaturé les pièces du dossier. <br/>
<br/>
               7. En quatrième lieu, en jugeant qu'eu égard à l'état d'anxiété des collègues de la salariée mise en cause se disant victimes de harcèlement moral de sa part et au grave préjudice qu'était susceptible de leur porter la divulgation de leur identité, l'inspecteur du travail avait pu, sans entacher sa décision d'illégalité, s'abstenir de communiquer à l'intéressée l'identité et la qualité de ces personnes entendues lors de l'enquête, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit. <br/>
<br/>
               8. En cinquième lieu, en faisant état des violences, notamment psychologiques, ainsi que des menaces proférées par l'intéressée contre ses collègues et en faisant référence au rapport du cabinet Revest, la cour administrative d'appel n'a pas entaché son arrêt d'insuffisance de motivation dans l'énoncé des faits reprochés. <br/>
<br/>
               9. Enfin, en jugeant que les faits reprochés n'étaient pas prescrits dès lors que l'employeur n'avait pu en mesurer pleinement la nature et l'ampleur qu'à partir du 5 mai 2015, date de la remise du rapport précédemment mentionné révélant des agissements continus de la part de l'intéressée, la cour administrative d'appel, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit.   <br/>
<br/>
               10. Compte tenu de tout ce qui précède, Mme B... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
               11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Hôtel Negresco qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B... la somme demandée par la société Hôtel Negresco au même titre.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : Les conclusions de la société Hôtel Negresco présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme A... B..., à la société Hôtel Negresco et à la ministre du travail.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-02-01 TRAVAIL ET EMPLOI. - LICENCIEMENTS. - AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. - PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. - ENTRETIEN PRÉALABLE. - DROIT DU SALARIÉ PROTÉGÉ DONT LE LICENCIEMENT EST ENVISAGÉ D'ÊTRE ENTENDU (ART. R. 2421-11 DU CODE DU TRAVAIL) - DROIT NE POUVANT ÊTRE EXERCÉ COLLECTIVEMENT, MÊME À LA DEMANDE DU SALARIÉ CONCERNÉ [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-02-01 L'article R. 2421-11 du code du travail implique, pour le salarié protégé dont le licenciement est envisagé, le droit d'être entendu personnellement et individuellement par l'inspecteur du travail, sauf s'il s'abstient, sans motif légitime, de donner suite à la convocation. Ce droit ne saurait être exercé collectivement, même si le salarié protégé demande à être entendu en même temps qu'un autre salarié protégé faisant également l'objet d'une procédure d'autorisation administrative de licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., Cass. soc, 23 avril 2003, n° 01-40.817, Bull civ. 2003, V, n° 138.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
