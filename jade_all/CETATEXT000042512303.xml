<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042512303</ID>
<ANCIEN_ID>JG_L_2020_11_000000428027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/51/23/CETATEXT000042512303.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 09/11/2020, 428027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP L. POULET-ODENT ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428027.20201109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... et Bernadette A... ont demandé au tribunal administratif de Strasbourg d'annuler, d'une part, la décision du 29 novembre 2015 par laquelle le maire de Strasbourg a implicitement rejeté leur demande tendant au retrait de l'arrêté du 25 février 2014 délivrant à la SARL Foncière du Roseneck un permis de construire modificatif n° 67 482 12 V0042 M2, d'autre part, l'arrêté du 11 septembre 2015 accordant à la SARL Foncière du Roseneck un permis modificatif n° 67 482 12 V 0042 M3. Par un jugement n° 1505783-1507250 du 9 novembre 2017, le tribunal administratif de Strasbourg a, d'une part, annulé la décision implicite de rejet du 29 novembre 2015 et enjoint au maire de Strasbourg de procéder au retrait du permis de construire modificatif du 25 février 2014, d'autre part, annulé l'arrêté du 11 septembre 2015.<br/>
<br/>
              Par un arrêt n°s 18NC00068, 18NC00070, 18NC00162 du 13 décembre 2018, la cour administrative d'appel de Nancy a, sur appel de la SARL Foncière du Roseneck, annulé le jugement du tribunal administratif de Strasbourg, rejeté les demandes présentées par M. et Mme A... devant le tribunal administratif de Strasbourg et mis à leur charge le versement à la ville de Strasbourg et à la SARL Foncière du Roseneck d'une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 février et 15 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Strasbourg et de la SARL Foncière du Roseneck la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - l'ordonnance n° 2011-1539 du 29 décembre 2011 ;<br/>
              - le décret n° 2011-2054 du 29 décembre 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - Les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Buk Lament-Robillot, avocat de M. et Mme A..., à la SCP Sevaux, Mathonnet, avocat de la commune de Strasbourg, et à la SCP L. Poulet-Odent, avocat de la SARL Foncière du Roseneck ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 13 septembre 2012, le maire de Strasbourg a délivré à la SARL Foncière du Roseneck un permis de construire pour le réaménagement d'un ensemble immobilier lui appartenant situé à l'angle formé par le quai Jacques Sturm et la rue du général de Castelnau. Par un deuxième arrêté du 25 février 2014, la ville de Strasbourg a délivré à cette société du Roseneck un premier permis modificatif (ci-après permis de construire modificatif n° 1). <br/>
<br/>
              2.	Par un arrêté du 11 septembre 2015, la ville de Strasbourg a délivré à la SARL Foncière du Roseneck un second permis modificatif (ci-après permis de construire modificatif n° 2). <br/>
<br/>
              3.	Par un jugement du 9 novembre 2017, le tribunal administratif de Strasbourg, saisi par les époux A..., a, d'une part, annulé la décision implicite née le 29 novembre 2015 rejetant la demande des époux A... tendant au retrait du permis de construire modificatif n° 1 et enjoint à la ville de Strasbourg de retirer ce permis modificatif, d'autre part, annulé le permis de construire modificatif n° 2. Par un arrêt du 13 décembre 2018, la cour administrative d'appel de Nancy a, sur appel de la SARL Foncière du Roseneck, annulé le jugement du tribunal administratif de Strasbourg et rejeté les demandes présentées par M. et Mme A... devant le tribunal administratif de Strasbourg.<br/>
<br/>
              4.	En premier lieu, l'article R. 600-1 du code de l'urbanisme impose à l'auteur d'un recours contentieux ou d'un recours administratif contre une autorisation d'urbanisme de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification est prescrite à peine d'irrecevabilité du recours, si bien que son absence doit être relevée d'office par le juge, en première instance comme en appel. Le juge d'appel statue irrégulièrement s'il ne s'assure pas du respect de la formalité prévue par cet article.<br/>
<br/>
              5.	La cour, après avoir jugé qu'il ressortait des pièces des dossiers de première instance que le tribunal administratif de Strasbourg ne s'était pas assuré du respect par M. et Mme A... de la formalité prévue par l'article R. 600-1 du code de l'urbanisme et que, par suite, la Foncière du Roseneck et la ville de Strasbourg étaient fondées à soutenir que le tribunal administratif avait statué irrégulièrement, s'est prononcée, après s'être assurée du respect des dispositions prévues par ces dispositions, sur les demandes de M. et Mme A.... Ce faisant, elle n'a commis aucune erreur de droit.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les demandes relatives au permis de construire modificatif n° 1 :<br/>
<br/>
              6.	En premier lieu, les requérants font valoir que la cour a commis une erreur de droit en jugeant que l'augmentation de la surface entre le permis initial et le permis modificatif n° 1 s'expliquait par le changement de mode de calcul des surfaces dans le droit de l'urbanisme, résultant de l'ordonnance du 29 décembre 2011 relative à la définition des surfaces de plancher prises en compte dans le droit de l'urbanisme et du décret du 29 décembre 2011 pris pour son application, qui ont substitué la notion de surface de plancher à la surface hors oeuvre nette pour le calcul des surfaces, alors que l'application de ces textes aurait dû entraîner une baisse de la surface et non une hausse. Toutefois, dès lors que le passage de la surface hors oeuvre nette à la surface de plancher n'entraîne pas une diminution pour chaque construction, compte tenu de la modification des autres dispositions de l'article R. 112-2 du code de l'urbanisme introduites par le décret du 29 décembre 2011 et entrées en vigueur le 1er mars 2012, soit postérieurement à la demande de permis initial présentée par la SARL Foncière du Roseneck, le moyen ne peut qu'être écarté.<br/>
<br/>
              7.	En deuxième lieu, la cour n'a pas dénaturé les pièces du dossier en jugeant que l'erreur commise dans le calcul de la surface à usage d'habitation dans le dossier de la demande de permis de construire initial était présentée dans la demande de permis modificatif n°1 et que l'administration disposait d'éléments suffisants pour se prononcer sur cette dernière. <br/>
<br/>
              8.	En troisième lieu, dès lors qu'elle avait écarté l'existence d'une manoeuvre délibérée destinée à tromper l'administration, c'est par des motifs surabondants que la cour a déduit des dispositions de l'article 12UB du plan d'occupation des sols de la commune de Strasbourg que, le changement de destination d'un immeuble existant ou le changement d'affectation de certaines des surfaces existantes étant, en eux-mêmes, sans incidence sur l'application des règles prévues en matière de stationnement, les époux A... n'étaient pas fondés à soutenir que la Foncière du Roseneck avait délibérément minoré les surfaces destinées au commerce pour contourner les règles locales d'urbanisme en matière de stationnement. Par suite, les moyens tirés de ce que la cour aurait, ce faisant, insuffisamment motivé son arrêt ou commis une erreur de droit ne peuvent, en tout état de cause, qu'être écartés.<br/>
<br/>
              9.	En quatrième lieu, si les requérants soutiennent que la cour a commis une erreur de qualification juridique en assimilant " open space " et salle de réception, il ressort des termes de son arrêt qu'elle a jugé que la commune de Strasbourg était en mesure de comprendre que l'" open space " serait transformé en salle de réception grâce aux plans joints à la demande de permis modificatif n°1 et que, par conséquent, cette demande n'était pas frauduleuse. Elle a ainsi, sans dénaturer les pièces du dossier, jugé que le pétitionnaire n'avait pas dissimulé frauduleusement ses intentions à la commune de Strasbourg. Elle n'était, sur ce moyen, pas tenue de répondre à tous les arguments des écritures qui lui étaient soumises, en particulier celui tiré de ce qu'elle aurait dû vérifier que la création de la seconde salle de réunion n'avait pas été comptabilisée dans le tableau des surfaces.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les demandes relatives au permis de construire modificatif n° 2 :<br/>
<br/>
              10.	En premier lieu, la cour a suffisamment motivé son arrêt en jugeant que l'avis rendu par l'architecte des bâtiments de France sur le permis modificatif n° 2 avait été rendu au vu d'un dossier complet. Contrairement à ce que soutiennent les requérants, la cour n'a pas méconnu les dispositions de l'article R. 611-7 du code de justice administrative en n'invitant pas les parties à préciser le contenu du dossier transmis à l'architecte des bâtiments de France.<br/>
<br/>
              11.	En deuxième lieu, en jugeant que les pièces jointes au dossier de demande, notamment les plans des différents étages et les plans de coupe, étaient de nature à permettre à la commune de Strasbourg de délivrer le permis de construire modificatif n° 2 en toute connaissance de cause, sans que les inexactitudes affectant le tableau de répartition des surfaces et l'indication des places de stationnement soient de nature à fausser son appréciation du projet, la cour a suffisamment motivé son arrêt et n'a pas dénaturé les pièces du dossier.<br/>
<br/>
              12.	En troisième lieu, aux termes de l'article 11 UB du plan d'occupation des sols de la ville de Strasbourg : " 1. Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains, ainsi qu'à la conservation des perspectives monumentales. / La définition volumétrique et architecturale des façades et des toitures doit s'intégrer à la composition de la rue, de la place, de l'îlot... / En outre, les constructions nouvelles doivent s'intégrer harmonieusement à la séquence dans laquelle elles s'insèrent, en tenant notamment compte des hauteurs des constructions riveraines et voisines. Pour cette raison il peut être imposé des hauteurs inférieures aux maximales fixées à l'article 10 UB ci-dessus. (...) ".<br/>
<br/>
              13.	En jugeant que la surélévation de la toiture du bâtiment A prévue par le permis de construire modificatif n° 2 n'était pas de nature à porter atteinte à l'intérêt des lieux car les immeubles avoisinants n'étaient pas tous de la même hauteur et que l'apposition d'un bardage métallique de couleur champagne sur l'une des façades du bâtiment B, immeuble datant des années 1970 et non de l'empire allemand et ne présentant pas d'intérêt architectural, la cour, qui a examiné les éléments extérieurs du projet dans leur ensemble, n'a pas commis d'erreur de droit au regard des dispositions de l'article 11UB du plan d'occupation des sols ni dénaturé les pièces du dossier.<br/>
<br/>
              14.	Il résulte de tout ce qui précède que le pourvoi de M. et Mme A... ne peut qu'être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à leur charge une somme à ce titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme A... est rejeté.<br/>
Article 2 : Les conclusions de la commune de Strasbourg et de la SARL Foncière du Roseneck présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée M. et Mme B... et Bernadette A..., à la commune de Strasbourg et de la SARL Foncière du Roseneck.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
