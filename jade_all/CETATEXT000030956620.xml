<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956620</ID>
<ANCIEN_ID>JG_L_2015_07_000000374991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956620.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 374991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374991.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête et un mémoire en réplique enregistrés les 28 janvier et 23 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, l'Union des industries de fertilisation (Unifa) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande du 27 juin 2013 tendant à l'abrogation des dispositions des articles R. 255-1 et R. 255-1-1 du code rural et de la pêche maritime en tant qu'elles organisent une procédure d'homologation préalable par le ministre chargé de l'agriculture, après avis de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail (ANSES), des matières fertilisantes et des supports de culture destinés à être importés, détenus, mis en vente, vendus, utilisés et distribués sur le territoire français ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment ses articles 34, 36 et 267 ;<br/>
              - le règlement (CE) n° 2003/2003 du Parlement européen et du Conseil du 13 octobre 2003 ;<br/>
              - le règlement (CE) n° 764/2008 du Parlement européen et du Conseil du 9 juillet 2008 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. D'une part, aux termes de l'article 1er du règlement (CE) n° 764/2008 du Parlement européen et du Conseil du 9 juillet 2008 établissant les procédures relatives à l'application de certaines règles techniques nationales à des produits commercialisés légalement dans un autre État membre et abrogeant la décision n° 3052/95/CE : " 1. Le présent règlement a pour objectif de renforcer le fonctionnement du marché intérieur en améliorant la libre circulation des marchandises. / 2. Le présent règlement établit les règles et procédures à suivre par les autorités compétentes d'un Etat membre lorsqu'elles prennent ou ont l'intention de prendre une décision visée à l'article 2, paragraphe 1, qui entraverait la libre circulation d'un produit commercialisé légalement dans un autre Etat membre et relevant de l'article 28 du traité (...) ". Aux termes de l'article 2 de ce règlement : " 1. Le présent règlement s'applique aux décisions administratives, dont les opérateurs économiques sont destinataires, qui sont prises ou envisagées, sur la base d'une règle technique au sens du paragraphe 2, pour tout produit (...) commercialisé légalement dans un autre État membre, et dont l'effet direct ou indirect est l'un des suivants : / a) l'interdiction de mise sur le marché du produit ou du type de produit (...) / 2. Aux fins du présent règlement, on entend par règle technique toute disposition législative, réglementaire ou autre disposition administrative d'un État membre : / a) qui ne fait pas l'objet d'une harmonisation au niveau communautaire, et / b) qui interdit la commercialisation d'un produit ou d'un type de produit sur le territoire dudit État membre, ou dont le respect est obligatoire lorsqu'un produit ou un type de produit est commercialisé sur le territoire dudit État membre (...) ". Aux termes de son article 5 : " Les États membres ne rejettent pas en invoquant des motifs relatifs à sa compétence les certificats ou rapports d'essais délivrés par un organisme d'évaluation de la conformité (...) ". Aux termes de son article 6 : " 1. Lorsqu'une autorité compétente a l'intention d'adopter une décision visée à l'article 2, paragraphe 1, elle envoie à l'opérateur économique (...) une notification écrite de cette intention, précisant la règle technique sur laquelle la décision doit être fondée et fournissant des éléments techniques ou scientifiques attestant que : / a)  la décision prévue est justifiée par l'une des raisons d'intérêt public visées à l'article 30 du traité ou par référence à une autre raison impérieuse d'intérêt public ; / b)  la décision prévue est conforme au but d'atteindre l'objectif visé et n'excède pas ce qui est nécessaire pour atteindre cet objectif. / Toute décision prévue doit être fondée sur les caractéristiques du produit ou du type de produit en question. / Après avoir reçu une telle notification, l'opérateur économique concerné dispose d'au moins vingt jours ouvrables pour soumettre ses observations. La notification précise le délai imparti pour la présentation des observations. / 2.  Toute décision visée à l'article 2, paragraphe 1, est prise et notifiée à l'opérateur économique concerné et à la Commission dans une période de vingt jours ouvrables suivant l'expiration du délai de réception des observations de l'opérateur économique visé au paragraphe 1 du présent article. La décision tient dûment compte desdites observations et elle précise les motifs sur lesquels elle repose, y compris ceux qui motivent, le cas échéant, le rejet des arguments avancés par l'opérateur, ainsi que les éléments techniques ou scientifiques visés au paragraphe 1 du présent article. / Lorsque la complexité de la question le justifie dûment, l'autorité compétente peut prolonger une fois, pour une durée maximale de vingt jours ouvrables, la période visée au premier alinéa. Cette prolongation doit être dûment motivée et notifiée à l'opérateur économique avant l'expiration du délai initial. / Toute décision visée à l'article 2, paragraphe 1, indique en outre les moyens de recours disponibles en vertu du droit applicable dans l'État membre concerné ainsi que les délais s'appliquant à ces moyens de recours. Une telle décision peut faire l'objet d'un recours devant les juridictions nationales ou d'autres instances de recours. / (...) 4. Lorsque l'autorité compétente ne notifie pas à l'opérateur économique une décision visée à l'article 2, paragraphe 1, dans la période fixée au paragraphe 2 du présent article, le produit est réputé être légalement commercialisé dans cet État membre, pour ce qui concerne l'application de la règle technique visée au paragraphe 1 du présent article ".<br/>
<br/>
              2. D'autre part, aux termes de l'article L. 255-1 du code rural et de la pêche maritime : " (...) Au sens du présent chapitre : / 1° Les matières fertilisantes comprennent les engrais, les amendements et, d'une manière générale, tous les produits dont l'emploi est destiné à assurer ou à améliorer la nutrition des végétaux ainsi que les propriétés physiques, chimiques et biologiques des sols ; / 2° Les supports de culture sont des produits destinés à servir de milieu de culture à certains végétaux ". Aux termes de l'article L. 255-2 du même code : " Il est interdit d'importer, de détenir en vue de la vente, de mettre en vente, de vendre, d'utiliser ou de distribuer à titre gratuit, sous quelque dénomination que ce soit, des matières fertilisantes et des supports de culture lorsqu'ils n'ont pas fait l'objet d'une homologation ou, à défaut, d'une autorisation provisoire de vente, d'une autorisation de distribution pour expérimentation ou d'une autorisation d'importation. / Toutefois, sous réserve de l'innocuité des matières fertilisantes ou supports de culture à l'égard de l'homme, des animaux, ou de leur environnement, dans des conditions d'emploi prescrites ou normales, les dispositions du premier alinéa ne sont pas applicables : / 1° Aux produits dont la normalisation, au sens de la loi du 24 mai 1941, a été rendue obligatoire ; / 2° Aux produits mis sur le marché dans les conditions prévues par les dispositions réglementaires prises en application de directives des communautés européennes, lorsque ces dispositions ne prévoient ni homologation ni autorisation préalable à la mise en vente ;/ 3° Aux rejets, dépôts, déchets ou résidus dont l'évacuation, le déversement ou l'épandage sur des terrains agricoles est réglementé, cas par cas, en application de la loi n° 64-1245 du 16 décembre 1964 relative au régime et à la répartition des eaux et à la lutte contre leur pollution ou du livre V (titre Ier) du code de l'environnement ou de la loi n° 92-3 du 3 janvier 1992 sur l'eau, eu égard à la conservation de la fertilité des sols ; / 4° Aux produits organiques bruts et aux supports de culture d'origine naturelle non mentionnés au 3°, livrés en l'état ou mélangés entre eux, lorsqu'ils sont obtenus à partir de matières naturelles sans traitement chimique, qu'ils constituent des sous-produits d'une exploitation agricole ou d'un établissement non agricole d'élevage ou d'entretien des animaux et sont cédés directement, à titre gratuit ou onéreux, par l'exploitant ". Aux termes de l'article R. 255-1 du même code : " Un arrêté des ministres chargés de l'agriculture, de la santé, du travail, de la consommation et de l'environnement fixe, après avis de la commission des produits phytopharmaceutiques, des matières fertilisantes et supports de culture et de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail, les modalités de dépôt des demandes d'homologation et d'autorisation provisoire de vente ou d'importation prévues à l'article L. 255-2 et notamment la composition des dossiers de demande. / Le ministre chargé de l'agriculture prend, après avis de l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail, les décisions d'octroi des homologations et des autorisations provisoires de vente ou d'importation. Il prend, s'il le juge utile après avis de l'agence, les décisions de retrait de ces homologations et autorisations et les décisions de suppression des dispenses d'homologation prévues aux 1° à 4° de l'article L. 255-2 ". Aux termes de l'article R. 255-1-1 du même code : " La demande d'homologation est adressée à l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail. Lorsque le dossier est incomplet, l'agence invite le demandeur à le compléter. Lorsque le dossier est complet, l'agence en accuse réception et transmet copie de cet accusé de réception au ministre chargé de l'agriculture. / Dans un délai de six mois à compter de la date de l'accusé de réception, l'agence transmet au ministre chargé de l'agriculture un avis comprenant les éléments mentionnés à l'article R. 253-3. Cet avis est également transmis aux ministres chargés de la santé, du travail, de la consommation et de l'environnement. / Dans le cas de produits bénéficiant déjà d'une homologation ou d'une autorisation de mise sur le marché délivrée par un autre Etat membre de l'Union européenne ou partie à l'accord instituant l'Espace économique européen, ce délai est de trois mois. / Dans le cas de produits bénéficiant déjà d'une homologation ou d'une autorisation provisoire de vente ou d'importation en France, ce délai est de deux mois. / Lorsque l'évaluation du produit l'exige, l'agence peut réclamer au demandeur des informations complémentaires en lui impartissant pour les fournir un délai qui ne peut excéder deux mois. Le délai dont dispose l'agence pour donner son avis est prorogé d'une durée égale. / Lorsque l'agence n'a pas émis son avis à l'issue des délais prévus aux alinéas précédents, son avis est réputé défavorable. / Le ministre chargé de l'agriculture notifie sa décision au demandeur et en adresse copie à l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail dans un délai de deux mois à compter de la réception de l'avis de l'agence, ou, si l'agence n'a pas émis d'avis, à compter de l'expiration du délai qui lui est imparti. / Les décisions relatives à la mise sur le marché des matières fertilisantes et supports de culture sont publiées par voie électronique par l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail ".<br/>
<br/>
              3. En premier lieu, la requérante, qui ne critique pas les dispositions, citées au point 2 de la présente décision, de l'article L. 255-2 du code rural et de la pêche maritime, lesquelles ont directement pour objet d'interdire l'importation sur le marché français, la détention en vue de la vente, la mise en vente, la vente et la distribution de matières fertilisantes et de supports de cultures à défaut d'homologation préalable ou d'une autorisation provisoire, ne peut utilement soutenir que les dispositions des articles R. 255-1 et R. 255-1-1 du même code seraient entachées d'illégalité au motif qu'elles " instituent une procédure d'homologation d'engrais déjà autorisés dans un Etat membre " de l'Union européenne autre que la France, ou que cette procédure serait, dans son principe, contraire au principe de proportionnalité.<br/>
<br/>
              4. En deuxième lieu, les dispositions du règlement (CE) n° 764/2008 du 9 juillet 2008, lequel fixe un cadre général aux décisions administratives ayant pour effet d'interdire la mise sur le marché intérieur de certains produits commercialisés par un opérateur ou de subordonner la mise sur le marché au respect d'un ensemble de règles destinées notamment à protéger les consommateurs ou l'environnement, sont directement applicables aux décisions prises, dans le cadre de la procédure d'homologation des matières fertilisantes et supports de culture instituée par l'article L. 255-2 du code rural et de la pêche maritime et organisée par l'article R. 255-1-1 du même code, par l'autorité administrative compétente, laquelle est même tenue, au besoin, de laisser inappliquée une règle de procédure qui serait contraire aux dispositions du règlement (CE) n° 764/2008. Ainsi, l'Unifa ne peut utilement soutenir que la procédure définie aux articles R. 251-1 et R. 255-1-1 du code rural et de la pêche maritime serait illégale au motif qu'elle ne transposerait pas ou ne reproduirait pas les dispositions du règlement (CE) n° 764/2008.<br/>
<br/>
              5. En troisième lieu, il résulte des termes mêmes des paragraphes 1 et 2 de l'article 6 du règlement (CE) n° 764/2008, d'une part, que l'autorité compétente de l'Etat membre concerné doit impartir à un opérateur économique, pour lui permettre de présenter ses observations, un délai minimum de vingt jours ouvrables, lorsque cette autorité envisage de refuser ou de limiter la mise sur le marché d'un produit sur la base d'une règle technique et, d'autre part, que la décision administrative doit être prise dans une période de vingt jours ouvrables suivant l'expiration du délai imparti à l'opérateur pour produire ses observations. Ainsi, l'Unifa n'est pas fondée à soutenir que l'article R. 255-1-1 du code rural et de la pêche maritime, en tant qu'il fixe à trois mois la durée maximale de la procédure d'homologation d'engrais dont la commercialisation est déjà autorisée dans un autre Etat membre de l'Union européenne, ne serait pas compatible avec les dispositions du paragraphe 2 de l'article 6 du règlement (CE) n° 764/2008. En outre, le moyen tiré de ce que le délai de trois mois précité serait en tout état de cause disproportionné n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              6. En quatrième lieu, si la requérante fait valoir que l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail (ANSES) n'émet pas systématiquement un avis dans le délai qui lui est imparti par les dispositions de l'article R. 255-1-1 du code rural et de la pêche maritime, une telle critique dirigée contre une pratique administrative est inopérante à l'appui des conclusions de la requête.<br/>
<br/>
              7. Enfin, le moyen tiré de ce que la procédure d'homologation exposerait le pétitionnaire à des frais importants n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              8. Il résulte de tout ce qui précède que l'Unifa n'est pas fondée à demander l'annulation de la décision implicite de rejet de sa demande tendant à l'abrogation des dispositions des articles R. 255-1 et R. 255-1-1 du code rural et de la pêche maritime. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Unifa est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Union des industries de fertilisation, au Premier ministre, à la ministre des affaires sociales et de la santé et au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du Gouvernement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
