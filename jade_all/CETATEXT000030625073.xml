<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625073</ID>
<ANCIEN_ID>JG_L_2015_05_000000382526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 22/05/2015, 382526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382526.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. J...C..., M. K... B..., Mme L... A...et Mme I... D...ont demandé au tribunal administratif de Caen d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Courseulles-sur-Mer (14470). Par un jugement n° 1400765 du 17 juin 2014, le tribunal administratif de Caen a annulé l'élection de M. H... G...en qualité de conseiller municipal et proclamé M. F... E...élu à sa place.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 11 juillet et 31 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M.  G...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a annulé son élection et proclamé M. E... élu à sa place ;<br/>
<br/>
              2°) de rejeter, dans cette mesure, la protestation de M. C... et autres.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.C..., de M.B..., de Mme A...et de Mme D...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 231 du code électoral dans sa rédaction issue de l'article 22 de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires, et modifiant le calendrier électoral : " (...) Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : (...) / 8° Les personnes exerçant, au sein du conseil régional, du conseil départemental, de la collectivité territoriale de Corse, de Guyane ou de Martinique, d'un établissement public de coopération intercommunale à fiscalité propre ou de leurs établissements publics, les fonctions de directeur général des services, directeur général adjoint des services, directeur des services, directeur adjoint des services ou chef de service, ainsi que les fonctions de directeur de cabinet, directeur adjoint de cabinet ou chef de cabinet en ayant reçu délégation de signature du président, du président de l'assemblée ou du président du conseil exécutif " ;<br/>
<br/>
              2. Considérant que les dispositions du 8e de l'article L. 231 du code électoral citées au point 1 doivent s'entendre, eu égard à leur objet, comme visant non le conseil régional ou le conseil départemental mais les collectivités dont ils sont les organes délibérants ; qu'entrent ainsi dans le champ de ces dispositions, qui sont d'interprétation stricte, d'une part, les établissements publics dépendant exclusivement d'une région ou d'un département, ainsi que des autres collectivités territoriales et établissements mentionnés par ces dispositions, d'autre part, ceux qui sont communs à plusieurs de ces collectivités ; que doivent être seulement regardés comme dépendant de ces collectivités ou établissements ou comme étant communs à plusieurs collectivités, pour l'application de ces dispositions, les établissements publics créés par ces seuls collectivités ou établissements ou à leur demande ; qu'en revanche, il ne ressort pas de ces dispositions que l'inéligibilité qu'elles prévoient s'étende aux personnes exerçant les fonctions qu'elles mentionnent dans d'autres établissements publics que ceux qui dépendent d'une ou plusieurs des collectivités et établissements qu'elles citent ou sont communs à plusieurs de ces collectivités ;<br/>
<br/>
              3. Considérant que, par le jugement attaqué, le tribunal administratif de Caen a annulé l'élection de M. G... en qualité de conseiller municipal et, en l'absence d'altération de la sincérité globale du scrutin, proclamé M.E..., inscrit immédiatement après le dernier élu de la liste électorale sur laquelle figurait M. G..., au motif que ce dernier avait exercé des fonctions de chef de service au sein du service départemental d'incendie et de secours du Calvados moins de six mois avant le scrutin ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 1424-1 du code général des collectivités territoriales : " Il est créé dans chaque département un établissement public, dénommé "service départemental d'incendie et de secours", qui comporte un corps départemental de sapeurs-pompiers, composé dans les conditions prévues à l'article L. 1424-5 et organisé en centres d'incendie et de secours " ; qu'aux termes de l'article L. 1424-24 du même code : " Le service départemental d'incendie et de secours est administré par un conseil d'administration composé de représentants du département, des communes et des établissements publics de coopération intercommunale compétents en matière de secours et de lutte contre l'incendie (...) " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que les services départementaux d'incendie et de secours, qui associent, pour la gestion et la mise en oeuvre des moyens au niveau local, les communes au département et aux établissements publics de coopération intercommunale, ne sont pas seulement rattachés à des collectivités ou établissements mentionnés au 8° de l'article L. 231 du code électoral ; qu'en outre, ils ne sont pas créés par le département ou à sa demande mais par la loi dans chaque département ; que, par suite, les services départementaux d'incendie et de secours ne peuvent être regardés comme des établissements publics du département au sens et pour l'application du 8° de l'article L. 231 du code électoral ; que, dès lors, contrairement à ce qu'a jugé le tribunal administratif de Caen, les fonctions exercées par M. G... au sein du service départemental d'incendie et de secours du Calvados ne le rendent pas inéligible aux élections municipales de Courseulles-sur-Mer en application de ces dispositions ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de sa requête, que M. G... est fondé à demander l'annulation du jugement attaqué en tant qu'il a annulé son élection en qualité de conseiller municipal de Courseulles-sur-Mer et le rejet, dans cette même mesure, de la protestation présentée par M. C...et autres devant le tribunal administratif de Caen ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 17 juin 2014 est annulé en tant qu'il annule l'élection de M. G... et proclame M. E...élu à sa place.<br/>
Article 2 : L'élection de M. G... en qualité de conseiller municipal de la commune de Courseulles-sur-Mer est validée.<br/>
Article 3 : La protestation présentée par M. C... et autres devant le tribunal administratif de Caen est rejetée en tant qu'elle tend à l'annulation de l'élection de M. G....<br/>
Article 4 : La présente décision sera notifiée à M. H... G..., à M. J...C...et au ministre de l'intérieur. <br/>
Copie en sera adressée pour information à M. F... E....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
