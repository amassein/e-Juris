<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029559808</ID>
<ANCIEN_ID>JG_L_2014_10_000000380966</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/55/98/CETATEXT000029559808.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 08/10/2014, 380966, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380966</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380966.20141008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 380966, la requête, enregistrée le 5 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A..., élisant domicile ...; M. A...demande au Conseil d'Etat, sur le fondement de l'article R. 532-1 du code de justice administrative, d'ordonner la communication d'une copie certifiée du dossier dont la section de l'intérieur du Conseil d'Etat a été saisie par le ministre de l'intérieur sur le projet de décret portant délimitation des cantons dans le département de Tarn-et-Garonne ;<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 380967, la requête sommaire et le mémoire complémentaire, enregistrés les 5 juin et 13 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentés par M. B...A..., élisant domicile ...; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-273 du 27 février 2014 portant délimitation des cantons dans le département de Tarn-et-Garonne ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code électoral, notamment ses articles L. 191-1, L. 194 et L. 195 ;<br/>
<br/>
              Vu le code général des collectivités territoriales, notamment son article L. 3113-2 ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ; <br/>
<br/>
              Vu le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2013-667 DC du 16 mai 2013 ;<br/>
<br/>
              Vu la décision du 16 juillet 2014 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé une première question prioritaire de constitutionnalité soulevée par M. A...;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes visées ci-dessus se rapportent au même litige ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2.	Considérant qu'en vertu de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la même loi du 17 mai 2013, applicable à la date du décret attaqué : " I.- Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / II. - La qualité de chef-lieu de canton est maintenue aux communes qui la perdent dans le cadre d'une modification des limites territoriales des cantons, prévue au I, jusqu'au prochain renouvellement général des conseils généraux. / III. La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants. / IV. Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              4.	Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de Tarn-et-Garonne, compte tenu de la réduction du nombre de ces cantons de trente à quinze résultant de l'application de l'article L. 191-1 du code électoral ; <br/>
<br/>
              Sur les questions prioritaires de constitutionnalité :<br/>
<br/>
              5.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              En ce qui concerne le I de l'article L. 3113-2 du code général des collectivités territoriales :<br/>
<br/>
              6.	Considérant que M. A...soutient, à l'appui du recours pour excès de pouvoir qu'il a formé contre le décret attaqué, que les dispositions du I de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la loi du 17 mai 2013, méconnaissent le principe de libre administration des collectivités territoriales garanti par la Constitution, faute de prévoir une information ou une consultation des communes et des établissements publics de coopération intercommunale concernés préalablement à l'adoption des décrets portant modification des limites territoriales des cantons ;<br/>
<br/>
              7.	Considérant toutefois qu'il revient au législateur, en vertu de l'article 34 de la Constitution, d'une part, de fixer les règles concernant le régime électoral des assemblées locales ainsi que les conditions d'exercice des mandats électoraux et des fonctions électives des membres des assemblées délibérantes des collectivités territoriales, d'autre part, de déterminer les principes fondamentaux de la libre administration des collectivités territoriales, de leurs compétences et de leurs ressources ; que le législateur a prévu, au I de l'article L. 3113-2, la consultation du conseil général préalablement à l'intervention des décrets procédant à la délimitation des cantons, qui sont des circonscriptions électorales destinées à permettre l'élection des membres de l'assemblée délibérante du département ; que le principe de libre administration des collectivités territoriales, résultant de l'article 72 de la Constitution, ne lui imposait nullement d'instituer en outre une information ou une consultation préalable obligatoire des communes et établissements publics de coopération intercommunale ; que, par suite, la question de constitutionnalité invoquée à l'encontre du I de l'article L. 3113-2 du code général des collectivités territoriales, qui n'est pas nouvelle, ne présente, en tout état de cause, pas un caractère sérieux ; que, dès lors, le moyen tiré de ce que les dispositions de ce I porteraient atteinte aux droits et libertés garantis par la Constitution ne peut qu'être écarté, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à son encontre ;<br/>
<br/>
              En ce qui concerne les articles L. 194 et L. 195 du code électoral :<br/>
<br/>
              8.	Considérant que les articles L. 194 et L. 195 du code électoral, dans leur rédaction issue de la loi du 17 mai 2013, précisent les conditions d'éligibilité et des cas d'inéligibilité au mandat de conseiller départemental qui se substituera au conseiller général à compter du prochain renouvellement général des assemblées départementales ; que si M. A... soutient, à l'appui de son recours pour excès de pouvoir, que ces dispositions porteraient atteinte aux droits et libertés garantis par la Constitution faute de prévoir des règles destinées à prévenir d'éventuels conflits d'intérêts entre les deux personnes qui devront se présenter en binôme à l'élection départementale en application de l'article L. 191 du code électoral tel qu'issu de la loi du 17 mai 2013, les articles contestés sont sans rapport avec la délimitation des cantons à laquelle a procédé le décret attaqué ; que, par suite, les dispositions critiquées ne peuvent être regardées comme étant applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; qu'il s'ensuit que le moyen tiré de ce que les dispositions des articles L. 194 et L. 195 du code électoral portent atteinte aux droits et libertés garantis par la Constitution ne peut qu'être écarté, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée à leur encontre ;<br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              9.	Considérant, en premier lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que les ministres chargés de l'exécution sont ceux qui ont compétence pour signer les mesures que comporte nécessairement l'exécution des actes en cause ; qu'en l'espèce, aucune disposition du décret attaqué, qui procède à la délimitation des cantons du département de Tarn-et-Garonne, n'implique nécessairement l'intervention de mesures réglementaires ou individuelles que le garde des sceaux, ministre de la justice, le ministre chargé de l'agriculture, le ministre chargé de la sécurité sociale ou le ministre chargé de l'urbanisme auraient été compétents pour signer ou contresigner ; que, par suite, le moyen tiré de ce que le décret attaqué n'aurait pas été contresigné par tous les ministres chargés de son exécution doit être écarté ;<br/>
<br/>
              10.	Considérant, en deuxième lieu, qu'aux termes de l'article L. 1211-3 du code général des collectivités territoriales : " Le comité des finances locales contrôle la répartition de la dotation globale de fonctionnement. (...) Le Gouvernement peut le consulter sur tout projet de loi, tout projet d'amendement du Gouvernement ou sur toutes dispositions réglementaires à caractère financier concernant les collectivités locales. Pour les décrets, cette consultation est obligatoire (...) " ; que le décret attaqué, qui ne présente pas de caractère réglementaire et a pour seul objet de déterminer la délimitation des circonscriptions électorales que sont les cantons, pour le département de Tarn-et-Garonne, ne saurait être regardé comme un décret à caractère financier concernant les collectivités territoriales au sens de l'article L. 1211-3 ; qu'il n'est donc pas au nombre de ceux pour lesquels la consultation du comité des finances locales est obligatoire en vertu de ces dispositions ; <br/>
<br/>
              11.	Considérant, en troisième lieu, que si, en vertu de l'article L. 1211-4-2 du code général des collectivités territoriales, dans sa version applicable à la date du décret attaqué, la commission consultative d'évaluation des normes créée au sein du comité des finances locales est " consultée préalablement à leur adoption sur l'impact financier des mesures réglementaires créant ou modifiant des normes à caractère obligatoire concernant les collectivités territoriales, leurs groupements et leurs établissements publics ", l'intervention du décret attaqué, qui ne présente pas de caractère réglementaire et n'a donc ni pour objet ni pour effet de créer des normes à caractère obligatoire, n'avait pas à être précédée de la consultation de cette commission ;<br/>
<br/>
              12.	Considérant, en quatrième lieu, que le décret attaqué a procédé, après l'intervention de la loi du 17 mai 2013, à une nouvelle délimitation des cantons du département de Tarn-et-Garonne compte tenu de l'exigence de réduction du nombre des cantons qui résulte de l'application de l'article L. 191-1 du code électoral ; qu'il a été pris sur le fondement des dispositions de l'article L. 3113-2 du code général des collectivités territoriales, qui déterminent les règles applicables à la délimitation du territoire des cantons et les conditions dans lesquelles il peut être porté à ces règles des exceptions limitées ; qu'au nombre des règles fixées par l'article L. 3113-2 jugées conformes à la Constitution dans les motifs et le dispositif de la décision du Conseil constitutionnel n° 2013-667 DC du 16 mai 2013, figure celle selon laquelle la délimitation des cantons doit être faite sur des bases essentiellement démographiques, c'est-à-dire en prenant en considération non le nombre des électeurs mais le chiffre de la population ; qu'il s'ensuit que le moyen tiré par voie d'exception de l'illégalité de l'article R. 25-1 du code électoral, selon lequel le " chiffre de population auquel il convient de se référer en matière électorale est le dernier chiffre de population municipale authentifié avant l'élection. (...) ", ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              13.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'ordonner les mesures d'instruction sollicitées par le requérant dans sa requête enregistrée sous le n° 380966, que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; <br/>
<br/>
              14.	Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par M. A...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M.A....<br/>
<br/>
Article 2 : Les requêtes de M. A...sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
