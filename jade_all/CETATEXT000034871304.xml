<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034871304</ID>
<ANCIEN_ID>JG_L_2017_06_000000400395</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/87/13/CETATEXT000034871304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 01/06/2017, 400395, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400395</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400395.20170601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a saisi le tribunal administratif de Nice, d'une part, à titre principal, d'une opposition à la contrainte délivrée à son encontre le 5 mars 2013 par Pôle emploi pour le recouvrement de la somme de 2 498,10 euros correspondant à un trop-perçu d'allocation de solidarité spécifique, augmentée des frais de contrainte et de signification, et, à titre subsidiaire, à ce qu'il soit enjoint à Pôle Emploi de lui accorder une remise totale de dette, et, d'autre part, a demandé la condamnation de Pôle Emploi à lui payer la somme de 62 500 euros, assortie des intérêts au taux légal à compter du 28 mars 2013, en réparation du préjudice résultant des fautes commises dans le traitement de son dossier d'allocation d'assurance chômage. Par un jugement n° 1300973 du 30 juin 2015, le tribunal administratif de Nice a condamné Pôle Emploi à payer à Mme B...la somme de 2 000 euros, assortie des intérêts au taux légal à compter du 28 mai 2014 et rejeté le surplus de la demande de l'intéressée.<br/>
<br/>
              Par un arrêt n° 16MA00316 du 2 juin 2016, la cour administrative d'appel de Marseille, saisie d'un appel de Mme B...et d'un appel incident de Pôle emploi, a :<br/>
              - annulé le jugement du tribunal administratif de Nice en tant qu'il condamne Pôle emploi à payer à Mme B...la somme de 2 000 euros (article 1er) ;<br/>
              - rejeté les conclusions indemnitaires de Mme B...comme portées devant un ordre de juridiction incompétent pour en connaître (article 2) ;<br/>
              - renvoyé au Conseil d'Etat les conclusions de Mme B...relatives à l'opposition à contrainte et à la demande d'injonction présentée à titre subsidiaire (article 3).<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par ce pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 6 juin, 26 juillet et 29 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 du jugement du tribunal administratif de Nice, qui rejette le surplus des conclusions de sa demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions à fin d'opposition à contrainte de sa demande et, subsidiairement, à celles à fin d'injonction ;<br/>
<br/>
              3°) de mettre à la charge de Pôle emploi la somme de 3 500 euros à verser à la SCP Marlange, de la Burgade, avocat de MmeB..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de MmeB..., et à la SCP Boullez, avocat de Pôle emploi.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, par contrainte en date du 5 mars 2013, signifiée le 15 mars 2013, le directeur de Pôle Emploi Provence-Alpes-Côte d'Azur a demandé à Mme A...B...le paiement de la somme de 2 498,10 euros au titre d'un montant d'allocation spécifique de solidarité indûment versé, augmentée des frais de contrainte et de signification. Mme B...a, notamment, formé opposition à cette contrainte devant le tribunal administratif de Nice, en lui demandant, à titre principal, d'annuler cette décision et de la décharger du paiement de la somme qui lui est réclamée et, à titre subsidiaire, d'enjoindre à Pôle Emploi de lui accorder une remise gracieuse. Par l'article 3 d'un jugement du 30 juin 2016, contre lequel Mme B...se pourvoit régulièrement en cassation, le tribunal administratif de Nice a rejeté cette demande. <br/>
<br/>
              2. Aux termes de l'article L. 5426-8-2 du code du travail, dans sa rédaction applicable au litige : " Pour le remboursement des allocations, aides, ainsi que de toute autre prestation indûment versées par l'institution prévue à l'article L. 5312-1, pour son propre compte, pour le compte de l'Etat, du fonds de solidarité prévu à l'article L. 5423-24 ou des employeurs mentionnés à l'article L. 5424-1, le directeur général de l'institution prévue à l'article L. 5312-1 ou la personne qu'il désigne en son sein peut, dans les délais et selon les conditions fixés par voie réglementaire, et après mise en demeure, délivrer une contrainte qui, à défaut d'opposition du débiteur devant la juridiction compétente, comporte tous les effets d'un jugement et confère le bénéfice de l'hypothèque judiciaire ". L'article R. 5426-20 du même code, dans sa rédaction alors en vigueur, dispose que : " La contrainte prévue à l'article L. 5426-8-2 est délivrée après que le débiteur a été mis en demeure de rembourser l'allocation, l'aide ou toute autre prestation indue mentionnée à l'article L. 5426-8-1. / Le directeur général de l'institution mentionnée à l'article L. 5312-1 lui adresse, par lettre recommandée avec demande d'avis de réception, une mise en demeure qui comporte le motif, la nature et le montant des sommes demeurant.réclamées, la date du ou des versements indus donnant lieu à recouvrement ainsi que, le cas échéant, le motif ayant conduit à rejeter totalement ou partiellement le recours formé par le débiteur / Si la mise en demeure reste sans effet au terme du délai d'un mois à compter de sa notification, le directeur général de l'institution mentionnée à l'article L. 5312-1 peut décerner la contrainte prévue à l'article L. 5426-8-2 ".<br/>
<br/>
              3. En premier lieu, en indiquant, pour écarter le moyen tiré par Mme B...à l'appui de son opposition à contrainte de ce que l'indu réclamé par Pôle Emploi trouvait exclusivement son origine dans une faute de cette institution, qu'était sans incidence sur le bien-fondé de la créance de Pôle Emploi à raison de l'allocation de solidarité spécifique perçue à tort par Mme B...la circonstance que cette institution soit à l'origine du cumul de la perception par l'intéressée, pour une même période, de l'allocation de solidarité spécifique et de l'allocation d'aide au retour à l'emploi, alors que l'allocation de solidarité spécifique est versée aux travailleurs privés d'emploi qui ont épuisé leur droits à l'allocation d'aide au retour à l'emploi, le tribunal administratif a suffisamment motivé son jugement.<br/>
<br/>
              4. En deuxième lieu, si, contrairement à ce que soutient Pôle Emploi, il entre dans l'office du juge de l'opposition à contrainte d'apprécier tant la régularité que le bien-fondé de la contrainte, le tribunal administratif a pu, en portant sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et sans commettre d'erreur de droit, retenir, au regard des éléments de l'instruction, que la circonstance que la contrainte litigieuse avait été décernée dès le 5 mars 2013, soit avant le terme du délai d'un mois suivant la notification à Mme B..., le 13 février, d'une mise en demeure de payer l'indu, au-delà duquel la contrainte pouvait être décernée, n'avait pas en l'espèce été susceptible d'exercer une influence sur le sens de la décision ni privé Mme B...d'une garantie, dès lors notamment qu'il résultait de l'instruction que cette contrainte n'avait été signifiée à l'intéressée qu'après l'expiration du délai d'un mois prescrit par l'article R. 5426-20 du code du travail et que la mise en demeure était restée sans effet pendant ce délai.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'article 3 du jugement attaqué.<br/>
<br/>
              6. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Marlange, de la Burgade, avocat de MmeB.réclamées, la date du ou des versements indus donnant lieu à recouvrement ainsi que, le cas échéant, le motif ayant conduit à rejeter totalement ou partiellement le recours formé par le débiteur<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à Pôle Emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
