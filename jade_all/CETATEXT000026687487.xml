<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687487</ID>
<ANCIEN_ID>JG_L_2012_11_000000349511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/74/CETATEXT000026687487.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/11/2012, 349511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Edmond Honorat</PRESIDENT>
<AVOCATS>SCP ODENT, POULET ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349511.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 mai et 23 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Dourdan, représentée par son maire, et  pour le maire de Dourdan ès qualité ; la commune et le maire de Dourdan demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE01999 du 3 mars 2011 par lequel la cour administrative d'appel de Versailles a rejeté la requête tendant, d'une part, à l'annulation du jugement n° 1002656 du 30 avril 2010 par lequel le tribunal administratif de Versailles a rejeté la demande tendant à ce qu'il déclare M. Henri A démissionnaire d'office de ses fonctions de membre du conseil municipal et, d'autre part, à ce qu'il déclare M. A démissionnaire d'office de ses fonctions de membre du conseil municipal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de déclarer M. A démissionnaire d'office de ses fonctions de membre du conseil municipal ;<br/>
<br/>
              3°) de mettre à la charge de M. A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune et du maire de Dourdan, et de la SCP Odent, Poulet, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune et du maire de Dourdan, et à la SCP Odent, Poulet, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 8 mars 2010, le maire de Dourdan a désigné M. Henri A, neuvième dans l'ordre du tableau des conseillers municipaux de la commune, comme assesseur du bureau de vote n° 4 en vue des élections régionales des 14 et 21 mars 2010 ; que M. A ne s'est présenté au bureau de vote n° 4 ni le 14, ni le 21 mars 2010 ; que la commune et le maire de Dourdan se pourvoient en cassation contre l'arrêt du 3 mars 2011 par lequel la cour administrative d'appel de Versailles a rejeté la requête tendant à l'annulation du jugement du 30 avril 2010 par lequel le tribunal administratif de Versailles a rejeté la demande du 1er avril 2010 tendant à ce qu'il déclare M. A démissionnaire d'office de ses fonctions de membre du conseil municipal ;<br/>
<br/>
              Sur la recevabilité du pourvoi :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 432-4 du code de justice administrative : " L'Etat est dispensé du ministère d'avocat au Conseil d'Etat soit en demande, soit en défense, soit en intervention. / Les recours et les mémoires, lorsqu'ils ne sont pas présentés par le ministère d'un avocat au Conseil d'Etat, doivent être signés par le ministre intéressé ou par le fonctionnaire ayant reçu délégation à cet effet. " ; qu'aux termes de l'article L. 2121-5 du code général des collectivités territoriales : " Tout membre d'un conseil municipal qui, sans excuse valable, a refusé de remplir une des fonctions qui lui sont dévolues par les lois, est déclaré démissionnaire par le tribunal administratif. [...] " ; qu'aux termes de l'article R. 2121-5 du même code : " Dans les cas prévus à l'article L. 2121-5, la démission d'office des membres des conseils municipaux est prononcée par le tribunal administratif. / Le maire, après refus constaté dans les conditions prévues par l'article L. 2121-5 saisit dans le délai d'un mois, à peine de déchéance, le tribunal administratif. [...] " ;<br/>
<br/>
              3. Considérant que, lorsqu'il saisit le tribunal administratif d'une demande de démission d'office d'un membre du conseil municipal, le maire agit en tant qu'autorité de l'Etat ; que, dès lors, conformément aux dispositions de l'article R. 432-4 du code de justice administrative, le ministre de l'intérieur avait seul qualité pour se pourvoir en cassation contre l'arrêt de la cour administrative d'appel de Versailles du 3 mars 2011 ;  que, par suite, ni le maire ni, en tout état de cause, la commune de Dourdan ne sont recevables à former un tel pourvoi ; que, toutefois, l'irrecevabilité de ce pourvoi a été couverte par l'appropriation des conclusions qu'il comporte par le ministre de l'intérieur dans un mémoire du 1er octobre 2012 ;<br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 44 du code électoral : " Les assesseurs de chaque bureau sont désignés conformément aux dispositions ci-après : / - Chaque candidat ou chaque liste en présence a le droit de désigner un assesseur et un seul pris parmi les électeurs du département ; / - Des assesseurs supplémentaires peuvent être désignés par le maire parmi les conseillers municipaux dans l'ordre du tableau puis, le cas échéant, parmi les électeurs de la commune. / Le jour du scrutin, si, pour une cause quelconque, le nombre des assesseurs se trouve être inférieur à deux, les assesseurs manquants sont pris parmi les électeurs présents sachant lire et écrire le français, selon l'ordre de priorité suivant : l'électeur le plus âgé, puis l'électeur le plus jeune " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que la fonction d'assesseur de bureau de vote qui peut être confiée par le maire à des membres du conseil municipal compte parmi les fonctions qui leur sont dévolues par les lois au sens de l'article L. 2121-5 du code général des collectivités territoriales ; que, dès lors, en jugeant que cette fonction n'était pas inhérente à l'exercice du mandat de membre du conseil municipal et ne pouvait être regardée comme lui étant dévolue par les lois au sens de l'article L. 2121-5 du code général des collectivités territoriales, la cour administrative d'appel de Versailles a commis une erreur de droit ; que, par suite, le ministre de l'intérieur est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le ministre de l'intérieur au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 3 mars 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Le surplus des conclusions du pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur, à la commune et au maire de Dourdan et à M. Henri A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
