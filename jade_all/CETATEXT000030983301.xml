<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983301</ID>
<ANCIEN_ID>JG_L_2015_07_000000361520</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983301.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 31/07/2015, 361520, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361520</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:361520.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Banque Fédérative du Crédit Mutuel a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquelles elle a été assujettie au titre de l'exercice clos en 2004. Par un jugement n°1003663 du 1er juin 2011, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11VE02358 du 19 juin 2012, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Banque Fédérative du Crédit Mutuel contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 31 juillet 2012, 29 octobre 2012, 2 mars 2015 et 27 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la société Banque Fédérative du Crédit Mutuel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel qu'elle avait présenté devant la cour ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Banque Fédérative du Crédit Mutuel ;<br/>
<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis aux juges du fond que la société Banque Fédérative du Crédit Mutuel, qui avait opté pour le régime fiscal des sociétés mères défini à l'article 216 du code général des impôts, a retranché de son bénéfice imposable au titre de l'exercice 2004 les dividendes reçus de ses filiales établies en France sous déduction d'une quote-part de frais et charges égale à 5 % des dividendes perçus. A l'occasion d'une vérification de sa comptabilité, l'administration fiscale a estimé que l'assiette de la quote-part de frais et charges devait inclure les avoirs fiscaux attachés aux dividendes reçus et a rectifié, en conséquence, le résultat imposable de la société. Par un jugement du 16 juin 2011, le tribunal administratif de Montreuil a rejeté la demande de la société tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt mises à sa charge du fait de cette rectification. Ce jugement a été confirmé par un arrêt du 19 juin 2012 de la cour administrative d'appel de Versailles contre lequel la société se pourvoit en cassation.<br/>
<br/>
              2. En premier lieu, aux termes du I de l'article 216 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Les produits nets des participations, ouvrant droit à l'application du régime des sociétés mères et visées à l'article 145, touchés au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice net total de celle-ci, défalcation faite d'une quote-part de frais et charges. / La quote-part de frais et charges ... est fixée uniformément à 5 % du produit total des participations, crédit d'impôt compris. Cette quote-part ne peut toutefois excéder, pour chaque période d'imposition, le montant total des frais et charges de toute nature exposés par la société participante au cours de la même période ". Il résulte des termes mêmes de ces dispositions que les crédits d'impôt, quelle que soit leur nature et nonobstant la circonstance qu'ils ne constitueraient pas des " produits de participations ", doivent être inclus dans l'assiette de la quote-part de frais et charges. <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 158 bis du code général des impôts, alors en vigueur : " I. Les personnes qui perçoivent des dividendes distribués par des sociétés françaises disposent à ce titre d'un revenu constitué : / a) par les sommes qu'elles reçoivent de la société ; / b) par un avoir fiscal représenté par un crédit ouvert sur le Trésor. / Ce crédit d'impôt est égal à la moitié des sommes effectivement versées par la société. / Il ne peut être utilisé que dans la mesure où le revenu est compris dans la base de l'impôt sur le revenu dû par le bénéficiaire. / Il est reçu en paiement de cet impôt. / Il est restitué aux personnes physiques dans la mesure où son montant excède celui de l'impôt dont elles sont redevables. / II. - Par exception aux dispositions prévues au I, ce crédit d'impôt est égal à 40 % des sommes effectivement versées par la société lorsque la personne susceptible d'utiliser ce crédit n'est pas une personne physique. Cette disposition ne s'applique pas lorsque le crédit d'impôt est susceptible d'être utilisé dans les conditions prévues au 2 de l'article 146 (...) ". Il résulte de ces dispositions que l'avoir fiscal avait, pour son bénéficiaire, tout à la fois le caractère d'un revenu et d'un crédit d'impôt. Les dispositions du 1 de l'article 209 bis du code général des impôts, alors en vigueur, aux termes desquelles " Les dispositions des articles 158 bis et 158 ter sont applicables aux personnes morales ayant leur siège social en France, dans la mesure où le revenu distribué est compris dans la base de l'impôt sur les sociétés dû par le bénéficiaire ", qui restreignent les possibilités d'imputer l'avoir fiscal attribué aux sociétés mères, n'ont pour objet de faire obstacle ni à l'attribution de cet avoir fiscal, ni à ce qu'il soit qualifié de crédit d'impôt au sens et pour l'application des dispositions précitées de l'article 216 du code général des impôts. Il en résulte que la cour n'a pas commis d'erreur de droit en jugeant que les crédits d'impôts mentionnés au I de l'article 216 du code général des impôts incluaient l'avoir fiscal alors prévu au b) du I de l'article 158 bis du même code.<br/>
<br/>
              4. En précisant que les dispositions du 1 de l'article 209 bis concernaient les conditions d'utilisation et non d'attribution de l'avoir fiscal et étaient, par suite, sans incidence sur sa qualification de crédit d'impôt au sens et pour l'application du deuxième alinéa du I de l'article 216 du code général des impôts la cour, qui n'était pas tenue de répondre à chacun des arguments invoqués par la société requérante à l'appui de son moyen, doit être regardée comme ayant écarté comme inopérante l'argumentation développée dans le mémoire en réplique du 17 janvier 2012, tirée de ce que les avoirs fiscaux de l'année 2004 devaient être exclus de la quote-part de frais et charges de cet exercice dès lors qu'ils ne pouvaient plus être utilisés pour le paiement du précompte. Le moyen tiré de l'insuffisante motivation de l'arrêt attaqué doit, dès lors, être écarté.<br/>
<br/>
              5. En dernier lieu, la société requérante soutient que la cour, en interprétant l'article 216 du code général des impôts comme incluant l'avoir fiscal dans l'assiette de la quote-part de frais et charges, a méconnu l'article 4 de la directive 90/345/CEE du 23 juillet 1990 concernant le régime fiscal commun applicable aux sociétés mères et filiales d'Etats membres différents, aux termes duquel le montant forfaitaire des frais de gestion des participations " ne peut excéder 5 % des bénéfices distribués par la filiale ". Ce moyen doit toutefois être écarté dès lors, d'une part, que le litige porte sur l'inclusion dans l'assiette de la quote-part de frais et charges de l'avoir fiscal attaché à des dividendes reçus par la société requérante de ses filiales établies en France et que, d'autre part, selon les dispositions de l'article 158 bis du code général des impôts alors en vigueur, l'avoir fiscal n'était attaché qu'aux dividendes distribués par des sociétés françaises, de telle sorte que le législateur ne peut être regardé comme ayant entendu traiter de la même façon les revenus distribués par des sociétés françaises et les revenus distribués par des sociétés d'un autre Etat membre.<br/>
<br/>
              6. Il résulte de ce qui précède que la société Banque Fédérative du Crédit Mutuel n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ne peuvent, dès lors, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Banque Fédérative du Crédit Mutuel est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Banque Fédérative du Crédit Mutuel et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
