<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031309632</ID>
<ANCIEN_ID>JG_L_2015_10_000000387619</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/30/96/CETATEXT000031309632.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 09/10/2015, 387619, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387619</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:387619.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...-G... et autres ont demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la décision implicite du préfet des Alpes-Maritimes portant refus d'abrogation de l'arrêté du 12 avril 2007 par lequel le directeur de cabinet du préfet des Alpes-Maritimes a approuvé par délégation le plan de prévention des risques naturels prévisibles d'incendies de forêts sur le territoire de la commune de Tourrettes-sur-Loup, et d'enjoindre au préfet de procéder à cette abrogation. Par un jugement n° 1002864 du 6 août 2012, le tribunal administratif de Nice a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12MA03920 du 2 décembre 2014, la cour administrative d'appel de Marseille a, sur la requête de Mme C...-G... et autres, d'une part, annulé ce jugement du tribunal administratif de Nice et la décision implicite du 12 avril 2007, en tant que le plan de prévention des risques naturels prévisibles d'incendies de forêts sur le territoire de la commune de Tourrettes-sur-Loup procède au classement des parcelles appartenant à Mme C... -G... et autres en zone rouge et en zone bleue, d'autre part, enjoint au préfet des Alpes-Maritimes d'abroger, dans cette mesure, dans le délai d'un mois à compter de la notification de son arrêt, l'arrêté du 12 avril 2007.<br/>
<br/>
              Par un pourvoi enregistré le 3 février 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2004-374 du 29 avril 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de Mme C...-G..., de M. F...C..., de M. D... C...et de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 12 avril 2007, le directeur de cabinet du préfet des Alpes-Maritimes a approuvé par délégation le plan de prévention des risques naturels prévisibles d'incendies de forêts sur le territoire de la commune de Tourrettes-sur-Loup ; que la cour administrative d'appel de Marseille, après avoir cité l'article 43 du décret du 29 avril 2004 relatif aux pouvoirs des préfets, à l'organisation et à l'action des services de l'Etat dans les régions et départements, disposant que " Le préfet de département peut donner délégation de signature (...) 4° Pour les matières relevant de ses attributions, au directeur de cabinet (...) ", a relevé que, par l'article 1er de l'arrêté du 2 mars 2007, régulièrement publié au recueil des actes administratifs du département du 5 mars 2007, le préfet des Alpes-Maritimes avait notamment autorisé le directeur de son cabinet, à signer " les arrêtés, décisions, correspondances relatifs aux plans de prévention des risques majeurs " ; qu'elle a toutefois retenu que l'administration ne produisait aucune décision fixant les attributions du directeur de cabinet, signataire de l'arrêté litigieux portant approbation du plan de prévention des risques naturels prévisibles d'incendies de forêts sur le territoire de la commune de Tourrettes-sur-Loup en méconnaissance des dispositions précitées de l'article 43 du décret du 29 avril 2004 ; <br/>
<br/>
              2. Considérant, d'une part, que ni l'article 43 du décret du 29 avril 2004, ni aucune autre disposition, n'imposent qu'une décision détermine les attributions du directeur de cabinet préalablement à ce que le préfet du département lui donne délégation de signature ; que, dès lors, la cour a commis une erreur de droit en se fondant sur le motif rappelé ci-dessus pour censurer le jugement et l'arrêté litigieux ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier. " ; qu'en application de l'article L. 562-1 du code de l'environnement dans sa version applicable au litige, les plans de prévention des risques naturels prévisibles d'incendies de forêt font partie des plans de prévention des risques naturels prévisibles, lesquels ont pour objet de délimiter des zones exposées à des risques naturels à l'intérieur desquelles s'appliquent des contraintes d'urbanisme importantes ; que ces contraintes s'imposent directement aux personnes publiques ainsi qu'aux personnes privées et peuvent notamment fonder l'octroi ou le refus d'une autorisation d'occupation ou d'utilisation du sol ; que, par suite, les plans de prévention des risques naturels prévisibles constituent des documents d'urbanisme auxquels s'applique l'article L. 600-4-1 du code de l'urbanisme cité ci-dessus en cas de litige, nonobstant la circonstance que ces plans sont établis en application de dispositions législatives qui n'ont pas été incorporées dans le code de l'urbanisme ;<br/>
<br/>
              4. Considérant que la cour a retenu que le jugement du tribunal administratif de Nice devait être annulé en raison de la méconnaissance des dispositions du 4° de l'article 43 du décret du 29 avril 2004 " sans qu'il soit besoin d'examiner les autres moyens de la requête " ; qu'en statuant ainsi, la cour a méconnu les obligations résultant de l'article L. 600-4-1 du code de l'urbanisme ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 2 décembre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par Mme C...-G... et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie, à Mme B...C...-G..., M. F...C..., M. D...C..., Mme E...C...épouseA....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
