<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043335961</ID>
<ANCIEN_ID>JG_L_2021_03_000000450912</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/33/59/CETATEXT000043335961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/03/2021, 450912, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450912</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450912.20210329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2021-296 du 19 mars 2021 en tant qu'il interdit dans plusieurs départements, et notamment dans celui des Alpes-Maritimes, tout déplacement au-delà d'un rayon de dix kilomètres sauf à justifier du motif de son déplacement par la fourniture d'une attestation accompagnée le cas échéant de documents justificatifs.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la mesure d'interdiction de déplacement est entrée en vigueur le 20 mars 2021 ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté individuelle, et à la liberté d'aller et venir ; <br/>
              - les dispositions attaquées sont entachées de détournement de pouvoir dès lors que l'article L. 3131-15 du code de la santé publique ne poursuit pas un objectif de protection de la santé publique mais a pour finalité de masquer les errances de la gestion gouvernementale qui perdurent depuis un an ;<br/>
              - elles sont disproportionnées en ce qu'elles traitent de la même manière des zones géographiques pourtant situées dans des situations différentes, notamment le département des Alpes-Maritimes dont la situation sanitaire n'est pas comparable à celle des départements des régions Ile-de-France et Hauts-de-France, mais également, au sein-même des Alpes-Maritimes, les zones rurales et les zones urbaines, entre lesquelles il existe de fortes disparités en matière sanitaire ;<br/>
              - elles ne sont pas en adéquation avec les circonstances de temps et de lieu dès lors que l'interdiction des déplacements sans justificatif au-delà de dix kilomètres du domicile et de 30 kilomètres en lisière du département ne repose sur aucun critère objectif.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret 2021-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-296 du 19 mars 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Par décret du 19 mars 2021, en application de la loi du 15 février 2021, le Premier ministre a modifié les dispositions du décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire pour étendre à seize départements, dont les Alpes maritimes, diverses mesures de restriction des déplacements. Mme A..., domiciliée dans les Alpes maritimes, demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre les dispositions de ce décret en tant qu'elles interdisent la circulation, sous réserve des exceptions qu'elles prévoient, des personnes domiciliées dans les départements concernés entre six heures et dix-neuf heures.<br/>
<br/>
              3. Au soutien de sa demande, Mme A... estime en premier lieu que le décret qu'elle critique est entaché de détournement de pouvoir, l'article L 3131-15 du code de la santé publique ne permettant d'édicter les restrictions qu'il prévoit qu'aux fins de garantir la santé publique. Toutefois, en énonçant des considérations générales sur la circonstance que le décret aurait été pris dans le but de remédier aux carences de la politique publique en matière de gestion des hôpitaux, Mme A... n'établit nullement que les dispositions critiquées, qui visent à réduire la pression sur les capacités hospitalières afin de mieux préserver la santé publique, conformément aux prévisions de l'article L 3131-15, seraient affectées d'une telle illégalité.<br/>
<br/>
              4. Elle soutient ensuite que les mesures critiquées seraient disproportionnées, au regard de la situation d'autres départements, ou excessives, au regard de la situation des différentes communes de son département de résidence et de l'évolution de cette situation au cours des semaines précédentes. Ces considérations sont dénuées de toute précision qui permettraient d'en apprécier la portée, et ne sauraient suffire à remettre en cause des restrictions dont la légalité ne dépend pas d'une analyse des motifs qui les sont inspirées commune par commune.<br/>
<br/>
              5. Mme A... soutient enfin que le décret serait illégal faute de comporter une limite dans le temps, mais il résulte des dispositions de la loi du 15 février 2021 et de celles du code de la santé publique que cette dernière met en oeuvre, que le décret a nécessairement pour terme le plus éloigné celui de l'état d'urgence sanitaire, fixé, à la date de la présente ordonnance, au 1er juin 2021. Par ailleurs, les limites de déplacement de 10 et 30 kilomètres que le décret fixe, qu'elle juge arbitraires, n'apparaissent pas réaliser une conciliation déséquilibrée entre les exigences d'efficacité sanitaire et la satisfaction des besoins essentiels de la vie courante des habitants concernés ou de ceux des départements limitrophes.<br/>
<br/>
              6. Il résulte de ce qui précède que les moyens articulés par Mme A... ne sont manifestement pas de nature à faire regarder les dispositions qu'elle conteste comme portant une atteinte illégale grave et immédiate aux droits et libertés et par suite à fonder l'exercice par le juge des référés des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative. La requête de Mme A... ne peut dès lors qu'être rejetée selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
