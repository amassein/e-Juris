<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311317</ID>
<ANCIEN_ID>JG_L_2014_07_000000361821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 30/07/2014, 361821</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP ROGER, SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361821.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 10 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour M.B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC00803 du 5 avril 2012 de la cour administrative d'appel de Nancy en tant qu'il annule l'article 2 du jugement n° 0801446 du 17 mars 2011 du tribunal administratif de Châlons-en-Champagne condamnant l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à l'indemniser des préjudices qu'il a subis du fait d'une infection nosocomiale contractée au centre hospitalier universitaire de Reims et le condamne à rembourser l'indemnité reçue de l'ONIAM ; <br/>
<br/>
              2°) de mettre à la charge de l'ONIAM ou du centre hospitalier universitaire de Reims la somme de 3 000 euros à verser à la SCP Boulloche, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 juin 2014, présenté pour l'ONIAM ; <br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M.B..., à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, et à Me Le Prado, avocat du centre hospitalier universitaire de Reims ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., atteint d'une forte cataracte de l'oeil droit, a été opéré le 26 septembre 2003 au centre hospitalier universitaire de Reims ; qu'à cette occasion, il a contracté une infection nosocomiale qui a imposé de nouvelles interventions réalisées les 30 septembre, 10 octobre et 31 octobre 2003 ; que ces interventions n'ont pas permis d'empêcher la perte de son oeil ; que, par un jugement du 17 mars 2011, le tribunal administratif de Châlons-en-Champagne, estimant que l'infection avait causé une atteinte à l'intégrité physique d'un taux supérieur à celui au-dessus duquel, en vertu des dispositions de l'article L. 1142-1-1 du code de la santé publique, la réparation est assurée par l'ONIAM au titre de la solidarité nationale, a condamné cet établissement public à verser à M. B...une indemnité de 39 200 euros et a rejeté les conclusions de l'intéressé dirigées contre le centre hospitalier universitaire de Reims ; que, par un arrêt du 5 avril 2012, la cour administrative d'appel de Nancy, statuant sur l'appel de l'ONIAM et sur l'appel incident de M. B..., a fixé à 6 % le taux d'incapacité imputable à l'infection et a, en conséquence, mis l'ONIAM hors de cause et condamné le centre hospitalier universitaire de Reims à verser à M. B...une indemnité de 11 100 euros au titre du I de l'article L. 1142-1 du code de la santé publique ; que M. B...se pourvoit en cassation contre cet arrêt en tant qu'il annule la condamnation prononcée par les premiers juges contre l'ONIAM ; que, par la voie d'un pourvoi incident, le centre hospitalier universitaire de Reims demande l'annulation de cet arrêt ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1142-1 du code de la santé publique : " I. - Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute. / Les établissements, services et organismes susmentionnés sont responsables des dommages résultant d'infections nosocomiales, sauf s'ils rapportent la preuve d'une cause étrangère " ; qu'aux termes de l'article L. 1142-1-1 du même code : " Sans préjudice des dispositions du septième alinéa de l'article L. 1142-17, ouvrent droit à réparation au titre de la solidarité nationale : / 1° Les dommages résultant d'infections nosocomiales dans les établissements, services ou organismes mentionnés au premier alinéa du I de l'article L. 1142-1 correspondant à un taux d'atteinte permanente à l'intégrité physique ou psychique supérieur à 25 % déterminé par référence au barème mentionné au II du même article, ainsi que les décès provoqués par ces infections nosocomiales... " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'opération de la cataracte subie par M. B...était une intervention communément pratiquée, qu'elle ne présentait pas de risque particulier, qu'elle s'est déroulée sans incident et qu'elle devait ainsi normalement permettre au patient de recouvrer une grande partie des capacités fonctionnelles de son oeil droit ; que la survenue d'une infection nosocomiale peu après l'intervention a toutefois entraîné la perte définitive de l'usage de cet oeil ; que, pour fixer à 11 100 euros l'indemnité due à l'intéressé et pour décider qu'elle incombait au centre hospitalier universitaire de Reims et non à l'ONIAM, la cour a tenu compte d'un taux d'atteinte à l'intégrité physique de 6 % correspondant à la différence entre sa capacité visuelle avant l'intervention et sa capacité après consolidation des conséquences de l'infection ; qu'en se prononçant ainsi, alors que, eu égard aux circonstances rappelées ci-dessus, il lui appartenait d'évaluer l'atteinte à l'intégrité physique résultant de l'infection nosocomiale en se référant à la capacité visuelle dont l'intervention aurait permis la récupération en l'absence de cette infection, la cour a commis une erreur de droit ; que M. B...est, par suite, fondé à demander que l'arrêt attaqué soit annulé en tant qu'il décharge l'ONIAM de la condamnation prononcée contre lui par les premiers juges ; que le centre hospitalier universitaire de Reims est fondé, pour le même motif, à demander l'annulation totale de l'arrêt ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède qu'il y a lieu d'annuler intégralement l'arrêt du 5 avril 2012 de la cour administrative d'appel de Nancy ;<br/>
<br/>
              5. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Boulloche, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'ONIAM la somme de 3 000 euros, à verser à la SCP Boulloche ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à ce titre à la charge de M. B...et du centre hospitalier universitaire de Reims, qui ne sont pas, dans la présente instance, les parties perdantes, les sommes demandées par l'ONIAM ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 5 avril 2012 de la cour administrative d'appel de Nancy est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : L'ONIAM versera à la SCP Boulloche, avocat de M.B..., la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat au titre de l'aide juridictionnelle.<br/>
<br/>
Article 4 : Les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au centre hospitalier universitaire de Reims et à la caisse primaire d'assurance maladie de Laon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - RÉGIME D'INDEMNISATION AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-1-1 DU CSP) - INFECTION NOSOCOMIALE - CALCUL DE L'INDEMNITÉ - DÉTERMINATION DU TAUX D'ATTEINTE À L'INTÉGRITÉ PHYSIQUE DU PATIENT - CAS D'UNE INFECTION CONTRACTÉE À L'OCCASION D'UNE OPÉRATION COMMUNÉMENT PRATIQUÉE, SANS RISQUE PARTICULIER ET S'ÉTANT DÉROULÉE SANS INCIDENT - MÉTHODE - CALCUL DE LA DIFFÉRENCE ENTRE LA CAPACITÉ DU PATIENT AVANT L'INTERVENTION ET SA CAPACITÉ APRÈS CONSOLIDATION DES CONSÉQUENCES DE L'INFECTION - ABSENCE - PRISE EN COMPTE DE LA CAPACITÉ DONT L'INTERVENTION AURAIT PERMIS LA RÉCUPÉRATION EN L'ABSENCE DE L'INFECTION - EXISTENCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-005-02 Dans le cas d'une infection nosocomiale contractée à l'occasion d'une opération communément pratiquée, ne présentant pas de risque particulier, et s'étant déroulée sans incident, devant donc normalement permettre au patient de recouvrer une grande partie de ses capacités fonctionnelles, le taux d'atteinte à l'intégrité du patient doit être calculé non pas par la différence entre sa capacité avant l'intervention et sa capacité après consolidation des conséquences de l'infection, mais en se référant à la capacité dont l'intervention aurait permis la récupération en l'absence de cette infection.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
