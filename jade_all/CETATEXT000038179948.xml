<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038179948</ID>
<ANCIEN_ID>JG_L_2019_02_000000417554</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/17/99/CETATEXT000038179948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/02/2019, 417554, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417554</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417554.20190227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler la décision implicite du 18 septembre 2014 du recteur de l'académie de Versailles rejetant sa demande d'admission à la retraite anticipée comme père de trois enfants ou à l'âge de 65 ans avec le bénéfice de bonifications pour enfants. <br/>
<br/>
              Par un jugement n° 1516976 du 28 novembre 2017, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 janvier, 10 avril et 26 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre à l'Etat de réexaminer son titre de pension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le décret n° 2010-1741 du 30 décembre 2010 ;<br/>
              - le décret n° 2016-810 du 16 juin 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.A..., ancien fonctionnaire de l'éducation nationale et père de trois enfants, a demandé par courrier du 18 septembre 2014 à bénéficier soit de la jouissance immédiate de pension comme père de trois enfants, sur le fondement des dispositions des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite, soit de la bonification pour enfants prévue à l'article L. 12 du même code et a contesté la décision implicite de rejet née du silence gardé par l'administration sur sa demande. Par un jugement du 28 novembre 2017 contre lequel il se pourvoit en cassation, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Sur le bénéfice de la liquidation anticipée de pension :<br/>
<br/>
              2. L'article 44 de la loi du 9 novembre 2010 portant réforme des retraites a modifié l'article L. 24 du code des pensions civiles et militaires de retraite en supprimant la possibilité ouverte auparavant aux fonctionnaires parents de trois enfants ayant accompli 15 années de services civils effectifs de partir à la retraite de façon anticipée avec jouissance immédiate de leur pension, tout en maintenant cette possibilité pour les parents d'un enfant atteint d'une invalidité égale ou supérieure à 80 % ainsi que, à titre transitoire, pour les parents de trois enfants remplissant certaines conditions. Ainsi, aux termes de l'article 44 de cette loi : " Par dérogation à l'article L. 24 du code des pensions civiles et militaires de retraite, le fonctionnaire civil et le militaire ayant accompli quinze années de services civils ou militaires effectifs avant le 1er janvier 2012 et parent à cette date de trois enfants vivants, ou décédés par faits de guerre, conserve la possibilité de liquider sa pension par anticipation à condition d'avoir, pour chaque enfant, interrompu ou réduit son activité dans des conditions fixées par un décret en Conseil d'Etat ".  Aux termes du I de l'article R. 37 du code des pensions civiles et militaires de retraite, tel que modifié par le décret du 30 décembre 2010 portant application aux fonctionnaires, aux militaires et aux ouvriers des établissements industriels de l'Etat des articles 44 et 52 de la loi du 9 novembre 2010 précitée, applicable au litige : " (...) Cette interruption ou réduction d'activité doit avoir eu lieu pendant la période comprise entre le premier jour de la quatrième semaine précédant la naissance ou l'adoption et le dernier jour du trente-sixième mois suivant la naissance ou l'adoption ". En vertu du dernier alinéa du I du même article, pour certains enfants énumérés au II de l'article L. 18 du même code, dont ceux du conjoint issus d'un mariage précédent, que l'intéressé a élevés dans les conditions prévues au III de cet article, " l'interruption ou la réduction d'activité doit intervenir soit avant leur seizième anniversaire, soit avant l'âge où ils ont cessé d'être à charge au sens des articles L. 512-3 et R. 512-2 à R. 512-3 du code de la sécurité sociale (...) ". Enfin, si l'article R. 37 du code des pensions civiles et militaires de retraite dans sa rédaction issue du décret du 16 juin 2016 prévoit désormais que l'interruption ou la réduction d'activité ouvrant droit à liquidation anticipée doit intervenir avant l'âge où l'enfant a cessé d'être à charge au sens des articles L. 512-3 et R. 512-2 du code de la sécurité sociale, ces nouvelles dispositions ne sont en tout état de cause pas applicables au présent litige, les droits à pension s'appréciant en fonction de la législation en vigueur à la date d'ouverture des droits à pension.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. A...a pris un congé parental d'une durée d'un an après la naissance de son troisième enfant, alors que ses deux premiers enfants étaient âgés de 5 et de 9 ans. Par suite, il ne remplissait pas la condition d'interruption ou de réduction d'activité pour chacun de ses enfants posée par l'article 44 de la loi. Il ne peut à cet égard utilement faire valoir que lorsqu'un fonctionnaire prend un congé pour accueillir les enfants de son conjoint issus d'un précédent mariage ou en cas de naissances multiples, il peut prétendre au bénéfice de ces dispositions du code des pensions civiles et militaires de retraite sans qu'y fasse obstacle la circonstance que le congé ait été pris de manière globale pour les enfants. Par suite, le tribunal administratif de Paris, qui a suffisamment motivé son jugement sur ce point, n'a pas commis d'erreur de droit en lui faisant application de l'exigence, posée par loi, d'interruption ou de réduction d'activité pour chaque enfant.<br/>
<br/>
              4. En second lieu, il résulte des dispositions citées au point 2 que le bénéfice d'un départ anticipé à la retraite avec jouissance immédiate de la pension est conditionné pour les parents de trois enfants à ce que cette interruption ou réduction soit intervenue avant l'âge de trois ans de l'enfant. Si, par dérogation, cette condition d'âge est de seize ans pour les enfants du conjoint issus d'un mariage précédent accueillis au foyer, cette différence vise à tenir compte de la situation spécifique des bénéficiaires de pension qui ont accueilli les enfants de leur conjoint à un âge dépassant les trois ans. La différence de traitement qui en résulte entre familles recomposées ou non est en rapport direct avec l'objet de la norme qui l'établit et n'est pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. <br/>
<br/>
              5. Si M. A...fait également valoir que le Conseil d'Etat a jugé illégales les dispositions du deuxième alinéa du I de l'article R. 37 en ce qu'elles excluent du bénéfice du départ anticipé à la retraite avec jouissance immédiate les parents d'enfants lourdement handicapés ayant interrompu ou réduit leur activité après que leur enfant a atteint l'âge de trois ans et alors qu'il est encore à leur charge, les parents d'enfants handicapés sont, pour l'application de ces dispositions, dans une situation différente de celle des parents d'autres enfants et la différence de traitement qui en résulte est également en rapport direct avec l'objet de la norme qui l'établit et n'est pas non plus manifestement disproportionnée au regard des motifs susceptibles de la justifier.<br/>
<br/>
              6. Par suite, en n'écartant pas les dispositions du I de l'article R. 37 du code des pensions civiles et militaires de retraite dans leur rédaction applicable au litige, en ce qu'elles imposent que l'interruption ou la réduction d'activité soit intervenue avant l'âge de trois ans de l'enfant, le tribunal administratif de Paris, qui a suffisamment motivé son jugement sur ce point également, n'a pas commis d'erreur de droit. <br/>
<br/>
              Sur le bénéfice d'une bonification pour enfants :<br/>
<br/>
              7. Aux termes de l'article L. 12 du code des pensions civiles et militaires de retraite: " Aux services effectifs s'ajoutent, dans les conditions déterminées par un décret en Conseil d'Etat, les bonifications ci-après : / (...) b) Pour chacun de leurs enfants légitimes et de leurs enfants naturels nés antérieurement au 1er janvier 2004, pour chacun de leurs enfants dont l'adoption est antérieure au 1er janvier 2004 et, sous réserve qu'ils aient été élevés pendant neuf ans au moins avant leur vingt et unième anniversaire, pour chacun des autres enfants énumérés au II de l'article L. 18 dont la prise en charge a débuté antérieurement au 1er janvier 2004, les fonctionnaires et militaires bénéficient d'une bonification fixée à un an, qui s'ajoute aux services effectifs, à condition qu'ils aient interrompu ou réduit leur activité dans des conditions fixées par décret en Conseil d'Etat (...) ". Aux termes de l'article R. 13 du même code : " Sont prises en compte pour le bénéfice des dispositions du b de l'article L. 12 les périodes ayant donné lieu à une interruption ou à une réduction de l'activité dans les conditions suivantes : " 1° L'interruption d'activité doit être d'une durée continue au moins égale à deux mois et être intervenue dans le cadre : a) Du congé maternité (...) / b) Du congé d'adoption (...) / c) Du congé parental tel que prévu... à l'article 54 de la loi du 11 janvier 1984 (...) ". <br/>
<br/>
              8. Il résulte des dispositions citées au point 7 que pour l'application de l'article L. 12 du code des pensions civiles et militaires de retraite, les bonifications d'un an par enfant sont conditionnées à une interruption d'activité d'une durée continue au moins égale à deux mois pour chaque enfant. Ainsi qu'il a été dit ci-dessus, M. A...n'a interrompu son activité qu'à l'occasion de la naissance de son troisième enfant. S'il fait valoir qu'en cas de naissances multiples, le droit à bonification est ouvert pour chacun des enfants alors même que le congé est pris de manière globale, les parents d'enfants nés successivement sont, pour l'application de ces dispositions, dans une situation différente de celle des parents ayant eu des naissances multiples. <br/>
<br/>
              9. Par ailleurs l'article R. 13 du code des pensions civiles et militaires de retraite indique que cette interruption d'activité doit être intervenue notamment dans le cadre d'un congé parental. L'article 54 de la loi du 11 janvier 1984 prévoit que le congé parental est accordé pour un enfant de moins de trois ans. Ainsi qu'il a été dit ci-dessus, les deux premiers enfants de M. A...étaient âgés de 5 et de 9 ans lorsqu'il a pris un congé parental. M. A...n'était par suite pas éligible au congé parental pour ces enfants.<br/>
<br/>
              10. Il résulte de ce qui précède que le tribunal administratif de Paris n'a pas commis d'erreur de droit en refusant de lui accorder le bénéfice d'une bonification pour ses deux premiers enfants.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
