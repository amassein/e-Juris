<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035260383</ID>
<ANCIEN_ID>JG_L_2017_07_000000408509</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/26/03/CETATEXT000035260383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 21/07/2017, 408509, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408509</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:408509.20170721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés les 30 mai et 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le Conseil supérieur de l'Ordre des géomètres-experts demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tenant à l'annulation du décret n° 2017-252 du 27 février 2017 relatif à l'établissement du projet architectural, paysager et environnemental d'un lotissement, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 441-4 du code de l'urbanisme, dans sa rédaction issue de l'article 81 de la loi n° 2016-925 du 7 juillet 2016. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'urbanisme, notamment son article L. 441-4 ; <br/>
              - la loi n° 77-2 du 3 janvier 1977 ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat du Conseil supérieur de l'Ordre des géomètres-experts.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article  L. 441-4 du code de l'urbanisme, dans sa rédaction issue de l'article 81 de la loi du 7 juillet 2016 relative à la liberté de création, à l'architecture et au patrimoine : " La demande de permis d'aménager concernant un lotissement ne peut être instruite que si la personne qui désire entreprendre des travaux soumis à une autorisation a fait appel aux compétences nécessaires en matière d'architecture, d'urbanisme et de paysage pour établir le projet architectural, paysager et environnemental dont, pour les lotissements de surface de terrain à aménager supérieure à un seuil fixé par décret en Conseil d'Etat, celles d'un architecte au sens de l'article 9 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture ", soit une personne inscrite à un tableau régional d'architectes. Il résulte de l'article R. 442-5 du code de l'urbanisme que le projet architectural, paysager et environnemental qui doit être joint à la demande de permis d'aménager un lotissement comprend notamment le programme et les plans des travaux d'aménagement indiquant les caractéristiques des ouvrages à réaliser, le tracé des voies, l'emplacement des réseaux et les modalités de raccordement aux bâtiments qui seront édifiés par les acquéreurs de lots, ainsi qu'un document graphique faisant apparaître une ou plusieurs hypothèses d'implantation des bâtiments. <br/>
<br/>
              3. Le Conseil supérieur de l'Ordre des géomètres-experts soutient qu'en rendant obligatoire le recours à un architecte pour l'élaboration du projet architectural, paysager et environnemental qui doit être joint à la demande de permis d'aménager un lotissement dont la superficie excède un certain seuil, les dispositions de l'article  L. 441-4 du code de l'urbanisme, dans leur rédaction issue de l'article 81 de la loi du 7 juillet 2016, portent atteinte à la liberté d'entreprendre et au principe d'égalité devant la loi, respectivement garantis par les articles 4 et 6 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              4. En premier lieu, il est loisible au législateur d'apporter à la liberté d'entreprendre, qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi. <br/>
<br/>
              5. Le permis d'aménager, requis pour tout lotissement qui prévoit la création ou l'aménagement de voies, d'espaces ou d'équipements communs à plusieurs lots destinés à être bâtis et propres au lotissement, ou bien qui est situé dans un secteur sauvegardé, un site classé ou un site en instance de classement, a notamment pour objet d'autoriser les travaux d'aménagement destinés à rendre constructibles les lots issus de l'opération, au vu du programme et des plans de ces travaux indiquant, en particulier, les caractéristiques des ouvrages à réaliser, le tracé des voies et l'emplacement des réseaux, ainsi que d'un document graphique faisant apparaître une ou plusieurs hypothèses d'implantation des bâtiments. Il résulte des dispositions de l'article L. 441-4 du code de l'urbanisme, éclairées notamment par les travaux préparatoires de la loi du 7 juillet 2016 relative à la liberté de la création, à l'architecture et au patrimoine, que le législateur a entendu imposer le recours à un architecte, dès le permis d'aménager un lotissement, lorsque la superficie du terrain à aménager excède un certain seuil, dans l'intérêt de la qualité des constructions futures et de leur insertion dans les paysages naturels ou urbains. Ce faisant, le législateur, qui n'a, en tout état de cause, pas exclu le concours d'autres professionnels de l'aménagement, de l'urbanisme ou des paysages, parmi lesquels les géomètres-experts, à la constitution du dossier de demande de permis d'aménager, et qui a prévu une dérogation en faveur des projets de faible importance, n'a pas porté à la liberté d'entreprendre une atteinte disproportionnée au regard de l'objectif d'intérêt général qu'il poursuivait. <br/>
<br/>
              6. En second lieu, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit.<br/>
<br/>
              7. La différence de traitement instituée par les dispositions critiquées, qui repose sur la différence de situation existant entre les architectes et les autres professionnels de l'aménagement, de l'urbanisme ou des paysages, parmi lesquels les géomètres-experts, eu égard à leurs qualifications et compétences respectives, est en rapport direct avec l'objet de la loi qui l'établit. Par conséquent, ces dispositions ne méconnaissent pas le principe d'égalité devant la loi. <br/>
<br/>
              8. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le Conseil supérieur de l'Ordre des géomètres-experts, le moyen tiré de ce que les dispositions de l'article L. 441-4 du code de l'urbanisme, dans leur rédaction issue de l'article 81 de la loi du 7 juillet 2016, méconnaissent les droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le Conseil supérieur de l'Ordre des géomètres-experts. <br/>
Article 2 : La présente décision sera notifiée au Conseil supérieur de l'Ordre des géomètres-experts et à la ministre de la culture.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de la cohésion des territoires. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
