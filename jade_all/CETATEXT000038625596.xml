<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038625596</ID>
<ANCIEN_ID>JG_L_2019_06_000000428557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/62/55/CETATEXT000038625596.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 14/06/2019, 428557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:428557.20190614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...et M. D...A...ont demandé au juge des référés du tribunal administratif de Nantes, en premier lieu, de suspendre l'exécution, d'une part, des décisions du directeur de la maison d'arrêt du Mans du 19 octobre 2018 et du 29 octobre 2018, confirmées par une décision du directeur interrégional de services pénitentiaires de Rennes du 2 novembre 2018, refusant de délivrer à Mme B...un permis pour rendre visite à M. A... et, d'autre part, de la décision du directeur de la maison d'arrêt du Mans du 20 novembre 2018, portant suppression définitive des lignes téléphoniques enregistrées sous le nom de Mme B...et, en second lieu, d'enjoindre au directeur de la maison d'arrêt du Mans de délivrer à Mme B...un permis de rendre visite à M. A...et de rétablir l'autorisation accordée à M. A...de lui téléphoner.<br/>
<br/>
              Par une ordonnance n° 1900568 du 13 février 2019, le juge des référés du tribunal administratif de Nantes a fait droit à ces demandes.<br/>
<br/>
              Par un pourvoi  enregistré le 1er mars 2019, la garde des sceaux, ministre de la justice, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de Mme B...et de M.A.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de procédure pénale ;<br/>
              - la loi pénitentiaire n° 2009-1436 du 24 novembre 2009;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme C...B...et de M. D...A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 521-1 du code de justice administrative dispose que : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier qui lui était soumis qu'en considérant que le moyen tiré de l'atteinte excessive au droit au respect de la vie familiale des requérants étaient de nature à susciter un doute sérieux sur la légalité des décisions litigieuses qui reposent, s'agissant du refus de permis de visite, sur la circonstance que Mme B...a été la victime des faits de violence pour lesquels son conjoint, M. A...a été condamné et, s'agissant de la suspension de la ligne téléphonique, sur le caractère violent et menaçant à l'égard du personnel pénitentiaire, des propos échangés entre les intéressés, le juge des référés du tribunal administratif de Nantes a entaché l'ordonnance du 13 février 2019 de dénaturation.<br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la garde des sceaux, ministre de la justice, est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au titre de la procédure de référé engagée par Mme B...et M.A..., en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il ressort des pièces du dossier qu'aucun des moyens soulevés n'est de nature à faire naitre un doute sérieux sur la légalité des décisions litigieuses à la date à laquelle elles ont été prises.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence posée à l'article de L. 521-1 du code de justice administrative, que la requête de Mme B...et M. A...doit être rejetée.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du 13 février 2019 du juge des référés du tribunal administratif de Nantes est annulée.<br/>
Article 2 : La requête présentée par Mme B...et M. A...devant le juge des référés du tribunal administratif de Nantes est rejetée.<br/>
Article 3 : Les conclusions présentées par Mme B...et M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4: La présente décision sera notifiée à la garde des sceaux, ministre de la justice, à Mme C... B...et à M. D...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
