<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034293460</ID>
<ANCIEN_ID>JG_L_2017_03_000000398904</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/29/34/CETATEXT000034293460.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/03/2017, 398904, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398904</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398904.20170327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler la décision du 15 décembre 2011 par laquelle le ministre de la défense a refusé de faire droit à son recours administratif du 20 octobre 2011 tendant au rétablissement de sa rémunération à son niveau antérieur et à ce que l'Etat soit condamné à lui verser une somme de 10 000 euros. Par un jugement n° 1203041/5-3 du 18 juillet 2014, le tribunal administratif de Paris a  partiellement fait droit à sa demande et a annulé la décision attaquée et condamné l'Etat à lui verser une somme nette correspondant à une rémunération brute de 2 757 euros.<br/>
<br/>
              Par un arrêt n°s 14PA04008, 14PA04045 du 19 février 2016, la cour administrative d'appel de Paris a, sur appel du ministre de la défense, annulé le jugement du tribunal administratif et rejeté l'appel formé par M. B...tendant à la réformation de ce jugement en tant qu'il a limité la condamnation de l'Etat à une somme de 2 757 euros.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 avril et 9 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la défense et de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 51-582 du 22 mai 1951 ;<br/>
              - l'arrêté du 8 février 2007 fixant le régime de maintien de la rémunération du personnel à statut ouvrier du ministère de la défense muté dans le cadre des restructurations ;<br/>
              - l'arrêté du 28 novembre 2008 fixant le régime de rémunération des personnels ouvriers de l'Etat mensualisés du ministère de la défense ;<br/>
              - l'instruction du 26 juillet 2002 relative à la durée du travail effectif des ouvriers de l'Etat du ministère de la défense ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.B....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ouvrier de l'Etat, recruté au ministère de la défense en qualité de conducteur de véhicules depuis le 1er août 2000, a été muté, à la suite d'une restructuration, au service parisien de soutien de l'administration centrale à compter du 28 juin 2010 ; qu'il a signé à cette occasion une convention de mobilité prévoyant le maintien du bénéfice de la rémunération d'heures supplémentaires ; qu'après avoir constaté que le volume d'heures supplémentaires qui lui était payé à compter du mois de juillet 2011 était inférieur à celui prévu par sa convention de mobilité, il a saisi le ministre de la défense d'un recours administratif tendant à obtenir le paiement des heures supplémentaires non payées effectuées entre les mois de juillet et d'octobre 2011 ; que le ministre a rejeté sa demande par une décision du 15 décembre 2011 ; que, sur la demande de M. B...et par un jugement du 18 juillet 2014, le tribunal administratif de Paris a annulé cette décision et condamné l'Etat à verser à M. B...une somme nette correspondant à une rémunération brute de 2 757 euros ; que, par l'arrêt attaqué, la cour administrative d'appel de Paris a, sur appel du ministre de la défense, annulé le jugement du tribunal administratif et rejeté l'appel formé par M. B...tendant à la réformation de ce jugement en tant qu'il a limité la condamnation pécuniaire de l'Etat à une somme de 2 757 euros ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 1er de l'arrêté interministériel du 8 février 2007 fixant le régime de maintien de la rémunération du personnel à statut ouvrier du ministère de la défense muté dans le cadre des restructurations : " Tout ouvrier de l'Etat qui fait l'objet d'une mutation dans le cadre des restructurations conserve son groupe de rémunération (...) " ; qu'aux termes de l'article 3 de cet arrêté : " Le régime des heures supplémentaires effectuées au cours de l'année qui précède la mutation est fixé ainsi qu'il suit : - les heures qui correspondent à des heures exceptionnelles dues à une charge ou à une organisation de travail inhabituelle n'ont pas à être prises en compte dans la conservation de la rémunération ; - les heures qui correspondent à la charge de travail normale dans l'emploi occupé par l'ouvrier avant sa mutation sont conservées. Le nombre d'heures maintenu est déterminé par la moyenne annuelle des heures supplémentaires effectuées par l'ouvrier pendant l'année qui précède sa mutation. De nouvelles heures supplémentaires ne peuvent être versées que lorsque les heures réellement effectuées excèdent le nombre d'heures correspondant à celles ainsi rémunérées " ; qu'il résulte de l'article 7 de ce même arrêté que la rémunération ainsi déterminée figure dans un " contrat de mobilité " ; que ces dispositions ont pour objet d'instituer en faveur des ouvriers de l'Etat mutés dans le cadre d'une restructuration une garantie de maintien de la rémunération des heures supplémentaires, correspondant à la charge de travail normale de l'agent, perçue durant l'année précédant la mutation indépendamment du nombre d'heures supplémentaires effectuées par les intéressés après cette mutation ; que le " contrat de mobilité " fixant l'avantage financier ainsi déterminé crée des droits au profit des ouvriers de l'Etat qui en bénéficient ; <br/>
<br/>
              3. Considérant que, sous réserve de dispositions législatives ou réglementaires contraires, et hors le cas où il est satisfait à une demande du bénéficiaire, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale ; qu'une décision administrative explicite accordant un avantage financier crée des droits au profit de son bénéficiaire alors même que l'administration avait l'obligation de refuser cet avantage ; <br/>
<br/>
              4. Considérant que la cour administrative d'appel de Paris a estimé qu'une partie des heures supplémentaires dont le contrat de mobilité de M. B...prévoyait que la rémunération serait conservée avait été effectuée en méconnaissance de la durée maximale du travail, fixée à quarante-huit heures au cours d'une même semaine ou quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives par l'article 1er de l'arrêté interministériel du 28 novembre 2008 fixant le régime de rémunération des personnels ouvriers de l'Etat mensualisés du ministère de la défense et les dispositions de l'instruction du 26 juillet 2002 relative à la durée du travail effectif des ouvriers de l'Etat du ministère de la défense ; qu'elle en a déduit que M. B...ne pouvait se prévaloir d'aucun droit acquis au maintien de cette rémunération, en relevant que le caractère créateur de droits de l'attribution d'un avantage financier ne fait pas obstacle à ce que la décision soit abrogée si l'intéressé ne remplit plus les conditions auxquelles cet avantage est subordonné ou si l'administration modifie l'appréciation qui avait justifié son attribution ; que, toutefois, s'il appartient à l'autorité compétente de cesser d'attribuer un avantage financier donnant lieu à des versements réguliers lorsque son maintien est subordonné à des conditions qui doivent être régulièrement vérifiées et qu'elle constate que celles-ci ne sont plus remplies, tel n'est pas le cas de la décision de garantir à un agent, dans le cadre d'un contrat de mobilité, qu'il conservera son niveau de rémunération antérieur, décision dont le caractère créateur de droits exclut qu'elle puisse être remise en cause au-delà du délai de quatre mois en raison de son illégalité ; qu'ainsi, à supposer même que le " contrat de mobilité " de M. B...doive être regardé comme entaché d'illégalité en tant qu'il prévoit le maintien de la rémunération d'heures supplémentaires effectuées en méconnaissance de l'arrêté et de l'instruction précités, la garantie de rémunération qu'il institue ne pouvait être remise en cause, ainsi qu'il a été dit au point 3, que dans le délai de quatre mois à compter de son entrée en vigueur ; que par suite, M. B...est fondé à soutenir que la cour administrative d'appel de Paris a commis une erreur de droit ; <br/>
<br/>
              5. Considérant que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt de la cour administrative d'appel de Paris doit être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 19 février 2016 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à M. B...une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
