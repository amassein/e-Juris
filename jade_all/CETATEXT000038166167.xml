<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166167</ID>
<ANCIEN_ID>JG_L_2019_02_000000411775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166167.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 25/02/2019, 411775, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:411775.20190225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 22 juin 2017 et le 5 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'Union des industries de la protection des plantes demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-590 du 20 avril 2017 relatif à la mise en oeuvre du dispositif expérimental des certificats d'économie de produits phytopharmaceutiques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 modifié ;<br/>
              - la directive 2009/128/CE du Parlement européen et du Conseil du 21 octobre 2009 modifiée ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de l'Union des industries de la protection des plantes (UIPP) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par décret du 20 avril 2017, le Premier ministre a précisé les conditions et modalités de réalisation des actions que doivent mettre en oeuvre les distributeurs de produits phytopharmaceutiques et qui donnent lieu, à titre expérimental, à la délivrance de certificats d'économie de produits phytopharmaceutiques, telles que prévues par les articles L. 254-10 à L. 254-10-9 du code rural et de la pêche maritime. L'Union des industries de la protection des plantes demande au Conseil d'Etat l'annulation de ce décret.<br/>
<br/>
              Sur le moyen tiré de la non conformité à la Constitution des articles L. 254-10 à L. 254-10-9 du code rural et de la pêche maritime :<br/>
<br/>
              2. Par une décision du 15 septembre 2017, le Conseil d'Etat statuant au contentieux a jugé qu'il n'y avait pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité mettant en cause les articles L. 254-10 à L. 254-10-9 du code rural et de la pêche maritime. Dès lors, le moyen tiré de ce que le décret attaqué serait illégal au motif qu'il permet l'application de ces articles, alors que ceux-ci porteraient atteinte aux droits et libertés garantis par la Constitution, ne peut qu'être écarté.<br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des articles L. 254-10 à L. 254-10-9 du code rural et de la pêche maritime avec le droit de l'Union européenne :<br/>
<br/>
              3. Aux termes du troisième considérant de la directive 2009/128/CE du Parlement européen et du Conseil du 21 octobre 2009, instaurant un cadre d'action communautaire pour parvenir à une utilisation des pesticides compatible avec le développement durable : " Il convient que les mesures prévues par la présente directive complètent, sans y porter atteinte, les mesures stipulées dans d'autres dispositions connexes de la législation communautaire, en particulier [...] le règlement (CE) no 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009 concernant la mise sur le marché des produits phytopharmaceutiques [...] ". Aux termes de l'article 1er de la même directive : " La présente directive instaure un cadre pour parvenir à une utilisation des pesticides compatible avec un développement durable en réduisant les risques et les effets des pesticides sur la santé humaine et sur l'environnement et en encourageant le recours à la lutte intégrée contre les ennemis des cultures et à des méthodes ou techniques de substitution, telles que les moyens non chimiques alternatifs aux pesticides ". Aux termes du paragraphe 1 de l'article 4 de la même directive : " Les États membres adoptent des plans d'action nationaux pour fixer leurs objectifs quantitatifs, leurs cibles, leurs mesures et leurs calendriers en vue de réduire les risques et les effets de l'utilisation des pesticides sur la santé humaine et l'environnement et d'encourager l'élaboration et l'introduction de la lutte intégrée contre les ennemis des cultures et de méthodes ou de techniques de substitution en vue de réduire la dépendance à l'égard de l'utilisation des pesticides. [...] / Les plans d'action nationaux comprennent aussi des indicateurs destinés à surveiller l'utilisation des produits phytopharmaceutiques contenant des substances actives particulièrement préoccupantes, notamment quand il existe des solutions de substitution. [...] / Ils établissent également, sur la base de ces indicateurs et compte tenu, le cas échéant, des objectifs de réduction du risque ou de l'utilisation déjà atteints avant l'application de la présente directive, des calendriers et des objectifs pour la réduction de l'utilisation, notamment si la réduction de l'utilisation est un moyen approprié d'obtenir une réduction du risque quant aux éléments définis comme prioritaires selon l'article 15, paragraphe 2, point c). Ces objectifs peuvent être intermédiaires ou finaux. Les États membres emploient tous les moyens nécessaires conçus pour atteindre ces objectifs [...] ".<br/>
<br/>
              4. Aux termes du quarante-septième considérant du règlement (CE) n° 1107/2009 du Parlement européen et du Conseil du 21 octobre 2009, concernant la mise sur le marché des produits phytopharmaceutiques et abrogeant les directives 79/117/CEE et 91/414/CEE du Conseil : " Les mesures prévues dans le présent règlement s'appliquent sans préjudice des autres actes législatifs communautaires, en particulier la directive 2009/128/CE [...] ". Il résulte de ces dispositions que l'existence du régime d'autorisation des produits phytopharmaceutiques organisé par le règlement n°1107/2009 ne fait pas obstacle à la poursuite des objectifs de la directive 2009/128/CE, notamment par l'incitation à recourir à des méthodes ou techniques de substitution à l'utilisation de ces produits. <br/>
<br/>
              5. Aux termes de l'article L. 253-6 du code rural et de la pêche maritime : " Un plan d'action national fixe les objectifs quantitatifs, les cibles, les mesures et calendriers en vue de réduire les risques et les effets de l'utilisation des produits phytopharmaceutiques sur la santé humaine et l'environnement, et les mesures encourageant l'élaboration et l'introduction de la lutte intégrée contre les ennemis des cultures et les méthodes ou techniques de substitution en vue de réduire la dépendance à l'égard de l'utilisation des produits phytopharmaceutiques. Il comprend des indicateurs de suivi des objectifs fixés [...] ".<br/>
<br/>
              6. Aux termes de l'article L. 254-10 du code rural et de la pêche maritime : " A titre expérimental et pour une période allant du 1er juillet 2016 au 31 décembre 2022, il est mis en place en métropole un dispositif visant à la réduction de l'utilisation de certains produits phytopharmaceutiques mentionnés à l'article L. 253-1 dont la liste est fixée par décret en Conseil d'Etat et comportant l'émission de certificats d'économie de ces produits ". Aux termes de l'article L. 254-10-1 du code rural et de la pêche maritime : " I. - Sont soumises à des obligations de réalisation d'actions tendant à la réduction de l'utilisation de produits phytopharmaceutiques les personnes qui vendent en métropole, à des utilisateurs professionnels, des produits mentionnés à l'article L. 254-10. Ces personnes sont dénommées les " obligés ". / L'obligé est tenu de mettre en place des actions visant à la réalisation d'économies de produits phytopharmaceutiques ou de faciliter la mise en oeuvre de telles actions. / II. - L'autorité administrative notifie à chaque obligé l'obligation de réalisation d'actions qui lui incombe du 1er janvier 2021 au 31 décembre 2021 en vertu de la présente section compte tenu des quantités de produits phytopharmaceutiques qu'il a déclarées en application des articles L. 213-10-8 et L. 213-11 du code de l'environnement. / Cette obligation est proportionnelle aux quantités de chaque substance active contenues dans ces produits phytopharmaceutiques, pondérées, dans des conditions fixées par décret en Conseil d'Etat, par des coefficients liés soit aux caractéristiques d'emploi de ces produits, soit aux dangers des substances actives qu'ils contiennent. Elle est exprimée en nombre de certificats d'économie de produits phytopharmaceutiques [...] ". Il résulte de ces dispositions que les obligations de réalisation d'actions d'économie dans l'usage de certains produits phytopharmaceutiques mises à la charge, à titre expérimental, des personnes qui vendent ces produits sur le territoire métropolitain à des professionnels utilisateurs sont des obligations de moyens qu'il leur appartient de mettre en place dans leur activité de distribution, notamment en termes d'offres de solutions ou de produits alternatifs.<br/>
<br/>
              7. En premier lieu, au regard des objectifs d'un plan national d'action, tels que définis par l'article 4-1 la directive 2009/128/CE cité au point 3 ci-dessus, qui incluent notamment l'élaboration et l'introduction de méthodes ou de techniques de substitution en vue de réduire la dépendance à l'égard de l'utilisation des pesticides, de telles obligations entrent bien dans le champ des actions susceptibles d'être mises en oeuvre par un Etat membre au titre d'un tel plan d'action. En deuxième lieu, compte-tenu de ce qui a été dit au point 4, de telles obligations ne sont pas en soi incompatibles avec l'existence de ce régime d'autorisation. En troisième lieu, si une mesure remettant en cause les termes d'une autorisation délivrée dans le cadre de ce régime prévu par le règlement n° 1107/2009 ne saurait être fondée que sur l'existence d'un risque avéré, impliquant le cas échéant de tenir compte à la fois de la dangerosité du produit et des conditions d'utilisation dont est assortie l'autorisation le concernant, c'est sans porter atteinte à ces dispositions du droit de l'Union européenne que l'article L. 254-10-1 précité a prévu que les obligations prévues par le dispositif contesté, qui n'ont ni pour objet ni pour effet d'instituer une telle remise en cause, sont modulées en fonction du risque potentiel de ces produits, résultant soit de leurs caractéristiques d'emploi soit des dangers des substances actives qu'ils emploient. En quatrième lieu, dès lors que ces obligations de moyens n'ont pas pour objet ou pour effet d'interdire ou d'imposer des restrictions à l'utilisation de pesticides, elles peuvent être mises en oeuvre dans le cadre d'un plan d'action national sans qu'y fasse obstacle l'article 12 de la directive 2009/128/CE qui prévoit que le Etats membres veillent à restreindre ou interdire l'utilisation de pesticides dans certaines zones spécifiques. Il résulte de tout ce qui précède que le moyen tiré de ce que les articles L. 254-10 à L. 254-10-9 du code rural et de la pêche maritime, pour l'application desquels le décret attaqué a été pris, seraient incompatibles avec la directive 2009/128/CE et le règlement n° 1107/2009 doit être écarté.<br/>
<br/>
              Sur le moyen tiré de ce que le décret attaqué a été pris en méconnaissance du principe de non-rétroactivité des peines :<br/>
<br/>
              9. Aux termes de l'article L. 254-10-5 du code rural et de la pêche maritime : " A l'issue d'une procédure contradictoire, les obligés qui, au 31 décembre 2021, n'ont pas satisfait à l'obligation qui leur a été notifiée doivent verser au Trésor public une pénalité proportionnelle au nombre de certificats d'économie de produits phytopharmaceutiques manquants pour atteindre l'objectif dont le montant est arrêté par l'autorité administrative [...] ". Aux termes du II de l'article R. 254-31 du code rural et de la pêche maritime, créé par le décret attaqué : " Les actions mentionnées au I de l'article L. 254-10-1 sont menées du 1er juillet 2016 au 31 décembre 2021 ". Aux termes du II de l'article R. 254-35 du même code, créé par le décret attaqué : " Un obligé peut acquérir des certificats auprès d'un éligible pendant la période fixée au II de l'article R. 254-31 et, à compter du 1er juillet 2021, auprès d'un obligé ". Aux termes de l'article 2 du décret attaqué : " Par dérogation au I de l'article R. 254-35 du code rural et de la pêche maritime, pour les actions réalisées entre le 1er juillet 2016 et le 31 décembre 2016, les demandes de délivrance de certificats d'économie de produits phytopharmaceutiques peuvent être transmises jusqu'au 30 septembre 2017 ".<br/>
<br/>
              10. Si, à la suite de l'annulation de l'ordonnance n° 2015-1244 du 7 octobre 2015 par une décision n° 394696 du 28 décembre 2016 du Conseil d'Etat statuant au contentieux, le législateur a entendu maintenir l'ouverture de l'expérimentation prévue par cette ordonnance à la date du 1er juillet 2016 que celle-ci avait initialement fixée, comme cela résulte des dispositions de l'article L. 254-10 du code rural et de la pêche maritime cité au point 6 ci-dessus, et si les dispositions réglementaires citées au point 9 ci-dessus permettent de convertir en certificats d'économie de produits phytopharmaceutiques les actions qui avaient commencé à être mises en oeuvre à compter de cette date, il résulte en revanche des dispositions du II de l'article L. 254-10-1 du même code, citées au point 6 ci-dessus, que la pénalité instituée par l'article L. 254-10-5 n'est susceptible d'être infligée aux obligés qu'en cas de non-respect par ceux-ci des obligations qui leur auront été notifiées par l'autorité administrative pour la période allant du 1er janvier 2021 au 31 décembre 2021. Cette pénalité ne saurait donc sanctionner des faits antérieurs à ces dispositions législatives et au décret attaqué, lequel ne comporte aucune disposition dérogeant à cette limite. Le moyen tiré de ce que ce décret méconnaîtrait le principe de non-rétroactivité des peines et des sanctions garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 doit donc, en tout état de cause, être écarté.<br/>
<br/>
              11. Il résulte de l'ensemble de ce qui précède que l'Union des industries de la protection des plantes n'est pas fondée à demander l'annulation du décret qu'elle attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union des industries de la protection des plantes est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Union des industries de la protection des plantes, au Premier ministre et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
