<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042250405</ID>
<ANCIEN_ID>JG_L_2020_08_000000442563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/25/04/CETATEXT000042250405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/08/2020, 442563, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-08-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:442563.20200812</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 7 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... D... et M. B... A... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du 10 juin 2020 portant adaptation des épreuves du concours de recrutement des personnels de direction d'établissement d'enseignement ou de formation relevant du ministre de l'éducation nationale (PERDIR) ouvert au titre de l'année 2020 en raison de la crise sanitaire née de l'épidémie de covid-19 et, par conséquent, des opérations et délibérations du concours de de recrutement des PERDIR toutes sections confondues et des listes arrêtant les admis de ce concours ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'éducation nationale, de la jeunesse et des sports de procéder, d'une part, à l'édiction d'un nouvel arrêté relatif à l'organisation du concours de recrutement des PERDIR 2020 et, d'autre part, au retrait des actes individuels litigieux ; <br/>
<br/>
              3°) de mettre à la charge du ministre de l'éducation nationale, de la jeunesse et des sports la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition de l'urgence est remplie eu égard aux risques graves d'atteinte à leurs intérêts et dès lors que les admis au concours interne de recrutement des PERDIR vont bénéficier dans les semaines à venir d'une nomination dans le corps des certifiés, que la suspension des opérations du concours interne de recrutement des PERDIR ne saurait remettre en cause la continuité du service public de l'éducation, que la préparation du concours nécessite une anticipation indispensable à l'organisation de révisions et que le contexte sanitaire ne peut justifier le bien-fondé des actes contestés ;<br/>
              - il existe un doute sérieux quant à la légalité des actes contestés ;<br/>
              - l'arrêté du 10 juin 2020 du ministre de l'éducation nationale, de la jeunesse et des sports et du ministre de l'action et des comptes publics est entaché d'une erreur manifeste d'appréciation dès lors que la suppression des épreuves orales d'admission n'est pas justifiée et proportionnée à l'objectif poursuivi, eu égard en particularité au maintien de telles épreuves pour d'autres concours et à l'absence de risque sur la continuité du service public de l'éducation ;<br/>
              - les actes contestés méconnaissent le principe d'égalité de traitement en ce que les opérations du concours interne de recrutement des PERDIR ont placé les candidats à ce concours dans une situation de différence de traitement par rapport aux candidats du concours externe injustifiée et disproportionnée, dès lors que cette différence ne peut être justifiée au regard des objectifs poursuivis et qu'elle prive les candidats au concours interne d'une chance de démontrer leur mérite lors des épreuves d'admission et de bénéficier des mêmes garanties et conditions que les autres candidats pourtant destinés à rejoindre le même corps ;<br/>
              - ils méconnaissent les principes de sécurité juridique et de confiance légitime dès lors que l'annulation des épreuves orales et les nouvelles modalités de concours ont rendu les congés de formation sollicités par les candidats au concours interne sans objet et rend difficile l'octroi futur de congés de formation, heurtant ainsi directement les engagements que l'Etat a conclu avec ces agents.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2020-774 du 9 juin 2020 ;<br/>
              - l'ordonnance n° 2020-251 du 27 mars 2020 ;<br/>
              - le décret n° 2020-437 du 16 avril 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d' une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les circonstances et le cadre juridique du litige : <br/>
<br/>
              2. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants, élèves et étudiants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12 heures, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département.<br/>
<br/>
              3. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020 puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Par un décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, plusieurs fois modifié et complété, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Par décrets du 11 mai 2020, pris sur le fondement de la loi du 11 mai 2020, le Premier ministre a abrogé l'essentiel des mesures précédemment ordonnées par le décret du 23 mars 2020 et a prescrit les nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. Par un décret du 31 mai 2020, le Premier ministre a mis fin, à compter du 2 juin 2020, à une partie des mesures restrictives jusqu'alors en vigueur. Enfin, par un décret du 21 juin 2020, le Premier ministre a modifié le décret du 31 mai 2020. <br/>
<br/>
              4. Le législateur a ensuite, par la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence, autorisé le Premier ministre à prendre, à compter du 11 juillet 2020, et jusqu'au 30 octobre 2020 inclus, diverses mesures dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19. Le Premier ministre peut notamment, dans ce cadre, ordonner la fermeture provisoire d'une ou de plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunions lorsqu'ils accueillent des activités qui, par leur nature même, ne permettent pas de garantir la mise en oeuvre des mesures de nature à prévenir les risques de propagation du virus ou lorsqu'ils se situent dans certaines parties du territoire dans lesquelles est constatée une circulation active du virus. <br/>
<br/>
              5. Par ailleurs, aux termes de l'article 5 de l'ordonnance n° 2020-351 du 27 mars 2020 relative à l'organisation des examens et concours pendant la crise sanitaire née de l'épidémie de covid-19 : " Les voies d'accès aux corps, cadres d'emplois, grades et emplois des agents publics de la fonction publique de l'Etat, de la fonction publique territoriale, de la fonction publique hospitalière et de la fonction publique des communes de la Polynésie française peuvent être adaptées, notamment s'agissant du nombre et du contenu des épreuves. / Nonobstant toute disposition législative ou réglementaire contraire, peuvent être prévues des dérogations à l'obligation de la présence physique des candidats ou de tout ou partie des membres du jury ou de l'instance de sélection, lors de toute étape de la procédure de sélection. / Les garanties procédurales et techniques permettant d'assurer l'égalité de traitement des candidats et la lutte contre la fraude sont fixées par décret... ". Aux termes notamment de l'article 24 du décret du 16 avril 2020 pris pour l'application des articles 5 et 6 de cette ordonnance : " Lorsque l'organisation des voies d'accès mentionnées en annexe, incluant notamment la publication des listes de lauréats, n'est pas achevée au 12 mars 2020, le nouveau calendrier et les nouvelles conditions d'organisation peuvent faire l'objet, le cas échéant, d'un arrêté ou d'une décision de l'autorité organisatrice reportant les épreuves concernées, publiés dans les mêmes conditions que celles applicables à l'ouverture ". Et aux termes de son<br/>
article 25 : " Sans préjudice des dispositions du titre Ier, lorsqu'une épreuve de l'une des voies d'accès mentionnées en annexe a été interrompue ou n'a pu donner lieu, à compter du 12 mars 2020, à l'examen de la totalité des candidats par le jury ou l'instance de sélection, cette épreuve peut être annulée et reportée pour l'ensemble des candidats à une date fixée par l'arrêté ou la décision mentionnée à l'article 24 ".   <br/>
<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              6. Par un arrêté du 10 juin 2020, le ministre de l'éducation nationale et de la jeunesse et le ministre de l'action et des comptes publics ont adapté les épreuves du concours de recrutement des personnels de direction d'établissement d'enseignement ou de formation relevant du ministre de l'éducation nationale (PERDIR) ouvert au titre de l'année 2020 en raison de la crise sanitaire née de l'épidémie de covid-19. Ses dispositions ont ainsi pour effet de supprimer les oraux d'admission du concours, ces oraux étant donc remplacés par les résultats d'épreuves d'admissibilité. M. D... et M. A... ont saisi le juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une demande tendant, d'une part, à la suspension de l'exécution de l'arrêté du 10 juin 2020 et, par conséquent, des opérations et délibérations du concours de recrutement  des PERDIR et des listes arrêtant les admis de ce concours et, d'autre part, à ce qu'il soit enjoint au ministre de l'éducation nationale, de la jeunesse et des sports de procéder à l'édiction d'un nouvel arrêté relatif à l'organisation du concours de recrutement des PERDIR et au retrait des actes individuels litigieux.<br/>
<br/>
              7. En premier lieu, les requérants ne peuvent, en tout état de cause, utilement se prévaloir de ce que les épreuves orales d'admission auraient été maintenues dans le cadre du " concours externe " de recrutement des PERDIR pour soutenir que le principe d'égalité aurait été méconnu dès lors qu'un concours externe de recrutement est distinct du concours interne.<br/>
<br/>
              8. En deuxième lieu, les requérants soutiennent, à l'appui de leur moyen tiré de l'erreur manifeste d'appréciation, que la suppression des épreuves orales du concours de recrutement des PERDIR n'est pas justifiée et proportionnée à l'objectif poursuivi, eu égard en particularité au maintien de telles épreuves pour d'autres concours et à l'absence de risque sur la continuité du service public de l'éducation qui rendaient possible le maintien de ces oraux d'admission. Toutefois, compte tenu de la persistance de la circulation du virus, de la nécessité de clore les opérations de ce concours ouvert au titre de l'année 2020 sans pouvoir prévoir la situation sanitaire dans les semaines à venir et alors même que les ministres auraient pu opter pour un maintien de l'ensemble des épreuves orales d'admission en septembre-octobre, comme cela avait été d'abord envisagé, le moyen tiré de ce que le choix de supprimer les épreuves orales du concours de recrutement des PERDIR serait entaché d'une erreur manifeste d'appréciation n'est pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des dispositions contestées.<br/>
<br/>
              9. En dernier lieu, si les requérants soutiennent que l'annulation des épreuves orales d'admission méconnaît les principes de sécurité juridique et de confiance légitime, la seule circonstance selon laquelle certains candidats auraient bénéficié de congés de formation aux fins de préparer les épreuves orales d'admission du concours de recrutement des<br/>
PERDIR n'est pas, par elle-même, de nature à créer un doute sérieux quant à la légalité des<br/>
actes contestés. <br/>
<br/>
              10. Il résulte de ce qui précède que, eu égard aux moyens soulevés par M. D... et M. A..., leur requête peut être rejetée par application des dispositions de l'article L. 522-3 précitées du code de justice administrative, dont leurs conclusions tendant à l'application des dispositions de l'article L 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. D... et de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... D..., premier requérant dénommé.<br/>
Copie en sera adressée au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
