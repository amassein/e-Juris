<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038633894</ID>
<ANCIEN_ID>JG_L_2019_06_000000420833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/63/38/CETATEXT000038633894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 17/06/2019, 420833, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420833.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 mai et 20 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Vivre notre Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° CODEP-OLS-2018-012716 du 20 mars 2018 du président de l'Autorité de sûreté nucléaire autorisant la société Electricité de France (EDF) à créer provisoirement une aire d'entreposage de déchets potentiellement pathogènes sur le site de la centrale nucléaire de Belleville-sur-Loire ; <br/>
<br/>
              2°) de mettre à la charge de l'Autorité de sûreté nucléaire la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2007-1557 du 2 novembre 2007 ;<br/>
              - le décret n° 2019-190 du 14 mars 2019 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de l'association Vivre notre Loire et à la SCP Piwnica, Molinié, avocat de la société Electricité de France (EDF).<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2019, présentée par l'association Vivre notre Loire.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2019, présentée par la société Electricité de France.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 mai 2019, présentée par l'Autorité de sûreté nucléaire.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 593-15 du code de l'environnement dispose qu'en dehors notamment du cas mentionné au II de l'article L. 593-14 de ce code relatif aux modifications substantielles d'une installation nucléaire de base, les modifications notables d'une telle installation, de ses modalités d'exploitation autorisées, des éléments ayant conduit à son autorisation ou à son autorisation de mise en service sont soumises, en fonction de leur importance, soit à déclaration auprès de l'Autorité de sûreté nucléaire, soit à autorisation par cette autorité. L'article 26 du décret du 2 novembre 2007 relatif aux installations nucléaires de base et au contrôle, en matière de sûreté nucléaire, du transport de substances radioactives dans sa rédaction en vigueur, repris à l'article R. 593-55 du code de l'environnement, prévoit que, sauf dans les cas mentionnés à l'article 27 dans lesquels les modifications ne remettent pas en cause de manière significative le rapport de sûreté ou l'étude d'impact de l'installation et dont la liste est fixée par décision de l'Autorité de sûreté nucléaire, repris à l'article R. 593-59 du même code, les modifications notables mentionnées à l'article L. 593-15 du code de l'environnement sont soumises à autorisation. <br/>
<br/>
              2. Lorsqu'il est saisi d'un recours contre une décision autorisant des modifications notables à une installation nucléaire de base, il appartient au juge du plein contentieux d'apprécier le respect des règles de procédure régissant la demande d'autorisation au regard des circonstances de fait et de droit en vigueur à la date de délivrance de la décision attaquée. Les obligations relatives à la composition du dossier de demande d'autorisation d'une modification notable d'une installation nucléaire de base relèvent, s'agissant du contentieux des installations classées, des règles de procédure. Les inexactitudes, omissions ou insuffisances affectant ce dossier ne sont susceptibles de vicier la procédure et ainsi d'entacher d'irrégularité l'autorisation que si elles ont eu pour effet de nuire à l'information complète de la population ou si elles ont été de nature à exercer une influence sur la décision de l'autorité administrative.<br/>
<br/>
              3. Aux termes de l'article 26 du décret du 2 novembre 2007, alors en vigueur, aujourd'hui repris, en substance, aux articles R. 593-55 et suivants du code de l'environnement : " Sauf dans les cas mentionnés à l'article 27, les modifications mentionnées à l'article L. 593-15 du code de l'environnement sont soumises à autorisation. / I - Pour obtenir cette autorisation, l'exploitant dépose auprès de l'Autorité de sûreté nucléaire une demande accompagnée d'un dossier comportant tous les éléments de justification utiles, notamment les mises à jour rendues nécessaires des documents mentionnés aux articles 8 et 20 (...) ". Aux termes de l'article 20 du même décret, alors en vigueur : " (...) II. - En vue de la mise en service de l'installation, l'exploitant adresse à l'Autorité de sûreté nucléaire un dossier comprenant : / (...) 3° Une étude sur la gestion des déchets de l'installation faisant état des objectifs de l'exploitant pour limiter le volume et la toxicité radiologique, chimique et biologique des déchets produits dans ses installations et pour réduire, par la valorisation et le traitement de ces déchets ainsi produits, le stockage définitif réservé aux déchets ultimes. Cette étude prend en compte l'ensemble des filières de gestion des déchets de l'installation jusqu'à l'élimination de ceux-ci. Elle peut couvrir les déchets produits par l'ensemble des installations et équipements situés dans le périmètre (...) ". <br/>
<br/>
              4. Il résulte de ces dispositions, en vigueur à la date de l'autorisation attaquée, que lorsqu'une modification d'une installation nucléaire de base est soumise à autorisation, la demande de l'exploitant doit être accompagnée d'un dossier comportant tous les éléments de justification utiles et en particulier, si cela est nécessaire, la mise à jour de l'étude de gestion des déchets.<br/>
<br/>
              5. La société Electricité de France (EDF) a demandé à l'Autorité de sûreté nucléaire la possibilité de créer sur le site de la centrale nucléaire de Belleville-sur-Loire, hors de l'îlot nucléaire, une aire provisoire d'entreposage des boues et tartres provenant des circuits de refroidissement tertiaires de la centrale. L'objet de cette demande était, dans l'attente de la réalisation d'une aire d'entreposage pérenne, de remplacer l'aire initiale utilisée pour le chantier NPGV (nettoyage chimique préventif de générateurs de vapeur). L'Autorité de sûreté nucléaire a estimé que la création de cette aire d'entreposage de déchets biologiques constituait une modification notable des modalités d'exploitation de la centrale nucléaire soumise à la procédure d'autorisation. Après instruction de cette demande, elle a accordé l'autorisation de créer cette aire provisoire d'entreposage par la décision attaquée du 20 mars 2018.<br/>
<br/>
              6. Il résulte de l'instruction, d'une part, que ces déchets sont susceptibles de contenir des germes (amibes et légionelles) potentiellement pathogènes pour l'homme et, d'autre part, ainsi d'ailleurs que l'admettent l'Autorité de sûreté nucléaire et Electricité de France dans leurs écritures, qu'une mise à jour de l'étude de gestion des déchets était nécessaire eu égard à la nature de ces déchets. Si Electricité de France a indiqué au point 7.1.6 intitulé " Etude déchets " de la note d'analyse du cadre réglementaire de la création et de l'exploitation de l'aire provisoire de déchets potentiellement pathogènes accompagnant le dossier de demande d'autorisation, que " cette modification sera intégrée dans la montée d'indice de l'étude déchets du CNPE de Belleville-sur-Loire ", il ne résulte de l'instruction ni que cette mise à jour ait été jointe au dossier de demande ni même, au demeurant, qu'elle ait été réalisée. Cette omission est, dans les circonstances de l'espèce, de nature à avoir exercé une influence sur la décision de l'Autorité de sûreté nucléaire. La décision attaquée est par suite intervenue au terme d'une procédure irrégulière. <br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que l'association Vivre notre Loire est fondée à demander l'annulation de la décision qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'association Vivre notre Loire, qui n'est pas partie perdante dans la présente instance, la somme que demande Electricité de France au titre des frais exposés par elle et non compris dans les dépens. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Autorité de sûreté nucléaire et d'Electricité de France la somme de 1 000 euros chacune qui sera versée à l'association Vivre notre Loire, au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision n° CODEP-OLS-2018-012716 du 20 mars 2018 du président de l'Autorité de sûreté nucléaire est annulée.<br/>
<br/>
Article 2 : L'Autorité de sûreté nucléaire et la société Electricité de France verseront chacune à l'association Vivre notre Loire la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Association Vivre notre Loire, à l'Autorité de sûreté nucléaire, à la société Electricité de France et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
