<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029308676</ID>
<ANCIEN_ID>JG_L_2014_07_000000370065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/30/86/CETATEXT000029308676.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 02/07/2014, 370065, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:370065.20140702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre délégué, chargé du budget, enregistré le 11 juillet 2013 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat d'annuler le jugement n° 1101214 du 13 mai 2013 par lequel le tribunal administratif de Lille a déchargé Mme B...de la cotisation de taxe d'habitation à laquelle elle a été assujettie au titre de l'année 2010 dans les rôles de la commune de Lomme (59160) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le protocole n° 7 sur les privilèges et immunités de l'Union européenne ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., fonctionnaire de l'Union européenne, est propriétaire, avec le partenaire auquel elle est liée par un pacte civil de solidarité, d'une maison à Lomme ; qu'elle a demandé que les rémunérations qu'elle perçoit de l'Union européenne ne soient pas prises en compte pour le calcul du plafonnement, dont elle a demandé le bénéfice, de la taxe d'habitation établie à raison de l'occupation de cette maison ; que le ministre délégué, chargé du budget se pourvoit en cassation contre le jugement du 13 mai 2013 par lequel le tribunal administratif de Lille a fait droit à la demande de Mme B...;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 12 du protocole n°7 sur les privilèges et immunités de l'Union européenne : " (...) les fonctionnaires et autres agents de l'Union sont soumis au profit de celle-ci à un impôt sur les traitements, salaires et émoluments versés par elle. / Ils sont exempts d'impôts nationaux sur les traitements, salaires et émoluments versés par l'Union " ;  qu'aux termes de l'article 1414 A du code général des impôts, dans sa rédaction applicable à l'année 2010 : " I. Les contribuables autres que ceux mentionnés à l'article 1414, dont le montant des revenus de l'année précédente n'excède par la limite prévue au II de l'article 1417, sont dégrevés d'office de la taxe d'habitation afférente à leur habitation principale pour la fraction de leur cotisation qui excède 3,44% de leur revenu au sens du IV de l'article 1417 diminué d'un abattement (...). II. Pour l'application du I : a. le revenu s'entend du revenu du foyer fiscal du contribuable au nom duquel la taxe est établie (...) " ; qu'aux termes du IV de l'article 1417 du même code : " 1° Pour l'application du présent article, le montant des revenus s'entend du montant net après application éventuelle des règles de quotient définies à l'article 163-0 A des revenus et plus-values retenus pour l'établissement de l'impôt sur le revenu au titre de l'année précédente. / Ce montant est majoré : / (...) c. du montant des revenus (...) perçus par les fonctionnaires des organisations internationales (...) " ; <br/>
<br/>
              3. Considérant que, comme le juge la Cour de justice de l'Union européenne, les dispositions du deuxième alinéa de l'article 12 du protocole sur les privilèges et immunités de l'Union européenne excluent toute imposition non seulement directe mais également indirecte, par les Etat membres, des rémunérations versées par l'Union ; que, toutefois, ces stipulations ne s'opposent pas à ce qu'un avantage fiscal s'appliquant de manière non discriminatoire aux ménages disposant de revenus inférieurs à un certain montant soit refusé aux ménages dont un des conjoints a la qualité de fonctionnaire ou d'agent des Communautés européennes lorsque son traitement est supérieur à ce montant ainsi que l'a jugé la Cour de justice des Communautés européennes dans son arrêt du 14 octobre 1999 Vander Zwalmen et Massart (affaire C-229/98) ; que le protocole s'oppose, en revanche, à ce que ces rémunérations soient prises en compte pour calculer le montant de l'imposition due, ainsi que l'a jugé ensuite la Cour dans son arrêt du 5 juillet 2012 Bourges-Maunoury (affaire C-558/10) relatif au mode de détermination du montant plafonné de l'impôt français de solidarité sur la fortune ; <br/>
<br/>
              4. Considérant que le ministre délégué, chargé du budget soutient que le tribunal administratif de Lille a commis une erreur de droit en jugeant qu'il y a lieu d'écarter l'application des dispositions de l'article 1417 du code général des impôts qui prévoient la prise en compte des revenus perçus par les fonctionnaires ou autres agents de l'Union européenne pour le calcul du montant de la taxe d'habitation résultant du plafonnement prévu à l'article 1414 A du même code ; que la réponse à ce moyen du ministre dépend de la réponse à la question de savoir si les dispositions du deuxième alinéa de l'article 12 du protocole sur les privilèges et immunités de l'Union européenne s'opposent à toute prise en compte, pour le calcul du revenu théorique d'un foyer fiscal, de la rémunération perçue par un fonctionnaire ou un agent de l'Union européenne membre de ce foyer fiscal, dès lors que cette prise en compte est susceptible d'exercer une influence sur le montant de l'imposition due par ce foyer fiscal, ou s'il y a lieu de continuer à tirer les conséquences de l'arrêt de la Cour du 14 octobre 1999 citée au point 3 dans les cas où la prise en compte d'une telle rémunération n'a pour objet, en vue de l'application éventuelle d'une mesure sociale tendant à l'exonération du paiement d'une taxe, à l'octroi d'un abattement sur son assiette ou, plus généralement, à un allègement d'imposition, que de vérifier si le revenu théorique du foyer fiscal est ou non inférieur au seuil défini par le droit fiscal national pour l'octroi du bénéfice - éventuellement modulé en fonction du revenu théorique - de cette mesure sociale ;<br/>
<br/>
              5. Considérant que cette question est déterminante pour la solution du litige que le Conseil d'Etat doit juger ; qu'elle présente une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi du ministre ;<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                   --------------<br/>
<br/>
 Article 1er : Il est sursis à statuer sur le pourvoi du ministre délégué, chargé du budget jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante : " Les dispositions du deuxième alinéa de l'article 12 du protocole sur les privilèges et immunités de l'Union européenne s'opposent-elles à toute prise en compte, pour le calcul du revenu théorique d'un foyer fiscal, de la rémunération perçue par un fonctionnaire ou autre agent de l'Union européenne, membre de ce foyer fiscal, dès lors que cette prise en compte est susceptible d'exercer une influence sur le montant de l'imposition due par ce foyer fiscal, ou y a-t-il lieu de continuer à tirer les conséquences de l'arrêt de la Cour du 14 octobre 1999 (affaire C-229/98) dans les cas où la prise en compte d'une telle rémunération n'a pour objet, en vue de l'application éventuelle d'une mesure sociale tendant à l'exonération du paiement d'une taxe, à l'octroi d'un abattement sur son assiette ou, plus généralement, à un allègement d'imposition, que de vérifier si le revenu théorique du foyer fiscal est ou non inférieur au seuil défini par le droit fiscal national pour l'octroi du bénéfice - éventuellement modulé en fonction du revenu théorique - de cette mesure sociale ' ".<br/>
<br/>
 Article 2 : La présente décision sera notifiée au ministre des finances et des comptes publics, à Mme A...B...et au président de la Cour de justice de l'Union européenne. <br/>
 Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
