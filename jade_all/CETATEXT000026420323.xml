<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026420323</ID>
<ANCIEN_ID>JG_L_2012_09_000000359389</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/42/03/CETATEXT000026420323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 26/09/2012, 359389</TITRE>
<DATE_DEC>2012-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359389</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:359389.20120926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 14 mai, 29 mai et 4 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le GIE " Groupement des poursuites extérieures ", dont le siège est 26 bis rue Kléber à Montreuil (93100) ; le GIE " Groupement des poursuites extérieures " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1202866 du 26 avril 2012 par laquelle le magistrat désigné par le président du tribunal administratif de Cergy-Pontoise, statuant en application de l'article L. 551-1 du code de justice administrative a, sur la demande de la SCP Sibran Cheenne Diebold Sibran-Vuillemin, d'une part, annulé la procédure engagée par la direction départementale des finances publiques des Hauts-de-Seine pour l'intervention des huissiers de justice en vue du recouvrement amiable des créances, amendes, condamnations pécuniaires et produits locaux pris en charge par les comptables de la direction générale des finances publiques et, d'autre part, enjoint à la direction départementale des finances publiques des Hauts-de-Seine, si elle entend poursuivre le projet de marché, de recommencer la procédure y afférente dans son intégralité ;<br/>
<br/>
              2°) de mettre à la charge de la SCP Sibran Cheenne Diebold Sibran-Vuillemin la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu l'ordonnance n° 45-2592 du 2 novembre 1945 ;<br/>
<br/>
              Vu la loi n° 66-879 du 29 novembre 1966 ;<br/>
<br/>
              Vu la loi n° 2004-1485 du 30 décembre 2004 et notamment son article 128 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Celice, Blancpain, Soltner, avocat du GIE " Groupement des poursuites extérieures " et de Me Le Prado, avocat de la SCP Sibran Cheenne Diebold Sibran-Vuillemin,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Celice, Blancpain, Soltner, avocat du GIE " Groupement des poursuites extérieures " et à Me Le Prado, avocat de la SCP Sibran Cheenne Diebold Sibran-Vuillemin ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...). / Le juge est saisi avant la conclusion du contrat " ; qu'aux termes de l'article L. 551-10 du même code: " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du référé précontractuel que, par un avis d'appel public à la concurrence publié le 1er février 2012, la direction départementale des finances publiques des Hauts-de-Seine a lancé un appel à candidatures pour la passation d'un marché, selon une procédure adaptée, ayant pour objet l'intervention d'huissiers ou de structures d'huissier de justice en vue du recouvrement amiable de créances, amendes, condamnations pécuniaires et produits locaux pris en charge par les comptables de la direction générale des finances publiques ; que l'offre présentée par le GIE " Groupement des poursuites extérieures " a été retenue ; que, par un courrier du 23 mars 2012, la SCM GTP 92 a été informée du rejet de son offre ; que la SCP Sibran Cheenne Diebold Sibran-Vuillemin, qui avait donné mandat à la SCM GTP 92 pour présenter une offre et la représenter, ainsi que sept autres sociétés civiles professionnelles, devant le pouvoir adjudicateur, a saisi le juge du référé précontractuel du tribunal administratif de Cergy-Pontoise sur le fondement des dispositions précitées de l'article L. 551-1 du code de justice administrative ; que, par l'ordonnance attaquée du 26 avril 2012, ce magistrat a annulé la procédure de passation ;<br/>
<br/>
              Sur la compétence du juge du référé précontractuel :<br/>
<br/>
              3. Considérant qu'aux termes du I. de l'article 128 de la loi n° 2004-1485 du 30 décembre 2004 : " Lorsque le comptable du Trésor public est autorisé par des dispositions législatives ou réglementaires à procéder au recouvrement forcé d'une créance ou d'une condamnation pécuniaire, il peut, préalablement à la mise en oeuvre de toute procédure coercitive, demander à un huissier de justice d'obtenir du débiteur ou du condamné qu'il s'acquitte entre ses mains du montant de sa dette ou de sa condamnation pécuniaire. / Les frais de recouvrement sont versés directement par le débiteur ou le condamné à l'huissier de justice. / Le montant des frais, qui restent acquis à l'huissier de justice, est calculé selon un taux proportionnel aux sommes recouvrées fixé par arrêté conjoint des ministres chargés des finances et de la justice (...) " ;<br/>
<br/>
              4. Considérant que le contrat envisagé a pour objet de confier à des huissiers de justice ou à des structures d'huissier de justice, sur le fondement des dispositions précitées de l'article 128 de la loi du 30 décembre 2004 de finances rectificative pour 2004, le soin de procéder, à la demande de comptables du Trésor public, au recouvrement amiable de créances ou de condamnations pécuniaires préalablement à la mise en oeuvre de toute procédure coercitive ; qu'alors même que le contrat envisagé ne se traduit par aucune dépense directe de l'Etat et que le cocontractant de l'administration est rémunéré non par l'Etat mais par le versement de frais de recouvrement mis à la charge du débiteur ou du condamné par les dispositions du deuxième alinéa du I de l'article 128 de la loi du 30 décembre 2004, ce contrat a pour objet l'exécution d'une prestation de service pour le compte de l'Etat avec une contrepartie économique constituée par un prix ; que, par suite, le juge des référés n'a pas commis d'erreur de droit en jugeant que le contrat litigieux entrait dans le champ d'application des dispositions précitées de l'article <br/>
L. 551-1 du code de justice administrative ;<br/>
<br/>
              Sur l'intérêt de la SCP Sibran Cheenne Diebold Sibran-Vuillemin à saisir le juge du référé précontractuel :<br/>
<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 551-10 du code de justice administrative : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ; qu'aux termes de l'article 1er de l'ordonnance du 2 novembre 1945 relative au statut des huissiers : " Les huissiers de justice peuvent (...) procéder au recouvrement amiable (...) de toutes créances " ; qu'aux termes de l'article 36 de la loi du 29 novembre 1966 relative aux sociétés civiles professionnelles : " Nonobstant toutes dispositions législatives ou réglementaires contraires, les personnes physiques ou morales exerçant des professions libérales et notamment les officiers publics et ministériels, peuvent constituer entre elles des sociétés civiles ayant pour objet exclusif de faciliter à chacun de leurs membres l'exercice de son activité. / A cet effet, les associés mettent en commun les moyens utiles à l'exercice de leurs professions, sans que la société puisse elle-même exercer celle-ci. " ; qu'il résulte de ces dispositions que si une société civile de moyens ainsi constituée entre plusieurs personnes physiques ou morales exerçant des professions libérales peut se porter candidate à l'obtention d'une commande publique pour le compte de ses associés, seuls ceux-ci pourront exécuter les prestations objet du contrat ; qu'ainsi, la circonstance que la société civile de moyens GTP 92 ait répondu à l'avis d'appel public à la concurrence lancé par la direction générale des finances ne privait pas la SCP Sibran Cheenne Diebold Sibran-Vuillemin, titulaire d'un office d'huissier de justice et associée au sein de la société civile de moyens GTP 92, de son intérêt, au sens des dispositions précitées de l'article L. 551-10 du code de justice administrative, à saisir le juge du référé précontractuel ; que, par suite, en jugeant recevable la demande de la SCP Sibran Cheenne Diebold Sibran-Vuillemin, le juge des référés n'a pas commis d'erreur de droit ; <br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              6. Considérant que, pour assurer le respect des principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, l'information appropriée des candidats sur les critères d'attribution d'un marché public est nécessaire, dès l'engagement de la procédure d'attribution du marché, dans l'avis d'appel public à concurrence ou le cahier des charges tenu à la disposition des candidats ; que dans le cas où le pouvoir adjudicateur souhaite retenir d'autres critères que celui du prix, l'information appropriée des candidats doit alors porter également sur les conditions de mise en oeuvre de ces critères ; qu'il appartient au pouvoir adjudicateur d'indiquer les critères d'attribution du marché et les conditions de leur mise en oeuvre selon les modalités appropriées à l'objet, aux caractéristiques et au montant du marché concerné ; <br/>
<br/>
              7. Considérant qu'il ressort des pièces soumises au juge du référé précontractuel que l'article 8 du règlement de la consultation du marché tenu à la disposition des candidats prévoit que : " (...) Les offres seront classées suivant les critères suivants : / 1) engagement et justification de l'huissier ou de la structure à respecter les dispositions des cahiers des charges en matière d'échanges dématérialisés ; / 2) dimensionnement de l'étude (nombre de collaborateurs dédiés au recouvrement des créances prises en charge par les comptables de la DGFIP, nombre de donneurs d'ordre pour lesquels l'étude travaille déjà, plages horaires de réception des appels en provenance des services de la DGFIP, capacité maximale de traitement par mois (en nombre d'actes ou titres) ; 3) moyens techniques dont dispose l'huissier ou la structure à la réalisation de la phase comminatoire (envois de courriers, messages, relances téléphoniques, déplacement...) / 4) fréquence et stratégie d'utilisation des moyens techniques pour le traitement d'un dossier donné (nombre rythme et nature des relances...) " ; <br/>
<br/>
              8. Considérant que le pouvoir adjudicateur ayant souhaité, ainsi qu'il ressort des pièces du dossier soumis au juge des référés, retenir d'autres critères que celui du prix, l'information appropriée des candidats à l'attribution de ce marché public devait également porter non seulement sur les critères de sélection des offres mais également sur les conditions de leur mise en oeuvre ; que s'il est loisible au pouvoir adjudicateur, lorsqu'il passe un marché selon une procédure adaptée en application de l'article 28 du code des marchés publics, de pondérer ou de hiérarchiser les critères de sélection qu'il retient, y compris en leur attribuant une égale importance, il est tenu d'informer les candidats de son choix de mise en oeuvre des critères de sélection ; que, par suite, le juge du référé précontractuel du tribunal administratif de <br/>
Cergy-Pontoise n'a pas commis d'erreur de droit en estimant que le pouvoir adjudicateur avait manqué à son obligation d'indiquer avec une précision suffisante l'importance respective des critères d'attribution de ce marché, dès lors que ces critères avaient été énumérés sans aucune indication quant à leur pondération ou à leur hiérarchisation ; <br/>
<br/>
              9. Considérant qu'il ressort du règlement de consultation précité soumis au juge du référé précontractuel que les quatre critères d'attribution du marché sont numérotés sans autres précisions ; que devant le juge du référé, le directeur départemental des finances publiques des Hauts-de-Seine soutenait que ces critères étaient hiérarchisés par ordre d'importance ; qu'en estimant que ce règlement de consultation ne pouvait être regardé comme ayant énoncé clairement que les critères étaient hiérarchisés par ordre décroissant et qu'ainsi, le pouvoir adjudicateur n'avait pas apporté aux candidats une information appropriée sur les conditions de mise en oeuvre des critères d'attribution de ce marché, le juge du référé précontractuel s'est livré à une appréciation souveraine, exempte de dénaturation ;<br/>
<br/>
              10. Considérant qu'en application des dispositions des articles L. 551-1 et L. 551-10 du code de justice administrative, il appartient au juge du référé précontractuel de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auxquels ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de façon indirecte en avantageant une entreprise concurrente ; qu'il ressort des pièces soumises au juge du référé que la SCP Sibran Cheenne Diebold Sibran-Vuillemin avait donné mandat à la SCM GTP 92 pour présenter une offre en son nom et celui de sept autres sociétés civiles professionnelles ; qu'alors même que tous les candidats à cette offre ont pu être affectés par l'incertitude tenant aux conditions de mise en oeuvre des critères, ce manquement a été susceptible de défavoriser l'offre présentée par la SCM GTP 92, qui aurait pu être modifiée si le pouvoir adjudicateur avait clairement précisé ses intentions ; que, par suite, en estimant, par l'ordonnance attaquée, qui est suffisamment motivée sur ce point, que l'incertitude relevée constituait, pour le pouvoir adjudicateur, un manquement à ses obligations de publicité et de mise en concurrence susceptible, eu égard à sa portée et au stade de la procédure auquel il se rapporte, de léser la SCP Sibran Cheenne Diebold Sibran-Vuillemin, le juge du référé précontractuel de Cergy-Pontoise  n'a pas inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le GIE " Groupement des poursuites extérieures " n'est pas fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant, qu'il y a lieu de mettre à la charge du GIE " Groupement des poursuites extérieures " une somme de 3 000 euros au bénéfice de la SCP Sibran Cheenne Diebold Sibran-Vuillemin au titre des dispositions susvisées ; qu'en revanche, ces dispositions font obstacle à ce que la somme demandée par le GIE " Groupement des poursuites extérieures " soit mise à la charge de la SCP Sibran Cheenne Diebold Sibran-Vuillemin qui n'est pas partie perdante dans la présente instance ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du GIE " Groupement des poursuites extérieures " est rejeté.<br/>
Article 2 : Le GIE " Groupement des poursuites extérieures " versera à la SCP Sibran Cheenne Diebold Sibran-Vuillemin la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée au GIE " Groupement des poursuites extérieures ", à la SCP Sibran Cheenne Diebold Sibran-Vuillemin, à la chambre nationale des huissiers de justice et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-03 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. HUISSIERS DE JUSTICE. - CONTRAT AYANT POUR OBJET DE CONFIER DES PRESTATIONS DE RECOUVREMENT DE CRÉANCES À DES HUISSIERS - RÉMUNÉRATION ASSURÉE NON PAR L'ETAT MAIS PAR LE VERSEMENT DE FRAIS DE RECOUVREMENT - QUALIFICATION - MARCHÉ DE SERVICES AYANT UNE CONTREPARTIE ÉCONOMIQUE SOUS FORME DE PRIX - CONSÉQUENCE - CHAMP D'APPLICATION DU RÉFÉRÉ PRÉCONTRACTUEL (ART. L. 551-1 DU CJA) - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - CONTRAT AYANT POUR OBJET DE CONFIER DES PRESTATIONS DE RECOUVREMENT DE CRÉANCES À DES HUISSIERS - 1) RÉMUNÉRATION ASSURÉE NON PAR L'ETAT MAIS PAR LE VERSEMENT DE FRAIS DE RECOUVREMENT - QUALIFICATION - MARCHÉ DE SERVICES AYANT UNE CONTREPARTIE ÉCONOMIQUE SOUS FORME DE PRIX - CONSÉQUENCE - CHAMP D'APPLICATION DU RÉFÉRÉ PRÉCONTRACTUEL (ART. L. 551-1 DU CJA) - EXISTENCE - 2) INTÉRÊT POUR SAISIR LE JUGE DU RÉFÉRÉ PRÉCONTRACTUEL (ART. L. 551-10 DU CJA) - CAS D'UNE SCP TITULAIRE D'UN OFFICE ASSOCIÉE D'UNE SOCIÉTÉ CIVILE DE MOYENS AYANT CANDIDATÉ À UN MARCHÉ - INTÉRÊT DE CETTE SCP À FORMER UN RÉFÉRÉ PRÉCONTRACTUEL - EXISTENCE.
</SCT>
<ANA ID="9A"> 37-04-03 Un contrat ayant pour objet de confier à des huissiers de justice ou à des structures d'huissier de justice, sur le fondement des dispositions de l'article 128 de la loi n° 2004-1485 du 30 décembre 2004 de finances rectificative pour 2004, le soin de procéder, à la demande de comptables du Trésor public, au recouvrement amiable de créances ou de condamnations pécuniaires préalablement à la mise en oeuvre de toute procédure coercitive, alors même qu'il ne se traduit par aucune dépense directe de l'Etat et que le cocontractant de l'administration est rémunéré non par l'Etat mais par le versement de frais de recouvrement mis à la charge du débiteur ou du condamné, a pour objet l'exécution d'une prestation de service pour le compte de l'Etat avec une contrepartie économique constituée par un prix. Par suite, il entre dans le champ d'application de l'article L. 551-1 du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 39-08-015-01 1) Un contrat ayant pour objet de confier à des huissiers de justice ou à des structures d'huissiers de justice, sur le fondement des dispositions de l'article 128 de la loi n° 2004-1485 du 30 décembre 2004 de finances rectificative pour 2004, le soin de procéder, à la demande de comptables du Trésor public, au recouvrement amiable de créances ou de condamnations pécuniaires préalablement à la mise en oeuvre de toute procédure coercitive, alors même qu'il ne se traduit par aucune dépense directe de l'Etat et que le cocontractant de l'administration est rémunéré non par l'Etat mais par le versement de frais de recouvrement mis à la charge du débiteur ou du condamné, a pour objet l'exécution d'une prestation de service pour le compte de l'Etat avec une contrepartie économique constituée par un prix. Par suite, il entre dans le champ d'application de l'article L. 551-1 du code de justice administrative (CJA).,,2) Si une société civile de moyens constituée entre plusieurs personnes physiques ou morales exerçant des professions libérales peut se porter candidate à l'obtention d'une commande publique pour le compte de ses associés, seuls ceux-ci pourront exécuter les prestations objet du contrat. Ainsi, une SCP titulaire d'un office d'huissier de justice et associée au sein d'une société civile de moyens ayant candidaté à un marché a intérêt, au sens des dispositions précitées de l'article L. 551-10 du CJA, à saisir le juge du référé précontractuel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
