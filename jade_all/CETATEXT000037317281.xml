<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037317281</ID>
<ANCIEN_ID>JG_L_2018_08_000000412663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/31/72/CETATEXT000037317281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/08/2018, 412663, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412663.20180816</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société NSHHD a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir l'arrêté du 16 août 2016 par lequel le maire de La Teste-de-Buch a, à la demande de MmeA..., retiré le permis de construire et le permis modificatif qu'il lui avait délivrés respectivement le 15 avril et le 24 novembre 2014 pour la réalisation d'une maison et d'une piscine. Par un jugement nos 1504208, 1600176 et 1604527 du 15 juin 2017, le tribunal administratif de Bordeaux a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 juillet et 23 octobre 2017 et 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société NSHHD demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler l'arrêté du maire de La Teste-de-Buch du 16 août 2016 ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...et de la commune de La Teste-de-Buch la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
Vu : <br/>
- le code des relations entre le public et l'administration ;<br/>
- le code de l'urbanisme ;<br/>
- le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de la société NSHHD , à la SCP Piwnica, Molinié, avocat de la commune de La Teste-de-Buch et à la SCP Célice, Soltner, Texidor, Perier, avocat de MmeA....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir obtenu le 24 mars 2014 un permis de démolir concernant une maison existante dénommée " la Villa Gaume ", la société NSHHD a été bénéficiaire d'un permis de construire pour la construction d'une maison et d'une piscine délivré par le maire de La Teste-de-Buch le 15 avril 2014, puis d'un permis de construire modificatif concernant la façade et la hauteur de la maison ainsi que les dimensions de la piscine, délivré le 24 novembre 2014. Par arrêté du 16 août 2016, le maire a cependant retiré ces deux permis. La société NSHHD a demandé au tribunal administratif de Bordeaux l'annulation pour excès de pouvoir de cet arrêté. Par un jugement du 15 juin 2017 rendu en premier et dernier ressort contre lequel la société NSHHD se pourvoit en cassation, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2.	Un permis ne peut faire l'objet d'un retrait, une fois devenu définitif, qu'au vu d'éléments, dont l'administration a connaissance postérieurement à la délivrance du permis, établissant l'existence d'une fraude à la date où il a été délivré. La caractérisation de la fraude résulte de ce que le pétitionnaire a procédé de manière intentionnelle à des manoeuvres de nature à tromper l'administration sur la réalité du projet dans le but d'échapper à l'application d'une règle d'urbanisme. Une information erronée ne peut, à elle seule, faire regarder le pétitionnaire comme s'étant livré à l'occasion du dépôt de sa demande à des manoeuvres destinées à tromper l'administration. <br/>
<br/>
              3.	En premier lieu, après avoir rappelé ces principes, le tribunal administratif a relevé que les plans de coupe joints au dossier de demande des permis litigieux représentaient de façon erronée le terrain d'assiette du projet comme étant plat. S'il a par ailleurs relevé qu'il ressort du plan de coupe de la construction existante et du permis de construire délivré en 1964 pour son édification que le terrain était en pente, il a pu sans erreur de droit considérer que les informations erronées du dossier de demande étaient de nature à caractériser une fraude dès lors que les autres éléments du dossier ne permettaient pas d'établir la déclivité du terrain d'assiette au niveau de la construction autorisée, la réalité de celle-ci étant confirmé, comme il le relève, par le relevé topographique produit par devant lui.<br/>
<br/>
              4.	En deuxième lieu, si la société NSHHD fait valoir qu'elle a repris le plan de coupe figurant dans le dossier de demande présenté par le précédent propriétaire, et ayant donné lieu à un permis de construire délivré en 2013, le tribunal administratif n'a pas commis d'erreur de droit, ni dénaturé les pièces du dossier en estimant, sans insuffisance de motivation, que le pétitionnaire ne pouvait ignorer la déclivité du terrain et omettre de le signaler dans ses demandes de permis de construire et de permis modificatif réalisées par une agence d'architectes.<br/>
<br/>
              5.	En troisième lieu, aux termes de l'article 10 du règlement du plan local d'urbanisme de la commune de La Teste-de-Buch  définissant la " hauteur absolue (prise au faîtage) par rapport au sol naturel avant  travaux " : " La hauteur maximale des constructions ne peut excéder 8 mètres. " Il ressort des pièces du dossier soumis au juge du fond que le niveau du terrain naturel mesuré de chaque côté du terrain au droit de la façade ouest de la construction, tel qu'il est mesuré sur le relevé topographique, varie des côtes - 5,35 à - 3,38 mètres tandis que le point de niveau du sous-sol de la construction est à la côte - 3,98 mètres. Le point haut de l'acrotère supérieur de la construction se situe pour sa part au niveau + 6,2 mètres. Le tribunal n'a, par suite, pas dénaturé les pièces du dossier en retenant que le niveau du sol naturel avant travaux se situait, à cet endroit de la construction, au niveau de la dalle du sous-sol de la maison détruite, qui correspond au sous-sol de la nouvelle construction, pour en déduire que le projet méconnaissait l'article 10 du règlement de la zone UPA du plan local d'urbanisme.<br/>
<br/>
              6.	En dernier lieu, si la société soutient que le retrait d'un acte obtenu par fraude devrait être soumis à un délai raisonnable d'un an au nom du principe de sécurité juridique, un tel moyen doit être écarté, un acte obtenu par fraude pouvant être légalement retiré à tout moment. <br/>
<br/>
              7.	Il résulte de tout ce qui précède que le pourvoi de la société NSHHD doit être rejeté.<br/>
<br/>
              8.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de La Teste-de-Buch et de Mme A...qui ne sont pas, dans la présente instance, les parties perdantes. Il y a lieu, dans les circonstances de l'espèce de mettre à la charge de la société NSHHD la somme globale de 3 000 euros à verser à la commune de La Teste-de-Buch et à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société NSHHD est rejeté.<br/>
Article 2 : La société NSHHD versera à la commune de La Teste-de-Buch et à Mme A...une somme de 1 500 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société NSHHD, à Mme B...A...et à la commune de La Teste-de-Buch.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
