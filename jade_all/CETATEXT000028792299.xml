<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028792299</ID>
<ANCIEN_ID>JG_L_2014_03_000000360344</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/79/22/CETATEXT000028792299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 28/03/2014, 360344, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360344</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2014:360344.20140328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 19 juin et 19 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le président de l'Autorité des marchés financiers, dont le siège est 17, place de la Bourse à Paris Cedex 2 (75082) ; le président de l'Autorité des marchés financiers demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 19 avril 2012 par laquelle la commission des sanctions de l'Autorité des marchés financiers a constaté qu'étaient atteints par la prescription les faits objet de la poursuite engagée à l'encontre des caisses d'épargne et de prévoyance Ile-de-France, Normandie, Provence-Alpes-Corse et Loire-Drôme-Ardèche ainsi qu'à l'encontre de la société Natixis Asset Management, venant aux droits de la société Ecureuil Gestion ; <br/>
<br/>
              2°) de prononcer, à l'encontre, respectivement, de la caisse d'épargne et de prévoyance (CEP) Ile-de-France, de la CEP Normandie, de la CEP Provence-Alpes-Corse, de la CEP Loire-Drôme-Ardèche et de la société Natixis Asset Management, des sanctions pécuniaires d'un montant de 1,5 million d'euros, 1,1 million d'euros, 1,3 million d'euros, 1 million d'euros et 1,5 million d'euros ; <br/>
<br/>
              3°) d'ordonner la publication de la présente décision sur le site internet de l'Autorité des marchés financiers, sous une forme préservant l'anonymat de la société Natixis Asset Management ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code monétaire et financier ; <br/>
<br/>
              Vu le règlement n° 89-02 de la Commission des opérations de bourse relatif aux organismes de placement collectif en valeurs mobilières ; <br/>
<br/>
              Vu le règlement n° 96-03 de la Commission des opérations de bourse relatif aux règles de bonne conduite ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Vincent, Ohl, avocat du président de l'Autorité des marchés financiers et à la SCP Piwnica, Molinié, avocat des caisses d'épargne et de prévoyance Ile-de-France, Normandie, Provence-Alpes-Corse et Loire-Drôme-Ardèche et de la société Natixis-Asset Management ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction qu'à compter du 7 juin 2001, les caisses d'épargne et de prévoyance (CEP) Ile-de-France, Normandie, Provence-Alpes-Corse et Loire-Drôme-Ardèche ont commercialisé les fonds structurés à formule, dits " à promesse " ou " garantis ", des gammes " Doubl'Ô " et " Doubl'Ô Monde ", conçus par la société Ecureuil Gestion et agréés par la Commission des opérations de bourse, conformément à l'article 6 du règlement COB n° 89-02 et à l'instruction du 15 décembre 1998 relative aux organismes de placement collectif en valeurs mobilières (OPCVM) ; que la période de commercialisation de ces fonds s'est achevée le 25 avril 2002 ; <br/>
<br/>
              2. Considérant que chacun des fonds à formule des gammes " Doubl'Ô " et " Doubl'Ô Monde " avait pour principales caractéristiques de garantir le capital à l'échéance et de viser à bénéficier de la croissance d'actions internationales dans des secteurs d'activités diversifiés ; que ces fonds comprenaient, dans une proportion d'au moins 75 %, des actions européennes ou titres assimilés ; que, pour chaque fonds, était constitué un panier de référence de douze actions internationales à grosse capitalisation, avec des zones géographiques et des secteurs d'activité diversifiés, dont l'évolution permettait de déterminer la réalisation de différentes hypothèses de performance ; que, destinés à une clientèle " grand public ", ces fonds offraient aux souscripteurs la garantie, à l'échéance de la période de placement, du montant en capital souscrit, minoré du montant des droits d'entrée, fixé à 1 % pour la gamme " Doubl'Ô " et 2 % pour la gamme " Doubl'Ô Monde ", à condition que le client ait souscrit au fonds dans la période de réservation et ait conservé son investissement durant six ans ; qu'à défaut, s'appliquaient des commissions de souscription de 6 % pour la gamme " Doubl'Ô " et 4 % pour la gamme " Doubl'Ô Monde ", ainsi que des commissions de rachat ; qu'en outre, dans l'hypothèse où à l'une des huit dates de constatation trimestrielles prédéfinies, la première intervenant quatre ans et trois mois après la première date de valorisation du fonds, aucune des douze actions constituant le panier de référence n'enregistrait une baisse supérieure ou égale à 40 % par rapport à son cours lors de la première date de valorisation, le porteur recevait, à l'échéance, déduction faite du montant des droits d'entrée, le meilleur remboursement entre, d'une part, 200 % de son investissement initial, soit un rendement annuel de 12,5 %, d'autre part, 100 % de son investissement initial, multiplié par l'évolution du panier calculée à l'échéance ; que, dans l'hypothèse contraire où l'une des douze actions constituant le panier de référence enregistrait, à l'une des huit dates de constatation trimestrielles, une baisse supérieure ou égale à 40 % par rapport à son cours lors de la première date de valorisation, entrait en jeu le mécanisme dit de " barrière désactivante ", qui conduisait le porteur à recevoir, à l'échéance, déduction faite du montant des droits d'entrée, le meilleur remboursement entre, d'une part, 100 % de son investissement initial, majoré le cas échéant d'un coupon qui progressait de 12,5 % par trimestre échu à compter de la première date de constatation trimestrielle et ce jusqu'à la date de constatation où il était observé qu'une action du panier avait enregistré une baisse d'au moins 40 %, d'autre part, 100 % de son investissement initial, multiplié par un pourcentage de l'évolution du panier calculée à l'échéance, d'un montant de 60 % à la date de la première constatation et augmentant de 5 % à chacune des dates de constatation suivantes jusqu'à 95 % ; que le dispositif commercial de grande ampleur établi au plan national par la société Ecureuil Gestion a permis de collecter sur ces fonds un total de 2,49 milliards d'euros auprès de 240 000 souscripteurs, 99 % d'entre eux étant des personnes physiques, et de prélever chaque année des " frais de gestion " s'élevant à 1,7 % du capital initialement investi ;<br/>
<br/>
              3. Considérant qu'en vertu de la loi du 1er août 2003 de sécurité financière, l'Autorité des marchés financiers a succédé à la Commission des opérations de bourse ; que le secrétaire général adjoint de cette autorité, après avoir été saisi de réclamations de porteurs sur l'incohérence entre les montants récupérés à l'échéance des fonds et le message publicitaire qui accompagnait leur commercialisation, a ouvert, le 30 octobre 2008, sept procédures de contrôle portant sur le respect, par la société Ecureuil Gestion, devenue Natixis Epargne Financière par changement de dénomination, et par les quatre caisses d'épargne et de prévoyance déjà mentionnées de leurs " obligations professionnelles, notamment en matière de commercialisation des fonds de placement des gammes " Doubl'Ô " et " Doubl'Ô Monde " " ; qu'au vu du rapport de contrôle et sur décision du collège, le président de l'Autorité des marchés financiers a, par lettres recommandées avec avis de réception du 22 mars 2011, notifié à ces sociétés des griefs tirés de la violation des articles 33 et 33 bis du règlement n° 89-02 de la Commission des opérations de bourse relatif aux organismes de placement collectif en valeurs mobilières, applicable à l'époque des faits, ainsi que de l'article 24 du règlement n° 96-03 de la COB relatif aux règles de bonne conduite, pour avoir communiqué au public des documents publicitaires ne paraissant pas cohérents avec l'investissement proposé ; que par une décision du 19 avril 2012, la commission des sanctions de l'Autorité des marchés financiers a estimé que les faits à l'origine des poursuites étaient atteints par la prescription ; que le président de l'Autorité des marchés financiers demande l'annulation de cette décision ainsi que le prononcé de sanctions proportionnées aux manquements commis par les sociétés mises en cause ; <br/>
<br/>
              4. Considérant, d'une part, qu'aux termes de L. 533-4 du code monétaire et financier, dans sa rédaction applicable à la date de la commercialisation des fonds des gammes " Doubl'Ô " et " Doubl'Ô Monde " : " Les prestataires de services d'investissement et les personnes mentionnées à l'article L. 421-8 ainsi que les personnes mentionnées à l'article L. 214-83-1, sont tenus de respecter des règles de bonne conduite destinées à garantir la protection des investisseurs et la régularité des opérations. / (...) / Elles obligent notamment à : / 1. Se comporter avec loyauté et agir avec équité au mieux des intérêts de leurs clients et de l'intégrité du marché ; / (...) / 4. S'enquérir de la situation financière de leurs clients, de leur expérience en matière d'investissement et de leurs objectifs en ce qui concerne les services demandés ; / 5. Communiquer, d'une manière appropriée, les informations utiles dans le cadre des négociations avec leurs clients ; / (...) " ; que l'article 33 du règlement n° 89-02 de la COB relatif aux organismes de placement collectif en valeurs mobilières, dans sa rédaction applicable à la date de commercialisation des fonds dispose : " La publicité concernant des OPCVM ou des compartiments doit être cohérente avec l'investissement proposé et mentionner, le cas échéant, les caractéristiques moins favorables et les risques inhérents aux options qui peuvent être le corollaire des avantages énoncés. " ; qu'aux termes de l'article 33 bis du même règlement : " les informations utiles lui [le client] sont communiquées afin de lui permettre de prendre une décision d'investissement ou de désinvestissement en toute connaissance de cause. La personne qui commercialise les parts ou actions d'OPCVM ou de compartiments met, le cas échéant, en garde contre les risques encourus " ; qu'aux termes de l'article 24 du règlement n° 96-03 relatif aux règles de bonne conduite applicables au service de gestion de portefeuille pour le compte de tiers, dans sa rédaction applicable à l'espèce : " la publicité doit être cohérente avec les services proposés et mentionner, le cas échéant, les caractéristiques les moins favorables et les risques inhérents aux options qui peuvent être le corollaire des avantages énoncés " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'aux termes du deuxième alinéa du I de l'article L. 621-15 du code monétaire et financier, issu de l'article 14 de la loi du 1er  août 2003 : " La commission des sanctions ne peut être saisie de faits remontant à plus de trois ans s'il n'a été fait pendant ce délai aucun acte tendant à leur recherche, à leur constatation ou à leur sanction. " ; que ces dispositions, qui instituent, pour l'exercice du pouvoir de sanction de l'Autorité des marchés financiers, une règle de prescription, sont immédiatement applicables à compter de leur entrée en vigueur, le 2 août 2003 ; que le délai de trois ans qu'elles prévoient a commencé à courir à cette date pour les faits antérieurs à la publication de la loi du 1er août 2003 ; <br/>
<br/>
              6. Considérant que l'Autorité des marchés financiers a pour mission, en vertu de l'article L. 621-1 du code monétaire et financier, de veiller à la protection de l'épargne investie dans les instruments financiers, à l'information des investisseurs et au bon fonctionnement des marchés financiers ; qu'il résulte de la combinaison des dispositions citées aux points 4 et 5 que lorsque sont en cause des manquements aux obligations professionnelles relatives à la cohérence, avec les caractéristiques de l'investissement proposé, de l'information délivrée au public dans les documents accompagnant la commercialisation de produits financiers, le point de départ du délai de prescription doit être fixé au jour où le manquement est apparu et a pu être constaté dans des conditions permettant l'exercice, par l'Autorité des marchés financiers, de ses missions de contrôle, notamment en vue de l'ouverture d'une procédure de sanction ; <br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que les fonds des gammes " Doubl'Ô " et " Doubl'Ô Monde " ont été agréés par la Commission des opérations de bourse entre le 3 mai 2001 et le 12 mars 2002 sur la base de leurs notices d'information ; que celles-ci faisaient apparaître que ces fonds étaient destinés à une clientèle " grand public " et ce alors que la formule de rémunération du capital investi dans les fonds était particulièrement complexe ; que ces fonds ont été commercialisés dans le réseau des caisses d'épargne et ont fait l'objet d'une promotion commerciale de grande ampleur ; que si, à la date d'agrément des fonds, aucune disposition n'imposait de joindre à la demande d'agrément les supports utilisés pour leur commercialisation, la Commission des opérations de bourse pouvait, le cas échéant, en exiger la production, à ce stade ou ultérieurement, s'il apparaissait des éléments justifiant une telle communication ; que, dans les circonstances particulières de l'espèce, eu égard aux caractéristiques des fonds, des souscripteurs visés, du réseau de commercialisation et des moyens de communication publicitaire utilisés, qui justifiaient une vigilance particulière de la Commission des opérations de bourse puis de l'Autorité des marchés financiers dans l'exercice de leur mission de contrôle, les manquements allégués ne peuvent être regardés comme ayant été dissimulés à l'égard de ces autorités au-delà de la fin de la période de commercialisation des fonds qui, ainsi qu'il a été dit au point 1, s'est achevée le 25 avril 2002 ; que le délai de prescription de trois ans, qui avait commencé à courir le 2 août 2003, était en conséquence expiré lorsqu'a été accompli, par l'ouverture le 30 octobre 2008 des procédures de contrôle, le premier acte tendant à la recherche et à la constatation des faits imputés aux sociétés en cause ; que c'est par suite à bon droit que la commission des sanctions a estimé que les faits dont elle était saisie étaient prescrits ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le président de l'Autorité des marchés financiers n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que sa requête doit, par suite, être rejetée ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête du président de l'Autorité des marchés financiers est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée au président de l'Autorité des marchés financiers et à la société Natixis Asset Management, premier défendeur dénommé. Les autres défendeurs seront informés de la présente décision par la SCP Piwnica, Molinié, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - COMMISSION DES SANCTIONS - EXERCICE DU POUVOIR DE SANCTION - DÉLAI DE PRESCRIPTION DE TROIS ANS (I DE L'ART. L. 621-15 DU CMF) [RJ1] - 1) APPLICATION DANS LE TEMPS - 2) POINT DE DÉPART - CAS DES MANQUEMENTS AUX OBLIGATIONS PROFESSIONNELLES RELATIVES À LA COHÉRENCE DE L'INFORMATION DÉLIVRÉE AU PUBLIC SUR LES PRODUITS FINANCIERS - JOUR OÙ LE MANQUEMENT EST APPARU ET A PU ÊTRE CONSTATÉ DANS DES CONDITIONS PERMETTANT L'EXERCICE PAR L'AMF DE SES MISSIONS DE CONTRÔLE - 3) ESPÈCE.
</SCT>
<ANA ID="9A"> 13-01-02-01 1) Les dispositions du deuxième alinéa du I de l'article L. 621-15 du code monétaire et financier (CMF) issues de l'article 14 de la loi n° 2003-706 du 1er août 2003, qui instituent, pour l'exercice du pouvoir de sanction de l'Autorité des marchés financiers (AMF), une règle de prescription, sont immédiatement applicables à compter de leur entrée en vigueur, le 2 août 2003. Le délai de trois ans qu'elles prévoient a commencé à courir à cette date pour les faits antérieurs à la publication de la loi du 1er août 2003.,,,2) Lorsque sont en cause des manquements aux obligations professionnelles relatives à la cohérence, avec les caractéristiques de l'investissement proposé, de l'information délivrée au public dans les documents accompagnant la commercialisation de produits financiers, le point de départ du délai de prescription doit être fixé au jour où le manquement est apparu et a pu être constaté dans des conditions permettant l'exercice, par l'AMF, de ses missions de contrôle, notamment en vue de l'ouverture d'une procédure de sanction.,,,3) Procédure de contrôle débouchant sur la notification de griefs tirés de manquements aux obligations professionnelles mentionnées au 2, en raison de l'utilisation massive, pour la commercialisation dans le réseau des caisses d'épargne de fonds destinés à une clientèle grand public, de supports promotionnels trompeurs quant à la formule de rémunération du capital investi dans ces fonds. Si, à la date d'agrément de ces fonds, aucune disposition n'imposait de joindre à la demande d'agrément les supports utilisés pour leur commercialisation, l'autorité de contrôle pouvait, le cas échéant, en exiger la production à ce stade ou ultérieurement, s'il apparaissait des éléments justifiant une telle communication. Dans les circonstances particulières de l'espèce, eu égard aux caractéristiques des fonds, des souscripteurs visés, du réseau de commercialisation et des moyens de communication publicitaire utilisés, qui justifiaient une vigilance particulière dans l'exercice des missions de contrôle, les manquements ne pouvaient être regardés comme ayant été dissimulés à l'égard des autorités de contrôle au-delà de la fin de la période de commercialisation, antérieure au 2 août 2003. Le délai de prescription de trois ans a donc commencé à courir à compter de cette date.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 29 mars 2010, Piard et autres, n°s 323354 323488 323491 324395, T. p. 648.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
