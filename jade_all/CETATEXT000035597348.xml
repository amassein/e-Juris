<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035597348</ID>
<ANCIEN_ID>JG_L_2017_09_000000392510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/59/73/CETATEXT000035597348.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/09/2017, 392510, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392510.20170920</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Norma a demandé au tribunal administratif de Paris de prononcer la décharge de l'amende qui lui a été infligée au titre de l'année 2009, pour un montant de 175 326 euros, sur le fondement de l'article 1736 du code général des impôts. Par un jugement n° 1314361 du 2 juillet 2014, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA03849 du 9 juillet 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Norma contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 10 août et 10 novembre 2015 et 15 février 2017 au secrétariat du contentieux du Conseil d'Etat, la société Norma demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Norma ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Norma a fait l'objet d'une vérification de comptabilité à l'issue de laquelle elle s'est vue infliger, au titre de l'année 2009, une amende de 175 326 euros sur le fondement de l'article 1736 du code général des impôts. La société se pourvoit en cassation contre l'arrêt du 9 juillet 2015 par lequel la cour administrative d'appel de Paris a rejeté son appel contre un jugement du tribunal administratif de Paris du 2 juillet 2014 qui a rejeté sa demande tendant à la décharge de cette amende.<br/>
<br/>
              2. Aux termes de l'article 1736 du code général des impôts : " I. - 1. Entraîne l'application d'une amende égale à 50 % des sommes non déclarées le fait de ne pas se conformer aux obligations prévues (...) au 1 de l'article 242 ter (...). ". Aux termes de l'article 242 ter du même code, dans sa rédaction applicable : " Les personnes qui assurent le paiement des revenus de capitaux mobiliers visés aux articles 108 à 125 ainsi que des produits des bons ou contrats de capitalisation et placements de même nature sont tenues de déclarer l'identité et l'adresse des bénéficiaires ainsi que, par nature de revenus, le détail du montant imposable et du crédit d'impôt, le revenu brut soumis à un prélèvement libératoire et le montant dudit prélèvement et le montant des revenus exonérés. (...). ". Aux termes de l'article 124 du même code : " Sont considérés comme revenus au sens du présent article, lorsqu'ils ne figurent pas dans les recettes provenant de l'exercice d'une profession industrielle, commerciale, artisanale ou agricole, ou d'une exploitation minière, les intérêts, arrérages, primes de remboursement et tous autres produits : / (...) 2° Des dépôts de sommes d'argent à vue ou à échéance fixe, quel que soit le dépositaire et quelle que soit l'affectation du dépôt. ". Aux termes de l'article 75 à l'annexe II à ce même code : " Sont regardés comme établissements payeurs : / (...) 4° Les sociétés visés à l'article 8 du code général des impôts, pour les revenus définis au 4 de l'article 79. ". Aux termes du 4 de l'article 79 de la même annexe à ce code : " Les sociétés visées au 4° de l'article 75 sont réputées verser à chacun de leurs associés la quote-part des revenus correspondant à ses droits, le jour où elles ont elles-mêmes encaissé lesdits revenus ou ont été créditées de leur montant ". Aux termes de l'article 41 duodecies A de l'annexe III à ce même code : " Les personnes, sociétés et organismes définis à l'article 75 de l'annexe II au code général des impôts, dénommés ci-après " établissements payeurs " qui payent, à quelque titre que ce soit, des intérêts, arrérages et autres produits visés au I de l'article 125 A du code précité doivent se conformer aux dispositions des articles 41 duodecies B à 41 duodecies G et 381 S ". Aux termes de l'article 41 duodecies G de la même annexe à ce code : " Les sociétés visées au 4° de l'article 75 de l'annexe II au code général des impôts sont réputées verser à chacun de leurs associés la quote-part des revenus correspondant à ses droits, le jour même où elles ont encaissé lesdits revenus ou ont été créditées de leur montant. ". Il résulte de ces dispositions qu'une société civile, percevant des intérêts, qui n'a pas opté pour l'assujettissement à l'impôt sur les sociétés, dont les associés sont par conséquent personnellement soumis à l'impôt sur le revenu sur la part des bénéfices sociaux correspondant à leurs droits dans celle-ci, doit être regardée comme assurant le paiement de ces revenus, au sens de l'article 242 ter du code général des impôts, et est tenue en tant que telle de déclarer à l'administration les éléments mentionnés à cet article, sous peine de se voir appliquer l'amende prévue à l'article 1736 du même code.<br/>
<br/>
              3. Aux termes de l'article 1961 du code civil : " La justice peut ordonner le séquestre : / (...) 3° Des choses qu'un débiteur offre pour sa libération. ". Aux termes de l'article 1963 du même code : " Le séquestre judiciaire est donné, soit à une personne dont les parties intéressées sont convenues entre elles, soit à une personne nommée d'office par le juge. / Dans l'un et l'autre cas, celui auquel la chose a été confiée est soumis à toutes les obligations qu'emporte le séquestre conventionnel. ". Aux termes de l'article 1956 du même code : " Le séquestre conventionnel est le dépôt fait par une ou plusieurs personnes, d'une chose contentieuse, entre les mains d'un tiers qui s'oblige de la rendre, après la contestation terminée, à la personne qui sera jugée devoir l'obtenir. ". Aux termes de l'article 1153 du même code, dans sa rédaction alors applicable : " Dans les obligations qui se bornent au paiement d'une certaine somme, les dommages-intérêts résultant du retard dans l'exécution ne consistent jamais que dans la condamnation aux intérêts au taux légal, sauf les règles particulières au commerce et au cautionnement. / (...) Ils ne sont dus que du jour de la sommation de payer, ou d'un autre acte équivalent telle une lettre missive s'il en ressort une interpellation suffisante, excepté dans le cas où la loi les fait courir de plein droit. / (...). ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 3 ci-dessus que ni l'acquéreur des titres, qui est, en vertu de l'article 1961 du code civil, libéré lorsqu'il remet au séquestre désigné par justice le prix de vente convenu, ni la personne à laquelle cette somme d'argent a été confiée en application de l'article 1963 du même code ne doivent au vendeur des intérêts au taux légal pendant la durée du séquestre. Les intérêts produits par le placement de la somme d'argent pendant la durée du séquestre judiciaire, remis au vendeur avec la somme elle-même à l'issue de la contestation, ne sont pas dus, en application de l'article 1153 du même code, à raison du versement différé des sommes par la personne à laquelle le séquestre judiciaire a été donné, mais en tant que fruits de la somme séquestrée.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la société Norma, société civile n'ayant pas opté pour l'assujettissement à l'impôt sur les sociétés, a perçu le 20 octobre 2009 de la société Duparc, agissant en qualité de séquestre, le versement du prix de vente de titres qui avait été placé sous séquestre judiciaire en raison de la contestation de leur propriété, ainsi qu'une somme de 350 652 euros correspondant au produit du placement financier des fonds par le séquestre. <br/>
<br/>
              6. En premier lieu, il ressort des énonciations de l'arrêt attaqué que la cour a jugé que la somme correspondant aux intérêts du placement financier des fonds par le séquestre ne pouvait être qualifiée d'intérêts moratoires en raison de ce que le retard avec lequel la société Norma avait perçu le fruit de la vente des titres n'était pas imputable au séquestre. Il résulte de ce qui précède et de ce qui a été dit au point 4 qu'en statuant ainsi, la cour n'a pas commis d'erreur de droit.  <br/>
<br/>
              7. En deuxième lieu, il résulte de l'article 1153 du code civil que la créance d'une somme d'argent dont le principe et le montant résultent de la loi ou du contrat et non de l'appréciation du juge porte intérêt dès la sommation de payer. Or il ne ressort pas des pièces du dossier soumis aux juges du fond qu'une sommation de payer les sommes dues aurait été adressée par la société Norma à la société Duparc en qualité de séquestre. Par suite, le moyen tiré de ce que la cour aurait entaché son arrêt de contradiction de motifs, d'erreur de droit et d'erreur de qualification juridique des faits en jugeant, pour écarter la qualification d'intérêts moratoires et retenir celle de revenus de capitaux mobiliers, qu'il n'existait aucune créance sur le séquestre est inopérant dès lors qu'il est dirigé contre un motif surabondant.<br/>
<br/>
              8. En troisième lieu, la cour n'a pas commis d'erreur de droit en reconnaissant à la " somme de 350 652 euros correspondant aux intérêts provenant du placement financier des fonds par le séquestre " la qualité de revenus des capitaux mobiliers sans rechercher si elle constituait l'accessoire du produit de la vente des titres. La circonstance que ces produits ont été constatés pendant la durée du séquestre judiciaire est sans incidence sur cette qualification. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la société requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administratives ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Norma est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Norma et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
