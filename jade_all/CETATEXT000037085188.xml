<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037085188</ID>
<ANCIEN_ID>J4_L_2018_06_000001702380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/51/CETATEXT000037085188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANTES, 1ère chambre, 18/06/2018, 17NT02380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>CAA de NANTES</JURIDICTION>
<NUMERO>17NT02380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme la Pdte. PHEMOLANT</PRESIDENT>
<AVOCATS>ALERION SOCIETE D'AVOCATS ; ALERION SOCIETE D'AVOCATS ; ALERION SOCIETE D'AVOCATS</AVOCATS>
<RAPPORTEUR>M. Hubert  DELESALLE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. JOUNO</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. et Mme A... D...B...ont demandé au tribunal administratif de Rennes de prononcer la décharge, en droits et pénalités, des suppléments d'impôt sur le revenu et de contributions sociales auxquels ils ont été assujettis au titre des années 2006 et 2007. <br/>
<br/>
       Par un jugement n° 1103371 du 3 juillet 2014, le tribunal administratif de Rennes a rejeté leur demande.<br/>
<br/>
       Par un arrêt n° 14NT02338 du 5 novembre 2015, la cour administrative d'appel de Nantes a, d'une part, déchargé M. et Mme B...des suppléments d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans leurs bases d'imposition de la somme de 8 500 euros en 2006 et de la somme de 15 532 euros en 2007, d'autre part, rejeté le surplus de leur appel contre ce jugement. <br/>
<br/>
       Par une décision du 28 juillet 2017, le Conseil d'Etat, statuant au contentieux a, saisi d'un pourvoi présenté par le ministre des finances et des comptes publics, annulé les articles 1er et 2 de l'arrêt du 5 novembre 2015 de la cour administrative d'appel de Nantes en tant qu'ils ont accordé à M. et Mme A...D...B...la décharge des suppléments d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans leur bases d'imposition de la somme de 8 500 euros au titre de l'année 2006 et de la somme de 12 012 euros au titre de l'année 2007 et a, dans cette mesure, renvoyé l'affaire devant la même cour.<br/>
<br/>
<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par un mémoire, enregistré le 21 août 2017, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il fait valoir qu'il s'en rapporte à l'ensemble des précédents mémoires de l'administration.<br/>
<br/>
       Par un mémoire enregistré le 13 mars 2018, M. et MmeB..., représentés par Me C..., demandent à la cour : <br/>
<br/>
       1°) de prononcer la décharge, en droits et pénalités, des suppléments d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans leur bases d'imposition de la somme de 8 500 euros au titre de l'année 2006 et de la somme de 12 012 euros au titre de l'année 2007 et de réformer le jugement en ce sens ; <br/>
       2°) d'ordonner le reversement des sommes acquittées, augmentées des intérêts moratoires en application de l'article L. 208 du livre des procédures fiscales ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
       Ils soutiennent que :<br/>
       - ils justifient par des attestations que l'origine des fonds n'était pas professionnelle mais provenait de prêts à caractère amical qu'il s'agisse de prêts qu'ils ont obtenus ou de remboursements de prêts qu'ils ont accordés ;<br/>
       - en tout état de cause, ils doivent bénéficier du régime de la présomption de prêt s'agissant de sommes en provenance de proches et il appartient à l'administration fiscale d'apporter la preuve du caractère non-imposable de celles-ci. <br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Delesalle,<br/>
       - et les conclusions de M. Jouno, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
       1. M. et Mme B..., ressortissants de la Corée du Sud et résidents en France, ont relevé appel du jugement du 3 juillet 2014 par lequel le tribunal administratif de Rennes a rejeté leur demande tendant à la décharge des suppléments d'impôt sur le revenu et de contributions sociales auxquels ils ont été assujettis en 2006 et en 2007 en raison de l'imposition de crédits bancaires que l'administration a refusé de regarder, d'une part, à hauteur de 19 280 euros et de 27 012 euros, comme des prêts consentis par des proches ou remboursés par eux et, d'autre part, à hauteur de 70 363 euros et 45 575 euros, comme provenant d'une plus-value immobilière réalisée et imposée en Corée du Sud. Par un arrêt du 5 novembre 2015, la cour administrative d'appel de Nantes a, d'une part, déchargé M. et Mme B...des suppléments d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans leurs bases d'imposition de la somme de 8 500 euros en 2006 et de la somme de 15 532 euros en 2007, d'autre part, rejeté le surplus de leur appel contre ce jugement. Saisi d'un pourvoi par le ministre des finances et des comptes publics, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt en tant qu'il prononçait la décharge des suppléments d'impôt sur le revenu et de contributions sociales résultant de la réintégration dans les bases d'imposition de la somme de 8 500 euros au titre de l'année 2006 et de la somme de 12 012 euros au titre de l'année 2007 et a renvoyé l'affaire devant la cour dans cette mesure.<br/>
<br/>
       2. Aux termes de l'article L. 16 du livre des procédures fiscales : " En vue de l'établissement de l'impôt sur le revenu, l'administration peut demander au contribuable des éclaircissements. Elle peut, en outre, lui demander des justifications au sujet de sa situation et de ses charges de famille, des charges retranchées du revenu net global ou ouvrant droit à une réduction d'impôt sur le revenu en application des articles 156 et 199 septies du code général des impôts, ainsi que des avoirs ou revenus d'avoirs à l'étranger. (...). / Elle peut également lui demander des justifications lorsqu'elle a réuni des éléments permettant d'établir que le contribuable peut avoir des revenus plus importants que ceux qu'il a déclarés. ". Aux termes de l'article L. 69 du même livre : " Sous réserve des dispositions particulières au mode de détermination des bénéfices industriels et commerciaux, des bénéfices agricoles et des bénéfices non commerciaux, sont taxés d'office à l'impôt sur le revenu les contribuables qui se sont abstenus de répondre aux demandes d'éclaircissements ou de justifications prévues à l'article L. 16. ". Il résulte de ces dispositions que l'administration peut régulièrement taxer d'office, en application de l'article L. 69 du livre des procédures fiscales, les sommes dont la nature demeure inconnue, au vu des renseignements dont elle disposait avant l'envoi de la demande de justifications fondée sur l'article L. 16 du livre des procédures fiscales et des réponses apportées par le contribuable à cette demande. Il est toutefois loisible au contribuable régulièrement taxé d'office d'apporter devant le juge de l'impôt la preuve que ces sommes ne constituent pas des revenus imposables. En revanche, il appartient à l'administration fiscale, lorsqu'elle entend remettre en cause, même par voie d'imposition d'office, le caractère non imposable de sommes perçues par un contribuable mais dont il est établi qu'elles ont été versées à l'intéressé par l'un de ses parents avec lequel il n'entretient aucune relation d'affaires, de justifier que les sommes en cause ne revêtent pas le caractère d'un prêt familial mais celui de revenus professionnels.<br/>
<br/>
       3. Il résulte de l'instruction que des crédits bancaires d'un montant de 7 000 euros, 1 500 euros, 6 012 euros et 6 000 euros versés respectivement les 8 et 16 mars 2006, 17 janvier et 16 novembre 2007 ont été imposés dans la catégorie des revenus d'origine indéterminée et dans le cadre de la procédure de taxation d'office, en application des articles L. 16 et L. 69 du livre des procédures fiscales, au motif que M. et Mme B...n'ont pas justifié, par des contrats de prêt enregistrés, de l'origine de ces sommes qui leur auraient été versées par des proches à titre de prêts ou de remboursement de prêts.<br/>
<br/>
       4. D'une part, en l'absence de relation familiale avec les trois ressortissants coréens à l'origine de ces versements, il appartient à M. et MmeB..., contrairement à ce qu'ils soutiennent, de justifier de la nature de ces sommes. <br/>
<br/>
       5. D'autre part, les requérants produisent trois attestations de ressortissants coréens résidant en Corée du Sud, dont la traduction du coréen a été légalisée selon eux le 26 mars 2010 par l'ambassade de Corée du Sud en France, faisant état, pour l'une, d'un prêt et, pour les autres, d'un remboursement de prêt. Toutefois, alors de plus que l'une n'est datée que du 23 octobre 2010, elles ne comportent aucune précision notamment quant aux conditions de ces prêts et à leur date exacte et ne sont pas assorties, au surplus, de justificatifs d'identité de leurs auteurs. Dans ces conditions, et quand bien il n'existerait pas de formalités d'enregistrement des prêts en Corée du Sud où les prêts auraient été faits avant leur venue en France, M. et MmeB..., n'apportent pas la preuve qui leur incombe de la nature des sommes concernées.<br/>
<br/>
       6. Il résulte de tout ce qui précède que M. et Mme B...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a rejeté leur demande. Doivent être rejetées, par voie de conséquence, leurs conclusions tendant au remboursement des sommes assorties d'intérêts moratoires, en tout état de cause, ainsi que celles tendant à l'application de l'article L. 761-1 du code de justice administrative. <br/>
DECIDE :<br/>
Article 1er : Le surplus de la requête de M. et Mme B...est rejeté.<br/>
Article 4 : Le présent arrêt sera notifié à M. et Mme A...D...B...et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
       Délibéré après l'audience du 31 mai 2018, à laquelle siégeaient :<br/>
<br/>
       - Mme Phémolant, présidente de la cour, <br/>
       - M. Geffray, président assesseur,<br/>
       - M. Delesalle, premier conseiller.<br/>
<br/>
<br/>
       Lu en audience publique le 18 juin 2018.<br/>
<br/>
Le rapporteur,<br/>
H. DelesalleLa présidente,<br/>
B. Phémolant       <br/>
<br/>
Le greffier,<br/>
<br/>
C. Croiger<br/>
       La République mande et ordonne au ministre de l'action et des comptes publics, en ce qui le concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
N° 17NT023802<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
