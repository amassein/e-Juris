<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986843</ID>
<ANCIEN_ID>JG_L_2020_06_000000424344</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986843.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 10/06/2020, 424344, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424344</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424344.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Metzervisse Contact a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir l'arrêté du 28 mai 2015 par lequel le préfet de la Moselle a autorisé certaines catégories de commerces à déroger au régime du repos dominical et des jours fériés. Par un jugement n° 1505188 du 23 mars 2016, le tribunal administratif de Strasbourg a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16NC00905 du 19 juillet 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Metzervisse Contact contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 septembre et 17 décembre 2018 et le 19 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Metzervisse Contact demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code du travail ;<br/>
              - le décret n° 2007-1888 du 26 décembre 2007 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet-Odent, avocat de la société Metzervisse Contact ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. En vertu de l'article L. 3134-2 du code du travail, l'emploi de salariés dans les entreprises industrielles, commerciales ou artisanales est, dans les départements de la Moselle, du Bas-Rhin et du Haut-Rhin, interdit les dimanches et jours fériés, sauf dans les cas prévus par le chapitre IV du titre III du livre Ier de la troisième partie de ce code, au sein duquel cet article figure. A ce titre, l'article L. 3134-4 de ce code dispose que : " Dans les exploitations commerciales, les salariés ne peuvent être employés le premier jour des fêtes de Noël, de Pâques ou de la Pentecôte. / Les autres dimanches et jours fériés, leur travail ne peut dépasser cinq heures. / Par voie de statuts ayant force obligatoire, adoptés après consultation des employeurs et des salariés et publiés selon les formes prescrites, les départements ou communes peuvent réduire la durée du travail ou interdire complètement le travail pour toutes les exploitations commerciales ou pour certaines branches d'activité. (...) ". L'article L. 3134-7 de ce code, combiné avec son article R. 3134-3, prévoit que des dérogations aux dispositions de l'article L. 3134-4 peuvent être accordées par le préfet " pour les catégories d'activité dont l'exercice complet ou partiel est nécessaire les dimanches ou les jours fériés pour la satisfaction de besoins de la population présentant un caractère journalier ou se manifestant particulièrement ces jours-là. (...) ". L'article L. 3134-11 du même code précise que : " Lorsqu'il est interdit, en application des articles L. 3134-4 à L. 3134-9, d'employer des salariés dans les exploitations commerciales, il est également interdit durant ces jours de procéder à une exploitation industrielle, commerciale ou artisanale dans les lieux de vente au public. (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 18 mai 2015, le conseil départemental de la Moselle a, en application des dispositions du troisième alinéa de l'article L. 3134-4 du code du travail, citées au point précédent, adopté un nouveau statut relatif à l'ouverture des commerces en Moselle les dimanches et jours fériés. Ce statut prévoit, en son article 1er, une interdiction d'ouvrir au public les exploitations commerciales et d'y occuper des salariés les dimanches et jours fériés et, en son article 2, une dérogation, sauf les jours de Noël, de Pâques et de la Pentecôte, dans la limite de cinq heures d'ouverture, le premier dimanche des soldes d'hiver et le premier dimanche des soldes d'été pour toutes les exploitations commerciales de la Moselle hors concessions automobiles et quatre dimanches dans l'année pour les concessions automobiles. Par un arrêté du 28 mai 2015, le préfet de la Moselle a, sur le fondement de l'article L. 3134-7 du code du travail, accordé à certains commerces et activités des dérogations au régime du repos dominical et des jours fériés résultant des dispositions législatives et de ce statut départemental. Le préfet a, à ce titre, par l'article 2 de son arrêté, autorisé les commerces d'alimentation générale d'une superficie inférieure ou égale à 200 mètres carrés à ouvrir au public et à employer du personnel les dimanches et jours fériés, jusqu'à 13 heures. Par un jugement du 23 mars 2016, le tribunal administratif de Strasbourg a rejeté la demande de la société Metzervisse Contact tendant à l'annulation pour excès de pouvoir de cet arrêté. Cette société se pourvoi en cassation contre l'arrêt du 19 juillet 2018 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              3. Il résulte des dispositions citées au point 1 que l'octroi de dérogations au régime du repos dominical par le préfet sur le fondement de l'article L. 3134-7 du code du travail ne peut être accordé qu'à des catégories d'activités dont l'exercice complet ou partiel est nécessaire les dimanches ou jours fériés pour la satisfaction de besoins de la population présentant un caractère journalier ou se manifestant particulièrement ce jour-là. La cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que le préfet n'avait pas fait une inexacte application de ces dispositions en accordant une telle dérogation aux commerces d'alimentation générale d'une superficie inférieure ou égale à 200 mètres carrés, alors même que ce seuil ne se rattache pas à une distinction existante et ne couvre qu'un nombre limité de commerces. <br/>
<br/>
              4. Il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que la société Metzervisse Contact demande au titre des frais exposés par elle et non compris dans les dépens. <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Metzervisse Contact est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Metzervisse Contact et à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
