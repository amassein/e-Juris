<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042519141</ID>
<ANCIEN_ID>JG_L_2020_11_000000425701</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/51/91/CETATEXT000042519141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/11/2020, 425701</TITRE>
<DATE_DEC>2020-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425701</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425701.20201112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés le 26 novembre 2018 et le 26 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. D... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 20 septembre 2018 par laquelle la formation restreinte du Haut Conseil du commissariat aux comptes a prononcé à son encontre la sanction disciplinaire de la radiation de la liste des commissaires aux comptes ; <br/>
<br/>
              2°) de mettre à la charge du Haut Conseil du commissariat aux comptes la somme de 6 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive n° 2014/56/UE du parlement européen et du conseil du 16 avril 2014 ;<br/>
              - le code de commerce ;<br/>
              - le code général des impôts ;<br/>
              - l'ordonnance n° 2016-315 du 17 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinie, avocat de M. A... et à la SCP Ohl, Vexliard, avocat du Haut Conseil du commissariat aux comptes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction qu'à la suite du signalement effectué par le procureur général près la cour d'appel de Fort-de-France, relatif à la condamnation pénale, devenue définitive, de M. A..., commissaire aux comptes, pour des faits de soustraction frauduleuse à l'établissement et au paiement de l'impôt, dissimulation de sommes et fraude fiscale, le rapporteur général du Haut conseil du commissariat aux comptes a, en application de l'article L. 824-5 du code de commerce, diligenté, le 11 octobre 2016, une procédure d'enquête disciplinaire concernant ce commissaire aux comptes. Le Haut conseil du commissariat aux comptes hors la présence des membres de la formation restreinte a, sur le fondement de l'article L. 824-8 du code de commerce, arrêté les griefs notifiés, le 2 novembre 2017, à M. A... tenant à la commission de faits contraires à l'honneur et à la probité de la profession de commissaire aux comptes pour avoir omis, en 2010 et 2011, de déclarer des bénéfices non commerciaux et de payer la taxe sur la valeur ajoutée. Le 20 septembre 2018, la formation restreinte du Haut conseil du commissariat aux comptes a prononcé à l'encontre de M. A... la sanction de la radiation de la liste des commissaires aux comptes. M. A... demande au Conseil d'Etat l'annulation de cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 824-2 du code de commerce : " I.- Les commissaires aux comptes sont passibles des sanctions suivantes : / 1° L'avertissement ; / 2° Le blâme ; / 3° L'interdiction d'exercer la fonction de commissaire aux comptes pour une durée n'excédant pas cinq ans ; / 4° La radiation de la liste ; / 5° Le retrait de l'honorariat. (...) ". Aux termes de l'article L. 824-12 du même code : " Les sanctions sont déterminées en tenant compte : / 1° De la gravité et de la durée de la faute ou du manquement reprochés ; / 2° De la qualité et du degré d'implication de la personne intéressée ; / 3° De la situation et de la capacité financière de la personne intéressée, au vu notamment de son patrimoine et, s'agissant d'une personne physique de ses revenus annuels, s'agissant d'une personne morale de son chiffre d'affaires total ; / 4° De l'importance soit des gains ou avantages obtenus, soit des pertes ou coûts évités par la personne intéressée, dans la mesure où ils peuvent être déterminés ; / 5° Du degré de coopération dont a fait preuve la personne intéressée dans le cadre de l'enquête ; / 6° Des manquements commis précédemment par la personne intéressée ; / 7° Lorsque la sanction est prononcée en raison de manquement aux dispositions des sections 3 à 6 du chapitre Ier du titre VI du livre V du code monétaire et financier, elle est en outre déterminée en tenant compte, le cas échéant, de l'importance du préjudice subi par les tiers. " <br/>
<br/>
              3. En premier lieu, les dispositions de l'article L. 824-12 du code de commerce citées précédemment, issues de l'ordonnance du 17 mars 2016 relative au commissariat aux comptes, ont été prises pour assurer la transposition de l'article 30 ter de la directive n° 2014/56/UE du parlement européen et du conseil du 16 avril 2014 modifiant la directive n° 2006/43/CE concernant les contrôles légaux des comptes annuels et des comptes consolidés, laquelle a notamment pour objectif que les Etats membres appliquent des critères identiques lorsqu'ils définissent la sanction à imposer à l'encontre, notamment, d'un commissaire aux comptes. Cet article, d'une part, énumère les critères pertinents susceptibles d'être pris en compte par les autorités compétentes en matière de sanctions et mesures administratives, d'autre part, prévoit que d'autres éléments ne peuvent être pris en compte par ces autorités que s'ils sont précisés dans le droit national. Il résulte de l'article L. 824-12 du code de commerce cité précédemment, interprété à la lumière de l'article 30 ter de la directive n° 2014/56/UE du parlement européen et du conseil du 16 avril 2014 modifiant la directive n° 2006/43/CE concernant les contrôles légaux des comptes annuels et des comptes consolidés, que si le Haut conseil statuant en formation restreinte, chargé, en vertu de L. 824-10 du même code, de connaître de l'action disciplinaire intentée à l'encontre des commissaires aux comptes inscrits sur la liste mentionnée au I de l'article L. 822-1 de ce code, ne peut déterminer la sanction qu'il prononce qu'au regard des seuls critères que ce texte énumère, il peut, toutefois, ne se fonder que sur ceux de ces critères qui sont pertinents au regard des faits de l'espèce.<br/>
<br/>
              4. Il résulte tout d'abord de l'instruction que les faits pour lesquels M. A... a été sanctionné disciplinairement sont ceux pour lesquels il avait fait l'objet d'une condamnation pénale devenue définitive et dont le rapporteur général du Haut conseil du commissariat aux comptes avait été saisi par le procureur général près la cour d'appel de Fort-de-France en application de l'article L. 824-4 du code de commerce. Ces faits, tels que constatés par le juge pénal, s'imposaient en conséquence à l'autorité disciplinaire et ne pouvaient être utilement discutés devant elle par M. A.... Par suite et dans les circonstances de l'espèce, le critère mentionné au 5° de l'article L. 824-12 du code de commerce relatif au " degré de coopération dont a fait preuve la personne intéressée dans le cadre de l'enquête " ne pouvait être pris en considération pour la détermination de la sanction à prononcer.<br/>
<br/>
              5. Il résulte également de l'instruction que les efforts consentis par M. A... pour la mise en place, postérieurement à la notification de griefs, d'une procédure au sein de son cabinet de suivi des chèques et de leur encaissement, destinée à éviter que les faits punis se reproduisent, ne se rattachent à aucun des critères énumérés à l'article L. 824-12 du code de commerce.<br/>
<br/>
              6. Il résulte enfin de l'instruction que, contrairement à ce qui est soutenu, la formation restreinte du Haut conseil du commissariat aux comptes a pris en compte, au titre du critère énuméré au 6° de l'article L. 824-12 du code de commerce, l'absence d'antécédent sur le plan disciplinaire de M. A....<br/>
<br/>
              7. Il résulte de ce qui précède que M. A... n'est pas fondé à soutenir que la formation restreinte du Haut conseil du commissariat aux comptes n'aurait pas fait une exacte application des 5° et 6° de l'article L. 824-12 du code de commerce pour déterminer la sanction disciplinaire qu'il a prononcé à son encontre. <br/>
<br/>
              8. En second lieu, la sanction doit être déterminée, comme il est rappelé au point 3, selon les critères énumérés à l'article L. 824-12 du même code, appréciés au vu des faits de l'espèce.<br/>
<br/>
              9. Par jugement du 12 octobre 2015, M. A... a été condamné pour fraude fiscale à trois mois d'emprisonnement avec sursis et 15 000 euros d'amende, ainsi qu'à une peine complémentaire d'affichage pendant un mois, notamment pour avoir omis de déclarer à l'administration fiscale des bénéfices non commerciaux de 224 693 euros en 2010 et de 231 786 euros en 2011 et minoré ses déclarations relatives à la taxe sur la valeur ajoutée, éludant à ce titre les sommes de 19 099 euros et de 19 702 euros pour les années 2010 et 2011. Si M. A... ne conteste pas la matérialité des faits, au demeurant revêtus de la chose jugée au pénal, il soutient qu'il n'avait pas la volonté de tromper l'administration fiscale. Cependant, il résulte de l'instruction que, compte tenu des sommes en litige et de sa qualité de commissaire aux comptes, il ne pouvait qu'avoir conscience du caractère non sincère de ses déclarations fiscales. De plus, ces faits se sont étalés, pour la période non prescrite, sur deux années. Ils ont été commis alors que M. A..., d'une part, exerçait la profession de commissaire aux comptes, lequel prête serment devant l'autorité judiciaire et a pour mission de certifier la régularité, la sincérité et la fidélité des comptes qu'il contrôle et, d'autre part, présidait la compagnie régionale des commissaires aux comptes de Fort-de-France, qui a pour objet de s'assurer du respect des règles et obligations professionnelles des commissaires aux comptes de la Martinique. Eu égard à la nature et à la particulière gravité des manquements reprochés, ayant consisté à soustraire des revenus au paiement de l'impôt, commis par un professionnel expérimenté dont le rôle est de s'assurer de la sincérité des comptes qu'il contrôle et qui, lors de leur commission, présidait la compagnie régionale des commissaires aux comptes de Fort-de-France, la formation restreinte du Haut conseil du commissariat aux comptes n'a pas prononcé une sanction disproportionnée en prononçant à l'encontre du requérant la sanction de la radiation de la liste des commissaires aux comptes, alors même que ce dernier a apuré la quasi-totalité de sa dette auprès de l'administration fiscale et n'avait, jusqu'alors, aucun antécédent disciplinaire. Il est, à cet égard indifférent que la chambre nationale de discipline auprès du Conseil supérieur de l'ordre des experts-comptables n'ait, pour les mêmes faits, prononcé à l'encontre de M. A..., qui est également expert-comptable, qu'une mesure de suspension de deux ans.<br/>
<br/>
              10. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision de la formation restreinte du Haut conseil du commissariat aux comptes qu'il attaque. Par voie de conséquence, ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 3 000 euros à verser au Haut conseil du commissariat aux comptes, au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : M. A... versera au Haut Conseil du commissariat aux comptes une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. D... A... et au Haut Conseil du commissariat aux comptes.<br/>
Copie en sera adressée au garde des sceaux, ministre de la justice, et à la compagnie nationale des commissaires aux comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-06-05 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. PROFESSIONS NON ORGANISÉES EN ORDRES ET NE S'EXERÇANT PAS DANS LE CADRE D'UNE CHARGE OU D'UN OFFICE. COMMISSAIRES AUX COMPTES. - DISCIPLINE PROFESSIONNELLE - DÉTERMINATION DE LA SANCTION - OBLIGATION POUR L'AUTORITÉ DE SANCTION DE PRENDRE EN COMPTE L'ENSEMBLE DES CRITÈRES MENTIONNÉS À L'ARTICLE L. 824-12 DU CODE DE COMMERCE - ABSENCE, L'AUTORITÉ POUVANT SE FONDER SUR LES SEULS CRITÈRES PERTINENTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. - DISCIPLINE PROFESSIONNELLE - DÉTERMINATION DE LA SANCTION - OBLIGATION POUR L'AUTORITÉ DE SANCTION DE PRENDRE EN COMPTE L'ENSEMBLE DES CRITÈRES MENTIONNÉS À L'ARTICLE L. 824-12 DU CODE DE COMMERCE - ABSENCE, L'AUTORITÉ POUVANT SE FONDER SUR LES SEULS CRITÈRES PERTINENTS.
</SCT>
<ANA ID="9A"> 55-03-06-05 Il résulte de l'article L. 824-12 du code de commerce, interprété à la lumière de l'article 30 ter de la directive n° 2014/56/UE du 16 avril 2014, que si le Haut conseil du commissariat aux comptes statuant en formation restreinte, chargé, en vertu de L. 824-10 du même code, de connaître de l'action disciplinaire intentée à l'encontre des commissaires aux comptes inscrits sur la liste mentionnée au I de l'article L. 822-1 de ce code, ne peut déterminer la sanction qu'il prononce qu'au regard des seuls critères que ce texte énumère, il peut, toutefois, ne se fonder que sur ceux de ces critères qui sont pertinents au regard des faits de l'espèce.</ANA>
<ANA ID="9B"> 55-04 Il résulte de l'article L. 824-12 du code de commerce, interprété à la lumière de l'article 30 ter de la directive n° 2014/56/UE du 16 avril 2014, que si le Haut conseil du commissariat aux comptes statuant en formation restreinte, chargé, en vertu de L. 824-10 du même code, de connaître de l'action disciplinaire intentée à l'encontre des commissaires aux comptes inscrits sur la liste mentionnée au I de l'article L. 822-1 de ce code, ne peut déterminer la sanction qu'il prononce qu'au regard des seuls critères que ce texte énumère, il peut, toutefois, ne se fonder que sur ceux de ces critères qui sont pertinents au regard des faits de l'espèce.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
