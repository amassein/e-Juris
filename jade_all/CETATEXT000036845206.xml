<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845206</ID>
<ANCIEN_ID>JG_L_2018_04_000000390203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845206.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 26/04/2018, 390203, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:390203.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° La société Les Entrepôts du Centre a demandé au tribunal administratif d'Orléans de prononcer la décharge ou la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2008 à 2011 à raison d'un immeuble situé dans la commune d' Ouzouer-sur-Trézée (Loiret). Par un jugement nos 1000385, 1001948, 1100717, 1104332 du 30 mai 2012, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par une décision n° 361566 du 19 septembre 2014, le Conseil d'Etat statuant au contentieux a annulé ce jugement et renvoyé l'affaire devant le tribunal administratif d'Orléans.<br/>
<br/>
              Par un jugement nos 1401953, 1403563 du 12 mars 2015, le tribunal administratif d'Orléans a de nouveau rejeté la demande de la société Les Entrepôts du Centre.<br/>
<br/>
              2° La société Les Entrepôts du Centre a demandé au tribunal administratif d'Orléans de prononcer la décharge ou la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2012 et 2013 à raison d'un immeuble situé dans la commune d' Ouzouer-sur-Trézée (Loiret).<br/>
<br/>
              Par le même jugement nos 1401953, 1403563 du 12 mars 2015, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux autres mémoire, enregistrés les 15 mai et 14 août 2015, le 14 février 2017 et le 28 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Les Entrepôts du Centre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la Société Les Entrepôts du Centre.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 août 2005, le préfet du Loiret a procédé à la fermeture administrative de locaux à usage industriel à Ouzouer-sur-Trézée (Loiret) en vue de l'exécution de travaux permettant leur dépollution. Ceux-ci ont été confiés à l'Agence de l'environnement et de la maîtrise de l'énergie et étaient en cours au 1er janvier de chacune des années d'imposition en litige. La société Les Entrepôts du Centre ayant acquis ces locaux le 9 juillet 2007, elle a été assujettie à la taxe foncière sur les propriétés bâties au titre des années 2008 à 2013. Elle a demandé au tribunal administratif d'Orléans de prononcer la décharge de ces impositions et, subsidiairement, leur réduction. Par une décision du 19 septembre 2014, le Conseil d'Etat, statuant au contentieux, a annulé le jugement du 30 mai 2012 par lequel le tribunal administratif d'Orléans avait rejeté la demande de la société requérante et renvoyé l'affaire devant ce tribunal. La société Les Entrepôts du Centre se pourvoit en cassation contre le jugement du 12 mars 2015 par lequel le tribunal administratif d'Orléans, d'une part, a joint à sa demande tendant à la décharge des impositions en litige au titre des années 2008 à 2011 une demande de la même société tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties au titre des années 2012 et 2013, d'autre part, a rejeté ces deux demandes. <br/>
<br/>
              Sur l'étendue du litige :<br/>
<br/>
              2. Par deux décisions du 28 février 2018, l'administration a prononcé des dégrèvements partiels des impositions en litige, pour un montant de 90 991 euros s'agissant de la taxe foncière sur les propriétés bâties due au titre de 2008 et pour un montant de 24 829 euros s'agissant de la taxe foncière sur les propriétés bâties due au titre de 2009, résultant de l'application pour ces années de l'évaluation des locaux en litige selon la méthode par comparaison prévue à l'article 1498 du code général des impôts. Il en résulte qu'il n'y a plus lieu de statuer, dans cette mesure, sur les conclusions du pourvoi de la société.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Aux termes de l'article R. 732-1-1 du code de justice administrative: " Sans préjudice de l'application des dispositions spécifiques à certains contentieux prévoyant que l'audience se déroule sans conclusions du rapporteur public, le président de la formation de jugement ou le magistrat statuant seul peut dispenser le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience sur tout litige relevant des contentieux suivants : (...) 5° Taxe d'habitation et taxe foncière sur les propriétés bâties afférentes aux locaux d'habitation et à usage professionnel au sens de l'article 1496 du code général des impôts ainsi que contribution à l'audiovisuel public ; (...) ". Il résulte de ces dispositions que le rapporteur public ne peut être dispensé de prononcer des conclusions dans un litige relatif à une taxe foncière sur les propriétés bâties pour des biens dont la valeur locative n'a pas été déterminée en application de l'article 1496 du code général des impôts. Ainsi, la dispense de conclusions permise par les dispositions de l'article R. 732-1-1 précité ne peut s'appliquer au jugement d'un litige portant sur des locaux évalués selon les méthodes prévues à l'article 1498 ou à l'article 1499 du code général des impôts. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que les locaux en litige ont été initialement évalués, selon les années, par application de la méthode prévue à l'article 1498 du code général des impôts ou de la méthode prévue à l'article 1499 du même code. Par suite, le président de la formation de jugement ne pouvait dispenser le rapporteur public de prononcer ses conclusions à l'audience du 17 février 2015 sur les demandes de la société Les Entrepôts du Centre tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle avait été assujettie au titre des années 2008 à 2013 à raison de ces locaux. Le jugement est, en conséquence, irrégulier, et la société requérante est fondée pour ce motif et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à en demander l'annulation dans la mesure des impositions restant en litige. <br/>
<br/>
              5. Aux termes de l'article L. 821-2 du code de justice administrative : " S'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, le Conseil d'Etat peut soit renvoyer l'affaire devant la même juridiction statuant, sauf impossibilité tenant à la nature de la juridiction, dans une autre formation, soit renvoyer l'affaire devant une autre juridiction de même nature, soit régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie. / Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, pour les cotisations afférentes aux années 2008 à 2011, d'un second pourvoi en cassation, il y a lieu de régler l'affaire au fond. Il y a également lieu, dans les circonstances de l'espèce, de régler l'affaire au fond s'agissant des cotisations afférentes aux années 2012 et 2013. <br/>
<br/>
              Sur les demandes de la société Les Entrepôts du Centre :<br/>
<br/>
              6. En premier lieu, aux termes du I de l'article 1389 du code général des impôts : " Les contribuables peuvent obtenir le dégrèvement de la taxe foncière en cas de vacance d'une maison normalement destinée à la location ou d'inexploitation d'un immeuble utilisé par le contribuable lui-même à usage commercial ou industriel, à partir du premier jour du mois suivant celui du début de la vacance ou de l'inexploitation jusqu'au dernier jour du mois au cours duquel la vacance ou l'inexploitation a pris fin. / Le dégrèvement est subordonné à la triple condition que la vacance ou l'inexploitation soit indépendante de la volonté du contribuable, qu'elle ait une durée de trois mois au moins et qu'elle affecte soit la totalité de l'immeuble, soit une partie susceptible de location ou d'exploitation séparée ". Il résulte de ces dispositions que si l'inexploitation d'un immeuble peut ouvrir droit au dégrèvement qu'elles prévoient, c'est notamment à la double condition que le contribuable utilise lui-même cet immeuble à des fins commerciales ou industrielles et que son exploitation soit interrompue du fait de circonstances indépendantes de sa volonté. Le respect de cette condition exige, en principe, que le contribuable exploite lui-même l'établissement avant l'interruption de l'exploitation. Toutefois, lorsqu'un contribuable achète un immeuble dont l'exploitation à des fins industrielles ou commerciales a été interrompue du fait de circonstances indépendantes de sa volonté, il peut prétendre à l'exonération prévue par les dispositions précitées s'il résulte de l'instruction qu'il a acquis cet immeuble en vue de l'exploiter lui-même à des fins industrielles et commerciales. <br/>
<br/>
              7. D'une part, il résulte de l'instruction que, par un arrêté du 25 août 2005, le préfet du Loiret a procédé à la fermeture administrative des locaux litigieux en vue de l'exécution de travaux permettant leur dépollution et que ces travaux, confiés à l'Agence de l'environnement et de la maîtrise de l'énergie, étaient en cours au 1er janvier de chacune des années d'imposition en litige. Une telle circonstance doit être regardée comme indépendante de la volonté de la société requérante, alors même qu'elle en aurait eu connaissance lors de l'acquisition du bien par voie d'adjudication, ainsi qu'il ressort des mentions mêmes de l'acte de vente.<br/>
              8. D'autre part, pour soutenir qu'elle avait, lors de l'acquisition du bien en cause, l'intention de l'exploiter à des fins industrielles ou commerciales, la société requérante se prévaut de son objet social, tel qu'il ressort de ses statuts, qui comporte non seulement " l'administration et l'exploitation par bail, location ou autrement " de l'immeuble en cause, mais aussi " toutes activités de stockage, de logistique et de manutention ". Elle se prévaut également d'un courrier qu'elle a adressé le 10 octobre 2008 au centre des impôts de Gien, tendant au dégrèvement de la taxe foncière pour l'année 2008, d'un courrier de la préfecture du Loiret en date du 20 juillet 2009 mentionnant un projet " concernant l'implantation d'un stockage de céréales ", d'un courrier de la direction régionale de l'industrie, de la recherche et de l'environnement du Centre en date du 23 avril 2010 relatif à un projet " concernant une demande d'autorisation d'exploiter un entrepôt ", ainsi que de l'activité de stockage de céréales exercée par la société de droit belge Les silos des Bastions, dont deux des trois associés sont ses propres associés. <br/>
<br/>
              9. Toutefois, l'objet social déclaré lors de l'immatriculation de la société, tel qu'il figure sur l'extrait Kbis produit par l'administration, mentionne uniquement " l'acquisition, la gestion, la location et l'administration de tous biens mobiliers et immobiliers " et les informations portées par la société requérante dans sa déclaration de taxe professionnelle 1003-K et relatives à l'activité exercée l'année 2008 mentionnent une activité d'" acquisition, gestion, location et administration ". Si les courriers produits par la société mentionnent notamment le dépôt par elle, en juin 2009, d'un dossier de déclaration d'une installation classée, cette circonstance, au demeurant postérieure au rejet par l'administration de la première demande de décharge présentée pour l'année 2008, n'est pas par elle-même de nature à établir que la requérante a acquis le site, le 9 juillet 2007, dans l'intention de l'exploiter elle-même dans le cadre d'une activité industrielle ou commerciale. <br/>
<br/>
              10. En deuxième lieu, la société requérante soutient pouvoir bénéficier au titre des années 2008, 2010 et 2011 de l'exonération des parts départementale et régionale de taxe foncière sur les propriétés bâties prévue à l'article 1383 du code général des impôts qui exonère de la taxe les constructions nouvelles, reconstructions et additions de construction au titre des deux années suivant celle de leur achèvement, ainsi que les bâtiments ruraux convertis en maison ou en usine et l'affectation de terrains à un usage commercial ou industriel. Toutefois, il ne résulte pas de l'instruction que les locaux en cause entreraient dans les prévisions de cet article.<br/>
<br/>
              11. En troisième lieu, compte-tenu des dégrèvements intervenus en cours d'instance et mentionnés au point 2 ci-dessus, les moyens tirés d'une erreur portant sur la méthode d'évaluation retenue pour établir les cotisations de taxe foncière des années 2008 et 2009 sont inopérants. La requérante ne peut davantage utilement soutenir que l'administration aurait dû faire application de la valeur locative plancher prévue par les dispositions de l'article 1518 B du code général des impôts, dès lors que les locaux en cause sont désormais évalués par l'administration selon la méthode par comparaison prévue à l'article 1498 du code général des impôts. Dans ces conditions, le moyen tiré de l'article 1517 du code général des impôts est également inopérant. <br/>
<br/>
              12. En dernier lieu, aux termes de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " La  jouissance des droits et libertés reconnus dans la présente Convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation. ". Aux termes de l'article 1er du protocole additionnel à cette même convention : " Toute personne physique ou morale a droit au respect de  ses biens.  Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes. ". <br/>
<br/>
              13. Si la société requérante soutient que le cumul des impositions mises à sa charge, à défaut pour elle de pouvoir bénéficier de l'exonération prévue au I de l'article 1389 du code général des impôts, présente un caractère confiscatoire au regard du prix qu'elle a acquitté pour son acquisition, en méconnaissance des stipulations combinées des articles 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et 1er du protocole additionnel à cette convention, elle n'établit ni même n'allègue que le prix qu'elle a acquitté pour le bien dont il s'agit, acquis dans le cadre d'une procédure de liquidation judiciaire par voie d'adjudication, ne tiendrait pas compte de l'état du site, faisant obstacle à son exploitation avant dépollution. Ainsi, elle ne démontre pas le caractère confiscatoire des impositions mises à sa charge, qui sont au demeurant déterminées annuellement et d'un montant raisonnable.<br/>
<br/>
              14. Il résulte de tout ce qui précède que les demandes de la société requérante doivent être rejetées. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi à hauteur de 90 991 euros s'agissant de la taxe foncière sur les propriétés bâties due au titre de 2008 et à hauteur de 24 829 euros s'agissant de la taxe foncière sur les propriétés bâties au titre de 2009. <br/>
Article 2 : Le jugement du 12 mars 2015 du tribunal administratif d'Orléans est annulé dans la mesure des impositions restant en litige.<br/>
Article 3 : Le surplus des conclusions des demandes présentées par la société Les Entrepôts du Centre devant le tribunal administratif d'Orléans sous les nos 1401953, 1000385, 1001948, 1100717 et 1104332 est rejeté. <br/>
Article 4 : L'Etat versera la somme de 2 000 euros à la société Les Entrepôts du Centre au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la SARL Les Entrepôts du Centre et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
