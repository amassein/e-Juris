<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800420</ID>
<ANCIEN_ID>JG_L_2018_04_000000418205</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/04/CETATEXT000036800420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 13/04/2018, 418205, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418205</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418205.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B..., à l'appui de leur demande présentée devant le tribunal administratif de Cergy-Pontoise tendant notamment à la réduction de la cotisation d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2011, ont présenté deux mémoires, enregistrés les 16 mars et 12 juin 2017 au greffe du tribunal, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels ils soulèvent une question prioritaire de constitutionnalité. <br/>
<br/>
              Par un jugement n° 1611266 du 13 février 2018, enregistré le 15 février 2018 au secrétariat du contentieux du Conseil d'Etat, la 5ème chambre du tribunal administratif de Cergy-Pontoise a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat cette question relative à la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 199 undecies B du code général des impôts, dans leur rédaction issue de l'article 98 de la loi du 29 décembre 2010 de finances pour 2011.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010, notamment son article 98 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. et MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article 199 undecies B du code général des impôts institue une réduction d'impôt sur le revenu en faveur des contribuables domiciliés en France, qui réalisent, dans les départements et territoires d'outre-mer, " des investissements productifs neufs " dans le cadre d'une entreprise exerçant une activité agricole ou une activité industrielle, commerciale ou artisanale relevant de l'article 34. En vertu du dix-neuvième alinéa du I de cet article, cette réduction d'impôt s'applique également lorsque le contribuable procède à de tels investissements par l'intermédiaire d'une société soumise au régime d'imposition prévu à l'article 8, dont les parts sont détenues directement, ou par l'intermédiaire d'une entreprise unipersonnelle à responsabilité limitée. Le 1° du I de l'article 98 de la loi du 29 décembre 2010 de finances pour 2011 a cependant exclu du bénéfice de cette réduction d'impôt les investissements réalisés par l'intermédiaire d'une société en participation à compter de l'imposition des revenus de l'année 2011. M. et Mme B...soutiennent que ces dispositions méconnaissent l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              3. Aux termes de l'article 16 de la Déclaration des droits de l'homme et du citoyen: " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ". Il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou d'abroger ceux-ci en leur substituant, le cas échéant, d'autres dispositions. Ce faisant, il ne saurait toutefois priver de garanties légales des exigences constitutionnelles. En particulier, il ne saurait, sans motif d'intérêt général suffisant, ni porter atteinte aux situations légalement acquises ni remettre en cause les effets qui peuvent légitimement être attendus de telles situations.<br/>
<br/>
              4. Les dispositions contestées sont applicables à compter de l'imposition des revenus de l'année 2011 et sont dépourvues de portée rétroactive. Elles n'ont eu ni pour objet ni pour effet de modifier le fait générateur de la réduction d'impôt prévue à l'article 199 undecies B du code général des impôts, qui est la date de la création de l'immobilisation au titre de laquelle l'investissement productif a été réalisé ou de sa livraison effective dans le département d'outre-mer et, dans ce dernier cas, la date à laquelle l'entreprise, disposant matériellement de l'investissement productif, peut commencer son exploitation effective et, dès lors, en retirer des revenus. Par suite, les requérants ne sauraient se fonder sur la circonstance qu'ils avaient souscrit au capital de sociétés en participation avant l'entrée en vigueur de ces nouvelles dispositions pour soutenir qu'elles portent atteinte à des situations légalement acquises ou aux effets qui peuvent légitimement être attendus de telles situations. Ils ne pouvaient, en particulier, légitimement attendre que leur soient appliquées les dispositions en vigueur à la date de cette souscription, alors que les installations n'ont été effectivement exploitées que postérieurement à l'entrée en vigueur des nouvelles dispositions et qu'ainsi, à la date de la souscription, le fait générateur de la réduction d'impôt prévue à l'article 199 undecies B du code général des impôts n'était pas encore intervenu. <br/>
<br/>
              5. Il résulte de ce qui précède que la question posée, qui n'est pas nouvelle, est dépourvue de caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du 1° du I de l'article 98 de la loi du 29 décembre 2010 de finances pour 2011 portent atteinte aux droits et libertés garantis par la Constitution doit être écarté. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et MmeB....<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Cergy-Pontoise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
