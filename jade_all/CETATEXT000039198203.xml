<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198203</ID>
<ANCIEN_ID>JG_L_2019_10_000000416771</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198203.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/10/2019, 416771</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416771</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416771.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Melun d'annuler la décision du 23 octobre 2014 par laquelle la directrice académique des services de l'éducation nationale, directrice des services départementaux de l'éducation nationale du Val-de-Marne a retiré la décision du 2 septembre 2014 portant admission à la retraite à compter du 1er septembre 2015 et de procéder à l'inscription en faux du document intitulé " Etat général des services ". Par un jugement n° 1410482/8 du 7 février 2017, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17PA00995 du 21 décembre 2017, enregistrée le 22 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 24 mars 2017 au greffe de cette cour, présenté par Mme B.... Par ce pourvoi et deux nouveaux mémoires, enregistrés le 1er février et 26 mars 2018, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 2009-972 du 3 août 2009 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 24 du code des pensions civiles et militaires de retraite dans sa rédaction alors applicable : " I. - La liquidation de la pension intervient : / 1° Lorsque le fonctionnaire civil est radié des cadres par limite d'âge, ou s'il a atteint, à la date de l'admission à la retraite, l'âge mentionné à l'article L. 161-17-2 du code de la sécurité sociale, ou de cinquante-sept ans s'il a accompli au moins dix-sept ans de services dans des emplois classés dans la catégorie active. / Sont classés dans la catégorie active les emplois présentant un risque particulier ou des fatigues exceptionnelles. La nomenclature en est établie par décret en Conseil d'Etat ; / (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article R. 35 du même code : " Les services rendus par les agents qui, terminant leur carrière au service de l'Etat, ont auparavant relevé du régime de la Caisse nationale de retraites des agents des collectivités locales et des administrations mentionnées aux 3° et 5° de l'article L. 5 sont toujours réputés accomplis dans la catégorie sédentaire. / Toutefois, pour les agents qui ont été intégrés d'office dans les cadres de l'Etat, sont assimilés à des services de la catégorie active les services accomplis sous le régime de la Caisse nationale de retraites des agents des collectivités locales, et classés dans la catégorie active au titre de ce régime ".<br/>
<br/>
              4. Les dispositions de l'article L. 24 du code des pensions civiles et militaires de retraite citées au point 2 ont pour objet, en accordant une possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active, de tenir compte du risque particulier ou des fatigues exceptionnelles que présentent certains emplois. Alors que les services accomplis dans des emplois classés dans la catégorie active ouvrent droit, s'ils sont accomplis au service de l'Etat, à une liquidation anticipée de la pension, les dispositions de l'article R. 35 du même code citées au point 3 conduisent à ce qu'il n'aille pas de même si les services classés en catégorie active ont été rendus par des agents qui, terminant leur carrière au service de l'Etat, ont auparavant relevé du régime de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) et les ont effectués alors qu'ils relevaient du régime de cette caisse. Il n'en va différemment que si ces agents ont été intégrés d'office dans les cadres de l'Etat, tous les services relevant de la catégorie active sous le régime de la CNRACL étant alors assimilés à des services de la catégorie active.<br/>
<br/>
              5. Les dispositions du premier alinéa de l'article R. 35 du code des pensions civiles et militaires de retraite en tant qu'elles excluent toute prise en compte au titre de la catégorie active, pour les agents ayant terminé leur carrière au service de l'Etat après avoir relevé du régime de la CNRACL, des services classés en catégorie active qu'ils ont rendus auparavant alors qu'ils relevaient du régime de cette caisse, sans égard pour le risque particulier ou les fatigues exceptionnelles que ces services présentaient effectivement, ont institué une différence de traitement entre ces agents et les agents ayant effectué toute leur carrière au service de l'Etat. Cette différence de traitement est sans rapport avec l'objet de la norme qui établit la possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active en raison du risque particulier ou des fatigues exceptionnelles que présentent ces emplois. En l'absence de considérations d'intérêt général de nature à justifier cette différence, elle porte atteinte au principe d'égalité de traitement des agents publics.<br/>
<br/>
              6. Dès lors, en estimant, pour rejeter la demande de Mme B... tendant à l'annulation de la décision ayant pour effet de lui refuser de faire valoir ses droits à la retraite de façon anticipée, au motif que les services qu'elle a accomplis en tant qu'assistante sociale du 1er juillet 1981 au 11 octobre 1989, relevant de la CNRACL, sont nécessairement comptabilisés, en application des dispositions de l'article R. 35 du code des pensions civiles et militaires de retraite, dans la catégorie sédentaire en raison de son recrutement par concours dans les cadres de l'Etat, que les dispositions de l'article R. 35 du code des pensions civiles et militaires de retraite n'étaient pas contraires, sur ce point, au principe d'égalité devant la loi, le tribunal administratif de Melun a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B... est fondée à demander l'annulation du jugement attaqué.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. En premier lieu, il ressort des pièces du dossier que, par une décision du 23 octobre 2014, la directrice académique des services de l'éducation nationale, directrice des services départementaux de l'éducation nationale du Val-de-Marne a retiré la décision du 2 septembre 2014 portant admission à la retraite de Mme B... à compter du 1er septembre 2015 au motif, notamment, que les services qu'elle a accomplis en tant qu'assistante sociale du 1er juillet 1981 au 11 octobre 1989, relevant de la CNRACL, sont comptabilisés, en application des dispositions de l'article R. 35 du code des pensions civiles et militaires de retraite, dans la catégorie sédentaire en raison de son recrutement par concours dans les cadres de l'Etat et qu'en conséquence elle n'a pas accompli le nombre requis d'années de service dans un ou plusieurs emplois classés en catégorie active pour bénéficier d'une liquidation anticipée de sa pension en application des dispositions précitées du 1° du I de l'article L. 24 du même code.<br/>
<br/>
              9. Toutefois, dans la mesure de ce qui a été dit au point 5, les dispositions du premier aliéna de l'article R. 35 du code des pensions civiles et militaires de retraite sont contraires au principe d'égalité devant la loi. Il n'est par ailleurs pas contesté que si les services accomplis par Mme B... en catégorie active alors qu'elle était dans la fonction publique hospitalière sont pris en compte, elle remplit la condition d'accomplissement de dix-sept années de services accomplis en catégorie active posée par le 1° du I de l'article L. 24 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              10. En second lieu, la solution du litige ne dépend pas du document intitulé " Etat général des services " produit par la rectrice de l'académie de Créteil et argué de faux par Mme B.... Dès lors, les conclusions présentées par Mme B... et tendant, sur le fondement de l'article R. 633-1 du code de justice administrative, à l'inscription de faux de ce document doivent être rejetées.<br/>
<br/>
              11. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, Mme B... est fondée à demander l'annulation de la décision du 23 octobre 2014 par laquelle la directrice académique des services de l'éducation nationale, directrice des services départementaux de l'éducation nationale du Val-de-Marne a retiré la décision du 2 septembre 2014 portant admission à la retraite à compter du 1er septembre 2015.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 7 février 2017 du tribunal administratif de Melun est annulé.<br/>
Article 2 : La décision du 23 octobre 2014 par laquelle la directrice académique des services de l'éducation nationale, directrice des services départementaux de l'éducation nationale du Val-de-Marne a retiré la décision du 2 septembre 2014 portant admission à la retraite de Mme B... à compter du 1er septembre 2015 est annulée.<br/>
Article 3 : Le surplus des conclusions de la demande présentée par Mme B... devant le tribunal administratif de Melun est rejeté.<br/>
Article 4 : L'Etat versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme A... B..., au ministre de l'éducation nationale et de la jeunesse et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS. - POSSIBILITÉ DE LIQUIDATION ANTICIPÉE DE LA PENSION EN CAS D'ACCOMPLISSEMENT D'ANNÉES DE SERVICES DANS DES EMPLOIS CLASSÉS DANS LA CATÉGORIE ACTIVE (ART. L. 24 DU CPCMR) - NON PRISE EN COMPTE AU TITRE DE LA CATÉGORIE ACTIVE, POUR LES AGENTS AYANT TERMINÉ LEUR CARRIÈRE AU SERVICE DE L'ETAT APRÈS AVOIR RELEVÉ DU RÉGIME DE LA CNRACL, DES SERVICES CLASSÉS EN CATÉGORIE ACTIVE QU'ILS ONT RENDUS SOUS CE RÉGIME (ART. 35 DU CPCMR) -  DIFFÉRENCE DE TRAITEMENT NON JUSTIFIÉE ENTRE CES AGENTS ET LES AGENTS AYANT EFFECTUÉ TOUTE LEUR CARRIÈRE AU SERVICE DE L'ETAT - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-02-03-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. LIQUIDATION DE LA PENSION. SERVICES PRIS EN COMPTE. - POSSIBILITÉ DE LIQUIDATION ANTICIPÉE DE LA PENSION EN CAS D'ACCOMPLISSEMENT D'ANNÉES DE SERVICES DANS DES EMPLOIS CLASSÉS DANS LA CATÉGORIE ACTIVE (ART. L. 24 DU CPCMR) - NON PRISE EN COMPTE AU TITRE DE LA CATÉGORIE ACTIVE, POUR LES AGENTS AYANT TERMINÉ LEUR CARRIÈRE AU SERVICE DE L'ETAT APRÈS AVOIR RELEVÉ DU RÉGIME DE LA CNRACL, DES SERVICES CLASSÉS EN CATÉGORIE ACTIVE QU'ILS ONT RENDUS SOUS CE RÉGIME (ART. 35 DU CPCMR) -  DIFFÉRENCE DE TRAITEMENT NON JUSTIFIÉE ENTRE CES AGENTS ET LES AGENTS AYANT EFFECTUÉ TOUTE LEUR CARRIÈRE AU SERVICE DE L'ETAT - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS.
</SCT>
<ANA ID="9A"> 01-04-03-03-02 L'article L. 24 du code des pensions civiles et militaires de retraite (CPCMR) a pour objet, en accordant une possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active, de tenir compte du risque particulier ou des fatigues exceptionnelles que présentent certains emplois. Alors que les services accomplis dans des emplois classés dans la catégorie active ouvrent droit, s'ils sont accomplis au service de l'Etat, à une liquidation anticipée de la pension, les dispositions de l'article R. 35 du même code conduisent à ce qu'il n'en aille pas de même si les services classés en catégorie active ont été rendus par des agents qui, terminant leur carrière au service de l'Etat, ont auparavant relevé du régime de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) et les ont effectués alors qu'ils relevaient du régime de cette caisse. Il n'en va différemment que si ces agents ont été intégrés d'office dans les cadres de l'Etat, tous les services relevant de la catégorie active sous le régime de la CNRACL étant alors assimilés à des services de la catégorie active.,,,Les dispositions du premier alinéa de l'article R. 35 du CPCMR en tant qu'elles excluent toute prise en compte au titre de la catégorie active, pour les agents ayant terminé leur carrière au service de l'Etat après avoir relevé du régime de la CNRACL, des services classés en catégorie active qu'ils ont rendus auparavant alors qu'ils relevaient du régime de cette caisse, sans égard pour le risque particulier ou les fatigues exceptionnelles que ces services présentaient effectivement, ont institué une différence de traitement entre ces agents et les agents ayant effectué toute leur carrière au service de l'Etat. Cette différence de traitement est sans rapport avec l'objet de la norme qui établit la possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active en raison du risque particulier ou des fatigues exceptionnelles que présentent ces emplois. En l'absence de considérations d'intérêt général de nature à justifier cette différence, elle porte atteinte au principe d'égalité de traitement des agents publics.,,,Dès lors, en estimant, pour rejeter la demande de la requérante tendant à l'annulation de la décision ayant pour effet de lui refuser de faire valoir ses droits à la retraite de façon anticipée, au motif que les services qu'elle a accomplis en tant qu'assistante sociale du 1er juillet 1981 au 11 octobre 1989, relevant de la CNRACL, sont nécessairement comptabilisés, en application des dispositions de l'article R. 35 du CPCMR, dans la catégorie sédentaire en raison de son recrutement par concours dans les cadres de l'Etat, que les dispositions de l'article R. 35 du CPCMR n'étaient pas contraires, sur ce point, au principe d'égalité devant la loi, le tribunal administratif a commis une erreur de droit.</ANA>
<ANA ID="9B"> 48-02-02-03-02 L'article L. 24 du code des pensions civiles et militaires de retraite (CPCMR) a pour objet, en accordant une possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active, de tenir compte du risque particulier ou des fatigues exceptionnelles que présentent certains emplois. Alors que les services accomplis dans des emplois classés dans la catégorie active ouvrent droit, s'ils sont accomplis au service de l'Etat, à une liquidation anticipée de la pension, les dispositions de l'article R. 35 du même code conduisent à ce qu'il n'en aille pas de même si les services classés en catégorie active ont été rendus par des agents qui, terminant leur carrière au service de l'Etat, ont auparavant relevé du régime de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) et les ont effectués alors qu'ils relevaient du régime de cette caisse. Il n'en va différemment que si ces agents ont été intégrés d'office dans les cadres de l'Etat, tous les services relevant de la catégorie active sous le régime de la CNRACL étant alors assimilés à des services de la catégorie active.,,,Les dispositions du premier alinéa de l'article R. 35 du CPCMR en tant qu'elles excluent toute prise en compte au titre de la catégorie active, pour les agents ayant terminé leur carrière au service de l'Etat après avoir relevé du régime de la CNRACL, des services classés en catégorie active qu'ils ont rendus auparavant alors qu'ils relevaient du régime de cette caisse, sans égard pour le risque particulier ou les fatigues exceptionnelles que ces services présentaient effectivement, ont institué une différence de traitement entre ces agents et les agents ayant effectué toute leur carrière au service de l'Etat. Cette différence de traitement est sans rapport avec l'objet de la norme qui établit la possibilité de liquidation anticipée de la pension en cas d'accomplissement de dix-sept années de services dans des emplois classés dans la catégorie active en raison du risque particulier ou des fatigues exceptionnelles que présentent ces emplois. En l'absence de considérations d'intérêt général de nature à justifier cette différence, elle porte atteinte au principe d'égalité de traitement des agents publics.,,,Dès lors, en estimant, pour rejeter la demande de la requérante tendant à l'annulation de la décision ayant pour effet de lui refuser de faire valoir ses droits à la retraite de façon anticipée, au motif que les services qu'elle a accomplis en tant qu'assistante sociale du 1er juillet 1981 au 11 octobre 1989, relevant de la CNRACL, sont nécessairement comptabilisés, en application des dispositions de l'article R. 35 du CPCMR, dans la catégorie sédentaire en raison de son recrutement par concours dans les cadres de l'Etat, que les dispositions de l'article R. 35 du CPCMR n'étaient pas contraires, sur ce point, au principe d'égalité devant la loi, le tribunal administratif a commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
