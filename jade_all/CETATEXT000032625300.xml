<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032625300</ID>
<ANCIEN_ID>JG_L_2016_06_000000392621</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/53/CETATEXT000032625300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 01/06/2016, 392621</TITRE>
<DATE_DEC>2016-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392621</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392621.20160601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montpellier d'annuler l'arrêté du 20 septembre 2012 par lequel le maire de la commune de Sète a prononcé son licenciement pour insuffisance professionnelle. Par un jugement n° 1204896 du 30 avril 2014, le tribunal administratif de Montpellier a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14MA02911 du 12 juin 2015, la cour administrative d'appel de Marseille a, sur appel de M.A..., annulé le jugement du tribunal administratif de Montpellier et l'arrêté du 20 septembre 2012 du maire de la commune de Sète. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 août et 10 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Sète demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de M. A...; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code du travail ;  <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;  <br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - le décret n° 88-145 du 15 février 1988 ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Sète et à la SCP Lyon-Caen, Thiriez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par un arrêté du 20 septembre 2012, le maire de la commune de Sète a prononcé le licenciement pour insuffisance professionnelle de M.A..., agent non titulaire de la commune affecté au centre de formation des apprentis de Sète et exerçant depuis 1976 les fonctions de formateur en mathématiques, sciences et technologie ; que par un jugement du 30 avril 2014, le tribunal administratif de Montpellier a rejeté la demande de M. A...tendant à l'annulation de cet arrêté ; que par un arrêt du 12 juin 2015 contre lequel la commune de Sète se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ce jugement et l'arrêté prononçant le licenciement de M. A...; <br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que le licenciement pour inaptitude professionnelle d'un agent public ne peut être fondé que sur des éléments révélant l'inaptitude de l'agent à exercer normalement les fonctions pour lesquelles il a été engagé ou correspondant à son grade et non sur une carence ponctuelle dans l'exercice de ces fonctions ; que, toutefois, une telle mesure ne saurait être subordonnée à ce que l'insuffisance professionnelle ait été constatée à plusieurs reprises au cours de la carrière de l'agent ni qu'elle ait persisté après qu'il ait été invité à remédier aux insuffisances constatées ; que, par suite, une évaluation portant sur la manière dont l'agent a exercé ses fonctions durant une période suffisante et révélant son inaptitude à un exercice normal de ses fonctions est de nature à justifier légalement son licenciement ; qu'en particulier, aucune disposition législative ou réglementaire ni aucun principe ne fait obstacle à ce que l'insuffisance professionnelle d'un agent contractuel de la fonction publique territoriale exerçant des fonctions d'enseignement dans un centre de formation des apprentis soit constatée à l'occasion d'une visite d'inspection pédagogique diligentée dans les conditions prévues par les articles R. 6251-1 et suivants du code du travail et portant sur l'activité pédagogique de l'agent examinée dans la durée ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la cour a commis une erreur de droit en jugeant, pour annuler le jugement du tribunal administratif de Montpellier et l'arrêté du 20 septembre 2012, que si M. A...a fait l'objet le 18 juin 2012 d'une inspection pédagogique à laquelle il s'est opposé en refusant de dispenser son cours aux apprentis et si le rapport de visite des inspecteurs établi le 27 juin 2012 à partir des supports pédagogiques de l'enseignant et des travaux des élèves a mis en évidence des carences pédagogiques de l'intéressé, une inspection ne saurait, " en raison de son caractère ponctuel et limité, sauf carences particulièrement graves ou persistantes déjà constatées, suffire à fonder une mesure de licenciement " pour insuffisance professionnelle ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'arrêt attaqué de la cour administrative d'appel de Marseille doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la légalité de la décision de licenciement pour insuffisance professionnelle : <br/>
<br/>
              5. Considérant, en premier lieu, qu'aucune disposition législative ou réglementaire ne prévoit que les formateurs des centres de formation des apprentis doivent être préalablement avertis de l'inspection pédagogique dont ils doivent faire l'objet de la part du service de l'inspection de l'éducation nationale ; qu'il ressort en tout état de cause des pièces du dossier que M. A...a été prévenu le vendredi 15 juin 2012 qu'il ferait l'objet d'une visite pédagogique le lundi 18 juin suivant ;  <br/>
<br/>
              6. Considérant, en deuxième lieu, que M A...ne peut utilement soutenir que l'appréciation de sa valeur professionnelle ne peut reposer sur l'évaluation réalisée par les inspecteurs au motif que celle-ci ne s'appuie essentiellement que sur les documents pédagogiques relatifs à l'année scolaire 2011-2012 dont ils disposaient dès lors qu'il est constant que M. A...s'est lui-même opposé, le jour de l'inspection, à l'entrée des apprentis dans la classe et a refusé de dispenser son cours ; <br/>
<br/>
              7. Considérant, en troisième lieu, qu'il ressort du rapport d'évaluation du service de l'inspection de l'éducation nationale mentionné au point 3 que si M.A..., en charge de l'enseignement en mathématiques, sciences et technologie, assure un enseignement en mathématiques, au demeurant réducteur au regard des recommandations pédagogiques nationales car effectué principalement sous forme d'activités, il ne dispense aucun enseignement en sciences physiques ; que l'objectif pédagogique des cours qu'il dispense n'est pas clairement identifié et que l'évaluation des acquisitions des apprentis est absente ; qu'au vu de ces carences, le recteur de l'académie de Montpellier a fait savoir le 21 août 2012 au maire de la commune de Sète qu'il " lui semblerait pertinent de procéder au licenciement de M. A..." ; que la circonstance que M. A...n'avait jamais fait l'objet auparavant d'une évaluation professionnelle n'est pas de nature à faire obstacle à ce que son insuffisance professionnelle puisse être relevée ; que par suite, M. A...n'est pas fondé à soutenir que la décision prononçant son licenciement reposerait sur une appréciation erronée de sa valeur professionnelle ; <br/>
<br/>
              8. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              9. Considérant qu'il résulte de tout de ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation de l'arrêté du 20 septembre 2012 par lequel le maire de Sète l'a licencié pour insuffisance professionnelle ; que les dispositions de l'article L. 761-1 du code de justice administrative font en conséquence obstacle à ce que la somme demandée à ce titre par MA..., partie perdante à l'instance, soit mise à la charge de la commune de Sète ; qu'il y a lieu, en revanche, de mettre à la charge de M. A...une somme 2 000 euros à verser à la commune de Sète en application des mêmes dispositions au titre de la procédure suivie devant la cour administrative d'appel et le Conseil d'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 juin 2015 est annulé.<br/>
Article 2 : L'appel de M. A...et ses conclusions tendant à l'application des dispositions de l'article L 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : M. A...versera une somme de 2 000 euros à la commune de Sète au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la commune de Sète et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-02-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL ENSEIGNANT. - LICENCIEMENT POUR INAPTITUDE PROFESSIONNELLE - 1) FAITS DE NATURE À JUSTIFIER UN LICENCIEMENT POUR INAPTITUDE PROFESSIONNELLE ET MODALITÉS DU CONSTAT DE L'INSUFFISANCE PROFESSIONNELLE - 2) CAS D'UN ENSEIGNANT - FACULTÉ DE CONSTATER L'INSUFFISANCE PROFESSIONNELLE À L'OCCASION D'UNE VISITE D'INSPECTION PÉDAGOGIQUE PORTANT SUR L'ACTIVITÉ DE L'AGENT EXAMINÉE DANS LA DURÉE - EXISTENCE - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-06-03 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. INSUFFISANCE PROFESSIONNELLE. - 1) FAITS DE NATURE À JUSTIFIER UN LICENCIEMENT POUR INAPTITUDE PROFESSIONNELLE - 2) CONSTAT DE L'INSUFFISANCE PROFESSIONNELLE - NÉCESSITÉ D'UN CONSTAT À PLUSIEURS REPRISES ET D'UNE PERSISTANCE APRÈS INVITATION DE L'AGENT À Y REMÉDIER - ABSENCE - CONSÉQUENCES - CAS D'UN ENSEIGNANT.
</SCT>
<ANA ID="9A"> 30-01-02-01 1) Le licenciement pour inaptitude professionnelle d'un agent public ne peut être fondé que sur des éléments révélant l'inaptitude de l'agent à exercer normalement les fonctions pour lesquelles il a été engagé ou correspondant à son grade et non sur une carence ponctuelle dans l'exercice de ces fonctions.,,,Toutefois, une telle mesure ne saurait être subordonnée à ce que l'insuffisance professionnelle ait été constatée à plusieurs reprises au cours de la carrière de l'agent ni qu'elle ait persisté après qu'il ait été invité à remédier aux insuffisances constatées. Par suite, une évaluation portant sur la manière dont l'agent a exercé ses fonctions durant une période suffisante et révélant son inaptitude à un exercice normal de ses fonctions est de nature à justifier légalement son licenciement.,,,2) Aucune disposition législative ou réglementaire ni aucun principe ne fait obstacle à ce que l'insuffisance professionnelle d'un agent contractuel de la fonction publique territoriale exerçant des fonctions d'enseignement dans un centre de formation des apprentis (CFA) soit constatée à l'occasion d'une visite d'inspection pédagogique diligentée dans les conditions prévues par les articles R. 6251-1 et suivants du code du travail et portant sur l'activité pédagogique de l'agent examinée dans la durée.,,,3) En l'espèce, il ressort du rapport d'évaluation du service de l'inspection de l'éducation nationale que si l'intéressé, en charge de l'enseignement en mathématiques, sciences et technologie, assure un enseignement en mathématiques, au demeurant réducteur au regard des recommandations pédagogiques nationales car effectué principalement sous forme d'activités, il ne dispense aucun enseignement en sciences physiques. En outre, l'objectif pédagogique des cours qu'il dispense n'est pas clairement identifié et l'évaluation des acquisitions des apprentis est absente. Au vu de ces carences, le recteur de l'académie a fait savoir au maire de la commune qu'il lui semblerait pertinent de procéder au licenciement de l'intéressé. La circonstance que celui-ci n'avait jamais fait l'objet auparavant d'une évaluation professionnelle n'est pas de nature à faire obstacle à ce que son insuffisance professionnelle puisse être relevée. Absence d'erreur dans l'appréciation de sa valeur professionnelle.</ANA>
<ANA ID="9B"> 36-10-06-03 1) Le licenciement pour inaptitude professionnelle d'un agent public ne peut être fondé que sur des éléments révélant l'inaptitude de l'agent à exercer normalement les fonctions pour lesquelles il a été engagé ou correspondant à son grade et non sur une carence ponctuelle dans l'exercice de ces fonctions.,,,2) Toutefois, une telle mesure ne saurait être subordonnée à ce que l'insuffisance professionnelle ait été constatée à plusieurs reprises au cours de la carrière de l'agent ni qu'elle ait persisté après qu'il ait été invité à remédier aux insuffisances constatées. Par suite, une évaluation portant sur la manière dont l'agent a exercé ses fonctions durant une période suffisante et révélant son inaptitude à un exercice normal de ses fonctions est de nature à justifier légalement son licenciement.,,,En particulier, aucune disposition législative ou réglementaire ni aucun principe ne fait obstacle à ce que l'insuffisance professionnelle d'un agent contractuel de la fonction publique territoriale exerçant des fonctions d'enseignement dans un centre de formation des apprentis (CFA) soit constatée à l'occasion d'une visite d'inspection pédagogique diligentée dans les conditions prévues par les articles R. 6251-1 et suivants du code du travail et portant sur l'activité pédagogique de l'agent examinée dans la durée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
