<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024815309</ID>
<ANCIEN_ID>JG_L_2011_11_000000321410</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/81/53/CETATEXT000024815309.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 18/11/2011, 321410, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>321410</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESEC:2011:321410.20111118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 octobre 2008 et 7 janvier 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE QUINTO AVENIO, dont le siège est 37 bis rue Greneta à Paris (75002), représentée par son gérant en exercice ; la SOCIETE QUINTO AVENIO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les décisions du 15 janvier 2008 par lesquelles le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa candidature en vue de l'exploitation du service de radio Skyrock par voie hertzienne terrestre dans les zones de Dole et de Lons-le-Saunier du ressort du comité technique radiophonique de Dijon ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel de lui délivrer les autorisations demandées sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que la SOCIETE QUINTO AVENIO demande l'annulation des décisions du 15 janvier 2008 par lesquelles le Conseil supérieur de l'audiovisuel a rejeté sa candidature en vue de l'exploitation d'un service de radiodiffusion sonore par voie hertzienne dénommé Skyrock et relevant de la catégorie C pour les zones de Dole et Lons-le-Saunier du ressort du comité technique radiophonique de Dijon ;   <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              Considérant qu'aux termes de l'article 29 de la loi du 30 septembre 1986 relative à la liberté de communication : " (...) l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article. / Pour les zones géographiques et les catégories de services qu'il a préalablement déterminées, le conseil publie une liste de fréquences disponibles ainsi qu'un appel à candidatures. Il fixe le délai dans lequel les candidatures doivent être déposées. / (...) A l'issue du délai prévu au deuxième alinéa ci-dessus, le conseil arrête la liste des candidats dont le dossier est recevable. / Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public (...) " ; qu'aux termes de l'article 32 : " Les autorisations prévues à la présente section sont publiées au Journal officiel de la République française avec les obligations dont elles sont assorties. / Les refus d'autorisation sont motivés et sont notifiés aux candidats dans un délai d'un mois après la publication prévue à l'alinéa précédent. Lorsqu'ils s'appliquent à un service de radio diffusé par voie hertzienne terrestre, ils peuvent être motivés par référence à un rapport de synthèse explicitant les choix du conseil au regard des critères mentionnés aux articles 1er et 29 " ; <br/>
<br/>
              Considérant qu'afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance ; que la circonstance qu'une décision de refus soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi, est sans incidence sur la légalité de cette décision ; qu'il résulte de ce qui précède que la circonstance que les décisions de rejet des candidatures de la SOCIETE QUINTO AVENIO, prises par le Conseil supérieur de l'audiovisuel lors de sa séance du 15 janvier 2008, lui aient été notifiées par une lettre du 6 août 2008, alors que les décisions d'autorisation prises lors de la même séance, et que cette société n'a pas contestées, avaient été publiées au Journal officiel du 28 février 2008, est sans incidence sur leur légalité ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              Considérant, d'une part, que, par deux communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le Conseil supérieur de l'audiovisuel, faisant usage de la compétence qui lui a été conférée par l'article 29 précité de la loi du 30 septembre 1986, a déterminé cinq catégories de services en vue de l'appel à candidatures pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre ; que ces cinq catégories sont ainsi définies : services associatifs éligibles au fonds de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (C), services thématiques à vocation nationale (D), et services généralistes à vocation nationale (E) ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 29 de la loi du 30 septembre 1986, dans sa rédaction applicable à la date de la décision attaquée, le Conseil supérieur de l'audiovisuel " accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. / Il tient également compte : (...) 5° de la contribution à la production de programmes réalisés localement. / (...) Le Conseil supérieur de l'audiovisuel veille, sur l'ensemble du territoire, à ce qu'une part suffisante des ressources en fréquences soit attribuée aux services édités par une association et accomplissant une mission sociale de proximité (...) / Le Conseil veille également au juste équilibre entre les réseaux nationaux de radiodiffusion, d'une part, et les services locaux, régionaux et thématiques indépendants, d'autre part (...) " ;<br/>
<br/>
              Considérant qu'il ne ressort pas des pièces du dossier qu'en statuant sur l'ensemble des candidatures au cours d'une même séance, le Conseil supérieur de l'audiovisuel ait omis d'examiner l'intérêt particulier de chaque projet ;     	<br/>
<br/>
              En ce qui concerne la zone de Dole :<br/>
<br/>
              Considérant que dans la zone de Dole où un service était autorisé en catégorie E, et où trois fréquences étaient disponibles, le conseil a retenu un service en catégorie A, un service en catégorie B et un service en catégorie D ; que pour écarter dans cette zone la candidature du service Skyrock offert en catégorie C par la SOCIETE QUINTO AVENIO, il a indiqué que ce service ne disposait pas d'un ancrage dans le Jura et n'était pas en mesure d'assurer au mieux " l'expression pluraliste des courants socioculturels " ; que ces motifs ne sont entachés ni d'erreur de droit ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ;<br/>
<br/>
              En ce qui concerne la zone de Lons-le-Saunier :<br/>
<br/>
              Considérant que sur la zone de Lons-le-Saunier, où émettaient un service en catégorie B et un service en catégorie E, et où dix fréquences étaient disponibles, le conseil a retenu deux services en catégorie A, trois services en catégorie C, trois services en catégorie D, et deux services en catégorie E ; que le conseil a attribué quatre de ces dix fréquences aux services Virgin Radio Dijon et Fun Radio Bourgogne en catégorie C, et NRJ et Rire et Chanson en catégorie D, au motif que ces fréquences étaient très proches de celles attribuées aux mêmes services dans les zones voisines de Dijon, Beaune, Besançon et Mâcon, et que leur affectation à la diffusion d'un autre programme aurait entraîné des phénomènes de brouillage qu'il n'était pas possible de prévenir en soumettant les émissions à des conditions techniques particulières ; qu'il a écarté la candidature de la société requérante au motif que le service Skyrock s'adressait à un public proche de celui de Fun Radio, choisi dans cette zone ; que ce motif n'est entaché ni d'erreur de droit ni d'une erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986, dès lors notamment que ces deux services de même catégorie visent des publics d'âge comparable et diffusent des programmes caractérisés par une dominante musicale de variétés contemporaines ;<br/>
<br/>
              Considérant, par ailleurs, que la seule circonstance que le groupe ORBUS, auquel appartient la SOCIETE QUINTO AVENIO, dispose dans le ressort du comité technique radiophonique de Dijon d'un nombre de fréquences sensiblement inférieur à celui dont disposent les groupes auxquels appartiennent les services autorisés n'est pas à elle seule de nature à établir que le conseil aurait méconnu l'impératif de diversification des opérateurs ;<br/>
<br/>
              Considérant, enfin, que les décisions attaquées, qui résultent d'une exacte application des critères posés par la loi du 30 septembre 1986, ne méconnaissent pas les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SOCIETE QUINTO AVENIO n'est pas fondée à demander l'annulation des décisions qu'elle attaque ni à ce que soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement à la SOCIETE QUINTO AVENIO d'une somme au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SOCIETE QUINTO AVENIO est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE QUINTO AVENIO et au Conseil supérieur de l'audiovisuel.<br/>
Copie pour information en sera adressée au ministre de la culture et de la communication.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION REFUSANT UNE AUTORISATION. - REFUS D'AUTORISATION D'EXPLOITATION DE FRÉQUENCES HERTZIENNES - NON-RESPECT DU DÉLAI DE NOTIFICATION PRÉVU PAR L'ARTICLE 32 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986 - INCIDENCE SUR LA LÉGALITÉ DE LA DÉCISION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-07-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. PROMULGATION - PUBLICATION - NOTIFICATION. NOTIFICATION. EFFETS DE LA NOTIFICATION. - AUTORISATIONS D'EXPLOITATION DE FRÉQUENCES HERTZIENNES - DÉCISION DE REFUS - NON-RESPECT DU DÉLAI DE NOTIFICATION PRÉVU PAR L'ARTICLE 32 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986 - INCIDENCE SUR LA LÉGALITÉ DE LA DÉCISION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - AUTORISATIONS D'EXPLOITATION DE FRÉQUENCES HERTZIENNES - DÉCISION DE REFUS - NON-RESPECT DU DÉLAI DE NOTIFICATION PRÉVU PAR L'ARTICLE 32 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986 - INCIDENCE SUR LA LÉGALITÉ DE LA DÉCISION - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">56-04-01-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. OCTROI DES AUTORISATIONS. - DÉCISION DE REFUS - NON-RESPECT DU DÉLAI DE NOTIFICATION PRÉVU PAR L'ARTICLE 32 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986 - INCIDENCE SUR LA LÉGALITÉ DE LA DÉCISION - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-06 Afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance. L'article 32 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication dispose, en son premier alinéa, que les autorisations d'exploitation de fréquences hertziennes sont publiées au Journal officiel de la République française avec les obligations dont elles sont assorties et, en son second alinéa, que les refus d'autorisation sont motivés et sont notifiés aux candidats dans le délai d'un mois après la publication prévue à l'alinéa précédent. La circonstance qu'une décision de refus soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi, est sans incidence sur la légalité de cette décision.</ANA>
<ANA ID="9B"> 01-07-03-03 Afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance. L'article 32 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication dispose, en son premier alinéa, que les autorisations d'exploitation de fréquences hertziennes sont publiées au Journal officiel de la République française avec les obligations dont elles sont assorties et, en son second alinéa, que les refus d'autorisation sont motivés et sont notifiés aux candidats dans le délai d'un mois après la publication prévue à l'alinéa précédent. La circonstance qu'une décision de refus soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi, est sans incidence sur la légalité de cette décision.</ANA>
<ANA ID="9C"> 56-01 Afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance. L'article 32 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication dispose, en son premier alinéa, que les autorisations d'exploitation de fréquences hertziennes sont publiées au Journal officiel de la République française avec les obligations dont elles sont assorties et, en son second alinéa, que les refus d'autorisation sont motivés et sont notifiés aux candidats dans le délai d'un mois après la publication prévue à l'alinéa précédent. La circonstance qu'une décision de refus soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi, est sans incidence sur la légalité de cette décision.</ANA>
<ANA ID="9D"> 56-04-01-01 Afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance. L'article 32 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication dispose, en son premier alinéa, que les autorisations d'exploitation de fréquences hertziennes sont publiées au Journal officiel de la République française avec les obligations dont elles sont assorties et, en son second alinéa, que les refus d'autorisation sont motivés et sont notifiés aux candidats dans le délai d'un mois après la publication prévue à l'alinéa précédent. La circonstance qu'une décision de refus soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi, est sans incidence sur la légalité de cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
