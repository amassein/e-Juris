<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035436398</ID>
<ANCIEN_ID>JG_L_2017_08_000000413354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/43/63/CETATEXT000035436398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/08/2017, 413354, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-08-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:413354.20170814</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Sous le n° 413354, par une requête enregistrée le 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'enjoindre au Premier ministre d'interdire la tenue du " Camp d'été décolonial " dont l'organisation serait projetée du 12 au 16 août 2017 ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au Premier ministre de prescrire son ouverture à tous, sans distinction de couleur de peau ou d'origine ethnique ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 euro au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Sous le n° 413355, par une requête enregistrée le 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, d'enjoindre au ministre d'Etat, ministre de l'intérieur, d'interdire la tenue du " Camp d'été décolonial " dont l'organisation serait projetée du 12 au 16 août 2017 ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au ministre d'Etat, ministre de l'intérieur de prescrire son ouverture à tous, sans distinction de couleur de peau ou d'origine ethnique ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 euro au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              M. A... soutient que : <br/>
              - dès lors que le lieu de la réunion projetée est inconnu, les pouvoirs de police à son égard ne peuvent être exercés par les préfets mais relèvent du Premier ministre et du ministre d'Etat, ministre de l'intérieur et qu'en conséquence le Conseil d'Etat est compétent pour statuer en premier ressort sur la requête ; <br/>
              - il dispose d'un intérêt à agir en tant que citoyen français de type " caucasien ", exclu en cette qualité du " Camp d'été décolonial " en raison de la discrimination raciale dont il fait l'objet ;<br/>
              - la condition d'urgence est remplie dès lors que le " Camp d'été décolonial " débutera le 12 août 2017 et se déroulera jusqu'au 16 août 2017 ;<br/>
              - la manifestation litigieuse porte une atteinte grave et manifestement illégale au droit au respect de la dignité humaine, au droit de ne pas être victime de discriminations raciales dans la fourniture d'un bien ou d'un service, au respect de l'égalité entre les personnes et notamment entre les citoyens sans aucune distinction de race ou d'ethnie, aux valeurs et principes de la République ainsi qu'à la cohésion nationale ;<br/>
              - le risque que des infractions pénales, notamment au sens des articles 225-1 et 225-2 du code pénal, soient commises justifie également de prévenir leur commission en interdisant la réunion litigieuse.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code pénal ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en particulier, le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article ; <br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; <br/>
<br/>
              3. Considérant qu'il résulte des éléments produits au soutien des requêtes qu'a été annoncée au moyen d'un site internet l'organisation d'un " camp d'été " réservé exclusivement " aux personnes subissant à titre personnel le racisme d'Etat  en contexte français ", devant se tenir du 12 au 16 août 2017, sans que le lieu envisagé pour cette réunion soit rendu public ; que cette réunion consiste, selon les indications mentionnées sur ce site internet, à organiser pendant quelques jours des " espaces de transmission, de rencontres et de formation politique " au sujet de " l'antiracisme politique " ; que l'objectif affiché par les organisateurs de cette réunion est de " se former, de partager et de [se] renforcer pour les luttes et mobilisations à venir " ; que la participation à ce regroupement nécessite de se conformer à une procédure d'inscription préalable sur le site internet en cause et de contribuer à la couverture des frais de l'évènement ; que M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au Premier ministre ou au ministre d'Etat, ministre de l'intérieur de prononcer l'interdiction de cette réunion ou, à défaut, de prescrire son ouverture à tous, sans distinction de couleur de peau ou d'origine ;<br/>
<br/>
              4. Considérant que la simple abstention, en l'état, du Premier ministre et du ministre d'Etat, ministre de l'intérieur de faire de leur propre initiative usage ou de prescrire aux préfets de faire usage de leurs pouvoirs de police administrative en vue d'interdire une réunion ou d'enjoindre à ses organisateurs de respecter certaines prescriptions, à la suite et sur le seul fondement d'éléments mentionnés dans une annonce faite sur un site internet, au motif que l'accès à cette réunion, dont ni le contenu exact ni le lieu ne sont connus et dont il n'est pas établi, compte tenu des modalités d'organisation indiquées sur ce site, qu'elle revêtirait le caractère d'une manifestation publique, serait exclusivement réservé à une catégorie de personnes déterminée ne constitue pas par elle-même une atteinte manifestement illégale portée par une autorité administrative à une liberté fondamentale qu'il appartiendrait au juge administratif des référés de faire cesser ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que les requêtes formées par M. A... ne peuvent, en l'état, qu'être rejetées, y compris en ce qui concerne les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les requêtes de M. A...sont rejetées. <br/>
Article 2 : La présente ordonnance sera notifiée à M.B..., au Premier ministre et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
