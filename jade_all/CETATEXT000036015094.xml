<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036015094</ID>
<ANCIEN_ID>JG_L_2017_11_000000396589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/01/50/CETATEXT000036015094.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 08/11/2017, 396589</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396589.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              La société Lyonnaise des Eaux France a demandé au tribunal administratif de Nice, d'une part, d'interpréter les stipulations du contrat de délégation du service public d'assainissement collectif sur le territoire des communes de La Roquette-sur-Siagne, Théoule-sur-Mer, Auribeau-sur-Siagne et Cannes, conclu le 24 juillet 2008 avec le syndicat intercommunal d'assainissement du bassin cannois (SIABC), comme autorisant le délégataire à percevoir le produit de la " part fonctionnement " des contributions dues par les communes du Cannet, de Mandelieu-la-Napoule, de Mougins et de Pégomas, en application des conventions de déversement qu'elles avaient conclues entre 1968 et 1985 avec la commune de Cannes et, d'autre part, de mettre à la charge du syndicat intercommunal d'assainissement unifié du bassin cannois (SIAUBC), qui s'est substitué au SIABC, le montant de la contribution pour l'aide juridique dont elle s'est acquittée dans le cadre de cette instance.<br/>
<br/>
              Par un jugement n° 1304878 du 21 novembre 2014, le tribunal administratif de Nice a rejeté la demande de la société Lyonnaise des Eaux France et mis à sa charge la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts acquittée par le SIAUBC.<br/>
<br/>
              Par un arrêt n° 14MA04840 du 14 décembre 2015, la cour administrative d'appel de Marseille a annulé l'article 2 de ce jugement mettant à sa charge la contribution pour l'aide juridique acquittée par le SIAUBC et rejeté le surplus des conclusions d'appel de la société Lyonnaise des Eaux France.<br/>
<br/>
              Par un pourvoi sommaire et des mémoires complémentaires, enregistrés les 29 janvier, 29 avril et 28 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Lyonnaise des Eaux France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt en tant qu'il rejette le surplus de ses conclusions d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SIAUBC une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Lyonnaise des Eaux France et à la SCP Gaschignard, avocat du syndicat intercommunal d'assainissement unifié du bassin cannois.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que les communes du Cannet, de Mandelieu-la-Napoule, de Mougins et de Pégomas ont conclu avec la commune de Cannes, entre 1968 et 1985, des conventions de déversement prévoyant leur raccordement au réseau public d'assainissement de la commune de Cannes. Par un arrêté du 21 juillet 2005, le préfet des Alpes-Maritimes a créé le syndicat intercommunal d'assainissement du bassin cannois (SIABC) entre les communes de La Roquette-sur-Siagne, Théoule-sur-Mer, Auribeau-sur-Siagne et Cannes, qui s'est substitué aux communes membres pour exercer les compétences en matière d'assainissement et de traitement des eaux pluviales. Par un contrat du 21 juillet 2008, le SIABC a confié à la société Lyonnaise des Eaux France la gestion du service public d'assainissement collectif sur le territoire des communes membres du syndicat, service comprenant la collecte, le transport des eaux usées et des eaux pluviales ainsi que le traitement des eaux usées à la station d'épuration de Miramar. Par un arrêté du 20 mai 2009, le préfet des Alpes-Maritimes a créé le syndicat intercommunal d'assainissement unifié du bassin cannois (SIAUBC) regroupant, outre les communes membres du SIABC, celles du Cannet, de Mandelieu-la-Napoule, de Mougins et de Pégomas, chargé d'exercer la compétence obligatoire de traitement des eaux usées, les compétences en matière de collecte et transport des eaux usées et d'assainissement restant optionnelles. <br/>
<br/>
              2. La société Lyonnaise des Eaux France a demandé au tribunal administratif de Nice d'interpréter les stipulations du contrat de délégation du service public d'assainissement collectif conclu le 21 juillet 2008 comme autorisant le délégataire à percevoir le produit de la " part fonctionnement " des contributions dues par les communes du Cannet, de Mandelieu-la-Napoule, de Mougins et de Pégomas, en application des conventions de déversement qu'elles ont conclues entre 1968 et 1985 et qui sont annexées à ce contrat. Par un jugement du 21 novembre 2014, le tribunal administratif de Nice a rejeté cette demande. La société Lyonnaise des Eaux France se pourvoit en cassation contre l'arrêt du 14 décembre 2015 de la cour administrative d'appel de Marseille en tant qu'il a fait droit à la fin de non-recevoir opposée par le SIAUBC aux conclusions à fin d'interprétation des stipulations du contrat de délégation du service public d'assainissement collectif conclu le 21 juillet 2008.<br/>
<br/>
              3. Un recours en interprétation de stipulations contractuelles n'est recevable que dans la mesure notamment où il peut être valablement soutenu que ces stipulations sont obscures ou ambiguës.<br/>
<br/>
              4. L'article 39.1 du contrat de délégation du service public d'assainissement collectif conclu le 21 juillet 2008, intitulé " Composantes de la rémunération du service " prévoit que " Le délégataire est autorisé à appliquer aux abonnés du service un tarif qui comprend: / Pour la partie eaux usées / - sa rémunération (part fermière et contribution des communes non membres rejetant leurs effluents dans les réseaux de la Collectivité) : tarif appliqué à chaque période de facturation et qui tient compte d'une annexe du service de base ; / - la part de la Collectivité (surtaxe) : part versée par le Délégataire à la Collectivité et destinée à couvrir les charges supportées par cette dernière. (...) ". Si l'article 39.2 du même contrat relatif aux modalités de calcul des rémunérations du délégataire ne mentionne pas, parmi ces rémunérations, les contributions des communes non membres rejetant leurs effluents dans les réseaux de la Collectivité, l'article 41, relatif à l'actualisation des rémunérations du délégataire et des éléments financiers du contrat, prévoit, quant à lui, que les rémunérations des conventions spéciales de déversement sont actualisées selon les modalités propres à chaque convention. L'article 43 du même contrat, intitulé " Part de la collectivité (surtaxe) ", prévoit que " Le délégataire perçoit, pour le compte de la collectivité et sans rémunération supplémentaire, une ou des part(s) " Collectivité " qui s'ajoute(nt) à tous les tarifs perçus. (...) ". L'article 43.2 du même contrat, intitulé " Pour la part perçue sur les conventions de déversement conclues avec d'autres collectivités ", prévoit que " Le délégataire reverse à la Collectivité l'intégralité du montant de la part " Collectivité " facturée pour son compte en application des conventions de déversements conclues pour la collectivité (cf ANNEXE 6). (...) ".<br/>
<br/>
              5. En jugeant, d'une part, qu'il résulte clairement des stipulations de l'article 39.1 du contrat du 21 juillet 2008 que le délégataire n'est pas en droit de percevoir, sur leur fondement, d'autre somme que celle correspondant au produit du tarif qu'il applique aux seuls abonnés au service public des eaux usées, dont ne font pas partie les communes du Cannet, de Mandelieu-la-Napoule, de Mougins et de Pégomas et, d'autre part, que ni les stipulations de cet article 39.1 ni celles des articles 43 et 43.2 du même contrat ne peuvent, en tout état de cause, être regardées comme de nature à permettre au délégataire de percevoir le produit de tout ou partie des contributions versées par ces communes sur le fondement des conventions particulières de déversement et en écartant ainsi toute obscurité et ambiguïté sur ce point du contrat, la cour administrative d'appel en a dénaturé les stipulations ; que par suite, en rejetant comme irrecevable la demande d'interprétation présentée par la société Lyonnaise des Eaux France, la cour administrative d'appel a entaché son arrêt d'inexacte qualification juridique. Il y a donc lieu, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, d'annuler son arrêt.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération Cannes Pays de Lérins, venue aux droits du syndicat intercommunal d'assainissement unifié du bassin cannois,  la somme de 3 000 euros à verser à la société Lyonnaise des Eaux France au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Lyonnaise des Eaux France qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 14 décembre 2015 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La communauté d'agglomération Cannes Pays de Lérins versera à la société Lyonnaise des Eaux France la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la communauté d'agglomération Cannes Pays de Lérins au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Lyonnaise des eaux France et à la communauté d'agglomération Cannes Pays de Lérins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS EN INTERPRÉTATION DE STIPULATIONS CONTRACTUELLES [RJ1] - CONDITIONS - 1) STIPULATIONS OBSCURES OU AMBIGÜES - EXISTENCE [RJ2] - 2) CONTRÔLE DU JUGE DE CASSATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-03-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS EN INTERPRÉTATION. RECEVABILITÉ. - RECOURS EN INTERPRÉTATION DE STIPULATIONS CONTRACTUELLES [RJ1] - CONDITIONS - 1) STIPULATIONS OBSCURES OU AMBIGÜES - EXISTENCE [RJ2] - 2) CONTRÔLE DU JUGE DE CASSATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - RECOURS EN INTERPRÉTATION DE STIPULATIONS CONTRACTUELLES [RJ1] - CONDITION DE RECEVABILITÉ TENANT À L'EXISTENCE DE STIPULATIONS OBSCURES OU AMBIGÜES [RJ2] - CONTRÔLE EXERCÉ SUR LE CARACTÈRE RECEVABLE DU RECOURS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - RECOURS EN INTERPRÉTATION DE STIPULATIONS CONTRACTUELLES [RJ1] - CONDITION TENANT À L'EXISTENCE DE STIPULATIONS OBSCURES OU AMBIGÜES [RJ2] - APPRÉCIATION DU CARACTÈRE OBSCUR OU AMBIGU.
</SCT>
<ANA ID="9A"> 39-08 1) Un recours direct en interprétation de stipulations contractuelles n'est recevable que dans la mesure notamment où il peut être valablement soutenu que ces stipulations sont obscures ou ambiguës.,,,2) Le juge de cassation laisse à l'appréciation souveraine des juges du fond le caractère obscur ou ambigu de stipulations contractuelles et exerce un contrôle de la qualification juridique des faits sur le caractère recevable du recours.</ANA>
<ANA ID="9B"> 54-02-03-01 1) Un recours direct en interprétation de stipulations contractuelles n'est recevable que dans la mesure notamment où il peut être valablement soutenu que ces stipulations sont obscures ou ambiguës.,,,2) Le juge de cassation laisse à l'appréciation souveraine des juges du fond le caractère obscur ou ambigu de stipulations contractuelles et exerce un contrôle de la qualification juridique des faits sur le caractère recevable du recours.</ANA>
<ANA ID="9C"> 54-08-02-02-01-02 Un recours direct en interprétation de stipulations contractuelles n'est recevable que dans la mesure notamment où il peut être valablement soutenu que ces stipulations sont obscures ou ambiguës.,,,Le juge de cassation laisse à l'appréciation souveraine des juges du fond le caractère obscur ou ambigu de stipulations contractuelles et exerce un contrôle de la qualification juridique des faits sur le caractère recevable du recours.</ANA>
<ANA ID="9D"> 54-08-02-02-01-04 Un recours direct en interprétation de stipulations contractuelles n'est recevable que dans la mesure notamment où il peut être valablement soutenu que ces stipulations sont obscures ou ambiguës.,,,Le juge de cassation laisse à l'appréciation souveraine des juges du fond le caractère obscur ou ambigu de stipulations contractuelles et exerce un contrôle de la qualification juridique des faits sur le caractère recevable du recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'existence d'un tel recours, CE, Section, 3 juillet 1959, Ministre des Travaux publics et SNCF c/ Société des Produits alimentaires et diététiques et Association nationale des propriétaires et usagers d'embranchements particuliers, n°s 16358 31455, p. 422 ; pour un cas d'application CE, Assemblée, 21 décembre 2012, Commune de Douai, n° 342788, p. 479.,,[RJ2] Rappr., s'agissant du recours en interprétation d'une décision juridictionnelle, CE, 28 novembre 1934, Ville de Bagnères-de-Luchon, p. 1122 ;,CE, Assemblée, 7 juillet 1950, Secrétaire d'Etat à la présidence du Conseil, p. 427 ; CE, 14 novembre 1956, Sieur,, p. 431 ; CE, 16 mai 1975, Ministre de l'équipement c/ consorts,, n° 96229, T. p. 1209 ; CE, 13 mars 2013, Département de Tarn-et-Garonne c/ Epoux,, n° 339943, T. pp. 759-788-805 ; CE, 27 juillet 2016, M.,, n° 388098, T. p. 873.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
