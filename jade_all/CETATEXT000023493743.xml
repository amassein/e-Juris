<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023493743</ID>
<ANCIEN_ID>JG_L_2010_12_000000316022</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/49/37/CETATEXT000023493743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 30/12/2010, 316022, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316022</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Cécile  Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Collin Pierre</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 mai et 9 juillet 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL VERIMEX, dont le siège est 44 rue de Salon à Biarritz (64200), représentée par son gérant ; la SARL VERIMEX demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°06BX00389 du 10 mars 2008 par lequel la cour administrative d'appel de Bordeaux, réformant le jugement du 6 décembre 2005 du tribunal administratif de Pau rejetant ses demandes tendant à la décharge, d'une part, des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre des exercices clos en 1995, 1996 et 1997, d'autre part, des rappels de taxe sur la valeur ajoutée réclamés pour la période allant du 1er janvier 1996 au 31 décembre 1997 ainsi que des pénalités correspondantes, a déchargé la société des rappels de taxe sur la valeur ajoutée mentionnés ci-dessus à raison des prestations facturées à la société Idemat en rémunération de prestations rendues en vue de la livraison de biens à l'exportation et a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la SARL VERIMEX, <br/>
<br/>
              - les conclusions de M. Pierre Collin, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la SARL VERIMEX ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL VERIMEX, qui exerce une activité d'intermédiaire entre des fournisseurs français ou étrangers et des entreprises algériennes, a fait l'objet d'une vérification de comptabilité qui a porté sur la période comprise entre le 1er janvier 1995 et le 31 décembre 1997 ; que des redressements d'impôt sur les sociétés lui ont été notifiés en raison notamment de la réintégration de diverses charges dans ses résultats, portant sur des commissions excessives, des salaires fictifs et des frais de voyage non exposés dans l'intérêt de l'entreprise ; que des rappels de taxe sur la valeur ajoutée lui ont également été notifiés notamment à la suite de la remise en cause de l'exonération instituée au profit des livraisons intracommunautaires ; que la SARL VERIMEX se pourvoit en cassation contre l'arrêt du 10 mars 2008 par lequel la cour administrative d'appel de Bordeaux, réformant le jugement du 6 décembre 2005 du tribunal administratif de Pau, n'a que partiellement fait droit à sa requête tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et des rappels de taxe sur la valeur ajoutée qui lui ont été assignés à la suite de la vérification de comptabilité dont elle a fait l'objet ;<br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              Considérant que pour juger que la société requérante ne pouvait être regardée comme ayant été privée de la possibilité d'un débat oral et contradictoire, la cour s'est fondée sur la circonstance, d'une part, qu'après avoir demandé à ce que le contrôle se déroule dans les locaux de l'administration et s'être vu remettre par le vérificateur, le jour du début des opérations de contrôle, un reçu détaillé des pièces emportées, le gérant de la société avait été convoqué à trois reprises dans les locaux de l'administration et s'était rendu à ces rendez-vous qui avaient donné lieu à un échange sur les conditions particulières de l'exploitation de la société et, d'autre part, que le vérificateur s'était rendu dans les locaux de la société à plusieurs reprises, dont une troisième fois le 17 décembre 1998 ; qu'en relevant que la notification des redressements consécutifs au contrôle, bien que datée du 14 décembre, avait été envoyée postérieurement à l'entretien du 17 décembre, la cour a entendu ainsi juger que l'administration fiscale avait souhaité pouvoir prendre en considération les éléments susceptibles d'être apportés à l'occasion de cet entretien ; que, par suite, la SARL VERIMEX n'est pas fondée à soutenir que la cour n'aurait pas tiré toutes les conséquences légales des constatations auxquelles elle s'était livrée, au vu des pièces du dossier qui lui était soumis ;<br/>
<br/>
              Sur le bien-fondé des suppléments d'impôt sur les sociétés :<br/>
<br/>
              Considérant qu'aux termes de l'article 39 du code général des impôts, applicable pour la détermination de l'impôt sur les sociétés en vertu de l'article 209 du même code : 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant, sous réserve des dispositions du 5, notamment : / 1° Les frais généraux de toute nature, les dépenses de personnel et de main-d'oeuvre (...) / Toutefois, les rémunérations ne sont admises en déduction des résultats que dans la mesure où elles correspondent à un travail effectif et ne sont pas excessives eu égard à l'importance du service rendu. Cette disposition s'applique à toutes les rémunérations directes ou indirectes, y compris les indemnités, allocations, avantages en nature et remboursement de frais (...). / 5. Sont également déductibles les dépenses suivantes : / a) les rémunérations directes et indirectes, y compris les remboursements de frais versés aux personnes les mieux rémunérées ; / b) les frais de voyage et de déplacements exposés par ces personnes (...) / Les dépenses ci-dessus énumérées peuvent également être réintégrées dans les bénéfices imposables dans la mesure où elles sont excessives et où la preuve n'a pas été apportée qu'elles ont été engagées dans l'intérêt direct de l'entreprise (...) ;<br/>
<br/>
              Considérant que si, en vertu des règles gouvernant l'attribution de la charge de la preuve devant le juge administratif, applicables sauf loi contraire, il incombe, en principe, à chaque partie d'établir les faits qu'elle invoque au soutien de ses prétentions, les éléments de preuve qu'une partie est seule en mesure de détenir ne sauraient être réclamés qu'à celle-ci ; qu'il appartient, dès lors, au contribuable, pour l'application des dispositions précitées du code général des impôts, de justifier tant du montant des charges qu'il entend déduire du bénéfice net défini à l'article 38 du code général des impôts que de la correction de leur inscription en comptabilité, c'est-à-dire du principe même de leur déductibilité ; que le contribuable apporte cette justification par la production de tous éléments suffisamment précis portant sur la nature de la charge en cause, ainsi que sur l'existence et la valeur de la contrepartie qu'il en a retirée ; que dans l'hypothèse où le contribuable s'acquitte de cette obligation, il incombe ensuite au service, s'il s'y croit fondé, d'apporter la preuve de ce que la charge en cause n'est pas déductible par nature, qu'elle est dépourvue de contrepartie, qu'elle a une contrepartie dépourvue d'intérêt pour le contribuable ou que la rémunération de cette contrepartie est excessive ;<br/>
<br/>
              Considérant qu'en écartant le moyen tiré de ce que l'administration aurait à tort réintégré les salaires de Mme Arrayet, épouse du gérant de la SARL VERIMEX, dans les résultats de cette société au motif que celle-ci n'avait produit aucun document de nature à établir la réalité des fonctions exercées par Mme Arrayet et que ni l'existence d'un contrat de travail ni la production d'attestations établies a posteriori par des entreprises placées en situation de relations d'affaires avec la société requérante n'établissaient la réalité du travail rémunéré, la cour administrative d'appel n'a pas commis d'erreur de droit dans l'attribution de la charge de la preuve ;<br/>
<br/>
              Considérant que, pour écarter le moyen tiré de ce que l'administration aurait, à tort, réintégré une partie des commissions versées par la société à son gérant, la cour s'est, pour les exercices clos en 1994 et 1996, fondée sur la circonstance que la société n'apportait pas d'éléments précis quant aux prestations d'intermédiaire que le gérant aurait accomplies à son profit et, pour l'exercice clos en 1995, sur le fait que ses allégations générales n'expliquaient pas la différence constatée entre le taux de commission pratiqué par la société à l'égard de ses clients et celui pratiqué par son dirigeant à son égard ; que, contrairement à ce que soutient la société requérante, la cour n'a aucunement jugé que l'absence de production de notes d'honoraires faisait obstacle à la comptabilisation en charges des commissions litigieuses au-delà du taux utilisé par la société avec ses autres commissionnaires ; qu'il suit de là que le moyen tiré de ce que, ce faisant, la cour administrative d'appel de Bordeaux aurait entaché son arrêt d'erreur de droit et de défaut de base légale ne peut qu'être écarté ;<br/>
<br/>
              Considérant que c'est par une appréciation souveraine des faits, exempte de dénaturation, que la cour administrative d'appel de Bordeaux a jugé que les dépenses de voyage et de déplacement supportées par la SARL VERIMEX n'avaient pas été exposées dans l'intérêt direct de celle-ci ;<br/>
<br/>
              Sur les pénalités dont ont été assortis les suppléments d'impôt sur les sociétés :<br/>
<br/>
              Considérant qu'en relevant le caractère systématique des minorations par la SARL VERIMEX de son bénéfice déclaré, la cour administrative d'appel a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ; qu'en déduisant de ces constatations que l'administration devait être regardée comme apportant la preuve de l'absence de bonne foi de la société requérante, la cour a exactement qualifié les faits de la cause ;<br/>
<br/>
              Sur les rappels de taxe sur la valeur ajoutée :<br/>
<br/>
              Considérant qu'aux termes de l'article 289 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige : I. Tout assujetti doit délivrer une facture ou un document en tenant lieu pour les biens livrés ou les services rendus à un autre assujetti ou à une personne morale non assujettie, ainsi que pour les acomptes perçus au titre de ces opérations lorsqu'ils donnent lieu à exigibilité de la taxe. / Tout assujetti doit également délivrer une facture ou un document en tenant lieu pour les livraisons de biens visées aux articles 258 A et 258 B et pour les livraisons de biens exonérées en application du I de l'article 262 ter et du II de l'article 298 sexies, ainsi que pour les acomptes perçus au titre de ces opérations. / (...) / L'assujetti doit conserver un double de tous les documents émis. / II. La facture ou le document en tenant lieu doit faire apparaître : (...) / 2° Les numéros d'identification à la taxe sur la valeur ajoutée du vendeur et de l'acquéreur pour les livraisons désignées au I de l'article 262 ter et la mention Exonération T.V.A., article 262 ter I du code général des impôts (...) ;<br/>
<br/>
              Considérant que si la mention du numéro d'identification à la taxe sur la valeur ajoutée de l'acquéreur, prévue à l'article 289 du code général des impôts, permet de présumer que les biens ont été livrés à un autre assujetti à cette taxe, l'absence de mention de ce numéro sur une facture ne saurait entraîner à elle seule la perte du droit à exonération prévu au I de l'article 262 ter du code général des impôts pour les livraisons intracommunautaires ; qu'il appartient toutefois dans ce cas à l'assujetti d'apporter la preuve par tout moyen de ce que l'acquéreur était effectivement identifié à la taxe sur la valeur ajoutée au moment où les biens ont été livrés ;<br/>
<br/>
              Considérant qu'en jugeant que l'administration avait pu à bon droit remettre en cause l'exonération de taxe sur la valeur ajoutée au titre de certaines livraisons intracommunautaires au motif que la SARL VERIMEX, qui n'avait pas indiqué sur les factures établies au nom de l'un de ses clients espagnols le numéro d'identification à la taxe sur la valeur ajoutée de celui-ci, avait fourni à l'administration, à l'issue du contrôle dont elle avait fait l'objet, un numéro d'identification erroné, et alors qu'elle n'avait produit aucun autre élément de nature à établir que ce client était bien identifié à la taxe sur la valeur ajoutée, la cour administrative d'appel de Bordeaux n'a ni commis d'erreur de droit ni déduit des dispositions précitées de l'article 289 du code général des impôts une règle excessive, de nature à rendre pratiquement impossible ou excessivement difficile l'exercice du droit à exonération ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de cet article font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande la SARL VERIMEX au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la SARL VERIMEX est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SARL VERIMEX et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
