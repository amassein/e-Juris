<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042519145</ID>
<ANCIEN_ID>JG_L_2020_11_000000428931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/51/91/CETATEXT000042519145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/11/2020, 428931</TITRE>
<DATE_DEC>2020-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Cécile Vaullerin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428931.20201112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le conseil régional de l'ordre des architectes de Bretagne a demandé à la chambre régionale de discipline de Bretagne de sanctionner M. B... D..., architecte, à raison d'agissements contraires aux articles 41 du décret n° 77-1481 du 28 décembre 1977, aux articles 3 et 15 de la loi n° 77-2 du 3 janvier 1977 et aux articles 5, 11 et 28 du code de déontologie des architectes. <br/>
<br/>
              Par une décision du 15 décembre 2017, la chambre régionale de discipline a prononcé à l'encontre de M. D... la sanction de suspension pour une durée d'un an du tableau régional de l'ordre assortie de la publication à sa charge de la décision dans le bulletin du conseil régional de l'ordre des architectes de Bretagne et du paiement des frais engagés par ce conseil ainsi que l'indemnité versée au gestionnaire. <br/>
<br/>
              Par une décision n° 2018-190 du 18 janvier 2019, la chambre nationale de discipline des architectes a, sur appel de M. D..., annulé partiellement ce jugement en prononçant à l'encontre de l'appelant, la sanction de suspension de l'inscription au tableau régional des architectes pour une durée d'un an assortie d'un sursis de neuf mois. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 mars et 5 juin 2019 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du conseil régional de l'ordre des architectes de Bretagne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales<br/>
              - le code de déontologie des architectes ;<br/>
              - la loi n° 77-2 du 3 janvier 1977 ;<br/>
              - le décret n° 77-1481 du 28 décembre 1977 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... C..., auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. D... et à la SCP Boulloche, avocat du conseil régional de l'ordre des architectes de Bretagne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, sur saisine du conseil régional de l'ordre de Bretagne, la chambre régionale de discipline a, par une décision du 15 décembre 2017, prononcé à l'encontre de M. D..., architecte, la sanction de la suspension du tableau régional des architectes pour une durée d'un an. Par une décision du 18 janvier 2019, contre laquelle M. D... se pourvoit en cassation, la chambre nationale de discipline a ramené la sanction à une suspension du tableau régional des architectes pour une durée d'un an assortie d'un sursis de neuf mois.<br/>
<br/>
              Sur la procédure devant le conseil régional de l'ordre des architectes de Bretagne :<br/>
<br/>
              2. Aux termes de l'article 28 de la loi du 3 janvier 1977 sur l'architecture : " La chambre régionale de discipline des architectes peut prononcer les sanctions suivantes : /   avertissement ; / - blâme ; / - suspension, avec ou sans sursis, de l'inscription au tableau régional des architectes pour une période de trois mois à trois ans ; / - radiation du tableau régional des architectes. " L'article 44 du décret du 28 décembre 1977 sur l'organisation de la profession d'architecte précise que : " La procédure devant la chambre régionale de discipline est écrite et contradictoire. / Le secrétaire de la chambre régionale de discipline qui est saisie de la plainte procède à son enregistrement et, si elle est recevable, la notifie dans un délai de quinze jours à l'architecte poursuivi, sous le contrôle du président. Il adresse à l'architecte poursuivi, par lettre recommandée avec demande d'avis de réception, une copie intégrale de la plainte. / Cette lettre précise à l'intéressé qu'il a la possibilité de se faire assister tout au long de la procédure par un architecte, un avocat ou par l'un et l'autre. (...) ". Aux termes de l'article 45 du même décret : " Dès réception de la plainte, qui doit être motivée, le président désigne, parmi les trois architectes membres de la chambre régionale de discipline, un rapporteur (...). ". L'article 46 du même décret précise que : " le rapporteur (...) transmet, dans les trois mois de sa désignation, son rapport au président de la chambre régionale de discipline ou rend compte des motifs qui l'empêchent de respecter ce délai. Dans ce cas, le président peut soit prolonger le délai, soit dessaisir le rapporteur et en désigner un autre. (...) ". Enfin, aux termes de l'article 47 du même décret : " Le dossier de l'affaire comprenant, notamment, le rapport du rapporteur, est tenu à la disposition de l'architecte poursuivi et de son ou ses défenseurs, sans déplacement de pièces, au secrétariat de la chambre régionale de discipline, dix jours calendaires avant la date de l'audience. ".<br/>
<br/>
              3. En premier lieu, si le principe des droits de la défense garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales s'applique à la procédure de sanction ouverte par la notification des griefs et non à la phase préalable à la saisine de la chambre régionale de discipline, cette phase préalable ne saurait, sans entacher d'irrégularité la sanction prise au terme de l'instance juridictionnelle, porter par avance une atteinte irrémédiable aux droits de la défense des personnes qui font l'objet d'une procédure de sanction. En l'espèce, si le requérant soutient qu'il n'a été informé, pendant la phase préalable d'enquête administrative, ni de ce que les éléments recueillis au cours de son audition par la commission de déontologie étaient susceptibles d'être utilisés dans le cadre d'une procédure disciplinaire, ni qu'il pouvait se faire assister par un conseil, d'une part aucun texte n'imposait une telle information et, d'autre part, cette circonstance n'est pas de nature à avoir porté, par avance, une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ayant donné lieu à la sanction attaquée. Par suite, la chambre nationale de discipline a pu, sans erreur de droit, juger que les irrégularités invoquées concernant la procédure administrative préalable étaient sans incidence sur la régularité de la procédure juridictionnelle.  <br/>
<br/>
              4. En deuxième lieu, contrairement à ce que soutient M. D..., les échanges devant la commission de déontologie ne peuvent, ainsi qu'il a été dit au point précédent, être assimilés à une procédure de sanction. Par suite, les moyens tirés de la méconnaissance du principe de non cumul des poursuites et des sanctions et de l'erreur de droit qu'aurait commise la chambre nationale de discipline de l'ordre en ne relevant pas d'office cette irrégularité ne peuvent, en tout état de cause, qu'être écartés. <br/>
<br/>
              5. En dernier lieu, si le rapport du rapporteur devant la chambre régionale de discipline doit, en principe, être déposé trois mois après sa saisie, cette exigence, qui n'est pas prescrite à peine d'irrégularité de la procédure, a pour objet de garantir un délai raisonnable de la procédure. Dans ces conditions, c'est sans erreur de droit que la chambre nationale de discipline a jugé que la procédure suivie devant la chambre régionale de discipline n'était pas entachée d'irrégularité nonobstant la circonstance que le rapport du rapporteur avait été déposé plus de trois mois après sa saisine.<br/>
<br/>
              Sur les manquements reprochés :<br/>
<br/>
              6. Aux termes de l'article 3 de la loi du 3 janvier 1977 sur l'architecture : " quiconque désire entreprendre des travaux soumis à une autorisation de construire doit faire appel à un architecte pour établir le projet architectural faisant l'objet de la demande de permis de construire, sans préjudice du recours à d'autres personnes participant soit individuellement soit en équipe à la conception (...). Le projet architectural mentionné ci-dessus définit par des plans et documents écrits l'implantation des bâtiments, leur composition, leur organisation et l'expression de leur volume ainsi que le choix des matériaux et des couleurs (...) ". Aux termes de l'article 5 du décret du 20 mars 1980 portant code de déontologie des architectes : " un architecte qui n'a pas participé à l'élaboration d'un projet ne peut en aucun cas y apposer sa signature ni prétendre à une rémunération à ce titre ; la signature de complaisance est interdite. Le nom et les titres de tout architecte qui ont effectivement participé à l'élaboration d'un projet doivent être explicitement mentionnés après accord de l'intéressé sur les éléments de ce projet auxquels il a participé. " Aux termes de l'article 16 du même code : " le projet architectural mentionné à l'article 3 de la loi sur l'architecture relatif au recours obligatoire à l'architecte, comporte au moins les documents graphiques et écrits définissant : / - l'insertion au site, au relief et l'adaptation au climat ; / - l'implantation du ou des bâtiments compte tenu de l'alignement, de la marge de recul, des prospects et des niveaux topographiques ; / - la composition du ou des bâtiments : plans de masse précisant la disposition relative des volumes ; / - l'organisation du ou des bâtiments : plans et coupes faisant apparaître leur distribution, leur fonction, leur utilisation, leurs formes et leurs dimensions ; / - l'expression des volumes : élévations intérieures et extérieures précisant les diverses formes des éléments et leur organisation d'ensemble ; / - le choix des matériaux et des couleurs. <br/>
<br/>
              7. Pour rechercher si M. D... avait fourni une contribution effective aux projets architecturaux qu'il a signés, la chambre nationale de discipline, a relevé, par une appréciation souveraine dénuée de dénaturation, que ses honoraires avaient été en l'espèce particulièrement faibles, que les documents graphiques avaient été élaborés par des bureaux d'études et que si l'intéressé, qui s'était déplacé sur les lieux, soutenait avoir réalisé des esquisses, aucune n'était produite à l'instance. Par suite, les moyens tirés de ce qu'elle aurait, ce faisant, entaché sa décision d'une dénaturation et d'une erreur de droit doivent être écartés. <br/>
<br/>
              Sur la proportionnalité de la sanction :<br/>
<br/>
              8. L'article 28 de la loi du 3 janvier 1977 sur l'architecture dispose que : " La chambre régionale de discipline des architectes peut prononcer les sanctions suivantes : /   avertissement ; / - blâme ; / - suspension, avec ou sans sursis, de l'inscription au tableau régional des architectes pour une période de trois mois à trois ans ; / - radiation du tableau régional des architectes. (...) ".<br/>
<br/>
              9. Le manquement relevé par la chambre nationale de discipline est de nature à justifier une sanction disciplinaire au regard des dispositions de l'article 5 du code de déontologie des architectes. En retenant une sanction de suspension de l'inscription du requérant au tableau régional de l'ordre d'une durée d'un an, assortie d'un sursis de neuf mois, la chambre régionale de l'ordre a prononcé une sanction qui n'est pas hors de proportion par rapport au manquement en cause. Le moyen tiré de la disproportion de la sanction doit donc être écarté. <br/>
<br/>
              10. Il résulte de tout ce qu'il précède que le pourvoi de M. D... doit être rejeté, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sur les conclusions du conseil régional de l'ordre des architectes de Bretagne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. D... la somme de 1 500 euros à verser au conseil régional de l'ordre des architectes de Bretagne au titre de ces dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. D... est rejeté.<br/>
Article 2 : M. E... versera au conseil régional de l'ordre des architectes de Bretagne une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Monsieur B... D..., au conseil national de l'ordre des architectes et au conseil régional de l'ordre des architectes de Bretagne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. - PHASE PRÉALABLE À LA SAISINE DE LA JURIDICTION ORDINALE - 1) APPLICATION DE L'ARTICLE 6 DE LA CONVENTION EDH - ABSENCE [RJ1] - INTERDICTION D'UNE ATTEINTE IRRÉMÉDIABLE AUX DROITS DE LA DÉFENSE - EXISTENCE [RJ1] - 2) ABSENCE D'INFORMATION DE L'INTÉRESSÉ SUR LES SUITES POUVANT ÊTRE DONNÉES À UNE AUDITION PAR LA COMMISSION DE DÉONTOLOGIE ET LA POSSIBILITÉ DE S'Y FAIRE ASSISTER PAR UN AVOCAT - ATTEINTE IRRÉMÉDIABLE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. RÉGULARITÉ. - PHASE PRÉALABLE À LA SAISINE DE LA JURIDICTION ORDINALE - 1) APPLICATION DE L'ARTICLE 6 DE LA CONVENTION EDH - ABSENCE [RJ1] - INTERDICTION D'UNE ATTEINTE IRRÉMÉDIABLE AUX DROITS DE LA DÉFENSE - EXISTENCE [RJ1] - 2) ABSENCE D'INFORMATION DE L'INTÉRESSÉ SUR LES SUITES POUVANT ÊTRE DONNÉES À UNE AUDITION PAR LA COMMISSION DE DÉONTOLOGIE ET LA POSSIBILITÉ DE S'Y FAIRE ASSISTER PAR UN AVOCAT - ATTEINTE IRRÉMÉDIABLE - ABSENCE.
</SCT>
<ANA ID="9A"> 55-04-01 1) Si le principe des droits de la défense garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH) s'applique à la procédure de sanction ouverte par la notification des griefs et non à la phase préalable à la saisine de la chambre régionale de discipline de l'ordre des architectes, cette phase préalable ne saurait, sans entacher d'irrégularité la sanction prise au terme de l'instance juridictionnelle, porter par avance une atteinte irrémédiable aux droits de la défense des personnes qui font l'objet d'une procédure de sanction.... ,,2) La seule circonstance que la personne poursuivie n'ait été informée, pendant la phase préalable d'enquête administrative, ni que les éléments recueillis au cours de son audition par la commission de déontologie étaient susceptibles d'être utilisés dans le cadre d'une procédure disciplinaire, ni qu'elle pouvait se faire assister par un conseil, n'est pas de nature à avoir porté, par avance, une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ayant donné lieu à la sanction prononcée par la chambre de discipline.</ANA>
<ANA ID="9B"> 59-02-02-02 1) Si le principe des droits de la défense garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH) s'applique à la procédure de sanction ouverte par la notification des griefs et non à la phase préalable à la saisine de la chambre régionale de discipline de l'ordre des architectes, cette phase préalable ne saurait, sans entacher d'irrégularité la sanction prise au terme de l'instance juridictionnelle, porter par avance une atteinte irrémédiable aux droits de la défense des personnes qui font l'objet d'une procédure de sanction.... ,,2) La seule circonstance que la personne poursuivie n'ait été informée, pendant la phase préalable d'enquête administrative, ni que les éléments recueillis au cours de son audition par la commission de déontologie étaient susceptibles d'être utilisés dans le cadre d'une procédure disciplinaire, ni qu'elle pouvait se faire assister par un conseil, n'est pas de nature à avoir porté, par avance, une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ayant donné lieu à la sanction prononcée par la chambre de discipline.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des enquêtes réalisées par les agents de l'Autorité des marchés financiers, CE, 15 mai 2013, Société Alternative Leaders France, n° 356054, T. pp. 453-597-742.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
