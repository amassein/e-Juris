<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768110</ID>
<ANCIEN_ID>JG_L_2012_12_000000329821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768110.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 12/12/2012, 329821, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:329821.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 juillet et 19 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Annie B, demeurant ... ; Mme B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06DA00292 du 14 mai 2009 de la cour administrative d'appel de Douai en tant qu'après avoir prononcé un non lieu à statuer sur les conclusions de sa requête tendant à la décharge des intérêts légaux, d'un montant de 33 178,19 euros, auxquels elle a été assujettie et lui avoir accordé la décharge partielle de la cotisation supplémentaire d'impôt sur le revenu et des pénalités auxquelles elle a été assujettie au titre de l'année 1995, il a rejeté le surplus de ses conclusions dirigées contre le jugement n° 0000622-0101327-0200683-0300732 du tribunal administratif de Rouen du 20 décembre 2005 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit, dans cette mesure, à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de Mme B,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de Mme B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme Annie B a acquis, le 10 décembre 1992, 150 000 bons de souscription autonomes de la société anonyme Square au prix de 2 400 000 francs, cédés par la suite le 15 décembre 1995 à la société Stell au prix de 40 000 000 francs ; que l'administration fiscale, constatant à l'occasion d'une vérification de comptabilité de la société Square que l'intéressée s'était abstenue de déclarer la plus-value réalisée lors de cette cession, l'a mise en demeure le 30 juillet 1997 de souscrire l'imprimé relatif aux plus-values immobilières ; que la contribuable a donné suite à cette demande le 29 août 1997 en assortissant sa déclaration de réserves sur le bien-fondé de cette imposition ; que l'administration fiscale a mis à sa charge une cotisation supplémentaire d'impôt sur le revenu au titre de l'année 1995, notamment à raison de l'imposition entre ses mains, en application des dispositions de l'article 150 A bis du code général des impôts, de la plus-value de cession ainsi réalisée ;<br/>
<br/>
              2. Considérant qu'après le rejet de ses réclamations contentieuses dirigées contre cette imposition supplémentaire, Mme B a porté le litige devant le juge de l'impôt ; qu'elle se pourvoit en cassation contre l'arrêt du 14 mai 2009 de la cour administrative d'appel de Douai, en tant qu'après avoir prononcé la décharge partielle des cotisations supplémentaires d'impôt sur le revenu et des pénalités auxquelles elle a été assujettie au titre de l'année 1995, il a rejeté le surplus de ses conclusions tendant à la décharge de la fraction de cette cotisation, résultant de l'imposition de la plus-value de cession réalisée en 1995 et des intérêts de retard correspondants ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux. " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le mémoire en réplique du ministre du 29 janvier 2008 se bornait à conclure, à nouveau, au rejet de la requête, en précisant que les précédentes écritures adressées par la requérante à la juridiction d'appel n'appelaient pas d'observations de sa part ; que ce mémoire ne comportait donc aucun moyen de droit ou de fait ; qu'ainsi Mme B n'est pas fondée à soutenir que, faute de communication de ce mémoire, l'arrêt attaqué serait intervenu en méconnaissance tant des dispositions précitées de l'article R. 611-1 du code de justice administrative que du caractère contradictoire de la procédure ;<br/>
<br/>
              5. Considérant, en second lieu, que, pour écarter la contestation de Mme B tirée de ce qu'elle n'aurait pas été informée, avant leur mise en recouvrement, du montant des intérêts de retard mis à sa charge, la cour s'est fondée sur une lettre, datée du 7 août 1998, par laquelle l'administration avait porté le montant de ces intérêts à sa connaissance ; qu'il ressort des pièces du dossier d'appel et qu'il n'est pas contesté que ce document a été transmis à la cour, à sa demande, par l'administration et qu'il n'a pas été communiqué à la requérante ; qu'ainsi, l'arrêt attaqué a été rendu en méconnaissance du caractère contradictoire de la procédure ; qu'il doit, par suite, être annulé en tant  qu'il statue sur l'application des intérêts de retard ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              En ce qui concerne la régularité de la procédure d'imposition :<br/>
<br/>
              6. Considérant, en premier lieu, que selon l'article L. 57 du livre des procédures fiscales, dans sa rédaction applicable à la procédure en litige : " L'administration adresse au contribuable une notification de redressement qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. (...) " ; qu'aux termes de l'article R. 57-1 du même livre, dans sa rédaction alors applicable : " La notification de redressement prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs du redressement envisagé. (...) " ; qu'il résulte de ces dispositions que, pour être régulière, une notification de redressement doit comporter la désignation de l'impôt concerné, de l'année d'imposition et de la base d'imposition, et énoncer les motifs sur lesquels l'administration entend se fonder pour justifier les redressements envisagés, de façon à permettre au contribuable de formuler utilement ses observations ; que, toutefois, aucune disposition législative ou réglementaire ne fait obligation à l'administration de mentionner les dispositions du code général des impôts sur lesquelles les redressements sont fondés ;<br/>
<br/>
              7. Considérant que la cour a relevé que la notification de redressement adressée à Mme B comportait la désignation de l'impôt en cause, de l'année d'imposition et de la base d'imposition, et énonçait les motifs sur lesquels l'administration entendait se fonder pour justifier les redressements envisagés ; que, par suite, en jugeant, au terme d'une appréciation souveraine exempte de dénaturation, que si les dispositions de l'article 150 A bis du code général des impôts n'avaient pas été citées par le vérificateur, la seule mention de cet article permettait à Mme B de formuler ses observations sur le bien-fondé de son application et en en déduisant que la notification de redressement était suffisamment motivée, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant, en second lieu, que la cour a relevé que la notification de redressement indiquait que l'actif brut de la SA Square était exclusivement constitué d'immeubles ; qu'en jugeant que, par cette mention, le vérificateur avait suffisamment informé la contribuable que le redressement envisagé s'appuyait sur la comptabilité de la société Square, la cour n'a pas dénaturé la notification de redressement ni commis d'erreur de droit ;<br/>
<br/>
              9. Considérant que la cour a également relevé que Mme B disposait du bilan de la société Square pour l'exercice clos le 31 décembre 1995 qu'elle avait elle-même produit ; qu'en déduisant de cette circonstance que la requérante n'était pas fondée à soutenir que la notification de redressement se serait appuyée sur des éléments, extérieurs à sa déclaration, qui ne lui auraient pas été rappelés et dont elle n'aurait pu disposer, la cour n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              En ce qui concerne le bien fondé des impositions supplémentaires en litige :<br/>
<br/>
              10. Considérant qu'aux termes de l'article 150 A bis du code général des impôts , dans sa rédaction alors applicable : " Les gains nets retirés de cessions à titre onéreux de valeurs mobilières ou de droits sociaux de sociétés non cotées dont l'actif est principalement constitué d'immeubles ou de droits portant sur ces biens relèvent exclusivement du régime d'imposition prévu pour les biens immeubles. Pour l'application de cette disposition, ne sont pas pris en considération les immeubles affectés par la société à sa propre exploitation industrielle, commerciale, agricole ou à l'exercice d'une profession non commerciale " ; qu'aux termes de l'article 74 A bis de l'annexe II audit code : " Pour l'application de l'article 150 A bis du code général des impôts, sont considérées comme sociétés à prépondérance immobilière les sociétés non cotées en bourse, autres que les sociétés pour le commerce et l'industrie, dont l'actif est constitué pour plus de 50 % de sa valeur par des immeubles ou des droits portant sur des immeubles, non affectés à leur propre exploitation industrielle, commerciale, agricole ou à l'exercice d'une profession non commerciale " ; que, pour l'application de ces dispositions, les immeubles affectés à l'exploitation s'entendent exclusivement des moyens permanents d'exploitation, à l'exclusion de ceux qui sont l'objet même de cette exploitation ou qui constituent des placements en capitaux ;<br/>
<br/>
              11. Considérant qu'après avoir relevé que la société Square exerçait une activité commerciale d'administrateur de biens et de syndic de copropriété, et que l'exercice de cette activité ne requérait pas que cette société soit propriétaire des immeubles dont elle assurait la gestion, la cour n'a pas fait une inexacte application des dispositions précitées de l'article 150 A bis du code général des impôts en jugeant, par un arrêt exempt de dénaturation, que les immeubles composant l'actif ne pouvaient être regardés comme affectés à l'exploitation commerciale de la société Square et que, par suite, compte tenu de la composition de cet actif, l'administration avait à bon droit qualifié la société Square de société à prépondérance immobilière au sens des dispositions précitées du code général des impôts ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que Mme B n'est fondée à demander l'annulation de l'arrêt qu'elle attaque qu'en tant que la cour s'est prononcée sur l'application des intérêts de retard à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatée en 1995 ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a  lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à Mme B, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 14 mai 2009 est annulé en tant  qu'il statue sur les conclusions relatives à l'application des intérêts de retard à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatée en 1995.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai dans la mesure de l'annulation ainsi prononcée.<br/>
Article 3 : L'Etat versera à Mme B une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi de Mme B est rejeté.<br/>
Article 5 : La présente décision sera notifiée à Mme Annie B et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
