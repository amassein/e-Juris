<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035774989</ID>
<ANCIEN_ID>JG_L_2017_10_000000401807</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/49/CETATEXT000035774989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 11/10/2017, 401807</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401807</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401807.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La Fédération des artisans et commerçants de Caen " Les vitrines de Caen ", la société Ethnika, M. A...C..., Mme B...D..., l'association des commerçants du centre commercial régional de Mondeville 2, la société les Comptoirs de l'Univers et la société Cora ont demandé à la cour administrative d'appel de Nantes d'annuler pour excès de pouvoir la décision du 1er octobre 2014 par laquelle la Commission nationale d'aménagement commercial a autorisé la société Inter Ikéa Centre Fleury à créer un ensemble commercial à Fleury-sur-Orne (Calvados). Par un arrêt nos 15NT00133, 15NT00149, 15NT00210 du 24 mai 2016, la cour administrative d'appel a rejeté leurs requêtes.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 401807, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juillet et 25 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération des artisans et commerçants de Caen " Les vitrines de Caen ", la société Ethnika, M. C...et Mme D...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la société Inter Ikea Centre Fleury la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 401809, par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 25 juillet, 25 octobre, 31 octobre 2016 et le 8 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, l'association des commerçants du centre commercial régional de Mondeville 2 et la société les Comptoirs de l'Univers demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la société Inter Ikea Centre Fleury la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2010-788 du 12 juillet 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Fédération des artisans et commerçants de Caen " Les vitrines de Caen " et autres, à la SCP Monod, Colin, Stoclet, avocat de la société Inter Ikea Centre Fleury et à la SCP Célice, Soltner, Texidor, Perier, avocat de l'association des commerçants du centre commercial régional Mondeville 2 et la société les Comptoirs de l'Univers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 septembre 2017, présentée sous les nos 401807 et 401809 par la société Inter Ikea Centre Fleury ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le pourvoi de la Fédération des artisans et commerçants de Caen " Les vitrines de Caen " et autres et le pourvoi de l'association des commerçants du centre commercial régional de Mondeville 2 et de la société les Comptoirs de l'Univers sont dirigés contre le même arrêt de la cour administrative d'appel de Nantes ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que la commune de Caen a intérêt à l'annulation de l'arrêt attaqué ; que, par suite, son intervention en cassation est recevable ;<br/>
<br/>
              3. Considérant que l'article L. 122-1 du code de l'urbanisme, dans sa rédaction issue de la loi du 4 août 2008 de modernisation de l'économie et applicable au schéma de cohérence de Caen-Métropole adopté le 20 octobre 2011 en vertu des dispositions de l'article 17 de la loi du 12 juillet 2010, dispose que les autorisations d'aménagement commercial doivent être compatibles avec les schémas de cohérence territoriale ; qu'il prévoit que, à l'exception des cas limitativement prévus par la loi dans lesquels les schémas de cohérence territoriale peuvent contenir des normes prescriptives, ces derniers doivent se borner à fixer des orientations et des objectifs ; qu'en matière d'aménagement commercial, s'il ne leur appartient pas, sous réserve des dispositions applicables aux zones d'aménagement commercial, d'interdire par des dispositions impératives certaines opérations de création ou d'extension relevant des qualifications et procédures prévues au titre V du livre VII du code de commerce, ils peuvent fixer des orientations générales et des objectifs d'implantations préférentielles des activités commerciales définis en considération des exigences d'aménagement du territoire, de protection de l'environnement ou de qualité de l'urbanisme ; que si de tels objectifs peuvent être pour partie exprimés sous forme quantitative, il appartient aux commissions d'aménagement commercial non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des schémas de cohérence territoriale, mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le document d'orientations générales du schéma de cohérence territoriale de Caen-Métropole comprend, dans sa partie relative aux activités économiques, un chapitre consacré à l'équipement commercial et à la localisation préférentielle des commerces, lequel définit des " objectifs ", des " recommandations " et des " orientations " ; qu'il précise notamment, au titre de ces orientations, que " les documents d'urbanisme devront prévoir que (...) l'implantation des commerces de détail et des ensembles commerciaux se fera prioritairement  au sein des zones urbanisées ou à défaut en continuité avec celles-ci ", que " les ensembles commerciaux portant sur une surface hors oeuvre nette de bâtiment de plus de 5 000 m² devront prévoir la réalisation du stationnement en ouvrage (...) " et que " (...) les ensembles commerciaux portant sur une surface hors oeuvre nette de bâtiment de plus de 10 000 m² devront prévoir également leur construction sur au moins deux niveaux (...) " ;<br/>
<br/>
              5. Considérant qu'il résulte des termes mêmes de l'arrêt attaqué que, pour juger que la décision du 1er octobre 2014 par laquelle la Commission nationale d'aménagement commercial a autorisé la société Inter Ikéa Centre Fleury à créer un ensemble commercial à Fleury-sur-Orne n'était pas incompatible avec le schéma de cohérence territoriale de Caen-Métropole adopté le 20 octobre 2011, la cour administrative d'appel de Nantes s'est notamment fondée sur le caractère inopérant de l'invocation des orientations de ce schéma qui étaient relatives aux documents d'urbanisme ; qu'en statuant ainsi, alors qu'il lui appartenait d'apprécier la compatibilité du projet avec les orientations générales du schéma de cohérence territoriale prises dans leur ensemble, y compris celles se présentant formellement comme régissant des actes distincts des autorisations d'exploitation commerciale, la cour administrative d'appel de Nantes a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens des pourvois, les requérants sont fondés à demander l'annulation de son arrêt en tant qu'il rejette leurs appels ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Inter Ikéa Centre Fleury, tant au titre de l'instance au fond que de l'instance de cassation, une somme de 750 euros chacun à verser à la Fédération des artisans et commerçants de Caen " Les vitrines de Caen ", à la société Ethnika, à M. C...et à Mme D... et une somme de 1 500 euros chacun à verser à l'association des commerçants du centre commercial régional de Mondeville 2 et à la société les Comptoirs de l'Univers, au titre de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Fédération des artisans et commerçants de Caen et autres ou à la charge de l'association des commerçants du centre commercial régional de Mondeville 2 et de la société les Comptoirs de l'Univers, qui ne sont pas les parties perdantes dans la présente instance ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'intervention de la commune de Caen est admise.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Nantes du 24 mai 2016 est annulé.<br/>
Article 3 : Les affaires sont renvoyées à la cour administrative d'appel de Nantes.<br/>
Article 4 : La société Inter Ikéa Centre Fleury versera une somme de 750 euros chacun à la Fédération des artisans et commerçants de Caen " Les vitrines de Caen ", à la société Ethnika, à M. C...et à Mme D...et une somme de 1 500 euros chacun à l'association des commerçants du centre commercial régional de Mondeville 2 et à la société les Comptoirs de l'Univers, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par la société Inter Ikéa Centre Fleury au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la Fédération des artisans et commerçants de Caen " Les vitrines de Caen ", représentant désigné, pour l'ensemble des requérants sous le n° 401807, à l'association des commerçants du centre commercial régional de Mondeville 2, représentant désigné, pour l'ensemble des requérants sous le n° 401809, à la commune de Caen, à la société Inter Ikea Centre Fleury et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. RÈGLES DE FOND. - OBLIGATION DE COMPATIBILITÉ DES AUTORISATIONS D'AMÉNAGEMENT COMMERCIAL AVEC LE SCOT (ART. L. 122-1 DU CODE DE L'URBANISME) - 1) CHAMP D'APPLICATION TEMPOREL - INCIDENCE DE L'ABROGATION DE L'ART. L. 122-1 SUR L'EXAMEN DES DEMANDES D'AUTORISATION - ABSENCE, DÈS LORS QUE LE DROIT D'OPTION EN FAVEUR DE LA LÉGISLATION ANTÉRIEURE OUVERT PAR LE LÉGISLATEUR A ÉTÉ EXERCÉ [RJ1] - 2) CONTRÔLE - APPRÉCIATION AU REGARD DES ORIENTATIONS GÉNÉRALES ET OBJECTIFS DU SCOT PRIS DANS LEUR ENSEMBLE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-006-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. SCHÉMAS DE COHÉRENCE TERRITORIALE. EFFETS. - OBLIGATION DE COMPATIBILITÉ DES AUTORISATIONS D'AMÉNAGEMENT COMMERCIAL AVEC LE SCOT (ART. L. 122-1 DU CODE DE L'URBANISME) - 1) CHAMP D'APPLICATION TEMPOREL - INCIDENCE DE L'ABROGATION DE L'ART. L. 122-1 SUR L'EXAMEN DES DEMANDES D'AUTORISATION - ABSENCE, DÈS LORS QUE LE DROIT D'OPTION EN FAVEUR DE LA LÉGISLATION ANTÉRIEURE OUVERT PAR LE LÉGISLATEUR A ÉTÉ EXERCÉ [RJ1] - 2) CONTRÔLE - APPRÉCIATION AU REGARD DES ORIENTATIONS GÉNÉRALES ET OBJECTIFS DU SCOT PRIS DANS LEUR ENSEMBLE [RJ2].
</SCT>
<ANA ID="9A"> 14-02-01-05-03 1) L'article 17 de la loi n° 2010-788 du 12 juillet 2010 a abrogé le dernier alinéa de l'article L. 122-1 du code de l'urbanisme en vertu duquel les autorisations d'aménagement commercial prévues par l'article L. 752-1 du code de commerce sont compatibles avec les schémas de cohérence territoriale (SCOT). Le même article a toutefois prévu que les SCOT en cours d'élaboration ou de révision approuvés avant le 1er juillet 2013 dont le projet de schéma a été arrêté par l'organe délibérant de l'établissement de coopération intercommunale (EPCI) avant 1er juillet 2012 peuvent opter pour l'application des dispositions antérieures. Par suite, applicabilité, dès lors que le droit d'option prévu par le législateur a été exercé par l'auteur du SCOT, de l'obligation de compatibilité des autorisations d'aménagement commercial avec le SCOT prévue par l'article L. 122-1 du code de l'urbanisme abrogé.... ,,2) Il appartient aux commissions d'aménagement commercial, non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des SCOT, mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent pris dans leur ensemble, y compris ceux se présentant formellement comme régissant des actes distincts des autorisations d'exploitation commerciale, tels que par exemple des documents d'urbanisme.</ANA>
<ANA ID="9B"> 68-01-006-02 1) L'article 17 de la loi n° 2010-788 du 12 juillet 2010 a abrogé le dernier alinéa de l'article L. 122-1 du code de l'urbanisme en vertu duquel les autorisations d'aménagement commercial prévues par l'article L. 752-1 du code de commerce sont compatibles avec les schémas de cohérence territoriale (SCOT). Le même article a toutefois prévu que les SCOT en cours d'élaboration ou de révision approuvés avant le 1er juillet 2013 dont le projet de schéma a été arrêté par l'organe délibérant de l'établissement de coopération intercommunale (EPCI) avant 1er juillet 2012 peuvent opter pour l'application des dispositions antérieures. Par suite, applicabilité, dès lors que le droit d'option prévu par le législateur a été exercé par l'auteur du SCOT, de l'obligation de compatibilité des autorisations d'aménagement commercial avec le SCOT prévue par l'article L. 122-1 du code de l'urbanisme abrogé.... ,,2) Il appartient aux commissions d'aménagement commercial, non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des SCOT, mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent pris dans leur ensemble, y compris ceux se présentant formellement comme régissant des actes distincts des autorisations d'exploitation commerciale, tels que par exemple des documents d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., notamment, CE, 11 juillet 2011, SAS Sodigor, n° 353880, inédite au Recueil ; CE, 24 novembre 2014, SAS SADEF et autres, n°s 353451, 353454, 356617, inédite au Recueil., ,[RJ2] Cf. CE, 28 septembre 2005, Société Sumidis et société Coco Fruits, n°s 274706, 274707, p. 399 ; CE, 12 décembre 2012, Société Davalex, n° 353496, T. pp. 618-1018.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
