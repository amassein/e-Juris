<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682848</ID>
<ANCIEN_ID>JG_L_2018_03_000000408353</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682848.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 07/03/2018, 408353, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408353</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408353.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 19 août 2014 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, le bénéfice de la protection subsidiaire. La Cour a rejeté sa demande par une décision n° 14027945 du 15 juin 2016.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires enregistrés les 24 février, 24 mai et 1er août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Matuchansky, Poupot et Valdelièvre au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du deuxième alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Afin d'assurer une bonne administration de la justice et de faciliter la faculté ouverte aux intéressés de présenter leurs explications à la cour, le président de cette juridiction peut prévoir que la salle d'audience de la cour est reliée, en direct, par un moyen de communication audiovisuelle qui garantit la confidentialité de la transmission avec une salle d'audience spécialement aménagée à cet effet ouverte au public et située dans des locaux relevant du ministère de la justice plus aisément accessibles par le demandeur, dans des conditions respectant les droits de l'intéressé prévues par le premier alinéa. (...) Ces opérations donnent lieu à l'établissement d'un procès-verbal dans chacune des salles d'audience ou à un enregistrement audiovisuel ou sonore ... " ; qu'aux termes de l'article R 733-23 du même code : " Sauf dans le cas où il est procédé à un enregistrement audiovisuel ou sonore de l'audience, un procès-verbal est rédigé par l'agent chargé du greffe dans chacune des deux salles d'audience./ Chacun de ces procès-verbaux mentionne : - le nom et la qualité de l'agent chargé de sa rédaction ...<br/>
              - la date et l'heure du début de la communication audiovisuelle ; - les éventuels incidents techniques relevés lors de l'audience, susceptibles d'avoir perturbé la communication ; - l'heure de la fin de la communication audiovisuelle. / Le cas échéant, sont également mentionnés le nom de l'avocat et le nom de l'interprète sur le procès-verbal établi dans la salle d'audience où ils se trouvent. / Ces procès-verbaux attestent de l'ouverture au public des deux salles d'audience ... ". ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier de la Cour nationale du droit d'asile que la décision attaquée rejetant le recours de Mme A...contre la décision de l'Office français de protection des réfugiés et apatrides du 19 août 2014 rejetant sa demande d'asile et lui refusant le bénéfice de la protection subsidiaire, a été rendue à la suite d'une audience au cours de laquelle l'intéressé a présenté ses explications à la cour par " vidéo-audience ", en application des dispositions du deuxième alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que l'audience a ainsi eu lieu, le 25 mai 2016, simultanément dans la salle d'audience de la cour à Montreuil et dans une salle d'audience à Mayotte, où se trouvait Mme A...; qu'il ne ressort pas des pièces du dossier que le procès-verbal d'audience, requis par les dispositions précitées, aurait été dressé à Mayotte par l'agent chargé du greffe dans la salle d'audience où était présente la requérante ; qu'eu égard à la portée de l'article R. 733-23 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'absence de ce procès verbal, à elle seule, entache la régularité de la procédure ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à demander l'annulation de la décision qu'elle attaque ; <br/>
<br/>
              3.	Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Matuchansky, Poupot et Valdelièvre renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à cette SCP de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 15 juin 2016 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à l'avocat de Mme A..., la SCP Matuchansky, Poupot et Valdelièvre, sous réserve qu'elle renonce à l'indemnité due au titre de l'aide juridictionnelle totale, la somme de 3 000 euros sur le fondement des dispositions du 2ème alinéa de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
