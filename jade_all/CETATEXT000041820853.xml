<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041820853</ID>
<ANCIEN_ID>JG_L_2020_04_000000440117</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/82/08/CETATEXT000041820853.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/04/2020, 440117, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440117</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440117.20200422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 15 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. J... B..., Mme F... I..., Mme G... C..., M. A... D... et M. H... E... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre des solidarités et de la santé de prendre des mesures ou de saisir les autorités compétentes en vue de l'adoption de mesures destinées à permettre à tous les établissements hospitaliers de France de prendre connaissance du protocole de soins préconisé par le Dr Paul Marik, reposant notamment sur l'administration de doses importantes de vitamine C par perfusion et de l'utiliser pour le traitement du covid-19.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est remplie eu égard au nombre de patients qui décèdent chaque jour ;<br/>
              - l'absence de diffusion par le ministre des solidarités et de la santé à l'ensemble des équipes médicales du protocole proposé par le Dr Marik est constitutive d'une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins appropriés à son état de santé, tels qu'appréciés par le médecin.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la Constitution ;<br/>
<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 3030-423 du 14 avril 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 de ce code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou qu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou mal fondée.<br/>
<br/>
              2. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai et qu'il est possible de prendre utilement de telles mesures. Celles-ci doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              3. Pour l'application de l'article L. 521-2 du code de justice administrative, le droit au respect de la vie constitue une liberté fondamentale au sens des dispositions de cet article. En outre, une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins appropriés à son état de santé, tels qu'appréciés par le médecin, peut faire apparaître, pour l'application de ces dispositions, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle risque d'entraîner une altération grave de l'état de santé de la personne intéressée.<br/>
<br/>
              4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19, de caractère pathogène et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. Par un nouveau décret du 23 mars 2020, pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, le Premier ministre a réitéré les mesures qu'il avait précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Leurs effets ont été prolongés en dernier lieu par décret du 14 avril 2020.<br/>
<br/>
<br/>
              5. Les requérants, médecins généralistes, font valoir l'intérêt que pourrait présenter " le protocole proposé par le Dr Marik ", lequel aurait, selon eux, constaté " des résultats remarquables " par l'administration de doses importantes de vitamine C par perfusion et demandent en conséquence au ministre des solidarités et de la santé de prendre des mesures ou de saisir les autorités compétentes en vue de l'adoption de mesures destinées à permettre à tous les établissements hospitaliers de France de prendre connaissance de ce " protocole " et de l'utiliser pour le traitement du covid-19. Alors qu'aucun traitement n'est à ce jour connu pour soigner les patients atteints du covid-19 et que le Haut Conseil de la santé publique, saisi par le ministre des solidarités et de la santé, a rendu le 23 mars 2020 un avis sur les recommandations thérapeutiques dans la prise en charge de cette maladie, préconisant notamment " que tout prescripteur prenne en compte l'état très limité des connaissances actuelles et soit conscient de l'engagement de sa responsabilité lors de la prescription de médicaments dans des indications hors AMM, en dehors du cadre d'essais cliniques et des recommandations " qu'il décline, confirmant dans un avis du 8 avril 2020 relatif à la prise en charge de ces patients à domicile ou en structure de soins qu' " à ce jour, aucune prescription de traitement à effet antiviral attendu n'est recommandée en ambulatoire en dehors d'essais cliniques ", la demande des requérants, qui ne soutiennent pas même que des établissements hospitaliers envisageraient d'expérimenter ce " protocole " et en seraient empêchés, n'apparaît reposer que, d'une part, sur le rappel des propriétés antivirales et antioxydantes de la vitamine C, sans cependant qu'ils produisent d'étude relative à son efficacité sur le covid-19, et, d'autre part, sur l'extrapolation par ce " protocole ", pour l'appliquer au traitement du covid-19 apparu en novembre 2019, d'une publication intervenue en 2017 dans une revue médicale américaine, de valeur scientifique non établie, relative à l'association entre hydrocortisone, vitamine C et thiamine dans le traitement des seuls chocs septiques et sepsis sévères, publication indiquant au demeurant elle-même appeler des études additionnelles, ainsi que sur l'affirmation, relayée par un site internet spécialisé dans la " médecine orthomoléculaire ", selon laquelle ce traitement aurait été recommandé en Chine. <br/>
<br/>
<br/>
              6. Dans ces conditions, l'absence d'adoption des mesures demandées ne peut manifestement être regardée, à ce jour, comme portant une atteinte grave et manifestement illégale au droit au respect de la vie et au droit de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins appropriés à son état de santé, tels qu'appréciés par le médecin. Il y a lieu, par suite, de rejeter la requête de M. B... et autres, sans qu'il soit besoin de statuer sur sa recevabilité ni sur la condition d'urgence, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... et des autres requérants est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. J... B..., premier dénommé, pour l'ensemble des requérants.<br/>
Copie en sera adressée au ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
