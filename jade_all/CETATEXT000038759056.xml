<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038759056</ID>
<ANCIEN_ID>JG_L_2019_07_000000419398</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/90/CETATEXT000038759056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 10/07/2019, 419398, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419398</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419398.20190710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) Air France a demandé au tribunal administratif de Montreuil de prononcer la décharge des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période allant du 1er avril 2004 au 31 mars 2007, ainsi que des intérêts de retard correspondants. Par un jugement n° 1412114 du 16 juin 2016, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16VE02450 du 30 janvier 2018, la cour administrative d'appel de Versailles  a rejeté l'appel formé  par la société Air France contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 mars et 29 juin 2018 et le 27 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la société Air France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la sixième directive 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977, en matière d'harmonisation des législations des États membres relatives aux taxes sur le chiffre d'affaires ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'arrêt C-250/14 du 23 décembre 2015 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Air France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Air France a, pour les vols intérieurs qui sont soumis à la taxe sur la valeur ajoutée, conservé la totalité des sommes acquittées par ses clients correspondant au prix des billets non échangeables périmés en raison de l'absence des clients lors de l'embarquement ou des billets échangeables ou remboursables inutilisés dans le délai de leur validité, sans reverser à l'administration fiscale la taxe sur la valeur ajoutée sur le produit de la vente de ces billets au motif que ces recettes ne pouvaient être rattachées à l'exécution d'une prestation de service de transport aérien et qu'ainsi, elles n'entraient pas dans le champ d'application de cette taxe. A l'issue d'une vérification de comptabilité de la société Air France, l'administration lui a notifié, au titre de la période du 1er avril 2004 au 31 mars 2007, des rappels de taxe sur la valeur ajoutée au taux réduit de 5,5 % portant sur les sommes correspondant à la vente des billets périmés ou inutilisés dans leur délai de validité. Par un jugement du 16 juin 2016, le tribunal administratif de Montreuil a rejeté la  demande de cette société tendant à la décharge de ces rappels de taxe sur la valeur ajoutée. La société Air France se pourvoit en cassation contre l'arrêt du 30 janvier 2018 par lequel la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Aux termes de l'article 256 du code général des impôts : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel. (...) ". Aux termes de l'article 269 de ce code : " 1. Le fait générateur de la taxe se produit : / a) Au moment où la livraison, l'acquisition intracommunautaire du bien ou la prestation de services est effectué ; / (...) 2. La taxe est exigible : / a) Pour les livraisons et les achats visés au a du 1 et pour les opérations mentionnées aux b et d du même 1, lors de la réalisation du fait générateur ; / (...) c) Pour les prestations de services autres que celles visées au b bis, lors de l'encaissement des acomptes, du prix, de la rémunération ou, sur option du redevable, d'après les débits. (...) ". Enfin, aux termes du 1 de l'article 272 du même code : " La taxe sur la valeur ajoutée qui a été perçue à l'occasion de ventes ou de services est imputée ou remboursée dans les conditions prévues à l'article 271 lorsque ces ventes ou services sont par la suite résiliés ou annulés (...) "<br/>
<br/>
              3. D'une part, aux termes de l'article 2, point 1, de la sixième directive du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires, dont l'article 256 du code général des impôts assure la transposition : " sont soumises à la TVA les prestations de services effectuées à titre onéreux à l'intérieur du pays par un assujetti agissant en tant que tel ". Dans l'arrêt C-250/14 du 23 décembre 2015, la Cour de justice de l'Union européenne a dit pour droit que les services dont la fourniture correspond à l'exécution des obligations découlant d'un contrat de transport aérien de personnes sont l'enregistrement ainsi que l'embarquement des passagers et l'accueil de ces derniers à bord de l'avion au lieu de décollage convenu dans le contrat de transport en cause, le départ de l'appareil à l'heure prévue, le transport des passagers et de leurs bagages du lieu de départ au lieu d'arrivée, la prise en charge des passagers pendant le vol et, enfin, le débarquement de ceux-ci, dans des conditions de sécurité, au lieu d'atterrissage et à l'heure convenus dans ce contrat. Toutefois, dès lors que la réalisation de ces prestations n'est possible qu'à la condition que le passager de la compagnie aérienne se présente à la date et au lieu de l'embarquement prévus, la contre-valeur du prix versé lors de l'achat du billet est constituée par le droit qu'en tire le passager de bénéficier de l'exécution des obligations découlant du contrat de transport, indépendamment du fait qu'il mette en oeuvre ce droit, la compagnie aérienne réalisant la prestation dès lors qu'elle met le passager en mesure de bénéficier de ces prestations.<br/>
<br/>
              4. D'autre part, aux termes de l'article 10, paragraphe 2 de la même sixième directive : " Le fait générateur de la taxe intervient et la taxe devient exigible au moment où la livraison du bien ou la prestation de services est effectuée. (...) / Toutefois en cas de versements d'acomptes avant que la livraison de biens ou la prestation de services ne soit effectuée, la taxe devient exigible au moment de l'encaissement à concurrence du montant encaissé ". Il résulte de l'interprétation de ces dispositions retenue par la Cour de justice de l'Union européenne que, si le fait générateur de la taxe sur la valeur ajoutée et son exigibilité interviennent en principe au moment où la livraison du bien ou la prestation de services est effectuée, elle devient toutefois exigible dès l'encaissement, à concurrence du montant encaissé, lorsque des acomptes sont versés avant que la prestation de services ne soit effectuée. Pour que la taxe sur la valeur ajoutée soit exigible sans que la prestation ait encore été effectuée, il faut et il suffit que tous les éléments pertinents du fait générateur, c'est-à-dire de la future prestation, soient déjà connus et donc, en particulier, que, au moment du versement de l'acompte, les biens ou les services soient désignés avec précision. Enfin, le caractère intégral et non partiel du paiement du prix n'est pas susceptible de remettre en cause une telle interprétation. Dans l'arrêt C-250/14 du 23 décembre 2015, la Cour de justice de l'Union européenne a dit pour droit, d'une part, que s'agissant des prestations telles que celles en litige, les conditions d'application du paragraphe 2 précité de l'article 10 de la directive étaient susceptibles d'être réunies pour autant que l'ensemble des éléments de la future prestation de transport sont déjà connus et identifiés avec précision au moment de l'achat du billet et, d'autre part, que dans l'hypothèse d'un passager défaillant, la compagnie aérienne qui vend un billet de transport remplit ses obligations contractuelles en mettant le passager en mesure de faire valoir ses droits aux prestations prévues par le contrat de transport, la taxe sur la valeur ajoutée acquittée au moment de l'achat du billet d'avion par le passager qui n'a pas utilisé son billet devient exigible au moment de l'encaissement du prix du billet, que ce soit par la compagnie aérienne elle-même, par un tiers agissant en son nom et pour son compte, ou par un tiers agissant en son nom propre, mais pour le compte de la compagnie aérienne. <br/>
<br/>
              5. Les principes énoncés au point 4 s'appliquent également dans l'hypothèse où le passager qui renonce à faire valoir ses droits aux prestations qu'il a acquises dispose de la faculté d'obtenir le remboursement de tout ou partie du prix acquitté en contrepartie de cette renonciation, qu'il exerce ou non cette faculté. Cependant, dans l'hypothèse où le passager exerce son droit à remboursement du billet et que la prestation de service est ainsi annulée, la compagnie aérienne qui a émis le billet est en droit, conformément au 1 de l'article 272 du code général des impôts, de solliciter la restitution de la taxe sur la valeur ajoutée collectée lors de l'encaissement du prix de la prestation.<br/>
<br/>
              Sur le litige :<br/>
<br/>
              6. En premier lieu, la cour administrative d'appel de Versailles a jugé sans erreur de droit que le fait générateur de la taxe sur la valeur ajoutée afférente à la prestation de service de transport aérien acquise par le passager, qui est constitué par la réalisation effective du voyage ou par l'expiration des obligations contractuelles du transporteur aérien, est indépendant du fait que le client mette ou non en oeuvre le droit qu'il tire du contrat de transport et, le cas échéant, s'agissant des billets remboursables, indépendant du fait que ce même client ait définitivement renoncé à demander le remboursement du prix acquitté. La cour, qui a suffisamment motivé son arrêt, en a déduit sans commettre d'erreur de droit que devaient être soumises à cette taxe les sommes conservées par l'opérateur de transport aérien au titre des billets non utilisés et qui n'ont pas donné lieu à une demande de remboursement.<br/>
<br/>
              7. En second lieu, la cour, après avoir relevé que les billets d'avion remboursables dont les recettes ont fait l'objet des rehaussement litigieux n'avaient pas été effectivement échangés, utilisés ou remboursés au cours de leur période de validité, a estimé, par une appréciation souveraine exempte de dénaturation, que l'ensemble des éléments pertinents relatifs à la future prestation de transport aérien étaient connus lors de l'émission et de l'achat de ces billets. La cour a pu déduire de ces faits, sans commettre d'erreur de droit, que l'exigibilité de la taxe sur la valeur ajoutée afférente aux sommes perçues au titre de la vente d'un tel billet résultait de l'encaissement du prix par l'opérateur chargé de la réalisation des prestations de service de transport, indépendamment de la circonstance que la prestation était susceptible d'être ultérieurement annulée. <br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de la société Air France doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Air France est rejeté.<br/>
Article 2 : La présente décision sera notifiée à société anonyme Air France et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
