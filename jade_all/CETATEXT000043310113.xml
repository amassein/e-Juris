<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043310113</ID>
<ANCIEN_ID>JG_L_2021_03_000000445917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/31/01/CETATEXT000043310113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 29/03/2021, 445917, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445917.20210329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société TTS et la société Benjamine ont demandé au juge des référés du tribunal administratif de La Réunion, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 30 juillet 2020 par laquelle l'agent de contrôle de l'inspection du travail de l'unité de la Réunion a ordonné l'arrêt des travaux réalisés à l'aide d'une foreuse sur traîneau alimentée par air comprimé sur le chantier de la résidence Benjamine à Sainte-Suzanne, et d'enjoindre à l'administration d'autoriser la reprise des travaux. Par une ordonnance n° 2000888 du 19 octobre 2020, le juge des référés a suspendu l'exécution de cette décision et enjoint à l'administration d'examiner la demande de reprise des travaux dans un délai de huit jours.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 novembre 2020 et 15 janvier 2021 au secrétariat du contentieux du Conseil d'État, la ministre du travail, de l'emploi et de l'insertion demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Benjamine ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 4731-1 du code du travail : " L'agent de contrôle de l'inspection du travail (...) peut prendre toutes mesures utiles visant à soustraire immédiatement un travailleur qui ne s'est pas retiré d'une situation de danger grave et imminent pour sa vie ou sa santé, constituant une infraction aux obligations des décrets pris en application des articles L. 4111-6, L. 4311-7 ou L. 4321-4, notamment en prescrivant l'arrêt temporaire de la partie des travaux ou de l'activité en cause, lorsqu'il constate que la cause de danger résulte : (...) / 4° Soit de l'utilisation d'équipements de travail dépourvus de protecteurs, de dispositifs de protection ou de composants de sécurité appropriés ou sur lesquels ces protecteurs, dispositifs de protection ou composants de sécurité sont inopérants  (...) ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de La Réunion que, par une décision du 30 juillet 2020, l'agent de contrôle de l'inspection du travail de l'unité de la Réunion a ordonné l'arrêt temporaire des travaux du chantier de la résidence Benjamine à Sainte-Suzanne, en raison de l'utilisation d'une foreuse présentant un danger grave et imminent pour la vie ou la santé des travailleurs de ce chantier, au double motif que celle-ci, dépourvue de marquage CE apparent, comportait, d'une part, une pièce en mouvement dépourvue de dispositif de protection ou de sécurité, d'autre part, le maintien par un fil de fer du tuyau d'arrivée d'air. La société TTS, en charge de ces travaux, et la société Benjamine, maître d'ouvrage, ont demandé au juge des référés du tribunal administratif de La Réunion, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision. Par une ordonnance du 19 octobre 2020, le juge des référés a suspendu l'exécution de cette décision et enjoint à l'administration d'examiner la demande de reprise des travaux dans un délai de huit jours. La ministre du travail, de l'emploi et de l'insertion se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              3. En premier lieu, si la ministre du travail, de l'emploi et de l'insertion soutient que le juge des référés a omis de viser une note en délibéré enregistrée le 19 octobre 2020, il ressort des pièces de la procédure que cette note a été produite après la lecture de l'ordonnance attaquée. Ce moyen ne peut, dès lors et en tout état de cause, qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 4722-1 du code du travail : " L'agent de contrôle de l'inspection du travail (...) peut (...) demander à l'employeur de faire procéder à des contrôles techniques, consistant notamment : / 1° A faire vérifier l'état de conformité de ses installations et équipements avec les dispositions qui lui sont applicables (...) ". <br/>
<br/>
              5. Le prononcé, par l'agent de contrôle de l'inspection du travail, d'une des mesures prévues au 4° de L. 4731-1 du code du travail, cité au point 1, ne saurait, eu égard à son objet, être subordonné à l'engagement préalable de la procédure de vérification prévue par les dispositions citées au point précédent. Par suite, en estimant que le moyen tiré de ce que la décision contestée n'avait pas été précédée de la vérification prévue au 1° de l'article L. 4722-1 du code du travail était propre à créer un doute sérieux sur sa légalité, le juge des référés a entaché son ordonnance d'erreur de droit. <br/>
<br/>
              6. Toutefois, en estimant qu'il n'était pas établi, en l'état des éléments qui lui étaient soumis, que la foreuse en litige présentait, en raison de l'absence de dispositif de protection ou de sécurité, un danger grave et imminent de nature à justifier qu'il soit fait application des dispositions de l'article L. 4731-1 du code du travail, le juge des référés n'a pas dénaturé les pièces du dossier qui lui était soumis, les nouveaux éléments produits devant le Conseil d'Etat par la ministre du travail, de l'emploi et de l'insertion ne pouvant à ce titre être pris en compte par le juge de cassation pour se prononcer sur ce moyen. En outre, il résulte des termes mêmes de son ordonnance qu'il a jugé que le second motif de la décision contestée, relatif à l'utilisation d'un fil de fer, ne pouvait légalement être retenu en l'état de l'instruction. Par suite, la ministre du travail, de l'emploi et de l'insertion ne saurait soutenir que l'ordonnance attaquée est entachée d'erreur de droit, faute pour le juge des référés d'avoir examiné si le second motif de la décision contestée était propre à la justifier.<br/>
<br/>
              7. Les motifs, exposés au point précédent, et à propos desquels les moyens de cassation ont été écartés, suffisent à justifier l'article 1er de l'ordonnance attaquée prononçant la suspension de l'exécution de la décision contestée. <br/>
<br/>
              8. En dernier lieu, la suspension de la décision d'arrêt de travaux contestée permettant, par elle-même, la reprise des travaux en cause, le juge des référés ne pouvait, sans entacher son ordonnance d'erreur de droit, enjoindre, à l'article 2 de son ordonnance, à l'administration de statuer dans un délai de huit jours sur la reprise des travaux. <br/>
<br/>
              9. Il résulte de tout ce qui précède que la ministre du travail, de l'emploi et de l'insertion n'est fondée qu'à demander l'annulation de l'article 2 de l'ordonnance du 19 octobre 2020 du juge des référés du tribunal administratif de La Réunion. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de régler, dans la mesure de la cassation prononcée, l'affaire au titre de la procédure de référé engagée par la société TTS et la société Benjamine, en application des dispositions de l'article L. 821-1 du code de justice administrative. <br/>
<br/>
              11. Eu égard à ce qui a été dit au point 8, les conclusions de la demande à fin d'injonction ne peuvent qu'être rejetées. <br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'ordonnance du juge des référés du tribunal administratif de la Réunion du 19 octobre 2020 est annulé.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la ministre du travail, de l'emploi et de l'insertion est rejeté.<br/>
Article 3 : Les conclusions à fin d'injonction présentées en première instance par la société Benjamine et la société TTS sont rejetées. <br/>
Article 4 : L'Etat versera une somme de 2 000 euros à la société Benjamine au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à la ministre du travail, de l'emploi et de l'insertion, à la société Benjamine et à la société TTS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
