<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036586696</ID>
<ANCIEN_ID>JG_L_2018_02_000000411758</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/58/66/CETATEXT000036586696.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 08/02/2018, 411758, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411758</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411758.20180208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL AFUR a demandé au juge des référés du tribunal administratif de Pau, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet des Pyrénées-Atlantiques du 4 avril 2017 la mettant en demeure de déposer des panneaux publicitaires implantés au PR 64.835 et au PR 77.365 de la route départementale 817. Par une ordonnance n° 1700956 du 6 juin 2017, le juge des référés du tribunal administratif de Pau a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi, enregistré le 22 juin 2017, le ministre d'Etat, ministre de la transition écologique et solidaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la SARL AFUR ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une demande en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2.	Considérant qu'en vertu de l'article L. 581-7 du code de l'environnement, toute publicité est interdite " en dehors des lieux qualifiés d'agglomération par les règlements relatifs à la circulation routière " ; <br/>
<br/>
              3.	Considérant que, pour ordonner la suspension de l'exécution de l'arrêté du préfet des Pyrénées-Atlantiques du 4 avril 2017 mettant en demeure la SARL AFUR de déposer des panneaux publicitaires implantés au PR 64.835 et au PR 77.365 de la route départementale 817 sur le territoire, respectivement, des communes de Castétis et de Baigts-de-Béarn, le juge des référés du tribunal administratif de Pau s'est fondé sur le motif que le moyen tiré de ce que le préfet aurait commis une erreur de qualification juridique en retenant que les panneaux litigieux étaient implantés hors agglomération au sens de l'article L. 581-7 du code de l'environnement était de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
<br/>
              4.	Considérant, toutefois, qu'il ressort des pièces du dossier soumis au juge des référés que les dispositifs publicitaires en cause, implantés dans un environnement rural, ne sont pas situés dans des zones où seraient groupés des immeubles bâtis rapprochés ; que, dans ces conditions, en estimant qu'était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision contestée, le moyen tiré des dispositions de l'article L. 581-7 du code de l'environnement, le juge des référés a dénaturé les pièces du dossier qui lui étaient soumises ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que le ministre d'Etat, ministre de la transition écologique et solidaire est fondé, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce et par application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ; <br/>
<br/>
              7.	Considérant que, pour demander la suspension de l'exécution de l'arrêté du 4 avril 2017, la SARL AFUR soutient que le procès-verbal de constat d'infraction dressé le 28 février 2017 ne donne pas les dimensions des différents panneaux publicitaires ; qu'en conséquence, le caractère contradictoire de la procédure a été méconnu ; que le procès-verbal et l'arrêté attaqué sont entachés d'un vice de forme en tant qu'ils font une erreur quant à l'article du code de l'environnement dont la méconnaissance est alléguée ; que les panneaux ne sont pas situés en dehors d'une agglomération ; <br/>
<br/>
              8.	Considérant qu'en l'état de l'instruction, aucun de ces moyens n'est propre à créer de doute sérieux sur la légalité de la décision attaquée ; que, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par la SARL AFUR tendant à ce que soit ordonnée la suspension de l'exécution de l'arrêté du préfet des Pyrénées-Atlantiques du 4 avril 2017 la mettant en demeure de déposer les panneaux publicitaires implantés au PR 64.835 et au PR 77.365 de la route départementale 817 doit être rejetée ;<br/>
<br/>
              9.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Pau du 6 juin 2017 est annulée.<br/>
<br/>
Article 2 : La demande présentée par la SARL AFUR devant le tribunal administratif de Pau et les conclusions de la SARL AFUR au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SARL AFUR et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
