<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028471757</ID>
<ANCIEN_ID>JG_L_2014_01_000000367938</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/47/17/CETATEXT000028471757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 15/01/2014, 367938, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367938</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; RICARD</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367938.20140115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 avril et 19 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Linpac Packaging Provence, dont le siège est au parc d'activité de Kerguilloten, à Noyal-Pontivy (56920), représentée par son président directeur général en exercice ; cette société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 12MA01568 du 19 février 2013 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1007613 du 13 mars 2012 par lequel le tribunal administratif de Marseille a annulé la décision du 27 novembre 2009 du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville autorisant le licenciement de M. A...B..., d'autre part, au rejet de la demande de première instance de M. B...; <br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la société Linpac Packaging Provence SAS et à Me Ricard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              1. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ; qu'à ce titre, lorsque la demande est fondée sur la cessation d'activité de l'entreprise, celle-ci n'a pas à être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise ; qu'il appartient alors à l'autorité administrative de contrôler, outre le respect des exigences procédurales légales et des garanties conventionnelles, que la cessation d'activité de l'entreprise est totale et définitive, que l'employeur a satisfait, le cas échéant, à l'obligation de reclassement prévue par le code du travail et que la demande ne présente pas de caractère discriminatoire ;<br/>
<br/>
              2. Considérant que, dès lors qu'une demande d'autorisation de licenciement fondée sur la cessation d'activité de l'entreprise n'a pas à être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise, il n'appartient pas à l'autorité administrative, pour apprécier la réalité du motif de cessation d'activité invoqué à l'appui d'une demande d'autorisation de licenciement d'un salarié protégé présentée par une société faisant partie d'un groupe, d'examiner la situation économique des autres entreprises de ce groupe ; qu'il lui incombe, en revanche, de vérifier que la cessation de cette activité est totale et définitive ; <br/>
<br/>
              3. Considérant, par suite, que le requérant est fondé à soutenir que la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant que " la cessation définitive de l'activité et la fermeture d'une société ne saurait en elle-même constituer l'énoncé d'un motif économique au sens de l'article L. 1233-3 du code du travail dans l'hypothèse où ladite société appartient à un groupe, l'appréciation de la situation économique devant dans un tel cas se faire (...) par rapport à l'ensemble des sociétés du groupe intervenant dans le même secteur d'activité " ; qu'il y a lieu, par suite d'annuler son arrêt ; <br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Linpac Packaging Provence au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette société qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 19 février 2013 est annulé. <br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille. <br/>
Article 3 : Les conclusions présentées par la société Linpac Packaging Provence au titre des dispositions de l'article L. 761-1 du code de justice administrative et par M. B...au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Linpac Packaging Provence, à M. A... B...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
