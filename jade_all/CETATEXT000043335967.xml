<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043335967</ID>
<ANCIEN_ID>JG_L_2021_04_000000441542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/33/59/CETATEXT000043335967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 02/04/2021, 441542, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2021:441542.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... et la société La Métrie ont demandé au tribunal administratif de Rennes de condamner solidairement le département d'Ille-et-Vilaine et la société Eiffage Rail Express ou l'un à défaut de l'autre à leur verser la somme de 95 282 euros au titre de l'indemnisation des dommages de travaux publics qu'ils estiment avoir subis lors des opérations d'aménagement foncier liés aux travaux de construction de la ligne à grande vitesse Bretagne Pays de Loire. Par un jugement n° 1601278 du 22 juin 2018, le tribunal administratif de Rennes a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 18NT03116 du 7 février 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B... et la société La Métrie contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1erjuillet et 1eroctobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... et la société La Métrie demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département d'Ille-et-Vilaine et de la société Eiffage Rail Express la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Didier-Pinet, avocat de M. B... et de la société La Métrie ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'ils attaquent, M. B... et la société La Métrie soutiennent que la cour administrative d'appel de Nantes a : <br/>
              - insuffisamment motivé son arrêt et commis une erreur de droit en se fondant uniquement, pour rejeter le droit à réparation de M. B..., sur les difficultés d'exploitation entraînées par la présence d'une pointe au sud de la nouvelle parcelle ZH 72 et par l'existence d'un biais sur toute la longueur côté ouest de la nouvelle parcelle ZV 29 ;<br/>
              - dénaturé les pièces du dossier en jugeant que les contraintes d'exploitation issues du remembrement des parcelles du requérant étaient largement compensées par ses avantages ;<br/>
              - dénaturé les pièces du dossier en rejetant la demande d'indemnité résultant de la perte de 2,35 hectares de surface d'épandage au motif que le requérant n'avait produit à l'appui de cette demande qu'un document de la société GES, alors que l'intéressé avait également produit la décision de la commission départementale d'aménagement foncier d'Ille-et-Vilaine du 18 juillet 2014, trois rapports ou notes d'un expert foncier et agricole près la cour d'appel de Rennes et un autre document d'étude de la société GES qui établissaient tous l'existence d'une perte de surface épandable ;<br/>
              - dénaturé les faits qui lui étaient soumis en se fondant, pour rejeter la demande d'indemnisation relative à la perte d'épandage, sur un argument inopérant tiré du refus du requérant de recourir aux prestations de la société Sede environnement, mandatée par la société Eiffage Rail Express, en vue de procéder à la réfection de l'ensemble des plans d'épandage et de fumure des exploitations concernées par les opérations d'aménagement foncier ;<br/>
              - omis de statuer sur le chef de préjudice résultant des frais supportés par le requérant pour remettre en état la parcelle anciennement cadastrée section J n° 433, devenue ZH 72, à la suite de la destruction d'une maison d'habitation sur ordre de la société Eiffage Rail Express ;<br/>
              - commis une erreur de droit et dénaturé les pièces du dossier en refusant toute indemnisation à raison de la perte sur l'investissement réalisé en 2001 en matière de drainage sur la parcelle J 680 au motif que cet investissement était ancien et largement amorti, alors que l'article 6.3.2 du protocole d'accord du 21 juillet 2011 sur les conditions de réparation des dommages de travaux conclu entre Réseau Ferré de France et la société Eiffage Rail Express pour l'indemnisation des préjudices causés aux exploitations agricoles situées à proximité de la LGV Bretagne-Pays de la Loire prévoyait une telle indemnisation ;<br/>
              - omis de statuer sur le préjudice de perte d'exploitation entraîné par la cession de trois hectares de la parcelle J 680 qui disposait d'un réseau de drainage depuis 2001.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt en tant qu'il a omis de statuer sur les conclusions tendant à la réparation du préjudice résultant pour M. B... du coût des travaux de remise en état de la parcelle anciennement cadastrée section J n° 433, devenue ZH 72. <br/>
<br/>
              4. En revanche, s'agissant des autres conclusions dirigées contre l'arrêt attaqué, aucun des moyens soulevés n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de M. B... et de la société La Métrie qui sont dirigées contre l'arrêt attaqué en tant qu'il a omis de statuer sur les conclusions tendant à la réparation du préjudice résultant du coût des travaux de remise en état de la parcelle anciennement cadastrée section J n° 433, devenue ZH 72, sont admises.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. B... et de la société La Métrie n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et à la société La Métrie.<br/>
Copie en sera adressée au département d'Ille-et-Vilaine et à la société Eiffage Rail Express. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
