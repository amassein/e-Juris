<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044052301</ID>
<ANCIEN_ID>JG_L_2021_08_000000450228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/05/23/CETATEXT000044052301.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 06/08/2021, 450228, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450228.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire distinct et un mémoire en réplique, enregistrés les 31 mai et 7 juillet 2021 au secrétariat du contentieux du Conseil d'État, la société par actions simplifiée Compagnie Européenne des Emballages Robert Schisler, la société Huhtamaki, la société SEDA International Packaging Group SPA, et l'association EPPA (European Paper Packaging Alliance) demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation de l'article 5 du décret n° 2020-1724 du 28 décembre 2020 relatif à l'interdiction d'élimination des invendus non alimentaires et à diverses dispositions de lutte contre le gaspillage, en tant qu'il crée l'article D. 541-342 du code de l'environnement, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 541-15-10, III, 2° (16ème alinéa) du code de l'environnement, dans sa rédaction issue de l'article 77 de la loi n° 2020-105 du 10 février 2020 relative à la lutte contre le gaspillage et à l'économie circulaire. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule, et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'environnement, et notamment son article L. 541-15-10 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société par actions simplifiée Compagnie Européenne des Emballages Robert Schisler et autres ;<br/>
<br/>
              Vu la note en délibéré enregistrée le 12 juillet 2021 présentée par la société Compagnie Européenne des Emballages Robert Schisler et autres. <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du seizième alinéa du 2° du III de l'article L. 541-15-10 du code de l'environnement : " A compter du 1er janvier 2023, les établissements de restauration sont tenus de servir les repas et boissons consommés dans l'enceinte de l'établissement dans des gobelets, y compris leurs moyens de fermeture et couvercles, des assiettes et des récipients réemployables ainsi qu'avec des couverts réemployables. Les modalités de mise en œuvre du présent alinéa sont précisées par décret ". A l'appui de leur question prioritaire de constitutionnalité, la Compagnie Européenne des Emballages Robert Schisler et autres soutiennent que ces dispositions méconnaissent le devoir de prévention inscrit à l'article 3 de la Charte de l'environnement et le principe d'égalité et la liberté d'entreprendre protégés par les articles 4 et 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789. <br/>
<br/>
              3. En premier lieu, aux termes de l'article 3 de la Charte de l'environnement : " Toute personne doit, dans les conditions définies par la loi, prévenir les atteintes qu'elle est susceptible de porter à l'environnement ou, à défaut, en limiter les conséquences ". Il en résulte qu'il appartient au législateur de veiller au respect de ce principe lorsqu'il est appelé à en déterminer les modalités de mise en œuvre par la définition du cadre de la prévention ou de la limitation des conséquences d'une atteinte à l'environnement. Les dispositions attaquées, qui ont pour objet de mettre en œuvre le principe de prévention en limitant la quantité de déchets produite par les établissements de restauration, prévoient que ces établissements auront l'obligation de servir les repas et boissons consommés dans l'enceinte de l'établissement dans des gobelets, y compris leurs moyens de fermeture et couvercles, des assiettes et des récipients réemployables ainsi qu'avec des couverts réemployables à partir du 1er janvier 2023. En se bornant à soutenir que les dispositions en cause auraient dû être adoptées sur le fondement d'études précises et normées portant spécifiquement sur l'objet de la loi et prévoir une disposition autorisant toute solution alternative présentant un meilleur résultat global sur le plan de l'environnement, les associations requérantes ne critiquent pas utilement cette disposition. Par suite, le grief tiré de la méconnaissance de l'article 3 de la Charte de l'environnement ne présente pas de caractère sérieux.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 6 de la Déclaration des droits de l'Homme et du citoyen du 26 août 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. S'il est soutenu que les dispositions contestées établissent une différence de traitement, d'une part, entre les établissements de restauration satisfaisant déjà à l'obligation de servir les repas et boissons consommés dans leur enceinte dans de la vaisselle réemployable ou pouvant s'y conformer facilement et les autres établissements, et, d'autre part, entre les producteurs français qui vendent leur vaisselle à usage unique en France, qui n'y seront plus autorisés à compter du 1er janvier 2023, et les producteurs d'autres États membres de l'Union européenne, dans lesquels les établissements de restauration sont encore autorisés à utiliser ces produits, il résulte des termes du seizième alinéa de l'article III. 2°) de l'article L. 541-15-10 du code de l'environnement que les dispositions contestées s'appliquent indifféremment à tous les établissements de restauration sur le territoire national et n'interdisent pas en elles-mêmes la vente de vaisselle à usage unique par les producteurs français et européens. Par suite et en tout état de cause, il ne peut être sérieusement soutenu que ces dispositions porteraient atteinte au principe constitutionnel d'égalité devant la loi. <br/>
<br/>
              5. En troisième lieu, il est loisible au législateur d'apporter à la liberté d'entreprendre, qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen de 1789, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées par rapport à l'objectif poursuivi. En imposant aux établissements de restauration l'utilisation de vaisselle réemployable, le législateur a entendu favoriser la réduction des déchets plastiques, dans un but de protection de l'environnement. Cette obligation n'impose toutefois pas aux établissements de restauration un choix particulier de procédé industriel, de distribution, de commercialisation et de consommation, et s'appliquera seulement à partir du 1er janvier 2023. L'atteinte ainsi portée à la liberté d'entreprendre des établissements de restauration par le législateur n'est donc pas, compte tenu du champ de cette obligation, manifestement disproportionnée au regard de l'objectif d'intérêt général de protection de l'environnement. Par suite, le grief tiré de la méconnaissance de la liberté d'entreprendre ne présente pas de caractère sérieux.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la question de la conformité aux droits et libertés garantis par la Constitution des dispositions contestées ne présente pas un caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité. <br/>
Article 2 : La présente décision sera notifiée à la Société Compagnie Européenne des Emballages Robert Schisler, première dénommée pour l'ensemble des requérants, et à la ministre de la transition écologique. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au ministre de l'économie, des finances et de la relance, et au garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
