<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198235</ID>
<ANCIEN_ID>JG_L_2019_10_000000429049</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198235.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 09/10/2019, 429049, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429049</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:429049.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nantes de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 3 août 2018 par lequel le ministre de l'agriculture et de l'alimentation l'a licencié pour insuffisance professionnelle et de la décision implicite rejetant son recours gracieux contre cet arrêté. Par une ordonnance n° 1901405 du 7 mars 2019, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mars et 8 avril 2019, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 3 août 2018, le ministre de l'agriculture et de l'alimentation a licencié M. B..., vétérinaire inspecteur contractuel, pour insuffisance professionnelle, puis a implicitement rejeté son recours gracieux contre cette mesure. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nantes a rejeté, pour défaut d'urgence, la demande de suspension de l'exécution de ces décisions présentées par M. B.... En se fondant, pour apprécier si la décision du 3 août 2018 préjudiciait de manière suffisamment grave et immédiate à la situation du requérant, sur ce qu'il n'établissait pas le montant de certaines dépenses, notamment d'alimentation, et sur ce qu'il ne justifiait " d'aucune dépense précise de la vie courante qu'il ne serait pas en mesure d'honorer du fait de la décision litigieuse ", alors qu'un agent public ayant fait l'objet d'une mesure d'éviction qui le prive de sa rémunération n'est pas tenu de fournir de telles précisions à l'appui de sa demande de suspension de l'exécution de cette mesure, le juge des référés a commis une erreur de droit. M. B... est, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              3. Il y a lieu, au titre de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par M. B....<br/>
<br/>
              4. D'une part, l'exécution de l'arrêté litigieux a pour effet de diminuer d'environ 45 % les ressources dont dispose le requérant. Compte tenu des charges dont il fait état, la mesure contestée entraîne un bouleversement des conditions d'existence de M. B.... Par suite, la condition d'urgence exigée par l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.<br/>
<br/>
              5. D'autre part, le moyen tiré de ce que ses comptes rendus d'entretien d'évaluation ne figuraient pas dans le dossier individuel de M. B... et que ce dernier n'a, en conséquence, pas pu en prendre connaissance, préalablement au prononcé de son licenciement, est, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de l'arrêté du 3 août 2018 du ministre de l'agriculture et de l'alimentation et, par suite, de la décision implicite rejetant le recours gracieux formé contre celui-ci.<br/>
<br/>
              6. Dès lors, il y a lieu d'ordonner la suspension de l'arrêté du 3 août 2018 du ministre de l'agriculture et de l'alimentation et de la décision implicite rejetant le recours gracieux formé contre celui-ci.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme de 3 000 euros que M. B... demande au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 7 mars 2019 du juge des référés du tribunal administratif de Nantes est annulée.<br/>
Article 2 : L'exécution de l'arrêté du 3 août 2018 du ministre de l'agriculture et de l'alimentation et de la décision implicite de rejet du recours gracieux formé contre cet arrêté est suspendue.<br/>
Article 3 : L'Etat versera à M. B... la somme de 3 000 euros au titre des frais exposés par lui et non compris dans les dépens.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
