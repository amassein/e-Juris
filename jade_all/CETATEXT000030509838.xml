<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509838</ID>
<ANCIEN_ID>JG_L_2015_04_000000385628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/98/CETATEXT000030509838.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 17/04/2015, 385628, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385628.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme C...A...a demandé au tribunal administratif de Basse-Terre d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux et communautaires dans la commune du Gosier (Guadeloupe). Par un jugement n° 1400307 du 9 octobre 2014, le tribunal administratif de Basse-Terre a rejeté sa protestation. <br/>
<br/>
              Par une requête, enregistrée le 7 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Basse-Terre ;<br/>
<br/>
              2°) de faire droit à sa protestation et de déclarer M. D...B...inéligible en application de l'article L. 118-4 du code électoral ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code électoral ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue des opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux et communautaires dans la commune du Gosier (Guadeloupe), la liste " Le grand rassemblement pour un Gosier uni ", conduite par M. B..., a obtenu 5 419 voix, soit 61,45 % des suffrages exprimés, tandis que la liste " Ensemble pour un Gosier équilibre et équitable ", conduite par MmeA..., a obtenu 3 399 voix et ainsi recueilli 38,54 % des suffrages exprimés ; que Mme A...demande l'annulation du jugement du 9 octobre 2014 par lequel le tribunal administratif de Basse-Terre a rejeté sa protestation tendant à l'annulation de ces opérations électorales ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 51 du code électoral : " Pendant la durée de la période électorale, dans chaque commune, des emplacements spéciaux sont réservés par l'autorité municipale pour l'apposition des affiches électorales. / Dans chacun de ces emplacements, une surface égale est attribuée à chaque candidat ou à chaque liste de candidats. / Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, tout affichage relatif à l'élection, même par affiches timbrées, est interdit en dehors de cet emplacement ou sur l'emplacement réservé aux autres candidats, ainsi qu'en dehors des panneaux d'affichage d'expression libre lorsqu'il en existe " ; qu'aux termes de l'article L. 52-1 du même code : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que 35 affiches électorales en faveur de la liste conduite par M. B...ainsi qu'une banderole annonçant la tenue d'une réunion de présentation du programme électoral de la même liste ont été apposées en dehors des emplacements prévus à cet effet, en méconnaissance des dispositions de l'article L. 51 du code électoral ; que, par ailleurs, la publication, dans le bulletin municipal " Gran Gouzié ", tiré à 15 000 exemplaires, une semaine avant la tenue des opérations électorales, d'une série d'articles retraçant de façon exhaustive et avantageuse les actions menées par l'équipe municipale sortante au cours de l'année écoulée, constitue un procédé de publicité commerciale au sens de l'article L. 52-1 du code électoral ; que, toutefois, eu égard à l'écart de voix entre les deux listes, il ne résulte pas de l'instruction que ces irrégularités auraient été de nature à altérer la sincérité du scrutin ; <br/>
<br/>
              4. Considérant, en second lieu, que si le juge administratif n'est pas compétent pour statuer sur la régularité des inscriptions sur la liste électorale, il lui appartient cependant d'apprécier dans quelle mesure des irrégularités commises lors de l'établissement de la liste ont constitué des manoeuvres susceptibles d'avoir vicié les résultats du scrutin ; qu'il ne résulte pas de l'instruction, et notamment des éléments produits par la requérante, que des électeurs auraient été irrégulièrement radiés de la liste électorale de la commune du Gosier ni, en tout état de cause, que l'absence de coïncidence alléguée entre les numéros du registre d'émargement et les numéros figurant sur les cartes d'électeurs résulterait de manoeuvres susceptibles d'avoir vicié les résultats du scrutin ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation du jugement qu'elle attaque ; que les dispositions de l'article            L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par M. B...; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée. <br/>
<br/>
Article 2 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme C...A..., à M. D...B...et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
