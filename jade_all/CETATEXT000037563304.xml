<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037563304</ID>
<ANCIEN_ID>JG_L_2018_10_000000424855</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/56/33/CETATEXT000037563304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/10/2018, 424855, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424855</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:424855.20181025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, outre de l'admettre au bénéfice de l'aide juridictionnelle provisoire, d'enjoindre au préfet territorialement compétent d'enregistrer sa demande d'asile et de lui délivrer le dossier à présenter à l'Office français de protection des réfugiés et apatrides ainsi qu'une attestation de demande d'asile dans un délai de 48 heures à compter de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 1816978/9 du 27 septembre 2018, le juge des référés du tribunal administratif de Paris a admis M. A...à l'aide juridictionnelle provisoire et a enjoint au préfet de police d'enregistrer la demande d'asile de M. A...dans un délai de sept jours et de lui délivrer une attestation de demande d'asile puis un dossier de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              Par une requête, enregistrée le 15 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la demande de première instance présentée par M.A....<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - son appel est recevable ;  <br/>
              - la situation d'urgence dont se prévaut M. A...est imputable à sa propre attitude ;<br/>
              - le préfet de police n'a pas porté une atteinte grave et manifestement illégale au droit d'asile de M. A...en refusant d'enregistrer sa demande d'asile au-delà du délai de six mois suivant l'acceptation de son transfert par l'Italie, le délai de transfert ayant été porté à<br/>
dix-huit mois dès lors que l'intéressé doit être regardé comme en fuite au sens de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013.<br/>
<br/>
              Par un mémoire en défense, enregistré le 23 octobre 2018, M. A...conclut au rejet de la requête. Il soutient que :<br/>
              - le fait de faire l'objet d'une décision de transfert à un Etat membre de l'Union européenne constitue une situation d'urgence et son comportement n'a pas contribué à cette urgence dès lors qu'il a déféré à toutes les convocations préfectorales, y compris celle l'ayant conduit à être placé en rétention aux fins d'exécution de la décision de transfert, et que sa soustraction intentionnelle à l'exécution de son transfert n'est pas caractérisée ;<br/>
              - il est porté une atteinte grave et manifestement illégale à son droit d'asile eu égard à l'expiration du délai de transfert ;<br/>
              - il existe une défaillance systémique dans la prise en charge des demandeurs d'asile par les autorités italiennes ;<br/>
              - le préfet de police était incompétent pour déterminer l'Etat membre responsable du traitement de sa demande d'asile et pour se prononcer sur l'existence d'une fuite et prolonger à ce titre le délai de transfert ;<br/>
              - l'arrêté de transfert n'est pas signé par l'interprète présent lors de sa notification.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, le ministre de l'intérieur et, d'autre part, M. A...; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 25 octobre 2018 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
- les représentants du ministre de l'intérieur ;<br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, représentant de M. A... ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au même jour à 12 heures ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) no 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) no 343/2003 du Conseil ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par les articles L. 741-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. <br/>
<br/>
              3. Il résulte de l'article 29 du règlement du 26 juin 2013 que le transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, cette période étant susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ". Aux termes de l'article 7 du règlement de la Commission du 2 septembre 2003 : " 1. Le transfert vers l'Etat responsable s'effectue de l'une des manières suivantes : a) à l'initiative du demandeur, une date limite étant fixée ; b) sous la forme d'un départ contrôlé, le demandeur étant accompagné jusqu'à l'embarquement par un agent de l'Etat requérant et le lieu, la date et l'heure de son arrivée étant notifiées à l'Etat responsable dans un délai préalable convenu : c) sous escorte, le demandeur étant accompagné par un agent de l'Etat requérant, ou par le représentant d'un organisme mandaté par l'Etat requérant à cette fin, et remis aux autorités de l'Etat responsable (...) ". Il résulte de ces dispositions que le transfert d'un demandeur d'asile vers un Etat membre qui a accepté sa prise ou sa reprise en charge, sur le fondement du règlement du 26 juin 2013, s'effectue selon l'une des trois modalités définies à l'article 7 cité<br/>
ci-dessus : à l'initiative du demandeur, sous la forme d'un départ contrôlé ou sous escorte. <br/>
<br/>
              4. Il résulte clairement des dispositions mentionnées au point précédent que, d'une part, la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. D'autre part, dans l'hypothèse où le transfert du demandeur d'asile s'effectue sous la forme d'un départ contrôlé, il appartient, dans tous les cas, à l'Etat responsable de ce transfert d'en assurer effectivement l'organisation matérielle et d'accompagner le demandeur d'asile jusqu'à l'embarquement vers son lieu de destination. Une telle obligation recouvre la prise en charge du titre de transport permettant de rejoindre l'Etat responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant et si nécessaire, celle du pré-acheminement du lieu de résidence du demandeur au lieu d'embarquement. Enfin, dans l'hypothèse où le demandeur d'asile se soustrait intentionnellement à l'exécution de son transfert ainsi organisé, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013 rappelées au point 3.<br/>
<br/>
              5. M.A..., ressortissant soudanais, a déclaré être entré irrégulièrement en France le 5 juillet 2017. Il a sollicité l'asile le 4 septembre 2017 et le relevé de ses empreintes a alors fait apparaître qu'il avait été précédemment identifié en Italie. Le 8 septembre 2017, l'administration française a saisi les autorités italiennes d'une demande de reprise en charge, qui a été implicitement acceptée le 8 novembre 2017 en vertu de l'article 22 du règlement du 26 juin 2013. Par un arrêté du 13 mars 2018, le préfet de police a pris un arrêté de transfert, notifié le même jour à M.A.... Par un arrêté du 2 mai 2018, le préfet de police a placé M. A...en rétention administrative en vue de son éloignement. Le 3 mai 2018, des agents de la préfecture de police se sont présentés sur son lieu d'hébergement et l'ont acheminé jusqu'à son lieu d'embarquement. M. A...a cependant refusé d'embarquer sur le vol à destination de Milan sur lequel une place lui avait été réservée. Le 4 mai 2018, les autorités italiennes ont été informées de la prolongation du délai de transfert de six à dix-huit mois en raison de la fuite de l'intéressé. M. A...a présenté une nouvelle demande d'enregistrement de sa demande d'asile le 15 mai 2018, à laquelle un refus a été opposé. Par arrêté du 20 septembre 2018, le préfet de police a, à nouveau, placé M. A...en rétention administrative. Le 24 septembre 2018, M. A... a saisi le juge de référés du tribunal administratif de Paris d'un recours sollicitant, sur le fondement de l'article L. 521-2 du code de justice administrative, l'enregistrement de sa demande d'asile en raison de l'expiration du délai de six mois dans lequel la France pouvait procéder à son transfert. Par une ordonnance du 27 septembre 2018, dont le ministre relève appel, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'enregistrer la demande d'asile de M. A...dans un délai de sept jours et de lui délivrer une attestation de demande d'asile ainsi que le dossier à présenter à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              6. Il résulte de ce qui est indiqué au point 4 que, dans l'hypothèse où l'administration a respecté les obligations qui sont les siennes dans l'organisation d'un départ contrôlé et où l'intéressé s'est soustrait intentionnellement à l'exécution de ce départ, puis a demandé à nouveau l'enregistrement de sa demande après l'expiration du délai de transfert de six mois, le demandeur doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013. Il résulte de l'instruction que l'administration a procédé à l'acheminement de M.A..., qui avait refusé l'aide qui lui avait été proposée par l'Office français de l'immigration et d'intégration pour l'exécution de son transfert, en vue de son embarquement vers l'Italie par un vol organisé le 3 mai 2018. Il résulte également de l'instruction, sans que l'intéressé le conteste sérieusement, qu'il a alors refusé de quitter le véhicule qui l'acheminait à l'aéroport d'Orly pour embarquer, en précisant qu'il souhaitait rester en France. Dans ces conditions, et alors même que M. A...a déféré aux convocations qui lui ont été faites par l'administration, le ministre de l'intérieur est fondé, en raison de l'obstruction qu'il a opposée le jour de son transfert, à estimer qu'il était en fuite au sens de l'article 29 du règlement du 26 juin 2013. C'est, dès lors, à tort que le juge des référés du tribunal administratif de Paris a jugé que M. A... n'était pas en fuite et pouvait se prévaloir du délai de six mois prévu par les dispositions du règlement précité.<br/>
<br/>
              7. Il y a lieu pour le juge des référés du Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la requête. <br/>
<br/>
              8. Toutefois, il résulte des pouvoirs confiés au juge par les dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, des délais qui lui sont impartis pour se prononcer et des conditions de son intervention que la procédure spéciale prévue par le code de l'entrée et du séjour des étrangers et du droit d'asile, applicable au recours prévu à l'article L. 742-4 de ce code contre une décision de transfert, présente des garanties au moins équivalentes à celles des procédures régies par le livre V du code de justice administrative et est exclusive de celle prévues par ce livre. Il n'en va autrement que dans le cas où les modalités selon lesquelles il est procédé à l'exécution de la décision de transfert emporteraient des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi en application du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, a statué ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à sa mise à exécution.<br/>
<br/>
              9. Par suite, M. A...ne saurait utilement faire valoir, au soutien de ses conclusions tendant à ce que sa demande d'asile soit enregistrée en France, que la décision de transfert, qu'il n'a pas contestée, aurait été prise par un préfet autre que celui qui en aurait eu la compétence ou ne lui aurait pas été notifiée dans une langue qu'il comprend. En outre, et en tout état de cause, il ne résulte pas de l'instruction que les autorités italiennes seraient dans l'incapacité structurelle d'examiner la demande d'asile de M.A..., qui ne fait au demeurant état d'aucune difficulté qu'il aurait personnellement rencontrée à ce titre.<br/>
<br/>
              10. Il résulte de tout ce qui précède qu'en refusant d'enregistrer la demande d'asile de M. A...au motif que le délai de transfert n'était pas expiré, l'administration n'a pas porté une atteinte grave et manifestement illégale au droit d'asile de l'intéressé. Il y a lieu, par suite, d'annuler l'ordonnance attaquée et de rejeter la requête de M.A.... <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 27 septembre 2018 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La requête présentée par M. A...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
