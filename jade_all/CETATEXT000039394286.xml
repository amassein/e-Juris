<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039394286</ID>
<ANCIEN_ID>JG_L_2019_11_000000418783</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/39/42/CETATEXT000039394286.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/11/2019, 418783, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418783</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418783.20191118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société French Wholesale Properties a demandé au tribunal administratif d'Amiens de prononcer la réduction de la taxe foncière sur les propriétés bâties et de la taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2015 dans les rôles de la commune de Gauchy (Aisne) à raison du local commercial dont elle est propriétaire sur le territoire de cette commune. Par un jugement n° 1603282 du 28 décembre 2017, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 mars et 5 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société French Wholesale Properties demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société French Wholesale Properties ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1498 du code général des impôts, dans sa rédaction applicable à l'année d'imposition en litige : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : (...) / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée :/ - soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date ;/ - soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société requérante est propriétaire d'un immeuble situé 9001, rue Eugène Freyssinet à Gauchy (Aisne). En jugeant que l'administration avait pu à bon droit retenir comme terme de comparaison pour l'évaluation de la valeur locative de cet immeuble, en application de la méthode prévue au a du 2° de l'article 1498 du code général des impôts précité, le local-type n° 56 du procès-verbal de la commune de Gauchy, sans rechercher si, dans les circonstances particulières de l'espèce, ce local était loué à des conditions de prix normales au 1er janvier 1970 ou avait été évalué par comparaison avec un local loué dans de telles conditions à cette date et s'il pouvait toujours servir de terme de comparaison au 1er janvier 2015 compte tenu notamment des mentions figurant au procès-verbal de la commune, le tribunal a commis une erreur de droit. Dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société French Wholesale Properties est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à la société French Wholesale Properties au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 28 décembre 2017 du tribunal administratif d'Amiens est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Amiens. <br/>
Article 3 : L'Etat versera une somme de 2 000 euros à la société French Wholesale Properties au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société French Wholesale Properties et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
