<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029893521</ID>
<ANCIEN_ID>JG_L_2014_12_000000381712</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/89/35/CETATEXT000029893521.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 12/12/2014, 381712, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381712</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:381712.20141212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...E...et Mme C...E..., demeurant ... ; M. et Mme E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1401391 du 28 mai 2014 par lequel le tribunal administratif de Rennes a rejeté leur protestation dirigée contre les opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux dans la commune d'Aucaleuc (Côte d'Armor) ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 décembre 2014, présentée par M. et Mme E...; <br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 65 du code électoral dans sa rédaction alors en vigueur : " (...) Le bureau désigne parmi les électeurs présents un certain nombre de scrutateurs sachant lire et écrire, lesquels se divisent par tables de quatre au moins. Si plusieurs candidats ou plusieurs listes sont en présence, il leur est permis de désigner respectivement les scrutateurs, lesquels doivent être répartis également autant que possible par chaque table de dépouillement. Le nombre de tables ne peut être supérieur au nombre d'isoloirs. / Les enveloppes contenant les bulletins sont regroupées par paquet de 100. Ces paquets sont introduits dans des enveloppes spécialement réservées à cet effet. Dès l'introduction d'un paquet de 100 bulletins, l'enveloppe est cachetée et y sont apposées les signatures du président du bureau de vote et d'au moins deux assesseurs représentant, sauf liste ou candidat unique, des listes ou des candidats différents. / A chaque table, l'un des scrutateurs extrait le bulletin de chaque enveloppe et le passe déplié à un autre scrutateur ; celui-ci le lit à haute voix ; les noms portés sur les bulletins sont relevés par deux scrutateurs au moins sur des listes préparées à cet effet. Si une enveloppe contient plusieurs bulletins, le vote est nul quand les bulletins portent des listes et des noms différents. Les bulletins multiples ne comptent que pour un seul quand ils désignent la même liste ou le même candidat. (...) " ; qu'aux termes de l'article R. 63 du même code : " Le dépouillement suit immédiatement le dénombrement des émargements. Il doit être conduit sans désemparer jusqu'à son achèvement complet. / Les tables sur lesquelles s'effectue le dépouillement sont disposées de telle sorte que les électeurs puissent circuler autour " ; qu'en vertu de l'article R. 64 du même code : " Le dépouillement est opéré par des scrutateurs sous la surveillance des membres du bureau. / A défaut de scrutateurs en nombre suffisant, le bureau de vote peut y participer " ; que, selon l'article R. 65 du même code : " Les scrutateurs désignés, en application de l'article L. 65, par les candidats ou mandataires des listes en présence ou par les délégués prévus à l'article R. 47, sont pris parmi les électeurs présents ; les délégués peuvent être également scrutateurs. Leurs nom, prénoms et date de naissance sont communiqués au président du bureau au moins une heure avant la clôture du scrutin. Ces scrutateurs sont affectés aux tables de dépouillement de telle sorte que la lecture des bulletins et l'inscription des suffrages soient, autant que possible, contrôlées simultanément par un scrutateur de chaque candidat ou de chaque liste " ; qu'enfin, aux termes des dispositions du troisième alinéa de l'article R. 65-1 du même code : " Après avoir vérifié que les enveloppes de centaines sont conformes aux dispositions du deuxième alinéa de l'article L. 65, les scrutateurs les ouvrent, en extraient les enveloppes électorales et procèdent comme il est dit au troisième alinéa dudit article " ; <br/>
<br/>
              2.	Considérant, en premier lieu, que ces dispositions ne faisaient pas obstacle à ce que le bureau de vote désigne comme scrutateurs, parmi les électeurs présents, des membres du conseil municipal ; qu'il n'est pas allégué que les scrutateurs désignés n'auraient pas eu la qualité d'électeurs ; que, par ailleurs, il est constant que la table de dépouillement comportait au moins quatre personnes ;<br/>
<br/>
              3.	Considérant, en deuxième lieu, que la seule circonstance que, au cours des opérations de dépouillement, certaines des prescriptions fixées par les dispositions précédemment citées n'aient pas été respectées n'est pas de nature à justifier l'annulation des opérations électorales, dès lors que les irrégularités commises n'ont pas conduit à fausser les résultats du scrutin ; qu'il résulte de l'instruction que les opérations de dépouillement se sont déroulées en présence de nombreux électeurs ; qu'il n'est pas établi qu'en l'espèce la disposition de la table de dépouillement dans la salle de scrutin, qui permettait aux électeurs de se placer directement face à elle ainsi qu'à son extrémité, aurait empêché ces derniers d'exercer un contrôle sur les opérations de dépouillement et notamment sur la validité des bulletins ; qu'il n'est pas non plus établi que la circonstance que les scrutateurs chargés de reporter les résultats sur les listes prévues à cet effet ont siégé à une table différente de celle où étaient placés les bulletins aurait conduit à des erreurs dans la comptabilisation des votes ni qu'elle aurait rendu impossible le contrôle des opérations de dépouillement par les électeurs ; que si les appelants soutiennent que les enveloppes contenant les paquets de cent enveloppes de vote n'ont pas été cachetées après l'introduction des paquets d'enveloppes, et s'ils produisent une photographie prise lors des opérations du dépouillement par un organe de presse sur laquelle figurent des enveloppes de centaines ouvertes, aucune observation sur les opérations de dépouillement, concernant des irrégularités dans le décompte des enveloppes trouvées dans l'urne, ne figure sur le procès-verbal ; qu'ainsi, cette irrégularité ne peut être tenue pour établie ; <br/>
<br/>
              4.	Considérant, enfin, que la durée du dépouillement n'a pas, par elle-même, d'incidence sur la régularité de celui-ci ; que si, en l'espèce, cette durée a été particulièrement longue, il n'est pas établi qu'elle aurait conduit à des erreurs de comptabilisation des bulletins ni à rendre impossible le contrôle du dépouillement par le public, le procès-verbal de l'élection ne comportant d'ailleurs à cet égard aucune observation ni réclamation ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que M. et Mme E...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a rejeté leur protestation ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. et Mme E...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...E..., à Mme C...E..., à M. G...D..., à Mme F...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
