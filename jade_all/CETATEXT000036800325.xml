<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800325</ID>
<ANCIEN_ID>JG_L_2018_04_000000397257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/03/CETATEXT000036800325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/04/2018, 397257, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:397257.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 3 avril 2003 de l'inspectrice du travail de Lens autorisant Maîtres Jérôme Theetten et Philippe Martin, liquidateurs judiciaires de la société Metaleurop Nord, à procéder à son licenciement et la décision du 18 août 2003 par laquelle le ministre des affaires sociales, du travail et de la solidarité a rejeté le recours dirigé contre cette décision. Par un jugement n° 1202798 du 2 octobre 2013, le tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 13DA01902,13DA01939 du 31 décembre 2015, la cour administrative d'appel de Douai a rejeté les appels formés, d'une part, par Maîtres Theetten et Martin et, d'autre part, par la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social contre ce jugement. <br/>
<br/>
<br/>
              Procédures devant le Conseil d'Etat : <br/>
<br/>
              1° Sous le n° 397257, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 février, 24 mai et 14 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Recylex demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 397554, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er mars et 1er juin 2016 et le 25 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Maîtres Jérôme Theetten et Philippe Martin demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt du 31 décembre 2015 de la cour administrative d'appel de Douai ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Recylex, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...et à la SCP Baraduc, Duhamel, Rameix, avocat de Maîtres Theetten et Martin ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que, par une décision du 3 avril 2003, l'inspectrice du travail de la 2ème section de Lens a autorisé Maîtres Theetten et Martin, liquidateurs judiciaires de la société Metaleurop Nord, filiale de la société Metaleurop SA, à licencier M.A..., salarié protégé ; que, par une décision implicite du 16 août 2003, confirmée par une décision expresse du 18 août suivant, le ministre des affaires sociales, du travail et de la solidarité a rejeté le recours formé contre la décision de l'inspectrice du travail ; que M. A...n'a demandé l'annulation des décisions du 3 avril et du 18 août 2003 que par une demande enregistrée le 6 décembre 2012 au greffe du tribunal administratif de Lille ; que, par les pourvois visés ci-dessus, la société Recylex, venant aux droits de la société Metaleurop SA, d'une part, et Maîtres Theetten et Martin, d'autre part, demandent l'annulation de l'arrêt du 31 décembre 2015 par lequel la cour administrative d'appel de Douai a rejeté leurs requêtes dirigées contre le jugement du 2 octobre 2013 par lequel le tribunal administratif de Lille a annulé ces deux décisions ; que ces pourvois étant dirigés contre le même arrêt, il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la recevabilité du pourvoi de la société Recylex : <br/>
<br/>
              2. Considérant que la société Recylex, qui est actionnaire à 99 % de la société Metaleurop Nord et qui se prévaut de sa qualité de " co-employeur " de M.A..., n'a pas été appelée en défense lors de l'instance introduite par ce dernier devant le tribunal administratif de Lille et n'a, ainsi, pas été présente à cette instance ; qu'elle doit toutefois être regardée comme ayant été représentée comme défendeur à cette instance par Maîtres Theetten et Martin, liquidateurs de la société Metaleurop Nord, employeur de M. A...; <br/>
<br/>
              3. Considérant, par suite, que la société Recylex n'aurait pas été recevable à faire tierce-opposition contre le jugement du tribunal administratif ; qu'ainsi, son intervention en demande devant la cour administrative d'appel ne lui confère pas la qualité de partie en instance d'appel ; que M. A...est, dès lors, fondé à soutenir que son pourvoi est irrecevable et qu'il doit, pour ce motif, être rejeté ;<br/>
<br/>
              Sur le pourvoi de Maîtres Theetten et Martin :<br/>
<br/>
              4. Considérant que, devant les juridictions administratives et dans l'intérêt d'une bonne justice, le juge a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci ; qu'il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser ; que, s'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser ; que, dans le cas particulier où cette production contient l'exposé d'une circonstance de fait ou d'un élément de droit dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et qui est susceptible d'exercer une influence sur le jugement de l'affaire, le juge doit en tenir compte, à peine d'irrégularité de sa décision ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que, postérieurement à l'audience publique du 3 décembre 2015 devant la cour administrative d'appel de Douai, M. A...a, par une note en délibéré du 14 décembre 2015, soutenu que sa demande de première instance n'était pas tardive, compte tenu d'une décision du Conseil d'Etat, statuant au contentieux du 7 décembre 2015, selon laquelle les délais de recours contre une décision administrative expresse prise en matière d'autorisation de licenciement ne sont opposables qu'à la condition d'avoir été mentionnés dans sa notification, y compris lorsqu'il s'agit d'une décision du ministre chargé du travail saisi par la voie du recours hiérarchique ; qu'il ressort des énonciations de l'arrêt attaqué que, pour rejeter les appels dont elle était saisie, la cour administrative d'appel a repris les motifs de la décision précitée du Conseil d'Etat, sans rouvrir l'instruction ; que l'arrêt attaqué a ainsi été rendu à l'issue d'une procédure irrégulière ; que, par suite, Maîtres Theetten et Martin sont fondés, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette leur requête ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel formé par Maîtres Theetten et Martin contre le jugement du tribunal administratif de Lille du 2 octobre 2013 ;<br/>
<br/>
              7. Considérant que la société Recylex justifie d'un intérêt suffisant à l'annulation du jugement attaqué ; qu'ainsi, son intervention est recevable ; <br/>
<br/>
              8. Considérant que le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci en a eu connaissance ; que dans une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable ; qu'en règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance ; qu'il appartient au juge administratif de faire application de cette règle au litige dont il est saisi, quelle que soit la date des faits qui lui ont donné naissance ;<br/>
<br/>
              9. Considérant qu'il n'est établi, ni que le recours hiérarchique formé contre la décision de l'inspectrice du travail du 3 avril 2003 a fait l'objet de l'accusé de réception prévu à l'époque par les dispositions de l'article 19 de la loi du 12 avril 2000 sur les droits des citoyens dans leurs relations avec les administrations, ni que la décision expresse de rejet du ministre chargé du travail du 18 août 2003 a été notifiée à M. A...; que, d'ailleurs, il ressort des pièces du dossier que les archives administratives qui auraient, le cas échéant, comporté les documents susceptibles de l'établir, ont été détruites au terme de deux ans, en vertu de règles établies conjointement par la direction des archives de France et la direction générale du travail ; que, par suite, les délais de recours fixés par le code de justice administrative ne sont pas opposables à M. A..., ni en ce qui concerne la décision implicite de rejet du 16 août 2003 ni en ce qui concerne la décision expresse du 18 août 2003 ; <br/>
<br/>
              10. Considérant, toutefois, qu'il ressort également des pièces du dossier que M. A... dont le licenciement est intervenu le 8 avril 2003, a engagé, dans le courant de l'année 2005, une action indemnitaire contre la société Recylex devant le conseil de prud'hommes de Lens, en soutenant qu'il justifiait d'un préjudice distinct de celui résultant de son licenciement, né de la méconnaissance des engagements pris par la société Recylex dans le cadre du plan de sauvegarde de l'emploi de la société Metaleurop Nord ; qu'il résulte tant de cette action contentieuse, dans laquelle l'intéressé ne soulevait pas la nullité de son licenciement, que des énonciations non contestées de l'arrêt du 17 décembre 2010 de la cour d'appel de Douai, qui relève qu'il avait connaissance des décisions administratives le concernant, que M. A...doit être regardé comme ayant eu connaissance, au plus tard dans le courant de l'année 2005, de la décision autorisant son licenciement et du rejet du recours hiérarchique dirigé contre cette décision ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que, si le délai de deux mois fixé par les articles R. 421-1 et R. 421-2 du code de justice administrative n'était pas opposable à M.A..., le recours dont il a saisi le tribunal administratif de Lille plus de six ans après qu'il a eu connaissance de la décision autorisant son licenciement excédait, en revanche, le délai raisonnable durant lequel il pouvait être exercé ; que Maîtres Theetten et Martin sont ainsi fondés à soutenir que la demande présentée par M. A...était tardive et, par suite, irrecevable ; que, sans qu'il soit besoin d'examiner les autres moyens de la requête, le jugement attaqué doit être annulé ;<br/>
<br/>
              12. Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur la demande présentée par M. A...devant le tribunal administratif de Lille ;<br/>
<br/>
              13. Considérant qu'ainsi qu'il vient d'être dit, la demande présentée par M. A... est tardive ; qu'elle est, par suite, irrecevable ; <br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Maîtres Theetten et Martin, qui ne sont pas, dans la présente instance, la partie perdante la somme que demande, à ce titre, M. A... ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Recylex la somme que demande, au même titre, M.A... ; qu'enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre, tant en appel qu'en cassation, par Maîtres Theetten et Martin ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Recylex est rejeté.<br/>
<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Douai du 31 décembre 2015 est annulé en tant qu'il rejette la requête de Maîtres Theetten et Martin.<br/>
Article 3 : L'intervention de la société Recylex présentée devant la cour administrative d'appel de Douai est admise. <br/>
Article 4 : Le jugement du 2 octobre 2013 du tribunal administratif de Lille est annulé.<br/>
Article 5 : La demande présentée par M. A...devant le tribunal administratif de Lille et le surplus de ses conclusions de cassation présenté au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 6 : Les conclusions présentées, tant en appel qu'en cassation, par Maîtres Theetten et Martin au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à Maîtres Jérôme Theetten et Philippe Martin, à la société Recylex et à M. B...A.... <br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
