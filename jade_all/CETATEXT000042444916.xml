<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042444916</ID>
<ANCIEN_ID>JG_L_2020_10_000000445057</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/44/49/CETATEXT000042444916.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/10/2020, 445057, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445057</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445057.20201013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... A..., agissant tant en son nom propre qu'en sa qualité de représentant légal de son fils mineur M. B... A..., a demandé au juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au président du conseil départemental de l'Aude de mettre en oeuvre le placement de M. B... A... auprès de l'aide sociale à l'enfance, sous astreinte de 1 000 euros par jour de retard à compter du lendemain du prononcé de l'ordonnance. Par une ordonnance n° 2004053 du 17 septembre 2020, le juge des référés du tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 et 8 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... A..., agissant tant en son nom propre qu'en sa qualité de représentant légal de son fils mineur B... A..., demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes présentée devant le juge des référés du tribunal administratif de Montpellier ; <br/>
<br/>
              3°) de mettre à la charge du département de l'Aude la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie eu égard aux graves dangers dans lesquels se trouve M. B... A... en raison de l'absence de mise en oeuvre de la mesure du placement de l'aide sociale à l'enfance qui a été décidée le 6 mars 2020 par le juge des enfants du tribunal judiciaire de Carcassonne et confirmée par la cour d'appel de Montpellier le 9 juillet 2020 ;<br/>
              - la décision de placement auprès des services de l'aide sociale à l'enfance ne peut être regardée comme mise en oeuvre en l'absence d'hébergement de M. B... A... au sein d'une structure adaptée, une simple " prise en charge " par une association ne suffisant pas à caractériser une mise en oeuvre effective du placement ;<br/>
              - le refus du service d'aide sociale à l'enfance du département de l'Aude de mettre en oeuvre la mesure de placement de M. B... A... porte une atteinte grave et manifestement illégale à l'exigence de protection des intérêts supérieurs de l'enfant.<br/>
              Par un mémoire en défense, enregistré le 7 octobre 2020, le département de l'Aude conclut au rejet de la requête et à ce que soit mise à la charge de M. C... A... la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Il soutient qu'aucun des moyens de la requête n'est de nature à révéler une carence portant une atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A... et, d'autre part, le département de l'Aude ;<br/>
<br/>
              A été entendu lors de l'audience publique du 12 octobre 2020 à 10 heures 30, lors de laquelle M. A... n'était ni présent, ni représenté ;<br/>
<br/>
              - Me Gaschignard, avocat au Conseil d'Etat et à la Cour de cassation, avocat du département de l'Aude ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ".<br/>
              2. D'une part, l'article 375 du code civil dispose que : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public. (...) ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ". <br/>
              3. D'autre part, l'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". <br/>
<br/>
               4. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, en tenant compte des moyens dont l'administration départementale dispose ainsi que de la situation du mineur intéressé, quelles sont les mesures qui peuvent être utilement ordonnées sur le fondement de l'article L. 521-2.<br/>
<br/>
              5. Par un jugement du 6 mars 2020, le juge des enfants du tribunal judiciaire de Carcassonne, tout en appelant M. C... A... à une coopération avec les services de l'administration et à " une remise en question profonde " de son attitude, a ordonné le renouvellement de la mesure de placement auprès de l'aide sociale à l'enfance à compter du 6 mars 2020 de son fils M. B... A..., mineur né le 14 novembre 2003. Par un arrêt du 9 juillet 2020, la chambre des mineurs de la cour d'appel de Montpellier, saisie d'un appel du président du conseil départemental de l'Aude, a confirmé ce jugement, tout en précisant qu'il n'y avait pas lieu de " céder aux demandes pressantes de M. A... d'obtenir une réponse institutionnelle immédiate ". M. C... A... relève appel de l'ordonnance du 17 septembre 2020 par laquelle le juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il enjoigne au président du conseil départemental de l'Aude de mettre en oeuvre sans délai cette mesure de placement.<br/>
<br/>
              6. Il résulte de l'instruction que M. B... A..., après avoir fait l'objet de la mesure mentionnée ci-dessus de placement auprès des services de l'aide sociale à l'enfance du conseil départemental de l'Aude, a été reçu avec son père M. C... A... à la demande de ces services à compter du mois de juillet 2020 par l'association PEP 11, qui leur a proposé, eu égard aux nombreux et graves échecs de prise en charge rencontrés par le passé, l'entrée de l'intéressé dans un dispositif expérimental d'accompagnement éducatif individualisé. M. C... A... a estimé que cette proposition, qui comprenait dans un premier temps une phase d'observation sans hébergement, ne suffisait pas à assurer la mise en oeuvre des obligations incombant au département. Il résulte cependant de l'instruction, et il n'est pas contesté, que, malgré les délais de mise en place auxquels ont contribué tant son comportement que celui de son père, M. B... A... est, depuis le 25 septembre 2020, engagé dans le programme d'accompagnement individualisé mis en oeuvre par l'association PEP 11, dès lors que, après avoir été hébergé dans un appartement hôtel, il a suivi un programme de rupture avec son environnement dans le Pays basque, avant d'être maintenant hébergé dans le Tarn où, suivi par un éducateur, il prépare avec celui-ci la suite de sa prise en charge par la définition d'un projet de formation et d'insertion. <br/>
<br/>
              7. Les conditions de prise en charge de M. B... A... par les services d'aide sociale à l'enfance ne révélant dès lors pas de carence, au sens décrit au point 4 ci-dessus, de nature à justifier l'intervention du juge des référés en application de l'article L. 521-2 du code de justice administrative, M. A... n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Montpellier a rejeté sa demande. Il y a lieu, par suite, de rejeter sa requête, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, en revanche, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le département de l'Aude et de mettre à la charge de M. A... la somme de 1 500 euros en application de ces dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : M. C... A... versera la somme de 1 500 euros au département de l'Aude au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente ordonnance sera notifiée à M. C... A... et au département de l'Aude.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
