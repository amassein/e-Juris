<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027362529</ID>
<ANCIEN_ID>JG_L_2013_04_000000357222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/36/25/CETATEXT000027362529.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/04/2013, 357222, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357222.20130426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 février et 25 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Saint-Servais, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1994 du 27 décembre 2011 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, en tant qu'il fixe sa population totale, pour l'application des lois et règlements à compter du 1er janvier 2012, à un chiffre de 676 habitants, qu'elle estime inférieur à la réalité ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre un nouveau décret authentifiant les résultats du recensement de la population de la commune au 1er janvier 2009, dans un délai de deux mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la décision du Conseil d'Etat, statuant au contentieux du 5 juillet 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Saint-Servais,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Saint-Servais ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, qu'aux termes de l'article R. 2151-1 du code général des collectivités territoriales : " I. - Les personnes prises en compte dans les catégories de population définies ci-dessous sont les personnes résidant dans les logements d'une commune, celles résidant dans les communautés telles que définies aux V et VI du présent article, les personnes sans abri et les personnes résidant habituellement dans des habitations mobiles. / II. - Les catégories de population sont : / 1. La population municipale ; / 2. La population comptée à part ; / 3. La population totale, qui est la somme des deux précédentes. / III. - La population municipale d'une commune, mentionnée au 1 du II du présent article, comprend : / 1. Les personnes ayant leur résidence habituelle sur le territoire de la commune. La résidence habituelle, au sens du présent décret, d'une personne ayant plusieurs résidences en France métropolitaine, dans les départements d'outre-mer ou à Saint-Pierre-et-Miquelon est : / b) Pour une personne résidant dans une communauté appartenant aux catégories 1 à 3 définies au VI du présent article, la communauté ; / (...) V. - Une communauté est un ensemble de locaux d'habitation relevant d'une même autorité gestionnaire et dont les habitants partagent à titre habituel un mode de vie commun. La population de la communauté comprend les personnes qui résident dans la communauté, à l'exception de celles résidant dans des logements de fonction. / VI. - Les catégories de communautés sont : / (...) 3. Les casernes, quartiers, bases ou camps militaires ou assimilés ; (...) " ; que, pour l'application de ces dispositions aux personnes séjournant dans des casernes, quartiers, bases ou camps militaires situés sur le territoire de plusieurs communes, il appartient à l'institut national de la statistique et des études économiques (INSEE) de répartir la population recensée entre ces communes en tenant compte de la situation des locaux d'habitation et de l'utilisation par ces personnes des principaux services publics dont la charge incombe à la commune ; que, dans le cas où ces personnes utiliseraient principalement des services publics à la charge d'une ou de plusieurs communes autres que celles sur le territoire desquelles les locaux d'habitation sont situés, la répartition de la population correspondant à la situation des locaux d'habitation devrait être pondérée en conséquence ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, si la base aéronautique navale de Landivisiau est située pour partie sur le territoire de la commune de Saint-Servais et pour partie sur celui de la commune de Bodilis, les locaux d'habitation de la base sont situés sur le territoire de la seule commune de Bodilis ; qu'à la date du décret attaqué, la distribution d'eau dans la base et l'assainissement étaient assurés par la commune de Bodilis, la collecte des déchets faisait l'objet d'un contrat conclu directement entre la base et un prestataire externe et les principales routes permettant d'accéder à la base étaient des routes départementales, dont l'entretien incombe au département ; qu'ainsi, et en dépit du fait que des engins militaires empruntent la voirie communale de Saint-Servais, ce qui a conduit la commune à y effectuer des travaux de sécurisation, les personnes résidant sur la base n'utilisaient pas principalement des services publics à la charge de la commune de Saint-Servais ; que, par suite, la répartition des personnes résidant à la base aéronautique navale entre les communes de Saint-Servais et de Bodilis n'est pas entachée d'erreur de droit ;<br/>
<br/>
              3. Considérant, en second lieu, que, par sa décision du 5 juillet 2010, le Conseil d'Etat, statuant au contentieux a annulé le décret du 30 décembre 2008 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, en tant que ce décret fixait la population de la commune de Saint-Servais pour l'application des lois et règlements à compter du 1er janvier 2009 ; que, dès lors que le décret attaqué fixe la population de la commune de Saint-Servais pour l'application des lois et règlements à compter du 1er janvier 2012, il n'a pas méconnu l'autorité de la chose jugée attachée à cette décision ; que ce moyen doit, par suite, être écarté ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que la commune de Saint-Servais n'est pas fondée à demander l'annulation pour excès de pouvoir du décret attaqué ; que, par voie de conséquence, il y a lieu de rejeter ses conclusions tendant à ce qu'il soit enjoint au Premier ministre d'édicter un nouveau décret ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Saint-Servais est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la commune de Saint-Servais, au ministre de l'économie et des finances, au ministre de l'intérieur et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
