<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311417</ID>
<ANCIEN_ID>JG_L_2014_07_000000380492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/14/CETATEXT000029311417.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/07/2014, 380492, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380492.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 mai 2014 et 3 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat de la presse hebdomadaire régionale, dont le siège est 72, rue d'Hauteville à Paris (75010) ; le syndicat demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1404041 du 5 mai 2014 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution des arrêtés du préfet du Val d'Oise des 25 mars et 8 avril 2014 abrogeant son arrêté du 30 décembre 2013 et fixant la liste des journaux habilités à publier les annonces judiciaires et légales pour 2014 dans le département du Val d'Oise ; <br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 55-4 du 4 janvier 1955 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat du Syndicat de la presse hebdomadaire régionale et à la SCP Piwnica, Molinié, avocat de la société Les Echos ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que la condition à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; <br/>
<br/>
              2. Considérant que, par l'ordonnance attaquée du 5 mai 2014, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté, pour défaut d'urgence, la demande du Syndicat de la presse hebdomadaire régionale tendant à la suspension des arrêtés du préfet du Val d'Oise des 28 mars et 8 avril 2014 relatifs à la liste des journaux habilités à publier des annonces judiciaires et légales pour l'année 2014 ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il appartient au juge des référés afin, notamment, de mettre le juge de cassation en mesure d'exercer son contrôle, de faire apparaître les raisons de droit et de fait pour lesquelles, soit il considère que l'urgence justifie la suspension de l'acte attaqué, soit il estime qu'elle ne la justifie pas ; que, pour estimer que l'urgence ne justifiait pas, en l'espèce, la suspension demandée, le juge des référés du tribunal administratif de Cergy-Pontoise ne s'est pas borné, contrairement ce que soutient le Syndicat de la presse hebdomadaire régionale, à juger que la circonstance qu'une décision administrative est immédiatement exécutoire et, dans le cas de l'arrêté attaqué, qu'elle exerce ses effets jusqu'au terme de l'année civile, n'est pas de nature, en elle-même et à elle seule, à caractériser une situation d'urgence ; qu'il a, en effet, également apprécié concrètement, compte tenu des justifications fournies par le syndicat requérant, si les effets de l'acte litigieux étaient de nature à caractériser une urgence justifiant la suspension demandée ; que, ce faisant, le juge des référés, qui n'est pas tenu de répondre à tous les arguments du demandeur, a mis le juge de cassation à même d'exercer un contrôle sur les motifs qu'il a retenus et n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en second lieu, que le juge des référés ne saurait, lorsqu'il recherche s'il y a, au sens des dispositions de l'article L. 521-1 du code de justice administrative, urgence à prendre, avant tout jugement au fond, les mesures conservatoires prévues par ce texte, se fonder sur la nécessité de prévenir les conséquences d'une éventuelle annulation de la décision litigieuse mais doit apprécier, à la date à laquelle il statue, les conséquences de l'éventuelle illégalité de la décision attaquée ; <br/>
<br/>
              5. Considérant que, pour juger que l'urgence dont se prévalait le Syndicat de la presse hebdomadaire régionale n'était pas établie, le juge des référés du tribunal administratif de Cergy-Pontoise a pu estimer, eu égard à son office et compte tenu des éléments qui lui ont été fournis, que l'illégalité éventuelle de habilitation délivrée à la société Les Echos n'aurait pas pour conséquence nécessaire de rendre illégales les publications auxquelles ce journal aura procédé ni, a fortiori, de vicier les actes et procédures requérant une telle publicité eu égard, notamment, à la théorie des apparences ; que, ce faisant, le juge des référés, qui a suffisamment motivé son ordonnance sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le Syndicat de la presse hebdomadaire régionale n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque ; que doivent être rejetées, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Les Echos au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du Syndicat de la presse hebdomadaire régionale est rejeté. <br/>
Article 2 : Les conclusions présentées par la société Les Echos au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 3 : La présente décision sera notifiée au Syndicat de la presse hebdomadaire régionale, à la société Les Echos et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
