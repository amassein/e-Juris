<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043424678</ID>
<ANCIEN_ID>JG_L_2021_04_000000450423</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/42/46/CETATEXT000043424678.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/04/2021, 450423, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450423</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450423.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8, 17 et 20 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société Cours Progress demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution, d'une part, du décret n° 2021-209 du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2021 pour l'année scolaire 2020-2021 et du décret n° 2021-210 du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2022 pour l'année scolaire 2020-2021 en tant qu'ils excluent de leur champ d'application les élèves inscrits dans des établissements d'enseignement privé hors contrat et, d'autre part, de la note de service du 23 février 2021 relative au calendrier 2021 du baccalauréat dans le contexte de l'épidémie de la covid-19 publiée au Bulletin officiel de l'éducation nationale, de la jeunesse et des sports n° 8 du 25 février 2021 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre un décret intégrant les candidats au baccalauréat scolarisés dans les établissements privés hors contrat dans le champ d'application des décrets n° 2021-209 et n° 2021-210 du 25 février 2021 prévoyant de retenir les notes obtenues au titre du continu dans les enseignements communs et de spécialités pour les sessions 2021 et 2022 du baccalauréat ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable dès lors qu'elle justifie d'un intérêt à agir, eu égard à son objet social, qui comprend notamment la formation et l'éducation des jeunes ainsi que la gestion d'établissements scolaires ;<br/>
              - la condition d'urgence est satisfaite en ce que les dispositions des décrets et de la note de service contestés portent une atteinte grave et immédiate à sa situation et aux intérêts des élèves scolarisés dans les établissements privés hors contrat eu égard à l'imminence des épreuves de la session 2021 du baccalauréat qui se dérouleront à compter du 7 juin 2021, aux incertitudes quant au déroulement des examens et au traitement discriminatoire entre ces élèves et les candidats au baccalauréat scolarisés dans les établissements publics et privés sous contrat, qui sont dispensés des épreuves portants sur les matières communes et les enseignements de spécialités ; <br/>
              - il existe un doute sérieux quant à la légalité des dispositions attaquées ; <br/>
              - elles méconnaissent les dispositions de l'ordonnance n° 2020-1694 du 24 décembre 2020 relative à l'organisation des concours et examens pendant la crise sanitaire née de l'épidémie de covid-19 l'exclusion des élèves des établissements hors contrat du champ d'application du dispositif du contrôle continu n'étant pas justifiée au regard de l'objectif de ce texte, qui consiste à limiter les risques de propagation de l'épidémie à l'occasion des examens et concours ; <br/>
              - elles méconnaissent le principe d'égalité de traitement entre les candidats au baccalauréat en instituant entre, d'une part, les élèves des établissements d'enseignement public et privés sous contrat et ceux inscrits auprès du Centre national d'enseignement à distance et, d'autre part, les élèves de l'enseignement privé hors contrat une différence de traitement dépourvue de lien avec l'objectif des auteurs de l'ordonnance du 24 décembre 2020, consistant à lutter contre la propagation du virus ; <br/>
              - elles méconnaissent le principe d'égal accès à l'enseignement supérieur dès lors qu'en raison de la situation sanitaire actuelle, un nombre important d'élèves ne pourra prendre part aux épreuves qui se dérouleront au mois de juin 2021 et devra subir les épreuves de remplacement en septembre, les plaçant ainsi dans une situation défavorable pour les inscriptions dans l'enseignement supérieur par rapport aux étudiants issus de l'enseignement public ou privé sous contrat qui, ayant bénéficié du contrôle continu, pourront obtenir leurs résultats du baccalauréat dès le mois de juillet. <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 mars 2021, le ministre de l'éducation nationale, de la jeunesse et des sports conclut au rejet de la requête. Il soutient qu'elle est irrecevable eu égard à l'absence d'intérêt à agir de la société requérante, que la condition d'urgence n'est pas satisfaite et qu'aucun des moyens invoqués n'est de nature à créer un doute sérieux quant à la légalité de des décrets et de la note de service contestés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-1379 du 14 novembre 2020 ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ; <br/>
              - l'ordonnance n° 2020-351 du 27 mars 2020 ;<br/>
              - l'ordonnance n° 2020-1694 du 24 décembre 2020 ;<br/>
              - le décret n° 2018-614 du 16 juillet 2018 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-209 du 25 février 2021 ;<br/>
              - le décret n° 2021-210 du 25 février 2021 ;<br/>
              - l'arrêté du 16 juillet 2018 modifié relatif aux épreuves du baccalauréat général à compter de la session de 2021 ;<br/>
              - l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et technologique ; <br/>
              - l'arrêté du 11 octobre 2019 modifiant l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Cours Progress, et d'autre part, le ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 22 mars 2021, à 11 heures : <br/>
<br/>
              - Me Delamarre, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société requérante ;<br/>
<br/>
              - les représentants de la société requérante ; <br/>
<br/>
              - les représentants du ministre de l'éducation nationale, de la jeunesse et des sports ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              2. L'article 20 du décret du 16 juillet 2018 relatif aux enseignements conduisant au baccalauréat général et aux formations technologiques conduisant au baccalauréat technologique a modifié l'article D. 334-4 du code de l'éducation en disposant, pour ce qui est du baccalauréat général, que : " L'évaluation des enseignements obligatoires repose sur des épreuves terminales et sur des évaluations de contrôle continu tout au long du cycle terminal " et qu'" Un arrêté du ministre chargé de l'éducation nationale définit les modalités d'organisation du contrôle continu pour le baccalauréat général et les conditions dans lesquelles est attribuée une note de contrôle continu aux candidats qui ne suivent les cours d'aucun établissement, aux candidats inscrits dans un établissement d'enseignement privé hors contrat, aux candidats scolarisés au Centre national d'enseignement à distance et aux sportifs de haut niveau (...) ".<br/>
<br/>
              3. Pour l'application de ces dispositions, l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et technologique a fixé, par ses articles 1er à 8, les modalités d'organisation de ce contrôle pour les candidats scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat. Les articles 1er et 2 de l'arrêté du 16 juillet 2018 prévoient, d'une part, que les candidats scolarisés dans les établissements d'enseignement publics ou privés sous contrat subissent trois sessions d'épreuves de contrôle continu, deux en classe de première et une en classe de terminale et, d'autre part, que la note de contrôle continu attribuée aux candidats au baccalauréat qui sont scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat compte pour quarante pour cent des coefficients attribués pour l'examen et est fixée, pour une part de trente pour cent, sur la base de trois sessions d'épreuves dites " évaluations communes " et, pour une part de dix pour cent, sur la base de l'évaluation des résultats de l'élève au cours du cycle terminal, telle qu'elle résulte des notes attribuées par ses professeurs. Le I de l'article 9 de de l'arrêté du 16 juillet 2018 prévoit en revanche que, pour les candidats scolarisés dans les établissements privés hors contrat, ceux-ci sont convoqués à une évaluation ponctuelle pour l'enseignement de spécialité ne donnant pas lieu à une épreuve terminale et à une évaluation ponctuelle pour chacun des autres enseignements faisant l'objet d'évaluations communes de contrôle continu, la note de contrôle continu mentionnée à l'article 1er  de ce arrêté étant fixée, conformément au II de son article 9, en tenant compte des notes obtenues aux évaluations ponctuelles prévues au I de ce même article pour l'enseignement de spécialité ne donnant pas lieu à une évaluation terminale et pour chacun des autres enseignements faisant l'objet d'évaluations communes. Enfin, par un arrêté du 11 octobre 2019, le ministre de l'éducation nationale, de la jeunesse et des sports a modifié l'article 2 et le I de l'article 9 de l'arrêté du 16 juillet 2018 en reportant du deuxième au troisième trimestre de l'année de terminale la série d'épreuves communes de contrôle continu passées en classe de terminale, tant par les candidats scolarisés dans l'enseignement public et dans l'enseignement privé sous contrat que pour les autres candidats.<br/>
<br/>
              4. Par ailleurs, aux termes de l'article 3 de l'ordonnance du 24 décembre 2020 relative à l'organisation des concours et examens pendant la crise sanitaire née de l'épidémie de covid-19, prise sur le fondement de l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et de l'article 10 de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire : " Nonobstant toute disposition législative ou réglementaire contraire, les autorités compétentes pour la détermination des modalités d'accès aux formations de l'enseignement supérieur dispensées par les établissements relevant des livres IV et VII du code de l'éducation ainsi que pour la détermination des modalités de délivrance des diplômes de l'enseignement supérieur, y compris le baccalauréat, peuvent apporter à ces modalités les adaptations nécessaires à leur mise en oeuvre. / S'agissant des épreuves des examens ou concours, ces adaptations peuvent porter, dans le respect du principe d'égalité de traitement des candidats, sur leur nature, leur nombre, leur contenu, leur coefficient ou leurs conditions d'organisation, qui peut notamment s'effectuer de manière dématérialisée. " <br/>
<br/>
              5. Sur le fondement des dispositions citées au point 5, les articles 2 et 3 du décret du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2021 ont prévu que les notes retenues au titre des épreuves terminales des enseignements de spécialité et des évaluations communes de la classe de terminale sont les moyennes annuelles de la classe de terminale inscrites dans le livret scolaire pour les enseignements concernés, pour les candidats scolarisés dans les établissements publics, dans les établissements privés sous contrat ou dans les établissements scolaires français à l'étranger qui figurent sur la liste prévue à l'article R. 451-2 du code de l'éducation pour le cycle terminal du lycée général et technologique. Par ailleurs, le deuxième alinéa de l'article 1er de ce décret dispose que ses articles 2 à 4 ne sont applicables qu'aux candidats scolarisés en classe de terminale pendant l'année scolaire 2020-2021 dans un établissement public, dans un établissement privé sous contrat d'association avec l'Etat ou dans un établissement scolaire français à l'étranger qui figurent sur la liste prévue à l'article R. 451-2 du code de l'éducation pour le cycle terminal du lycée général et technologique. Sur le fondement de cette même ordonnance, l'article 2 du décret du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2022 pour l'année scolaire 2020-2021 a prévu que les notes attribuées au titre des évaluations communes de la classe de première sont les moyennes annuelles de la classe de première, dans les enseignements concernés, inscrites dans le livret scolaire des candidats, arrondies au dixième de point supérieur.<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              6. La société Cour Progress demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative d'ordonner la suspension de l'exécution des décrets n° 2021-209 du 25 février 2021 et n° 2021-210 du 25 février 2021, en tant qu'ils excluent de leur champ d'application les élèves inscrits dans des établissements d'enseignement privé hors contrat, ainsi que de la note de service du 23 février 2021 publiée au Bulletin officiel de l'éducation nationale, de la jeunesse et des sports n° 8 du 25 février 2021. La société requérante demande également au juge des référés d'enjoindre au Premier ministre de prendre un décret intégrant les élèves des établissements privés hors contrat dans le champ d'application du dispositif prévoyant de retenir les notes obtenues dans le cadre du contrôle continu pour l'évaluation des enseignements communs et de spécialités en classe de terminale pour la session 2021 et en classe de première pour la session 2022 du baccalauréat. <br/>
<br/>
              7. En premier lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. Or, aux termes de l'article L. 442-2 du code de l'éducation : " (...) le contrôle de l'Etat sur les établissements d'enseignement privés qui ne sont pas liés à l'Etat par contrat se limite aux titres exigés des directeurs et des enseignants, à l'obligation scolaire, à l'instruction obligatoire, au respect de l'ordre public, à la prévention sanitaire et sociale et à la protection de l'enfance et de la jeunesse ". Par suite, compte tenu de la liberté ainsi reconnue à ces établissements en matière de programmes d'enseignement et de déroulement de la scolarité du second degré, la faculté de prévoir des modalités distinctes de fixation de la note de contrôle continu du baccalauréat, d'une part, pour les candidats inscrits dans des établissements publics et des établissements privés sous contrat, d'autre part, pour ceux qui sont inscrits dans des établissements privés hors contrat, qui résulte des dispositions de l'article D. 334-4 du code de l'éducation, qui se traduit par le fait que la note de contrôle continu prévue à l'article 1er  de l'arrêté du 16 juillet 2018 est fixée, pour les candidats inscrits dans les établissements privés hors contrat, conformément au II de l'article 9 de cet arrêté, en tenant compte des notes obtenues par ces candidats aux évaluations ponctuelles prévues au I du même article, ne porte pas atteinte au principe d'égalité entre les candidats au baccalauréat.<br/>
<br/>
              8. En deuxième lieu, si le décret n° 2021-209 du 25 février 2009 prévoit que les notes des épreuves terminales des enseignements de spécialités sont les notes moyennes annuelles pour les élèves du centre national d'enseignement à distance (CNED), il résulte des dispositions du dernier alinéa de l'article R. 426-2 du code de l'éducation que les enseignements dispensés dans ce cadre, destinés à des élèves ne pouvant être scolarisés dans un établissement scolaire, respectent les programmes pédagogiques qui s'imposent aux établissements publics ou privés sous contrat, et donnent lieu à l'établissement d'un relevé de notes et d'un livret scolaire dans les mêmes conditions que dans ces établissements. Par suite, les élèves concernés, qui bénéficient de la prise en compte de l'évaluation chiffrée annuelle des résultats au cours du cycle terminal à hauteur de 10 % de la note de contrôle continu en application du II de l'article 9 de l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et technologique, se trouvent dans une situation différente de celle des candidats au baccalauréat scolarisés dans un établissement privé hors contrat, lesquels ne sont pas soumis aux mêmes règles d'évaluation, ainsi qu'il a été dit au point 7. Il s'ensuit que le moyen tiré de ce qu'en réservant à ces derniers un traitement différent de celui applicable aux élèves qui suivent les enseignements délivrés par le CNED, le décret aurait méconnu le principe d'égalité entre les candidats à un même examen n'est pas propre à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              9. En troisième lieu, s'il est soutenu que les élèves de terminale issus des établissements publics ou privés sous contrat bénéficient d'un avantage lors de l'inscription sur la plateforme " Parcoursup ", en ce qu'ils seraient en mesure de se prévaloir des notes de leur livret scolaire et de leurs résultats au baccalauréat à l'issue de la délibération du jury au mois de juillet, alors que les élèves des établissements privés hors contrat ne seront pas en mesure de le faire, il résulte de l'instruction que les inscriptions sur cette plateforme ont pris fin le 8 avril 2021. Par suite, aucun candidat scolarisé dans les établissements publics ou privés sous contrat n'a été en mesure de présenter, lors de son inscription, les notes qui seront prises en compte pour le baccalauréat au titre des évaluations communes et des épreuves de spécialités. En outre, si la société requérante soutient qu'une part importante des candidats issus de l'enseignement privé hors contrat ne pourra, en raison de la situation sanitaire, prendre part aux épreuves qui se dérouleront au mois de juin 2021 et devra subir les épreuves de remplacement au moins septembre, ce qui les placera dans une situation défavorable pour les inscriptions dans l'enseignement supérieur par rapport aux étudiants issus de l'enseignement public ou privé sous contrat qui auront leurs résultats du baccalauréat dès le mois de juillet, aucun élément n'est, en l'état de l'instruction, de nature à confirmer l'existence d'un tel risque. Les moyens tirés de l'existence d'une rupture d'égalité entre les deux catégories de candidats et d'une méconnaissance du principe d'égal accès à l'enseignement supérieur ne peuvent donc être regardés comme de nature à faire naître un doute sérieux sur la légalité des dispositions contestées. <br/>
<br/>
              10. En quatrième et dernier lieu, la reprise de la diffusion de l'épidémie de covid-19 s'est traduite, au cours de la période récente, par une aggravation significative sur l'ensemble du territoire national de la diffusion du virus, qui a conduit les pouvoirs publics à annoncer le 31 mars 2021 la généralisation des mesures jusque-là imposées à un nombre limité de départements et la suspension de l'accueil des élèves dans les établissements d'enseignement secondaire du 5 avril jusqu'au 3 mai 2021. Ces circonstances, invoquées par la société requérante au soutien de son moyen tiré de ce que les décrets du 25 février 2021 et la note de service du 23 février 2021 qu'elle conteste ont méconnu les dispositions de l'ordonnance du 24 décembre 2020 relative à l'organisation des concours et examens pendant la crise sanitaire en réservant le bénéfice de la prise en compte du contrôle continu pour les épreuves sanctionnant les enseignements communs et de spécialités aux seuls élèves scolarisés dans les établissements publics et privés sous contrat, ne permettent toutefois pas, à elles seules, de préjuger de la situation sanitaire qui prévaudra à la date à laquelle se tiendront les épreuves de la session 2021 du baccalauréat. S'il appartient dans tous les cas au ministre de l'éducation nationale, de la jeunesse et des sports de prendre l'ensemble des mesures nécessaires afin de garantir le droit à la santé et la sécurité des élèves, des enseignants et des personnels chargés d'assurer le bon déroulement des épreuves de cet examen, il résulte des indications données par les représentants du ministère de l'éducation nationale lors de l'audience qu'en dépit de la situation sanitaire actuelle, les épreuves prévues pour les élèves scolarisés dans les établissements privés hors contrat pourront, compte tenu notamment du nombre limité d'élèves concernés, évalué à 4 000 pour un nombre total de candidats au baccalauréat qui s'élevait à 745 000 en 2020, se dérouler en présentiel dans des conditions offrant les garanties de sécurité sanitaire requises. Par suite, le moyen tiré de ce que les décrets du 25 février 2021 auraient méconnu la portée des dispositions de l'ordonnance du 24 décembre 2020 en ne réitérant pas, pour les élèves de l'enseignement privé hors contrat qui se présenteront à la session 2021 du baccalauréat, la prise en compte des notes obtenues dans le cadre du contrôle continu, admise à titre exceptionnel en 2020 dans un contexte marqué par la fermeture des établissements scolaires pendant plusieurs mois et l'impossibilité d'organiser à brève échéance les épreuves du baccalauréat, n'est pas, en l'état de l'instruction et à la date à laquelle le juge des référés du Conseil d'Etat statue, de nature à faire naître un doute sérieux quant à la légalité des dispositions contestées.<br/>
<br/>
              11. Par suite, et sans qu'il soit besoin de se prononcer, d'une part, sur la fin de non-recevoir soulevée en défense par le ministère de l'éducation, de la jeunesse et des sports et, d'autre part, sur la condition d'urgence, la requête de la société Cour Progress doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Cours Progress est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Cours Progress ainsi qu'au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
