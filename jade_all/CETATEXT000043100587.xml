<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043100587</ID>
<ANCIEN_ID>JG_L_2021_02_000000434659</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/05/CETATEXT000043100587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/02/2021, 434659</TITRE>
<DATE_DEC>2021-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434659</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434659.20210205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       M. C... B... a demandé au tribunal administratif de Versailles d'annuler la décision du 28 octobre 2014 par laquelle le directeur interrégional des services pénitentiaires de Paris a rejeté le recours administratif préalable qu'il avait formé contre la sanction de vingt jours de cellule disciplinaire et de déclassement prononcée à son encontre le 12 septembre 2014 par la commission de discipline de la maison d'arrêt de Fleury-Mérogis et de condamner l'Etat à lui verser la somme de 20 000 euros en réparation de son préjudice. Par un jugement n° 1408998 du 9 février 2018, le tribunal administratif a fait droit à sa demande d'annulation et rejeté ses conclusions indemnitaires. <br/>
<br/>
       Par un arrêt n° 18VE01188 du 9 juillet 2019, la cour administrative d'appel de Versailles a annulé les articles 1er et 2 de ce jugement et rejeté la demande présentée par M. B... devant le tribunal administratif de Versailles. <br/>
<br/>
       1° Par un pourvoi sommaire, enregistré le 12 septembre 2019 au secrétariat du contentieux du Conseil d'Etat sous le n° 434659, M. B... demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
       2° Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 7 novembre 2019 et les 20 janvier et 15 septembre 2020 au secrétariat du contentieux du Conseil d'Etat sous le n° 435829, M. B... demande au Conseil d'Etat :<br/>
<br/>
       1°) d'annuler l'arrêt du 9 juillet 2019 ;<br/>
<br/>
       2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Delvolvé-Trichet, son avocat, en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
                 Vu les autres pièces du dossier ;<br/>
<br/>
             Vu : <br/>
         - le code de procédure pénale ;<br/>
         - la loi n° 91-647 du 10 juillet 1991 ;<br/>
         - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
         Après avoir entendu en séance publique :<br/>
<br/>
         - le rapport de Mme A... D..., maître des requêtes en service extraordinaire,  <br/>
<br/>
         - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
         La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M. C... B... ;<br/>
<br/>
         Vu les notes en délibéré, enregistrées le 4 février 2020, présentées par le garde des sceaux, ministre de la justice ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les mémoire et pièces enregistrés sous le n° 435829 constituent, en réalité, après octroi de l'aide juridictionnelle totale, des productions ayant régularisé le pourvoi enregistré sous le n° 434659. Il y a donc lieu de radier le pourvoi n° 435829 des registres du secrétariat du contentieux du Conseil d'Etat et d'enregistrer les productions mentionnées sous le n° 434659. <br/>
<br/>
              2. Il ressort des énonciations de l'arrêt attaqué que la commission de discipline de la maison d'arrêt de Fleury-Mérogis a infligé, le 12 septembre 2014, à M. B..., incarcéré dans cet établissement, une sanction de vingt jours de cellule disciplinaire, assortie d'un déclassement de son emploi. Par une décision du 28 octobre 2014, qui s'est substituée à celle du 12 septembre 2014, le directeur interrégional des services pénitentiaires de Paris a rejeté le recours administratif préalable formé par l'intéressé contre cette sanction. Par un jugement du 9 février 2018, le tribunal administratif de Versailles a annulé la décision du 28 octobre 2014 du directeur interrégional des services pénitentiaires de Paris. M. B... se pourvoit en cassation contre l'arrêt du 9 juillet 2019 par lequel la Cour administrative d'appel de Versailles a, sur appel du garde des sceaux, ministre de la justice, d'une part, annulé les articles 1er et 2 du jugement du 9 février 2018 et, d'autre part, rejeté la demande de M. B... présentée devant le tribunal administratif de Versailles.<br/>
<br/>
              3. En vertu de l'article 726 du code de procédure pénale, la commission disciplinaire appelée à connaître des fautes commises par les personnes détenues placées en détention provisoire ou exécutant une peine privative de liberté doit comprendre au moins un membre extérieur à l'administration pénitentiaire. Aux termes de l'article R. 57-7-6 du même code : " La commission de discipline comprend, outre le chef d'établissement ou son délégataire, président, deux membres assesseurs ". Aux termes de l'article R. 57-7-7 du même code : " Les sanctions disciplinaires sont prononcées, en commission, par le président de la commission de discipline. Les membres assesseurs ont voix consultative ". L'article R. 57-7-8 du même code dispose que : " Le président de la commission de discipline désigne les membres assesseurs./ Le premier assesseur est choisi parmi les membres du premier ou du deuxième grade du corps d'encadrement et d'application du personnel de surveillance de l'établissement./ Le second assesseur est choisi parmi des personnes extérieures à l'administration pénitentiaire qui manifestent un intérêt pour les questions relatives au fonctionnement des établissements pénitentiaires, habilitées à cette fin par le président du tribunal de grande instance territorialement compétent. La liste de ces personnes est tenue au greffe du tribunal de grande instance ". Enfin, aux termes de l'article R. 57-7-12 du même code : " Il est dressé par le chef d'établissement un tableau de roulement désignant pour une période déterminée les assesseurs extérieurs appelés à siéger à la commission de discipline ". <br/>
<br/>
              4. Il résulte de ces dispositions que la présence dans la commission de discipline d'un assesseur choisi parmi des personnes extérieures à l'administration pénitentiaire, alors même qu'il ne dispose que d'une voix consultative, constitue une garantie reconnue au détenu, dont la privation est de nature à vicier la procédure, alors même que la décision du directeur interrégional des services pénitentiaires, prise sur le recours administratif préalable obligatoire exercé par le détenu, se substitue à celle du président de la commission de discipline. <br/>
<br/>
              5. Il appartient à l'administration pénitentiaire de mettre en oeuvre tous les moyens à sa disposition pour s'assurer de la présence effective de cet assesseur, en vérifiant notamment en temps utile la disponibilité effective des personnes figurant sur le tableau de roulement prévu à l'article R. 57-7-12. Si, malgré ses diligences, aucun assesseur extérieur n'est en mesure de siéger, la tenue de la commission de discipline doit être reportée à une date ultérieure, à moins qu'un tel report compromette manifestement le bon exercice du pouvoir disciplinaire.<br/>
<br/>
              6. En jugeant que l'absence d'un assesseur extérieur lors de la réunion de la commission de discipline du 12 septembre 2014 n'avait pas vicié la procédure, au seul motif que les onze assesseurs extérieurs habilités inscrits sur le tableau de roulement établi pour la période en cause avaient été régulièrement convoqués par courrier électronique et qu'aucun d'entre eux n'avait informé l'établissement de son indisponibilité, sans rechercher si l'administration avait mis en oeuvre tous les moyens à sa disposition pour s'assurer de la présence effective de l'assesseur extérieur dans la commission de discipline et s'il existait un obstacle au report de la réunion, la cour administrative d'appel de Versailles a entaché son arrêt d'erreur de droit.   <br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'arrêt attaqué doit être annulé. <br/>
<br/>
              8. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Delvolvé, Trichet, avocat de M. B... renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la SCP Delvolvé, Trichet.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : Le pourvoi enregistré sous le n° 435829 est radié des registres du secrétariat du contentieux du Conseil d'Etat.<br/>
Article 2 : L'arrêt du 9 juillet 2019 de la cour administrative de Versailles est annulé.<br/>
Article 3 : L'affaire est renvoyée devant la cour administrative d'appel de Versailles. <br/>
Article 4 : L'Etat versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
Article 5 : La présente décision sera notifiée à M. C... B... et au garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. QUESTIONS GÉNÉRALES. - FORMALITÉ IMPOSSIBLE - DISCIPLINE DES DÉTENUS - COMMISSION DE DISCIPLINE - COMPOSITION - ASSESSEUR CHOISI PARMI DES PERSONNES EXTÉRIEURES (ART. 726 DU CPP) - CONDITIONS - 1) SATISFACTION À UNE OBLIGATION DE MOYENS [RJ3] - 2) IMPOSSIBILITÉ DE REPORTER LA TENUE DE LA COMMISSION DE DISCIPLINE SANS COMPROMETTRE MANIFESTEMENT LE BON EXERCICE DU POUVOIR DISCIPLINAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DISCIPLINE DES DÉTENUS - COMMISSION DE DISCIPLINE - COMPOSITION - ASSESSEUR CHOISI PARMI DES PERSONNES EXTÉRIEURES (ART. 726 DU CPP) - 1) CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE - 2) MOYEN TIRÉ DE LA PRIVATION DE CETTE GARANTIE - OPÉRANCE, ALORS MÊME QUE LA DÉCISION DU DISP, PRISE SUR RAPO, SE SUBSTITUE À CELLE DU PRÉSIDENT DE LA COMMISSION DE DISCIPLINE [RJ2] - 3) FORMALITÉ IMPOSSIBLE - CONDITIONS - A) SATISFACTION À UNE OBLIGATION DE MOYENS [RJ3] - B) IMPOSSIBILITÉ DE REPORTER LA TENUE DE LA COMMISSION DE DISCIPLINE SANS COMPROMETTRE MANIFESTEMENT LE BON EXERCICE DU POUVOIR DISCIPLINAIRE.
</SCT>
<ANA ID="9A"> 01-03-02-01 Il résulte des articles 726, R. 57-7-6, R. 57-7-7, R. 57-7-8 et R. 57-7-12 du code de procédure pénale (CPP) que la présence dans la commission de discipline d'un assesseur choisi parmi des personnes extérieures à l'administration pénitentiaire, alors même qu'il ne dispose que d'une voix consultative, constitue une garantie reconnue au détenu, dont la privation est de nature à vicier la procédure.,,,1) Il appartient à l'administration pénitentiaire de mettre en oeuvre tous les moyens à sa disposition pour s'assurer de la présence effective de cet assesseur, en vérifiant notamment en temps utile la disponibilité effective des personnes figurant sur le tableau de roulement prévu à l'article R. 57-7-12.... ,,2) Si, malgré ses diligences, aucun assesseur extérieur n'est en mesure de siéger, la tenue de la commission de discipline doit être reportée à une date ultérieure, à moins qu'un tel report compromette manifestement le bon exercice du pouvoir disciplinaire.</ANA>
<ANA ID="9B"> 37-05-02-01 1) Il résulte des articles 726, R. 57-7-6, R. 57-7-7, R. 57-7-8 et R. 57-7-12 du code de procédure pénale (CPP) que la présence dans la commission de discipline d'un assesseur choisi parmi des personnes extérieures à l'administration pénitentiaire, alors même qu'il ne dispose que d'une voix consultative, constitue une garantie reconnue au détenu, dont la privation est de nature à vicier la procédure.,,,2) Il en va ainsi alors même que la décision du directeur interrégional des services pénitentiaires (DISP), prise sur le recours administratif préalable obligatoire (RAPO) exercé par le détenu, se substitue à celle du président de la commission de discipline.... ,,3) a) Il appartient à l'administration pénitentiaire de mettre en oeuvre tous les moyens à sa disposition pour s'assurer de la présence effective de cet assesseur, en vérifiant notamment en temps utile la disponibilité effective des personnes figurant sur le tableau de roulement prévu à l'article R. 57-7-12.... ,,b) Si, malgré ses diligences, aucun assesseur extérieur n'est en mesure de siéger, la tenue de la commission de discipline doit être reportée à une date ultérieure, à moins qu'un tel report compromette manifestement le bon exercice du pouvoir disciplinaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, M.,et autres, n° 335033, p. 649.,,[RJ2] Rappr., s'agissant de l'opérance, à l'encontre des décisions prises sur RAPO, de certains moyens tirés des vices de procédure qui affectent la décision initiale, CE, Section, 18 novembre 2005,,, n° 270075, p. 514.,,[RJ3] Cf. CE, Section, 1er juillet 1967, Société d'exploitation de la clinique Rech et autres, n° 61750, p. 429.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
