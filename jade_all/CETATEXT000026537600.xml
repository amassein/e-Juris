<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026537600</ID>
<ANCIEN_ID>JG_L_2012_10_000000336737</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/76/CETATEXT000026537600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/10/2012, 336737</TITRE>
<DATE_DEC>2012-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336737</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Chrystelle Naudan-Carastro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336737.20121026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 17 février 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale (USGERES), dont le siège est 4, place Félix Eboué à Paris cedex 12 (75583), représentée par son président ; l'USGERES demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande tendant, d'une part, à la modification des articles R. 2272-3, R. 1431-6 et R. 4642-4 du code du travail et, d'autre part, à cette fin, des décrets n° 97-80 du 30 janvier 1997, n° 84-360 du 10 mai 1984, n° 84-873 du 28 septembre 1984 et n° 2008-244 du 7 mars 2008 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de statuer à nouveau sur sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le décret n° 84-360 du 10 mai 1984 ;<br/>
<br/>
              Vu le décret n° 84-873 du 28 septembre 1984 ; <br/>
<br/>
              Vu le décret n° 97-80 du 30 janvier 1997 ;<br/>
<br/>
              Vu le décret n° 2008-244 du 7 mars 2008 ;<br/>
<br/>
              Vu le code du travail ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Chrystelle Naudan-Carastro, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions dirigées contre les décrets des 10 mai 1984, 28 septembre 1984 et 30 janvier 1997 :<br/>
<br/>
              1. Considérant que l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale demande l'annulation de la décision par laquelle le Premier ministre a rejeté sa demande tendant à la modification des décrets des 10 mai 1984, 28 septembre 1984 et 30 janvier 1997 en tant qu'ils ont introduit au code du travail les articles R. 136-3, R. 136-9 et R. 136-10 qui fixaient la liste des organisations d'employeurs autorisées à proposer des représentants au sein, respectivement, de la Commission nationale de la négociation collective, du Conseil supérieur de la prud'homie et du conseil d'administration de l'Agence nationale de l'amélioration des conditions de travail ; qu'à la date du refus opposé par le Premier ministre, ces dispositions avaient été abrogées par le décret du 7 mars 2008 relatif au code du travail ; que, par suite, l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale n'est pas fondée à soutenir que ce refus d'abrogation serait entaché d'illégalité ; <br/>
<br/>
              Sur les conclusions relatives au décret du 7 mars 2008 et aux articles R. 1431-6, R. 2272-3 et R. 4642-4 du code du travail :<br/>
<br/>
              2. Considérant que l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale doit être regardée comme demandant l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à la modification des articles R. 1431-6, R. 2272-3 et R. 4642-4 du code du travail, introduits dans ce code par le décret du 7 mars 2008 ;<br/>
<br/>
              3. Considérant que l'article L. 1431-1 du code du travail dispose que : " Le Conseil supérieur de la prud'homie, organisme consultatif, siège auprès du garde des sceaux, ministre de la justice, et du ministre chargé du travail. / En font partie, outre les représentants des ministères intéressés, des représentants, en nombre égal, des organisations syndicales et des organisations professionnelles représentatives au plan national. " ; que l'article L. 2272-1 du même code dispose que : " La Commission nationale de la négociation collective comprend des représentants de l'Etat, du Conseil d'Etat, ainsi que des représentants des organisations d'employeurs représentatives au niveau national et des organisations syndicales de salariés représentatives au niveau national. " ; qu'enfin, l'article L. 4642-2 du même code, dans sa rédaction applicable à la date de la décision litigieuse, dispose que : " L'Agence nationale pour l'amélioration des conditions de travail est administrée par un conseil d'administration qui comprend en nombre égal : 1° Des représentants des organisations d'employeurs représentatives au niveau national ; / 2° Des représentants des organisations syndicales de salariés représentatives au niveau national ; / 3° Des représentants des ministres intéressés et de personnes qualifiées. " ;<br/>
<br/>
              4. Considérant que pour l'application des dispositions citées ci-dessus, les articles R. 1431-6, R. 2272-3, et R. 4642-4 du code du travail ont fixé pour, respectivement, le Conseil supérieur de la prud'homie, la Commission nationale de la négociation collective et le conseil d'administration de l'Agence nationale pour l'amélioration des conditions de travail, la liste des organisations d'employeurs habilitées à présenter des représentants ; que l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale soutient que ces articles violent les dispositions législatives sur le fondement desquelles ils ont été pris en raison de ce qu'aucune organisation d'employeurs se disant représentative des entreprises de " l'économie solidaire ", et notamment elle-même, n'est habilitée à présenter des représentants ; <br/>
<br/>
              5. Considérant qu'il résulte des termes des articles L. 1431-1, L. 2272-1 et L. 4642-2 du code du travail cités ci-dessus que le pouvoir exécutif est tenu d'assurer la représentation, au sein des organismes mentionnés par ces articles, de toutes les organisations d'employeurs représentatives au niveau national et interprofessionnel, au sens des dispositions de l'article L. 2122-9 du code du travail ; qu'il ressort des pièces du dossier que ni l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale ni aucune autre organisation d'employeurs se disant représentative des entreprises de " l'économie solidaire " ne peut faire état d'une représentativité interprofessionnelle au niveau national ; <br/>
<br/>
              6. Considérant, toutefois, qu'il est loisible au pouvoir exécutif, pour l'application des mêmes dispositions des articles L. 1431-1, L. 2272-1 et L. 4642-2 du code du travail, de prévoir également la représentation, au sein des organismes mentionnés par ces articles, d'organisations d'employeurs qui ne sont représentatives au niveau national que pour certaines professions ou certains secteurs d'activité précisément déterminés, dès lors que cette présence peut se justifier par les spécificités de ces professions ou de ces secteurs d'activité au regard des compétences propres aux organismes en question ; <br/>
<br/>
              7. Considérant qu'il ne ressort pas des pièces du dossier que le secteur d'activité de " l'économie solidaire " présenterait des spécificités au regard des compétences propres aux organismes en cause de nature à justifier sa représentation en leur sein ; que l'union requérante n'est, par suite, pas fondée à soutenir que c'est à tort que, par la décision attaquée, le Premier ministre a refusé de faire droit à sa demande tendant à ce que soient modifiés les articles R. 1431-6, R. 2272-3 et R. 4642-4 du code du travail ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les conclusions à fin d'injonction ainsi que celles que la requérante a présentées  sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'Union de syndicats et groupements d'employeurs représentatifs dans l'économie sociale, au Premier ministre et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-05-01 TRAVAIL ET EMPLOI. SYNDICATS. REPRÉSENTATIVITÉ. - 1) OBLIGATION D'ASSURER LA REPRÉSENTATION DE TOUTES LES ORGANISATIONS D'EMPLOYEURS REPRÉSENTATIVES AU NIVEAU NATIONAL ET INTERPROFESSIONNEL - EXISTENCE [RJ1] - 2) FACULTÉ DE PRÉVOIR ÉGALEMENT LA REPRÉSENTATION D'ORGANISATIONS D'EMPLOYEURS QUI NE SONT REPRÉSENTATIVES AU NIVEAU NATIONAL QUE POUR CERTAINES PROFESSIONS OU CERTAINS SECTEURS D'ACTIVITÉ - EXISTENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 66-05-01 1) Il résulte des termes des articles L. 1431-1, L. 2272-1 et L. 4642-2 du code du travail que le pouvoir exécutif est tenu d'assurer la représentation, au sein des organismes mentionnés par ces articles, de toutes les organisations d'employeurs représentatives au niveau national et interprofessionnel, au sens des dispositions de l'article L. 2122-9 du code du travail.,,2) Toutefois, il est loisible au pouvoir exécutif, pour l'application des mêmes dispositions des articles L. 1431-1, L. 2272-1 et L. 4642-2 du même code, de prévoir également la représentation, au sein des organismes mentionnés par ces articles, d'organisations d'employeurs qui ne sont représentatives au niveau national que pour certaines professions ou certains secteurs d'activité précisément déterminés, dès lors que cette présence peut se justifier par les spécificités de ces professions ou de ces secteurs d'activité au regard des compétences propres aux organismes en question.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 5 novembre 2004, Union nationale des syndicats autonomes (UNSA), n° 257878, p. 418.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
