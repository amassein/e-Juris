<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037942867</ID>
<ANCIEN_ID>JG_L_2018_12_000000418535</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/94/28/CETATEXT000037942867.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 31/12/2018, 418535, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418535</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418535.20181231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 23 février, 20 avril et 28 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 10 juillet 2017 rapportant le décret du 19 février 2015 en ce qu'il lui avait accordé la nationalité française et autorisé à franciser son prénom ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 700 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que M.B..., ressortissant marocain, a déposé une demande d'acquisition de la nationalité française le 10 décembre 2013 par laquelle il a indiqué être divorcé, ne pas avoir d'enfants et avoir sa résidence en France ; qu'il s'est engagé sur l'honneur à signaler tout changement dans sa situation personnelle et familiale ; qu'au vu de ses déclarations, il a été naturalisé par décret du 19 février 2015 et autorisé à ajouter à son prénom celui de Julien ; que, toutefois, par un décret du 10 juillet 2017, le Premier ministre a rapporté le décret du 19 février 2015 au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation personnelle et familiale ;<br/>
<br/>
              3.	Considérant, en premier lieu, qu'il ressort des mentions de l'ampliation du décret du 10 juillet 2017 certifiée conforme par le secrétaire général du Gouvernement, que le décret a été signé par le Premier ministre et contresigné par le ministre d'Etat, ministre de l'intérieur ; que l'ampliation notifiée à M. B...n'avait pas à être revêtue de ces signatures ;<br/>
<br/>
              4.	Considérant, en deuxième lieu, que le délai de deux ans imparti pour rapporter le décret de naturalisation de M. B...a commencé à courir à la date à laquelle la réalité de la situation familiale et personnelle de l'intéressé a été portée à la connaissance du ministre chargé des naturalisations ; qu'il ressort des pièces du dossier que si M. B... fait valoir qu'il a, postérieurement au décret du 19 février 2015 lui ayant accordé la nationalité française, déposé le 5 mai 2015 auprès du consulat général de France à Québec une demande de transcription de l'acte de naissance à Québec de son fils et qu'il a informé de cette naissance le service central d'état-civil dépendant du ministère des affaires étrangères par courrier électronique du 7 juillet 2015, ce n'est que le 10 juillet 2015 qu'il a adressé un courrier électronique aux services du ministre chargé des naturalisations indiquant la naissance de son fils au Canada ; que c'est à compter de cette dernière date que le ministre chargé des naturalisations a eu connaissance des faits qui ont motivé le décret attaqué ; que ce décret, signé le 10 juillet 2017, a ainsi été pris dans le délai de deux ans imparti par les dispositions de l'article 27-2 du code civil ; <br/>
<br/>
              5.	Considérant, en troisième lieu, qu'aux termes de l'article 21-16 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation " ; qu'il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts ; que, pour apprécier si cette condition se trouve remplie, l'autorité administrative peut notamment prendre en compte la situation personnelle et familiale en France de l'intéressé ; que, par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressé disposait d'un titre de séjour au Canada où était né et résidait son fils était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts ;<br/>
<br/>
              6.	Considérant qu'il ressort des pièces du dossier que M. B...a indiqué, dans sa demande de naturalisation, qu'il était divorcé et sans enfant et qu'il résidait à Pessac ; qu'il s'est rendu à la préfecture de la Gironde le 6 janvier 2015 pour y signaler l'acquisition d'un appartement à Cenon, signalant ainsi un changement d'adresse ; que s'il soutient, en outre, avoir pris contact par téléphone avec la préfecture de Gironde ce même 6 janvier 2015 pour indiquer la naissance à venir de son enfant et s'être présenté en personne à la préfecture de Gironde après la naissance de cet enfant, le 21 janvier 2015 à Québec, aucune pièce versée au dossier ne permet de corroborer ses allégations ; qu'il n'a, de plus, pas informé l'administration en charge de sa demande de naturalisation de sa décision de bénéficier du certificat de sélection du Québec qu'il avait sollicité et obtenu en 2011 pour une durée de trois ans, ce qui a conduit à la délivrance, le 29 août 2014, d'une carte de résident permanent au Canada ; que ces circonstances ont constitué des changements de sa situation personnelle et familiale que l'intéressé, ainsi qu'il s'y était engagé, aurait dû porter à la connaissance des services instruisant la demande de naturalisation, ce qu'il n'a pas fait ; que, dans ces conditions, il doit être regardé comme ayant sciemment dissimulé le changement de sa situation personnelle et familiale ; que, par suite, en rapportant le décret lui ayant accordé la nationalité française à raison de cette fraude, le Premier ministre n'a pas fait une inexacte application de l'article 27-2 du code civil ;<br/>
<br/>
              7.	Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; que ses conclusions, présentées au titre de l'article L. 761-1 du code de justice administrative, ne peuvent, en conséquence, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
