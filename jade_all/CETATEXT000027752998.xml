<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752998</ID>
<ANCIEN_ID>JG_L_2013_07_000000364327</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752998.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 25/07/2013, 364327, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364327</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364327.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 6 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant..., agissant en exécution d'un jugement du tribunal des affaires de sécurité sociale de Tours en date du 23 juillet 2012 ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer l'article R. 173-4-3 du code de la sécurité sociale illégal et de dire que ses dispositions doivent s'appliquer à l'ensemble des assurés ayant acquis des droits dans des régimes professionnels différents ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2003-775 du 21 août 2003, ensemble la décision du Conseil constitutionnel n° 2003-483 DC du 14 août 2003 ;<br/>
<br/>
              Vu la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
<br/>
              Vu le décret n° 2006-83 du 27 janvier 2006 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M.B..., agissant en exécution d'un jugement du tribunal des affaires de sécurité sociale de Tours du 23 juillet 2012, demande au Conseil d'Etat d'apprécier la légalité, au regard de l'article L. 161-17 A du code de la sécurité sociale, de l'article R. 173-4-3 du même code ; que cet article prévoit, dans sa rédaction issue du décret du 27 janvier 2006, que lorsqu'un assuré a acquis, dans deux ou plusieurs des régimes d'assurance vieillesse du régime général, du régime social des indépendants et du régime des salariés des professions agricoles, des droits à pension dont le montant est fixé sur la base d'un salaire ou revenu annuel moyen soumis à cotisations, le nombre d'années retenu pour calculer ce salaire ou revenu est déterminé, pour les pensions prenant effet postérieurement au 31 décembre 2003, en multipliant le nombre d'années fixé dans le régime considéré par le rapport entre la durée d'assurance accomplie au sein de ce régime et le total des durées d'assurance accomplies dans les trois régimes considérés ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 161-17 A du code de la sécurité sociale : " La Nation réaffirme solennellement le choix de la retraite par répartition au coeur du pacte social qui unit les générations. / Tout retraité a droit à une pension en rapport avec les revenus qu'il a tirés de son activité. / Les assurés doivent pouvoir bénéficier d'un traitement équitable au regard de la retraite, quels que soient leur sexe, leurs activités professionnelles passées et le ou les régimes dont ils relèvent (...) " ; que ces dispositions, insérées dans le code de la sécurité sociale par l'article 1er de la loi du 9 novembre 2010 portant réforme des retraites, reprennent celles des articles 1er à 3 de la loi du 21 août 2003, en ajoutant l'objectif d'un traitement équitable indépendamment du sexe ; qu'appelé à se prononcer sur la conformité à la Constitution de l'article 3 de la loi du 21 août 2003, le Conseil constitutionnel a jugé, par sa décision susvisée du 14 août 2003, que les dispositions de cet article, qui se bornaient à exposer le motif d'équité inspirant plusieurs des dispositions particulières figurant dans le texte de loi, étaient dépourvues de valeur normative ; qu'il en va de même des dispositions du troisième alinéa de l'article L. 161-17 A du code de la sécurité sociale ; que, par suite, M. B...ne peut utilement soutenir que l'article R. 173-4-3 du même code méconnaîtrait le principe de traitement équitable au regard de la retraite énoncé à l'article L. 161-17 A, en ce qu'il réserve aux seuls assurés ayant acquis des droits au régime général, au régime des salariés agricoles ou au régime social des indépendants la règle de proratisation du salaire annuel moyen qu'il prévoit ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que l'article R. 173-4-3 du code de sécurité sociale est entaché d'illégalité ; que, par suite, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la caisse d'assurance retraite et de santé au travail du Centre.<br/>
Copie en sera adressée pour information au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
