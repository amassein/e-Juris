<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806223</ID>
<ANCIEN_ID>JG_L_2021_12_000000449172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806223.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 449172, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449172.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... G... a demandé au tribunal administratif de Grenoble d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires dans la commune d'Annemasse (Haute-Savoie). Par un jugement n° 2001860 du 31 décembre 2020, le tribunal administratif a annulé ces opérations électorales.<br/>
<br/>
              Par une requête enregistrée le 28 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. H... F... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de M. G....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-760 du 22 juin 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boutet-Hourdeaux, avocat de M. H... F... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires dans la commune d'Annemasse (Haute-Savoie), la liste " Annemasse ville d'avenirs ", conduite par M. H... F..., maire sortant, a recueilli 2 087 voix, soit 50,02% des suffrages exprimés, tandis que la liste " Générations Annemasse ", conduite par M. C... G..., a obtenu 1 682 voix soit 40,31% des suffrages exprimés, et que la liste " Renouveau Annemasse ", conduite par M. A... E... - Dos Ramos, a obtenu 403 voix, soit 9,65% des suffrages exprimés. M. F... relève appel du jugement du 31 décembre 2020 par lequel le tribunal administratif de Grenoble, faisant droit à la protestation de M. G..., a annulé ces opérations électorales.<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              3. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              4. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ".<br/>
<br/>
              5. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal et au conseil communautaire à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              6. En l'espèce, M. G... faisait seulement valoir à l'appui de sa protestation que le taux d'abstention s'était élevé à 72,21 % dans la commune, sans invoquer aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée, bien que sensiblement supérieur à la moyenne nationale, ne peut être regardé comme ayant altéré la sincérité du scrutin. Par suite, M. F... est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble s'est fondé sur le niveau élevé de l'abstention et sur la circonstance que la liste conduite par M. F... avait obtenu la majorité absolue des suffrages plus une voix seulement pour annuler les opérations électorales qui se sont déroulées dans la commune d'Annemasse.<br/>
<br/>
              7. Toutefois, il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. G... devant le tribunal administratif.<br/>
<br/>
              Sur la campagne électorale :<br/>
<br/>
              8. Aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ".<br/>
<br/>
              9. Il résulte de l'instruction qu'une " déclaration commune des élus du bureau communautaire " de la communauté d'agglomération Annemasse Agglo, destinée à la presse, a été publiée sur le site internet de la commune d'Annemasse le 2 mars 2020.  Si cette publication procède à un bilan avantageux des réalisations de la communauté d'agglomération depuis sa création en 2008 et appelle de ses vœux la poursuite de l' "esprit constructif " ayant régné au sein de la communauté " avec chacun des présidents et maires successifs ", elle ne contient aucun élément de polémique électorale et ne peut être regardée comme constitutive d'une campagne de promotion publicitaire au sens des dispositions précitées, alors même que les dépenses afférentes à l'édition et à la diffusion de ce document ont été prises en charge par la communauté d'agglomération et pour partie intégrées au compte de campagne de M. F... par la Commission nationale des comptes de campagne et des financements politiques dans sa décision du 5 octobre 2020. Par suite, le grief tiré de la méconnaissance des dispositions de l'article L. 52-1 du code électoral doit être écarté. <br/>
<br/>
              Sur la validité des bulletins :<br/>
<br/>
              10. En premier lieu, aux termes de l'article R. 66-2 du code électoral : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : (...) 6° Les circulaires utilisées comme bulletin ; (...) ". Dès lors, M. G... n'est pas fondé à soutenir que c'est à tort que les suffrages exprimés en faveur de sa liste par des électeurs ayant fait usage non d'un bulletin de vote, mais de la profession de foi de sa liste, ont été regardés comme nuls.<br/>
<br/>
              11. En second lieu, les griefs tirés, d'une part, de ce que le nombre d'émargements aurait été différent du nombre de votants dans le bureau de vote n° 1 sans qu'il résulte de l'instruction que le procès-verbal de ce bureau de vote comporte la mention d'une telle différence, et, d'autre part, de ce qu'un bulletin présentant une légère trace de vernis aurait été déclaré nul au motif qu'il s'agissait d'un signe de reconnaissance, ne sont pas assortis de précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              12. Il résulte de ce qui précède que M. G... n'est pas fondé à demander l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune d'Annemasse. Il n'y a pas lieu, dans les circonstances d'espèce, de faire droit aux conclusions présentées par M. F... devant le tribunal administratif au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 31 décembre 2020 du tribunal administratif de Grenoble est annulé.<br/>
Article 2 : La protestation de M. G... est rejetée.<br/>
Article 3 : Les conclusions de M. F... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. H... F..., à M. C... G..., à M. A... E... I... et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et M. Edouard Solier, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		Le rapporteur : <br/>
      Signé : M. Edouard Solier<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
