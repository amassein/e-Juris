<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038462136</ID>
<ANCIEN_ID>JG_L_2019_05_000000418085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/21/CETATEXT000038462136.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 10/05/2019, 418085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418085.20190510</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 12 février, 14 mai, 2 novembre 2018 et 12 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des producteurs et élaborateurs de Crémant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté interministériel du 8 décembre 2017 relatif à l'indication géographique protégée " Coteaux de l'Ain " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles ;<br/>
              - le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Fédération nationale des producteurs et élaborateurs de Crémant et à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par arrêté du 8 décembre 2017, les ministres de l'économie et des finances, de l'agriculture et de l'alimentation et de l'action et des comptes publics ont homologué le cahier des charges de l'indication géographique protégée " Coteaux de l'Ain " proposé par l'Institut national de l'origine et de la qualité (INAO). Si la Fédération nationale des producteurs et élaborateurs de Crémant demande au Conseil d'Etat l'annulation pour excès de pouvoir de cet arrêté, il ressort des termes de sa requête et notamment des moyens développés qu'elle entend attaquer cet arrêté en tant seulement qu'il étend aux vins mousseux de qualité rouges, rosés et blancs la possibilité de se prévaloir de l'indication géographique protégée " Coteaux de l'Ain ", auparavant réservée aux seuls vins tranquilles.<br/>
<br/>
              Sur les interventions de la Confédération des vins IGP de France et du syndicat des vins des coteaux alpins :<br/>
<br/>
              2. La Confédération des vins IGP de France et le syndicat des vins des coteaux alpins ont intérêt au maintien de l'arrêté attaqué. Leurs interventions sont donc recevables.<br/>
<br/>
              Sur la légalité de l'arrêt attaqué :<br/>
<br/>
              3. Conformément aux dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, les trois signataires de l'arrêté attaqué, régulièrement nommés par des textes publiés au Journal officiel de la République française, avaient de ce fait qualité pour signer, au nom de leur ministre respectif, l'arrêté litigieux. Le moyen tiré de l'incompétence des signataires de l'acte attaqué doit donc être écarté.<br/>
<br/>
              4. Pour le secteur vitivinicole, le b) du paragraphe 1 de l'article 93 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles, qui reprend sur ce point les dispositions du b) du paragraphe 1 de l'article 118 ter du règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007, définit l'indication géographique protégée comme : " une indication renvoyant à une région, à un lieu déterminé ou, dans des cas exceptionnels, à un pays, qui sert à désigner un produit (...) : / i) possédant une qualité, une réputation ou d'autres caractéristiques particulières attribuables à cette origine géographique (...) ". Selon le 2 de l'article 94 du même règlement : " Le cahier des charges permet aux parties intéressées de vérifier le respect des conditions de production associées à l'appellation d'origine ou à l'indication géographique. / Il comporte au minimum les éléments suivants : / (...) g) les éléments qui corroborent le lien visé (...) à l'article 93, paragraphe 1, point b) i) ". Enfin, l'article 7 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole dispose que : " 1. Les éléments qui corroborent le lien géographique (...) expliquent dans quelle mesure les caractéristiques de la zone géographique délimitée influent sur le produit final (...) 3. Pour une indication géographique, le cahier des charges contient : a) des informations détaillées sur la zone géographique contribuant au lien ; / b) des informations détaillées sur la qualité, la réputation ou d'autres caractéristiques spécifiques du produit découlant de son origine géographique ; / c) une description de l'interaction causale entre les éléments visés au point a) et ceux visés au point b). / 4. Pour une indication géographique, le cahier des charges précise si l'indication se fonde sur une qualité ou une réputation spécifique ou sur d'autres caractéristiques liées à l'origine géographique ".<br/>
<br/>
              5. Il résulte clairement de ces dispositions que l'homologation d'un cahier des charges d'une indication géographique protégée, qui n'est pas une simple indication de provenance géographique, ne peut légalement intervenir que si ce cahier précise les éléments qui permettent d'attribuer à une origine géographique déterminée une qualité, une réputation ou d'autres caractéristiques particulières du produit qui fait l'objet de l'indication et met en lumière de manière circonstanciée le lien géographique et l'interaction causale entre la zone géographique et la qualité, la réputation ou d'autres caractéristiques du produit. Il découle en outre nécessairement de ces mêmes dispositions qu'elles ne permettent de reconnaître un lien avec une origine géographique que pour une production existante, attestée dans la zone géographique à la date de l'homologation et depuis un temps suffisant pour établir ce lien. Enfin, celui-ci doit être établi pour un produit déterminé et ne peut donc procéder d'une analogie avec un autre produit, même voisin.<br/>
<br/>
              6. En premier lieu, la décision du Conseil d'Etat, statuant au contentieux du 14 décembre 2016 a prononcé l'annulation de l'arrêté interministériel du 26 novembre 2015 relatif à l'indication géographique protégée " Coteaux de l'Ain " en tant qu'il homologue celles des dispositions du cahier des charges de cette indication géographique protégée relatives aux vins mousseux de qualité rosés et blancs au motif que la preuve de l'existence d'une interaction causale entre la zone géographique et la qualité, la réputation ou d'autres caractéristiques spécifiques des vins mousseux ne ressortait pas du cahier des charges. Le nouveau cahier des charges étant différent sur ce point, l'arrêté attaqué du 8 décembre 2017 n'a pu, en l'homologuant, méconnaître l'autorité de la chose jugée par cette décision du 14 décembre 2016.<br/>
<br/>
              7. En deuxième lieu, il ressort des éléments figurant au cahier des charges, tels qu'étayés par les pièces du dossier, que l'antériorité de la production de vins mousseux de qualité, rouges, rosés et blancs, dans la zone géographique de l'indication géographique protégée " Coteaux de l'Ain ", peut être regardée comme établie, de manière continue, depuis au moins vingt ans.<br/>
<br/>
              8. En troisième lieu, il ressort du cahier des charges de l'indication géographique protégée " Coteaux de l'Ain " qu'il comporte, notamment à ses points 7.2 et 7.3, des développements précis et concrets sur les caractéristiques organoleptiques des vins mousseux rouges, rosés et blancs qui résultent de la culture de cépages autochtones et des conditions climatiques de la zone géographique considérée. Ainsi, l'existence d'un lien avec l'origine géographique doit être regardée comme établie par ce cahier des charges pour les vins mousseux de qualité rouges, rosés et blancs. Dans ces conditions, le moyen tiré de que l'homologation de l'arrêté attaqué procèderait d'une erreur d'appréciation en l'absence d'un tel lien doit être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par la Confédération des vins IGP de France, que la fédération requérante n'est pas fondée à demander l'annulation de l'arrêté attaqué. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fédération nationale des producteurs et élaborateurs de Crémant la somme de 2 000 euros à verser à l'INAO au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la Confédération des vins IGP de France et du syndicat des vins des coteaux alpins sont admises.<br/>
Article 2 : La requête de la Fédération nationale des producteurs et élaborateurs de Crémant est rejetée.<br/>
Article 3 : La Fédération nationale des producteurs et élaborateurs de Crémant versera une somme de 2 000 euros à l'Institut national de l'origine et de la qualité au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Fédération nationale des producteurs et élaborateurs de Crémant, au ministre de l'économie et des finances, au ministre de l'agriculture et de l'alimentation, au ministre de l'action et des comptes publics, à l'Institut national de l'origine et de la qualité, à la Confédération des vins IGP de France et au syndicat des vins des coteaux alpins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
