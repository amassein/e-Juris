<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044167188</ID>
<ANCIEN_ID>JG_L_2021_10_000000448820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/16/71/CETATEXT000044167188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 04/10/2021, 448820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DUHAMEL - RAMEIX - GURY- MAITRE</AVOCATS>
<RAPPORTEUR>M. François-René Burnod</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448820.20211004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Etablissements Darty et Fils a demandé au tribunal administratif d'Orléans de prononcer la décharge de la cotisation de taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2016 dans les rôles de la commune de Tours (Indre-et-Loire) à raison de locaux situés 31 rue Gustave Eiffel, ainsi que la restitution de la somme qu'elle a acquittée à ce titre. Par un jugement n° 1802700 du 30 novembre 2020, le tribunal a prononcé la décharge sollicitée et rejeté le surplus des conclusions de la demande de la société.  <br/>
<br/>
              Par un pourvoi enregistré le 18 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 1er ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de rejeter la demande de la société Etablissements Darty et Fils.<br/>
<br/>
<br/>
<br/>
              A... les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François-René Burnod, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Duhamel - Rameix - Gury, avocat de la société Etablissements Darty et Fils ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Etablissements Darty et Fils a demandé la décharge de la taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2016 dans les rôles de la commune de Tours (Indre-et-Loire) à raison de locaux dont elle est propriétaire situés au 31 rue Gustave Eiffel en excipant de l'illégalité de la délibération du 16 décembre 2015 par laquelle la communauté d'agglomération Tour(s)plus a fixé le taux de la taxe d'enlèvement des ordures ménagères. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre le jugement du 30 novembre 2020 par lequel le tribunal administratif d'Orléans a fait droit à cette demande. <br/>
<br/>
              2. Dans l'hypothèse où l'illégalité d'un acte règlementaire a cessé, du fait d'un changement de circonstances, à la date à laquelle le juge doit se placer pour apprécier la légalité d'un acte pris pour son application ou dont il constitue la base légale, il incombe au juge, saisi d'une exception d'illégalité de cet acte réglementaire soulevée à l'appui de la contestation de ce second acte, de l'écarter. De la même façon, lorsque le juge de l'impôt est saisi, au soutien d'une contestation du bien-fondé de l'impôt, d'une exception d'illégalité de l'acte réglementaire sur la base duquel a été prise une décision individuelle d'imposition, il lui appartient de l'écarter lorsque cet acte réglementaire est, par l'effet d'un changement de circonstances, devenu légal à la date du fait générateur de l'imposition. <br/>
<br/>
              3. Aux termes du I de l'article 1520 du code général des impôts, dans sa rédaction issue de l'article 57 de la loi du 29 décembre 2015 portant loi de finances rectificative pour 2015, rendu applicable à compter du 1er janvier 2016 en vertu du A du III de ce même article : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service de collecte et de traitement des déchets ménagers et des déchets mentionnés à l'article L. 2224-14 du code général des collectivités territoriales, dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal (...) ". Aux termes de l'article 1521 du même code : " La taxe porte sur toutes les propriétés soumises à la taxe foncière sur les propriétés bâties ou qui en sont temporairement exonérées ". Aux termes de l'article 1415 du même code : " La taxe foncière sur les propriétés bâties, la taxe foncière sur les propriétés non bâties et la taxe d'habitation sont établies pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition ". Les déchets mentionnés à l'article L. 2224-14 du code général des collectivités territoriales s'entendent des déchets non ménagers que ces collectivités peuvent, eu égard à leurs caractéristiques et aux quantités produites, collecter et traiter sans sujétions techniques particulières. <br/>
<br/>
              4. Il résulte de la combinaison des dispositions citées au point 3 qu'à la date du fait générateur de la taxe d'enlèvement des ordures ménagères en litige, soit le 1er janvier 2016, cette imposition avait pour objet de couvrir les dépenses exposées par les collectivités territoriales pour assurer l'enlèvement et le traitement tant des ordures ménagères que des déchets non ménagers, dès lors qu'elles ne sont pas couvertes par des recettes non fiscales. <br/>
<br/>
              5. Par suite, en se bornant à constater qu'à la date du 16 décembre 2015, à laquelle l'assemblée délibérante de la communauté d'agglomération Tour(s)plus a fixé le taux de la taxe d'enlèvement des ordures ménagères pour l'année 2016, les dispositions alors applicables de l'article 1520 du code général des impôts ne permettaient de couvrir que la collecte et le traitement des seules ordures ménagères, pour en déduire que le taux ainsi fixé était entaché d'illégalité en ce qu'il aboutissait à une disproportion manifeste entre le produit de cette imposition et les dépenses exposées par la communauté d'agglomération pour l'enlèvement et le traitement des ordures ménagères non couvertes par des recettes non fiscales, alors qu'il lui appartenait de rechercher si cette illégalité subsistait à la date du fait générateur de l'imposition, eu égard au périmètre des dépenses pouvant être couvertes par le produit de cette taxe à compter du 1er janvier 2016, le tribunal administratif d'Orléans a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que le ministre de l'économie, des finances et de la relance est fondé à demander l'annulation de l'article 1er du jugement qu'il attaque. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente affaire, la somme que demande la société Etablissements Darty et Fils au titre des frais exposés par elle et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du 30 novembre 2020 du tribunal administratif d'Orléans est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif d'Orléans.<br/>
Article 3 : Les conclusions présentées par la société Etablissements Darty et Fils au titre de de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société par actions simplifiée Etablissements Darty et Fils.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
