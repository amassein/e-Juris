<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042512356</ID>
<ANCIEN_ID>JG_L_2020_11_000000445586</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/51/23/CETATEXT000042512356.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/11/2020, 445586, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445586</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445586.20201103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au juge des référés du tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au président du conseil départemental d'Indre-et-Loire de lui trouver un hébergement dans une structure agréée au titre de la protection de l'enfance et de prendre en charge son alimentation et ses besoins élémentaires, dans un délai de douze heures à compter de la notification de son ordonnance, sous astreinte de 200 euros par jour de retard, jusqu'à ce que le juge des enfants ait définitivement statué sur le recours fondé sur les articles 375 et suivants du code civil et, à titre subsidiaire, de prononcer la même injonction, assortie des mêmes conditions de délai et d'astreinte à l'encontre de la préfète d'Indre-et-Loire. Par une ordonnance n° 2003440 du 9 octobre 2020, le juge des référés du tribunal administratif d'Orléans a enjoint au département d'Indre-et-Loire de proposer à M. A... un hébergement d'urgence adapté à son âge présumé, incluant la prise en charge de son alimentation et de ses besoins élémentaires, dans un délai de 24 heures à compter de la notification de l'ordonnance, dans l'attente de la décision du juge des enfants, saisi par l'intéressé. <br/>
<br/>
              Par une requête, enregistrée le 22 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le département d'Indre-et-Loire demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler l'article 2 de l'ordonnance du 9 octobre 2020 ;<br/>
<br/>
              2°) de rejeter les conclusions présentées par M. A... en première instance ainsi que celles tendant à l'application des dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif d'Orléans était incompétent dès lors que la saisine préalable du juge des enfants en application de l'article 375 du code civil, par laquelle le mineur peut obtenir son admission à l'aide sociale à l'enfance, a rendu le recours formé devant le juge administratif irrecevable ;<br/>
              - la situation de précarité alléguée par M. A... ne saurait, à elle seule, même dans un contexte particulier de recrudescence de l'épidémie de covid-19, justifier d'une atteinte grave et manifestement illégale à une liberté fondamentale ;<br/>
              - l'ordonnance contestée est entachée d'une erreur de droit en ce que le juge des référés a méconnu son office limité au contrôle de l'erreur manifeste d'appréciation dès lors qu'il a exigé du département d'Indre-et-Loire qu'il rapporte la preuve, d'une part, que le requérant n'était " manifestement pas mineur " et, d'autre part, que l'acte de naissance du requérant avait un caractère irrégulier ;<br/>
              - elle est entachée d'un défaut de motivation dès lors qu'il n'est pas démontré en quoi l'appréciation portée par le département d'Indre-et-Loire, selon laquelle l'intéressé ne peut être qualifié de mineur isolé, est manifestement erronée ; <br/>
              - elle est entachée d'une erreur de droit et d'une erreur manifeste d'appréciation dès lors que le juge des référés de première instance a considéré que le département d'Indre-et-Loire ne remettait pas valablement en cause la véracité des mentions portées sur l'acte d'état civil de M. A..., alors que le département s'est fondé pour ce faire sur l'évaluation réalisée par les services du conseil départemental qui conclut, après une analyse précise et circonstanciée, à sa non-minorité ; <br/>
              - le département d'Indre-et-Loire n'a pas commis d'erreur manifeste d'appréciation en écartant comme non-probant, en se fondant sur l'évaluation réalisée par les services du conseil départemental, l'acte d'état civil de M. A....<br/>
              Par un mémoire en défense, enregistré le 29 octobre 2020, M. A... conclut à son admission au bénéfice de l'aide juridictionnelle, au rejet de la requête et à ce que soit mis à la charge du département d'Indre-et-Loire le versement à son conseil de la somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il soutient qu'aucun des moyens de la requête n'est fondé. <br/>
<br/>
              La requête a été communiquée au ministre des solidarités et de la santé qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le département d'Indre-et-Loire, d'autre part, M. A... ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 30 octobre 2020 à 14 heures 30 ;<br/>
<br/>
              - Me B..., avocate au Conseil d'Etat et à la Cour de cassation, avocate du département d'Indre-et-Loire ; <br/>
<br/>
              - le représentant du département d'Indre-et-Loire ; <br/>
<br/>
              - Me Piwnica, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ; <br/>
              - le représentant de M. A... ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) ".<br/>
<br/>
              2. Il résulte de l'instruction que M. C... A..., qui indique être un ressortissant bangladais né le 1er mai 2004, a sollicité sa prise en charge au titre de l'aide sociale à l'enfance auprès du département d'Indre-et-Loire. Par décision du 2 octobre 2020, le président du conseil départemental a mis fin à son accueil provisoire et refusé de poursuivre sa prise en charge par le service de l'aide sociale à l'enfance au motif que l'intéressé n'avait pas la qualité de mineur isolé. M. A... a saisi le juge des référés du tribunal administratif d'Orléans, le 6 octobre 2020, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au département d'Indre-et-Loire, et subsidiairement, à l'Etat, de procéder à son hébergement dans une structure agréée au titre de la protection de l'enfance et de prendre en charge ses besoins alimentaires, médicaux et vestimentaires. Par une ordonnance du 9 octobre 2020, le juge des référés de ce tribunal a enjoint au département d'Indre-et-Loire d'assurer à M. A... un hébergement d'urgence adaptée à son âge présumé, incluant la prise en charge de son alimentation et de ses besoins élémentaires, dans un délai de 24 heures à compter de la notification de l'ordonnance, dans l'attente de la décision du juge des enfants, saisi par l'intéressé. Le département d'Indre-et-Loire relève appel de cette ordonnance.<br/>
<br/>
              3. D'une part, aux termes de l'article 375 du code civil : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public (...) ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ". Aux termes des deux premiers alinéas de l'article 373-5 de ce code : " A titre provisoire mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4. / En cas d'urgence, le procureur de la République du lieu où le mineur a été trouvé a le même pouvoir, à charge de saisir dans les huit jours le juge compétent, qui maintiendra, modifiera ou rapportera la mesure. Si la situation de l'enfant le permet, le procureur de la République fixe la nature et la fréquence du droit de correspondance, de visite et d'hébergement des parents, sauf à les réserver si l'intérêt de l'enfant l'exige ".<br/>
<br/>
              4. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt et un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre (...) / ; 3° Mener en urgence des actions de protection en faveur des mineurs mentionnés au 1° du présent article ; / 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". L'article L. 223-2 de ce code dispose que : " Sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / (...) Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil ". L'article R. 221-11 du même code dispose que : " I. - Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II. - Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. (...) / IV. - Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge délivrée dans les conditions des articles L. 222-5 et R. 223-2. En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". Le même article dispose que les décisions de refus de prise en charge sont motivées et mentionnent les voies et délais de recours.<br/>
<br/>
              5. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants ou par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. A cet égard, une obligation particulière pèse sur ces autorités lorsqu'un mineur privé de la protection de sa famille est sans abri et que sa santé, sa sécurité ou sa moralité est en danger. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              6. Il en résulte également que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance, le président du conseil départemental peut seulement, au-delà de la période provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire mais ne peut, en aucun cas, décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire l'ait ordonné. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge judiciaire pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite. Lorsque le département refuse de saisir l'autorité judiciaire à l'issue de l'évaluation mentionnée au point 4 ci-dessus, au motif que l'intéressé n'aurait pas la qualité de mineur isolé, l'existence d'une voie de recours devant le juge des enfants par laquelle le mineur peut obtenir son admission à l'aide sociale rend irrecevable le recours formé devant le juge administratif contre la décision du département.<br/>
<br/>
              7. Il appartient toutefois au juge du référé, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, lorsqu'il lui apparaît que l'appréciation portée par le département sur l'absence de qualité de mineur isolé de l'intéressé est manifestement erronée et que ce dernier est confronté à un risque immédiat de mise en en danger de sa santé ou de sa sécurité, d'enjoindre au département de poursuivre son accueil provisoire.<br/>
<br/>
              8. D'autre part, l'article 47 du code civil dispose que : " Tout acte de l'état civil des Français et des étrangers fait en pays étranger et rédigé dans les formes usitées dans ce pays fait foi, sauf si d'autres actes ou pièces détenus, des données extérieures ou des éléments tirés de l'acte lui-même établissent, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ". <br/>
<br/>
              9. Il résulte de l'instruction que pour refuser de poursuivre, au titre de l'aide sociale à l'enfance, la prise en charge de M. A... qui déclare être âgé de 16 ans, le département d'Indre-et-Loire, qui a satisfait aux obligations d'accueil provisoire d'urgence qui lui incombaient en vertu des dispositions de l'article R. 221-11 du code de l'action sociale et des famille citées au point 4 ci-dessus, s'est fondé sur les conclusions de l'évaluation prévue par ces mêmes dispositions, selon lesquelles, alors même que le certificat de naissance produit par M. A... avait reçu un avis favorable de la direction interdépartementale de la police aux frontières à laquelle ce document avait été transmis pour examen, le discours, le comportement et l'apparence physique de l'intéressé ne permettaient pas de corroborer la minorité alléguée. Pour contester les conclusions de cette évaluation, M. A... se borne à se prévaloir des mentions figurant sur le certificat de naissance produit alors qu'il résulte des dispositions de l'article 47 du code civil cité au point 8 ci-dessus que les actes d'état-civil étrangers peuvent être écartés lorsque des données extérieures établissent que les faits qui y sont déclarés ne correspondent pas à la réalité, et à critiquer, sans étayer son argumentation, la manière dont cette évaluation aurait été conduite, son caractère subjectif et la formation des agents chargés de ces évaluations. Dans ces conditions, l'appréciation portée par le président du conseil départemental d'Indre-et-Loire sur le défaut de qualité de mineur isolé de l'intéressé n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, manifestement erronée. Par suite, son refus de poursuivre la prise en charge de M. A... ne révèle, de sa part, aucune atteinte grave et manifestement illégale au droit à l'hébergement et à la prise en charge éducative d'un enfant mineur. Le département d'Indre-et-Loire est ainsi fondé à soutenir que c'est à tort que, par l'article 2 de l'ordonnance attaquée, le juge des référés du tribunal administratif d'Orléans lui a enjoint de poursuivre la prise en charge de l'hébergement de M. A... dans une structure agréée au titre de l'aide sociale à l'enfance et d'assurer ses besoins alimentaires, sanitaires et médicaux, dans l'attente de la décision du juge des enfants saisi par l'intéressé sur le fondement des articles 375 et suivants du code civil.<br/>
<br/>
              10. Il appartient au juge des référés du Conseil d'Etat d'examiner, au titre de l'effet dévolutif de l'appel, les conclusions subsidiaires présentées par M. A... devant le juge des référés du tribunal administratif d'Orléans.<br/>
<br/>
              11. Ainsi qu'il a été dit au point 5 ci-dessus, il incombe aux seules autorités du département de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. Par suite, les conclusions subsidiaires présentées par M. A... tendant à ce qu'il soit enjoint à la préfète d'Indre-et-Loire, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, de lui trouver un hébergement dans une structure agréée au titre de la protection de l'enfance et de prendre en charge son alimentation et ses besoins élémentaires jusqu'à ce que le juge des enfants ait définitivement statué sont irrecevables. Elles doivent dès lors être rejetées.<br/>
<br/>
              12. Sans qu'il y ait lieu d'admettre M. A... au bénéfice de l'aide juridictionnelle provisoire, les dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à la charge du département d'Indre-et-Loire, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'article 2 de l'ordonnance du 9 octobre 2020 du juge des référés du tribunal administratif d'Orléans est annulé. <br/>
Article 2 : Les conclusions aux fins d'injonction présentées par M. A... devant le juge des référés du tribunal administratif d'Orléans ainsi que ses conclusions présentées devant le juge des référés du Conseil d'Etat sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée au président du conseil départemental d'Indre-et-Loire et à M. C... A....<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
