<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024942945</ID>
<ANCIEN_ID>JG_L_2011_12_000000341352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/94/29/CETATEXT000024942945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 07/12/2011, 341352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 juillet et 11 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société civile immobilière (SCI) GID, dont le siège est au 5 rue Euryale Dehaynin à Paris (75019) ; la SCI GID demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0808629 du 24 mars 2010 par lequel le tribunal administratif de Montreuil a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une indemnité de 376 843,45 euros, assortie des intérêts au taux légal et des intérêts des intérêts, en réparation du préjudice résultant pour elle, au titre de la période du 12 septembre 2006 au 31 décembre 2009, de la décision implicite de rejet née du silence gardé pendant plus de deux mois par le préfet de la Seine-Saint-Denis sur sa demande du 11 juillet 2006 tendant à ce que lui soit accordé le concours de la force publique en vue de l'exécution du jugement du juge unique du tribunal de grande instance de Bobigny du 26 octobre 2005 ordonnant l'expulsion des occupants de locaux d'un immeuble situé chemin des vignes à Pantin ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu la loi n° 91-650 du 9 juillet 1991 ; <br/>
<br/>
              Vu le décret n° 92-755 du 31 juillet 1992 ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la SCI GID, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la SCI GID ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 15 mai 2003, la SCI GID a donné à bail, par des contrats portant la qualification de baux commerciaux, des locaux dont elle est propriétaire rue des Vignes à Pantin et que ces locaux ont été en réalité, avec l'accord des preneurs, utilisés par d'autres personnes qui y avaient leur habitation principale ; que, par jugement du 26 octobre 2005, le juge unique du tribunal de grande instance de Bobigny a ordonné l'expulsion de ces occupants ; que le préfet de la Seine-Saint-Denis a implicitement rejeté le 11 septembre 2006 la demande de concours de la force publique que la SCI a formée le 11 juillet 2006 en vue de l'exécution de ce jugement ; que la SCI GID se pourvoit en cassation contre le jugement du 24 mars 2010 par lequel le tribunal administratif de Montreuil a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une indemnité en réparation du préjudice résultant pour elle, à compter du 12 septembre 2006, de la décision implicite de rejet du 11 septembre 2006 ;<br/>
<br/>
              Considérant que, pour rejeter cette demande d'indemnité, le tribunal administratif a jugé que le préjudice lié à la perte de loyers, dont la SCI demandait réparation, était exclusivement imputable à sa propre imprudence qui tenait à ce que, comme l'avait jugé la cour d'appel de Paris par un arrêt du 21 mars 2007, les baux conclus par la SCI ne pouvaient être qualifiés de baux commerciaux dès lors que cette qualification leur avait été donnée par la SCI dans son seul intérêt et que ces baux ne portaient ni sur un local commercial ni sur des bureaux commerciaux compte tenu de l'état délabré des lieux ; <br/>
<br/>
              Considérant que le tribunal administratif n'a pas commis d'erreur de qualification juridique en estimant qu'il existait un lien direct de causalité entre d'une part le comportement de la SCI, qui est à l'origine de ce que les locaux n'ont pas été utilisés conformément aux baux qu'elle avait consentis, et d'autre part le préjudice résultant pour elle de ce qu'aucun loyer ne lui était versé par les occupants qui se sont installés dans les locaux pour y habiter ; qu'en revanche, en excluant tout lien de causalité direct entre ce préjudice et l'absence d'une décision d'octroi du concours de la force publique en vue de l'expulsion des occupants, le tribunal administratif a entaché son jugement d'une erreur de qualification juridique ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SCI GID est fondée à demander l'annulation du jugement du tribunal administratif de Montreuil du 24 mars 2010  ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'aux termes de l'article 16 de la loi du 9 juillet 1991 portant réforme des procédures civiles d'exécution :   L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation  et qu'aux termes de l'article 61 de la même loi :  Sauf disposition spéciale, l'expulsion ou l'évacuation d'un immeuble ou d'un lieu habité ne peut être poursuivie qu'en vertu d'une décision de justice ou d'un procès-verbal de conciliation exécutoire ...   ; qu'aux termes de l'article 539 du code de procédure civile :  Le délai de recours par voie ordinaire suspend l'exécution d'un jugement. Le recours exercé dans le délai est également suspensif   et qu'aux termes du 1er alinéa de l'article 514 du code de procédure civile :   L'exécution provisoire ne peut pas être poursuivie sans avoir été ordonnée si ce n'est pour les décisions qui en bénéficient de plein droit  ;<br/>
<br/>
              Considérant, en premier lieu, que le jugement du 26 octobre 2005 du juge unique du tribunal de grande instance de Bobigny ordonnant l'expulsion des occupants des locaux appartenant à la SCI GID, qui n'était pas exécutoire de plein droit à titre provisoire et qui n'ordonnait pas dans son dispositif son exécution provisoire, ne constituait pas un titre exécutoire ; que le préfet de la Seine-Saint-Denis était par suite tenu de rejeter la demande de concours de la force publique que la SCI GID lui a présentée le 11 juillet 2006 pour l'exécution de ce jugement ; que, dès lors, la décision implicite du 11 septembre 2006 par laquelle le préfet a rejeté cette demande n'est pas de nature à engager la responsabilité de l'Etat ; <br/>
<br/>
              Considérant, en second lieu, que le jugement du 26 octobre 2005, frappé d'appel par la SCI GID, a été infirmé par un arrêt du 21 mars 2007 de la cour d'appel de Paris, laquelle, par un second arrêt du 12 septembre 2007 rendu sur une requête en omission de statuer formée par la SCI, a complété son premier arrêt en ordonnant l'expulsion des occupants du chef des preneurs dans le délai de deux mois suivant la notification à ceux-ci de son arrêt ainsi complété, avec commandement de quitter les lieux ; que, si ce second arrêt constitue un titre exécutoire, il ne résulte pas de l'instruction que la SCI GID aurait demandé le concours de la force publique en vue de son exécution ; qu'en effet, la nouvelle demande de concours de la force publique dont elle se prévaut, qu'elle a adressée au préfet le 22 août 2008, ne contient pas, comme le prescrit l'article 50 du décret du 31 juillet 1992 pris pour l'application de la loi du 9 juillet 1991, une copie du dispositif de cet arrêt et n'en fait d'ailleurs aucune mention ; que, dans ces conditions, le préfet de la Seine-Saint-Denis ne peut être regardé comme ayant été ainsi saisi d'une demande de concours de la force publique en vue de l'exécution de l'arrêt du 12 septembre 2007 ; que, par suite, la décision implicite du 22 octobre 2008 par laquelle le préfet a rejeté la demande de concours de la force publique du 22 août 2008 n'est pas davantage de nature à engager le responsabilité de l'Etat ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la demande de la SCI GID tendant à la condamnation de l'Etat à lui verser une indemnité doit être rejetée ; que doivent être rejetées par voie de conséquence ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil du 24 mars 2010 est annulé.<br/>
<br/>
Article 2 : La demande présentée par la SCI GID devant le tribunal administratif de Montreuil est rejetée.<br/>
<br/>
		Article 3 : Le surplus des conclusions du pourvoi de la SCI GID est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à SCI GID et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
