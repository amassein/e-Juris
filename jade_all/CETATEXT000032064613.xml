<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032064613</ID>
<ANCIEN_ID>JG_L_2016_02_000000384353</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/06/46/CETATEXT000032064613.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 15/02/2016, 384353, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384353</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:384353.20160215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 9 et 11 septembre 2014, 27 novembre 2014 et 26 février 2015 au secrétariat du contentieux du Conseil d'Etat, la société Mutuelle des transports assurances (MTA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 10 juillet 2014 par laquelle l'Autorité de contrôle prudentiel et de résolution (ACPR) a engagé à son encontre la procédure de transfert d'office de son portefeuille de contrats, bulletins ou adhésions ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 62 ;<br/>
              - le code monétaire et financier ;<br/>
              - la décision du 21 novembre 2014 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société MTA ;<br/>
              - la décision n° 2014-449 QPC du 6 février 2015 statuant sur la question prioritaire de constitutionnalité soulevée par cette société ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Mutuelle des transports assurances et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>1. Aux termes du I de l'article L. 612-33 du code monétaire et financier, dans sa rédaction alors en vigueur : " Lorsque la solvabilité ou la liquidité d'une personne soumise au contrôle de l'Autorité ou lorsque les intérêts de ses clients, assurés, adhérents ou bénéficiaires, sont compromis ou susceptibles de l'être, (...) l'Autorité de contrôle prudentiel et de résolution prend les mesures conservatoires nécessaires./ Elle peut, à ce titre : / (...) 8° Prononcer le transfert d'office de tout ou partie du portefeuille des contrats d'assurance ou de règlements ou de bulletins d'adhésion à des contrats ou règlements des personnes mentionnées aux 1°, 3° et 5° du B du I de l'article L. 612-2 ainsi que tout ou partie d'un portefeuille de crédits ou de dépôts d'un établissement de crédit ; (...) ".<br/>
<br/>
              2. La décision attaquée, du 10 juillet 2014, par laquelle le collège de supervision de l'Autorité de contrôle prudentiel et de résolution a engagé, en application des dispositions précitées, la procédure de transfert d'office du portefeuille de contrats, bulletins ou adhésions de la société MTA, fait grief à cette dernière. Par suite, la fin de non recevoir opposée sur ce point par cette autorité doit être écartée.<br/>
<br/>
              3. Par sa décision n° 2014-449 QPC du 6 février 2015, le Conseil constitutionnel, statuant sur la question prioritaire de constitutionnalité soulevée par la société requérante, que le Conseil d'Etat statuant au contentieux lui avait transmise par la décision visée ci-dessus, a déclaré contraires à la Constitution les mots " tout ou partie du portefeuille des contrats d'assurance ou de règlements ou de bulletins d'adhésion à des contrats ou règlements des personnes mentionnées aux 1°, 3° et 5° du B du I de l'article L. 612-2 ainsi que " figurant au 8° du I de l'article L. 612-33 du code monétaire et financier. Le Conseil constitutionnel a précisé que cette déclaration d'inconstitutionnalité était applicable à toutes les affaires non jugées définitivement à la date de publication de sa décision.<br/>
<br/>
              4. Il résulte de la décision mentionnée ci-dessus du Conseil constitutionnel que les dispositions législatives en vertu desquelles le collège de supervision de l'Autorité de contrôle prudentiel et de résolution a engagé, par la décision attaquée, la procédure de transfert d'office du portefeuille de contrats, bulletins ou adhésions de la société MTA sont contraires à la Constitution. Par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, la décision attaquée qui, du fait de l'exercice par la société requérante du recours pour excès de pouvoir sur lequel il est statué par la présente décision, n'était pas devenue définitive à la date de publication de la décision du Conseil constitutionnel, doit être annulée.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat (Autorité de contrôle prudentiel et de résolution) la somme de 3 500 euros à verser à la société MTA au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la société requérante qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 10 juillet 2014 par laquelle le collège de supervision de l'Autorité de contrôle prudentiel et de résolution a engagé la procédure de transfert d'office du portefeuille de contrats, bulletins ou adhésions de la société MTA est annulée.<br/>
Article 2 : L'Etat (Autorité de contrôle prudentiel et de résolution) versera à la société MTA une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées au même titre sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Mutuelle des transports assurances (MTA) et à l'Autorité de contrôle prudentiel et de résolution.<br/>
Copie en sera adressée pour information au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
