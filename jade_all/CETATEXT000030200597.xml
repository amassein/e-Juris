<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200597</ID>
<ANCIEN_ID>JG_L_2015_01_000000383462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 26/01/2015, 383462, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383462.20150126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 5 août 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ; le ministre demande au Conseil d'Etat d'annuler l'ordonnance n° 1402913 du 30 juillet 2014 par laquelle le juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a, sur la demande de la commune de Saint-Cyr-sur-Mer, suspendu l'exécution de la décision du 12 juin 2014 par laquelle le recteur de l'académie a refusé d'autoriser l'expérimentation des rythmes scolaires pour l'année 2014-2015 projetée par cette commune ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 2013-77 du 24 janvier 2013 ;<br/>
<br/>
              Vu le décret n° 2014-457 du 7 mai 2014 ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Saint-Cyr-sur-Mer ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision." ; <br/>
<br/>
              2. Considérant que pour retenir qu'il y avait urgence à suspendre la décision du 12 juin 2014 par laquelle le recteur de l'académie de Nice a refusé d'autoriser la commune de Saint-Cyr-sur-Mer à mener une expérimentation dans l'organisation des rythmes scolaires pour l'année 2014-2015, le juge des référés du tribunal administratif de Nice a relevé que cette expérimentation avait vocation à s'appliquer dès la rentrée de septembre 2014 ; qu'il a, ce faisant, suffisamment motivé son ordonnance et n'a pas commis d'erreur de droit ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 1er du décret du 7 mai 2014 portant autorisation d'expérimentations relatives à l'organisation des rythmes scolaires dans les écoles maternelles et élémentaires : " A titre expérimental, pour une durée de trois ans, le recteur d'académie peut autoriser des adaptations à l'organisation de la semaine scolaire dérogeant aux dispositions des premier, deuxième et quatrième alinéas de l'article D. 521-10 du code de l'éducation. Ces adaptations ne peuvent toutefois avoir pour effet de répartir les enseignements sur moins de huit demi-journées par semaine, comprenant au moins cinq matinées, ni d'organiser les heures d'enseignement sur plus de vingt-quatre heures hebdomadaires, ni sur plus de six heures par jour et trois heures trente par demi-journée. Ces adaptations peuvent s'accompagner d'une dérogation aux dispositions de l'article D. 521-2 du même code. / Les adaptations prévues à l'alinéa précédent ne peuvent avoir pour effet de réduire ou d'augmenter sur une année scolaire le nombre d'heures d'enseignement ni de modifier leur répartition. (...) Le recteur s'assure du bien-fondé éducatif de l'expérimentation, de sa cohérence avec les objectifs poursuivis par le service public de l'éducation, de sa compatibilité avec l'intérêt du service et, le cas échéant, avec le projet éducatif territorial mentionné à l'article L. 551-1 du code de l'éducation (...) " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la commune de Saint-Cyr-sur-Mer a demandé, le 27 mai 2014, au recteur de l'académie de Nice de l'autoriser à adapter, à titre expérimental, l'organisation de la semaine scolaire ; que, par une décision du 12 juin 2014, le recteur a rejeté cette demande ; <br/>
<br/>
              5. Considérant qu'en jugeant que le moyen tiré de la méconnaissance de l'article 1er du décret du 7 mai 2014 était propre, en l'état de l'instruction, à créer un doute sérieux sur la légalité de la décision du recteur dès lors qu'il n'apparaissait pas que le projet de la commune, conforme aux règles de la semaine scolaire requises à minima, ne permettrait pas aux enfants les plus en difficulté de bénéficier des activités pédagogiques complémentaires proposées par les enseignants après la classe, ni que l'ajout d'une demi-journée sans classe le vendredi après-midi ne garantirait pas la continuité du temps scolaire et ne serait pas en cohérence avec les objectifs poursuivis par le service public de l'éducation, le juge des référés du tribunal administratif de Nice, qui a suffisamment motivé son ordonnance sur ce point, n'a, eu égard à son office, pas commis d'erreur de droit ni dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche n'est pas fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la commune de Saint-Cyr-sur-Mer au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est rejeté.  <br/>
Article 2 : L'Etat versera à la commune de Saint-Cyr-sur-Mer une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à la commune de Saint-Cyr-sur-Mer.<br/>
Copie en sera adressée pour information au recteur de l'académie de Nice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
