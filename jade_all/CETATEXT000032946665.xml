<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032946665</ID>
<ANCIEN_ID>JG_L_2016_07_000000401626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/66/CETATEXT000032946665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/07/2016, 401626</TITRE>
<DATE_DEC>2016-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:401626.20160728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la décision par laquelle le département des Hautes-Pyrénées a mis fin à sa prise en charge au titre de l'aide sociale à l'enfance en qualité de mineur étranger isolé. Par une ordonnance n° 1601295 du 11 juillet 2016, le juge des référés du tribunal administratif de Pau a rejeté sa demande. <br/>
<br/>
              Par une requête enregistrée le 19 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de suspendre la décision du conseil départemental mettant fin à sa prise en charge et de lui enjoindre de reprendre la prise en charge dans l'attente de la décision du juge des enfants saisi sur le fondement de l'article 375 du code civil ;<br/>
<br/>
              3°) de mettre à la charge du département des Hautes-Pyrénées, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision mettant fin à sa prise en charge au titre de l'aide sociale à l'enfance en qualité de mineur étranger isolé le place dans une situation de danger et de détresse ;<br/>
              - il est porté une atteinte grave et manifestement illégale, d'une part, à son droit de demander l'asile dès lors que le refus d'enregistrement de sa demande d'asile le contraint à retourner au Bangladesh et, d'autre part, à son droit à l'hébergement d'urgence ;<br/>
              - l'ordonnance contestée est entachée d'une erreur d'appréciation des faits en ce que le juge des référés n'a pas procédé aux vérifications de la force probante de son acte de naissance ;<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle méconnaît l'article 388 du code civil.<br/>
<br/>
<br/>
              Par une intervention, enregistrée le 21 juillet 2016, l'association La Cimade demande que le Conseil d'Etat fasse droit aux conclusions de la requête. Elle se réfère aux moyens exposés dans la requête de M.A....<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 juillet 2016, le conseil départemental des Hautes-Pyrénées conclut au rejet de la requête. Il soutient qu'aucun des moyens invoqués n'est fondé.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A...et la Cimade, d'autre part, le département des Hautes Pyrénées, ainsi que la ministre des affaires sociales et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 juillet 2016 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - le représentant de la Cimade ; <br/>
<br/>
              - Me Delamarre, avocat au Conseil d'Etat et à la Cour de cassation, avocat du département des Hautes Pyrénées ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur l'intervention de la Cimade : <br/>
<br/>
               1. La Cimade, qui intervient au soutien des conclusions de la requête, justifie, eu égard à son objet statutaire et à la nature du litige, d'un intérêt suffisant pour intervenir dans la présente instance. Son intervention est, par suite, recevable.<br/>
<br/>
<br/>
<br/>
              Sur l'appel de M.A... : <br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures". Le droit d'asile et le droit à l'hébergement d'urgence constituent des libertés fondamentales au sens de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              3. M. A...est un ressortissant bangladais arrivé à Tarbes le 29 février 2016. Ce même jour, le conseil départemental des Hautes-Pyrénées l'a pris en charge au titre de l'aide sociale à l'enfance en qualité de mineur étranger isolé. Mais au vu des résultats d'une expertise osseuse pratiquée par le centre hospitalier de Bigorre le 18 mars 2016, concluant à sa majorité, le conseil départemental a interrompu sa prise en charge le 21 mars. Par courrier du 24 mars 2016, M. A...a saisi de sa situation le juge des enfants du tribunal de grande instance de Tarbes. Par un courrier du 18 avril 2016, le parquet a indiqué que la radiographie révélait un âge supérieur à 18 ans, que M. A...n'entrait donc pas dans le dispositif des mineurs non accompagnés et qu'en conséquence son dossier avait fait l'objet le 31 mars d'un classement sans suite pour non lieu à assistance éducative. M. A...s'est cependant vu refuser, le 30 mars 2016, par les services des étrangers de la préfecture des Hautes Pyrénées, l'enregistrement de sa demande d'asile, faute de tutelle. M. A... a alors saisi le juge des référés du tribunal administratif de Pau sur le fondement de l'article L. 521-2 du code de justice administrative en demandant la suspension de l'exécution de la décision par laquelle le département des Hautes-Pyrénées avait mis fin à sa prise en charge au titre de l'aide sociale à l'enfance en qualité de mineur étranger isolé. Par une ordonnance du 11 juillet 2016, le juge des référés du tribunal administratif de Pau a rejeté sa demande. M. A... relève appel de cette ordonnance.<br/>
<br/>
              4. L'article 375 du code civil dispose que : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public. Dans le cas où le ministère public a été avisé par le président du conseil départemental, il s'assure que la situation du mineur entre dans le champ d'application de l'article L. 226-4 du code de l'action sociale et des familles. Le juge peut se saisir d'office à titre exceptionnel ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier :/ (...) 3° A un service départemental de l'aide sociale à l'enfance (...) ". L'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ". <br/>
<br/>
              5. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés par l'autorité judiciaire au service de l'aide sociale à l'enfance. Il en résulte également que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance, le président du conseil départemental peut seulement, au-delà de la période provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire mais ne peut, en aucun cas, décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire l'ait ordonné. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge judiciaire pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.<br/>
<br/>
              6. Dans la présente affaire, compte tenu de la décision du procureur de la République selon laquelle M. A...n'entrait pas dans le champ des mesures prévues pour les mineurs isolés, et en l'absence d'autre décision de l'autorité judiciaire, la suspension de la prise en charge de l'intéressé par le département des Hautes-Pyrénées ne révèle, quelles que soient les conditions dans lesquelles l'expertise osseuse à laquelle M. A...a été soumis a été pratiquée, aucune atteinte grave et manifestement illégale au droit à l'hébergement et à la prise en charge éducative d'un enfant mineur. M. A...n'est donc pas fondé à se plaindre du rejet de sa requête par le juge des référés du tribunal administratif de Pau. Son appel, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut en conséquence qu'être rejeté. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Cimade est admise.<br/>
Article 2 : La requête de M. A...est rejetée. <br/>
Article 3 : La présente ordonnance sera notifiée à M. B...A..., à la Cimade, au département des Hautes Pyrénées et à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-01-01 AIDE SOCIALE. ORGANISATION DE L'AIDE SOCIALE. COMPÉTENCES DU DÉPARTEMENT. - 1) PRISE EN CHARGE DE L'HÉBERGEMENT ET DES BESOINS D'UN MINEUR CONFIÉ À L'ASE PAR LE JUGE JUDICIAIRE [RJ1] - 2) DEMANDE D'UN MINEUR D'ÊTRE ADMIS À L'ASE [RJ2] - A) POUVOIR DU PCD DE SAISIR L'AUTORITÉ JUDICIAIRE - EXISTENCE - POSSIBILITÉ D'ADMETTRE LE MINEUR SANS DÉCISION DE JUSTICE - ABSENCE - B) VOIE DE RECOURS SPÉCIFIQUE, POUR LE MINEUR, DEVANT LE JUGE DES ENFANTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - 1) DEVOIR DU DÉPARTEMENT - PRISE EN CHARGE DE L'HÉBERGEMENT ET DES BESOINS D'UN MINEUR CONFIÉ À L'ASE PAR LE JUGE JUDICIAIRE [RJ1] - 2) DEMANDE D'UN MINEUR D'ÊTRE ADMIS À L'ASE [RJ2] - A) POUVOIR DU PCD DE SAISIR L'AUTORITÉ JUDICIAIRE - EXISTENCE - POSSIBILITÉ D'ADMETTRE LE MINEUR SANS DÉCISION DE JUSTICE - ABSENCE - B) VOIE DE RECOURS SPÉCIFIQUE, POUR LE MINEUR, DEVANT LE JUGE DES ENFANTS.
</SCT>
<ANA ID="9A"> 04-01-01 1) Il résulte des articles  375 et 375-3 du code civil ainsi que des articles L. 221-1 et L. 222-5 du code de l'action sociale et des familles qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés par l'autorité judiciaire au service de l'aide sociale à l'enfance (ASE).... ,,2) a) Il en résulte également que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance, le président du conseil départemental (PCD) peut seulement, au-delà de la période provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire mais ne peut, en aucun cas, décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire l'ait ordonné.,,,b) L'article 375 du code civil autorise le mineur à solliciter lui-même le juge judiciaire pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.</ANA>
<ANA ID="9B"> 04-02-02 1) Il résulte des articles  375 et 375-3 du code civil ainsi que des articles L. 221-1 et L. 222-5 du code de l'action sociale et des familles qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés par l'autorité judiciaire au service de l'aide sociale à l'enfance (ASE).... ,,2) a) Il en résulte également que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance, le président du conseil départemental (PCD) peut seulement, au-delà de la période provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire mais ne peut, en aucun cas, décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire l'ait ordonné....  ,,b) L'article 375 du code civil autorise le mineur à solliciter lui-même le juge judiciaire pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 juillet 2016, Département du Nord c/ M.,, n° 400055, p. 387.,,[RJ2] Cf. CE, 1er juillet 2015, Département du Nord, n° 386769, T. p. 551-594-791.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
