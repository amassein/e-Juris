<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037103070</ID>
<ANCIEN_ID>JG_L_2018_06_000000407821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/10/30/CETATEXT000037103070.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 25/06/2018, 407821, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407821.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler la décision du 16 juillet 2013 par laquelle le maire de Mauguio-Carnon a refusé de réexaminer le calcul du montant de l'allocation d'aide au retour à l'emploi qu'elle lui verse et de lui restituer les sommes indûment retenues, selon l'intéressée, à l'occasion du paiement de cette allocation et, d'autre part, d'enjoindre à la commune de Mauguio-Carnon de procéder à un nouvel examen de sa situation en vue de la rétrocession des retenues indues sur son allocation d'aide au retour à l'emploi, sous astreinte de 100 euros par jour de retard à compter de la notification de la décision à intervenir. Par un jugement n° 1304309 du 5 juin 2015, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt N° 15MA02982 du 7 février 2017, enregistré le 8 février 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire, enregistrés les 21 juillet 2015 et 21 juillet 2016 au greffe de cette cour, présentés par MmeA.... Par cette requête, ce mémoire et par un nouveau mémoire enregistré le 30 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier du 5 juin 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Mauguio-Carnon la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - l'arrêté du 15 juin 2011 portant agrément de la convention du 6 mai 2011 relative à l'indemnisation du chômage et de son règlement général annexé ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... A..., agent territorial spécialisé des écoles maternelles titulaire, employée par la commune de Mauguio-Carnon, a été placée à sa demande en position de disponibilité pour convenances personnelles pour une durée d'un an renouvelable à compter du 1er mars 2008. Après trois renouvellements de sa disponibilité pour convenances personnelles, elle a demandé sa réintégration dans les services communaux mais, faute d'emploi vacant, a été placée en disponibilité d'office à compter du 1er mars 2012. Elle a été admise par la commune de Mauguio-Carnon, à compter du mois de mars 2012, au bénéfice de l'allocation d'aide au retour à l'emploi, dont le montant a été fixé après déduction des rémunérations perçues au titre d'une activité occasionnelle auprès de l'université de Montpellier I. Par une décision du 16 juillet 2013, le maire de Mauguio-Carnon a refusé de réexaminer le montant de son allocation d'aide au retour à l'emploi et de lui verser les sommes représentatives des montants déduits de cette allocation du fait de son activité occasionnelle. Mme A...se pourvoit en cassation contre le jugement du tribunal administratif de Montpellier du 5 juin 2015 qui a rejeté sa demande tendant à l'annulation de cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 5422-1 du code du travail, dans sa rédaction applicable au litige : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi (...), aptes au travail et recherchant un emploi qui satisfont à des conditions d'âge et d'activité antérieure ". Aux termes de l'article L. 5424-1 du même code : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs, les agents titulaires des collectivités territoriales ainsi que les agents statutaires des autres établissements publics administratifs ainsi que les militaires (...) ". Il résulte de ces dispositions ainsi que de celles des articles L. 5422-2, L. 5422-3 et L. 5422-20 du même code que les agents titulaires des collectivités locales involontairement privés d'emploi ont droit à une allocation d'assurance dans les conditions définies par l'accord prévu à l'article L. 5422-20, dès lors qu'un tel accord est intervenu et a été agréé et qu'il n'est pas incompatible avec les règles qui gouvernent l'emploi des agents publics. <br/>
<br/>
              3. Aux termes de l'article 26 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité, de congé parental des fonctionnaires territoriaux et à l'intégration : " Sauf dans le cas où la période de mise en disponibilité n'excède pas trois mois, le fonctionnaire mis en disponibilité sur sa demande fait connaître à son administration d'origine sa décision de solliciter le renouvellement de la disponibilité ou de réintégrer son cadre d'emplois d'origine trois mois au moins avant l'expiration de la disponibilité. / (...) / Le fonctionnaire qui a formulé avant l'expiration de la période de mise en disponibilité une demande de réintégration est maintenu en disponibilité jusqu'à ce qu'un poste lui soit proposé dans les conditions prévues à l'article 97 de la loi du 26 janvier 1984 précitée (...) ". Un agent titulaire d'une collectivité territoriale ayant sollicité sa réintégration, qui était de droit, à l'issue d'une période de mise en disponibilité pour convenances personnelles, dont la demande a été rejetée faute de poste vacant et qui n'a reçu, du centre national de la fonction publique territoriale ou du centre de gestion local, aucune proposition en vue de son reclassement dans un emploi vacant correspondant à son grade, doit être regardé comme ayant été non seulement involontairement privé d'emploi mais aussi à la recherche d'un emploi, au sens de l'article L. 5422-1 du code du travail.<br/>
<br/>
              4. Selon les dispositions de l'article L. 5425-1 du code du travail, les allocations versées aux travailleurs involontairement privés d'emploi " (...) peuvent se cumuler avec les revenus tirés d'une activité occasionnelle ou réduite ainsi qu'avec les prestations de sécurité sociale ou d'aide sociale dans les conditions et limites fixées : / 1° Pour l'allocation d'assurance, par l'accord prévu à l'article L. 5422-20. (...) ". Aux termes du premier paragraphe de l'article 28 du règlement général annexé à la convention du 6 mai 2011 relative à l'indemnisation du chômage, agréé par arrêté du 15 juin 2011 : " Le salarié privé d'emploi qui remplit les conditions fixées aux articles 2 à 4 et qui exerce une activité occasionnelle ou réduite dont l'intensité mensuelle n'excède pas 110 heures perçoit l'allocation d'aide au retour à l'emploi, sous réserve : / a) Que la ou les activités conservées ne lui procurent pas des rémunérations excédant 70 % des rémunérations brutes mensuelles perçues avant la perte d'une partie de ses activités, ou / b) Que l'activité salariée reprise postérieurement à la perte de ses activités ne lui procure pas des rémunérations excédant 70 % des rémunérations brutes mensuelles prises en compte pour le calcul de l'allocation. / Pour l'application du seuil de 70 %, la rémunération procurée par l'activité occasionnelle ou réduite s'apprécie par mois civil ". Le premier alinéa de l'article 29 du règlement général stipule que : " L'allocation est intégralement cumulable avec les revenus tirés de l'activité occasionnelle ou réduite conservée. / L'allocation journalière est déterminée (...) sur la base d'un salaire de référence composé des rémunérations de l'emploi perdu ". Le premier alinéa de son article 30 prévoit que : " L'allocation est partiellement cumulable avec les revenus tirés de l'activité occasionnelle ou réduite reprise ". <br/>
<br/>
              5. Les dispositions des articles 28 et 29 du règlement général annexé à la convention du 6 mai 2011 relative à l'indemnisation du chômage qui prévoient le cumul intégral de l'allocation d'assurance avec les revenus tirés d'une activité occasionnelle ou réduite conservée visent à garantir l'indemnisation des personnes qui, exerçant simultanément plusieurs activités, viennent à perdre l'une d'elles. Par suite, afin de vérifier, pour l'application des dispositions des articles 28 à 30 de ce règlement général, si une activité occasionnelle ou réduite exercée par un agent titulaire d'une collectivité territoriale placé en disponibilité d'office faute d'avoir été réintégré à l'issue d'une période de mise en disponibilité pour convenances personnelles peut être regardée comme conservée, il convient de rechercher si cette activité a été exercée simultanément à l'activité qui lui ouvre droit à indemnisation au titre de l'assurance chômage.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...a droit à l'allocation d'aide au retour à l'emploi en raison de l'activité d'agent territorial spécialisé des écoles maternelles de la commune de Mauguio-Carnon qu'elle a exercée jusqu'à son placement en disponibilité pour convenances personnelles le 1er mars 2008, et qu'elle a commencé à exercer une activité occasionnelle auprès de l'université de Montpellier I, en qualité de surveillant d'examen, en décembre 2009, soit après l'interruption de son activité au sein de la commune. Par suite, en jugeant que cette activité occasionnelle ne pouvait être qualifiée d'activité conservée au sens de l'article 29 du règlement général annexé à la convention du 6 mai 2011 relative à l'indemnisation du chômage, alors même qu'elle n'a été admise au bénéfice de l'allocation d'aide au retour d'aide à l'emploi qu'à la suite de son placement en disponibilité d'office le 1er mars 2012, et en en déduisant que cette allocation ne pouvait faire l'objet d'un cumul intégral avec les revenus ainsi tirés de l'activité occasionnelle exercée, le tribunal n'a pas commis d'erreur de droit. Il en résulte que Mme A...n'est pas fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Mauguio-Carnon, qui n'est pas, dans la présente espèce, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la commune de Mauguio-Carnon. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
