<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027061605</ID>
<ANCIEN_ID>JG_L_2013_02_000000353157</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/16/CETATEXT000027061605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 11/02/2013, 353157, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353157</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353157.20130211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 5 octobre 2011 et 26 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 29 juillet 2011 du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, et du ministre de la fonction publique fixant la liste des professions prises en compte pour le classement dans le corps régi par le décret n° 2010-986 du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques, en tant que cet arrêté réserve le bénéfice de la reprise d'ancienneté qu'il prévoit aux personnes ayant exercé des activités professionnelles salariées ;<br/>
<br/>
              2°) d'enjoindre au ministre de la fonction publique et au ministre dont relèvent les personnels de catégorie A de la direction générale des finances publiques de prendre un nouvel arrêté, dans un délai de deux mois, avec effet rétroactif au 1er septembre 2011, en application de l'article 9 du décret n° 2006-1827 du 23 décembre 2006 relatif aux règles de classement d'échelon consécutif à la nomination dans certains corps de catégorie A de la fonction publique de l'Etat, sans restreindre le bénéfice du dispositif de reprise d'ancienneté aux périodes d'activité professionnelle accomplies sous un régime juridique de salarié ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 2006-1827 du 23 décembre 2006 ;<br/>
<br/>
              Vu le décret n° 2010-986 du 26 août 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur, <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la recevabilité de la requête et de l'intervention de la Fédération des finances et des affaires économiques CFDT :<br/>
<br/>
              1. Considérant que, par un arrêté du 29 juillet 2011, le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, et le ministre de la fonction publique ont, en application de l'article 9 du décret du 23 décembre 2006 relatif aux règles de classement d'échelon consécutif à la nomination dans certains corps de catégorie A, fixé la liste des professions prises en compte pour le classement dans le corps régi par le décret du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques ; que cette liste comprend vingt-deux  professions, dont celle d'avocat ;<br/>
<br/>
              2. Considérant que M. A..., qui a exercé, avant d'intégrer le corps des inspecteurs des finances publiques, la profession d'avocat à titre libéral, ne justifie pas en cette qualité d'un intérêt à agir contre les dispositions de l'arrêté contesté en ce qu'elles concernent les vingt-et-une autres professions ; qu'il n'est par suite recevable à demander l'annulation des dispositions attaquées qu'en tant qu'elles privent du dispositif de reprise d'ancienneté les anciens avocats qui ont exercé leur activité à titre libéral ;<br/>
<br/>
              3. Considérant que l'intervention au soutien des  conclusions de M. A...de la fédération des finances et des affaires économiques  CFDT n'est recevable que dans cette même mesure ;<br/>
<br/>
              Sur la légalité des dispositions attaquées :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 9 du décret du 23 décembre 2006 relatif aux règles de classement d'échelon consécutif à la nomination dans certains corps de catégorie A de la fonction publique de l'Etat : " Les personnes qui justifient de l'exercice d'une ou plusieurs activités professionnelles accomplies sous un régime juridique autre que celui d'agent public, dans des fonctions et domaines d'activité susceptibles d'être rapprochés de ceux dans lesquels exercent les membres du corps dans lequel ils sont nommés, sont classées à un échelon déterminé en prenant en compte, dans la limite de sept années, la moitié de cette durée totale d'activité professionnelle. / Un arrêté du ministre chargé de la fonction publique et du ministre intéressé fixe la liste des professions prises en compte et les conditions d'application du présent article. (...) " ;<br/>
<br/>
              5. Considérant qu'en vertu de l'article 1er de l'arrêté attaqué, sont prises en compte, pour le classement dans le corps régi par le décret du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques, les périodes de travail effectif exercées dans vingt-deux professions définies par la nomenclature des professions et catégories socioprofessionnelles des emplois salariés d'entreprise (PCS ESE) 2003 ; que, parmi elles, figure la profession d'avocat ; qu'aux termes de l'article 2 de l'arrêté : " L'inspecteur des finances publiques stagiaire qui demande à bénéficier des dispositions de l'article 9 du décret du 23 décembre 2006 susvisé doit fournir à l'appui de sa demande, et pour toute période dont il demande la prise en compte, un descriptif détaillé de l'emploi tenu, portant notamment sur le domaine d'activité, le positionnement de l'emploi au sein de l'organisme employeur, le niveau de qualification nécessaire et les principales fonctions attachées à cet emploi. Il doit en outre produire : - une copie du contrat de travail ; / - pour les périodes d'activité relevant du droit français, un certificat de l'employeur délivré dans les conditions prévues à l'article L. 1234-19 du code du travail. / A défaut des documents mentionnés aux deux précédents alinéas, il peut produire tout document établi par un organisme habilité attestant de la réalité de l'exercice effectif d'une activité salariée dans la profession pendant la période considérée. / Lorsque les documents ne sont pas rédigés en langue française, il en produit une traduction certifiée par un traducteur agréé. / L'administration a la possibilité de demander la production de tout ou partie des bulletins de paie correspondant aux périodes travaillées. / Elle peut demander la présentation des documents originaux ; ces documents ne peuvent être conservés par l'administration que pour le temps nécessaire à leur vérification et doivent en tout état de cause être restitués à leur possesseur dans un délai de quinze jours. " ;<br/>
<br/>
              6. Considérant qu'il résulte de ces dispositions que l'article 2 de l'arrêté attaqué réserve le bénéfice du dispositif de reprise d'ancienneté prévu par l'article 9 du décret du 23 décembre 2006 pour les fonctionnaires classés dans le corps des personnels de catégorie A de la direction générale des finances publiques ayant exercé antérieurement la profession d'avocat, à ceux qui ont exercé cette profession à titre salarié ;<br/>
<br/>
              7. Considérant d'une part, qu'une telle exigence d'exercice salarié n'est pas prévue par les dispositions de l'article 9 du décret, lequel se borne à prévoir la prise en compte d'activités professionnelles accomplies sous un régime juridique autre que celui d'agent public ; que, d'autre part, contrairement à ce que soutiennent le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, et le ministre de la fonction publique, l'article 9 du décret ne peut être interprété, en ce qu'il vise les " fonctions et domaines d'activité susceptibles d'être rapprochés de ceux dans lesquels exercent les membres du corps dans lequel ils sont nommés ", comme réservant le bénéfice de la reprise d'ancienneté aux  personnes ayant exercé leur profession à titre salarié ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède qu'en limitant, pour les  personnes ayant exercé la profession d'avocat, la prise en compte des activités antérieures, pour le classement dans le corps des inspecteurs des finances publiques, aux seules années pendant lesquelles ces activités ont été exercées sous un régime salarié, l'arrêté attaqué a ajouté une condition non prévue par le décret ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, M. A... est fondé à demander l'annulation de l'arrêté du 29 juillet 2011 en tant qu'il ne permet pas la prise en compte des années d'exercice de la profession d'avocat à titre libéral ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              10. Considérant que l'exécution de la présente décision implique nécessairement que les ministres compétents prennent les mesures permettant la prise en compte, pour le classement dans le corps des inspecteurs des finances publiques, des années d'exercice de la profession d'avocat à titre libéral ; qu'il y a lieu de leur enjoindre de prendre ces mesures dans un délai de quatre mois à compter de la notification de la présente décision ;	<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 100 euros à verser à M. A... au titre de ces dispositions ; que les conclusions présentées sur le même fondement au bénéfice de M. A... par la Fédération des finances et des affaires économiques de la CFDT ne sont pas recevables et doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'intervention de la Fédération des finances et des affaires économiques CFDT est admise  au soutien des seules conclusions dirigées contre l'arrêté du 29 juillet 2011 en tant qu'il ne permet pas la prise en compte des années d'exercice de la profession d'avocat à titre libéral.<br/>
<br/>
Article 2 : L'article 2 de l'arrêté du 29 juillet 2011 du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, et du ministre de la fonction publique fixant la liste des professions prises en compte pour le classement dans le corps régi par le décret n° 2010-986 du 26 août 2010 portant statut particulier des personnels de catégorie A de la direction générale des finances publiques est annulé, en tant qu'il ne permet pas la prise en compte des années d'exercice de la profession d'avocat à titre libéral. <br/>
<br/>
Article 3 : Il est enjoint au ministre de l'économie et des finances et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique de prendre les mesures permettant la prise en compte, pour le classement dans le corps des inspecteurs des finances publiques, des années d'exercice de la profession d'avocat à titre libéral, dans un délai de quatre mois à compter de la notification de la présente décision.<br/>
<br/>
 Article 4 : Le surplus des conclusions de la requête de M. A... est rejeté.<br/>
<br/>
Article 5 : L'Etat versera à M. A... la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : Les conclusions présentées par la Fédération des finances et des affaires économiques CFDT sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 7 : La présente décision sera notifiée à M. B... A..., à la Fédération des finances et des affaires économiques CFDT, au ministre de l'économie et des finances et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
