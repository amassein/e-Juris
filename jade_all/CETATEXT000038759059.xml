<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038759059</ID>
<ANCIEN_ID>JG_L_2019_07_000000419795</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/90/CETATEXT000038759059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 10/07/2019, 419795, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419795</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419795.20190710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...et Mme C...A...ont demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir l'arrêté du 15 janvier 2015 par lequel le maire de la commune de la Ferté-Saint-Aubin a retiré le permis de construire qu'ils avaient tacitement obtenu le 24 novembre 2014 et a refusé de leur délivrer un permis de construire afin de réhabiliter l'annexe au château de la Luzière dont ils sont propriétaires. Par un jugement n° 1501049 du 19 avril 2016, le tribunal administratif d'Orléans a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16NT01977 du 12 février 2018, la cour administrative d'appel de Nantes a annulé ce jugement et la décision du maire de la Ferté-Saint-Aubin du 15 janvier 2015. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 avril et 12 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de la Ferté-Saint-Aubin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. et Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune de la Ferté-Saint-Aubin et à la SCP Le Bret-Desaché, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A...ont acquis, en 2010, une des dépendances du château de la Luzière, situé en zone N du plan local d'urbanisme de la Ferté-Saint-Aubin. Par un arrêté du 15 janvier 2015, le maire de cette commune a retiré le permis de construire qu'il avait tacitement accordé le 24 novembre 2014 pour l'aménagement de ce bâtiment en maison d'habitation, la modification extérieure du pignon sud et la réduction des dimensions de la piscine et rejeté la demande de permis de construire déposée par M. et MmeA..., aux motifs que le bâtiment concerné, situé en zone N, n'était pas désigné par le règlement du plan local d'urbanisme comme pouvant faire l'objet d'un changement de destination et que le projet était de nature à porter atteinte à la sécurité publique en l'absence de moyens de défense extérieure contre l'incendie. Par un jugement du 19 avril 2016, le tribunal administratif d'Orléans a rejeté la requête à fin d'annulation de cette décision dont il avait été saisi par M. et MmeA.... La commune de la Ferté-Saint-Aubin se pourvoit en cassation contre l'arrêt du 12 février 2018 par lequel la cour administrative d'appel de Nantes a annulé ce jugement et la décision du maire du 15 janvier 2015. <br/>
<br/>
              2. Aux termes de l'article L. 421-1 du code de l'urbanisme : " Les constructions, même ne comportant pas de fondations, doivent être précédées de la délivrance d'un permis de construire. / Un décret en Conseil d'Etat arrête la liste des travaux exécutés sur des constructions existantes ainsi que des changements de destination qui, en raison de leur nature ou de leur localisation, doivent également être précédés de la délivrance d'un tel permis ". Aux termes de l'article R. 421-14 du même code, dans sa rédaction applicable au litige : " Sont soumis à permis de construire les travaux suivants, exécutés sur des constructions existantes, à l'exception des travaux d'entretien ou de réparations ordinaires : / (...) c) Les travaux ayant pour effet de modifier les structures porteuses ou la façade du bâtiment, lorsque ces travaux s'accompagnent d'un changement de destination entre les différentes destinations et sous-destinations définies à l'article R. 123-9 ; / (...) Pour l'application du c du présent article, les locaux accessoires d'un bâtiment sont réputés avoir la même destination que le local principal ". Aux termes de l'article R. 123-9 du même code, dans sa rédaction applicable au litige : " (...) Les règles édictées dans le présent article peuvent être différentes, dans une même zone, selon que les constructions sont destinées à l'habitation, à l'hébergement hôtelier, aux bureaux, au commerce, à l'artisanat, à l'industrie, à l'exploitation agricole ou forestière ou à la fonction d'entrepôt. En outre, des règles particulières peuvent être applicables aux constructions et installations nécessaires aux services publics ou d'intérêt collectif. (...) ".<br/>
<br/>
              3. Pour juger que les travaux de rénovation prévus par M. et Mme A...sur le bien qu'ils avaient acquis en 2010 par un acte notarié mentionnant une " maison à usage d'habitation " n'emportaient pas un changement de destination au sens des dispositions de l'article R. 421-14 du code de l'urbanisme cité au point précédent, la cour a relevé que la construction avait été édifiée en tant que bâtiment annexe du château de la Luzière sans que la commune ne démontre, ni même n'allègue, que l'immeuble aurait, malgré son utilisation à certaines périodes comme un local d'infirmerie attenant à un centre de vacances, perdu les caractéristiques qui, initialement, le destinaient à l'habitation. En recherchant ainsi si le bâtiment litigieux avait fait l'objet de transformations affectant sa destination initiale, sans s'en tenir à la seule circonstance que l'ensemble architectural " château de la Luziere " aurait été entre 1931 et 1995 affecté exclusivement à l'accueil de colonies de vacances, la cour n'a pas commis d'erreur de droit. Elle a, par ailleurs, porté une appréciation souveraine sur les pièces du dossier, exempte de dénaturation, en estimant que le bien litigieux avait été édifié en tant que dépendance du château de la Luzière à destination initiale d'habitation, non remise en cause par des travaux ou aménagements postérieurs.  <br/>
<br/>
              4. Après avoir jugé que les travaux envisagés par M et Mme A...sur leur bien n'emportaient pas changement de destination, la cour en a déduit que le maire de la Ferté-Saint-Aubin n'avait pu légalement leur appliquer le régime du permis de construire. Dans ces conditions, et alors que ceux des travaux soumis à déclaration préalable sur le fondement de l'article R. 421-17 du code de l'urbanisme n'avaient pas la même consistance, ni le second motif ayant justifié le refus de permis de construire opposé aux époux A...ni le motif dont la commune demandait qu'il y soit substitué ne pouvaient avoir d'incidence sur la légalité de la décision attaquée. Par suite, la commune n'est pas fondée à soutenir que la cour aurait insuffisamment motivé son arrêt et méconnu son office en ne se prononçant pas sur ces motifs, alors même qu'elle écartait implicitement, en application des dispositions de l'article L. 600-4-1 du code de l'urbanisme, les moyens autres que celui qu'elle retenait pour annuler la décision attaquée.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la commune de la Ferté-Saint-Aubin n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de la Ferté-Saint-Aubin une somme de 2 400 euros à verser à M. et Mme A...au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise au même titre à la charge M. et MmeA..., qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de la Ferté-Saint-Aubin est rejeté. <br/>
Article 2 : La commune de la Ferté-Saint-Aubin versera à M. et Mme A...une somme de 2 400 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la commune de la Ferté-Saint-Aubin et à M. et Mme B...et ChristèleA.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
