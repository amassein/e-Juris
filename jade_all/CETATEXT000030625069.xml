<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625069</ID>
<ANCIEN_ID>JG_L_2015_05_000000377001</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 22/05/2015, 377001</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377001</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP CAPRON</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:377001.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sbart a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir la décision du 7 février 2011 par laquelle l'inspecteur du travail de la 9ème section d'Ille-et-Vilaine a déclaré M. A...B...inapte à son poste de cuisinier. Par un jugement n° 1101325 du 13 décembre 2012, le tribunal administratif de Rennes a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13NT00471 du 30 janvier 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par la société Sbart.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er avril et 2 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la société Sbart demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat et de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Sbart et à la SCP Capron, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., employé par la société Sbart, a été victime d'un accident du travail le 9 juillet 2009 ; qu'à la suite des examens de reprise de travail, le médecin du travail a rendu un avis déclarant M. B... inapte à son poste de cuisinier mais apte à tout poste ne comportant pas de manutention ni de station assise ou debout prolongée ; que sur recours formé devant l'inspection du travail par la société Sbart, l'inspecteur du travail de la 9ème section d'Ille-et-Vilaine a décidé le 7 février 2011, après enquête contradictoire et avis du médecin inspecteur régional du travail, que M. B...était " inapte à son poste de cuisinier " ; que par un jugement du 13 décembre 2012, le tribunal administratif de Rennes a rejeté la demande de la société Sbart tendant à l'annulation de cette décision de l'inspecteur du travail ; que la société Sbart se pourvoit en cassation contre l'arrêt du 30 janvier 2014 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4624-1 du code du travail : " Le médecin du travail est habilité à proposer des mesures individuelles telles que mutations ou transformations de postes, justifiées par des considérations relatives notamment à l'âge, à la résistance physique ou à l'état de santé physique et mentale des travailleurs. L'employeur est tenu de prendre en considération ces propositions et, en cas de refus, de faire connaître les motifs qui s'opposent à ce qu'il y soit donné suite. En cas de difficulté ou de désaccord, l'employeur ou le salarié peut exercer un recours devant l'inspecteur du travail. Ce dernier prend sa décision après avis du médecin inspecteur du travail " ; qu'aux termes de l'article R. 4624-31 du même code : " Le médecin du travail ne peut constater l'inaptitude médicale du salarié à son poste de travail que s'il a réalisé : / 1° Une étude de ce poste / 2° Une étude des conditions de travail dans l'entreprise / 3° Deux examens médicaux de l'intéressé espacés de deux semaines, accompagnés, le cas échéant, des examens complémentaires (...) " : <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'en cas de difficulté ou de désaccord sur les propositions formulées par le médecin du travail concernant l'aptitude d'un salarié à occuper son poste de travail, il appartient à l'inspecteur du travail, saisi par l'une des parties, de se prononcer définitivement sur cette aptitude ; que son appréciation, qu'elle soit confirmative ou infirmative de l'avis du médecin du travail, se substitue à cet avis ; que seule la décision rendue par l'inspecteur du travail est susceptible de faire l'objet d'un recours devant le juge de l'excès de pouvoir ; que, par suite, en jugeant, par un arrêt suffisamment motivé sur ce point, que les irrégularités dont étaient le cas échéant entachée, au regard des dispositions de l'article R. 4624-31 du code du travail cité ci-dessus, la procédure au terme de laquelle le médecin du travail avait donné son avis, étaient sans incidence sur la légalité de la décision rendue par l'inspecteur du travail, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'en estimant qu'il ressortait des pièces du dossier qui lui était soumis que l'inspecteur du travail n'avait pas entaché sa décision d'erreur manifeste d'appréciation en concluant à l'inaptitude professionnelle de M.B..., la cour administrative d'appel a porté sur les faits de l'espèce une appréciation souveraine qui n'est pas entachée de dénaturation ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Sbart contre l'arrêt de la cour administrative d'appel de Nantes doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Sbart est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Sbart et à M. A...B....<br/>
Copie en sera adressée, pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - PROPOSITIONS DU MÉDECIN DU TRAVAIL EN CAS D'INAPTITUDE D'UN SALARIÉ À TENIR SON POSTE - SAISINE DE L'INSPECTEUR DU TRAVAIL EN CAS DE DÉSACCORD - RECOURS PRÉALABLE OBLIGATOIRE AVANT LA SAISINE DU JUGE - EXISTENCE - CONSÉQUENCE - INOPÉRANCE DES MOYENS CRITIQUANT LA DÉCISION DU MÉDECIN DU TRAVAIL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-01-01-02 TRAVAIL ET EMPLOI. INSTITUTIONS DU TRAVAIL. ADMINISTRATION DU TRAVAIL. INSPECTION DU TRAVAIL. - PROPOSITIONS DU MÉDECIN DU TRAVAIL EN CAS D'INAPTITUDE D'UN SALARIÉ À TENIR SON POSTE - SAISINE DE L'INSPECTEUR DU TRAVAIL EN CAS DE DÉSACCORD - RECOURS PRÉALABLE OBLIGATOIRE AVANT LA SAISINE DU JUGE - EXISTENCE - CONSÉQUENCE - INOPÉRANCE DES MOYENS CRITIQUANT LA DÉCISION DU MÉDECIN DU TRAVAIL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-03-04 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. MÉDECINE DU TRAVAIL. - PROPOSITIONS DU MÉDECIN DU TRAVAIL EN CAS D'INAPTITUDE D'UN SALARIÉ À TENIR SON POSTE - SAISINE DE L'INSPECTEUR DU TRAVAIL EN CAS DE DÉSACCORD - RECOURS PRÉALABLE OBLIGATOIRE AVANT LA SAISINE DU JUGE - EXISTENCE - CONSÉQUENCE - INOPÉRANCE DES MOYENS CRITIQUANT LA DÉCISION DU MÉDECIN DU TRAVAIL.
</SCT>
<ANA ID="9A"> 54-01-02-01 Il résulte des dispositions des articles L. 4624-1 et R. 4624-31 du code du travail qu'en cas de difficulté ou de désaccord sur les propositions formulées par le médecin du travail concernant l'aptitude d'un salarié à occuper son poste de travail, il appartient à l'inspecteur du travail, saisi par l'une des parties, de se prononcer définitivement sur cette aptitude [RJ1]. Son appréciation, qu'elle soit confirmative ou infirmative de l'avis du médecin du travail, se substitue à cet avis et seule la décision rendue par l'inspecteur du travail est susceptible de faire l'objet d'un recours devant le juge de l'excès de pouvoir. Les moyens critiquant la procédure au terme de laquelle le médecin du travail avait donné son avis sont donc sans incidence sur la légalité de la décision rendue par l'inspecteur du travail.</ANA>
<ANA ID="9B"> 66-01-01-02 Il résulte des dispositions des articles L. 4624-1 et R. 4624-31 du code du travail qu'en cas de difficulté ou de désaccord sur les propositions formulées par le médecin du travail concernant l'aptitude d'un salarié à occuper son poste de travail, il appartient à l'inspecteur du travail, saisi par l'une des parties, de se prononcer définitivement sur cette aptitude. Son appréciation, qu'elle soit confirmative ou infirmative de l'avis du médecin du travail, se substitue à cet avis et seule la décision rendue par l'inspecteur du travail est susceptible de faire l'objet d'un recours devant le juge de l'excès de pouvoir. Les moyens critiquant la procédure au terme de laquelle le médecin du travail avait donné son avis sont donc sans incidence sur la légalité de la décision rendue par l'inspecteur du travail.</ANA>
<ANA ID="9C"> 66-03-04 Il résulte des dispositions des articles L. 4624-1 et R. 4624-31 du code du travail qu'en cas de difficulté ou de désaccord sur les propositions formulées par le médecin du travail concernant l'aptitude d'un salarié à occuper son poste de travail, il appartient à l'inspecteur du travail, saisi par l'une des parties, de se prononcer définitivement sur cette aptitude. Son appréciation, qu'elle soit confirmative ou infirmative de l'avis du médecin du travail, se substitue à cet avis et seule la décision rendue par l'inspecteur du travail est susceptible de faire l'objet d'un recours devant le juge de l'excès de pouvoir. Les moyens critiquant la procédure au terme de laquelle le médecin du travail avait donné son avis sont donc sans incidence sur la légalité de la décision rendue par l'inspecteur du travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 27 septembre 2006, SNC Pneu Laurent, n° 275845, p. 422 ; CE, 16 avril 2010, Sté Presta Cuir Color, n° 326553, p. 125.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
