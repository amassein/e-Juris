<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434191</ID>
<ANCIEN_ID>JG_L_2020_10_000000424775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/41/CETATEXT000042434191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/10/2020, 424775, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP DELVOLVE ET TRICHET ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:424775.20201016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... et Mme D... ont demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir les arrêtés du maire de Thoiry des 17 novembre 2015, 29 novembre 2016 et 9 janvier 2018 délivrant, d'une part, à la société civile de construction-vente HPL Allemogne, un permis de construire quinze logements locatifs conventionnés, et d'autre part, à la société civile de construction-vente HPL Allemognes, un permis de construire et un permis modificatif portant sur la même parcelle et le même projet. Par un jugement nos 1600241, 1704367, 1801134 du 26 juin 2018, le tribunal administratif de Lyon a rejeté leurs demandes.<br/>
<br/>
              Par une ordonnance n° 18LY03200 du 5 octobre 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi enregistré le 12 novembre 2018 au greffe de cette cour, présenté par M. et Mme C.... <br/>
<br/>
              Par ce pourvoi, un mémoire complémentaire et deux mémoires en réplique enregistrés les 9 janvier et 6 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Lyon du 26 juin 2018 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Thoiry, de la société HPL Allemogne et de la société HPL Allemognes la somme de 4 500 euros au titre de l'article L. 761 -1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et Mme C..., à la SCP Delvolvé et Trichet, avocat de la commune de Thoiry et à la SCP Delamarre, Jéhannin, avocat de la société SCCV HPL Allemogne et autre ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire de Thoiry a d'abord délivré, le 17 novembre 2015, à la société civile de construction-vente HPL Allemogne, un permis de construire quinze logements locatifs conventionnés sur la parcelle voisine du terrain dont M. et Mme C... sont propriétaires. Il a ensuite délivré, le 29 novembre 2016, à la société civile de construction-vente HPL Allemognes, un permis de construire quinze logements locatifs conventionnés sur la même parcelle, puis, le 9 janvier 2018, un permis modificatif. M. et Mme C... ont demandé au tribunal administratif de Lyon l'annulation du permis de construire délivré le 17 novembre 2015 à la société HPL Allemogne, ainsi que du permis de construire et du permis modificatif délivrés les 29 novembre 2016 et 9 janvier 2018 à la société HPL Allemognes. Ils se pourvoient en cassation contre le jugement par lequel ce tribunal a rejeté leur requête. <br/>
<br/>
              Sur le jugement en ce qu'il rejette les conclusions dirigées contre le permis de construire délivré le 29 novembre 2016 à la société HPL Allemognes :<br/>
<br/>
              2. Lorsque le juge de l'excès de pouvoir est saisi par un tiers d'une décision d'autorisation qui est, en cours d'instance, soit remplacée par une décision de portée identique, soit modifiée dans des conditions qui n'en altèrent pas l'économie générale, le délai ouvert au requérant pour contester le nouvel acte ne commence à courir qu'à compter de la notification qui lui est faite de cet acte.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que le permis de construire, délivré le 29 novembre 2016, à la société HPL Allemognes, porte sur un projet de construction de quinze logements locatifs conventionnés pour une surface de plancher autorisée de 1325,35 m², qui est conçu par le même architecte et concerne la même parcelle que le permis délivré le 17 novembre 2015 à la société HPL Allemogne, qui est domiciliée à la même adresse et représentée par la même personne, et est comparable à celui-ci. Dans ces conditions, ce nouveau permis de construire doit être regardé comme remplaçant, implicitement mais nécessairement, le permis précédent ou comme modifiant, dans des conditions qui n'en altèrent pas l'économie générale, le permis délivré le 17 novembre 2015 à la société HPL Allemogne. <br/>
<br/>
              4. Il en résulte qu'en jugeant que ni la commune, ni la société bénéficiaire du permis de construire du 29 novembre 2016 n'étaient tenues de notifier ce permis à M. et Mme C..., dans le cadre d'une instance engagée à l'encontre du permis du 17 novembre 2015, délivré à une société différente même si de très fortes analogies existaient dans les projets autorisés et les dénominations des sociétés qui les portaient et en en déduisant que la requête de M. et Mme C... était tardive faute d'avoir été contestée dans le délai de deux mois à compter du premier jour d'affichage du permis sur le terrain, le tribunal administratif a entaché son jugement d'erreur de droit.<br/>
<br/>
              Sur le jugement en ce qu'il rejette les conclusions dirigées contre le permis délivré le 17 novembre 2015 à la société HPL Allemogne :<br/>
<br/>
              5. Les conclusions tendant à l'annulation d'un permis de construire qui a été retiré par un second permis ne deviennent sans objet du fait de ce nouveau permis qu'à la condition que le retrait qu'il a opéré ait acquis, à la date à laquelle le juge qui en est saisi se prononce, un caractère définitif. Tel n'est pas le cas lorsque le nouveau permis de construire a fait l'objet d'un recours en annulation, quand bien même aucune conclusion expresse n'aurait été dirigée contre le retrait qu'il opère. En l'espèce, il résulte de ce qui a été dit plus haut que le permis délivré le 29 novembre 2016 a fait l'objet d'une requête en annulation, dont le rejet pour tardiveté par le tribunal administratif de Lyon est, en conséquence de ce qui a été dit au point 4, entaché d'erreur de droit. Par suite, les conclusions à l'encontre du permis délivré le 17 novembre 2015 ne sont pas devenues sans objet.<br/>
<br/>
              6. En premier lieu, aux termes de l'article U3 du règlement du PLU de la commune de Thoiry, " (...) tout terrain enclavé est inconstructible à moins que son propriétaire ne produise une servitude de passage suffisante. / Les occupations et utilisations du sol peuvent être refusées sur des terrains qui ne seraient pas desservis par des voies publiques ou privées dans des conditions répondant à l'importance ou à la destination de l'immeuble ou de l'ensemble d'immeubles envisagé, et notamment si les caractéristiques de ces voies rendent difficile la circulation ou l'utilisation des engins de lutte contre l'incendie, des engins de déneigement et des engins d'enlèvement d'ordures ménagères (...) ". <br/>
<br/>
              7. Le permis, qui est délivré sous réserve des droits des tiers, a pour seul objet d'assurer la conformité des travaux qu'il autorise à la réglementation d'urbanisme. Dès lors, si l'administration et le juge administratif doivent, pour l'application des règles d'urbanisme relatives à la desserte et à l'accès des engins d'incendie et de secours, s'assurer de l'existence d'une desserte suffisante de la parcelle par une voie ouverte à la circulation publique et, le cas échéant, de l'existence d'un titre créant une servitude de passage donnant accès à cette voie, il ne leur appartient de vérifier ni la validité de cette servitude ni l'existence d'un titre permettant l'utilisation de la voie qu'elle dessert, si elle est privée, dès lors que celle-ci est ouverte à la circulation publique. <br/>
<br/>
              8. En jugeant que la parcelle du projet était suffisamment desservie au regard des exigences de l'article U3 du règlement d'urbanisme précité, après avoir relevé que cette desserte était assurée par une servitude de passage, dont il ne ressortait pas des pièces du dossier qui lui était soumis qu'elle aurait vocation à devenir ouverte à la circulation publique mais qui n'apparaissait pas inadaptée à l'opération projetée et qui donnait accès à une voie ouverte à la circulation publique, le tribunal administratif n'a pas commis d'erreur de droit ni entaché son jugement de contradiction de motifs. <br/>
<br/>
              9. En second lieu, aux termes de l'article U9 du règlement du PLU de la commune de Thoiry : " (...) Dans les périmètres identifiés au titre de l'article L. 123-1, 7° du code de l'urbanisme : / - le coefficient d'emprise au sol des constructions ne doit pas dépasser 0,50 (...) " ; aux termes de l'article R. 420-1 du code de l'urbanisme : " L'emprise au sol (...) est la projection verticale du volume de la construction, tous débords et surplombs inclus ".<br/>
<br/>
              10. En l'absence de prescriptions particulières dans le document d'urbanisme précisant la portée de la notion d'emprise au sol, celle-ci s'entend, en principe, comme la projection verticale du volume de la construction, tous débords inclus. Il en résulte que le tribunal administratif n'a pas commis d'erreur de droit en jugeant, après avoir relevé qu'il ressortait des pièces du dossier qui lui était soumis, que le bassin de rétention des eaux pluviales, prévu par le projet, était destiné à être enterré, qu'aucune emprise au sol ne lui correspondait.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. et Mme C... ne sont fondés à demander l'annulation du jugement attaqué qu'en tant qu'il juge tardive leur requête contre le permis de construire délivré le 29 novembre 2016.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Thoiry, de la société HPL Allemogne et de la société HPL Allemognes, la somme de 1 000 euros chacune à verser à M. et Mme C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme C..., qui ne sont pas, dans la présente instance, la partie perdante.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 26 juin 2018 est annulé en tant qu'il rejette les conclusions relatives au permis de construire du 29 novembre 2016. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Lyon.<br/>
Article 3 : : La commune de Thoiry, la SCCV HPL Allemogne et la SCCV HPL Allemognes verseront chacune la somme de 1 000 euros à M. et Mme C... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : Les conclusions présentées par la commune de Thoiry, la SCCV HPL Allemogne et la SCCV HPL Allemognes au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. E... et Mme D..., à la commune de Thoiry, à la SCCV HPL Allemogne et à la SCCV HPL Allemognes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
