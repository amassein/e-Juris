<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006633</ID>
<ANCIEN_ID>JG_L_2007_06_000000300208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/66/CETATEXT000018006633.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/06/2007, 300208</TITRE>
<DATE_DEC>2007-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin Laprade</PRESIDENT>
<AVOCATS>SCP CHOUCROY, GADIOU, CHEVALLIER ; SCP BARADUC, DUHAMEL ; SCP PARMENTIER, DIDIER</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 28 décembre 2006 et 11 janvier 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Charles C, demeurant ... ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 15 décembre 2006 par laquelle le juge des référés du tribunal administratif de Grenoble a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 13 septembre 2006 du maire de la commune de Châteauneuf-du-Rhône délivrant un permis de construire à M. David A et Mme Valérie B en vue de l'extension et de l'aménagement de leur maison d'habitation sise dans cette commune ;<br/>
<br/>
              2°) statuant en référé, d'ordonner la suspension de l'exécution de cet arrêté ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Châteauneuf-du-Rhône, de M. A et de Mme B, le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, Auditeur,  <br/>
<br/>
              - les observations de la SCP Parmentier, Didier, avocat M. C et de la SCP Choucroy, Gadiou, Chevallier, avocat de la commune de Châteauneuf-du-Rhône et autres, <br/>
<br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L 521-1 du code de justice administrative : « Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision » ;<br/>
<br/>
              Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le demandeur, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              Considérant que si, en règle générale, l'urgence s'apprécie compte tenu des justifications fournies par le demandeur quant au caractère suffisamment grave et immédiat de l'atteinte que porterait un acte administratif à sa situation ou aux intérêts qu'il entend défendre, il en va différemment de la demande de suspension d'un permis de construire pour laquelle, eu égard au caractère difficilement réversible de la construction d'un bâtiment, la condition d'urgence doit en principe être constatée lorsque les travaux vont commencer ou ont déjà commencé sans être pour autant achevés ; qu'il ne peut en aller autrement que dans le cas où le pétitionnaire ou l'autorité qui a délivré le permis justifient de circonstances particulières, tenant, notamment, à l'intérêt s'attachant à ce que la construction soit édifiée sans délai ; <br/>
<br/>
              Considérant  que, pour estimer que la condition d'urgence n'était pas remplie et refuser  en conséquence de suspendre l'exécution de l'arrêté du 13 septembre 2006 autorisant M. A et Mme B à réaliser des travaux d'extension et d'aménagement de leur maison d'habitation, le juge des référés du tribunal administratif de Grenoble s'est fondé sur ce que, d'une part, M. C n'aurait pas de vue directe sur la construction projetée et, d'autre part, il n'établirait pas que le projet autorisé porterait une atteinte suffisamment grave et immédiate à sa situation ; que l'un et l'autre de ces deux motifs sont entachés d'erreur de droit, dès lors que le premier procède d'une appréciation erronée de l'intérêt pour agir du requérant, et que le second, en faisant peser sur lui la charge d'établir l'urgence à suspendre l'exécution du permis litigieux, procède d'une erreur sur la charge de la preuve en cette matière, où, ainsi qu'il a été dit, cette urgence est présumée à défaut d'éléments contraires ; que l'ordonnance attaquée doit, pour ce motif, être annulée ;<br/>
<br/>
              Considérant qu'il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner la fin de non-recevoir soulevée par la commune de Châteauneuf-du-Rhône et par M. Oziol et Mme Bremond :<br/>
<br/>
              Considérant qu'à l'appui de sa demande de suspension de l'exécution du permis de construire contesté, M. C soutient que celui-ci méconnaît les dispositions des articles L. 421-3 et R. 421-2 du code de l'urbanisme et de l'article N 13 du plan local d'urbanisme de la commune de Châteauneuf-du-Rhône ; que le plan local d'urbanisme de la commune, sur le fondement duquel est intervenu l'arrêté litigieux, est entaché d'erreur manifeste d'appréciation, d'erreur de droit et de contrariété avec les principes du rapport de présentation en ce qu'il classe les parcelles litigieuses dans une « micro-zone » N, et non en zone agricole ; que l'arrêté attaqué méconnaît l'autorité de la chose jugée qui s'attache au jugement du 23 novembre 2005 du tribunal administratif de Grenoble annulant un permis de construire portant sur une parcelle voisine de celle de M. A et de Mme B ; que cet arrêté a été pris en méconnaissance des règles d'urbanisme applicables sous l'ancien plan d'occupation des sols de la commune ;<br/>
<br/>
              Considérant qu'aucun de ces moyens n'est de nature à faire naître, en l'état de l'instruction, un doute sérieux quant à la légalité du permis de construire contesté ; que, par suite, la demande présentée par M. C doit être rejetée, y compris, par voie de conséquence, les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Considérant en revanche qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C le versement à la commune de Châteauneuf-du-Rhône, d'une part, et à M et Mme , d'autre part, d'une somme de 1 000 euros chacun au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Grenoble en date du 15 décembre 2006 est annulée.<br/>
Article 2 : La demande présentée par M. C devant le tribunal administratif de Grenoble et le surplus de ses conclusions devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : M. C versera à la commune de Châteauneuf-du-Rhône et à M. A et Mme B une somme de 1 000 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Charles C, à la commune de Châteauneuf-du-Rhône, à M. David A, à Mme Valérie B et au ministre d'Etat, ministre de l'écologie, du développement et de l'aménagement durables.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - DEMANDE DE SUSPENSION D'UN PERMIS DE CONSTRUIRE - APPRÉCIATION DE LA CONDITION D'URGENCE - ERREURS DE DROIT - A) PRISE EN COMPTE D'ÉLÉMENTS RELATIFS À LA VUE DU REQUÉRANT SUR LA CONSTRUCTION PROJETÉE - B) MÉCONNAISSANCE DE LA PRÉSOMPTION D'URGENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURE D'URGENCE. RÉFÉRÉ. - RÉFÉRÉ-SUSPENSION (ART. L. 521-1 DU CJA) - DEMANDE DE SUSPENSION D'UN PERMIS DE CONSTRUIRE - APPRÉCIATION DE LA CONDITION D'URGENCE - ERREURS DE DROIT - A) PRISE EN COMPTE D'ÉLÉMENTS RELATIFS À LA VUE DU REQUÉRANT SUR LA CONSTRUCTION PROJETÉE - B) MÉCONNAISSANCE DE LA PRÉSOMPTION D'URGENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-035-02-03-02 Pour estimer que la condition d'urgence n'était pas remplie et refuser en conséquence de suspendre l'exécution d'un permis de construire, un juge des référés s'est fondé sur ce que, d'une part, la maison du requérant n'avait pas de vue directe sur la construction projetée et, d'autre part, celui-ci n'établissait pas que le projet autorisé porterait une atteinte suffisamment grave et immédiate à sa situation.,,a) Le premier de ces motifs procède d'une appréciation erronée de l'intérêt pour agir du requérant et est par suite entaché d'une erreur de droit.,,b) Le second, en faisant peser sur lui la charge d'établir l'urgence à suspendre l'exécution du permis litigieux, procède d'une erreur sur la charge de la preuve en cette matière, où cette urgence est présumée à défaut d'éléments contraires.</ANA>
<ANA ID="9B"> 68-06-02-01 Pour estimer que la condition d'urgence n'était pas remplie et refuser en conséquence de suspendre l'exécution d'un permis de construire, un juge des référés s'est fondé sur ce que, d'une part, la maison du requérant n'avait pas de vue directe sur la construction projetée et, d'autre part, celui-ci n'établissait pas que le projet autorisé porterait une atteinte suffisamment grave et immédiate à sa situation.,,a) Le premier de ces motifs procède d'une appréciation erronée de l'intérêt pour agir du requérant et est par suite entaché d'une erreur de droit.,,b) Le second, en faisant peser sur lui la charge d'établir l'urgence à suspendre l'exécution du permis litigieux, procède d'une erreur sur la charge de la preuve en cette matière, où cette urgence est présumée à défaut d'éléments contraires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. 27 juillet 2001, Commune de Meudon, n° 231991, T. p. 115.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
