<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445653</ID>
<ANCIEN_ID>JG_L_2015_03_000000373524</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 20/03/2015, 373524</TITRE>
<DATE_DEC>2015-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373524</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373524.20150320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Châlons-en-Champagne d'annuler la décision du 4 avril 2012 par laquelle le directeur du centre hospitalier de Chaumont a refusé de réviser sa notation pour l'année 2011, de procéder à une révision de ses notations depuis l'année 2002 et d'enjoindre au centre hospitalier de lui verser un complément de prime de service depuis 2002. Par un jugement n° 1200844 du 26 septembre 2013, le tribunal administratif a annulé la décision du 4 avril 2012 et rejeté le surplus des conclusions dont il était saisi.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 novembre 2013 et 26 février 2014 au secrétariat du contentieux, le centre hospitalier de Chaumont demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il annule la décision du 4 avril 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 86-33 du 9 janvier 1986, notamment son article 65-1 ;<br/>
<br/>
              - le décret n° 2010-1153 du 29 septembre 2010 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Brouchot, avocat du centre hospitalier de Chaumont ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article 17 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les notes et appréciations générales attribuées aux fonctionnaires et exprimant leur valeur professionnelle leur sont communiquées " ; qu'aux termes du premier alinéa de l'article 65 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière : " Le pouvoir de fixer les notes et appréciations générales exprimant la valeur professionnelle des fonctionnaires dans les conditions définies à l'article 17 du titre Ier du statut général est exercé par l'autorité investie du pouvoir de nomination, après avis du ou des supérieurs hiérarchiques directs " ; que les modalités de cette notation sont fixées par l'arrêté du 6 mai 1959 relatif à la notation du personnel des établissements d'hospitalisation, de soins et de cure publics ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article 65-1 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, dans sa rédaction issue de l'article 44 de la loi du 5 juillet 2010 relative à la rénovation du dialogue social et comportant diverses dispositions relatives à la fonction publique : " Au titre des années 2011, 2012 et 2013, les établissements mentionnés à l'article 2 de la présente loi peuvent être autorisés, à titre expérimental et par dérogation au premier alinéa des articles 17 du titre Ier du statut général et 65 de la présente loi, à se fonder sur un entretien professionnel pour apprécier la valeur professionnelle des fonctionnaires prise en compte pour l'application des articles 67, 68 et 69 " ; qu'aux termes de l'article 1er du décret du 29 septembre 2010 portant application de ces dernières dispositions : " Sur décision de l'autorité investie du pouvoir de nomination des établissements mentionnés à l'article 2 de la loi du 9 janvier 1986 susvisée, après avis du comité technique d'établissement, les fonctionnaires et les agents contractuels employés à durée indéterminée, à l'exception des personnels de direction et des directeurs des soins, peuvent faire l'objet, à titre expérimental, d'une évaluation ayant pour but d'apprécier leur valeur professionnelle dans les conditions fixées par le présent décret. / Dans ce cas, les dispositions de l'arrêté du 6 mai 1959 relatif à la notation des établissements d'hospitalisation, de soins et de cure publics cessent d'être applicables " ; <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions qu'au titre des années 2011, 2012 et 2013, les agents de la fonction publique hospitalière devaient faire l'objet d'une notation dans les conditions prévues par l'arrêté du 6 mai 1959, sauf si l'établissement auquel ils appartenaient avait adopté, sur décision expresse de l'autorité investie du pouvoir de nomination prise après avis du comité technique d'établissement et rendue exécutoire par l'accomplissement des mesures de publicité requises, le dispositif dérogatoire d'évaluation ouvert, à titre expérimental, par l'article 65-1 de la loi du 9 janvier 1986 ;  <br/>
<br/>
              4. Considérant que, pour annuler la décision du 4 avril 2012 du directeur du centre hospitalier de Chaumont refusant de réviser la notation de Mme B...au titre de l'année 2011, le tribunal administratif de Châlons-en-Champagne a jugé que l'administration avait méconnu le champ d'application de l'arrêté du 6 mai 1959 en mettant ses dispositions en oeuvre alors que l'établissement avait adopté le dispositif expérimental prévu par l'article 65-1 de la loi 9 janvier 1986 ; qu'en déduisant cette adoption de la seule circonstance que le centre hospitalier procédait à des entretiens individuels d'évaluation des agents, sans rechercher si son directeur avait pris, sur le fondement des dispositions précitées de l'article 1er du décret du 29 septembre 2010, une décision expresse rendue exécutoire par l'accomplissement des mesures de publicité requises, le tribunal a commis une erreur de droit ; que, par suite, son jugement doit être annulé en tant qu'il fait droit aux conclusions de Mme B...dirigées contre la décision du 4 avril 2012 ;<br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme demandée par le centre hospitalier de Chaumont au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du 26 septembre 2013 du tribunal administratif de Châlons-en-Champagne est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Châlons-en-Champagne dans la limite de la cassation prononcée.<br/>
<br/>
Article 3 : Les conclusions présentées par le centre hospitalier de Chaumont au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Chaumont et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-06-01 FONCTIONNAIRES ET AGENTS PUBLICS. NOTATION ET AVANCEMENT. NOTATION. - EVALUATION PAR VOIE D'ENTRETIEN PROFESSIONNEL, À TITRE DÉROGATOIRE, EN 2011, 2012, 2013 DANS LA FONCTION PUBLIQUE HOSPITALIÈRE - CONDITION - DÉCISION EXPRESSE DE L'AUTORITÉ INVESTIE DU POUVOIR DE NOMINATION DE RECOURIR À CETTE MÉTHODE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-04 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE HOSPITALIÈRE (LOI DU 9 JANVIER 1986). - EVALUATION PAR VOIE D'ENTRETIEN PROFESSIONNEL, À TITRE DÉROGATOIRE, EN 2011, 2012, 2013 - CONDITION - DÉCISION EXPRESSE DE L'AUTORITÉ INVESTIE DU POUVOIR DE NOMINATION DE RECOURIR À CETTE MÉTHODE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - EVALUATION PAR VOIE D'ENTRETIEN PROFESSIONNEL, À TITRE DÉROGATOIRE, EN 2011, 2012, 2013 - CONDITION - DÉCISION EXPRESSE DE L'AUTORITÉ INVESTIE DU POUVOIR DE NOMINATION DE RECOURIR À CETTE MÉTHODE.
</SCT>
<ANA ID="9A"> 36-06-01 Il résulte des dispositions de l'article 65-1 de la loi n° 86-33 du 9 janvier 1986 et de l'article 1er du décret n° 2010-1153 du 29 septembre 2010 qu'au titre des années 2011, 2012 et 2013, les agents de la fonction publique hospitalière devaient faire l'objet d'une notation dans les conditions prévues par l'arrêté du 6 mai 1959, sauf si l'établissement auquel ils appartenaient avait adopté, sur décision expresse de l'autorité investie du pouvoir de nomination prise après avis du comité technique d'établissement et rendue exécutoire par l'accomplissement des mesures de publicité requises, le dispositif dérogatoire d'évaluation ouvert, à titre expérimental, par l'article 65-1 de la loi du 9 janvier 1986.</ANA>
<ANA ID="9B"> 36-07-01-04 Il résulte des dispositions de l'article 65-1 de la loi n° 86-33 du 9 janvier 1986 et de l'article 1er du décret n° 2010-1153 du 29 septembre 2010 qu'au titre des années 2011, 2012 et 2013, les agents de la fonction publique hospitalière devaient faire l'objet d'une notation dans les conditions prévues par l'arrêté du 6 mai 1959, sauf si l'établissement auquel ils appartenaient avait adopté, sur décision expresse de l'autorité investie du pouvoir de nomination prise après avis du comité technique d'établissement et rendue exécutoire par l'accomplissement des mesures de publicité requises, le dispositif dérogatoire d'évaluation ouvert, à titre expérimental, par l'article 65-1 de la loi du 9 janvier 1986.</ANA>
<ANA ID="9C"> 36-11 Il résulte des dispositions de l'article 65-1 de la loi n° 86-33 du 9 janvier 1986 et de l'article 1er du décret n° 2010-1153 du 29 septembre 2010 qu'au titre des années 2011, 2012 et 2013, les agents de la fonction publique hospitalière devaient faire l'objet d'une notation dans les conditions prévues par l'arrêté du 6 mai 1959, sauf si l'établissement auquel ils appartenaient avait adopté, sur décision expresse de l'autorité investie du pouvoir de nomination prise après avis du comité technique d'établissement et rendue exécutoire par l'accomplissement des mesures de publicité requises, le dispositif dérogatoire d'évaluation ouvert, à titre expérimental, par l'article 65-1 de la loi du 9 janvier 1986.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
