<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029604146</ID>
<ANCIEN_ID>JG_L_2014_10_000000365840</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/41/CETATEXT000029604146.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 17/10/2014, 365840, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365840</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365840.20141017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 février et 7 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération nationale de l'entretien des textiles, dont le siège est 11, rue Pierre Ricard, à Paris (75018), représentée par son président ; la fédération requérante demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêté du ministre de l'écologie, du développement durable et de l'énergie du 5 décembre 2012 modifiant l'arrêté du 31 août 2009 relatif aux prescriptions générales applicables aux installations classées pour la protection de l'environnement soumises à déclaration sous la rubrique n° 2345 relative à l'utilisation de solvants pour le nettoyage à sec et le traitement des textiles ou des vêtements ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens, y compris le timbre fiscal acquitté au titre de la contribution pour l'aide juridique ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de la Fédération nationale de l'entretien des textiles ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 512-10 du code de l'environnement : " Pour la protection des intérêts mentionnés à l'article L. 511-1, le ministre chargé des installations classées peut fixer par arrêté, après consultation des ministres intéressés et du Conseil supérieur de la prévention des risques technologiques, les prescriptions générales applicables à certaines catégories d'installations soumises à déclaration. / Ces arrêtés s'imposent de plein droit aux installations nouvelles. / Ils précisent, après avis des organisations professionnelles intéressées, les délais et les conditions dans lesquels ils s'appliquent aux installations existantes. Ils précisent également les conditions dans lesquelles ces prescriptions peuvent être adaptées par arrêté préfectoral aux circonstances locales " ; que la Fédération nationale de l'entretien des textiles demande l'annulation de l'arrêté du ministre de l'écologie, du développement durable et de l'énergie du 5 décembre 2012 modifiant l'arrêté du 31 août 2009 relatif aux prescriptions générales applicables aux installations classées pour la protection de l'environnement soumises à déclaration sous la rubrique n° 2345 relative à l'utilisation de solvants pour le nettoyage à sec et le traitement des textiles ou des vêtements, pris sur le fondement de ces dispositions ;<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué : <br/>
<br/>
              2. Considérant que la fédération requérante soutient que l'arrêté attaqué méconnaît les dispositions citées ci-dessus faute d'avoir été précédé de la consultation de certains des ministres intéressés et du recueil de son propre avis, alors même qu'elle figure parmi les organisations professionnelles intéressées par l'arrêté attaqué ; que, toutefois, d'une part, il ressort des pièces du dossier que, contrairement à ce qui est soutenu, le ministre chargé de la santé et le ministre chargé du travail ont été consultés sur le projet d'arrêté ; que, d'autre part, il ressort des pièces du dossier que la fédération française des pressings et blanchisseries et le conseil fédéral de l'entretien des textile - pressings de France ont été consultés ; qu'au surplus, le centre technique de la teinture et du nettoyage - institut de recherche sur l'entretien et le nettoyage a également été consulté et le projet d'arrêté a fait l'objet d'une mise en ligne, pour consultation du public, sur le site internet du ministère de l'écologie, du développement durable et de l'énergie du 4 au 27 mai 2012 ; qu'ainsi, le ministre a satisfait aux obligations de consultation prévues par les dispositions de l'article L. 512-10 du code de l'environnement ; <br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 2.3.3. de l'annexe I de l'arrêté attaqué : " Les machines de nettoyage à sec utilisant du perchloroéthylène ou tout autre solvant dont la tension de vapeur à 20° C est supérieure ou égale à 1900 Pa, ne sont pas situées dans des locaux contigus à des locaux occupés par des tiers " ; que la fédération requérante soutient que cette mesure d'interdiction n'est pas proportionnée aux buts en vue desquels elle a été prise, dès lors, d'une part, que le Haut conseil de la santé publique a fixé, dans un avis du 16 juin 2010, une valeur repère de concentration de perchloroéthylène dans l'air des espaces clos dont le strict respect serait de nature à protéger contre les risques pour la santé et, d'autre part, qu'elle aurait pour effet d'imposer l'usage de solvants de substitution dont les risques sont soit mal connus, soit avérés ; <br/>
<br/>
              4. Considérant, toutefois, que les dispositions contestées tiennent compte des risques importants pour la santé publique que présente le perchloroéthylène, solvant dont le Haut conseil de la santé publique souligne, dans son avis du 16 juin 2010, qu'il est classé parmi les substances cancérogènes probables pour l'homme par le Centre international de recherche contre le cancer (groupe 2A) et cancérogène possible (catégorie 3) par l'Union européenne et qu'il est, en tout état de cause, à l'origine de troubles neurologiques, hépatiques et rénaux ; que, par ailleurs, l'interdiction de l'usage de ce solvant par des machines de nettoyage à sec est limitée aux seuls locaux contigus à des locaux occupés par des tiers, dans lesquels les campagnes de contrôle menées depuis 2002 ont démontré qu'une importante concentration de perchloroéthylène dans l'air était fréquemment constatée ; qu'en outre, son entrée en vigueur est, en vertu de l'annexe III de l'arrêté attaqué, échelonnée sur une période qui court du 1er mars 2013 au 1er janvier 2022, avec prise en compte de la date de mise en service de la machine de nettoyage à sec concernée ; qu'il ne ressort pas, enfin, des pièces du dossier que cette interdiction devrait avoir pour conséquence inévitable l'usage de substances présentant des risques importants pour la santé ; que, dans ces conditions, la prescription contestée n'est pas disproportionnée aux buts de santé publique en vue desquels elle a été prise ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la fédération requérante n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération nationale de l'entretien des textiles est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la Fédération nationale de l'entretien des textiles et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
