<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030642953</ID>
<ANCIEN_ID>JG_L_2015_05_000000380652</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/29/CETATEXT000030642953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 27/05/2015, 380652, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380652</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:380652.20150527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association France nature environnement, dont le siège est 10, rue Barbier au Mans (72000) ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-368 du 24 mars 2014 relatif à la transaction pénale en matière environnementale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la directive 2008/99/CE du Parlement européen et du Conseil du 19 novembre 2008 relative à la protection de l'environnement par le droit pénal ; <br/>
<br/>
              Vu la directive 2012/13/UE du Parlement européen et du Conseil du 22 mai 2012 relative au droit à l'information dans le cadre des procédures pénales ; <br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2014-416 QPC du 26 septembre 2014 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article L. 173-12 du code de l'environnement définit la procédure par laquelle, tant que l'action publique n'a pas été mise en mouvement, l'autorité administrative peut transiger avec les personnes physiques et les personnes morales sur la poursuite des délits et contraventions de cinquième classe prévus et réprimés par le code de l'environnement ; qu'en vertu de cet article, la proposition de transaction, qui doit être acceptée par l'auteur de l'infraction, précise l'amende transactionnelle que celui-ci devra payer, dont le montant ne peut excéder le tiers du montant de l'amende encourue, ainsi que, le cas échéant, les obligations qui lui seront imposées tendant à faire cesser l'infraction, à éviter son renouvellement, à réparer le dommage ou à remettre en conformité les lieux ; que ces dispositions prévoient que la transaction doit être homologuée par le procureur de la République ; que le décret attaqué, qui précise les modalités d'application de cet article, détermine l'autorité administrative habilitée à établir la proposition de transaction, fixe le contenu de celle-ci et définit les modalités de son homologation et de sa notification ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du 3° de l'article L. 213-1 du code de l'environnement, le comité national de l'eau a notamment pour mission de " donner son avis sur les projets de décret concernant la protection des peuplements piscicoles " ; que le décret attaqué, dont l'objet a été rappelé ci-dessus, ne concerne pas la protection des peuplements piscicoles ; qu'ainsi, l'avis du comité national de l'eau n'avait pas à être recueilli ; que le moyen d'irrégularité de la procédure tiré du défaut de consultation préalable de ce comité ne peut, par suite, qu'être écarté comme inopérant ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 1er de la directive du Parlement européen et du Conseil du 19 novembre 2008 relative à la protection de l'environnement par le droit pénal : " La présente directive établit des mesures en relation avec le droit pénal afin de protéger l'environnement de manière plus efficace. " ; que selon son article 5 : " Les États membres prennent les mesures nécessaires pour que les infractions visées aux articles 3 et 4 soient passibles de sanctions pénales effectives, proportionnées et dissuasives. " ; qu'aux termes de son article 7 : " Les États membres prennent les mesures nécessaires pour que les personnes morales tenues pour responsables d'une infraction en vertu de l'article 6 soient passibles de sanctions effectives, proportionnées et dissuasives. " ; qu'aux termes de son considérant 10 : " La présente directive fait obligation aux Etats membres de prévoir dans leur législation nationale des sanctions pénales pour les violations graves des dispositions du droit communautaire relatif à la protection de l'environnement. La présente directive ne crée pas d'obligation concernant l'application de telles sanctions ou de tout autre système de répression existant dans des cas particuliers " ; que, d'une part, le dispositif de transaction pénale institué par l'article L. 173-12 du code de l'environnement et dont les modalités d'application sont précisées par le décret attaqué n'a pas pour objet de définir les infractions ou les sanctions pénales dans le domaine de la protection de l'environnement, lesquelles relèvent d'autres dispositions ; que, d'autre part, les dispositions citées ci-dessus ne font pas obstacle à ce que les Etats membres prévoient des modes alternatifs de règlement des litiges dans le domaine de la protection de l'environnement ; qu'enfin,  la décision de recourir à la transaction pénale n'est qu'une faculté, en faveur de laquelle l'autorité administrative se détermine selon les circonstances de l'infraction, sa gravité, la personnalité de son auteur ainsi que ses ressources et ses charges ; que la conclusion d'une transaction est en outre subordonnée à l'accord du procureur de la République, qui doit l'homologuer ; que, par suite et contrairement à ce qui est soutenu, l'existence d'une procédure de transaction pénale en matière environnementale ne méconnaît pas les objectifs fixés aux Etats membres par les articles 5 et 7 de la directive du 19 novembre 2008 cités ci-dessus ; <br/>
<br/>
              4. Considérant, en dernier lieu, que, d'une part, aux termes de l'article 1er de la directive du Parlement européen et du Conseil du 22 mai 2012 relative au droit à l'information dans le cadre des procédures pénales : " La présente directive définit des règles concernant le droit des suspects ou des personnes poursuivies d'être informés de leurs droits dans le cadre des procédures pénales et de l'accusation portée contre eux. (...) " ; qu'aux termes de son article 2 : " 1. La présente directive s'applique dès le moment où des personnes sont informées par les autorités compétentes d'un État membre qu'elles sont soupçonnées d'avoir commis une infraction pénale ou qu'elles sont poursuivies à ce titre, et jusqu'au terme de la procédure, qui s'entend comme la détermination définitive de la question de savoir si le suspect ou la personne poursuivie a commis l'infraction pénale, y compris, le cas échéant, la condamnation et la décision rendue sur tout appel. (...) " ; que, d'autre part, aux termes de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement et dans un délai raisonnable, par un tribunal indépendant et impartial, établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle. (...). / (...) / 3. Tout accusé a droit notamment à : / a. être informé, dans le plus court délai, dans une langue qu'il comprend et d'une manière détaillée, de la nature et de la cause de l'accusation portée contre lui ; / b. disposer du temps et des facilités nécessaires à la préparation de sa défense ; / c. se défendre lui-même ou avoir l'assistance d'un défenseur de son choix et, s'il n'a pas les moyens de rémunérer un défenseur, pouvoir être assisté gratuitement par un avocat d'office, lorsque les intérêts de la justice l'exigent ; / d. interroger ou faire interroger les témoins à charge et obtenir la convocation et l'interrogation des témoins à décharge dans les mêmes conditions que les témoins à charge ; / e. se faire assister gratuitement d'un interprète, s'il ne comprend pas ou ne parle pas la langue employée à l'audience. " ; <br/>
<br/>
              5. Considérant, d'une part, que la procédure de transaction organisée par l'article L. 173-12 du code de l'environnement, et dont les modalités sont précisées par le décret attaqué, suppose l'accord libre et non équivoque, avec l'assistance éventuelle de son avocat, de l'auteur des faits ; qu'en outre la transaction homologuée ne présente, en elle-même, aucun caractère exécutoire et n'entraîne aucune privation ou restriction des droits de l'intéressé ; qu'elle doit être exécutée volontairement par ce dernier ; que ce n'est que dans le cas où l'auteur de l'infraction n'a pas exécuté, dans les délais impartis, l'intégralité des obligations résultant pour lui de l'acceptation de la transaction que l'action publique est susceptible d'être mise en mouvement ; <br/>
<br/>
              6. Considérant, d'autre part, que le dispositif de transaction pénale issu des dispositions litigieuses ne fait par lui-même nullement obstacle à ce que l'auteur d'infraction se fasse assister par un avocat ; qu'il ne fait pas non plus obstacle à ce que l'intéressé présente des observations sur la proposition de transaction qui lui est faite, dans le délai d'un mois qui lui est imparti par l'article R. 173-3 pour retourner, en cas d'accord, l'exemplaire de la proposition de transaction signé ; qu'enfin, dès lors que l'article R. 173-2 du code de l'environnement, dans sa rédaction issue du décret attaqué prévoit que " La proposition de transaction mentionne : / 1° La nature des faits reprochés et leur qualification juridique ; (...) ", l'intéressé est informé, d'une manière précise, de la nature des faits qui lui sont reprochés et de leur qualification juridique ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'eu égard à la nature d'alternative aux poursuites de la transaction pénale et à l'ensemble des garanties qui encadrent sa conclusion, les moyens tirés de la méconnaissance de la directive 2012/13/UE du 22 mai 2012 relative à l'information dans le cadre des procédures pénales ainsi que le paragraphe 3 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent qu'en tout état de cause être écartés ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que l'association France nature environnement n'est pas fondée à demander l'annulation du décret du 24 mars 2014 ; que sa requête doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association France nature environnement est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association France nature environnement, au Premier ministre, à la ministre de l'écologie, du développement durable et de l'énergie et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
