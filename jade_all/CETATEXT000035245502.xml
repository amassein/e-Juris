<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245502</ID>
<ANCIEN_ID>JG_L_2017_07_000000391402</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245502.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 19/07/2017, 391402</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391402</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391402.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Fort-de-France d'annuler pour excès de pouvoir la décision du 29 décembre 2010 par laquelle le ministre chargé du travail a autorisé la société l'Agence du bâtiment à la licencier. Par un jugement n° 1100204 du 31 janvier 2013, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 13BX00905 du 30 mars 2015, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société l'Agence du bâtiment contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 juin et 30 septembre 2015 et le 24 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société l'Agence du bâtiment demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'Agence du bâtiment et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 juillet 2017, présentée par le ministre du travail ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 29 décembre 2010, le ministre du travail, de l'emploi et de la santé a, d'une part, annulé la décision du 21 juin 2010 par laquelle l'inspecteur du travail a refusé à la société l'Agence du bâtiment l'autorisation de licencier Mme A...pour motif économique et, d'autre part, autorisé ce licenciement ; que, sur demande de MmeA..., le tribunal administratif de Fort-de-France a annulé cette décision du ministre par un jugement du 31 janvier 2013 ; que la société l'Agence du bâtiment se pourvoit en cassation contre l'arrêt du 30 mars 2015 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel formé contre ce jugement ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions des articles R. 2421-4 et R. 2421-11 du code du travail, l'inspecteur du travail saisi d'une demande d'autorisation de licenciement d'un salarié protégé doit, quel que soit le motif de la demande, procéder à une enquête contradictoire ; qu'en revanche, aucune règle ni aucun principe ne fait obligation au ministre chargé du travail, saisi d'un recours hiérarchique sur le fondement des dispositions de l'article R. 2422-1 du même code, de procéder lui-même à cette enquête contradictoire ; qu'il en va toutefois autrement si l'inspecteur du travail n'a pas lui-même respecté les obligations de l'enquête contradictoire et que, par suite, le ministre annule sa décision et statue lui-même sur la demande d'autorisation ;<br/>
<br/>
              3. Considérant, par ailleurs, qu'aux termes des dispositions de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, aujourd'hui codifiées aux articles L. 121-1, L. 122-1 et L. 211-2 du code des relations entre le public et l'administration : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. " ; qu'il résulte de ces dispositions qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans leur champ de mettre elle-même la personne intéressée en mesure de présenter des observations ; qu'il en va de même, à l'égard du bénéficiaire d'une décision, lorsque l'administration est saisie par un tiers d'un recours gracieux ou hiérarchique contre cette décision ; qu'ainsi, le ministre chargé du travail, saisi sur le fondement des dispositions de l'article R. 2422-1 du code du travail, d'un recours contre une décision autorisant ou refusant d'autoriser le licenciement d'un salarié protégé, doit mettre le tiers au profit duquel la décision contestée a créé des droits - à savoir, respectivement, l'employeur ou le salarié protégé - à même de présenter ses observations, notamment par la communication de l'ensemble des éléments sur lesquels le ministre entend fonder sa décision ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que le ministre chargé du travail, saisi d'un recours hiérarchique formé par la société l'Agence du bâtiment contre le refus d'autorisation qui lui avait été opposé par l'inspecteur du travail, était tenu, alors même qu'était en cause un licenciement pour motif économique, de mettre la salariée à même de présenter ses observations en lui communiquant l'ensemble des éléments sur lesquels il entendait fonder sa décision, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, ne l'a pas entaché d'erreur de droit ; <br/>
<br/>
              5. Considérant qu'en estimant que les données économiques relatives à l'exercice 2010 de la société l'Agence du bâtiment, jointes par cette société à son recours hiérarchique, étaient au nombre des éléments sur lesquels le ministre s'est fondé pour prendre sa décision, la cour administrative d'appel a porté sur les pièces du dossier une appréciation souveraine qui n'est pas entachée de dénaturation ; que, le droit de Mme A...de présenter ses observations sur un tel recours hiérarchique revêtant le caractère d'une garantie, la cour a pu, sans erreur de droit, en déduire que, faute qu'elle ait été mise à même de discuter utilement ces données économiques, la décision par laquelle le ministre du travail, de l'emploi et de la santé a autorisé son licenciement était entachée d'illégalité ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société l'Agence du bâtiment doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société l'Agence du bâtiment une somme de 3 500 euros à verser à Mme A...au titre des mêmes dispositions ;  <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société l'Agence du bâtiment est rejeté.<br/>
Article 2 : La société l'Agence du bâtiment versera une somme de 3 500 euros à Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société l'Agence du bâtiment et à Mme B... A.... <br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. MODALITÉS. - 1) ENQUÊTE CONTRADICTOIRE PRÉALABLE À LA DÉLIVRANCE D'UNE AUTORISATION ADMINISTRATIVE DE LICENCIEMENT - OBLIGATION DE PROCÉDER À UNE TELLE ENQUÊTE - EXISTENCE, QUEL QUE SOIT LE MOTIF DE LA DEMANDE - 2) OBLIGATION FAITE AU MINISTRE DE METTRE À MÊME L'EMPLOYEUR OU LE SALARIÉ AU PROFIT DUQUEL LA DÉCISION D'AUTORISATION OU DE REFUS DE LICENCIEMENT A CRÉÉ DES DROITS DE PRÉSENTER SES OBSERVATIONS EN CAS DE RECOURS HIÉRARCHIQUE - EXISTENCE - PORTÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-03-02-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. MODALITÉS D'INSTRUCTION DE LA DEMANDE. ENQUÊTE CONTRADICTOIRE. - OBLIGATION DE PROCÉDER À UNE TELLE ENQUÊTE - EXISTENCE, QUEL QUE SOIT LE MOTIF DE LA DEMANDE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07-01-03-04 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. RECOURS HIÉRARCHIQUE. - POUVOIRS ET DEVOIRS DU MINISTRE - 1) ENQUÊTE CONTRADICTOIRE - A) PRINCIPE - OBLIGATION D'Y PROCÉDER - ABSENCE - B) EXCEPTION - CAS OÙ, APRÈS AVOIR ANNULÉ LA DÉCISION DE L'INSPECTEUR DU TRAVAIL POUR NON RESPECT DES OBLIGATIONS DE L'ENQUÊTE CONTRADICTOIRE, IL STATUE LUI-MÊME SUR LA DEMANDE D'AUTORISATION - 2) OBLIGATION DE METTRE À MÊME L'EMPLOYEUR OU LE SALARIÉ AU PROFIT DUQUEL LA DÉCISION D'AUTORISATION OU DE REFUS DE LICENCIEMENT A CRÉÉ DES DROITS DE PRÉSENTER SES OBSERVATIONS EN CAS DE RECOURS HIÉRARCHIQUE - EXISTENCE - PORTÉE [RJ1].
</SCT>
<ANA ID="9A"> 01-03-03-03 1) En vertu des articles R. 2421-4 et R. 2421-11 du code du travail, l'inspecteur du travail saisi d'une demande d'autorisation de licenciement d'un salarié protégé doit, quel que soit le motif de la demande, procéder à une enquête contradictoire.... ,,2) Il résulte de l'article 24 de la loi n° 2000-321 du 12 avril 2000, aujourd'hui codifié aux articles L. 121-1, L. 122-1 et L. 211-2 du code des relations entre le public et l'administration (CRPA) qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans leur champ de mettre elle-même la personne intéressée en mesure de présenter des observations. Il en va de même, à l'égard du bénéficiaire d'une décision, lorsque l'administration est saisie par un tiers d'un recours gracieux ou hiérarchique contre cette décision. Ainsi, le ministre chargé du travail, saisi sur le fondement de l'article R. 2422-1 du code du travail d'un recours contre une décision autorisant ou refusant d'autoriser le licenciement d'un salarié protégé, doit mettre le tiers au profit duquel la décision contestée a créé des droits - à savoir, respectivement, l'employeur ou le salarié protégé - à même de présenter des observations, notamment par la communication de l'ensemble des éléments sur lesquels le ministre entend fonder sa décision.</ANA>
<ANA ID="9B"> 66-07-01-03-02-01 En vertu des articles R. 2421-4 et R. 2421-11 du code du travail, l'inspecteur du travail saisi d'une demande d'autorisation de licenciement d'un salarié protégé doit, quel que soit le motif de la demande, procéder à une enquête contradictoire.</ANA>
<ANA ID="9C"> 66-07-01-03-04 1) a) Aucune règle ni aucun principe ne fait obligation au ministre chargé du travail, saisi d'un recours hiérarchique sur le fondement des dispositions de l'article R. 2422-1 du code du travail, de procéder lui-même à une enquête contradictoire.... ,,b) Il en va toutefois autrement si l'inspecteur du travail n'a pas lui-même respecté les obligations de l'enquête contradictoire et que, par suite, le ministre annule sa décision et statue lui-même sur la demande d'autorisation.,,,2) Il résulte de l'article 24 de la loi n° 2000-321 du 12 avril 2000, aujourd'hui codifié aux articles L. 121-1, L. 122-1 et L. 211-2 du code des relations entre le public et l'administration (CRPA) qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans leur champ de mettre elle-même la personne intéressée en mesure de présenter des observations. Il en va de même, à l'égard du bénéficiaire d'une décision, lorsque l'administration est saisie par un tiers d'un recours gracieux ou hiérarchique contre cette décision. Ainsi, le ministre chargé du travail, saisi sur le fondement de l'article R. 2422-1 du code du travail d'un recours contre une décision autorisant ou refusant d'autoriser le licenciement d'un salarié protégé, doit mettre le tiers au profit duquel la décision contestée a créé des droits - à savoir, respectivement, l'employeur ou le salarié protégé - à même de présenter des observations, notamment par la communication de l'ensemble des éléments sur lesquels le ministre entend fonder sa décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la portée de l'enquête contradictoire dans l'hypothèse d'une demande d'autorisation de licencier un salarié protégé fondée sur un motif disciplinaire, décision du même jour, Société GSMC Innovation, anciennement dénommée Suventium, et autres, n° 389635, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
