<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026701761</ID>
<ANCIEN_ID>JG_L_2012_11_000000331224</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/70/17/CETATEXT000026701761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 28/11/2012, 331224, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331224</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Alain Ménéménis</PRESIDENT>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:331224.20121128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 août et 30 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mlle Céline B, demeurant ... ; Mlle B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06LY01348 du 25 juin 2009 de la cour administrative d'appel de Lyon, en tant qu'il n'a que partiellement fait droit aux conclusions de sa requête tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu mises à sa charge au titre de l'année 2000 et de contributions sociales mises à sa charge au titre des années 1999 et 2000 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de Mlle B,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de Mlle B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les sociétés civiles immobilières B, SCI 2P et SCI Céline, dont Mme Paule B, ses trois fils alors mineurs et sa fille, Mlle Céline B, détenaient la majorité des parts, et qui exerçaient au cours des années 1999 et 2000 une activité de sous-location nue d'immeubles industriels pris en crédit-bail immobilier, ont, le 12 mai 2000 pour les deux premières sociétés et le 19 octobre 2000 pour la dernière, levé les options d'achat de ces immeubles, qu'elles ont continué à donner en location aux mêmes entreprises ; que les plus-values professionnelles à court terme que ces opérations ont fait naître ont été placées en report d'imposition, en application du VI de l'article 93 quater du code général des impôts, jusqu'à la date de la transmission de l'immeuble ou des titres de la SCI propriétaire ; que le 23 décembre 2000, Mme Paule B et Mlle Céline B ont fait apport de la totalité de leurs parts à la SAS GP Immobilier, avec effet rétroactif au 1er janvier 2000, cette société étant ainsi au 31 décembre 2000, date de la clôture de l'exercice des trois SCI mentionnées ci-dessus, leur associée unique ; qu'au terme d'une vérification de comptabilité des trois SCI portant sur les exercices clos en 1999 et 2000, l'administration fiscale leur a notifié, ainsi qu'à Mlle Céline B, des redressements procédant, d'une part, de la taxation entre les mains des associés présents lors de la levée de chaque option d'achat des plus-values nées de ces levées d'option, dont le report d'imposition avait pris fin du fait de l'apport des parts à la société GP Immobilier, d'autre part, de la taxation immédiate entre les mains de leurs associés des bénéfices des SCI non encore imposés à la date des levées d'option, l'administration estimant que ces dernières entraînaient un changement d'activité et de régime fiscal au sens de l'article 202 ter du code général des impôts ; qu'après avoir vainement réclamé contre les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises à sa charge respectivement au titre de l'année 2000 et des années 1999 et 2000 ainsi que les pénalités correspondantes, Mlle Céline B a porté le litige devant le tribunal administratif de Dijon qui, par un jugement du 9 mai 2006, a rejeté sa demande ; qu'elle se pourvoit en cassation contre l'arrêt du 25 juin 2009 par lequel la cour administrative d'appel de Lyon n'a que partiellement fait droit à sa requête d'appel, et rejeté, par son article 4, ses conclusions tendant à la décharge des cotisations supplémentaires et pénalités relatives aux plus-values nées des levées d'option d'achat ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 93 quater du code général des impôts, dans sa rédaction applicable au litige : " I. Les plus-values réalisées sur des immobilisations sont soumises au régime des articles 39 duodecies à 39 quindecies. (...) III. Pour l'application des dispositions du premier alinéa du I, les contrats de crédit-bail conclus dans les conditions prévues aux 1° et 2° de l'article 1er de la loi n° 66-455 du 2 juillet 1966 relative aux entreprises pratiquant le crédit-bail sont considérés comme des immobilisations lorsque les loyers versés ont été déduits pour la détermination du bénéfice non commercial. / IV. 1. Pour l'application des dispositions du premier alinéa du I aux immeubles acquis dans les conditions prévues au 6 de l'article 93 et précédemment donnés en sous-location, l'imposition de la plus-value consécutive au changement de régime fiscal peut, sur demande expresse du contribuable, être reportée au moment où s'opérera la transmission de l'immeuble ou, le cas échéant, la transmission ou le rachat de tout ou partie des titres de la société propriétaire de l'immeuble ou sa dissolution " ;<br/>
<br/>
              3. Considérant que les bénéfices d'une société soumise au régime fiscal défini à l'article 8 du code général des impôts sont réputés réalisés à la clôture de l'exercice et acquis à cette date à chacun des associés alors présents, pour la part correspondant à ses droits dans la société ; qu'il en va ainsi alors même qu'au cours d'un même exercice, une levée d'option d'achat d'un immeuble détenu par la société en crédit-bail et donné par elle en location est susceptible, compte tenu des règles prévues par l'article 238 bis K pour la détermination et l'imposition de la part de bénéfices correspondant aux droits détenus dans une société de personne, de faire naître une plus-value immobilière taxable en application des dispositions précitées des I et III de l'article 93 quater du code général des impôts, que cette plus-value fait l'objet d'une demande de report d'imposition dans les formes prévues au IV du même article 93 quater puis qu'intervient un événement mettant fin à ce report d'imposition ;<br/>
<br/>
              4. Considérant, dès lors, qu'en jugeant que la levée de l'option d'achat d'immeubles pris en crédit-bail par une société civile immobilière qui, auparavant, les exploitait dans le cadre d'une sous-location et dont le revenu était imposable dans la catégorie des bénéfices non commerciaux, constituait, par détermination de la loi, le fait générateur d'une imposition immédiate de la plus-value dégagée par cette opération et que cette imposition devait, en cas de report, être mise, au terme du report, à la charge des associés présents dans la société civile immobilière au moment de la levée de l'option, la cour administrative d'appel de Lyon a commis une erreur de droit, en ce qui concerne tant  la date de l'exigibilité de l'imposition en cause que l'identité des redevables de l'imposition ; que son arrêt doit, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé en tant qu'il  rejette les conclusions de Mlle Céline B tendant à la décharge des cotisations supplémentaires et des pénalités correspondantes mises à sa charge à raison des plus-values litigieuses ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte des motifs qui constituent le fondement du dispositif, non contesté sur ce point, de l'arrêt de la cour administrative d'appel de Lyon du 25 juin 2009 en tant qu'il accorde une décharge partielle des impositions et cotisations sociales mises à la charge de Mlle Céline B au titre des années 1999 et 2000, que les SCI B, 2P et Céline ont clos le 31 décembre 2000 un exercice débuté le 1er janvier 2000 ; que les bénéfices afférents à cet exercice, y compris une éventuelle plus-value née de la levée d'option, ne pouvaient être taxés qu'à compter du 31 décembre 2000, entre les mains des associés présents à cette date ; que Mlle Céline B, qui n'était plus, alors, associée des SCI, ne pouvait donc, en tout état de cause, être imposée au titre des plus-values litigieuses ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la requérante est fondée à soutenir que c'est à tort que le tribunal administratif de Dijon a rejeté sa demande de décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises à sa charge respectivement au titre de l'année 2000 et des années 1999 et 2000 ainsi que des pénalités correspondantes, à raison de l'imposition des plus-values nées à l'occasion de la levée des options d'achat effectuée par les SCI B, 2P et Céline ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la requérante d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 4 de l'arrêt du 25 juin 2009 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : Mlle B est déchargée des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises à sa charge respectivement au titre de l'année 2000 et des années 1999 et 2000 ainsi que des pénalités correspondantes, à raison de l'imposition des plus-values réalisées à l'occasion de la levée des options d'achat effectuée par les SCI B, 2P et Céline.<br/>
Article 3 : Le jugement du 9 mai 2006 du tribunal administratif de Dijon est réformé en ce qu'il a de contraire aux motifs de la présente décision.<br/>
Article 4 : L'Etat versera à Mlle B une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mlle Céline B et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
