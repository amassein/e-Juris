<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028155118</ID>
<ANCIEN_ID>JG_L_2013_11_000000350889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/15/51/CETATEXT000028155118.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 04/11/2013, 350889, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; BALAT</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:350889.20131104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Toulon d'annuler l'arrêté du 19 juin 2009 par lequel le maire du Beausset (Var) a fait opposition à sa déclaration préalable de division ainsi que la décision du 24 août 2009 rejetant le recours gracieux qu'il avait formé contre cet arrêté. Par un jugement n° 0902601 du 12 mai 2011, le tribunal administratif de Toulon a rejeté sa demande.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 juillet et 12 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, M.A..., représenté par Me Balat, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 0902601 du tribunal administratif de Toulon du 12 mai 2011 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune du Beausset la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 10 janvier 2012, la commune du Beausset, représentée par la SCP Fabiani, Luc-Thaler, conclut au rejet du pourvoi et à ce que la somme de 3 000 euros soit mise à la charge de M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Le ministre de l'égalité des territoires et du logement a présenté des observations, enregistrées le 18 janvier 2013.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Ont été entendus en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole a été donnée, avant et après les conclusions, à Me Balat, avocat de M. B...A...et à la SCP Fabiani, Luc-Thaler, avocat de la commune du Beausset ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...A..., copropriétaire indivis d'un terrain situé sur le territoire de la commune du Beausset, classé en zone naturelle NB du plan d'occupation des sols, a déposé une déclaration préalable afin de procéder à la division de son terrain, en vue d'en détacher une parcelle à bâtir. Par un arrêté du 19 juin 2009, le maire du Beausset s'est opposé à cette déclaration préalable, au motif que l'article I NB 1 du règlement du plan d'occupation des sols de la commune interdisait les lotissements dans cette zone et, par une décision du 24 août 2009, a rejeté le recours gracieux formé par M. A...contre cet arrêté. Par un jugement du 12 mai 2011 contre lequel M. A...se pourvoit en cassation, le tribunal administratif de Toulon a rejeté sa demande tendant à l'annulation de cet arrêté.<br/>
<br/>
              2. Pour écarter le moyen tiré par M. A...de ce que l'opération de division envisagée ne pouvait être regardée comme un lotissement, le tribunal, après avoir cité les dispositions de l'article L. 442-1 du code de l'urbanisme définissant la notion de lotissement, s'est borné à affirmer que " l'opération sus décrite constitue un lotissement " au sens de ces dispositions, sans même l'avoir décrite précédemment. Par suite, il n'a pas mis le juge de cassation en mesure d'exercer son contrôle. M. A...est, dès lors, fondé à demander l'annulation du jugement pour ce motif, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, tirés de la méconnaissance du dernier alinéa de l'article R. 732-1 du code de justice administrative et des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'erreur de droit et de l'erreur de qualification juridique commises par le tribunal en regardant l'opération de division déclarée comme un lotissement, ainsi que du défaut de réponse à conclusions, de l'insuffisance de motivation et de l'erreur de droit entachant le jugement en ce qu'il écarte l'exception d'illégalité de l'article I NB 1 du règlement du plan d'occupation des sols.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Le livre IV du code de l'urbanisme fixe le régime des constructions, aménagements et démolitions et définit notamment les procédures administratives d'autorisation ou de déclaration auxquelles ils sont préalablement soumis. En vertu des dispositions de son article L. 442-1, applicables au présent litige, le lotissement constitue une division d'une ou plusieurs propriétés foncières en vue de l'implantation de bâtiments.<br/>
<br/>
              5. En vertu de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction applicable au plan d'occupation des sols du Beausset, le règlement du plan d'occupation des sols a pour objet de fixer les règles générales et les servitudes d'utilisation des sols, qui peuvent notamment comporter l'interdiction de construire, et, en particulier, doivent à cette fin délimiter les zones urbaines et définir, en fonction des circonstances locales, les règles concernant le droit d'implanter des constructions, leur destination et leur nature. Il ne ressort, en revanche, ni de ces dispositions ni d'aucune autre disposition législative que les auteurs du règlement d'un plan d'occupation des sols aient compétence pour interdire par principe ou pour limiter la faculté reconnue aux propriétaires de procéder, dans les conditions prévues au livre IV du code de l'urbanisme, à la division d'une ou de plusieurs propriétés foncières en vue de l'implantation de bâtiments, faculté qui participe de l'exercice de leur droit à disposer de leurs biens, dont il appartient au seul législateur de fixer les limites. Par suite, en interdisant par principe les lotissements dans une ou plusieurs zones qu'il délimite, le règlement d'un plan d'occupation des sols édicte des règles qui excèdent celles que la loi l'autorise à prescrire.<br/>
<br/>
              6. Il résulte de ce qui précède que M. A...est fondé à soutenir que les dispositions de l'article I NB 1 du règlement du plan d'occupation des sols de la commune du Beausset, en ce qu'elles interdisent tout lotissement dans la zone NB de la commune, sont entachées d'illégalité et, par suite, à demander l'annulation de l'arrêté du 19 juin 2009 pris sur leur fondement, ainsi que de la décision du maire du Beausset rejetant son recours gracieux contre cet arrêté.<br/>
<br/>
              7. Pour l'application de l'article L. 600-4-1 du code de l'urbanisme, en l'état du dossier soumis au Conseil d'Etat, les autres moyens soulevés par M.A..., tirés de l'incompétence du signataire de l'arrêté du 19 juin 2009 et de l'erreur de droit et de l'erreur manifeste d'appréciation commises par la commune en retenant l'existence d'un lotissement, ne sont pas de nature à justifier l'annulation des décisions attaquées.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante, les sommes que demande à ce titre la commune du Beausset. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre sur ce même fondement à la charge de la commune du Beausset le versement à M. A...d'une somme de 3 000 euros au titre des frais exposés par lui en première instance et en cassation.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Toulon du 12 mai 2011, l'arrêté du 19 juin 2009 du maire du Beausset et la décision du 24 août 2009 rejetant le recours gracieux formé par M. A...contre cet arrêté sont annulés.<br/>
Article 2 : La commune du Beausset versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la commune du Beausset présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à la commune du Beausset.<br/>
Copie en sera adressée à la ministre de l'égalité des territoires et du logement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
