<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783337</ID>
<ANCIEN_ID>JG_L_2018_12_000000410887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783337.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 07/12/2018, 410887</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410887.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 26 mai et 28 août 2017 au secrétariat du contentieux du Conseil d'Etat, la société TBF génie tissulaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-389 du 23 mars 2017 relatif aux conditions d'entrée et de sortie du territoire national des tissus, de leurs dérivés, des cellules issues du corps humain et des préparations de thérapie cellulaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2004/23/CE du 31 mars 2004 relative à l'établissement de normes de qualité et de sécurité pour le don, l'obtention, le contrôle, la transformation, la conservation, le stockage et la distribution des tissus et cellules humains ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2015-509 du 6 mai 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société TBF génie tissulaire.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 novembre 2018, présentée pour la société TBF génie tissulaire ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              En ce qui concerne la sortie du territoire national, vers un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, d'éléments ou produits du corps humain :<br/>
<br/>
              1. Aux termes des troisième à cinquième alinéas du I de l'article L. 1245-5 du code de la santé publique, dans leur rédaction issue de la loi du 23 février 2017 :  " Peuvent fournir, à des fins thérapeutiques, à un établissement agréé dans un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen les éléments et produits du corps humain mentionnés au premier alinéa du présent I, les établissements ou les organismes autorisés par l'Agence nationale de sécurité du médicament et des produits de santé en application de l'article L. 1243-2 qui disposent pour ces éléments ou produits de l'autorisation de procédé de préparation et de conservation prévue au même article L. 1243-2. / Lorsque ces établissements ou ces organismes ne disposent pas de cette autorisation de procédé, ils communiquent à l'Agence nationale de sécurité du médicament et des produits de santé les motifs pour lesquels cette autorisation ne leur a pas été délivrée ainsi que, le cas échéant, les informations relatives à ce procédé. L'agence communique, sur leur demande, aux autorités de santé compétentes du pays destinataire les motifs pour lesquels un établissement ou un organisme ne dispose pas de l'autorisation de procédé. / Lorsque l'agence a refusé de délivrer l'autorisation de procédé mentionnée audit article L. 1243-2 ou lorsqu'elle estime, au vu des informations transmises, qu'il y a un risque lié à la qualité ou à la sécurité du produit ou que les données transmises sont insuffisantes, elle peut interdire aux établissements ou aux organismes de fournir ces produits ".<br/>
<br/>
              2. Les dispositions des quatrième et cinquième alinéas du I de l'article L. 1245-5 du code de la santé publique permettent ainsi à l'Agence nationale de sécurité du médicament et des produits de santé d'interdire la fourniture d'éléments et produits du corps humain à un établissement agréé dans un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen par un établissement ou organisme situé en France qui ne dispose pas pour ces éléments ou produits d'une autorisation de procédé de préparation et de conservation, si elle estime qu'il existe un risque lié à leur qualité ou à leur sécurité ou qu'elle dispose de données insuffisantes. Elles contribuent ainsi à assurer la transposition du 2 de l'article 6 de la directive 2004/23/CE du Parlement européen et du Conseil du 31 mars 2004 relative à l'établissement de normes de qualité et de sécurité pour le don, l'obtention, le contrôle, la transformation, la conservation, le stockage et la distribution des tissus et cellules humains, qui impose que les autorités compétentes de chaque Etat membre autorisent les procédés de préparation de tissus et cellules qu'un établissement de tissus peut effectuer, conformément aux exigences techniques arrêtées par la Commission en matière de procédés de préparation de tissus et cellules.<br/>
<br/>
              3. Le décret du 23 mars 2017 relatif aux conditions d'entrée et de sortie du territoire national des tissus, de leurs dérivés, des cellules issus du corps humain et des préparations de thérapie cellulaire, dont la société requérante demande l'annulation pour excès de pouvoir, insère dans le code de la santé publique des articles R. 1245-17 et R. 1245-18 qui précisent que les établissements ou organismes envisageant de fournir de tels éléments ou produits à un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, alors qu'ils ne bénéficient pas de l'autorisation de procédé de préparation et de conservation, adressent un dossier, avant la sortie des éléments ou produits du territoire national, à l'Agence nationale de sécurité du médicament et des produits de santé. Celle-ci dispose alors d'un délai de quatre mois pour évaluer les informations transmises et notifier son intention d'interdire la sortie de l'élément ou du produit du territoire national, en raison du risque lié à sa qualité ou à sa sécurité ou à l'insuffisance des données. Ces dispositions se bornent ainsi à prévoir les conditions dans lesquelles est mise en oeuvre la faculté d'interdiction ouverte, ainsi qu'il a été dit au point 2 ci-dessus, à l'Agence nationale de sécurité du médicament et des produits de santé par les dispositions de l'article L. 1245-5 du code de la santé publique, qu'elles ne méconnaissent pas.   <br/>
<br/>
              En ce qui concerne l'exportation, vers un Etat qui n'est ni membre de l'Union européenne ni partie à l'accord sur l'Espace économique européen, d'éléments ou produits du corps humain :<br/>
<br/>
              4. Aux termes du 2 de l'article 9 de la directive 2004/23/CE du 31 mars 2004 : " Les États membres prennent toutes les mesures nécessaires pour que toutes les exportations de tissus ou cellules vers des pays tiers soient effectuées par des établissements de tissus agréés, désignés ou autorisés aux fins de ces activités. Les États membres qui expédient ces exportations veillent à ce que ces dernières satisfassent aux exigences de la présente directive ". Aux termes des troisième et quatrième alinéas du II de l'article L. 1245-5 du code de la santé publique, pris pour la transposition de ces dispositions : " Seuls les établissements ou les organismes autorisés par l'Agence nationale de sécurité du médicament et des produits de santé en application de l'article L. 1243-2 peuvent exporter, à des fins thérapeutiques, les éléments ou produits mentionnés au premier alinéa du I du présent article, vers un Etat non membre de l'Union européenne ou n'étant pas partie à l'accord sur l'Espace économique européen. Ces établissements ou ces organismes sont soumis à une autorisation d'exportation délivrée par l'Agence nationale de sécurité du médicament et des produits de santé, après avis de l'Agence de la biomédecine. / Les autorisations mentionnées aux premier et avant-dernier alinéas du présent II précisent notamment l'activité des établissements, organismes ou personnes physiques mentionnés au premier alinéa du présent II et la catégorie des tissus et de leurs dérivés ou des préparations de thérapie cellulaire importés ou exportés ainsi que, le cas échéant, les indications thérapeutiques reconnues (...) ".<br/>
<br/>
              5. Le décret attaqué insère dans le code de la santé publique un article R. 1245-5 dont le II précise que la demande d'autorisation d'exportation à des fins thérapeutiques prévue au troisième alinéa du II de l'article L. 1245-5 cité ci-dessus, adressée au directeur général de l'Agence nationale de sécurité du médicament et des produits de santé, est accompagnée d'un dossier qui comporte notamment : " 6° Les informations sur le prélèvement des tissus, et de cellules issus du corps humain, le procédé de préparation mis en oeuvre, les produits et matériels entrant en contact avec les tissus éléments ou produits mentionnés à l'article R. 1245-1, et, pour les produits finis, les informations sur le produit fini (...) ".<br/>
<br/>
              6. Ces dispositions, divisibles des autres dispositions du décret attaqué, se bornent à reprendre des dispositions figurant antérieurement au 6° de l'article R. 1245-3 du même code, issu du décret du 6 mai 2015 relatif à la simplification des régimes d'autorisations concernant les activités de préparation, conservation, distribution, cession, importation ou exportation de tissus, de leurs dérivés, des cellules et des préparations de thérapie cellulaire, issus du corps humain, utilisés à des fins thérapeutiques, publié au Journal officiel de la République française du 8 mai 2015. Toutefois, ainsi que le fait valoir la société requérante, le décret attaqué a été pris pour tirer les conséquences de la loi du 23 février 2017 ratifiant l'ordonnance n° 2016-966 du 15 juillet 2016 portant simplification de procédures mises en oeuvre par l'Agence nationale de sécurité du médicament et des produits de santé et comportant diverses dispositions relatives aux produits de santé, qui a supprimé l'exigence que l'autorisation d'exportation mentionne les procédés de préparation et de conservation mis en oeuvre ainsi que les indications thérapeutiques reconnues. Dès lors, les dispositions critiquées ne peuvent être regardées comme purement confirmatives des dispositions précédemment en vigueur et les conclusions tendant à leur annulation ne sont pas tardives.<br/>
<br/>
              7. Si les dispositions de l'article L. 1245-5 du code de la santé publique citées au point 4 ne prévoient plus que l'autorisation d'exportation délivrée par l'Agence nationale de sécurité du médicament et des produits de santé mentionne les procédés de préparation et de conservation mis en oeuvre ainsi que les indications thérapeutiques reconnues, elles ne font pas obstacle à ce que le pouvoir réglementaire exige des établissements et organismes demandeurs qu'ils lui fournissent les informations relatives au procédé de préparation mis en oeuvre, pour lui permettre, ainsi que l'impose le 2 de l'article 9 de la directive 2004/23/CE du 31 mars 2004, de s'assurer que les exportations de tissus ou cellules vers des pays tiers satisfont aux exigences de la directive, lesquelles portent notamment sur les procédés de préparation de tissus et cellules. Par suite, la société requérante n'est pas fondée à soutenir que les dispositions qu'elle critique méconnaîtraient les troisième et quatrième alinéas du II de l'article L. 1245-5 du code de la santé publique, non plus que les objectifs du 2 de l'article 9 de la directive 2004/23/CE du 31 mars 2004.  <br/>
<br/>
              8. Il résulte de tout ce qui précède que la société TBF génie tissulaire n'est pas fondée à demander l'annulation du décret attaqué. Les conclusions qu'elle présente au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent également, en conséquence, être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de société TBF génie tissulaire est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société TBF génie tissulaire et à la  ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-06-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. RÉOUVERTURE DES DÉLAIS. ABSENCE. DÉCISION CONFIRMATIVE. ABSENCE. - DISPOSITIONS D'UN ACTE RÉGLEMENTAIRE SE BORNANT À REPRENDRE DES DISPOSITIONS ANTÉRIEURES - DISPOSITIONS PUREMENT CONFIRMATIVES [RJ1] - ABSENCE, DÈS LORS QUE CET ACTE EST PRIS POUR TIRER LES CONSÉQUENCES D'UNE LOI NOUVELLE [RJ2].
</SCT>
<ANA ID="9A"> 54-01-07-06-01-02-01 Les dispositions du décret n° 2017-389 du 23 mars 2017 insérant dans le code de la santé publique (CSP) le 6° du II de l'article R. 1245-5, divisibles des autres dispositions de ce décret, se bornent à reprendre des dispositions figurant antérieurement au 6° de l'article R. 1245-3 du même code, issu du décret n° 2015-509 du 6 mai 2015, publié au Journal officiel de la République française (JORF) du 8 mai 2015. Toutefois, le décret n° 2017-389 du 23 mars 2017 a été pris pour tirer les conséquences de la loi n° 2017-220 du 23 février 2017. Dès lors, ces dispositions ne peuvent être regardées comme purement confirmatives des dispositions précédemment en vigueur et les conclusions tendant à leur annulation ne sont pas tardives.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur cette notion, CE, Section, 7 février 1969,,et autres, n° 71488, p. 83 ; CE, Assemblée, 12 octobre 1979, Rassemblement des nouveaux avocats de France et autres, n°s  01875 01905 01948 01951, p. 370,; CE, 29 juin 1992, SARL Procaes, n° 111423, T. p. 1209.,,[RJ2] Rappr., s'agissant d'un acte individuel, CE, 21 juillet 1970, Association pour la liberté d'expression à la télévision, n° 76665, p. 502.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
