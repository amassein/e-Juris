<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041523495</ID>
<ANCIEN_ID>JG_L_2020_01_000000419837</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/52/34/CETATEXT000041523495.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2020, 419837</TITRE>
<DATE_DEC>2020-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419837</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:419837.20200130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Non au Béton " et Mme B... ont demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du 20 janvier 2015 par lequel le maire de Saint-Clément-de-Rivière a délivré un permis d'aménager à la société Decathlon en vue de la réalisation d'un lotissement multi-activités dénommé " Oxylane " sur le territoire de la commune, ainsi que sa décision rejetant le recours gracieux qu'ils avaient formé contre cette décision. Par un jugement n° 1504071 du 15 février 2018, le tribunal administratif de Montpellier a partiellement fait droit à leur demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, deux mémoires en réplique et un nouveau mémoire enregistrés les 13 avril 2018, 13 juillet 2018, 9 avril 2019, 10 septembre 2019 et 2 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association " Non au Béton " et Mme A... B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Saint-Clément-de-Rivière et de la société Decathlon la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ; <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat de l'Association Non au Beton et de Mme B..., à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Saint-Clément-de-Rivière et à la SCP Foussard, Froger, avocat de la société Decathlon SA ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 janvier 2020, présentée par l'association Non au Béton et Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 20 janvier 2015, le maire de Saint-Clément-de-Rivière a délivré à la société Decathlon un permis d'aménager en vue de la réalisation d'un lotissement multi-activités dénommé " Oxylane ", sur un terrain de 235 500 mètres carrés situé en limite territoriale de la métropole de Montpellier. L'association Non Au Béton et Mme B... se pourvoient en cassation contre le jugement du 15 février 2018 par lequel le tribunal administratif de Montpellier a annulé cet arrêté en tant seulement qu'il prévoyait des constructions à usage de logement en méconnaissance des dispositions du IINA1 du règlement du plan d'occupation des sols. Leurs conclusions doivent être regardées comme dirigées contre ce jugement en tant qu'il a rejeté le surplus des conclusions de leur demande.<br/>
<br/>
<br/>
              En ce qui concerne l'inclusion de lots inconstructibles dans le périmètre du lotissement :<br/>
<br/>
<br/>
              2. L'article L. 442-1 du code de l'urbanisme dispose que " Constitue un lotissement la division en propriété ou en jouissance d'une unité foncière ou de plusieurs unités foncières contiguës ayant pour objet de créer un ou plusieurs lots destinés à être bâtis ". Aux termes de l'article L. 442-1-2 du même code : " Le périmètre du lotissement comprend le ou les lots destinés à l'implantation de bâtiments ainsi que, s'ils sont prévus, les voies de desserte, les équipements et les espaces communs à ces lots. Le lotisseur peut toutefois choisir d'inclure dans le périmètre du lotissement des parties déjà bâties de l'unité foncière ou des unités foncières concernées ". <br/>
<br/>
              3. Une opération d'aménagement ayant pour effet la division d'une propriété foncière en plusieurs lots constitue un lotissement, au sens de ces dispositions, s'il est prévu d'implanter des bâtiments sur l'un au moins de ces lots. Une telle opération doit respecter les règles tendant à la maîtrise de l'occupation des sols édictées par le code de l'urbanisme et les documents locaux d'urbanisme. Il appartient par suite à l'autorité compétente de refuser le permis d'aménager sollicité lorsque, compte tenu de ses caractéristiques telles qu'elles ressortent des pièces du dossier qui lui est soumis, le projet de lotissement prévoit l'implantation de constructions dont la conformité avec les règles d'urbanisme ne pourra être ultérieurement assurée lors de la délivrance des autorisations d'urbanisme requises. Toutefois, la circonstance que certains lots ne soient pas destinés à accueillir des constructions ne fait pas obstacle, par elle-même, à la réalisation d'une opération de lotissement incluant ces lots, dès lors que leur inclusion est nécessaire à la cohérence d'ensemble de l'opération et que la règlementation qui leur est applicable est respectée.  <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le projet en litige comprend huit lots. Les lots n° 1 à 5 sont destinés à accueillir des constructions à vocation commerciale ou d'activité, pourvues d'aires de stationnement attenantes, tandis que les lots n°s 6 et 7 sont destinés à des espaces agricoles et que le lot n° 8 correspond à un espace boisé classé. Les lots n°s 6 et 8 sont inconstructibles en vertu du plan d'occupation des sols, le lot n°s 6 étant classé en zone NC exclusivement réservée à la valorisation agricole des sols et le lot n° 8 en zone ND, zone naturelle dont la vocation est la conservation de l'espace boisé classé. Par suite, en écartant les moyens tirés de la méconnaissance des règles applicables en zones NC et ND et de ce que ces deux lots devaient en tout état de cause être exclus du périmètre du lotissement en vertu des dispositions citées au point 2 au motif qu'ils participaient à l'économie générale du lotissement, étant donc nécessaires à la cohérence de l'ensemble de l'opération,  et que leur exploitation était conforme à la réglementation de leurs zonages respectifs, le tribunal administratif de Montpellier n'a pas commis d'erreur de droit.<br/>
<br/>
<br/>
              En ce qui concerne la réalisation d'un parc de stationnement de 125 places dans le périmètre du lotissement et celle des aires de stationnement des lots n°s 4 et 5 :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge du fond qu'en relevant que le parc de stationnement de 125 places situé devant l'espace boisé classé destiné à être aménagé en parcours sportif était un équipement collectif qui avait vocation à desservir l'ensemble des lots du projet contesté, et qu'il était, de ce fait, un équipement commun à l'ensemble des lots au sens des dispositions précitées de l'article L. 442-1-2 du code de l'urbanisme justifiant qu'il soit intégré au périmètre du lotissement, le tribunal administratif de Montpellier n'a pas entaché son jugement de dénaturation. En outre, l'article IINA1 du règlement du plan d'occupation des sols relatif à l'occupation et l'utilisation des sols dispose que : " Ne sont admises (...) que les occupations ou utilisations du sol ci-après : (...) en secteur IINAd : / les constructions tertiaires à usage de commerce, d'enseignement, professions libérales, les structures d'accueil, d'hébergement, ainsi que les annexes s'y rattachant directement ". Si, aux termes de l'article IINA2 de ce même règlement, sont interdits : " Les divers modes d'occupation des sols prévus aux articles R. 442-1 et suivants du code de l'urbanisme ", parmi lesquels figurent notamment les " aires de stationnement ouvertes au public ", le tribunal administratif de Montpellier n'a pas entaché son jugement d'erreur de droit ni de dénaturation des faits de l'espèce en déduisant du constat que le parc de stationnement avait vocation à être utilisé par les usagers de l'ensemble des activités et commerces prévus dans le cadre du projet d'aménagement qu'il devait être regardé comme relevant des annexes aux constructions tertiaires à usage de commerce admises en secteur IINAd. <br/>
<br/>
              6. Par ailleurs, l'article L. 111-1-4 du code de l'urbanisme dispose que " En dehors des espaces urbanisés des communes, les constructions ou installations sont interdites dans une bande de cent mètres de part et d'autre de l'axe des autoroutes, des routes express et des déviations au sens du code de la voirie routière et de soixante-quinze mètres de part et d'autre de l'axe des autres routes classées à grande circulation. / Cette interdiction (...) ne s'applique pas : / aux constructions ou installations liées ou nécessaires aux infrastructures routières ". Aux termes de l'article IINA 6 du règlement du plan d'occupation des sols : " Les constructions doivent être implantées au-delà des marges de reculement suivantes : / - 75 mètres par rapport à l'axe de la route départementale 986. / - 15 mètres par rapport à l'axe des autres routes départementales. / - 5 mètres de l'alignement des autres voies. / Les bâtiments d'infrastructures ainsi que les installations (ouvrages de rétention, parcs de stationnement...) peuvent être implantés différemment suivant leur nature ". Il ressort des pièces du dossier soumis au juge du fond qu'en relevant qu'à la date de l'arrêté litigieux la route départementale 986 n'était plus classée en tant que route à grande circulation et n'avait pas été classée par arrêté ministériel comme route express, le tribunal administratif n'a pas entaché son jugement d'inexactitude matérielle des faits. Il s'ensuit qu'en en déduisant que les règles mentionnées à l'article L. 111-1-4 ne s'appliquaient pas au projet et que le maire de Saint-Clément-de-Rivière pouvait user de la faculté de déroger à la marge de recul prévue à l'article IINA 6 en autorisant l'implantation du parc de stationnement mentionné au point 6 et des aires de stationnement des lots n°s 4 et 5 en deçà de la limite prescrite, eu égard à la nature de ces installations, le tribunal, qui a suffisamment motivé son jugement sur ce point, n'a pas commis d'erreur de droit. <br/>
<br/>
<br/>
              En ce qui concerne l'activité prévue sur le lot n° 7 :<br/>
<br/>
              7. Il ressort des pièces du dossier soumis aux juges du fond que le lot n° 7 est situé en secteur IINAd du plan d'occupation des sols. Aux termes de l'article IINA1 du règlement du plan d'occupation des sols : " Ne sont admises (...) que les occupations ou utilisations du sol ci-après : (...) en secteur IINAd : / les constructions tertiaires à usage de commerce ". <br/>
<br/>
              8. Le tribunal a retenu, par une appréciation souveraine exempte de dénaturation,  que la culture de fruits et légumes prévue sur le lot n° 7 n'avait pas d'autre objet que la production de denrées alimentaires en vue de leur commercialisation, " en circuit court ", par une enseigne implantée sur un autre lot du lotissement spécialisée dans la vente de produits locaux et de ses productions faites sur place, estimant ainsi implicitement mais nécessairement que, ce faisant, elle participait de l'activité de cette enseigne commerciale. En en déduisant que l'activité prévue sur le lot n° 7 devait être regardée comme une activité commerciale et économique conforme au règlement du secteur IINAd, le tribunal n'a pas commis d'erreur de droit. <br/>
<br/>
<br/>
              En ce qui concerne la participation mise à la charge du lotisseur au titre de la réalisation d'équipements exceptionnels :<br/>
<br/>
              9. L'article L. 332-8 du code de l'urbanisme dispose que : " Une participation spécifique peut être exigée des bénéficiaires des autorisations de construire qui ont pour objet la réalisation de toute installation à caractère industriel, notamment relative aux communications électroniques, agricole, commercial ou artisanal qui, par sa nature, sa situation ou son importance, nécessite la réalisation d'équipements publics exceptionnels. / Lorsque la réalisation des équipements publics exceptionnels n'est pas de la compétence de l'autorité qui délivre le permis de construire, celle-ci détermine le montant de la contribution correspondante, après accord de la collectivité publique à laquelle incombent ces équipements ou de son concessionnaire ". Aux termes de l'article L. 332-7 du même code : " L'illégalité des prescriptions exigeant des taxes ou des contributions aux dépenses d'équipements publics est sans effet sur la légalité des autres dispositions de l'autorisation de construire. / Lorsque l'une de ces prescriptions est annulée pour illégalité, l'autorité qui a délivré l'autorisation prend, compte tenu de la décision juridictionnelle devenue définitive, un nouvel arrêté portant la prescription d'une taxe ou d'une contribution aux dépenses d'équipements publics ". Il résulte de ces dispositions, applicables aux permis d'aménager en vertu de l'article L. 332-12 du même code, qu'un requérant ne saurait utilement se prévaloir de l'illégalité de prescriptions relatives à la participation prévue à l'article L. 332-8 à l'appui de conclusions dirigées contre l'autorisation de construire ou d'aménager. Par conséquent, les moyens tirés de l'illégalité des prescriptions relatives à cette contribution, prévues à l'article 12 de l'arrêté attaqué, soulevés devant les juges du fond à l'appui de conclusions dirigées contre le permis d'aménager litigieux, étaient inopérants. Ce motif, qui n'emporte l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs retenus par le jugement attaqué, dont il justifie légalement le dispositif. Il s'ensuit que les moyens d'erreur de droit, d'inexacte qualification juridique des faits et de dénaturation invoqués à l'appui du pourvoi ne sont pas fondés.<br/>
<br/>
<br/>
              En ce qui concerne l'atteinte au caractère et aux qualités paysagères du site :<br/>
<br/>
              10. En premier lieu, l'article R. 111-21 du code de l'urbanisme dans sa rédaction applicable au litige dispose que : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales ". Il résulte de ces dispositions que, si les constructions projetées portent atteinte aux paysages naturels avoisinants, l'autorité administrative compétente peut refuser de délivrer le permis d'aménager sollicité ou l'assortir de prescriptions spéciales. Pour rechercher l'existence d'une telle atteinte, il lui appartient d'apprécier, dans un premier temps, la qualité du site naturel sur lequel la construction est projetée et d'évaluer, dans un second temps, l'impact que cette construction, compte tenu de sa nature et de ses effets, pourrait avoir sur le site. Les dispositions de cet article excluent qu'il soit procédé dans le second temps du raisonnement à une balance d'intérêts divers en présence autres que ceux mentionnés à l'article R. 111-21. Pour l'application de ces dispositions, l'article IINA 11 du plan d'occupation des sols dispose que " Par leur aspect extérieur, les constructions et autres modes d'occupation du sol ne doivent pas porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites et aux paysages urbains ".  <br/>
<br/>
              11. En jugeant, par une appréciation souveraine exempte de dénaturation, que le terrain d'assiette du projet, situé à proximité de secteurs urbanisés, de zones commerciales et d'activités, entouré de routes très fréquentées et ne faisant l'objet d'aucun classement particulier, hormis un espace boisé qui sera préservé et mis en valeur, ne s'insérait pas dans un site présentant un caractère paysager exceptionnel, le tribunal administratif de Montpellier n'a pas mis en balance des intérêts autres que ceux visés à l'article R. 111-21 du code de l'urbanisme. Il s'ensuit que le moyen d'erreur de droit doit être écarté. <br/>
<br/>
              12. En second lieu, en relevant pour les mêmes motifs que le terrain d'assiette du projet ne s'inscrivait pas dans un paysage présentant des qualités susceptibles de justifier son classement en zone naturelle, le tribunal administratif n'a pas entaché son jugement de dénaturation des pièces du dossier qui lui était soumis. <br/>
<br/>
<br/>
              En ce qui concerne l'étude d'impact :<br/>
<br/>
              13. L'article R. 122-5 du code de l'environnement définit le contenu de l'étude d'impact, qui est proportionné à la sensibilité environnementale de la zone susceptible d'être affectée par le projet, à l'importance et la nature des travaux, ouvrages et aménagements projetés et à leurs incidences prévisibles sur l'environnement ou la santé humaine et présente une esquisse des principales solutions de substitution examinées par le pétitionnaire ou le maître d'ouvrage et les raisons pour lesquelles, eu égard aux effets sur l'environnement ou la santé humaine, le projet présenté a été retenu. Les inexactitudes, omissions ou insuffisances d'une étude d'impact ne sont susceptibles de vicier la procédure et donc d'entraîner l'illégalité de la décision prise au vu de cette étude que si elles ont pu avoir pour effet de nuire à l'information complète de la population ou si elles ont été de nature à exercer une influence sur la décision de l'autorité administrative. <br/>
<br/>
              14. Le tribunal, qui a suffisamment motivé sa décision sur ce point, a relevé, par une appréciation souveraine exempte de dénaturation, que l'absence d'esquisse de solutions de substitution pour éviter les terrains les plus sensibles pour les chiroptères n'avait pas nui à l'information complète des personnes auxquelles l'étude d'impact était destinée et n'avait pas non plus exercé d'influence sur la décision de l'autorité municipale dès lors, d'une part, que cette étude fait clairement ressortir l'importance des atteintes susceptibles d'être portées à la population de chiroptères et étudie les mesures propres à compenser ou réduire les effets négatifs du projet et, d'autre part, qu'elle indique les raisons pour lesquelles le projet a été retenu. <br/>
<br/>
<br/>
              En ce qui concerne la régularité de la concertation préalable à l'élaboration du plan d'occupation des sols :<br/>
<br/>
              15. Il ressort des pièces du dossier soumis au juge du fond que le conseil municipal de Saint-Clément-de-Rivière a adopté une délibération prescrivant la révision du plan d'occupation des sols de la commune le 15 septembre 1995 puis a défini, par une délibération du 28 septembre 2000, les objectifs poursuivis par cette révision, avant d'approuver le projet de révision par une délibération du 27 décembre 2001. <br/>
<br/>
              16. Dans sa rédaction en vigueur à la date de la délibération du 15 septembre 1993, l'article L. 300-2 du code de l'urbanisme dispose que : " I - Le conseil municipal délibère sur les objectifs poursuivis et sur les modalités d'une concertation associant, pendant toute la durée de l'élaboration du projet, les habitants, les associations locales et les autres personnes concernées dont les représentants de la profession agricole, avant : / a) Toute modification ou révision du plan d'occupation des sols qui ouvre à l'urbanisation tout ou partie d'une zone d'urbanisation future ; (...) ", tandis qu'aux termes de l'article L. 600-11 du même code, dans sa rédaction applicable au litige : " Les documents d'urbanisme et les opérations mentionnées aux articles L. 103-2 et L. 300-2 ne sont pas illégaux du seul fait des vices susceptibles d'entacher la concertation, dès lors que les modalités définies aux articles L. 103-1 à L. 103-6 et par la décision ou la délibération prévue à l'article L. 103-3 ont été respectées. Les autorisations d'occuper ou d'utiliser le sol ne sont pas illégales du seul fait des vices susceptibles d'entacher cette délibération ou les modalités de son exécution ". Il résulte de ces dispositions que l'adoption ou la révision du plan local d'urbanisme doit être précédée d'une concertation associant les habitants, les associations locales et les autres personnes concernées. Le conseil municipal doit, avant que ne soit engagée la concertation, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser ce document d'urbanisme et, d'autre part, sur les modalités de la concertation. Si cette délibération est susceptible de recours devant le juge de l'excès de pouvoir, son illégalité ne peut, en revanche, eu égard à son objet et à sa portée, être utilement invoquée contre la délibération approuvant le plan local d'urbanisme. Ainsi que le prévoit l'article L. 300-2 du code de l'urbanisme précité, les irrégularités ayant affecté le déroulement de la concertation au regard des modalités définies par la délibération prescrivant la révision du document d'urbanisme demeurent ....  <br/>
<br/>
              17. Les requérantes soutenaient devant le tribunal administratif que le maire de Saint-Clément-de-Rivière ne pouvait légalement se fonder sur le plan d'occupation des sols compte tenu des irrégularités ayant entaché le déroulement de la concertation préalable à son approbation. Elles faisaient en particulier valoir que la brièveté du délai entre la délibération définissant les objectifs poursuivis et celle arrêtant le projet de révision du plan d'occupation des sols avait privé la concertation d'effet utile. Il résulte toutefois de ce qui a été dit au point 16 que les requérantes ne pouvaient utilement invoquer des irrégularités ayant affecté le déroulement de la concertation qu'au regard des modalités définies par la délibération du 15 septembre 1995 prescrivant la révision du plan d'occupation des sols, ce qu'ils n'ont pas fait. Le moyen soulevé devant les juges du fond était donc inopérant. Ce motif, qui n'emporte l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par le jugement attaqué, dont il justifie légalement le dispositif. Il s'ensuit que les moyens d'erreur de droit et de dénaturation invoqués à l'appui du pourvoi sont sans incidence sur le bien-fondé du jugement attaqué. <br/>
<br/>
              18. Il résulte de tout ce qui précède que le pourvoi de l'association Non au Béton et Mme B... doit être rejeté, y compris les conclusions présentées au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
              19. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association Non au Béton et Mme B... la somme de 750 euros chacune qu'elles verseront à la commune de Saint-Clément-de-Rivière ainsi que la somme de 750 euros chacune  qu'elles verseront à la société Decathlon au titre de ces dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association Non au Béton et Mme B... est rejeté. <br/>
Article 2 : L'association Non au Béton et Mme B... verseront la somme de 750 euros chacune à la commune de Saint-Clément-de-Rivière et la somme de 750 euros chacune  à la société Decathlon au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à l'association Non au Béton, à Mme A... B..., à la commune de Saint-Clément-de-Rivière et à la société Decathlon.  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. AUTORISATION DE LOTIR. CONTENU DE L'AUTORISATION. - POSSIBILITÉ D'INCLURE DES LOTS NON DESTINÉS À ÊTRE BÂTIS DANS LE PÉRIMÈTRE D'UN LOTISSEMENT [RJ1] - EXISTENCE, DÈS LORS QUE CETTE INCLUSION EST NÉCESSAIRE À LA COHÉRENCE D'ENSEMBLE DE L'OPÉRATION ET QUE LA RÉGLEMENTATION QUI LEUR EST APPLICABLE EST RESPECTÉE [RJ2].
</SCT>
<ANA ID="9A"> 68-02-04-02-02 Une opération d'aménagement ayant pour effet la division d'une propriété foncière en plusieurs lots constitue un lotissement, au sens de l'article L. 442-1 du code de l'urbanisme, s'il est prévu d'implanter des bâtiments sur l'un au moins de ces lots. Une telle opération doit respecter les règles tendant à la maîtrise de l'occupation des sols édictées par le code de l'urbanisme et les documents locaux d'urbanisme. Il appartient, en conséquence, à l'autorité compétente de refuser le permis d'aménager sollicité lorsque, compte tenu de ses caractéristiques telles qu'elles ressortent des pièces du dossier qui lui est soumis, le projet de lotissement prévoit l'implantation de constructions dont la conformité avec les règles d'urbanisme ne pourra être ultérieurement assurée lors de la délivrance des autorisations d'urbanisme requises. Toutefois, la circonstance que certains lots ne soient pas destinés à accueillir des constructions ne fait pas obstacle, par elle-même, à la réalisation d'une opération de lotissement incluant ces lots, dès lors que leur inclusion est nécessaire à la cohérence d'ensemble de l'opération et que la réglementation qui leur est applicable est respectée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la définition du lotissement, CE, 20 février 2013, Mme,et autres, n° 345728, T. p. 878.,,[RJ2] Cf., sur la condition du respect par les opérations de lotissement des règles d'urbanisme tendant à la maîtrise de l'occupation des sols, CE, 24 février 2016, Commune de Pia, n° 383079, T. p. 991.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
