<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815748</ID>
<ANCIEN_ID>JG_L_2019_07_000000391519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815748.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 24/07/2019, 391519</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; HAAS</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:391519.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 10 mai 2017, le Conseil d'Etat, statuant au contentieux sur la requête de la société France Télévisions tendant à l'annulation pour excès de pouvoir de la décision n° 2015-232 du 27 mai 2015 par laquelle le Conseil supérieur de l'audiovisuel l'a mise en demeure de ne pas s'opposer à la reprise par la société Playmédia, sur son site Internet, des services qu'elle édite, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              1°) Une entreprise qui propose le visionnage de programmes de télévision en flux continu et en direct sur Internet doit-elle, de ce seul fait, être regardée comme une entreprise qui exploite un réseau de communications électroniques utilisé pour la diffusion publique d'émissions de radio ou de télévision au sens du paragraphe 1 de l'article 31 de la directive 2002/22/CE du 7 mars 2002 '<br/>
<br/>
              2°) En cas de réponse négative à la première question, un Etat membre peut-il, sans méconnaître la directive ou d'autres règles du droit de l'Union européenne, prévoir une obligation de diffusion de services de radio ou de télévision pesant à la fois sur des entreprises exploitant des réseaux de communications électroniques et sur des entreprises qui, sans exploiter de tels réseaux, proposent le visionnage de programmes de télévision en flux continu et en direct sur Internet '<br/>
<br/>
              3°) En cas de réponse positive à la deuxième question, les Etats membres peuvent-ils s'abstenir de subordonner l'obligation de diffusion, en ce qui concerne les distributeurs de services qui n'exploitent pas des réseaux de communications électroniques, à l'ensemble des conditions prévues au paragraphe 1 de l'article 31 de la directive 2002/22/CE du 7 mars 2002, alors que ces conditions s'imposeront en vertu de la directive en ce qui concerne les exploitants de réseaux '<br/>
<br/>
              4°) Un Etat membre qui a institué une obligation de diffusion de certains services de radio ou de télévision sur certains réseaux peut-il, sans méconnaître la directive, prévoir l'obligation pour ces services d'accepter d'être diffusés sur ces réseaux, y compris, s'agissant d'une diffusion sur un site Internet, lorsque le service en cause diffuse lui-même ses propres programmes sur Internet '<br/>
<br/>
              5°) La condition selon laquelle un nombre significatif d'utilisateurs finals des réseaux soumis à l'obligation de diffusion doivent les utiliser comme leurs moyens principaux pour recevoir des émissions de radio ou de télévision prévue au paragraphe 1 de l'article 31 de la directive 2002/22/CE doit-elle, s'agissant d'une diffusion par Internet, s'apprécier au regard de l'ensemble des utilisateurs qui visionnent des programmes de télévision en flux continu et en direct sur le réseau Internet ou des seuls utilisateurs du site soumis à l'obligation de diffusion ' <br/>
<br/>
              Par un arrêt n° C-298/17 du 13 décembre 2018, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
              Par un mémoire, enregistré le 14 janvier 2019, la société France Télévision maintient par les mêmes moyens les conclusions de sa requête. Elle soutient en outre que la mise en demeure attaquée a été prise sur le fondement de dispositions incompatibles avec l'article 56 du Traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              Par un mémoire, enregistré le 4 mars 2019, la société Playmédia maintient ses conclusions à fin d'annulation et demande qu'une somme de 10 000 euros soit mise à la charge de la société France Télévisions au titre des dispositions de l'article L. 761-1 du code de justice administrative. Elle soutient qu'aucun des moyens soulevés par cette société n'est fondé.<br/>
<br/>
              Le ministre de la culture a présenté des observations, enregistrées le 15 mars 2019.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat statuant au contentieux du 10 mai 2017 ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2002/21/CE du Parlement européen et du Conseil du 7 mars 2002 ;<br/>
              - la directive 2002/22/CE du Parlement européen et du Conseil du 7 mars 2002 ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - l'arrêt n° C-298/17 du 13 décembre 2018 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société France Télévisions et à Me Haas, avocat de la société Playmedia.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 juillet 2019, présentée par la société France Télévisions ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 27 mai 2015, le Conseil supérieur de l'audiovisuel (CSA) a mis en demeure la société France Télévisions de se conformer aux dispositions de l'article 34-2 de la loi du 30 septembre 1986 en ne s'opposant pas à la reprise de ses programmes par la société Playmédia, en flux continu, sur son site Internet. La société France Télévisions demande l'annulation pour excès de pouvoir de cette mise en demeure.<br/>
<br/>
              2. Aux termes du paragraphe 1 de l'article 31 de la directive 2002/22/CE du Parlement européen et du Conseil, du 7 mars 2002, concernant le service universel et les droits des utilisateurs au regard des réseaux et services de communications électroniques (directive " service universel ") : " Les Etats membres peuvent imposer des obligations raisonnables de diffuser ("must carry") pour la transmission de chaînes de radio et de télévision spécifiées et de services complémentaires, notamment les services d'accessibilité destinés à assurer un accès approprié pour les utilisateurs finals handicapés, aux entreprises relevant de leur ressort qui fournissent des réseaux de communications électroniques utilisés pour la diffusion publique de chaînes de radio et de télévision, lorsqu'un nombre significatif d'utilisateurs finals utilisent ces réseaux comme leur moyen principal pour recevoir des chaînes de radio et de télévision. Ces obligations ne sont imposées que lorsqu'elles sont nécessaires pour atteindre des objectifs d'intérêt général clairement définis par chaque État membre, et sont proportionnées et transparentes (...) ". Ces dispositions reconnaissent aux Etats membres la faculté d'instituer, sous réserve des conditions qu'elles prévoient, une obligation de diffusion de certains services de radio et de télévision pesant sur des entreprises qui fournissent des réseaux de communications électroniques utilisés pour la diffusion publique de tels services. <br/>
<br/>
              3. Le a de l'article 2 de la directive 2002/21 du Parlement et du Conseil du 7 mars 2002 relative à un cadre réglementaire commun pour les réseaux et services de communications électroniques (directive cadre), auquel renvoie l'article 2 de la directive " service universel ", définit les réseaux de communications électroniques comme " les systèmes de transmission et, le cas échéant, les équipements de commutation ou de routage et les autres ressources, y compris les éléments de réseau qui ne sont pas actifs, qui permettent l'acheminement de signaux par câble, par voie hertzienne, par moyen optique ou par d'autres moyens électromagnétiques, comprenant les réseaux satellitaires, les réseaux terrestres fixes (avec commutation de circuits ou de paquets, y compris l'Internet) et mobiles, les systèmes utilisant le réseau électrique, pour autant qu'ils servent à la transmission de signaux, les réseaux utilisés pour la radiodiffusion sonore et télévisuelle et les réseaux câblés de télévision, quel que soit le type d'information transmise ". Aux termes du point 45 de l'exposé des motifs de cette même directive : " Les services fournissant un contenu, tels qu'une offre de vente de contenus de radiodiffusion sonore ou de télévision, ne sont pas couverts par le cadre réglementaire commun pour les réseaux et services de communications électroniques. Les fournisseurs de ces services ne devraient pas être soumis aux obligations de service universel pour ces activités. La présente directive est, par conséquent, sans préjudice des mesures arrêtées au niveau national, conformément à la législation communautaire, à l'égard de ces services ".<br/>
<br/>
              4. Aux termes du I de l'article 34-2 de la loi du 30 septembre 1986 relative à la liberté de communication : " I.- Sur le territoire métropolitain, tout distributeur de services sur un réseau n'utilisant pas de fréquences terrestres assignées par le Conseil supérieur de l'audiovisuel met gratuitement à disposition de ses abonnés les services des sociétés mentionnées au I de l'article 44 et la chaîne Arte, diffusés par voie hertzienne terrestre en mode analogique ainsi que la chaîne TV 5, et le service de télévision diffusé par voie hertzienne terrestre en mode numérique ayant pour objet de concourir à la connaissance de l'outre-mer, spécifiquement destiné au public métropolitain, édité par la société mentionnée au I de l'article 44, sauf si ces éditeurs estiment que l'offre de services est manifestement incompatible avec le respect de leurs missions de service public. Lorsqu'il propose une offre de services en mode numérique, il met également gratuitement à disposition des abonnés à cette offre les services de ces sociétés qui sont diffusés par voie hertzienne terrestre en mode numérique (...) ". Aux termes de l'article 2-1 de la même loi : " Pour l'application de la présente loi, les mots : distributeur de services désignent toute personne qui établit avec des éditeurs de services des relations contractuelles en vue de constituer une offre de services de communication audiovisuelle mise à disposition auprès du public par un réseau de communications électroniques au sens du 2° de l'article L. 32 du code des postes et des communications électroniques. Est également regardée comme distributeur de services toute personne qui constitue une telle offre en établissant des relations contractuelles avec d'autres distributeurs ".<br/>
<br/>
              5. La société Playmédia propose le visionnage de programmes de télévision en flux continu et en direct sur un site Internet et se rémunère principalement par la diffusion de messages publicitaires qui précèdent et accompagnent le visionnage. Se prévalant de la qualité de distributeur de services au sens de l'article 2-1 de la loi du 30 septembre 1986, cette société estime tirer des dispositions de l'article 34-2 de la même loi le droit de diffuser les programmes édités par la société France Télévisions, qui constitue le corollaire nécessaire de l'obligation de reprise posée par ces dispositions. La société France Télévisions, qui par ailleurs diffuse elle-même ces programmes en flux continu et en direct sur un site Internet qu'elle met à la disposition du public, a été mise en demeure par le CSA de ne pas s'opposer à leur diffusion par la société Playmédia. A l'appui de son recours contre cette mesure, elle fait notamment valoir que les conditions prévues au paragraphe 1 de l'article 31 de la directive 2002/22/CE ne sont pas remplies dès lors, en particulier, qu'il n'est pas possible d'affirmer que des utilisateurs du réseau Internet en nombre significatif l'utilisent comme leur principal moyen pour recevoir des émissions de télévision. <br/>
<br/>
              6. Dans l'arrêt du 13 décembre 2018 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que le paragraphe 1 de l'article 31 de la directive " service universel " doit être interprété en ce sens qu'une entreprise qui propose le visionnage de programmes de télévision en flux continu et en direct sur Internet ne doit pas, en raison de ce seul fait, être regardée comme une entreprise qui fournit un réseau de communications électroniques utilisé pour la diffusion publique de chaînes de radio et de télévision. Il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que l'activité de la société Playmédia ne la fait pas rentrer dans le champ de l'obligation de diffusion prévue par l'article 31, paragraphe 1 de la directive " service universel ". <br/>
<br/>
              7. Toutefois, dans le même arrêt, la Cour de justice de l'Union européenne a également dit pour droit que les dispositions de la directive " service universel " doivent être interprétées en ce sens qu'elles ne s'opposent pas à ce qu'un Etat membre impose, dans une situation telle que celle en cause au principal, une obligation de diffuser à des entreprises qui, sans fournir des réseaux de communications électroniques, proposent le visionnage de programmes de télévision en flux continu et en direct sur Internet.<br/>
<br/>
              8. Il résulte de la combinaison des dispositions citées au point 4 que le législateur français a prévu une obligation de diffusion de certains services de télévision qui pèse sur les distributeurs de services tels qu'il les a définis, que ces distributeurs puissent ou non être regardés comme fournissant des réseaux de communications électroniques au sens de la directive " service universel ". La société Playmédia est, par son activité, susceptible de présenter le caractère d'un distributeur de services au sens de l'article 2-1 de la loi du 30 septembre 1986.<br/>
<br/>
              9. L'article 34-2 de cette même loi subordonne toutefois l'obligation de diffusion qu'il prévoit à la condition que la distribution de services soit destinée à des abonnés. Il résulte des dispositions de cet article, éclairées par les travaux préparatoires de la loi dont elles sont issues, que, pour leur application, la notion d'abonnés doit s'entendre des utilisateurs liés au distributeur de services par un contrat commercial prévoyant le paiement d'un prix. Par la décision attaquée, le CSA a constaté que l'offre de Playmédia s'adressait pour partie à des personne souscrivant, pour y accéder, " un engagement de nature contractuelle matérialisé par l'acceptation de conditions générales d'utilisation et par le renseignement de plusieurs informations personnelles telles que leur adresse de courrier électronique, leur date de naissance et leur sexe ". En en déduisant que la condition prévue à l'article 34-2 de la loi du 30 septembre 1986, tenant à la distribution du service à des abonnés, était remplie, alors que l'accès au service n'était pas subordonné au paiement d'un prix, le CSA a fait une application erronée des dispositions de cet article. La société France Télévisions est, par suite, fondée à demander l'annulation de la décision qu'elle attaque, sans qu'il soit besoin d'examiner les autres moyens de sa requête.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil supérieur de l'audiovisuel la somme de 3 000 euros à verser à la société France Télévisions au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société France Télévisions qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision n° 2015-232 du 27 mai 2015 du Conseil supérieur de l'audiovisuel est annulée.<br/>
Article 2 : Le Conseil supérieur de l'audiovisuel versera à la société France Télévisions la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par la société Playmédia sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société France Télévisions, au Conseil supérieur de l'audiovisuel, à la société Playmédia et au ministre de la culture.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-02-01 RADIO ET TÉLÉVISION. RÈGLES GÉNÉRALES. RÉGIME D`ÉMISSION ET OBLIGATIONS DE PRODUCTION. - OBLIGATION DE DIFFUSION DE CERTAINS SERVICES DE TÉLÉVISION PESANT SUR LES DISTRIBUTEURS DE SERVICE (ART. 2-1 ET 34-2 DE LA LOI DU 30 SEPTEMBRE 1986) - CONDITIONS - 1) DISTRIBUTEURS FOURNISSANT UN RÉSEAU DE COMMUNICATIONS ÉLECTRONIQUES AU SENS DE LA DIRECTIVE 2002/21/CE - ABSENCE [RJ1] - 2) DISTRIBUTION DE SERVICES DESTINÉE À DES ABONNÉS PAYANTS - EXISTENCE.
</SCT>
<ANA ID="9A"> 56-02-01 Société proposant le visionnage de programmes de télévision en flux continu et en direct sur un site Internet et se rémunérant principalement par la diffusion de messages publicitaires qui précèdent et accompagnent le visionnage. Société se prévalant de la qualité de distributeur de services au sens de l'article 2-1 de la loi n° 86-1067 du 30 septembre 1986 pour tirer des dispositions de l'article 34-2 de la même loi le droit de diffuser les programmes édités par la société France Télévisions, qui constitue le corollaire nécessaire de l'obligation de reprise posée par ces dispositions.,,,1) Il résulte de la combinaison du I de l'article 34-2 et de l'article 2-1 de la loi du 30 septembre 1986 que le législateur français a prévu une obligation de diffusion de certains services de télévision qui pèse sur les distributeurs de services tels qu'il les a définis, que ces distributeurs puissent ou non être regardés comme fournissant des réseaux de communications électroniques au sens de la directive 2002/21/CE du 7 mars 2002 dite Service universel. La société en cause est, par son activité, susceptible de présenter le caractère d'un distributeur de services au sens de l'article 2-1 de la loi du 30 septembre 1986.,,,2) L'article 34-2 de cette même loi subordonne toutefois l'obligation de diffusion qu'il prévoit à la condition que la distribution de services soit destinée à des abonnés. Il résulte de cet article, éclairé par les travaux préparatoires de la loi dont il est issu, que, pour leur application, la notion d'abonnés doit s'entendre des utilisateurs liés au distributeur de services par un contrat commercial prévoyant le paiement d'un prix. Par la décision attaquée, le Conseil supérieur de l'audiovisuel (CSA) a constaté que l'offre de la société en cause s'adressait pour partie à des personnes souscrivant, pour y accéder, un engagement de nature contractuelle matérialisé par l'acceptation de conditions générales d'utilisation et par le renseignement de plusieurs informations personnelles telles que leur adresse de courrier électronique, leur date de naissance et leur sexe. En en déduisant que la condition prévue à l'article 34-2 de la loi du 30 septembre 1986, tenant à la distribution du service à des abonnés, était remplie, alors que l'accès au service n'était pas subordonné au paiement d'un prix, le CSA a fait une application erronée des dispositions de cet article.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur le fait qu'une entreprise qui propose le visionnage de programmes de télévision en flux continu et en direct sur Internet ne doit pas, en raison de ce seul fait, être regardée comme une entreprise qui fournit un réseau de communications électroniques utilisé pour la diffusion publique de chaînes de radio et de télévision au sens de la directive 2002/21/CE et sur la possibilité, pour les Etats-membres, de soumettre néanmoins une telle entreprise à une obligation de diffusion, CJUE, 13 décembre 2018, France Télévisions SA, aff. C-298/17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
