<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164468</ID>
<ANCIEN_ID>JG_L_2020_06_000000440572</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164468.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/06/2020, 440572, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440572</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440572.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistré les 13 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... H... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret du Président de la République du 27 avril 2020, en tant qu'il nomme au tribunal de première instance de Nouméa Mme F... I... comme vice-présidente, Mme G... B... comme vice-présidente chargée de la présidence de la section détachée de Koné, et M. A... C... comme vice-président chargé de la présidence de la section détachée de Lifou ; <br/>
<br/>
              2°) d'enjoindre à la garde des sceaux, ministre de la justice de réexaminer les demandes de mutation concernant les emplois considérés dans un délai d'un mois suivant la notification de la présente décision.<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
             - la condition d'urgence est remplie en ce que, d'une part, l'affectation de trois magistrats du siège dans le ressort du tribunal de première instance de Nouméa a pour effet d'écarter pour longtemps sa candidature à des emplois dans ce ressort, et, d'autre part, cette décision implique qu'il continue de rester affecté à Mayotte, séparé par une très longue distance de sa famille, qui réside en Nouvelle-Calédonie,; <br/>
             - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
             - le décret méconnaît les dispositions de l'article 29 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature modifiée, ensemble l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales relatif au droit à une vie familiale normale, dès lors qu'il ne prend pas en considération sa situation familiale  ;<br/>
             - il est entaché d'une erreur de droit en ce que, d'une part, il applique une doctrine du Conseil supérieur de la magistrature conditionnant toute mutation à l'accomplissement d'une durée d'exercice des fonctions de deux ans qui ne résulte ni de l'ordonnance du 22 décembre 1958, ni du décret du 7 janvier 1993 pris pour son application et ne saurait faire obstacle à la prise en compte de sa situation familiale, et, d'autre part et en tout état de cause, il fait courir ce délai à compter de la date du 1er janvier 2020 à laquelle il a été à sa demande déchargé des fonctions de juge des contentieux de la protection, alors que, en premier lieu, la différence de traitement opérée entre les magistrats nommés à cette date juges des contentieux de la protection, qui conservent le bénéfice de leur ancienneté, et ceux qui, comme lui, ont sollicité leur décharge, qui résulte de l'application d'une note du directeur des services judiciaires du 8 avril 2019, est contraire au principe d'égalité, en deuxième lieu, elle résulte d'une décision prise par une autorité incompétente et qui n'a pas été portée à sa connaissance en temps utile, en troisième lieu, la décharge de fonctions consécutive à une réforme organique n'est pas assimilable à une demande ordinaire de décharge, et en quatrième lieu, une telle application de la règle dite des deux ans méconnaît l'obligation d'examen de sa situation personnelle ;<br/>
             - il est entaché d'une erreur manifeste d'appréciation, dès lors notamment que ses aptitudes et expériences correspondent particulièrement aux qualifications requises pour les affectations en cause, alors que les qualifications et le parcours des magistrats qui sont nommés ne présentent pas le même degré d'adéquation avec ces affectations.<br/>
              Par un mémoire en défense, enregistré le 4 juin 2020, la garde des sceaux, ministre de la justice conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie, eu égard à l'intérêt public qui s'attache au bon fonctionnement du service public de la justice, et que les moyens de la requête ne sont pas de nature à créer un doute sérieux quant à la légalité du décret attaqué, la décision de ne pas le nommer en Nouvelle-Calédonie tenant compte de sa situation familiale mais se fondant sur le risque de mise en cause de son impartialité, eu égard notamment à la très longue durée de son affectation précédente et aux courriers de deux élus de Nouvelle-Calédonie en faveur de sa candidature et n'étant pas entachée d'erreur manifeste dans l'appréciation des candidatures retenues de préférence à la sienne. <br/>
<br/>
              Par un mémoire en réplique, enregistré le 8 juin 2020, M. H... persiste dans les conclusions et moyens de sa requête. Il soutient en outre que le motif d'intérêt public avancé par l'administration pour écarter l'urgence de sa requête et tenant au risque de mise en cause de son impartialité n'est pas constitué et soulève un nouveau moyen tiré de la violation de son droit d'accès à son dossier, faute pour les deux courriers d'élus de Nouvelle-Calédonie mentionnés par l'administration de lui avoir été communiqués avant l'intervention du décret attaqué.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi du 22 avril 1905 ;<br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le décret n° 93-21 du 7 janvier 1993;<br/>
              - le code de justice administrative ;<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 10 juin 2020 à 10 heures.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. M. H..., magistrat en poste à Mamoudzou, a présenté en 2019 dans le cadre de la procédure dite " de transparence ", sa candidature à plusieurs postes dans le ressort notamment de la cour d'appel de Nouméa. Il demande la suspension du décret du Président de la République du 27 avril 2020 en tant qu'il nomme au tribunal de première instance de Nouméa Mme F... I... au poste de vice-présidente, Mme G... B... au poste de vice-présidente chargée de la section détachée de Koné et M. A... C..., au poste de vice-président, chargé de la section détachée de Lifou.<br/>
<br/>
              3. Aux termes de l'article 29 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Dans toute la mesure compatible avec le bon fonctionnement du service et les particularités de l'organisation judiciaire, les nominations des magistrats tiennent compte de leur situation de famille ".<br/>
<br/>
              4. Il résulte de l'instruction que M. H..., après une première affectation de magistrat en métropole, a exercé ses fonctions pendant 18 ans dans le ressort de la cour d'appel de Nouméa, au sein de la section détachée de Koné. Il a été nommé à compter du 1er septembre 2017 vice-président du tribunal de grande instance de Mamoudzou, chargé du tribunal d'instance, avant d'être nommé, sur sa demande de décharge de fonctions spécialisées, depuis le 1er janvier 2020 vice-président du tribunal judiciaire de Mamoudzou. <br/>
<br/>
              5. Si, en premier lieu, M. H... soutient que le décret attaqué a été pris au terme d'une procédure irrégulière, faute pour lui d'avoir été mis en mesure de prendre connaissance des courriers par lesquels deux élus de Nouvelle-Calédonie sont intervenus auprès de la garde des sceaux, ministre de la justice en faveur de son affectation en Nouvelle-Calédonie, dont il n'aurait pris connaissance qu'à la suite de leur production par le ministre dans le cadre de sa défense dans la présente instance, le décret attaqué, n'est cependant pas, y compris en ce qu'il ne retient pas sa candidature, au nombre des mesures qui ne peuvent intervenir sans que l'intéressé ait été mis à même de demander la communication de son dossier. Le moyen tiré de la méconnaissance de l'article 65 de la loi du 22 avril 1905 n'est, par suite, pas de nature à créer un doute sérieux quant à la légalité du décret attaqué. <br/>
<br/>
              6. En deuxième lieu, M. H... soutient que le refus de le nommer dans l'un des trois postes situés dans le ressort de la cour d'appel de Nouméa est intervenu sans qu'il soit tenu compte de sa situation de famille ni procédé à l'examen de sa situation personnelle, alors que la règle qui lui a été appliquée imposant une durée d'exercice minimale dans les fonctions auxquelles un magistrat est nommé ne résulte d'aucune disposition applicable et que le calcul de cette durée à compter seulement du 1er janvier 2020, date de sa décharge de ses fonctions spécialisées, serait illégal. Il ne résulte cependant pas de l'instruction, eu égard notamment au courrier du Conseil supérieur de la magistrature du 21 mars 2019 signalant, sur la base de ses observations détaillées, sa situation à la garde des sceaux, ministre de la justice, qu'il n'ait pas été tenu compte de sa situation familiale. Par ailleurs, la ministre soutient en défense que la décision de ne pas le nommer dans le ressort de la cour d'appel de Nouméa a été prise en considération notamment du risque de mise en cause de son impartialité qui résulterait des termes de deux courriers d'élus de Nouvelle-Calédonie, mentionnés ci-dessus, qui sont intervenus auprès d'elle en faveur d'une telle nomination. L'existence et la teneur de ces courriers, qui sont produits au dossier, ne sont pas contestés. Il en résulte que la décision contestée, n'a pas été prise, contrairement à ce qu'il soutient, par application automatique d'une règle tenant à la durée d'exercice des fonctions et sans examen de sa situation personnelle. Les moyens tirés de la méconnaissance des dispositions citées ci-dessus de l'article 29 de l'ordonnance du 22 décembre 1958 ainsi que de l'obligation d'examen de la situation personnelle de l'intéressé ne sont dès lors pas de nature à créer un doute sur la légalité du décret attaqué. Il en va de même des moyens tirés de l'illégalité de la règle de durée minimale des fonctions et de son application à compter du 1er janvier 2020, dès lors que la décision contestée n'a pas été prise sans examen de sa situation personnelle.<br/>
<br/>
              7. En troisième lieu, M. H..., pour soutenir que le décret attaqué est entaché d'erreur manifeste d'appréciation, invoque la gravité de l'atteinte à son droit à mener une vie privée et familiale normale eu égard à la distance qui le sépare de sa famille, domiciliée en Nouvelle Calédonie, ainsi que son expérience et ses qualifications toutes particulières pour les postes en cause par comparaison avec celles des magistrats qui ont été nommés par le décret attaqué. Malgré ces éléments, il appartenait à l'autorité de nomination de porter son appréciation sur les différentes candidatures déclarées aux postes litigieux au regard de l'intérêt du service, y compris du risque pour l'impartialité du service public de la justice et pour la perception de cette impartialité, étayé par les deux courriers des élus mentionnés ci-dessus. Il ne résulte pas de l'instruction ni que la nomination de M. H... aux postes souhaités était rendue impérative par les besoins du service, ni que l'expérience et les aptitudes des candidats retenus les qualifiaient manifestement moins que M. H... pour ces affectations. Eu égard à l'ensemble de ces considérations et malgré les fortes contraintes imposées à la poursuite de la vie familiale de M. H..., le moyen tiré l'erreur manifeste d'appréciation n'est pas de nature à créer un doute sérieux quant à la légalité du décret attaqué.<br/>
<br/>
              8. Il résulte de tout ce qui précède qu'aucun des moyens invoqués par M. H... ne paraît, en l'état de l'instruction, propre à faire naître un doute sérieux quant à la légalité du décret contesté. Il en résulte que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, sa demande de suspension ne peut être accueillie. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. H... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D... H... et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
