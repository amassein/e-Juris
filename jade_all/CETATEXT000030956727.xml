<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956727</ID>
<ANCIEN_ID>JG_L_2015_07_000000387557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/67/CETATEXT000030956727.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 27/07/2015, 387557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:387557.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...F...a demandé au tribunal administratif d'Amiens d'une part, d'annuler la délibération du conseil municipal du 13 octobre 2014 ayant élu Mme E...C...et M. A...B...en qualité de conseillers communautaires représentant la commune d'Ault au sein de la communauté de communes Bresle-Maritime et, d'autre part, de confirmer son élection en qualité de conseiller communautaire. Par une ordonnance n° 1404034 du 29 décembre 2014, le tribunal administratif d'Amiens a rejeté pour irrecevabilité sa protestation. <br/>
<br/>
              Par une requête, enregistrée le 2 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. F...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance n° 1404034 du 29 décembre 2014 du tribunal administratif d'Amiens ;<br/>
<br/>
              2°) de faire droit à sa requête de première instance.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 21 août 2014, le préfet de la région Picardie, préfet de la Somme et le préfet de la région Haute-Normandie, préfet de la Seine-Maritime ont constaté la nouvelle composition du conseil communautaire de la communauté de communes Bresle-Maritime à la suite de la décision du Conseil constitutionnel n° 2014-405 QPC du 20 juin 2014. Aux termes de cet arrêté, le nombre de délégués de la commune d'Ault a été réduit de un par rapport à la composition précédente. Par une délibération du 13 octobre 2014, le conseil municipal d'Ault a procédé à la désignation des deux conseillers communautaires parmi les trois proclamés élus à l'issue du scrutin du 23 mars 2014. L'élection de Mme C...et de M. B...a été confirmée, contrairement à celle de M.F.... Ce dernier interjette appel de l'ordonnance du 29 décembre 2014 par laquelle le tribunal administratif d'Amiens a rejeté sa demande d'annulation de cette délibération ; <br/>
<br/>
              2. Aux termes de l'article de l'article L. 2121-33 du code général des collectivités territoriales : " Le conseil municipal procède à la désignation de ses membres ou de délégués pour siéger au sein d'organismes extérieurs dans les cas et conditions prévus par les dispositions du présent code et des textes régissant ces organismes. La fixation par les dispositions précitées de la durée des fonctions assignées à ces membres ou délégués ne fait pas obstacle à ce qu'il puisse être procédé à tout moment, et pour le reste de cette durée, à leur remplacement par une nouvelle désignation opérée dans les mêmes formes. ". Aux termes de l'article L. 273-6 du code électoral : " Les conseillers communautaires représentant les communes de 1 000 habitants et plus au sein des organes délibérants des communautés de communes, des communautés d'agglomération, des communautés urbaines et des métropoles sont élus en même temps que les conseillers municipaux et figurent sur la liste des candidats au conseil municipal. / L'élection a lieu dans les conditions prévues aux chapitres Ier, III et IV du titre IV du présent livre, sous réserve des dispositions du chapitre Ier du présent titre et du présent chapitre. ". Ainsi, en application des dispositions précitées, les contestations relatives aux opérations électorales dont l'objet est de désigner les délégués d'une commune à l'assemblée d'un établissement public de coopération intercommunale sont régies par les dispositions de l'article L. 248 du code électoral et doivent être formées dans le délai de recours de cinq jours fixé par l'article R. 119 du même code ;  <br/>
<br/>
              3. Il résulte de ce qui précède, d'une part, que les conclusions tendant à l'annulation de la délibération du 13 octobre 2014 du conseil municipal d'Ault doivent être regardées comme tendant à l'annulation des opérations électorales qui se sont déroulées en son sein pour la désignation des délégués de la commune à la communauté de communes de Bresle-Maritime et, d'autre part, que cette délibération ne peut être contestée devant le tribunal administratif que dans les conditions spéciales prévues aux articles L. 248 et R. 119 du code électoral. Ainsi, la mention erronée du délai de recours applicable en général aux délibérations du conseil municipal est sans incidence sur les règles régissant le contentieux électoral ;<br/>
<br/>
              4. Par suite, la protestation de M. F...contre les opérations électorales du 13 octobre 2014, enregistrée au greffe du tribunal administratif d'Amiens le 29 octobre 2014, était tardive. M. F...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D...F...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D...F...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
