<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074286</ID>
<ANCIEN_ID>JG_L_2021_01_000000439582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074286.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 26/01/2021, 439582, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439582.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société d'exercice libéral par actions simplifiée (SELAS) Biomnis a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 12 juin 2013 par laquelle le directeur régional des finances publiques de Rhône-Alpes et du département du Rhône a refusé de lui accorder le bénéfice de son option pour le régime d'intégration fiscale prévue par l'article 223 A du code général des impôts. Par un jugement n° 1305752 du 11 octobre 2016, ce tribunal a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 16LY03896 du 12 avril 2018, la cour administrative d'appel de Lyon a, sur appel du ministre de l'économie et des finances, annulé ce jugement et rejeté la demande de la société.<br/>
<br/>
              Par une décision n° 421460 du 1er juillet 2019, le Conseil d'État, statuant au contentieux a fait droit au pourvoi en cassation présenté par la société Biomnis, annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Lyon<br/>
<br/>
              Par un arrêt n° 19LY02728 du 14 janvier 2020, la cour administrative d'appel de Lyon, statuant sur renvoi du Conseil d'État, a annulé le jugement du tribunal administratif de Lyon et rejeté la demande de la société Biomnis.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 mars et 17 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la société Biomnis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de l'action et des comptes publics ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2014-1655 du 29 décembre 2014 ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la société Biomnis ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
               1. Il ressort des pièces du dossier soumis aux juges du fond que la société Biomnis, qui exerce l'activité de laboratoire d'analyse médicale et détient 99,99 % du capital social de la société Centre d'explorations fonctionnelles, devenue Eurofins-CEF, et de la société Centre de biologie médicale, devenue Eurofins-CMB69, a adressé le 7 mai 2013 au service des impôts dont elle relève, en application de l'article 46 quater-0 ZD de l'annexe III au code général des impôts, un courrier portant option pour le régime d'intégration fiscale prévu par les articles 223 A et suivants du même code. Par une lettre du 12 juin 2013, le directeur départemental des finances publiques de Rhône-Alpes et du département du Rhône a toutefois refusé de lui accorder le bénéfice de ce régime, au motif qu'elle ne satisfaisait pas à la condition, prévue par l'article 46 quater-0 ZF de l'annexe III au code général des impôts, tenant à la détention d'au moins 95 % des droits de vote au sein de ses deux filiales. Par un jugement du 11 octobre 2016, le tribunal administratif de Lyon a annulé pour excès de pouvoir ce refus. La société Biomnis se pourvoit en cassation contre l'arrêt du 14 janvier 2020 par lequel la cour administrative d'appel de Lyon, statuant sur renvoi du Conseil d'État après l'annulation de son arrêt du 12 avril 2018, a annulé ce jugement et rejeté sa demande.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 223 A du code général des impôts dans sa rédaction applicable jusqu'à l'intervention de l'article 71 de la loi  du 29 décembre 2014 de finances rectificative pour 2014 : " Une société peut se constituer seule redevable de l'impôt sur les sociétés dû sur l'ensemble des résultats du groupe formé par elle-même et les sociétés dont elle détient 95 % au moins du capital, de manière continue au cours de l'exercice, directement ou indirectement par l'intermédiaire de sociétés ou d'établissements stables membres du groupe (...) ". Aux termes de l'article 46 quater-0 ZF de l'annexe III au même code, dans sa rédaction alors applicable : " Pour l'application des dispositions de l'article 223 A du code général des impôts, la détention de 95% au moins du capital d'une société s'entend de la détention en pleine propriété de 95% au moins des droits à dividendes et de 95% au moins des droits de vote attachés aux titres émis par cette société (...) ".<br/>
<br/>
              3. Il résulte des dispositions précitées de l'article 223 A du code général des impôts, qui ne comportent aucune obscurité justifiant qu'il soit recouru, pour apprécier leur portée, aux travaux préparatoires de la loi de laquelle elles sont issues, que la possibilité, pour une société, de se constituer seule redevable de l'impôt sur les sociétés dû sur l'ensemble des résultats du groupe qu'elle forme avec ses filiales est subordonnée à la seule condition que cette société détienne, dans les autres sociétés du groupe, une participation représentant au moins 95 % des parts sociales. En ce qu'il exige, pour qu'une société puisse être comprise dans le périmètre d'intégration fiscale, que la société tête de groupe détienne en outre, directement ou indirectement, en pleine propriété, 95% au moins des droits à dividendes et 95% au moins des droits de vote attachés aux titres émis par cette société, l'article 46 quater-O ZF de l'annexe III au code général des impôts, dans sa rédaction applicable au litige, méconnaît la portée de la disposition législative dont il a pour objet de préciser les modalités d'application et est ainsi  entaché d'incompétence. <br/>
<br/>
              4. Il s'en déduit qu'en écartant le moyen, soulevé par la voie de l'exception, tiré de l'illégalité de l'article 46 quater-O ZF précité, au motif que le pouvoir réglementaire s'était borné à préciser les modalités d'appréciation du seuil de détention du capital fixé par l'article 223 A du code général des impôts précité, la cour administrative d'appel de Lyon a commis une erreur de droit. <br/>
<br/>
              5. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la société Biomnis est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              7. Il résulte de ce qui a été dit au point 3 ci-dessus que l'administration fiscale ne pouvait légalement refuser d'accorder à la société requérante le bénéfice de l'option qu'elle sollicitait pour le régime de l'intégration fiscale au seul motif que, si elle détenait 99,99 % des parts sociales de ses deux filiales, représentant le même pourcentage dans les droits financiers, cette participation n'ouvrait droit qu'à 49,90 % des droits de vote. Dès lors, le ministre n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a annulé la décision du directeur régional des finances publiques de Rhône-Alpes et du département du Rhône du 12 juin 2013.  <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 3000 à verser à la société Biomnis au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 14 janvier 2020 est annulé.<br/>
Article 2 : La requête du ministre de l'action et des comptes publics contre le jugement du tribunal administratif de Lyon du 11 octobre 2006 est rejetée. <br/>
Article 3 : L'État versera à la société Biomnis la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société d'exercice libéral par actions simplifiée Biomnis et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
