<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037525341</ID>
<ANCIEN_ID>JG_L_2018_10_000000415573</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/53/CETATEXT000037525341.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/10/2018, 415573, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415573</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415573.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1716843 du 6 novembre 2017, enregistrée au secrétariat du contentieux du Conseil d'Etat le 9 novembre 2017, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée au greffe de ce tribunal le 3 novembre 2017, présentée par M. A... B.... Par cette requête, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande du 31 juillet 2017 d'abrogation de l'article R. 4113-3 du code de la santé publique ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir l'article R. 4113-3 du code de la santé publique ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 90-1258 du 31 décembre 1990 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que M. B...demande, d'une part, l'annulation pour excès de pouvoir de l'article R. 4113-1 du code de la santé publique et, d'autre part, l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation de ce même article ; que cet article dispose que : " Un associé ne peut exercer la profession de médecin qu'au sein d'une seule société d'exercice libéral de médecins et ne peut cumuler cette forme d'exercice avec l'exercice à titre individuel ou au sein d'une société civile professionnelle, excepté dans le cas où l'exercice de sa profession est lié à des techniques médicales nécessitant un regroupement ou un travail en équipe ou à l'acquisition d'équipements ou de matériels soumis à autorisation en vertu de l'article L. 6122-1 ou qui justifient des utilisations multiples " ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du troisième alinéa de la loi du 31 décembre 1990 relative à l'exercice sous forme de sociétés des professions libérales soumises à un statut législatif ou réglementaire ou dont le titre est protégé, des décrets en Conseil d'Etat " peuvent prévoir qu'un associé n'exerce sa profession qu'au sein d'une seule société d'exercice libéral et ne peut exercer la même profession à titre individuel ou au sein d'une société civile professionnelle " ; que ces dispositions législatives habilitent le pouvoir réglementaire à édicter, pour la profession de médecin, l'interdiction de cumuler un exercice professionnel au sein d'une société d'exercice libéral avec un exercice à titre individuel ; que par suite, M. B...n'est pas fondé à soutenir que les dispositions de l'article R. 4113-1 du code de la santé publique sont entachées d'incompétence ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en rendant applicable aux médecins libéraux, en vue notamment de garantir leur indépendance professionnelle en décourageant tout comportement de captation de clientèle, l'interdiction de cumuler un exercice au sein d'une société civile professionnelle et un exercice à titre individuel, le pouvoir réglementaire n'a, compte tenu de ce que cette interdiction de cumul est prévue dans son principe, pour toute profession libérale soumise à un statut législatif ou réglementaire, par les dispositions de la loi du 31 décembre 1990 citées ci-dessus, pas porté à la liberté d'entreprendre des médecins une atteinte disproportionnée au regard des objectifs poursuivis ; <br/>
<br/>
              4. Considérant, en troisième lieu, que la circonstance que d'autres professions entrant dans le champ des dispositions de cette loi du 31 décembre 1990 ne se seraient pas vu appliquer l'interdiction de cumul dont elles prévoient le principe est sans incidence sur l'application de cette interdiction à la profession de médecin ; que M. B...ne saurait, par suite, utilement soutenir que les dispositions de l'article R. 4113-1 du code de la santé publique méconnaissent, pour ce motif, le principe d'égalité ;<br/>
<br/>
              5. Considérant, enfin, que le moyen tiré de ce que les exceptions introduites, par les dispositions réglementaires attaquées, à cette interdiction de cumul seraient trop " restrictives " n'est pas assorti des précisions permettant d'en apprécier la portée ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par la ministre des solidarités et de la santé, M. B...n'est pas fondé à demander l'annulation de l'article R. 4113-1 du code de la santé publique ; qu'il n'est, par suite, pas davantage fondé à demander l'annulation de la décision par laquelle le Premier ministre a refusé d'abroger cet article ; que sa requête doit ainsi être rejetée, y compris les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à la ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
