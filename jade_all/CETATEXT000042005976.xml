<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042005976</ID>
<ANCIEN_ID>J0_L_2020_06_000001802612</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/00/59/CETATEXT000042005976.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 1ère chambre, 09/06/2020, 18VE02612, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>18VE02612</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BEAUJARD</PRESIDENT>
<AVOCATS>CABINET LEXCOM</AVOCATS>
<RAPPORTEUR>M. Patrice  BEAUJARD</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme MERY</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société confraternelle d'exploitation et de répartition pharmaceutique <br/>
Rhin-Rhône-Méditerranée (CERP-RRM) a demandé au Tribunal administratif de Montreuil la décharge des rappels de cotisation sur la valeur ajoutée des entreprises et de taxe additionnelle à la cotisation sur la valeur ajoutée des entreprises mis à sa charge au titre des exercices clos en 2013 et 2014, pour un montant global de 812 120 euros, assortie de l'octroi des intérêts moratoires correspondants. <br/>
<br/>
       Par un jugement n° 1705406 du 31 mai 2018, le Tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête et un mémoire en réplique, enregistrés le 25 juillet 2018 et le 20 mai 2019, la société CERP-RRM, représentée par Me Bonnabry, avocat, demande à la Cour :<br/>
<br/>
       1° d'annuler le jugement attaqué ;<br/>
<br/>
       2° de la décharger des sommes en litige à hauteur d'un montant global de 812 120 euros ;<br/>
<br/>
       3° d'enjoindre au Trésor public d'émettre un avis de dégrèvement des sommes indûment perçues par celui-ci au titre des années 2013 et 2014 ; <br/>
<br/>
       4° de mettre à la charge de l'État la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       La société CERP-RRM soutient que : <br/>
       - les règles relatives à l'assiette et à la liquidation de la contribution prévue par l'article L. 138-1 du code de la sécurité sociale ne peuvent justifier qu'elle ne soit pas prise en compte dans le calcul du prix des biens vendus aux clients de la société, ainsi que le montre la comparaison avec la taxe générale sur les activités polluantes ; la référence à la norme comptable, telle qu'appliquée par le Conseil d'État dans sa décision CERP de Rouen du 29 juin 2018, ne peut être que secondaire et ne peut se limiter à la référence aux modalités d'enregistrement des prélèvements dans la comptabilité ; il faut retenir la volonté du législateur de définir une valeur ajoutée fiscale qui reflète la création de richesses imputable à l'entreprise ; la qualification comptable des opérations n'est pas un critère opposable, ainsi que l'a jugé le Conseil d'État dans un arrêt n° 413121 du 28 novembre 2018 Société Lancôme Parfums et Beauté et Cie ; <br/>
       - le Tribunal administratif de Montreuil, dans un jugement du 29 mars 2018, Orange SA, n°1704265, pour la taxe sur les services fournis par les opérateurs de communications électroniques, prévue par les dispositions de l'article 302 bis KH du code général des impôts, a jugé que cette taxe appartient à la catégorie des taxes sur le chiffre d'affaires et des taxes assimilées, et en a déduit sa déductibilité pour le calcul de la valeur ajoutée en application du I de l'article 1586 sexies du code général des impôts, alors même que cette taxe et la contribution de l'article L. 138-1 du code général des impôts sont également assises sur le chiffre d'affaires et grèvent l'évaluation de la production réelle de manière identique ;<br/>
       - la Cour de justice de l'Union européenne, dans une décision du 20 décembre 2017, C-462/16, Finanzamt Bingen-Alzey contre Boehringer Ingelheim Pharma GmbH and Co.KG, a jugé que les remises consenties aux assureurs privés par les laboratoires en Allemagne, dans le cadre d'une politique publique, doivent s'analyser comme une réduction de prix et donc de base imposable à la taxe sur la valeur ajoutée ; ce faisant, les juges européens ont pris en compte la réalité économique du chiffre d'affaires réalisé par l'entreprise.<br/>
<br/>
       Par un mémoire en défense enregistré le 7 mai 2019, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il fait valoir que :<br/>
       - le raisonnement suivi par la société n'est pas le raisonnement suivi par le Conseil d'État, notamment dans sa décision du 21 avril 2017, SAS Pierre Fabre Médicament, qui considère, depuis sa décision Esso SAF du 23 juin 2014, que seules les taxes qui, eu égard à leur objet doivent être regardées comme grevant le prix des biens et des services vendus par l'entreprise, peuvent être déduites du chiffre d'affaires ; la contribution de l'article L. 138-1 du code de la sécurité sociale, du seul fait qu'elle a pour assiette le chiffre d'affaires, ne peut être regardée comme une taxe grevant le prix des biens et des services vendus par une entreprise ; elle n'a pas été conçue pour être répercutée sur le consommateur final ; <br/>
       - la position adoptée par le Tribunal administratif de Montreuil dans un jugement du 29 mars 2018 dont la société requérante se prévaut, a été remise en cause par la décision postérieure du Conseil d'État du 29 juin 2018 ; <br/>
       - l'arrêt de la cour de justice de l'union européenne du 20 décembre 2017, dont la société requérante se prévaut, et qui porte sur l'application de la réglementation en matière de taxe sur la valeur ajoutée, ne peut être transposé en matière de cotisation sur la valeur ajoutée des entreprises.<br/>
<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - l'arrêt C-462/16 du 20 décembre 2017 de la Cour de justice de l'Union européenne ; <br/>
       - l'ordonnance n° 2020-305 du 25 mars 2020 modifiée portant adaptation des règles applicables devant les juridictions de l'ordre administratif ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience, en application de l'article 8 de l'ordonnance n° 2020-305 du 25 mars 2020 modifiée.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de M. Beaujard, président, a été entendu au cours de l'audience publique.<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
       1. La société confraternelle d'exploitation et de répartition pharmaceutique <br/>
Rhin-Rhône-Méditerranée (CERP-RRM), société anonyme, exerce l'activité de <br/>
grossiste-répartiteur de médicaments et produits de santé. A la suite d'une vérification de comptabilité, les services fiscaux, par une proposition de rectification du 2 juillet 2015, ont remis en cause la valeur ajoutée calculée par la société ayant servi d'assiette à la cotisation sur la valeur ajoutée des entreprises pour les années 2013 et 2014, au motif que cette valeur ne pouvait être minorée de la contribution directe versée à l'Agence centrale des organismes de sécurité sociale (ACOSS), prévue par les dispositions de l'article L. 138-1 du code de la sécurité sociale. Par la présente requête, la société CERP-RRM demande l'annulation du jugement du 31 mai 2018 par lequel le Tribunal administratif de Montreuil a rejeté sa demande tendant à la décharge des suppléments de cotisation sur la valeur ajoutée des entreprises, de taxe additionnelle à la cotisation sur la valeur ajoutée des entreprises au titre des exercices clos en 2013 et 2014, pour un montant global de 812 120 euros.<br/>
<br/>
       2. En vertu du I de l'article 1586 sexies du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " Pour la généralité des entreprises, à l'exception des entreprises visées aux II à VI : / (...) 4. La valeur ajoutée est égale à la différence entre : / a) D'une part, le chiffre d'affaires tel qu'il est défini au 1, majoré : / - des autres produits de gestion courante (...) ; / - de la production immobilisée, à hauteur des seules charges qui ont concouru à sa formation et qui figurent parmi les charges déductibles de la valeur ajoutée ; (...) / - des subventions d'exploitation et des abandons de créances à caractère financier (...) ; / - de la variation positive des stocks ; / - des transferts de charges déductibles de la valeur ajoutée, autres que ceux pris en compte dans le chiffre d'affaires ; / b) Et, d'autre part : / - les achats stockés de matières premières et autres approvisionnements, les achats d'études et prestations de services, les achats de matériel, équipements et travaux, les achats non stockés de matières et fournitures, les achats de marchandises et les frais accessoires d'achat ; / - diminués des rabais, remises et ristournes obtenus sur achats ; / - la variation négative des stocks ; / - les services extérieurs diminués des rabais, remises et ristournes obtenus (...) ; / - les taxes sur le chiffre d'affaires et assimilées, les contributions indirectes, la taxe intérieure de consommation sur les produits énergétiques ; (...) ". Ces dispositions fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée servant de base à la cotisation sur la valeur ajoutée des entreprises. Il y a lieu, pour déterminer si une charge ou un produit se rattache à l'une de ces catégories, de se reporter aux normes comptables, dans leur rédaction en vigueur lors de l'année d'imposition concernée, dont l'application est obligatoire pour l'entreprise en cause.<br/>
<br/>
       3. Il résulte des dispositions citées au point 5 ci-dessus, éclairées par les travaux préparatoires, que la notion de " taxes sur le chiffre d'affaires et assimilées " désigne, non les taxes qui figurent au titre II de la première partie du livre premier du code général des impôts, mais la taxe sur la valeur ajoutée et les taxes qui, en application des normes comptables, grèvent le prix des biens et des services vendus par l'entreprise. En fixant ce critère de déduction pour le calcul de la valeur ajoutée, le législateur a fondé son appréciation de la capacité contributive des entreprises sur un critère objectif et rationnel.<br/>
<br/>
       4. Si la société requérante soutient que le montant de la contribution définie à l'article L. 138-1 du code de la sécurité sociale, qu'elle verse à l'agence centrale des organismes de sécurité sociale, se répercute nécessairement sur le prix des biens qu'elle vend, elle ne conteste pas que cette contribution n'a pas fait et ne pouvait faire l'objet d'une comptabilisation distincte en produit, en sus du montant net des ventes. Par suite, la société CERP-RRM, qui ne peut utilement se prévaloir ni de l'arrêt du Conseil d'État statuant au contentieux n° 413121 du 28 novembre 2018 Société Lancôme Parfums et Beauté et Cie relatif à la déduction d'indemnités de départ à la retraite , ni de la décision de la Cour de justice de l'Union européenne dans une affaire C-462/16, Finanzamt Bingen-Alzey contre Boehringer Ingelheim Pharma GmbH and Co.KG, portant sur la taxe sur la valeur ajoutée, n'est pas fondée à soutenir que la contribution doit être retenue dans le calcul de la valeur ajoutée tel que défini par les dispositions de l'article 1586 sexies du code général des impôts, en déduction du chiffre d'affaires. <br/>
<br/>
       5. Il résulte de ce qui précède, que la société CERP-RRM n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Montreuil a rejeté sa demande. Par voie de conséquence, ses conclusions présentées aux fins d'injonction et tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent également être rejetées. <br/>
<br/>
<br/>
       DÉCIDE :<br/>
<br/>
<br/>
Article 1er : La requête de la société CERP-RRM est rejetée.<br/>
2<br/>
N° 18VE02612<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-03-045-03-02 Contributions et taxes. Impositions locales ainsi que taxes assimilées et redevances.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">19-06-04 Contributions et taxes. Taxes sur le chiffre d'affaires et assimilées. Taxes sur le chiffre d`affaires et taxes assimilées autres que la TVA.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
