<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034505303</ID>
<ANCIEN_ID>JG_L_2017_04_000000392317</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/50/53/CETATEXT000034505303.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/04/2017, 392317, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392317</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392317.20170421</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme C...B...ont demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2008. Par un jugement n° 1009930 du 24 février 2012, le tribunal administratif de Montreuil a fait droit à la demande de M. et MmeB.... <br/>
<br/>
              Par un arrêt n° 12VE01930 du 12 mai 2015, la cour administrative d'appel de Versailles a fait droit à l'appel formé par le ministre des finances et des comptes publics contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 août 2015 et 3 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre des finances et des comptes publics ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 61-1 et 88-1 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 267 ;<br/>
              - la directive 90/434/CEE du Conseil du 23 juillet 1990 ; <br/>
              - la convention entre la France et la Belgique tendant à éviter les doubles impositions et à établir les règles d'assistance administrative et juridique réciproque en matière d'impôts sur les revenus signée à Bruxelles le 10 mars 1964 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de M. A...Mme C...B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 27 mai 1992, M. et Mme B...ont fait apport à la société B...Savoye et Cie de 4 152 actions qu'ils détenaient dans la société B...Savoye SA et de 3 actions qu'ils détenaient dans la société Lucas B...SEP. Ils ont reçu en échange 6 042 actions de la société B...Savoye et Cie. La plus-value réalisée à l'occasion de cette opération d'échange a été placée, à leur demande, en report d'imposition conformément aux dispositions alors en vigueur du II de l'article 92 B et du 4 du I ter de l'article 160 du code général des impôts. En 2005, M. et Mme B...ont transféré leur domicile fiscal en Belgique puis, en janvier 2008, ils ont cédé une partie des titres de la société B...Savoye et Cie qu'ils détenaient. Le centre des impôts des non-résidents les a assujettis à l'impôt sur le revenu sur la plus-value d'échange réalisée le 27 mai 1992, à la suite du dépôt d'une déclaration souscrite par les contribuables au titre de leurs revenus de l'année 2008. Par un jugement du 24 février 2012, le tribunal administratif de Montreuil a déchargé M. et Mme B...des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre de l'année 2008 à hauteur de 162 727 euros. M et Mme B...demandent l'annulation de l'arrêt du 12 mai 2015 par lequel la cour administrative d'appel de Versailles, sur appel du ministre des finances et des comptes publics, a annulé ce jugement et rétabli les cotisations supplémentaires d'impôt sur le revenu auxquels ils ont été assujettis au titre de l'année 2008, à hauteur de 162 727 euros. <br/>
<br/>
              2. A l'appui de leur pourvoi en cassation contre cet arrêt, M. et Mme B...soulèvent notamment une question prioritaire de constitutionnalité tirée de la méconnaissance, par les dispositions II de l'article 92 B et du 4 du I ter de l'article 160 du code général des impôts applicables au litige, des principes d'égalité devant la loi et devant les charges publiques.<br/>
<br/>
              Sur le cadre juridique du pourvoi :<br/>
<br/>
              3. Aux termes de l'article 61-1 de la Constitution du 4 octobre 1958 : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation qui se prononce dans un délai déterminé. / Une loi organique détermine les conditions d'application du présent article ". En vertu des dispositions de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, prises pour l'application de ces dispositions constitutionnelles, le Conseil constitutionnel doit être saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
<br/>
<br/>
              4. Le Conseil d'Etat, lorsqu'il est saisi d'un moyen contestant la conformité  d'une disposition législative aux droits et libertés garantis par la Constitution, doit, en se prononçant par  priorité sur le renvoi de la question de constitutionnalité au Conseil constitutionnel, statuer dans un délai de trois mois. En vertu des dispositions de l'article 23-7 de la même ordonnance, si le Conseil d'État ne s'est pas prononcé dans ce délai, la question est transmise au Conseil constitutionnel. Aux termes de l'article 267 du traité sur le fonctionnement de l'Union européenne : " La Cour de justice de l'Union européenne est compétente pour statuer, à titre préjudiciel: a) sur l'interprétation des traités, b) sur la validité et l'interprétation des actes pris par les institutions, organes ou organismes de l'Union. / Lorsqu'une telle question est soulevée devant une juridiction d'un des États membres, cette juridiction peut, si elle estime qu'une décision sur ce point est nécessaire pour rendre son jugement, demander à la Cour de statuer sur cette question. / Lorsqu'une telle question est soulevée dans une affaire pendante devant une juridiction nationale dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne, cette juridiction est tenue de saisir la Cour ".<br/>
<br/>
              5. Les dispositions précitées de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ne font pas obstacle à ce que le juge administratif, juge de droit commun de l'application du droit de l'Union européenne, en assure l'effectivité, soit en l'absence de question prioritaire de constitutionnalité, soit au terme de la procédure d'examen d'une telle question, soit à tout moment de cette procédure, lorsque l'urgence le commande, pour faire cesser immédiatement tout effet éventuel de la loi contraire au droit de l'Union. Le juge administratif dispose de la possibilité de poser à tout instant, dès qu'il y a lieu de procéder à un tel renvoi, en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, une question préjudicielle à la Cour de justice de l'Union européenne. Lorsque l'interprétation ou l'appréciation de la validité d'une disposition du droit de l'Union européenne détermine la réponse à la question prioritaire de constitutionnalité, il appartient au Conseil d'Etat de saisir sans délai la Cour de justice de l'Union européenne <br/>
<br/>
              Sur l'interprétation des dispositions invoquées dans le présent litige :<br/>
<br/>
              6. Aux termes de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents : " 1. L'attribution, à l'occasion d'une fusion, d'une scission ou d'un échange d'actions, de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé. / 2. (...) L'application du paragraphe 1 n'empêche pas les États membres d'imposer le profit résultant de la cession ultérieure des titres reçus de la même manière que le profit qui résulte de la cession des titres existant avant l'acquisition ".<br/>
<br/>
              7. Aux termes du II de l'article 92 B du code général des impôts, dans sa rédaction applicable en l'espèce, résultant de la loi n° 91-716 du 26 juillet 1991 portant diverses dispositions d'ordre économique et financier : " 1° A compter du 1er janvier 1992 ou du 1er janvier 1991 pour les apports de titres à une société passible de l'impôt sur les sociétés, l'imposition de la plus-value réalisée en cas d'échange de titres résultant d'une opération d'offre publique, de fusion, de scission, d'absorption d'un fonds commun de placement par une société d'investissement à capital variable réalisée conformément à la réglementation en vigueur ou d'un apport de titres à une société soumise à l'impôt sur les sociétés, peut être reportée au moment où s'opérera la cession ou le rachat des titres reçus lors de l'échange ". Aux termes du I ter de l'article 160 du code général des impôts, alors en vigueur : " 4. L'imposition de la plus-value réalisée à compter du 1er janvier 1991 en cas d'échange de droits sociaux résultant d'une opération de fusion, scission ou d'apport de titres à une société soumise à l'impôt sur les sociétés peut être reportée dans les conditions prévues au II de l'article 92 B ".<br/>
<br/>
              8. Il résulte des dispositions précitées du code général des impôts, selon l'interprétation constante qui en est donnée par le Conseil d'Etat, statuant au contentieux, qu'elles ont pour seul effet de permettre, par dérogation à la règle suivant laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de constater et de liquider la plus-value d'échange l'année de sa réalisation et de l'imposer l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange. La circonstance que le contribuable ait, entre temps, transféré son domicile fiscal dans un autre Etat est sans incidence sur le pouvoir dont dispose l'Etat, dont il était le résident au moment de la réalisation de la plus-value d'échange, d'imposer celle-ci au moment de la cession finale des titres reçus en échange.<br/>
<br/>
              9. Néanmoins, M. et Mme B...font valoir que ces dispositions, telles qu'interprétées par le Conseil d'Etat, méconnaissent les objectifs résultant des dispositions précitées de l'article 8 de la directive du 23 juillet 1990, en permettant à la France, à l'occasion de la cession des titres reçus lors de l'échange, d'imposer la plus-value réalisée lors de l'échange des titres initialement détenus et placée en report d'imposition. Selon eux, l'opération d'échange d'actions ne peut être regardée comme le fait générateur d'une imposition. Elle devrait être traitée comme une opération intercalaire fiscalement neutre. C'est la cession des titres reçus à l'échange qui constituerait le fait générateur d'une plus-value. Les requérants soutiennent que la cour administrative d'appel de Versailles aurait commis une erreur de droit en n'interprétant pas les dispositions du II de l'article 92 B et du 4 du I ter de l'article 160 du code général des impôts de manière conforme aux dispositions de l'article 8 de la directive du 23 juillet 1990 dont elles assurent la transposition en droit français, dans la mesure où elles s'appliquent de manière uniforme aux opérations d'échange d'actions résultant d'une opération de fusion, de scission ou d'apport, qu'elles soient réalisées entre deux sociétés d'Etats membres différents et donc régies par la directive, ou entre deux sociétés françaises en dehors du champ d'application de la directive.<br/>
<br/>
              10. M. et Mme B...soutiennent, en outre, en soulevant une question prioritaire de constitutionnalité, qu'en cas d'incompatibilité entre les dispositions nationales et le droit communautaire, il en résulte une discrimination à rebours contraire aux principes d'égalité devant la loi et devant les charges publiques, dès lors qu'une plus-value d'échange d'actions entre sociétés françaises serait imposable en vertu des règles nationales, tandis qu'une plus-value d'échange d'actions résultant d'une opération de fusion, de scission ou d'apport entre sociétés d'Etats membres différents ne serait pas taxable conformément aux dispositions de la directive du 23 juillet 1990.<br/>
<br/>
              11. L'interprétation des dispositions nationales et l'appréciation de leur compatibilité avec la directive du 23 juillet 1990 dépendent de la réponse à la question de savoir si les dispositions précitées de l'article 8 de la directive doivent être interprétées en ce sens qu'elles interdisent,  dans le cas d'une opération d'échange de titres entrant dans le champ de la directive, un mécanisme de report d'imposition prévoyant que, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, une plus-value d'échange est constatée et liquidée à l'occasion de l'opération d'échange de titres et est imposée l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange.<br/>
<br/>
              12. Elles dépendent également de la réponse à la question de savoir si, à la supposer imposable, la plus-value d'échange de titres peut être taxée par l'Etat de la résidence du contribuable au moment de l'opération d'échange, alors que celui-ci, à la date de la cession des titres reçus à l'occasion de cet échange à laquelle la plus-value d'échange est effectivement imposée, a transféré son domicile fiscal dans un autre Etat membre.<br/>
<br/>
              13. Les questions énoncées aux points 11 et 12 présentent une difficulté sérieuse d'interprétation du droit de l'Union européenne. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité soulevée par M. et Mme B... :<br/>
<br/>
              14. Selon la réponse apportée aux questions énoncées ci-dessus, il appartiendra au juge de l'impôt, soit de juger que les dispositions contestées doivent être regardées comme incompatibles avec la directive du 23 juillet 1990 et d'en écarter l'application aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, soit de juger qu'elles ne sont pas incompatibles avec la directive, compte tenu, le cas échéant, de la possibilité d'en donner une interprétation conforme aux objectifs de celle-ci. Tant que l'interprétation de l'article 8 de la directive n'aura pas conduit le juge de l'impôt à écarter l'application des dispositions contestées aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, aucune différence dans le traitement fiscal des opérations d'échange n'est susceptible d'en résulter au détriment des plus-value issues d'un échange d'actions entre sociétés françaises. Ainsi, en l'état, la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle, ne peut être regardée comme revêtant un caractère sérieux et il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur les autres moyens du pourvoi en cassation soulevés par M. et Mme B...:<br/>
<br/>
              15. Les questions énoncées aux points 11 et 12 présentent, comme il a été dit ci-dessus, une difficulté sérieuse d'interprétation du droit de l'Union européenne. Toutefois, par une décision n° 393881 du 31 mai 2016, le Conseil d'Etat statuant au contentieux a transmis à la Cour de justice de l'Union européenne les questions préjudicielles suivantes : " Les dispositions de l'article 8 de la directive du 23 juillet 1990 doivent-elles être interprétées en ce sens qu'elles interdisent, dans le cas d'une opération d'échange de titres entrant dans le champ de la directive, un mécanisme de report d'imposition prévoyant que, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, une plus-value d'échange est constatée et liquidée à l'occasion de l'opération d'échange de titres et est imposée l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange ' " et " Les dispositions de l'article 8 de la directive du 23 juillet 1990 doivent-elles être interprétées en ce sens qu'elles interdisent, dans le cas d'une opération d'échange de titres entrant dans le champ de la directive, que la plus-value d'échange de titres, à la supposer imposable, soit taxée par l'Etat de la résidence du contribuable au moment de l'opération d'échange, alors que celui-ci, à la date de la cession des titres reçus à l'occasion de cet échange à laquelle la plus-value d'échange est effectivement imposée, a transféré son domicile fiscal dans un autre Etat membre ' ". Il y a donc lieu, dans la présente affaire, de surseoir à statuer sur le pourvoi en cassation présenté par M. et Mme B...dans l'attente de la réponse de la Cour à ces questions.<br/>
<br/>
              16. Dans le cas où, à la suite de la décision de la Cour de justice de l'Union européenne, les requérants présenteraient à nouveau au Conseil d'Etat la question prioritaire de constitutionnalité invoquée, l'autorité de la chose jugée par la présente décision du Conseil d'Etat ne ferait pas obstacle au réexamen de la conformité à la Constitution des dispositions du II de l'article 92 B et du 4 du I ter de l'article 160 du code général des impôts.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et MmeB....<br/>
Article 2 : Il est sursis à statuer sur le pourvoi en cassation de M. et Mme B...jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions, mentionnées au point 15, dont elle a été saisie par le Conseil d'Etat statuant au contentieux dans la décision n° 393881 du 31 mai 2016. <br/>
Article 3 : La présente décision sera notifiée à M. et Mme C...B...et au ministre de l'économie et des finances. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
