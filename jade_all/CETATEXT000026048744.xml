<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026048744</ID>
<ANCIEN_ID>JG_L_2012_06_000000345750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/04/87/CETATEXT000026048744.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/06/2012, 345750</TITRE>
<DATE_DEC>2012-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:345750.20120620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 janvier et 16 mars 2011 au secrétariat du contentieux du Conseil d'État, présentés pour M. Eric  A, détenu ... ; M. A demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07LY01483 du 21 janvier 2010 de la cour administrative d'appel de Lyon en tant qu'il a confirmé le jugement du 28 juin 2007 par lequel le tribunal administratif de Clermont-Ferrand a rejeté sa demande d'annulation de la décision implicite par laquelle le directeur de la maison centrale de Moulins-Yzeure a rejeté sa demande tendant à la rectification d'erreurs entachant la gestion du compte de ses valeurs pécuniaires ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler la décision implicite du directeur de la maison centrale de Moulins-Yzeure ;<br/>
<br/>
              3°) d'enjoindre au ministre de la justice d'ordonner aux services compétents de la maison centrale de Moulins-Yzeure, d'une part, de rectifier le compte de valeurs pécuniaires de M. A en ce qui concerne les sommes dues aux parties civiles en application du V du dispositif de l'arrêt de la cour d'assises du Rhône du 22 octobre 1999 et, d'autre part, d'autoriser ce dernier à opérer des retraits sur les sommes versées sur son livret d'épargne à partir de sa part disponible, en vue d'effectuer des versements volontaires aux victimes ;<br/>
<br/>
              4°) de mettre à la charge de l'État la somme de 2 000 euros à verser à la SCP Waquet-Farge-Hazan, avocat de M. A, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de M. A, <br/>
<br/>
           - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A a demandé, le 12 mars 2006, au directeur de la maison centrale de Moulins-Yzeure où il est détenu, de rectifier différentes erreurs entachant la gestion du compte de ses valeurs pécuniaires relatives notamment au montant des sommes dont il était constitué débiteur au titre du remboursement des frais irrépétibles dus aux parties civiles et au refus d'effectuer sur son livret d'épargne les prélèvements volontaires qu'il souhaitait réaliser au profit du fonds de garantie subrogé dans les droits des victimes ; que, par un jugement du 28 juin 2007, le tribunal administratif de Clermont-Ferrand a rejeté la requête de M. A tendant à l'annulation de la décision implicite de rejet qui lui a été opposée ; que, par un arrêt du 21 janvier 2010 contre lequel M. A se pourvoit en cassation, la cour administrative d'appel de Lyon, après avoir partiellement annulé le jugement du tribunal administratif, a confirmé le rejet de la demande d'annulation de la décision implicite de rejet du directeur de la maison centrale de Moulins-Yzeure ;<br/>
<br/>
              Considérant qu'aux termes de l'article 728-1 du code de procédure pénale : " Les valeurs pécuniaires des détenus, inscrites à un compte nominatif ouvert à l'établissement pénitentiaire, sont divisées en trois parts : la première sur laquelle seules les parties civiles et les créanciers d'aliments peuvent faire valoir leurs droits ; la deuxième, affectée au pécule de libération, qui ne peut faire l'objet d'aucune voie d'exécution ; la troisième, laissée à la libre disposition des détenus. / Les sommes destinées à l'indemnisation des parties civiles leur sont versées directement, sous réserve des droits des créanciers d'aliments, à la demande du procureur de la République, par l'établissement pénitentiaire (...). /  La consistance des valeurs pécuniaires, le montant respectif des parts et les modalités de gestion du compte nominatif sont fixés par décret " ; que l'article D. 320-1 du même code prévoit que : " La première part, affectée à l'indemnisation des parties civiles et créanciers d'aliments, est déterminée en appliquant à la fraction des sommes qui échoient aux détenus les taux de : / - 20 %, pour la fraction supérieure à 200 euros et inférieure ou égale à 400 euros ; / - 25 %, pour la fraction supérieure à 400 euros et inférieure ou égale à 600 euros ; / - 30 %, pour la fraction supérieure à 600 euros. (...) " ; que l'article D. 320-2 du même code précise que : " La deuxième part, affectée à la constitution du pécule de libération, est déterminée en appliquant à la fraction des sommes qui échoient aux détenus le taux de 10 % (...) " ; qu'aux termes de l'article D. 320-3 du même code : " La troisième part, laissée à la libre disposition des détenus, correspond aux sommes restantes après que les prélèvements prévus aux articles D. 320 à D. 320-2 ont été opérés " ;<br/>
<br/>
              Considérant, en premier lieu, qu'en jugeant qu'il ressort clairement du dispositif de l'arrêt du 22 octobre 1999 de la cour d'assises du Rhône que la somme de 6 000 francs a été allouée, comme celle de 25 000 francs correspondant à l'indemnisation du préjudice moral, à chacune des parties civiles citées au titre des frais non payés par l'État et exposés par elles, la cour administrative d'appel, qui a suffisamment motivé son arrêt, n'a pas dénaturé les pièces du dossier ni méconnu le principe de l'autorité de la chose jugée qui s'attache au dispositif de l'arrêt et aux motifs qui en sont le soutien nécessaire ;<br/>
<br/>
              Considérant, en second lieu, qu'aux termes de l'article D. 324 du code de procédure pénale, dans sa rédaction applicable à la date de la décision de l'administration pénitentiaire : " Les sommes constituant le pécule de libération sont inscrites à un compte spécial ; lorsqu'elles dépassent une somme fixée par arrêté du garde des sceaux, ministre de la justice, elles sont versées à un livret de caisse d'épargne. (...) / Pendant l'incarcération, le pécule de libération est indisponible et ne peut faire l'objet d'aucune voie d'exécution " ; que l'article D.331 du même code dispose que : " Les détenus peuvent verser sur leur livret de caisse d'épargne des sommes prélevées sur leur part disponible. / Les opérations éventuelles de retrait sont subordonnées, pendant la détention, à l'accord du chef d'établissement " ; qu'il résulte de ces dispositions que la disponibilité des sommes placées sur le livret d'épargne du détenu s'apprécie en fonction de la part du compte nominatif à laquelle ces sommes continuent d'appartenir ; qu'ainsi, si les sommes issues de la part disponible peuvent, avec l'accord du chef d'établissement, faire l'objet d'un retrait, le cas échéant pour procéder à un versement volontaire au profit des victimes, les sommes placées sur le livret d'épargne et appartenant au pécule de libération ne peuvent pas, en principe, faire l'objet d'un retrait ;<br/>
<br/>
              Considérant qu'en jugeant que M. A n'était pas fondé à invoquer l'illégalité du refus d'autorisation de prélèvement aux fins de versement aux parties civiles opposé par l'administration pénitentiaire en raison du caractère indisponible du pécule de libération, sans rechercher si les sommes dont le détenu demandait le retrait, à partir de son livret d'épargne, provenaient du pécule de libération ou de la part disponible, la cour a commis une erreur de droit ; que, dès lors, son arrêt doit, dans cette mesure, être annulé ;<br/>
<br/>
              Considérant qu'en application de l'article L. 821-2 du code de justice administrative,  il y a lieu, dans cette mesure, de régler l'affaire au fond ;<br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus que M. A est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Clermont-Ferrand a rejeté, pour le même motif, son recours dirigé contre le refus de l'administration pénitentiaire d'autoriser le prélèvement de sommes sur son livret d'épargne aux fins de versement aux parties civiles ; que, pour les mêmes motifs, il y a lieu d'annuler la décision attaquée ; qu'il appartiendra à l'administration pénitentiaire, après avoir identifié l'origine des sommes placées sur le livret d'épargne de M. A, d'en déterminer la disponibilité pour se prononcer sur la demande de prélèvement ;<br/>
<br/>
              Considérant que M. A a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Waquet-Farge-Hazan, avocat de M. A, renonce à percevoir la somme correspondant à la part contributive de l'État, de mettre à la charge de l'État la somme de 1 500 euros à verser à la SCP Waquet-Farge-Hazan ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 21 janvier 2010 et le jugement du tribunal administratif de Clermont-Ferrand du 28 juin 2007 sont annulés en tant qu'ils ont rejeté la demande d'annulation de la décision implicite du directeur de la maison centrale de Moulins-Yzeure rejetant la demande de M. A de prélèvement de sommes sur son livret d'épargne aux fins de versement aux parties civiles.<br/>
<br/>
Article 2 : La décision du directeur de la maison centrale de Moulins-Yzeure rejetant la demande de M. A de prélèvement de sommes sur son livret d'épargne aux fins de versement aux parties civiles est annulée.<br/>
<br/>
Article 3 : L'État versera à la SCP Waquet-Farge-Hazan, avocat de M. A, une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'État.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. A est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. Eric A et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. - DISPONIBILITÉ DES SOMMES VERSÉES SUR LE LIVRET D'ÉPARGNE D'UN DÉTENU.
</SCT>
<ANA ID="9A"> 37-05-02 La disponibilité des sommes placées sur le livret d'épargne d'un détenu s'apprécie en fonction de la part du compte nominatif à laquelle ces sommes continuent d'appartenir. Ainsi, si les sommes issues de la part disponible peuvent, avec l'accord du chef d'établissement, faire l'objet d'un retrait, le cas échéant pour procéder à un versement volontaire au profit des victimes, les sommes placées sur le livret d'épargne et appartenant au pécule de libération ne peuvent pas, en principe, faire l'objet d'un retrait.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
