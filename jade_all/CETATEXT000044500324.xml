<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044500324</ID>
<ANCIEN_ID>JG_L_2021_12_000000440589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/03/CETATEXT000044500324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 14/12/2021, 440589, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:440589.20211214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a demandé au tribunal administratif de Clermont-Ferrand de condamner le centre hospitalier universitaire (CHU) de Clermont-Ferrand et son assureur, la société hospitalière d'assurances mutuelles (SHAM), à lui verser la somme de 121 908,75 euros au titre des sommes versées à un patient en réparation du dommage subi par celui-ci et la somme de 18 286,30 euros sur le fondement de l'article L. 1142-15 du code de la santé publique. La caisse primaire d'assurance maladie (CPAM) du Puy-de-Dôme a demandé au tribunal administratif de condamner le CHU de Clermont-Ferrand et la SHAM au remboursement de ses débours. Par un jugement n° 1501958 du 6 février 2018, le tribunal administratif a condamné le CHU de Clermont-Ferrand et la SHAM à verser à l'ONIAM la somme de 90 306,45 euros et à la CPAM du Puy-de-Dôme la somme de 31 281,17 euros. <br/>
<br/>
              Par un arrêt n° 18LY01439, 18LY01482 du 10 mars 2020, la cour administrative d'appel de Lyon a, sur appels de l'ONIAM, du CHU de Clermont-Ferrand et de la SHAM, porté à 90 983,70 euros la somme que le CHU est condamné à verser à l'ONIAM.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 mai et 31 juillet 2020 et le 5 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du CHU de Clermont-Ferrand et de la SHAM la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Me Le Prado, avocat du centre hospitalier universitaire de Clermont-Ferrand et de la société hospitalière d'assurances mutuelles.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une intervention chirurgicale réalisée au centre hospitalier universitaire (CHU) de Clermont Ferrand ayant consisté en une embolisation bilatérale des artères maxillaires d'un patient souffrant d'hémorragie, ce dernier a souffert d'une occlusion de l'artère centrale de la rétine, entraînant la perte fonctionnelle de l'œil gauche. Par un jugement du 6 février 2018, le tribunal administratif de Clermont Ferrand a condamné le CHU de Clermont-Ferrand et son assureur, la société hospitalière d'assurances mutuelles (SHAM) à rembourser à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) la somme de 90 306,45 euros et à la caisse primaire d'assurance maladie du Puy-de-Dôme la somme de 31 281,17 euros au titre de ses débours. Par un arrêt du 10 mars 2020, la cour administrative d'appel de Lyon a, sur appels de l'ONIAM, du CHU de Clermont-Ferrand et de la SHAM, porté à 90 983,70 euros la somme que le CHU de Clermont-Ferrand est condamné à verser à l'ONIAM. L'ONIAM se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Il ressort des termes mêmes de l'arrêt attaqué que, pour se prononcer sur les conclusions indemnitaires présentées par l'ONIAM, il juge, dans un premier temps, qu'en faisant le choix de recourir à la technique de l'embolisation des vaisseaux irriguant la fosse nasale pour mettre fin à l'hémorragie dont souffrait le patient, alors que cette technique, contrairement à la technique alternative de ligature de l'artère sphénopalatine, comporte un faible risque d'entraîner une cécité, le CHU de Clermont-Ferrand avait, par le choix de cette démarche thérapeutique, commis une faute en lien direct avec le préjudice subi par l'intéressé. Par suite, en en déduisant, dans un second temps, que cette faute n'était à l'origine que d'une perte de chance de se soustraire à la survenue du dommage, alors qu'il résultait de ses propres constatations que, sans la faute commise dans le choix de l'indication thérapeutique, le patient n'aurait pas perdu l'usage de l'œil gauche, la cour a commis une erreur de droit qui justifie l'annulation de son arrêt.<br/>
<br/>
              3. Il résulte de ce qui précède que les conclusions du pourvoi incident du CHU de Clermont-Ferrand et de la SHAM qui tendent à l'annulation du même arrêt sont devenues sans objet. Il n'y a dès lors pas lieu d'y statuer.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'ONIAM les sommes que demandent, à ce titre, le CHU de Clermont-Ferrand et la SHAM. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHU de Clermont-Ferrand et de la SHAM le versement à l'ONIAM d'une somme de 1 500 euros chacun au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 10 mars 2020 est annulé.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions du pourvoi incident du CHU de Clermont-Ferrand et de la SHAM tendant à l'annulation du même arrêt.<br/>
<br/>
		Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 4 : Le CHU de Clermont-Ferrand et la SHAM verseront à l'ONIAM une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions présentées par le CHU de Clermont-Ferrand et la SHAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et infections nosocomiales, au centre hospitalier universitaire de Clermont-Ferrand et à la société hospitalière d'assurances mutuelles.<br/>
Copie en sera adressée à la caisse primaire d'assurance maladie du Puy-de-Dôme.<br/>
              Délibéré à l'issue de la séance du 18 novembre 2021 où siégeaient : M. Denis Piveteau, président de chambre, présidant ; M. Olivier Yeznikian, conseiller d'Etat et M. Florian Roussel, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 14 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Denis Piveteau<br/>
 		Le rapporteur : <br/>
      Signé : M. Florian Roussel<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
