<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032484123</ID>
<ANCIEN_ID>JG_L_2016_05_000000388615</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/48/41/CETATEXT000032484123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 02/05/2016, 388615, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388615</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:388615.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Grenoble d'annuler la décision du 14 septembre 2011 par laquelle le préfet de Haute-Savoie a refusé de procéder à l'échange de son permis de conduire délivré par la mission d'administration provisoire des Nations-Unies au Kosovo (MINUK) contre un permis de conduire français ainsi que la décision par laquelle il a rejeté son recours gracieux. Par un jugement n° 1304106 du 24 décembre 2014, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi enregistré le 11 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant kosovar, s'est vu délivrer en octobre 2003 un permis de conduire d'une durée de validité de dix ans par la mission d'administration provisoire des Nations Unies au Kosovo (MINUK) ; qu'ayant obtenu un titre de séjour français en qualité de réfugié le 19 septembre 2008, il a demandé le 12 août 2011 au préfet de la Haute-Savoie de procéder à l'échange de ce permis de conduire contre un permis français ; que le préfet a rejeté cette demande par une décision du 14 septembre 2011 au motif qu'elle avait été présentée au-delà du délai d'un an prévu par les dispositions de l'article R. 222-3 du code de la route ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 24 décembre 2014 par lequel le tribunal administratif de Grenoble a annulé la décision du préfet de Haute-Savoie du 14 septembre 2011 ainsi que sa décision rejetant le recours gracieux de M. A... ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. (...) Au terme de ce délai, ce permis n'est plus reconnu et son titulaire perd tout droit de conduire un véhicule pour la conduite duquel le permis de conduire est exigé " ; qu'aux termes de l'article 10.2 de l'arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen , alors applicable : " Le délai d'un an de reconnaissance et d'échange du permis de conduire étranger d'un ressortissant étranger ayant sollicité une carte de l'OFPRA court à compter de la date d'établissement du titre de séjour provisoire, si le principe de réciprocité est appliqué par le pays d'origine du permis (...) " ; qu'aux termes de l'article 6 du même arrêté : " Tout titulaire d'un permis de conduire national doit obligatoirement demander l'échange de ce titre contre le permis français pendant le délai d'un an qui suit l'acquisition de sa résidence normale en France, la date d'acquisition de cette résidence étant celle d'établissement effectif du premier titre  de séjour ou de résident. (...) Enfin, l'échange demeure possible ultérieurement si, pour des raisons d'âge ou pour des motifs légitimes d'empêchement, il n'a pu être effectué dans le délai prescrit " ;<br/>
<br/>
              3. Considérant que, pour annuler les décisions litigieuses, le jugement attaqué retient que l'échange des permis de conduire délivrés par la MINUK n'a été possible qu'à compter du 4 octobre 2010, date à laquelle le Conseil d'Etat a émis sur le fondement de l'article L. 113-1 du code de justice administrative un avis contentieux selon lequel les permis de conduire délivrés par la MINUK devaient être regardés, pour l'application des dispositions de l'article R. 222-3 du code de la route et de l'arrêté du 8 novembre 1999, comme remplissant la condition tenant à la délivrance par un Etat ou au nom d'un Etat ; que le tribunal en a déduit que M. A...justifiait avant le 4 octobre 2010 d'un motif légitime d'empêchement au sens de l'article 6 de l'arrêté du 8 février 1999, ayant eu pour effet de repousser jusqu'à cette date le point de départ du délai d'échange d'un an ; <br/>
<br/>
              4. Considérant, toutefois, que, par son avis contentieux du 4 octobre 2010, le Conseil d'Etat s'est borné à constater que les titulaires de permis de conduire délivrés par la MINUK tiraient des dispositions réglementaires en vigueur le droit d'en obtenir l'échange contre des permis de conduire français ; qu'il appartenait aux intéressés, qui détenaient ce droit dès avant l'intervention de l'avis, de demander l'échange de leur permis dans le délai fixé par ces dispositions puis, en cas de refus, d'exercer un recours pour excès de pouvoir devant la juridiction administrative afin d'en faire censurer la méconnaissance ; que, dès lors, en jugeant que l'échange était impossible avant l'intervention de l'avis du 4 octobre 2010, le tribunal a commis une erreur de droit qui justifie l'annulation de son jugement ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit, M. A...a obtenu son premier titre de séjour le 19 septembre 2008 ; qu'en application des dispositions précitées, il disposait d'un délai d'un an à compter de cette date pour demander l'échange de son permis de conduire délivré par la MINUK ; qu'il résulte de ce qui a été dit ci-dessus que la circonstance que l'avis contentieux du Conseil d'Etat du 4 octobre 2010 n'est intervenu qu'après l'expiration de ce délai n'implique pas l'existence d'un motif légitime d'empêchement ; que, dès lors, c'est par une exacte application des dispositions de l'article R. 222-3 du code de la route que le préfet de la Haute-Savoie a rejeté la demande dont il a été saisi le 12 août 2011 ; que, si l'intéressé soutient qu'il avait présenté dans le délai d'un an des demandes d'échange qui avait été illégalement rejetées, il lui appartenait de présenter un recours contre ces décisions ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision du 14 septembre 2011 du préfet de Haute-Savoie ni de la décision rejetant son recours gracieux ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 24 décembre 2014 du tribunal administratif de Grenoble est annulé. <br/>
<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Grenoble est rejetée. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
