<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029308650</ID>
<ANCIEN_ID>JG_L_2014_06_000000362705</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/30/86/CETATEXT000029308650.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 18/06/2014, 362705, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362705</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362705.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 septembre 2012 et 11 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme E...A..., épouseB..., demeurant... ; Mme A... épouse B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 11007684 du 30 janvier 2012 par laquelle la cour nationale du droit d'asile a rejeté sa demande tendant à l'annulation de la décision du 25 février 2011 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP de Chaisemartin-Courjon, avocat de MmeD..., sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 mai 2014, présentée pour Mme A... ; <br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, Auditeur,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de Mme A... épouse B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte du dossier soumis à la Cour nationale du droit d'asile que Mme A...épouseB..., qui déclare être de nationalité azerbaïdjanaise et d'origine arménienne, est arrivée sur le territoire français en décembre 2009 et a demandé, à raison d'agressions et de harcèlements dont elle-même et les membres de sa famille auraient fait l'objet sur le territoire de la Fédération de Russie où elle faisait état d'une résidence continue depuis 1990, le bénéfice du statut de réfugié au titre de la Convention de Genève du 28 juillet 1951 ou, à défaut, de la protection subsidiaire au titre de l'article L. 721-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'elle se pourvoit en cassation contre la décision du 30 janvier 2012 par laquelle la Cour nationale du droit d'asile a rejeté son recours tendant à l'annulation de la décision du 25 janvier 2011 du directeur de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile ;<br/>
<br/>
              2. Considérant, en premier lieu, que l'erreur de plume qui affecte la décision de l'OFPRA en la datant, de manière inexacte, du 25 janvier 2011, soit antérieurement à l'entretien avec un officier de protection, est dénuée d'incidence sur la régularité de la procédure conduite par l'office ; qu'ainsi, et en tout état de cause, c'est sans affecter la régularité de sa décision que la CNDA a pu s'abstenir de répondre au moyen tiré du vice qui aurait entaché la procédure devant l'OFPRA ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que si Mme A... épouse B...soutient qu'il ne ressort pas de la décision attaquée que la cour a statué au regard des éléments produits devant elle, postérieurement à la décision de l'office, il ressort des mentions de sa décision, qui vise " les autres pièces du dossier ", qu'elle a pris en compte l'ensemble des éléments qui lui ont été soumis, qu'elle n'était pas tenue de mentionner en détail ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes du 2° du A de l'article 1er de la Convention de Genève du 28 juillet 1951, la qualité de réfugié est reconnue à toute personne qui, craignant d'être persécutée, " se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays; ou qui, si elle n'a pas de nationalité et se trouve hors du pays dans lequel elle avait sa résidence habituelle à la suite de tels évènements, ne peut ou, en raison de ladite crainte ne veut y retourner " ;<br/>
<br/>
              5. Considérant que c'est par une appréciation souveraine, exempte de dénaturation, que pour déterminer la nationalité de l'intéressée, la cour a estimé, après avoir écarté la nationalité azerbaïdjanaise de la requérante par des motifs qui ne sont pas contestés en cassation, d'une part, que l'article 13 de la loi du 28 novembre 1991 sur la nationalité de la Fédération de Russie devait être interprété comme reconnaissant de plein droit cette nationalité aux ressortissants de l'ex-Union soviétique qui, à la date de son entrée en vigueur, le 6 février 1992, avaient une résidence permanente sur le territoire de la Fédération et, d'autre part, que cette condition de résidence renvoyait à une résidence effective, indépendante de toute formalité d'enregistrement légale ; qu'en jugeant, au regard de cette interprétation, que, bien que ne disposant pas de documents d'identité russes, Mme C...était " en droit de se réclamer " de la nationalité de ce pays, la cour, qui a ainsi expressément statué sur la nationalité de la requérante, n'a pas méconnu son office ; qu'elle n'a pas davantage entaché sa décision de dénaturation en relevant que l'intéressée n'établissait pas qu'elle aurait vainement engagé des démarches en vue d'acquérir cette nationalité ; qu'il résulte de ce qui précède que c'est sans erreur de droit que la cour a jugé que le seul pays au regard duquel ses craintes de persécution devaient être analysées était la Fédération de Russie ;<br/>
<br/>
              6. Considérant, en dernier lieu, que c'est par une appréciation souveraine, exempte de dénaturation, que la cour a estimé qu'il ne résultait ni de l'instruction ni des déclarations de la requérante, d'une part, que les autorités russes lui auraient opposé un refus ou ne seraient pas en mesure de lui offrir une protection pour l'un des motifs énumérés par les stipulations du 2° du A de l'article 1er de la Convention de Genève, d'autre part, qu'elle serait susceptible d'être exposée à une menace grave de traitements inhumains ou dégradants en cas de retour en Fédération de Russie ; pour en déduire qu'elle n'était fondée à se voir reconnaître ni la qualité de réfugié ni le bénéfice de la protection subsidiaire ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme A...épouse B...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er: Le pourvoi de Mme A...épouse B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme E... A...épouse B...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
