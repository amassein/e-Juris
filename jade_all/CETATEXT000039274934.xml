<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274934</ID>
<ANCIEN_ID>JG_L_2019_10_000000420647</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 24/10/2019, 420647, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420647</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420647.20191024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société La Suite Villa a demandé au tribunal administratif de la Martinique, d'une part, de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie dans les rôles de la commune des Trois-Ilets au titre des années 2011 à 2015 ainsi que des pénalités correspondantes et, d'autre part, d'ordonner le versement d'intérêts moratoires. <br/>
<br/>
              Par un jugement n° 1700274 du 20 mars 2018, le tribunal administratif de la Martinique a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 mai et 25 juillet 2018 et le 22 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la société La Suite Villa demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société La Suite Villa ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société La Suite Villa a demandé la réduction des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2011 à 2015. Par une décision du 24 février 2017, le directeur régional des finances publiques de la Martinique a partiellement fait droit à la réclamation de la société en ce qui concerne la taxe foncière sur les propriétés bâties au titre des années 2014 et 2015 et a rejeté la réclamation en tant qu'elle porte sur la taxe foncière sur les propriétés bâties au titre des années 2011 à 2013 et sur la taxe d'enlèvement des ordures ménagères au titre de l'ensemble des années. La société La Suite Villa se pourvoit en cassation contre le jugement du 20 mars 2018 par lequel le tribunal administratif de la Martinique a rejeté sa demande tendant à la décharge des impositions et pénalités restant à sa charge.<br/>
<br/>
              Sur le bien-fondé du jugement en ce qui concerne les impositions au titre des années 2011 à 2013 : <br/>
<br/>
              2. Aux termes de l'article R. 196-2 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts directs locaux et aux taxes annexes doivent être présentées à l'administration des impôts au plus tard le 31 décembre de l'année suivant celle, selon le cas : / a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement. / (...) ". <br/>
<br/>
              3. Il résulte de cette disposition que le délai de réclamation court, en ce qui concerne la taxe foncière sur les propriétés bâties, à compter de la date de mise en recouvrement. Toutefois, lorsqu'il est établi que le contribuable n'a pas reçu l'avis d'imposition du fait d'une erreur de l'administration, le point de départ du délai de réclamation ne court qu'à compter de la date où il a connaissance de l'impôt. Par suite, le tribunal administratif a commis une erreur de droit en jugeant inopérant le moyen tiré de ce que la société La Suite Villa n'aurait reçu qu'en 2015 et 2016 les avis d'imposition correspondant aux impositions des années 2011 à 2013 au motif que le délai de réclamation court dès la mise en recouvrement, alors que la société ne le contestait pas mais faisait valoir qu'elle n'avait pas reçu les avis d'imposition. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi au titre des années 2011 à 2013, la société requérante est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il porte sur ces impositions et sur les pénalités correspondantes, à l'exception des pénalités correspondant à l'année 2011 qui n'ont pas fait l'objet d'une réclamation préalable.<br/>
<br/>
              Sur le bien-fondé du jugement en ce qui concerne la taxe d'enlèvement des ordures ménagères au titre des années 2014 et 2015 : <br/>
<br/>
              4. Aux termes de l'article 1388 du code général des impôts : " La taxe foncière sur les propriétés bâties est établie d'après la valeur locative cadastrale de ces propriétés déterminées conformément aux principes définis par les articles 1494 à 1508 et 1516 à 1518 B et sous déduction de 50 % de son montant en considération des frais de gestion, d'assurances, d'amortissement, d'entretien et de réparation ". Aux termes de l'article 1388 quinquies du même code : " I.- Sauf délibération contraire de la collectivité territoriale ou de l'établissement public de coopération intercommunale doté d'une fiscalité propre prise dans les conditions prévues au I de l'article 1639 A bis, la base d'imposition à la taxe foncière sur les propriétés bâties des immeubles ou parties d'immeubles rattachés entre le 1er janvier 2009 et le 31 décembre 2018 à un établissement réunissant les conditions requises pour bénéficier de l'abattement prévu à l'article 1466 F fait l'objet d'un abattement dégressif lorsqu'ils sont situés en Guadeloupe, en Guyane, en Martinique, à Mayotte ou à La Réunion ". Aux termes du I de l'article 1522 du même code : " I. - La taxe est établie d'après le revenu net servant de base à la taxe foncière, défini par l'article 1388 ". <br/>
<br/>
              5. Il résulte des dispositions précitées que les abattements portant sur l'assiette de la taxe foncière sur les propriétés bâties ne sont pas applicables à l'assiette de la taxe d'enlèvement des ordures ménagères. Par suite, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que l'abattement institué par l'article 1388 quinquies du code général des impôts n'a pas à être pas pris en compte pour le calcul de l'assiette de la taxe d'enlèvement des ordures ménagères. Les conclusions du pourvoi doivent être rejetées dans cette mesure. <br/>
<br/>
              Sur le bien-fondé du jugement en tant qu'il porte sur les pénalités au titre des années 2014 et 2015 : <br/>
<br/>
              6. Le tribunal administratif a insuffisamment motivé son jugement et commis une erreur de droit en jugeant que la société était passible au titre de ces années de la pénalité prévue à l'article 1730 du code général des impôts sans rechercher si elle devait être regardée comme ayant eu connaissance des impositions qui lui étaient réclamées avant le délai fixé pour un paiement. La société est fondée, dans cette mesure, à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              Sur les conclusions au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la société La Suite Villa au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de la Martinique du 20 mars 2018 est annulé en tant qu'il statue sur les cotisations de taxe foncière sur les propriétés bâties et sur les cotisations de taxe d'enlèvement des ordures ménagères auxquelles la société a été assujettie au titre des années 2011 à 2013 et sur les pénalités prévues par l'article 1730 au titre des impositions des années 2012 à 2015.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de la Martinique.<br/>
Article 3 : L'Etat versera la somme de 1 500 euros à la société La Suite Villa au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de la société La Suite Villa est rejeté. <br/>
Article 5 : La présente décision sera notifiée à la société La Suite Villa et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
