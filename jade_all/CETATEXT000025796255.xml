<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025796255</ID>
<ANCIEN_ID>JG_L_2012_04_000000338777</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/79/62/CETATEXT000025796255.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/04/2012, 338777</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338777</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean-Philippe Thiellay</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:338777.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 avril et 19 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Sébastien A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0705007 du 4 février 2010 par lequel le tribunal administratif de Toulouse a condamné l'Etat à lui verser une indemnité de 10 000 euros en réparation des préjudices ayant résulté pour lui du refus de concours de la force publique qui lui a été opposé par le préfet de la Haute-Garonne le 3 juillet 2007 et a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions présentées devant le tribunal administratif de Toulouse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la consommation ;<br/>
<br/>
              Vu la loi n° 91-650 du 9 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, le 3 mai 2007, M. A, propriétaire d'un appartement situé 12, chemin de Pelleport à Toulouse, a demandé au préfet de la Haute-Garonne de lui prêter le concours de la force publique pour l'exécution d'une décision du 8 septembre 2006 du juge des référés du tribunal d'instance de Toulouse ordonnant l'expulsion de Mme B, qui avait pris les lieux à bail en mars 2005 et ne versait plus les loyers ; qu'à la suite du rejet implicite de cette demande, acquis le 3 juillet 2007, l'intéressé a recherché la responsabilité de l'Etat devant le tribunal administratif de Toulouse ; que, par un jugement du 4 février 2010, le tribunal a déclaré l'Etat responsable des préjudices ayant résulté pour le propriétaire de l'occupation irrégulière de son bien entre le 3 juillet 2007 et le 15 juin 2009, date de la libération des lieux, et fixé à 10 000 euros le montant de l'indemnité due à M. A ; que ce dernier et, par la voie d'un pourvoi incident, le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration demandent l'annulation du jugement ; <br/>
<br/>
              Sans qu'il soit besoin de statuer sur le pourvoi incident et sans qu'il soit besoin d'examiner les autres moyens du pourvoi principal ;<br/>
<br/>
              Considérant que, pour fixer l'indemnité mise à la charge de l'Etat, le jugement attaqué énonce que M. A peut prétendre à la réparation du préjudice correspondant au montant des loyers et charges locatives dont il a été privé pendant la période de responsabilité, puis évalue ce préjudice à la somme de 10 000 euros ; qu'en retenant ce chiffre sans s'en expliquer, alors que, dans un mémoire enregistré le 14 janvier 2010, l'intéressé avait présenté un état détaillé de ses pertes de loyers et de charges récupérables, pour un montant total de 14 711,61 euros, le juge du fond a entaché son jugement d'une insuffisance de motivation ; que M. A est, par suite, fondé à en demander l'annulation ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Sur la responsabilité :<br/>
<br/>
              Considérant qu'aux termes de l'article 16 de la loi du 9 juillet 1991 : " L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation " ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'à la date du 3 juillet 2007, à laquelle le préfet de la Haute-Garonne a implicitement refusé d'accorder à M. A le concours de la force publique, la décision du 8 septembre 2006 du juge des référés du tribunal d'instance de Toulouse était exécutoire ; que, par suite, le rejet de la réquisition dont l'administration avait été régulièrement saisie a engagé la responsabilité de l'Etat à l'égard du propriétaire ; que si la commission départementale de surendettement de la Haute-Garonne a, le 28 décembre 2007, saisi le juge de l'exécution aux fins d'ouverture d'une procédure de rétablissement personnel de Mme B et si, par application des dispositions alors en vigueur de l'article L. 331-3-1 du code de la consommation, cette saisine a emporté suspension des voies d'exécution à l'encontre de l'intéressée, y compris les mesures d'expulsion du logement, cette circonstance postérieure à la date à laquelle le concours de la force publique a été refusé et indépendante de la volonté du propriétaire n'a pas, contrairement à ce que soutient le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, eu pour effet de suspendre la responsabilité de l'Etat ; qu'il incombe à ce dernier de réparer l'ensemble des préjudices que l'occupation irrégulière a causés au propriétaire entre le 3 juillet 2007 et le 15 juin 2009, date à laquelle les lieux ont été libérés ; <br/>
<br/>
              Sur le préjudice :<br/>
<br/>
              En ce qui concerne les pertes de loyers et de charges locatives :<br/>
<br/>
              Considérant que, contrairement à ce que soutient le ministre, la circonstance que la décision du 18 mars 2009 par laquelle le juge de l'exécution du tribunal d'instance de Toulouse a ordonné la clôture pour insuffisance d'actif de la procédure de rétablissement personnel ouverte à l'égard de Mme B a effacé les dettes de l'intéressée antérieures au 21 mai 2008 est sans incidence sur le droit de M. A d'obtenir réparation des pertes de loyers qu'il a subies avant cette date ; que les pertes subies entre le 3 juillet 2007 et le 15 juin 2009 doivent être regardées comme une conséquence directe du refus de concours de la force publique qui a rendu possible la poursuite de l'occupation irrégulière ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que le bail de Mme B stipulait un loyer de 516 euros par mois ; que les pertes de loyers calculées sur cette base pendant la période de responsabilité de l'Etat s'élèvent à 12 216 euros, auxquels il convient d'ajouter une somme de 1 498 euros correspondant au montant des charges incombant au locataire ; que si M. A demande qu'il soit tenu compte des augmentations légales du montant du loyer, il ressort d'un courrier adressé par lui à sa locataire le 10 mars 2008 qu'il ne les lui appliquait pas ; qu'ainsi le préjudice résultant des pertes de loyers et de charges s'élève à 13 624 euros ; qu'il y a lieu toutefois de déduire de ce montant la provision de 9 200 euros augmentée des intérêts au taux légal accordée à M. A au titre des pertes de loyers et de charges par une ordonnance du 27 novembre 2008 du juge des référés de la cour administrative d'appel de Bordeaux ; que le complément d'indemnité dû par l'Etat pour ce chef de préjudice s'élève donc à 4 424 euros ; <br/>
<br/>
              En ce qui concerne les autres chefs de préjudice invoqués :<br/>
<br/>
              Considérant qu'il ne résulte pas de l'instruction que les dépenses exposées pour la remise en état de l'appartement à la suite du départ de l'ancienne locataire, qui incombaient normalement au propriétaire, aient été majorées du fait du refus de concours de la force publique ; <br/>
<br/>
              Considérant qu'il n'est pas davantage établi que la poursuite de l'occupation irrégulière à la faveur de ce refus ait eu pour conséquence une diminution de la valeur vénale de l'appartement de M. A ; <br/>
<br/>
              Considérant que M. A indique avoir contracté en novembre 2006 un emprunt qu'il entendait rembourser à l'aide du produit de la vente de l'appartement, laquelle a été retardée du fait du refus de concours de la force publique  ; qu'il demande que les intérêts qu'il a versés au titre de cet emprunt pendant la période de responsabilité de l'Etat soient mis à la charge de celui-ci ; que, toutefois, dès lors qu'il obtient, sur sa demande, une indemnisation au titre de la perte des revenus qu'il aurait tirés de la location de son bien, il ne saurait prétendre en outre à une indemnité au titre d'un préjudice imputé à l'impossibilité de le vendre et consistant dans le coût de l'immobilisation du capital correspondant ; <br/>
<br/>
              Considérant que M. A demande à être indemnisé des troubles qu'il aurait subis dans ses conditions d'existence du fait du refus de concours de la force publique ; que, contrairement à ce que soutient le ministre, la circonstance que ce chef de préjudice n'ait pas été expressément mentionné dans la réclamation préalable adressée à l'administration ne rend pas irrecevables les conclusions tendant à sa réparation ; qu'il ressort de l'instruction qu'indépendamment de l'indisponibilité de son bien, réparée par l'indemnité qui lui est allouée au titre des pertes de loyers, M. A a subi, du fait du refus qui lui a été opposé par le préfet de la Haute-Garonne, des troubles dont il sera fait une juste appréciation en lui accordant une indemnité de 3 000 euros ;<br/>
<br/>
              Sur les intérêts et les intérêts des intérêts :<br/>
<br/>
              Considérant que M. A a droit à compter du 25 juillet 2007, date à laquelle il a saisi le préfet d'une réclamation préalable, aux intérêts sur l'indemnité de 3 000 euros réparant les troubles dans ses conditions d'existence ; que l'indemnité de 4 424 euros qui lui est accordée au titre des pertes de loyers, en complément de la provision allouée par l'ordonnance du 27 novembre 2008 du juge des référés de la cour administrative d'appel de Bordeaux correspondant aux pertes encourues entre décembre 2008 et juin 2009, elle portera intérêts à compter du premier jour de chaque mois de cette période pour la perte encourue au cours de ce mois ; que la capitalisation des intérêts, demandée par un mémoire enregistré le 30 octobre 2008, date à laquelle il était dû plus d'un mois d'intérêts, sera effectuée à cette date puis à chaque échéance annuelle ultérieure ; <br/>
<br/>
              Sur la subrogation :<br/>
<br/>
              Considérant qu'il y a lieu de subordonner le versement des indemnités fixées par la présente décision à la subrogation de l'Etat dans les droits que détiendrait M. A sur Mme Catherine B ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 761-1 du code de justice administrative, de mettre à la charge de l'Etat la somme de 4 500 euros au titre des frais exposés par M. A devant le Conseil d'Etat et devant le tribunal administratif de Toulouse et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Toulouse du 4 février 2010 est annulé.<br/>
<br/>
Article 2 : L'Etat versera à M. A une somme de 3 000 euros portant intérêt à compter du 25 juillet 2007 et une somme de 4 424 euros portant intérêt à compter du premier jour de chaque mois entre décembre 2008 et juin 2009 pour la fraction correspondant aux pertes de loyers subies au cours de ce mois. Les intérêts échus le 30 octobre 2008 seront capitalisés à cette date puis à chaque échéance annuelle à compter de cette date pour produire eux-mêmes intérêts. <br/>
<br/>
Article 3 : Le paiement des sommes allouées par la présente décision est subordonné à la subrogation de l'Etat dans les droits que détiendrait M. A sur Mme Catherine B.<br/>
<br/>
Article 4 : L'Etat versera à M. A la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions de M. A est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. Sébastien A et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - 1) DEMANDE DE CONCOURS DE LA FORCE PUBLIQUE ANTÉRIEURE À UNE SAISINE DU JUGE DE L'EXÉCUTION PAR LA COMMISSION DE SURENDETTEMENT - RESPONSABILITÉ DE L'ETAT MALGRÉ CETTE CIRCONSTANCE - EXISTENCE - 2) CIRCONSTANCE QUE LE JUGE DE L'EXÉCUTION A EFFACÉ LES DETTES DU LOCATAIRE - CIRCONSTANCE SANS INCIDENCE SUR LE DROIT DU PROPRIÉTAIRE D'OBTENIR RÉPARATION DES PERTES DE LOYERS SUBIES ANTÉRIEUREMENT - 3) CHEFS DE PRÉJUDICE - INDISPONIBILITÉ DU BIEN - ABSENCE D'INDEMNISATION DANS LE CAS OÙ UNE INDEMNISATION DES LOYERS NON TOUCHÉS EST OBTENUE.
</SCT>
<ANA ID="9A"> 37-05-01 1) Lorsque la réquisition dont l'administration avait été régulièrement saisie a engagé la responsabilité de l'Etat à l'égard du propriétaire, la circonstance que la commission départementale de surendettement a ultérieurement saisi le juge de l'exécution aux fins d'ouverture d'une procédure de rétablissement personnel et si, par application des dispositions alors en vigueur de l'article L. 331-3-1 du code de la consommation, cette saisine a emporté suspension des voies d'exécution, y compris les mesures d'expulsion du logement, cette circonstance postérieure à la date à laquelle le concours de la force publique a été refusé et indépendante de la volonté du propriétaire n'a pas eu pour effet de suspendre la responsabilité de l'Etat.,,2) La circonstance que le juge de l'exécution a ordonné la clôture pour insuffisance d'actif de la procédure de rétablissement personnel ouverte à l'égard du locataire a effacé les dettes de l'intéressé antérieures à une certaine date est sans incidence sur le droit du propriétaire d'obtenir réparation des pertes de loyers qu'il a subies avant cette date.,,3) Dès lors qu'il obtient, sur sa demande, une indemnisation au titre de la perte des revenus qu'il aurait tirés de la location de son bien, un propriétaire ne saurait prétendre en outre à une indemnité au titre d'un préjudice imputé à l'impossibilité de le vendre et consistant dans le coût de l'immobilisation du capital correspondant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
