<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044211402</ID>
<ANCIEN_ID>JG_L_2021_09_000000456306</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/14/CETATEXT000044211402.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/09/2021, 456306, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456306</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456306.20210930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Par une requête et deux mémoires en réplique, enregistrés les 3 et 28 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Ethypharm et la société Laboratoires Ethypharm demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
               1°) de suspendre l'exécution de l'arrêté du 1er juillet 2021 modifiant la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics en tant qu'il étend l'inscription sur cette liste de la spécialité Baclofène Zentiva 10 mg à une nouvelle indication ; <br/>
<br/>
               2°) de suspendre l'exécution de l'arrêté du 1er juillet 2021 modifiant la liste des spécialités pharmaceutiques remboursables aux assurés sociaux en tant qu'il étend l'inscription sur cette liste de la spécialité Baclofène Zentiva 10 mg à une nouvelle indication ;<br/>
<br/>
               3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
               Elles soutiennent que :<br/>
               - la condition d'urgence est satisfaite dès lors que, d'une part, les décisions contestées préjudicient de manière suffisamment grave et immédiate à un intérêt public en ce que, en premier lieu, l'administration du Baclofène Zentiva 10 mg dans le traitement de la dépendance à l'alcool présente un risque pour la santé publique eu égard aux insuffisances des études réalisées sur ce traitement, en deuxième lieu, son remboursement est de nature à créer un surcoût pour les dépenses publiques et, en dernier lieu, les arrêtés litigieux méconnaissent le droit de l'Union européenne et, d'autre part, elles préjudicient de manière suffisamment grave et immédiate à leur situation en ce que les modalités de remboursement du Baclofène Zentiva 10 mg créent une grave distorsion concurrentielle à leur détriment ; <br/>
               - il existe un doute sérieux quant à la légalité des arrêtés contestés ;<br/>
               - les arrêtés contestés ont été pris au vu d'un avis de la commission de la transparence rendu en méconnaissance du principe d'impartialité et adopté selon des modalités irrégulières ; <br/>
               - l'exécution des arrêtés contestés doit être suspendue par voie de conséquence de l'annulation à intervenir de la décision du 27 novembre 2020 de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) portant modification de l'autorisation de mise sur le marché de la spécialité Baclofène Zentiva 10 mg, qui fait l'objet d'un recours en excès de pouvoir devant le tribunal administratif de Paris ; <br/>
               - par voie d'exception, l'extension de l'indication de l'autorisation de mise sur le marché de la spécialité Baclofène Zentiva 10 mg, en premier lieu, méconnaît les règles de protection des données fournies à l'appui de sa demande d'autorisation de la spécialité Baclocur, en deuxième lieu, est entachée d'une erreur de droit faute de production par la société Zentiva d'études précliniques ou cliniques significatives et, en dernier lieu, viole le principe de protection de la confiance légitime ; <br/>
               - les arrêtés contestés portent atteinte au principe d'égalité et aux règles de la concurrence en ce qu'ils engendrent une distorsion de concurrence entre les sociétés qui commercialisent respectivement Baclofène Zentiva 10 mg et Baclocur ; <br/>
               - les arrêtés contestés ont été pris en méconnaissance des dispositions de l'article R. 163-5 du code de la sécurité sociale dès lors qu'un refus doit être opposé à la demande d'inscription d'un médicament lorsqu'il n'apporte aucune amélioration du service médical rendu et ne permet pas à l'assurance maladie de réaliser une économie dans le coût du traitement, ou qu'il est susceptible d'entraîner des hausses de consommation ou des dépenses injustifiées pour l'assurance maladie, et qu'il fait l'objet d'une publicité auprès du grand public ; <br/>
               - l'arrêté du 1er juillet 2021 modifiant la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics méconnait l'obligation d'évaluation préalable du coût de la prise en charge de la spécialité Baclofène Zentiva 10 mg dans les établissements publics ;<br/>
               - le taux de remboursement de trente pourcent de la spécialité Baclofène Zentiva 10 mg est entaché d'erreur manifeste d'appréciation.<br/>
<br/>
               Par un mémoire, enregistré le 16 septembre 2021, l'Agence nationale de sécurité du médicament et des produits de santé conclut au rejet de la requête. Elle soutient qu'aucun des moyens invoqués n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
               Par un mémoire en défense, enregistré le 24 septembre 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'aucun des moyens invoqués n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée. <br/>
<br/>
               Par un mémoire en défense, enregistré le 24 septembre 2021, la société Zentiva France conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas satisfaite et qu'aucun des moyens invoqués n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
               Par un mémoire, enregistré le 24 septembre 2021, la Haute Autorité de santé a déposé des observations sur la requête.  <br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - la directive 2001/83/CE du Parlement européen et du Conseil du 6 novembre 2001 ; <br/>
               - le code de la santé publique ; <br/>
               - le code de la sécurité sociale ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique les société Ethypharm et Laboratoires Ethypharm, le ministre des solidarités et de la santé, l'Agence nationale de sécurité du médicament et des produits de santé, la Haute Autorité de santé et la société Zentiva France. <br/>
<br/>
               Ont été entendus lors de l'audience publique du 29 septembre 2021, à 10 heures : <br/>
<br/>
               - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Ethypharm et la société Laboratoires Ethypharm ;<br/>
<br/>
               - Me Piwnica, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Zentiva France ; <br/>
<br/>
               - les représentants de la société Ethypharm et la société Laboratoires Ethypharm ; <br/>
<br/>
               - les représentants de la société Zentiva France ; <br/>
<br/>
               - les représentants du ministre des solidarités et de la santé ; <br/>
<br/>
               à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
               2. Il résulte de l'instruction que la société Zentiva France a obtenu en 1990 une autorisation de mise sur le marché (AMM) pour la spécialité Baclofène Zentiva 10 mg, pour le traitement des contractures spastiques liées à la sclérose en plaques et aux affections médullaires et d'origine cérébrale. Elle a bénéficié, à compter du 17 mars 2014, sur le fondement de l'article L. 5121-12-1 du code de la santé publique, d'une recommandation temporaire d'utilisation (RTU) encadrant l'utilisation de ce médicament dans le traitement de la dépendance à l'alcool. La société Ethypharm a quant à elle, pour le seul traitement de cette même indication, développé le Baclocur, médicament à base de Baclofène pour lequel elle a obtenu des autorisations de mise sur le marché le 23 octobre et le 13 décembre 2018. Par une décision du 27 novembre 2020, l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) a modifié l'AMM du Baclofène Zentiva 10 mg pour l'étendre à l'indication de traitement de la dépendance à l'alcool. Il a également été mis fin à la RTU applicable à cette indication. <br/>
<br/>
               3. Les sociétés requérantes demandent la suspension de l'exécution des deux arrêtés du 1er juillet 2021 modifiant, d'une part, la liste des spécialités pharmaceutiques agréées à l'usage des collectivités et divers services publics et, d'autre part, la liste des spécialités pharmaceutiques remboursables aux assurés sociaux en tant qu'ils étendent l'inscription sur ces listes de la spécialité Baclofène Zentiva pour l'indication de " réduction de la consommation d'alcool, après échec des autres traitements médicamenteux disponibles, chez les patients adultes ayant une dépendance à l'alcool et une consommation d'alcool à risque élevé ".<br/>
<br/>
               4. Pour justifier de l'urgence à suspendre les arrêtés attaqués, les sociétés requérantes invoquent en premier lieu le risque pour la santé publique qui résulterait de l'insuffisance des études cliniques et précliniques préalables à la modification de l'AMM du Baclofène Zentiva 10 mg pour le traitement de la dépendance à l'alcool, en particulier pour les personnes âgées et les patients souffrant d'insuffisance hépatique. Il résulte cependant de l'instruction, ainsi qu'il a été dit ci-dessus, que le traitement des personnes dépendantes à l'alcool par la spécialité Baclofène Zentiva 10 mg s'est poursuivi depuis 2014 sous couvert d'une RTU, avec par ailleurs une dose maximale qui a été réduite au cours du temps, et depuis 2020 d'une AMM. Si les sociétés requérantes invoquent la protection s'attachant aux études qu'elles ont produites pour que soit délivrée une AMM à la spécialité Baclocur, et soutiennent que ces études auraient été prises en compte, pour l'extension de l'AMM de Baclofène Zentiva 10 mg, au mépris de cette protection, une telle argumentation n'établit cependant pas en elle-même que l'exécution des arrêtés contestés caractériserait une situation d'urgence suffisante pour en justifier la suspension. <br/>
<br/>
               5. Si, en deuxième lieu, les sociétés requérantes invoquent également le risque de surcoût pour les finances publiques qui résulterait de l'exécution des arrêts attaqués, elles n'établissent cependant pas que ce risque présente une gravité suffisante, eu égard tant aux taux de remboursement et aux prix applicables aux deux spécialités qu'aux termes de la décision du 21 janvier 2021 par laquelle la Haute autorité de santé a estimé que la spécialité en cause n'était pas susceptible d'avoir un impact significatif sur les dépenses de l'assurance maladie au sens du 2° du I de l'article R. 161-71-3 du code de la sécurité sociale. De même, les arguments tirés de l'atteinte au droit de l'Union européenne ne caractérisent pas en l'espèce de situation d'urgence. <br/>
<br/>
               6. Enfin, les sociétés requérantes soutiennent que le désavantage concurrentiel résultant pour elles des décisions contestées, eu égard au prix et au taux de remboursement de trente pourcent qui s'applique au Baclofène Zentiva 30 mg, comparé au taux de quinze pourcent applicable au Baclocur, est constitutif pour elles d'une situation d'urgence, dès lors qu'il leur interdirait de fait tout développement commercial sur ce marché. En se bornant à invoquer les sommes investies pour le développement de la spécialité Baclocur et le plan d'affaires envisagé, sans étayer leurs allégations sur ces points par aucun élément probant, ni en ce qui concerne les investissements et le plan d'affaires ni en ce qui concerne leur situation d'ensemble, les sociétés requérantes n'établissent pas la situation d'urgence qui pourrait résulter, s'il était suffisamment grave et étayé, d'un risque d'exclusion durable du marché visé, mais qui ne saurait résulter d'une simple perte de la possibilité d'augmenter leur chiffre d'affaires. <br/>
<br/>
               7. Dans ces conditions, les requérantes n'établissent pas que leur requête caractériserait une urgence justifiant que, sans attendre le jugement de leur requête au fond, une mesure de suspension soit prononcée. Par suite, et sans qu'il y ait lieu d'examiner s'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté contesté, la requête des sociétés Ethypharm et Laboratoires Ethypharm doit être rejetée, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu de faire droit aux conclusions de la société Zentiva au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête des sociétés Ethypharm et Laboratoires Ethypharm est rejetée.<br/>
Article 2 : Les conclusions présentées par la société Zentiva France en application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée aux sociétés Ethypharm et Laboratoires Ethypharm, au ministre des solidarités et de la santé, à l'Agence nationale de sécurité du médicament et des produits de santé et à la société Zentiva France.<br/>
Copie en sera adressée à la Haute autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
