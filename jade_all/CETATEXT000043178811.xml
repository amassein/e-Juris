<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043178811</ID>
<ANCIEN_ID>JG_L_2021_02_000000439375</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/17/88/CETATEXT000043178811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 23/02/2021, 439375, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439375</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439375.20210223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... a demandé au tribunal administratif de Clermont-Ferrand, d'une part, d'annuler l'arrêté du 23 novembre 2018 par lequel la préfète de l'Allier lui a refusé la délivrance d'un titre de séjour, l'a obligé à quitter le territoire français dans le délai de trente jours et a désigné le pays à destination duquel il serait reconduit d'office, d'autre part, d'enjoindre à la préfète de l'Allier de lui délivrer une carte de séjour temporaire ou, à tout le moins, de réexaminer sa demande, dans le délai d'un mois à compter de la notification du jugement à intervenir, sous astreinte de 100 euros par jour de retard. Par un jugement n° 1900309 du 29 mai 2019, le tribunal administratif de Clermont-Ferrand a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19LY02839 du 19 décembre 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 mars et 25 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros, à verser à la SCP Marlange, de la Burgade, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme C... B..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant malien, déclare être né le 24 mars 2000 et être entré en France le 29 juin 2017. Il a présenté une demande de titre de séjour sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par arrêté du 23 novembre 2018, la préfète de l'Allier a refusé de l'admettre au séjour, lui a fait obligation de quitter le territoire français dans le délai de trente jours et a désigné le pays de renvoi, aux motifs, d'une part, que ses documents d'identité étaient frauduleux, de sorte qu'il n'établissait pas être mineur lors de son arrivée en France, d'autre part, qu'il ne justifiait ni suivre une formation pouvant lui apporter une qualification professionnelle depuis au moins six mois, ni être dépourvu de liens avec sa famille au Mali. Par un jugement du 29 mai 2019, le tribunal administratif de Clermont-Ferrand a rejeté sa demande tendant à l'annulation de cet arrêté. M. A... se pourvoit en cassation contre l'arrêt du 19 décembre 2019 par lequel la cour administrative d'appel de Lyon a rejeté sa requête d'appel. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors en vigueur : " A titre exceptionnel et sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire prévue au 1° de l'article L. 313-10 portant la mention " salarié " ou la mention " travailleur temporaire " peut être délivrée, dans l'année qui suit son dix-huitième anniversaire, à l'étranger qui a été confié à l'aide sociale à l'enfance entre l'âge de seize ans et l'âge de dix-huit ans et qui justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle, sous réserve du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française. (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui est arrivé en France en juin 2017 à l'âge de 17 ans et a été pris en charge par les services de l'aide sociale à l'enfance du conseil départemental de l'Allier jusqu'à sa majorité, a effectué plusieurs stages dans le secteur de la restauration avant de signer en août 2018 un contrat d'apprentissage avec un restaurateur de Montluçon et de s'inscrire en septembre 2018 à l'unité de formation en apprentissage du lycée professionnel de Montluçon pour y préparer un certificat d'aptitude professionnel (CAP) en alternance (spécialité cuisine). Or, si les stages effectués par le requérant avant son inscription en CAP ont pu contribuer à le guider dans ses choix d'orientation professionnelle, ils ne sauraient toutefois être regardés, en eux-mêmes, comme une formation destinée à lui apporter une qualification professionnelle au sens de l'article L. 313-15 précité du code de l'entrée et du séjour des étrangers et du droit d'asile, dès lors qu'ils ont été réalisés en dehors du cadre d'une telle formation. Par suite, en estimant qu'à la date à laquelle la préfète de l'Allier a pris l'arrêté litigieux, le 23 novembre 2018, M. A... ne justifiait suivre une formation professionnelle que depuis deux mois et ne remplissait donc pas la condition de suivi d'une formation pendant six mois requise par les dispositions de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile, la cour administrative d'appel de Lyon n'a pas entaché son arrêt d'insuffisance de motivation, ni commis d'erreur de droit ni dénaturé les pièces du dossier.<br/>
<br/>
              4. En deuxième lieu, l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " 1. Toute personne a droit au respect de sa vie privée et familiale (...) ". Aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) / 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 313 2 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République. (...) ".<br/>
<br/>
              5. Si M. A... fait valoir qu'il n'a plus d'attache familiale dans son pays d'origine après y avoir été rejeté par sa famille, et fait état de son intégration et de son implication dans sa scolarité depuis son arrivée en France, il ressort toutefois des pièces du dossier soumis aux juges du fond qu'il n'est entré en France qu'en juin 2017 à l'âge de 17 ans, après avoir passé l'essentiel de sa vie au Mali, et qu'il ne témoigne d'aucune relation personnelle ou amicale susceptible d'établir l'existence ou l'intensité de sa vie personnelle et familiale en France. Par suite, la cour administrative d'appel de Lyon n'a pas inexactement qualifié les faits de l'espèce en déduisant de l'ensemble de ces circonstances que le refus de délivrance du titre de séjour litigieux n'avait pas porté une atteinte disproportionnée au droit de M. A... au respect de sa vie privée et familiale et n'avait, par suite, pas méconnu les dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ni les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Elle n'a, en portant cette appréciation, pas davantage commis d'erreur de droit ni entaché son arrêt de contradiction de motifs.<br/>
<br/>
              6. En dernier lieu, aux termes de l'article L. 3122 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors en vigueur : " La commission [du titre de séjour] est saisie par l'autorité administrative lorsque celleci envisage de refuser de délivrer ou de renouveler une carte de séjour temporaire à un étranger mentionné à l'article L. 1311 (...). ". Il résulte de ces dispositions que le préfet n'est tenu de saisir la commission du titre de séjour que du cas des seuls étrangers qui remplissent effectivement les conditions prévues aux articles visés par ces dispositions auxquels il envisage néanmoins de refuser le titre de séjour sollicité, et non de celui de tous les étrangers qui s'en prévalent. <br/>
<br/>
              7. Ainsi qu'il a été dit au point 5, M. A... ne remplit pas les conditions pour obtenir un titre de séjour au titre des dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par suite, la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit en estimant que la préfète de l'Allier n'était pas tenue de soumettre son cas à la commission du titre de séjour avant de rejeter sa demande.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font, par suite, obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Marlange, de la Burgade, son avocat.<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. D... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
