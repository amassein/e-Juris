<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788922</ID>
<ANCIEN_ID>JG_L_2016_12_000000370350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788922.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/12/2016, 370350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:370350.20161228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 9 avril 2015, le Conseil d'Etat statuant au contentieux sur la requête de l'Association des utilisateurs et distributeurs de l'agrochimie européenne (AUDACE) tendant à l'annulation pour excès de pouvoir de la décision par laquelle le Premier ministre a implicitement rejeté sa demande, reçue le 26 avril 2013, tendant à l'abrogation de l'article R. 5141-123-17 introduit dans le code de la santé publique par le décret n° 2005-558 du 27 mai 2005 relatif aux importations de médicaments vétérinaires et modifiant le code de la santé publique, a sursis à statuer sur ses conclusions tendant à l'annulation de cette décision et sur ses conclusions tendant à ce qu'il soit enjoint au Premier ministre, à titre principal, d'abroger cet article et, à titre subsidiaire, de procéder à un nouvel examen de sa demande d'abrogation, jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question préjudicielle dont la cour d'appel de Pau l'avait saisie par un arrêt du 15 janvier 2015, portant sur l'interprétation de la directive 2001/82/CE du 6 novembre 2001 instituant un code communautaire relatif aux médicaments vétérinaires, de la directive 2006/123/CE du 12 septembre 2006 relative aux services dans le marché intérieur et des articles 34, 36, 56 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              Par un arrêt C-114/15 du 27 octobre 2016, la Cour de justice de l'Union européenne s'est prononcée sur cette question. <br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 9 avril 2015 ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2001/82/CE du Parlement européen et du Conseil du 6 novembre 2001 ;<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de l'Association des utilisateurs et distributeurs de l'agrochimie européenne.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 décembre 2016, présentée par l'Association des utilisateurs et distributeurs de l'agrochimie européenne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions de l'association requérante tendant à l'annulation du refus d'abrogation de l'article R. 5141-123-17 du code de la santé publique :<br/>
<br/>
              1. Aux termes de l'article R. 5141-123-6 du code de la santé publique : " Constitue une importation parallèle, en vue d'une mise sur le marché en France, l'importation d'une spécialité pharmaceutique vétérinaire : / 1° Qui provient d'un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, dans lequel elle a obtenu une autorisation de mise sur le marché pour les mêmes animaux de destination ; / 2° Dont la composition quantitative et qualitative en principes actifs et en excipients, la forme pharmaceutique et les effets thérapeutiques sont identiques à ceux d'une spécialité pharmaceutique vétérinaire ayant obtenu une autorisation de mise sur le marché délivrée par l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail. / Toutefois, dans les conditions prévues aux 3° et 4° du I de l'article R. 5141-123-8, la spécialité peut comporter des quantités de principes actifs ou d'excipients différentes ou des excipients de nature différente de ceux de la spécialité ayant obtenu une autorisation de mise sur le marché (...), dès lors que ces différences n'ont aucune incidence thérapeutique et qu'elles n'entraînent pas de risque pour la santé publique ". L'article R. 5141-123-17 du même code, dont les dispositions ont fait l'objet de la demande d'abrogation formée par l'association requérante et implicitement rejetée par le Premier ministre, prévoit que : " L'exploitation, telle que définie au deuxième alinéa du 3° de l'article R. 5142-1 et, pour ce qui concerne la pharmacovigilance, aux articles R. 5141-104, R. 5141-105 et R. 5141-108, d'une spécialité pharmaceutique vétérinaire bénéficiant d'une autorisation d'importation parallèle est assurée par le titulaire de cette autorisation, sous réserve qu'il ait obtenu l'autorisation d'ouverture prévue à l'article L. 5142-2 ". En vertu de cet article L. 5142-1, l'ouverture d'un établissement de fabrication, d'importation, d'exportation, de distribution en gros ou d'exploitation de médicaments vétérinaires " est subordonnée à une autorisation délivrée par l'Agence nationale chargée de la sécurité sanitaire de l'alimentation, de l'environnement et du travail (...) ". <br/>
<br/>
              En ce qui concerne l'importation par les éleveurs pour les besoins de leurs propres élevages :  <br/>
<br/>
              2. Par son arrêt du 27 octobre 2016 par lequel elle s'est prononcée sur la question dont la cour d'appel de Pau l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que les articles 34 et 36 du traité sur le fonctionnement de l'Union européenne doivent être interprétés en ce sens qu'ils s'opposent à une réglementation nationale qui réserve l'accès aux importations parallèles de médicaments vétérinaires aux distributeurs en gros titulaires de l'autorisation prévue à l'article 65 de la directive 2001/82/CE du Parlement européen et du Conseil du 6 novembre 2001 instituant un code communautaire relatif aux médicaments vétérinaires, telle que modifiée par le règlement (CE) nº 596/2009 du Parlement européen et du Conseil du 18 juin 2009, et qui, par conséquent, exclut de l'accès à de telles importations les éleveurs désirant importer des médicaments vétérinaires pour les besoins de leurs propres élevages.<br/>
<br/>
              3. Toutefois, l'article R. 5141-123-17 du code de la santé publique s'applique seulement à l'exploitation d'une spécialité pharmaceutique vétérinaire bénéficiant d'une autorisation d'importation parallèle, c'est-à-dire, selon l'article R. 5142-1 du même code, aux " opérations de vente en gros ou de cession à titre gratuit, de publicité, d'information, de pharmacovigilance, de suivi des lots et, s'il y a lieu, de leur retrait, ainsi que, le cas échéant, les opérations de stockage correspondantes ". Par suite, l'article R. 5141-123-17 n'a pas pour effet, par lui-même, d'interdire l'importation parallèle de médicaments vétérinaires par des éleveurs pour les besoins de leurs propres élevages. Dès lors, l'association requérante n'est pas fondée à soutenir que ces dispositions, en excluant de l'accès aux importations parallèles les éleveurs désirant importer des médicaments vétérinaires à cette fin, méconnaîtraient les articles 34 et 36 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              En ce qui concerne les obligations de pharmacovigilance :<br/>
<br/>
              4. Aux termes de l'article 34 du traité sur le fonctionnement de l'Union européenne : " Les restrictions quantitatives à l'importation ainsi que toutes mesures d'effet équivalent sont interdites entre les Etats membres ". Il résulte de l'article 36 du même traité que, si les stipulations de l'article 34 ne font pas obstacle aux interdictions ou restrictions d'importation " justifiées par des raisons de moralité publique, d'ordre public, de sécurité publique, de protection de la santé et de la vie des personnes et des animaux ou de préservation des végétaux (...) ", " ces interdictions ou restrictions ne doivent constituer ni un moyen de discrimination arbitraire ni une restriction déguisée dans le commerce entre les Etats membres ". Il résulte de la jurisprudence de la Cour de justice de l'Union européenne que les dispositions de la directive 2001/82/CE du Parlement européen et du Conseil du 6 novembre 2001 instituant un code communautaire relatif aux médicaments vétérinaires qui régissent la procédure de délivrance d'une autorisation de mise sur le marché n'ont pas vocation à s'appliquer aux importations parallèles et que le régime d'autorisation de ces importations doit être examiné à la lumière des stipulations du traité sur le fonctionnement de l'Union européenne relatives à la libre circulation des marchandises.<br/>
<br/>
              5. Aux termes de l'article 72 de la directive 2001/82/CE du 6 novembre 2001 : " 1. Les Etats membres prennent toutes les mesures appropriées pour encourager la notification des effets indésirables présumés des médicaments vétérinaires à l'autorité compétente (...) ". L'article 73 de cette directive prévoit que : " Afin d'assurer l'adoption de décisions réglementaires appropriées et harmonisées concernant les médicaments vétérinaires autorisés dans la Communauté, au vu des informations recueillies sur les effets indésirables présumés des médicaments vétérinaires dans les conditions normales d'emploi, les États membres gèrent un système de pharmacovigilance vétérinaire. Ce système est destiné à permettre de recueillir des informations utiles pour la surveillance des médicaments vétérinaires, notamment quant à leurs effets indésirables sur les animaux et sur l'être humain, et d'évaluer scientifiquement ces informations (...) ". Les articles 74 et 75 de la même directive précisent les obligations de pharmacovigilance qui pèsent sur le titulaire de l'autorisation de mise sur le marché. <br/>
<br/>
              6. Dans son arrêt du 27 octobre 2016, la Cour de justice de l'Union européenne a dit pour droit que les articles 34 et 36 du traité sur le fonctionnement de l'Union européenne doivent être interprétés en ce sens qu'ils ne s'opposent pas à une réglementation nationale qui impose aux éleveurs, qui importent de manière parallèle des médicaments vétérinaires pour les besoins de leurs propres élevages, de satisfaire à l'ensemble des obligations de pharmacovigilance prévues aux articles 72 à 79 de la directive 2001/82, telle que modifiée par le règlement nº 596/2009. La Cour a en outre jugé, aux points 54 et 55 de cet arrêt, que, pour assurer l'effet utile de cette directive et garantir l'objectif de sauvegarde de la santé publique qu'elle poursuit, les personnes qui importent de manière parallèle un médicament vétérinaire " doivent disposer d'une autorisation de mise sur le marché délivrée, quand bien même par une procédure simplifiée, par les autorités nationales compétentes, et deviennent les responsables de la mise sur le marché des médicaments vétérinaires importés de manière parallèle dans l'État membre de destination ". Elle a également jugé, au point 56 de cet arrêt, que si les dispositions de la directive 2001/82 relatives à la procédure de délivrance d'une autorisation de mise sur le marché n'ont pas vocation à s'appliquer, en revanche, rien ne justifie que ses dispositions relatives " à la détention, à la délivrance, à l'étiquetage et à la notice ainsi qu'à la pharmacovigilance, qui font partie du système cohérent de mesures mis en place par ladite directive, afin de garantir un haut niveau de protection de la santé publique, ne s'appliquent pas en cas d'importation parallèle. Tout au contraire, si lesdites dispositions n'étaient pas applicables en cas d'importation parallèle, il existerait un risque de voir les exploitants du secteur des médicaments vétérinaires contourner les obligations prévues par la directive 2001/82 en pratiquant l'importation parallèle de tels médicaments ".<br/>
<br/>
              7. Il résulte de ce qui précède que l'association requérante, qui ne peut ainsi se prévaloir utilement d'une discrimination entre importateurs parallèles et importateurs d'autres médicaments ou titulaires d'autorisations de mise sur le marché, n'est pas fondée à soutenir que l'article R. 5141-123-17 du code de la santé publique méconnaîtrait l'article 34 du traité sur le fonctionnement de l'Union européenne et les objectifs de la directive 2001/82/CE en tant qu'il soumet l'exploitation de médicaments vétérinaires bénéficiant d'une importation parallèle, à l'instar de toute exploitation de médicaments vétérinaires, aux obligations de pharmacovigilance prévues par les articles R. 5141-104, R. 5141-105 et R. 5141-108 du même code, assurant la transposition des articles 74 et 75 de la directive.<br/>
<br/>
              En ce qui concerne l'obligation de disposer d'un établissement autorisé au titre de l'article L. 5142-2 du code de la santé publique :<br/>
<br/>
              8. Aux termes de l'article 56 du traité sur le fonctionnement de l'Union européenne : " (...) les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui du destinataire de la prestation (...) ". L'article 16 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 septembre 2006 relative aux services dans le marché intérieur dispose que : " 1. Les États membres respectent le droit des prestataires de fournir des services dans un État membre autre que celui dans lequel ils sont établis. / L'État membre dans lequel le service est fourni garantit le libre accès à l'activité de service ainsi que son libre exercice sur son territoire. / (...) / 2. Les États membres ne peuvent pas restreindre la libre prestation de services par un prestataire établi dans un autre État membre en imposant l'une des exigences suivantes : / a) l'obligation pour le prestataire d'avoir un établissement sur leur territoire ; / b) l'obligation pour le prestataire d'obtenir une autorisation de leurs autorités compétentes (...). / 3. Les présentes dispositions n'empêchent pas l'État membre dans lequel le prestataire se déplace pour fournir son service d'imposer des exigences concernant la prestation de l'activité de service lorsque ces exigences sont justifiées par des raisons d'ordre public, de sécurité publique, de santé publique ou de protection de l'environnement et conformément au paragraphe 1 (...) ". En outre, le 1 de l'article 3 de cette directive prévoit que : " Si les dispositions de la présente directive sont en conflit avec une disposition d'un autre acte communautaire régissant des aspects spécifiques de l'accès à une activité de services ou à son exercice dans des secteurs spécifiques ou pour des professions spécifiques, la disposition de l'autre acte communautaire prévaut et s'applique à ces secteurs ou professions spécifiques (...) ".<br/>
<br/>
              9. L'article 65 de la directive 2001/82/CE prévoit que : " 1. Les États membres prennent toutes dispositions utiles pour que la distribution en gros des médicaments vétérinaires soit soumise à la possession d'une autorisation (...) ", et que : " 2. Pour obtenir l'autorisation de distribution, le demandeur dispose du personnel ayant des compétences techniques, de locaux et équipements adaptés et suffisants, conformes aux exigences relatives à la conservation et à la manipulation des médicaments vétérinaires définies dans l'État membre concerné. (...) ". <br/>
<br/>
              10. Aux points 72 et 73 de son arrêt du 27 octobre 2016, la Cour de justice de l'Union européenne a relevé " qu'un régime national consistant à réserver l'accès aux importations parallèles de médicaments vétérinaires aux seuls détenteurs d'une autorisation de distribution en gros, au sens de l'article 65 de la directive 2001/82, apparaît de nature à garantir la réalisation de l'objectif de protection de la santé humaine et animale. En effet, le demandeur d'une autorisation de distribution en gros doit se conformer aux obligations découlant de cet article 65, qui visent, en particulier, ainsi qu'il ressort du paragraphe 2 de ce dernier, à ce que l'exercice de la distribution en gros soit effectué dans des conditions conformes aux exigences relatives à la conservation et à la manipulation des médicaments vétérinaires ". Il découle de cet article 65 qu'une telle autorisation doit être délivrée par les autorités compétentes de l'Etat sur le territoire duquel l'opérateur entend procéder à la distribution en gros de médicaments vétérinaires, en fonction des personnels et des installations dont il dispose à cette fin. La Cour de justice a seulement réservé la situation des éleveurs important des médicaments vétérinaires pour les besoins de leurs propres élevages, au motif que " l'obligation de disposer d'un personnel ayant des compétences techniques, de locaux et d'équipements adaptés et suffisants, conformes aux exigences relatives à la conservation et à la manipulation des médicaments vétérinaires définies dans l'État membre concerné, au sens dudit article 65, paragraphe 2, ne saurait être imposée, dans le cadre de la procédure d'obtention d'une autorisation de mise sur le marché, aux éleveurs qui importent de manière parallèle des médicaments vétérinaires pour les besoins de leurs propres élevages ".<br/>
<br/>
              11. Par suite, c'est en conformité avec les exigences définies par l'article 65 de la directive 2001/82/CE, qui prévalent, dans le secteur des médicaments vétérinaires, sur celles de la directive 2006/123/CE, que l'article R. 5141-123-17 du code de la santé publique subordonne l'exploitation de médicaments vétérinaires bénéficiant d'une autorisation d'importation parallèle, laquelle comprend la distribution en gros de ces médicaments, à l'obligation de disposer d'un établissement visé à l'article L. 5142-1 du même code et autorisé au titre de l'article L. 5142-2 de ce code, pris pour la transposition de l'article 65 de la directive 2001/82/CE. Il en résulte que le moyen tiré de la méconnaissance des stipulations des articles 34 et 56 du traité sur le fonctionnement de l'Union européenne et des dispositions de l'article 16 de la directive 2006/123/CE doit être écarté.<br/>
<br/>
              12. Il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation de la décision du 26 juin 2013 par laquelle le Premier ministre a implicitement rejeté sa demande d'abrogation du décret du 27 mai 2005 en tant qu'il a introduit dans le code de la santé publique l'article R. 5141-123-17.<br/>
<br/>
              Sur les conclusions de l'association requérante à fin d'injonction :<br/>
<br/>
              13. Les conclusions de l'association requérante tendant à l'annulation de la décision par laquelle le Premier ministre a implicitement rejeté sa demande d'abrogation du décret du 27 mai 2005 en tant qu'il a introduit l'article R. 5141-123-17 dans le code de la santé publique étant rejetées, celles visant à enjoindre au Premier ministre, à titre principal, d'abroger cet article et, à titre subsidiaire, de procéder à un nouvel examen de sa demande d'abrogation ne peuvent qu'être également rejetées.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association des utilisateurs et distributeurs de l'agrochimie européenne est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'Association des utilisateurs et distributeurs de l'agrochimie européenne, au Premier ministre, à la ministre des affaires sociales et de la santé et au ministre de l'agriculture, de l'agroalimentaire et de la forêt. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
