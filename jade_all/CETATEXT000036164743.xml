<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036164743</ID>
<ANCIEN_ID>JG_L_2017_12_000000407804</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/16/47/CETATEXT000036164743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 07/12/2017, 407804, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407804</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Dominique Bertinotti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407804.20171207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...ont demandé au tribunal administratif de Marseille d'annuler l'arrêté du 14 octobre 2014 du maire de Plan-de-Cuques (Bouches-du-Rhône) refusant de leur délivrer un permis de construire.<br/>
<br/>
              Par un jugement n° 1408881 du 9 juin 2016, le tribunal administratif de Marseille a annulé ce permis.<br/>
<br/>
              La commune de Plan-de-Cuques a relevé appel de ce jugement et demandé qu'il soit sursis à son exécution. Par un arrêt n° 16MA030604 du 8 décembre 2016, la cour administrative d'appel de Marseille a rejeté la requête de la commune tendant à ce qu'il soit sursis à l'exécution du jugement du tribunal administratif. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 et 24 février 2017 au secrétariat du contentieux du Conseil d'Etat, la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire, d'ordonner le sursis à exécution du jugement ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme A...la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Bertinotti, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Plan-de-Cuques ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par un jugement du 9 juin 2016, le tribunal administratif de Marseille a annulé, sur la demande de M. et MmeA..., l'arrêté du 14 octobre 2014 par lequel le maire de Plan-de-Cuques (Bouches-du-Rhône) avait refusé de leur délivrer un permis de construire ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a rejeté la requête de la commune tendant à ce qu'il soit sursis à l'exécution de ce jugement ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 811-15 du code de justice administrative : " Lorsqu'il est fait appel d'un jugement de tribunal administratif prononçant l'annulation d'une décision administrative, la juridiction d'appel peut, à la demande de l'appelant, ordonner qu'il soit sursis à l'exécution de ce jugement si les moyens invoqués par l'appelant paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation ou la réformation du jugement attaqué, le rejet des conclusions à fin d'annulation accueillies par ce jugement " ; qu'aux termes de l'article R. 811-17 du même code : " Dans les autres cas, le sursis peut être ordonné à la demande du requérant si l'exécution de la décision de première instance attaquée risque d'entraîner des conséquences difficilement réparables et si les moyens énoncés dans la requête paraissent sérieux en l'état de l'instruction " ; <br/>
<br/>
              3.	Considérant qu'il est loisible à la partie qui s'y croit fondée de présenter au juge d'appel des conclusions à fin de sursis à exécution d'un jugement ayant annulé une décision administrative, sur le fondement tant des dispositions particulières de l'article R. 811-15 que des dispositions générales de l'article R. 811-17 du code de justice administrative ; que, présentées, instruites, jugées et, le cas échéant, susceptibles de recours selon des règles identiques, des conclusions fondées sur chacun de ces articles peuvent être présentées simultanément dans une même requête ; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel que la commune de Plan-de-Cuques a saisi la cour d'une requête à fin de sursis à exécution du jugement du tribunal administratif qui avait annulé le refus de permis de construire opposé à M. et MmeA..., laquelle était fondée à titre principal sur les dispositions de l'article R. 811-17 du code de justice administrative et à titre subsidiaire sur celles de l'article R. 811-15 du même code ; qu'en se fondant, pour rejeter la demande de sursis, sur un motif tiré de ce que les dispositions de l'article R. 811-17 n'auraient pas été applicables en l'espèce, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ; <br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de statuer sur la requête à fin de sursis à exécution présentée par la commune de Plan-de-Cuques ; <br/>
<br/>
              6.	Considérant que, pour annuler l'arrêté du 14 octobre 2014 par lequel le maire de Plan-de-Cuques a refusé de délivrer un permis de construire à M. et MmeA..., le tribunal administratif de Marseille a jugé que cet arrêté devait être regardé comme ayant procédé au retrait d'un permis de construire tacitement accordé, que ce retrait était illégal pour être intervenu au terme d'une procédure irrégulière et après l'expiration du délai prévu par l'article L. 424-5 du code de l'urbanisme et que l'unique motif du refus, tiré d'un risque d'inondation en cas de crue, était entaché d'illégalité ; que, pour demander qu'il soit sursis à l'exécution de ce jugement, la commune de Plan-de-Cuques se borne à soutenir qu'aucun permis tacite n'avait été accordé et que l'appréciation portée par le tribunal quant au risque d'inondation et de rupture du canal de Provence serait erronée ; qu'en l'état de l'instruction, ce moyen n'apparaît pas de nature à justifier qu'il soit sursis à l'exécution du jugement contesté ; que la requête à fin de sursis à exécution de la commune de Plan-de-Cuques ne peut, par suite, qu'être rejetée ;<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Plan-de-Cuques  la somme de 2 000 euros à verser à M. et Mme A... ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 8 décembre 2016 est annulé. <br/>
<br/>
Article 2 : La requête à fin de sursis à exécution présentée par la commune de Plan-de-Cuques devant la cour administrative d'appel de Marseille et le surplus des conclusions du pourvoi de la commune sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Plan-de-Cuques et à M. et Mme A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
