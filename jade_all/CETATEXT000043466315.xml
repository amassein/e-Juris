<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466315</ID>
<ANCIEN_ID>JG_L_2021_04_000000451249</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/63/CETATEXT000043466315.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/04/2021, 451249, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451249</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA-PRIGENT-DRUSCH</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451249.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 31 mars et le 7 avril 2021 au secrétariat du contentieux du Conseil d'Etat, Mme D... J..., M. L... O..., M. E... H..., M. P... N..., M. Q..., M. B... K..., Mme I... F..., M. G... M..., Mme D... A... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la circulaire n° 6248/SG du Premier ministre du 22 février 2021 relative aux mesures frontalières mises en oeuvre dans le cadre de l'état d'urgence sanitaire en ce qu'elle ne prévoit pas de dérogation à l'interdiction d'entrée sur le territoire français pour les conjoints et enfants à charge des ressortissants algériens titulaires d'un certificat de résidence en leur qualité de médecin exerçant en France ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre les mesures réglementaires strictement proportionnées aux risques sanitaires liés à l'entrée en France des conjoints et enfants à charge des ressortissants algériens titulaires d'un certificat de résidence en leur qualité de médecin ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite eu égard aux troubles dans leurs conditions d'existence, causés par la séparation depuis plusieurs mois de leur famille, ce qui constitue une atteinte grave et immédiate à leur droit à mener une vie privée et familiale normale ainsi qu'à l'intérêt supérieur de l'enfant pendant une période relativement longue ; <br/>
              - il existe un doute sérieux quant à la légalité de la circulaire attaquée ;<br/>
              - elle porte une atteinte disproportionnée et injustifiée au droit de mener une vie familiale normale dès lors que, d'une part, le nombre de personnes concernées est limité et, d'autre part, des mesures efficaces sont déjà mises en oeuvre pour limiter le risque sanitaire ;<br/>
              - elle méconnaît le principe d'égalité dès lors que, en ne prévoyant pas de dérogation à l'interdiction d'entrer en France pour les membres de la famille des médecins étrangers alors que de telles dérogations sont possibles pour les étudiants ou les membres de la famille des titulaires du " passeport talent ", elle instaure une différence de traitement qui n'est pas fondée sur un critère objectif et qui est d'autant plus injustifiée que les services hospitaliers sont très sollicités et la présence des requérants est jugée indispensable par les chefs de services dans lesquels ils exercent.<br/>
<br/>
              Par un mémoire en défense, enregistré le 7 avril 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et qu'aucun moyen n'est de nature à créer un doute sérieux quant à la légalité de la circulaire attaquée. <br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
              Vu le mémoire, enregistré le 13 avril 2021, présenté par le ministre de l'intérieur ;<br/>
<br/>
              Vu le mémoire, enregistré le 14 avril 2021, présenté par Mme J... et autres ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'accord franco-algérien du 27 décembre 1968 modifié relatif à la circulation, à l'emploi et au séjour des ressortissants algériens et de leurs familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-99 du 30 janvier 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme D... J... et les autres requérants, et d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 12 avril 2021, à 10 heures : <br/>
<br/>
              - Me Prigent, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme J... et autres ;<br/>
<br/>
              - le représentant des requérants ; <br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 13 avril à 12 heures puis au 14 avril à 12 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Mme J... et autres demandent la suspension de l'exécution de la circulaire du Premier ministre du 22 février 2021 ayant pour objet les mesures frontalières mises en oeuvre dans le cadre de l'état d'urgence sanitaire en tant qu'elle fait obstacle à ce que les conjoints et enfants à charge des ressortissants algériens venus en France pour concourir à la lutte contre l'épidémie de covid-19 puissent venir les rejoindre en France.<br/>
<br/>
              En ce qui concerne le cadre juridique du litige :<br/>
<br/>
              3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain (...) en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) ". Aux termes de l'article L. 3131-15 du même code : " I.- Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 1° Réglementer ou interdire la circulation des personnes (...). / II.- (...) La liste des zones de circulation de l'infection est fixée par arrêté du ministre chargé de la santé. / (...) ". Ces mesures " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. " <br/>
<br/>
              4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou Covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit les autorités compétentes à prendre diverses mesures destinées à réduire les risques de contagion. Une nouvelle progression de l'épidémie de covid-19 sur le territoire national a conduit le Président de la République à prendre, le 14 octobre 2020, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire de la République. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire modifié par la loi du 15 février 2021 a prorogé l'état d'urgence sanitaire jusqu'au 1er juin 2021 inclus. Le Premier ministre a pris, sur le fondement de l'article L. 313-15 du code de la santé publique, le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'urgence sanitaire et le ministre chargé de la santé a pris, le 10 juillet 2020, en application de ces dispositions, un arrêté identifiant les zones de circulation de l'infection du virus SARS-CoV-2 dont l'article 1er dispose que " Pour l'application du II de l'article L. 3131-15 du code de la santé publique, (...) constituent une zone de circulation de l'infection du virus SARS-CoV-2 l'ensemble des pays du monde à l'exception, pour la France, des collectivités mentionnées à l'article 72-3 de la Constitution. " <br/>
<br/>
              5. La circulaire du 22 février 2021 fixe les catégories de personnes qui, par exception, bien que provenant d'un pays dans lequel le virus est regardé comme circulant, peuvent être admises à entrer en France, parmi lesquelles figurent celle de " professionnel de santé ou de recherche étranger concourant à la lutte contre la Covid-19 " et celle de " ressortissant de pays tiers disposant d'un visa de long séjour (VLS) " passeport Talent " ".<br/>
<br/>
              En ce qui concerne la demande en référé : <br/>
<br/>
              6. Il résulte de l'instruction que les requérants, ressortissants algériens, ont été autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19. A ce titre, ils bénéficient d'un certificat de résidence permettant l'exercice d'une activité professionnelle soumise à autorisation délivrée conformément aux stipulations du c) de l'article 7 de l'accord franco-algérien du 27 décembre 1968. Ils ont vu l'examen des demandes de visas visiteurs de leurs conjoints et enfants à charge être refusé en raison des mesures frontalières mises en oeuvre dans le cadre de l'état d'urgence sanitaire. Ils soutiennent que, faute d'avoir prévu spécifiquement leur situation, la circulaire du 25 janvier 2021 est entachée d'illégalité. <br/>
<br/>
              En ce qui concerne la condition d'urgence : <br/>
<br/>
              7. Pour l'application de l'article L. 521-1 du code de justice administrative, la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre.<br/>
<br/>
              8. Il résulte de l'instruction que, depuis le mois d'octobre 2020, les services consulaires français refusent de délivrer des visas aux conjoints et enfants des ressortissants algériens autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19. Eu égard aux troubles dans les conditions d'existence subies par les conjoints et les familles qui doivent rester séparées depuis désormais de nombreux mois les requérants sont fondés à soutenir que la condition d'urgence est remplie.<br/>
<br/>
              En ce qui concerne le doute sérieux quant à la légalité de la décision contestée : <br/>
<br/>
              9. Il résulte de l'instruction, et notamment des déclarations des représentants du ministre de l'intérieur lors de l'audience publique, que la possibilité pour le conjoint et les enfants d'un ressortissant titulaire d'un " passeport Talent " de rejoindre le territoire français a été maintenue après l'intervention de la circulaire du 22 février 2021. Les stipulations de l'accord franco-algérien, qui régissent de manière complète les conditions dans lesquelles les ressortissants algériens peuvent être admis à séjourner en France et y exercer une activité professionnelle, les règles concernant la nature des titres de séjour qui peuvent leur être délivrés, ainsi que les conditions dans lesquelles leurs conjoints et leurs enfants mineurs peuvent s'installer en France, font obstacle à ce qu'ils puissent être titulaires d'un " passeport Talent " puisque les dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile, notamment celles de l'article L. 313-20 relatives au " passeport Talent ", ne leur sont pas applicables. Si le ministre de l'intérieur soutient que tous les médecins ressortissants de pays tiers autorisés à entrer en France dans le cadre de la lutte contre l'épidémie de covid-19 sont soumis à la délivrance d'un visa salarié et ne relèvent pas de la procédure " passeport Talent ", il résulte des éléments produits par les requérants qu'un " passeport Talent " a pu être délivré à des médecins étrangers recrutés au même moment qu'eux, la délivrance de ce visa permettant de faire entrer sur le territoire français le conjoint et les enfants.<br/>
<br/>
              10. Dans ces conditions, et alors même que les dispositions applicables ne font pas obstacle à ce que, face à une situation de pandémie, le Premier ministre restreigne provisoirement, par des mesures réglementaires justifiées par des considérations sanitaires, les entrées sur le territoire national, en particulier en provenance de pays à risque, y compris en ce qui concerne les conjoints et les enfants d'étrangers bénéficiant d'un titre de séjour donnant en principe un droit au séjour en France, le moyen tiré de ce que les prescriptions contestées créeraient une différence de traitement non justifiée en ne prévoyant aucune dérogation pour les ressortissants algériens autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19 est, en l'espèce et en l'état de l'instruction, propre à créer un doute sérieux sur leur légalité. La présente décision implique seulement qu'il soit enjoint, en application de l'article L. 911-1 du code de justice administrative, au Premier ministre de prendre les mesures réglementaires strictement proportionnées aux risques sanitaires liés à l'entrée en France des conjoints et enfants des ressortissants algériens autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19. <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 4 000 euros à verser à l'ensemble des requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'exécution de la circulaire du Premier ministre n° 6248/SG du 22 février 2021 est suspendue en tant qu'elle ne prévoit pas de dérogations pour permettre l'entrée en France des conjoints et enfants des ressortissants algériens autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19.<br/>
Article 2: Il est enjoint au Premier ministre de prendre les mesures réglementaires strictement proportionnées aux risques sanitaires liés à l'entrée en France des conjoints et enfants des ressortissants algériens autorisés à entrer en France pour exercer une activité médicale en lien avec la lutte contre l'épidémie de covid-19.<br/>
Article 3 : L'Etat versera une somme globale de 4 000 euros aux requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente ordonnance sera notifiée à Mme C... J..., première dénommée, pour l'ensemble des requérants, et au ministre de l'intérieur. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
