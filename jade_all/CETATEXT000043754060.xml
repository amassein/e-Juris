<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043754060</ID>
<ANCIEN_ID>JG_L_2021_07_000000433733</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/40/CETATEXT000043754060.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/07/2021, 433733</TITRE>
<DATE_DEC>2021-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433733</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433733.20210702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               La commune de Neuilly-sur-Seine a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du 15 octobre 2013 par laquelle la ministre de l'égalité des territoires et du logement a maintenu à 746 logements son obligation de réalisation de logements sociaux pour la période 2008-2010. Par un jugement n° 1400344 du 11 juillet 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
               Par un arrêt n° 17VE02936 du 20 juin 2019, la cour administrative d'appel de Versailles a, sur appel de la commune de Neuilly-sur-Seine, annulé ce jugement et la décision du 15 octobre 2013.<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 août et 20 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - le code de la construction et de l'habitation ;<br/>
               - la loi n° 2013-61 du 18 janvier 2013 ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
               - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Neuilly-sur-Seine ;<br/>
<br/>
               Vu les notes en délibéré, enregistrées les 17 et 28 juin 2021, présentées par la commune de Neuilly-sur-Seine ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               Sur les règles applicables :<br/>
<br/>
               1. Dans sa rédaction applicable à la période triennale 2005-2007 en litige, l'article L. 302-8 du code de la construction et de l'habitation prévoit que, pour atteindre, dans les communes d'Ile-de-France de plus de 1 500 habitants, un nombre de logements locatifs sociaux au moins égal à 20 % du nombre de résidences principales : " (...) le conseil municipal définit un objectif de réalisation de logements locatifs sociaux par période triennale. Cet objectif ne peut être inférieur au nombre de logements locatifs sociaux nécessaires pour atteindre, au plus tard à la fin de l'année 2025, le taux mentionné, selon les cas, au premier, au deuxième ou au septième alinéa de l'article L. 302-5 ". Aux termes du même article : " Les programmes locaux de l'habitat précisent l'échéancier annuel et les conditions de réalisation, ainsi que la répartition équilibrée de la taille, des logements sociaux soit par des constructions neuves, soit par l'acquisition de bâtiments existants, par période triennale. (...) A défaut de programme local de l'habitat adopté, la commune prend, sur son territoire, les dispositions nécessaires pour permettre la réalisation du nombre de logements locatifs sociaux prévus au premier alinéa ci-dessus. Les périodes triennales visées au présent alinéa débutent le 1er janvier 2002 ". Dans les cas mentionnés au I de l'article L. 302-5 du même code, le taux de 20 % du nombre de résidences principales a été porté à 25 % par la loi du 18 janvier 2013 relative à la mobilisation du foncier public en faveur du logement et au renforcement des obligations de production de logement social.<br/>
<br/>
               2. Lorsqu'au terme d'une des périodes triennales mentionnées ci-dessus les objectifs de réalisation de logements locatifs sociaux n'ont pas été tenus, le préfet peut, dans les conditions fixées par l'article L. 302-9-1 du code de la construction et de l'habitation, prononcer par arrêté motivé la carence de la commune, lequel peut faire l'objet d'un recours de pleine juridiction. Par ailleurs, aux termes de l'article L. 302-9-1-1 du même code dans sa rédaction applicable à l'espèce : " I. - Pour les communes n'ayant pas respecté la totalité de leur objectif triennal, le représentant de l'Etat dans le département réunit une commission chargée de l'examen du respect des obligations de réalisation de logements sociaux. (...) / Cette commission est chargée d'examiner les difficultés rencontrées par la commune l'ayant empêchée de remplir la totalité de ses objectifs, d'analyser les possibilités et les projets de réalisation de logements sociaux sur le territoire de la commune et de définir des solutions permettant d'atteindre ces objectifs. / Si la commission parvient à déterminer des possibilités de réalisation de logements sociaux correspondant à l'objectif triennal passé sur le territoire de la commune, elle peut recommander l'élaboration, pour la prochaine période triennale, d'un échéancier de réalisations de logements sociaux permettant, sans préjudice des obligations fixées au titre de la prochaine période triennale, de rattraper le retard accumulé au cours de la période triennale échue. Si la commune a fait l'objet d'un arrêté de carence, la commission peut doubler la majoration prévue par l'arrêté. / Si la commission parvient à la conclusion que la commune ne pouvait, pour des raisons objectives, respecter son obligation triennale, elle saisit, avec l'accord du maire concerné, une commission nationale placée auprès du ministre chargé du logement. / II. - La commission nationale (...) entend le maire de la commune concernée ainsi que le représentant de l'Etat du département dans lequel la commune est située. / Si la commission parvient à la conclusion que la commune ne pouvait, pour des raisons objectives, respecter son obligation triennale, elle peut recommander au ministre chargé du logement un aménagement des obligations prévues à l'article L. 302-8. / Si la commission parvient à déterminer des possibilités de réalisation de logements sociaux correspondant à l'objectif triennal passé, elle recommande l'élaboration, pour la prochaine période triennale, d'un échéancier de réalisations de logements sociaux permettant, sans préjudice des obligations fixées au titre de la prochaine période triennale, de rattraper le retard accumulé au cours de la période triennale échue et la mise en œuvre de l'article L. 302-9-1. / Les avis de la commission sont motivés et rendus publics. / (...) ".<br/>
<br/>
               3. Il résulte de ces dispositions que, lorsque, pour une commune n'ayant pas respecté son objectif triennal de réalisation de logements sociaux, la commission départementale estime que l'absence d'atteinte des objectifs s'explique par des raisons objectives et que la commission nationale, saisie par la commission départementale, estime à son tour que cette absence d'atteinte s'explique par des raisons objectives, elle peut saisir le ministre chargé du logement d'une recommandation tendant à aménager les obligations prévues à l'article L. 302-8 du code de la construction et de l'habitation. Il incombe alors au ministre chargé du logement d'apprécier, au vu des circonstances ayant prévalu au cours de la période triennale en question et sous le contrôle du juge de l'excès de pouvoir, si des raisons objectives justifient que la commune n'ait pas respecté l'obligation résultant des objectifs fixés pour cette période. Dans l'affirmative, il appartient au ministre de modifier le cas échéant, compte tenu des circonstances qui prévalent à la date de sa décision, les objectifs de la période triennale qui est en cours à la date à laquelle il se prononce ou, s'ils sont déjà fixés, ceux d'une période ultérieure.<br/>
<br/>
               Sur le pourvoi :<br/>
<br/>
               4. Il ressort des pièces du dossier soumis aux juges du fond que, la commune de Neuilly-sur-Seine n'ayant atteint que 49 % de ses objectifs de réalisation de logements locatifs sociaux au cours de la période triennale 2005-2007, le préfet des Hauts-de-Seine a, par un arrêté du 16 juillet 2008, prononcé sa carence et a, parallèlement, saisi la commission départementale mentionnée à l'article L. 302-9-1-1 du code de la construction et de l'habitation. Celle-ci ayant estimé que l'absence d'atteinte des objectifs s'expliquait par des raisons objectives, elle a saisi la commission nationale mentionnée au même article qui, par un avis du 4 mars 2009, a recommandé au ministre chargé du logement de ramener de 746 à 600 logements les objectifs de la commune au titre de la période 2008-2010. Par une décision du 3 novembre 2009, le ministre a toutefois maintenu à 746 logements l'objectif assigné à la commune de Neuilly-sur-Seine au titre de cette période. Cette décision ayant été annulée pour insuffisance de motivation par un arrêt de la cour administrative d'appel de Versailles du 28 mars 2013, le ministre a, par une nouvelle décision du 15 octobre 2013, réitéré son refus d'aménager les objectifs de la commune. La ministre de la cohésion des territoires et des relations avec les collectivités locales se pourvoit en cassation contre l'arrêt du 20 juin 2019 par lequel la cour administrative d'appel de Versailles a annulé cette décision.<br/>
<br/>
               5. Il résulte des termes de l'arrêt attaqué que, pour annuler le refus litigieux, la cour administrative d'appel s'est fondée sur ce que la commune de Neuilly-sur-Seine pouvait se prévaloir, pour justifier de l'insuffisance de construction de logements sociaux sur la période triennale 2005-2007, de raisons objectives tenant, notamment, à la rareté et au coût anormalement élevé du foncier disponible sur son territoire.<br/>
<br/>
               6. En jugeant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que la commune de Neuilly-sur-Seine n'avait, au cours de la période triennale en litige, pas de programme local de l'habitat et n'avait, avant cette période ou au cours de celle-ci, ni modifié ni révisé son plan local d'urbanisme en vue de favoriser le logement social, que la rareté du foncier et le coût consécutivement élevé de la construction constituaient des raisons objectives susceptibles de faire obstacle au respect des obligations prévues à l'article L. 302-8 du code de la construction et de l'habitation, sans rechercher si les raisons invoquées par la commune ne résultaient pas, pour une large part, de choix opérés par elle, la cour a commis une erreur de droit.<br/>
<br/>
               7. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
               8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel formé par la commune de Neuilly-sur-Seine contre le jugement du tribunal administratif de Cergy-Pontoise du 11 juillet 2017.<br/>
<br/>
               9. En premier lieu, il ressort des pièces du dossier que la commune de Neuilly-sur-Seine n'avait, à l'époque de la période triennale 2005-2007, pas de programme local de l'habitat depuis la fin d'un premier programme à la fin de l'année 1999. Il ressort également des pièces du dossier qu'elle n'a, avant cette période ou au cours de celle-ci, ni modifié ou révisé ses documents d'urbanisme en vue de favoriser le logement social, n'ayant notamment jamais inscrit d'emplacement réservé au logement social dans son plan d'occupation des sols ou son plan local d'urbanisme, ni imposé de quota minimum de logements sociaux aux programmes immobiliers, s'étant bornée à adopter la faculté légale de majoration du coefficient d'occupation des sols pour la construction de logements sociaux. Ainsi, les obstacles invoqués par la commune, liés à l'absence de foncier disponible et au coût extrêmement élevé du foncier ne peuvent, en l'espèce, dès lors qu'ils résultent en grande partie de la faiblesse des instruments dont elle s'était, à l'époque, dotée pour les combattre, être regardés comme revêtant, pour la commune requérante et sur la période en litige, le caractère d'une raison objective au sens des dispositions de l'article L. 302-9-1-1 du code de la construction et de l'habitation.<br/>
<br/>
               10. En deuxième lieu, il ne ressort pas des pièces du dossier que l'existence, sur le territoire de la commune requérante, de terrains inondables inconstructibles soit de nature à justifier l'absence d'atteinte des objectifs de réalisation de logements sociaux sur la même période.<br/>
<br/>
               11. Dans ces conditions, alors qu'il ressort au surplus des pièces du dossier que la commune de Neuilly-sur-Seine n'a, au cours de la période 2005-2007, pas pleinement mobilisé les ressources d'accroissement du parc de logements sociaux par la voie de la préemption ou du conventionnement du parc privé, la ministre de l'égalité des territoires et du logement, en refusant, par sa décision du 15 octobre 2013, de modifier les objectifs triennaux fixés sur le fondement de l'article L. 302-8 du code de la construction et de l'habitation, n'a pas fait une inexacte application des dispositions de l'article L. 302-9-1-1 du même code.<br/>
<br/>
               12. La commune de Neuilly-sur-Seine n'est, par suite, pas fondée à soutenir que c'est à tort que, par le jugement attaqué du 11 juillet 2017, le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de cette décision.<br/>
<br/>
               13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre, tant en appel qu'en cassation, la commune de Neuilly-sur-Seine. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
               --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 20 juin 2019 est annulé. <br/>
Article 2 : L'appel formé par la commune de Neuilly-sur-Seine devant la cour administrative d'appel de Versailles est rejeté.<br/>
Article 3 : Les conclusions présentées par la commune de Neuilly-sur-Seine au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Neuilly-sur-Seine et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-04 LOGEMENT. - HABITATIONS À LOYER MODÉRÉ. - AMÉNAGEMENT DE L'OBLIGATION DE RÉALISER DES LOGEMENTS SOCIAUX JUSTIFIÉ PAR DES RAISONS OBJECTIVES (ART. L. 302-9-1-1 DU CCH) - NOTION - 1) CONTRÔLE DU JUGE - CONTRÔLE NORMAL - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. - POUVOIRS ET DEVOIRS DU JUGE. - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. - APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - EXISTENCE DE RAISONS OBJECTIVES JUSTIFIANT UN AMÉNAGEMENT DE L'OBLIGATION DE RÉALISATION DE LOGEMENTS SOCIAUX (ART. L. 302-9-1-1 DU CCH).
</SCT>
<ANA ID="9A"> 38-04 Il résulte de l'article L. 302-9-1-1 du code de la construction et de l'habitation (CCH) que, lorsque, pour une commune n'ayant pas respecté son objectif triennal de réalisation de logements sociaux, la commission départementale estime que l'absence d'atteinte des objectifs s'explique par des raisons objectives et que la commission nationale, saisie par la commission départementale, estime à son tour que cette absence d'atteinte s'explique par des raisons objectives, elle peut saisir le ministre chargé du logement d'une recommandation tendant à aménager les obligations prévues à l'article L. 302-8 du CCH.... ...1) Il incombe alors au ministre chargé du logement d'apprécier, au vu des circonstances ayant prévalu au cours de la période triennale en question et sous le contrôle du juge de l'excès de pouvoir, si des raisons objectives justifient que la commune n'ait pas respecté l'obligation résultant des objectifs fixés pour cette période.......Dans l'affirmative, il appartient au ministre de modifier le cas échéant, compte tenu des circonstances qui prévalent à la date de sa décision, les objectifs de la période triennale qui est en cours à la date à laquelle il se prononce ou, s'ils sont déjà fixés, ceux d'une période ultérieure.......2) Commune n'ayant atteint que 49 % de ses objectifs de réalisation de logements locatifs sociaux au cours de la période triennale 2005-2007 et se prévalant, pour justifier de cette insuffisance, de raisons tenant, notamment, à la rareté et au coût anormalement élevé du foncier disponible sur son territoire.......Commune n'ayant, à l'époque de la période triennale 2005-2007, pas de programme local de l'habitat depuis la fin d'un premier programme à la fin de l'année 1999, et n'ayant, avant cette période ou au cours de celle-ci, ni modifié ou révisé ses documents d'urbanisme en vue de favoriser le logement social, n'ayant notamment jamais inscrit d'emplacement réservé au logement social dans son plan d'occupation des sols ou son plan local d'urbanisme, ni imposé de quota minimum de logements sociaux aux programmes immobiliers, s'étant bornée à adopter la faculté légale de majoration du coefficient d'occupation des sols pour la construction de logements sociaux. ......Ainsi, les obstacles invoqués par la commune, liés à l'absence de foncier disponible et au coût extrêmement élevé du foncier ne peuvent, en l'espèce, dès lors qu'ils résultent en grande partie de la faiblesse des instruments dont elle s'était, à l'époque, dotée pour les combattre, être regardés comme revêtant, pour la commune requérante et sur la période en litige, le caractère d'une raison objective au sens des dispositions de l'article L. 302-9-1-1 du CCH.</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur l'existence de raisons objectives justifiant, en application de l'article L. 302-9-1-1 du code de la construction et de l'habitation (CCH), un aménagement par le ministre des obligations, prévues à l'article L. 302-8 de ce code, qui pèsent une commune en matière de réalisation de logements sociaux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
