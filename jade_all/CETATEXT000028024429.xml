<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028024429</ID>
<ANCIEN_ID>JG_L_2013_10_000000357745</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/02/44/CETATEXT000028024429.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 02/10/2013, 357745, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357745</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:357745.20131002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 20 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SARL Radio Impact FM, dont le siège est 14 rue Pailleron à Lyon (69004), représentée par son gérant en exercice ; la SARL Radio Impact FM demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 19 juillet 2011, confirmée sur recours gracieux le 25 janvier 2012, par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa demande d'autorisation d'exploiter par voie hertzienne terrestre le service radiophonique Radio Impact FM dans la zone de Mâcon ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que par décision n° 2010-670 du 14 septembre 2010 le Conseil supérieur de l'audiovisuel a lancé un appel à candidatures en vue de l'attribution de fréquences radiophoniques dans le ressort du comité territorial de l'audiovisuel de Dijon ; que le conseil supérieur a délibéré le 28 septembre 2010 de l'attribution des fréquences disponibles dans la zone de Mâcon ; que la société Radio Impact FM demande l'annulation pour excès de pouvoir de la décision du Conseil supérieur de l'audiovisuel du 19 juillet 2011 rejetant sa candidature ; <br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              2. Considérant qu'aux termes du second alinéa de l'article 32 de la loi du 30 septembre 1986 relative à la liberté de communication : " Les refus d'autorisation sont motivés (...). Lorsqu'ils s'appliquent à un service de radio diffusé par voie hertzienne terrestre, ils peuvent être motivés par référence à un rapport de synthèse explicitant les choix du Conseil au regard des critères mentionnés aux articles 1er à 29 " ; que la décision attaquée, motivée par un rapport de synthèse commun à plusieurs candidatures comme le permettent ces dispositions, permet d'identifier ceux des critères énumérés aux articles 1er et 29 de la loi du 30 septembre 1986 sur lesquels le Conseil supérieur de l'audiovisuel s'est fondé pour refuser l'autorisation demandée par la société Radio Impact FM et précise les éléments de fait qu'il a retenus pour rejeter la candidature de celle-ci ; qu'ainsi, la décision attaquée satisfait à l'obligation faite au Conseil supérieur de l'audiovisuel par l'article 32 de la loi précitée de motiver les refus d'autorisation ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 29 de la loi du 30 septembre 1986 : " Sous réserve des dispositions de l'article 26 de la présente loi, l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article. (...) Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. Il tient également compte : 1° De l'expérience acquise par le candidat dans les activités de communication ; / 2° Du financement et des perspectives d'exploitation du service notamment en fonction des possibilités de partage des ressources publicitaires entre les entreprises de presse écrite et les services de communication audiovisuelle ; (...) Le Conseil veille également au juste équilibre entre les réseaux nationaux de radiodiffusion, d'une part, et les services locaux, régionaux et thématiques indépendants, d'autre part. Il s'assure que le public bénéficie de services dont les programmes contribuent à l'information politique et générale. (...) " ; <br/>
<br/>
              4. Considérant, d'autre part, que par ses communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le Conseil supérieur de l'audiovisuel, faisant usage de la compétence que lui confère le deuxième alinéa de l'article 29 de la loi du 30 septembre 1986 de déterminer des catégories de service en vue de l'appel à candidature pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre, a déterminé cinq catégories ainsi définies : services associatifs éligibles au fond de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (catégorie B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (catégorie C), services thématiques à vocation nationale (catégorie D) et les services généralistes à vocation nationale (catégorie E) ;<br/>
<br/>
              5. Considérant qu'avant l'appel aux candidatures litigieux, étaient autorisés dans la zone de Mâcon les services Aléo et Club Attitude en catégorie A, Radio Scoop en catégorie B, Chérie FM Bourgogne Franche-Comté en catégorie C, Skyrock en catégorie D et Europe 1, RMC et RTL en catégorie E ; que le Conseil supérieur de l'audiovisuel a attribué les quatre fréquences disponibles à RCF Parabole en catégorie A, à Virgin Radio Mâcon et Nostalgie Mâcon Villefranche en catégorie C et à Rire et Chansons en catégorie D ; que la société requérante fait valoir que le service Radio Impact FM qu'elle exploite, s'adressant à un public adulte et senior, propose une programmation composée de titres des années 60, 70 et 80, faisant en outre une large place à l'accordéon et aux titres des années 30 à 50, et est dès lors susceptible de toucher un public plus large que Nostalgie Mâcon ; qu'il ressort toutefois des pièces du dossier que la composante musicale du programme de Radio Impact FM était déjà représentée en catégorie A par le service Aléo, dont la programmation est proche et s'adresse à un public adulte et senior, et que le service Nostalgie Mâcon, autorisé en catégorie C, propose une offre de titres de variétés des années 60 à 90 proche de celle Radio Impact FM, accompagnée de programmes d'intérêt local et d'un " décrochage " spécifique à la zone considérée ; qu'il ne ressort pas des pièces du dossier que les choix opérés par le Conseil supérieur de l'audiovisuel résultent d'une erreur d'appréciation au regard des critères prévus à l'article 29 de la loi du 30 septembre 1986, notamment les impératifs prioritaires de sauvegarde du pluralisme des courants d'expression socioculturels et de diversification des opérateurs ainsi que la nécessité d'éviter les abus de position dominante et les pratiques entravant le libre exercice de la concurrence et le juste équilibre entre les réseaux nationaux de radiodiffusion et les services locaux, régionaux et thématiques indépendants ; qu'en relevant, sans en faire le motif déterminant de sa décision, que Nostalgie Macon justifiait d'une expérience à Mâcon avant l'appel à candidatures, le Conseil supérieur de l'audiovisuel n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la SARL Radio Impact FM n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne sauraient, par suite, être accueillies ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Radio Impact FM est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Radio Impact FM et au Conseil supérieur de l'audiovisuel. <br/>
Copie pour information en sera adressée au ministre de la culture et de la communication.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
