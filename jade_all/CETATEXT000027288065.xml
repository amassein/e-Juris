<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288065</ID>
<ANCIEN_ID>JG_L_2013_04_000000364105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288065.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 08/04/2013, 364105</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Marie-Astrid Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364105.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 novembre et 11 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; il demande au Conseil d'Etat : <br/>
              1°) d'annuler l'ordonnance n° 1202230 du 9 novembre 2012 par laquelle le juge des référés du tribunal administratif de Nancy, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant, d'une part, à la suspension de l'arrêté du 10 octobre 2012 prononçant sa révocation jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision, d'autre part, à ce qu'il soit enjoint à la commune de Saint-Dié-des-Vosges de le réintégrer dans un délai de 8 jours, sous astreinte de 150 euros par jour de retard ; <br/>
<br/>
               2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Dié-des-Vosges la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Didier, Pinet, avocat de M.A..., et de la SCP Monod, Colin, avocat de la commune de Saint-Dié-des-Vosges,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Didier, Pinet, avocat de M. A..., et à la SCP Monod, Colin, avocat de la commune de Saint-Dié-des-Vosges ;<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis au juge des référés que, par arrêté du 8 juillet 2011 pris à la suite de l'avis du conseil de discipline, le maire de la commune de Saint-Dié-des-Vosges a prononcé la révocation de M.A..., chef du service de la police municipale ; que, par avis du 26 septembre 2011, le conseil de discipline régional de recours, saisi par ce fonctionnaire, a préconisé que la sanction d'exclusion de ses fonctions pour une durée de six mois lui soit infligée ; que le maire de la commune a, en application de l'article 91 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, selon lequel l'autorité territoriale ne peut prononcer, à l'encontre d'un fonctionnaire territorial, une sanction plus sévère que celle proposée par le conseil de discipline de recours, pris, par arrêté du 15 décembre 2011, une sanction conforme à cet avis et réintégré le fonctionnaire dans ses fonctions ; que, sur demande de la commune, le tribunal administratif de Nancy a, par jugement du 28 août 2012, annulé cet avis pour erreur manifeste d'appréciation ; qu'à la suite de ce jugement, le maire de la commune a prononcé, par arrêté du 10 octobre 2012, la révocation de ce fonctionnaire ; que, par l'ordonnance contre laquelle M. A... se pourvoit en cassation, le juge des référés du même tribunal a rejeté sa demande tendant à ce que, sur le fondement de l'article L. 521-1 du code de justice administrative, soit prononcée la suspension de l'exécution de cette nouvelle décision ; <br/>
<br/>
              2. Considérant qu'aucune disposition législative ou réglementaire ni aucun principe, et notamment pas le principe d'impartialité eu égard au fait que l'article 89 de la loi du 26 janvier 1984 permet au maire d'infliger une sanction plus sévère que celle préconisée par le conseil de discipline régional de recours mais distincte de la révocation, ne faisait obstacle à ce que le magistrat qui a présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis du conseil de discipline régional de recours siégeât en qualité de juge des référés pour statuer sur le litige qui lui a été soumis par M. A...et relatif à la décision de révocation  prise à la suite de ce jugement ; que, dès lors, le requérant n'est pas fondé à soutenir que le juge des référés aurait statué dans des conditions irrégulières ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              4. Considérant, d'une part, que le juge des référés a pu, sans commettre d'erreur de droit ni dénaturer les pièces du dossier qui lui était soumis, estimer qu'en l'état de l'instruction n'était pas de nature à faire naître un doute sérieux quant à la légalité de la décision de révocation le moyen tiré de ce que le maire de la commune de Saint-Dié-des-Vosges ne pouvait, en vertu du principe " non bis in idem ", le sanctionner deux fois à raison des mêmes faits, par l'arrêté du 15 décembre 2011 prononçant son exclusion temporaire de fonctions pour une durée de six mois et par l'arrêté du 10 octobre 2012  prononçant sa révocation ; <br/>
<br/>
              5. Considérant, d'autre part, qu'en jugeant que n'était pas davantage de nature à créer un doute sérieux sur la légalité de la décision de révocation, en l'état de l'instruction, le moyen tiré de ce que cette sanction était entachée d'une erreur manifeste d'appréciation en estimant que la gravité des faits reprochés à M. A...justifiait que soit prononcée à son encontre la sanction de la révocation, le juge des référés a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Saint-Dié-des-Vosges au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
<br/>
Article 2 : Les conclusions de la commune de Saint-Dié-des-Vosges présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et à la commune de Saint-Dié-des-Vosges. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. - COMPOSITION DE LA FORMATION DE JUGEMENT - PRINCIPE D'IMPARTIALITÉ - MAGISTRAT AYANT PRÉSIDÉ LA FORMATION DE JUGEMENT AYANT ANNULÉ POUR ERREUR MANIFESTE D'APPRÉCIATION L'AVIS DU CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SIÈGE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR LE LITIGE RELATIF À LA DÉCISION DE RÉVOCATION PRISE À LA SUITE DE CE JUGEMENT - ABSENCE, EU ÉGARD AU FAIT QUE L'ARTICLE 89 DE LA LOI DU 26 JANVIER 1984 PERMET AU MAIRE D'INFLIGER UNE SANCTION PLUS SÉVÈRE QUE CELLE PRÉCONISÉE PAR LE CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS MAIS DISTINCTE DE LA RÉVOCATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-03 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. - PRINCIPE D'IMPARTIALITÉ - MAGISTRAT AYANT PRÉSIDÉ LA FORMATION DE JUGEMENT AYANT ANNULÉ POUR ERREUR MANIFESTE D'APPRÉCIATION L'AVIS DU CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SIÈGE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR LE LITIGE RELATIF À LA DÉCISION DE RÉVOCATION PRISE À LA SUITE DE CE JUGEMENT - ABSENCE, EU ÉGARD AU FAIT QUE L'ARTICLE 89 DE LA LOI DU 26 JANVIER 1984 PERMET AU MAIRE D'INFLIGER UNE SANCTION PLUS SÉVÈRE QUE CELLE PRÉCONISÉE PAR LE CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS MAIS DISTINCTE DE LA RÉVOCATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - MAGISTRAT AYANT PRÉSIDÉ LA FORMATION DE JUGEMENT AYANT ANNULÉ POUR ERREUR MANIFESTE D'APPRÉCIATION L'AVIS DU CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SIÈGE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR LE LITIGE RELATIF À LA DÉCISION DE RÉVOCATION PRISE À LA SUITE DE CE JUGEMENT - ABSENCE, EU ÉGARD AU FAIT QUE L'ARTICLE 89 DE LA LOI DU 26 JANVIER 1984 PERMET AU MAIRE D'INFLIGER UNE SANCTION PLUS SÉVÈRE QUE CELLE PRÉCONISÉE PAR LE CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS MAIS DISTINCTE DE LA RÉVOCATION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - PRINCIPE D'IMPARTIALITÉ - MAGISTRAT AYANT PRÉSIDÉ LA FORMATION DE JUGEMENT AYANT ANNULÉ POUR ERREUR MANIFESTE D'APPRÉCIATION L'AVIS DU CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SIÈGE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR LE LITIGE RELATIF À LA DÉCISION DE RÉVOCATION PRISE À LA SUITE DE CE JUGEMENT - ABSENCE, EU ÉGARD AU FAIT QUE L'ARTICLE 89 DE LA LOI DU 26 JANVIER 1984 PERMET AU MAIRE D'INFLIGER UNE SANCTION PLUS SÉVÈRE QUE CELLE PRÉCONISÉE PAR LE CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS MAIS DISTINCTE DE LA RÉVOCATION.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - MAGISTRAT AYANT PRÉSIDÉ LA FORMATION DE JUGEMENT AYANT ANNULÉ POUR ERREUR MANIFESTE D'APPRÉCIATION L'AVIS DU CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS - CIRCONSTANCE FAISANT OBSTACLE À CE QU'IL SIÈGE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR LE LITIGE RELATIF À LA DÉCISION DE RÉVOCATION PRISE À LA SUITE DE CE JUGEMENT - ABSENCE, EU ÉGARD AU FAIT QUE L'ARTICLE 89 DE LA LOI DU 26 JANVIER 1984 PERMET AU MAIRE D'INFLIGER UNE SANCTION PLUS SÉVÈRE QUE CELLE PRÉCONISÉE PAR LE CONSEIL DE DISCIPLINE RÉGIONAL DE RECOURS MAIS DISTINCTE DE LA RÉVOCATION.
</SCT>
<ANA ID="9A"> 36-13-01 Cas d'une commune ayant infligé à l'un de ses agents une sanction d'exclusion de ses fonctions pour une durée de six mois, conformément à l'avis du conseil de discipline régional de recours, puis, après avoir obtenu l'annulation de cet avis pour erreur manifeste d'appréciation en raison de l'insuffisance de la sanction préconisée, ayant prononcé la révocation de cet agent.,,,Après l'annulation de l'avis du conseil de discipline régional de recours préconisant la sanction d'exclusion, le maire avait à sa disposition un éventail de sanctions plus sévères parmi lesquelles la révocation n'était qu'un des choix possibles. Dès lors aucune disposition ni aucun principe, notamment pas le principe d'impartialité, ne s'opposait à ce que le magistrat qui avait présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis siégeât en qualité de juge des référés pour statuer sur la sanction de révocation prise à la suite de ce jugement.</ANA>
<ANA ID="9B"> 37-03 Cas d'une commune ayant infligé à l'un de ses agents une sanction d'exclusion de ses fonctions pour une durée de six mois, conformément à l'avis du conseil de discipline régional de recours, puis, après avoir obtenu l'annulation de cet avis pour erreur manifeste d'appréciation en raison de l'insuffisance de la sanction préconisée, ayant prononcé la révocation de cet agent.,,,Après l'annulation de l'avis du conseil de discipline régional de recours préconisant la sanction d'exclusion, le maire avait à sa disposition un éventail de sanctions plus sévères parmi lesquelles la révocation n'était qu'un des choix possibles. Dès lors aucune disposition ni aucun principe, notamment pas le principe d'impartialité, ne s'opposait à ce que le magistrat qui avait présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis siégeât en qualité de juge des référés pour statuer sur la sanction de révocation prise à la suite de ce jugement.</ANA>
<ANA ID="9C"> 54-035-02-04 Cas d'une commune ayant infligé à l'un de ses agents une sanction d'exclusion de ses fonctions pour une durée de six mois, conformément à l'avis du conseil de discipline régional de recours, puis, après avoir obtenu l'annulation de cet avis pour erreur manifeste d'appréciation en raison de l'insuffisance de la sanction préconisée, ayant prononcé la révocation de cet agent.,,,Après l'annulation de l'avis du conseil de discipline régional de recours préconisant la sanction d'exclusion, le maire avait à sa disposition un éventail de sanctions plus sévères parmi lesquelles la révocation n'était qu'un des choix possibles. Dès lors aucune disposition ni aucun principe, notamment pas le principe d'impartialité, ne s'opposait à ce que le magistrat qui avait présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis siégeât en qualité de juge des référés pour statuer sur la sanction de révocation prise à la suite de ce jugement.</ANA>
<ANA ID="9D"> 54-06-01 Cas d'une commune ayant infligé à l'un de ses agents une sanction d'exclusion de ses fonctions pour une durée de six mois, conformément à l'avis du conseil de discipline régional de recours, puis, après avoir obtenu l'annulation de cet avis pour erreur manifeste d'appréciation en raison de l'insuffisance de la sanction préconisée, ayant prononcé la révocation de cet agent.,,,Après l'annulation de l'avis du conseil de discipline régional de recours préconisant la sanction d'exclusion, le maire avait à sa disposition un éventail de sanctions plus sévères parmi lesquelles la révocation n'était qu'un des choix possibles. Dès lors aucune disposition ni aucun principe, notamment pas le principe d'impartialité, ne s'opposait à ce que le magistrat qui avait présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis siégeât en qualité de juge des référés pour statuer sur la sanction de révocation prise à la suite de ce jugement.</ANA>
<ANA ID="9E"> 54-06-03 Cas d'une commune ayant infligé à l'un de ses agents une sanction d'exclusion de ses fonctions pour une durée de six mois, conformément à l'avis du conseil de discipline régional de recours, puis, après avoir obtenu l'annulation de cet avis pour erreur manifeste d'appréciation en raison de l'insuffisance de la sanction préconisée, ayant prononcé la révocation de cet agent.,,,Après l'annulation de l'avis du conseil de discipline régional de recours préconisant la sanction d'exclusion, le maire avait à sa disposition un éventail de sanctions plus sévères parmi lesquelles la révocation n'était qu'un des choix possibles. Dès lors aucune disposition ni aucun principe, notamment pas le principe d'impartialité, ne s'opposait à ce que le magistrat qui avait présidé la formation de jugement ayant prononcé pour erreur manifeste d'appréciation l'annulation de l'avis siégeât en qualité de juge des référés pour statuer sur la sanction de révocation prise à la suite de ce jugement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
