<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029100338</ID>
<ANCIEN_ID>JG_L_2014_06_000000359999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/03/CETATEXT000029100338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 17/06/2014, 359999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Jean-Marie Deligne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:359999.20140617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lyon la décharge des impositions supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2003 et 2004. Par un jugement n° 0901732 du 15 mars 2011, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11LY01207 du 5 avril 2012, la cour administrative d'appel de Lyon l'a, d'une part, déchargé des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année 2004 à concurrence d'une réduction de ses bases imposables de 17 350 euros, et, d'autre part, a remis à sa charge les pénalités pour manquement délibéré dont il avait obtenu la décharge devant le tribunal administratif, à l'exception des pénalités correspondant au redressement résultant de la réintégration dans ses bases imposables de la somme de 17 530 euros.<br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi, enregistré le 5 juin 2012 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 11LY01207 du 5 avril 2012 de la cour administrative d'appel de Lyon ; <br/>
<br/>
              2°) réglant l'affaire au fond, à titre principal, de confirmer l'imposition de la somme de 17 350 euros au titre de l'année 2004 en tant que revenus d'origine indéterminée ou, à titre subsidiaire, d'imposer la somme en cause en tant que revenus relevant de l'article 120 du code général des impôts, et en tout état de cause, de remettre à la charge de M. A...la majoration pour manquement délibéré correspondant à l'imposition de la somme de 17 350 euros.<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marie Deligne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de M. A...;<br/>
<br/>
<br/>
<br/>	CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'un examen contradictoire de sa situation personnelle, M.A..., dirigeant et unique associé de la société de droit espagnol Socapes, a été taxé d'office à l'impôt sur le revenu, dans la catégorie des revenus d'origine indéterminée, à hauteur de 17 469 euros en 2003 et de 131 244 euros en 2004, après avoir été invité, en application des dispositions de l'article L. 16 du livre des procédures fiscales, à justifier de l'origine et de la nature de sommes créditées sur ses comptes bancaires et dont il n'avait pas fait mention dans ses déclarations de revenus de ces deux années. Le ministre de l'économie et des finances se pourvoit en cassation contre l'arrêt du 5 avril 2012 de la cour administrative d'appel de Lyon en tant que, d'une part, statuant sur la requête de M. A..., elle a prononcé la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année 2004, à raison de revenus s'élevant à 17 350 euros, et que, d'autre part, statuant sur sa requête, elle n'a pas remis à sa charge les pénalités pour manquement délibéré correspondant à ces cotisations supplémentaires.<br/>
<br/>
              2. En vertu de l'article 108 du code général des impôts, les articles 109 à 117 du même code fixent les règles suivant lesquelles sont déterminés les revenus distribués par les personnes morales qui sont passibles de l'impôt sur les sociétés, par celles qui se sont volontairement placées sous le même régime fiscal en exerçant l'option prévue au 3 de l'article 206, ainsi que les revenus distribués aux commanditaires dans les sociétés en commandite simple et aux associés autres que ceux indéfiniment responsables dans les sociétés en participation. Aux termes de l'article 209 de ce code : " I. (...) les bénéfices passibles de l'impôt sur les sociétés sont déterminés (...) en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France ainsi que de ceux dont l'imposition est attribuée à la France par une convention internationale relative aux doubles impositions (...) ". Par suite, en jugeant qu'il résultait des dispositions de l'article 109 que les virements d'un montant total de 17 350 euros sur différents comptes bancaires de M.A..., dont l'administration ne contestait pas qu'ils provenaient de la société Socapes, devaient être imposés au nom du contribuable dans la catégorie des revenus de capitaux mobiliers, sans rechercher si la société distributrice de ces revenus, dont il est constant qu'elle est de droit espagnol, était passible de l'impôt sur les sociétés en France, la cour administrative d'appel de Lyon a commis une erreur de droit. Son arrêt doit, dès lors, être annulé en tant qu'il statue sur l'imposition de la somme de 17 350 euros. <br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêt du 5 avril 2012 de la cour administrative d'appel de Lyon, ainsi que son article 2, en tant qu'il ne remet pas à la charge de M. A... les pénalités pour manquement délibéré correspondant au redressement résultant de la réintégration dans ses bases imposables de la somme de 17 350 euros, sont annulés. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : Les conclusions de M. A...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
