<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031603744</ID>
<ANCIEN_ID>JG_L_2015_04_000000361828</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/60/37/CETATEXT000031603744.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 15/04/2015, 361828, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361828</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:361828.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 août et 17 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A...B..., demeurant... ;  M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX01837 du 10 avril 2012 par lequel la cour administrative d'appel de Bordeaux, statuant sur renvoi du Conseil d'Etat, a rejeté leur requête tendant à l'annulation du jugement n° 0200562 du tribunal administratif de Bordeaux du 17 février 2005 rejetant leur demande tendant à la décharge des cotisations d'impôt sur le revenu et de contribution sociale auxquelles ils ont été assujettis au titre des années 1995, 1996 et 1997 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention fiscale entre la France et le Royaume-Uni de Grande-Bretagne et d'Irlande du Nord tendant à éviter les doubles impositions et à prévenir l'évasion fiscale en matière d'impôts sur les revenus, signée le 22 mai 1968 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une procédure de visite et de saisie, diligentée en application de l'article L. 16 B du livre des procédures fiscales, dans la résidence dont ils étaient propriétaires à Margaux (Gironde), M. et Mme B...ont fait l'objet d'un examen contradictoire de leur situation fiscale personnelle portant sur les années 1995 à 1997 ; que, faute d'avoir déposé, malgré l'envoi de mises en demeure, des déclarations de revenus au titre de ces trois années, ils ont été taxés d'office à l'impôt sur le revenu, sur le fondement du 1° de l'article L. 66 et de l'article L. 67 du livre des procédures fiscales, à raison de sommes réputées distribuées à leur profit par la société La Lomas, domiciliée à Guernesey, dont M. Sarjeant était le gérant; que, par un jugement du 17 février 2005, le tribunal administratif de Bordeaux a rejeté leur demande tendant à la décharge des cotisations d'impôt sur le revenu et de contribution sociale auxquelles ils ont été assujettis ainsi que des pénalités correspondantes ; qu'ils se pourvoient en cassation contre l'arrêt du 10 avril 2012 par lequel la cour administrative d'appel de Bordeaux, statuant sur renvoi du Conseil d'Etat, a rejeté leur requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 4 A du code général des impôts : " Les personnes qui ont en France leur domicile fiscal sont passibles de l'impôt sur le revenu en raison de l'ensemble de leurs revenus. (...) " ; qu'aux termes de l'article 4 B du même code : " 1. Sont considérées comme ayant leur domicile fiscal en France au sens de l'article 4 A :/ a. Les personnes qui ont en France leur foyer ou le lieu de leur séjour principal ;/ b. celles qui exercent en France une activité professionnelle, salariée ou non, à moins qu'elles ne justifient que cette activité y est exercée à titre accessoire ;/ c. celles qui ont en France le centre de leurs intérêts économiques... " ; que, pour l'application des dispositions du a du 1 de l'article 4 B précité, le foyer s'entend du lieu où le contribuable habite normalement et a le centre de ses intérêts familiaux, sans qu'il soit tenu compte des séjours effectués temporairement ailleurs en raison des nécessités de la profession ou de circonstances exceptionnelles ; que le lieu du séjour principal du contribuable ne peut déterminer son domicile fiscal que dans l'hypothèse où celui-ci ne dispose pas de foyer ;<br/>
<br/>
              3. Considérant que pour confirmer le principe de l'imposition en France de M. et Mme B..., la cour a jugé qu'ils devaient être regardés comme ayant eu en France le lieu de leur séjour principal, au sens et pour l'application des dispositions précitées des articles 4 A et 4 B du code général des impôts, durant les années 1995, 1996 et 1997 ; qu'en se fondant, ainsi, sur le critère, subsidiaire, mentionné au a du 1 de l'article 4B du code général des impôts, tiré du lieu de séjour principal, sans avoir, au préalable, recherché si le lieu du foyer des contribuables pouvait être déterminé, la cour a entaché son arrêt d'une erreur de droit ; que M. et Mme B...sont, par suite, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il incombe, par suite, au Conseil d'Etat de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction, notamment de la notification de redressement du 4 décembre 1998, que pour établir que M. et Mme B...avaient leur domicile fiscal en France et y étaient, par suite, soumis à des obligations déclaratives en application de l'article 170 bis du code général des impôts, l'administration fiscale s'est fondée sur des éléments qu'elle a recueillis à l'occasion, d'une part, de la procédure de visite et de saisie dont les contribuables ont fait l'objet en application de l'article L. 16 B du livre des procédures fiscales, d'autre part, de l'exercice de son droit de communication et, enfin, de la mise en oeuvre de la procédure d'assistance administrative internationale prévue par la convention fiscale franco-britannique ; que la régularité des impositions en litige est, ainsi, subordonnée à l'examen de la régularité de ces procédures ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 16 B du livre des procédures fiscales, dans sa rédaction applicable à l'opération de visite et de saisie litigieuse : " I. Lorsque l'autorité judiciaire, saisie par l'administration fiscale, estime qu'il existe des présomptions qu'un contribuable se soustrait à l'établissement ou au paiement des impôts sur le revenu ou sur les bénéfices ou de la taxe sur la valeur ajoutée en se livrant à des achats ou à des ventes sans facture, en utilisant ou en délivrant des factures ou des documents ne se rapportant pas à des opérations réelles ou en omettant sciemment de passer ou de faire passer des écritures ou en passant ou en faisant passer sciemment des écritures inexactes ou fictives dans des documents comptables dont la tenue est imposée par le code général des impôts, elle peut (...) autoriser les agents de l'administration des impôts (...) à rechercher la preuve de ces agissements, en effectuant des visites en tous lieux, même privés, où les pièces et documents s'y rapportant sont susceptibles d'être détenus et procéder à leur saisie. / II. Chaque visite doit être autorisée par une ordonnance du président du tribunal de grande instance dans le ressort duquel sont situés les lieux à visiter ou d'un juge délégué par lui. / (...) L'ordonnance mentionnée au premier alinéa n'est susceptible que d'un pourvoi en cassation selon les règles prévues par le code de procédure pénale (...). / (...) IV. Un procès-verbal relatant les modalités et le déroulement de l'opération et consignant les constatations effectuées est dressé sur-le-champ par les agents de l'administration des impôts. Un inventaire des pièces et documents saisis lui est annexé s'il y a lieu. (...) " ;  <br/>
<br/>
              7. Considérant qu'à la suite de l'arrêt Ravon et autres c/France (n° 18497/03) du 21 février 2008 par lequel la Cour européenne des droits de l'homme a jugé que les voies de recours ouvertes aux contribuables pour contester la régularité des visites et saisies opérées sur le fondement de l'article L. 16 B du livre des procédures fiscales ne garantissaient pas l'accès à un procès équitable au sens de l'article 6 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, l'article 164 de la loi du 4 août 2008 de modernisation de l'économie a prévu, pour les opérations mentionnées à l'article L. 16 B pour lesquelles le procès-verbal ou l'inventaire avait été remis ou réceptionné antérieurement à l'entrée en vigueur de cette loi, une procédure d'appel devant le premier président de la cour d'appel contre l'ordonnance autorisant la visite et un recours devant ce même juge contre le déroulement des opérations de visite et de saisie, les ordonnances rendues par ce dernier étant susceptibles d'un pourvoi en cassation ; que le d) du 1 du IV du même article 164 dispose, d'une part, que cet appel et ce recours sont ouverts notamment " lorsque, à partir d'éléments obtenus par l'administration dans le cadre d'une procédure de visite et de saisie, des impositions ont été établies (...) et qu'elles font (...) l'objet, à la date de l'entrée en vigueur de la présente loi, (...) d'un recours contentieux devant le juge, sous réserve des affaires dans lesquelles des décisions sont passées en force de chose jugée " et, d'autre part, que " le juge, informé par l'auteur de l'appel ou du recours ou par l'administration, sursoit alors à statuer jusqu'au prononcé de l'ordonnance du premier président de la cour d'appel " ; que le 3 du IV de ce même article fait obligation à l'administration d'informer les personnes visées par l'ordonnance ou par les opérations de visite et de saisie de l'existence de ces voies de recours et du délai de deux mois ouverts à compter de la réception de cette information pour, le cas échéant, faire appel contre l'ordonnance ou former un recours contre le déroulement des opérations de visite ou de saisie, cet appel et ce recours étant exclusifs de toute appréciation par le juge du fond de la régularité du déroulement de ces opérations ; qu'il précise qu'en l'absence d'information de la part de l'administration, ces personnes peuvent exercer cet appel ou ce recours sans condition de délai ;<br/>
<br/>
              8. Considérant que ces dispositions, qui ont ouvert aux contribuables la possibilité de bénéficier rétroactivement de ces nouvelles voies de recours contre l'ordonnance autorisant les opérations de visite et de saisie, ainsi que contre le déroulement de telles opérations antérieures à l'entrée en vigueur de l'article 164 de cette loi,  leur ont permis de bénéficier d'un contrôle juridictionnel effectif de cette ordonnance et de ces opérations ; que les requérants, ne sont, par suite, pas fondés à soutenir qu'elles constitueraient une immixtion du législateur dans un litige en cours et qu'elles méconnaitraient les stipulations des articles 6 § 1 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              9. Considérant, en revanche, qu'il résulte de l'instruction, que les contribuables n'ont, en l'espèce, pas été mis à même d'exercer, dans des conditions conformes aux dispositions du IV de l'article 164 de la loi du 4 août 2008, les voies de recours ouvertes par ces dispositions ; qu'en effet, en réponse au courrier du 14 décembre 2011 par lequel le président de la troisième chambre de la cour administrative d'appel de Bordeaux lui a demandé de verser au dossier le document par lequel elle avait informé les époux B..." de l'existence de voies de recours et du délai de deux mois ouvert à compter de la réception de cette information, document prévu par les dispositions du 3 d) du IV de l'article 164 de la loi n° 2008-776 du 4 août 2008 ", l'administration s'est bornée à adresser à la cour la copie de l'ordonnance du premier vice-président du tribunal de grande instance de Bordeaux ayant autorisé la procédure de visite et de saisie à l'encontre de M. B...en indiquant, à tort, que le délai pour se pourvoir en cassation, qui était mentionné sur cette ordonnance, n'était "pas susceptible d'être concerné par la loi n° 2008-776 s'agissant d'impositions relatives aux années 1995 à 1997, très antérieures à la promulgation de la loi " ; qu'à supposer que les contribuables puissent, néanmoins, être regardés comme ayant été informés de l'existence des nouvelles voies de recours par la communication du mémoire de l'administration, enregistré au greffe de la cour administrative d'appel de Bordeaux le 27 février 2012, ils n'ont, en tout état de cause, pas disposé, compte tenu de la date à laquelle la cour a statué, du délai de deux mois prévu par ces dispositions ; qu'il y a lieu, dans ces conditions, et dès lors que l'annulation, par la présente décision, de l'arrêt attaqué de la cour administrative d'appel rend à nouveau possible l'exercice des voies de recours prévues à l'article 164 de la loi du 4 août 2008, de surseoir à statuer sur leur requête et le surplus des conclusions de leur pourvoi aux fins de leur permettre, s'ils s'y croient fondés, de saisir le premier président de la cour d'appel en application de ces dispositions transitoires nonobstant la circonstance, à la supposer établie, que l'exercice de ces voies de recours aboutirait, compte tenu de l'ancienneté des opérations de visite et de saisies litigieuses, qui se sont déroulées en 1998, à méconnaître les exigences prévues à l'article 6 §1 de la convention, notamment en matière de délai raisonnable, dès lors que cette circonstance est sans incidence sur la régularité et le bien-fondé des impositions en litige ; qu'un délai de deux mois à compter de la notification de la présente décision est octroyé aux requérants à cette fin ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 avril 2012 est annulé.<br/>
Article 2 : Il est sursis à statuer sur la requête de M. et Mme B...et sur le surplus des conclusions de leur pourvoi.<br/>
Article 3 : M. et Mme B...devront, le cas échéant, justifier devant le Conseil d'Etat de la saisine, en application de l'article 2, de la juridiction compétente.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
