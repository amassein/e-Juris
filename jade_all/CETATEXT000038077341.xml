<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077341</ID>
<ANCIEN_ID>JG_L_2019_01_000000417342</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077341.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 30/01/2019, 417342</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417342</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417342.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 15 janvier et 13 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 14 novembre 2017 du Conseil national de l'ordre des médecins, statuant en formation restreinte, le suspendant pour une durée d'un an du droit d'exercer la médecine générale et subordonnant la reprise de son activité à la justification du suivi d'une formation de remise à niveau dans le cadre du diplôme interuniversitaire de médecine générale ;<br/>
<br/>
              2°) de mettre à la charge du conseil départemental du Pas-de-Calais de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4124-3-5 du code de la santé publique : "  I. - En cas d'insuffisance professionnelle rendant dangereux l'exercice de la profession, la suspension temporaire, totale ou partielle, du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. / Le conseil régional ou interrégional est saisi à cet effet soit par le directeur général de l'agence régionale de santé, soit par une délibération du conseil départemental ou du conseil national. Ces saisines ne sont pas susceptibles de recours. / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional dans les conditions suivantes : / 1° Pour les médecins, le rapport est établi par trois médecins qualifiés dans la même spécialité que celle du praticien concerné désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts (...) / IV. - Les experts procèdent ensemble, sauf impossibilité manifeste, à l'examen des connaissances théoriques et pratiques du praticien (...) Le rapport d'expertise (...) indique les insuffisances relevées au cours de l'expertise, leur dangerosité et préconise les moyens de les pallier par une formation théorique et, si nécessaire, pratique (...) / V. - Avant de se prononcer, le conseil régional ou interrégional peut, par une décision non susceptible de recours, décider de faire procéder à une expertise complémentaire dans les conditions prévues aux II, III et IV du présent article. / VI. - Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre. / VII. - La décision de suspension temporaire du droit d'exercer pour insuffisance professionnelle définit les obligations de formation du praticien. / La notification de la décision mentionne que la reprise de l'exercice professionnel par le praticien ne pourra avoir lieu sans qu'il ait au préalable justifié (...) avoir rempli les obligations de formation fixées par la décision (...) ".<br/>
<br/>
              2. Sur saisine du directeur général de l'agence régionale de santé des Hauts-de-France, la formation restreinte du Conseil national de l'ordre des médecins, auquel l'affaire avait été renvoyée en application des dispositions du VI de l'article R. 4124-3-5 du code de la santé publique cité ci-dessus, a, par la décision attaquée du 14 novembre 2017, suspendu M. B..., médecin généraliste, du droit d'exercer la médecine générale, en raison d'insuffisances professionnelles rendant dangereux l'exercice de sa profession et lui a fixé l'obligation de suivre une formation pendant la durée de la suspension.<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              3. Lorsqu'il examine, sur le fondement des dispositions citées au point 1, l'aptitude d'un praticien à exercer ses fonctions, le Conseil national de l'ordre des médecins prend une décision administrative. Si cette décision doit être motivée et doit, par suite, indiquer les éléments de fait et de droit sur lesquels elle se fonde, elle n'est, en revanche, pas tenue de répondre à l'ensemble des arguments  invoqués par le praticien.<br/>
<br/>
              4. Ainsi, la formation restreinte du Conseil national de l'ordre des médecins, qui s'est expressément appropriée certains constats et préconisations de l'expertise conduite en application des dispositions du IV de l'article R. 4124-3-5 du code de la santé publique citées au point 1, sur les connaissances théoriques et pratiques de M.B..., a, contrairement à ce que soutient ce dernier, suffisamment exposé les motifs pour lesquels elle retenait que son insuffisance professionnelle était de nature à faire courir un danger à ses patients. <br/>
<br/>
              5. Par ailleurs, M. B...ne saurait utilement soutenir que la décision attaquée est insuffisamment motivée au motif qu'elle ne répond pas à ses critiques dirigées contre l'expertise et qu'elle ne prend pas parti sur la possibilité qu'il sollicitait d'exercer la médecine du travail.<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              6. En prononçant la mesure attaquée à une date à laquelle il est constant que M. B... n'était plus inscrit au tableau de l'ordre, la formation restreinte du Conseil national de l'ordre des médecins n'a, contrairement à ce que soutient le requérant, pas méconnu le champ d'application des dispositions, citées au point 1, dont elle a fait application. <br/>
<br/>
              7. Par ailleurs, en estimant qu'il ressortait des pièces du dossier et des échanges devant elle que M. B...n'apportait pas la preuve d'un maintien ou d'un perfectionnement suffisant de ses connaissances et présentait de graves lacunes tant théoriques que pratiques, la formation restreinte du Conseil national de l'ordre des médecins n'a, en tout état de cause, méconnu aucune règle qui gouvernerait devant elle la charge de la preuve.<br/>
<br/>
              8. Enfin, il ressort des pièces du dossier qu'en estimant que M. B... présentait des insuffisances professionnelles dont la nature et l'ampleur rendaient dangereuse toute activité de médecin généraliste et qu'une mesure de suspension temporaire et totale du droit d'exercer cette discipline devait, par suite, être prise à son égard, la formation restreinte du Conseil national de l'ordre des médecins a fait une exacte application des dispositions de l'article R. 4124-3-5 du code de la santé publique et n'a entaché sa décision d'aucune erreur de droit.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision qu'il attaque. Par suite, les conclusions qu'il présente au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES MÉDECINS. - FACULTÉ POUR LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS DE PRONONCER UNE DÉCISION DE SUSPENSION TEMPORAIRE D'EXERCER LA MÉDECINE (ART. R. 4124-3-5 DU CSP) À UNE DATE À LAQUELLE L'INTÉRESSÉ N'EST PLUS INSCRIT AU TABLEAU DE L'ORDRE  - EXISTENCE.
</SCT>
<ANA ID="9A"> 55-01-02-01 En prononçant une décision interdiction temporaire d'exercer la médecine à une date à laquelle l'intéressé n'était plus inscrit au tableau de l'ordre, la formation restreinte du Conseil national de l'ordre des médecins n'a pas méconnu le champ d'application de l'article R. 4124-3-5 du code de la santé publique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
