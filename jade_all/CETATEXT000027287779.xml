<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027287779</ID>
<ANCIEN_ID>J0_L_2013_02_000001102626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/77/CETATEXT000027287779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Versailles, 1ère Chambre, 19/02/2013, 11VE02626, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-19</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Versailles</JURIDICTION>
<NUMERO>11VE02626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère Chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. SOUMET</PRESIDENT>
<AVOCATS>ROUZAUD</AVOCATS>
<RAPPORTEUR>M. Simon  FORMERY</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme DIOUX-MOEBS</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 juillet 2011 au greffe de la Cour administrative d'appel de Versailles, présentée pour la SARL DRANCY BAG, dont le siège au 60 rue Saint-Stenay à Drancy (93700), par Me Rouzaud, avocat à la Cour ; la SARL DRANCY BAG demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement n° 0802145 en date du 29 avril 2011 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la décharge des cotisations supplémentaires à l'impôt sur les sociétés au titre des exercices clos en 2004 et 2005 et, en matière de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2004 au 31 décembre 2006 ; <br/>
<br/>
       2°) de prononcer la décharge des impositions supplémentaires litigieuses ; <br/>
      Elle soutient qu'elle fournit les justificatifs établissant que les sommes débitées du compte de Mme Huang, épouse du gérant de la SARL DRANCY BAG, ont été apportées à la société ; <br/>
<br/>
       .......................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
       Vu le code de justice administrative ; <br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
        Après avoir entendu, au cours de l'audience publique du 5 février 2013 :<br/>
        - le rapport de M. Formery, président assesseur,<br/>
        - les conclusions de Mme Dioux-Moebs, rapporteur public ;<br/>
<br/>
      1. Considérant que la SARL DRANCY BAG relève appel du jugement du 29 avril 2011 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la décharge des impositions supplémentaires à l'impôt sur les sociétés et à la taxe sur la valeur ajoutée auxquelles elle a été assujettie au titre des exercices clos en 2004 et 2005 et de la période du 1er janvier 2004 au 31 décembre 2006 ;<br/>
<br/>
      Sur l'étendue du litige :<br/>
<br/>
      2. Considérant qu'il ressort de l'instruction qu'après avoir pris connaissance des pièces produites par la contribuable, l'administration fiscale déclare prononcer, sur les rappels en litige au titre de l'exercice 2005, un dégrèvement de 428 euros en droits et de 31 euros en pénalités ; que, dès lors, les conclusions de la requête sont, à cette hauteur, devenues sans d'objet ; qu'il n'y a, par suite, plus lieu d'y statuer ; <br/>
<br/>
      Sur le surplus des conclusions en décharge :<br/>
<br/>
      3. Considérant qu'aux termes de l'article 38 du code général des impôts, " 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés " ; qu'il appartient toujours au contribuable de justifier l'inscription d'une dette au passif du bilan de son entreprise, alors même que cette dette a été portée en comptabilité au cours d'un exercice prescrit ;<br/>
<br/>
      4. Considérant qu'au terme de la vérification de comptabilité qu'elle a effectuée, l'administration fiscale a pu constater que la SARL DRANCY BAG avait comptabilisé, au 1er janvier 2004, un montant de crédit en compte courant de 84 135 euros trouvant son origine au cours d'exercices antérieurs, et dont un montant de 30 828 euros n'était pas justifié ; qu'en se bornant à produire des copies de bordereaux de remise bancaire difficilement lisibles ainsi que des copies de relevés d'un compte bancaire ouvert au nom de Mme Shu Ping Huang mentionnant des écritures de débit dont le bénéficiaire n'est pas identifié, la SARL DRANCY BAG n'établit la réalité ni des dettes ni des apports dont elle se prévaut ; que la circonstance que la banque de la société ne serait plus en mesure de lui procurer les copies de relevés bancaires qu'elle avait demandé est sans incidence sur l'apport de la preuve dont elle avait la charge ; que, par suite, c'est à bon droit que l'administration a remis en cause les écritures de passif susmentionnées ;<br/>
<br/>
       5. Considérant qu'il résulte de ce qui précède que la SARL DRANCY BAG n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Cergy-Pontoise a rejeté sa demande ; <br/>
DECIDE : <br/>
Article 1er : Il n'y a plus lieu de statuer, à concurrence des sommes de 428 euros en droits et de 31 euros en pénalités, dégrevées par l'administration, sur les conclusions de la requête.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de la SARL DRANCY BAG est rejeté. <br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 11VE02626	<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-04-02-01-04-02 Contributions et taxes. Impôts sur les revenus et bénéfices. Revenus et bénéfices imposables - règles particulières. Bénéfices industriels et commerciaux. Détermination du bénéfice net. Dettes.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
