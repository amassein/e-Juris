<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374857</ID>
<ANCIEN_ID>JG_L_2016_04_000000396364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 06/04/2016, 396364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:396364.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par un mémoire et un nouveau mémoire, enregistrés les 25 janvier et 29 février 2016 au secrétariat du contentieux du Conseil d'État, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société Famille A...B...et la société Vincent A...investissements demandent au Conseil d'État, à l'appui de leur requête tendant à l'annulation pour excès de pouvoir de la décision implicite du Premier ministre rejetant leur demande d'abrogation des articles R. 611-13 et R. 611-16 du code de commerce, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article L. 611-2 du code de commerce, dans leur rédaction antérieure à la loi du 14 octobre 2015 d'actualisation du droit des outre-mer.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de commerce, notamment son article L. 611-2 ;<br/>
              - la loi n° 2015-1268 du 14 octobre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'en vertu des articles L. 232-21 à L. 232-23 du code de commerce, les sociétés commerciales sont tenues de déposer au greffe du tribunal, selon les cas dans un délai de un ou deux mois, leurs comptes annuels pour qu'ils soient annexés au registre du commerce et des sociétés ; qu'aux termes de l'article L. 611-2 du même code, dans sa rédaction antérieure à la loi du 14 octobre 2015 d'actualisation du droit des outre-mer : " I. Lorsqu'il résulte de tout acte, document ou procédure qu'une société commerciale, un groupement d'intérêt économique, ou une entreprise individuelle, commerciale ou artisanale connaît des difficultés de nature à compromettre la continuité de l'exploitation, ses dirigeants peuvent être convoqués par le président du tribunal de commerce pour que soient envisagées les mesures propres à redresser la situation. / A l'issue de cet entretien ou si les dirigeants ne se sont pas rendus à sa convocation, le président du tribunal peut, nonobstant toute disposition législative ou réglementaire contraire, obtenir communication, par les commissaires aux comptes, les membres et représentants du personnel, les administrations publiques, les organismes de sécurité et de prévoyance sociales ainsi que les services chargés de la centralisation des risques bancaires et des incidents de paiement, des renseignements de nature à lui donner une exacte information sur la situation économique et financière du débiteur.               / II.-Lorsque les dirigeants d'une société commerciale ne procèdent pas au dépôt des comptes annuels dans les délais prévus par les textes applicables, le président du tribunal peut leur adresser une injonction de le faire à bref délai sous astreinte. / Si cette injonction n'est pas suivie d'effet dans un délai fixé par décret en Conseil d'Etat, le président du tribunal peut également faire application à leur égard des dispositions du deuxième alinéa du I. / Le II est applicable, dans les mêmes conditions, à tout entrepreneur individuel à responsabilité limitée qui ne procède pas au dépôt des comptes annuels ou documents mentionnés au premier alinéa de l'article L. 526-14, lorsque l'activité professionnelle à laquelle le patrimoine est affecté est commerciale ou artisanale. " ;<br/>
<br/>
              3. Considérant que les dispositions du II de l'article L. 611-2 du code de commerce citées ci-dessus sont applicables au jugement du recours pour excès de pouvoir formé par la société Famille A...B...et la société Vincent A...investissements contre la décision implicite du Premier ministre rejetant leur demande d'abrogation des articles R. 611-13 et R. 611-16 du code de commerce ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce que, en l'absence de dispositions législatives prévoyant des garanties appropriées entourant la saisine d'office du président du tribunal de commerce, ces dispositions méconnaîtraient le principe d'impartialité des juridictions garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du II de l'article L. 611-2 du code de commerce dans sa rédaction antérieure à la loi du 14 octobre 2015 d'actualisation du droit des outre-mer est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête de la société Famille A...B...et de la société Vincent A...investissements jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Famille A...B..., à la société Vincent A...investissements et au garde des sceaux, ministre de la justice.<br/>
      Copie en sera adressée au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
