<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028049381</ID>
<ANCIEN_ID>JG_L_2013_10_000000369121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/93/CETATEXT000028049381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 07/10/2013, 369121, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Anne-Françoise Roul</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:369121.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 11MA01193 et 11MA01204 du 21 mai 2013, enregistré au secrétariat du contentieux du Conseil d'Etat le 6 juin 2013, par lequel la cour administrative d'appel de Marseille, avant de statuer sur les requêtes de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et du centre hospitalier universitaire de Montpellier dirigées contre le jugement n° 0902959 du 25 janvier 2011 du tribunal administratif de Montpellier faisant partiellement droit à la demande indemnitaire présentée contre cet établissement hospitalier par l'office, subrogé dans les droits de Mme A...a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de ces requêtes au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les dispositions de l'article L. 376-1 du code de la sécurité sociale imposent-elles à la juridiction, saisie par l'ONIAM du recours subrogatoire prévu par les dispositions de l'article L. 1142-15 du code de la santé publique, la mise en cause de l'organisme social et, de manière plus générale, des tiers payeurs ' <br/>
<br/>
              2°) Les dispositions de l'article L. 376-1 du code de la sécurité sociale imposent-elles à la juridiction, saisie par l'ONIAM du recours subrogatoire prévu par les dispositions de l'article L. 1142-15 du code de la santé publique, la mise en cause de la victime ' <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Anne-Françoise Roul, Conseiller d'Etat, <br/>
- les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Me Le Prado, avocat du centre hospitalier universitaire de Montpellier ;<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
<br/>
              1. L'article L. 376-1 du code de la sécurité sociale ouvre aux caisses de sécurité sociale qui ont servi des prestations à la victime d'un dommage corporel un recours subrogatoire contre le responsable de ce dommage. Dans sa rédaction issue de la loi n° 2006-1640 du 21 décembre 2006, le huitième alinéa  de cet article prévoit que : " L'intéressé ou ses ayants droit doivent indiquer, en tout état de la procédure, la qualité d'assuré social de la victime de l'accident ainsi que les caisses de sécurité sociale auxquelles celle-ci est ou était affiliée pour les divers risques. Ils doivent appeler ces caisses en déclaration de jugement commun ou réciproquement (...) ". En application de ces dispositions, il incombe au juge administratif, saisi d'un recours indemnitaire de la victime contre une personne publique regardée comme responsable de l'accident, de mettre en cause les caisses auxquelles la victime est ou était affiliée. Symétriquement, lorsque le juge est saisi d'un recours indemnitaire introduit contre la personne publique par une caisse agissant dans le cadre de la subrogation légale, il lui incombe de mettre en cause la victime. Le défaut de mise en cause, selon le cas, de la caisse ou de la victime entache la procédure d'irrégularité. <br/>
<br/>
              2. Par ailleurs, dans le cas où un dommage corporel est imputable à une activité de prévention, de diagnostic ou de soins, la victime ou, si elle est décédée, ses ayants droit, peuvent solliciter une indemnisation dans le cadre de la procédure amiable prévue par les articles L. 1142-4 et suivants du code de la santé publique. En vertu des dispositions de l'article L. 1142-14 de ce code, lorsque la commission régionale de conciliation et d'indemnisation a émis l'avis que le dommage engage la responsabilité d'un établissement de santé, il appartient à l'assureur de cet établissement de faire une offre d'indemnisation. Aux termes de l'article L. 1142-15 : " En cas de silence ou de refus explicite de la part de l'assureur de faire une offre, ou lorsque le responsable des dommages n'est pas assuré ou la couverture d'assurance prévue à l'article L. 1142-2 est épuisée ou expirée, l'office institué à l'article L. 1142-22 est substitué à l'assureur. / (...) / L'acceptation de l'offre de l'office vaut transaction au sens de l'article 2044 du code civil. La transaction est portée à la connaissance du responsable et, le cas échéant, de son assureur (...) / L'office est subrogé, à concurrence des sommes versées, dans les droits de la victime contre la personne responsable du dommage ou, le cas échéant, son assureur (...) ".<br/>
<br/>
              3. L'action engagée contre un établissement de santé par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, subrogé, en vertu des dispositions précitées de l'article L. 1142-15 du code de la santé publique, dans les droits de la victime qu'il a indemnisée, doit être regardée, au sens et pour l'application des dispositions de l'article L. 376-1 du code de la sécurité sociale, comme une action de la victime. Par suite, il incombe au juge administratif, saisi d'une telle action contre un établissement public, de mettre en cause les caisses de sécurité sociale auxquelles la victime est ou était affiliée.<br/>
<br/>
              4. En revanche, la circonstance qu'une caisse ainsi mise en cause présente des conclusions contre l'établissement n'a pas pour conséquence d'obliger le juge à mettre en cause la victime. En effet, dès lors que le troisième alinéa de l'article L. 1142-15 du code de la santé publique prévoit que l'acceptation par la victime de l'offre de l'ONIAM vaut transaction, la victime ne dispose plus d'une action contre l'établissement, de sorte que sa mise en cause serait dépourvue d'objet. Il n'en irait autrement que dans le cas où l'offre de l'office excluerait explicitement de son champ certains des préjudices imputables à l'établissement de santé. La victime conservant, dans cette hypothèse, une action contre l'établissement au titre des préjudices que l'indemnité versée par l'office n'avait pas pour objet de réparer, il y aurait lieu pour le juge de la mettre en cause.<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Marseille, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au centre hospitalier universitaire de Montpellier, à la caisse primaire d'assurance maladie de l'Hérault et à Mme B...A.... <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - RESPONSABILITÉ MÉDICALE - DOMMAGE CORPOREL IMPUTABLE À L'ACTIVITÉ DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS D'UN ÉTABLISSEMENT DE SANTÉ - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CODE DE LA SANTÉ PUBLIQUE) - ACTION ENGAGÉE PAR L'ONIAM  SUBROGÉ DANS LES DROITS DE LA VICTIME CONTRE L'ÉTABLISSEMENT DE SANTÉ - 1) NOTION D'ACTION DE LA VICTIME - INCLUSION - CONSÉQUENCE - OBLIGATION POUR LE JUGE SAISI D'UNE TELLE ACTION DE METTRE LES CAISSES DE SÉCURITÉ SOCIALE EN CAUSE - EXISTENCE - 2) CONCLUSIONS PRÉSENTÉES PAR UNE CAISSE AINSI MISE EN CAUSE CONTRE L'ÉTABLISSEMENT DE SANTÉ - OBLIGATION POUR LE JUGE DE METTRE EN CAUSE LA VICTIME - A) PRINCIPE - ABSENCE, L'ACCEPTATION PAR LA VICTIME DE L'OFFRE D'INDEMNISATION DE L'ONIAM VALANT TRANSACTION - B) EXCEPTION - EXISTENCE, DANS LE CAS PARTICULIER OÙ L'OFFRE DE L'ONIAM EXCLURAIT EXPLICITEMENT DE SON CHAMP CERTAINS PRÉJUDICES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. EXISTENCE D'UNE FAUTE MÉDICALE DE NATURE À ENGAGER LA RESPONSABILITÉ DU SERVICE PUBLIC. - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CODE DE LA SANTÉ PUBLIQUE) - ACTION ENGAGÉE PAR L'ONIAM  SUBROGÉ DANS LES DROITS DE LA VICTIME CONTRE L'ÉTABLISSEMENT DE SANTÉ - 1) NOTION D'ACTION DE LA VICTIME - INCLUSION - CONSÉQUENCE - OBLIGATION POUR LE JUGE SAISI D'UNE TELLE ACTION DE METTRE LES CAISSES DE SÉCURITÉ SOCIALE EN CAUSE - EXISTENCE - 2) CONCLUSIONS PRÉSENTÉES PAR UNE CAISSE AINSI MISE EN CAUSE CONTRE L'ÉTABLISSEMENT DE SANTÉ - OBLIGATION POUR LE JUGE DE METTRE EN CAUSE LA VICTIME - A) PRINCIPE - ABSENCE, L'ACCEPTATION PAR LA VICTIME DE L'OFFRE D'INDEMNISATION DE L'ONIAM VALANT TRANSACTION - B) EXCEPTION - EXISTENCE, DANS LE CAS PARTICULIER OÙ L'OFFRE DE L'ONIAM EXCLURAIT EXPLICITEMENT DE SON CHAMP CERTAINS PRÉJUDICES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-03-02-02-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. ÉTAT OU AUTRES COLLECTIVITÉS PUBLIQUES. ÉTAT OU ÉTABLISSEMENT PUBLIC. - DOMMAGE CORPOREL IMPUTABLE À L'ACTIVITÉ DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS D'UN ÉTABLISSEMENT DE SANTÉ - INDEMNISATION DE LA VICTIME PAR L'ONIAM DANS LE CADRE D'UNE PROCÉDURE AMIABLE (ART. L. 1142-4 ET SUIVANTS DU CODE DE LA SANTÉ PUBLIQUE) - ACTION ENGAGÉE PAR L'ONIAM  SUBROGÉ DANS LES DROITS DE LA VICTIME CONTRE L'ÉTABLISSEMENT DE SANTÉ - 1) NOTION D'ACTION DE LA VICTIME - INCLUSION - CONSÉQUENCE - OBLIGATION POUR LE JUGE SAISI D'UNE TELLE ACTION DE METTRE LES CAISSES DE SÉCURITÉ SOCIALE EN CAUSE - EXISTENCE - 2) CONCLUSIONS PRÉSENTÉES PAR UNE CAISSE AINSI MISE EN CAUSE CONTRE L'ÉTABLISSEMENT DE SANTÉ - OBLIGATION POUR LE JUGE DE METTRE EN CAUSE LA VICTIME - A) PRINCIPE - ABSENCE, L'ACCEPTATION PAR LA VICTIME DE L'OFFRE D'INDEMNISATION DE L'ONIAM VALANT TRANSACTION - B) EXCEPTION - EXISTENCE, DANS LE CAS PARTICULIER OÙ L'OFFRE DE L'ONIAM EXCLURAIT EXPLICITEMENT DE SON CHAMP CERTAINS PRÉJUDICES.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-05-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 376-1 (ANCIEN ART. L. 397) DU CODE DE LA SÉCURITÉ SOCIALE. - ACTION ENGAGÉE CONTRE UN ÉTABLISSEMENT DE SANTÉ PAR L'ONIAM SUBROGÉ DANS LES DROITS DE LA VICTIME QU'IL A INDEMNISÉE - 1) NOTION D'ACTION DE LA VICTIME - INCLUSION - CONSÉQUENCE - OBLIGATION POUR LE JUGE SAISI D'UNE TELLE ACTION DE METTRE LES CAISSES DE SÉCURITÉ SOCIALE EN CAUSE - EXISTENCE - 2) CONCLUSIONS PRÉSENTÉES PAR UNE CAISSE AINSI MISE EN CAUSE CONTRE L'ÉTABLISSEMENT DE SANTÉ - OBLIGATION POUR LE JUGE DE METTRE EN CAUSE LA VICTIME - A) PRINCIPE - ABSENCE, L'ACCEPTATION PAR LA VICTIME DE L'OFFRE D'INDEMNISATION DE L'ONIAM VALANT TRANSACTION - B) EXCEPTION - EXISTENCE, DANS LE CAS PARTICULIER OÙ L'OFFRE DE L'ONIAM EXCLURAIT EXPLICITEMENT DE SON CHAMP CERTAINS PRÉJUDICES.
</SCT>
<ANA ID="9A"> 54-07-01-07 1) L'action engagée contre un établissement de santé par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des maladies nosocomiales (ONIAM), subrogé dans les droits de la victime qu'il a indemnisée dans le cadre de la procédure amiable prévue aux articles L. 1142-4 et suivants du code de la santé publique, doit être regardée, au sens et pour l'application des dispositions de l'article L. 376-1 du code de la sécurité sociale, comme une action de la victime. Par suite, il incombe au juge administratif, saisi d'une telle action contre un établissement public, de mettre en cause les caisses de sécurité sociale auxquelles la victime est ou était affiliée.,,,2) a) En revanche, la circonstance qu'une caisse ainsi mise en cause présente des conclusions contre l'établissement n'a pas pour conséquence d'obliger le juge à mettre en cause la victime. En effet, dès lors que le troisième alinéa de l'article L. 1142-15 du code de la santé publique prévoit que l'acceptation par la victime de l'offre de l'ONIAM vaut transaction, la victime ne dispose plus d'une action contre l'établissement, de sorte que sa mise en cause serait dépourvue d'objet. b) Il n'en irait autrement que dans le cas où l'offre de l'office exclurait explicitement de son champ certains des préjudices imputables à l'établissement de santé. La victime conservant, dans cette hypothèse, une action contre l'établissement au titre des préjudices que l'indemnité versée par l'office n'avait pas pour objet de réparer, il y aurait lieu pour le juge de la mettre en cause.</ANA>
<ANA ID="9B"> 60-02-01-01-02-01 1) L'action engagée contre un établissement de santé par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des maladies nosocomiales (ONIAM), subrogé dans les droits de la victime qu'il a indemnisée dans le cadre de la procédure amiable prévue aux articles L. 1142-4 et suivants du code de la santé publique, doit être regardée, au sens et pour l'application des dispositions de l'article L. 376-1 du code de la sécurité sociale, comme une action de la victime. Par suite, il incombe au juge administratif, saisi d'une telle action contre un établissement public, de mettre en cause les caisses de sécurité sociale auxquelles la victime est ou était affiliée.,,,2) a) En revanche, la circonstance qu'une caisse ainsi mise en cause présente des conclusions contre l'établissement n'a pas pour conséquence d'obliger le juge à mettre en cause la victime. En effet, dès lors que le troisième alinéa de l'article L. 1142-15 du code de la santé publique prévoit que l'acceptation par la victime de l'offre de l'ONIAM vaut transaction, la victime ne dispose plus d'une action contre l'établissement, de sorte que sa mise en cause serait dépourvue d'objet. b) Il n'en irait autrement que dans le cas où l'offre de l'office exclurait explicitement de son champ certains des préjudices imputables à l'établissement de santé. La victime conservant, dans cette hypothèse, une action contre l'établissement au titre des préjudices que l'indemnité versée par l'office n'avait pas pour objet de réparer, il y aurait lieu pour le juge de la mettre en cause.</ANA>
<ANA ID="9C"> 60-03-02-02-04 1) L'action engagée contre un établissement de santé par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des maladies nosocomiales (ONIAM), subrogé dans les droits de la victime qu'il a indemnisée dans le cadre de la procédure amiable prévue aux articles L. 1142-4 et suivants du code de la santé publique, doit être regardée, au sens et pour l'application des dispositions de l'article L. 376-1 du code de la sécurité sociale, comme une action de la victime. Par suite, il incombe au juge administratif, saisi d'une telle action contre un établissement public, de mettre en cause les caisses de sécurité sociale auxquelles la victime est ou était affiliée.,,,2) a) En revanche, la circonstance qu'une caisse ainsi mise en cause présente des conclusions contre l'établissement n'a pas pour conséquence d'obliger le juge à mettre en cause la victime. En effet, dès lors que le troisième alinéa de l'article L. 1142-15 du code de la santé publique prévoit que l'acceptation par la victime de l'offre de l'ONIAM vaut transaction, la victime ne dispose plus d'une action contre l'établissement, de sorte que sa mise en cause serait dépourvue d'objet. b) Il n'en irait autrement que dans le cas où l'offre de l'office exclurait explicitement de son champ certains des préjudices imputables à l'établissement de santé. La victime conservant, dans cette hypothèse, une action contre l'établissement au titre des préjudices que l'indemnité versée par l'office n'avait pas pour objet de réparer, il y aurait lieu pour le juge de la mettre en cause.</ANA>
<ANA ID="9D"> 60-05-04-01-01 1) L'action engagée contre un établissement de santé par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des maladies nosocomiales (ONIAM), subrogé dans les droits de la victime qu'il a indemnisée dans le cadre de la procédure amiable prévue aux articles L. 1142-4 et suivants du code de la santé publique, doit être regardée, au sens et pour l'application des dispositions de l'article L. 376-1 du code de la sécurité sociale, comme une action de la victime. Par suite, il incombe au juge administratif, saisi d'une telle action contre un établissement public, de mettre en cause les caisses de sécurité sociale auxquelles la victime est ou était affiliée.,,,2) a) En revanche, la circonstance qu'une caisse ainsi mise en cause présente des conclusions contre l'établissement n'a pas pour conséquence d'obliger le juge à mettre en cause la victime. En effet, dès lors que le troisième alinéa de l'article L. 1142-15 du code de la santé publique prévoit que l'acceptation par la victime de l'offre de l'ONIAM vaut transaction, la victime ne dispose plus d'une action contre l'établissement, de sorte que sa mise en cause serait dépourvue d'objet. b) Il n'en irait autrement que dans le cas où l'offre de l'office exclurait explicitement de son champ certains des préjudices imputables à l'établissement de santé. La victime conservant, dans cette hypothèse, une action contre l'établissement au titre des préjudices que l'indemnité versée par l'office n'avait pas pour objet de réparer, il y aurait lieu pour le juge de la mettre en cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
