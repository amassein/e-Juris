<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032853058</ID>
<ANCIEN_ID>JG_L_2016_07_000000391684</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/85/30/CETATEXT000032853058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 06/07/2016, 391684</TITRE>
<DATE_DEC>2016-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391684</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Célia Verot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391684.20160706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 10 juillet 2015 et 14 juin 2016 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale pommes poires demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision implicite de refus, née du silence gardé par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, le ministre des affaires sociales, de la santé et des droits des femmes, et le ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à l'abrogation de l'arrêté du 12 septembre 2006 relatif à la mise sur le marché et à l'utilisation des produits visés à l'article L. 253-1 du code rural ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'agriculture, de l'agroalimentaire et de la forêt, au ministre des affaires sociales, de la santé et des droits des femmes, et au ministre de l'écologie, du développement durable et de l'énergie d'abroger l'arrêté du 12 septembre 2006 à compter de la notification de la présente décision sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 91/414/CEE du Conseil du 15 juin 1991 ;<br/>
              - la directive 98/34/CE du Parlement européen et du Conseil du 22 juin 1998 modifiée ;<br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Célia Verot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en application de l'article 8 de la directive 98/34/CE du 22 juin 1998 modifiée prévoyant une procédure d'information dans le domaine des normes et réglementations techniques, tout Etat membre qui souhaite adopter une nouvelle règle technique au sens de cette directive ou modifier une règle technique existante doit, sauf s'il s'agit d'une simple transposition intégrale d'une norme internationale ou européenne ou d'une exception expressément prévue par la directive, en informer la Commission européenne dans les conditions prévues par cet article ; qu'en vertu des dispositions de l'article 10.1 de la même directive, l'obligation prévue à l'article 8 n'est pas applicable aux dispositions législatives, réglementaires et administratives des États membres ou aux accords volontaires par lesquels ces derniers se conforment aux actes communautaires contraignants qui ont pour effet l'adoption de spécifications techniques ; que constitue une règle technique au sens de la directive, selon les termes du 11) de son article 1er, " une spécification technique ou une autre exigence, y compris les dispositions administratives qui s'y appliquent, dont l'observation est obligatoire, de jure ou de facto, pour la commercialisation ou l'utilisation dans un Etat membre ou dans une partie importante de cet Etat " ; que constitue une spécification technique, au sens de la directive, selon les termes du 2) de son article 1er, " une spécification qui figure dans un document définissant les caractéristiques requises d'un produit, telles que les niveaux de qualité ou de propriété d'emploi, la sécurité, les dimensions, y compris les prescriptions applicables au produit en ce qui concerne la dénomination de vente, la terminologie, les symboles, les essais et les méthodes d'essai, l'emballage, le marquage et l'étiquetage, ainsi que les procédures d'évaluation de la conformité " ainsi que " les méthodes et les procédés de production relatifs aux produits agricoles au titre de l'article 38, paragraphe 1, du traité, aux produits destinés à l'alimentation humaine et animale, ainsi qu'aux médicaments tels que définis à l'article 1er de la directive 65/65/CEE du Conseil (7), de même que les méthodes et procédés de production relatifs aux autres produits, dès lors qu'ils ont une incidence sur les caractéristiques de ces derniers " ; que les " autres exigences " au sens de ces dispositions comportent notamment, aux termes du 4 ) de l'article 1er de la directive, " une exigence, autre qu'une spécification technique, imposée à l'égard d'un produit pour des motifs de protection, notamment des consommateurs ou de l'environnement, et visant son cycle de vie après mise sur le marché, telle que ses conditions d'utilisation, de recyclage, de réemploi ou d'élimination lorsque ces conditions peuvent influencer de manière significative la composition ou la nature du produit ou sa commercialisation " ; qu'en vertu des dispositions de l'article 3.3 de la directive 91/414/CEE du 15 juillet 1991 concernant la mise sur le marché des produits phytopharmaceutiques, les États membres prescrivent que les produits phytopharmaceutiques doivent faire l'objet d'un usage approprié, comportant notamment l'application des bonnes pratiques phytosanitaires ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 253-3 du code rural et de la pêche maritime : " Dans l'intérêt de la santé publique ou de l'environnement, l'autorité administrative peut prendre toute mesure d'interdiction, de restriction ou de prescription particulière concernant la mise sur le marché, la délivrance, l'utilisation et la détention des produits mentionnés à l'article L. 253-1 " ;<br/>
<br/>
              3. Considérant que l'arrêté du 12 septembre 2006 relatif à la mise sur le marché et à l'utilisation des produits visés à l'article L. 253-1 du code rural dont l'Association nationale pommes poires demande l'abrogation comporte des exigences qui portent directement sur les conditions d'utilisation des produits phytosanitaires et influencent de manière significative leur commercialisation ; que si ses dispositions ne constituent pas des spécifications techniques telles que définies par le 2) de l'article 1er de la directive du 22 juin 1998 et ne peuvent dès lors être considérées comme prises pour la mise en conformité avec un acte communautaire contraignant ayant pour effet l'adoption de spécifications techniques au sens des dispositions de l'article 10.1 de la même directive, elles comprennent d'" autres exigences " telles que définies par le 4) de l'article 1er de la directive et revêtent, dès lors, le caractère de règles techniques dont le projet devait être notifié à la Commission européenne préalablement à leur adoption ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en l'absence de notification du projet d'arrêté à la Commission européenne, celui-ci a été pris à l'issue d'une procédure irrégulière ; que l'association requérante est fondée, sans qu'il soit besoin d'examiner les autres moyens de la requête, à demander l'annulation de la décision implicite de refus, née du silence gardé par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, le ministre des affaires sociales, de la santé et des droits des femmes, et le ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à son abrogation ; <br/>
<br/>
              Sur les conclusions à fins d'injonction : <br/>
<br/>
              5. Considérant que l'annulation de la décision implicite refusant d'abroger l'arrêté du 12 septembre 2006 implique nécessairement l'abrogation des dispositions réglementaires dont l'illégalité a été constatée ; qu'il y a lieu pour le Conseil d'Etat d'ordonner cette mesure dans un délai de six mois à compter de la présente décision ; que dans les circonstances de l'espèce il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par l'Association nationale pommes poires ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros demandée par l'Association nationale pommes poires sur le fondement de ces dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite de refus, née du silence gardé par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, le ministre des affaires sociales, de la santé et des droits des femmes, et le ministre de l'écologie, du développement durable et de l'énergie sur la demande de l'Association nationale pommes poires tendant à l'abrogation de  l'arrêté du 12 septembre 2006 est annulée.<br/>
Article 2 : Il est enjoint au ministre de l'agriculture, de l'agroalimentaire et de la forêt, au ministre des affaires sociales, de la santé et des droits des femmes, et au ministre de l'écologie, du développement durable et de l'énergie d'abroger l'arrêté du 12 septembre 2006 dans le délai de six mois à compter de la notification de la présente décision. <br/>
Article 3 : L'Etat versera à l'Association nationale pommes poires une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à l'Association nationale pommes poires, au ministre de l'agriculture, de l'agroalimentaire et de la forêt, au ministre des affaires sociales, de la santé et des droits des femmes, et au ministre de l'écologie, du développement durable et de l'énergie<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE, PRÉALABLEMENT À LEUR ADOPTION, DES DISPOSITIONS ÉDICTANT DES RÈGLES TECHNIQUES (ART. 8 DE LA DIRECTIVE 98/34/CE DU 22 JUIN 1998) - DÉROGATION À CETTE OBLIGATION POUR LES SPÉCIFICATIONS TECHNIQUES PRISES POUR LA MISE EN CONFORMITÉ AVEC UN ACTE COMMUNAUTAIRE - CHAMP DE CETTE DÉROGATION - RÈGLES TECHNIQUES AUTRES QUE LES SPÉCIFICATIONS TECHNIQUES - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉS DE CIRCULATION. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE, PRÉALABLEMENT À LEUR ADOPTION, DES DISPOSITIONS ÉDICTANT DES RÈGLES TECHNIQUES (ART. 8 DE LA DIRECTIVE 98/34/CE DU 22 JUIN 1998) - DÉROGATION À CETTE OBLIGATION POUR LES SPÉCIFICATIONS TECHNIQUES PRISES POUR LA MISE EN CONFORMITÉ AVEC UN ACTE COMMUNAUTAIRE - CHAMP DE CETTE DÉROGATION - RÈGLES TECHNIQUES AUTRES QUE LES SPÉCIFICATIONS TECHNIQUES - EXCLUSION.
</SCT>
<ANA ID="9A"> 01-03 En application de l'article 8 de la directive 98/34/CE du 22 juin 1998 modifiée prévoyant une procédure d'information dans le domaine des normes et réglementations techniques, tout Etat membre qui souhaite adopter une nouvelle règle technique au sens de cette directive ou modifier une règle technique existante doit, sauf s'il s'agit d'une simple transposition intégrale d'une norme internationale ou européenne ou d'une exception expressément prévue par la directive, en informer la Commission européenne dans les conditions prévues par cet article.... ,,En vertu des dispositions de l'article 10.1 de la même directive, l'obligation prévue à l'article 8 n'est pas applicable aux dispositions législatives, réglementaires et administratives des États membres ou aux accords volontaires par lesquels ces derniers se conforment aux actes communautaires contraignants qui ont pour effet l'adoption de spécifications techniques.,,,Cette dérogation à l'obligation prévue à l'article 8 ne s'applique pas aux exigences, autres que les spécifications techniques, qui constituent des règles techniques. Tel est notamment le cas de règles qui fixent les conditions d'utilisation d'un produit et influencent de manière significative leur commercialisation.</ANA>
<ANA ID="9B"> 15-05-01 En application de l'article 8 de la directive 98/34/CE du 22 juin 1998 modifiée prévoyant une procédure d'information dans le domaine des normes et réglementations techniques, tout Etat membre qui souhaite adopter une nouvelle règle technique au sens de cette directive ou modifier une règle technique existante doit, sauf s'il s'agit d'une simple transposition intégrale d'une norme internationale ou européenne ou d'une exception expressément prévue par la directive, en informer la Commission européenne dans les conditions prévues par cet article.... ,,En vertu des dispositions de l'article 10.1 de la même directive, l'obligation prévue à l'article 8 n'est pas applicable aux dispositions législatives, réglementaires et administratives des États membres ou aux accords volontaires par lesquels ces derniers se conforment aux actes communautaires contraignants qui ont pour effet l'adoption de spécifications techniques.,,,Cette dérogation à l'obligation prévue à l'article 8 ne s'applique pas aux exigences, autres que les spécifications techniques, qui constituent des règles techniques. Tel est notamment le cas de règles qui fixent les conditions d'utilisation d'un produit et influencent de manière significative leur commercialisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
