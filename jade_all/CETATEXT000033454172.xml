<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033454172</ID>
<ANCIEN_ID>JG_L_2016_11_000000383815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/45/41/CETATEXT000033454172.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/11/2016, 383815, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:383815.20161121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montpellier de condamner l'Etat à lui verser la somme de 10 791,42 euros en réparation du préjudice subi du fait de la double imposition de ses revenus par les administrations fiscales française et espagnole. Par un jugement n° 0902075 du 9 juin 2011, le tribunal administratif a fait droit à sa demande à hauteur de 6 485,05 euros et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 11MA03217 du 17 juin 2014, la cour administrative d'appel de Marseille a, sur le recours du ministre des finances et des comptes publics, annulé les articles 1er et 2 de ce jugement et rejeté les conclusions présentées par M. B... devant le tribunal administratif de Montpellier ainsi que les conclusions de son appel incident.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 août et 20 novembre 2014 et le 24 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le recours formé par le ministre et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention entre la France et l'Espagne du 10 octobre 1995 en vue d'éviter les doubles impositions et de prévenir l'évasion et la fraude fiscales en matière d'impôts sur le revenu et sur la fortune ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le point 12 du protocole annexé à la convention signée le 10 octobre 1995 entre les gouvernements français et espagnol en vue d'éviter les doubles impositions et de prévenir l'évasion et la fraude fiscale prévoit l'imposition des travailleurs frontaliers dans l'Etat dont ils sont résidents pour les salaires qu'ils perçoivent en cette qualité. La zone frontalière est définie par l'article 2 de l'accord complémentaire entre la France et l'Espagne relatif aux travailleurs frontaliers signé le 25 janvier 1961, ultérieurement modifié, en dernier lieu par un échange de notes des 21 mai et 1er juin 1965. Enfin, par un échange de lettres en date du 19 février 1998, a été prévue l'utilisation de formulaires d'attestation, destinés notamment à certifier le lieu de domiciliation des personnes concernées.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., domicilié... ". A cette date, M. B... a déposé auprès du centre des impôts de Céret une demande de délivrance du certificat n° 5166, qui constitue le formulaire d'attestation prévu par l'échange de lettres mentionné au point 1, qui devait lui permettre, en le présentant à l'administration fiscale espagnole, d'éviter l'imposition de ses revenus en Espagne. Le centre des impôts de Céret a refusé d'établir ce document le 8 juin 2006 et a informé M. B... qu'il ne pourrait le lui délivrer que lors de sa prochaine déclaration annuelle de revenus. M. B...n'a obtenu ce document que le 24 mai 2007. A compter du 8 juin 2006, les salaires de M. B...ont ainsi été imposés, par voie d'un prélèvement à la source au taux de 25%, à l'impôt sur le revenu espagnol, alors qu'il était régulièrement assujetti, en France, à l'impôt sur le revenu sur les mêmes salaires. Il se pourvoit en cassation contre l'arrêt du 17 juin 2014 par lequel la cour administrative d'appel de Marseille, d'une part, a annulé, sur le recours du ministre, le jugement du tribunal administratif de Montpellier qui lui avait accordé une indemnité de 6 485,05 euros au titre de l'indemnisation du préjudice qu'il estimait avoir subi du fait de la faute commise par les services fiscaux en refusant de lui délivrer le certificat n° 5166 dès le 8 juin 2006, d'autre part, a rejeté son appel incident tendant à ce que cette indemnisation soit portée à la somme de 10 791,42 euros. <br/>
<br/>
              3. Aux termes de l'article 26 de la convention fiscale franco-espagnole signée le 10 octobre 1995 : " 1. Lorsqu'une personne estime que les mesures prises par un État contractant ou par les deux États contractants entraînent ou entraîneront pour elle une imposition non conforme aux dispositions de la présente Convention, elle peut, indépendamment des recours prévus par le droit interne de ces États, soumettre son cas à l'autorité compétente de l'État contractant dont elle est un résident ou, si son cas relève du paragraphe 1 de l'article 25, à celle de l'État contractant dont elle possède la nationalité. Le cas doit être soumis dans les trois ans qui suivent la première notification de la mesure qui entraîne une imposition non conforme aux dispositions de la Convention. / 2. L'autorité compétente s'efforce, si la réclamation lui paraît fondée et si elle n'est pas elle même en mesure d'y apporter une solution satisfaisante, de résoudre le cas par voie d'accord amiable avec l'autorité compétente de l'autre État contractant, en vue d'éviter une imposition non conforme à la Convention. L'accord est appliqué quels que soient les délais prévus par le droit interne des États contractants. / 3. Les autorités compétentes des États contractants s'efforcent par voie d'accord amiable, de résoudre les difficultés ou de dissiper les doutes auxquels peuvent donner lieu l'interprétation ou l'application de la Convention. Elles peuvent aussi se concerter en vue d'éliminer la double imposition dans les cas non prévus par la Convention. / 4. Les autorités compétentes des États contractants peuvent communiquer directement entre elles en vue de parvenir à un accord comme il est indiqué aux paragraphes précédents. Si des échanges de vue oraux semblent devoir faciliter cet accord, ces échanges de vue peuvent avoir lieu au sein d'une commission composée de représentants des autorités compétentes des États contractants. ".<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que la cour a jugé qu'après avoir obtenu le certificat n° 5166 en date du 24 mai 2007, M. B... n'avait saisi ni l'administration fiscale ni le juge espagnols et n'avait pas non plus recouru à la procédure amiable prévue par l'article 26 de la convention fiscale franco-espagnole du 10 octobre 1995, de sorte qu'il conservait la faculté d'obtenir le remboursement des sommes prélevées en Espagne en raison de la non délivrance du certificat n° 5166 avant le 24 mai 2007 et que, partant, son préjudice ne pouvait être regardé comme certain.<br/>
<br/>
              5. En statuant ainsi, alors qu'à défaut de détenir un certificat n° 5166 couvrant la période du 8 juin 2006 au 30 mars 2007, M. B...n'était pas en mesure d'obtenir le remboursement des sommes prélevées en Espagne sur les salaires perçus entre ces dates, et que, par ailleurs, par des courriers échangés avec le conciliateur départemental, il avait demandé en substance qu'il soit recouru à la procédure amiable prévue par l'article 26 de la convention fiscale franco-espagnole du 10 octobre 1995, la cour a entaché son arrêt de dénaturation des faits. Il en résulte que M. B...est fondé à en demander l'annulation.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur le recours du ministre des finances et des comptes publics :<br/>
<br/>
              7. En premier lieu, le moyen tiré de l'irrecevabilité de la demande de première instance pour défaut de liaison du contentieux doit être écarté pour les motifs retenus par les premiers juges.<br/>
<br/>
              8. En deuxième lieu, la demande indemnitaire d'un contribuable se prévalant d'une faute des services fiscaux qui a le même objet que celle qu'il a formée ou aurait pu former aux fins de décharge ou de restitution de l'impôt établi par l'administration française n'est pas recevable. Toutefois, M. B...n'a pas demandé au juge français la décharge des cotisations d'impôt sur le revenu qu'il a acquittées en France au cours des années 2006 et 2007, mais la réparation du préjudice né de la perception de salaires nets minorés, du fait du prélèvement à la source de l'impôt espagnol sur ses salaires perçus en Espagne. Dans ces conditions, sa demande formée devant le tribunal administratif de Montpellier était recevable. <br/>
<br/>
              9. En troisième lieu, le moyen tiré de ce que M. B...ne remplissait pas, entre le 8 juin 2006 et le 30 mars 2007, les conditions lui permettant de bénéficier du statut de travailleur frontalier tel qu'il est prévu par les textes mentionnés au point 1 doit être écarté pour les motifs retenus par les premiers juges.<br/>
<br/>
              10. En quatrième lieu, en refusant de remettre à M. B...le certificat n° 5166 dès le 8 juin 2006, alors qu'il avait déposé sa demande en temps utile pour l'obtenir à cette date, l'administration a commis une erreur dans l'appréciation de la situation de ce contribuable au regard des stipulations de l'échange de lettres en date du 19 février 1998 et des conséquences matérielles de son refus, quand bien même les textes ne lui assigneraient aucun délai pour délivrer ce document. Ce refus, contrairement à ce qui est soutenu par le ministre, est constitutif d'une faute de nature à engager la responsabilité de l'Etat vis-à-vis de M.B.fiscalement au Boulou, commune des Pyrénées-Orientales située dans la zone frontalière française a, à partir du 8 juin 2006, travaillé à Llansa, localité située dans la zone frontalière espagnole, en qualité de salarié de la société " Rio Iberica SA<br/>
<br/>
              11. En dernier lieu, si le ministre se prévaut de ce que M. B... aurait eu un comportement fautif en ne saisissant pas l'administration fiscale et les tribunaux espagnols d'une demande de remboursement des impôts qu'il avait supportés et en ne se prévalant pas de l'article 26 de la convention franco-espagnole du 10 octobre 1995, il résulte de ce qui est dit au point 5 qu'il n'a pas commis les fautes que lui sont ainsi reprochées.<br/>
<br/>
              12. Il résulte de tout ce qui précède que le ministre des finances et des comptes publics n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a condamné l'Etat à verser à M. B...la somme de 6 485,05 euros. <br/>
<br/>
              Sur les conclusions incidentes présentées par M.B... :<br/>
<br/>
              13. Par la voie de l'appel incident, M. B...conteste le montant de l'indemnisation qui lui a été accordée par le jugement du 9 juin 2011 du tribunal administratif de Montpellier et demande qu'elle soit portée de 6 485,05 à 10 791,42 euros, de façon à compenser également la perte de revenu net qu'il allègue avoir subie au titre de la période allant d'avril à septembre 2007. Toutefois, s'il établit avoir travaillé à Llansa, commune espagnole de la zone frontalière, jusqu'au 30 mars 2007, il résulte de l'instruction que les bulletins de paye qu'il fournit mentionnent, à partir d'avril 2007, une adresse de son employeur située en dehors de cette zone. Le préjudice dont il se prévaut à compter de cette date ne peut, dès lors, être regardé comme en lien direct avec la faute qu'ont commise les services fiscaux qui, au surplus, lui ont fourni le certificat n° 5166 le 24 mai 2007. Il suit de là que les conclusions présentées par M. B... par la voie de l'appel incident ne peuvent qu'être rejetées.<br/>
<br/>
              Sur l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat le versement à M.B..., pour l'ensemble de la procédure, de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 17 juin 2014 est annulé. <br/>
Article 2 : Le recours présenté par le ministre des finances et des comptes publics devant la cour administrative d'appel de Marseille est rejeté.<br/>
Article 3 : Les conclusions incidentes présentées par M. B...devant la cour administrative d'appel de Marseille sont rejetées.<br/>
Article 4 : L'Etat versera à M. B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. B...et au ministre de l'économie et des finances<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
