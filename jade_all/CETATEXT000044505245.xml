<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505245</ID>
<ANCIEN_ID>JG_L_2021_12_000000443584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505245.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 15/12/2021, 443584, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443584.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 10 mars 2021, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de M. D... A... dirigées contre l'arrêt du 2 juillet 2020 de la cour administrative d'appel de Lyon en tant seulement qu'il statue sur la réparation de la perte de ses droits à retraite et sur l'attribution des intérêts moratoires ainsi que sur leur capitalisation.<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 juin 2021, le centre hospitalier universitaire Grenoble Alpes conclut au rejet du pourvoi. Il soutient que les moyens soulevés par le requérant ne sont pas fondés. <br/>
<br/>
              Par un mémoire en réplique, enregistré le 27 septembre 2021, M. A... conclut aux mêmes fins que son pourvoi, par les mêmes moyens.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. A... et à Me Le Prado, avocat du centre hospitalier universitaire de Grenoble.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 2 juillet 2020, la cour administrative d'appel de Lyon, statuant sur l'appel de M. A... et sur les appels provoqué et incident du centre hospitalier universitaire (CHU) Grenoble Alpes, formés contre le jugement du 31 octobre 2016 par lequel le tribunal administratif de Grenoble a partiellement fait droit aux conclusions indemnitaires présentées par M. et Mme A... au titre de la cécité corticale et des autres séquelles neurologiques laissées à M. A... par sa prise en charge dans cet établissement en avril 2012, a réformé ce jugement en substituant à la rente annuelle de 42 987 euros jusqu'au 31 décembre 2021 puis à la rente trimestrielle viagère de 30 577 euros après cette date qu'il prévoyait une rente trimestrielle viagère d'un montant de 6 586,34 euros au titre de l'assistance future par une tierce personne et une rente trimestrielle complémentaire d'un montant de 542,49 euros versée jusqu'aux 62 ans de l'intéressé, âge auquel il aurait pu prétendre à une pension à taux plein, au titre de ses pertes de revenus professionnels futurs. Par une décision du 10 mars 2021, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. A... dirigées contre cet arrêt, en tant qu'il statue sur la réparation de la perte de ses droits à retraite et sur l'attribution des intérêts moratoires ainsi que sur leur capitalisation.<br/>
<br/>
              Sur les droits à retraite de M. A... :<br/>
<br/>
              2. Il ressort du jugement du tribunal administratif de Grenoble que celui-ci a indemnisé dans le chef de M. A..., d'une part, une incidence professionnelle chiffrée à 30 000 euros et réparant uniquement la difficulté accrue de retrouver un emploi, d'autre part, des pertes de gains professionnels futurs incluant nécessairement, du fait de leur réparation à titre viager, la perte d'une partie de ses droits à retraite. Par suite, en substituant à la rente viagère réparant les pertes des gains professionnels futures de M. A... une rente trimestrielle versée jusqu'à ses soixante-deux ans, " âge auquel l'intéressé aurait pu prétendre à une pension à taux plein ", au motif qu'il était " par ailleurs indemnisé au titre de l'incidence professionnelle par la mise à la charge non contestée du centre hospitalier universitaire Grenoble Alpes d'une somme de 30 000 euros ", la cour administrative d'appel de Lyon a méconnu le principe de réparation intégrale du préjudice. <br/>
<br/>
              3. Il y a lieu, par suite, d'annuler son arrêt en tant qu'il omet de statuer sur la perte des droits à retraite de M. A.... <br/>
<br/>
              Sur les intérêts moratoires et leur capitalisation :<br/>
<br/>
              4. M. A... a demandé à la cour administrative d'appel de juger que les sommes mises à la charge du CHU Grenoble Alpes " porteraient intérêts à compter de la demande préalable et en tout état de cause à compter de la saisine initiale du tribunal administratif ". L'arrêt attaqué omettant de statuer sur ces conclusions présentées sur le fondement de l'article 1153 du code civil, il y a également lieu de l'annuler dans cette mesure. En revanche, M. A... n'ayant pas demandé à la cour administrative d'appel que les intérêts soient capitalisés, il n'y a pas lieu d'annuler son arrêt en tant qu'il s'abstient de statuer sur de telles conclusions. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHU Grenoble Alpes la somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 2 juillet 2020 est annulé en tant qu'il statue sur la réparation de la perte de droits à retraite de M. A... et sur l'application des intérêts prévus par l'article 1153 du code civil.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon dans la limite de la cassation ainsi prononcée.<br/>
<br/>
		Article 3 : Le surplus des conclusions du pourvoi de M. A... est rejeté.<br/>
<br/>
Article 4 : Le CHU Grenoble Alpes versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. D... A... et au centre hospitalier universitaire Grenoble Alpes.<br/>
              Délibéré à l'issue de la séance du 17 novembre 2021 où siégeaient : M. Jean-Philippe Mochon, assesseur, présidant ; M. Olivier Yeznikian, conseiller d'Etat et M. Jean-Dominique Langlais, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 15 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. F... B...<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Dominique Langlais<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
