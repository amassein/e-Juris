<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037279253</ID>
<ANCIEN_ID>JG_L_2018_07_000000411345</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/92/CETATEXT000037279253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/07/2018, 411345</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411345</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411345.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 juin 2017 au secrétariat du contentieux du Conseil d'État, la Fédération des médecins de France (FMF) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 7 avril 2017 portant nomination du président et des membres du collège de la Haute Autorité de santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2013-907 du 11 octobre 2013 ;<br/>
              - la loi n° 2017-55 du 20 janvier 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 161-37 du code de la sécurité sociale, la Haute Autorité de santé est une autorité publique indépendante à caractère scientifique. L'article L. 161-42 du même code dispose que son collège est " composé de sept membres choisis en raison de leur expertise et de leur expérience dans [ses] domaines de compétence ", " nommés par décret ", parmi lesquels " trois membres désignés par les ministres chargés de la santé et de la sécurité sociale ". La Fédération des médecins de France demande l'annulation pour excès de pouvoir du décret du 7 avril 2017 portant nomination du président et des membres du collège de la Haute Autorité de santé, en tant qu'il nomme M. B...A...membre de ce collège.<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              2. Aux termes de l'article 13 de la Constitution : " Le Président de la République signe les ordonnances et les décrets délibérés en conseil des ministres. / Il nomme aux emplois civils et militaires de l'État. (...) ". Aux termes de l'article 19 de la Constitution : " Les actes du Président de la République autres que ceux prévus aux articles 8 (1er alinéa), 11, 12, 16, 18, 54, 56 et 61 sont contresignés par le Premier ministre et, le cas échéant, par les ministres responsables ".<br/>
<br/>
              3. Il résulte des articles 13 et 19 de la Constitution que le décret par lequel, en vertu de l'article L. 161-42 du code de la sécurité sociale, les membres du collège de la Haute Autorité de santé sont nommés doit être pris par le Président de la République et contresigné par le Premier ministre et les ministres responsables.<br/>
<br/>
              4. Il ressort des pièces du dossier, notamment de l'ampliation de la minute du décret attaqué, communiquée par le Premier ministre et versée au dossier de l'instruction écrite contradictoire, que ce décret a, contrairement à ce qui est soutenu par la fédération requérante, été contresigné par le Premier ministre et par le ministre des affaires sociales et de la santé, sur le rapport desquels il a été pris. Par suite, la fédération requérante n'est pas fondée à soutenir que le décret attaqué serait irrégulier, faute d'être revêtu de ces contreseings.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              5. D'une part, aux termes de l'article 9 de la loi du 20 janvier 2017 portant statut général des autorités administratives indépendantes et des autorités publiques indépendantes : " Les membres des autorités administratives indépendantes et des autorités publiques indépendantes exercent leurs fonctions avec dignité, probité et intégrité et veillent à prévenir ou à faire cesser immédiatement tout conflit d'intérêts, au sens de la loi n° 2013-907 du 11 octobre 2013 relative à la transparence de la vie publique. / (...) / Les membres des autorités administratives indépendantes et des autorités publiques indépendantes ne prennent, à titre personnel, aucune position publique préjudiciable au bon fonctionnement de l'autorité à laquelle ils appartiennent ". Aux termes de l'article 12 de la même loi : " Aucun membre de l'autorité administrative indépendante ou de l'autorité publique indépendante ne peut siéger ou, le cas échéant, ne peut participer à une délibération, une vérification ou un contrôle si : / 1° Il y a un intérêt, au sens de l'article 2 de la loi n° 2013-907 du 11 octobre 2013 précitée, ou il y a eu un tel intérêt au cours des trois années précédant la délibération, la vérification ou le contrôle ; / 2° Il exerce des fonctions ou détient des mandats ou, si au cours de la même période, il a exercé des fonctions ou détenu des mandats au sein d'une personne morale concernée par la délibération, la vérification ou le contrôle ; / 3° Il représente ou, au cours de la même période, a représenté une des parties intéressées ". L'article 2 de la loi du 11 octobre 2013 relative à la transparence de la vie publique précise que : " (...) constitue un conflit d'intérêts toute situation d'interférence entre un intérêt public et des intérêts publics ou privés qui est de nature à influencer ou à paraître influencer l'exercice indépendant, impartial et objectif d'une fonction ". <br/>
<br/>
              6. D'autre part, en vertu du I de l'article L. 1451-1 du code de la santé publique, les membres du collège de la Haute Autorité de santé : " sont tenus, lors de leur prise de fonctions, d'établir une déclaration d'intérêts. / (...) / Elle mentionne les liens d'intérêts de toute nature, directs ou par personne interposée, que le déclarant a, ou qu'il a eus pendant les cinq années précédant sa prise de fonctions, avec des entreprises, des établissements ou des organismes dont les activités, les techniques et les produits entrent dans le champ de compétence de l'autorité sanitaire au sein de laquelle il exerce ses fonctions ou de l'organe consultatif dont il est membre ainsi qu'avec les sociétés ou organismes de conseil intervenant dans les mêmes secteurs. / (...) / Les personnes mentionnées au présent article ne peuvent prendre part aux travaux, aux délibérations et aux votes des instances au sein desquelles elles siègent qu'une fois la déclaration souscrite ou actualisée. Elles ne peuvent, sous les peines prévues à l'article 432-12 du code pénal, prendre part ni aux travaux, ni aux délibérations, ni aux votes de ces instances si elles ont un intérêt, direct ou indirect, à l'affaire examinée (...) ". Enfin, aux termes de l'article R. 161-86 du code de la sécurité sociale : " Les membres du collège ne peuvent avoir par eux-mêmes, ou par personne interposée, dans les établissements ou entreprises intervenant dans les domaines de compétence de la Haute Autorité, des intérêts de nature à compromettre leur indépendance. Ils ne peuvent exercer parallèlement des fonctions de direction dans des organismes ou services liés par convention avec des entreprises exploitant des médicaments ou fabricant des produits de santé. / Les membres du collège qui détiennent de tels intérêts ou exercent de telles fonctions disposent, à compter de la date de leur nomination, d'un délai de trois mois pour s'en défaire ou les quitter. A défaut, ils sont déclarés démissionnaires d'office (...) ".<br/>
<br/>
              7. Tout d'abord, l'obligation qui résulte de l'article 9 de la loi du 20 janvier 2017, comme du principe d'impartialité, de s'abstenir de toute prise de position publique qui serait préjudiciable au bon fonctionnement d'une autorité administrative ou publique indépendante n'est applicable qu'aux déclarations faites par leurs membres. Si le principe d'impartialité peut également s'opposer à la nomination d'une personne comme membre d'une telle autorité eu égard à ses prises de positions publiques antérieures, c'est uniquement dans le cas où elles seraient incompatibles avec le bon fonctionnement de cette autorité. En l'espèce,  il ne résulte pas de la seule circonstance que M. A...a exprimé en sa qualité de représentant d'une association de patients, quelques années avant sa nomination comme membre du collège de la Haute Autorité de santé par le décret attaqué, des positions pouvant être perçues comme hostiles aux médecins généralistes exerçant à titre libéral que sa nomination pourrait être regardée comme contraire au principe d'impartialité.<br/>
<br/>
              8. Ensuite, si la fédération requérante relève que M.A..., peu de temps avant sa nomination ou à la date de celle-ci, avait des intérêts entrant dans le champ des compétences de la Haute Autorité de santé, en invoquant notamment sa participation au conseil d'administration d'une fondation d'entreprise créée par un laboratoire pharmaceutique, il résulte des dispositions des articles L. 1451-1 du code de la santé publique et R. 161-86 du code de la sécurité sociale que, d'une part, de tels intérêts doivent être déclarés et que, d'autre part, s'ils sont de nature à compromettre l'indépendance du membre du collège qui les détient, celui-ci dispose, à compter de la date de sa nomination, d'un délai de trois mois pour s'en défaire. Le risque que de tels intérêts portent atteinte au fonctionnement impartial du collège de la Haute Autorité de santé est prévenu par l'obligation, pour ses membres, de s'abstenir de participer aux travaux, délibérations et votes lorsque le collège statue sur des questions susceptibles de mettre en cause ces intérêts, ainsi que le prévoient les dispositions citées aux points 5 et 6. S'il incombe à l'autorité de nomination de s'assurer que la personne qu'elle envisage de nommer ne se trouve pas dans une situation telle que l'application des règles de déport la conduirait à devoir s'abstenir de participer aux travaux de l'autorité administrative ou publique indépendante à une fréquence telle que le fonctionnement normal de cette autorité en serait entravé, il ne ressort pas des pièces du dossier, notamment des procès-verbaux du comité de validation de l'analyse des déclarations d'intérêts produits par la Haute Autorité de santé, que M. A... serait de façon systématique, du fait de cette obligation de déport, dans l'impossibilité de participer aux travaux du collège de la Haute Autorité de santé. <br/>
<br/>
              9. Enfin, d'une part, si la fédération requérante soutient que deux déclarations d'intérêts remplies en 2012 et 2015 par M. A...auraient un caractère lacunaire, il ressort des pièces du dossier que ces déclarations, faites à l'occasion de colloques organisés par la Haute Autorité de santé, avaient pour seul objet d'identifier les liens d'intérêt avec les industries de santé en rapport avec le thème de la présentation réalisée. D'autre part, il est vrai que, dans sa déclaration d'intérêts du 6 avril 2017, à la différence de celle souscrite le 2 mai 2017, M. A...n'a pas fait mention de sa qualité de membre du conseil d'administration d'une fondation d'entreprise relevant du secteur de la protection sociale complémentaire jusqu'en 2015 ni de président, de février à octobre 2014, d'une association promouvant l'ouverture des données de santé de l'assurance maladie. Toutefois, l'établissement d'une déclaration mentionnant les liens d'intérêts du déclarant n'est pas une condition préalable à la nomination d'un membre du collège de la Haute Autorité de santé mais seulement à sa participation aux travaux, délibérations et votes de ce collège. <br/>
<br/>
              10. Il suit de là que la fédération requérante n'est pas fondée à soutenir que le décret attaqué méconnaîtrait les principes d'indépendance et d'impartialité du fait des prises de position passées de M.A..., des intérêts qu'il détient ou a détenus et des omissions réelles ou supposées des déclarations d'intérêts qu'il a souscrites. Il ne ressort pas non plus des pièces du dossier que, pour les mêmes motifs, et alors que les omissions relevées par la requérante dans la déclaration d'intérêts faite par M. A...le 6 avril 2017 ne révèlent pas une volonté de sa part de dissimuler des situations de conflits d'intérêts, le décret attaqué serait entaché d'erreur manifeste d'appréciation en ce qu'il le nomme membre du collège de la Haute Autorité de santé.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la fédération requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque. Il n'est, dès lors, pas nécessaire d'examiner son intérêt à agir, alors même qu'il est contesté en défense par le ministre des solidarités et de la santé. <br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                        --------------<br/>
<br/>
Article 1er : La requête de la Fédération des médecins de France est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération des médecins de France, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la Haute Autorité de santé et à M. B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - MOYEN TIRÉ DE SA MÉCONNAISSANCE - OPÉRANCE À L'ENCONTRE D'UNE DÉCISION NOMMANT LE PRÉSIDENT ET LES MEMBRES D'UNE AUTORITÉ ADMINISTRATIVE INDÉPENDANTE (AAI) OU D'UNE AUTORITÉ PUBLIQUE INDÉPENDANTE (API) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - 1) MOYEN SOULEVÉ À L'APPUI D'UNE DÉCISION NOMMANT LE PRÉSIDENT ET LES MEMBRES DE LA HAUTE AUTORITÉ DE SANTÉ (HAS) ET TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ - ABSENCE [RJ1] - 2) MOYEN SOULEVÉ À L'APPUI DE LA MÊME DÉCISION ET TIRÉ DE CE QUE L'APPLICATION DES RÈGLES DE DÉPORT CONDUIRAIT UNE PERSONNE À S'ABSTENIR DE PARTICIPER AUX TRAVAUX DE LA HAS À UNE FRÉQUENCE TELLE QUE LE FONCTIONNEMENT NORMAL DE CETTE AUTORITÉ EN SERAIT ENTRAVÉ - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-11-02 SANTÉ PUBLIQUE. - DÉCISION NOMMANT SON PRÉSIDENT ET SES MEMBRES - 1) MOYEN TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ - OPÉRANCE - EXISTENCE [RJ1] - 2) MOYEN TIRÉ DE CE QUE L'APPLICATION DES RÈGLES DE DÉPORT CONDUIRAIT UNE PERSONNE À S'ABSTENIR DE PARTICIPER AUX TRAVAUX DE LA HAS À UNE FRÉQUENCE TELLE QUE LE FONCTIONNEMENT NORMAL DE CETTE AUTORITÉ EN SERAIT ENTRAVÉ - OPÉRANCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03 Le moyen tiré de la méconnaissance du principe d'impartialité est opérant à l'encontre d'une décision nommant le président et les membres d'une AAI ou d'une API.</ANA>
<ANA ID="9B"> 54-07-01-04-03 1) Le moyen tiré de la méconnaissance du principe d'impartialité est opérant à l'encontre d'une décision nommant le président et les membres de la Haute Autorité de santé (HAS).,,2) Le moyen tiré de ce qu'une personne qu'il est envisagé de nommer se trouve dans une situation telle que l'application des règles de déport la conduirait à devoir s'abstenir de participer aux travaux de la HAS à une fréquence telle que le fonctionnement normal de cette autorité en serait entravé est opérant à l'encontre d'une décision nommant son président et ses membres.</ANA>
<ANA ID="9C"> 61-11-02 1) Le moyen tiré de la méconnaissance du principe d'impartialité est opérant à l'encontre d'une décision nommant le président et les membres de la Haute Autorité de santé (HAS).,,2) Le moyen tiré de ce qu'une personne qu'il est envisagé de nommer se trouve dans une situation telle que l'application des règles de déport la conduirait à devoir s'abstenir de participer aux travaux de la HAS à une fréquence telle que le fonctionnement normal de cette autorité en serait entravé est opérant à l'encontre d'une décision nommant son président et ses membres.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du Haut Conseil des biotechnologies, CE, 3 octobre 2010, Comité de recherche et d'information indépendante sur le génie génétique et,, n° 328326, T. p. 748.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
