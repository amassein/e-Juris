<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027328326</ID>
<ANCIEN_ID>JG_L_2013_04_000000362776</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/32/83/CETATEXT000027328326.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 17/04/2013, 362776, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362776</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362776.20130417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 11BX03397 du 14 septembre 2012, enregistrée au secrétariat de la section du contentieux du Conseil d'Etat le 17 septembre 2012, par laquelle le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire distinct de M. B...A... ;<br/>
<br/>
              Vu la requête, enregistrée au greffe de la cour administrative d'appel de Bordeaux le 26 décembre 2011, présentée par M.A..., demeurant... ; M. A...demande l'annulation du jugement n° 1100894 du tribunal administratif de Saint-Denis du 3 novembre 2011 rejetant sa demande tendant à l'annulation de la décision du 2 septembre 2011 par laquelle le préfet de La Réunion a prononcé sa démission d'office de son mandat de conseiller municipal de la commune de Saint-Leu ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que M.A..., élu conseiller municipal de la commune de Saint-Leu à La Réunion lors des élections municipales de mars 2008 a été recruté en qualité de directeur de cabinet du président du conseil régional de La Réunion, par un arrêté du 31 mars 2010 de ce dernier ; que, par une décision du 2 septembre 2011, le préfet de La Réunion l'a déclaré démissionnaire d'office, en application des dispositions combinées des articles L. 231 et L. 236 du code électoral, de son mandat de conseiller municipal de la commune de Saint-Leu pour cause d'inéligibilité intervenue en cours de mandat ; que M. A...relève appel du jugement du 3 novembre 2011 par lequel le tribunal administratif de Saint-Denis a rejeté sa requête tendant à l'annulation de cette décision ; qu'il conteste, à l'occasion de cet appel, l'ordonnance du 23 septembre 2011 par laquelle le président du tribunal administratif de Saint-Denis a refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 8° de l'article L. 231 du code électoral ;<br/>
<br/>
              2. Considérant que les dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel prévoient que lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 231 du code électoral, dans sa rédaction applicable à la date de la décision attaquée : " Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : / (...) 8° Les directeurs de cabinet du président du conseil général et du président du conseil régional, les directeurs généraux, les directeurs, les directeurs adjoints, chefs de service et chefs de bureau de conseil général et de conseil régional, (...) " ; que M. A... soutient qu'en ce qu'elles mentionnent les directeurs de cabinet du président du conseil général et du président du conseil régional, mais ne visent pas, notamment, les directeurs de cabinet des présidents d'établissements publics de coopération intercommunale à fiscalité propre, ces dispositions instaurent une discrimination contraire au principe d'égalité protégé par les articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen de 1789 et portent une atteinte disproportionnée au droit d'être élu qui découle de ce même article 6 ;<br/>
<br/>
              4. Considérant que les dispositions précitées  sont applicables au litige et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité, présente un caractère sérieux ; qu'il en résulte, d'une part, que M. A...est fondé à demander l'annulation de l'ordonnance du 23 septembre 2011 par laquelle le président du tribunal administratif de Saint-Denis a refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 8° de l'article L. 231 du code électoral, dans leur rédaction applicable à la date de la décision attaquée, d'autre part, qu'il y a lieu, en application des dispositions de l'article 23-5 de l'ordonnance portant loi organique du 7 novembre 1958, de renvoyer au Conseil constitutionnel cette question prioritaire de constitutionnalité ; <br/>
<br/>
              5. Considérant qu'il y a lieu de surseoir à statuer sur la requête de M. A...jusqu'à ce que le Conseil constitutionnel ait tranché cette question de constitutionnalité ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Saint-Denis du 23 septembre 2011 est annulée.<br/>
Article 2 : La question de la conformité à la Constitution des dispositions du 8° de l'article L. 231 du code électoral, dans leur rédaction applicable à la date du 2 septembre 2011, est renvoyée au Conseil constitutionnel.<br/>
Article 3 : Il est sursis à statuer sur la requête de M. A...jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
Article 4 : La présente décision sera notifiée à M. B... A..., au ministre de l'intérieur et au préfet de La Réunion.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
