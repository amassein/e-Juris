<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033172378</ID>
<ANCIEN_ID>JG_L_2016_09_000000393943</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/17/23/CETATEXT000033172378.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/09/2016, 393943, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393943</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393943.20160930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nancy d'annuler la décision 48 SI du 7 février 2014 portant invalidation de son permis de conduire ainsi que plusieurs décisions antérieures portant retrait de points. Par un jugement n° 1402732 du 29 juillet 2015, le tribunal administratif a partiellement fait droit à ces conclusions. <br/>
<br/>
              Par un pourvoi, enregistré le 6 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de M.B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'infractions commises entre le 21 mars 2008 et le 4 août 2013, le ministre de l'intérieur a constaté par une décision 48 SI du 7 février 2014 la perte de validité du permis de conduire de M. B... et lui a enjoint de restituer son titre de conduite ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 29 juillet 2015 par lequel le tribunal administratif de Nancy, après avoir prononcé un non-lieu à statuer sur les conclusions de M. B... dirigées contre les décisions de retrait de points ayant fait suite aux infractions commises les 25 février 2009 et 1er janvier 2011, a annulé la décision du 7 février 2014 portant invalidation de son permis de conduire, la décision du 2 octobre 2014 portant rejet de son recours gracieux ainsi que les décisions de retraits de points afférentes aux infractions des 31 mai 2012, 24 juillet 2012 et 4 août 2013, puis enjoint au ministre de restituer sept points au capital affecté au permis de conduire de l'intéressé ; que le ministre de l'intérieur se pourvoit contre ce jugement en tant qu'il a fait droit aux conclusions de M.B... ;<br/>
<br/>
              2. Considérant qu'il incombe à l'administration, lorsqu'elle oppose une fin de non-recevoir tirée de la tardiveté de l'action introduite devant un tribunal administratif, d'établir que l'intéressé a régulièrement reçu notification de la décision ; qu'en cas de retour à l'administration du pli contenant la décision, cette preuve peut résulter soit des mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal, conformément à la réglementation en vigueur, d'un avis d'instance prévenant le destinataire de ce que le pli était à sa disposition au bureau de poste ; qu'il ressort des pièces du dossier soumis aux juges du fond qu'une telle preuve a été suffisamment apportée par la production de la copie du pli recommandé qui faisait apparaître que M. B...avait été avisé le 18 février 2014 de la présentation du pli envoyé par le fichier national du permis de conduire ; que, par suite, M. B... s'étant abstenu d'aller le retirer au bureau de poste dans le délai de 15 jours imparti pour ce faire, la notification de la décision litigieuse doit être réputée être intervenue le 18 février 2014, date de l'avis de passage ; que, dès lors, le délai de deux mois durant lequel l'intéressé pouvait saisir le juge d'une demande d'annulation, ou former devant l'administration un recours administratif lui permettant de conserver ce délai, était dépassé lorsque M. B... a saisi l'administration d'un recours gracieux le 28 août 2014 ; que, dans ces circonstances, le juge du fond a commis une erreur de droit en écartant la fin de non-recevoir du ministre au motif que la décision était produite sous la seule forme d'un pli fermé retourné à l'intéressé faute d'avoir été réclamé et qu'il n'était pas soutenu qu'elle mentionnait les voies et délais de recours ; que son jugement du 29 juillet 2015 doit, pour ce motif, être annulé en tant qu'il a fait droit aux conclusions de M. B...;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              4. Considérant qu'ainsi qu'il a été indiqué ci-dessus, les conclusions de M. B... ayant conservé un objet ont été présentées tardivement et sont, par suite, irrecevables ; qu'elles doivent, dès lors, être rejetées ; que doivent, par voie de conséquence, être également rejetées les conclusions présentées par l'intéressé à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les articles 2 à 5 du jugement du 29 juillet 2015 du tribunal administratif de Nancy sont annulés.<br/>
Article 2 : Les conclusions présentées par M. B...devant le tribunal administratif de Nancy autres que celles sur lesquelles il a été statué par l'article 1er du jugement attaqué sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
