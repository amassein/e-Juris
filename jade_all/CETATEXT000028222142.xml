<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028222142</ID>
<ANCIEN_ID>JG_L_2013_11_000000362881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/22/21/CETATEXT000028222142.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 20/11/2013, 362881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:362881.20131120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 septembre et 19 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant ... et pour la Fédération nationale CGT des travailleurs de l'Etat (FNTE-CGT) ; M. B... et la FNTE-CGT demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01061-10NT01062 du 19 juillet 2012 par lequel la cour administrative d'appel de Nantes a rejeté la requête tendant à l'annulation du jugement du 18 mars 2010 du tribunal administratif de Caen rejetant la demande de condamnation de l'Etat à verser à M. B...la somme de 19 920,26 euros en réparation des préjudices subis du fait de la discrimination syndicale dont il affirme avoir été victime dans le déroulement de sa carrière d'ouvrier d'Etat affecté au sein de l'établissement de Cherbourg de la société DCNS ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions présentées en appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2001-1276 du 28 décembre 2001 ;<br/>
<br/>
              Vu la loi n° 2008-496 du 27 mai 2008 ;<br/>
<br/>
              Vu le décret n° 82-447 du 28 mai 1982 ;<br/>
<br/>
              Vu le décret n° 2002-832 du 3 mai 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Bereyziat, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et de la FNTE-CGT ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A...B...a été recruté en 1978 en qualité d'ouvrier de l'Etat à la direction des constructions navales du ministère de la défense puis mis à disposition de l'entreprise nationale DCN, devenus DCNS ; qu'il bénéficie d'une décharge partielle d'activité de service aux fins de se consacrer à ses activités syndicales pour la FNTE-CGT ; qu'estimant avoir subi, à compter du 1er janvier 1993 et en raison de ces activités, un retard d'avancement jusqu'au 31 décembre 2001, il a demandé réparation de ce préjudice au ministre de la défense qui a rejeté sa demande par une lettre du 26 septembre 2008 ; que le 23 décembre 2008, M. B...a saisi le tribunal administratif de Caen d'une demande de condamnation de l'Etat qui a été rejetée par un jugement du 18 mars 2010 ; que le requérant et la FNTE-CGT se pourvoient contre l'arrêt du 19 juillet 2012 par lequel la cour administrative d'appel de Nantes a rejeté les conclusions dirigées contre ce jugement ; <br/>
<br/>
              2. Considérant que, de manière générale, il appartient au juge administratif, dans la conduite de la procédure inquisitoire, de demander aux parties de lui fournir tous les éléments d'appréciation de nature à établir sa conviction ; que cette responsabilité doit, dès lors qu'il est soutenu qu'une mesure a pu être empreinte de discrimination, s'exercer en tenant compte des difficultés propres à l'administration de la preuve en ce domaine et des exigences qui s'attachent aux principes à valeur constitutionnelle des droits de la défense et de l'égalité de traitement des personnes ; que, s'il appartient au requérant qui s'estime lésé par une telle mesure de soumettre au juge des éléments de fait susceptibles de faire présumer une atteinte à ce dernier principe, il incombe au défendeur de produire tous ceux permettant d'établir que la décision attaquée repose sur des éléments objectifs étrangers à toute discrimination ; que la conviction du juge, à qui il revient d'apprécier si la décision contestée devant lui a été ou non prise pour des motifs entachés de discrimination, se détermine au vu de ces échanges contradictoires ; qu'en cas de doute, il lui appartient de compléter ces échanges en ordonnant toute mesure d'instruction utile ;<br/>
<br/>
              3. Considérant que pour établir l'existence du préjudice de carrière qu'il allègue avoir subi à raison de la discrimination liée à ses activités syndicales, M. B...a procédé à une comparaison entre sa situation et la situation moyenne de 9 ouvriers recrutés entre les années 1978 à 1980 à la DCN de Cherbourg dans la même profession matriculaire et exerçant la même profession d'emploi au 31 janvier 2005 ; que, contrairement à ce qui est soutenu, c'est sans commettre d'erreur de droit que la cour, pour apprécier l'existence d'une présomption de discrimination dans le déroulement de carrière du requérant, a comparé la situation de M. B...à celle des 66 ouvriers recrutés en 1978 se trouvant dans une situation analogue à la sienne ; que c'est également sans erreur de droit que la cour a pris en compte, pour apprécier l'existence d'une présomption de discrimination, la promotion ultérieure du requérant au groupe supérieur, ainsi que celle d'autres représentants syndicaux dont le retard d'avancement était invoqué à titre de comparaison  ;<br/>
<br/>
              4. Considérant que la cour n'a pas dénaturé les pièces du dossier en estimant que l'absence de versement de certaines primes ne révélait pas de volonté de discriminer de la part de la société DCNS ou du ministère de la défense mais relevait de simples erreurs matérielles qui ont d'ailleurs été corrigées après leur signalement ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B...et la FNTE-CGT ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...B...et de la FNTE-CGT est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à la Fédération nationale CGT des travailleurs de l'Etat, à la société DCNS et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
