<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034514936</ID>
<ANCIEN_ID>JG_L_2017_04_000000390644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/51/49/CETATEXT000034514936.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 28/04/2017, 390644, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:390644.20170428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Grands Magasins Galeries Lafayette a demandé au tribunal administratif de Montreuil  de prononcer la décharge des cotisations minimales de taxe professionnelle auxquelles elle a été assujettie au titre des années 2004 et 2005 et la restitution de la cotisation qu'elle a acquittée au titre de 2006. Par des jugements n° 0801134 du 18 mai 2010 et n° 0809640 du 23 novembre 2010, le tribunal a fait droit à ces demandes.<br/>
<br/>
              Par des arrêts n° 10VE03241 et n° 11VE00278 du 3 juillet 2012, la cour administrative d'appel de Versailles a rejeté les appels formés par le ministre des finances et des comptes publics contre ces jugements.<br/>
<br/>
              Par une décision nos 362686, 362687 du 30 avril 2014 le Conseil d'Etat statuant au contentieux, saisi de deux pourvois présentés par le ministre de l'économie et des finances, a annulé ces arrêts et renvoyé les affaires à la cour.<br/>
<br/>
              Par un arrêt nos 14VE01317, 14VE01318 du 31 mars 2015, la cour administrative d'appel de Versailles, statuant à nouveau, a rejeté les appels formés par le ministre des finances et des comptes publics. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 2 juin 2015 et 15 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
                     Vu les autres pièces du dossier ;<br/>
<br/>
                     Vu : <br/>
                     - le code général des impôts et le livre des procédures fiscales ;<br/>
                     - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat SAS Grands Magasins Galeries Lafayette ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1447 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " I. La taxe professionnelle est due chaque année par les personnes physiques ou morales qui exercent à titre habituel une activité professionnelle non salariée. ". La location d'un immeuble nu par son propriétaire ne présente pas le caractère d'une activité professionnelle au sens de ces dispositions sauf dans l'hypothèse où, à travers cette location, le bailleur ne se borne pas à gérer son propre patrimoine mais poursuit, selon des modalités différentes, une exploitation commerciale antérieure ou participe à l'exploitation du locataire. Il en va de même lorsqu'un immeuble nu est donné en sous-location par une personne qui en dispose en vertu d'un contrat de crédit-bail. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, jusqu'en 2003, la SAS Grands Magasins Galeries Lafayette était propriétaire des murs et des fonds de commerce exploités sous les enseignes Galeries Lafayette et Nouvelles Galeries, à l'exception du magasin du boulevard Haussmann à Paris, et que les fonds étaient donnés en location-gérance à des sociétés du groupe Galeries Lafayette, la SAS Magasins Galeries Lafayette, la SAS Les Galeries, la Société des Grands Magasins " La Riviera ", la société des Etablissements du Capitole et la société rennaise de Grands Magasins. D'autres fonds de commerce du groupe étaient par ailleurs la propriété de la Société des Grands Magasins " La Riviera ", de la société des Etablissements du Capitole et de la société rennaise de Grands Magasins. A la suite d'une restructuration, en 2003, du groupe constitué entre ces sociétés, la SAS Grands Magasins Galeries Lafayette a absorbé, par voie de fusion, la Société des Grands Magasins " La Riviera ", la société des Etablissements du Capitole et la société rennaise de Grands Magasins et repris les fonds dont celles-ci étaient propriétaires. Puis elle a fait apport aux sociétés Magasins Galeries Lafayette et Les Galeries, qu'elle détient à 100 %, des fonds de commerce des magasins à l'enseigne Galeries Lafayette et Nouvelles Galeries. Elle est restée propriétaire et, à titre accessoire, crédit-preneur, des immeubles dans lesquels les fonds apportés sont exploités et a donné à bail ces immeubles aux sociétés Magasins Galeries Lafayette et Les Galeries. <br/>
<br/>
              3. D'une part, la cour a jugé que si les baux consentis par la SAS Grands Magasins Galeries Lafayette à ses preneurs stipulent que les locaux seront obligatoirement exploités sous l'enseigne Galeries Lafayette, que les sous-locations ne pourront être conclues qu'au bénéfice du GIE Cofinoga ou de Galeries Lafayette Voyages, membres du groupe Galeries Lafayette, que le droit au bail ne pourra être cédé qu'à l'acquéreur du fonds, que l'adhésion à l'association des commerçants est obligatoire et qu'ils traduisent des avantages consentis aux sociétés du groupe, ces seules circonstances ne permettaient pas de regarder la SAS Grands Magasins Galeries Lafayette comme participant à l'exploitation des locataires. En statuant ainsi, la cour, qui a porté sur les baux litigieux une appréciation souveraine, n'en a pas, contrairement à ce que soutient le ministre, dénaturé les clauses.<br/>
<br/>
              4. D'autre part, la cour, après avoir exposé, par des motifs non contestés de son arrêt, qu'il résultait de l'instruction que la SAS Grands Magasins Galeries Lafayette avait pour activité principale la location et, dans une moindre mesure, la sous-location en tant que crédit-preneur, des immeubles nus dont elle disposait, qu'elle donnait à bail à ses filiales détenues à 100 %, les sociétés Magasins Galeries Lafayette et Les Galeries qui détiennent et exploitent les fonds de commerce qu'elle leur a cédés lors de la restructuration et qu'elle exploitait antérieurement en location-gérance, a jugé que la seule circonstance que la dissociation, lors de la restructuration, entre les actifs fonciers et ceux de l'exploitation commerciale, n'aurait pas eu pour effet de modifier l'objet et la destination de ces actifs ni la finalité de l'activité poursuivie par la SAS Grands Magasins Galeries Lafayette ne permettait pas de regarder cette société comme poursuivant, selon des modalités différentes, l'exploitation commerciale qu'elle assurait avant la restructuration. En statuant ainsi, la cour a porté sur les faits qui lui étaient soumis, et qui ne sont pas argués de dénaturation, une appréciation souveraine non susceptible d'être discutée en cassation. <br/>
<br/>
              5. En déduisant des circonstances de fait rappelées aux points 3 et 4 ci-dessus, qu'elle a souverainement appréciés, que l'activité de location et de sous-location de locaux nus exercée par la SAS Grands Magasins Galeries Lafayette ne présentait pas le caractère d'une activité professionnelle au sens des dispositions de l'article 1447 du code général des impôts, la cour n'a pas commis d'erreur de qualification juridique des faits.<br/>
<br/>
              6. Il résulte de ce qui précède que le ministre des finances et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt attaqué. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la SAS Grands Magasins Galeries Lafayette en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à la SAS Grands Magasins Galeries Lafayette une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société Grands Magasins Galeries Lafayette.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
