<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237371</ID>
<ANCIEN_ID>JG_L_2016_10_000000384687</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/73/CETATEXT000033237371.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/10/2016, 384687, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384687</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384687.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...a demandé au tribunal administratif de Dijon de condamner la commune de Chenôve (Côte d'Or) à lui verser une indemnité correspondant à 72 mois de salaire en réparation des préjudices subis à la suite de faits de harcèlement moral. Par un jugement n° 1202149 du 13 juin 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13LY02301 du 22 juillet 2014, la cour administrative d'appel de Lyon a rejeté l'appel de Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et deux autres mémoires, enregistrés les 23 septembre et 19 décembre 2014 et le 2 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Chenôve une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de Mme C...B...et à la SCP Rocheteau, Uzan-Sarano, avocat de la commune de Chenôve ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme C...B..., éducatrice chargée d'activités physiques et sportives, a demandé le 6 juillet 2012 à la commune de Chenôve de lui verser une indemnité correspondant à soixante-douze mois de salaire en réparation des préjudices qu'elle prétend avoir subis en raison de faits de harcèlement moral ; que, par une décision du 2 août 2012, le maire de cette commune a rejeté sa demande ; que par un jugement du 13 juin 2013, le tribunal administratif de Dijon a rejeté la demande de Mme B...tendant à la condamnation de la commune de Chenôve à lui verser cette indemnité ; que Mme B...se pourvoit en cassation contre l'arrêt du 22 juillet 2014 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'elle a formé contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 6 quinquies de la loi du 13 juillet 1983 susvisée : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel. / Aucune mesure concernant notamment le recrutement, la titularisation, la formation, la notation, la discipline, la promotion, l'affectation et la mutation ne peut être prise à l'égard d'un fonctionnaire en prenant en considération :/ 1° Le fait qu'il ait subi ou refusé de subir les agissements de harcèlement moral visés au premier alinéa ;/ 2° Le fait qu'il ait exercé un recours auprès d'un supérieur hiérarchique ou engagé une action en justice visant à faire cesser ces agissements (...). Les dispositions du présent article sont applicables aux agents non titulaires de droit public " ;<br/>
<br/>
              3. Considérant qu'il appartient à un agent public qui soutient avoir été victime d'agissements constitutifs de harcèlement moral, de soumettre au juge des éléments de fait susceptibles de faire présumer l'existence d'un tel harcèlement ; qu'il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement ; que la conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au vu de ces échanges contradictoires, qu'il peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a été affectée une première fois au service du développement durable de la commune de Chenôve de mai 2009 à février 2010 et que son supérieur hiérarchique direct était M.A... ; qu'il ressort des éléments de produits par la requérante, notamment des témoignages circonstanciés d'agents ayant travaillé sous l'autorité de M. A...et des constatations opérées à l'occasion d'une enquête administrative interne diligentée par la commune de Chenôve, que M. A... a fait preuve d'un comportement vindicatif et humiliant à l'égard de l'intéressée, se traduisant par de fréquents propos dévalorisants ainsi que des critiques répétées sur la qualité de son travail, alors pourtant qu'il n'exerçait pas de manière adéquate ses fonctions d'encadrement, soit en s'abstenant de lui donner des consignes pour l'exécution du service, soit en alternant sans justification les ordres et les contre-ordres ; qu'après sa réaffectation en octobre 2010 au sein du service du développement durable de la commune, M.A..., qui n'était plus alors son supérieur direct, a de nouveau manifesté à l'égard de Mme B...un comportement vindicatif et humiliant ; que le maire n'a adressé à M. A...une lettre lui enjoignant de modifier son attitude que le 20 juin 2011 ; que les éléments ainsi produits par Mme B...étaient susceptibles de faire présumer l'existence d'agissements constitutifs de harcèlement moral ; que la commune de Chenôve n'a, en revanche, pas produit d'éléments permettant de retenir que les agissements en cause étaient justifiés par des considérations étrangères à tout harcèlement moral ; que si la commune soutient que les agissements imputés à M. A... se sont produits sur une période de temps relativement brève, cette circonstance ne fait pas obstacle à ce qu'ils soient qualifiés d'agissements constitutifs de harcèlement moral ; qu'il résulte de ce qui précède qu'en retenant l'absence de harcèlement moral, la cour administrative d'appel a donné aux faits qu'elle a souverainement appréciés une qualification juridique erronée ;<br/>
<br/>
              5. Considérant, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme B...est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Chenôve le versement à Mme B...d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'il soit fait droit à la demande présentée au même titre par la commune de Chenôve, Mme B...n'étant pas la partie perdante dans la présente instance ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 22 juillet 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La commune de Chenôve versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Chenôve au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme C...B...et à la commune de Chenôve.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
