<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806161</ID>
<ANCIEN_ID>JG_L_2021_12_000000439424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 30/12/2021, 439424, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SAS CABINET BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439424.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F... B..., M. D... A..., Mme G... B... et M. C... E... ont demandé au tribunal administratif de Bastia, à titre principal, d'annuler pour excès de pouvoir l'arrêté du préfet de la Corse-du-Sud du 24 décembre 2015 déclarant d'utilité publique les travaux de prélèvement et de dérivation des eaux au captage des sources de Ghjuvan Marcu et de Casale 1 et 2, instaurant des périmètres de protection et autorisant l'utilisation de l'eau en vue de la consommation humaine sur le territoire de la communauté de communes du Sartenais Valinco, à titre subsidiaire, d'annuler pour excès de pouvoir l'article 4-2 de cet arrêté instaurant un périmètre de protection rapprochée. Par un jugement n° 1600308 du 15 février 2018, le tribunal administratif a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 18MA01492 du 14 janvier 2020, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... et autres contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire ampliatif et un mémoire en réplique, enregistrés les 10 mars 2020, 21 août 2020 et 7 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la communauté de communes du Sartenais Valinco la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 62 ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2019-774 du 24 juillet 2019 ;<br/>
              - la décision du 19 novembre 2020 par laquelle le Conseil d'Etat, statuant au contentieux, d'une part, n'a pas renvoyé au Conseil constitutionnel la première question prioritaire de constitutionnalité soulevée par Mme B... et autres, d'autre part, lui a renvoyé la seconde question prioritaire de constitutionnalité soulevée par ces derniers ;<br/>
              - la décision du Conseil constitutionnel n° 2020-883 du 12 février 2021 statuant sur la seconde question prioritaire de constitutionnalité soulevée par Mme B... et autres ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Boulloche, avocat de Mme B... et autres ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... et autres ont demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir l'arrêté du préfet de la Corse-du-Sud du 24 décembre 2015 déclarant d'utilité publique les travaux de prélèvement et de dérivation des eaux au captage des sources de Ghjuvan Marcu et de Casale 1 et 2, instaurant des périmètres de protection et autorisant l'utilisation de l'eau en vue de la consommation humaine sur le territoire de la communauté de communes du Sartenais Valinco. Par un arrêt du 14 janvier 2020, la cour administrative d'appel de Marseille a rejeté leur appel formé contre le jugement du tribunal administratif de Bastia du 15 février 2018 rejetant leur demande. Eu égard aux moyens qu'il soulève, le pourvoi de Mme B... et autres doit être regardé comme ne tendant à l'annulation de cet arrêt qu'en tant qu'il juge qu'ils ne sont pas fondés à contester la partie du jugement attaqué relative à l'instauration d'un périmètre de protection rapprochée.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 1321-2 du code de la santé publique : " En vue d'assurer la protection de la qualité des eaux, l'acte portant déclaration d'utilité publique des travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines mentionné à l'article L. 215-13 du code de l'environnement détermine autour du point de prélèvement un périmètre de protection immédiate dont les terrains sont à acquérir en pleine propriété, un périmètre de protection rapprochée à l'intérieur duquel peuvent être interdits ou réglementés toutes sortes d'installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols de nature à nuire directement ou indirectement à la qualité des eaux et, le cas échéant, un périmètre de protection éloignée à l'intérieur duquel peuvent être réglementés les installations, travaux, activités, dépôts, ouvrages, aménagement ou occupation des sols et dépôts ci-dessus mentionnés. " Le deuxième alinéa de cet article, dans sa version en vigueur à la date d'édiction de l'arrêté contesté, prévoit toutefois que " Lorsque les conditions hydrologiques et hydrogéologiques permettent d'assurer efficacement la préservation de la qualité de l'eau par des mesures de protection limitées au voisinage immédiat du captage, l'acte portant déclaration d'utilité publique peut n'instaurer qu'un périmètre de protection immédiate. ". Le troisième alinéa, devenu aujourd'hui le cinquième alinéa, dispose que : " Lorsque des terrains situés dans un périmètre de protection immédiate appartiennent à une collectivité publique, il peut être dérogé à l'obligation d'acquérir les terrains visée au premier alinéa par l'établissement d'une convention de gestion entre la ou les collectivités publiques propriétaires et l'établissement public de coopération intercommunale ou la collectivité publique responsable du captage ".<br/>
<br/>
              3. Le paragraphe III de l'article 61 de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé a notamment modifié le deuxième alinéa de l'article L. 1321-2 du code de la santé publique et ajouté un troisième alinéa nouveau pour prévoir qu'il y a lieu d'instaurer un simple périmètre de protection immédiate lorsque les conditions hydrologiques et hydrogéologiques permettent d'assurer efficacement la préservation de la qualité de l'eau par des mesures de protection limitées au voisinage immédiat du captage ainsi qu'en cas de captages d'eau d'origine souterraine dont le débit exploité est inférieur, en moyenne annuelle, à 100 mètres cubes par jour. Si le paragraphe IX du même article 61 a précisé que ces nouvelles dispositions ne s'appliquaient pas aux captages d'eau pour lesquels un arrêté d'ouverture d'une enquête publique relative à l'instauration d'un périmètre de protection avait été publié à la date de publication de la loi, le Conseil constitutionnel, par sa décision n° 2020-883 QPC du 12 février 2021 statuant sur la question prioritaire de constitutionnalité soulevée par les requérants, a déclaré inconstitutionnel ce paragraphe à compter de la publication de sa décision, applicable à toutes les affaires non jugées définitivement à cette date, au motif que, en faisant dépendre l'application du nouveau régime institué par le paragraphe III de l'existence ou non d'un arrêté d'ouverture d'une enquête publique relative à l'instauration d'un périmètre de protection à la date de publication de la loi, le législateur avait établi entre les propriétaires de terrains situés à proximité de captages d'eau une différence de traitement méconnaissant le principe d'égalité.<br/>
<br/>
              4. Il résulte de ce qui précède que si, eu égard aux effets de la décision du Conseil constitutionnel du 12 février 2021, les nouvelles dispositions de l'article L. 1321-2 du code de la santé publique issues de la loi du 24 juillet 2019 s'appliquent à tous les actes portant déclaration d'utilité publique de travaux de prélèvement d'eau destinée à l'alimentation des collectivités humaines pris postérieurement à l'entrée en vigueur de cette loi, ces dispositions ne s'appliquent pas aux actes pris antérieurement à l'entrée en vigueur de la loi, contrairement à ce que soutiennent les requérants. <br/>
<br/>
              5. Toutefois, en jugeant qu'il résultait des dispositions de l'article L. 1321-2 du code de la santé publique, dans leur version alors applicable citée au point 2, que la mise en place d'un périmètre de protection rapprochée s'imposait légalement à la collectivité territoriale, alors que les dispositions du deuxième alinéa alors en vigueur, dont les requérants se prévalaient devant elle, ouvraient la possibilité, sous certaines conditions, de n'instaurer qu'un périmètre de protection immédiate, la cour administrative d'appel a commis une erreur de droit. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, les requérants sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il juge qu'ils ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Bastia a rejeté leurs conclusions tendant à l'annulation de l'arrêté du préfet de la Corse-du-Sud du 24 décembre 2015 en tant qu'il instaure un périmètre de protection rapprochée.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la communauté de communes du Sartenais Valinco les sommes de 2 000 euros chacun à verser à Mme B... et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 14 janvier 2020 de la cour administrative d'appel de Marseille est annulé en tant qu'il rejette les conclusions de Mme B... et autres dirigées contre le jugement du tribunal administratif de Bastia du 15 février 2018 en tant qu'il rejette leurs conclusions tendant à l'annulation de l'arrêté du préfet de la Corse-du-Sud du 24 décembre 2015 en tant qu'il instaure un périmètre de protection rapprochée.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille. <br/>
Article 3 : L'Etat et la communauté de communes du Sartenais Valinco verseront chacun à Mme B... et autres une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme F... B..., première requérante dénommée, au ministre des solidarités et de la santé et à la communauté de communes du Sartenais Valinco.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
