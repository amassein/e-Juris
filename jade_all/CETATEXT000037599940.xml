<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599940</ID>
<ANCIEN_ID>JG_L_2018_10_000000424974</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599940.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 31/10/2018, 424974, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424974</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:424974.20181031</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au préfet du Nord de procéder à l'enregistrement de sa demande d'asile et de lui délivrer une attestation de demande d'asile dans un délai de huit jours à compter de l'ordonnance à intervenir, sous astreinte de 155 euros par jour de retard, et d'interrompre la procédure de transfert vers l'Italie, à titre subsidiaire, de suspendre l'exécution de l'arrêté du<br/>
21 mars 2018 portant son transfert vers l'Italie. Par une ordonnance n° 1809158 du 12 octobre 2018, le juge des référés du tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 19 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'interrompre ou, à titre subsidiaire, de suspendre l'exécution de la mesure de transfert vers l'Italie ;<br/>
<br/>
              3°) d'ordonner à l'administration de procéder à l'enregistrement de sa demande d'asile et de lui délivrer une attestation de demande d'asile au plus tard dans un délai de huit jours à compter de l'ordonnance à venir, sous astreinte de 155 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à son conseil, en contrepartie de sa renonciation à percevoir la somme correspondant à la part contributive de l'Etat, en application de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision de transfert le concernant est susceptible de faire l'objet à tout moment d'une mesure d'exécution forcée et alors que sa situation de santé s'est dégradée postérieurement à l'édiction de cette décision ;<br/>
              - l'exécution de l'arrêté de transfert du 21 mars 2018 porte une atteinte grave et manifestement illégale au droit d'asile et au droit de recevoir des soins appropriés prévus par l'article 4 de la charte des droits fondamentaux et l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'arrêté de transfert du 21 mars 2018 emporte des effets qui excèdent ceux qui s'attachent normalement à une telle exécution en raison de l'aggravation de son état de santé qui ressort de ses certificats médicaux et des ordonnances rendues par le juge des libertés et de la détention et de la chambre des libertés ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile : " I. - L'étranger qui a fait l'objet d'une décision de transfert mentionnée à l'article L. 742-3 peut, dans le délai de sept jours à compter de la notification de cette décision, en demander l'annulation au président du tribunal administratif./ Le président ou le magistrat qu'il désigne à cette fin parmi les membres de sa juridiction ou les magistrats honoraires inscrits sur la liste mentionnée à l'article L. 222-2-1 du code de justice administrative statue dans un délai de quinze jours à compter de sa saisine./ Aucun autre recours ne peut être introduit contre la décision de transfert./ L'étranger peut demander au président du tribunal ou au magistrat désigné par lui le concours d'un interprète. L'étranger est assisté de son conseil, s'il en a un. Il peut demander au président du tribunal administratif ou au magistrat désigné à cette fin qu'il lui en soit désigné un d'office./ L'audience est publique. Elle se déroule sans conclusions du rapporteur public, en présence de l'intéressé, sauf si celui-ci, dûment convoqué, ne se présente pas " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le législateur a entendu organiser une procédure spéciale applicable au cas où un étranger fait l'objet d'une décision de transfert vers un autre Etat responsable de l'examen de sa demande d'asile ; que la procédure spéciale prévue par le code de l'entrée et du séjour des étrangers et du droit d'asile présente des garanties au moins équivalentes à celles des procédures régies par le livre V du code de justice administrative, eu égard aux pouvoirs confiés au juge par ces dispositions, aux délais qui lui sont impartis pour se prononcer, et aux conditions de son intervention ; qu'elle est dès lors exclusive de ces procédures ; qu'il n'en va autrement que dans le cas où les modalités selon lesquelles il est procédé à l'exécution d'une décision de transfert emportent des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi sur le fondement de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, a statué ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à l'exécution d'une telle décision ;<br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que M.A..., ressortissant guinéen né le 15 avril 1987, a formulé une demande d'asile auprès des services de la préfecture du Nord le 20 février 2018 ; qu'après avoir constaté que l'intéressé avait demandé l'asile auprès des autorités italiennes en juin 2016, le préfet du Nord a, par arrêté du 21 mars 2018, ordonné son transfert aux autorités italiennes et l'a assigné à résidence ; que par un jugement du 26 avril 2018, le magistrat désigné par le président du tribunal administratif de Lille a rejeté les conclusions présentées contre cet arrêté ; que M. A...relève appel de l'ordonnance du 12 octobre 2018 par laquelle le juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, à titre principal, à ce qu'il soit fait injonction au préfet du Nord de procéder à l'enregistrement de sa demande d'asile et de lui délivrer une attestation de demande d'asile dans un délai de huit jours, sous astreinte de 155 euros par jour de retard, et d'interrompre la procédure de transfert vers l'Italie et, à titre subsidiaire, à ce qu'il prononce la suspension de l'exécution de l'arrêté du 21 mars 2018.<br/>
<br/>
              5. Considérant que M. A...fait état de très sévères troubles psychologiques, connus des services préfectoraux à la date de la mesure de transfert et pour lesquels il est pris en charge depuis le mois de février 2018 ; que les certificats médicaux qu'il produit, datés des 22 et 23 mai et du 9 octobre 2018, s'ils attestent notamment d'un syndrome post-traumatique et d'un nécessaire suivi médical de l'intéressé ne caractérisent pas, ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Lille une aggravation de son état de santé depuis l'intervention de l'arrêté de transfert et du jugement du 26 avril revêtant le caractère d'un changement de circonstance justifiant que l'exécution de la mesure de remise aux autorités italiennes soit suspendue ; qu'il en va de même de l'intervention de l'ordonnance du juge des libertés et de la détention du tribunal de grande instance de Lille du 5 octobre 2018, confirmée le 8 octobre 2018 par la Cour d'appel de Douai, constatant l'irrégularité de son placement en rétention le 3 octobre 2018 au motif que n'ayant pas été précédée d'une évaluation individuelle de l'état de vulnérabilité de M.A..., la mesure de rétention n'avait pas été suffisamment motivée au regard des exigences légales ; que ni en elles-mêmes ni par leur motivation, ces ordonnances ne peuvent être regardées comme un changement dans les circonstances de droit ou de fait  ;          <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lille a rejeté sa demande ; que, par suite, sa requête, y compris les conclusions tendant à l'application  des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, doit être rejetée ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
