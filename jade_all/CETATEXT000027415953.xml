<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027415953</ID>
<ANCIEN_ID>JG_L_2013_05_000000361492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/59/CETATEXT000027415953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 17/05/2013, 361492, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BOUZIDI, BOUHANNA ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Eliane Chemla</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361492.20130517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 31 juillet et 2 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SCI Gutenberg Aressy, dont le siège est ZAC de la Marinière, BP 13 à Bondoufle (91071), et la société laboratoire Cattier, dont le siège est 31 avenue de la Sibelle à Paris (75014) ; elles demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1100309 du 26 juin 2012 par lequel, statuant sur la demande de la société Groupe Durieu faisant suite à la question préjudicielle posée par le juge de la mise en état du tribunal de grande instance d'Evry avant de statuer sur le litige dont elles l'avaient saisi, le tribunal administratif de Versailles a jugé qu'à la date de la délibération du 22 octobre 2007 par laquelle le conseil de la communauté d'agglomération Evry-Centre-Essonne a approuvé la cession à la société Rieu-Investissement de la parcelle AI n° 220  située sur le territoire de la commune de Bondoufle, cette parcelle n'appartenait pas au domaine public routier de la communauté d'agglomération au sens de l'article L. 112-8 du code de la voirie routière ; <br/>
<br/>
              2°) de faire droit à leurs conclusions présentées devant le tribunal administratif ; <br/>
<br/>
              3°) de mettre à la charge de la société Groupe Durieu et de la communauté d'agglomération Evry-Centre-Essonne le versement d'une somme de 3 000 euros chacune sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré enregistrée le 22 avril 2013, présentée pour la SCI Gutenberg Aressy et la société Laboratoire Cattier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 avril 2013, présentée pour la Communauté d'agglomération Evry-Centre-Essonne ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de la voirie routière ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Eliane Chemla, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la SCI Gutenberg Aressy et de la société Laboratoire Cattier et à la SCP Lyon-Caen, Thiriez, avocat de la société Communauté d'agglomération Evry-centre-Essonne ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'afin de permettre l'extension d'un réseau de transports en commun en site propre, dont la réalisation avait été envisagée lors de la création de la ville nouvelle d'Evry, des réserves foncières ont été constituées dans plusieurs zones d'aménagement concerté de l'agglomération nouvelle ; qu'à ce titre, la parcelle cadastrée AI n° 220, située dans la zone d'aménagement concerté de la Marinière, créée en 1973, sur le territoire de la commune de Bondoufle, a été inscrite en emplacement réservé ; que cette parcelle est bordée par un terrain appartenant à la société Durieu, un terrain appartenant à la société Gutenberg Aressy et la société Laboratoires Cattier et par une voie publique ; que l'itinéraire initialement envisagé ayant été abandonné, le conseil de la communauté d'agglomération Evry-Courcourones-Bondoufles-Lisses a décidé, par délibération du 30 juin 2003, de supprimer l'emprise foncière réservée à l'extension de l'infrastructure de transport en site propre dans cette zone d'activités et a précisé que les terrains lui appartenant et qui étaient ainsi libérés "  pourraient être cédés aux industriels de la zone d'activités de la Marinière selon des modalités à définir "  ; que, par délibération du 22 octobre 2007, le conseil de la communauté d'agglomération Evry Centre-Essonne, qui avait succédé à la communauté d'agglomération Evry-Courcourones-Bondoufles-Lisses, a approuvé la cession de la parcelle AI 220 à la société Rieu-Investissement, société mère de la société Durieu ;  que la société Gutenberg Aressy et la société Laboratoires Cattier ont fait assigner les 8 et 9 octobre 2009 la communauté d'agglomération et la société Durieu devant le tribunal de grande instance d'Evry afin, notamment, de constater que " la vente des délaissés de voirie constitués par les parcelles formant l'emprise initialement destinée au transport en commun du parc d'activité de la Marinière à Bondoufle était exercée en violation de l'article L. 112-8 du code de la voirie routière " et que la décision du conseil de la communauté d'agglomération de vendre la totalité de cette parcelle à la société Rieu-Investissement sans avoir préalablement mis en demeure la société Gutenberg Aressy, en sa qualité de propriétaire riverain, d'exercer son droit de priorité constituait " une dépossession attentatoire aux droits fondamentaux attachés à la propriété privée " et d'annuler cette décision constitutive d'une voie de fait ainsi que la promesse de vente et la vente intervenue par acte notarié le 22 septembre 2009 ; que, par ordonnance du 21 octobre 2010, le juge de la mise en état du  tribunal a sursis à statuer sur les demandes relevant de la compétence du tribunal de grande instance dans l'attente de la réponse à la question préjudicielle devant être soumise au juge administratif et portant sur le point de savoir si la parcelle AI n° 220 appartenait, à la date de la délibération du 22 octobre 2007, au domaine public routier de la communauté d'agglomération Evry Centre-Essonne, au sens de l'article L. 112-8 du code de la voirie routière ; que la société Gutenberg Aressy et la société Laboratoires Cattier font appel du jugement du 26 juin 2012 par lequel le tribunal administratif de Versailles, saisi de cette question par la société Durieu, y a apporté une réponse négative ;<br/>
<br/>
              2. Considérant, en premier lieu, que le tribunal administratif, qui n'était pas tenu de répondre à chaque élément de l'argumentation des sociétés requérantes, a suffisamment motivé son jugement en indiquant dans ses motifs les raisons pour lesquelles il estimait que la parcelle litigieuse n'appartenait pas au domaine public routier de la communauté d'agglomération ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes de l'article L. 111-1 du code de la voirie routière : " Le domaine public routier comprend l'ensemble des biens du domaine public de l'Etat, des départements et des communes affectés aux besoins de la circulation terrestre, à l'exception des voies ferrées " ; qu'aux termes de l'article L. 112-8 du même code : " Les propriétaires riverains des voies du domaine public routier ont une priorité pour l'acquisition des parcelles situées au droit de leur propriété et déclassées par suite d'un changement de tracé de ces voies ou de l'ouverture d'une voie nouvelle. Le prix de cession est estimé, à défaut d'accord amiable, comme en matière d'expropriation. / Si, mis en demeure d'acquérir ces parcelles, ils ne se portent pas acquéreurs dans un délai d'un mois, il est procédé à l'aliénation de ces parcelles suivant les règles applicables au domaine concerné. /... " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que la bande de terrain à laquelle appartient la parcelle cadastrée AI n° 220 et dont la propriété avait été transférée par acte du 17 décembre 1990 du syndicat d'agglomération nouvelle d'Evry à la communauté d'agglomération Evry-Courcourones-Bondoufles-Lisses avait été incluse dans la réserve foncière constituée en vue de l'extension alors envisagée d'une infrastructure de transport public en site propre et inscrit en emplacement réservé dans la zone d'activité de la Marinière à Bondoufle ; qu'ainsi qu'il a été dit ci-dessus, cet emplacement réservé a été supprimé par délibération du 30 juin 2003 du conseil de la communauté d'agglomération Evry-Courcourones-Bondoufles-Lisses, à la suite de l'abandon de ce projet ;<br/>
<br/>
              5. Considérant, d'une part, que lorsque, avant l'entrée en vigueur, le 1er juillet 2006, du code général de la propriété des personnes publiques dont l'article L. 2211-1, prévoit que les réserves foncières relèvent du domaine privé des personnes publiques, un terrain a été acquis par une personne publique en vue de la constitution d'une réserve foncière, il n'a pas été, de ce seul fait et dès ce moment, soumis aux principes de la domanialité publique, alors même qu'il a été précisé que cette réserve était justifiée par la réalisation envisagée d'un aménagement d'une infrastructure de transport public en site propre ;<br/>
<br/>
              6. Considérant, d'autre part, que la " surélévation constituée par une butte de terre massive " alléguée n'a pas eu pour conséquence l'affectation de la parcelle à l'usage du public et n'a pas constitué un aménagement spécial en vue d'un service public ; que la circonstance, à la supposer établie, que la procédure prévue par l'article L. 112-8 du code de la voirie routière aurait été suivie pour d'autres parcelles faisant auparavant partie de la même réserve foncière ne peut être utilement alléguée pour établir l'appartenance de la parcelle en litige au domaine public routier de la personne publique ; que la parcelle AI n° 220, dont il n'est au demeurant pas soutenu qu'elle aurait été classée dans la voirie de la communauté d'agglomération  et qui n'a jamais été ouverte à la circulation, n'appartenait, à la date de la délibération du 22 octobre 2007, ni au domaine public ni a fortiori au domaine public routier de la communauté d'agglomération Evry Centre-Essonne ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la parcelle AI n° 220 n'a pu constituer une parcelle déclassée au sens de l'article L. 112-8 du code de la voirie routière ; que, dès lors, la société Gutenberg Aressy et la société Laboratoires Cattier ne sont pas fondées à demander l'annulation du jugement du 26 juin 2012 du tribunal administratif de Versailles ;<br/>
<br/>
              Sur les conclusions fondées sur l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la communauté d'agglomération Evry Centre-Essonne et la société Groupe Durieu, qui ne sont pas la partie perdante dans la présente instance, versent aux sociétés requérantes la somme qu'elles demandent au titre des frais exposés par elles et non compris dans les dépens ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la société Gutenberg Aressy, d'une part, et de la société Laboratoires Cattier, d'autre part, la somme de 2 500 euros à verser tant à la communauté d'agglomération Evry Centre-Essonne qu'à la société Groupe Durieu, au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société Gutenberg Aressy et de la société Laboratoires Cattier est rejetée.<br/>
<br/>
Article 2 : La société Gutenberg Aressy et la société Laboratoires Cattier verseront chacune une somme de 2 500 euros, d'une part, à la communauté d'agglomération Evry Centre-Essonne et, d'autre part, à la société Groupe Durieu au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Gutenberg Aressy, à la société Laboratoires Cattier, à la société Groupe Durieu et à la communauté d'agglomération Evry Centre-Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
