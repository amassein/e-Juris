<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026738934</ID>
<ANCIEN_ID>JG_L_2012_12_000000330782</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/73/89/CETATEXT000026738934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 07/12/2012, 330782, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330782</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:330782.20121207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 août et 13 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL Gestion Camping Caravaning, dont le siège est 44 route de la Corniche, PN 44, à Boulouris (83700), représentée par son gérant en exercice ; la SARL Gestion Camping Caravaning demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06MA00313 et n° 06MA00940 du 16 juin 2009 par lequel la cour administrative d'appel de Marseille, après avoir, sur l'appel du ministre de l'économie, des finances et de l'industrie, annulé l'article 1er du jugement du tribunal administratif de Nice du 3 novembre 2005 et remis à sa charge l'amende fondée sur les dispositions de l'article 1763 A du code général des impôts, a rejeté sa requête tendant à l'annulation de ce jugement en tant qu'il avait rejeté ses conclusions tendant à la décharge, en droits et pénalités, des cotisations d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos les 30 juin des années 1994 à 1996, de la contribution additionnelle de 10 % sur l'impôt sur les sociétés à laquelle elle a été assujettie au titre des mêmes exercices, de l'imposition forfaitaire annuelle au titre des années 1995 à 1996 et de la taxe d'apprentissage de l'année 1995 ainsi que des rappels de taxe sur la valeur ajoutée réclamés pour la période du 1er juillet 1993 au 30 juin 1996 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel du ministre de l'économie, des finances et de l'industrie ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Laugier, Caston, avocat de la SARL Gestion Camping Caravaning,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Laugier, Caston, avocat de la SARL Gestion Camping Caravaning ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL Gestion Camping Caravaning, qui exploite un terrain de camping situé dans le Var, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er juillet 1993 au 30 juin 1996 à l'issue de laquelle des compléments d'impôt sur les sociétés et des rappels de taxe sur la valeur ajoutée et de taxe d'apprentissage ont notamment été mis à sa charge ; qu'elle a, en outre, été assujettie à la pénalité alors prévue à l'article 1763 A du code général des impôts ; que, par jugement du 3 novembre 2005, le tribunal administratif de Nice a prononcé la décharge de cette pénalité mais a rejeté le surplus des conclusions de la société tendant à la décharge des impositions et autres pénalités mises à sa charge ; qu'elle se pourvoit en cassation contre l'arrêt du 16 juin 2009 par lequel la cour administrative d'appel de Marseille, après avoir annulé, sur appel du ministre de l'économie, des finances et de l'industrie, l'article 1er de ce jugement et remis à la charge de la société la pénalité de l'article 1763 A du code général des impôts, a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Marseille en tant qu'il lui était défavorable ;<br/>
<br/>
              Sur l'étendue du litige :<br/>
<br/>
              2. Considérant que, par décision du 16 février 2012, postérieure à l'introduction du pourvoi, l'administration a prononcé le dégrèvement de la somme de 68 490 euros correspondant à la totalité des rappels de taxe sur la valeur ajoutée et de taxe d'apprentissage, et des pénalités correspondantes, mis en recouvrement auprès de la SARL Gestion Camping Caravaning par avis de mise en recouvrement du 12 novembre 1998 ; que les conclusions du pourvoi sont, dans cette mesure, devenues sans objet ; qu'il n'y a pas lieu, par suite, d'y statuer ;<br/>
<br/>
              Sur le surplus des conclusions du pourvoi :<br/>
<br/>
              - En ce qui concerne le redressement fondé sur un acte anormal de gestion :<br/>
<br/>
              3. Considérant que la cour a relevé que l'administration soutenait, sans être contredite, que la SARL Gestion Camping Caravaning avait loué à l'Association Caravaning Camping Club des emplacements de camping à un prix inférieur à celui pratiqué à l'égard des autres clients dont le montant avait été fixé, non pas, comme le prévoyait la convention de location, par la SARL, mais par l'association elle-même, à l'occasion de ses assemblées générales et en fonction de ses disponibilités ; qu'elle a, par ailleurs, estimé que si la SARL Gestion Camping Caravaning soutenait que ce tarif préférentiel avait été consenti à l'association dans le but d'assurer la location tout au long de l'année d'emplacements dont les caractéristiques physiques étaient plus défavorables que celles des emplacements loués aux particuliers, elle n'en justifiait pas ; qu'en se fondant sur ces éléments de fait, qu'elle a souverainement appréciés, pour en déduire que l'administration devait être regardée comme rapportant la preuve de ce que l'avantage tarifaire accordé à l'association était constitutif d'un acte anormal de gestion, la cour n'a pas commis d'erreur de droit et n'a pas inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              - En ce qui concerne la pénalité mise à la charge de la société en application de l'article 1763 A du code général des impôts :<br/>
<br/>
              4. Considérant qu'aux termes du 1 de l'article 109 du code général des impôts : " Sont considérés comme revenus distribués : 1° Tous les bénéfices  et produits qui ne sont pas mis en réserve ou incorporés au capital ... " ; que selon l'article 117 du même code, dans sa rédaction applicable à la présente procédure : " Au cas où la masse des revenus distribués excède le montant total des distributions tel qu'il résulte des déclarations de la personne morale visées à l'article 116, celle-ci est invitée à fournir à l'administration, dans un délai de trente jours, toutes indications complémentaires sur les bénéficiaires de l'excédent de distribution. / En cas de refus ou à défaut de réponse dans ce délai, les sommes correspondantes donnent lieu à l'application de la pénalité prévue à l'article 1763 A " ;  qu'aux termes de ce dernier article, désormais abrogé : " Les sociétés et les autres personnes morales passibles de l'impôt sur les sociétés qui versent ou distribuent, directement ou par l'intermédiaire de tiers, des revenus à des personnes dont, contrairement aux dispositions des articles 117 et 240, elles ne révèlent pas l'identité sont soumises à une pénalité égale à 100 p. 100 des sommes versées ou distribuées. (...)./ Les dirigeants sociaux (...)  ainsi que les dirigeants de fait gestionnaires de la société (...) sont solidairement responsables du paiement de cette pénalité qui est établie et recouvrée comme en matière d'impôt sur le revenu " ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort du texte de l'arrêt attaqué, tel qu'il figure sur sa minute, que, statuant sur la motivation, contenue dans la notification de redressement adressée à la SARL Gestion Camping Caravaning, relative à la pénalité prévue par les dispositions précitées de l'ancien article 1763 A du code général des impôts, la cour a jugé, d'une part, que le service n'était tenu de motiver cette pénalité qu'à l'égard de la société et non des personnes solidairement responsables et, d'autre part, qu'il appartenait à la société risquant de se voir infliger une telle amende de respecter ses obligations légales et d'indiquer à l'administration l'identité des bénéficiaires, sans qu'elle puisse apprécier s'il y avait lieu ou non de le faire ; qu'elle en a déduit que la SARL Gestion Camping Caravaning ne pouvait utilement soutenir qu'en s'abstenant de mentionner, dans la notification de redressements, les dispositions du deuxième alinéa de l'article 1763 A du code général des impôts aux termes desquels les dirigeants sociaux sont solidairement responsables du paiement de la pénalité prévue par cet article, le vérificateur aurait insuffisamment motivé ce document et méconnu le principe des droits de la défense ; que ces motifs de l'arrêt, par lesquels la cour a écarté le moyen tiré de l'insuffisante motivation de la pénalité, ne sont pas, contrairement à ce que soutenait initialement la société requérante à l'appui de son pourvoi, entachés d'une contradiction avec l'article 2 du dispositif, qui remet la pénalité de l'article 1763 A à sa charge ; que, par ailleurs, la cour n'a, en statuant comme elle l'a fait, ni insuffisamment motivé sa décision ni commis d'erreur de droit ; enfin, que la société requérante ne peut utilement soutenir, par un moyen qui est nouveau en cassation, que la procédure prévue à l'article 117 du code général des impôts ne pouvait être régulièrement mise en oeuvre dès la notification de redressement et qu'en se fondant sur les mentions de ce document, la cour aurait commis une erreur de droit ;<br/>
<br/>
              6. Considérant, en second lieu, qu'après avoir cité les dispositions du 1° du 1. de l'article 109 du code général des impôts et celles de l'article 110 du même code, la cour a exposé qu'il résultait de la combinaison de ces dispositions que les revenus distribués sont notamment ceux qui, d'une part, ont été imposés à l'impôt sur les sociétés par une décision devenue définitive et, d'autre part, n'ont été ni mis en réserve ni incorporés au capital et que les bénéfices ainsi visés s'entendent après application, le cas échéant, des redressements qui ont pu être apportés à la suite d'une vérification, aux bénéfices déclarés et qui ont donné lieu à l'établissement d'une cotisation d'impôt sur le revenu ; qu'elle a relevé qu'en l'espèce, les sommes regardées par l'administration comme des revenus distribués correspondaient à des sommes réintégrées aux résultats et retenus pour l'assiette de l'impôt sur les sociétés ; qu'elle a ensuite précisé que le 1° du 1. de l'article 109 du code général des impôts établissait une présomption de distribution à l'égard de tous les bénéfices qui ne sont pas investis dans l'entreprise ; qu'elle a relevé, enfin, que, pour écarter cette présomption, la SARL Gestion Camping Caravaning se bornait, sans en justifier, à alléguer que les sommes qualifiées par le service de revenus distribués, constituées d'une charge comptabilisée deux fois par erreur, de sommes relatives à l'omission de la comptabilisation de la taxe sur la valeur ajoutée facturée aux clients et payée par la société au Trésor public ou encore des sommes relatives à l'omission sur facturations établies, étaient restées investies dans l'entreprise ; qu'elle en a déduit que le tribunal administratif de Nice avait à tort prononcé la décharge de la pénalité mise à la charge de la société en application de l'article 1763 A ; qu'en statuant ainsi, la cour, qui avait précédemment rappelé, d'une part, que la pénalité était due par les sociétés et les autres personnes morales passibles de l'impôt sur les sociétés qui versent ou distribuent des revenus à des personnes dont elles ne révèlent pas l'identité et, d'autre part, que la société, interrogée par le service, n'avait pas donné de réponse satisfaisante en ce qui concerne l'identité des bénéficiaires d'une partie des revenus distribués, a suffisamment motivé sa décision qui n'est entachée, sur ce point, d'aucune erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la SARL Gestion Camping Caravaning n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la SARL Gestion Camping Caravaning tendant à l'application, à son profit, des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la SARL Gestion Camping Caravaning dirigées contre l'arrêt de la cour administrative de Marseille du 16 juin 2009 en tant qu'il a statué sur les rappels de taxe sur la valeur ajoutée et de taxe d'apprentissage mis à sa charge.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la SARL Gestion Camping Caravaning est rejeté. <br/>
Article 3 : La présente décision sera notifiée à la SARL Gestion Camping Caravaning et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
