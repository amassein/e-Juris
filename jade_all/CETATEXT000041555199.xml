<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041555199</ID>
<ANCIEN_ID>JG_L_2020_02_000000424245</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/55/51/CETATEXT000041555199.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/02/2020, 424245</TITRE>
<DATE_DEC>2020-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424245</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424245.20200210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1804790 du 3 septembre 2018, enregistrée le 26 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Cergy-Pontoise a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. A... B....<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Cergy-Pontoise le 22 mai 2018, et par un mémoire en réplique, enregistré le 29 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 2 février 2018 du ministre de la transition écologique et solidaire modifiant l'arrêté du 4 janvier 2017 portant désignation d'emplois éligibles à la nouvelle bonification indiciaire au sein du secrétariat général du ministère de l'environnement, de l'énergie et de la mer, en charge des relations internationales sur le climat, en tant que cet arrêté prévoit, d'une part, la suppression, à compter du 1er janvier 2017, de l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature de la liste des emplois éligibles à la nouvelle bonification indiciaire et, d'autre part, l'inscription, à compter du 1er mars 2017, du même emploi sur cette même liste avec attribution de 21 points ; <br/>
<br/>
              2°) d'annuler l'arrêté du 2 février 2018 du ministre de la transition écologique et solidaire portant désignation d'emplois éligibles à la nouvelle bonification indiciaire au sein du secrétariat général du ministère de la transition écologique et solidaire, en tant que cet arrêté prévoit l'inscription de l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature sur la liste des emplois éligibles à la nouvelle bonification indiciaire, à compter du 22 avril 2017, avec attribution de 21 points ; <br/>
<br/>
              3°) d'enjoindre au ministre de régulariser sa situation dans un délai d'un mois.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration, notamment ses articles L. 211-2 et L. 211-5 ;<br/>
              - la loi n° 91-73 du 18 janvier 1991 ;<br/>
              - la loi n° 94-628 du 25 juillet 1994 ;<br/>
              - le décret n° 91-1067 du 14 octobre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 janvier 2020, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., attaché principal, a été nommé le 1er janvier 2017 dans l'emploi d'adjoint au chef du bureau des affaires juridiques de l'eau et de la nature, au sein de la direction des affaires juridiques du ministère de la transition écologique et solidaire. Il conteste les dispositions de deux arrêtés ministériels du 2 février 2018, publiés au bulletin officiel du ministère le 25 mars 2018, en tant, pour le premier, qu'il supprime avec effet rétroactif pour la période allant du 1er janvier au 28 février 2017 la nouvelle bonification indiciaire attachée à ce poste, qui était fixée antérieurement au taux de 25 points, puis ne la rétablit qu'au taux de 21 points à compter du 1er mars 2017 et, pour le second, en tant qu'il ne rétablit cette nouvelle bonification, au taux de 21 points, qu'à compter du 22 avril 2017.<br/>
<br/>
              2. Aux termes du I de l'article 27 de la loi du 18 janvier 1991 portant dispositions relatives à la santé publique et aux assurances sociales : " La nouvelle bonification indiciaire des fonctionnaires et des militaires instituées à compter du 1er août 1990 est attribuée pour certains emplois comportant une responsabilité ou une technicité particulières dans des conditions fixées par décret ". <br/>
<br/>
              3. Le bénéfice de la nouvelle bonification indiciaire instituée par les dispositions citées ci-dessus est lié à l'emploi occupé par le fonctionnaire ou le militaire, compte tenu de la nature des fonctions attachées à cet emploi. Ce bénéfice, qui ne constitue pas un avantage statutaire, a un caractère temporaire qui cesse avec la cessation des fonctions y ouvrant droit, et peut être modifié ou supprimé par l'effet de l'arrêté qui fixe la liste des emplois attributaires et le nombre de points qui leur sont attachés. Il est à cet égard loisible à l'administration, lorsqu'elle établit la liste des emplois ouvrant droit à cette bonification, de prendre en considération des raisons budgétaires ou des orientations de politique de gestion des personnels. <br/>
<br/>
              4. En premier lieu, les arrêtés attaqués présentant un caractère réglementaire, le requérant ne peut utilement soulever à leur encontre les moyens tirés respectivement de ce qu'ils seraient insuffisamment motivés au regard des prescriptions des articles L. 211-2 et L. 211-5 du code des relations entre le public et l'administration et de ce qu'ils retireraient illégalement des décisions créatrices de droit au-delà du délai de quatre mois suivant leur adoption.<br/>
<br/>
              5. En deuxième lieu, il ne ressort pas des pièces du dossier que la décision de fixer à 21 points le taux de la nouvelle bonification indiciaire attribué à l'emploi d'adjoint au chef du bureau des affaires juridiques de l'eau et de la nature aurait été prise pour des motifs liés à la personne du titulaire de l'emploi.<br/>
<br/>
              6. En troisième lieu, le bénéfice de la nouvelle bonification indiciaire étant lié à la nature des fonctions exercées dans le cadre d'un emploi déterminé, la circonstance que d'autres emplois d'adjoint à un chef de bureau se soient vu maintenir une nouvelle bonification indiciaire à un taux de 25 points est, par elle-même, sans incidence sur la légalité des arrêtés contestés, dès lors qu'il ne ressort pas des pièces du dossier que ces emplois comporteraient la même responsabilité ou la même technicité particulières que l'emploi d'adjoint au chef du bureau des affaires juridiques de l'eau et de la nature.<br/>
<br/>
              7. En revanche, si l'article 25 de la loi du 25 juillet 1994 relative à l'organisation du temps de travail, aux recrutements et aux mutations dans la fonction publique, aux termes duquel " Les dispositions réglementaires prises pour l'application de l'accord sur la rénovation de la grille des classifications et des rémunérations conclus le 9 février 1990 peuvent prendre effet à une date antérieure à leur publication, dès lors que les crédits nécessaires ont fait l'objet d'une inscription dans la loi de finances correspondante ", permet de faire prendre effet à une date antérieure à leur publication à des dispositions réglementaires attribuant pour certains emplois le bénéfice de la nouvelle bonification indiciaire, il ne peut servir de fondement légal à des dispositions réglementaires retirant rétroactivement le bénéfice total ou partiel de cette bonification pour certains emplois y ouvrant déjà droit en vertu des dispositions prises antérieurement. Dès lors, en supprimant, pour la période allant du 1er janvier au 28 février 2017, la nouvelle bonification indiciaire de 25 points attachée à l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature, puis en la fixant à 21 points pour la période antérieure au 26 mars 2018, les arrêtés attaqués ont méconnu le principe de non-rétroactivité des actes réglementaires. Par suite, M. B... est fondé à demander leur annulation en tant qu'ils comportent cet effet rétroactif. <br/>
<br/>
              8. En vertu de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ". <br/>
<br/>
              9. Dès lors que l'acte réglementaire déterminant la liste des emplois ouvrant droit à la nouvelle bonification indiciaire et fixant le nombre de points qui leur est attaché n'appelle, par lui-même, aucune mesure individuelle d'application, l'exécution de la présente décision juridictionnelle implique nécessairement que soit réexaminée la situation de M. B... au titre de sa rémunération pour la période s'étendant du 1er janvier 2017 au 25 mars 2018, date de publication des arrêtés litigieux. Il y a lieu, par suite, d'enjoindre au ministre de la transition écologique et solidaire de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les deux arrêtés du 2 février 2018 du ministre de la transition écologique et solidaire sont annulés en tant qu'ils suppriment, pour la période allant du 1er janvier au 28 février 2017, la nouvelle bonification indiciaire de 25 points attachée à l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature, puis la fixent à 21 points pour la période antérieure au 26 mars 2018.<br/>
Article 2 : Il est enjoint au ministre de la transition écologique et solidaire de réexaminer la situation de M. B... au titre de sa rémunération pour la période s'étendant du 1er janvier 2017 au 25 mars 2018, dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus des conclusions de M. B... est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., à la ministre de la transition écologique et solidaire et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. RÉTROACTIVITÉ. - MODIFICATION RÉTROACTIVE DE LA NBI (ART. 25 DE LA LOI DU 25 JUILLET 1994) - 1) PRINCIPE [RJ1] - A) ATTRIBUTION DU BÉNÉFICE DE LA NOUVELLE NBI À CERTAINS EMPLOIS - LÉGALITÉ - B) RETRAIT DU BÉNÉFICE TOTAL OU PARTIEL DE CETTE NBI À CERTAINES EMPLOIS - ILLÉGALITÉ - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - NOUVELLE BONIFICATION INDICIAIRE (NBI) - MODIFICATION DE LA NBI APPLICABLE À UN EMPLOI D'ADJOINT AU CHEF DE BUREAU - 1) MAINTIEN D'UNE NBI À UN TAUX SUPÉRIEUR POUR D'AUTRES EMPLOIS D'ADJOINT AU CHEF DE BUREAU - CIRCONSTANCE SANS INCIDENCE DÈS LORS QUE CES EMPLOIS NE SONT PAS COMPARABLES - 2) RÉTROACTIVITÉ DE LA MODIFICATION (ART. 25 DE LA LOI DU 25 JUILLET 1994) - A) PRINCIPE [RJ1] - I) ATTRIBUTION DU BÉNÉFICE DE LA NOUVELLE NBI À CERTAINS EMPLOIS - LÉGALITÉ - II) RETRAIT DU BÉNÉFICE TOTAL OU PARTIEL DE CETTE NBI À CERTAINES EMPLOIS - ILLÉGALITÉ - B) APPLICATION.
</SCT>
<ANA ID="9A"> 01-08-02 1) a) Si l'article 25 de la loi n° 94-628 du 25 juillet 1994 permet de faire prendre effet à une date antérieure à leur publication à des dispositions réglementaires attribuant pour certains emplois le bénéfice de la nouvelle bonification indiciaire (NBI), b) il ne peut servir de fondement légal à des dispositions réglementaires retirant rétroactivement le bénéfice total ou partiel de cette bonification pour certains emplois y ouvrant déjà droit en vertu des dispositions prises antérieurement.... ,,2) Dès lors, en supprimant, pour la période allant du 1er janvier au 28 février 2017, la nouvelle bonification indiciaire de 25 points attachée à l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature, puis en la fixant à 21 points pour la période antérieure au 26 mars 2018, les arrêtés attaqués ont méconnu le principe de non-rétroactivité des actes réglementaires.</ANA>
<ANA ID="9B"> 36-08-03 Arrêtés ministériels supprimant, pour le premier, avec effet rétroactif pour la période allant du 1er janvier au 28 février 2017 la nouvelle bonification indiciaire (NBI) attachée à un poste d'adjoint au chef de bureau, qui était fixée antérieurement au taux de 25 points, puis ne la rétablissant qu'au taux de 21 points à compter du 1er mars 2017 et, pour le second, ne rétablissant cette nouvelle bonification, au taux de 21 points, qu'à compter du 22 avril 2017.,,,1) Le bénéfice de la NBI étant lié à la nature des fonctions exercées dans le cadre d'un emploi déterminé, la circonstance que d'autres emplois d'adjoint à un chef de bureau se soient vu maintenir une NBI à un taux de 25 points est, par elle-même, sans incidence sur la légalité des arrêtés contestés, dès lors qu'il ne ressort pas des pièces du dossier que ces emplois comporteraient la même responsabilité ou la même technicité particulières que l'emploi d'adjoint au chef du bureau en cause.,,,2) a) i) Si l'article 25 de la loi n° 94-628 du 25 juillet 1994 permet de faire prendre effet à une date antérieure à leur publication à des dispositions réglementaires attribuant pour certains emplois le bénéfice de la NBI, ii) il ne peut servir de fondement légal à des dispositions réglementaires retirant rétroactivement le bénéfice total ou partiel de cette bonification pour certains emplois y ouvrant déjà droit en vertu des dispositions prises antérieurement.... ,,b) Dès lors, en supprimant, pour la période allant du 1er janvier au 28 février 2017, la NBI de 25 points attachée à l'emploi d'adjoint du chef du bureau des affaires juridiques de l'eau et de la nature, puis en la fixant à 21 points pour la période antérieure au 26 mars 2018, les arrêtés attaqués ont méconnu le principe de non-rétroactivité des actes réglementaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 14 décembre 1998,,, n° 192114, T. pp. 722-750-991.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
