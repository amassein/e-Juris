<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044176756</ID>
<ANCIEN_ID>JG_L_2021_10_000000435991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/67/CETATEXT000044176756.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/10/2021, 435991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:435991.20211006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les associations " Les amis de la Terre Val-d'Oise ", " Val d'Oise environnement " et " SOS Vallée de Montmorency " ainsi que M. B... C... et M. D... A... ont demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir l'arrêté n° 2016-12999 du préfet du Val-d'Oise déclarant d'utilité publique, au profit du département du Val-d'Oise, le projet de réalisation de l'avenue du Parisis section Est entre la RD 301 à Groslay et la RD 84A à Bonneuil-en-France, sur le territoire des communes d'Arnouville, Bonneuil-en-France, Garges-les-Gonesse, Groslay et Sarcelles, et portant approbation des nouvelles dispositions des plans locaux d'urbanisme de Bonneuil-en-France, Garges-les-Gonnesse et Sarcelles. Par un jugement n° 1606126 du 13 mars 2018, le tribunal administratif de Cergy-Pontoise a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n°18VE01287, 18VE01317, 18VE01653 du 19 septembre 2019, la cour administrative d'appel de Versailles a rejeté les appels formés contre ce jugement par le département du Val d'Oise et par le ministre de l'intérieur et jugé qu'il n'y avait pas lieu de statuer sur la requête du département du Val d'Oise tendant à ce qu'il soit sursis à l'exécution du même jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 novembre 2019 et le 13 février 2020 au secrétariat du contentieux du Conseil d'Etat, le département du Val d'Oise demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre la somme de 5 000 euros à la charge des associations " Les amis de la Terre Val-d'Oise ", " Val d'Oise environnement " et " SOS Vallée de Montmorency " et de MM. C... et A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du département du Val-d'Oise et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'association Val d'Oise environnement, de l'association SOS Vallée de Montmorency, de M. C..., de M. A... et de l'association Les amis de la Terre Val-d'Oise ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 avril 2016, le préfet du Val-d'Oise a, d'une part, déclaré d'utilité publique le projet de réalisation de la section Est de l'avenue du Parisis, entre la RD 301 à Groslay et la RD 84 A à Bonneuil-en-France, sur le territoire des communes de Groslay, Sarcelles, Garges-lès-Gonesse, Arnouville et Bonneuil-en-France, et, d'autre part, approuvé les nouvelles dispositions des plans locaux d'urbanisme de Sarcelles, Garges-les-Gonesse et Bonneuil-en-France. Saisi par les associations " Les amis de la Terre Val-d'Oise ", " Val d'Oise environnement " et " SOS Vallée de Montmorency " et par MM. C... et A..., le tribunal administratif de Cergy-Pontoise a annulé cet arrêté par un jugement du 13 mars 2018. Par un arrêt du 19 septembre 2019, la cour administrative d'appel de Versailles a rejeté les appels formés par le ministre de l'intérieur et par le département du Val-d'Oise contre ce jugement et jugé qu'il n'y avait pas lieu de statuer sur la requête du département tendant à ce qu'il soit sursis à l'exécution de ce jugement. Le département du Val-d'Oise se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              Sur l'arrêt, en tant qu'il rejette l'appel du ministre de l'intérieur contre le jugement du tribunal administratif de Cergy-Pontoise du 13 mars 2018 :<br/>
<br/>
              2. Seul l'Etat avait qualité pour former un pourvoi en cassation contre l'arrêt attaqué en tant que ce dernier a rejeté l'appel du ministre de l'intérieur, sans qu'ait d'incidence à cet égard la circonstance que le département du Val d'Oise avait, par une requête distincte, relevé appel du même jugement et que la cour administrative d'appel, joignant l'appel du ministre et les requête du département du Val d'Oise, y a statué par un unique arrêt. Par suite, le département du Val d'Oise ne justifie pas d'un intérêt à se pourvoir en cassation contre cet arrêt en tant qu'il rejette la requête du ministre de l'intérieur. Il n'est donc pas recevable à en demander, dans cette mesure, l'annulation. Son pourvoi doit être rejeté dans la même mesure.<br/>
<br/>
              Sur l'arrêt, en tant qu'il rejette l'appel formé par le département du Val d'Oise contre le jugement du tribunal administratif de Cergy-Pontoise du 13 mars 2018 :<br/>
<br/>
              3. Aux termes de l'article R. 741-2 du code de justice administrative : " La décision mentionne que l'audience a été publique (...). / Elle contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application. / (...) Mention est également faite de la production d'une note en délibéré (...) ". Il résulte de ces dispositions que, lorsqu'il est régulièrement saisi, à l'issue de l'audience, d'une note en délibéré émanant de l'une des parties, il appartient dans tous les cas au juge administratif d'en prendre connaissance avant de rendre sa décision ainsi que de la viser, sans toutefois l'analyser dès lors qu'il n'est pas amené à rouvrir l'instruction et à la soumettre au débat contradictoire pour tenir compte des éléments nouveaux qu'elle contient.<br/>
<br/>
              4. Il ressort des pièces de la procédure devant la cour administrative d'appel de Versailles qu'après l'audience publique qui s'est tenue le 5 septembre 2019, le département du Val-d'Oise a produit une note en délibéré, enregistrée au greffe de la cour le 6 septembre 2019, soit avant la lecture de l'arrêt. L'arrêt attaqué, qui ne vise pas cette note, est de ce fait entaché d'irrégularité. Dès lors, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, le département du Val-d'Oise est fondé à demander l'annulation de l'arrêt du 19 septembre 2019 en tant qu'il rejette son appel formé contre le jugement du tribunal administratif de Cergy-Pontoise du 13 mars 2018.<br/>
<br/>
              Sur l'arrêt, en tant qu'il prononce un non-lieu à statuer sur les conclusions du département du Val d'Oise à fin de sursis à exécution :<br/>
<br/>
              5. Il résulte de ce qui précède que l'arrêt attaqué doit être annulé en tant qu'il prononce un non-lieu à statuer sur les conclusions du département du Val d'Oise à fin de sursis à exécution du jugement du tribunal administratif de Cergy-Pontoise du 13 mars 2018. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du département du Val-d'Oise, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre la somme demandée par ce département au titre des mêmes dispositions à la charge des associations " Les amis de la Terre Val-d'Oise ", " Val d'Oise environnement " et " SOS Vallée de Montmorency " et de MM. C... et A....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 19 septembre 2019 de la cour administrative d'appel de Versailles est annulé en tant qu'il statue sur les requêtes du département du Val d'Oise.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département du Val d'Oise, aux associations " Les amis de la Terre Val-d'Oise ", " Val d'Oise environnement " et " SOS Vallée de Montmorency " et à MM. C... et A....<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
