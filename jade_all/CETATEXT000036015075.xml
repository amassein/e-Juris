<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036015075</ID>
<ANCIEN_ID>JG_L_2017_10_000000414890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/01/50/CETATEXT000036015075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/10/2017, 414890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:414890.20171030</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 et 19 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Terrada, M. C...A..., la société Galeries Cardinet et la société Les 3 D (groupeA...) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 28 juillet 2017 par laquelle la présidente de l'Autorité de la concurrence a refusé d'agréer la cession au groupe A...des points de vente exploités sous l'enseigne Darty situés au 25-35 boulevard de Belleville (75011 Paris) et aux 125-127 avenue de Saint-Ouen (75017 Paris) et visés dans la lettre des engagements annexés à la décision n°16-DCC-111 du 27 juillet 2016 par laquelle l'Autorité de la concurrence a autorisé, sous réserve de ces engagements, la prise de contrôle exclusif de Darty par la FNAC ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de la concurrence la somme de 5 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est satisfaite dès lors que la combinaison de la décision litigieuse avec celle, dont la suspension est également demandée, de refus de prolongation des délais d'exécution des engagements préjudicie de manière grave et immédiate à leurs intérêts économiques, en ce que, en premier lieu, elle met en échec un projet d'acquisition déjà considérablement avancé ayant conduit le groupe A...à exposer des frais importants, en deuxième lieu, elle prive ce groupe d'une opportunité d'implanter des enseignes de distribution de produits électroniques au sein de la capitale et, en dernier lieu, elle porte atteinte au rétablissement rapide d'une concurrence effective sur les marchés de distribution de produits électroniques ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - la décision de refus d'agréer la cession des points de vente en litige au groupe A...est intervenue en méconnaissance du principe du caractère contradictoire de la procédure et des droits de la défense, le groupe A...n'ayant été en mesure ni de prendre connaissance des rapports établis par le mandataire désigné par l'Autorité de la concurrence, ni de faire valoir ses observations ;<br/>
              - la décision contestée est illégale en ce qu'elle est prise pour la mise en oeuvre de la décision du 27 juillet 2016 de l'Autorité de la concurrence, elle-même illégale dès lors qu'elle exige des engagements excédant les mesures strictement nécessaires pour remédier aux effets de l'opération de concentration sur la concurrence et procède d'une mise en oeuvre arbitraire de la méthode dite de " scoring " pour la détermination des cessionnaires des points de vente devant être cédés par la société FNAC Darty ;<br/>
              - la décision contestée est entachée d'une erreur manifeste d'appréciation et d'une méconnaissance du principe de non-discrimination en ce qu'elle réserve la reprise des enseignes Darty à des opérateurs spécialisés en produits " bruns " et " gris " ;<br/>
                - elle est entachée d'une erreur manifeste d'appréciation en ce que, pour estimer que le groupe A...ne saurait constituer un acteur en mesure de rétablir une concurrence effective et suffisante sur les marchés de produits électrodomestiques dans les zones de chalandise en cause, elle omet de prendre en compte l'expérience de ce groupe dans la vente au détail de produits électroménagers (produits " blancs "), laquelle présente des caractéristiques, proches de celles de la vente au détail de produits électroniques (produits " bruns " et " gris ").<br/>
<br/>
              Par un mémoire en défense, enregistré le 17 octobre 2017, l'Autorité de la concurrence conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Terrada,  M. C...A..., la société Galeries Cardinet et la société Les 3 D, d'autre part, l'Autorité de la concurrence ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 20 octobre 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Chevallier, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de la société Terrada, M. C...A..., la société Galeries Cardinet et de la société Les 3 D ;<br/>
<br/>
              - M. B...A..., représentant du groupeA... ;<br/>
<br/>
              - les représentants de l'Autorité de la concurrence ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au vendredi 27 octobre à 18 heures.<br/>
<br/>
              Vu le nouveau mémoire produit pour la société Terrada et autres le 25 octobre 2017 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 430-1 du code de commerce : " I. -  Une opération de concentration est réalisée : 1° Lorsque deux ou plusieurs entreprises antérieurement indépendantes fusionnent ; / 2° Lorsqu'une ou plusieurs personnes, détenant déjà le contrôle d'une entreprise au moins ou lorsqu'une ou plusieurs entreprises acquièrent, directement ou indirectement, que ce soit par prise de participation au capital ou achat d'éléments d'actifs, contrat ou tout autre moyen, le contrôle de l'ensemble ou de parties d'une ou plusieurs autres entreprises. (...) " ; qu'il appartient à l'Autorité de la concurrence, saisie d'une opération de concentration, à partir d'une analyse prospective tenant compte de l'ensemble des données pertinentes et se fondant sur un scénario économique plausible, de caractériser les effets anticoncurrentiels de l'opération et d'apprécier si ces effets sont de nature à porter atteinte au maintien d'une concurrence suffisante sur les marchés qu'elle affecte ; que l'Autorité de la concurrence, à laquelle une opération de concentration entrant dans le champ défini par les articles L. 430-1 et L. 430-2  du code commerce a été notifiée, peut, en vertu des dispositions de l'article L. 430-5 du même code, soit autoriser l'opération en la subordonnant éventuellement à la réalisation effective d'engagements pris par les parties, soit, si elle estime qu'il existe un doute sérieux d'atteinte à la concurrence, engager un examen approfondi au terme duquel elle prend une décision qui peut être d'autorisation, le cas échéant assortie d'engagements, de prescriptions ou d'injonctions, ou d'interdiction, dans les conditions prévues aux articles L. 430-6 et L. 430-7 du même code ; qu'aux termes de l'article L. 430-8 du code de commerce " IV. - Si elle estime que les parties n'ont pas exécuté dans les délais fixés une injonction, une prescription ou un engagement figurant dans sa décision, l'Autorité de la concurrence constate l'inexécution. Elle peut : /1° Retirer la décision ayant autorisé la réalisation de l'opération. A moins de revenir à l'état antérieur à la concentration, les parties sont tenues de notifier de nouveau l'opération dans un délai d'un mois à compter du retrait de la décision, sauf à encourir les sanctions prévues au I ; / 2° Enjoindre sous astreinte, dans la limite prévue au II de l'article L. 464-2, aux parties auxquelles incombait l'obligation non exécutée d'exécuter dans un délai qu'elle fixe les injonctions, prescriptions ou engagements figurant dans la décision ; / 3° Enjoindre sous astreinte, dans la limite prévue au II de l'article L. 464-2, aux parties auxquelles incombait l'obligation, d'exécuter dans un délai qu'elle fixe des injonctions ou des prescriptions en substitution de l'obligation non exécutée. / En outre, l'Autorité de la concurrence peut infliger aux personnes auxquelles incombait l'obligation non exécutée une sanction pécuniaire qui ne peut dépasser le montant défini au I " ;<br/>
<br/>
              3. Considérant que par une décision du 27 juillet 2016, l'Autorité de la concurrence a autorisé, sous réserve du respect des engagements souscrits par la partie notifiante et annexés à sa décision, la prise de contrôle exclusif de Darty par la FNAC ; qu'en vue de prévenir les effets anticoncurrentiels de l'opération de concentration, la FNAC s'est engagée à céder six points de ventes exploités sous les enseignes FNAC ou Darty, chaque contrat de cession étant subordonné à son approbation par l'autorité de la concurrence ; qu'après la conclusion de promesses de vente des magasins " Darty Belleville " et " Darty Saint-Ouen " au profit de sociétés du groupeA..., la FNAC a sollicité de l'Autorité de la concurrence, par courrier du 10 mai 2017, l'agrément de la cession de ces points de vente à cet acquéreur ; que, par une décision du 28 juillet 2017, la présidente de l'Autorité de la concurrence a rejeté cette demande ; que la société Terrada, la société Galeries Cardinet, la société Les 3 D, toutes trois membres du groupeA..., ainsi que M. C...A...demandent la suspension de l'exécution de cette décision ;<br/>
<br/>
              4. Considérant que les requérants soutiennent, en premier lieu, pour établir que la condition d'urgence posée par l'article L.521-1 du code de justice administrative est satisfaite, que la décision de refus d'agrément opposée par la présidente de l'Autorité de la concurrence au projet de cession à des sociétés du groupe A...des points de vente Darty ci-dessus mentionnée  préjudicie de manière grave et immédiate aux intérêts économiques de ces sociétés, en ce qu'elle met en échec une opération déjà très avancée pour laquelle elles ont exposé des frais importants, consistant notamment en l'acquisition d'entrepôts de stockage de marchandises, et en ce qu'elle prive ce groupe d'une opportunité d'implanter des enseignes de distribution de produits électroniques au sein de la capitale ; <br/>
<br/>
              5. Considérant, toutefois, d'une part, que les sociétés requérantes ne pouvaient ignorer que les projets d'acquisition dans lesquels elles se sont engagées étaient subordonnés, en application des engagements souscrits par la FNAC et annexés à la décision du 27 juillet 2016 autorisant l'opération de concentration, à un agrément de l'autorité de la concurrence ; que les promesses de vente qu'elles ont conclues sur les magasins " Darty Belleville " et " Darty Saint-Ouen " l'ont été sous condition suspensive d'approbation de la cession par l'Autorité ; qu'ainsi, les frais exposés par les sociétés requérantes l'ont été en toute connaissance du risque attaché à l'opération ; qu'au demeurant, il n'est pas établi que ces frais, qui ont notamment consisté en l'acquisition d'éléments d'actifs, auraient été exposés de manière irréversible, ni, en tout état de cause, que leur montant serait tel qu'il pourrait porter une atteinte grave et immédiate à la pérennité économique ou financière des sociétés du groupeA... ; <br/>
<br/>
              6. Considérant, d'autre part, que si la décision contestée a pour effet de priver les sociétés requérantes de la possibilité d'augmenter leur activité par une implantation sur le marché de la distribution de produits électroniques à Paris, un tel préjudice potentiel, en l'absence de difficultés économiques et financières affectant la situation d'ensemble de la société, ne caractérise pas à lui seul une situation d'urgence ; <br/>
<br/>
              7. Considérant, que si les requérants soutiennent, en second lieu, qu'il y aurait urgence à suspendre la décision contestée au motif qu'elle pérenniserait une situation de non respect des engagements attachés à l'autorisation de concentration et irait de ce fait à l'encontre de l'intérêt public tenant au rétablissement rapide d'une concurrence effective sur les marchés de distribution de produits électroniques dans les zones de chalandise concernées, une telle argumentation ne peut qu'être écartée dès lors que l'autorité de la concurrence dispose, en l'absence de respect des engagements pris par les parties notifiantes et en application du IV de l'article L. 430-8 du code de commerce, de la possibilité soit de retirer l'autorisation de concentration, soit d'enjoindre à ces parties, éventuellement sous astreinte, d'exécuter ces engagements ou des prescriptions de substitution ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative ne peut être regardée comme satisfaite ; qu'ainsi, sans qu'il soit besoin d'examiner si l'un au moins des moyens soulevés par les requérants est de nature à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision contestée, leur demande tendant à ce que soit ordonnée la suspension de son exécution doit être rejetée, y compris leurs conclusions tendant à la mise en oeuvre des dispositions de l'article L.761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Terrada, M. C...A..., la société Galeries Cardinet et la société Les 3 D est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Terrada, à M. C...A..., à la société Galeries Cardinet, à la société Les 3 D et à l'Autorité de la concurrence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
