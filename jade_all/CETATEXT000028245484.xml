<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028245484</ID>
<ANCIEN_ID>JG_L_2013_11_000000359801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/24/54/CETATEXT000028245484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 27/11/2013, 359801</TITRE>
<DATE_DEC>2013-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359801.20131127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 30 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat SUD travail affaires sociales, dont le siège est 12, boulevard de Bonne Nouvelle à Paris (75010) ; le syndicat requérant demande au Conseil d'Etat d'annuler pour excès de pouvoir les dispositions de la note du 6 avril 2012 du ministre du travail, de l'emploi et de la santé relative aux conditions d'organisation des réunions statutaires et d'information syndicale en ce qu'elle impose qu'une section syndicale soit présente au sein d'un bâtiment pour pouvoir y organiser des réunions, qu'une demande écrite soit déposée 8 jours à l'avance pour pouvoir bénéficier de locaux en vue de réunions, qu'un délai de 48 heures soit observé pour informer de la venue d'un représentant syndical, qu'une demande d'autorisation spéciale d'absence soit déposée au moins 5 jours à l'avance pour assister à une réunion statutaire, qu'un délai de 24 heures soit observé pour informer de l'assistance à une réunion d'information organisée pendant les heures de service et que la présence des agents aux réunions syndicales fasse l'objet d'un suivi figurant, le cas échéant, dans leur dossier ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, et notamment son Préambule ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu le décret n° 82-447 du 28 mai 1982 ;<br/>
<br/>
              Vu le décret n° 2012-224 du 16 février 2012 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>1. Considérant que le décret du 16 février 2012 a modifié, notamment, les dispositions des articles 4 à 7, relatives aux réunions syndicales, du décret du 28 mai 1982 relatif à l'exercice du droit syndical dans la fonction publique ; que, pour commenter et préciser ces modifications, le ministre du travail, de l'emploi et de la santé a adopté le 6 avril 2012 une circulaire dont le syndicat requérant demande l'annulation partielle ;<br/>
<br/>
              2. Considérant, en premier lieu, que l'article 4 du décret du 28 mai 1982 dispose que : " Les organisations syndicales peuvent tenir des réunions statutaires ou d'information à l'intérieur des bâtiments administratifs en dehors des horaires de service. Elles peuvent également tenir des réunions durant les heures de service mais dans ce cas seuls les agents qui ne sont pas en service ou qui bénéficient d'une autorisation spéciale d'absence peuvent y assister " ; qu'aux termes du I de l'article 5 de ce même décret, dans sa rédaction issue du décret du 16 février 2012 : " Les organisations syndicales représentatives sont en outre autorisées à tenir, pendant les heures de service, des réunions mensuelles d'information. / Sont considérées comme représentatives, d'une part, les organisations syndicales disposant d'au moins un siège au sein du comité technique déterminé en fonction du service ou groupe de services concerné, d'autre part, les organisations syndicales disposant d'au moins un siège au sein du comité technique ministériel ou du comité technique d'établissement public de rattachement. / Chacun des membres du personnel a le droit de participer à l'une de ces réunions, dans la limite d'une heure par mois. [...] " ; <br/>
<br/>
              3. Considérant que ni ces dispositions ni aucune autre règle ou principe ne prévoient que seules les organisations syndicales qui disposent d'une section syndicale à l'intérieur des bâtiments où sont organisées les réunions statutaires ou d'information peuvent organiser de telles réunions ; qu'en imposant une telle exigence, le ministre a excédé sa compétence ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article 7 du même décret : " La tenue des réunions mentionnées aux articles 4, 5 et 6 ne doit pas porter atteinte au bon fonctionnement du service ou entraîner une réduction de la durée d'ouverture de ce service aux usagers. / Les demandes d'organisation de telles réunions doivent, en conséquence, être formulées au moins une semaine avant la date de la réunion " ; qu'en exigeant que les demandes ainsi prévues soient formulées au moins huit jours avant la tenue de la réunion, le ministre a méconnu ces dispositions et excédé sa compétence ; qu'il lui était en revanche loisible, en tant que chef de service, de prévoir que ces demandes devaient être formulées par écrit ;<br/>
<br/>
              5. Considérant, en troisième lieu, que l'article 6 du décret prévoit que : " Tout représentant mandaté à cet effet par une organisation syndicale a libre accès aux réunions tenues par cette organisation à l'intérieur des bâtiments administratifs, même s'il n'appartient pas au service dans lequel une réunion se tient. / Le chef de service doit être informé de la venue de ce représentant avant le début de la réunion " ; que, s'il était loisible au ministre, en sa qualité de chef de service, de fixer un délai raisonnable d'information préalable, il a, en retenant un délai de quarante-huit heures, fixé une condition excessive au regard des nécessités d'un bon fonctionnement du service et, par suite, excédé sa compétence ;<br/>
<br/>
              6. Considérant, en quatrième lieu, qu'il découle des dispositions déjà citées du même décret, notamment de ses articles 4 et 7, que la participation des agents aux réunions syndicales durant les heures de service est subordonnée à la condition, pour les réunions autres que les réunions mensuelles d'information, qu'elle fasse l'objet d'une autorisation spéciale d'absence et, pour toutes les réunions, à la condition qu'elle ne porte pas atteinte au bon fonctionnement du service et n'entraîne pas une réduction de la durée d'ouverture de ce service aux usagers ; qu'en vertu du second alinéa de l'article 7, la demande d'organisation d'une réunion doit être formulée au moins une semaine à l'avance ; qu'en prévoyant que les demandes d'autorisation spéciale d'absence devaient être présentées au moins cinq jours ouvrés à l'avance, c'est-à-dire le plus souvent également une semaine à l'avance, le ministre a fixé une condition excessive au regard des nécessités du bon fonctionnement du service et, par suite, excédé sa compétence ; qu'en revanche, en prévoyant, pour les réunions mensuelles d'information, que les agents devaient informer leur supérieur hiérarchique au moins vingt-quatre heures avant de s'y rendre, le ministre a fait usage de ses pouvoirs d'organisation du service sans excéder sa compétence ni méconnaître le droit, consacré par le Préambule de la Constitution de 1946, pour toute personne, de défendre ses droits et ses intérêts par l'action syndicale ;  <br/>
<br/>
              7. Considérant, enfin, qu'aux termes de l'article 18 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Le dossier du fonctionnaire doit comporter toutes les pièces intéressant la situation administrative de l'intéressé, enregistrées, numérotées et classées sans discontinuité. / Il ne peut être fait état dans le dossier d'un fonctionnaire, de même que dans tout document administratif, des opinions ou des activités politiques, syndicales, religieuses ou philosophiques de l'intéressé " ; que la fréquentation de réunions mensuelles d'information organisées par les syndicats relève des activités syndicales ; que les informations relatives à cette fréquentation ne sauraient dès lors être consignées dans le dossier d'un fonctionnaire ; que, par suite, le ministre ne pouvait légalement prévoir, par la circulaire attaquée, que le tableau de suivi de la participation à ces réunions pourrait être inséré dans le dossier administratif de l'agent ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la deuxième phrase du troisième alinéa et les quatrième et cinquième alinéas du point 1, les mots " au moins 8 jours à l'avance " au sixième alinéa du même point, les mots " qui ne peut être inférieur, sauf circonstances exceptionnelles, à 48 h avant le début de la réunion " au dernier alinéa du même point, les mots " qui ne doit pas être inférieur à 5 jours ouvrés précédant la date programmée de la réunion " au deuxième alinéa du point 2 et les mots " et, le cas échéant, insérer dans le dossier administratif de l'agent " au sixième alinéa du b) du point 3 de la circulaire attaquée, qui sont divisibles de ses autres dispositions, doivent être annulés ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La deuxième phrase du troisième alinéa et les quatrième et cinquième alinéas du point 1, les mots " au moins 8 jours à l'avance " au sixième alinéa du même point, les mots " qui ne peut être inférieur, sauf circonstances exceptionnelles, à 48 h avant le début de la réunion " au dernier alinéa du même point, les mots " qui ne doit pas être inférieur à 5 jours ouvrés précédant la date programmée de la réunion " au deuxième alinéa du point 2 et les mots " et, le cas échéant, insérer dans le dossier administratif de l'agent " au sixième alinéa du b) du point 3 de la note du 6 avril 2012 du ministre du travail, de l'emploi et de la santé relative aux conditions d'organisation des réunions statutaires et d'information syndicale sont annulés.<br/>
Article 2 : Le surplus des conclusions de la requête du syndicat SUD travail affaires sociales est rejeté.<br/>
Article 3 : La présente décision sera notifiée au syndicat SUD travail affaires sociales et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. - POUVOIR JAMART - COMPÉTENCE POUR PRÉCISER LE DÉLAI DANS LEQUEL L'OBLIGATION D'INFORMATION DU CHEF DE SERVICE, PRÉVUE PAR DÉCRET, AVANT LA VENUE D'UN REPRÉSENTANT SYNDICAL DANS UN BÂTIMENT ADMINISTRATIF, DOIT ÊTRE MISE EN &#140;UVRE - EXISTENCE - PORTÉE.
</SCT>
<ANA ID="9A"> 01-02-02-01-03 Circulaire prise pour commenter le décret n° 2012-224 du 16 février 2012 modifiant le décret n° 82-447 du 28 mai 1982 relatif à l'exercice du droit syndical dans la fonction publique, dont l'article 6 prévoit que, si tout représentant mandaté à cet effet par une organisation syndicale a libre accès aux réunions tenues par cette organisation à l'intérieur des bâtiments administratifs, le chef de service doit être informé de la venue de ce représentant avant le début de la réunion.... ,,S'il était loisible au ministre, en sa qualité de chef de service, de fixer un délai raisonnable d'information préalable, il a, en retenant un délai de quarante-huit heures, fixé une condition excessive au regard des nécessités d'un bon fonctionnement du service et, par suite, excédé sa compétence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
