<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042409947</ID>
<ANCIEN_ID>JG_L_2020_10_000000425749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/99/CETATEXT000042409947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/10/2020, 425749, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425749.20201007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... B... ont demandé au tribunal administratif de Versailles de prononcer la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2010 et des pénalités correspondantes. Par un jugement n° 1405098 du 11 octobre 2016, le tribunal administratif de Versailles a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16VE03560 du 2 octobre 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. et Mme B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 novembre 2018 et 11 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2008-1425 du 27 décembre 2008 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de M. et Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont déduit de leur revenu global imposable à l'impôt sur le revenu au titre de l'année 2010 le déficit foncier correspondant aux travaux de restauration afférents au lot relevant d'un ensemble immobilier classé à l'inventaire supplémentaire des monuments historiques, situé rue des Réservoirs à Versailles, qu'ils avaient acquis le 27 décembre 2010. Dans le cadre d'un contrôle sur pièces du dossier, l'administration fiscale a remis en cause l'imputation de ce déficit foncier au motif que l'immeuble, qui avait fait l'objet d'une division en copropriété en 1924, avait de nouveau été divisé postérieurement au 1er janvier 2009 sans avoir fait l'objet de l'agrément prévu au V de l'article 156 bis du code général des impôts. Par un jugement du 11 octobre 2016, le tribunal administratif de Versailles a rejeté la demande de M. et Mme B... tendant à la décharge du supplément d'impôt sur le revenu consécutif à cette rectification ainsi que de la pénalité correspondante. Les contribuables demandent l'annulation de l'arrêt du 2 octobre 2018 par lequel la cour administrative d'appel de Versailles a confirmé ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 156 du code général des impôts dans sa rédaction applicable à l'année 2010 : " L'impôt sur le revenu est établi d'après le montant total du revenu net annuel dont dispose chaque foyer fiscal. Ce revenu net est déterminé (...) sous déduction : / I. du déficit constaté pour une année dans une catégorie de revenus ; (...) Toutefois, n'est pas autorisée l'imputation : / 3° Des déficits fonciers, lesquels s'imputent exclusivement sur les revenus fonciers des dix années suivantes ; cette disposition n'est pas applicable aux propriétaires de monuments classés monuments historiques, inscrits à l'inventaire supplémentaire ou ayant fait l'objet d'un agrément ministériel ou ayant reçu le label délivré par la " Fondation du patrimoine " en application de l'article L. 143-2 du code du patrimoine si ce label a été accordé sur avis favorable du service départemental de l'architecture et du patrimoine (...) ". Aux termes de l'article 156 bis de ce même code dans sa rédaction applicable à l'année 2010 : " V. _ Le bénéfice des dispositions de l'article 156 propres aux immeubles classés ou inscrits au titre des monuments historiques (...) n'est pas ouvert aux immeubles ayant fait l'objet d'une division à compter du 1er janvier 2009 sauf si cette division fait l'objet d'un agrément délivré par le ministre du budget, après avis du ministre de la culture, lorsque l'intérêt patrimonial du monument et l'importance des charges relatives à son entretien la justifient ". Il résulte des dispositions du V de l'article 156 bis du code général des impôts que lorsque des immeubles classés ou inscrits à l'inventaire supplémentaire des monuments historiques ont fait l'objet d'une division après le 1er janvier 2009, qu'il s'agisse d'une première division ou de divisions ultérieures, le bénéfice du régime fiscal dérogatoire prévu à l'article 156 du code général des impôts est subordonné à l'agrément de cette division par le ministre chargé du budget.<br/>
<br/>
              3. En second lieu, aux termes de l'article L. 80 A du livre des procédures fiscales dans sa rédaction applicable au litige : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration. / Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente. Sont également opposables à l'administration, dans les mêmes conditions, les instructions ou circulaires publiées relatives au recouvrement de l'impôt et aux pénalités fiscales ".<br/>
<br/>
              4. Aux termes des points 36 et 37 de l'instruction du 6 octobre 2009 publiée au bulletin officiel des impôts le 14 octobre 2009 et référencée 5 D-2-09 : " 36. Le V de l'article 156 bis du CGI prévoit que le bénéfice des dispositions de l'article 156 propres aux immeubles historiques ou assimilés n'est pas ouvert aux immeubles ayant fait l'objet d'une division à compter du 1er janvier 2009. Une exception est toutefois prévue lorsque la division fait l'objet d'un agrément délivré par le ministre du budget, après avis du ministre de la culture, si l'intérêt patrimonial du monument et l'importance des charges relatives à son entretien le justifient. / 37. Exclusion des immeubles mis en copropriété. La division consiste à répartir par lots la propriété d'un immeuble entre plusieurs personnes, ces lots comprenant une partie privative et une quote-part de parties communes (...) ". Aux termes des deux premiers alinéas du point 42 de cette même instruction : " 42. Ces dispositions, relatives à l'absence de division de l'immeuble, s'appliquent à compter de l'imposition des revenus de l'année 2009, pour les divisions intervenant à compter du 1er janvier 2009. / Autrement dit, les propriétaires d'immeubles historiques et assimilés qui ont été mis en copropriété avant le 1er janvier 2009 ne sont pas soumis à cette condition et bénéficient du régime spécial dans les conditions habituelles ".<br/>
<br/>
              5. Il résulte du second alinéa du point 42 de l'instruction citée au point 4 ci-dessus que les propriétaires de lots d'immeubles historiques et assimilés ayant fait l'objet d'une mise en copropriété avant le 1er janvier 2009 et dont la copropriété n'avait pas disparu à cette date du fait notamment de la réunion de tous les lots entre les mains d'un même propriétaire, bénéficient du régime spécial prévu à l'article 156 du code général des impôts, alors même que de nouvelles divisions seraient intervenues après cette date. Par suite, en jugeant que l'administration fiscale n'avait pas donné des dispositions du V de l'article 156 bis du code général des impôts, une interprétation différente de celle exposée au point 2 ci-dessus et que les contribuables ne pouvaient s'en prévaloir sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales, la cour a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. et Mme B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 2 octobre 2018 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à M. et Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme A... B... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
