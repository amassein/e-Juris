<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043205054</ID>
<ANCIEN_ID>JG_L_2021_03_000000436654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/20/50/CETATEXT000043205054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 01/03/2021, 436654</TITRE>
<DATE_DEC>2021-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436654.20210301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      M. B... et Mme C... A... ont demandé au tribunal administratif de Strasbourg, d'une part, d'annuler la décision implicite par laquelle l'Eurométropole de Strasbourg a rejeté leur demande de communication de documents administratifs relatifs à la décision de sélection d'un groupement d'aménageurs pour l'aménagement de la ZAC Jean Monnet à Eckbolsheim, d'autre part, à ce qu'il soit enjoint à l'Eurometropole de Strasbourg de leur transmettre ces documents dans un délai de quinze jours à compter de la notification du jugement à intervenir, sous astreinte journalière.  <br/>
<br/>
      Par un jugement n° 1700175 du 27 mars 2019, le tribunal administratif de Strasbourg a rejeté leur demande.    <br/>
<br/>
      Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 décembre 2019 et 11 mars 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... A... demande au Conseil d'Etat :<br/>
<br/>
      1°) d'annuler ce jugement ;<br/>
<br/>
      2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
      3°) de mettre à la charge de l'Eurométropole de Strasbourg la somme de 3 000 euros, à verser à son avocat, la SCP JCP Caston, au titre des articles L. 761-1 du code de justice administrative et 37-de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
<br/>
      Vu : <br/>
      - le traité sur le fonctionnement de l'Union européenne ;<br/>
      - la directive 2003/4/CE du Parlement européen et du Conseil du 28 janvier 2003 ;<br/>
      - le code de l'environnement ; <br/>
      - le code des relations entre le public et l'administration ; <br/>
      - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;   <br/>
<br/>
<br/>
<br/>
      Après avoir entendu en séance publique :<br/>
<br/>
      - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
      - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
      La parole ayant été donnée, après les conclusions, à la SCP Jean-Philippe Caston, avocat de Mme C... A... et à la SCP Sevaux, Mathonnet, avocat de la société Eurométropole de Strasbourg ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération en date du 25 octobre 2013, la communauté urbaine de Strasbourg, devenue le 1er janvier 2015, l'Eurométropole de Strasbourg, a approuvé le dossier de création de la ZAC Jean Monnet à Eckbolsheim afin d'y réaliser un " éco-quartier ". Par une décision du 8 janvier 2016, un jury de l'Eurométropole de Strasbourg a sélectionné un groupement d'aménageurs en vue de l'aménagement de la zone. M. et Mme A... ont sollicité, par un courrier du 27 avril 2016, la copie intégrale des propositions des promoteurs retenus ainsi que des décisions de découpage de la zone en lots et d'attribution de ces lots, le courrier informant les promoteurs que leur projet était retenu, tous les documents préparatoires à cette décision d'attribution en dehors de l'appel d'offres, toute note ou tout rapport établi par les services analysant la ou les propositions déposées dans le cadre de l'appel d'offres, le compte rendu de toute commission ayant donné un avis sur la proposition retenue, ainsi que les contrats signés avec les promoteurs. Après avoir saisi le 17 juin 2016 la commission d'accès aux documents administratifs qui a émis son avis le 6 octobre 2016, M. et Mme A... ont demandé au tribunal administratif de Strasbourg d'annuler la décision implicite par laquelle l'Eurométropole de Strasbourg a rejeté leur demande de communication de ces documents et de lui enjoindre de les leur transmettre. Mme A... se pourvoit en cassation contre le jugement du tribunal administratif de Strasbourg du 27 mars 2019 rejetant cette demande.<br/>
<br/>
              Sur les conclusions dirigées contre le jugement du 27 mars 2019 en tant qu'il statue sur les documents demandés à l'exception des informations environnementales :  <br/>
<br/>
              2. L'article L. 311-1 du code des relations entre le public et l'administration dispose que : " Sous réserve des dispositions des articles L. 311-5 et L. 311-6, les administrations mentionnées à l'article L. 300-2 sont tenues de publier en ligne ou de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent livre ". Aux termes de l'article L. 311-2 du même code, " Le droit à communication ne s'applique qu'à des documents achevés. Le droit à communication ne concerne pas les documents préparatoires à une décision administrative tant qu'elle est en cours d'élaboration ". <br/>
<br/>
              3. Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de contrôler la régularité et le bien-fondé d'une décision de refus de communication de documents administratifs sur le fondement des dispositions, citées au point 2, des articles L. 311-1 et L. 311-2 du code des relations entre le public et l'administration. Pour ce faire, par exception au principe selon lequel le juge de l'excès de pouvoir apprécie la légalité d'un acte administratif à la date de son édiction, il appartient au juge, eu égard à la nature des droits en cause et à la nécessité de prendre en compte l'écoulement du temps et l'évolution des circonstances de droit et de fait afin de conférer un effet pleinement utile à son intervention, de se placer à la date à laquelle il statue. <br/>
<br/>
              4. Il ressort des énonciations du jugement attaqué que si la requérante soutenait que l'annulation, par un arrêt de la cour administrative d'appel de Nancy en date du 29 mars 2018, de l'arrêté du préfet du Bas-Rhin déclarant d'utilité publique les acquisitions et travaux nécessaires à la création de la ZAC Jean-Monnet, avait fait perdre aux documents demandés leur caractère préparatoire, le motif d'annulation retenu par la cour, qui portait sur l'absence dans l'arrêté attaqué de la disposition faisant obligation au maître d'ouvrage de participer financièrement à l'installation de M. et Mme A... dans une nouvelle exploitation agricole à la suite de l'expropriation de leurs terrains, n'imposait pas à l'Eurométropole de Strasbourg d'abandonner son projet de ZAC. Par suite, en jugeant que cette décision juridictionnelle d'annulation était sans incidence sur l'appréciation du caractère communicable des documents en litige, le tribunal administratif n'a pas commis d'erreur de droit. Si la requérante se prévaut en cassation des délibérations du conseil de l'Eurometropole de Strasbourg en date des 29 novembre 2019 et 20 novembre 2020 décidant l'abandon du projet de ZAC, ces éléments, produits pour la première fois en cassation, ne peuvent qu'être écartés. <br/>
<br/>
              Sur les conclusions dirigées contre le jugement du 27 mars 2019 en tant qu'il statue sur la communication des informations environnementales :<br/>
              5. L'article premier de la directive 2003/4/CE du Parlement européen et du Conseil du 28 janvier 2003 concernant l'accès du public à l'information en matière d'environnement et abrogeant la directive 90/313/CEE du Conseil dispose que cette directive a " pour objectifs: a) de garantir le droit d'accès aux informations environnementales détenues par les autorités publiques ou pour leur compte et de fixer les conditions de base et les modalités pratiques de son exercice, et / b) de veiller à ce que les informations environnementales soient d'office rendues progressivement disponibles et diffusées auprès du public afin de parvenir à une mise à disposition et une diffusion systématiques aussi larges que possible des informations environnementales auprès du public. À cette fin, il convient de promouvoir l'utilisation, entre autres, des technologies de télécommunication informatique et/ou des technologies électroniques, lorsqu'elles sont disponibles ". Aux termes de son article 2, " Aux fins de la présente directive, on entend par: 1) "information environnementale": toute information disponible sous forme écrite, visuelle, sonore, électronique ou toute autre forme matérielle, concernant: a) l'état des éléments de l'environnement, tels que l'air et l'atmosphère, l'eau, le sol, les terres, les paysages et les sites naturels, y compris les biotopes humides, les zones côtières et marines, la diversité biologique et ses composantes, y compris les organismes génétiquement modifiés, ainsi que l'interaction entre ces éléments ; b) des facteurs, tels que les substances, l'énergie, le bruit, les rayonnements ou les déchets, y compris les déchets radioactifs, les émissions, les déversements et autres rejets dans l'environnement, qui ont ou sont susceptibles d'avoir des incidences sur les éléments de l'environnement visés au point a); c) les mesures (y compris les mesures administratives), telles que les politiques, les dispositions législatives, les plans, les programmes, les accords environnementaux et les activités ayant ou susceptibles d'avoir des incidences sur les éléments et les facteurs visés aux points a) et b), ainsi que les mesures ou activités destinées à protéger ces éléments (...) ". Ces dispositions ont été transposées en droit interne par l'article L. 124-1 du code de l'environnement aux termes duquel : " Le droit de toute personne d'accéder aux informations relatives à l'environnement détenues, reçues ou établies par les autorités publiques mentionnées à l'article L. 124-3 ou pour leur compte s'exerce dans les conditions définies par les dispositions du titre Ier du  titre Ier du livre III du code  des relations entre le public et l'administration,  sous réserve des dispositions du présent chapitre " et par l'article L. 124-2 du même code qui prévoit que : " Est considérée comme information relative à l'environnement au sens du présent chapitre toute information disponible, quel qu'en soit le support, qui a pour objet : 1° L'état des éléments de l'environnement, notamment l'air, l'atmosphère, l'eau, le sol, les terres, les paysages, les sites naturels, les zones côtières ou marines et la diversité biologique, ainsi que les interactions entre ces éléments ; 2° Les décisions, les activités et les facteurs, notamment les substances, l'énergie, le bruit, les rayonnements, les déchets, les émissions, les déversements et autres rejets, susceptibles d'avoir des incidences sur l'état des éléments visés au 1° ; (...) ".<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que parmi les documents dont la communication est demandée par la requérante figurent des documents émanant des candidats qui ont pour objet d'indiquer les moyens mis en oeuvre par les futurs aménageurs pour répondre aux objectifs à atteindre en matière environnementale. Le cahier des charges et le règlement de la consultation précisent notamment que les candidats aménageurs doivent fournir au jury de l'Eurométropole de Strasbourg dès la première phase du processus de sélection une note explicitant leur parti pris environnemental qui sera ensuite actualisée au fur et à mesure de l'avancement du projet. La production de ces documents s'inscrit ainsi dans le cadre de la procédure de choix par l'Eurométropole de Strasbourg d'un aménageur de la ZAC. Mais tant que cette sélection n'a pas conduit à la conclusion d'un contrat avec un aménageur, les informations relatives à l'environnement qu'ils contiennent ne sauraient, à ce stade, être regardées comme ayant pour objet des décisions ou des activités susceptibles d'avoir des incidences sur l'état des éléments de l'environnement, au sens des dispositions citées au point 5 du 2° de l'article L. 124-2 du code de l'environnement. Par suite, en jugeant que les documents demandés ne pouvaient être regardés comme contenant des informations relatives à l'environnement au sens de cet article L. 124-1 du code de l'environnement, le tribunal administratif n'a commis ni erreur de droit, ni erreur de qualification juridique des faits. <br/>
              7. Il résulte de tout ce qui précède que la requérante n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP JCP Caston, avocat de Mme A....  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A... est rejeté. <br/>
Article 2 : La présente décision sera notifiée Mme C... A... et à l'Eurométropole de Strasbourg. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. CONTENTIEUX. - REFUS DE COMMUNICATION DE DOCUMENTS ADMINISTRATIFS (ART. L. 311-1 ET L. 311-2 DU CRPA) - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - APPRÉCIATION À LA DATE À LAQUELLE LE JUGE STATUE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX INFORMATIONS EN MATIÈRE D'ENVIRONNEMENT. - INFORMATIONS EN MATIÈRE ENVIRONNEMENTALE (ART. L. 124-2 DU CODE DE L'ENVIRONNEMENT) - CHAMP - INFORMATIONS RELATIVES À L'ENVIRONNEMENT FIGURANT DANS LES OFFRES DES CANDIDATS À L'AMÉNAGEMENT D'UNE ZAC - EXCLUSION, TANT QUE LA SÉLECTION DES CANDIDATS N'A PAS CONDUIT À LA CONCLUSION D'UN CONTRAT AVEC UN AMÉNAGEUR.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-006 NATURE ET ENVIRONNEMENT. - INFORMATIONS EN MATIÈRE ENVIRONNEMENTALE (ART. L. 124-2 DU CODE DE L'ENVIRONNEMENT) - CHAMP - INFORMATIONS RELATIVES À L'ENVIRONNEMENT FIGURANT DANS LES OFFRES DES CANDIDATS À L'AMÉNAGEMENT D'UNE ZAC - EXCLUSION, TANT QUE LA SÉLECTION DES CANDIDATS N'A PAS CONDUIT À LA CONCLUSION D'UN CONTRAT AVEC UN AMÉNAGEUR.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. - APPRÉCIATION À LA DATE À LAQUELLE LE JUGE STATUE [RJ1] - REFUS DE COMMUNICATION DE DOCUMENTS ADMINISTRATIFS (ART. L. 311-1 ET L. 311-2 DU CRPA).
</SCT>
<ANA ID="9A"> 26-06-01-04 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de contrôler la régularité et le bien-fondé d'une décision de refus de communication de documents administratifs sur le fondement des articles L. 311-1 et L. 311-2 du code des relations entre le public et l'administration (CRPA). Pour ce faire, par exception au principe selon lequel le juge de l'excès de pouvoir apprécie la légalité d'un acte administratif à la date de son édiction, il appartient au juge, eu égard à la nature des droits en cause et à la nécessité de prendre en compte l'écoulement du temps et l'évolution des circonstances de droit et de fait afin de conférer un effet pleinement utile à son intervention, de se placer à la date à laquelle il statue.</ANA>
<ANA ID="9B"> 26-06-04 Lancement, dans le cadre de la création d'une ZAC, d'une consultation pour sélectionner un groupement d'opérateurs auquel des terrains seront cédés en vue d'y réaliser une opération d'aménagement.... ,,Tant que la sélection des candidats n'a pas conduit à la conclusion d'un contrat avec un aménageur, les informations relatives à l'environnement que contiennent les documents émanant des candidats qui ont pour objet d'indiquer les moyens mis en oeuvre par les futurs aménageurs pour répondre aux objectifs à atteindre en matière environnementale ne sauraient, à ce stade, être regardées comme ayant pour objet des décisions ou des activités susceptibles d'avoir des incidences sur l'état des éléments de l'environnement, au sens du 2° de l'article L. 124-2 du code de l'environnement.</ANA>
<ANA ID="9C"> 44-006 Lancement, dans le cadre de la création d'une ZAC, d'une consultation pour sélectionner un groupement d'opérateurs auquel des terrains seront cédés en vue d'y réaliser une opération d'aménagement.,,,Tant que la sélection des candidats n'a pas conduit à la conclusion d'un contrat avec un aménageur, les informations relatives à l'environnement que contiennent les documents émanant des candidats qui ont pour objet d'indiquer les moyens mis en oeuvre par les futurs aménageurs pour répondre aux objectifs à atteindre en matière environnementale ne sauraient, à ce stade, être regardées comme ayant pour objet des décisions ou des activités susceptibles d'avoir des incidences sur l'état des éléments de l'environnement, au sens du 2° de l'article L. 124-2 du code de l'environnement.</ANA>
<ANA ID="9D"> 54-07-02 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de contrôler la régularité et le bien-fondé d'une décision de refus de communication de documents administratifs sur le fondement des articles L. 311-1 et L. 311-2 du code des relations entre le public et l'administration (CRPA). Pour ce faire, par exception au principe selon lequel le juge de l'excès de pouvoir apprécie la légalité d'un acte administratif à la date de son édiction, il appartient au juge, eu égard à la nature des droits en cause et à la nécessité de prendre en compte l'écoulement du temps et l'évolution des circonstances de droit et de fait afin de conférer un effet pleinement utile à son intervention, de se placer à la date à laquelle il statue.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]. Rappr., s'agissant d'un refus de déréférencement, CE, 6 décembre 2019, Mme X., n° 391000, T. pp. 750-946 ; s'agissant d'un refus de consultation anticipée d'archives du Président de la République et des membres du gouvernement (art. L. 213-4 du code du patrimoine), CE, Assemblée, 12 juin 2020, M.,, 422327 431026, p. 213.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
