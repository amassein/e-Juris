<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043424671</ID>
<ANCIEN_ID>JG_L_2021_04_000000438766</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/42/46/CETATEXT000043424671.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 23/04/2021, 438766, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438766</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438766.20210423</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 5 février 2018 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a, sur le fondement des dispositions du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, mis fin au statut de réfugié qui lui avait été reconnu et de le rétablir dans ce statut.<br/>
<br/>
              Par une décision n° 18009632 du 17 décembre 2019, la Cour nationale du droit d'asile a annulé cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 février et 6 mai 2020 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 31 janvier 1967 relatifs aux réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Clément Tonon, auditeur, <br/>
<br/>
              - Les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'OFPRA, et à la SCP Bouzidi, Bouhanna, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge du fond que M. B..., ressortissant de la République démocratique du Congo, s'est vu reconnaître la qualité de réfugié par une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) du 5 mars 2003. L'OFPRA se pourvoit en cassation contre la décision du 17 décembre 2019 par laquelle la Cour nationale du droit d'asile a annulé sa décision du 5 février 2018 mettant fin au statut de réfugié de l'intéressé sur le fondement du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile et l'a rétabli dans le statut de réfugié.<br/>
<br/>
              2.	L'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige, dispose que : " Le statut de réfugié peut être refusé ou il peut être mis fin à ce statut lorsque : (...) 2° La personne concernée a été condamnée en dernier ressort en France soit pour un crime, soit pour un délit constituant un acte de terrorisme ou puni de dix ans d'emprisonnement, et sa présence constitue une menace grave pour la société ". Il résulte de ces dispositions que la possibilité de refuser le statut de réfugié ou d'y mettre fin, qui est sans incidence sur le fait que l'intéressé a ou conserve la qualité de réfugié dès lors qu'il en remplit les conditions, est subordonnée à deux conditions cumulatives. Il appartient à l'OFPRA et, en cas de recours, à la Cour nationale du droit d'asile, d'une part, de vérifier si l'intéressé a fait l'objet de l'une des condamnations que visent les dispositions précitées et, d'autre part, d'apprécier si sa présence sur le territoire français est de nature à constituer, à la date de leur décision, une menace grave pour la société au sens des dispositions précitées, c'est-à-dire si elle est de nature à affecter un intérêt fondamental de la société, compte tenu des infractions pénales commises - lesquelles ne sauraient, à elles seules, justifier légalement une décision refusant le statut de réfugié ou y mettant fin - et des circonstances dans lesquelles elles ont été commises, mais aussi du temps qui s'est écoulé et de l'ensemble du comportement de l'intéressé  depuis la commission des infractions ainsi que de toutes les circonstances  pertinentes à la date à laquelle ils statuent.<br/>
<br/>
              3.	Il ressort des énonciations de la décision de la Cour nationale du droit d'asile que M. B... a été condamné, par un arrêt du 31 mars 2016, devenu définitif, de la cour d'assises de l'Essonne, à douze ans d'emprisonnement et à quatre ans de suivi socio-judiciaire pour viol, commis par un ascendant, courant 2012, et pour menace de mort et violence sans incapacité sur la victime, commises de mars à juin 2013. La Cour a également relevé qu'il avait fait l'objet de condamnations antérieures par les tribunaux correctionnels d'Amiens et Evry pour des faits de vol, de circulation avec un véhicule terrestre à moteur sans assurance, de conduite sans permis et d'escroquerie. En se bornant à relever, après avoir rappelé ces différentes condamnations, que M. B... avait déclaré à l'audience avoir pris conscience de son crime et de la gravité de l'acte commis envers sa fille, être bien inséré dans la prison au sein de laquelle il occupe un emploi, faire l'objet d'un suivi psychologique, bénéficier de permissions de sortie et souhaiter retrouver, à sa sortie, un emploi et sa place auprès de ses enfants et de sa femme, pour en déduire que sa présence en France ne constituait pas, à la date de sa décision, une menace grave pour la société au sens du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, la cour a entaché sa décision d'inexacte qualification juridique des faits. <br/>
<br/>
              4.	Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées en défense au titre de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er :  La décision de la Cour nationale du droit d'asile du 17 décembre 2017 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
<br/>
Article 3 : Les conclusions présentées au titre de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
