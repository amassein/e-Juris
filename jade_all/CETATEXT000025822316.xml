<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025822316</ID>
<ANCIEN_ID>JG_L_2012_05_000000339036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/82/23/CETATEXT000025822316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 04/05/2012, 339036, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:339036.20120504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision du 4 mai 2012 par laquelle le Conseil d'Etat, statuant au contentieux sur la requête enregistrée sous le n° 339036, présentée par la Confédération paysanne et tendant à l'annulation pour excès de pouvoir des paragraphes 2, 3 et 4 de l'article 1er de l'arrêté du ministre de l'alimentation, de l'agriculture et de la pêche du 23 février 2010 modifiant l'arrêté du 20 novembre 2006 portant application du décret n° 2006-710 du 19 juin 2006 relatif à la mise en oeuvre de l'aide au revenu prévue par le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir :<br/>
<br/>
              1°) si les paragraphes 1 et 5 de l'article 40 du règlement n° 1782/2003 du Conseil du 29 septembre 2003 autorisent les Etats membres, eu égard à leurs termes, mais aussi à leur finalité, à fonder le droit à revalorisation du montant de référence des agriculteurs dont la production a été gravement affectée en raison des engagements agroenvironnementaux auxquels ils ont été soumis pendant tout ou partie de la période de référence sur la comparaison entre les montants des paiements directs perçus pendant les années affectées par de tels engagements et ceux qui ont été perçus pendant des années non affectées ;<br/>
<br/>
              2°) si les paragraphes 2 et 5 de ce même article autorisent les Etats membres à fonder le droit à revalorisation du montant de référence des agriculteurs dont la production a été gravement affectée en raison des engagements agroenvironnementaux auxquels ils ont été soumis pendant la totalité de la période de référence sur la comparaison entre le montant de paiements directs perçu lors de la dernière année non affectée par un engagement agroenvironnemental, y compris si cette année est antérieure de huit ans à la période de référence, et le montant moyen annuel de paiements directs perçu pendant la période de référence ;<br/>
<br/>
              Vu l'arrêt n° C-298/12 du 3 octobre 2013 par lequel la Cour de justice de l'Union européenne s'est prononcée sur ces questions ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 ;<br/>
<br/>
              Vu le décret n° 2006-710 du 19 juin 2006 ;<br/>
<br/>
              Vu l'arrêté du ministre de l'agriculture et de la pêche du 20 novembre 2006 modifié portant application du décret n° 2006-710 du 19 juin 2006 relatif à la mise en oeuvre de l'aide au revenu prévue par le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, que le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 établissant des règles communes pour le régime de soutien direct dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs a institué un régime de paiement unique calculé sur la base d'un montant de référence ; qu'en vertu des articles 37 et 38 de ce règlement, le montant de référence est égal à la moyenne annuelle des montants totaux d'aides accordés à un agriculteur au cours d'une période de référence correspondant aux trois années 2000 à 2002 ; qu'aux termes de l'article 40 de ce règlement, dans sa rédaction en vigueur à la date de l'arrêté litigieux : " 1. Par dérogation à l'article 37, tout agriculteur dont la production a été gravement affectée au cours de la période de référence par un cas de force majeure ou des circonstances exceptionnelles survenus avant ou pendant ladite période de référence est habilité à demander que le montant de référence soit calculé sur la base de l'année ou des années civiles de la période de référence qui n'ont pas été affectées par le cas de force majeure ou les circonstances exceptionnelles. / 2. Si la totalité de la période de référence a été affectée par le cas de force majeure ou les circonstances exceptionnelles, l'Etat membre calcule le montant de référence sur la base de la période 1997 à 1999. / (...) 5. Les paragraphes 1 [et] 2 (...) du présent article s'appliquent mutatis mutandis aux agriculteurs soumis, au cours de la période de référence, à des engagements agroenvironnementaux au titre des règlements (CEE) n° 2078/92 et (CE) n° 1257/1999 (...). / Au cas où les mesures visées au premier alinéa couvrent à la fois la période de référence et la période visée au paragraphe 2 du présent article, les Etats membres fixent, selon des critères objectifs et de manière à assurer l'égalité de traitement entre les agriculteurs et à éviter des distorsions du marché et de la concurrence, un montant de référence (...). " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes des neuvième, dixième et douzième alinéas de l'article 1er du décret du 19 juin 2006 relatif à la mise en oeuvre de l'aide au revenu prévue par le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003, dans sa rédaction en vigueur à la date du litige : " Pour l'application du 5 de l'article 40 du règlement du 29 septembre 2003 susvisé, ne peuvent être pris en compte que les engagements agroenvironnementaux dont la liste est fixée par arrêté du ministre chargé de l'agriculture et qui (...) ont conduit à une diminution au moins équivalente à 20 % : / (...) du montant d'aides perçu au titre des années affectées, calculé selon des modalités fixées par ce même arrêté, par rapport à celui versé au titre des années de la période de référence non affectées ; / (...) Lorsqu'un (...) engagement agro-environnemental affecte toutes les années (...) de la période de référence (...) et conduit (...) à une diminution du montant des aides (...), un arrêté du ministre chargé de l'agriculture définit les modalités de calcul de cette diminution. (...) " ;<br/>
<br/>
              3. Considérant que, par un arrêté du 23 février 2010, le ministre de l'alimentation, de l'agriculture et de la pêche a modifié l'arrêté du 20 novembre 2006 relatif à la mise en oeuvre de l'aide au revenu prévue par le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 et portant application du décret du 19 juin 2006 ; que le paragraphe 2 de l'article 1er de cet arrêté, modifiant l'article 5 de l'arrêté du 20 novembre 2006, étend aux années non affectées par des engagements agroenvironnementaux antérieures à 2000 la liste des aides, figurant à l'article 1er de cet arrêté, prises en compte pour la comparaison des montants d'aides perçus par les agriculteurs soumis à des engagements agroenvironnementaux ; que le paragraphe 3 du même article, modifiant l'article 6 de l'arrêté du 20 novembre 2006, fixe, pour les agriculteurs soumis à des engagements agroenvironnementaux pendant une ou deux des trois années de la période de référence, les règles de comparaison du montant d'aides perçu respectivement lors des années non affectées et lors des années affectées par de tels engagements et, dans le cas où le second montant est inférieur d'au moins 20% au premier montant, les modalités de calcul de la revalorisation du montant de référence ; que le paragraphe 4 du même article, modifiant l'article 7 de l'arrêté du 20 novembre 2006, fixe, pour les agriculteurs soumis à des engagements agroenvironnementaux pendant les trois années de la période de référence, les règles de comparaison du montant d'aides perçu lors de la dernière année non affectée par de tels engagements, laquelle peut remonter jusqu'à 1992, et du montant moyen d'aides perçu lors de la période de référence et, dans le cas où le second montant est inférieur d'au moins 20 % au premier montant, les modalités de calcul de la revalorisation du montant de référence ; que la Confédération paysanne demande l'annulation pour excès de pouvoir des paragraphes 2 à 4 de l'article 1er de cet arrêté ;<br/>
<br/>
              4. Considérant que, dans l'arrêt du 3 octobre 2013 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit, en premier lieu, que l'article 40, paragraphe 5, premier alinéa, du règlement n° 1782/2003 doit être interprété en ce sens que tout agriculteur, du seul fait d'avoir été soumis, au cours de la période de référence, à des engagements agroenvironnementaux au titre des règlements nos 2078/92 et 1257/1999, est habilité à demander que son montant de référence soit calculé sur la base de l'année ou des années civiles de la période de référence non soumises à de tels engagements et, en second lieu, que l'article 40, paragraphe 5, deuxième alinéa, du même règlement doit être interprété en ce sens que tout agriculteur, du seul fait d'avoir été soumis, au cours de la période de 1997 à 2002, à de tels engagements, est habilité à demander que son montant de référence soit calculé sur la base de critères objectifs et de manière à assurer l'égalité de traitement entre les agriculteurs et à éviter des distorsions du marché et de la concurrence, ce qu'il appartiendra à la juridiction de renvoi de vérifier ;<br/>
<br/>
              5. Considérant qu'il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que, d'une part, s'agissant des agriculteurs soumis à des engagements agroenvironnementaux pendant une partie de la période de 1997 à 2002, dès lors que leur droit à revalorisation du montant de référence, calculé sur la base de l'année ou des années civiles de la période de référence non soumises à de tels engagements, résulte du seul fait d'avoir été soumis au cours de cette période à de tels engagements, les neuvième et dixième alinéas de l'article 1er du décret du 19 juin 2006 et les dispositions litigieuses de l'arrêté du 20 novembre 2006 n'ont pu légalement subordonner ce droit à une comparaison du montant d'aides perçu respectivement au titre des années de la période affectées et au titre des années de la période non affectées par de tels engagements faisant apparaître une diminution au moins équivalente à 20% ; que, d'autre part, s'agissant du droit à revalorisation du montant de référence des agriculteurs soumis à des engagements agroenvironnementaux pendant toute la période de 1997 à 2002, le dispositif institué par les neuvième et dixième alinéas de l'article 1er du décret du 19 juin 2006 et les dispositions litigieuses de l'arrêté du 20 novembre 2006 qui le mettent en oeuvre ne peut être regardé, pour le même motif, comme de nature à garantir l'égalité de traitement entre tous les agriculteurs alors, au surplus, qu'il ressort des pièces du dossier que le montant des aides prises en compte pour la comparaison des montants d'aides a plus que décuplé entre 1992 et 1996 avant de se stabiliser entre 1996 et 2002 et que, par suite, la probabilité d'une diminution équivalente au moins à 20 % du montant d'aides au cours de la période pendant laquelle l'agriculteur est soumis à des engagements agroenvironnementaux par rapport au montant d'aides perçu au cours d'une année antérieure à 1997 pouvant servir de période de référence, est très faible ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen de la requête, la Confédération paysanne est fondée à demander l'annulation pour excès de pouvoir des paragraphes 2 à 4 de l'article 1er de l'arrêté attaqué ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les paragraphes 2 à 4 de l'article 1er de l'arrêté du ministre de l'alimentation, de l'agriculture et de la pêche du 23 février 2010 modifiant l'arrêté du 20 novembre 2006 portant application du décret n° 2006-710 du 19 juin 2006 relatif à la mise en oeuvre de l'aide au revenu prévue par le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 sont annulés.<br/>
Article 2 : La présente décision sera notifiée à la Confédération paysanne, au Premier ministre, au ministre de l'économie et des finances et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
