<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031523039</ID>
<ANCIEN_ID>JG_L_2015_11_000000369236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/52/30/CETATEXT000031523039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 09/11/2015, 369236, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369236.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 juin et 11 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Arkema France, dont le siège est 420, rue d'Estienne d'Orves à Colombes (92705) ; la société Arkema France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY02991 du 11 avril 2013 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0704909 du 29 septembre 2011 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de l'arrêté du préfet du Rhône du 14 mai 2007 lui imposant des prescriptions complémentaires relatives au suivi et à la gestion de la pollution au trichloréthane affectant le site du quai Louis Aulagne à Saint-Fons ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ; <br/>
              - le code de commerce ; <br/>
              - le code de l'environnement ; <br/>
              - la loi n° 76-663 du 19 juillet 1976 ; <br/>
              - le décret n° 77-113 du 21 septembre 1977 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Arkema France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 14 mai 2007, le préfet du Rhône a prescrit à la société Arkema France, venue aux droits de la société Elf Atochem, plusieurs mesures relatives au suivi et à la gestion de la pollution historique au trichlorétane de l'ancien site Péchiney Saint-Gobain de Saint-Fons ; que, par un jugement du 29 septembre 2011, le tribunal administratif de Lyon a rejeté la demande de la société tendant à l'annulation de cet arrêté ; que par un arrêt du 11 avril 2013, contre lequel la société Arkema France se pourvoit en cassation, la cour administrative d'appel de Lyon a rejeté la requête de cette société contre ce jugement ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 12 juin 2002 devenu définitif, le tribunal administratif de Lyon a annulé, à la demande de la société Elf Atochem, l'arrêté du 15 juillet 1999 par lequel le préfet du Rhône a mis en demeure cette société, aux droits de laquelle est venue la société Arkema France requérante, d'achever dans un délai de trois mois le diagnostic de la pollution au trichlorétane dans le secteur sud de son établissement de Saint-Fons et de proposer des travaux préventifs ou curatifs, ainsi que l'avait prescrit un arrêté du 10 janvier 1994 ; que ce jugement était motivé par les circonstances que la société Elf Atochem ne pouvait être destinataire des prescriptions de remise en état du site, dès lors que son activité n'était pas à l'origine de cette pollution et que ni elle-même ni les sociétés aux droits de laquelle elle était venue n'avaient été régulièrement substituées à l'exploitant à l'origine de la pollution historique ; <br/>
<br/>
              3. Considérant que, pour écarter l'exception tirée de l'autorité de chose jugée par ce jugement qui était opposée par la société Arkema France, la cour a notamment relevé qu'un nouveau foyer de pollution avait été détecté en juillet 2006 et que les prescriptions de l'arrêté attaqué du 14 mai 2007, édictées à la suite de diverses mesures et campagnes d'analyses, différaient de celles précédemment imposées par l'arrêté du 15 juillet 1999 ; qu'elle en a déduit que la demande de la société Arkema France tendant à l'annulation de l'arrêté du 14 mai 2007 n'avait pas le même objet que la demande ayant donné lieu à l'intervention du jugement du tribunal administratif de Lyon du 12 juin 2002 ; qu'en statuant ainsi, la cour n'a pas entaché son arrêt d'une erreur de qualification juridique des faits de l'espèce ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte de l'ensemble des dispositions de la loi du 19 juillet 1976, reprises aux articles L. 511-1 et suivants du code de l'environnement, que l'obligation de remise en état du site est applicable aux installations de la nature de celles soumises à autorisation en application du titre 1er du livre V du code de l'environnement, alors même qu'elles auraient cessé d'être exploitées avant l'entrée en vigueur de la loi du 19 juillet 1976, dès lors que ces installations demeurent susceptibles de présenter les dangers ou inconvénients énumérés à l'article L. 511-1 de ce code; que, dans cette hypothèse, l'obligation de remise en état du site imposée par l'article 34-1 du décret du 21 septembre 1977, alors en vigueur, aujourd'hui repris à l'article R. 512-39-1 du code de l'environnement, pèse sur l'ancien exploitant ou, si celui-ci a disparu, sur son ayant droit ; que lorsque l'exploitant ou son ayant droit a cédé le site à un tiers, cette cession ne l'exonère de ses obligations que si le cessionnaire s'est substitué à lui en qualité d'exploitant ;<br/>
<br/>
              5. Considérant, pour juger que la société Arkema France avait la qualité d'ayant droit du dernier exploitant du site de Saint-Fons, la cour administrative d'appel de Lyon s'est fondée sur les stipulations du traité d'apport partiel d'actifs du 20 octobre 1980 par lequel la société Rhône Poulenc Industrie a apporté à la société Activités Chimiques, aux droits de laquelle est venue, en dernier lieu, la société Arkema France, l'ensemble des biens, droits et éléments de passif se rapportant à la fabrication, au traitement, à la transformation, au transport, à la recherche, à l'expérimentation et à la vente de produits chimiques, au nombre desquels figurent les activités relatives au chlorure de vinyle monomère à l'origine de la pollution du site de Saint-Fons ; que la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit en jugeant que la société Activités Chimiques, aux droits de laquelle est venue, en dernier lieu, la société Arkema France, avait été substituée à la société Rhône Poulenc par l'effet de ce traité d'apport partiel d'actifs, placé sous le régime des scissions ; que c'est par une souveraine appréciation des pièces du dossier, qui n'est pas entachée de dénaturation, que la cour a estimé que ce traité couvrait l'activité d'enfouissement de déchets chlorés ; <br/>
<br/>
              6. Considérant, en dernier lieu, que pour écarter comme inopérant le moyen tiré de ce que l'arrêté litigieux ne pouvait légalement imposer la réalisation d'un plan de gestion, alors que les éléments de diagnostic n'étaient pas connus, la cour administrative d'appel de Lyon s'est fondée sur la circonstance que l'arrêté attaqué ne prescrivait à la société de proposer un plan de gestion qu'à l'issue du diagnostic approfondi du site et de l'interprétation des milieux ; qu'eu égard à la portée de cette prescription, la cour n'a pas, en statuant ainsi, entaché son arrêt d'erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société Arkema France n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que son pourvoi doit être rejeté, y compris les conclusions qu'elle a présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la société Arkema France est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Arkema France et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
