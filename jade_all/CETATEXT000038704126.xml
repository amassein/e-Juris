<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704126</ID>
<ANCIEN_ID>JG_L_2019_06_000000431425</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/41/CETATEXT000038704126.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 28/06/2019, 431425, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431425</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:431425.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Cap d'Ail a demandé au tribunal administratif de Nice d'ordonner, sur le fondement des dispositions de l'article L. 521-3 du code de justice administrative, l'expulsion de la société à responsabilité limitée (SARL) Cap Marquet de la dépendance du domaine public constituée par le lot de plage n° 1 de la convention de délégation de service public n° 2010-01, qu'elle occupe sans droit ni titre. Par une ordonnance n° 1901825 du 23 mai 2019, le juge des référés du tribunal administratif de Nice a enjoint à la société Cap Marquet d'évacuer cette dépendance dans un délai de vingt et un jours à compter de la notification de son ordonnance, sous astreinte de 300 euros par jour de retard. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 et 17 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la société Cap Marquet demande au Conseil d'Etat :<br/>
<br/>
              1°) de prononcer le sursis à exécution de cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Cap d'Ail la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Cap Marquet et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Cap d'Ail ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 18 et 19 juin 2019, présentées par la société Cap Marquet ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 juin 2019, présentée par la commune de Cap d'Ail. <br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 821.5 du code de justice administrative, la formation de jugement peut, à la demande de l'auteur d'un pourvoi en cassation, " ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond. / A tout moment, il peut être mis fin par une formation de jugement au sursis qui avait été accordé ".<br/>
<br/>
              2. Pour demander qu'il soit sursis à l'exécution de l'ordonnance du 23 mai 2019, la société Cap Marquet soutient que le juge des référés du tribunal administratif de Nice n'était pas compétent pour connaître des demandes présentées par la commune de Cap d'Ail et qu'il a entaché son ordonnance d'erreur de droit, de dénaturation des pièces du dossier et d'insuffisance de motivation en jugeant que la mesure sollicitée remplissait les conditions d'urgence, d'utilité et d'absence de contestation sérieuse prévues par l'article L. 521-3 du code de justice administrative. <br/>
<br/>
              3. Ces moyens ne sont, en l'état de l'instruction, pas de nature à justifier, outre l'annulation de l'ordonnance attaquée, l'infirmation de la solution retenue par le juge des référés du tribunal administratif de Nice. <br/>
<br/>
              4. Par suite, sans qu'il soit besoin de se prononcer sur l'existence de conséquences difficilement réparables, les conclusions à fin de sursis à exécution de l'ordonnance attaquée ne peuvent qu'être rejetées.  <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Cap d'Ail qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par cette commune au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Cap Marquet est rejetée.<br/>
Article 2 : Les conclusions de la commune de Cap d'Ail tendant à la mise en oeuvre des dispositions de l'article L.761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la société à responsabilité limitée Cap Marquet, à la commune de Cap d'Ail, à la Métropole Nice Côte d'Azur et au ministre de la cohésion des territoires. <br/>
		Copie en sera adressée au préfet des Alpes-Maritimes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
