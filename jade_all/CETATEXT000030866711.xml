<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866711</ID>
<ANCIEN_ID>JG_L_2015_07_000000383373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/67/CETATEXT000030866711.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 10/07/2015, 383373, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383373.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Versailles d'annuler la décision du 21 juin 2010 par laquelle la caisse nationale de retraites des agents des collectivités locales (CNRACL) a rejeté sa demande de départ à la retraite anticipée en qualité de fonctionnaire ayant plus de quinze ans de service et de mère de trois enfants, et à ce qu'il soit enjoint à la CNRACL de réexaminer sa demande en la faisant bénéficier de la jouissance immédiate à la retraite en application des dispositions de l'article L. 24 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              Par un jugement n° 1008117 du 25 février 2014, le tribunal administratif de Versailles a rejeté la demande de MmeA....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 août, 3 novembre 2014 et 24 juin 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de la CNRACL le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ; <br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de Mme A...et à la SCP Odent, Poulet, avocat de la caisse nationale de retraites des agents des collectivités locales ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, que, le 24 février 2010, MmeA..., fonctionnaire territoriale, a sollicité un départ anticipé à la retraite avec jouissance immédiate du droit de pension, sur le fondement de l'article 25 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la caisse nationale de retraites des agents des collectivités locales ; que, par une décision du 21 juin 2010, le directeur général de la caisse nationale de retraites des agents des collectivités locales a rejeté sa demande, au motif que l'intéressée, mère de trois enfants, ne remplissait pas les conditions requises par ce texte faute d'avoir interrompu son activité, ni au moment de l'arrivée au foyer des deux enfants de son conjoint, ni postérieurement à la date à laquelle elle les a adoptés ; que Mme A...se pourvoit en cassation contre le jugement du 25 février 2014 par lequel le tribunal administratif de Versailles a rejeté ses conclusions dirigées contre cette décision ; <br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article 25 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la caisse nationale de retraites des agents des collectivités locales : " Les dispositions du I de l'article L. 24 du code des pensions civiles et militaires de retraite s'appliquent aux fonctionnaires territoriaux " ; qu'aux termes du 3° du I de l'article 24 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable au litige : " I. - La liquidation de la pension intervient : (...) 3° Lorsque le fonctionnaire civil est parent de trois enfants vivants, ou décédés par faits de guerre, ou d'un enfant vivant, âgé de plus d'un an et atteint d'une invalidité égale ou supérieure à 80 %, à condition qu'il ait, pour chaque enfant, interrompu son activité dans des conditions fixées par décret en Conseil d'Etat. Sont assimilées à l'interruption d'activité mentionnée à l'alinéa précédent les périodes n'ayant pas donné lieu à cotisation obligatoire dans un régime de retraite de base, dans des conditions fixées par décret en Conseil d'Etat. Sont assimilés aux enfants mentionnés au premier alinéa les enfants énumérés au II de l'article L. 18 que l'intéressé a élevés dans les conditions prévues au III dudit article " ; qu'en vertu des I et II de l'article R. 37 du même code, applicables au litige, le bénéfice des dispositions précitées du 3° du I de l'article L. 24 est subordonné à une interruption d'activité d'une durée continue au moins égale à deux mois dans le cadre d'un congé pour maternité, d'un congé pour adoption, d'un congé parental, d'un congé de présence parentale, ou d'une disponibilité pour élever un enfant de moins de huit ans ; que l'article R. 13 du même code pose les mêmes conditions d'interruption d'activité pour l'octroi du bénéfice de bonification pour enfant prévu au b de l'article L. 12 du même code ; <br/>
<br/>
              3. Considérant, en premier lieu, que le moyen tiré de ce que la condition d'interruption d'activité de deux mois posée à l'article R. 37 du code des pensions civiles et militaires de retraite pour l'octroi du bénéfice en cause et à l'article R. 13 du même code pour l'octroi de la bonification pour enfant prévue à l'article L. 12 du même code méconnaîtrait l'article 157 du traité sur le fonctionnement de l'Union européenne est nouveau en cassation et doit, par suite, être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que si Mme A...soutient avoir présenté devant le tribunal administratif un moyen précisément tiré de ce que la décision litigieuse méconnaîtrait les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il ressort de ses écritures que ces stipulations étaient simplement évoquées à l'appui des moyens tirés de la méconnaissance des stipulations de l'article 14 de cette convention et de celles de l'article 1er du  premier protocole auxquels le tribunal a répondu ; que, par suite, le tribunal n'a, contrairement à ce qui est soutenu, entaché son jugement ni d'erreur de droit ni d'insuffisance de motivation ; <br/>
<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 1er du premier protocole à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. (...) " et que, selon l'article 14 de cette convention : " La jouissance des droits et libertés reconnus dans la présente Convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation. " ; que la loi de finances rectificative du 30 décembre 2004 a modifié l'article L. 24 du code des pensions civiles et militaires de retraite, en conditionnant l'octroi du bénéfice de départ anticipé avec jouissance immédiate à une condition d'interruption d'activité ; que l'article L. 24 du code des pensions civiles et militaires de retraite, en tant qu'il s'applique à des demandes postérieures à l'entrée en vigueur de cette loi, n'a pas d'effet rétroactif et ne saurait dès lors être regardé comme méconnaissant les stipulations précitées de l'article 1er du premier protocole à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que par suite en écartant le moyen que MmeA..., qui sollicitait un départ anticipé avec liquidation à compter du 2 février 2011, date postérieure à l'entrée en vigueur de ces dispositions, tirait de la méconnaissance de ces stipulations, le tribunal administratif n'a commis ni erreur de droit, ni erreur de qualification juridique ; que doit également être écarté, par voie de conséquence, le moyen tiré de l'erreur de droit qu'aurait commise le tribunal administratif en jugeant que l'article L. 24 du même code et les dispositions réglementaires prises pour son application méconnaîtraient les stipulations de l'article 1er du premier protocole à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales combinées avec celles de l'article 14 de la même convention ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de Mme A... doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la caisse nationale de retraites des agents des collectivités locales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
