<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253432</ID>
<ANCIEN_ID>JG_L_2017_12_000000403017</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/34/CETATEXT000036253432.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 20/12/2017, 403017, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403017</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Bertrand Dacosta</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2017:403017.20171220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1612837 du 25 août 2016, enregistrée au secrétariat du contentieux le 30 août 2016, la présidente du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête que M. A...B...avait présentée à ce tribunal.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif le 16 août 2016, et un mémoire complémentaire, enregistré au secrétariat du contentieux du Conseil d'Etat le 29 décembre 2016, M. B...demande : <br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) en date du 16 juin 2016, par laquelle le ministre de l'intérieur lui a refusé l'accès aux données susceptibles de le concerner et figurant dans le traitement automatisé de données du système national d'information Schengen (N-SIS) ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de lui communiquer les informations le concernant figurant dans ce fichier et, le cas échéant, de procéder à l'effacement des données illégalement intégrées, sous astreinte de 100 &#128; par jour de retard ;<br/>
<br/>
              3°) de renvoyer la requête devant le tribunal administratif de Paris pour la partie du traitement qui ne ressortit pas à la compétence de la formation spécialisée du Conseil d'Etat ;<br/>
<br/>
              4°) de condamner l'Etat à lui verser une somme de 3 000 euros en réparation de son préjudice moral et financier ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat le versement à la SCP Garreau Bauer-Violas Feschotte-Desbois, son avocat, d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. A...B..., et d'autre part, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
              - le rapport de M. Bertrand Dacosta, conseiller d'Etat, <br/>
              - et, hors la présence des parties, les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. / Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant. / Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi ". Aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'Etat, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...) Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur ".<br/>
<br/>
              2. L'article 26 de la loi du 6 janvier 1978 dispose que : " I. Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; (...) / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement. / III. Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission (...) ". <br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure, issu de la loi du 24 juillet 2015 relative au renseignement, dispose que : " Le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat ". L'article R. 841-2 du même code prévoit que : " Relèvent des dispositions de l'article L. 841-2 du présent code les traitements ou parties de traitements automatisés de données à caractère personnel intéressant la sûreté de l'Etat autorisés par les actes réglementaires ou dispositions suivants : / (...) / 7° Le 1° de l'article R. 231-3 du code de la sécurité intérieure, pour les seules données mentionnées au 3° de l'article R. 231-8 du même code (...) ". Le 1° de l'article R. 231-3 de code fait référence au " système informatique national dénommé N-SIS II, créé en application des articles 4 du règlement du Parlement européen et du Conseil (CE) n° 1987/2006 et de la décision du Conseil 2007/533/ JAI ". Aux termes de l'article R. 231-8 du même code : " Peuvent être enregistrées dans le traitement N-SIS II, aux seules fins de contrôle discret ou de contrôle spécifique, les données relatives aux personnes ou aux véhicules, embarcations, aéronefs et conteneurs signalés pour la répression d'infractions pénales ou pour la prévention de menaces pour la sécurité publique :/ (...) 3° Lorsque des indices concrets permettent de supposer que les informations visées à l'article 37 de la décision mentionnée au 1° de l'article R. 231-3 sont nécessaires à la prévention d'une menace grave émanant de l'intéressé ou d'autres menaces graves pour la sûreté intérieure et extérieure de l'Etat ". Enfin, aux termes du I de l'article R. 231-12 du même code : " Les droits d'accès et de rectification relatifs aux données enregistrées dans le traitement N-SIS II s'exercent auprès de la Commission nationale de l'informatique et des libertés, dans les conditions prévues à l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. / II.-Par exception au I, les droits d'accès et de rectification s'exercent directement auprès du ministère de l'intérieur (direction centrale de la police judiciaire) : 1° Lorsqu'ils concernent des données relatives à l'état civil, au sexe, à la nationalité, aux signes physiques particuliers, aux photographies et au motif de signalement, s'agissant des personnes mentionnées à l'article 9 du décret n° 2010-569 du 28 mai 2010 relatif au fichier des personnes recherchées ; 2° Lorsqu'ils concernent des données relatives aux objets volés, perdus, invalidés ou détournés ".<br/>
<br/>
              4. Il résulte de la combinaison de ces dispositions qu'à l'exception de celles des données précisément désignées par l'article R. 231-12 du code de la sécurité intérieure, pour lesquelles un accès direct est possible auprès du ministère de l'intérieur, l'accès des personnes aux données susceptibles de les concerner qui seraient contenues dans le traitement automatisé des données du système national d'information Schengen s'exerce par l'intermédiaire de la Commission nationale de l'informatique et des libertés. Par ailleurs, les contestations dirigées contre le refus du responsable du traitement de communiquer au demandeur tout ou partie des informations le concernant doivent être portées devant le Conseil d'Etat lorsque ce refus concerne des données  intéressant la sûreté de l'Etat et devant le tribunal administratif lorsque tel n'est pas le cas.<br/>
<br/>
              Sur les conclusions de la requête dirigées contre le refus du ministre en tant qu'il est relatif à des informations intéressant la sûreté de l'Etat :<br/>
<br/>
              5. L'article L. 773-8 du code de justice administrative, issu de la loi du 24 juillet 2015, dispose que : " Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article <br/>
R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. / Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. / Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              6. Il ressort des pièces du dossier que M. B...a saisi le 15 avril 2016 la Commission nationale de l'informatique et des libertés (CNIL) afin de pouvoir accéder aux données le concernant contenues dans le traitement automatisé des données du système national d'information Schengen (N-SIS), devenu le N-SIS II. La Commission a désigné, en application de l'article 41 de la loi du 6 janvier 1978, un membre pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par une lettre en date du 16 juin 2016, la présidente de la Commission a informé M. B...qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans apporter à l'intéressé d'autres informations. Saisi par M.B..., le tribunal administratif de Paris a transmis au Conseil d'Etat le recours dirigé contre le refus du ministre de l'intérieur, révélé par ce courrier, de lui donner accès aux mentions susceptibles de le concerner et figurant dans le fichier litigieux. M. B...demande, en outre, qu'il soit enjoint sous astreinte au ministre de l'intérieur de lui communiquer les informations le concernant figurant dans ce fichier et, le cas échéant, de procéder à l'effacement des données illégalement intégrées, de condamner l'Etat à l'indemniser de son préjudice moral et financier par le versement d'une somme de 3 000 euros. <br/>
<br/>
              7. Le ministre de l'intérieur et la CNIL ont communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé. <br/>
<br/>
              8. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              9. La formation spécialisée a procédé à l'examen des éléments fournis par le ministre de l'intérieur et par la CNIL, laquelle a effectué les diligences qui lui incombent dans le respect des règles de compétence et de procédure applicables. Il résulte de cet examen, qui s'est déroulé selon les modalités décrites au point précédent et n'a révélé aucune illégalité, que les conclusions de M.B..., qui ne peut utilement invoquer le défaut de motivation de la décision attaquée, ni la circonstance qu'elle porterait atteinte à son droit au travail, doivent être rejetées, y compris ses conclusions à fin d'injonction, ses conclusions indemnitaires et celles présentées au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
              En ce qui concerne les conclusions de la requête dirigées contre le refus du ministre en tant qu'il est relatif à des informations susceptibles de le concerner autres que celles intéressant la sûreté de l'Etat :<br/>
<br/>
              10. Ainsi qu'il a été dit ci-dessus au point 4, le jugement de ces conclusions relève du tribunal administratif de Paris, compétent pour en connaître en application de l'article R. 312-1 du code de justice administrative. Il y a lieu de lui renvoyer ces conclusions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la requête de M. B...relatives au refus du ministre de l'intérieur de lui communiquer les informations contenues dans le traitement automatisé des données du système national d'information Schengen susceptibles de le concerner et intéressant la sûreté de l'Etat sont rejetées.<br/>
Article 2 : Le surplus des conclusions de la requête est renvoyé au tribunal administratif de Paris.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
