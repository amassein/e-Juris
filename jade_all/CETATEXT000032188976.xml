<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188976</ID>
<ANCIEN_ID>JG_L_2016_03_000000383060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188976.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 09/03/2016, 383060, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:383060.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler pour excès de pouvoir l'arrêté du 14 février 2011 par lequel le maire de la commune de Beaulieu (Hérault) a sursis à statuer sur sa demande de permis de construire déposée le 18 mai 2009 et, d'autre part, d'enjoindre à la commune de Beaulieu, à titre principal, de lui délivrer le permis de construire sollicité et, à titre subsidiaire, de procéder à un nouvel examen de sa demande, dans le délai d'un mois à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard. Par un jugement n° 1101681 du 10 novembre 2011, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12MA00113 du 26 mai 2014, la cour administrative d'appel de Marseille, à la demande de M.A..., a annulé le jugement du tribunal administratif de Montpellier du 10 novembre 2011 et l'arrêté du maire de la commune de Beaulieu du 14 février 2011 et a enjoint au maire de procéder à l'instruction de la demande de permis de construire de M. A...dans un délai d'un mois.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 juillet 2014, 24 octobre 2014 et 1er avril 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Beaulieu demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Beaulieu, et à la SCP Didier, Pinet, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, saisi le 18 mai 2009 par M. B...A...d'une demande de permis de construire un bâtiment destiné au stockage du fourrage sur un terrain lui appartenant, le maire de la commune de Beaulieu a opposé à cette demande, par un arrêté du 16 juin 2009, pris sur le fondement du dernier aliéna de l'article L. 123-6 du code de l'urbanisme, une décision de sursis à statuer ; que, par un jugement du 16 décembre 2010, devenu définitif, le tribunal administratif de Montpellier a annulé cette décision, au motif que la commune n'établissait pas qu'à la date du 16 juin 2009, la révision du plan d'occupation des sols était suffisamment avancée pour lui permettre d'opposer à M. A...un sursis à statuer, et a enjoint au maire de réexaminer la demande ; qu'après que le conseil municipal eut arrêté, par une délibération du 12 janvier 2011, le projet de plan local d'urbanisme de la commune, le maire a, par un arrêté du 14 février 2011, de nouveau sursis à statuer sur la demande au motif que le projet de construction était de nature à compromettre l'exécution de ce futur plan, tel qu'il ressortait de cette délibération ; que, par un jugement du 10 novembre 2011, le tribunal administratif de Montpellier a rejeté le recours formé par M. A...contre ce second arrêté ; que, par un arrêt du 26 mai 2014, la cour administrative d'appel de Marseille a annulé ce jugement et l'arrêté du 14 février 2011, au motif que la durée totale des sursis à statuer successivement opposés au demandeur avait excédé trois ans, en méconnaissance des dispositions combinées du dernier alinéa de l'article L. 123-6 du code de l'urbanisme et de l'article L. 111-8 du même code ;<br/>
<br/>
              2. Considérant qu'aux termes du dernier alinéa de l'article L. 123-6 du code de l'urbanisme, dans sa rédaction applicable à la décision en litige : " A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan " ; qu'aux termes de l'article L. 111-8 du même code, dans sa rédaction alors applicable : " Le sursis à statuer doit être motivé et ne peut excéder deux ans. / Lorsqu'une décision de sursis a été prise (...), l'autorité compétente ne peut, à l'expiration du délai de validité du sursis ordonné, opposer à une même demande d'autorisation un nouveau sursis fondé sur le même motif que le sursis initial. / Si des motifs différents rendent possible l'intervention d'une décision de sursis à statuer par application d'une disposition législative autre que celle qui a servi de fondement au sursis initial, la durée totale des sursis ordonnés ne peut en aucun cas excéder trois ans (...) " ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en jugeant qu'il résulte de ces dispositions que le respect de la durée maximale pendant laquelle il peut être sursis à statuer, par plusieurs décisions successives, sur une demande de permis de construire doit être apprécié en tenant compte de la période pendant laquelle l'une de ces décisions a produit ses effets à l'égard du pétitionnaire avant de faire l'objet d'une annulation contentieuse, la cour administrative d'appel a commis une erreur de droit ;<br/>
<br/>
              4. Considérant, toutefois, qu'aux termes de l'article L. 600-2 du code de l'urbanisme : " Lorsqu'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol ou l'opposition à une déclaration de travaux régies par le présent code a fait l'objet d'une annulation juridictionnelle, la demande d'autorisation ou la déclaration confirmée par l'intéressé ne peut faire l'objet d'un nouveau refus ou être assortie de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date d'intervention de la décision annulée sous réserve que l'annulation soit devenue définitive et que la confirmation de la demande ou de la déclaration soit effectuée dans les six mois suivant la notification de l'annulation au pétitionnaire " ; que doit être regardée comme un refus, au sens de ces dernières dispositions, une décision de sursis à statuer prise sur le fondement de l'article L. 123-6 du même code ;<br/>
<br/>
              5. Considérant que ces dispositions faisaient obstacle à ce que la demande de permis de construire de M. A...fasse, à l'issue de son réexamen ordonné par le jugement du 16 décembre 2010 et intervenu postérieurement à la confirmation par M. A...de sa demande, l'objet d'une nouvelle décision de sursis à statuer sur le fondement de la délibération du conseil municipal du 12 janvier 2011 arrêtant le projet de plan local d'urbanisme de la commune, qui n'est intervenue que postérieurement à la décision de sursis annulée ; que ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui répond à un moyen soulevé devant la cour administrative d'appel, doit être substitué au motif erroné en droit retenu par l'arrêt attaqué, dont il justifie le dispositif ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte de ce qui précède que le maire de Beaulieu ne pouvait légalement opposer à la demande de permis de construire de M. A... une nouvelle décision de sursis à statuer sur le fondement de la délibération du conseil municipal du 12 janvier 2011, quelle qu'en fût la durée ; que, par suite, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier qui lui était soumis en prononçant l'annulation totale du sursis à statuer opposé le 14 février 2011 à la demande de M.A... ;<br/>
<br/>
              7. Considérant, en dernier lieu, qu'il résulte de ce qui a été dit au point 4 que l'article L. 600-2 du code de l'urbanisme est applicable aux décisions de sursis à statuer ; que, par suite, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en jugeant, pour statuer sur les conclusions à fin d'injonction présentées par M.A..., que, sous réserve que celui-ci confirme sa demande de permis de construire dans les six mois suivant la notification qui lui serait faite de l'annulation définitive de la décision de sursis attaquée, les dispositions de cet article seraient applicables à la nouvelle instruction de cette demande à laquelle le maire de Beaulieu devrait procéder en exécution de son arrêt ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la commune de Beaulieu n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Beaulieu la somme de 3 000 euros à verser à M. A...au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Beaulieu est rejeté.<br/>
Article 2 : La commune de Beaulieu versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Beaulieu et à M. B... A.... <br/>
Copie en sera adressée à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-025-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. SURSIS À STATUER. - DÉCISION AYANT LE CARACTÈRE D'UN REFUS AU SENS DE L'ART. L. 600-2 DU CODE DE L'URBANISME - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-025-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. SURSIS À STATUER. DURÉE. - CALCUL DE LA DURÉE - PRISE EN COMPTE DE LA PÉRIODE PENDANT LAQUELLE UNE DÉCISION DE SURSIS A PRODUIT SES EFFETS AVANT DE FAIRE L'OBJET D'UNE ANNULATION CONTENTIEUSE - ABSENCE.
</SCT>
<ANA ID="9A"> 68-03-025-01 Une décision de sursis à statuer prise sur le fondement de l'article L. 123-6 du code de l'urbanisme doit être regardée comme un refus au sens de l'article L. 600-2 de ce code, qui prévoit que lorsqu'un refus opposé à une demande d'autorisation d'urbanisme a fait l'objet d'une annulation juridictionnelle, cette demande ne peut faire l'objet d'un nouveau refus sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date d'intervention de la décision annulée. Il s'ensuit qu'une demande d'autorisation ne peut, à la suite de l'annulation de la décision de sursis à statuer dont elle avait fait l'objet, donner lieu à un nouveau sursis à statuer sur le fondement d'une délibération arrêtant le projet de plan local d'urbanisme (PLU) de la commune intervenue postérieurement à la décision initiale de sursis qui a été annulée.</ANA>
<ANA ID="9B"> 68-03-025-01-02 Il résulte des articles L. 111-8 et L. 123-6 du code de l'urbanisme que le respect de la durée maximale pendant laquelle il peut être sursis à statuer, par plusieurs décisions successives, sur une demande de permis de construire s'apprécie sans tenir compte de la période pendant laquelle l'une de ces décisions a produit ses effets à l'égard du pétitionnaire avant de faire l'objet d'une annulation contentieuse.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
