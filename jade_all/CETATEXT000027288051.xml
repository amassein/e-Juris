<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288051</ID>
<ANCIEN_ID>JG_L_2013_04_000000349662</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 08/04/2013, 349662, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349662</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>JACOUPY</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349662.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 27 mai et 18 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Association " Les Amis de Pablo Romero ", dont le siège est 12 rue Emile Jamais à Nîmes (30000) ; l'Association " Les Amis de Pablo Romero " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA04550 du 24 mars 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement n° 0630359 du 9 juin 2008 du tribunal administratif de Nîmes ayant prononcé la décharge des rappels de taxe sur la valeur ajoutée réclamés à l'association requérante au titre de la période correspondant aux années 2001 à 2003 et des cotisations supplémentaires d'impôt sur les sociétés auxquels elle a été assujettie au titre des années 2001, 2002 et 2003, a remis à sa charge l'impôt sur les sociétés et la contribution additionnelle en droit et en pénalités au titre des années 2001 et 2002 à concurrence de la somme de 14 902 euros ainsi que le complément de taxe sur la valeur ajoutée qui lui a été réclamé à concurrence de la somme de 19 971 euros en droits et pénalités au titre de la période correspondant aux mêmes années ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Jacoupy, avocat de l'Association " Les Amis de Pablo Romero ",<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée à Me Jacoupy, avocat de l'Association " Les Amis de Pablo Romero " ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 256 du code général des impôts : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel " ; qu'aux termes de l'article 261 du même code, dans sa rédaction applicable aux impositions en litige, " Sont exonérées de taxe sur la valeur ajoutée (...) 7 (Organismes d'utilité générale) 1° a. Les services de caractère social, éducatif, culturel ou sportif rendus à leurs membres par les organismes légalement constitués agissant sans but lucratif, et dont la gestion est désintéressée (...) c. Les recettes de six manifestations de bienfaisance ou de soutien organisées dans l'année à leur profit exclusif par les organismes désignés au a et b ainsi que par les organismes permanents à caractère social des collectivités locales et des entreprises (...) " ; qu'aux termes de l'article 207 du code général des impôts, dans sa rédaction applicable aux impositions en litige " 1. Sont exonérés de l'impôt sur les sociétés : (...) 5° bis. Les organismes sans but lucratif mentionnés au 1° du 7 de l'article 261, pour les opérations à raison desquelles ils sont exonérés de la taxe sur la valeur ajoutée " ;<br/>
<br/>
              2. Considérant qu'en application de ces dispositions, les associations régies par la loi du 1er juillet 1901 ne sont exonérées de la taxe sur la valeur ajoutée et de l'impôt sur les sociétés que si, d'une part, leur gestion présente un caractère désintéressé et, d'autre part, les services qu'elles rendent ne sont pas offerts en concurrence dans la même zone géographique d'attraction avec ceux proposés au même public par des entreprises commerciales exerçant une activité identique ; que, toutefois, même dans le cas où l'association intervient dans un domaine d'activité et dans un secteur géographique où existent des entreprises commerciales, l'exonération de taxe sur la valeur ajoutée lui est acquise, et par voie de conséquence l'exonération d'impôt sur les sociétés à raison des même opérations, si elle exerce son activité dans des conditions différentes de celles des entreprises commerciales, soit en répondant à certains besoins insuffisamment satisfaits par le marché, soit en s'adressant à un public qui ne peut normalement accéder aux services offerts par les entreprises commerciales, notamment en pratiquant des prix inférieurs à ceux du secteur concurrentiel et à tout le moins des tarifs modulés en fonction de la situation des bénéficiaires, sous réserve de ne pas recourir à des méthodes commerciales excédant les besoins de l'information du public sur les services qu'elle offre ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort du jugement du tribunal administratif de Nîmes du 9 juin 2008 que pour accorder à l'Association " Les Amis de Pablo Romero " la décharge des impositions supplémentaires qu'elle sollicitait, ce tribunal s'est fondé sur ce que l'association entrait dans le champ des dispositions précitées du c du 1° du 7 de l'article 261 du code général des impôts et devait bénéficier, à ce titre, d'une exonération de taxe sur la valeur ajoutée - et en conséquence d'impôt sur les sociétés - à concurrence, pour chaque année, des recettes réalisées au cours de six ferias, chacune de ces ferias devant être analysée comme une unique manifestation de bienfaisance ou de soutien au sens de ces dispositions ; qu'en censurant le motif de décharge ainsi retenu par le tribunal au motif que, dès lors que l'association requérante n'entrait pas dans le champ d'application du a du 1° du 7 de l'article 261 du code général des impôts, elle ne pouvait pas entrer dans celui du c du 1° du 7 du même article, la cour n'a pas substitué une autre base légale à celle sur laquelle l'administration avait fondé les impositions litigieuses mais s'est bornée à examiner le bien fondé de l'invocation, par l'association, pour faire échec aux dispositions législatives prévoyant l'assujettissement à la taxe sur la valeur ajoutée des prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel, d'une disposition d'exonération ; que, ce faisant, elle n'a entaché son arrêt ni d'erreur de droit, ni de contradiction de motifs, ni d'insuffisance de motivation ;<br/>
<br/>
              4. Considérant, en second lieu, que, pour juger que l'association n'entrait pas dans le champ de l'exonération prévue par le a du 1° du 7 de l'article 261 du code général des impôts en faveur des associations rendant, sans but lucratif et au moyen d'une gestion désintéressée, au profit de leurs seuls membres des services de caractère social, éducatif, culturel ou sportif, la cour administrative d'appel s'est fondée sur ce qu'il résultait de l'instruction, d'une part, que les services fournis par l'association étaient rendus à d'autres personnes que ses membres ou leurs ayants-droit, d'autre part, que les ressources de l'association provenaient de façon prépondérante de l'ouverture d'une " bodega " concurrençant le secteur marchand selon des modalités de gestion analogues ; que la cour, qui a porté sur les faits une appréciation souveraine non entachée de dénaturation, a ainsi caractérisé sans erreur de droit l'absence de but non lucratif de l'association ; qu'elle a pu légalement en déduire, sans avoir à rechercher si, en outre, la gestion de celle-ci revêtait un caractère non désintéressé, que l'association ne pouvait prétendre au bénéfice de l'exonération qu'elle sollicitait ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le pourvoi de l'Association " Les Amis de Pablo Romero " doit être rejeté, y compris ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de l'Association " Les Amis de Pablo Romero " est rejeté. <br/>
Article 2 : La présente décision sera notifiée à l'Association " Les Amis de Pablo Romero " et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
