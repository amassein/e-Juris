<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166776</ID>
<ANCIEN_ID>JG_L_2017_03_000000393830</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/67/CETATEXT000034166776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 10/03/2017, 393830, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393830</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:393830.20170310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Grenoble d'annuler la décision de rejet de sa demande de remise gracieuse d'intérêts de retard correspondant à la rectification des droits d'enregistrement dus au titre de la succession de son père résultant d'une décision du 5 août 2009 de la direction des services fiscaux de la Savoie, ainsi que la décision de rejet du 19 novembre 2009 du conciliateur fiscal de la Savoie. Par un jugement n° 1000221 du 20 décembre 2013, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14LY00455 du 29 septembre 2015, la cour administrative d'appel de Lyon a transmis au Conseil d'Etat les conclusions de la requête de Mme B...tendant à l'annulation de ce jugement en tant qu'il a rejeté sa demande tendant à l'annulation de ces décisions.<br/>
<br/>
              Par un pourvoi, enregistré le 17 février 2014 au greffe de la cour administrative d'appel de Lyon, et un mémoire complémentaire, enregistré le 8 mars 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2003-1311 du 30 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a formé le 29 juillet 2009 une réclamation contentieuse relative aux droits d'enregistrement mis à sa charge au titre de la succession de son père ainsi qu'une demande de remise gracieuse des intérêts de retard correspondant à ces droits ; que le 5 août 2009, l'administration lui a accordé un dégrèvement partiel des impositions qu'elle contestait et l'a informée du rejet de sa demande de remise gracieuse des intérêts de retard ; que, par un courrier du 19 septembre 2009, Mme B...a saisi le conciliateur fiscal départemental d'une demande tendant à la remise gracieuse de ces intérêts ; que celui-ci a également rejeté sa demande de remise gracieuse le 19 novembre 2009 ; que Mme B...se pourvoit en cassation contre le jugement du tribunal administratif de Grenoble du 20 décembre 2013 en tant qu'il a rejeté sa demande d'annulation des décisions de rejet du 5 août et du 19 septembre 2009 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 247 du livre des procédures fiscales dans sa rédaction applicable : " L'administration peut accorder sur la demande du contribuable : / 1° Des remises totales ou partielles d'impôts directs régulièrement établis lorsque le contribuable est dans l'impossibilité de payer par suite de gêne ou d'indigence ; / 2° Des remises totales ou partielles d'amendes fiscales ou de majorations d'impôts lorsque ces pénalités et, le cas échéant, les impositions auxquelles elles s'ajoutent sont définitives ; (...) Les dispositions des 2° et 3° sont le cas échéant applicables s'agissant des sommes dues au titre de l'intérêt de retard visé à l'article 1727 du code général des impôts " ; <br/>
<br/>
              3. Considérant que, pour juger que le rejet par l'administration de la demande formée par Mme B...n'était pas entaché d'une erreur manifeste d'appréciation et rejeter, par suite, ses conclusions, le tribunal administratif s'est fondé sur l'unique motif qu'elle ne soutenait pas être dans l'impossibilité de payer la somme réclamée par suite de gêne ou d'indigence ; que, toutefois, s'il résulte des termes du 1° de l'article L. 247 du livre des procédures fiscales que l'administration fiscale ne peut accorder, à titre gracieux, des remises totales ou partielles d'impôts directs régulièrement établis que lorsque le contribuable est dans l'impossibilité de payer par suite de gêne ou d'indigence, il résulte des 2° et 3° du même article relatifs aux remises d'amendes fiscales ou de majorations d'impôts, qui ont été rendus applicables aux intérêts de retard de l'article 1727 du code général des impôts par l'article 35 de la loi de finances pour 2004, que la possibilité pour l'administration d'accorder de telles remises n'est pas limitée au seul cas où le contribuable est dans l'impossibilité de payer par suite de gêne ou d'indigence ; que, par suite, en ne recherchant pas si les motifs invoqués par MmeB..., tirés ce qu'elle était de bonne foi et de ce que la procédure de rectification n'avait été suivie qu'à l'égard de son frère cohéritier solidaire avec lequel ses relations étaient difficiles n'étaient pas de nature à justifier une remise gracieuse, le tribunal a commis une erreur de droit ; par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à MmeB..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 20 décembre 2013 est annulé.  <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Grenoble.<br/>
Article 3 : L'Etat versera à Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
