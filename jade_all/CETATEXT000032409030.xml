<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032409030</ID>
<ANCIEN_ID>JG_L_2016_04_000000394282</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/90/CETATEXT000032409030.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 15/04/2016, 394282, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394282</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394282.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1502813 du 22 octobre 2015, enregistré le 27 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête enregistrée le 20 février 2015 au greffe de ce tribunal. Par cette requête, Mme A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Conseil national de l'ordre des chirurgiens-dentistes, portée à sa connaissance par un courrier du 7 janvier 2015 du conseil départemental de Paris de l'ordre des chirurgiens-dentistes, refusant de reconnaître le diplôme inter-universitaire d'homéopathie délivré en 2008 par l'université Paris 13 ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de reconnaître ce diplôme, sous astreinte de 200 euros par jour de retard, à compter du quinzième jour suivant la notification de sa décision ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4127-216 du code de la santé publique, qui est au nombre des dispositions formant le code de déontologie des chirurgiens-dentistes : " Les seules indications que le chirurgien-dentiste est autorisé à mentionner sur ses imprimés professionnels, notamment ses feuilles d'ordonnances, notes d'honoraires et cartes professionnelles, sont : (...) / 3° Les diplômes, titres et fonctions reconnus par le Conseil national de l'ordre (...) " ; et qu'aux termes de l'article R. 4127-218 du même code : " Les seules indications qu'un chirurgien-dentiste est autorisé à faire figurer sur une plaque professionnelle à la porte de son immeuble ou de son cabinet sont (...)  les diplômes, titres ou fonctions reconnus par le Conseil national de l'ordre " ; que pour reconnaître un diplôme en application de ces dispositions, le Conseil national de l'ordre des chirurgiens-dentistes peut notamment fonder son appréciation sur l'apport clinique de ce diplôme dans la pratique quotidienne du praticien et sur l'intérêt de sa mention pour l'information du patient ;<br/>
<br/>
              2. Considérant que par la décision attaquée, prise pour l'application des dispositions citées ci-dessus, le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme inter-universitaire d'homéopathie délivré en 2008 par l'université Paris 13 au motif que cette formation se situait en dehors du champ de capacité professionnelle des chirurgiens-dentistes et était de nature à induire une confusion dans l'esprit des patients sur l'étendue de l'art dentaire ;<br/>
<br/>
              3. Considérant que le courrier du 7 janvier 2015 par lequel le conseil départemental de Paris de l'ordre des chirurgiens-dentistes a communiqué à Mme B...le refus litigieux du conseil national de l'ordre n'avait à comporter ni l'avis de la commission de l'enseignement et des titres de ce conseil, ni la composition de cette commission, ni le procès verbal de la délibération du conseil national ; que Mme B...n'est, par suite, pas fondée à soutenir que la décision qu'elle attaque serait, pour ce motif, entachée d'irrégularité ;<br/>
<br/>
              4. Considérant qu'aucun texte ni aucun principe n'impose que la décision attaquée, qui revêt un caractère réglementaire, soit motivée ; que Mme B...n'est, par suite, pas fondée à soutenir que la décision attaquée serait illégale faute de mentionner les considérations de droit et de fait qui la fondent ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que le diplôme universitaire dont Mme B...a demandé la reconnaissance est délivré au terme d'une formation qui ne comporte que des apprentissages généraux sur l'homéopathie, sans aucune application spécifique à l'art dentaire ; que par suite, en refusant sa reconnaissance au titre des dispositions des articles R. 4127-216 et R. 4127-218 du code de la santé publique au motif qu'il était dépourvu d'apport particulier pour la pratique de la chirurgie-dentaire et était susceptible d'induire en erreur les patients sur la nature des soins dispensés par le chirurgien-dentiste, le Conseil national de l'ordre des chirurgien-dentistes n'a pas fait une inexacte application de ces mêmes dispositions ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que les conclusions de Mme B... tendant à l'annulation de la décision du Conseil national de l'ordre des chirurgiens-dentistes doivent être rejetées ; que, par suite, ses conclusions aux fins d'injonction et d'astreinte, ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, également, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au Conseil national de l'ordre national des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
