<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311348</ID>
<ANCIEN_ID>JG_L_2014_07_000000371313</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311348.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 30/07/2014, 371313, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371313</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:371313.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
       Par un jugement n° 1200680 du 14 mai 2013, le tribunal administratif de la Polynésie française a rejeté pour tardiveté la requête présentée devant lui le 10 décembre 2012 par M. C...A...tendant, d'une part, à l'annulation de l'arrêté du 5 juillet 2012 en tant qu'il l'a promu seulement à compter du 1er janvier 2009 au grade hors classe du cadre d'emplois des rééducateurs de la Polynésie française, d'autre part, à ce qu'il soit enjoint à la Polynésie française de le promouvoir à compter du 1er janvier 2008.<br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
       Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 16 août et le 12 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...A..., représenté par Me Balat, demande au Conseil d'Etat :<br/>
<br/>
       1°) d'annuler le jugement n° 1200680 du 14 mai 2013 du tribunal administratif de la Polynésie française ;<br/>
<br/>
       2°) de mettre à la charge de la Polynésie française la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
       3°) de mettre à la charge de la Polynésie française la contribution pour l'aide juridique.<br/>
<br/>
       M. B...A...soutient que le tribunal administratif de la Polynésie française :<br/>
       - a dénaturé les faits et pièces du dossier en jugeant établie l'existence d'un appel téléphonique dont il conteste la réalité ;<br/>
       - a entaché son jugement d'une erreur de droit en jugeant que le délai de recours courrait à compter de la date à laquelle le pli recommandé contenant la décision contestée a été retourné à l'expéditeur par la Poste en dépit de l'absence de mention indiquant, sur ce pli, la date à laquelle il aurait été avisé de son existence.<br/>
<br/>
       Par un mémoire en défense, enregistré au secrétariat du contentieux le 27 février 2014, la Polynésie française conclut au rejet du pourvoi et à ce qu'une somme de 2 500 euros soit mise à la charge de M. B...A...au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
       Vu :<br/>
       - les autres pièces du dossier ;<br/>
       - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
       - les conclusions de M. Edouard Crépey, rapporteur public.<br/>
<br/>
       La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. C... A...et à la SCP de Chaisemartin, Courjon, avocat de la Présidence de la Polynésie française.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la requête par laquelle M. B...A...contestait devant le tribunal administratif de la Polynésie française un arrêté du 5 juillet  2012 le nommant, à compter du 1er janvier 2009, au grade hors classe du cadre d'emplois de rééducateur de la Polynésie française a été enregistrée le 10 décembre 2012 au greffe du tribunal administratif. <br/>
<br/>
              2. Pour déclarer irrecevable la requête à raison de sa tardiveté, le tribunal administratif a relevé que le pli recommandé contenant la décision contestée avait été expédié à l'adresse indiquée par M. B...A..., était parvenu le 13 juillet 2012 à la poste de Faa'a, ce dont le requérant avait été prévenu par un appel téléphonique, puis avait été retourné au service expéditeur sans que M. B...A...ne vienne retirer le pli. Le tribunal administratif en a déduit que ce dernier s'était volontairement soustrait à la notification. Il a, dès lors, considéré qu'en l'absence de mention indiquant la date à laquelle le destinataire avait été avisé, le délai de recours contentieux avait commencé à courir à compter de la date à laquelle le pli avait été retourné à l'expéditeur, soit le 6 août 2012. Il en a conclu que la requête de M. B...A...le 10 décembre 2012 avait été présentée hors délai. <br/>
<br/>
              3. Pour opposer à un requérant une irrecevabilité tirée de la tardiveté de sa requête, il incombe au tribunal administratif de vérifier que l'intéressé a reçu notification de la décision qu'il conteste dans des conditions régulières. Dans ce cadre, en cas de retour à l'administration du pli contenant la décision, cette preuve peut résulter soit des mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal.<br/>
<br/>
              4. En l'espèce,  en jugeant que la référence à un appel téléphonique , dont l'existence ne résultait que  d'allégations non corroborées par les pièces qui lui étaient soumises, et sur les cachets apposés sur les plis , dont l'examen indique pourtant qu'ils ne portent que les dates  de réception par le bureau de poste du pli recommandé, de réexpédition de ce dernier au service expéditeur et de réception par celui-ci, le tribunal administratif n'a pu, contrairement à ce qu'il a estimé, régulièrement  établir que le requérant avait effectivement reçu régulièrement notification de la décision qu'il conteste ; <br/>
<br/>
              5. Dès lors qu'il ne pouvait sans erreur de droit estimer que cette notification n'a pas été régulièrement effectuée, le tribunal administratif de la Polynésie française n'était pas fondé à juger que M. B...A...s'était volontairement soustrait à la notification du jugement attaqué et que le délai de recours contentieux avait commencé à courir, en l'absence de mention indiquant la date à laquelle le destinataire avait été avisé de l'arrivée d'un pli recommandé à son nom, à compter de la date à laquelle le pli avait été retourné au service expéditeur.<br/>
<br/>
              6. Pour ces motifs, le jugement du tribunal administratif de la Polynésie française est entaché d'erreur de droit et doit, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Polynésie française le versement d'une somme globale de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et de celles de l'article R. 761-1 relatives au remboursement de la contribution pour l'aide juridique. En revanche, les dispositions de l'article L. 761-1 de ce même code font obstacle à ce qu'une somme soit mise à la charge de M. B...A..., qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1er : Le jugement du 14 mai 2013 du tribunal administratif de la Polynésie française est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de la Polynésie française.<br/>
Article 3 : La Polynésie française versera à M. B...A...une somme de 2 000 euros en application des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C...A...et à la Polynésie française.  <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
