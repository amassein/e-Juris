<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217445</ID>
<ANCIEN_ID>JG_L_2019_10_000000424512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217445.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 14/10/2019, 424512, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424512.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Dijon, d'une part, d'annuler pour excès de pouvoir la décision implicite par laquelle le directeur du centre hospitalier d'Auxerre a refusé de lui communiquer la copie de son dossier médical, notamment la partie relative à l'année 2013, et d'autre part, d'enjoindre au centre hospitalier de lui communiquer les documents demandés dans un délai de 15 jours à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 1600920 du 29 juin 2018, le tribunal administratif de Dijon a prononcé un non-lieu à statuer sur les conclusions à fin d'annulation de la décision implicite du directeur du centre hospitalier d'Auxerre et a rejeté le surplus de conclusions des parties, y compris celles tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26  septembre et 26 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il n'a pas fait droit à ses conclusions tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Boullez, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.   <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;    <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. B... A... et à la SCP WAQUET, FARGE, HAZAN, avocat du centre hospitalier d'Auxerre ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la fin de non-recevoir opposée par le centre hospitalier d'Auxerre : <br/>
<br/>
              1. L'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique dispose que : " Les auxiliaires de justice rémunérés selon un tarif peuvent renoncer à percevoir la somme correspondant à la part contributive de l'Etat et poursuivre contre la partie condamnée aux dépens et non bénéficiaire de l'aide juridictionnelle le recouvrement des émoluments auxquels ils peuvent prétendre./ Dans toutes les instances, le juge condamne la partie tenue aux dépens, ou qui perd son procès, et non bénéficiaire de l'aide juridictionnelle, à payer à l'avocat du bénéficiaire de l'aide juridictionnelle, partielle ou totale, une somme qu'il détermine et qui ne saurait être inférieure à la part contributive de l'Etat, au titre des honoraires et frais non compris dans les dépens que le bénéficiaire de l'aide aurait exposés s'il n'avait pas eu cette aide. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation (...) ". <br/>
<br/>
              2. Par un jugement en date du 29 juin 2018, le tribunal administratif de Dijon, après avoir prononcé un non-lieu à statuer sur les conclusions de M. A... tendant à l'annulation de la décision du directeur du centre hospitalier d'Auxerre refusant de lui communiquer son dossier médical, a rejeté les conclusions présentées, sur le fondement des dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, par l'association d'avocats à responsabilité professionnelle individuelle Themis, avocat de M. A... qui avait bénéficié de l'aide juridictionnelle totale. Seul cet avocat avait qualité pour former un pourvoi en cassation contre le jugement attaqué en tant que ce dernier s'est prononcé sur ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Par suite, le centre hospitalier d'Auxerre est fondé à soutenir que M. A... ne justifiait pas d'un intérêt à se pourvoir en cassation contre cette partie du jugement qu'il attaque. Son pourvoi doit dès lors être rejeté ainsi que, par voie de conséquence, les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.  <br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au centre hospitalier d'Auxerre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
