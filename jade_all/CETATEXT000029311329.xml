<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311329</ID>
<ANCIEN_ID>JG_L_2014_07_000000364730</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 30/07/2014, 364730, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364730</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:364730.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 décembre 2012 et 21 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association communale de chasse agréée de Bertrichamps, dont le siège est au Chalet de chasse, 2 quater, rue du 27 septembre 1870, à Bertrichamps (54120), représentée par son président en exercice ; l'association communale de chasse agréée de Bertrichamps demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC01700 du 18 octobre 2012 par lequel la cour administrative d'appel de Nancy a, d'une part, annulé l'ordonnance n° 0901885 du 25 août 2011 par laquelle la présidente de la 2ème chambre du tribunal administratif de Nancy s'est déclarée incompétente pour connaître de la demande de l'association tendant à l'annulation de la délibération du 22 avril 2011 par laquelle le conseil municipal de Bertrichamps a décidé de louer de gré à gré, pour la saison de chasse 2011-2012, la forêt communale de Bertrichamps à M. C... A...et, d'autre part, a rejeté cette demande ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Bertrichamps la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association communale de chasse agréée de Bertrichamps et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Bertrichamps ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond et des énonciations de l'arrêt attaqué que, par une délibération du 22 avril 2011, le conseil municipal de Bertrichamps a décidé de louer de gré à gré à M. C...A...deux lots de chasse dans le domaine forestier privé de la commune pour la période du 1er juin 2011 au 29 février 2012 ; que l'association communale de chasse agréée de Bertrichamps a demandé au tribunal administratif de Nancy d'annuler cette délibération ; que, par une ordonnance rendue le 12 septembre 2011, la présidente de la 2ème chambre de ce tribunal administratif s'est déclarée incompétente pour connaître de cette demande ; que, par un arrêt du 18 octobre 2012, contre lequel l'association communale de chasse agréée de Bertrichamps se pourvoit en cassation, la cour administrative d'appel de Nancy a annulé cette ordonnance et rejeté la demande de l'association ;<br/>
<br/>
              2. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour s'est fondée, s'agissant des clauses particulières du contrat de bail, sur la délibération du conseil municipal de Bertrichamps en date du 13 août 2009 et non sur celle du conseil municipal de Bertrichamps en date du 22 avril 2011 qui est en litige dans la requête n° 11NC01700 ; qu'en indiquant que la délibération litigieuse a attribué des droits de chasse à MM. A...etB..., alors qu'à la différence de la délibération du 13 août 2009, cette délibération ne concernait que M. A..., la cour administrative d'appel a entaché ses motifs d'erreur matérielle ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'association communale de chasse agréée de Bertrichamps est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'association communale de chasse agréée de Bertrichamps qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la commune de Bertrichamps au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune la somme de 1 000 euros qui sera versée à l'association communale de chasse agréée de Bertrichamps au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 11NC01700 de la cour administrative d'appel de Nancy en date du 18 octobre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nancy. <br/>
Article 3 : La commune de Bertrichamps versera à l'association communale de chasse agréée de Bertrichamps une somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Bertrichamps au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à l'association communale de chasse agréée de Bertrichamps, à la commune de Bertrichamps, à M. C...A...et M. C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
