<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806215</ID>
<ANCIEN_ID>JG_L_2021_12_000000448692</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/12/2021, 448692, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448692</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448692.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi le 8 octobre 2020 le tribunal administratif de Montpellier, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 28 septembre 2020 par laquelle elle a rejeté le compte de campagne de M. F... C..., candidat aux élections municipales et communautaires qui se sont déroulées le 15 mars 2020 à Canet-en-Roussillon (Pyrénées-Orientales). Par un jugement n° 2004400 du 15 décembre 2020, le tribunal administratif a confirmé le rejet du compte de campagne de M. C... par la Commission nationale des comptes de campagne et des financements politiques et a prononcé l'inéligibilité de M. C... pour toutes les élections pour une durée de six mois à compter de la date à laquelle le jugement deviendra définitif et, par voie de conséquence, a annulé son élection en qualité de conseiller municipal et proclamé élu à sa place, M. D... E....<br/>
<br/>
              Par une requête, enregistrée le 15 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) d'annuler la décision de la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Carine Chevrier, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par une décision du 28 septembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. C..., candidat tête de liste à l'élection des conseillers municipaux et communautaires de la commune de Canet-en-Roussillon (Pyrénées-Orientales), qui s'est déroulée le 15 mars 2020 aux motifs que le compte de campagne déposé par M. C..., dont la liste a obtenu plus de 1 % des suffrages, n'est appuyé que de pièces disparates et incomplètes, ne permettant pas d'attester de la réalité et de la régularité des opérations réalisées. Saisi en application de l'article L. 52-15 du code électoral, le tribunal administratif de Montpellier a jugé que la Commission nationale des comptes de campagne et des financements politiques avait rejeté le compte de campagne à bon droit, a déclaré M. C... inéligible pour une durée de six mois pour toutes les élections à compter de la date définitive du jugement, a annulé l'élection de M. C... en qualité de conseiller municipal et proclamé, en remplacement de M. C..., l'élection de M. E... en qualité de conseiller municipal de la commune de Canet-en-Roussillon. M. C... relève appel de ce jugement.<br/>
<br/>
              Sur la régularité de la procédure suivie devant la Commission nationale des comptes de campagne et des financements politiques :<br/>
<br/>
              2. Il est constant que la décision de la Commission nationale des comptes de campagne et des financements politiques litigieuse est signée par son président et mentionne le nom de l'ensemble des membres ayant siégé et participé au délibéré à l'issue de la séance qui s'est tenue le même jour.  Dès lors, contrairement à ce qu'allègue M. C..., la décision ne méconnaît pas, en tout état de cause, les règles posées par son règlement intérieur. <br/>
<br/>
              Sur le bien-fondé du rejet du compte de campagne :<br/>
<br/>
              3. Aux termes de l'article L. 52-12 du code électoral : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes, notamment d'une copie des contrats de prêts conclus en application de l'article L. 52-7-1 du présent code, ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts (...) ". En vertu du 4° du XII de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " Pour les listes de candidats présentes au seul premier tour, la date limite mentionnée à la première phrase du deuxième alinéa de l'article L. 52-12 du code électoral est fixée au 10 juillet 2020 à 18 heures ". Aux termes de l'article L. 52-15 du code électoral : " (...) / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) ".<br/>
<br/>
              4. En premier lieu, l'acte par lequel la Commission nationale des comptes de campagne et des financements politiques saisit le juge de l'élection n'a pas le caractère d'une décision au sens de l'article L. 211-2 du code des relations entre le public et l'administration. Par suite, le grief tiré d'une méconnaissance des dispositions de cet article est inopérant.<br/>
<br/>
              5. En second lieu, il résulte de l'instruction que le compte de campagne de M. C... comportait des dépenses dont le caractère électoral n'a pu être établi, faute de transmission de pièces justificatives suffisantes concernant certains frais d'impression inscrits au compte d'imputation " publications impressions hors dépenses de la campagne officielle ", représentant une somme totale de 1 188,40 euros, deux factures inscrites au compte " transports et déplacements " relatives à des prestations alimentaires pour un montant de 289,18 euros et une facture d'hôtel de 151,90 euros ainsi que les dépenses inscrites au compte " réunions publiques " pour un montant de 2 688,09 euros, les deux courriers de demande d'informations des 30 juillet et 17 août 2020 adressés par la commission à M. C... étant restés sans réponse. Dans ces conditions, et dès lors que M. C..., à l'appui de sa contestation du rejet de son compte de campagne par la Commission, n'apporte pas d'éléments susceptibles de qualifier ces dépenses, c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. C... et a saisi le juge électoral. <br/>
<br/>
              Sur l'inéligibilité : <br/>
<br/>
              6. Aux termes des dispositions de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : / 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / (...) L'inéligibilité mentionnée au présent article est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision. Si le juge de l'élection a prononcé l'inéligibilité d'un candidat ou des membres d'un binôme proclamé élu, il annule son élection ou, si l'élection n'a pas été contestée, déclare le candidat ou les membres du binôme démissionnaires d'office ". Il résulte de ces dispositions qu'en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier, d'une part, si elles révèlent un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et, d'autre part, si ce manquement présente un caractère délibéré.<br/>
<br/>
              7. Compte tenu des éléments cités au point 5 tels qu'ils résultent de l'instruction, le compte de campagne de M. C... comportait des dépenses pour un montant total de 3 219 euros dont le caractère électoral n'a pu être établi, faute de justifications suffisantes apportées par M. C..., privant la Commission de la possibilité de garantir l'exactitude et de la sincérité des comptes soumis à son examen. Si M. C... invoque que son compte de campagne, dressé par un expert-comptable et présentant un caractère réduit, a été déposé dans les délais et qu'il a adressé à la Commission nationale des comptes de campagne et des financements politiques, par courrier du 10 juillet 2020, l'ensemble des pièces utiles à l'examen de ses comptes de campagne, il ne résulte pas de l'instruction que les pièces complémentaires demandées par courriers des 30 juillet et 17 août 2020 aient été transmises à la commission, ni que l'intéressé ait adressé depuis les justificatifs nécessaires. Au regard des circonstances de l'espèce, M. C... doit être regardé comme ayant commis un manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. Dans ces conditions, il n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier l'a déclaré inéligible pour une durée de six mois. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. F... C..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 20 décembre 2021 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; Mme Suzanne von Coester, conseillère d'Etat et Mme Carine Chevrier, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
 		La rapporteure : <br/>
      Signé : Mme Carine Chevrier<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
