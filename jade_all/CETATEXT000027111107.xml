<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027111107</ID>
<ANCIEN_ID>JG_L_2013_02_000000343164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/11/11/CETATEXT000027111107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 13/02/2013, 343164</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343164.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 septembre et 9 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C...B..., demeurant..., M. A...B..., demeurant connu d'après les renseignements qu'il a pu recueillir auprès du service du cadastre ou du conservateur des hypothèques ou par tout autre moyenet M. D...B..., demeurant...; M. C...B...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02317 du 8 juillet 2010 par lequel la cour administrative d'appel de Marseille a rejeté leur requête tendant à l'annulation du jugement n° 0704605-0704873 du 21 avril 2009 du tribunal administratif de Montpellier ayant rejeté leur demande tendant à l'annulation de l'arrêté n° 2007-1-1022 du 29 mai 2007 du préfet de l'Hérault en tant qu'il a déclaré cessibles au profit de la communauté d'agglomération de Montpellier les immeubles bâtis ou non bâtis nécessaires à la réalisation des travaux d'aménagement de l'avenue du Mas de Rochet et des arrêtés rectificatifs des 7, 18 et 27 juin 2007 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ;<br/>
<br/>
              Vu le décret n° 55-22 du 4 janvier 1955 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gadiou, Chevallier, avocat de MM.B..., et de la SCP Lyon-Caen, Thiriez, avocat de la communauté d'agglomération de Montpellier,<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gadiou, Chevallier, avocat de MM.B..., et à la SCP Lyon-Caen, Thiriez, avocat de la communauté d'agglomération de Montpellier ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 mai 2007, le préfet de l'Hérault a déclaré d'utilité publique des travaux d'aménagement de l'avenue Mas de Rochet, située à Castelnau-le-Lez, et déclaré cessibles au profit de la communauté d'agglomération de Montpellier les immeubles bâtis ou non bâtis dont l'acquisition était nécessaire pour la réalisation de cette opération ; que, par un jugement du 21 avril 2009, le tribunal administratif de Montpellier a rejeté la demande de MM.A..., D...et C...B..., propriétaires indivisaires d'une parcelle située dans l'emprise du projet, tendant à l'annulation de l'arrêté du 29 mai 2007 en tant qu'il a déclaré cessibles certains biens immobiliers ; que, par un arrêt du 8 juillet 2010, contre lequel les intéressés se pourvoient en cassation, la cour administrative d'appel de Marseille a rejeté leur requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2. Considérant, en premier lieu, que les requérants soutiennent qu'en jugeant régulière la procédure de notification du dépôt de dossier d'enquête parcellaire à l'égard de MM. D... et C...B...par la voie d'un simple affichage en mairie, alors que leurs avis de notification individuelle avaient été retournés à l'expéditeur assortis respectivement des mentions " non réclamé " et " n'habite pas à l'adresse indiquée ",  la cour a méconnu les dispositions de l'article R. 11-22 du code de l'expropriation pour cause d'utilité publique, qui imposent à l'expropriant de rechercher par tout moyen le domicile des propriétaires, et ainsi entaché son arrêt d'erreur de droit ; <br/>
<br/>
              3. Considérant qu'aux termes de  l'article R. 11-19 du code de l'expropriation pour cause d'utilité publique : " L'expropriant adresse au préfet, pour être soumis à enquête parcellaire dans chacune des communes où sont situés les immeubles à exproprier : (...)/ 2° la liste des propriétaires établie à l'aide d'extraits des documents cadastraux délivrés par le service du cadastre ou à l'aide des renseignements délivrés par le conservateur des hypothèques au vu du fichier immobilier ou par tous autres moyens " ; que l'article R. 11-22 du même code dispose: " Notification individuelle du dépôt du dossier à la mairie est faite par l'expropriant, sous pli recommandé avec demande d'avis de réception aux propriétaires figurant sur la liste établie en application de l'article R. 11-19 lorsque leur domicile est connu d'après les renseignements recueillis par l'expropriant (...) ; en cas de domicile inconnu, la notification est faite en double copie au maire qui en fait afficher une, et, le cas échéant, aux locataires et preneurs à bail rural " ;  <br/>
<br/>
              4. Considérant qu'il résulte des dispositions rappelées au point 3 que l'expropriant doit notifier le dépôt du dossier d'enquête parcellaire aux propriétaires figurant sur la liste mentionnée au 2° de l'article R. 11-19 et dont le domicile est...; que ces dispositions n'imposent pas à l'expropriant de procéder à de nouvelles recherches lorsque la notification au domicile ainsi déterminé revient avec la mention " non réclamé ", auquel cas la notification est réputée avoir été régulièrement faite à ce domicile, ou  avec la mention " n'habite pas à l'adresse indiquée ", auquel cas, l'affichage en mairie se substitue régulièrement à la formalité de notification individuelle ; que, par suite, les requérants ne sont pas fondés à soutenir que la cour administrative d'appel aurait commis une erreur de droit en retenant que la notification du dépôt en mairie du dossier d'enquête parcellaire avait été faite dans des conditions régulières ; <br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article R.11-28 du code de l'expropriation pour cause d'utilité publique : " Sur le vu du procès-verbal et des documents y annexés, le préfet, par arrêté, déclare cessibles les propriétés ou parties de propriétés dont la cession est nécessaire. Ces propriétés sont désignées conformément aux dispositions de l'article 7 du décret n° 55-22 du 4 janvier 1955 portant réforme de la publicité foncière et l'identité des propriétaires est précisée conformément aux dispositions de l'alinéa 1er de l'article 5 de ce décret ou de l'alinéa 1er de l'article 6 du même décret, sans préjudice des cas exceptionnels mentionnés à l'article 82 du décret d'application n° 55-1350 du 14 octobre 1955. Toutefois, il peut n'être établi qu'un seul document d'arpentage pour l'ensemble des parcelles contiguës comprises dans une même feuille de plan cadastral ; il n'est plus alors exigé de document d'arpentage soit à l'occasion de cessions amiables postérieures à l'arrêté de cessibilité ou à tous actes en tenant lieu, soit à l'occasion de l'ordonnance d'expropriation " ; qu'en jugeant que ces dispositions n'imposaient pas d'établir un document d'arpentage lorsque les états parcellaires sont suffisamment explicites, la cour n'a pas entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              6. Considérant, en dernier lieu, qu'en relevant que la surface à exproprier représentait 303 mètres carrés sur une surface initiale de 1829 mètres carrés, la cour a porté sur les faits de l'espèce une appréciation souveraine qui est exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout de qui précède que MM.D..., A...et C...B...ne sont pas fondés à demander l'annulation de l'arrêt du 8 juillet 2010 de la cour administrative d'appel de Marseille ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à leur charge le versement à la communauté d'agglomération de Montpellier d'une somme de 1 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de MM.C..., A...et D...B...est rejeté.<br/>
<br/>
Article 2 : MM.D..., A...et C...B...verseront à la communauté d'agglomération de Montpellier une somme de 1000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à MM.D..., A...et C...B...et à la communauté d'agglomération de Montpellier. Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-01-02 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ENQUÊTES. ENQUÊTE PARCELLAIRE. - DÉPÔT DU DOSSIER EN MAIRIE PAR L'EXPROPRIANT - NOTIFICATION AUX PROPRIÉTAIRES CONCERNÉS (ART. R. 11-22 DU CODE DE L'EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE) - NOTIFICATION À PARTIR DE LA LISTE ÉTABLIE DANS LES CONDITIONS POSÉES À L'ARTICLE R. 11-19 DU MÊME CODE - COURRIER DE NOTIFICATION RETOURNÉ À L'EXPÉDITEUR POUR DÉFAUT DE RÉCLAMATION OU CHANGEMENT D'ADRESSE DU DESTINATAIRE - OBLIGATION POUR L'EXPROPRIANT DE PROCÉDER À DE NOUVELLES RECHERCHES - ABSENCE.
</SCT>
<ANA ID="9A"> 34-02-01-02 Il résulte des dispositions des articles R. 11-19 et R. 11-22 du code de l'expropriation pour cause d'utilité publique que l'expropriant doit notifier le dépôt du dossier d'enquête parcellaire aux propriétaires des immeubles à exproprier figurant sur la liste mentionnée au 2° de l'article R. 11-19 et dont le domicile est connu d'après les renseignements qu'il a pu recueillir auprès du service du cadastre ou du conservateur des hypothèques ou par tout autre moyen. Ces dispositions n'imposent pas à l'expropriant de procéder à de nouvelles recherches lorsque la notification au domicile ainsi déterminé revient avec la mention non réclamé, auquel cas la notification est réputée avoir été régulièrement faite à ce domicile, ou avec la mention n'habite pas à l'adresse indiquée, auquel cas l'affichage en mairie se substitue régulièrement à la formalité de notification individuelle.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
