<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032377994</ID>
<ANCIEN_ID>JG_L_2016_04_000000377317</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/79/CETATEXT000032377994.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/04/2016, 377317, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377317</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:377317.20160407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du ministre du travail, de la solidarité et de la fonction publique du 24 avril 2010 annulant la décision du 27 janvier 2010 par laquelle l'inspecteur du travail de la 21ème section des Hauts-de-Seine l'avait déclaré apte à la reprise du travail. Par un jugement n° 1005635 du 4 février 2013, le tribunal administratif a fait droit à cette demande. <br/>
<br/>
              Par un arrêt n° 13VE01052 du 28 janvier 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société CMA-CGM contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 avril et 10 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la société CMA-CGM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société CMA-CGM ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une décision du 27 janvier 2010, l'inspecteur du travail de la 21ème section des Hauts-de-Seine, saisi par M.B..., salarié protégé employé par la société CMA-CGM, d'une contestation de l'avis du médecin du travail le déclarant inapte à la reprise de son emploi de responsable commercial, a déclaré ce dernier apte et préconisé la recherche d'un reclassement à l'essai sur un poste d'un autre établissement ; que, sur le recours hiérarchique de la société, le ministre chargé du travail a, par une décision du 24 avril 2010, d'une part, annulé cette décision de l'inspecteur du travail et, d'autre part, estimé qu'il n'y avait plus lieu de se prononcer sur l'aptitude du salarié au motif qu'il avait, entre-temps, été licencié ; que, par un jugement du 4 février 2013, le tribunal administratif de Cergy-Pontoise a annulé pour excès de pouvoir cette décision au motif que la circonstance que M. B...avait été licencié à la date à laquelle le ministre avait eu à se prononcer était sans effet sur la compétence de ce dernier pour connaître de cette contestation ; que la société CMA-CGM se pourvoit en cassation contre l'arrêt du 28 janvier 2014 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre ce jugement ; <br/>
<br/>
              2. Considérant que, contrairement à ce que soutient la société requérante, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit en jugeant que le tribunal administratif n'avait pas porté une atteinte excessive à une relation contractuelle en cours ni privé cette société de son droit au recours en ayant annulé la décision du 24 avril 2010 en tant que le ministre se déclarait incompétent pour se prononcer sur l'aptitude du salarié en faisant application au litige de la jurisprudence issue de la décision n° 334834 du 27 juin 2011 du Conseil d'Etat, statuant au contentieux, qui juge que la seule circonstance que le salarié en cause a été licencié est sans effet sur la compétence de l'administration pour se prononcer sur le recours prévu par l'article L. 4624-1 du code du travail concernant l'aptitude d'un salarié protégé ; qu'en revanche, en se fondant sur le motif tiré de ce que la décision ministérielle du 24 avril 2010 présentait un caractère indivisible pour écarter le moyen tiré de ce que le tribunal administratif l'avait, à tort, annulée dans son intégralité, la cour administrative d'appel de Versailles a entaché son arrêt d'erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, la société CMA-CGM est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société CMA-CGM au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 28 janvier 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société CMA-CGM, à M. A...B...et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
