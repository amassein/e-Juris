<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039357577</ID>
<ANCIEN_ID>JG_L_2019_11_000000424424</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/75/CETATEXT000039357577.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 08/11/2019, 424424</TITRE>
<DATE_DEC>2019-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424424</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424424.20191108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt du 18 septembre 2018, enregistré le 24 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la cour d'appel d'Aix-en-Provence a sursis à statuer sur le litige opposant M. A... B... à Pôle emploi et a saisi le Conseil d'Etat de la question de la légalité de l'arrêté du ministre du travail, de l'emploi et de la santé du 15 juin 2011 portant agrément des accords d'application numérotés 1 à 24 relatifs à la convention du 6 mai 2011 relative à l'indemnisation du chômage, en tant que cet arrêté agrée le troisième paragraphe de l'accord d'application n° 9, au regard des dispositions de l'article L. 5422-20 du code du travail.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 5422-20 du code du travail, dans sa rédaction en vigueur à la date d'adoption de l'arrêté en litige : " Les mesures d'application des dispositions du présent chapitre [relatif au régime d'assurance des travailleurs involontairement privés d'emploi], à l'exception des articles L. 5422-14 à L. 5422-16, font l'objet d'accords conclus entre les organisations représentatives d'employeurs et de salariés. / Ces accords sont agréés dans les conditions définies par la présente section. / En l'absence d'accord ou d'agrément de celui-ci, les mesures d'application sont déterminées par décret en Conseil d'Etat ". L'article L. 5422-21 du même code prévoit que : " L'agrément rend obligatoires les dispositions de l'accord pour tous les employeurs et salariés compris dans le champ d'application professionnel et territorial de cet accord (...) ". L'article L. 5422-22 de ce code, dans sa rédaction alors applicable, précise que : " (...) Ces accords doivent être conformes aux dispositions légales et réglementaires en vigueur (...) ". Enfin, aux termes de l'article R. 5422-16 de ce code, dans sa rédaction applicable à la même date : " L'agrément des accords mentionnés à l'article L. 5422-22 est délivré par le ministre chargé de l'emploi, après avis du Conseil national de l'emploi (...) ".<br/>
<br/>
              2. Les conventions d'assurance chômage, conclues entre les organisations représentatives d'employeurs et de salariés, ont le caractère de conventions de droit privé. Toutefois, leur entrée en vigueur est subordonnée à l'intervention d'un arrêté ministériel d'agrément, qui a le caractère d'un acte administratif réglementaire et qui les rend obligatoires pour tous les employeurs et salariés compris dans leur champ d'application. D'une part, le juge judiciaire a compétence pour apprécier la validité d'une telle convention et peut en écarter, le cas échéant, l'application, alors même que l'arrêté qui l'agrée n'aurait pas été contesté devant le juge administratif. D'autre part, compte tenu de la nature particulière de ces conventions, auxquelles le législateur a confié le soin de définir les mesures prises pour l'application de la loi et dont il a subordonné l'entrée en vigueur à l'intervention d'un arrêté ministériel d'agrément, le juge administratif, compétemment saisi d'une contestation mettant en cause la légalité de cet agrément, a également compétence pour se prononcer sur les moyens mettant en cause la validité de la convention. <br/>
<br/>
              3. En application de l'article L. 5422-20 du code du travail, le ministre du travail, de l'emploi et de la santé a, par un arrêté du 15 juin 2011, agréé les accords d'application numérotés 1 à 24 relatifs à la convention du 6 mai 2011 relative à l'indemnisation du chômage. La cour d'appel d'Aix-en-Provence a sursis à statuer sur le litige opposant devant elle M. B... à Pôle Emploi et saisi le Conseil d'Etat à titre préjudiciel de la question de la légalité de cet arrêté du 15 juin 2011 en tant qu'il a agréé le troisième paragraphe de l'accord d'application n° 9 de la convention, aux termes duquel : " (...) lorsque la période d'activité non déclarée est d'une durée supérieure à 3 jours calendaires au cours du mois civil considéré, elle n'est pas prise en compte pour la recherche de l'affiliation en  vue d'une réadmission (...), et les rémunérations correspondantes ne sont pas incluses dans le  salaire  de  référence ", en l'interrogeant sur la compétence des organisations représentatives d'employeurs et de salariés pour adopter de telles clauses. <br/>
<br/>
              4. Ainsi qu'il a été dit au point 1, il appartient aux organisations représentatives d'employeurs et de salariés, en vertu de l'article L. 5422-20 du code de travail, dans sa rédaction alors applicable, de fixer par voie d'accord les " mesures d'application " des dispositions du chapitre II du titre II du livre IV de la cinquième partie du code du travail, à l'exception de ses articles L. 5422-14 à L. 5422-16. Aucune disposition de ce chapitre ne régit les conditions dans lesquelles les droits à l'assurance chômage peuvent être réduits ou supprimés en cas de méconnaissance d'une obligation déclarative par un travailleur privé d'emploi. Le contrôle et les sanctions applicables font d'ailleurs l'objet du chapitre VI du même titre, au sein duquel l'article L. 5426-9 prévoit que : " Un décret en conseil d'Etat détermine les modalités d'application du présent chapitre (...) ". De même, les obligations du demandeur d'emploi sont déterminées au chapitre Ier du titre Ier du même livre IV, au sein duquel l'article L. 5411-10 renvoie également à un décret en Conseil d'Etat la détermination de ses conditions d'application. Par suite, les parties à l'accord n'étaient pas compétentes pour prévoir une réduction des droits des travailleurs privés d'emploi qui auraient omis de déclarer, dans les conditions prévues par cet accord, des périodes d'activité. Les stipulations du troisième paragraphe de l'accord d'application n° 9 de la convention du 6 mai 2011 relative à l'indemnisation du chômage ne pouvaient ainsi légalement faire l'objet d'un agrément.<br/>
<br/>
              5. Il résulte de ce qui précède que l'arrêté du ministre du travail, de l'emploi et de la santé du 15 juin 2011 portant agrément des accords d'application numérotés 1 à 24 relatifs à la convention du 6 mai 2011 relative à l'indemnisation du chômage est illégal en tant qu'il agrée le troisième paragraphe de l'accord d'application n° 9.<br/>
<br/>
              6. Enfin, il n'appartient pas au Conseil d'Etat, saisi de la question de la légalité d'un acte administratif par le juge judiciaire, de se prononcer sur les effets de la déclaration d'illégalité qu'il prononce.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il est déclaré que l'arrêté du ministre du travail, de l'emploi et de la santé du 15 juin 2011 portant agrément des accords d'application numérotés 1 à 24 relatifs à la convention du 6 mai 2011 relative à l'indemnisation du chômage est illégal en tant qu'il agrée les stipulations du troisième paragraphe de l'accord d'application n° 9.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à Pôle emploi, à la ministre du travail et à la cour d'appel d'Aix-en-Provence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-04-02 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. CONTENTIEUX DE L'APPRÉCIATION DE LA LÉGALITÉ. - QUESTION PRÉJUDICIELLE POSÉE PAR L'AUTORITÉ JUDICIAIRE - COMPÉTENCE DU CONSEIL D'ETAT POUR SE PRONONCER SUR LES EFFETS DE LA DÉCLARATION D'ILLÉGALITÉ QU'IL PRONONCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-04-02-02 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. CONTENTIEUX DE L'APPRÉCIATION DE LA LÉGALITÉ. CAS OÙ UNE QUESTION PRÉJUDICIELLE NE S'IMPOSE PAS. - CONVENTIONS D'ASSURANCE CHÔMAGE - COMPÉTENCE POUR EN APPRÉCIER LA VALIDITÉ - 1) COMPÉTENCE DU JUGE JUDICIAIRE - EXISTENCE - 2) COMPÉTENCE DU JUGE ADMINISTRATIF - EXISTENCE, DANS LE CADRE D'UN RECOURS CONTRE L'ARRÊTÉ MINISTÉRIEL D'AGRÉMENT [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-023 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - CHAMP D'APPLICATION - EXCLUSION - DÉCLARATION D'ILLÉGALITÉ PRONONCÉE EN RÉPONSE À UNE QUESTION PRÉJUDICIELLE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">66-02-03 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. AGRÉMENT DE CERTAINES CONVENTIONS COLLECTIVES. - CONVENTIONS D'ASSURANCE CHÔMAGE - COMPÉTENCE POUR EN APPRÉCIER LA VALIDITÉ - 1) COMPÉTENCE DU JUGE JUDICIAIRE - EXISTENCE - 2) COMPÉTENCE DU JUGE ADMINISTRATIF - EXISTENCE, DANS LE CADRE D'UN RECOURS CONTRE L'ARRÊTÉ MINISTÉRIEL D'AGRÉMENT [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - CONVENTIONS D'ASSURANCE CHÔMAGE - COMPÉTENCE POUR EN APPRÉCIER LA VALIDITÉ - 1) COMPÉTENCE DU JUGE JUDICIAIRE - EXISTENCE - 2) COMPÉTENCE DU JUGE ADMINISTRATIF - EXISTENCE, DANS LE CADRE D'UN RECOURS CONTRE L'ARRÊTÉ MINISTÉRIEL D'AGRÉMENT [RJ1].
</SCT>
<ANA ID="9A"> 17-04-02 Il n'appartient pas au Conseil d'Etat, saisi de la question de la légalité d'un acte administratif par le juge judiciaire, de se prononcer sur les effets de la déclaration d'illégalité qu'il prononce.</ANA>
<ANA ID="9B"> 17-04-02-02 Les conventions d'assurance chômage, conclues entre les organisations représentatives d'employeurs et de salariés, ont le caractère de conventions de droit privé. Toutefois, leur entrée en vigueur est subordonnée à l'intervention d'un arrêté ministériel d'agrément, qui a le caractère d'un acte administratif réglementaire et qui les rend obligatoires pour tous les employeurs et salariés compris dans leur champ d'application.... ,,1) D'une part, le juge judiciaire a compétence pour apprécier la validité d'une telle convention et peut en écarter, le cas échéant, l'application, alors même que l'arrêté qui l'agrée n'aurait pas été contesté devant le juge administratif.... ,,2) D'autre part, compte tenu de la nature particulière de ces conventions, auxquelles le législateur a confié le soin de définir les mesures prises pour l'application de la loi et dont il a subordonné l'entrée en vigueur à l'intervention d'un arrêté ministériel d'agrément, le juge administratif, compétemment saisi d'une contestation mettant en cause la légalité de cet agrément, a également compétence pour se prononcer sur les moyens mettant en cause la validité de la convention.</ANA>
<ANA ID="9C"> 54-07-023 Il n'appartient pas au Conseil d'Etat, saisi de la question de la légalité d'un acte administratif par le juge judiciaire, de se prononcer sur les effets de la déclaration d'illégalité qu'il prononce.</ANA>
<ANA ID="9D"> 66-02-03 Les conventions d'assurance chômage, conclues entre les organisations représentatives d'employeurs et de salariés, ont le caractère de conventions de droit privé. Toutefois, leur entrée en vigueur est subordonnée à l'intervention d'un arrêté ministériel d'agrément, qui a le caractère d'un acte administratif réglementaire et qui les rend obligatoires pour tous les employeurs et salariés compris dans leur champ d'application.... ,,1) D'une part, le juge judiciaire a compétence pour apprécier la validité d'une telle convention et peut en écarter, le cas échéant, l'application, alors même que l'arrêté qui l'agrée n'aurait pas été contesté devant le juge administratif.... ,,2) D'autre part, compte tenu de la nature particulière de ces conventions, auxquelles le législateur a confié le soin de définir les mesures prises pour l'application de la loi et dont il a subordonné l'entrée en vigueur à l'intervention d'un arrêté ministériel d'agrément, le juge administratif, compétemment saisi d'une contestation mettant en cause la légalité de cet agrément, a également compétence pour se prononcer sur les moyens mettant en cause la validité de la convention.</ANA>
<ANA ID="9E"> 66-10-02 Les conventions d'assurance chômage, conclues entre les organisations représentatives d'employeurs et de salariés, ont le caractère de conventions de droit privé. Toutefois, leur entrée en vigueur est subordonnée à l'intervention d'un arrêté ministériel d'agrément, qui a le caractère d'un acte administratif réglementaire et qui les rend obligatoires pour tous les employeurs et salariés compris dans leur champ d'application.... ,,1) D'une part, le juge judiciaire a compétence pour apprécier la validité d'une telle convention et peut en écarter, le cas échéant, l'application, alors même que l'arrêté qui l'agrée n'aurait pas été contesté devant le juge administratif.... ,,2) D'autre part, compte tenu de la nature particulière de ces conventions, auxquelles le législateur a confié le soin de définir les mesures prises pour l'application de la loi et dont il a subordonné l'entrée en vigueur à l'intervention d'un arrêté ministériel d'agrément, le juge administratif, compétemment saisi d'une contestation mettant en cause la légalité de cet agrément, a également compétence pour se prononcer sur les moyens mettant en cause la validité de la convention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 23 mars 2012, Fédération SUD Santé Sociaux, n° 331805, p. 102.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
