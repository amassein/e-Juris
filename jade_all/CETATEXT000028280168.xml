<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028280168</ID>
<ANCIEN_ID>JG_L_2013_12_000000345518</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/28/01/CETATEXT000028280168.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 06/12/2013, 345518, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345518</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:345518.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 4 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 3 de l'arrêt n° 09NT02083 du 8 novembre 2010 par lesquels la cour administrative d'appel de Nantes a, d'une part, rejeté son recours tendant à l'annulation de l'article 1er du jugement n° 08-1871 du 7 mai 2009 par lequel le tribunal administratif de Caen a prononcé la réduction d'une somme de 858 802 euros des bases d'imposition de M. A... à l'impôt sur le revenu au titre de l'année 2003 et réduit en conséquence les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles celui-ci avait été assujetti au titre de la même année, et a, d'autre part, mis à la charge de l'Etat le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>	1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SCI Massia, détenue par M. et MmeA..., a perçu en 2003, à la suite de la destruction par incendie de deux poulaillers à usage industriel dont elle était propriétaire et qu'elle donnait en location, une indemnité d'assurance destinée à compenser notamment le coût de reconstruction de ces bâtiments, à hauteur de 858 802 euros ; que l'administration fiscale a réintégré cette somme dans les recettes imposables de la société au titre de l'année 2003 et, par suite, dans la catégorie des revenus fonciers de M. et Mme A..., à concurrence de leur quote-part dans les droits de la société ; que le tribunal administratif de Caen a fait droit à la demande de M. A...tendant à la décharge des impositions correspondant à la réintégration de l'indemnité dévolue à la reconstruction des poulaillers par un jugement du 7 mai 2009 confirmé par un arrêt du 8 novembre 2010 de la cour administrative d'appel de Nantes contre lequel le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation ;<br/>
<br/>
              2. Considérant que l'article 29 du code général des impôts dispose que les subventions et indemnités destinées à financer des charges déductibles sont comprises dans le revenu brut des immeubles ou parties d'immeubles donnés en location ; qu'aux termes de l'article 31 du même code, dans sa rédaction applicable aux impositions en litige : " I. Les charges de la propriété déductibles pour la détermination du revenu net comprennent : / (...) 2° Pour les propriétés rurales : (...) / c Les dépenses d'amélioration non rentables afférentes aux éléments autres que les locaux d'habitation et effectivement supportées par le propriétaire. Les dépenses engagées pour la construction d'un nouveau bâtiment d'exploitation rurale, destiné à remplacer un bâtiment de même nature, vétuste ou inadapté aux techniques modernes de l'agriculture, sont considérées comme des dépenses d'amélioration non rentables à condition que la construction nouvelle n'entraîne pas une augmentation du fermage ; (...) " ;<br/>
<br/>
              3. Considérant que, pour rejeter les conclusions en appel du ministre, la cour administrative d'appel a jugé que l'administration avait à tort estimé que l'indemnité destinée à la reconstruction des bâtiments agricoles détruits par l'incendie était destinée à financer des charges déductibles au titre des dispositions du c du 2° du I de l'article 31 du code général des impôts, dès lors qu'il ne résultait pas de l'instruction et qu'il n'était d'ailleurs même pas allégué que ces bâtiments étaient vétustes ou inadaptés aux techniques modernes ; qu'en estimant ainsi que la destruction des bâtiments par un incendie ne suffisait pas à les faire regarder comme ayant été, avant leur destruction, vétustes ou inadaptés aux techniques modernes, au sens des dispositions précitées, la cour n'a commis ni erreur de droit ni erreur de qualification juridique ; que, par suite, le ministre n'est pas fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, est rejeté.<br/>
Article 2 : L'Etat versera à M. A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
