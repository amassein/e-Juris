<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030253281</ID>
<ANCIEN_ID>JG_L_2015_02_000000382876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/25/32/CETATEXT000030253281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 17/02/2015, 382876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382876.20150217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. E...B...a demandé au tribunal administratif de Toulouse d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 en vue de l'élection des conseillers municipaux et des conseillers communautaires de la commune de La Salvetat-Saint-Gilles (Haute-Garonne). Par un jugement n° 1401638 du 19 juin 2014, le tribunal administratif de Toulouse a rejeté sa protestation.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 juillet et 5 décembre 2014 au secrétariat du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Toulouse du 19 juin 2014 ; <br/>
<br/>
              2°) de faire droit à sa protestation ;<br/>
<br/>
              3°) de mettre à la charge de M. C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M.C....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour de scrutin organisé le 30 mars 2014 en vue de l'élection des conseillers municipaux et des conseillers communautaires de la commune de La Salvetat-Saint-Gilles, la liste " Bien vivre à La Salvetat, pour une Salvetat citoyenne ", conduite par M.C..., a obtenu 44,06 % des suffrages exprimés et vingt-et-un élus au conseil municipal, tandis que la liste " La Salvetat ensemble ", conduite par M.B..., maire sortant, obtenait 39,61 % des suffrages et six élus et la liste "Cap@venir ", conduite par M.A..., obtenait 16,32 % des suffrages et deux élus au conseil municipal.<br/>
<br/>
              2. Le deuxième alinéa de l'article L. 52-8 du code électoral, applicable dans toutes les communes, y compris celles, comme la commune de La Salvetat-Saint-Gilles, dans lesquelles les dispositions combinées des articles L. 52-11 et L. 118-3 du même code ne sont pas applicables, dispose que : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ". <br/>
<br/>
              3. Eu égard à l'objet de la législation relative, d'une part, à la transparence financière de la vie politique, d'autre part, au financement des campagnes électorales et à la limitation des dépenses électorales, une personne morale de droit privé qui s'est assignée un but politique ne peut être regardée comme un " parti ou groupement politique " au sens de l'article L. 52-8 du code électoral que si elle relève des articles 8, 9 et 9-1 de la loi du 11 mars 1988 relative à la  transparence financière de la vie politique ou s'est soumise aux règles  fixées par les articles 11 à 11-7 de la même loi qui imposent notamment aux partis et groupements politiques de ne recueillir des fonds que par l'intermédiaire d'un mandataire qui peut être soit une personne physique dont le nom est déclaré à la préfecture soit une association de financement agréée par la Commission nationale des comptes de campagne et des financements politiques. En l'espèce, il résulte de l'instruction que l'association " Bien vivre à La Salvetat " ne satisfait pas à ces conditions et ne peut, par suite, être regardée comme un " parti ou groupement politique " au sens de l'article L. 52-8 du code électoral. Dès lors, elle ne pouvait participer au financement de la campagne électorale de la liste " Bien vivre à La Salvetat, pour une Salvetat citoyenne ", ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. <br/>
<br/>
              4. En premier lieu, il résulte de l'instruction que la liste " Bien vivre à La Salvetat, pour une Salvetat citoyenne ", dont le nom est proche de celui de l'association " Bien vivre à La Salvetat " et permet à la liste de s'inscrire dans la continuité des actions conduites par l'association, a utilisé, sur ses documents de propagande et ses bulletins de vote, le logo de l'association " Bien vivre à La Salvetat " et a bénéficié d'une mention de l'adresse de son blog, assortie d'un lien hypertexte, sur le site de l'association. Toutefois, d'une part, l'utilisation du nom et du logo de l'association  " Bien vivre à La Salvetat " ne peut être regardée comme un don ou un avantage accordé par cette association à la liste conduite par M.C..., en méconnaissance des dispositions  de l'article L. 52-8 du code électoral. D'autre part, eu égard à la faible audience du site de l'association, dont l'activité a été mise en sommeil à compter de l'annonce de la constitution de la liste, le renvoi au blog de celle-ci ne peut pas plus être considéré, dans les circonstances de l'espèce, comme un don ou un avantage prohibé par l'article L. 52-8.<br/>
<br/>
              5. En second lieu, il résulte de l'instruction, tout d'abord, que si la liste conduite par M. C...a fait usage d'un duplicopieur appartenant à l'association " Bien vivre à La Salvetat " pour l'impression de ses bulletins de vote, elle a financé elle-même l'achat du papier et de l'encre nécessaires et cette utilisation lui a été facturée par l'association à un niveau raisonnable. Ensuite, il ne résulte pas de l'instruction que la comptabilité de la liste conduite par M.C..., qui avait désigné un mandataire financier, aurait fait l'objet de confusions avec celle de l'association " Bien vivre à La Salvetat ", ni que cette association aurait pris en charge certaines des dépenses exposées pour la campagne. Enfin, il ne résulte pas plus de l'instruction que cette association disposerait de locaux dont elle aurait fait profiter les candidats de la liste conduite par M.C.... Par suite, contrairement à ce que soutient le requérant, la liste conduite par M. C...ne peut être regardée comme ayant bénéficié d'avantages directs ou indirects de la part de l'association " Bien vivre à La Salvetat ", en méconnaissance du deuxième alinéa de l'article L. 52-8 du code électoral.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse a rejeté sa protestation.<br/>
<br/>
              7.  Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions de M. B...présentées à ce titre. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. C...présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : Les conclusions de M. C...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. E...B..., à M. F...A..., à M. D... C...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
