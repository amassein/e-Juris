<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032405411</ID>
<ANCIEN_ID>JG_L_2016_04_000000365172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/54/CETATEXT000032405411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 13/04/2016, 365172, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:365172.20160413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 21 mai 2014, le Conseil d'Etat, statuant au contentieux sur le pourvoi de la société Air France-KLM tendant à l'annulation de l'arrêt du 13 novembre 2012 de la cour administrative d'appel Versailles relatif à sa demande tendant à la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er avril 2000 au 31 mars 2003, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              1°) les dispositions des articles 2§1 et 10§2 de la directive 77/388/CEE du Conseil du 17 mai 1977 doivent-elles être interprétées en ce sens que la délivrance du billet peut être assimilée à l'exécution effective de la prestation de transport et que les sommes conservées par une compagnie aérienne lorsque le titulaire du billet d'avion n'a pas utilisé son billet et que celui-ci est devenu périmé sont soumises à la taxe sur la valeur ajoutée '<br/>
<br/>
              2°) dans cette hypothèse, la taxe collectée doit-elle être reversée au Trésor dès l'encaissement du prix, alors même que le voyage peut ne pas avoir lieu du fait du client '<br/>
<br/>
              Par un arrêt n° C-250/14 du 23 décembre 2015, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 21 mai 2014 ; <br/>
<br/>
              Vu : <br/>
              - la sixième directive 77/388/CEE du Conseil des Communautés européennes du 17 mai 1977, en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ;<br/>
              - le code de la consommation ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Air France-KLM ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à compter de 1999, la société Air France, devenue Air France-KLM, a, pour les vols intérieurs qui sont soumis à la taxe sur la valeur ajoutée, conservé la totalité des sommes acquittées par ses clients correspondant aux billets non échangeables périmés en raison de l'absence des clients lors de l'embarquement ou aux billets échangeables inutilisés dans le délai de leur validité, sans reverser à l'administration fiscale la taxe sur la valeur ajoutée sur le produit de la vente de ces billets au motif que ces recettes ne pouvaient être rattachées à l'exécution d'une prestation de services de transport et qu'elles constituaient dans leur ensemble des indemnités de résiliation n'entrant pas dans le champ d'application de la taxe ; qu'à l'issue d'une vérification de comptabilité de la société Air France-KLM, l'administration lui a notifié, au titre de la période du 1er avril 2000 au 31 mars 2003, des rappels de taxe sur la valeur ajoutée au taux réduit de 5,5 % portant sur les sommes correspondant à la vente de ces billets ; que la société se pourvoit en cassation contre l'arrêt du 13 novembre 2012 par lequel la cour administrative d'appel de Versailles a confirmé le jugement du 9 juin 2011 du tribunal administratif de Cergy-Pontoise qui a rejeté sa demande tendant à la décharge de ces rappels de taxe sur la valeur ajoutée ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 256 du code général des impôts : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel. (...) " ; qu'aux termes de l'article 269 de ce code : " 1. Le fait générateur de la taxe se produit : / a) Au moment où la livraison, l'acquisition intracommunautaire du bien ou la prestation de services est effectué ; / (...) 2. La taxe est exigible : / a) Pour les livraisons et les achats visés au a du 1 et pour les opérations mentionnées aux b et d du même 1, lors de la réalisation du fait générateur ; / (...) c) Pour les prestations de services autres que celles visées au b bis, lors de l'encaissement des acomptes, du prix, de la rémunération ou, sur option du redevable, d'après les débits. (...) " ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 2, point 1, de la sixième directive, dont l'article 256 du code général des impôts assure la transposition : " sont soumises à la TVA les prestations de services effectuées à titre onéreux à l'intérieur du pays par un assujetti agissant en tant que tel " ; que, dans l'arrêt C-250/14 du 23 décembre 2015 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que les services dont la fourniture correspond à l'exécution des obligations découlant d'un contrat de transport aérien de personnes sont l'enregistrement ainsi que l'embarquement des passagers et l'accueil de ces derniers à bord de l'avion au lieu de décollage convenu dans le contrat de transport en cause, le départ de l'appareil à l'heure prévue, le transport des passagers et de leurs bagages du lieu de départ au lieu d'arrivée, la prise en charge des passagers pendant le vol et, enfin, le débarquement de ceux-ci, dans des conditions de sécurité, au lieu d'atterrissage et à l'heure convenus dans ce contrat ; que, toutefois, dès lors que la réalisation de ces prestations n'est possible qu'à la condition que le passager de la compagnie aérienne se présente à la date et au lieu de l'embarquement prévus, la contre-valeur du prix versé lors de l'achat du billet est constituée par le droit qu'en tire le passager de bénéficier de l'exécution des obligations découlant du contrat de transport, indépendamment du fait qu'il mette en oeuvre ce droit, la compagnie aérienne réalisant la prestation dès lors qu'elle met le passager en mesure de bénéficier de ces prestations ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article 10, paragraphe 2 de la sixième directive : "  Le fait générateur de la taxe intervient et la taxe devient exigible au moment où la livraison du bien ou la prestation de services est effectuée. (...) / Toutefois en cas de versements d'acomptes avant que la livraison de biens ou la prestation de services ne soit effectuée, la taxe devient exigible au moment de l'encaissement à concurrence du montant encaissé " ; qu'il résulte de l'interprétation de ces dispositions retenue par la Cour de justice de l'Union européenne que si le fait générateur de la taxe sur la valeur ajoutée et son exigibilité interviennent en principe au moment où la livraison du bien ou la prestation de services est effectuée, elle devient toutefois exigible dès l'encaissement, à concurrence du montant encaissé, lorsque des acomptes sont versés avant que la prestation de services ne soit effectuée ; que, pour que la taxe sur la valeur ajoutée soit exigible sans que la prestation ait encore été effectuée, il faut et il suffit que tous les éléments pertinents du fait générateur, c'est-à-dire de la future prestation, soient déjà connus et donc, en particulier, que, au moment du versement de l'acompte, les biens ou les services soient désignés avec précision ; qu'enfin, le caractère intégral et non partiel du paiement du prix n'est pas susceptible de remettre en cause une telle interprétation ; que dans l'arrêt C-250/14 du 23 décembre 2015, la Cour de justice de l'Union européenne a dit pour droit, d'une part, que s'agissant des prestations en litige, les conditions d'application du paragraphe 2 précité de l'article 10 de la directive étaient susceptibles d'être réunies pour autant que l'ensemble des éléments de la future prestation de transport sont déjà connus et identifiés avec précision au moment de l'achat du billet, d'autre part, que dès lors que dans l'hypothèse d'un passager défaillant, la compagnie aérienne qui vend un billet de transport remplit ses obligations contractuelles en mettant le passager en mesure de faire valoir ses droits aux prestations prévues par le contrat de transport, la taxe sur la valeur ajoutée acquittée au moment de l'achat du billet d'avion par le passager qui n'a pas utilisé son billet devient exigible au moment de l'encaissement du prix du billet, que ce soit par la compagnie aérienne elle-même, par un tiers agissant en son nom et pour son compte, ou par un tiers agissant en son nom propre, mais pour le compte de la compagnie aérienne ; <br/>
<br/>
              5. Considérant, en premier lieu, que,  dès lors que l'ensemble des éléments de la future prestation de transport sont déjà connus et identifiés avec précision au moment de l'achat du billet, la cour a pu juger sans erreur de droit, d'une part, que le fait générateur de la prestation acquise par le passager est constitué par la réalisation effective du voyage ou par l'expiration des obligations contractuelles du transporteur aérien, d'autre part, que l'exigibilité de la taxe résulte de l'encaissement du prix par l'opérateur en charge de la réalisation de la prestation de service de transport ; que la cour n'a ainsi méconnu ni les dispositions de l'article 1234 du code civil relatif à l'extinction des obligations contractuelles ni les dispositions précitées du code général des impôts ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'en jugeant que l'obligation née du contrat de transport pour la société consistait à donner au passager le droit d'être transporté, exerçable à sa convenance pendant un an, la cour, qui n'a pas déduit d'obligations distinctes au titre de la réservation du billet et du transport lui-même, n'a entaché son arrêt ni de dénaturation ni d'erreur de qualification juridique des faits ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'ainsi que l'a relevé la Cour de justice de l'Union européenne dans l'arrêt C-250/14 du 23 décembre 2015, le prix payé par le passager défaillant correspond à l'intégralité du prix à payer et le montant conservé par les compagnies aériennes ne vise pas à indemniser un préjudice qu'elles auraient subi du fait de la défaillance d'un passager, mais constitue une rémunération et ce, même si le passager n'a pas bénéficié du transport ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que les sommes conservées à la suite de l'inexécution définitive de la prestation de transport devaient être soumises à cette taxe en relevant que le passager ne pouvait être regardé comme ayant résilié le contrat que dans le cas où il exerce sa possibilité de se dédire d'un contrat de transport remboursable ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la société Air France KLM doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Air France-KLM est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Air France-KLM et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
