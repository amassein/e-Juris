<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032712993</ID>
<ANCIEN_ID>JG_L_2016_06_000000381288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/29/CETATEXT000032712993.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 15/06/2016, 381288, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:381288.20160615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Châlons-en-Champagne la décharge des cotisations supplémentaire d'impôt sur le revenu et de contributions sociales, assorties notamment des majorations exclusives de bonne foi, auxquelles il a été assujetti au titre de l'année 2006. Par un jugement n° 1100517 du 13 février 2013, le tribunal a rejeté sa demande.<br/>
<br/>
              Par les articles 1er à 3 d'un arrêt n° 13NC00661 du 17 avril 2014, la cour administrative d'appel de Nancy, faisant partiellement droit à l'appel formé par M.B..., a, respectivement, prononcé la décharge des majorations exclusives de bonne foi, réformé en ce sens le jugement de première instance et rejeté le surplus des conclusions en décharge du contribuable ainsi que celles présentées par l'intéressé au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 juin et 12 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de cet arrêt du 17 avril 2014 de la cour administrative d'appel de Nancy ;<br/>
<br/>
              2°) réglant, dans cette mesure, l'affaire au fond, de faire droit à celles de ses conclusions d'appel restant en litige ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 65-566 du 12 juillet 1965 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B...a acquis, de janvier 1986 à décembre 1989, 7 106 des 14 211 parts numérotées du GAECB..., d'une valeur nominale de 100 francs (15, 24 euros) par part ; que ce GAEC a été ultérieurement transformé en société civile d'exploitation agricole (SCEA) ; que, le 19 mai 2006, la société a procédé à une augmentation de capital, par la création de 3 555 parts supplémentaires numérotées, d'une valeur nominale de 15 euros, assorties d'une prime d'émission de 85 euros et attribuées, pour les 1 777 dernières, à M. A...B...; que, le même jour, M. B...a cédé 1 776 de ces 1 777 nouvelles parts numérotées, pour un prix unitaire de 100 euros, à une société civile dont il détenait, avec son épouse, la totalité du capital ; que cette cession n'a donné lieu à aucune déclaration de plus-value auprès de l'administration fiscale ; qu'à la suite d'une vérification de la comptabilité du GAEC et de la SCEA, le vérificateur a estimé qu'en application des dispositions du 6 de l'article 39 duodecies du code général des impôts, la cession de 1 776 parts intervenue le 19 mai 2006 devait être réputée avoir porté sur les parts les plus anciennes constituant le portefeuille d'actifs professionnels de M.B..., c'est-à-dire, en l'espèce, sur des parts sociales acquises avant 1990 à un coût égal à leur valeur nominale ; que l'administration fiscale a ainsi considéré que cette cession avait fait naître, au titre de l'année 2006, une plus-value imposable à l'impôt sur le revenu, selon le régime des plus-values professionnelles à long terme, ainsi qu'aux contributions sociales ; que les impositions litigieuses procèdent, en droits et majorations, de cette rectification ;<br/>
<br/>
              2. Considérant, en premier lieu, que le I de l'article 151 nonies du code général des impôts disposait, dans sa rédaction applicable aux faits de l'espèce, que : " Lorsqu'un contribuable exerce son activité professionnelle dans le cadre d'une société dont les bénéfices sont, en application des articles 8 et 8 ter, soumis en son nom à l'impôt sur le revenu dans la catégorie des bénéfices agricoles réels (...), ses droits ou parts dans la société sont considérés notamment pour l'application des articles 38, 72 et 93, comme des éléments d'actif affectés à l'exercice de la profession (...) " ; que l'article 39 duodecies du même code disposait que : " 1. Par dérogation aux dispositions de l'article 38, les plus-values provenant de la cession d'éléments de l'actif immobilisé sont  soumises à des régimes distincts suivant qu'elles sont réalisées à court ou à long terme. 2. Le régime des plus-values à court terme est  applicable : a) aux plus-values provenant de la cession d'éléments acquis ou créés depuis moins de deux ans (...) 3. Le régime des plus-values à long terme est applicable aux plus-values autres que celles définies au 2. (...) 6. Pour l'application du présent article, les cessions de titres compris dans le portefeuille sont réputées porter par priorité sur les titres de même nature acquis ou souscrits à la date la plus ancienne (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des termes mêmes de l'article 39 duodecies cité ci-dessus, ainsi, au demeurant, que des travaux préparatoires à l'adoption du paragraphe 5 de l'article 9 de la loi du 12 juillet 1965 modifiant l'imposition des entreprises et des revenus de capitaux mobiliers duquel ces dispositions sont issues, que le législateur a entendu fixer, par principe et sous la seule réserve des dérogations expressément prévues au même article, la règle selon laquelle, lorsqu'un contribuable qui relève des dispositions du I de l'article 151 nonies cède des valeurs mobilières constituant des éléments d'actif affectés à l'exercice de sa profession, ces cessions sont réputées porter sur les titres de même nature acquis ou souscrits à la date la plus ancienne, quelle qu'en soit la date d'acquisition effective ; qu'en jugeant que faute d'entrer dans le champ d'aucune des dérogations prévues à l'article 39 duodecies du code général des impôts, la cession litigieuse devait être réputée porter sur des titres de portefeuille, au sens et pour l'application des dispositions du 6 de cet article, et que la numérotation des parts cédées par M.B...  n'était pas de nature à faire obstacle à l'application de la règle analysée ci-dessus, alors même qu'une telle numérotation aurait permis d'établir la date exacte d'acquisition et le coût réel d'acquisition de chacun des titres cédés la cour n'a pas méconnu les dispositions législatives précitées et a donné une exacte qualification juridique aux faits qui lui étaient soumis ; qu'elle n'était pas tenue de répondre à chacun des arguments soulevés devant elle ni d'adopter une motivation différente de celle qu'avaient retenu les premiers juges et a suffisamment motivé son arrêt sur ce point ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des pièces du dossier soumis aux juges d'appel que, pour faire obstacle à l'application de la règle de calcul des plus-values professionnelles instituée par les dispositions législatives mentionnées ci-dessus, M.B...  se prévalait sur le fondement de l'article L. 80 A du livre des procédures fiscales, des termes du paragraphe 39 d'une instruction fiscale du 18 mars 1966, selon lesquels " le régime de l'article 39 duodecies et les modalités d'application de l'article 38 octies sont donc exclusivement réservés aux titres pour lesquels il est difficile d'appréhender la durée de détention ainsi que la valeur d'acquisition d'origine " ; que toutefois, des mêmes pièces que cette instruction contenait également la disposition suivante : " Principe : En matière de cession de titres de portefeuille, l'appréciation de la durée de détention de ces titres pose un problème particulier lorsque des valeurs de même nature ont été acquises ou souscrites à des dates différentes. A cet égard, l'article 9, paragraphe 5 de la loi du 12 juillet 1965 (...) dispo[se] que les cessions de titres en portefeuille sont réputées porter par priorité sur les titres de même nature, acquis ou souscrits à la date la plus ancienne " ; qu'il résulte de la combinaison de ces dispositions que l'instruction en cause ne donnait pas des dispositions de l'article 39 duodecies du code général des impôts une interprétation différente de celle dont les juges d'appel venaient de faire application ; que, dès lors, en jugeant, pour ce motif, que M. B...ne pouvait utilement se prévaloir de l'interprétation de la loi fiscale contenue dans cette instruction, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis, ni entaché son arrêt d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'article 3 de l'arrêt qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Monsieur A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
