<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030642914</ID>
<ANCIEN_ID>JG_L_2015_04_000000374676</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/29/CETATEXT000030642914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 15/04/2015, 374676, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374676</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374676.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
Procédure contentieuse antérieure<br clear="none"/>
<br clear="none"/>
La société Paris Saint-Germain Football a demandé au tribunal administratif de Montreuil de prononcer la restitution des cotisations de taxe sur les salaires qu'elle avait acquittées au titre des années 2007 à 2009 et le remboursement de la taxe sur la valeur ajoutée qu'elle n'avait pu déduire au titre de la période du 1er janvier 2007 au 31 décembre 2009. Par deux jugements n° 1009134-1009135 du 16 juin 2011 et n° 1101507 du 24 novembre 2011, le tribunal administratif de Montreuil a rejeté ces demandes.<br clear="none"/>
<br clear="none"/>
Par un arrêt n° 11VE02706, 12VE00197 du 7 novembre 2013, la cour administrative d'appel de Versailles a rejeté les appels formés par la société Paris Saint-Germain Football contre ces jugements.<br clear="none"/>
<br clear="none"/>
Procédure devant le Conseil d'Etat<br clear="none"/>
<br clear="none"/>
Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 15 janvier, 11 avril, 17 juillet, 10 novembre et 19 décembre 2014 ainsi que le 22 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la société Paris Saint-Germain Football demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler l'arrêt n° 11VE02706, 12VE00197 du 7 novembre 2013 de la cour administrative d'appel de Versailles ;<br clear="none"/>
<br clear="none"/>
2°) à titre subsidiaire, de surseoir à statuer dans l'attente de l'issue de la procédure d'infraction n° 2012/4194 engagée par la Commission européenne ou de saisir la Cour de justice de l'Union européenne à titre préjudiciel de la question relative à l'interprétation de l'article 371 de la directive 2006/112/CE du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée en ce qui concerne le maintien d'une exonération de taxe sur la valeur ajoutée, liée au versement d'un autre impôt, dans les cas où l'autre impôt n'est pas acquitté ;<br clear="none"/>
<br clear="none"/>
3°) réglant l'affaire au fond, de faire droit à ses appels ou, à titre subsidiaire, de prononcer, outre la restitution de la taxe sur les salaires demandée, le remboursement de la différence entre la taxe sur la valeur ajoutée qu'elle n'a pu déduire au titre de la période litigieuse et la taxe sur la valeur ajoutée qu'elle est réputée avoir collectée au titre de cette période, au taux de 5,5 %, après avoir, le cas échéant, interrogé la Cour de justice de l'Union européenne à titre préjudiciel sur les modalités de calcul des droits de taxe sur la valeur ajoutée qu'elle est réputée avoir collectée ;<br clear="none"/>
<br clear="none"/>
4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu la note en délibéré, enregistrée le 26 mars 2015, présentée pour la société Paris Saint-Germain Football ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br clear="none"/>
- le code général des impôts ;<br clear="none"/>
- la loi n° 66-10 du 6 janvier 1966 ;<br clear="none"/>
- la loi n° 78-1240 du 29 décembre 1978 ;<br clear="none"/>
- la loi n° 89-936 du 29 décembre 1989 ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Bastien Lignereux, auditeur,<br clear="none"/>
<br clear="none"/>
- les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Paris Saint Germain Football ;<br clear="none"/>
</p>
<p>1. Considérant qu'aux termes de l'article 261 E du code général des impôts, dans sa rédaction applicable aux périodes en litige : " Sont exonérés de la taxe sur la valeur ajoutée : (...) / 3° Les droits d'entrée perçus par les organisateurs de réunions sportives soumises à l'impôt sur les spectacles, jeux et divertissements. " ;<br clear="none"/>
<br clear="none"/>
2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Paris Saint-Germain Football a contesté l'inclusion d'une partie de ses recettes dans le champ de l'exonération prévue par ces dispositions et demandé en conséquence le remboursement de la fraction de taxe sur la valeur ajoutée qu'elle n'a pu déduire, du fait de cette inclusion, au titre de la période du 1er janvier 2007 au 31 décembre 2009 ainsi que la restitution des cotisations de taxe sur les salaires qu'elle a acquittées au titre des années 2007 à 2009 ; que la société Paris Saint-Germain Football se pourvoit en cassation contre l'arrêt du 7 novembre 2013 par lequel la cour administrative d'appel de Versailles a rejeté ses appels contre les jugements du tribunal administratif de Montreuil des 16 juin et 24 novembre 2011 rejetant ces demandes ;<br clear="none"/>
<br clear="none"/>
3. Considérant, en premier lieu, qu'en édictant les dispositions, citées au point 1, du 3° de l'article 261 E du code général des impôts, issues de la loi du 29 décembre 1978 de finances rectificatives pour 1978 et prises pour la transposition de la directive 77/388/CEE du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires, le législateur a entendu maintenir l'exonération dont bénéficiaient auparavant les réunions sportives, sur le fondement du 5° du 1 de l'article 261 du même code qui exonérait " les affaires qui entrent dans le champ de l'impôt sur les spectacles " ; que, par suite, les réunions sportives soumises à l'impôt sur les spectacles au sens des dispositions du 3° de l'article 261 E de ce code sont celles qui entrent dans le champ d'application de cet impôt en vertu de l'article 1559 du même code aux termes duquel : " Les spectacles, jeux et divertissements de toute nature sont soumis à un impôt dans les formes et modalités déterminées par les articles 1560 à 1566. (...) " ;<br clear="none"/>
<br clear="none"/>
4. Considérant que si la loi du 29 décembre 1989 de finances rectificative pour 1989 a prévu, au b du 3° de l'article 1561 du code général des impôts, la faculté pour le conseil municipal d'exempter d'impôt sur les spectacles " l'ensemble des compétitions sportives organisées pendant l'année sur le territoire de la commune ", alors que cette possibilité d'exonération était jusque-là limitée aux seules " réunions exceptionnelles ", il résulte de ce qui précède que cette extension de l'exonération facultative d'impôt sur les spectacles est restée sans incidence sur la portée de l'exonération de taxe sur la valeur ajoutée applicable aux réunions sportives dès lors que ces dernières, même lorsqu'elles en sont exonérées, demeurent dans le champ d’application de l’impôt sur les spectacles ; que la cour n'a, dès lors, pas commis d'erreur de droit sur ce point ;<br clear="none"/>
<br clear="none"/>
5. Considérant, en second lieu, qu'aux termes de l'article 371 de la directive 2006/112/CE du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée, qui reprend les dispositions du b) du 3 de l'article 28 de la sixième directive 77/388/CEE du 17 mai 1977 : " Les États membres qui, au 1er janvier 1978, exonéraient les opérations dont la liste figure à l'annexe X, partie B, peuvent continuer à les exonérer, dans les conditions qui existaient dans chaque État membre concerné à cette même date. " ; que le 1) de la partie B de l'annexe X à cette directive mentionne " la perception de droits d'entrée aux manifestations sportives " ; que ces dispositions interdisent aux Etats membres de l'Union européenne d'introduire de nouvelles exonérations de taxe sur la valeur ajoutée ou d'étendre la portée des exonérations existantes postérieurement au 1er janvier 1978 ;<br clear="none"/>
<br clear="none"/>
6. Considérant qu'il résulte de ce qui a été dit aux points 3 et 4 que l'exonération de taxe sur la valeur ajoutée applicable à la perception de droits d'entrée aux manifestations sportives, qui couvre l'ensemble des manifestations situées dans le champ d'application de l'impôt sur les spectacles, est restée inchangée depuis le 1er janvier 1978 ; que par suite, la cour n'a pas commis d'erreur de droit en jugeant que le maintien de cette exonération ne méconnaissait pas les dispositions de l'article 371 de la directive du 28 novembre 2006 ;<br clear="none"/>
<br clear="none"/>
7. Considérant que, comme elle l'a rendu public, la Commission européenne a clôturé, le 26 février 2015, la procédure d'infraction n° 2012/4194 qu'elle avait engagée par un avis motivé du 10 juillet 2014 ; que, par suite, les conclusions tendant à ce qu'il soit sursis à statuer dans l'attente de l'issue de cette procédure sont devenues sans objet ;<br clear="none"/>
<br clear="none"/>
8. Considérant qu'il résulte de tout ce qui précède et sans qu'il soit besoin de saisir à titre préjudiciel la Cour de justice de l'Union européenne que la société Paris Saint-Germain Football n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : Le pourvoi de la société Paris Saint-Germain Football est rejeté.<br clear="none"/>
Article 2 : La présente décision sera notifiée à la société Paris Saint-Germain Football et au ministre des finances et des comptes publics.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
