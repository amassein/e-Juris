<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028253856</ID>
<ANCIEN_ID>JG_L_2013_11_000000364093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/25/38/CETATEXT000028253856.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 28/11/2013, 364093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:364093.20131128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Natixis Asset Management, dont le siège est 21, quai d'Austerlitz à Paris (75013), représentée par son président directeur général en exercice ; la société Natixis Asset Management demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation et à la modification du premier alinéa de l'article R. 461-1 du code de l'organisation judiciaire ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre, sous astreinte de 500 euros par jour de retard, d'édicter de nouvelles dispositions au sein de l'article R. 461-1 du code de l'organisation judiciaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5000 euros en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ; <br/>
<br/>
              Vu le code de l'organisation judiciaire ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Natixis Asset Management ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la société Natixis Asset Management a, le 31 octobre 2012, soulevé devant le tribunal de grande instance de Paris une question prioritaire de constitutionnalité portant sur la conformité à la Constitution de la portée effective de l'interprétation jurisprudentielle donnée par la Cour de cassation à l'ancien article L. 422-9 du code du travail ; qu'aux termes de l'article R. 461-1 du code de l'organisation judiciaire, à la réception d'une question prioritaire de constitutionalité par la Cour de cassation, l'affaire est distribuée à la chambre qui connaît des pourvois dans la matière considérée ; que la société requérante a saisi le Premier ministre, le 6 août 2012, d'une demande d'abrogation et de modification de ces dispositions au motif qu'elles n'offraient pas de garanties suffisantes pour exclure tout doute sur l'impartialité de la chambre concernée, les parties pouvant craindre que celle-ci refuse de revenir sur une jurisprudence que cette chambre aurait contribué à forger ; que le silence gardé pendant plus de deux mois par le Premier ministre sur cette demande a fait naître une décision implicite de rejet, dont la société Natixis Asset Management demande l'annulation ; <br/>
<br/>
              2. Considérant que l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel a confié aux juridictions suprêmes de l'ordre administratif et judiciaire le soin de se prononcer sur le renvoi au Conseil constitutionnel de la question de la conformité de dispositions législatives aux droits et libertés que la Constitution garantit ; que l'article R. 461-1 du code de l'organisation judiciaire prévoit l'attribution d'une question prioritaire de constitutionnalité à la chambre de la Cour de cassation qui connaît des pourvois dans la matière considérée ; qu'aucune règle ni aucun principe ne fait obstacle à ce qu'une des chambres de la Cour de cassation ayant contribué à forger, sur certains points, l'interprétation à donner de dispositions législatives statue, en application de l'article 61-1 de la Constitution, sur le bien-fondé du renvoi au Conseil constitutionnel d'une question de constitutionnalité mettant en cause la portée effective que cette interprétation jurisprudentielle a conférée à une disposition législative ; que, par suite, les dispositions de l'article R. 461-1 du code de l'organisation judiciaire ne sauraient être regardées comme méconnaissant le principe d'impartialité ou le droit à un recours effectif, garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyens de 1789 et rappelés par les articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              3. Considérant, dès lors, que la société Natixis Asset Management n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que ses conclusions à fin d'injonction et d'astreinte ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : La requête de la société Natixis Asset Management est rejetée. <br/>
 Article 2 : La présente décision sera notifiée la société Natixis Asset Management. Copie en sera adressée pour information à la garde des sceaux, ministre de la justice et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
