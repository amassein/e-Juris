<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065789</ID>
<ANCIEN_ID>JG_L_2020_06_000000432029</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065789.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 29/06/2020, 432029, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432029</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2020:432029.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 21 octobre 2019, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de l'université des Antilles dirigées contre l'ordonnance n° 1900290 du 31 mai 2019 du juge des référés du tribunal administratif de la Guadeloupe en tant seulement que, pour l'exécution de la suspension de l'exécution de la décision du 26 février 2019 par laquelle la direction des ressources humaines de l'université a refusé de reconnaître l'existence du contrat de travail la liant à M. A... B... depuis le 1er janvier 2019, elle a enjoint à l'université, d'une part, de réintégrer M. B... à la date de son éviction et, d'autre part, de verser à l'intéressé une somme au moins égale à sa rémunération des mois de janvier et février 2019 ainsi que les frais de mission engagés au cours de cette même période.<br/>
<br/>
              Le pourvoi a été communiqué à M. B..., qui n'a pas produit de mémoire en défense.<br/>
<br/>
              Le pourvoi a été communiqué à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, qui n'a pas produit d'observations.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de l'université des Antilles ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal (....) ".  Aux termes de L. 521-1 du même code : "Quand une décision administrative fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ". Enfin, aux termes de l'article L. 911-1 du même code : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ". <br/>
<br/>
              2. Si, dans le cas où les conditions posées par l'article L. 521-1 du code de justice administrative sont remplies, le juge des référés peut suspendre l'exécution d'une décision administrative, même de rejet, et assortir cette suspension d'une injonction, s'il est saisi de conclusions en ce sens, ou de l'indication des obligations qui en découleront pour l'administration, les mesures qu'il prescrit ainsi doivent présenter un caractère provisoire. Il suit de là que le juge des référés ne peut, sans excéder sa compétence, ni prononcer l'annulation d'une décision administrative, ni ordonner une mesure qui aurait des effets en tous points identiques à ceux qui résulteraient de l'exécution par l'autorité administrative d'un jugement annulant une telle décision. La suspension de l'exécution d'une décision administrative présentant le caractère d'une mesure provisoire, n'emportant pas les mêmes conséquences qu'une annulation prononcée par le juge administratif, laquelle seule a une portée rétroactive, ne prend effet qu'à la date à laquelle la décision juridictionnelle ordonnant la suspension est notifiée à l'auteur de la décision administrative contestée. <br/>
<br/>
              3. Il résulte de ce qui est dit au point 2 qu'en enjoignant à l'université des Antilles, d'une part, de réintégrer M. B... à la date de la décision litigieuse, soit le 27 février 2019, et non à la date de la notification de sa propre décision à l'université, d'autre part, de verser à M. B... une somme au moins égale à sa rémunération des mois de janvier et février 2019, ainsi que les frais de mission engagés par M. B... au cours de cette même période, le juge des référés du tribunal administratif de la Guadeloupe a prononcé une injonction dépourvue de caractère provisoire dans l'attente de l'intervention d'un jugement au fond et, par suite, méconnu les dispositions de l'article L. 511-1 du code de justice administrative. Dès lors, l'ordonnance du 31 mai 2019 du juge des référés du tribunal administratif de la Guadeloupe doit être annulée dans cette mesure. <br/>
<br/>
              4. Il y a lieu dans les circonstances de l'espèce de régler dans cette mesure l'affaire au titre de la procédure de référé engagée par M. B..., en application de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              5.  Eu égard à ses motifs, la suspension de l'exécution de la décision du 26 février 2019 prononcée par le juge des référés du tribunal administratif de la Guadeloupe implique que l'université des Antilles, d'une part, procède à la réintégration de  M. B... à la date de la notification à cette université de l'ordonnance du 31 mai 2019, jusqu'à ce qu'il soit statué au fond sur la demande de M. B... tendant à l'annulation de cette décision, et au plus tard jusqu'au 31 août 2019, terme du contrat, d'autre part, verse à M. B... son salaire et les éventuels frais de mission à compter de cette même date. Il y a lieu d'enjoindre à l'université des Antilles de prendre ces mesures.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 de l'ordonnance du 31 mai 2019 du juge des référés du tribunal administratif de la Guadeloupe sont annulés. <br/>
Article 2 : Il est enjoint à l'université des Antilles de procéder à la réintégration de M. B... et de verser à M. B... son salaire et les éventuels frais de mission exposés par lui dans les conditions fixées au point 5 de la présente décision.<br/>
Article 3 : La présente décision sera notifiée à l'université des Antilles et à M. A... B....<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à la section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
