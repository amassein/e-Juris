<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074661</ID>
<ANCIEN_ID>JG_L_2020_06_000000433790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/06/2020, 433790, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433790.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... E..., M. C... E... et Mme F... E... ont demandé au tribunal administratif de Caen de condamner le centre hospitalier universitaire (CHU) de Caen à leur verser respectivement les sommes de 319 227,04 euros, 8 382,85 euros et 5 000 euros en réparation des préjudices qu'ils estiment avoir subis en raison des fautes commises par cet établissement lors de la prise en charge de Mme D... E.... Par un jugement n° 1600952 du 8 juin 2017, le tribunal administratif a condamné le CHU de Caen à verser la somme de 84 520,45 euros à Mme D... E... et la somme de 1 000 euros à M. C... E... et a rejeté le suplus des conclusions.<br/>
<br/>
              Par un arrêt n° 17NT02469, 17NT02493 du 21 juin 2019, la cour administrative d'appel de Nantes a porté les sommes que le CHU est condamné à verser à Mme D... E... et à M. C... E... à, respectivement 99 898,99 euros et 5 000 euros, a condamné le CHU à verser à Mme F... E... la somme de 2000 euros et rejeté le surplus des conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 août et 28 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme D... E... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de leurs conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du CHU de Caen la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet, Odent, avocat de Mme E... et autres.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'ils attaquent, Mme E... et autres soutiennent qu'il est entaché :<br/>
              - de dénaturation des pièces du dossier en ce qu'il estime que le taux de perte de chance est de 85% ;<br/>
              - d'erreur de droit en ce qu'il applique le taux de perte de chance au montant d'indemnisation dû au titre des frais d'assistance par un médecin-conseil et des frais de déplacement ;<br/>
              - d'erreur de droit en ce qu'il juge que le préjudice d'incidence professionnelle ne peut donner lieu à aucune indemnisation en l'absence d'inaptitude, même partielle, à l'exercice d'une activité professionnelle ;<br/>
              - de dénaturation des pièces du dossier en ce qu'il estime que la poursuite d'une activité n'aurait pas permis le versement d'un salaire de 1 500 euros par mois ;<br/>
              - de dénaturation des pièces du dossier en ce qu'il estime que la fermeture du bar et sa vente ne sont pas les conséquences des fautes commises par le centre hospitalier.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi, en tant qu'il émane de Mme D... E..., qui sont dirigées contre l'arrêt attaqué en tant qu'il se prononce sur l'indemnisation des frais d'assistance par un médecin conseil et les frais de déplacement. En revanche, aucun des autres moyens soulevés n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de Mme E... et autres sont admises en tant qu'elles émanent de Mme D... E... et qu'elles portent sur l'indemnisation des frais d'assistance par un médecin conseil et des frais de déplacement.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme E... et autres n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme D... E..., première requérante dénommée.<br/>
		Copie en sera adressée au centre hospitalier universitaire de Caen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
