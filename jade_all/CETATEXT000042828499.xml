<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828499</ID>
<ANCIEN_ID>JG_L_2020_12_000000440027</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/12/2020, 440027, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440027</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440027.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. H... K..., Mme J... K..., M. A... K..., Mme E... K..., M. B... K..., Mme C... K..., M. D... K..., Mme I... K..., Mme G... M... et Mme L... N... ont demandé au tribunal administratif de Nantes de condamner le centre hospitalier universitaire (CHU) de Nantes à les indemniser des préjudices qu'ils estiment avoir subis du fait du décès de Mme F... K.... Par un jugement n° 1509518 du 28 mars 2018, le tribunal administratif a condamné le CHU de Nantes à verser aux héritiers de M. H... K... et Mme F... K... la somme de 27 340,45 euros, à M. A... K... et M. H... K... la somme de 4 000 euros chacun, à Mme C... K..., M. D... K... et M. B... K... la somme de 2 000 euros chacun, et a rejeté le surplus de leurs demandes.<br/>
<br/>
              Par un arrêt n° 18NT01815, 18NT02108 du 24 janvier 2020, la cour administrative d'appel de Nantes a, sur appel du CHU de Nantes et de M. K... et autres, ramené à 23 340,45 euros la somme globale à verser à M. H... K..., M. A... K... et M. D... K..., héritiers de M. H... K... et Mme F... K..., et à 1 500 euros chacun la somme à verser à Mme C... K..., M. D... K... et M. B... K....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 avril et 9 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. K... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge du CHU de Nantes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Leduc, Vigand, avocat de M. K... et autres.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              Pour demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'ils attaquent, M. K... et autres soutiennent qu'il est entaché :<br/>
              - de dénaturation des pièces du dossier en ce qu'il écarte tout lien de causalité entre la faute à l'origine du décès de Mme K... et les frais exposés pour la prise en charge de son mari dans un établissement spécialisé ;<br/>
              - d'erreur de droit en ce qu'il rejette les conclusions tendant à l'indemnisation des frais d'hébergement de M. K... en établissement spécialisé sans ordonner un complément d'expertise ;<br/>
              - de dénaturation des pièces du dossier et d'erreur de droit en ce qu'il rejette leur demande tendant à l'indemnisation des frais exposés au titre de l'assistance médicale dont ils ont bénéficié au cours de l'expertise ; <br/>
              - de dénaturation des pièces du dossier dans son évaluation du préjudice moral subi par le mari, les enfants et les petits-enfants de la défunte ; <br/>
              - de dénaturation des pièces du dossier et d'inexacte qualification juridique des faits en ce que, pour rejeter les conclusions d'indemnisation du préjudice moral subi par la soeur et la belle-fille de la défunte, il juge que l'existence d'un lien affectif n'est pas établi.<br/>
<br/>
              Eu égard aux moyen soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'arrêt attaqué en tant qu'il statue, d'une part, sur les frais d'assistance à expertise et, d'autre part, sur le préjudice moral subi par la soeur de la victime. En revanche, aucun des moyens n'est de nature à permettre l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de M. K... et autres dirigées contre l'arrêt attaqué en tant qu'il se prononce sur les frais d'assistance à expertise et sur le préjudice moral de Mme I... K... sont admises.<br/>
<br/>
		Article 2 : Le surplus des conclusions du pourvoi n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. H... K..., premier requérant dénommé.<br/>
		Copie en sera adressée au centre hospitalier universitaire de Nantes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
