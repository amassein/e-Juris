<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956579</ID>
<ANCIEN_ID>JG_L_2015_07_000000370454</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/65/CETATEXT000030956579.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 27/07/2015, 370454</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370454</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Henri Plagnol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370454.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              L'association de défense des propriétaires et résidents de Pont-d'Hérault-Le Sigal-Le Rey (ADPSR) a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir l'arrêté du sous-préfet du Vigan du 9 juillet 2008 portant déclaration d'utilité publique du projet de déviation de la route départementale 999. Par un jugement n° 0802903 du 1er juin 2011, le tribunal administratif de Nîmes a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 11MA02911 du 23 mai 2013, la cour administrative d'appel de Marseille, sur la demande de l'ADPSR, a annulé l'arrêté du sous-préfet du Vigan du 9 juillet 2008 et réformé le jugement du tribunal administratif de Nîmes en ce qu'il avait de contraire à son arrêt.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 juillet et 24 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, le département du Gard demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille du 23 mai 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'association de défense des propriétaires et résidents de Pont-d'Hérault-Le Sigal-Le Rey la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du même code.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Henri Plagnol, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du département du Gard ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 123-16 du code de l'urbanisme, dans sa rédaction en vigueur à la date de l'arrêté en litige, également applicable lorsque le document d'urbanisme est un plan d'occupation des sols : " La déclaration d'utilité publique (...) d'une opération qui n'est pas compatible avec les dispositions d'un plan local d'urbanisme ne peut intervenir que si : / a) L'enquête publique concernant cette opération a porté à la fois sur l'utilité publique ou l'intérêt général de l'opération et sur la mise en compatibilité du plan qui en est la conséquence ; / b) L'acte déclaratif d'utilité publique (...) est pris après que les dispositions proposées pour assurer la mise en compatibilité du plan ont fait l'objet d'un examen conjoint de l'Etat, de l'établissement public mentionné à l'article L. 122-4, s'il en existe un, de la région, du département et des organismes mentionnés à l'article L. 121-4, et après avis du conseil municipal. / La déclaration d'utilité publique emporte approbation des nouvelles dispositions du plan (...) " ; <br/>
<br/>
              2. Considérant que l'opération qui fait l'objet d'une déclaration d'utilité publique ne peut être regardée comme compatible avec un plan d'occupation des sols qu'à la double condition qu'elle ne soit pas de nature à compromettre le parti d'aménagement retenu par la commune dans ce plan et qu'elle ne méconnaisse pas les dispositions du règlement de la zone du plan dans laquelle sa réalisation est prévue ;<br/>
<br/>
              3. Considérant que la cour administrative d'appel de Marseille a relevé que le tracé routier envisagé pour la déviation des routes départementales 999 et 986 passe par des parcelles classées en zone NC sur le territoire de la commune de Sumène ; que le règlement du plan d'occupation des sols de cette commune précise que la zone NC est une zone naturelle à protéger en raison de la valeur économique des sols et réservée à l'exploitation agricole ; que l'article NC 1 de ce règlement fixe la liste exhaustive des occupations et utilisations du sol admises et qu'en vertu de son article NC 2 toutes les occupations et utilisations du sol non prévues à l'article NC 1 sont interdites ; que le projet de déviation des routes départementales 999 et 986, qui est sans rapport avec les besoins de la desserte des constructions autorisées par le règlement en zone NC, ne s'inscrit pas dans l'un des cas d'utilisation du sol autorisés par cet article NC 1 ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit au point 2 ci-dessus qu'en déduisant de ces constatations que le projet en cause ne pouvait être regardé comme " compatible " avec le plan d'occupation des sols de la commune de Sumène au sens des dispositions précitées et que celui-ci aurait dû faire l'objet d'une mise en compatibilité, alors même que les parcelles classées en zone NC de la commune représentent près de 2 000 hectares et que le projet de déviation ne concernerait qu'une superficie d'environ 4 hectares située sur un versant de montagne boisé ne faisant l'objet d'aucune exploitation agricole, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit et a exactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le département du Gard n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de laisser à la charge du département du Gard la contribution pour l'aide juridique mentionnée par l'article R. 761-1 du code de justice administrative dans sa rédaction en vigueur à la date d'introduction du pourvoi ; que les dispositions de l'article L. 761-1 du même code font obstacle à ce qu'il soit fait droit aux conclusions présentées par le département du Gard à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: Le pourvoi du département du Gard est rejeté.<br/>
Article 2 : La présente décision sera notifiée au département du Gard et à l'association de défense des propriétaires et résidents de Pont-d'Hérault-Le Sigal-Le Rey.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-02 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ACTE DÉCLARATIF D'UTILITÉ PUBLIQUE. - COMPATIBILITÉ DE L'OPÉRATION FAISANT L'OBJET D'UNE DUP AVEC UN POS OU UN PLU - NOTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). - COMPATIBILITÉ DE L'OPÉRATION FAISANT L'OBJET D'UNE DUP AVEC UN POS OU UN PLU - NOTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AMÉNAGEMENT DU TERRITOIRE. - COMPATIBILITÉ DE L'OPÉRATION FAISANT L'OBJET D'UNE DUP AVEC UN POS OU UN PLU - NOTION.
</SCT>
<ANA ID="9A"> 34-02-02 L'opération qui fait l'objet d'une déclaration d'utilité publique (DUP) ne peut être regardée comme compatible avec un POS ou un PLU, pour l'application de l'article L. 123-16 du code de l'urbanisme, qu'à la double condition qu'elle ne soit pas de nature à compromettre le parti d'aménagement retenu par la commune au travers de ce plan et qu'elle ne méconnaisse pas les dispositions du règlement de la zone du plan dans laquelle sa réalisation est prévue.</ANA>
<ANA ID="9B"> 68-01-01 L'opération qui fait l'objet d'une déclaration d'utilité publique (DUP) ne peut être regardée comme compatible avec un POS ou un PLU, pour l'application de l'article L. 123-16 du code de l'urbanisme, qu'à la double condition qu'elle ne soit pas de nature à compromettre le parti d'aménagement retenu par la commune au travers de ce plan et qu'elle ne méconnaisse pas les dispositions du règlement de la zone du plan dans laquelle sa réalisation est prévue.</ANA>
<ANA ID="9C"> 68-05 L'opération qui fait l'objet d'une déclaration d'utilité publique (DUP) ne peut être regardée comme compatible avec un POS ou un PLU, pour l'application de l'article L. 123-16 du code de l'urbanisme, qu'à la double condition qu'elle ne soit pas de nature à compromettre le parti d'aménagement retenu par la commune au travers de ce plan et qu'elle ne méconnaisse pas les dispositions du règlement de la zone du plan dans laquelle sa réalisation est prévue.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
