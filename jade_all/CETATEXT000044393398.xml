<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044393398</ID>
<ANCIEN_ID>JG_L_2021_11_000000447077</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/39/33/CETATEXT000044393398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/11/2021, 447077, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447077</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Rectif. d'erreur matérielle</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447077.20211130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme E... et A... D... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir la décision implicite par laquelle le maire du Lavandou a rejeté leur recours gracieux tendant à l'abrogation de la délibération du 28 mars 2013 par laquelle le conseil municipal de la commune du Lavandou a approuvé le plan local d'urbanisme de la commune, en tant que celle-ci classe les parcelles dont ils sont propriétaires dans le secteur 1Nr et au sein du périmètre " espaces boisés classés ". <br/>
<br/>
              Par un jugement n° 1401988 du 25 juillet 2016, le tribunal administratif de Toulon a annulé cette délibération en tant seulement qu'elle interdit le changement de destination des hôtels existants dans les zones urbaines.<br/>
<br/>
              Par un arrêt n° 16MA03755 du 12 juin 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. et Mme D... contre ce jugement.<br/>
<br/>
              Par une décision n° 423135 du 28 septembre 2020, le Conseil d'Etat statuant au contentieux a rejeté le pourvoi formé par M. et Mme D... contre cet arrêt.<br/>
<br/>
              Recours en rectification d'erreur matérielle<br/>
<br/>
              Par une requête, enregistrée le 30 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme D... demandent au Conseil d'Etat :<br/>
<br/>
               1°) de rectifier pour erreur matérielle la décision n° 423125 du 28 septembre 2020 par laquelle il a rejeté leur pourvoi ; <br/>
<br/>
              2°) statuant à nouveau sur leur pourvoi, de faire droit à leurs conclusions ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Lavandou une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de M. et Mme D..., et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune du Lavandou ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article R. 833-1 du code de justice administrative : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification. " Le recours en rectification d'erreur matérielle n'est ouvert qu'en vue de corriger des erreurs de caractère matériel qui ne sont pas imputables aux parties et qui ont pu avoir une influence sur le sens de la décision. Les appréciations d'ordre juridique auxquelles se livre le Conseil d'Etat pour statuer sur l'argumentation des parties ne sont pas susceptibles d'être remises en cause par la voie du recours en rectification d'erreur matérielle.<br/>
<br/>
              2.	Pour demander la rectification pour erreur matérielle de la décision du 28 septembre 2020 par laquelle le Conseil d'Etat, statuant au contentieux, a rejeté son pourvoi, M. et Mme D... font valoir que le Conseil d'Etat aurait commis une erreur matérielle en ce qui concerne l'identification de leurs parcelles comme espaces remarquables. Toutefois, la décision du Conseil d'Etat se prononce sur les moyens relatifs au classement de ces parcelles par le plan local d'urbanisme. La contestation, présentée par la voie du recours en rectification d'erreur matérielle, ne conduit pas à réparer une omission matérielle, mais revient à mettre en cause les appréciations d'ordre juridique auxquelles s'est livré le Conseil d'Etat en écartant les moyens qui lui étaient soumis.<br/>
<br/>
              3.	Il résulte de ce qui précède que le recours en rectification d'erreur matérielle, qui ne satisfait pas aux conditions posées par l'article R. 833-1 du code de justice administrative, n'est pas recevable et ne peut qu'être rejeté.<br/>
<br/>
              4.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune du Lavandou qui n'est pas, en la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme D... le versement à la commune du Lavandou de la somme de 1 000 euros à ce titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : Le recours en rectification d'erreur matérielle présenté par M. et Mme D... est rejeté.<br/>
Article 2 : M. et Mme D... verseront à la commune du Lavandou la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. et Mme E... et A... D... et à la commune du Lavandou.<br/>
              Délibéré à l'issue de la séance du 4 novembre 2021 où siégeaient : M. Nicolas Boulouis, président de chambre, présidant ; Mme Anne Courrèges, conseillère d'Etat et M. Sébastien Gauthier, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 30 novembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Nicolas Boulouis<br/>
 		Le rapporteur : <br/>
      Signé : M. Sébastien Gauthier<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
