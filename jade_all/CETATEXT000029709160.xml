<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709160</ID>
<ANCIEN_ID>JG_L_2014_11_000000363182</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 05/11/2014, 363182, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363182</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363182.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	M. A...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 11 août 2011 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'admission au bénéfice de l'asile.<br/>
	Par une décision n° 11022088 du 14 mai 2012, la Cour nationale du droit d'asile a  rejeté sa demande.<br/>
Procédure devant le Conseil d'Etat<br/>
	Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 octobre et 26 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
	1°) d'annuler cette décision n° 11022088 de la Cour nationale du droit d'asile du 14 mai 2012;<br/>
<br/>
	2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu :<br/>
              - la décision attaquée ;<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution ;<br/>
              - la convention de Genève et le protocole de New York relatifs au statut des réfugiés ;<br/>
              - la directive 2004/83/CE du Conseil du 29 avril 2004 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort du dossier soumis au juge du fond que M. A...a déposé le 6 mai 2010 une demande d'asile ; que cette demande a été rejetée le 11 août 2011 par une décision du directeur général de l'Office français de protection des réfugiés et apatrides ; que la Cour nationale du droit d'asile, saisie par l'intéressé, a, par décision du 14 mai 2012, estimé que M. A...ne pouvait être regardé comme craignant avec raison d'être exposé à des persécutions au sens des stipulations de l'article 1er A 2 de la convention de Genève, auquel se réfère l'article L. 711-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, ou à des menaces graves énoncées par l'article L. 712-1 du même code, en cas de retour dans la Bande de Gaza et a rejeté le recours de M. A...;<br/>
<br/>
              2. Considérant qu'aux termes du 2° du A de l'article 1er de la convention de Genève, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité ou de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " ; qu'aux termes de l'article L. 712-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié mentionnées à l'article L. 711-1 et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : a) La peine de mort ; b) La torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave, directe ou individuelle contre sa vie ou sa personne en raison d'une violence généralisée résultant d'une situation de conflit armé interne ou international (...) " ; qu'aux termes de l'article L 713-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Les persécutions prises en compte dans l'octroi de la qualité de réfugié et les menaces graves pouvant donner lieu au bénéfice de la protection subsidiaire peuvent être le fait des autorités de l'Etat, de partis ou d'organisations qui contrôlent l'Etat ou une partie substantielle du territoire de l'Etat, ou d'acteurs non étatiques dans les cas où les autorités définies à l'alinéa suivant refusent ou ne sont pas en mesure d'offrir une protection. Les autorités susceptibles d'offrir une protection peuvent être les autorités de l'Etat et des organisations internationales et régionales " ;<br/>
<br/>
              3. Considérant que, s'agissant des risques encourus en cas de retour dans la Bande de Gaza par M. A...du fait qu'il l'aurait fuie en compagnie de M. B...A..., la Cour nationale du droit d'asile a indiqué " qu'à l'issue de l'instruction, les faits de persécution allégués par M.A... ne peuvent être tenus pour établis et ses craintes en cas de retour pour fondées ; que, notamment, ses déclarations se sont révélées impersonnelles et très peu étayées concernant les agissements dont il aurait été l'objet de la part de miliciens du Hamas en vue de son enrôlement " ; que, par ailleurs, la Cour a précisé que M. A...n'avait pas davantage emporté la conviction sur son allégation selon laquelle il serait recherché par des miliciens du Hamas alors que son oncle Mouin serait engagé dans le mouvement depuis 2008 ; qu'ainsi, en relevant que M. A...ne pouvait être regardé comme craignant avec raison d'être exposé à des persécutions, au sens des  stipulations de l'article 1er A 2 de la convention de Genève, auquel se réfère l'article L. 711-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ou à des menaces graves  au sens des dispositions de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, la Cour nationale du droit d'asile, par une décision suffisamment motivée, a fait une exacte application de ces dispositions et n'a pas entaché sa décision d'une erreur de droit ;<br/>
<br/>
              4. Considérant que, s'agissant de la protection subsidiaire, l'existence d'une menace grave, directe et individuelle contre la vie ou la personne d'un demandeur est subordonnée à la condition qu'il rapporte la preuve qu'il est visé spécifiquement en raison d'éléments propres à sa situation personnelle dès lors que le degré de violence aveugle caractérisant le conflit armé n'atteint pas un niveau si élevé qu'il existe des motifs sérieux et avérés de croire qu'un civil renvoyé dans le pays ou la région concernés courrait, du seul fait de sa présence sur le territoire, un risque réel de subir lesdites menaces ; que l'existence d'un climat de violence généralisée résultant d'une situation de conflit interne ou international, découle d'une appréciation souveraine des faits, exempte de toute dénaturation, qui n'est pas susceptible d'être discutée devant le juge de cassation ; qu'en jugeant, compte tenu des déclarations de M. A..., que celui-ci ne pouvait être regardé comme craignant d'être exposé aux menaces graves énoncées par l'article L. 712-1 du code l'entrée et du séjour des étrangers et du droit d'asile, la Cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              6. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la Yves et Blaise Capron, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Monsieur A...A.... <br/>
Copie en sera adressée, pour information, au directeur général de l'Office français de protection des réfugiés et des apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
