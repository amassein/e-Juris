<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555871</ID>
<ANCIEN_ID>JG_L_2012_10_000000355648</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 29/10/2012, 355648</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355648</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355648.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 janvier et 8 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Hassan A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA06042 du 7 novembre 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement n° 1006018/5 du 16 novembre 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de l'arrêté du 25 juin 2010 par lequel le préfet du <br/>
Val-de-Marne a refusé le renouvellement de son titre de séjour, l'a obligé à quitter le territoire français et a fixé le pays de renvoi, en deuxième lieu, à l'annulation de cet arrêté et, en troisième lieu, à ce qu'il soit enjoint au préfet du Val-de-Marne de lui délivrer un titre de séjour portant la mention " vie privée et familiale " ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ainsi que le remboursement de la contribution pour l'aide juridique qu'il a acquittée, en application de l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de M. A ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors applicable : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : / (...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse effectivement bénéficier d'un traitement approprié dans le pays dont il est originaire, sans que la condition prévue à l'article L. 311-7 soit exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin inspecteur de santé publique compétent au regard du lieu de résidence de l'intéressé ou, à Paris, du médecin, chef du service médical de la préfecture de police. (...) " ; qu'aux termes de l'article R. 313-22 du même code : " Pour l'application du 11° de l'article L. 313-11, le préfet délivre la carte de séjour temporaire au vu d'un avis émis par le médecin inspecteur départemental de santé publique compétent au regard du lieu de résidence de l'intéressé et, à Paris, par le médecin, chef du service médical de la préfecture de police. / L'avis est émis dans les conditions fixées par arrêté du ministre chargé de l'immigration et du ministre chargé de la santé au vu, d'une part, d'un rapport médical établi par un médecin agréé ou un médecin praticien hospitalier et, d'autre part, des informations disponibles sur les possibilités de traitement dans le pays d'origine de l'intéressé " ; que l'arrêté interministériel du 8 juillet 1999, pris pour l'application de ces dispositions, impose au médecin inspecteur de santé publique de la direction départementale des affaires sanitaires et sociales d'émettre un avis précisant si l'état de santé de l'étranger nécessite ou non une prise en charge médicale, si le défaut de cette prise en charge peut ou non entraîner des conséquences d'une exceptionnelle gravité, si l'intéressé peut ou non bénéficier effectivement d'un traitement médical approprié dans son pays, quelle est la durée prévisible du traitement, et indiquant si l'état de santé de l'étranger lui permet de voyager sans risque vers le pays de renvoi ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, ressortissant marocain né le 9 février 1964, est entré en France en novembre 2002 ; qu'il a obtenu en septembre 2007 une carte de séjour temporaire portant la mention " vie privée et familiale " d'une durée d'un an, sur avis favorable du médecin inspecteur de santé publique ; qu'il a sollicité, sur le fondement du 11° de l'article L. 313-11 cité ci-dessus, le renouvellement de ce titre de séjour ; que, par un arrêté du 25 juin 2010, le préfet du Val-de-Marne a rejeté cette demande, a obligé M. A à quitter le territoire français dans un délai d'un mois et a fixé le pays de renvoi ; que, par un jugement du 16 novembre 2010, le tribunal administratif de Melun a rejeté la demande de M. A tendant à l'annulation de cet arrêté ; que, par un arrêt du 7 novembre 2011 contre lequel M. A se pourvoit en cassation, la cour administrative d'appel de Paris a confirmé ce jugement ; <br/>
<br/>
              3. Considérant que la cour n'a pas commis d'erreur de droit en relevant que l'absence dans l'avis médical du 30 décembre 2009 du médecin inspecteur de santé publique de la mention requise par l'arrêté interministériel du 8 juillet 1999, indiquant si l'état de santé de l'étranger lui permettait de voyager sans risque vers le pays de renvoi, n'affectait pas la légalité de la décision contestée dès lors qu'elle a également retenu, par une appréciation souveraine exempte de dénaturation, que le certificat du médecin inspecteur ne faisait pas ressortir que l'état de santé de M. A soulevait des interrogations sur sa capacité à supporter ce voyage ;<br/>
<br/>
<br/>
              4. Considérant que la cour a retenu, sans entacher son arrêt de dénaturation ou d'insuffisance de motivation, que l'unique certificat médical produit par le requérant n'était pas de nature à remettre en cause l'avis du médecin inspecteur de santé publique et, qu'en particulier, ce certificat médical, ni aucun autre élément produit par l'intéressé, ne fournissait de précision sur les traitements requis, ni, en tout état de cause, sur leur indisponibilité au Maroc ; qu'elle a, ce faisant, statué au vu des pièces du dossier sans commettre d'erreur de droit au regard de la charge de la preuve ; <br/>
<br/>
              5. Considérant, enfin, que la cour n'a pas commis d'erreur de qualification juridique des faits en estimant que l'arrêté du 25 juin 2010 n'a pas porté une atteinte disproportionnée au respect de la vie privée et familiale de M. A, en violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que s'il réside en France depuis 2002 et si son père et son frère y résident régulièrement, il ne justifie pas d'autres attaches qu'il aurait pu constituer en France, pas plus qu'il n'établit, ni même n'allègue, qu'il serait isolé en cas de retour vers le Maroc, où il a vécu jusqu'à l'âge de trente-huit ans ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de M. A doit être rejeté, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. Hassan A et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-01-01 ÉTRANGERS. RECONDUITE À LA FRONTIÈRE. LÉGALITÉ EXTERNE. PROCÉDURE. - ABSENCE, DANS L'AVIS MÉDICAL DU MÉDECIN INSPECTEUR, DE MENTION INDIQUANT SI L'ÉTAT DE SANTÉ DE L'ÉTRANGER LUI PERMET DE VOYAGER SANS RISQUE VERS LE PAYS DE RENVOI - CONSÉQUENCE - ABSENCE, LORSQUE CE MÊME AVIS NE FAIT PAS RESSORTIR D'INTERROGATION SUR LA CAPACITÉ DE L'INTÉRESSÉ À SUPPORTER LE VOYAGE [RJ1].
</SCT>
<ANA ID="9A"> 335-03-01-01 L'absence, dans l'avis médical du médecin inspecteur de santé publique, de la mention requise par l'arrêté interministériel du 8 juillet 1999, indiquant si l'état de santé de l'étranger lui permet de voyager sans risque vers le pays de renvoi, n'affecte pas la légalité de la décision d'OQTF fixant le pays de destination, lorsque le certificat du médecin inspecteur ne fait pas ressortir que l'état de santé de l'intéressé soulèverait des interrogations sur sa capacité à supporter ce voyage.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 mai 2004, Cheroud, n° 253013, T. p. 723.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
