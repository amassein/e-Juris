<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065784</ID>
<ANCIEN_ID>JG_L_2020_06_000000429393</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/06/2020, 429393, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429393</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429393.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... C... ont demandé au tribunal administratif de Cergy-Pontoise de prononcer, à titre principal, la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2008 et des pénalités correspondantes, et, à titre subsidiaire, la restitution de la somme de 138 131,12 euros prélevée au titre des contributions sociales à la suite d'un retrait opéré par M. C... sur son plan d'épargne en actions le 31 décembre 2008. Par un jugement n° 1304471 du 8 décembre 2015, le tribunal administratif de Cergy-Pontoise a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 16VE00384 du 29 novembre 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. et Mme C... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 avril et 2 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de M. et Mme A... C... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... a créé, le 28 décembre 2004, avec M. B... et la société Cavendish Square Holding BV, filiale du groupe WPP, la société Financière RKW Holding, qui avait pour objet l'acquisition et la gestion de titres sociaux. Le même jour, il a cédé à cette société des titres de la société par actions simplifiée KR Media France (KRM), qu'il avait constituée le 7 janvier 2004 avec M. B..., et qui exerce l'activité de centrale d'achats d'espaces publicitaires. Il a alors inscrit, pour leur valeur nominale, les titres qu'il détenait de la société Financière RKW Holding sur son plan d'épargne en actions. Le 20 juin 2008, M. C... a cédé l'intégralité de sa participation dans la société Financière RKW Holding à la société Cavendish Square Holding BV. A raison de l'inscription des titres de la société Financière RKW Holding sur le plan d'épargne en actions de M. C..., M. et Mme C... ont regardé la plus-value réalisée à l'occasion de la cession de ces titres comme exonérée d'impôt en application du 5 bis de l'article 157 du code général des impôts. A la suite d'un contrôle sur pièces du dossier fiscal de M. et Mme C..., l'administration fiscale a remis en cause, en recourant à la procédure de répression des abus de droit prévue par l'article L. 64 du livre des procédures fiscales, l'exonération dont avaient ainsi entendu bénéficier les contribuables au double motif, d'une part, que la société Financière RKW Holding avait été interposée afin de permettre à M. C... de respecter en apparence la condition, mentionnée au I de l'article 163 quinquies D du code général des impôts, relative à la détention directe ou indirecte des droits dans les bénéfices sociaux inférieure ou égale à 25 %, et, d'autre part, que la valeur des titres de la société Financière RKW Holding avait été volontairement minorée lors de leur inscription sur le plan d'épargne en actions de M. C..., pour assurer le respect formel du plafond de 132 000 euros applicable afin de bénéficier de l'exonération d'imposition des produits et plus-values procurés par des placements effectués dans le cadre d'un plan d'épargne en actions. M. et Mme C... ont, en conséquence, été assujettis à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre de l'année 2008, majorées des intérêts de retard et de pénalités de 80 % pour abus de droit sur le fondement de l'article 1729 du code général des impôts. Ils se pourvoient en cassation contre l'arrêt du 29 novembre 2018 de la cour administrative d'appel de Versailles rejetant leur appel contre le jugement du tribunal administratif de Cergy-Pontoise du 8 décembre 2015 rejetant leur demande de décharge de ces impositions. <br/>
<br/>
              2. L'article 157 du code général des impôts dispose que : " N'entrent pas en compte pour la détermination du revenu net global : / (...) 5° bis Les produits et plus-values que procurent les placements effectués dans le cadre du plan d'épargne en actions défini à l'article 163 quinquies D (...) ". Aux termes de l'article 163 quinquies D du même code, dans sa rédaction applicable au présent litige : " I. Les contribuables dont le domicile fiscal est situé en France peuvent ouvrir un plan d'épargne en actions dans les conditions définies par la loi n° 92-666 du 16 juillet 1992 modifiée. / Chaque contribuable ou chacun des époux soumis à imposition commune ne peut être titulaire que d'un plan. Un plan ne peut avoir qu'un titulaire. / Le titulaire d'un plan effectue des versements en numéraire dans une limite de 132 000 euros. II (...) 3. Le titulaire du plan, son conjoint et leurs ascendants et descendants ne doivent pas, pendant la durée du plan, détenir ensemble, directement ou indirectement, plus de 25 p. 100 des droits dans les bénéfices de sociétés dont les titres figurent au plan ou avoir détenu cette participation à un moment quelconque au cours des cinq années précédant l'acquisition de ces titres dans le cadre du plan ". <br/>
<br/>
              3. L'article L. 64 du livre des procédures fiscales, dans sa rédaction alors applicable, dispose que : " Afin d'en restituer le véritable caractère, l'administration est en droit d'écarter, comme ne lui étant pas opposables, les actes constitutifs d'un abus de droit, soit que ces actes ont un caractère fictif, soit que, recherchant le bénéfice d'une application littérale des textes ou de décisions à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, si ces actes n'avaient pas été passés ou réalisés, aurait normalement supportées eu égard à sa situation ou à ses activités réelles. / En cas de désaccord sur les rectifications notifiées sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité de l'abus de droit fiscal. L'administration peut également soumettre le litige à l'avis du comité. / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé de la rectification ". Il résulte de ces dispositions que, lorsque l'administration use des pouvoirs que lui confère ce texte dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors que ces actes ont un caractère fictif ou que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées, eu égard à sa situation ou à ses activités réelles. L'administration fiscale apporte cette preuve par la production de tous éléments suffisamment précis attestant du caractère fictif des actes en cause ou de l'intention du contribuable d'éluder ou d'atténuer ses charges fiscales normales. Dans l'hypothèse où l'administration s'acquitte de cette obligation, il incombe ensuite au contribuable, s'il s'y croit fondé, d'apporter la preuve de la réalité des actes contestés ou de ce que l'opération litigieuse est justifiée par un motif autre que celui d'éluder ou d'atténuer ses charges fiscales normales.<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que pour remettre en cause le bénéfice de l'exonération de l'imposition de la plus-value réalisée par M. C... lors de la cession des titres de la société Financière RKW Holding inscrits sur son plan d'épargne en actions, l'administration fiscale a retenu un premier motif d'abus de droit tiré de l'absence de substance économique de l'interposition de la société Financière RKW Holding qui n'aurait été créée que pour permettre au contribuable de respecter en apparence la condition, mentionnée au II de l'article 163 quinquies D du code général des impôts, cité au point 2, relative à la détention directe ou indirecte des droits dans les bénéfices sociaux inférieure ou égale à 25 %. Il ressort des énonciations de l'arrêt attaqué que pour écarter l'argumentation des requérants tirée de ce que la société Financière RKW Holding avait une réalité économique et que sa constitution avait pour objectif d'associer le groupe WPP au développement de la société KRM tout en préservant l'indépendance de cette dernière, la cour s'est fondée sur la circonstance que les éléments apportés par les contribuables ne démontraient pas la nécessité de l'interposition de la société Financière RKW Holding. En exigeant ainsi que les requérants justifient de ce que l'architecture d'ensemble mise en place était la seule possible pour atteindre l'objectif économique poursuivi, la cour a commis une erreur de droit. <br/>
<br/>
              5. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que pour remettre en cause le bénéfice de l'exonération de l'imposition de la plus-value réalisée par M. C... lors de la cession des titres de la société Financière RKW Holding inscrits sur son plan d'épargne en actions, l'administration fiscale a retenu un second motif d'abus de droit tiré de la minoration volontaire de la valeur des titres de la société Financière RKW Holding afin d'assurer le respect formel du plafond de 132 000 euros applicable afin de bénéficier de l'exonération d'imposition des produits et plus-values procurés par des placements effectués dans le cadre d'un plan d'épargne en actions. En se bornant à écarter le moyen des requérants tiré de ce que la valorisation des titres retenue par l'administration était excessive faute d'avoir pris en compte le risque résultant du contentieux judiciaire opposant l'ancien employeur de M. C..., la société Aegis, à la société KRM au seul motif que les contrats d'achats d'espaces publicitaires conclus par la société KRM ne comportaient aucune clause unilatérale de résiliation tenant au litige entre la société Aegis et la société KRM, sans évaluer le risque d'une condamnation de la société KRM et les conséquences s'y attachant, la cour a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. et Mme C... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. et Mme C... au titre de l'article L. 761-1 du code de justice adminstrative.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 29 novembre 2017 est annulé. <br/>
<br/>
             Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
<br/>
 Article 3 : L'Etat versera une somme de 3 000 euros à M. et Mme C... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
