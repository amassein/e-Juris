<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198444</ID>
<ANCIEN_ID>JG_L_2013_03_000000351365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/84/CETATEXT000027198444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 20/03/2013, 351365, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351365.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 28 juillet 2011, au secrétariat du contentieux du Conseil d'Etat, présenté pour la société 2H Energy, dont le siège est ZI du Babeuf à Saint-Léonard (76400), représentée par son président-directeur général en exercice ; la société 2H Energy demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10DA00076 du 26 mai 2011 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement n° 0500868 du 5 novembre 2009 du tribunal administratif de Rouen par lequel le tribunal, après avoir prononcé un non-lieu à hauteur de la somme de 2 639,31 euros, a rejeté le surplus de sa demande tendant à la décharge des rappels de taxe sur la valeur ajoutée et des pénalités correspondantes auxquelles elle a été assujettie au titre de la période du 1er janvier 1998 au 31 décembre 1999 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive n° 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la société 2h Energy,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la société 2h Energy ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS 2H Energy, qui exerce une activité de conception et de fabrication de groupes générateurs d'électricité, a fait l'objet d'une vérification de comptabilité pour la période du 1er janvier 1998 au 31 décembre 1999 ; que des redressements lui ont été notifiés à l'issue de ce contrôle, procédant de la remise en cause, d'une part, du bénéfice du régime de franchise de taxe sur la valeur ajoutée de l'article 275 du code général des impôts sur une vente de biens destinés à être exportés par un de ses clients au motif que la société requérante n'avait pas produit l'attestation, exigée par cet article, certifiant la destination de ces biens, et, d'autre part, de l'exonération de taxe, prévue par l'article 262 du code général des impôts, des livraisons de biens exportés hors de l'Union européenne au motif que la société n'avait pas davantage produit la déclaration d'exportation imposée par l'article 74 de l'annexe III à ce code ; que la société requérante se pourvoit en cassation contre un arrêt du 28 juillet 2011 par lequel la cour administrative d'appel de Douai a confirmé le jugement du 5 novembre 2009 du tribunal administratif de Rouen qui, après avoir prononcé un non-lieu à hauteur d'un dégrèvement intervenu en cours d'instance, a rejeté le surplus de la demande de la société tendant à la décharge des impositions supplémentaires procédant de ce redressement ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du 2 de l'article 16 de la directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaire : " Sous réserve de la consultation prévue à l'article 29, les Etats membres ont la faculté d'exonérer les acquisitions intracommunautaires de biens effectuées par un assujetti, les importations et les livraisons de biens destinés à un assujetti en vue d'être exportés en dehors de la Communauté, en l'état ou après transformation, ainsi que les prestations de services afférentes à son activité d'exportation, dans la limite du montant de ses exportations au cours des douze mois précédents. / Lorsqu'ils font usage de cette faculté, et sous réserve de la consultation prévue à l'article 29, les Etats membres accordent également le bénéfice de cette exonération aux acquisitions intra-communautaires de biens effectuées par un assujetti, aux importations et aux livraisons de biens destinés à un assujetti en vue de livraisons, en l'état ou après transformation, effectuées dans les conditions prévues à l'article 28 quater titre A, ainsi qu'aux prestations de services afférentes à ces livraisons, dans la limite du montant des livraisons de biens effectuées par l'assujetti, dans les conditions prévues à l'article 28 quater titre A, au cours des douze mois précédents  (...) " ; qu'aux termes du A de l'article 28 quater de la même directive : " Sans préjudice d'autres dispositions communautaires et dans les conditions qu'ils fixent en vue d'assurer l'application correcte et simple des exonérations prévues ci-dessous et de prévenir toute fraude, évasion ou abus éventuels, les Etats membres exonèrent :/ a) les livraisons de biens, au sens de l'article 5, expédiés ou transportés, par le vendeur ou par l'acquéreur ou pour leur compte en dehors du territoire visé à l'article 3 mais à l'intérieur de la Communauté, effectuées pour un autre assujetti, ou pour une personne morale non assujettie, agissant en tant que tel dans un Etat membre autre que celui du départ de l'expédition ou du transport des biens (...) " ; qu'aux termes du I de l'article 275 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " Les assujettis sont autorisés à recevoir ou à importer en franchise de la taxe sur la valeur ajoutée les biens qu'ils destinent à une livraison à l'exportation (...) ainsi que les services portant sur ces biens, dans la limite du montant des livraisons de cette nature qui ont été réalisées au cours de l'année précédente et qui portent sur des biens passibles de cette taxe. / Pour bénéficier des dispositions qui précèdent, les intéressés doivent, selon le cas, adresser à leurs fournisseurs, remettre au service des douanes ou conserver une attestation, visée par le service des impôts dont ils relèvent, certifiant que les biens sont destinés à faire l'objet, en l'état ou après transformation, d'une livraison mentionnée au premier alinéa ou que les prestations de services sont afférentes à ces biens. Cette attestation doit comporter l'engagement d'acquitter la taxe sur la valeur ajoutée au cas où les biens et les services ne recevraient pas la destination qui a motivé la franchise, sans préjudice des pénalités prévues aux articles 1725 à 1740 " ; qu'il résulte de ces dernières dispositions, prises pour la mise en oeuvre de la faculté ouverte par l'article 16 de la directive du 17 mai 1977, que le bénéfice de la franchise de taxe qu'elles instituent au profit des livraisons de biens destinés à l'exportation est subordonné à la détention par les assujettis concernés, préalablement à la livraison, d'une attestation de l'exportateur, visée par l'administration fiscale, établissant la vocation des biens à être exportés ;<br/>
<br/>
              3. Considérant que, pour écarter le moyen tiré de ce que le principe de proportionnalité issu du droit de l'Union européenne s'opposait à ce qu'un fournisseur puisse vendre en franchise de taxe sur la valeur ajoutée un bien destiné à être exporté lorsqu'il ne produit pas l'attestation prévue par le I de l'article 275 du code général des impôts, la cour a relevé que l'attestation visée par le service des impôts, certifiant que les biens sont destinés à faire l'objet d'une exportation, était propre à assurer une application correcte et simple de ce régime de franchise comme à prévenir toute fraude, évasion ou abus éventuels, et a estimé que cette exigence n'excédait pas ce qui était nécessaire à la mise en oeuvre de ces objectifs ; qu'ainsi, la cour, alors, d'une part, qu'il n'était pas soutenu devant elle que la production d'une telle attestation aurait été, en l'espèce, pratiquement impossible ou excessivement difficile, et, d'autre part, que les dispositions de l'article 275 du code général des impôts ne mettent en oeuvre qu'une simple faculté ouverte aux Etats membres, et non pas un droit, sans excéder ce qui est nécessaire pour assurer une application correcte et simple de ce régime et prévenir toute fraude, évasion ou abus éventuels, n'a pas commis d'erreur de droit ni entaché son arrêt d'insuffisance de motivation en jugeant que cette exigence n'était pas incompatible avec les objectifs de la directive du 16 mai 1977 et ne méconnaissait pas le principe de proportionnalité issu du droit de l'Union européenne ;<br/>
<br/>
              4. Considérant, en second lieu, que le moyen tiré de ce que les dispositions de l'article 74 de l'annexe III au code général des impôts instaurant une présomption irréfragable de non-exportation méconnaissent le principe de proportionnalité issu du droit de l'Union européenne, n'a pas été invoqué devant la cour administrative d'appel de Douai ; que ce moyen n'est pas né de l'arrêt attaqué et n'est pas d'ordre public ; que, par suite, la société requérante ne peut utilement soulever ce moyen pour contester le bien-fondé de l'arrêt qu'elle attaque, ni soutenir que la cour a entaché celui-ci d'insuffisance de motivation en n'y répondant pas ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le pourvoi doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société 2H Energy est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société 2H Energy et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
