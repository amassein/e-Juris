<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938210</ID>
<ANCIEN_ID>JG_L_2016_01_000000373559</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/82/CETATEXT000031938210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 21/01/2016, 373559, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373559</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:373559.20160121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Axa, agissant en qualité de mandataire des sociétés Axa Investment Managers, Axa Konzern AG et Axa UK PLC, a demandé au tribunal administratif de Paris de prononcer la décharge des retenues à la source acquittées au titre des années 2003 et 2004 sur les dividendes versés aux sociétés Axa Konzern AG et Axa UK PLC. Par l'article 2 du jugement n° 0718202 du 3 juin 2010, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 10PA03928 du 26 septembre 2013, la cour administrative d'appel de Paris a, sur le recours du ministre du budget, des comptes publics et de la réforme de l'Etat, annulé l'article 2 de ce jugement et rétabli les impositions correspondantes. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 novembre 2013, 26 février 2014 et 17 août 2015 au secrétariat du contentieux du Conseil d'Etat, la société Axa demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le traité de Rome instituant la Communauté économique européenne, devenue la Communauté européenne, et notamment ses articles 43, 48, 56 et 58 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - les arrêts Baars (C-251/98) du 13 avril 2000, Sté Denkavit Internationaal BV (C-170/05) du 14 décembre 2006 et Amurta SGPS (C-379/05) du 8 novembre 2007 de la Cour de justice des Communautés européennes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Axa ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Axa Investment Managers a versé des dividendes à la société allemande Axa Konzern AG et à la société britannique Axa UK PLC, qui détenaient des parts de son capital social aux côtés de la société de droit français Axa ; que ces distributions ont été assujetties à la retenue à la source prévue au 2 de l'article 119 bis du code général des impôts au titre des années 2003 et 2004 aux taux respectifs de 15 % et 5 % en application de cet article et des stipulations des conventions fiscales bilatérales ; que la société Axa, agissant en qualité de mandataire de l'établissement payeur et des deux sociétés étrangères bénéficiaires des dividendes, se pourvoit en cassation contre l'arrêt du 26 septembre 2013 par lequel la cour administrative d'appel de Paris a annulé l'article 2 du jugement du 3 juin 2010 du tribunal administratif de Paris, qui l'avait déchargée de ces impositions, et a remis celles-ci à sa charge au motif que sa réclamation présentée le 6 août 2007 était tardive ;  <br/>
<br/>
              2. Considérant qu'aux termes du 2 de l'article 119 bis du code général des impôts, dans sa rédaction applicable aux impositions en litige : "  Sous réserve des dispositions de l'article 239 bis B, les produits visés aux articles 108 à 117 bis donnent lieu à l'application d'une retenue à la source dont le taux est fixé par l'article 187-1 lorsqu'ils bénéficient à des personnes qui n'ont pas leur domicile fiscal ou leur siège en France (...) " ; que l'article 119 ter du même code, dans sa rédaction alors applicable, prévoyait que la retenue à la source n'était pas applicable aux dividendes distribués à une personne morale qui détenait de façon ininterrompue depuis deux ans ou plus 25% au moins du capital de la personne morale qui distribue les dividendes ; qu'aux termes  de l'article L. 190 du livre des procédures fiscales, dans sa rédaction applicable à la date de la réclamation présentée en 2007 : " Les réclamations relatives aux impôts (...) de toute nature (...) relèvent de la juridiction contentieuse lorsqu'elles tendent à obtenir soit la réparation d'erreurs commises dans l'assiette ou le calcul des impositions, soit le bénéfice d'un droit résultant d'une disposition législative ou réglementaire. / Sont instruites et jugées selon les règles du présent chapitre toutes actions tendant à (...) à l'exercice de droits à déduction, fondées sur la non-conformité de la règle de droit dont il a été fait application à une règle de droit supérieure (...) Lorsque cette non-conformité a été révélée par une décision juridictionnelle (...), l'action en restitution (...) ne peut porter que sur la période postérieure au 1er janvier de la période précédant celle où la décision ou l'avis révélant la non-conformité est intervenu (...) sont considérés comme des décisions juridictionnelles (...) les arrêts de la Cour de justice des Communautés européennes se prononçant sur (...) une question préjudicielle  "  ; qu'aux termes de l'article R. 196-1 du même livre : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas :/(...) c. de la réalisation de l'événement qui motive la réclamation (...) " ; que seules les décisions de la Cour de justice des Communautés européennes, devenue Cour de justice de l'Union européenne, retenant une interprétation du droit de l'Union qui révèle directement une incompatibilité avec ce droit d'une règle applicable en France sont de nature à constituer le point de départ du délai dans lequel sont recevables les réclamations motivées par la réalisation d'un tel événement, au sens et pour l'application de l'article R. 196-1 du livre des procédures fiscales, et de la période sur laquelle l'action en restitution peut s'exercer en application de l'article L. 190 du même livre ; que par l'arrêt rendu le 13 avril 2000 dans l'affaire Baars (C-251/98), la Cour de justice des Communautés européennes a jugé qu'exerce son droit d'établissement le ressortissant d'un Etat membre qui détient dans le capital d'une société établie dans un autre Etat membre une participation lui conférant une influence certaine sur les décisions de la société et lui permettant d'en déterminer les activités ; que, pour l'application de ces dispositions, une telle influence ne se présume pas ; qu'il appartient, par suite, aux parties qui s'en prévalent d'en rapporter la preuve ;<br/>
<br/>
              3. Considérant que la cour, après avoir relevé que la société Axa détenait près de 78 % du capital social de la société Axa Investment Managers, a estimé, par une appréciation souveraine non arguée de dénaturation, qu'il ne résultait pas de l'instruction que les participations détenues par les sociétés Axa Konzern AG et Axa UK PLC au capital de cette société Axa Investment Managers de respectivement 16% et 6,5% leur avaient permis d'exercer une influence certaine sur les décisions de cette dernière et de déterminer son activité au point de leur assurer un contrôle sur celle-ci ; qu'elle en a déduit que les participations des sociétés Axa Konzern AG et Axa UK PLC ne relevaient pas de la liberté d'établissement et que, dès lors, l'arrêt de la Cour de justice des Communautés européennes du 14 décembre 2006, Denkavit Internationaal BV (C-170/05) ne constituait pas un événement au sens du c de l'article R. 196-1 du livre des procédures fiscales ; qu'en ne présumant pas que les filiales étrangères agissaient de concert avec la société Axa au sein d'un groupe détenant l'intégralité du capital de cette société distributrice, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant que, par son arrêt du 14 décembre 2006 Denkavit Internationaal BV, la Cour de justice des Communautés européennes a jugé que la liberté d'établissement s'opposait à une législation nationale prévoyant, pour les seules sociétés non résidentes, une imposition par voie de retenue à la source des dividendes distribués par des filiales résidentes, dès lors que la société mère ne pouvait procéder à l'imputation de cet impôt sur celui qu'elle doit à son Etat de résidence ; que, par suite, en jugeant que cet arrêt ne portait pas sur la liberté de circulation des capitaux, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant que, s'agissant des décisions et avis rendus au contentieux par le Conseil d'Etat, la Cour de cassation, le Tribunal des conflits et la Cour de justice de l'Union européenne, seuls ceux qui révèlent directement l'incompatibilité avec une règle de droit supérieure de la règle de droit dont il a été fait application pour fonder l'imposition en litige sont de nature à constituer le point de départ du délai dans lequel sont recevables les réclamations motivées par la réalisation d'un événement au sens du c de l'article R. 196-1 du livre des procédures fiscales ainsi que de la période sur laquelle l'action en restitution peut s'exercer en application de l'article L. 190 du même livre ; que si, en principe, tel n'est pas le cas d'un arrêt de la Cour de justice concernant la législation d'un autre Etat membre, une telle décision constitue également un événement de nature à motiver une réclamation portant sur cette période dans l'hypothèse où elle révèle, par l'interprétation qu'elle donne d'une directive, la transposition incorrecte de cette dernière en droit français ; <br/>
<br/>
              6. Considérant que l'arrêt du 8 novembre 2007 de la Cour de justice des Communautés européennes Amurta SGPS (C-379/05), dont se prévalait la société requérante devant les juges du fond, ne porte ni sur la législation française relative à la retenue à la source ni, en tout état de cause, sur l'interprétation d'une directive ; que, par suite, en jugeant que cet arrêt ne pouvait être regardé comme ayant révélé la non-conformité à une règle supérieure du 2 de l'article 119 bis du code général des impôts, au sens de l'article L. 190 du livre des procédures fiscales, ni, par conséquent, comme un événement ayant motivé la réclamation de la société Axa au sens du c de l'article R. 196-1 du même livre, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              7. Considérant que les dispositions des 4ème et 5ème alinéas de l'article L. 190 du livre des procédures fiscales ont pour objet d'ouvrir pour des contribuables qui ont déjà bénéficié d'un premier délai de réclamation un nouveau délai de réclamation à compter de l'intervention d'une décision juridictionnelle ou d'un avis contentieux révélant la non-conformité d'une règle de droit à une règle de droit supérieure ; qu'en restreignant dans le temps la période de répétition sur laquelle peut porter cette nouvelle réclamation, relative à des impositions ayant déjà ouvert droit à un premier délai de réclamation et en limitant les conditions pour qu'un arrêt de la Cour de justice soit reconnu comme un événement révélant directement une incompatibilité avec ce droit d'une règle applicable en France, elles ne peuvent être regardées comme portant atteinte à l'effectivité du droit communautaire ou à une espérance légitime ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que ces dispositions ne méconnaissaient ni le principe d'effectivité du droit communautaire, ni les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Axa doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Axa est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Axa et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
