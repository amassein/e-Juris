<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861125</ID>
<ANCIEN_ID>JG_L_2015_12_000000372230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861125.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 30/12/2015, 372230, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:372230.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Les Laboratoires Servier a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir la décision du 2 février 2012 par laquelle le directeur général de la Caisse nationale de l'assurance maladie des travailleurs salariés (CNAMTS) a refusé de lui communiquer les données sources de deux études de la CNAMTS relatives à la spécialité Mediator et la décision implicite née du silence gardé pendant plus de deux mois à compter de la saisine de la Commission d'accès aux documents administratifs et, d'autre part, d'enjoindre au directeur général de la CNAMTS de lui communiquer ces documents. Par un jugement n° 1211832 du 15 juillet 2013, le tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              Par un pourvoi, un nouveau mémoire et trois mémoires en réplique, enregistrés les 16 septembre 2013, 4 février 2014, 20 juin 2014, 24 février 2015 et 14 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Les Laboratoires Servier demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Paris du 15 juillet 2013 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande et d'enjoindre au directeur général de la CNAMTS de lui communiquer les données sources sollicitées, s'il y a lieu après occultation des mentions ou données protégées, dans un délai de quinze jours à compter de la date de l'injonction, sous astreinte de 1 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de CNAMTS la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ; <br/>
              - la décision du 26 décembre 2013 par laquelle le Conseil d'Etat, statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Les Laboratoires Servier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Les Laboratoires Servier, et à la SCP Foussard, Froger, avocat de la caisse nationale de l'assurance maladie des travailleurs salariés ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Les Laboratoires Servier a demandé à la Caisse nationale de l'assurance maladie des travailleurs salariés (CNAMTS) la communication des données sources, issues du " système national d'information interrégimes de l'assurance maladie ", de deux études réalisées sous son égide, l'une intitulée " Benfluorex et valvulopathies cardiaques : une étude de cohorte sur un million de personnes traitées pour diabète " et publiée le 13 octobre 2010 sur " Wiley online library ", l'autre intitulée  " Benfluorex, valvulopathies cardiaques et décès ", et adressée le 28 septembre 2010 à l'Agence française de sécurité sanitaire des produits de santé ; que, par une décision du 2 février 2012, la CNAMTS a refusé la communication de ces données ; qu'à la suite de la saisine de la Commission d'accès aux documents administratifs et de l'avis défavorable émis par celle-ci le 19 avril 2012, la CNAMTS a confirmé son refus de communication par une décision implicite de rejet ; que, par le jugement attaqué du 15 juillet 2013, le tribunal administratif de Paris a rejeté la demande de la société Les Laboratoires Servier tendant à l'annulation de cette décision et à ce qu'il soit enjoint à la CNAMTS de lui communiquer ces données ;<br/>
<br/>
<br/>
              2. Considérant qu'il résulte de l'article 37 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés que les dispositions de cette loi ne font, en principe, pas obstacle à l'application, au bénéfice de tiers, des dispositions du titre Ier de la loi du 17 juillet 1978, relatif à la liberté d'accès aux documents administratifs et à la réutilisation des informations publiques ; que lorsque des données à caractère personnel ont également le caractère de documents administratifs, elles ne sont communicables aux tiers, en vertu du III de l'article 6 de la loi du 17 juillet 1978, que s'il est possible d'occulter ou de disjoindre les mentions portant atteinte, notamment, à la protection de la vie privée ou au secret médical ; qu'il ne peut être accédé à une demande de communication sur le fondement de la loi du 17 juillet 1978 que si le traitement nécessaire pour rendre impossible, s'agissant de données de santé, toute identification, directe ou indirecte, de l'une quelconque des personnes concernées, y compris par recoupement avec d'autres données, n'excède pas l'occultation ou la disjonction des mentions non communicables, seule envisagée par cette loi ; que, dans le cas contraire, sont seules applicables les dispositions de la loi du 6 janvier 1978 et des lois spéciales applicables au traitement de certaines catégories de données, notamment, en ce qui concerne les données de santé à caractère personnel, les chapitres IX et X de la loi du 6 janvier 1978 ; qu'il suit de là que, contrairement à ce que soutient la CNAMTS, la seule circonstance que les données en cause soient issues du système national d'information interrégimes de l'assurance maladie ne faisait pas, par elle-même, obstacle à ce qu'elles soient communiquées sur le fondement de la loi du 17 juillet 1978, si les conditions posées  par cette loi étaient réunies ;<br/>
<br/>
              3. Considérant qu'il résulte des articles 1er et 2 de la loi du 17 juillet 1978 que l'Etat, les collectivités territoriales ainsi que les autres personnes de droit public et les personnes de droit privé chargées d'une mission de service public sont tenues de communiquer aux personnes qui en font la demande les documents administratifs qu'elles détiennent, définis comme les documents produits ou reçus dans le cadre de leur mission de service public, quels qu'en soient la forme et le support, sous réserve des dispositions de l'article 6 de cette loi ; qu'aux termes du I de l'article 6 de la même loi : " Ne sont pas communicables : (...) 2° Les autres documents administratifs dont la consultation ou la communication porterait atteinte : (...) f) Au déroulement des procédures engagées devant les juridictions ou d'opérations préliminaires à de telles procédures, sauf autorisation donnée par l'autorité compétente " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dernières dispositions que la seule circonstance que la communication d'un document administratif soit de nature à affecter les intérêts d'une partie à une procédure juridictionnelle ou qu'un document ait été transmis à une juridiction dans le cadre d'une instance engagée devant elle ne fait pas obstacle à sa communication ; que, toutefois, il revient à la personne chargée d'une mission de service public qui est sollicitée pour communiquer des documents qu'elle détient de vérifier notamment, au cas par cas et selon les circonstances de l'espèce, si leur communication risquerait d'empiéter sur les compétences et prérogatives d'une autorité judiciaire ou d'une juridiction, auxquelles il appartient seules, dans le cadre des procédures engagées devant elles et en vertu des principes et des textes qui leur sont applicables, d'assurer le respect des droits de la défense et le caractère contradictoire de la procédure ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les études en cause avaient été produites dans le cadre de l'information judicaire ouverte contre la société Les Laboratoires Servier, mise en examen, et qu'elles faisaient l'objet d'une expertise judiciaire en cours dans le cadre de cette information judiciaire ; qu'en outre, l'identification des effets secondaires du Mediator, spécialité pharmaceutique contenant du benfluorex que la société Les Laboratoires Servier avait commercialisée avant son retrait du marché, constituait un élément essentiel de la caractérisation des éléments matériels de l'infraction pour laquelle elle était ainsi poursuivie ; qu'en jugeant que la communication demandée était de nature à porter atteinte au déroulement d'une procédure juridictionnelle, au sens du f) du 2° du I de l'article 6 de la loi du 17 juillet 1978, le tribunal administratif de Paris, qui a suffisamment motivé son jugement, n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant que le motif, par lequel  le tribunal administratif a ainsi jugé que le f) du 2° du I de l'article 6 de la loi du 17 juillet 1978 faisait obstacle à la communication des documents en cause, justifiait nécessairement, à lui seul, le dispositif de rejet ; que si le tribunal a également fondé sa décision sur des motifs tirés de ce que la communication des données demandées était de nature à porter atteinte, d'une part, au secret en matière industrielle et commerciale et, d'autre part, à la protection de la vie privée et au secret médical, il résulte de ce qui vient d'être dit que de tels motifs ne peuvent qu'être regardés comme surabondants ; que les moyens tirés de ce que ces motifs seraient entachés d'insuffisance de motivation et d'erreur de droit ne sauraient dès lors, quel qu'en soit le bien-fondé, entraîner l'annulation du jugement attaqué ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société Les Laboratoires Servier n'est pas fondée à demander l'annulation du jugement du tribunal administratif de Paris qu'elle attaque ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Les Laboratoires Servier une somme de 3 000 euros à verser à la CNAMTS au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la CNAMTS, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Les Laboratoires Servier est rejeté.<br/>
Article 2 : La société Les Laboratoires Servier versera une somme de 3 000 euros à la CNAMTS au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Les Laboratoires Servier et à la Caisse nationale de l'assurance maladie des travailleurs salariés.<br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. - APPLICATION DE LA LOI DU 17 JUILLET 1978 AUX DONNÉES RELEVANT DE LA LOI DU 6 JANVIER 1978 (ART. 37 DE CETTE LOI) - EXISTENCE - MODALITÉS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-02-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS NON COMMUNICABLES. - DOCUMENTS ADMINISTRATIFS DONT LA COMMUNICATION PORTERAIT ATTEINTE AU DÉROULEMENT DES PROCÉDURES ENGAGÉES DEVANT LES JURIDICTIONS OU D'OPÉRATIONS PRÉLIMINAIRES À DE TELLES PROCÉDURES (F DU 2° DU I DE L'ART. 6) [RJ1] - ESPÈCE - DOCUMENTS PRODUITS DANS LE CADRE D'UNE INFORMATION JUDICIAIRE OUVERTE CONTRE LE REQUÉRANT, FAISANT L'OBJET D'UNE EXPERTISE JUDICIAIRE EN COURS DANS CE CADRE ET DONT L'OBJET CONSTITUE UN ÉLÉMENT ESSENTIEL DE LA CARACTÉRISATION DES ÉLÉMENTS MATÉRIELS DE L'INFRACTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-07-01 DROITS CIVILS ET INDIVIDUELS. - APPLICATION DE LA LOI DU 17 JUILLET 1978 AUX DONNÉES RELEVANT DE LA LOI DU 6 JANVIER 1978 (ART. 37 DE CETTE LOI) - EXISTENCE - MODALITÉS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. - POURVOI CONTRE UN JUGEMENT DE TA RÉGULIER FONDÉ SUR PLUSIEURS MOTIFS DONT L'UN JUSTIFIE NÉCESSAIREMENT, À LUI SEUL, LE DISPOSITIF DE REJET - REJET DU POURVOI, LES AUTRES MOTIFS ÉTANT ALORS REGARDÉS COMME SURABONDANTS [RJ2].
</SCT>
<ANA ID="9A"> 26-06-01-02 Il résulte de l'article 37 de la loi n° 78-17 du 6 janvier 1978 que les dispositions de cette loi ne font, en principe, pas obstacle à l'application, au bénéfice de tiers, des dispositions du titre Ier de la loi n° 78-753 du 17 juillet 1978. Lorsque des données à caractère personnel ont également le caractère de documents administratifs, elles ne sont communicables aux tiers, en vertu du III de l'article 6 de la loi du 17 juillet 1978, que s'il est possible d'occulter ou de disjoindre les mentions portant atteinte, notamment, à la protection de la vie privée ou au secret médical. Il ne peut être accédé à une demande de communication sur le fondement de la loi du 17 juillet 1978 que si le traitement nécessaire pour rendre impossible, s'agissant de données de santé, toute identification, directe ou indirecte, de l'une quelconque des personnes concernées, y compris par recoupement avec d'autres données, n'excède pas l'occultation ou la disjonction des mentions non communicables, seule envisagée par cette loi. Dans le cas contraire, sont seules applicables les dispositions de la loi du 6 janvier 1978 et des lois spéciales applicables au traitement de certaines catégories de données, notamment, en ce qui concerne les données de santé à caractère personnel, les chapitres IX et X de la loi du 6 janvier 1978.</ANA>
<ANA ID="9B"> 26-06-01-02-03 Les documents sollicités étaient des études qui avaient été produites dans le cadre de l'information judicaire ouverte contre la société requérante, mise en examen, et qui faisaient l'objet d'une expertise judiciaire en cours dans le cadre de cette information judiciaire. En outre, le contenu de ces documents constituait un élément essentiel de la caractérisation des éléments matériels de l'infraction pour laquelle elle était ainsi poursuivie. Leur communication était donc de nature à porter atteinte au déroulement d'une procédure juridictionnelle au sens du f) du 2° du I de l'article 6 de la loi n° 78-753 du 17 juillet 1978.</ANA>
<ANA ID="9C"> 26-07-01 Il résulte de l'article 37 de la loi n° 78-17 du 6 janvier 1978 que les dispositions de cette loi ne font, en principe, pas obstacle à l'application, au bénéfice de tiers, des dispositions du titre Ier de la loi n° 78-753 du 17 juillet 1978. Lorsque des données à caractère personnel ont également le caractère de documents administratifs, elles ne sont communicables aux tiers, en vertu du III de l'article 6 de la loi du 17 juillet 1978, que s'il est possible d'occulter ou de disjoindre les mentions portant atteinte, notamment, à la protection de la vie privée ou au secret médical. Il ne peut être accédé à une demande de communication sur le fondement de la loi du 17 juillet 1978 que si le traitement nécessaire pour rendre impossible, s'agissant de données de santé, toute identification, directe ou indirecte, de l'une quelconque des personnes concernées, y compris par recoupement avec d'autres données, n'excède pas l'occultation ou la disjonction des mentions non communicables, seule envisagée par cette loi. Dans le cas contraire, sont seules applicables les dispositions de la loi du 6 janvier 1978 et des lois spéciales applicables au traitement de certaines catégories de données, notamment, en ce qui concerne les données de santé à caractère personnel, les chapitres IX et X de la loi du 6 janvier 1978.</ANA>
<ANA ID="9D"> 54-08-02-02 Lorsque le juge de cassation constate qu'un des motifs retenus par le tribunal administratif justifie nécessairement, à lui seul, le dispositif de rejet de son jugement, il regarde alors les autres motifs de ce jugement comme surabondants. Par suite, les moyens tirés de ce que ces motifs seraient entachés d'insuffisances de motivation et d'erreur de droit sont inopérants [RJ3].</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 26 décembre 2013, Société Les laboratoires Servier, n° 372230, T. pp. 603-674-675.,,[RJ2]Comp. CE, Section, 22 avril 2005, Commune de Barcarès, n° 257877, p. 170.,,[RJ3]Rappr., dans le cas d'une substitution de motifs en cassation, CE, 18 novembre 2015, M. et Mme Ahmed, n° 369502, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
