<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253403</ID>
<ANCIEN_ID>JG_L_2017_12_000000401772</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/34/CETATEXT000036253403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 22/12/2017, 401772, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401772</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:401772.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...-C... a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du 4 janvier 2011 par laquelle l'inspecteur du travail de la 8ème section du Val d'Oise a autorisé la société Clearstone central laboratories à la licencier. Par un jugement n° 1101636 du 23 juin 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14VE02426 du 24 mai 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par Mme A...-C... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juillet et 25 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...-C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Clearstone central laboratories la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail,<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de Mme A...-C... ;<br/>
<br/>
              Vu la note en délibérée, enregistrée le 21 novembre 2017, présentée par Mme A... -C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, dans le cadre d'un plan de sauvegarde de l'emploi mis en oeuvre par la société Clearstone central laboratories, l'inspecteur du travail de la 8ème section du Val d'Oise a, le 4 janvier 2011, autorisé cette société à licencier Mme A...-C..., salariée protégée ; que cette dernière se pourvoit en cassation contre l'arrêt du 24 mai 2016 par lequel la cour administrative d'appel de Versailles a rejeté sa requête dirigée contre le jugement du 23 juin 2014 du tribunal administratif de Cergy-Pontoise qui a rejeté sa demande d'annulation de cette décision ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions d'effectifs envisagées et de la possibilité d'assurer le reclassement du salarié ;<br/>
<br/>
              3. Considérant que, pour apprécier si l'employeur a satisfait à son obligation en matière de reclassement, l'autorité administrative doit s'assurer, sous le contrôle du juge de l'excès de pouvoir, qu'il a procédé à une recherche sérieuse des possibilités de reclassement du salarié, tant au sein de l'entreprise que dans les entreprises du groupe auquel elle appartient ; qu'il appartient à l'administration de tenir compte de l'ensemble des circonstances de fait qui lui sont soumises, notamment de ce que, le cas échéant, les recherches de reclassement ont débouché sur une proposition précise et, dans ce cas, des motifs de refus avancés par le salarié ;<br/>
<br/>
              4. Considérant qu'il résulte des termes mêmes de l'arrêt attaqué que, pour juger que les recherches de reclassement opérées par la société Clearstone central laboratories avaient été sérieuses, la cour s'est fondée sur ce que l'intéressée, bien qu'ayant formellement refusé le poste de reclassement qui lui était proposé, devait être regardée, compte tenu de la nature de ce poste et de la circonstance qu'elle avait, dans un premier temps, exprimé son intention d'y candidater, comme ayant accepté la proposition de reclassement ; qu'en statuant ainsi, la cour a dénaturé les pièces du dossier qui lui était soumis ; que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme A...-C... est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que, dans le cadre de la compression d'effectifs mise en oeuvre par la société Clearstone central laboratories, Mme A... -C... a, dans un premier temps, manifesté l'intention d'accepter le poste de " global document control specialist " que lui proposait son employeur pour assurer son reclassement dans l'entreprise ; qu'elle a ensuite renoncé à ce poste, en mettant en avant une modification du descriptif de ce poste qui, contrairement à ce qu'elle soutient, ne permettait pas de le faire regarder comme un poste distinct de celui auquel elle avait candidaté ; que, par suite, dans les circonstances propres à l'espèce et compte tenu, notamment, de l'accord qu'elle pensait avoir obtenu de la part de Mme A...-C... sur le poste qui lui avait été proposé, la société Clearstone central laboratories doit être regardée, alors même que, selon la requérante, d'autres postes auraient pu lui être proposés, comme ayant procédé à une recherche sérieuse des possibilités de reclassement de l'intéressée ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que Mme A...-C... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa demande d'annulation de la décision du 4 janvier 2011 par laquelle l'inspecteur du travail de la 8ème section du Val d'Oise a autorisé la société Clearstone central laboratories à la licencier ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Clearstone central laboratories les sommes que demande, à ce titre, tant en appel qu'en cassation Mme A...-C....<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 24 mai 2016 est annulé. <br/>
Article 2 : La requête de Mme A...-C... dirigée contre le jugement du 23 juin 2014 du tribunal administratif de Cergy-Pontoise est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de Mme A...-C... présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...-C..., à la société Clearstone central laboratories et à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
