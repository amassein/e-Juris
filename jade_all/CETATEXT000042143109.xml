<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143109</ID>
<ANCIEN_ID>JG_L_2020_07_000000431419</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143109.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 22/07/2020, 431419, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431419</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431419.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les sociétés Bouygues Télécom et Cellnex ont demandé au juge des référés du tribunal administratif de Toulouse, sur le fondement des articles L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 5 mars 2019 par lequel le maire de la commune de Muret s'est opposé à la déclaration préalable de travaux en vue de la réalisation d'une installation de téléphonie mobile et d'enjoindre à la commune de reprendre l'instruction de la déclaration préalable du 7 février 2019.<br/>
<br/>
              Par une ordonnance n° 1902364 du 22 mai 2019, le juge des référés du tribunal administratif de Toulouse a fait droit à leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 et 20 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Muret demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire comme juge des référés, de rejeter la requête des sociétés Cellnex et Bouygues Telecom ;<br/>
<br/>
              3°) de mettre à la charge des sociétés Cellnex et Bouygues Télécom la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet-Odent, avocat de la commune de Muret, et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la société Bouygues Telecom et de la société Cellnex France ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2.	Il ressort des pièces du dossier soumis au juge des référés que, le 7 février 2019, la société Cellnex, agissant dans le cadre d'un mandat l'unissant à la société Bouygues Télécom, a déposé auprès de la commune de Muret un dossier de déclaration préalable en vue de l'installation d'équipements de radiotéléphonie mobile. Le maire de Muret s'est opposé, par un arrêté en date du 5 mars 2019, à la déclaration préalable de la société Cellnex. Par une ordonnance du 22 mai 2019, le juge des référés du tribunal administratif de Toulouse a, à la demande des sociétés Cellnex et Bouygues Télécom, suspendu l'exécution de l'arrêté litigieux et enjoint au maire de Muret de procéder au réexamen de la déclaration préalable de la société Cellnex. La commune de Muret se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              3.	En premier lieu, le moyen tiré de de ce que celle-ci ne serait pas signée par son auteur manque en fait. <br/>
<br/>
              4.	En deuxième lieu, aux termes de l'article L. 111-11 du code de l'urbanisme " Lorsque, compte tenu de la destination de la construction ou de l'aménagement projeté, des travaux portant sur les réseaux publics de distribution d'eau, d'assainissement ou de distribution d'électricité sont nécessaires pour assurer la desserte du projet, le permis de construire ou d'aménager ne peut être accordé si l'autorité compétente n'est pas en mesure d'indiquer dans quel délai et par quelle collectivité publique ou par quel concessionnaire de service public ces travaux doivent être exécutés. / Lorsqu'un projet fait l'objet d'une déclaration préalable, l'autorité compétente doit s'opposer à sa réalisation lorsque les conditions mentionnées au premier alinéa ne sont pas réunies. (...). "<br/>
<br/>
              5.	Ces dispositions poursuivent notamment le but d'intérêt général d'éviter à la collectivité publique ou au concessionnaire d'être contraints, par le seul effet d'une initiative privée, de réaliser des travaux d'extension ou de renforcement des réseaux publics et de garantir leur cohérence et leur bon fonctionnement, sans prise en compte des perspectives d'urbanisation et de développement de la collectivité. Une modification de la consistance d'un des réseaux publics que ces dispositions mentionnent, notamment du réseau public de distribution d'eau, ne peut être réalisée sans l'accord de l'autorité administrative compétente. <br/>
<br/>
              6.	Pour estimer qu'était de nature à créer un doute sérieux sur la légalité de l'arrêté litigieux le motif tiré de la méconnaissance des dispositions de l'article L. 111-11 du code de l'urbanisme, le juge des référés a relevé que l'installation projetée d'équipements de radiotéléphonie mobile nécessitait un raccordement au réseau électrique d'une longueur de 45 mètres, qui ne pouvait être regardé, dans les circonstances de l'espèce, comme un équipement public résultant de travaux d'extension du réseau d'électricité alors qu'il ne nécessitait que de simples travaux de branchement. En prenant ainsi en considération la distance entre l'installation et le réseau, que le maire avait au demeurant retenue en tant que telle comme motif d'opposition, ainsi que l'ensemble des autres circonstances de l'espèce, pour déduire que les travaux de raccordement n'étaient pas des travaux d'extension du réseau, le juge des référés n'a commis aucune erreur de droit.<br/>
<br/>
              7.	En troisième lieu, aux termes de l'article A 10 du plan local d'urbanisme intitulé " Hauteur des constructions ", " La hauteur autorisée se mesure à partir du niveau du sol naturel au droit de la construction et jusque sur les lignes sablières dans le cas d'une toiture traditionnelle ou jusqu'à la partie supérieure de l'acrotère, dans le cas de toiture-terrasse. Des dépassements de hauteur pourront être admis pour des éléments de construction de très faible emprise tels que cheminées, etc. / 2° Pour les autres constructions ou installations nouvelles autorisées autres qu'habitation, la hauteur ne doit pas dépasser 10 mètres sur sablière et 15 mètres toitures comprises. ". En interprétant ces dispositions comme ne fixant une hauteur maximale que pour les constructions pourvues d'une toiture et non pour l'ensemble des bâtiments, en particulier le pylône, objet de la déclaration de travaux et, par suite, en jugeant qu'était de nature à créer un doute sérieux sur la légalité de l'acte attaqué le motif tiré de la méconnaissance de ces dispositions du plan local d'urbanisme, le juge des référés n'a pas commis d'erreur de droit.<br/>
<br/>
              8.	En quatrième lieu, aux termes de l'article A 11 du plan local d'urbanisme intitulé " Aspect extérieur des constructions et aménagement de leurs abords ", " Pour être autorisé, tout projet de construction nouvelle ou d'aménagement de construction déjà existant, doit garantir (...) la préservation de l'environnement, celle du caractère, de l'intérêt et de l'harmonie des lieux ou paysages avoisinants (sites naturels, urbains, perspectives monumentales, ...), celle de la nature du site existant, celle du caractère de la région [ainsi que] la recherche d'une certaine unité de style, de forme, de volume, de proportions de matériaux, de couleurs, etc. ". L'article R. 111-27 du code de l'urbanisme dispose que " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales ".<br/>
<br/>
              9.	Il ressort des pièces du dossier soumis au juge des référés que si le pylône pourrait éventuellement être visible depuis la zone urbanisée se trouvant à 300 mètres de distance du terrain agricole retenu pour le projet, l'impact visuel serait toutefois limité alors qu'a été retenue l'option d'un pylône de type treillis et que le territoire de la commune est classé à hauteur de 50 % en zone agricole. Par suite, c'est sans dénaturer les pièces du dossier qui lui était soumis que le juge des référés a regardé comme de nature à créer un doute sérieux sur la légalité de la décision attaquée le motif tiré de l'atteinte au caractère et à l'intérêt des lieux avoisinants.<br/>
<br/>
              10.	Il résulte de tout ce qui précède que la commune de Muret n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des sociétés Cellnex et Bouygues Télécom, qui ne sont pas, dans la présente instance, les parties perdantes. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Muret la somme 1 500 euros qui sera versée à la société Cellnex et la somme de 1 500 euros qui sera versée à la société Bouygues Télécom au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Muret est rejeté.<br/>
<br/>
Article 2 : La commune de Muret versera la somme 1 500 euros à la société Cellnex et la somme de 1 500 euros à la société Bouygues Télécom au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Muret et aux sociétés Cellnex et Bouygues Télécom.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
