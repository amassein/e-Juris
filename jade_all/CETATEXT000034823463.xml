<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034823463</ID>
<ANCIEN_ID>JG_L_2017_05_000000393448</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/82/34/CETATEXT000034823463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 29/05/2017, 393448</TITRE>
<DATE_DEC>2017-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393448</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393448.20170529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. B...C...a demandé au tribunal administratif de Rennes d'annuler l'arrêté du préfet du Finistère du 27 novembre 2009 autorisant le président de la fondation Pierre D... à conclure avec la commune de Plouescat un bail emphytéotique d'une durée de trente ans pour la location de bâtiments situés dans cette commune. Par un jugement n° 1000776 du 8 mars 2013, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13NT01289 du 30 juin 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. C...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 11 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M.C....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 mai 2017, présentée par le ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la fondation PierreD..., qui est propriétaire à Plouescat de bâtiments, les a mis gratuitement à disposition de l'association de gestion du lycée professionnel rural privé " Pierre D...", lequel y a dispensé un enseignement jusqu'à la fermeture de cet établissement en juin 2009. Le conseil d'administration de cette fondation a alors approuvé le principe d'une mise à disposition gracieuse de ces locaux à la commune de Plouescat à l'effet d'y accueillir des associations à caractère éducatif ou de loisir, à charge pour la commune de s'acquitter de l'entretien, de la mise aux normes, des assurances et des diverses taxes. Un projet de bail emphytéotique a été établi pour une durée de trente ans et soumis à l'autorisation du préfet du Finistère. Cette autorisation a été délivrée par un arrêté du 27 novembre 2009. Par un jugement du 8 mars 2013, le tribunal administratif de Rennes a rejeté comme irrecevable la demande de M. C...tendant à l'annulation de cet arrêté. M. C...se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Nantes du 30 juin 2015 qui a confirmé ce jugement. <br/>
<br/>
              2. Aux termes de l'article 954 du code civil : " Dans le cas de la révocation pour cause d'inexécution des conditions, les biens rentreront dans les mains du donateur, libres de toutes charges et hypothèques du chef du donataire ; et le donateur aura, contre les tiers détenteurs des immeubles donnés, tous les droits qu'il aurait contre le donataire lui-même ". Aux termes de l'article 1046 du même code : " Les mêmes causes qui, suivant l'article 954 et les deux premières dispositions de l'article 955, autoriseront la demande en révocation de la donation entre vifs, seront admises pour la demande en révocation des dispositions testamentaires ". En application de ces dispositions, les héritiers, directs ou indirects, de l'auteur d'un legs ou d'une donation ont intérêt à en demander la révocation au juge judiciaire pour inexécution des charges dont il ou elle est grevé. De même, ces héritiers ont intérêt à demander l'annulation d'un acte administratif approuvant un contrat de bail emphytéotique permettant la constitution de droits réels immobiliers sur des biens donnés ou légués par le testateur à une fondation reconnue d'utilité publique, lorsque le legs ou la donation en cause est grevé de charges et encore susceptible, au moment de l'introduction de la demande, de faire l'objet d'une action en révocation pour inexécution, notamment au regard des règles de prescription applicables à une telle action.<br/>
<br/>
              3. Pour écarter l'intérêt à agir de M.C..., la cour a notamment relevé qu'il ne justifiait d'aucun droit sur les immeubles donnés à bail par la fondation Pierre D..., dès lors que Mme E...A..., mère de M. C...et cousine au 5ème degré de M. D..., révélée comme sa seule héritière à la suite d'une recherche d'héritier entreprise après son décès, avait, d'une part, reçu un legs particulier de M. D...et, d'autre part, consenti le 26 avril 1967 à l'exécution des dispositions testamentaires de M. D... en faveur de l'association diocésaine de Quimper qu'il avait désignée comme son légataire universel. En statuant ainsi, alors que, M. C...présentant la qualité d'héritier de M. D..., il lui revenait de vérifier, au vu des éléments produits par le requérant, si les immeubles donnés à bail par la fondation étaient issus d'un legs ou d'une donation de M. D..., si chaque legs ou donation était grevé de charges et si , enfin, ces actes étaient susceptibles de faire l'objet, au moment de l'introduction de la demande de M.C..., d'une action en révocation pour inexécution, la cour a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M.C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>             D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 30 juin 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'Etat versera à M. C...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... C..., à la fondation Pierre D... et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">25-06 DONS ET LEGS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTÉRÊT POUR AGIR - HÉRITIERS DE L'AUTEUR D'UN LEGS OU D'UNE DONATION - DEMANDE D'ANNULATION D'UN ACTE ADMINISTRATIF APPROUVANT UN CONTRAT DE BAIL EMPHYTÉOTIQUE PERMETTANT LA CONSTITUTION DE DROITS RÉELS IMMOBILIERS SUR DES BIENS DONNÉS OU LÉGUÉS PAR LE TESTATEUR À UNE FONDATION RECONNUE D'UTILITÉ PUBLIQUE - EXISTENCE - CONDITION - LEGS OU DONATION EN CAUSE GREVÉ DE CHARGES ET ENCORE SUSCEPTIBLE DE FAIRE L'OBJET D'UNE ACTION EN RÉVOCATION POUR INEXÉCUTION.
</SCT>
<ANA ID="9A"> 25-06 En application des dispositions des articles 954 et 1046 du code civil, les héritiers, directs ou indirects, de l'auteur d'un legs ou d'une donation ont intérêt à en demander la révocation au juge judiciaire pour inexécution des charges dont il ou elle est grevé. De même, ces mêmes héritiers ont intérêt à demander l'annulation d'un acte administratif approuvant un contrat de bail emphytéotique permettant la constitution de droits réels immobiliers sur des biens donnés ou légués par le testateur à une fondation reconnue d'utilité publique, lorsque le legs ou la donation en cause est grevé de charges et encore susceptible, au moment de l'introduction de la demande, de faire l'objet d'une action en révocation pour inexécution, notamment au regard des règles de prescription applicables à une telle action.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
