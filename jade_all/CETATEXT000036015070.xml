<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036015070</ID>
<ANCIEN_ID>JG_L_2017_10_000000414655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/01/50/CETATEXT000036015070.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/10/2017, 414655, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:414655.20171030</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 septembre et 18 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la société FNAC Darty demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 28 juillet 2017 par laquelle la présidente de l'Autorité de la concurrence a refusé d'agréer la cession au groupe Dray des points de vente exploités sous l'enseigne Darty situés au 25-35 boulevard de Belleville (75011 Paris) et aux 125-127 avenue de Saint-Ouen (75017 Paris) et visés dans la lettre des engagements annexés à la décision n°16-DCC-111 du 27 juillet 2016 par laquelle l'Autorité de la concurrence a autorisé, sous réserve de ces engagements, la prise de contrôle exclusif de Darty par la FNAC ;<br/>
<br/>
              2°) d'enjoindre à l'Autorité de la concurrence de réexaminer la demande d'agrément du Groupe Dray comme acquéreur du contrôle de ces deux points de vente ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité de la concurrence la somme de 4 000 euros au titre de l'article L. 761-1 du code de la justice administrative.<br/>
<br/>
<br/>
<br/>
              La société FNAC Darty soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que la combinaison de la décision contestée avec celle, dont la suspension est également demandée, de refus de prolongation des délais d'exécution des engagements préjudicie de manière grave et immédiate à ses intérêts économiques, en ce que, d'une part, elle met la FNAC dans une situation d'inexécution de ses engagements, alors que l'Autorité de la concurrence a annoncé son intention de tirer les conséquences d'une telle situation par la mise en oeuvre des prérogatives qui lui sont reconnues par l'article L. 430-8 du code de commerce et, d'autre part, elle aurait des conséquences gravement préjudiciables dans ses rapports avec le groupe Dray ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée, qui doit s'analyser, non comme une décision de rejet de la demande d'agrément du groupe Dray en qualité de repreneur des points de vente Darty Belleville et Darty Saint-Ouen mais comme une décision de retrait de la décision implicite d'agrément née le 11 juillet 2017, du fait du silence conservé pendant deux mois par l'Autorité de la concurrence sur cette demande, formée par un courriel en date du 11 mai 2017 ; <br/>
              - la présidente de l'Autorité de la concurrence n'a pas respecté le principe du caractère contradictoire de la procédure et les droits de la défense, en s'abstenant de recueillir les explications de la FNAC sur les objections qu'aurait pu susciter le choix du groupe Dray comme repreneur des deux magasins en cause ;<br/>
              - la décision est entachée d'un vice d'incompétence dès lors qu'elle ne peut être regardée comme une simple " décision nécessaire à la mise en oeuvre des mesures mentionnées aux III et IV de l'article L. 430-7 du code de commerce " que la présidente de l'Autorité de la concurrence peut prendre ;<br/>
              - elle est entachée d'une insuffisance de motivation ;<br/>
              - elle est entachée d'illégalité en ce qu'elle repose, d'une part, sur une appréciation erronée de la portée et du rôle de la méthode dite de " scoring " sur laquelle l'autorité s'est principalement fondée pour refuser d'agréer le groupe Dray, ce qui a pour effet de modifier la substance des engagements souscrits en les rendant plus difficiles, voire impossibles à respecter et discriminatoires et, d'autre part, sur une mise en oeuvre erronée de cette même méthode au cas d'espèce ;<br/>
              - elle fixe, en subordonnant l'agrément à l'existence de garanties juridiquement contraignantes, un niveau d'exigence probatoire inaccessible à tout repreneur se présentant comme un acteur émergent sur le marché des produits électroniques, dits " bruns et gris " ;<br/>
                - elle est entachée d'une erreur manifeste d'appréciation en ce qu'elle ne prend pas en compte l'expérience du groupe Dray dans la vente au détail de produits électroménagers (produits blancs), laquelle différe peu de la vente au détail de produits électroniques (produits bruns et gris) ;<br/>
              - elle dénature les travaux du mandataire indépendant relatifs à l'analyse des données du business plan du groupe Dray, notamment concernant la part que représente la vente de produits bruns et gris dans le chiffre d'affaire total des magasins concernés.<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 13 et 19 octobre 2017, l'Autorité de la concurrence conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par les requérants ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société FNAC Darty, d'autre part, l'Autorité de la concurrence ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 20 octobre 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Texidor, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de la société FNAC Darty ;<br/>
<br/>
              - les représentants de la société FNAC Darty ;<br/>
<br/>
              - les représentants de l'Autorité de la concurrence ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au vendredi 27 octobre à 18 heures.<br/>
<br/>
              Vu les pièces nouvelles, enregistrées les 23 et 24 octobre 2017, présentés respectivement par l'autorité de la concurrence et pour la société FNAC Darty ;<br/>
<br/>
              Vu le nouveau mémoire de la société Fnac Darty enregistré le 25 octobre 2017 qui persiste dans ses précédentes écritures ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 430-1 du code de commerce : " I. - Une opération de concentration est réalisée :1° Lorsque deux ou plusieurs entreprises antérieurement indépendantes fusionnent ; / 2° Lorsqu'une ou plusieurs personnes, détenant déjà le contrôle d'une entreprise au moins ou lorsqu'une ou plusieurs entreprises acquièrent, directement ou indirectement, que ce soit par prise de participation au capital ou achat d'éléments d'actifs, contrat ou tout autre moyen, le contrôle de l'ensemble ou de parties d'une ou plusieurs autres entreprises. (...) " ; qu'il appartient à l'Autorité de la concurrence, saisie d'une opération de concentration, à partir d'une analyse prospective tenant compte de l'ensemble des données pertinentes et se fondant sur un scénario économique plausible, de caractériser les effets anticoncurrentiels de l'opération et d'apprécier si ces effets sont de nature à porter atteinte au maintien d'une concurrence suffisante sur les marchés qu'elle affecte ; que l'Autorité de la concurrence, à laquelle une opération de concentration entrant dans le champ défini par les articles L. 430-1 et L. 430-2  du code commerce a été notifiée, peut, en vertu des dispositions de l'article L. 430-5 du même code, soit autoriser l'opération en la subordonnant éventuellement à la réalisation effective d'engagements pris par les parties, soit, si elle estime qu'il existe un doute sérieux d'atteinte à la concurrence, engager un examen approfondi au terme duquel elle prend une décision qui peut être d'autorisation, le cas échéant assortie d'engagements, de prescriptions ou d'injonctions, ou d'interdiction, dans les conditions prévues aux articles L. 430-6 et L. 430-7 du même code ; qu'aux termes de l'article L. 430-8 du code de commerce " IV. - Si elle estime que les parties n'ont pas exécuté dans les délais fixés une injonction, une prescription ou un engagement figurant dans sa décision, l'Autorité de la concurrence constate l'inexécution. Elle peut : /1° Retirer la décision ayant autorisé la réalisation de l'opération. A moins de revenir à l'état antérieur à la concentration, les parties sont tenues de notifier de nouveau l'opération dans un délai d'un mois à compter du retrait de la décision, sauf à encourir les sanctions prévues au I ; / 2° Enjoindre sous astreinte, dans la limite prévue au II de l'article L. 464-2, aux parties auxquelles incombait l'obligation non exécutée d'exécuter dans un délai qu'elle fixe les injonctions, prescriptions ou engagements figurant dans la décision ; / 3° Enjoindre sous astreinte, dans la limite prévue au II de l'article L. 464-2, aux parties auxquelles incombait l'obligation, d'exécuter dans un délai qu'elle fixe des injonctions ou des prescriptions en substitution de l'obligation non exécutée. / En outre, l'Autorité de la concurrence peut infliger aux personnes auxquelles incombait l'obligation non exécutée une sanction pécuniaire qui ne peut dépasser le montant défini au I ".<br/>
<br/>
              3. Considérant que par une décision du 27 juillet 2016, l'Autorité de la concurrence a autorisé, sous réserve du respect des engagements souscrits par la partie notifiante et annexés à sa décision, la prise de contrôle exclusif de Darty par la FNAC ; qu'en vue de prévenir les effets anticoncurrentiels de l'opération de concentration, la FNAC s'est engagée à céder six points de ventes exploités sous les enseignes FNAC ou Darty, chaque contrat de cession étant subordonné à son approbation par l'autorité de la concurrence ; qu'après la conclusion de promesses de vente des magasins " Darty Belleville " et " Darty Saint-Ouen " au profit de sociétés du groupe Dray, la FNAC a sollicité de l'Autorité de la concurrence, par courrier du 10 mai 2017, l'agrément de la cession de ces points de vente à cet acquéreur ; que, par une décision du 28 juillet 2017, la présidente de l'Autorité de la concurrence a rejeté cette demande ; la société FNAC Darty demande la suspension de l'exécution de cette décision ;<br/>
<br/>
              4. Considérant que la société requérante soutient, en premier lieu, pour établir que la condition d'urgence posée par l'article L.521-1 du code de justice administrative est satisfaite, que la décision de refus d'agrément des projets de cession de points de vente au Groupe Dray que lui a opposée la présidente de l'Autorité de la concurrence la place en situation de manquement au respect de ses engagements et l'expose au risque de mise en oeuvre, à tout moment, par l'Autorité, des prérogatives qu'elle tient de l'article L.430-8 du code de commerce ; que, toutefois, si la mise en oeuvre à l'encontre de cette société de l'une des mesures énoncées à cet article, à savoir un retrait de l'autorisation de concentration, une injonction, éventuellement assortie d'une astreinte, d'exécuter ses engagements ou des prescriptions de substitution ou une sanction pécuniaire serait, le cas échéant, susceptible de porter une atteinte grave et immédiate à ses intérêts, elle ne pourrait procéder que d'une décision, distincte de celle contestée, prise par le collège de l'Autorité de la concurrence à l'issue d'une procédure contradictoire et dont la société pourrait, si elle s'y croyait fondée, demander la suspension de l'exécution ; que la décision contestée, à supposer qu'elle puisse être regardée comme détachable de cette procédure, ne fait, par elle-même, peser sur la société requérante aucune des obligations mentionnées au IV de l'article L.430-8 du code de commerce ; que le préjudice invoqué est, par suite, seulement éventuel à ce jour ;  <br/>
               5. Considérant que si la société soutient, en second lieu, que la décision contestée a des conséquences gravement préjudiciables sur ses rapports avec le groupe Dray et l'expose au risque que cette société renonce à son projet d'acquisition des points de vente " Darty Belleville " et " Darty Saint-Ouen ", elle n'établit pas que l'éventuel désengagement du groupe Dray de cette opération aurait sur sa propre situation économique ou financière des conséquences d'une gravité de nature à caractériser une situation d'urgence ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative ne peut être regardée comme satisfaite ; qu'ainsi, sans qu'il soit besoin d'examiner si l'un au moins des moyens soulevés par la société requérante est de nature à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision contestée, la demande de la société FNAC Darty tendant à ce que soit ordonnée la suspension de son exécution doit être rejetée, y compris ses conclusions tendant à la mise en oeuvre des dispositions de l'article L.761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société FNAC Darty est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société FNAC Darty et à l'Autorité de la concurrence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
