<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043605964</ID>
<ANCIEN_ID>JG_L_2021_05_000000447315</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/60/59/CETATEXT000043605964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/05/2021, 447315, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447315</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447315.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. B... A... a demandé à la commission du contentieux du stationnement payant d'annuler le titre exécutoire émis par l'Agence nationale de traitement automatisé des infractions, ayant donné lieu à un avertissement en date du 6 septembre 2018, en vue du recouvrement du forfait de post-stationnement mis à sa charge le 12 avril 2018 par la ville de Paris, ainsi que la majoration dont il est assorti. Par une ordonnance n° 19047618 du 28 septembre 2020, la magistrate désignée par la présidente de la commission a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 décembre 2020 et 8 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la Ville de Paris la somme de 300 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
              Par un mémoire, enregistré le 8 mars 2021, M. A... demande au Conseil d'Etat, à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du VII de l'article L. 2333-87 du code général des collectivités territoriales. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Le I de l'article L.2333-87 du code général des collectivités territoriales dispose que : " I.- (...) le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale (...), peut instituer une redevance de stationnement, compatible avec les dispositions du plan de déplacements urbains, s'il existe./ La délibération institutive établit : 1° Le barème tarifaire de paiement immédiat de la redevance, applicable lorsque la redevance correspondant à la totalité de la période de stationnement est réglée par le conducteur du véhicule dès le début du stationnement ; 2° Le tarif du forfait de post-stationnement, applicable lorsque la redevance correspondant à la totalité de la période de stationnement n'est pas réglée dès le début du stationnement ou est insuffisamment réglée (...) ". L'article L. 330-1 du code de la route dispose que : " Il est procédé, dans les services de l'Etat et sous l'autorité et le contrôle du ministre de l'intérieur, à l'enregistrement de toutes informations concernant les pièces administratives exigées pour la circulation des véhicules ou affectant la disponibilité de ceux-ci ".<br/>
<br/>
              3. Aux termes du VII de l'article L. 2333-87 du code général des collectivités territoriales : " (...) Lorsque, à la suite de la cession d'un véhicule, le système enregistrant les informations mentionnées à l'article L. 330-1 du code de la route mentionne un acquéreur qui n'est pas le titulaire du certificat d'immatriculation du véhicule, l'acquéreur est substitué au titulaire dudit certificat dans la mise en oeuvre des dispositions prévues aux II et IV du présent article ". Il résulte de ces dispositions que le débiteur du forfait de post-stationnement et de sa majoration éventuelle est la personne titulaire du certificat d'immatriculation du véhicule à la date d'émission de l'avis de paiement de ce forfait mais que, lorsque le véhicule a été cédé, son acquéreur est le débiteur du forfait de post-stationnement dès lors que le vendeur a cédé son véhicule avant l'émission de l'avis de paiement et a procédé à la déclaration prévue par l'article R. 322-4 du code de la route avant cette date ou, en tout état de cause, dans le délai de quinze jours prévu à cet article.<br/>
<br/>
              4. Ces dispositions, qui se bornent à fixer l'identité du débiteur du forfait de post-stationnement en cas de cession d'un véhicule, ne sauraient, contrairement à ce que soutient le requérant, ni porter atteinte au droit du propriétaire d'un véhicule de le céder, ni méconnaître la liberté contractuelle du vendeur et de l'acquéreur garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              5. Il résulte de ce qui est dit au point 3 que les dispositions contestées n'ont pour effet de mettre le paiement du forfait de post-stationnement à la charge du vendeur du véhicule que dans deux hypothèses. Soit lorsque l'avis de paiement est émis dans un délai de quinze jours suivant la vente du véhicule, qui est celui dont dispose le vendeur pour accomplir son obligation de déclaration, et qu'aucune déclaration de vente n'est effectuée par lui dans ce délai. Soit lorsque l'avis de paiement est émis après ce délai de quinze jours et que le vendeur n'a, à la date d'émission de cet avis, toujours pas déclaré la vente de son véhicule. Dans ces conditions, en ne rendant le vendeur débiteur des avis de paiement émis pour le stationnement du véhicule qu'il a vendu que dans les seuls cas où il a négligé d'en signaler la vente, le législateur n'a, en tout état de cause, pas porté une atteinte excessive à son droit de propriété, garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la question de la conformité à la Constitution des dispositions VII de l'article L. 2333-87 du code général des collectivités territoriales, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur le pourvoi en cassation :<br/>
<br/>
              7. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              8. Pour demander l'annulation de l'ordonnance de la magistrate déléguée par la présidente de la commission du contentieux du stationnement payant qu'il attaque, M. A... soutient qu'elle est entachée :<br/>
              - d'irrégularité en ce qu'elle n'est pas signée ;<br/>
              - d'erreur de droit en ce qu'elle subordonne à l'accomplissement de la déclaration prévue par les dispositions de l'article R. 322-4 du code de la route la démonstration de la cession de son véhicule par son ancien propriétaire ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle estime que cette déclaration n'a pas été effectuée.<br/>
<br/>
              9. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
<br/>
Article 1er : La question de la conformité à la Constitution du VII de l'article L. 2333-87 du code général des collectivités territoriales n'est pas renvoyée au Conseil constitutionnel.<br/>
Article 2 : Le pourvoi de M. A... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à la Ville de Paris, au Premier ministre, au ministre de l'économie, des finances et de la relance, au Garde des sceaux, ministre de la justice et à l'Agence nationale de traitement automatisé des infractions.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
