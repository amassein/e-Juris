<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039456965</ID>
<ANCIEN_ID>JG_L_2019_12_000000393769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/69/CETATEXT000039456965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/12/2019, 393769</TITRE>
<DATE_DEC>2019-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:393769.20191206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 24 février 2017, le Conseil d'Etat, statuant au contentieux sur la requête de M. A... tendant à l'annulation, pour excès de pouvoir, de la décision par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a clôturé sa plainte tendant au déréférencement de deux liens, renvoyant, d'une part, vers un article du quotidien Libération et, d'autre part, vers le site du Centre contre les manipulations mentales (CCMM) reprenant le contenu de cet article dans les résultats obtenus sur la base d'une recherche effectuée à partir de son nom sur le moteur de recherche exploité par la société Google, qui lui a été notifiée par un courrier du 28 août 2015, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir si : <br/>
<br/>
              1°) Eu égard aux responsabilités, aux compétences et aux possibilités spécifiques de l'exploitant d'un moteur de recherche, l'interdiction faite aux autres responsables de traitement de traiter des données relevant des paragraphes 1 et 5 de l'article 8 de la directive du 24 octobre 1995, sous réserve des exceptions prévues par ce texte, est-elle également applicable à cet exploitant en tant que responsable du traitement que constitue ce moteur '<br/>
<br/>
              2°) En cas de réponse positive à la question posée au 1°:<br/>
              - les dispositions de l'article 8 paragraphes 1 et 5 de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que l'interdiction ainsi faite, sous réserve des exceptions prévues par cette directive, à l'exploitant d'un moteur de recherche de traiter des données relevant de ces dispositions l'obligerait à faire systématiquement droit aux demandes de déréférencement portant sur des liens menant vers des pages web qui traitent de telles données '<br/>
              - dans une telle perspective, comment s'interprètent les exceptions prévues à l'article 8 paragraphe 2, sous a) et e), de la directive du 24 octobre 1995, lorsqu'elles s'appliquent à l'exploitant d'un moteur de recherche, eu égard à ses responsabilités, ses compétences et ses possibilités spécifiques ' En particulier, un tel exploitant peut-il refuser de faire droit à une demande de déréférencement lorsqu'il constate que les liens en cause mènent vers des contenus qui, s'ils comportent des données relevant des catégories énumérées au paragraphe 1 de l'article 8, entrent également dans le champ des exceptions prévues par le paragraphe 2 de ce même article, notamment le a) et le e) '<br/>
              - de même, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que, lorsque les liens dont le déréférencement est demandé mènent vers des traitements de données à caractère personnel effectués aux seules fins de journalisme ou d'expression artistique ou littéraire qui, à ce titre, en vertu de l'article 9 de la directive du 24 octobre 1995, peuvent collecter et traiter des données relevant des catégories mentionnées à l'article 8, paragraphes 1 et 5, de cette directive, l'exploitant d'un moteur de recherche peut, pour ce motif, refuser de faire droit à une demande de déréférencement '<br/>
<br/>
              3°) En cas de réponse négative à la question posée au 1° :<br/>
              - à quelles exigences spécifiques de la directive du 24 octobre 1995 l'exploitant d'un moteur de recherche, compte tenu de ses responsabilités, de ses compétences et de ses possibilités, doit-il satisfaire '<br/>
              - lorsqu'il constate que les pages web, vers lesquelles mènent les liens dont le déréférencement est demandé, comportent des données dont la publication, sur lesdites pages, est illicite, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens :<br/>
              - qu'elles imposent à l'exploitant d'un moteur de recherche de supprimer ces liens de la liste des résultats affichés à la suite d'une recherche effectuée à partir du nom du demandeur '<br/>
              - ou qu'elles impliquent seulement qu'il prenne en compte cette circonstance pour apprécier le bien-fondé de la demande de déréférencement '<br/>
              - ou que cette circonstance est sans incidence sur l'appréciation qu'il doit porter '<br/>
              En outre, si cette circonstance n'est pas inopérante, comment apprécier la licéité de la publication des données litigieuses sur des pages web qui proviennent de traitements n'entrant pas dans le champ d'application territorial de la directive du 24 octobre 1995 et, par suite, des législations nationales la mettant en oeuvre '<br/>
<br/>
              4°) Quelle que soit la réponse apportée à la question posée au 1° :<br/>
              - indépendamment de la licéité de la publication des données à caractère personnel sur la page web vers laquelle mène le lien litigieux, les dispositions de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que :<br/>
              - lorsque le demandeur établit que ces données sont devenues incomplètes ou inexactes, ou qu'elles ne sont plus à jour, l'exploitant d'un moteur de recherche est tenu de faire droit à la demande de déréférencement correspondante '<br/>
              - plus spécifiquement, lorsque le demandeur démontre que, compte tenu du déroulement de la procédure judiciaire, les informations relatives à une étape antérieure de la procédure ne correspondent plus à la réalité actuelle de sa situation, l'exploitant d'un moteur de recherche est tenu de déréférencer les liens menant vers des pages web comportant de telles informations '<br/>
              - les dispositions de l'article 8 paragraphe 5 de la directive du 24 octobre 1995 doivent-elles être interprétées en ce sens que les informations relatives à la mise en examen d'un individu ou relatant un procès, et la condamnation qui en découle, constituent des données relatives aux infractions et aux condamnations pénales ' De manière générale, lorsqu'une page web comporte des données faisant état des condamnations ou des procédures judiciaires dont une personne physique a été l'objet, entre-t-elle dans le champ de ces dispositions '<br/>
<br/>
              Par un arrêt C-136/17 du 24 septembre 2019, la Cour de justice de l'Union européenne s'est prononcée sur ces questions. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 24 février 2017 ; <br/>
<br/>
              Vu : <br/>
              - la Charte des droits fondamentaux de l'Union européenne ; <br/>
              - le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 13 mai 2014, Google Spain SL, Google Inc. contre Agencia Espanola de Proteccion de Datos, Mario Costeja Gonzalez (C-131/12) ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 24 septembre 2019, GC, AF, BH et ED contre CNIL (C-136/17) ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Google LLC ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 novembre 2019, présentée par la CNIL ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. A... a demandé à la société Google de procéder au déréférencement, dans les résultats affichés par le moteur de recherche qu'elle exploite à la suite d'une recherche portant sur son nom, d'un lien hypertexte renvoyant vers un article du quotidien Y en date du 9 septembre 2008 et d'un lien hypertexte renvoyant vers un site reprenant le contenu de cet article. A la suite du refus opposé par la société Google, il a saisi la Commission nationale de l'informatique et des libertés (CNIL) d'une plainte tendant à ce qu'il soit enjoint à cette société de procéder au déréférencement des liens en cause. Par un courrier du 28 août 2015, la présidente de la CNIL l'a informé de la clôture de sa plainte. M. A... demande l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure la société Google de procéder au déréférencement demandé. <br/>
<br/>
              Sur l'office du juge de l'excès de pouvoir :<br/>
<br/>
              2. L'effet utile de l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens vers des pages web réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour la CNIL de procéder à une telle mise en demeure afin que disparaissent de la liste de résultats affichée à la suite d'une recherche les liens en cause.<br/>
<br/>
              3. Il en résulte que lorsqu'il est saisi de conclusions aux fins d'annulation du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              4. L'article 51 de la loi du 6 janvier 1978 dispose, dans sa rédaction applicable à la date de la présente décision, que : " Le droit à l'effacement s'exerce dans les conditions prévues à l'article 17 du règlement (UE) 2016/679 du 27 avril 2016 ". <br/>
<br/>
              5. Aux termes de l'article 17 du règlement 2016/679 du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données et abrogeant la directive 95/46/CE du 24 octobre 1995, dit règlement général sur la protection des données : " 1. La personne concernée a le droit d'obtenir du responsable du traitement l'effacement, dans les meilleurs délais, de données à caractère personnel la concernant et le responsable du traitement a l'obligation d'effacer ces données à caractère personnel dans les meilleurs délais, lorsque l'un des motifs suivants s'applique : / a) les données à caractère personnel ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées ou traitées d'une autre manière ; / b) la personne concernée retire le consentement sur lequel est fondé le traitement, conformément à l'article 6, paragraphe 1, point a), ou à l'article 9, paragraphe 2, point a), et il n'existe pas d'autre fondement juridique au traitement ; / c) la personne concernée s'oppose au traitement en vertu de l'article 21, paragraphe 1, et il n'existe pas de motif légitime impérieux pour le traitement, ou la personne concernée s'oppose au traitement en vertu de l'article 21, paragraphe 2 ; / d) les données à caractère personnel ont fait l'objet d'un traitement illicite ; / e) les données à caractère personnel doivent être effacées pour respecter une obligation légale qui est prévue par le droit de l'Union ou par le droit de l'État membre auquel le responsable du traitement est soumis ; f) les données à caractère personnel ont été collectées dans le cadre de l'offre de services de la société de l'information visée à l'article 8, paragraphe 1. [...] 3. Les paragraphes 1 et 2 ne s'appliquent pas dans la mesure où ce traitement est nécessaire: / a) à l'exercice du droit à la liberté d'expression et d'information [...] ".<br/>
<br/>
              6. Par son arrêt du 24 septembre 2019 GC, AF, BH et ED contre CNIL (C-136/17), la Cour de justice de l'Union européenne a, en réponse aux questions que lui avait posées le Conseil d'Etat dans sa décision avant-dire-droit du 24 février 2017, précisé qu'elle examinerait ces questions " sous l'angle de la directive 95/46, en tenant, toutefois, également compte du règlement 2016/679 dans son analyse de celles-ci, afin d'assurer que ses réponses seront, en toute hypothèse, utiles pour la juridiction de renvoi ".<br/>
<br/>
              En ce qui concerne le " droit au déréférencement " de données à caractère personnel relevant de catégories particulières :<br/>
<br/>
              7. L'article 6 de la loi du 6 janvier 1978 dispose, dans sa rédaction applicable à la date de la présente décision, que : " I.- Il est interdit de traiter des données à caractère personnel qui révèlent la prétendue origine raciale ou l'origine ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale d'une personne physique ou de traiter des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique. / II.- Les exceptions à l'interdiction mentionnée au I sont fixées dans les conditions prévues par le 2 de l'article 9 du règlement (UE) 2016/679 du 27 avril 2016 et par la présente loi ". Ces dispositions assurent la mise en oeuvre en droit national de celles de l'article 9 du règlement général sur la protection des données, lesquelles ont abrogé et remplacé celles de l'article 8 paragraphe 1 de la directive 95/46/CE du 24 octobre 1995. <br/>
<br/>
              8. Aux termes de l'article 9 du règlement général sur la protection des données : " 1. Le traitement des données à caractère personnel qui révèle l'origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques ou l'appartenance syndicale, ainsi que le traitement des données génétiques, des données biométriques aux fins d'identifier une personne physique de manière unique, des données concernant la santé ou des données concernant la vie sexuelle ou l'orientation sexuelle d'une personne physique sont interdits. / 2. Le paragraphe 1 ne s'applique pas si l'une des conditions suivantes est remplie : / a) la personne concernée a donné son consentement explicite au traitement de ces données à caractère personnel pour une ou plusieurs finalités spécifiques, sauf lorsque le droit de l'Union ou le droit de l'État membre prévoit que l'interdiction visée au paragraphe 1 ne peut pas être levée par la personne concernée ; [...] / e) le traitement porte sur des données à caractère personnel qui sont manifestement rendues publiques par la personne concernée ; [...] / g) le traitement est nécessaire pour des motifs d'intérêt public important, sur la base du droit de l'Union ou du droit d'un État membre qui doit être proportionné à l'objectif poursuivi, respecter l'essence du droit à la protection des données et prévoir des mesures appropriées et spécifiques pour la sauvegarde des droits fondamentaux et des intérêts de la personne concernée [...] " .<br/>
<br/>
              9. Par l'arrêt déjà cité du 24 septembre 2019, la Cour de justice de l'Union européenne a dit pour droit que : " 1) Les dispositions de l'article 8, paragraphes 1 et 5, de la directive 95/46/CE du Parlement européen et du Conseil, du 24 octobre 1995, relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données, doivent être interprétées en ce sens que l'interdiction ou les restrictions relatives au traitement des catégories particulières de données à caractère personnel, visées par ces dispositions, s'appliquent, sous réserve des exceptions prévues par cette directive, également à l'exploitant d'un moteur de recherche dans le cadre de ses responsabilités, de ses compétences et de ses possibilités en tant que responsable du traitement effectué lors de l'activité de ce moteur, à l'occasion d'une vérification opérée par cet exploitant, sous le contrôle des autorités nationales compétentes, à la suite d'une demande introduite par la personne concernée ". Elle a également dit pour droit que : " 2) Les dispositions de l'article 8, paragraphes 1 et 5, de la directive 95/46 doivent être interprétées en ce sens que, en vertu de celles-ci, l'exploitant d'un moteur de recherche est en principe obligé, sous réserve des exceptions prévues par cette directive, de faire droit aux demandes de déréférencement portant sur des liens menant vers des pages web sur lesquelles figurent des données à caractère personnel qui relèvent des catégories particulières visées par ces dispositions. /  L'article 8, paragraphe 2, sous e), de la directive 95/46 doit être interprété en ce sens que, en application de celui-ci, un tel exploitant peut refuser de faire droit à une demande de déréférencement lorsqu'il constate que les liens en cause mènent vers des contenus comportant des données à caractère personnel qui relèvent des catégories particulières visées à cet article 8, paragraphe 1, mais dont le traitement est couvert par l'exception prévue audit article 8, paragraphe 2, sous e), à condition que ce traitement réponde à l'ensemble des autres conditions de licéité posées par cette directive et à moins que la personne concernée n'ait, en vertu de l'article 14, premier alinéa, sous a), de ladite directive, le droit de s'opposer audit traitement pour des raisons prépondérantes et légitimes tenant à sa situation particulière. / Les dispositions de la directive 95/46 doivent être interprétées en ce sens que, lorsque l'exploitant d'un moteur de recherche est saisi d'une demande de déréférencement portant sur un lien vers une page web sur laquelle des données à caractère personnel relevant des catégories particulières visées à l'article 8, paragraphe 1 ou 5, de cette directive sont publiées, cet exploitant doit, sur la base de tous les éléments pertinents du cas d'espèce et compte tenu de la gravité de l'ingérence dans les droits fondamentaux de la personne concernée au respect de la vie privée et à la protection des données à caractère personnel, consacrés aux articles 7 et 8 de la Charte des droits fondamentaux de l'Union européenne, vérifier, au titre des motifs d'intérêt public important visés à l'article 8, paragraphe 4, de ladite directive et dans le respect des conditions prévues à cette dernière disposition, si l'inclusion de ce lien dans la liste de résultats, qui est affichée à la suite d'une recherche effectuée à partir du nom de cette personne, s'avère strictement nécessaire pour protéger la liberté d'information des internautes potentiellement intéressés à avoir accès à cette page web au moyen d'une telle recherche, consacrée à l'article 11 de cette charte ". <br/>
<br/>
              10. Il découle de ce qui a été dit ci-dessus que lorsque des liens mènent vers des pages web contenant des données à caractère personnel relevant des catégories particulières visées à l'article 8 paragraphe 1 de la directive 95/46/CE du 24 octobre 1995, abrogé et remplacé par l'article 9 du règlement général sur la protection des données du 27 avril 2016, l'ingérence dans les droits fondamentaux au respect de la vie privée et à la protection des données à caractère personnel de la personne concernée est susceptible d'être particulièrement grave en raison de la sensibilité de ces données. Il s'ensuit qu'il appartient en principe à la CNIL, saisie par une personne d'une demande tendant à ce qu'elle mette l'exploitant d'un moteur de recherche en demeure de procéder au déréférencement de liens renvoyant vers des pages web, publiées par des tiers et contenant des données personnelles relevant de catégories particulières la concernant, de faire droit à cette demande. Il n'en va autrement que s'il apparaît, compte tenu du droit à la liberté d'information, que l'accès à une telle information à partir d'une recherche portant sur le nom de cette personne est strictement nécessaire à l'information du public. Pour apprécier s'il peut être légalement fait échec au droit au déréférencement au motif que l'accès à des données à caractère personnel relevant de catégories particulières à partir d'une recherche portant sur le nom de la personne concernée est strictement nécessaire à l'information du public, il incombe à la CNIL de tenir notamment compte, d'une part, de la nature des données en cause, de leur contenu, de leur caractère plus ou moins objectif, de leur exactitude, de leur source, des conditions et de la date de leur mise en ligne et des répercussions que leur référencement est susceptible d'avoir pour la personne concernée et, d'autre part, de la notoriété de cette personne, de son rôle dans la vie publique et de sa fonction dans la société. Il lui incombe également de prendre en compte la possibilité d'accéder aux mêmes informations à partir d'une recherche portant sur des mots-clés ne mentionnant pas le nom de la personne concernée. <br/>
<br/>
              11. Dans l'hypothèse particulière où les données litigieuses ont manifestement été rendues publiques par la personne qu'elle concerne, il appartient à la CNIL d'apprécier au regard des critères mentionnés au point 10 ci-dessus s'il existe ou non un intérêt prépondérant du public de nature à faire obstacle au droit au déréférencement, une telle circonstance n'empêchant pas l'intéressé de faire valoir, à l'appui de sa demande de déréférencement, des " raisons tenant à sa situation particulière ", ainsi que l'a relevé la Cour de justice de l'Union européenne dans son arrêt précité du 24 septembre 2019. <br/>
<br/>
              Sur la légalité de la décision attaquée : <br/>
<br/>
              12. Il ressort des pièces du dossier que M. A... a demandé à la CNIL d'enjoindre à la société Google de procéder au déréférencement de liens renvoyant vers un article du quotidien Y en date du 9 septembre 2008 et en reprenant le contenu. Cet article fait état d'une note attribuée aux services de renseignement, rédigée à l'occasion d'une information judiciaire consécutive au suicide d'une adepte de " l'Eglise de scientologie ". Le requérant y est cité à raison de fonctions exercées au sein de " l'Eglise de scientologie " et comme étant intervenu, à ce titre, auprès de la famille de la victime. Les circonstances dans lesquelles ses propos ont été recueillis y sont rapportées, notamment l'indication selon laquelle il s'est depuis les faits " mis au vert " et gère " actuellement une entreprise hôtelière ". L'information relative à l'appartenance de M. A... à " l'Eglise de scientologie " constitue une donnée relevant d'une des catégories particulières visées à l'article 9 du règlement général sur la protection des données cité au point 8. Compte tenu des responsabilités autrefois exercées par l'intéressé au sein de " l'Eglise de scientologie ", elle doit être regardée comme ayant été manifestement rendue publique par M. X.<br/>
<br/>
              13. Eu égard à la nature et au contenu des données à caractère personnel litigieuses, au fait non contesté que l'intéressé a quitté ses fonctions au sein de " l'Eglise de scientologie " depuis plus de dix ans à la date de la présente décision et qu'il n'exerce désormais plus d'activité en liaison avec cette organisation, à l'ancienneté des faits, à la circonstance que l'affaire rapportée dans l'article de presse s'est conclue par une ordonnance de non-lieu et aux répercussions qu'est susceptible d'avoir pour l'intéressé le maintien des liens permettant d'y avoir accès à partir d'une recherche effectuée sur son nom, la CNIL n'a pu légalement estimer, alors même que l'information litigieuse provient d'une source journalistique et que son exactitude n'est pas contestée, que le maintien de ces liens présentait un intérêt prépondérant pour le public, alors que, par ailleurs, les internautes intéressés peuvent, dans le cadre d'une recherche effectuée à partir de mots-clés ne mentionnant pas le nom de M. A..., continuer à y accéder.<br/>
<br/>
              14. Il résulte de tout ce qui précède que M. A... est fondé à demander l'annulation de la décision du 28 août 2015.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La décision de la CNIL du 28 août 2015 est annulée. <br/>
Article 2 : La présente décision sera notifiée à M. A..., à la Commission nationale de l'informatique et des libertés et à la société Google. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-05 DROITS CIVILS ET INDIVIDUELS. - DROIT AU DÉRÉFÉRENCEMENT S'AGISSANT DE DONNÉES PERSONNELLES SENSIBLES (ART. 9 DU RGPD) MANIFESTEMENT RENDUES PUBLIQUES PAR L'INTÉRESSÉ [RJ1] - ILLUSTRATION - DONNÉES PRÉSENTANT UN INTÉRÊT PRÉPONDÉRANT POUR LE PUBLIC - ABSENCE [RJ2] - CONSÉQUENCE - ILLÉGALITÉ DU REFUS DE DÉRÉFÉRENCEMENT.
</SCT>
<ANA ID="9A"> 26-07-05 Requérant demandant à la Commission nationale de l'informatique et des libertés (CNIL) d'enjoindre à un exploitant de moteur de recherche de procéder au déréférencement de liens renvoyant vers un article de presse en date du 9 septembre 2008 et en reprenant le contenu. Cet article fait état d'une note attribuée aux services de renseignement, rédigée à l'occasion d'une information judiciaire consécutive au suicide d'une adepte de l'Eglise de scientologie en 2006. Le requérant y est cité en qualité de responsable des relations publiques de l'Eglise de scientologie et comme étant intervenu, à ce titre, auprès de la famille de la victime. Les circonstances dans lesquelles ses propos ont été recueillis y sont rapportées, notamment l'indication selon laquelle il s'est depuis les faits mis au vert et gère actuellement une entreprise hôtelière. L'information relative à l'appartenance du requérant à l'Eglise de scientologie constitue une donnée relevant d'une des catégories particulières visées à l'article 9 du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 du 27 avril 2016 (RGPD). Compte tenu des responsabilités publiques autrefois exercées par l'intéressé au sein de l'Eglise de scientologie, elle doit être regardée comme ayant été manifestement rendue publique par l'intéressé.,,,Eu égard à la nature et au contenu des données à caractère personnel litigieuses, au fait non contesté que l'intéressé a quitté ses fonctions au sein de l'Eglise de scientologie depuis plus de dix ans à la date de la présente décision et qu'il n'exerce désormais plus d'activité en liaison avec cette organisation, à l'ancienneté des faits, à la circonstance que l'affaire rapportée dans l'article de presse s'est conclue par une ordonnance de non-lieu et aux répercussions qu'est susceptible d'avoir pour l'intéressé le maintien des liens permettant d'y avoir accès à partir d'une recherche effectuée sur son nom, la CNIL n'a pu légalement estimer, alors même que l'information litigieuse provient d'une source journalistique et que son exactitude n'est pas contestée, que le maintien de ces liens présentait un intérêt prépondérant pour le public, alors que, par ailleurs, les internautes intéressés peuvent, dans le cadre d'une recherche effectuée à partir de mots-clés ne mentionnant pas le nom du requérant, continuer à y accéder.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la méthode d'appréciation applicable, CE, 6 décembre 2019, Mme X., n° 395335, à publier au Recueil., ,[RJ2] Rappr. CE, 6 décembre 2019, M. X., n° 409212, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
