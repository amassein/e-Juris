<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037492998</ID>
<ANCIEN_ID>JG_L_2018_10_000000411658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/29/CETATEXT000037492998.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/10/2018, 411658, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411658.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1. Sous le n° 411658, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 juin et 21 septembre 2017 et le 21 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'association de défense et de recours des riverains de l'axe R.C.E.A., M. et Mme A...et la communauté de communes de Digoin-Val-de-Loire demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-579 du 20 avril 2017 déclarant d'utilité publique les travaux de mise à 2x2 voies de la route Centre Europe Atlantique (RN79) entre Montmarault (Allier) et Digoin (Saône-et-Loire), conférant le statut autoroutier à cette section de la RN 79 et portant mise en compatibilité des documents d'urbanisme des communes de Besson, Chemilly, Dompierre-sur-Besbre, Molinet et Sazeret dans le département de l'Allier et de la commune de Digoin dans le département de Saône-et-Loire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2. Sous le n° 411684, par une requête sommaire et un mémoire complémentaire, enregistrés les 20 juin et 20 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 200 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Megret, avocat de l'Association de défense et de recours des riverains de l'axe R.C.E.A. et autres, et à la SCP Gadiou, Chevallier, avocat de M. et Mme C...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes n° 411658 et 411684 sont dirigées contre un même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2.	Considérant que le désistement de la communauté de communes de Digoin-Val-de-Loire est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ;<br/>
<br/>
              3.	Considérant que, par le décret attaqué du 20 avril 2017, le Premier ministre a déclaré d'utilité publique les travaux de mise à 2x2 voies de la route Centre Europe Atlantique (RN79) entre Montmarault (Allier) et Digoin (Saône-et-Loire), conféré le statut autoroutier à cette section de la RN79 ainsi qu'à ses voies d'accès directes ; que ce décret emporte mise en compatibilité des documents d'urbanisme des communes de Besson, Chemilly, Dompierre-sur-Besbre, Molinet et Sazeret dans le département de l'Allier et de la commune de Digoin dans le département de Saône-et-Loire ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              En ce qui concerne le débat public :<br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier qu'un  projet de passage au statut autoroutier de la section de la route express Centre Europe Atlantique Montmarault - Chalon-sur-Saône / Mâcon a été soumis au débat public entre le 4 novembre 2010 et le 4 février 2011 ; que l'itinéraire en cause comportait la portion de route express située entre Montmarault et Digoin, d'une longueur de 92 kilomètres, objet du décret contesté, ainsi qu'un autre tronçon situé dans le département de Saône-et-Loire ; que  si le Gouvernement a ultérieurement renoncé à la transformation en autoroute de cet autre tronçon, cette modification n'a pas remis en cause les objectifs ou les caractéristiques essentielles du projet en ce qui concerne le premier tronçon ; que, par suite, les requérants ne sont pas fondés à soutenir qu'un nouveau débat public aurait dû être organisé ; <br/>
<br/>
              5.	Considérant que si l'association requérante soutient que son président a été interrompu lors de l'une de ses interventions au cours du débat public, il est constant que l'association a pu faire valoir, à plusieurs reprises, sa position à propos du projet lors du débat public ; que, par ailleurs, le bilan du débat, qui dresse une liste des contributions ayant fait l'objet d'un document écrit, n'avait pas à rendre compte précisément de la teneur de toutes les contributions ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que l'association requérante n'est pas fondée à soutenir que le projet n'aurait pas été soumis au débat public ou que celui-ci se serait déroulé de manière irrégulière ;<br/>
<br/>
              En ce qui concerne la procédure d'enquête publique :<br/>
<br/>
              7.	Considérant qu'il ressort des pièces du dossier que l'enquête publique s'est déroulée du 25 avril au 17 juin 2016 ; qu'à son issue, la commission d'enquête a émis un avis favorable au projet assorti de recommandations ; <br/>
<br/>
              8.	Considérant que, contrairement à ce que soutient l'association requérante, le projet soumis à enquête, qui concernait la portion de voie entre Montmarault et Digoin, correspond au projet qui a été déclaré d'utilité publique ;<br/>
<br/>
              9.	Considérant que les inexactitudes, omissions ou insuffisances d'une étude d'impact ne sont susceptibles de vicier la procédure et donc d'entraîner l'illégalité de la décision prise au vu de cette étude que si elles ont pu avoir pour effet de nuire à l'information complète de la population ou si elles ont été de nature à exercer une influence sur la décision de l'autorité administrative ;<br/>
<br/>
              10.	Considérant que  le moyen tiré de l'absence de volet agricole dans l'étude d'impact manque en fait ; que les imprécisions invoquées, qui portent sur la localisation exacte de certains péages et les conditions d'aménagements d'échangeurs ou le calendrier précis de réalisation des travaux, ne concernent pas les caractéristiques principales des ouvrages ; que les risques liés à la circulation de matières dangereuses en raison de la réalisation d'un parc de stationnement à proximité de la future autoroute sont la conséquence d'un projet distinct et avaient été portés à la connaissance du public dans le cadre d'une procédure antérieure ; <br/>
<br/>
              11.	Considérant que l'appréciation sommaire des dépenses jointe au dossier d'enquête publique a pour objet de permettre à tous les intéressés de s'assurer que les travaux ou ouvrages, compte tenu de leur coût réel, tel qu'il peut être raisonnablement estimé à l'époque de l'enquête, ont un caractère d'utilité publique ; qu'en l'espèce, le dossier d'enquête comporte une appréciation du coût du projet ; que la circonstance, en l'absence de toute précision sur ce point de la part des requérants, qu'il ne comporterait que des montants de dépense estimés par grande masse ne peut être regardé comme ayant été de nature à nuire, en l'espèce, à l'information du public ; <br/>
<br/>
              En ce qui concerne le moyen tiré de ce que l'étude d'impact du projet sur l'économie agricole n'a pas été jointe au dossier soumis à l'enquête :<br/>
<br/>
              12.	Considérant que l'article L. 112-1-3 du code rural et de la pêche maritime , issu de l'article 28 de la loi du 13 octobre 2014, dispose que : " Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation, sont susceptibles d'avoir des conséquences négatives importantes sur l'économie agricole font l'objet d'une étude préalable comprenant au minimum une description du projet, une analyse de l'état initial de l'économie agricole du territoire concerné, l'étude des effets du projet sur celle-ci, les mesures envisagées pour éviter et réduire les effets négatifs notables du projet ainsi que des mesures de compensation collective visant à consolider l'économie agricole du territoire. L'étude préalable et les mesures de compensation sont prises en charge par le maître d'ouvrage. Un décret détermine les modalités d'application du présent article, en précisant, notamment, les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui doivent faire l'objet d'une étude préalable " ; que le II de l'article 28 de la loi du 13 octobre 2014 précise que ces dispositions entrent en vigueur à une date fixée par décret, et au plus tard le 1er janvier 2016 ; que le législateur n'a, toutefois, pas entendu décider que la loi serait appliquée au cas où le décret nécessaire à l'application de cette disposition législative n'aurait pas été pris dans le délai prévu ; que le décret du 31 août 2016, nécessaire à l'application de la loi énonce que sont soumis à l'exigence de l'enquête préalable qu'il prévoit les projets de travaux, ouvrages ou aménagements publics et privés pour lesquels l'étude d'impact prévue à l'article L. 122-1 du code de l'environnement a été transmise à l'autorité administrative de l'État compétente en matière d'environnement, définie à l'article R. 122-6 du code de l'environnement, à compter du 1er décembre 2016 ;<br/>
<br/>
              13.	Considérant qu'il ressort des pièces du dossier que l'étude d'impact concernant les travaux déclarés d'utilité publique par le décret attaqué a été transmise à l'autorité environnementale compétente avant le 1er décembre 2016 ; qu'ainsi,  les dispositions de l'article L. 112-1-3 du code rural et de la pêche maritime n'étaient pas applicables au projet  ; que, par suite, le moyen tiré de ce que l'étude d'impact du projet sur l'économie agricole n'aurait pas été jointe au dossier d'enquête ne peut qu'être écarté ; <br/>
<br/>
              En ce qui concerne  le moyen tiré de ce que le dossier soumis à l'enquête ne comportait pas l'examen  d'une solution alternative à la concession autoroutière :<br/>
<br/>
              14.	Considérant que l'article R. 122-5 du code de l'environnement dispose que : " En application du 2o du II de l'article L. 122-3, l'étude d'impact comporte les éléments suivants, en fonction des caractéristiques spécifiques du projet et du type d'incidences sur l'environnement qu'il est susceptible de produire : (...) / 7o Une description des solutions de substitution raisonnables qui ont été examinées par le maître d'ouvrage, en fonction du projet proposé et de ses caractéristiques spécifiques, et une indication des principales raisons du choix effectué, notamment une comparaison des incidences sur l'environnement et la santé humaine (...) " ; que cette disposition est issue de l'article 1er du décret du 11 août 2016, pris pour l'application du d) du 2° du II de l'article L. 122-3 du code de l'environnement, dans sa rédaction issue de l'ordonnance du 3 août 2016 ; qu'en vertu des dispositions de  l'article 6 de cette ordonnance, les dispositions de cette dernière s'appliquent, s'agissant des projets pour lesquels l'autorité compétente est le maître d'ouvrage, à ceux dont l'enquête publique est ouverte à compter du 1er février 2017 ; <br/>
<br/>
              15.	Considérant que l'enquête publique préalable à la déclaration d'utilité publique litigieuse a été ouverte le 30 mars 2016 ; qu'il résulte de ce qui vient d'être dit que  les dispositions des articles L. 122-3 et R. 122-5 du code de l'environnement n'étaient pas applicables à ce projet ; que, par suite, le moyen tiré de leur méconnaissance  ne peut qu'être écarté ; <br/>
<br/>
              En ce qui concerne l'avis de la commission d'enquête :<br/>
<br/>
              16.	Considérant que l'absence de visa dans le rapport de la commission d'enquête de l'avis rendu par la chambre d'agriculture est, en tout état de cause, dépourvu d'incidence sur la régularité de l'avis rendu par cette commission ; <br/>
<br/>
              17.	Considérant que, la circonstance que, dans ses conclusions, la commission d'enquête ait fait siennes certaines des observations produites par le bénéficiaire de l'expropriation, n'entache pas son avis d'un défaut de motivation ou d'un manquement à l'obligation d'impartialité, dès lors qu'il ressort des pièces du dossier que la commission a, en l'espèce, formulé un avis propre et circonstancié sur le projet ; <br/>
<br/>
              18.	Considérant que l'existence d'un contentieux ancien et non réglé  entre les membres de l'association requérante et l'Etat, concernant la délimitation de l'emprise routière découlant d'une précédente déclaration d'utilité publique, est sans incidence sur l'appréciation qu'il incombait à la commission de porter sur l'utilité publique du projet ;<br/>
<br/>
              Sur la légalité interne du décret attaqué : <br/>
<br/>
              19.	Considérant que le moyen tiré de ce que le décret serait contraire à une déclaration ministérielle du 11 juillet 2013 manque, en tout état de cause, en fait ; <br/>
<br/>
              20.	Considérant qu'une opération ne peut être légalement déclarée d'utilité publique que si les atteintes à la propriété privée, le coût financier, les inconvénients d'ordre social, la mise en cause de la valorisation de l'environnement et l'atteinte éventuelle à d'autres intérêts publics qu'elle comporte ne sont pas excessifs eu égard à l'intérêt qu'elle présente ;<br/>
<br/>
              21.	Considérant que le projet déclaré d'utilité publique par le décret litigieux s'inscrit dans l'itinéraire dénommé " Route Centre Europe Atlantique ", visant à relier, par une grande liaison transversale, la côte atlantique française à l'Allemagne et l'Italie ; que cet itinéraire  constitue la première liaison est-ouest située au nord du massif central et répond à des objectifs d'aménagement du territoire ; que dans la traversée du département de l'Allier, cet itinéraire supporte un trafic très important, en particulier de poids lourds ; que la mise à 2x2 voies de cette route entre Montmarault et Chalon-sur-Saône avait été inscrite au schéma directeur routier national approuvé le 1er avril 1992 et les travaux nécessaires à cette mise à 2x2 voies déclarés d'utilité publique par trois décrets intervenus entre 1995 et 1997, sur la base desquels les expropriations nécessaires ont été réalisées ; que, toutefois, la réalisation de ces travaux n'a été que très partielle et le secteur se caractérise par un niveau très élevé d'accidents graves ; que le projet de réalisation d'une autoroute à péage a été justifié par la volonté d'améliorer la sécurité des usagers sur ce tronçon, compte tenu des garanties apportées par le standard autoroutier, et de permettre une accélération de la réalisation des travaux par le recours à la concession et aux modalités de financement que celle-ci permet ; que les atteintes au droit de propriété qui en résultent sont limitées, dès lors notamment que les acquisitions foncières nécessaires au passage à 2x2 voies ont été, pour l'essentiel, d'ores et déjà réalisées sur le fondement des précédentes déclarations d'utilité publique ; que les moyens relatifs aux atteintes à la santé et à l'environnement résultant d'une limitation de vitesse plus élevée sur les autoroutes que sur les voies expresses sont énoncées de manière générale sans lien avec le projet ; que l'atteinte portée à l'équilibre économique de l'exploitation agricole de M. et Mme C...ne peut, à elle seule et alors qu'elle devra, le cas échéant, faire l'objet d'une juste compensation lors de la phase ultérieure d'expropriation, retirer son utilité publique au projet ;<br/>
<br/>
              22.	Considérant qu'il en résulte que les moyens invoqués ne permettent pas de remettre en cause l'utilité publique du projet, lequel ne peut être utilement critiqué sur le fondement de l'égalité des citoyens devant la loi ;<br/>
<br/>
              23.	Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner la recevabilité de la requête de M. et MmeC..., que les requérants ne sont pas fondés à demander l'annulation pour excès de pouvoir du décret attaqué ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              24.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il est donné acte du désistement de la communauté de communes de Digoin-Val-de-Loire.  <br/>
<br/>
Article 2 : Les requêtes de l'Association de défense et de recours des riverains de l'axe R.C.E.A. et autres et de M. et Mme C...sont rejetées.<br/>
<br/>
Article  3 : La présente décision sera notifiée à l'association de défense et de recours des riverains de l'axe R.C.E.A, première requérante dénommée dans la requête n° 411658, à <br/>
M. et Mme B...C..., à la communauté de communes de Digoin-Val-de-Loire, au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
