<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258865</ID>
<ANCIEN_ID>JG_L_2019_10_000000426079</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 21/10/2019, 426079, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426079</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426079.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 6 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 26 octobre 2018 rapportant le décret du 12 octobre 2016 lui ayant accordé la nationalité française.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. B..., ressortissant algérien, a déposé une demande de naturalisation le 18 septembre 2015 par laquelle il a indiqué être divorcé. Au vu de ses déclarations, il a été naturalisé par décret du 12 octobre 2016. Postérieurement à l'adoption de ce décret, des éléments nouveaux concernant l'existence d'un mariage contracté en Algérie avant la signature du décret lui conférant la nationalité française, ont été portés à la connaissance du ministre de l'intérieur. Par le décret attaqué, le Premier ministre a rapporté le décret prononçant la naturalisation de M. B..., au motif que l'intéressé avait volontairement dissimulé sa situation familiale alors qu'il était en possession d'un acte de mariage dès le 5 septembre 2016.<br/>
<br/>
              3.	Aux termes de l'article 21-6 du code civil : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette dernière condition se trouve remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé. Par suite, ainsi que l'énonce le décret attaqué, la circonstance que l'intéressé ait dissimulé sa situation familiale était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              4.	En premier lieu, il ressort des pièces du dossier que le décret a été signé par le Premier ministre et contresigné par le ministre de l'intérieur. Le moyen tiré de ce que le décret attaqué aurait été pris par une autorité incompétente doit être écarté.<br/>
<br/>
              5.	En deuxième lieu, il n'est pas contesté que M. B... s'est marié le 4 août 2016 avec une ressortissante algérienne. Ce mariage a constitué un changement de sa situation personnelle et familiale que l'intéressé aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, comme il s'y était engagé en déposant sa demande de naturalisation, ce qu'il n'a pas fait avant que ne lui soit accordée la nationalité française. L'intéressé, qui maîtrise la langue française ainsi qu'il ressort du procès-verbal de l'entretien d'assimilation versé au dossier, ne pouvait se méprendre sur la portée de la déclaration qu'il a signée en déposant sa demande de naturalisation. Il doit donc être regardé comme ayant sciemment dissimulé sa situation familiale avant que soit prononcée sa naturalisation par décret du 12 octobre 2016. La circonstance qu'il a lui-même informé les services instruisant sa demande de naturalisation de l'existence de ce mariage, postérieurement à l'intervention du décret lui accordant la nationalité française, n'est pas de nature à remettre en cause l'appréciation du caractère frauduleux des déclarations faites en vue d'obtenir la nationalité française.  Par ailleurs, s'il soutient avoir attendu, sur les conseils d'un agent de la préfecture, d'obtenir un acte de mariage rectifié pour informer l'administration, il disposait d'un acte de mariage certifié lui permettant d'informer les services préfectoraux dans les délais requis. Par suite, en rapportant la naturalisation de M. B..., le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil.<br/>
<br/>
              6.	En troisième lieu, un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de M. B....<br/>
<br/>
              7.	Il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation du décret qu'il attaque.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
