<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799788</ID>
<ANCIEN_ID>JG_L_2021_07_000000448707</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/07/2021, 448707, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448707</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448707.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 12 novembre 2018 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile et refusé de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire.<br/>
<br/>
              Par une décision n° 18054661, 19009476 du 28 octobre 2020, la Cour nationale du droit d'asile a rejeté la demande de récusation présentée par M. B... sur le fondement de l'article R. 733-27 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              Par une décision n° 18054661 du 19 novembre 2020, la Cour nationale du droit d'asile a rejeté sa demande dirigée contre la décision du 12 novembre 2018 du directeur général de l'OFPRA.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 15 janvier, 13 avril et 15 juin 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ces deux décisions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions présentées devant la Cour nationale du droit d'asile ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 500 euros à verser à la SCP Rocheteau, Uzan-Sarano, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - la directive 2011/95/UE du Parlement européen et du Conseil, du 13 décembre 2011 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B..., et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides,<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 juillet 2021, présentée par M. B... ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 12 novembre 2018, le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé de faire droit à la demande de M. B..., de nationalité afghane, tendant à ce que lui soit reconnu le statut de réfugié ou, à défaut, accordé le bénéfice de la protection subsidiaire. Par une décision du 28 octobre 2020, la Cour nationale du droit d'asile a rejeté la demande présentée par M. B... tendant à la récusation de Mme D... A..., membre de la grande formation de jugement chargée de statuer sur son recours contre cette décision. Par une décision du 19 novembre 2019, la grande formation de la Cour nationale du droit d'asile a rejeté sa demande d'annulation de la décision du 12 novembre 2018 du directeur général de l'OFPRA. M. B... se pourvoit en cassation contre ces décisions. <br/>
<br/>
              Sur la demande de récusation :<br/>
<br/>
              2.	Aux termes des dispositions de l'article R. 733-27 du code de l'entrée et du séjour des étrangers et du droit d'asile, reprises aux articles R. 532-33 à R. 532-36 du code : " Le membre de la formation de jugement qui suppose en sa personne une cause de récusation ou estime en conscience devoir s'abstenir se fait remplacer par un autre membre que désigne le président de la cour. / La partie qui veut récuser un membre d'une formation de jugement doit, à peine d'irrecevabilité, le faire par un acte spécial remis à la cour dès qu'elle a connaissance de la cause de la récusation. Cet acte indique avec précision les motifs de la récusation et est accompagné des pièces propres à la justifier. En aucun cas, la récusation ne peut être formée après la fin de l'audience. / Le membre récusé fait connaître par écrit soit son acquiescement à la récusation, soit les motifs pour lesquels il s'y oppose. / Si le membre de la cour qui est récusé acquiesce à la demande, il est aussitôt remplacé. S'il ne peut être remplacé en temps utile, l'affaire est renvoyée à une audience ultérieure. / Dans le cas où le membre de la cour n'acquiesce pas à la demande de récusation, il est statué, le plus rapidement possible, sur cette demande par une autre formation de jugement. / La décision ne peut être contestée devant le juge de cassation qu'à l'occasion de la décision définitive de la cour ".<br/>
<br/>
              3.	En premier lieu, le premier alinéa de l'article R. 733-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu le premier alinéa de l'article R. 532-16, dispose que " lorsque le requérant est représenté par un avocat les actes de procédure sont accomplis à l'égard de ce mandataire à l'exception (...) de l'avis d'audience prévu à l'article R. 733-19 (...), adressés personnellement au requérant par lettre recommandée avec demande d'avis de réception ". Toutefois, ces dispositions ne sauraient être lues comme imposant, lorsqu'une partie représentée par un avocat a présenté une demande de récusation, qu'une convocation lui soit personnellement adressée en vue de l'audience d'examen de cette demande, sur laquelle, selon les termes mêmes des dispositions précitées de l'article R. 733-27, il doit être statué " le plus rapidement possible ". Par suite, la circonstance que M. B..., à la différence de son avocat, n'aurait pas été convoqué à l'audience n'est pas de nature à entacher d'irrégularité la procédure suivie.<br/>
<br/>
              4.	En second lieu, la circonstance que Mme A... était responsable du centre de recherche et documentation de la Cour nationale du droit d'asile lors de l'élaboration d'une note d'information intitulée " Problématique de la PS c) - Analyse de la notion de violence aveugle - Méthodologie ", diffusée aux membres de la grande formation de jugement ainsi qu'au sein de la Cour et qui donnait des éléments d'éclairage général sur des questions de droit, dont certaines n'étaient pas dénuées de lien avec celles sur lesquelles la Cour pourrait être amenée à se prononcer pour statuer sur la demande de protection présentée par M. B..., n'était pas, par elle-même, susceptible de faire obstacle à ce qu'elle siège au sein de la grande formation chargée d'examiner cette demande. Il en va de même de sa participation antérieure à une formation de jugement ayant eu à connaître de questions similaires à l'occasion de l'examen de la demande d'un autre requérant. Il suit de là qu'en estimant qu'aucun des motifs de récusation invoqués n'était de nature à mettre en doute l'impartialité de Mme A..., la Cour, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de qualification juridique. <br/>
<br/>
              Sur la composition de la formation de jugement :<br/>
<br/>
              5.	Aux termes des dispositions du I. de l'article R. 732-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, reprises à l'article R. 131-7 : " La grande formation de la cour comprend la formation de jugement saisie du recours, complétée par le vice-président ou le plus ancien des vice-présidents ou un président de section ou de chambre, deux assesseurs choisis parmi les personnalités mentionnées au 2° de l'article L. 732-1 et deux assesseurs choisis parmi les personnalités mentionnées au 3° du même article. / (...) Elle est présidée par le président de la cour et, en cas d'empêchement, par le vice-président ou le plus ancien des vice-présidents. / Les membres qui complètent ainsi la formation de jugement saisie du recours sont désignés selon un tableau établi annuellement (...) ".<br/>
<br/>
              6.	La seule circonstance qu'un membre de la formation de jugement saisie initialement du recours soit absent lorsque la grande formation de la Cour se prononce sur ce recours n'est pas de nature à entacher d'irrégularité la composition de celle-ci. Par suite, en jugeant que sa composition n'était pas irrégulière du seul fait que tous les membres de la formation de jugement saisie du recours n'avaient pu siéger, la grande formation de la Cour nationale du droit d'asile n'a pas commis d'erreur de droit. Pour le même motif, elle n'a pas davantage statué irrégulièrement.<br/>
<br/>
              Sur la reconnaissance de la qualité de réfugié :<br/>
<br/>
              7.	Aux termes du 2° du paragraphe A de l'article 1er de la convention de Genève du 28 juillet 1951, la qualité de réfugié est reconnue à " toute personne qui (...), craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ".<br/>
<br/>
              8.	Il ressort des pièces soumis aux juges du fond que pour solliciter son admission au bénéfice de l'asile, M. B... soutient qu'il craint d'être persécuté en cas de retour dans son pays d'origine en raison notamment de son appartenance à l'ethnie hazâra. Il ressort des énonciations de la décision attaquée que, pour refuser de lui accorder le statut de réfugié, la Cour nationale du droit d'asile, après avoir tenue pour établie d'une part, son origine hazâra et, d'autre part, la persistance d'actes de violences et de harcèlement à l'égard de cette communauté en Afghanistan, s'est fondée sur la circonstance que les sources publiques disponibles sur les traitements infligés à cette minorité, tels les rapports de la mission d'assistance des Nations Unies en Afghanistan, et les déclarations du requérant, par leur caractère sommaire et peu personnalisé, y compris pour ce qui concerne ses activités en Syrie, ne permettaient pas de confirmer la réalité des craintes personnelles alléguées en lien avec son origine hazâra. Ce faisant, la Cour a porté sur les faits qui lui étaient soumis des appréciations souveraines exemptes de dénaturation et n'a ni méconnu les règles de dévolution de la charge de la preuve ni commis d'erreur de droit.<br/>
<br/>
              Sur le bénéfice de la protection subsidiaire :<br/>
<br/>
              9.	Aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu l'article L. 512-1 : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes :/ (...) c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ". <br/>
<br/>
              10.	Il résulte de ces dispositions que l'existence d'une menace grave, directe et individuelle contre la vie ou la personne d'un demandeur de la protection subsidiaire n'est pas subordonnée à la condition qu'il rapporte la preuve qu'il est visé spécifiquement en raison d'éléments propres à sa situation personnelle dès lors que le degré de violence généralisée caractérisant le conflit armé atteint un niveau si élevé qu'il existe des motifs sérieux et avérés de croire qu'un civil renvoyé dans le pays ou la région concernés courrait, du seul fait de sa présence sur le territoire, un risque réel de subir ces menaces. Le bénéfice de la protection subsidiaire peut aussi résulter, dans le cas où la région que l'intéressé a vocation à rejoindre ne connaît pas une telle violence, de la circonstance qu'il ne peut s'y rendre sans nécessairement traverser une zone au sein de laquelle le degré de violence résultant de la situation de conflit armé est tel qu'il existe des motifs sérieux et avérés de croire que l'intéressé se trouverait exposé, du seul fait de son passage, même temporaire, dans la zone en cause, à une menace grave et individuelle contre sa vie ou sa personne. <br/>
<br/>
              11.	Par ailleurs, il résulte de ces mêmes dispositions, qui assurent la transposition de l'article 15, sous c), de la directive 2011/95/UE du Parlement européen et du Conseil, du 13 décembre 2011, concernant les normes relatives aux conditions que doivent remplir les ressortissants des pays tiers ou les apatrides pour pouvoir bénéficier d'une protection internationale, à un statut uniforme pour les réfugiés ou les personnes pouvant bénéficier de la protection subsidiaire, et au contenu de cette protection, tel qu'interprété par l'arrêt de la Cour de justice de l'Union européenne du 10 juin 2021, CF, DN c/ Bundesrepublik Deutschland (C-901/19), que la constatation de l'existence d'une telle menace ne saurait être subordonnée à la condition que le rapport entre le nombre de victimes dans la zone concernée et le nombre total d'individus que compte la population de cette zone atteint un seuil déterminé mais exige une prise en compte globale de toutes les circonstances du cas d'espèce, notamment de celles qui caractérisent la situation du pays d'origine du demandeur, par exemple, outre des critères quantitatifs relatifs au nombre de victimes, l'intensité des affrontements armés, le niveau d'organisation des forces armées en présence, la durée du conflit, l'étendue géographique de la situation de violence, ou l'agression éventuellement intentionnelle contre des civils exercée par les belligérants. <br/>
<br/>
              12.	Il ressort des énonciations de la décision attaquée et des pièces de la procédure que pour analyser la situation sécuritaire en Afghanistan, à Kaboul, zone par laquelle M. B... doit transiter, et dans la province d'Hérat, région qu'il a vocation à rejoindre, la Cour s'est fondée sur des rapports et documents librement accessibles au public, émanant notamment d'organisations et missions des Nations Unies, du Bureau européen d'appui en matière d'asile et d'organisations non gouvernementales. S'agissant de la situation prévalant à la date de sa décision en Afghanistan, elle a souligné la baisse relative du nombre de victimes et d'incidents sécuritaires, tout en admettant le caractère volatile de la situation, et le retour dans le pays de nombreuses personnes. Elle a également relevé que dans certaines provinces, il n'était pas observé de combats ouverts ou d'affrontements persistants ou ininterrompus, mais des incidents dont l'ampleur et l'intensité de la violence sont largement moindres que dans les provinces où se déroulent des combats ouverts, que la typologie et l'ampleur des violences dans les villes était différente de celle observée dans les zones rurales et que, plus généralement, la situation sécuritaire était marquée par de fortes différences régionales en termes de niveau ou d'étendue de la violence et d'impact du conflit sévissant dans ce pays. En ce qui concerne la situation à Kaboul, la Cour a notamment constaté la forte croissance démographique et urbaine de cette ville, l'absence de combats ouverts et la circonstance que les civils ne constituent pas les principales cibles des groupes insurgés à Kaboul, ainsi que la diminution relative du nombre de victimes et le flux de personnes venant s'y réfugier. Quant à la situation à Hérat, si la Cour a admis le caractère hautement stratégique de cette province, elle a pris en compte le nombre de victimes, d'incidents et de personnes déplacées ou de retour dans la province ainsi que les méthodes employées, l'intensité des combats et les cibles privilégiées. En jugeant, au vu des appréciations souveraines ainsi portées et exemptes de dénaturation, qu'à la date de sa décision, si la situation sécuritaire prévalant dans ces différentes zones se traduisait par un niveau significatif de violence, elle ne se caractérisait pas par un niveau de violence susceptible de s'étendre à des personnes sans considération de leur situation personnelle de nature à permettre de bénéficier de l'application des dispositions du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, la Cour n'a pas inexactement qualifié les faits de l'espèce. <br/>
<br/>
              13.	Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation des décisions attaquées.<br/>
<br/>
              14.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. C... B... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-01-03-02-03 - APPRÉCIATION [RJ1] - ESPÈCE - SITUATION EN AFGHANISTAN, À KABOUL, ZONE PAR LAQUELLE DOIT TRANSITER LE DEMANDEUR [RJ2], ET DANS LA PROVINCE D'HÉRAT, QU'IL A VOCATION À REJOINDRE - NIVEAU DE VIOLENCE SUSCEPTIBLE DE S'ÉTENDRE À DES PERSONNES SANS CONSIDÉRATION DE LEUR SITUATION PERSONNELLE DE NATURE À PERMETTRE L'OCTROI DE LA PROTECTION SUBSIDIAIRE - ABSENCE [RJ4].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-03-02 - 1) OBLIGATION D'ADRESSER UNE CONVOCATION AU DEMANDEUR REPRÉSENTÉ PAR UN AVOCAT - ABSENCE - 2) MOTIFS DE RÉCUSATION - A) JUGE ÉTANT RESPONSABLE DU CEREDOC LORS DE L'ÉLABORATION D'UNE NOTE D'INFORMATION PORTANT SUR DES QUESTIONS DE DROIT NON DÉNUÉES DE LIEN AVEC L'AFFAIRE - ABSENCE - B) JUGE AYANT EU À CONNAÎTRE DE QUESTIONS SIMILAIRES À L'OCCASION D'UNE AUTRE AFFAIRE - ABSENCE - 3) CONTRÔLE DU JUGE DE CASSATION SUR L'EXISTENCE DE TELS MOTIFS - QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-08-06-01 - CONTRÔLE DU JUGE DE CASSATION SUR L'EXISTENCE DE MOTIFS DE RÉCUSATION - QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">095-08-06-01 - CONTRÔLE DU JUGE DE CASSATION SUR LE NIVEAU DE VIOLENCE SUSCEPTIBLE DE S'ÉTENDRE À DES PERSONNES SANS CONSIDÉRATION DE LEUR SITUATION PERSONNELLE DE NATURE À PERMETTRE L'OCTROI DE LA PROTECTION SUBSIDIAIRE (ART. L. 712-1, C) DU CESEDA) - QUALIFICATION JURIDIQUE [RJ3].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - EXISTENCE DE MOTIFS DE RÉCUSATION.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - NIVEAU DE VIOLENCE SUSCEPTIBLE DE S'ÉTENDRE À DES PERSONNES SANS CONSIDÉRATION DE LEUR SITUATION PERSONNELLE DE NATURE À PERMETTRE L'OCTROI DE LA PROTECTION SUBSIDIAIRE (ART. L. 712-1, C) DU CESEDA) [RJ3].
</SCT>
<ANA ID="9A"> 095-03-01-03-02-03 Pour analyser la situation sécuritaire en Afghanistan, à Kaboul, zone par laquelle le demandeur doit transiter, et dans la province d'Hérat, région qu'il a vocation à rejoindre, la Cour nationale du droit d'asile (CNDA) s'est fondée sur des rapports et documents librement accessibles au public, émanant notamment d'organisations et missions des Nations Unies, du Bureau européen d'appui en matière d'asile et d'organisations non gouvernementales.... ,,S'agissant de la situation prévalant à la date de sa décision en Afghanistan, elle a souligné la baisse relative du nombre de victimes et d'incidents sécuritaires, tout en admettant le caractère volatile de la situation, et le retour dans le pays de nombreuses personnes. Elle a également relevé que dans certaines provinces, il n'était pas observé de combats ouverts ou d'affrontements persistants ou ininterrompus, mais des incidents dont l'ampleur et l'intensité de la violence sont largement moindres que dans les provinces où se déroulent des combats ouverts, que la typologie et l'ampleur des violences dans les villes était différente de celle observée dans les zones rurales et que, plus généralement, la situation sécuritaire était marquée par de fortes différences régionales en termes de niveau ou d'étendue de la violence et d'impact du conflit sévissant dans ce pays.... ,,En ce qui concerne la situation à Kaboul, la CNDA a notamment constaté la forte croissance démographique et urbaine de cette ville, l'absence de combats ouverts et la circonstance que les civils ne constituent pas les principales cibles des groupes insurgés à Kaboul, ainsi que la diminution relative du nombre de victimes et le flux de personnes venant s'y réfugier. Quant à la situation à Hérat, si la Cour a admis le caractère hautement stratégique de cette province, elle a pris en compte le nombre de victimes, d'incidents et de personnes déplacées ou de retour dans la province ainsi que les méthodes employées, l'intensité des combats et les cibles privilégiées.... ,,En jugeant, au vu des appréciations souveraines ainsi portées et exemptes de dénaturation, qu'à la date de sa décision, si la situation sécuritaire prévalant dans ces différentes zones se traduisait par un niveau significatif de violence, elle ne se caractérisait pas par un niveau de violence susceptible de s'étendre à des personnes sans considération de leur situation personnelle de nature à permettre de bénéficier de l'application du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), la CNDA n'a pas inexactement qualifié les faits de l'espèce.</ANA>
<ANA ID="9B"> 095-08-03-02 1) Le premier alinéa de l'article R. 733-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), devenu le premier alinéa de l'article R. 532-16, ne saurait être lu comme imposant, lorsqu'une partie représentée par un avocat a présenté une demande de récusation, qu'une convocation lui soit personnellement adressée en vue de l'audience d'examen de cette demande, sur laquelle, selon les termes mêmes de l'article R. 733-27, il doit être statué le plus rapidement possible. Par suite, la circonstance qu'un requérant, à la différence de son avocat, n'aurait pas été convoqué à l'audience n'est pas de nature à entacher d'irrégularité la procédure suivie.,,,2) a) La circonstance que l'un des membres de la formation de jugement était responsable du centre de recherche et documentation (Ceredoc) de la Cour nationale du droit d'asile (CNDA) lors de l'élaboration d'une note d'information diffusée aux membres de la grande formation de jugement ainsi qu'au sein de la Cour et qui donnait des éléments d'éclairage général sur des questions de droit, dont certaines n'étaient pas dénuées de lien avec celles sur lesquelles la Cour pourrait être amenée à se prononcer pour statuer sur la demande de protection présentée par l'auteur de la demande de récusation, n'est pas, par elle-même, susceptible de faire obstacle à ce que ce membre siège au sein de la grande formation chargée d'examiner cette demande de protection.... ,,b) Il en va de même de sa participation antérieure à une formation de jugement ayant eu à connaître de questions similaires à l'occasion de l'examen de la demande d'un autre requérant.,,,3) Le juge de cassation exerce un contrôle de qualification juridique sur l'existence de motifs de récusation de nature à mettre en doute l'impartialité du juge concerné.</ANA>
<ANA ID="9C"> 095-08-06-01 Le juge de cassation exerce un contrôle de qualification juridique sur l'existence de motifs de récusation de nature à mettre en doute l'impartialité du juge concerné.</ANA>
<ANA ID="9D"> 095-08-06-01 Le juge de cassation exerce un contrôle de qualification juridique sur l'existence d'un niveau de violence susceptible de s'étendre à des personnes sans considération de leur situation personnelle de nature à permettre l'octroi de la protection subsidiaire en application du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
<ANA ID="9E"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique sur l'existence de motifs de récusation de nature à mettre en doute l'impartialité du juge concerné.</ANA>
<ANA ID="9F"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique sur l'existence d'un niveau de violence susceptible de s'étendre à des personnes sans considération de leur situation personnelle de nature à permettre l'octroi de la protection subsidiaire en application du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des principes de cette appréciation, CE, 3 juillet 2009, Office français de protection des réfugiés et apatrides c/,, n° 320295, T. p. 788. Rappr. CJUE, 10 juin 2021, CF et DN c/ Bundesrepublik Deutschland, aff. C-901/19., ,[RJ2] Cf., s'agissant de la prise en compte des zones que devrait traverser le demandeur, CE, 16 octobre 2017, Office français de protection des réfugiés et apatrides, n° 401585, T. pp. 474-635.,,[RJ3] Comp., s'agissant du contrôle du juge de cassation sur l'existence d'un climat de violence généralisé, CE, 15 mai 2009, Melle,, n° 292564, T. pp. 790-924.,,[RJ4] Rappr. CEDH, 25 février 2020, ASN et autres c/ Pays-Bas, n° 68377/17 et 530/18.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
