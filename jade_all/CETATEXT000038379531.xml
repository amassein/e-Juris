<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038379531</ID>
<ANCIEN_ID>JG_L_2019_04_000000424361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/37/95/CETATEXT000038379531.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 15/04/2019, 424361</TITRE>
<DATE_DEC>2019-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; BOUTHORS</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424361.20190415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Cabinet de la Grand-Place a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 27 juin 2018 par laquelle le conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes a " refusé d'entériner " les modifications apportées à ses statuts. Par une ordonnance n° 1808845 du 5 septembre 2018, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, un mémoire en réplique et un nouveau mémoire, enregistrés les 20 septembre et 28 novembre 2018 et le 11 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la société Cabinet de la Grand-Place demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, d'enjoindre au conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes de reconnaître à M. A...la qualité d'associé professionnel exerçant et de président de la société Cabinet de la Grand-Place, dans un délai de 15 jours à compter de la décision à intervenir sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bouthors, avocat du Cabinet De La Grand-place et à la SCP Lyon-Caen, Thiriez, avocat du Conseil départemental de l'ordre des chirurgiens-dentistes des Hauts-de-Seine ;<br/>
<br/>
<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis au juge des référés que, saisi par la société Cabinet de la Grand-Place, société d'exercice libéral de chirurgiens-dentistes, d'une modification statutaire portant sur la répartition de son capital social et l'intégration d'un nouvel associé et l'informant également de l'élection d'un nouveau président, le conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes a, par une décision du 27 juin 2018, " refusé d'entériner " cette modification statutaire. La société Cabinet de la Grand-Place se pourvoit en cassation contre l'ordonnance du 5 septembre 2018 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la suspension de l'exécution de cette décision. <br/>
<br/>
              2. Aux termes de l'article R. 4113-4 du code de la santé publique, relatif aux sociétés d'exercice libéral dont l'objet social est l'exercice en commun de la profession de médecin, de chirurgien-dentiste ou de sage-femme : " La société est constituée sous la condition suspensive de son inscription au tableau de l'ordre. / La demande d'inscription de la société d'exercice libéral est présentée collectivement par les associés et adressée au conseil départemental de l'ordre du siège de la société par lettre recommandée avec demande d'avis de réception, accompagnée, sous peine d'irrecevabilité, des pièces suivantes : / 1° Un exemplaire des statuts et, s'il en a été établi, du règlement intérieur de la société ainsi que, le cas échéant, une expédition ou une copie de l'acte constitutif ; (...) 4° Une attestation des associés indiquant : a) La nature et l'évaluation distincte de chacun des apports effectués par les associés ; b) Le montant du capital social, le nombre, le montant nominal et la répartition des parts sociales ou actions représentatives de ce capital ; (...) / L'inscription ne peut être refusée que si les statuts ne sont pas conformes aux dispositions législatives et réglementaires en vigueur. (...) Toute modification des statuts et des éléments figurant au 4° ci-dessus est transmise au conseil départemental de l'ordre dans les formes mentionnées au présent article ".<br/>
<br/>
              3. Il résulte de ces dispositions que le conseil départemental de l'ordre des chirurgiens-dentistes, qui doit refuser l'inscription au tableau d'une société d'exercice libéral de chirurgiens-dentistes dont les statuts ne seraient pas conformes aux dispositions législatives et réglementaires, doit procéder au même examen lorsque lui est transmise une modification des statuts d'une société inscrite au tableau de l'ordre. S'il estime que cette modification n'est pas conforme aux dispositions législatives et réglementaires, il lui appartient de mettre en demeure la société de se conformer à ces dispositions et, si elle ne le fait pas, de la radier du tableau. Par suite, la décision par laquelle un conseil départemental de l'ordre se prononce sur la conformité d'une modification des statuts d'une société d'exercice libéral aux dispositions législatives et réglementaires a la nature d'une décision prise pour l'inscription au tableau.<br/>
<br/>
              4. Par ailleurs, il résulte des dispositions des articles L. 4112-4, R. 4112-5 et R. 4112-5-1 du code de la santé publique, d'une part, qu'un refus d'inscription au tableau de l'ordre des chirurgiens-dentistes décidé par un conseil départemental de cet ordre doit, préalablement à l'exercice d'un recours contentieux, faire l'objet d'un recours administratif devant le conseil régional puis, au besoin, devant le conseil national et, d'autre part, qu'un recours pour excès de pouvoir contre la décision du conseil national relève de la compétence de premier et dernier ressort du Conseil d'Etat. Dès lors, le Conseil d'Etat peut être saisi, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une demande tendant à ce que l'exécution du refus d'inscription soit suspendue, alors même qu'il n'aurait pas encore été statué sur le recours administratif, sous réserve que le conseil régional soit saisi d'un tel recours, ou, s'il a statué, que sa décision ait été contestée devant le Conseil national. Lorsqu'intervient la décision du Conseil national, il appartient au requérant de présenter contre cette dernière décision, d'une part de nouvelles conclusions tendant à sa suspension, d'autre part une requête tendant à son annulation.<br/>
<br/>
              5. Il résulte de ce qui précède que les conclusions par lesquelles la société Cabinet de la Grand-Place a demandé au juge des référés du tribunal administratif de Cergy-Pontoise de suspendre l'exécution de la décision du 28 juin 2018 du conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes relevaient de la compétence du juge des référés du Conseil d'Etat. Sans qu'il soit besoin d'examiner les moyens du pourvoi, il y a lieu d'annuler l'ordonnance attaquée et de statuer sur la demande de suspension présentée par la société Cabinet de la Grand-Place.<br/>
<br/>
              6. Il résulte de l'instruction que, par une décision du 13 décembre 2018, le Conseil national de l'ordre des chirurgiens-dentistes a rejeté le recours administratif introduit par la société Cabinet de la Grand-Place contre la décision du 28 juin 2018 du conseil départemental. Il résulte également de l'instruction que la société requérante a introduit, devant le tribunal administratif de Paris, une demande tendant à l'annulation pour excès de pouvoir de cette décision.<br/>
<br/>
              7. Par suite, dès lors que, dans le dernier état de ses conclusions, la société Cabinet de la Grand-Place demande au Conseil d'Etat de suspendre l'exécution de la décision du 13 décembre 2018, et alors même que la requête dirigée contre cette décision a été portée, à tort, devant le tribunal administratif de Paris, il y a lieu de surseoir à statuer et de rouvrir l'instruction, afin de permettre un débat contradictoire sur la procédure de référé engagée, laquelle, contrairement à ce que soutiennent le conseil départemental des Hauts-de-Seine et le Conseil national de l'ordre des chirurgiens-dentistes, n'a pas perdu son objet.<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental des Hauts-de-Seine de l'ordre des médecins la somme que demande la société Cabinet de la Grand-Place au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas davantage lieu de faire droit aux conclusions présentées au titre des mêmes dispositions par le conseil départemental des Hauts-de-Seine de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 5 septembre 2018 est annulée.<br/>
Article 2 : Il est sursis à statuer sur la demande présentée par la société Cabinet de la Grand-Place, au titre de l'article L. 521-1 du code de justice administrative, devant le juge des référés du tribunal administratif de Cergy-Pontoise<br/>
Article 3 : Le surplus des conclusions présenté par la société Cabinet de la Grand-Place et les conclusions présentées par le conseil départemental des Hauts-de-Seine de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Cabinet de la Grand-Place, au conseil départemental des Hauts-de-Seine de l'ordre des chirurgiens-dentistes et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02-07 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. DÉCISIONS ADMINISTRATIVES DES ORGANISMES COLLÉGIAUX À COMPÉTENCE NATIONALE. - DÉCISION DU CONSEIL NATIONAL DE L'ORDRE DES CHIRURGIENS-DENTISTES DE REFUS D'INSCRIPTION AU TABLEAU DE CET ORDRE - INCLUSION [RJ2] - POSSIBILITÉ D'INTRODUIRE DEVANT LE CONSEIL D'ETAT UNE DEMANDE TENDANT À LA SUSPENSION DE CE REFUS (ART. L. 521-1 DU CJA), NONOBSTANT L'EXISTENCE D'UN RECOURS ADMINISTRATIF PENDANT [RJ3] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - REFUS D'INSCRIPTION AU TABLEAU DE L'ORDRE DES CHIRURGIENS-DENTISTES DÉCIDÉ PAR UN CONSEIL DÉPARTEMENTAL DE CET ORDRE - 1) EXIGENCE D'UN DOUBLE RAPO DEVANT LE CONSEIL RÉGIONAL PUIS DEVANT LE CONSEIL NATIONAL [RJ1] - 2) COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT POUR CONNAÎTRE D'UN REP CONTRE LA DÉCISION DU CONSEIL NATIONAL PRISE SUR RAPO [RJ2] - 3) A) COMPÉTENCE DU CONSEIL D'ETAT POUR STATUER SUR UNE DEMANDE TENDANT À LA SUSPENSION DU REFUS D'INSCRIPTION, (ART. L. 521-1 DU CJA), NONOBSTANT L'EXISTENCE D'UN RECOURS ADMINISTRATIF PENDANT [RJ3] - B) EXIGENCE DE RÉGULARISATION DES REQUÊTES UNE FOIS INTERVENUE LA DÉCISION DU CONSEIL NATIONAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-01-02-015 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES CHIRURGIENS-DENTISTES. - REFUS D'INSCRIPTION AU TABLEAU DE L'ORDRE DES CHIRURGIENS-DENTISTES DÉCIDÉ PAR UN CONSEIL DÉPARTEMENTAL DE CET ORDRE - 1) EXIGENCE D'UN DOUBLE RAPO DEVANT LE CONSEIL RÉGIONAL PUIS DEVANT LE CONSEIL NATIONAL [RJ1] - 2) COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT POUR CONNAÎTRE D'UN REP CONTRE LA DÉCISION DU CONSEIL NATIONAL PRISE SUR RAPO [RJ2] - 3) A) COMPÉTENCE DU CONSEIL D'ETAT POUR STATUER SUR UNE DEMANDE TENDANT À LA SUSPENSION DU REFUS D'INSCRIPTION, (ART. L. 521-1 DU CJA), NONOBSTANT L'EXISTENCE D'UN RECOURS ADMINISTRATIF PENDANT [RJ3] - B) EXIGENCE DE RÉGULARISATION DES REQUÊTES UNE FOIS INTERVENUE LA DÉCISION DU CONSEIL NATIONAL.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-02-02 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. CHIRURGIENS-DENTISTES. - REFUS D'INSCRIPTION AU TABLEAU DE L'ORDRE DES CHIRURGIENS-DENTISTES DÉCIDÉ PAR UN CONSEIL DÉPARTEMENTAL DE CET ORDRE - 1) EXIGENCE D'UN DOUBLE RAPO DEVANT LE CONSEIL RÉGIONAL PUIS DEVANT LE CONSEIL NATIONAL [RJ1] - 2) COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT POUR CONNAÎTRE D'UN REP CONTRE LA DÉCISION DU CONSEIL NATIONAL PRISE SUR RAPO [RJ2] - 3) A) COMPÉTENCE DU CONSEIL D'ETAT POUR STATUER SUR UNE DEMANDE TENDANT À LA SUSPENSION DU REFUS D'INSCRIPTION, (ART. L. 521-1 DU CJA), NONOBSTANT L'EXISTENCE D'UN RECOURS ADMINISTRATIF PENDANT [RJ3] - B) EXIGENCE DE RÉGULARISATION DES REQUÊTES UNE FOIS INTERVENUE LA DÉCISION DU CONSEIL NATIONAL.
</SCT>
<ANA ID="9A"> 17-05-02-07 Il résulte des articles L. 4112-4, R. 4112-5 et R. 4112-5-1 du code de la santé publique (CSP) qu'un recours pour excès de pouvoir contre la décision du conseil national relève de la compétence de premier et dernier ressort du Conseil d'Etat. Dès lors, le Conseil d'Etat peut être saisi, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une demande tendant à ce que l'exécution du refus d'inscription soit suspendue, alors même qu'il n'aurait pas encore été statué sur le recours administratif, sous réserve que le conseil régional soit saisi d'un tel recours, ou, s'il a statué, que sa décision ait été contestée devant le Conseil national. Lorsqu'intervient la décision du Conseil national, il appartient au requérant de présenter contre cette dernière décision, d'une part de nouvelles conclusions tendant à sa suspension, d'autre part une requête tendant à son annulation.</ANA>
<ANA ID="9B"> 54-01-02-01 1) Il résulte des articles L. 4112-4, R. 4112-5 et R. 4112-5-1 du code de la santé publique (CSP) qu'un refus d'inscription au tableau de l'ordre des chirurgiens-dentistes décidé par un conseil départemental de cet ordre doit, préalablement à l'exercice d'un recours contentieux, faire l'objet d'un recours administratif devant le conseil régional puis, au besoin, devant le conseil national... ,,2) Il résulte également de ces articles qu'un recours pour excès de pouvoir contre la décision du conseil national relève de la compétence de premier et dernier ressort du Conseil d'Etat.... ,,3) a) Dès lors, le Conseil d'Etat peut être saisi, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une demande tendant à ce que l'exécution du refus d'inscription soit suspendue, alors même qu'il n'aurait pas encore été statué sur le recours administratif, sous réserve que le conseil régional soit saisi d'un tel recours, ou, s'il a statué, que sa décision ait été contestée devant le Conseil national.... ,,b) Lorsqu'intervient la décision du Conseil national, il appartient au requérant de présenter contre cette dernière décision, d'une part de nouvelles conclusions tendant à sa suspension, d'autre part une requête tendant à son annulation.</ANA>
<ANA ID="9C"> 55-01-02-015 1) Il résulte des articles L. 4112-4, R. 4112-5 et R. 4112-5-1 du code de la santé publique (CSP) qu'un refus d'inscription au tableau de l'ordre des chirurgiens-dentistes décidé par un conseil départemental de cet ordre doit, préalablement à l'exercice d'un recours contentieux, faire l'objet d'un recours administratif devant le conseil régional puis, au besoin, devant le conseil national... ,,2) Il résulte également de ces articles qu'un recours pour excès de pouvoir contre la décision du conseil national relève de la compétence de premier et dernier ressort du Conseil d'Etat.... ,,3) a) Dès lors, le Conseil d'Etat peut être saisi, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une demande tendant à ce que l'exécution du refus d'inscription soit suspendue, alors même qu'il n'aurait pas encore été statué sur le recours administratif, sous réserve que le conseil régional soit saisi d'un tel recours, ou, s'il a statué, que sa décision ait été contestée devant le Conseil national.... ,,b) Lorsqu'intervient la décision du Conseil national, il appartient au requérant de présenter contre cette dernière décision, d'une part de nouvelles conclusions tendant à sa suspension, d'autre part une requête tendant à son annulation.</ANA>
<ANA ID="9D"> 55-02-02 1) Il résulte des articles L. 4112-4, R. 4112-5 et R. 4112-5-1 du code de la santé publique (CSP) qu'un refus d'inscription au tableau de l'ordre des chirurgiens-dentistes décidé par un conseil départemental de cet ordre doit, préalablement à l'exercice d'un recours contentieux, faire l'objet d'un recours administratif devant le conseil régional puis, au besoin, devant le conseil national... ,,2) Il résulte également de ces articles qu'un recours pour excès de pouvoir contre la décision du conseil national relève de la compétence de premier et dernier ressort du Conseil d'Etat.... ,,3) a) Dès lors, le Conseil d'Etat peut être saisi, sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), d'une demande tendant à ce que l'exécution du refus d'inscription soit suspendue, alors même qu'il n'aurait pas encore été statué sur le recours administratif, sous réserve que le conseil régional soit saisi d'un tel recours, ou, s'il a statué, que sa décision ait été contestée devant le Conseil national.... ,,b) Lorsqu'intervient la décision du Conseil national, il appartient au requérant de présenter contre cette dernière décision, d'une part de nouvelles conclusions tendant à sa suspension, d'autre part une requête tendant à son annulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, 6 juin 2001,,, n° 202920, T. p. 1166.,,[RJ2]. Cf., CE, 23 mars 2011,,, n° 339086, T. pp. 853-1125.,,[RJ3] Rappr., dans l'hypothèse d'une RAPO simple, CE, Section, 12 octobre 2001, Société Produits Roche, n° 237376, p. 463 ; Cf. CE, 28 février 2019, M.,, n° 426952, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
