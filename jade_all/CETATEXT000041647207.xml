<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041647207</ID>
<ANCIEN_ID>JG_L_2020_02_000000436428</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/64/72/CETATEXT000041647207.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/02/2020, 436428</TITRE>
<DATE_DEC>2020-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436428</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436428.20200226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société JCDecaux France a demandé au juge des référés du tribunal administratif de Grenoble, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, premièrement, d'enjoindre à la commune de Saint-Julien-en-Genevois de lui communiquer les notes respectivement obtenues par l'offre de la société JCDecaux France et l'offre de l'attributaire dans le cadre de la passation par la commune d'une concession de services relative aux mobiliers urbains, deuxièmement, d'annuler la procédure de passation de cette concession et, à tout le moins, d'annuler les décisions de rejet de l'offre de la société JCDecaux France et d'attribution de la concession à la société Girod Médias. <br/>
<br/>
              Par une ordonnance n° 1907101 du 15 novembre 2019, le juge des référés du tribunal administratif de Grenoble a annulé la procédure de passation de la concession et enjoint à la commune de Saint-Julien-en-Genevois, si elle entend poursuivre son projet de concession de services, de reprendre la procédure de passation, au stade de l'analyse des candidatures, en se conformant à ses obligations de publicité et de mise en concurrence.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 et 19 décembre 2019 et le 6 février 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Julien-en-Genevois demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société JCDecaux France ;<br/>
<br/>
              3°) de mettre à la charge de la société JCDecaux France la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2014/23/UE du parlement européen et du conseil du 26 février 2014 ;<br/>
              - le règlement d'exécution (UE) n° 2015/1986 de la Commission du 11 novembre 2015 ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - l'ordonnance n° 2016-65 du 29 janvier 2016 relative aux contrats de concession ;<br/>
              - le décret n° 2016-86 du 1er février 2016 relatif aux contrats de concession ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la commune de Saint-Julien-en-Genevois et à la SCP Lyon-Caen, Thiriez, avocat de la société JCDecaux France ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 février 2020, présentée par la société JCDecaux France.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du référé précontractuel du tribunal administratif de Grenoble que, par un avis d'appel public à la concurrence du 11 février 2019, la commune de Saint-Julien-en-Genevois a lancé une procédure de concession de services portant sur la mise à disposition, l'installation, la maintenance, l'entretien et l'exploitation commerciale d'abris voyageurs et de mobiliers urbains, publicitaires ou non. Deux entreprises se sont portées candidates à l'attribution de ce contrat et ont été admises à concourir. Une fois achevée la phase de négociation et après analyse des offres, le maire de la commune de Saint-Julien-en-Genevois a décidé d'attribuer le contrat à la société Girod Médias. Par délibération du 18 septembre 2019, le conseil municipal de la commune de Saint-Julien-en-Genevois a autorisé le maire à signer le contrat avec cette société. Par l'ordonnance attaquée, le juge du référé précontractuel du tribunal administratif de Grenoble, saisi par la société JCDecaux France sur le fondement de l'article L. 551-1 du code de justice administrative, après avoir donné acte à la société du désistement de ses conclusions tendant à ce que la signature du marché soit différée dans l'attente de la communication des notes obtenues par elle et l'attributaire au titre de certains sous-critères, a annulé la procédure de passation de la concession et enjoint à la commune de Saint-Julien-en-Genevois, si elle entendait poursuivre son projet, de reprendre la procédure de passation au stade de l'analyse des candidatures.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique. (...) Le juge est saisi avant la conclusion du contrat ". En vertu de cet article, les personnes habilitées à agir pour mettre fin aux manquements du pouvoir adjudicateur à ses obligations de publicité et de mise en concurrence sont celles qui sont susceptibles d'être lésées par de tels manquements. Il appartient, dès lors, au juge des référés précontractuels de rechercher si l'opérateur économique qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auquel ils se rapportent, sont susceptibles de l'avoir lésé ou risquent de le léser, fût-ce de façon indirecte en avantageant un opérateur économique concurrent.<br/>
<br/>
              3. Aux termes de l'article 1er de l'ordonnance n° 2016-65 du 29 janvier 2016 relative aux contrats de concession, alors applicable : " I. - Les contrats de concession soumis à la présente ordonnance respectent les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures. / Ces principes permettent d'assurer l'efficacité de la commande publique et la bonne utilisation des deniers publics (...) ". Aux termes de l'article 36 de cette ordonnance : " Sans préjudice des dispositions du chapitre préliminaire et du chapitre Ier du titre Ier du livre IV de la première partie du code général des collectivités territoriales, l'autorité concédante organise librement la procédure qui conduit au choix du concessionnaire, dans le respect des principes énoncés à l'article 1er de la présente ordonnance, des dispositions du présent chapitre et des règles de procédure fixées par voie réglementaire (...) ". Aux termes de son article 27 : " La nature et l'étendue des besoins à satisfaire sont déterminées avant le lancement de la consultation en prenant en compte des objectifs de développement durable dans leurs dimensions économique, sociale et environnementale ".<br/>
<br/>
              4. Pour annuler la procédure de passation de la concession en litige, le juge du référé précontractuel du tribunal administratif de Grenoble a relevé que le règlement de la consultation mentionnait la possibilité de commander des prestations supplémentaires, évaluées au titre du critère de jugement des offres n° 8, mais qu'en l'absence de limite quantitative pour ces prestations, la commune avait insuffisamment défini l'étendue des besoins et s'était ainsi réservé une marge de choix discrétionnaire ne garantissant pas l'égalité de traitement des candidats et la transparence de la procédure. Il est toutefois loisible à l'autorité concédante, lorsqu'elle estime qu'elle pourra être placée dans la nécessité de commander des prestations supplémentaires au cours de l'exécution du contrat, sans être en mesure d'en déterminer le volume exact, de prévoir, au stade de la mise en concurrence initiale, un critère d'appréciation des offres fondé sur la comparaison des prix unitaires proposés par les candidats pour ces prestations. Il ressort des pièces du dossier soumis au juge du référé précontractuel que le critère de jugement des offres n° 8, intitulé " coûts supplémentaires pour la commune ", portait sur le coût d'achat de diverses prestations supplémentaires. A cette fin, le bordereau des prix unitaires figurant à l'annexe 3 du cahier des charges de la concession comportait un tableau de prix de mise à disposition s'appliquant " au déploiement de mobiliers supplémentaires par rapport au nombre de mobiliers à déployer fixé dans le cahier des charges et dont la charge incombe au titulaire ", dont les cinq lignes correspondaient à des mobiliers existants précisément décrits dans le cahier des charges, que les candidats devaient remplir en indiquant un " prix unitaire ". En jugeant que l'absence de limite quantitative à ces prestations avait méconnu le principe de la définition préalable par l'autorité concédante de l'étendue de ses besoins et avait laissé à la commune une marge de choix discrétionnaire, alors que ce tableau permettait de comparer les prix unitaires des différentes offres, et, au surplus, que les candidats admis à concourir étaient à même de demander des précisions sur ce point à l'autorité concédante s'ils l'estimaient souhaitable, le juge du référé précontractuel du tribunal administratif de Grenoble a commis une erreur de droit. Il suit de là, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que son ordonnance doit être annulée en tant qu'elle statue sur les conclusions tendant à l'annulation de la procédure de passation et des décisions de rejet de la candidature de la société JCDecaux France.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              6. En premier lieu, doit être écarté, pour les motifs précédemment exposés, le moyen tiré de l'absence de quantités maximales pour les prestations supplémentaires évaluées au titre du critère n° 8.<br/>
<br/>
              7. En deuxième lieu, si la société JCDecaux France soutient que le pouvoir adjudicateur lui aurait à tort attribué une note " satisfaisante " sur le sous-critère " mobilier 2 m2 avec deux faces dédiées à la communication ", alors que son prix était de 0 euro, il résulte de l'instruction que même si elle avait obtenu la note de 5 sur 5 et non de 3 sur 5 qui lui a été attribuée, sa note globale serait restée inférieure à celle de la société Girod médias. Le manquement, à le supposer établi, n'est donc, en tout état de cause, pas susceptible de l'avoir lésée.<br/>
<br/>
              8. En troisième lieu, aux termes de l'article 47 de l'ordonnance du 29 janvier 2016 : " Le contrat de concession est attribué au soumissionnaire qui a présenté la meilleure offre au regard de l'avantage économique global pour l'autorité concédante sur la base de plusieurs critères objectifs, précis et liés à l'objet du contrat de concession ou à ses conditions d'exécution. Lorsque la gestion d'un service public est concédée, l'autorité concédante se fonde également sur la qualité du service rendu aux usagers. / Les critères d'attribution n'ont pas pour effet de conférer une liberté de choix illimitée à l'autorité concédante et garantissent une concurrence effective. Ils sont rendus publics dans des conditions prévues par décret en Conseil d'État ".<br/>
<br/>
              9. Il résulte de l'instruction qu'aux termes de l'article 1er du cahier des charges de la concession, celle-ci " a pour objet la mise à disposition, l'installation, la maintenance, l'entretien et l'exploitation commerciale de mobiliers urbains publicitaires et non publicitaires ", qui y sont dénombrés par catégorie de mobilier. Le même article précise qu'" aucun mobilier supplémentaire ne pourra être implanté sans l'autorisation préalable de l'autorité concédante ". L'article 7 du même cahier des charges indique que " l'ensemble des mobiliers sera à fournir dès le début du contrat ". L'article 22 du même cahier des charges définit comme suit les conditions de modification du contrat : " Le présent contrat pourra être modifié par avenant conformément au 1° de l'article 36 du décret n° 2016-86 afin de prévoir l'ajout, le retrait ou le remplacement de mobiliers urbains, toutes catégories confondues. Les parties pourront, ainsi, décider de l'implantation ou du remplacement de certains mobiliers par une nouvelle catégorie de mobilier non prévue par le présent contrat. / Cet avenant pourra, également, prévoir de modifier le nombre et la consistance des campagnes de publicité municipales prises en charges par le concessionnaire. / De telles modifications peuvent, notamment, intervenir en cas de modification du périmètre de l'autorité concédante (extension du périmètre urbain, création d'une commune nouvelle, transfert de la compétence à un EPCI, etc.) afin d'assurer l'harmonisation des mobiliers présents à l'échelle du nouveau territoire. " Ainsi qu'il a été dit au point 4, en vertu de l'annexe 3 du même cahier des charges, les candidats à la concession doivent remplir un tableau des prix unitaires des mobiliers supplémentaires qui pourraient faire l'objet d'un tel avenant. Ces prix sont évalués par le critère n° 8 figurant à l'annexe au règlement de consultation et contribuent ainsi à apprécier l'offre économiquement la plus avantageuse.  Si les prestations faisant l'objet du critère n° 8 ne relèvent pas du contrat initial mais d'un éventuel avenant, elles correspondent toutefois à l'objet du marché et constituent une modalité d'exécution du contrat. Dès lors, le moyen tiré de ce que le critère n° 8 n'est pas lié à l'objet du contrat et, par suite, illégal doit être écarté. Par ailleurs, si ce critère fait l'objet d'une pondération à hauteur de 34 %, le choix de la collectivité d'attacher une importance particulière aux conditions financières dans lesquelles des prestations supplémentaires seraient susceptibles d'être commandées n'est pas entaché d'erreur manifeste d'appréciation.<br/>
<br/>
              10. En quatrième lieu, si la société JCDecaux France soutient que l'offre de la société Girod Médias serait " anormalement basse ", la prohibition des offres anormalement basses et le régime juridique relatif aux conditions dans lesquelles de telles offres peuvent être détectées et rejetées ne sont pas applicables, en tant que tels, aux concessions. Au demeurant il ne résulte pas de l'instruction qu'à supposer que des prestations supplémentaires soient effectivement commandées à la société Girod Médias aux conditions figurant dans son bordereau de prix unitaires, une telle circonstance serait, à l'évidence, de nature à compromettre la bonne exécution de la concession. Par suite, le moyen tiré de ce que la commune de Saint-Julien-en-Genevois aurait commis une erreur manifeste d'appréciation en retenant la candidature de la société Girod Médias doit être écarté.<br/>
<br/>
              11. Enfin, aux termes du I de l'article 34 du décret n° 2016-86 du 1er février 2016 : " I. - L'autorité concédante offre, sur son profil d'acheteur (...), un accès libre, direct et complet aux données essentielles du contrat de concession, notamment aux données suivantes : / (...) f) La valeur globale et les principales conditions financières du contrat ; (...) ". Le modèle d'avis de concession établi par l'annexe XXI au règlement n° 2015/1986 établissant les formulaires standard pour la publication d'avis dans le cadre de la passation de marchés publics, prévoit que doivent figurer dans l'avis de marché, la nature et la quantité des travaux ou services, ou l'indication des besoins et exigences, ainsi que la valeur estimée du contrat. <br/>
<br/>
              12. Il résulte de l'instruction que l'avis de concession publié par la commune de Saint-Julien-en-Genevois ne comportait aucune indication sur la nature et l'étendue des prestations et sur leur valeur estimée. Toutefois, le moyen tiré de ce que la commune n'a pas mentionné la valeur estimée du contrat de concession ne peut être regardé, dans les circonstances de l'espèce, comme ayant été susceptible de léser la société JCDecaux France qui n'a pas été dissuadée de remettre une candidature, et qui était, au surplus, en mesure de demander des précisions au cours des négociations. <br/>
<br/>
              13. Il résulte de tout ce qui précède que les conclusions de la société JCDecaux France tendant à l'annulation de la procédure ou du rejet de son offre doivent être rejetées.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Saint-Julien-en-Genevois, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société JCDecaux France le versement d'une somme de 4 500 euros à la commune de Saint-Julien-en-Genevois au titre des mêmes dispositions pour l'ensemble de la procédure.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 de l'ordonnance du 15 novembre 2019 du juge du référé précontractuel du tribunal administratif de Grenoble sont annulés. <br/>
Article 2 : Les conclusions à fins d'annulation de la procédure de passation de la concession et des décisions de rejet de la candidature de la société JCDecaux France devant le juge du référé précontractuel du tribunal administratif de Grenoble et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La société JCDecaux France versera à la commune de Saint-Julien-en-Genevois une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice. <br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Julien-en-Genevois et à la société JCDecaux France. <br/>
Copie en sera adressée à la société Girod Médias.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - CONCESSION - OBLIGATION DE DÉTERMINER LA NATURE ET L'ÉTENDUE DES BESOINS À SATISFAIRE - PORTÉE - 1) POSSIBILITÉ POUR L'AUTORITÉ CONCÉDANTE DE PRÉVOIR UN CRITÈRE D'APPRÉCIATION DES OFFRES FONDÉ SUR LA COMPARAISON DES PRIX UNITAIRES PROPOSÉS PAR LES CANDIDATS POUR DES PRESTATIONS SUPPLÉMENTAIRES - EXISTENCE, SANS OBLIGATION DE DÉTERMINER LE VOLUME EXACT DE CES PRESTATIONS - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. - CONCESSION - OBLIGATION DE DÉTERMINER LA NATURE ET L'ÉTENDUE DES BESOINS À SATISFAIRE - PORTÉE - 1) POSSIBILITÉ POUR L'AUTORITÉ CONCÉDANTE DE PRÉVOIR UN CRITÈRE D'APPRÉCIATION DES OFFRES FONDÉ SUR LA COMPARAISON DES PRIX UNITAIRES PROPOSÉS PAR LES CANDIDATS POUR DES PRESTATIONS SUPPLÉMENTAIRES - EXISTENCE, SANS OBLIGATION DE DÉTERMINER LE VOLUME EXACT DE CES PRESTATIONS - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) Il est loisible à l'autorité concédante, lorsqu'elle estime qu'elle pourra être placée dans la nécessité de commander des prestations supplémentaires au cours de l'exécution du contrat, sans être en mesure d'en déterminer le volume exact, de prévoir, au stade de la mise en concurrence initiale, un critère d'appréciation des offres fondé sur la comparaison des prix unitaires proposés par les candidats pour ces prestations.,,,2) Règlement de consultation en vue de l'attribution d'une concession de service portant sur la mise à disposition, l'installation, la maintenance, l'entretien et l'exploitation commerciale de mobiliers urbains. Règlement comportant un critère portant sur le coût d'achat de diverses prestations supplémentaires ainsi qu'un tableau de prix de mise à disposition, s'appliquant au déploiement de mobiliers supplémentaires correspondant à des mobiliers existants précisément décrits dans le cahier des charges, que les candidats devaient remplir en indiquant un prix unitaire.,,,L'absence de limite quantitative à ces prestations ne méconnaît pas le principe de la définition préalable par l'autorité concédante de l'étendue de ses besoins et ne laisse pas à la commune une marge de choix discrétionnaire, dès lors que ce tableau permet de comparer les prix unitaires des différentes offres, et, au surplus, que les candidats admis à concourir sont à même de demander des précisions sur ce point à l'autorité concédante s'ils l'estiment souhaitable.</ANA>
<ANA ID="9B"> 39-02-02 1) Il est loisible à l'autorité concédante, lorsqu'elle estime qu'elle pourra être placée dans la nécessité de commander des prestations supplémentaires au cours de l'exécution du contrat, sans être en mesure d'en déterminer le volume exact, de prévoir, au stade de la mise en concurrence initiale, un critère d'appréciation des offres fondé sur la comparaison des prix unitaires proposés par les candidats pour ces prestations.,,,2) Règlement de consultation en vue de l'attribution d'une concession de service portant sur la mise à disposition, l'installation, la maintenance, l'entretien et l'exploitation commerciale de mobiliers urbains. Règlement comportant un critère portant sur le coût d'achat de diverses prestations supplémentaires ainsi qu'un tableau de prix de mise à disposition, s'appliquant au déploiement de mobiliers supplémentaires correspondant à des mobiliers existants précisément décrits dans le cahier des charges, que les candidats devaient remplir en indiquant un prix unitaire.,,,L'absence de limite quantitative à ces prestations ne méconnaît pas le principe de la définition préalable par l'autorité concédante de l'étendue de ses besoins et ne laisse pas à la commune une marge de choix discrétionnaire, dès lors que ce tableau permet de comparer les prix unitaires des différentes offres, et, au surplus, que les candidats admis à concourir sont à même de demander des précisions sur ce point à l'autorité concédante s'ils l'estiment souhaitable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
