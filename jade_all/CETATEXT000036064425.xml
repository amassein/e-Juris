<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036064425</ID>
<ANCIEN_ID>JG_L_2017_11_000000396688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/06/44/CETATEXT000036064425.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 20/11/2017, 396688, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:396688.20171120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 3 février et  5 décembre 2016, et le 6 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des professions de l'automobile (CNPA) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1571 du 1er décembre 2015 relatif aux conditions d'application de l'article L. 213-2 du code de la route ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de commerce ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,   <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat du Conseil national des professions de l'automobile	.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 octobre 2017, présentée par le Conseil national des professions de l'automobile.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 213-1 du code de la route dans sa rédaction résultant de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " L'enseignement, à titre onéreux, de la conduite des véhicules à moteur d'une catégorie donnée et de la sécurité routière ainsi que l'animation des stages de sensibilisation à la sécurité routière (...) ne peuvent être organisés que dans le cadre d'un établissement dont l'exploitation est subordonnée à un agrément délivré par l'autorité administrative, après avis d'une commission. " ; qu'en vertu du troisième alinéa de l'article L. 213-2 du même code, dans sa rédaction résultant de la même loi : " La présentation du candidat aux épreuves du permis de conduire ne peut donner lieu à l'application d'aucuns frais. Les frais facturés au titre de l'accompagnement du candidat à l'épreuve sont réglementés dans les conditions prévues au deuxième alinéa de l'article L. 410-2 du code de commerce. " ; <br/>
<br/>
              2. Considérant que, pour l'application des dispositions de l'article L. 213-2 du code de la route citées ci-dessus, le décret du 1er décembre 2015 relatif aux conditions d'application de l'article L. 213-2 du code de la route a créé dans la partie réglementaire de ce code un article R. 213-3-3, dont le II dispose que : " Les frais appliqués au titre de l'accompagnement du candidat à l'épreuve sont déterminés préalablement à cette prestation. / Pour la partie pratique, ils couvrent forfaitairement l'ensemble de la charge de l'accompagnement, tant à l'épreuve en circulation que, le cas échéant, à celle hors circulation. Ils ne peuvent excéder les prix appliqués par l'établissement pour les durées de formation suivantes : - pour le permis des catégories A1, A2, A et BE : une heure et demie ; - pour le permis des catégories B1 et B : une heure ; - pour les permis des catégories C1, C, D1 et D : deux heures ; - pour les permis des catégories C1E, CE, D1E et DE : deux heures et demie. " ; que le conseil national des professions de l'automobile (CNPA) demande l'annulation pour excès de pouvoir de ces dispositions ; <br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article L. 410-2 du code de commerce, auquel renvoie l'article L. 213-2 du code de la route précité : " Sauf dans les cas où la loi en dispose autrement, les prix des biens, produits et services relevant antérieurement au 1er janvier 1987 de l'ordonnance n° 45-1483 du 30 juin 1945 sont librement déterminés par le jeu de la concurrence. / Toutefois, dans les secteurs ou les zones où la concurrence par les prix est limitée en raison soit de situations de monopole ou de difficultés durables d'approvisionnement, soit de dispositions législatives ou réglementaires, un décret en Conseil d'Etat peut réglementer les prix après consultation de l'Autorité de la concurrence. / (...) " ; qu'aux termes de l'article R. 462-1 du code de commerce : " Les avis rendus en application des articles L. 410-2 et L. 462-2 sont publiés avec les textes auxquels ils se rapportent. " ; qu'aux termes du deuxième alinéa de l'article 54 du règlement intérieur de l'Autorité de la concurrence : " Les avis rendus en application des articles L. 410-2 et L. 462-2 du code de commerce sont publiés sur le site Internet de l'Autorité une fois que le texte auquel ils se rapportent a été publié " ; <br/>
<br/>
              4. Considérant que, contrairement à ce que soutient le requérant, il ressort des pièces du dossier que l'Autorité de la concurrence a émis un avis sur les dispositions attaquées le 21 octobre 2015 ; qu'ainsi, le moyen tiré de ce que l'Autorité de la concurrence n'aurait pas été consultée sur le décret attaqué préalablement à son édiction manque en fait ; que les circonstances relatives à la publication de cet avis, qui a été réalisée le 9 mars 2016 sur le site internet de l'Autorité de la concurrence, sont sans incidence sur la légalité du décret attaqué ; <br/>
<br/>
              5. Considérant, en second lieu, que si le Conseil national des professions de l'automobile soutient que le décret attaqué serait entaché d'irrégularité du fait de l'absence de contreseing du ministre de l'intérieur et du secrétaire d'Etat chargé des transports, le décret attaqué n'appelle nécessairement l'intervention d'aucune mesure que le ministre de l'intérieur ni, en tout état de cause, le secrétaire d'Etat chargé des transports, serait compétent pour signer ou contresigner, eu égard à son objet et à leurs attributions respectives ; <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              6. Considérant, en premier lieu, d'une part, qu'en application des dispositions précitées de l'article L. 213-2 du code de la route, les frais susceptibles d'être facturés au candidat par l'établissement d'enseignement au titre de son accompagnement à l'épreuve de permis de conduire sont " réglementés " ; que le décret attaqué, qui fait application de ces dispositions, procède à cette réglementation en instaurant des plafonds limitant les sommes susceptibles d'être facturées aux candidats à l'épreuve du permis de conduire au titre de ces frais ; qu'ainsi, le moyen tiré de ce que le décret attaqué méconnaitrait les dispositions de l'article L. 213-2 du code de la route doit être écarté ;<br/>
<br/>
              7. Considérant, d'autre part, que ces plafonds sont fixés par référence aux tarifs appliqués par les établissements concernés pour les formations qu'ils délivrent pour la préparation aux différentes catégories de permis de conduire, comme l'avait, d'ailleurs, préconisé l'Autorité de la concurrence dans son avis du 21 octobre 2015, afin de tenir compte des particularités des différentes catégories de permis ; qu'il ressort des pièces du dossier que, si les frais supportés par les établissements concernés à l'occasion de l'accompagnement d'un candidat à son épreuve sont généralement plus élevés que le forfait arrêté par le décret attaqué lorsqu'un seul candidat est accompagné, il en va différemment en cas de candidatures groupées dès lors que, dans ce cas, le forfait est multiplié par le nombre de candidats alors même qu'une partie des frais effectivement supportés est en pratique commun ; qu'il résulte de ce qui précède que le décret attaqué, qui fixe un forfait moyen par catégorie de permis de conduire et par candidat accompagné, n'est pas entaché d'erreur manifeste d'appréciation ; <br/>
<br/>
              8. Considérant, en second lieu, que si la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, dont le décret attaqué fait application, a prévu que les frais facturés au titre de l'accompagnement du candidat à l'épreuve du permis de conduire sont réglementés, la conformité d'une telle disposition au principe constitutionnel de la liberté du commerce et de l'industrie, qui implique notamment que les personnes publiques n'apportent pas aux activités de production, de distribution ou de services exercées par des tiers des restrictions qui ne seraient pas justifiées par l'intérêt général et proportionnées à l'objectif poursuivi, ne saurait être contestée devant le Conseil d'Etat, statuant au contentieux en dehors de la procédure prévue à l'article 61-1 de la Constitution ; qu'en revanche, il appartient à celui-ci de vérifier si les mesures prises pour l'application de la loi n'ont pas elles-mêmes méconnu ces exigences ; que s'agissant du décret attaqué, pris pour l'application des dispositions en cause de la loi du 6 août 2015 précitée, la fixation des plafonds pour les sommes susceptibles d'être facturées par les établissements d'enseignement à la conduite aux candidats à l'épreuve du permis de conduire au titre des frais d'accompagnement à cette épreuve n'est pas, pour les raisons indiquées précédemment et dans le cadre de la réglementation du secteur de l'enseignement de la conduite automobile, en vertu notamment de l'article L. 213-1 du code de la route, manifestement disproportionnée ; que, par suite, le moyen tiré de ce que le décret attaqué méconnaîtrait le principe de la liberté du commerce et de l'industrie doit être écarté ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le Conseil national des professions de l'automobile n'est pas fondé à demander l'annulation du décret qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Conseil national des professions de l'automobile est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Conseil national des professions de l'automobile, au Premier ministre et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
