<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844868</ID>
<ANCIEN_ID>JG_L_2020_12_000000429649</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844868.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2020, 429649, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429649</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429649.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et trois nouveaux mémoires, enregistrés les 11 avril et 27 novembre 2019 et les 27 mai, 26 juin et 20 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de la section 11 du Conseil national des universités du 21 février 2019 ne retenant pas sa candidature à l'inscription sur la liste de qualification aux fonctions de professeur des universités ; <br/>
<br/>
              2°) d'enjoindre à la ministre de l'enseignement supérieur, de la recherche et de l'innovation de l'inscrire sur cette liste ou, à titre subsidiaire, de réunir à nouveau la section 11 du Conseil national des universités dans un délai de deux mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - l'arrêté de la ministre de l'enseignement supérieur et de la recherche du 19 mars 2010 fixant les modalités de fonctionnement du Conseil national des universités ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Mme D..., maître de conférences en anglais juridique, demande l'annulation pour excès de pouvoir de la décision de la section 11 du Conseil national des universités du 21 février 2019 ne retenant pas sa candidature à l'inscription sur la liste de qualification aux fonctions de professeur des universités.<br/>
<br/>
              2. Aux termes de l'article 45 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " I. - Les demandes d'inscription sur la liste de qualification aux fonctions de professeur des universités, assorties d'un dossier individuel de qualification, sont examinées par la section compétente du Conseil national des universités (...) La qualification est appréciée par rapport aux différentes fonctions des enseignants-chercheurs mentionnées à l'article L. 952-3 du code de l'éducation et compte tenu des diverses activités des candidats (...) III. - Le bureau communique par écrit à chaque candidat non inscrit sur la liste les motifs pour lesquels sa candidature a été écartée (...) ". Aux termes de l'article L. 952-3 du code de l'éducation : " Les fonctions des enseignants-chercheurs s'exercent dans les domaines suivants : / (...) 2° La recherche (...) ".  <br/>
<br/>
              3. En premier lieu, il résulte des dispositions du III de l'article 45 du décret du 6 juin 1984 citées au point 2 que la notification de la décision par laquelle la section compétente du Conseil national des universités refuse d'inscrire un candidat sur la liste de qualification aux fonctions de professeur d'université doit, à peine d'illégalité de la décision, être assortie de la communication de l'énoncé des motifs qui la fondent. En indiquant qu'" en dépit d'une palette de compétences rares et d'une expérience riche dans le domaine juridique, la candidate présente un dossier qui est plus celui d'une enseignante que d'une chercheuse " et en l'invitant, en vue d'une future nouvelle demande, " à consolider la conceptualisation et la méthodologie de ses travaux scientifiques ", la section 11 du Conseil national des universités a suffisamment motivé sa décision.  <br/>
<br/>
              4. En deuxième lieu, il résulte de ce qui a été dit au point précédent que la section 11 du Conseil national des universités s'est bornée à apprécier la qualification de l'intéressée au regard des différentes missions des enseignants-chercheurs, au nombre desquelles figure la recherche. Elle n'a ce faisant ni substitué son appréciation à celle du jury qui avait habilité Mme D... à diriger des recherches, appréciation dont l'intéressée ne peut en tout état de cause utilement se prévaloir, dans le cadre de la présente instance, ni commis d'erreur de droit. <br/>
<br/>
              5. En troisième lieu, aux termes de l'article 3 de l'arrêté de la ministre de l'enseignement supérieur et de la recherche du 19 mars 2010 relatif au fonctionnement du Conseil national des universités, " (...) pour chaque section, les critères et modalités d'appréciation des candidatures lors de l'examen des mesures individuelles relatives à la qualification, au recrutement, à la carrière et à la prime d'encadrement doctoral et de recherche ainsi que les modalités de mise en oeuvre du suivi de carrière des enseignants-chercheurs sont publiés selon une périodicité au moins annuelle sur le site internet du ministère de l'enseignement supérieur et de la recherche (...) ".  En se bornant à soutenir que les critères d'appréciation des candidatures aux fonctions de professeur des universités retenus par la section 11 du Conseil national des universités n'auraient pas été actualisés depuis 2018, Mme D... n'apporte aucun élément de nature à établir que la décision attaquée aurait été prise en méconnaissance des dispositions de l'article 3 de l'arrêté du 19 mars 2010. <br/>
<br/>
              6. Il résulte de tout ce qui précède que Mme D... n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par suite, être également rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme C... D... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
