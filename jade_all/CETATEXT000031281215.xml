<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281215</ID>
<ANCIEN_ID>JG_L_2015_09_000000393121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/09/2015, 393121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:393121.20150922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société S.E.M.E.V  demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 28 août 2015 de la ministre des affaires sociales, de la santé et des droits des femmes lui refusant l'agrément pour délivrer la formation spécifique à l'ostéopathie ; <br/>
<br/>
              2°) d'enjoindre à la ministre de procéder à compter de la notification de la présente ordonnance, à une nouvelle instruction de sa demande d'agrément sous astreinte de 1500 euros par jour ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - le Conseil d'Etat est compétent pour connaître de la demande en premier ressort ; <br/>
              - la condition d'urgence est remplie, dès lors que la décision contestée prive l'établissement de la possibilité d'exercer son activité d'enseignement et de formation en ostéopathie, perturbe considérablement la formation des étudiants et l'organisation de la prochaine rentrée et emporte de lourdes conséquences budgétaires et financières mettant en cause la pérennité de l'établissement ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - elle est entachée d'un vice de procédure car elle aurait dû être prise après un nouvel avis de la Commission consultative nationale d'agrément conformément à la décision initiale de refus d'agrément du 8 juillet 2015 ;<br/>
              - elle est entachée d'une irrégularité pour incompétence de son signataire; <br/>
              - elle est insuffisamment motivée et en ce sens méconnait les dispositions de la loi du 11 juillet 1979 ; <br/>
              - les motifs du refus d'agrément sont infondés, l'appréciation portée par la décision contestée quant à la conformité du dossier de demande d'agrément, notamment sur  l'activité clinique de l'établissement, étant erronée.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 17 septembre 2015, la ministre des affaires sociales, de la santé et des droits des femmes conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la société requérante ne sont pas fondés. <br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n°2002-303 du 4 mars 2002, notamment son article 75 ;<br/>
              - le décret n° 2014-1043 du 12 septembre 2014 ;<br/>
              - le décret n° 2014-1505 du 12 décembre 2014 ;<br/>
              - l'arrêté du 29 septembre 2014 relatif à l'agrément des établissements de formation en ostéopathie ;<br/>
              - l'arrêté du 12 décembre 2014 relatif à la formation en ostéopathie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société S.E.M.E.V d'autre part, la ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 21 septembre 2015 à 10 heures 30  au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants de la société S.E.M.E.V. ;<br/>
<br/>
              - les représentants de la ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              Sur les conclusions à fin de suspension :<br/>
<br/>
              En ce qui concerne la condition relative à l'urgence :<br/>
<br/>
              2. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce ;<br/>
<br/>
              3. Considérant qu'il résulte des éléments versés au dossier du juge des référés, ainsi que des échanges tenus lors de l'audience, que l'établissement OSTEOBIO de la société S.E.M.E.V est privé, depuis le 31 août 2015, par application des dispositions du décret du 12 septembre 2014 et en conséquence de la décision contestée du 28 août 2015, du bénéfice de l'agrément qui lui avait été délivré pour son activité de formation en ostéopathie ; que les conséquences de cette décision, d'une part, sur la situation des étudiants inscrits ayant commencé leur formation dans le centre, d'autre part, sur l'activité de l'établissement, sur sa situation financière et sur sa réputation, traduisent une situation d'urgence justifiant le prononcé de mesures provisoires en référé, en application de l'article L. 521-1 du code de justice administrative, sans qu'aucun intérêt public ne s'y oppose ;<br/>
<br/>
              En ce qui concerne la condition tenant à la légalité de la décision contestée :<br/>
<br/>
              4. Considérant qu'en vertu de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, " l'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire " ; que les conditions de l'agrément des établissements de formation en ostéopathie ont été modifiées par le décret du 12 septembre 2014, dont l'article 29 a prévu que les agréments antérieurement délivrés prendraient fin le 31 août 2015 et que les établissements agréés à la date de publication du décret devraient adresser une nouvelle demande d'agrément, conforme aux exigences du décret, entre le 1er janvier et le 28 février 2015 ;<br/>
<br/>
              5. Considérant que selon l'article 2 du décret du 12 septembre 2014 : " L'agrément permettant de délivrer la formation spécifique à l'ostéopathie mentionnée à l'article 75 de la loi du 4 mars 2002 susvisée est accordé aux établissements répondant aux conditions suivantes : 1° Justifier des déclarations préalables prévues par le code de l'éducation pour l'ouverture d'un établissement d'enseignement supérieur privé ; / 2° Proposer une formation permettant l'acquisition des connaissances et des compétences professionnelles, conformément aux dispositions réglementaires relatives à la formation des ostéopathes ; / 3° Présenter un dossier pédagogique répondant aux conditions fixées à l'article 17 ; / 4° Présenter une organisation interne conforme aux articles 10 à 14 ; / 5° Disposer de locaux et d'une capacité financière suffisante dans les conditions prévues aux articles 22 et 23 ; / 6° Bénéficier d'une équipe pédagogique justifiant d'une qualification et répondant aux conditions précisées aux articles 15, 16, 20 et 21 ; / 7° Justifier d'une organisation de la formation clinique répondant aux conditions prévues à l'article 18 ; / 8° Présenter l'engagement mentionné à l'article 5 " ;  que parmi les dispositions réglementaires mentionnées au 2° figure le référentiel de formation prévu à l'article 4 du décret du 12 décembre 2014, constitué par l'annexe III de l'arrêté conjoint des ministres chargés de l'enseignement supérieur et de la santé en date du 12 décembre 2014 ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces versées au dossier du juge des référés et des éléments indiqués au cours de l'audience que l'Institut OSTEOBIO a été créé en 2002 par la société S.E.M.E.V; qu'il a reçu, sur le fondement du décret du 25 mars 2007 pris pour l'application de la loi du 4 mars 2002, un agrément pour son activité de formation en ostéopathie, lequel a été renouvelé en 2012 ; qu'après l'intervention du décret du 12 septembre 2014, il a présenté, sur le fondement de ce décret, une nouvelle demande d'agrément afin de pouvoir continuer d'être agréé après le 31 août 2015 ; que cette demande a été rejetée par décision du 8 juillet 2015 ; que cette décision a été suspendue par une ordonnance du juge des référés du  tribunal de Melun le 31 juillet 2015 qui a enjoint à l'administration de prendre une nouvelle décision relative à la demande d'agrément de l'établissement OSTEOBIO dans un délai de quinze jours; qu'en l'absence de nouvelle décision de l'administration, la société S.E.M.E.V. a saisi une nouvelle fois le juge des référés du tribunal administratif de Melun d'une requête en exécution le 20 août 2015 ; que, par une nouvelle décision du 28 août 2015, l'administration a refusé de faire droit à la demande d'agrément pour l'établissement OSTEOBIO de la société S.E.M.E.V ; que la société requérante demande en référé la suspension de l'exécution de cette décision, sur le fondement de l'article L. 521-1 du code de justice administrative ; que cette demande n'échappe pas manifestement à la compétence du juge des référés du Conseil d'Etat ;<br/>
<br/>
              7.Considérant que la décision du 28 août 2015 est fondée sur le motif que la clinique pédagogique interne de l'établissement n'est pas conforme aux dispositions des articles 18 et 22 du décret du 12 septembre 2014, dès lors qu'elle procède de conventions passées avec deux établissements hospitaliers, le centre hospitalier de Villeneuve-Saint-Georges et un centre hospitalier de Saint-Cloud, dont l'objet serait de fournir une prestation de services à ces établissements ; que l'article 22 du décret, selon lequel les locaux de l'établissement sont exclusivement dédiés à la formation, ne paraît pas une référence pertinente, et que c'est au regard de l'article 18 qu'il y a lieu d'apprécier le bien fondé du motif invoqué par l'administration ;<br/>
<br/>
              8. Considérant que l'article 18 du décret du 12 septembre 2014 exige que la formation pratique clinique se déroule au moins pour les deux tiers " au sein de la clinique de l'établissement de formation dédiée à l'accueil des patients, en présence et sous la responsabilité d'un enseignant ostéopathe de l'établissement " ; que si ces dispositions ne s'opposent pas à ce que la formation en clinique interne soit prévue dans les locaux d'un établissement externe hospitalier, il appartient dans ce cas au demandeur d'un agrément de justifier qu'il a pris toutes les dispositions, notamment par voie conventionnelle, pour que la formation pratique clinique soit organisée sous la direction autonome et la responsabilité juridique de l'établissement de formation ; qu'en l'espèce, la clinique interne de l'établissement OSTEOBIO est d'une part, organisée à l'intérieur de l'établissement dans quatre salles de consultations  permettant la réalisation de 5760 consultations annuelles, d'autre part, organisée dans des locaux situés dans le Centre hospitalier de Villeneuve-Saint-Georges et le Centre Hospitalier des Villes à Saint-Cloud par voie conventionnelle ; que les stipulations de ces contrats précisent sans ambiguïté que l'activité clinique est organisée dans les locaux de ces établissements sous la direction et la responsabilité de l'institut OSTEOBIO ; que si, par ailleurs, l'activité en résultant peut être analysée comme un service rendu aux établissements hospitaliers d'accueil, une telle situation ne caractérise pas une méconnaissance des dispositions de l'article 18 rappelées ci-dessus; que, notamment, ces dispositions n'exigent pas qu'une éventuelle location de locaux doive se faire que moyennant un loyer fixe ; <br/>
<br/>
<br/>
              9. Considérant qu'il en résulte qu'un doute sérieux existe sur le bien fondé de ce motif et, par conséquent, sur la légalité de la décision contestée ; que l'institut requérant est donc fondé à demander la suspension de la décision contestée du 28 août 2015 ;<br/>
<br/>
              Sur les conclusions à fins d'injonction :<br/>
<br/>
              10. Considérant que la suspension décidée par la présente ordonnance implique que l'autorité administrative procède à un nouvel examen de la demande d'agrément dans un délai de 15 jours à compter de la notification de la présente ordonnance ; qu'il n'y a pas lieu d'assortir cette injonction d'une astreinte ; que si ce réexamen doit se faire au regard du motif de la suspension ordonnée par la présente décision, il y a lieu de préciser que la décision ainsi prise à la suite d'un examen ordonné en référé aura, par sa nature même, un caractère provisoire jusqu'à ce qu'il ait été statué sur le recours en annulation présenté par la société S.E.M.E.V..<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, sur le fondement de l'article L. 761-1 du code de justice administrative, la somme de 3 000 euros à verser à la société S.E.M.E.V. au titre des frais exposés dans le cadre de l'instance de référé et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La décision du 28 août 2015 de la ministre des affaires sociales, de la santé et des droits des femmes est suspendue. <br/>
Article 2 : Il est enjoint à la ministre des affaires sociales, de la santé et des droits des femmes de procéder au réexamen de la demande d'agrément dans un délai de quinze jours à compter de la notification de la présente ordonnance.<br/>
Article 3 : L'Etat versera à la société S.E.M.E.V. une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente ordonnance sera notifiée à la société S.E.M.E.V. et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
