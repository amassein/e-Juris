<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039627806</ID>
<ANCIEN_ID>JG_L_2019_12_000000426461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/62/78/CETATEXT000039627806.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 16/12/2019, 426461, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426461.20191216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 20 décembre 2018 et le 21 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° D 2018-53 du 7 juin 2018 par laquelle l'Agence française de lutte contre le dopage lui a infligé la sanction de l'interdiction de participer pendant quatre ans directement ou indirectement à l'organisation et au déroulement des manifestations sportives donnant lieu à une remise de prix en argent ou en nature et des manifestations sportives organisées ou autorisées par les fédérations sportives françaises agréées ou délégataires ainsi qu'à toutes autres activités relevant des autres fédérations sportives et, d'autre part, à titre subsidiaire, de minorer la sanction infligée et de la ramener à une durée qui ne saurait être supérieure à six mois ; <br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 000 euros au titre des articles 37 de la loi 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la décision du Conseil constitutionnel n° 2019-798 QPC du 26 juillet 2019,<br/>
              - le code du sport ;<br/>
              - le décret n° 2016-1923 du 19 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolay, de Lanouvelle, Hannotin, avocat de M. A..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	M. B... A... a été soumis à un contrôle antidopage le 27 juillet 2017 à l'occasion d'un stage de cohésion de l'équipe de France de rugby à XIII au Barcarès (Pyrénées-Orientales). Les résultats établis par le département des analyses de l'Agence française de lutte contre le dopage (AFLD) ont révélé la présence dans ses urines d'a-trenbolone, Par un courrier du 20 septembre 2017, la Fédération a informé M. A... de ces griefs. Par un courrier en date 17 octobre 2017, la Fédération française de rugby à XIII a informé l'AFLD que M. A... n'était plus licencié. L'AFLD, compétente pour lui infliger une sanction sur le fondement du 1° de l'article L. 232-22 du code du sport, en l'absence de détention d'une licence sportive par l'intéressé, a prononcé à son encontre le 7 juin 2018 une interdiction de participer pendant quatre ans directement ou indirectement à l'organisation et au déroulement des manifestations sportives donnant lieu à une remise de prix en argent ou en nature et des manifestations sportives organisées ou autorisées par les fédérations sportives françaises agréées ou délégataires ainsi qu'à toutes autres activités relevant des autres fédérations sportives.<br/>
<br/>
              Sur l'application des dispositions du 1° de l'article L. 232-22 du code du sport :<br/>
<br/>
              2.	Saisi par le Conseil d'Etat d'une question prioritaire de constitutionnalité soulevée par M. A... et portant sur la constitutionnalité du 1° de l'article L. 232-22 du code du sport dans sa rédaction résultant de l'ordonnance n° 2015-1207 du 30 septembre 2015, selon lequel l'agence française de lutte contre le dopage: " 1° [Elle] est compétente pour infliger des sanctions disciplinaires aux personnes non licenciées : a) Participant à des manifestations ou entraînements mentionnés aux 2° ou 3° du I de l'article L. 232-5 ; b) Organisant ou participant à l'organisation des manifestations ou entraînements mentionnés aux 2° ou 3° du I de l'article L. 232-5 ", le Conseil constitutionnel a, dans sa décision 2019-798 QPC du 26 juillet 2019 déclaré contraire à la Constitution cette disposition. Il a jugé toutefois que cette déclaration d'inconstitutionnalité peut être invoquée dans toutes les instances relatives à une sanction prononcée sur le fondement des dispositions contestées avant la publication de la présente décision et non définitivement jugées à cette date, à l'exception des hypothèses où un sportif, qui fait l'objet d'une procédure disciplinaire à raison de faits commis alors qu'il était licencié d'une fédération, a cessé d'être licencié par cette fédération à la date à laquelle les organes de la fédération devraient se prononcer, le dossier étant alors transmis à l'agence française de lutte contre le dopage, seule compétente en vertu du 1° de l'article L. 232-22 du code du sport pour exercer le pouvoir disciplinaire à l'égard de ce sportif.<br/>
<br/>
              3.	Il ressort des pièces du dossier, en particulier de la lettre du président de la Fédération française de rugby à XIII du 17 octobre 2017, que M. A... n'était plus licencié à la date de la décision attaquée. M. A... n'est ainsi pas fondé à invoquer la déclaration d'inconstitutionnalité à l'encontre de la décision attaquée. <br/>
<br/>
              Sur la régularité de la procédure :<br/>
<br/>
              4.	L'article L. 232-11 du code du sport dispose que " Outre les officiers et agents de police judiciaire agissant dans le cadre des dispositions du code de procédure pénale, sont habilités à procéder aux contrôles diligentés par l'Agence française de lutte contre le dopage ou demandés par les personnes mentionnées à l'article L. 232-13 et à rechercher et constater les infractions aux dispositions prévues aux articles L. 232 9 et L. 232-10 les agents relevant du ministre chargé des sports et les personnes agréées par l'agence et assermentés dans des conditions fixées par décret en Conseil d'Etat ". Selon l'article R. 232-70 du même code : " L'agrément des personnes chargées du contrôle au titre de l'article L. 232-11 est accordé par l'Agence française de lutte contre le dopage dans les conditions qu'elle définit. Il ne peut être accordé aux professionnels de santé qui ont fait l'objet d'une sanction disciplinaire dans les cinq années qui précèdent. De même, il ne peut être accordé aux personnes qui assurent des fonctions de membres d'un organe disciplinaire compétent en matière de dopage au sein d'une fédération sportive agréée. L'agrément est donné pour une durée de deux ans renouvelable ".<br/>
<br/>
              5.	Il résulte des pièces du dossier que la personne ayant procédé au contrôle avait fait l'objet d'un agrément et prêté serment. Le moyen tiré de l'irrégularité de la procédure manque donc en fait.<br/>
<br/>
              Sur le bien-fondé et la proportionnalité de la sanction :<br/>
<br/>
              6.	M. A... fait valoir qu'il n'a jamais eu l'intention de commettre le manquement qui lui est reproché et que la sanction est manifestement excessive compte tenu du manquement reproché et du contexte de l'affaire.<br/>
<br/>
              7.	L. 232-2 3-3-3 du code du sport dans sa rédaction alors applicable dispose que : " I. - La durée des mesures d'interdiction mentionnées au 2o du I de l'article L. 232-23 à raison d'un manquement à l'article L. 232-9 ou au 2° de l'article L. 232-10 : 1o Est de quatre ans lorsque ce manquement implique une substance non spécifiée. Cette durée est ramenée à deux ans lorsque le sportif démontre qu'il n'a pas eu l'intention de commettre ce manquement ". <br/>
<br/>
              8.	Selon l'article L. 232-23-3-10 du code du sport, dans sa rédaction alors en vigueur : " La durée des mesures d'interdiction prévues aux articles L. 232-23-3-3 à L. 232-23-3-8 peut être réduite par une décision spécialement motivée lorsque les circonstances particulières de l'affaire le justifient au regard du principe de proportionnalité ".<br/>
<br/>
              9.	En l'espèce le manquement constaté prohibé par l'article L. 232-9 du code du sport consiste en l'utilisation de substances ou de procédés référencés sur une liste en raison de leurs propriétés, qui sont de nature à modifier artificiellement les capacités des sportifs, l'a-trenbolone étant référencé parmi les agents anabolisants de la classe S1 figurant sur la liste annexée au décret du 19 décembre 2016 portant publication de l'amendement à l'annexe 1 de la convention internationale contre le dopage dans le sport.<br/>
<br/>
              10.	M. A... soutient, en produisant notamment des témoignages, qu'il n'avait aucune intention de faire usage d'anabolisants, le produit qu'il a absorbé lui ayant été présenté comme purement diététique et, par suite, qu'en l'absence en outre d'antécédents, la sanction qui lui a été infligée est disproportionnée. Aucun de ces éléments ne permettent toutefois de regarder la sanction prononcée contre M. A..., sportif de haut niveau, tenu à ce titre de s'assurer que les produits qu'il consomme ne contiennent pas de substances interdites, comme disproportionnée. <br/>
<br/>
              11.	Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision attaquée. Ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 doivent, par suite, être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à l'Agence française de lutte contre le dopage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
