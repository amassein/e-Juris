<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065816</ID>
<ANCIEN_ID>JG_L_2020_06_000000435502</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/58/CETATEXT000042065816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 29/06/2020, 435502</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435502</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; CABINET MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:435502.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Eaux douces a demandé au juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la délibération du 14 juin 2019 par laquelle la commission permanente du conseil départemental de la Vendée a décidé d'exercer le droit de préemption au titre des espaces naturels sensibles afin d'acquérir des parcelles, appartenant à Mme A... B..., cadastrées section F n°s 1206 et 1207 situées dans la commune des Epesses (Vendée). Par une ordonnance n° 1910493 du 8 octobre 2019, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 23 octobre et 7 novembre 2019 et les 4 mars, 11 mars et 19 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la SCI Eaux douces demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de suspendre l'exécution de la délibération du 14 juin 2019 ;<br/>
<br/>
              3°) de mettre à la charge du département de la Vendée la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la SCI Eaux douces et au cabinet Munier-Apaire, avocat du département de la Vendée ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes du premier alinéa de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Enfin, aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ".  <br/>
<br/>
              2. Si les ordonnances par lesquelles le juge des référés fait usage de ses pouvoirs de juge de l'urgence sont exécutoires et, en vertu de l'autorité qui s'attache aux décisions de justice, obligatoires, elles sont, compte tenu de leur caractère provisoire, dépourvues de l'autorité de chose jugée. Il en résulte que la circonstance que le juge des référés a rejeté une première demande de suspension présentée sur le fondement de l'article L. 521-1 du code de justice administrative ne fait pas obstacle à ce que la même partie saisisse ce juge d'une nouvelle demande ayant le même objet, notamment en soulevant des moyens ou en faisant valoir des éléments nouveaux, alors même qu'ils auraient pu lui être soumis dès sa première saisine. Une telle demande trouve son fondement non dans les dispositions de l'article L. 521-4, qui ne sauraient être utilement invoquées lorsque le juge des référés a rejeté purement et simplement une demande aux fins de suspension, mais dans celles de l'article L. 521-1. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que, par une délibération du 14 juin 2019, la commission permanente du conseil départemental de la Vendée a décidé d'exercer le droit de préemption au titre des espaces naturels sensibles, en vertu des articles L. 215-1 et suivants du code de l'urbanisme, afin d'acquérir des parcelles de terrain non bâties appartenant à Mme B..., cadastrées section F n°s 1206 et 1207, situées dans la zone de l'Aujardière, dans la commune des Epesses en Vendée. La SCI Eaux douces, acquéreur évincé, a demandé au tribunal administratif de Nantes l'annulation pour excès de pouvoir de cette délibération puis a saisi le juge des référés de ce tribunal administratif, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une demande tendant à la suspension de l'exécution de cette délibération, qui a été rejetée par une ordonnance du 27 août 2019. Elle a ensuite saisi le même juge d'une seconde demande tendant à la suspension de l'exécution de cette délibération, sur le même fondement, qui a été rejetée par une ordonnance du 8 octobre 2019. Elle se pourvoit en cassation contre cette dernière ordonnance.<br/>
<br/>
              4. Dans son ordonnance du 27 août 2019, le juge des référés du tribunal administratif de Nantes s'est notamment prononcé sur le moyen, soulevé par la SCI Eaux douces, tiré de la méconnaissance des dispositions, inapplicables au litige, du quatrième alinéa de l'article L. 213-2 du code de l'urbanisme par la décision critiquée, en ce qu'elle n'avait pas été notifiée régulièrement au vendeur dans le délai non franc de deux mois. En jugeant que la seconde demande présentée par la SCI Eaux douces, fondée sur la méconnaissance du délai prévu à l'article R. 215-12 du code de l'urbanisme, applicable à l'exercice du droit de préemption dans les zones naturelles sensibles, ne soulevait pas un moyen nouveau, ce dont il a déduit que cette demande était manifestement mal fondée, le juge des référés a, ainsi que le soutient la SCI Eaux douces, méconnu la portée de l'ordonnance du 27 août 2019. Dès lors, la SCI Eaux douces est fondée à demander l'annulation de l'ordonnance du 8 octobre 2019 qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la SCI Eaux douces.<br/>
<br/>
              6. Lorsque le juge des référés prend, sur le fondement des dispositions de l'article L. 5211 du code de justice administrative, une mesure de suspension de l'exécution d'une décision de préemption avant l'intervention du transfert de propriété au profit de la collectivité publique qui a exercé le droit de préemption, cette suspension a en principe pour effet de faire obstacle au transfert de propriété du bien préempté au bénéfice de cette collectivité et à la prise de possession du bien. Toutefois, le juge des référés, qui doit prendre en considération les incidences de la suspension pour l'ensemble des personnes intéressées, tout en préservant les intérêts du futur propriétaire, quel qu'il soit, peut notamment suspendre la décision de préemption en tant seulement qu'elle permet à la collectivité publique de disposer du bien et d'en user dans des conditions qui rendraient difficilement réversible la décision de préemption, en précisant alors que son ordonnance ne fait pas obstacle à la signature de l'acte authentique et au paiement du prix d'acquisition, ou au contraire la suspendre en tant qu'elle fait obstacle à la vente au bénéfice de l'acquéreur initial, à ses risques et périls et, le cas échéant, sous les mêmes réserves relatives à la disposition et à l'usage du bien.<br/>
<br/>
              7. D'une part, aux termes de l'article L. 213-7 du code de l'urbanisme, applicable au droit de préemption dans les espaces naturels sensibles en vertu de l'article L. 215-20 du même code : " A défaut d'accord sur le prix, tout propriétaire d'un bien soumis au droit de préemption, qui a manifesté son intention d'aliéner ledit bien, peut ultérieurement retirer son offre (...) ". L'article R. 213-8 du même code, applicable à ce droit de préemption en vertu de son article R. 215-9, dispose que : " Lorsque l'aliénation est envisagée sous forme de vente de gré à gré ne faisant pas l'objet d'une contrepartie en nature, le titulaire du droit de préemption notifie au propriétaire : / a) Soit sa décision de renoncer à l'exercice du droit de préemption ; / b) Soit sa décision d'acquérir aux prix et conditions proposés, y compris dans le cas de versement d'une rente viagère ; / c) Soit son offre d'acquérir à un prix proposé par lui et, à défaut d'acceptation de cette offre, son intention de faire fixer le prix du bien par la juridiction compétente en matière d'expropriation (...) ". L'article R. 213-10 de ce code, également applicable en vertu de l'article R. 215-9, précise que : " A compter de la réception de l'offre d'acquérir faite en application des articles R. 213-8 (c) ou R. 213-9 (b), le propriétaire dispose d'un délai de deux mois pour notifier au titulaire du droit de préemption : / a) Soit qu'il accepte le prix ou les nouvelles modalités proposés (...) ; / b) Soit qu'il maintient le prix ou l'estimation figurant dans sa déclaration et accepte que le prix soit fixé par la juridiction compétente en matière d'expropriation ; / c) Soit qu'il renonce à l'aliénation. / Le silence du propriétaire dans le délai de deux mois mentionné au présent article équivaut à une renonciation d'aliéner ". Si la circonstance que le propriétaire d'un bien a, à la suite de la réception de la décision de préemption à un prix inférieur à celui qui figure dans la déclaration d'intention d'aliéner, renoncé à l'aliénation de ce bien, dans les conditions prévues par ces dispositions, fait obstacle à ce que le titulaire du droit de préemption en poursuive l'acquisition, la décision de préemption continue toutefois d'empêcher que la vente soit menée à son terme au profit de l'acquéreur évincé.<br/>
<br/>
              8. D'autre part, aux termes de l'article L. 213-14 du code de l'urbanisme, issu de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové et applicable au droit de préemption dans les espaces naturels sensibles en vertu de l'article L. 215-20 du même code : " En cas d'acquisition d'un bien par voie de préemption (...), le transfert de propriété intervient à la plus tardive des dates auxquelles seront intervenus le paiement et l'acte authentique. / Le prix d'acquisition est payé ou, en cas d'obstacle au paiement, consigné dans les quatre mois qui suivent soit la décision d'acquérir le bien au prix indiqué par le vendeur ou accepté par lui, soit la décision définitive de la juridiction compétente en matière d'expropriation, soit la date de l'acte ou du jugement d'adjudication. / En cas de non-respect du délai prévu au deuxième alinéa du présent article, le vendeur peut aliéner librement son bien. (...) ". La méconnaissance du délai de quatre mois prévu par ces dispositions pour payer ou consigner le prix d'acquisition entraîne la caducité de la décision de préemption, dont le titulaire du droit de préemption ne peut plus poursuivre l'exécution.  <br/>
<br/>
              9. Eu égard à l'objet d'une décision de préemption et à ses effets pour l'acquéreur évincé, la condition d'urgence doit en principe être regardée comme remplie lorsque celui-ci demande la suspension d'une telle décision. Il peut toutefois en aller autrement dans le cas où le titulaire du droit de préemption justifie de circonstances particulières, tenant par exemple, s'agissant du droit de préemption urbain, à l'intérêt s'attachant à la réalisation rapide du projet qui a donné lieu à l'exercice du droit de préemption ou, s'agissant du droit de préemption dans les espaces naturels sensibles, aux nécessités de l'intervention rapide de mesures de protection de milieux naturels fragiles. En outre, lorsque le propriétaire du bien préempté renonce, implicitement ou explicitement, à son aliénation, empêchant ainsi la collectivité publique titulaire du droit de préemption de l'acquérir, l'urgence ne peut être regardée comme remplie au profit de l'acquéreur évincé que si celuici fait état de circonstances caractérisant la nécessité pour lui de réaliser à très brève échéance le projet qu'il envisage sur les parcelles considérées. Enfin, si la collectivité publique titulaire du droit de préemption ne respecte pas le délai qui lui est imparti par l'article L. 213-14 du code de l'urbanisme pour payer ou consigner le prix d'acquisition, la décision de préemption ne peut plus être exécutée et le vendeur peut aliéner librement son bien, de sorte que la condition d'urgence n'est, en tout état de cause, pas remplie.<br/>
<br/>
              10. Il résulte de l'instruction que la SCI Eaux douces a la qualité d'acquéreur évincé au regard de la décision de préemption litigieuse. Si le département fait valoir que la propriétaire des parcelles en litige, Mme B..., s'est abstenue de répondre à l'offre d'acquérir dans le délai de deux mois à compter de la notification de la décision de préemption au notaire chargé de la vente, le 17 juin 2019, la délibération critiquée décide d'exercer le droit de préemption aux prix et conditions proposés, comportant un capital de 7 689,05 euros et une rente viagère annuelle de 123,14 euros, de sorte que la propriétaire ne pouvait renoncer à l'aliénation des deux parcelles préemptées. En revanche, il résulte des réponses apportées à la mesure supplémentaire d'instruction diligentée par la 1ère chambre de la section du contentieux que le département n'a ni payé ni consigné le prix d'acquisition dans le délai de quatre mois prévu par l'article L. 213-14 du code de l'urbanisme. Dès lors, la décision de préemption ne fait plus obstacle à la réalisation de la vente au profit de la SCI Eaux douces et la condition d'urgence posée par l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie. <br/>
<br/>
              11. Il résulte de ce qui précède que la demande de la SCI Eaux douces tendant à la suspension de l'exécution de la délibération du 14 juin 2019 de la commission permanente du conseil départemental de la Vendée décidant de préempter, au titre des espaces naturels sensibles, les parcelles cadastrées section F n°s 1206 et 1207 dans la commune des Epesses doit être rejetée.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de la Vendée, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Eaux douces le versement au département de la somme qu'il demande au même titre.<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nantes du 8 octobre 2019 est annulée.<br/>
Article 2 : La demande de suspension présentée par la SCI Eaux douces devant le juge des référés du tribunal administratif de Nantes est rejetée. <br/>
Article 3 : Les conclusions de la SCI Eaux douces et du département de la Vendée présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SCI Eaux douces et au département de la Vendée.<br/>
Copie sera adressée à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - DÉCISION DE PRÉEMPTION - ACQUÉREUR ÉVINCÉ - 1) PRÉSOMPTION D'URGENCE - A) EXISTENCE [RJ1] - B) CIRCONSTANCES DE NATURE À RENVERSER LA PRÉSOMPTION - 2) EXCEPTIONS - A) RENONCIATION DU PROPRIÉTAIRE À L'ALIÉNATION [RJ2] - B) CADUCITÉ DE LA DÉCISION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - DÉCISION DE PRÉEMPTION - RÉFÉRÉ-SUSPENSION DE L'ACQUÉREUR ÉVINCÉ - 1) PRÉSOMPTION D'URGENCE - A) EXISTENCE [RJ1] - B) CIRCONSTANCES DE NATURE À RENVERSER LA PRÉSOMPTION - 2) EXCEPTIONS - A) RENONCIATION DU PROPRIÉTAIRE À L'ALIÉNATION [RJ2] - B) CADUCITÉ DE LA DÉCISION.
</SCT>
<ANA ID="9A"> 54-035-02-03-02 1) a) Eu égard à l'objet d'une décision de préemption et à ses effets pour l'acquéreur évincé, la condition d'urgence doit en principe être regardée comme remplie, pour l'application de l'article L. 521-1 du code de justice administrative, lorsque celui-ci demande la suspension d'une telle décision.... ,,b) Il peut toutefois en aller autrement dans le cas où le titulaire du droit de préemption justifie de circonstances particulières, tenant par exemple, s'agissant du droit de préemption urbain, à l'intérêt s'attachant à la réalisation rapide du projet qui a donné lieu à l'exercice du droit de préemption ou, s'agissant du droit de préemption dans les espaces naturels sensibles, aux nécessités de l'intervention rapide de mesures de protection de milieux naturels fragiles.... ,,2) a) Si la circonstance que le propriétaire d'un bien a, à la suite de la réception de la décision de préemption à un prix inférieur à celui qui figure dans la déclaration d'intention d'aliéner, renoncé à l'aliénation de ce bien, dans les conditions prévues par l'article R. 215-10 du code de l'urbanisme, fait obstacle à ce que le titulaire du droit de préemption en poursuive l'acquisition, la décision de préemption continue toutefois d'empêcher que la vente soit menée à son terme au profit de l'acquéreur évincé.,,,Dès lors, lorsque le propriétaire du bien préempté renonce, implicitement ou explicitement, à son aliénation, empêchant ainsi la collectivité publique titulaire du droit de préemption de l'acquérir, l'urgence ne peut être regardée comme remplie au profit de l'acquéreur évincé que si celui-ci fait état de circonstances caractérisant la nécessité pour lui de réaliser à très brève échéance le projet qu'il envisage sur les parcelles considérées.... ,,b) Enfin, la méconnaissance du délai de quatre mois prévu par l'article L. 213-14 du code de l'urbanisme pour payer ou consigner le prix d'acquisition entraîne la caducité de la décision de préemption, dont le titulaire du droit de préemption ne peut plus poursuivre l'exécution....  ,,Si la collectivité publique titulaire du droit de préemption ne respecte pas le délai qui lui est imparti par l'article L. 213-14 pour payer ou consigner le prix d'acquisition, la décision de préemption ne peut plus être exécutée et le vendeur peut aliéner librement son bien, de sorte que la condition d'urgence n'est, en tout état de cause, pas remplie.</ANA>
<ANA ID="9B"> 68-02-01-01 1) a) Eu égard à l'objet d'une décision de préemption et à ses effets pour l'acquéreur évincé, la condition d'urgence doit en principe être regardée comme remplie, pour l'application de l'article L. 521-1 du code de justice administrative, lorsque celui-ci demande la suspension d'une telle décision.... ,,b) Il peut toutefois en aller autrement dans le cas où le titulaire du droit de préemption justifie de circonstances particulières, tenant par exemple, s'agissant du droit de préemption urbain, à l'intérêt s'attachant à la réalisation rapide du projet qui a donné lieu à l'exercice du droit de préemption ou, s'agissant du droit de préemption dans les espaces naturels sensibles, aux nécessités de l'intervention rapide de mesures de protection de milieux naturels fragiles.... ,,2) a) Si la circonstance que le propriétaire d'un bien a, à la suite de la réception de la décision de préemption à un prix inférieur à celui qui figure dans la déclaration d'intention d'aliéner, renoncé à l'aliénation de ce bien, dans les conditions prévues par l'article R. 215-10 du code de l'urbanisme, fait obstacle à ce que le titulaire du droit de préemption en poursuive l'acquisition, la décision de préemption continue toutefois d'empêcher que la vente soit menée à son terme au profit de l'acquéreur évincé.,,,Dès lors, lorsque le propriétaire du bien préempté renonce, implicitement ou explicitement, à son aliénation, empêchant ainsi la collectivité publique titulaire du droit de préemption de l'acquérir, l'urgence ne peut être regardée comme remplie au profit de l'acquéreur évincé que si celui?ci fait état de circonstances caractérisant la nécessité pour lui de réaliser à très brève échéance le projet qu'il envisage sur les parcelles considérées.... ,,b) Enfin, la méconnaissance du délai de quatre mois prévu par l'article L. 213-14 du code de l'urbanisme pour payer ou consigner le prix d'acquisition entraîne la caducité de la décision de préemption, dont le titulaire du droit de préemption ne peut plus poursuivre l'exécution....  ,,Si la collectivité publique titulaire du droit de préemption ne respecte pas le délai qui lui est imparti par l'article L. 213-14 pour payer ou consigner le prix d'acquisition, la décision de préemption ne peut plus être exécutée et le vendeur peut aliéner librement son bien, de sorte que la condition d'urgence n'est, en tout état de cause, pas remplie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 novembre 2002, M.,, n° 248851, p. 396. Comp., s'agissant du propriétaire du bien, CE, 14 novembre 2003, Melle,, n° 258248, T. p. 924.,,[RJ2] Comp., en cas de simple désaccord sur le prix n'emportant pas renonciation à l'aliénation, CE, 26 janvier 2005, SCI Chopin-Leturc et Caisse des écoles de la ville de Saint-Germain-en-Laye, n°s 272126 272127, T. p. 1030.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
