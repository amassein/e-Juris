<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791249</ID>
<ANCIEN_ID>JG_L_2018_04_000000417206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791249.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/04/2018, 417206</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417206.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 11 janvier et 22 février 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération des acteurs de la solidarité, la Fondation AbbéA..., le Secours catholique, France Terre d'Asile, Médecins sans frontières, Médecins du Monde, Emmaüs Solidarité, Emmaüs France, la Fondation de l'armée du salut, la Ligue des droits de l'homme, le Groupe d'information et de soutien des immigré.e.s, la CIMADE (Comité inter-mouvements auprès des évacués), l'Association droit au logement, l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux, la Fédération des établissements hospitaliers et d'aide à la personne privés non lucratifs, Aurore, l'Association des cités du secours catholique, le Centre d'action sociale protestant, l'Association nationale le refuge, la Fédération d'entraide Protestante, l'Association nationale des assistants de service social, Dom'Asile, le Centre Primo Levi, Oppelia, l'Association les petits frères des pauvres, Charonne,  JRS France - Service jésuite des réfugiés et l'Amicale du nid demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du ministre de l'intérieur et du ministre de la cohésion des territoires du 12 décembre 2017 relative à l'examen des situations administratives dans l'hébergement d'urgence ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;  <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public, <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Fédération des acteurs de la solidarité et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par la circulaire du 12 décembre 2017 dont les associations requérantes demandent l'annulation, le ministre d'Etat, ministre de l'intérieur et le ministre de la cohésion des territoires ont adressé aux préfets de région et de département des instructions en vue de l'examen de la situation administrative des personnes de nationalité étrangère hébergées dans le dispositif d'hébergement d'urgence ; <br/>
<br/>
              Sur l'intervention :<br/>
<br/>
              2.	Considérant que le Groupe accueil et solidarité, justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la requête ; que son intervention est, par suite, recevable ;<br/>
<br/>
              Sur la légalité de la circulaire attaquée : <br/>
<br/>
              3.	Considérant que la circulaire attaquée demande aux préfets de mettre en place un dispositif de suivi des étrangers accueillis dans les structures d'hébergement d'urgence et de veiller à ce qu'ils puissent recevoir une orientation adaptée à leur situation, avec comme perspective la réduction du nombre de personnes qui, alors qu'elles sont hébergées dans ces structures pendant une durée parfois longue, restent " sans statut " ou ne peuvent bénéficier de tous les droits attachés à leur situation ; qu'à cet effet, la circulaire prévoit que des équipes composées d'agents des préfectures et de l'Office français de l'immigration et de l'intégration, ainsi que, le cas échéant, de personnels compétents en matière de veille ou d'évaluation sociale, recevront mission de se déplacer dans les structures d'hébergement d'urgence pour s'entretenir avec les personnes hébergées de nationalité étrangère, déterminer leurs conditions légales de séjour en France, s'assurer qu'elles ont pu faire valoir l'ensemble de leurs droits, les informer sur leurs droits et les procédures applicables et informer le gestionnaire de la structure d'hébergement sur les règles et procédures applicables en matière de séjour, d'accès à la procédure d'asile et d'aide au retour volontaire ; que la circulaire précise qu'en fonction de l'évaluation faite de la situation des personnes rencontrées, les agents pourront préconiser toute mesure utile en vue d'une orientation adaptée, en distinguant selon que les personnes concernées bénéficient déjà d'une protection internationale, qu'elles entendent demander à en bénéficier, que leur situation au regard du droit au séjour a, ou non, fait l'objet d'un examen récent ou qu'elles ont fait l'objet d'une obligation de quitter le territoire français ; que la circulaire indique que les équipes d'agents devront se rendre dans l'ensemble des structures d'hébergement d'urgence, y compris hôtelières, en annonçant leur venue au gestionnaire de la structure au moins vingt-quatre heures à l'avance, qu'elles pourront y retourner après leur évaluation pour s'assurer de la bonne maîtrise des règles de séjour applicables et s'informer des suites données à leurs préconisations et qu'elles rendront compte au préfet des difficultés rencontrées en vue d'une orientation adaptée des personnes hébergées ; <br/>
<br/>
              4.	Considérant, en premier lieu, que cette circulaire se borne à prévoir l'intervention dans les structures d'hébergement d'urgence d'équipes constituées notamment d'agents de préfecture et de l'Office français de l'immigration et de l'intégration en vue de procéder à l'évaluation de la situation administrative des personnes hébergées, de les informer sur leurs droits et, le cas échéant, d'envisager de les réorienter ; que ces équipes sont exclusivement chargées de recueillir, auprès des personnes hébergées qui acceptent de s'entretenir avec elles, les informations que ces personnes souhaitent leur communiquer ; que, par elle-même, la circulaire ne confère, et ne saurait d'ailleurs légalement conférer, aucun pouvoir de contrainte aux agents appelés à se rendre dans les lieux d'hébergement, que ce soit à l'égard des personnes hébergées ou des gestionnaires des lieux d'hébergement ; que la circulaire ne saurait, en particulier, constituer un titre pour pénétrer dans des locaux privés hors le consentement des personnes intéressées ; que, dans ces conditions, les associations requérantes ne sont pas fondées à soutenir que les auteurs de la circulaire attaquée auraient méconnu le droit au respect de la vie privée, en particulier l'inviolabilité du domicile, garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen et par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              5.	Considérant, en deuxième lieu, que si, en vertu de l'article L. 345-2-2 du code de l'action sociale et des familles, " toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d'hébergement d'urgence (...) " et si l'article L. 345-2-3 du même code dispose que : " toute personne accueillie dans une structure d'hébergement d'urgence doit pouvoir y bénéficier d'un accompagnement personnalisé et y demeurer, dès lors qu'elle le souhaite, jusqu'à ce qu'une orientation lui soit proposée (...) ", l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile permet par ailleurs à l'autorité administrative d'obliger des étrangers qui ne sont pas autorisés à séjourner régulièrement en France à quitter le territoire français, le cas échéant sans délai, en prévoyant qu'ils peuvent bénéficier d'un dispositif d'aide au retour dans le pays d'origine ; <br/>
<br/>
              6.	Considérant que la circulaire attaquée ne saurait conférer, ainsi qu'il a été dit au point 4, aucun pouvoir de contrainte à l'égard des personnes hébergées ; qu'elle ne saurait davantage permettre de déroger aux dispositions législatives et réglementaires applicables ; qu'en indiquant que celles des personnes de nationalité étrangère accueillies dans les structures d'hébergement d'urgence qui ont fait l'objet d'une obligation de quitter le territoire français se verront proposer une aide au retour et, à défaut de départ volontaire du territoire, devront être orientées vers un dispositif adapté en vue de l'organisation de leur départ contraint, la circulaire ne peut, par suite, qu'être comprise que comme se bornant à rappeler la possibilité de mettre en oeuvre les dispositions de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dans le respect des règles applicables en la matière ; qu'elle ne peut, dans ces conditions, être regardée comme ayant méconnu les dispositions législatives précédemment mentionnées ; <br/>
<br/>
              7.	Considérant, en troisième lieu, que si l'article L. 312-1 du code de l'action sociale et des familles prévoit que les conditions techniques minimales d'organisation et de fonctionnement des établissements et services sociaux et médico-sociaux sont définies par décret pris après avis du comité national de l'organisation sanitaire et sociale et si l'article L. 345-4 de ce code renvoie à un décret en Conseil d'Etat la détermination des conditions de fonctionnement et de financement des centres d'hébergement et de réinsertion sociale mentionnés à l'article L. 345-1, la circulaire attaquée n'a, contrairement à ce qui est soutenu, nullement pour objet de modifier les conditions de fonctionnement, au sens des dispositions précédemment mentionnées du code de l'action sociale et des familles, des structures d'hébergement qu'elle évoque ; que, dès lors, le moyen tiré de ce que la circulaire serait entachée d'incompétence compte tenu de ce que prévoient les articles L. 312-1, L. 345-1 et L. 345-4 du code de l'action sociale et des familles ne peut qu'être écarté ; <br/>
<br/>
              8.	Considérant, en quatrième lieu, que la circulaire attaquée ne prévoit pas la transmission d'informations nominatives aux agents de l'administration par les gestionnaires des structures d'hébergement ; qu'elle n'a ni pour objet ni pour effet de dispenser du respect des dispositions de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ; que, par suite, le moyen tiré de la méconnaissance du droit à la protection des données personnelles ne peut qu'être écarté ; <br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que la Fédération des acteurs de la solidarité et autres ne sont pas fondés à demander l'annulation pour excès de pouvoir de la circulaire qu'ils attaquent ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention du Groupe accueil et solidarité est admise.<br/>
<br/>
Article 2 : La requête de la Fédération des acteurs de la solidarité et autres est rejetée. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la Fédération des acteurs de la solidarité, première association requérante dénommée, au ministre d'Etat, ministre de l'intérieur, au ministre de la cohésion des territoires, au Premier ministre et au Groupe Accueil et Solidarité.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - DROIT AU RESPECT DE LA VIE PRIVÉE ET PRINCIPE D'INVIOLABILITÉ DU DOMICILE - ABSENCE - CIRCULAIRE ORGANISANT UN DISPOSITIF DE SUIVI DES ÉTRANGERS ACCUEILLIS DANS LES STRUCTURES D'HÉBERGEMENT D'URGENCE NE CONFÉRANT AUCUN POUVOIR DE CONTRAINTE AUX AGENTS APPELÉS À SE RENDRE DANS CES STRUCTURES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. - CIRCULAIRE ORGANISANT UN DISPOSITIF DE SUIVI DES ÉTRANGERS ACCUEILLIS DANS LES STRUCTURES D'HÉBERGEMENT D'URGENCE NE CONFÉRANT AUCUN POUVOIR DE CONTRAINTE AUX AGENTS APPELÉS À SE RENDRE DANS CES STRUCTURES - ATTEINTE AU DROIT AU RESPECT DE LA VIE PRIVÉE ET AU PRINCIPE D'INVIOLABILITÉ DU DOMICILE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - PRINCIPE CONSTITUTIONNEL D'INVIOLABILITÉ DU DOMICILE - VIOLATION - ABSENCE - CIRCULAIRE ORGANISANT UN DISPOSITIF DE SUIVI DES ÉTRANGERS ACCUEILLIS DANS LES STRUCTURES D'HÉBERGEMENT D'URGENCE NE CONFÉRANT AUCUN POUVOIR DE CONTRAINTE AUX AGENTS APPELÉS À SE RENDRE DANS CES STRUCTURES.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">26-055-01-08-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). VIOLATION. - ABSENCE - CIRCULAIRE ORGANISANT UN DISPOSITIF DE SUIVI DES ÉTRANGERS ACCUEILLIS DANS LES STRUCTURES D'HÉBERGEMENT D'URGENCE NE CONFÉRANT AUCUN POUVOIR DE CONTRAINTE AUX AGENTS APPELÉS À SE RENDRE DANS CES STRUCTURES.
</SCT>
<ANA ID="9A"> 01-04-005 Circulaire du ministre d'Etat, ministre de l'intérieur et du ministre de la cohésion des territoires du 12 décembre 2017 relative à l'examen des situations administratives dans l'hébergement d'urgence.... ,,Cette circulaire se borne à prévoir l'intervention dans les structures d'hébergement d'urgence d'équipes constituées notamment d'agents de préfecture et de l'Office français de l'immigration et de l'intégration (OFII) en vue de procéder à l'évaluation de la situation administrative des personnes hébergées, de les informer sur leurs droits et, le cas échéant, d'envisager de les réorienter. Ces équipes sont exclusivement chargées de recueillir, auprès des personnes hébergées qui acceptent de s'entretenir avec elles, les informations que ces personnes souhaitent leur communiquer. Par elle-même, la circulaire ne confère, et ne saurait d'ailleurs légalement conférer, aucun pouvoir de contrainte aux agents appelés à se rendre dans les lieux d'hébergement, que ce soit à l'égard des personnes hébergées ou des gestionnaires des lieux d'hébergement. La circulaire ne saurait, en particulier, constituer un titre pour pénétrer dans des locaux privés hors le consentement des personnes intéressées. Dans ces conditions, cette circulaire ne méconnaît pas le droit au respect de la vie privée, en particulier l'inviolabilité du domicile, garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen et par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9B"> 04-02 Circulaire du ministre d'Etat, ministre de l'intérieur et du ministre de la cohésion des territoires du 12 décembre 2017 relative à l'examen des situations administratives dans l'hébergement d'urgence.... ,,Cette circulaire se borne à prévoir l'intervention dans les structures d'hébergement d'urgence d'équipes constituées notamment d'agents de préfecture et de l'Office français de l'immigration et de l'intégration (OFII) en vue de procéder à l'évaluation de la situation administrative des personnes hébergées, de les informer sur leurs droits et, le cas échéant, d'envisager de les réorienter. Ces équipes sont exclusivement chargées de recueillir, auprès des personnes hébergées qui acceptent de s'entretenir avec elles, les informations que ces personnes souhaitent leur communiquer. Par elle-même, la circulaire ne confère, et ne saurait d'ailleurs légalement conférer, aucun pouvoir de contrainte aux agents appelés à se rendre dans les lieux d'hébergement, que ce soit à l'égard des personnes hébergées ou des gestionnaires des lieux d'hébergement. La circulaire ne saurait, en particulier, constituer un titre pour pénétrer dans des locaux privés hors le consentement des personnes intéressées. Dans ces conditions, cette circulaire ne méconnaît pas le droit au respect de la vie privée, en particulier l'inviolabilité du domicile, garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen et par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9C"> 26-03-10 Circulaire du ministre d'Etat, ministre de l'intérieur et du ministre de la cohésion des territoires du 12 décembre 2017 relative à l'examen des situations administratives dans l'hébergement d'urgence.... ,,Cette circulaire se borne à prévoir l'intervention dans les structures d'hébergement d'urgence d'équipes constituées notamment d'agents de préfecture et de l'Office français de l'immigration et de l'intégration (OFII) en vue de procéder à l'évaluation de la situation administrative des personnes hébergées, de les informer sur leurs droits et, le cas échéant, d'envisager de les réorienter. Ces équipes sont exclusivement chargées de recueillir, auprès des personnes hébergées qui acceptent de s'entretenir avec elles, les informations que ces personnes souhaitent leur communiquer. Par elle-même, la circulaire ne confère, et ne saurait d'ailleurs légalement conférer, aucun pouvoir de contrainte aux agents appelés à se rendre dans les lieux d'hébergement, que ce soit à l'égard des personnes hébergées ou des gestionnaires des lieux d'hébergement. La circulaire ne saurait, en particulier, constituer un titre pour pénétrer dans des locaux privés hors le consentement des personnes intéressées. Dans ces conditions, cette circulaire ne méconnaît pas le droit au respect de la vie privée, en particulier l'inviolabilité du domicile, garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen et par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9D"> 26-055-01-08-02 Circulaire du ministre d'Etat, ministre de l'intérieur et du ministre de la cohésion des territoires du 12 décembre 2017 relative à l'examen des situations administratives dans l'hébergement d'urgence.... ,,Cette circulaire se borne à prévoir l'intervention dans les structures d'hébergement d'urgence d'équipes constituées notamment d'agents de préfecture et de l'Office français de l'immigration et de l'intégration (OFII) en vue de procéder à l'évaluation de la situation administrative des personnes hébergées, de les informer sur leurs droits et, le cas échéant, d'envisager de les réorienter. Ces équipes sont exclusivement chargées de recueillir, auprès des personnes hébergées qui acceptent de s'entretenir avec elles, les informations que ces personnes souhaitent leur communiquer. Par elle-même, la circulaire ne confère, et ne saurait d'ailleurs légalement conférer, aucun pouvoir de contrainte aux agents appelés à se rendre dans les lieux d'hébergement, que ce soit à l'égard des personnes hébergées ou des gestionnaires des lieux d'hébergement. La circulaire ne saurait, en particulier, constituer un titre pour pénétrer dans des locaux privés hors le consentement des personnes intéressées. Dans ces conditions, cette circulaire ne méconnaît pas le droit au respect de la vie privée, en particulier l'inviolabilité du domicile, garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen et par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
