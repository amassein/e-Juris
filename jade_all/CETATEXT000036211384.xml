<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036211384</ID>
<ANCIEN_ID>JG_L_2017_12_000000412873</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/21/13/CETATEXT000036211384.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 04/12/2017, 412873, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412873</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:412873.20171204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 juillet et 29 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme G...L..., Mme K...L..., Mme C...N..., Mme O...A..., MmeW..., Mme D...B..., MmeV..., Mme E...H..., Mme M...T..., Mme P...J..., Mme I...F..., Mme Q...S..., Mme R...U..., l'association Terres et génération retrouvées et le groupement des généalogistes successoraux diplômés de Polynésie française demandent au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française la " loi du pays " n° 2017-15 LP/APF du 22 juin 2017 publiée au journal officiel de la Polynésie française le 30 juin 2017, portant modification de la loi du pays n° 2016-12 du 12 avril 2016 portant réglementation de l'activité de généalogie en Polynésie française ; <br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 74 ; <br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme L...et les autres requérants demandent au Conseil d'Etat, sur le fondement des dispositions de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, de déclarer non conformes au bloc de légalité défini au III de cet article les dispositions des articles LP1, LP2 et LP3 de la " loi du pays " du 22 juin 2017 portant modification de la " loi du pays " n° 2016-12 du 12 avril 2016 portant réglementation de l'activité de généalogie en Polynésie française. <br/>
<br/>
              2. Aux termes de l'article LP1 de la " loi du pays " attaquée, est ajouté aux conditions nécessaires à remplir pour pouvoir exercer l'activité de généalogiste en Polynésie française, prévues par l'article LP2 de la " loi du pays " n° 2016-12 du 12 avril 2016, le fait d'être " titulaire d'un diplôme délivré par l'Etat ou au nom de l'Etat et sanctionnant un niveau égal ou supérieur à trois années d'études supérieures après le baccalauréat en droit / ou [d'] être titulaire d'un diplôme universitaire dans le domaine de la généalogie et [ d']avoir exercé pendant au moins un an une activité professionnelle dans le domaine foncier ou généalogique ". Les requérants soutiennent notamment que ces dispositions méconnaissent le principe d'égalité et portent une atteinte disproportionnée à la liberté d'entreprendre. <br/>
<br/>
En ce qui concerne le moyen tiré de l'atteinte au principe d'égalité devant la loi :<br/>
<br/>
              3. Les requérants soutiennent que ces dispositions méconnaissent le principe d'égalité devant la loi en subordonnant l'autorisation d'exercer l'activité de généalogiste en Polynésie française à une expérience professionnelle minimale d'un an dans le domaine foncier ou généalogique pour les seuls titulaires d'un diplôme universitaire dans le domaine de la généalogie, les titulaires d'un diplôme délivré par l'Etat ou au nom de l'Etat sanctionnant un niveau égal ou supérieur à trois années d'études supérieures en droit étant dispensés de remplir cette condition. <br/>
<br/>
              4. Le principe d'égalité ne s'oppose ni à ce qu'une " loi du pays " règle de façon différente des situations différentes, ni à ce qu'elle déroge à l'égalité pour des raison d'intérêt général, à la condition que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet du texte qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. <br/>
<br/>
              5. Il ressort des pièces du dossier que la subordination de l'exercice de l'activité de généalogiste en Polynésie française à la délivrance d'une carte professionnelle est motivée par la nécessité de professionnaliser cette activité et de protéger les personnes qui ont recours à de telles prestations, compte tenu des spécificités locales et, en particulier, de la nécessité de constituer un cadastre dans un contexte foncier historiquement et juridiquement complexe. Si les étudiants qui préparent un diplôme universitaire dans le domaine de la généalogie suivent un cursus d'un an comportant 140 heures de formation, dont 64 heures en matière de droit des personnes et de la famille, ceux qui préparent un diplôme de l'Etat ou délivré au nom de l'Etat sanctionnant un niveau égal ou supérieur à trois années d'études supérieures en droit suivent un cursus comportant plus de 1 500 heures de formation en droit, dont plus de 120 heures en droit des personnes et de la famille. La différence de traitement instituée par les dispositions de l'article LP1 de la " loi du pays " attaquée entre les titulaires d'un diplôme délivré par l'Etat sanctionnant un niveau égal ou supérieur à trois années d'études supérieures en droit et les titulaires d'un diplôme universitaire en généalogie est ainsi, eu égard à la différence de contenu et de durée des études qu'ils ont suivies, en rapport direct avec l'objet de la " loi du pays " qui l'établit et n'est pas manifestement disproportionnée au regard des motifs qui la justifient. Par suite, le moyen tiré de l'atteinte au principe d'égalité devant la loi ne peut qu'être écarté.  <br/>
<br/>
En ce qui concerne le moyen tiré de l'atteinte portée à la liberté d'entreprendre :<br/>
<br/>
              6. Il est loisible à l'assemblée de la Polynésie française d'apporter à la liberté d'entreprendre des limitations justifiées par l'intérêt général, à condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi. En l'espèce, la subordination de l'exercice de l'activité de généalogiste à la délivrance d'une carte professionnelle pour le motif d'intérêt général mentionné au point 5 n'apporte pas en tant que telle à la liberté d'entreprendre des restrictions disproportionnées par rapport à l'objectif poursuivi. <br/>
<br/>
              7. Le fait de soumettre la délivrance de cette carte professionnelle pour les personnes titulaires d'un diplôme universitaire en généalogie à la condition d'avoir exercé préalablement pendant au moins un an une activité professionnelle dans le domaine foncier ou généalogique n'apporte pas non plus de restriction disproportionnée par rapport à l'objectif poursuivi dans la mesure où, contrairement à ce que les requérants soutiennent, cette période d'activité, dont la durée est courte, peut être effectuée auprès de nombreux professionnels intervenant en matière immobilière et foncière et pas uniquement auprès de généalogistes bénéficiant déjà d'une autorisation d'exercer cette activité en Polynésie française. Par suite, le moyen tiré de l'atteinte portée à la liberté d'entreprendre, garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen, ne peut qu'être écarté. <br/>
<br/>
En ce qui concerne les autres moyens soulevés par les requérants :<br/>
<br/>
              8. Si les dispositions de l'article LP2 de la " loi du pays " contestée ne prévoient de modifier les dispositions transitoires de la " loi du pays " n° 2016-12 du 12 avril 2016 qu'en prolongeant le délai dont disposent les agents d'affaires relevant de l'arrêté n° 447 AA du 7 avril 1956 modifié qui exercent déjà l'activité de généalogiste pour formuler une demande d'autorisation d'exercer cette activité, l'application immédiate de la nouvelle condition de diplôme et d'expérience professionnelle aux personnes diplômées ou en cours d'étude de généalogie à l'université de Polynésie française, qui tend à professionnaliser l'activité de généalogiste et à protéger les personnes qui ont recours à ces prestations, ne porte pas une atteinte excessive aux intérêts en cause. Les requérants ne peuvent soutenir, dans ces conditions, que la " loi du pays " contestée méconnaitrait le principe de sécurité juridique.   <br/>
<br/>
              9. La " loi du pays " attaquée détermine avec suffisamment de précisions les conditions d'accès à l'activité professionnelle de généalogiste. Dès lors, le moyen tiré de ce que l'article LP3 serait entaché d'incompétence négative en renvoyant à un arrêté en conseil des ministres la détermination de ses modalités d'application doit être écarté. <br/>
<br/>
              10. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander au Conseil d'Etat de déclarer illégales les dispositions des articles LP1, LP2 et LP3 de la " loi du pays " n° 2017-15 LP/APF du 22 juin 2017. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que les requérants demandent soit mise à la charge de la Polynésie française qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme L...et des autres requérants est rejetée. <br/>
Article 2 : La présente décision sera notifiée à Mme G...L..., première dénommée pour l'ensemble des requérants, au président de la Polynésie française, au président de l'assemblée de la Polynésie française et au haut-commissaire de la République en Polynésie française. <br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
