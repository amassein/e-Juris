<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028882977</ID>
<ANCIEN_ID>JG_L_2014_04_000000373132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/88/29/CETATEXT000028882977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 28/04/2014, 373132, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373132.20140428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 5 novembre et 5 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 2 août 2013 accordant son extradition aux autorités espagnoles ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros à la SCP Potier de la Varde, Buk-Lament, son avocat, sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu la convention établie sur la base de l'article K3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Vu la décision-cadre du Conseil de l'Union européenne du 13 juin 2002 relative au mandat d'arrêt européen et aux procédures de remise aux Etats membres ; <br/>
<br/>
              Vu le code pénal ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - Les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par le décret attaqué, le Premier ministre a accordé aux autorités espagnoles l'extradition de M. B...aux fins d'exécution d'une ordonnance du 29 décembre 2004 de la 2ème section de la chambre pénale de l'Audience nationale ayant ordonné le cumul des peines prononcées par des jugements du 2 décembre 1988, 30 novembre 1988 et 23 septembre 1989 rendus par la 2ème chambre de l'Audience nationale, condamnant l'intéressé à une peine de trente ans d'emprisonnement pour des faits de détention illégale avec utilisation illégale d'un véhicule appartenant à autrui, vol avec intimidation, contrefaçon de document officiel, appartenance à une bande organisée et armée, entrepôt d'armes de guerre, détention d'explosifs et attentat ;<br/>
<br/>
              2.	Considérant, en premier lieu, qu'il ressort des mentions de l'ampliation du décret attaqué versée au dossier, certifiée conforme par le secrétaire général du Gouvernement, que ce décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice ; que l'ampliation notifiée à M. B...n'avait pas à être revêtue de ces signatures ; <br/>
<br/>
              3.	Considérant, en deuxième lieu, que le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement ; qu'il satisfait ainsi à l'exigence de motivation prévue par l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; <br/>
<br/>
              4.	Considérant, en troisième lieu, qu'aux termes du second alinéa de l'article 1er des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition, l'extradition peut être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée ; que si M. B... fait valoir qu'il a bénéficié de mesures de libération en 1992 et 2004 pour raisons de santé alors qu'il était incarcéré, qu'il a été victime d'un arrêt cardiaque en 2004 et qu'il souffre de pathologies vasculaires veineuses, il ressort des pièces du dossier, notamment des résultats de l'examen médical auquel il a été procédé le 27 février 2012, que l'état de santé de l'intéressé ne fait obstacle ni à son extradition ni à l'incarcération qui en résultera ; qu'il n'y a pas de raison de douter, en l'état des éléments versés au dossier, que l'intéressé pourra faire l'objet en Espagne d'une surveillance médicale appropriée et, le cas échéant, d'une prise en charge adaptée à l'évolution de son état de santé, comme il a déjà pu en bénéficier lors de ses précédentes incarcérations en Espagne ; que, dans ces conditions, le Premier ministre n'a pas commis d'erreur manifeste d'appréciation en refusant de mettre en oeuvre la possibilité de refuser l'extradition résultant des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition ; que, dans ces conditions, le moyen tiré de ce que l'extradition exposerait l'intéressé à des traitements contraires à l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit aussi être écarté ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font en conséquence obstacle à ce qu'il soit fait droit aux conclusions présentées par son avocat sur le fondement de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
