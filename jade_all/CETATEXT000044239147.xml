<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044239147</ID>
<ANCIEN_ID>JG_L_2021_10_000000449811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/91/CETATEXT000044239147.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/10/2021, 449811, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449811.20211022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Société d'aménagement urbain et lotissement (SAUL) a demandé au tribunal administratif de Marseille de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos les 31 décembre 2012, 2013 et 2014, des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période correspondant à ces exercices, ainsi que des pénalités correspondantes.<br/>
<br/>
              Par un jugement n° 1803589 du 18 décembre 2019, ce tribunal a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 20MA00636 du 17 décembre 2020, la cour administrative d'appel de Marseille a rejeté l'appel formé par la Société d'aménagement urbain et lotissement contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 février et 17 mai 2021 au secrétariat du contentieux du Conseil d'État, la Société d'aménagement urbain et lotissement demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de la Société d'aménagement urbain et lotissement ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la Société d'aménagement urbain et lotissement soutient que la cour administrative d'appel de Marseille :<br/>
              - a commis une erreur de droit en jugeant que l'administration n'avait pas manqué à son obligation de loyauté dans le déroulement de la procédure d'imposition ;<br/>
              - l'a insuffisamment motivé et a méconnu l'article 268 du code général des impôts en jugeant que, pour établir le prix de revient au mètre carré intervenant dans le calcul de la taxe sur la valeur ajoutée basée sur la marge, applicable aux ventes des lots litigieux, il convenait de retenir la totalité de la surface du terrain initialement acquis, y compris la part de celui-ci cédée gratuitement à la commune pour établir la voirie desservant ces lots ;<br/>
              - a dénaturé les pièces du dossier, commis une erreur de droit et inexactement qualifié les faits en jugeant que l'attribution, au prix de 247 500 euros, de quatre lots aux sociétés civiles immobilières Satin, Tamaris, Centaure et Minotaure procédait d'un acte anormal de gestion ;<br/>
              - a inexactement qualifié les faits et commis une erreur de droit en jugeant que les impositions supplémentaires auxquelles elle a été assujettie devaient être assorties d'une majoration pour manquement délibéré.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur la demande en décharge des rappels de taxe sur la valeur ajoutée qui ont été réclamés à la Société d'aménagement urbain et lotissement ainsi que des pénalités correspondantes. En revanche, aucun des moyens n'est de nature à permettre l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la Société d'aménagement urbain et lotissement dirigées contre l'arrêt du 17 décembre 2020 de la cour administrative d'appel de Marseille en tant qu'il s'est prononcé sur sa demande en décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés ainsi que des pénalités correspondantes sont admises.<br/>
Article 2 : Le surplus des conclusions de la Société d'aménagement urbain et lotissement ne sont pas admises.<br/>
Article 3 : La présente décision sera notifiée à la société à responsabilité limitée Société d'aménagement urbain et lotissement et au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 16 septembre 2021 où siégeaient : M. Pierre Collin, Président de chambre, Présidant ; M. Jean-Claude Hassan, Conseiller d'Etat et M. Hervé Cassagnabère, Conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 22 octobre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Hervé Cassagnabère<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
