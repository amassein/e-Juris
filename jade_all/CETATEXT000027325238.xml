<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027325238</ID>
<ANCIEN_ID>JG_L_2013_03_000000345411</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/32/52/CETATEXT000027325238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 22/03/2013, 345411, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345411</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:345411.20130322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 décembre 2010 et 25 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0809431 du 20 octobre 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant, d'une part, à l'annulation de la décision du maire de Paris du 2 juin 2008 refusant de reconnaître l'imputabilité de sa maladie au service ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ou, à titre subsidiaire, d'ordonner une expertise afin que puisse être confirmée l'existence d'un lien de causalité direct entre sa pathologie et le rappel de vaccin contre l'hépatite B dont elle a fait l'objet en octobre 1997 ; <br/>
<br/>
              3°) de mettre à la charge de la ville de Paris une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de Mme A...et de Me Foussard, avocat de la ville de Paris,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A...et à Me Foussard, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au tribunal administratif que MmeA..., agent spécialisé des écoles maternelles de la ville de Paris, a reçu, en vue de l'obtention du certificat d'aptitude professionnelle " petite enfance " et de son recrutement, trois injections du vaccin contre l'hépatite B les 29 juillet, 2 septembre et 14 octobre 1996 et un rappel le 31 octobre 1997 ; qu'à la suite d'une biopsie musculaire ayant révélé qu'elle souffrait de lésions focales de myofasciite à macrophages, elle a été placée en congé de longue maladie à compter du 22 novembre 2002, puis en disponibilité d'office pour raison de santé à compter du 1er février 2007 ; que, par une décision du 21 mars 2008, confirmée le 2 juin suivant, le maire de Paris a refusé de reconnaître l'imputabilité de cette maladie au service ; que Mme A... se pourvoit en cassation contre le jugement du 20 octobre 2010 par lequel le tribunal administratif de Paris a rejeté sa requête tendant à l'annulation de cette décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / Toutefois, si la maladie provient (...) d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident. (...) / 3° A des congés de longue maladie d'une durée maximale de trois ans dans le cas où il est constaté que la maladie met l'intéressé dans l'impossibilité d'exercer ses fonctions, rend nécessaire un traitement et des soins prolongés et présente un caractère invalidant et de gravité confirmé. Le fonctionnaire conserve l'intégralité de son traitement pendant un an ; le traitement est réduit de moitié pendant les deux années qui suivent. L'intéressé conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. (...) / Les dispositions des deuxième, troisième et quatrième alinéas du 2° du présent article sont applicables  aux congés de longue maladie (...) " ; <br/>
<br/>
              3. Considérant que, pour apprécier si une maladie est imputable au service, il y a lieu de prendre en compte le dernier état des connaissances scientifiques, lesquelles peuvent être de nature à révéler la probabilité d'un lien entre une affection et le service, alors même qu'à la date à laquelle l'autorité administrative a pris sa décision, l'état de ces connaissances excluait une telle possibilité ; qu'il ressort des pièces du dossier soumis au tribunal administratif que, dans le dernier état des connaissances scientifiques, et alors même que cet état aurait été postérieur à la décision attaquée, des études scientifiques récentes n'avaient ni exclu, ni estimé comme très faiblement probable l'existence d'un lien entre les injections d'un vaccin contenant de l'aluminium, la présence de lésions musculaires caractéristiques à l'emplacement des injections et la combinaison de fatigue chronique, douleurs articulaires et musculaires, troubles du sommeil et troubles cognitifs, symptômes de la myofasciite à macrophages ; que, dès lors, en estimant que l'état des connaissances scientifiques excluait tout lien entre la présence de lésions histologiques à l'emplacement des injections vaccinales et un syndrome clinique spécifiques, le tribunal administratif a dénaturé les pièces du dossier qui lui était soumis ; que son jugement doit, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris le versement à Mme A...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeA..., qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 20 octobre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : La ville de Paris versera à Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la ville de Paris au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
