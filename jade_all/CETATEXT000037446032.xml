<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037446032</ID>
<ANCIEN_ID>JG_L_2018_09_000000409364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/44/60/CETATEXT000037446032.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 28/09/2018, 409364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409364.20180928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 409364, par une requête et un mémoire en réplique, enregistrés les 29 mars 2017 et 29 mars 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Halte à l'Obsolescence Programmée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire a rejeté sa demande tendant à la modification de l'article D. 111-4 du code de la consommation, de façon à rendre légal cet article et à assurer la pleine effectivité de l'article L. 111-4 du même code ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier en conséquence les dispositions de l'article D. 111-4 du code de la consommation dans un délai d'un mois à compter de la décision à intervenir, sous astreinte de 2 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 409463, par une requête et un mémoire en réplique, enregistrés le 31 mars 2017 et le 12 juin 2018 au secrétariat du contentieux du Conseil d'Etat, l'association UFC Que Choisir demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle la secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire a rejeté sa demande tendant à la modification de l'article D. 111-4 du code de la consommation, de façon à rendre légal cet article et à assurer la pleine effectivité de l'article L. 111-4 du même code ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier en conséquence les dispositions de l'article D. 111-4 du code de la consommation dans un délai d'un mois à compter de la décision à intervenir, sous astreinte de 2 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la consommation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 septembre 2018, présentée par l'association Halte à l'obsolescence programmée ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Les requêtes visées ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 111-4 du code de la consommation : " Le fabricant ou l'importateur de biens meubles informe le vendeur professionnel de la période pendant laquelle ou de la date jusqu'à laquelle les pièces détachées indispensables à l'utilisation des biens sont disponibles sur le marché. Cette information est délivrée obligatoirement au consommateur par le vendeur de manière lisible avant la conclusion du contrat et confirmée par écrit lors de l'achat du bien. / Dès lors qu'il a indiqué la période ou la date mentionnées au premier alinéa, le fabricant ou l'importateur fournit obligatoirement, dans un délai de deux mois, aux vendeurs professionnels ou aux réparateurs, agréés ou non, qui le demandent les pièces détachées indispensables à l'utilisation des biens vendus. / Les modalités d'application du présent article sont précisées par décret ". Aux termes de l'article D. 111-4 du même code : " En application des dispositions du premier alinéa de l'article L. 111-4, l'information délivrée par le fabricant ou l'importateur de biens meubles au vendeur professionnel, portant sur la période pendant laquelle ou la date jusqu'à laquelle les pièces détachées indispensables à l'utilisation d'un bien sont disponibles, figure sur tout document commercial ou sur tout support durable accompagnant la vente de biens meubles. / Cette information est portée à la connaissance du consommateur par le vendeur, de manière lisible, avant la conclusion de la vente, sur tout support adapté. Elle figure, également, sur le bon de commande s'il existe, ou sur tout autre support durable constatant ou accompagnant la vente ". <br/>
<br/>
              3. En premier lieu, il ressort, d'une part, des termes mêmes de l'article L. 111-4 du code de la consommation que, si les fabricants ou importateurs de biens pour lesquels des pièces détachées indispensables à leur utilisation sont disponibles sur le marché sont tenus d'informer les vendeurs professionnels de la période pendant laquelle ou de la date jusqu'à laquelle cette disponibilité sera assurée, ils n'ont, en revanche, pas l'obligation d'informer les vendeurs de ce que les pièces en cause ne sont pas disponibles. L'absence d'information de leur part vaut alors indication de ce que les pièces détachées indispensables à l'utilisation du bien ne sont pas disponibles. Il ressort, d'autre part, des mêmes dispositions que les fabricants ou importateurs doivent fournir ces pièces aux vendeurs professionnels ou réparateurs dans un délai de deux mois à compter de leur demande. Il suit de là que le moyen tiré de ce que le décret qu'il est demandé de modifier serait entaché d'erreur de droit, faute d'avoir prévu l'obligation d'informer les vendeurs professionnels de ce que les pièces détachées indispensables à l'utilisation d'un bien ne sont pas disponibles et d'avoir précisé le point de départ du délai de fourniture des pièces détachées, ne peut qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, il ressort de l'économie et de la finalité de ces dispositions, qui est d'assurer une information exacte et complète du consommateur quant à la durée pendant laquelle les pièces détachées sont disponibles, que le législateur a nécessairement entendu que les fabricants ou importateurs, lorsque l'information qu'ils délivrent fait référence à une période de disponibilité de ces pièces, indiquent aux vendeurs professionnels quel est le point de départ de cette période, à charge pour ces derniers de répercuter, sur les consommateurs, cette information. Cette période ne commence pas nécessairement à courir à la date d'achat du bien par les consommateurs. En conséquence, le moyen tiré de ce que le dispositif d'information ainsi mis en place serait incomplet et ineffectif, faute que le pouvoir réglementaire ait apporté les précisions nécessaires, entachant ainsi l'article D. 111-4 du code de la consommation d'erreur de droit, ne peut qu'être écarté, les précisions en cause résultant de la loi elle-même.<br/>
<br/>
              5. En troisième lieu, les dispositions de l'article D. 111-4 du code de la consommation citées au point 3 prévoient que l'information sur la période de disponibilité des pièces détachées est délivrée au vendeur professionnel, sur tout document commercial ou sur tout support durable accompagnant la vente de biens meubles et à l'égard du consommateur, sur tout support adapté, sur le bon de commande s'il existe ou sur tout autre support durable constatant ou accompagnant la vente. Pour assurer l'effectivité des dispositions de la loi, le pouvoir réglementaire n'était pas tenu de détailler davantage les modalités concrètes d'information du consommateur. Le moyen tiré de ce que ces dispositions seraient, pour ce motif, entachées d'erreur de droit doit donc être écarté. <br/>
<br/>
              7. Il résulte de ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation de la décision implicite de rejet de leur demande de modification de l'article D. 111-4 du code de la consommation. Leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association Halte à l'obsolescence programmée et de l'association UFC Que Choisir sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'association Halte à l'obsolescence programmée, l'association UFC Que Choisir, au Premier ministre et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
