<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230114</ID>
<ANCIEN_ID>JG_L_2012_07_000000343282</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230114.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/07/2012, 343282, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343282</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:343282.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 septembre et 14 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Henry Ferdinand B, demeurant ... ; M. B demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0702328 du 22 avril 2010 par lequel le tribunal administratif de Nice a rejeté sa demande tendant à la décharge de la taxe locale d'équipement, de la taxe pour le financement des dépenses des conseils d'architecture, d'urbanisme et d'environnement et de la taxe départementale des espaces naturels sensibles, ainsi que des amendes fiscales correspondantes, auxquelles il a été assujetti par un avis d'imposition du 26 février 2004 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de M. B,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de M. B ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B était le gérant de la société civile " Club 777 ", propriétaire de l'hôtel " Maeterlink " situé à Nice ; que par un procès-verbal du 15 juin 2001, la direction départementale de l'équipement (DDE) des Alpes-Maritimes a constaté que des travaux réalisés dans l'hôtel avaient abouti à la création d'une surface hors oeuvre nette supplémentaire de 941 m², en méconnaissance de précédents permis de construire et sans avoir fait l'objet d'une autorisation d'urbanisme ; que cette direction a, en conséquence, adressé au requérant le 26 février 2004 un avis d'imposition mettant à sa charge la taxe locale d'équipement, la taxe pour le financement des dépenses des conseils d'architecture, d'urbanisme et de l'environnement et la taxe départementale des espaces naturels sensibles, assorties de pénalités, pour un montant total de 67 820 euros ; que M. B se pourvoit en cassation contre le jugement du 22 avril 2010 par lequel le tribunal administratif de Nice a rejeté sa demande tendant à la décharge de ces taxes et pénalités ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 274 A du livre des procédures fiscales, alors applicable : " En ce qui concerne la taxe locale d'équipement, l'action en recouvrement de l'administration s'exerce jusqu'à l'expiration de la quatrième année suivant celle au cours de laquelle soit le permis de construire a été délivré ou la déclaration de construction déposée, soit le procès-verbal constatant une infraction a été établi " ; qu'aux termes du premier alinéa de l'article L. 189 de ce livre, dans sa rédaction alors applicable : " La prescription est interrompue par la notification d'une proposition de redressement, par la déclaration ou la notification d'un procès-verbal, de même que par tout acte comportant reconnaissance de la part des contribuables et par tous les autres actes interruptifs de droit commun " ;<br/>
<br/>
              3. Considérant, ainsi que le Conseil d'Etat statuant au contentieux l'a jugé par une décision n° 305835 du 16 avril 2010, que les dispositions de l'article L. 274 A du livre des procédures fiscales ont pour objet d'imposer à l'ordonnateur un délai maximum à compter du fait générateur de la taxe pour émettre, à peine de prescription, le titre de recettes, et non pas de fixer au comptable le délai maximum dans lequel il peut procéder au recouvrement des sommes mentionnées sur le titre de recettes ; que pour émettre l'avis de mise en recouvrement, l'ordonnateur dispose ainsi d'un délai qui s'achève à l'expiration de la quatrième année suivant celle de l'achèvement des travaux, en cas d'absence d'autorisation de construire, et qui peut être interrompu dans les conditions prévues par les dispositions de l'article L. 189 du livre des procédures fiscales, notamment par la notification d'un procès-verbal ;<br/>
<br/>
              4. Considérant qu'il suit de là qu'en jugeant que la date d'achèvement des travaux était sans incidence sur le calcul du délai de prescription prévu à l'article L. 274 A du livre des procédures fiscales, alors que cette date constituait, en l'espèce, le fait générateur des taxes en litige en raison de l'absence d'autorisation de construire et fixait ainsi le point de départ de la prescription, le tribunal administratif de Nice a commis une erreur de droit ; que dès lors, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son jugement doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros à M. B au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 22 avril 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nice.<br/>
Article 3 : L'Etat versera à M. B une somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Henry Ferdinand B et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
