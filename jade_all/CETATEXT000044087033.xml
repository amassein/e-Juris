<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087033</ID>
<ANCIEN_ID>JG_L_2021_08_000000455761</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087033.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 30/08/2021, 455761, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455761</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455761.20210830</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 20 août 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement des articles L. 521-1 et L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre tous les décrets du Premier ministre pris dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 sur le fondement du II de l'article 1er, des III, IV et V de l'article 3, du 1er alinéa de l'article 4 et du 1er alinéa de l'article 11 de la loi n° 2021-689 du 31 mai 2021 ; <br/>
<br/>
              2°) de suspendre tous les décrets du Premier ministre pris dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 sur le fondement de l'article 4 de la loi n° 2021-1040 du 5 août 2021 ; <br/>
<br/>
              3°) de suspendre tous les décrets du Premier ministre pris dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 sur le fondement de l'alinéa 3 du I de l'article 11 de la loi n° 2020-546 du 11 mai 2020 ; <br/>
<br/>
              4°) de suspendre tous les décrets du Premier ministre pris dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 sur le fondement des articles 12 à 20 de la loi n° 2021-1040 du 5 août 2021.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle a intérêt à agir dès lors qu'elle a la qualité de parent ; <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ;<br/>
              - les mesures contestées méconnaissent le droit constitutionnel à la santé, le principe de précaution et l'exigence de protection de l'intérêt supérieur des enfants ;<br/>
              - ces mesures méconnaissent le principe d'égalité et l'interdiction de toute discrimination dès lors que, en premier lieu, l'obligation vaccinale s'applique aux seuls soignants, en deuxième lieu, les travailleurs qui ne présentent pas de " passe sanitaire " sont exposés à une suspension de leur rémunération, en troisième lieu, les personnes n'étant pas en possession d'un " passe sanitaire " font l'objet de restrictions dans l'exercice de leur liberté d'aller et venir, et en dernier lieu, il existe une différence de traitement entre les commerces selon que leur activité s'exerce au sein ou en dehors des centres commerciaux ; <br/>
              - elles portent atteinte à la liberté d'aller et venir dès lors qu'elles s'appliquent aux activités de loisirs et de restauration sans distinction selon leurs conditions d'exercice ainsi qu'à toutes personnes de plus de 12 ans et qu'elles subordonnent l'accès aux transports publics à la présentation du " passe sanitaire " ; <br/>
              - elles portent atteinte au droit au respect de la vie privée et au droit d'expression collective des idées et des opinions dès lors que les données relatives à la santé sont conservées au sein des systèmes d'information mis en œuvre aux fins de lutter contre l'épidémie de covid-19.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le décret n° 2020-551 du 12 mai 2020 ;<br/>
              - le décret n° 2020-1690 du 25 décembre 2020 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le décret n° 2021-901 du 6 juillet 2021 ;<br/>
              - le décret n° 2021-1056 du 7 août 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Par une requête en " référé liberté et suspension ", Mme A... demande au juge des référés du Conseil d'Etat de " suspendre tous les décrets du Premier ministre pris dans l'intérêt de la sante´ publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 " pour l'application des dispositions de cette loi.<br/>
<br/>
              3. Aux termes du A du II de l'article 1er de la loi du 31 mai 2021 relative à la gestion de la sortie de la crise sanitaire, modifiée par la loi du 5 août 2021 relative à la gestion de la crise sanitaire : " A compter du 2 juin 2021 et jusqu'au 15 novembre 2021 inclus, le Premier ministre peut, par décret pris sur le rapport du ministre chargé de la santé, dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 : 1° Imposer aux personnes âgées d'au moins douze ans souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution, ainsi qu'aux personnels intervenant dans les services de transport concernés, de présenter le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, un justificatif de statut vaccinal concernant la covid-19 ou un certificat de rétablissement à la suite d'une contamination par la covid-19 ; / 2° Subordonner à la présentation soit du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, soit d'un justificatif de statut vaccinal concernant la covid-19, soit d'un certificat de rétablissement à la suite d'une contamination par la covid-19 l'accès à certains lieux, établissements, services ou évènements où sont exercées les activités suivantes : / a) Les activités de loisirs ; / b) Les activités de restauration commerciale ou de débit de boissons, à l'exception de la restauration collective, de la vente à emporter de plats préparés et de la restauration professionnelle routière et ferroviaire ; / c) Les foires, séminaires et salons professionnels ; / d) Sauf en cas d'urgence, les services et établissements de santé, sociaux et médico-sociaux, pour les seules personnes accompagnant ou rendant visite aux personnes accueillies dans ces services et établissements ainsi que pour celles qui y sont accueillies pour des soins programmés. La personne qui justifie remplir les conditions prévues au présent 2° ne peut se voir imposer d'autres restrictions d'accès liées à l'épidémie de covid-19 pour rendre visite à une personne accueillie et ne peut se voir refuser l'accès à ces services et établissements que pour des motifs tirés des règles de fonctionnement et de sécurité de l'établissement ou du service, y compris de sécurité sanitaire ; / e) Les déplacements de longue distance par transports publics interrégionaux au sein de l'un des territoires mentionnés au 1° du présent A, sauf en cas d'urgence faisant obstacle à l'obtention du justificatif requis ; / f) Sur décision motivée du représentant de l'Etat dans le département, lorsque leurs caractéristiques et la gravité des risques de contamination le justifient, les grands magasins et centres commerciaux, au-delà d'un seuil défini par décret, et dans des conditions garantissant l'accès des personnes aux biens et services de première nécessité ainsi, le cas échéant, qu'aux moyens de transport. / Cette réglementation est rendue applicable au public et, à compter du 30 août 2021, aux personnes qui interviennent dans ces lieux, établissements, services ou évènements lorsque la gravité des risques de contamination en lien avec l'exercice des activités qui y sont pratiquées le justifie, au regard notamment de la densité de population observée ou prévue. / Cette réglementation est applicable aux mineurs de plus de douze ans à compter du 30 septembre 2021 ". Il résulte en outre des articles 12 à 19 de la loi du 5 août 2021 une obligation de vaccination pour les professionnels de santé et les personnes exerçant leur activité dans des établissements relevant du secteur médico-social. Quatre décrets ont été pris le 7 août 2021 pour l'application de ces dispositions législatives, dont trois modifient des décrets antérieurs.<br/>
<br/>
              4. Si Mme Coti soutient, d'une part, que l'obligation générale de présenter le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19, un justificatif de statut vaccinal concernant la covid-19 ou un certificat de rétablissement à la suite d'une contamination par la covid-19 pour emprunter certains transports en commun et accéder à certains lieux, établissements, services ou évènements, d'autre part, que l'obligation faite aux seuls personnels des services de santé d'être vaccinés contre la covid-19, méconnaissent le principe de précaution, faute pour ces obligations d'avoir été précédées d'études indépendantes, le droit constitutionnel à la protection de la santé, le principe d'égalité entre les citoyens, selon qu'ils peuvent ou non présenter ce justificatif et entre les salariés selon qu'ils doivent ou non être vaccinés, portent atteinte à la liberté d'aller et venir, à la liberté d'expression et de manifestation de ses opinions ainsi qu'au droit au respect de la vie privée, notamment en raison de la conservation de données de santé dans des systèmes d'information, elle se borne à invoquer ces droits et libertés de manière générale, sans indiquer la ou les dispositions des décrets dont elle demande la suspension qui leur porteraient une atteinte illégale, alors que le principe et le champ d'application tant de l'obligation de présenter ce justificatif pour l'accès à certains lieux, établissements, services ou évènements que de l'obligation pour les personnels des services de santé d'être vaccinés résultent de la loi du 5 août 2021. Ainsi, la requérante ne saurait être regardée comme faisant état à l'encontre des décrets dont elle demande la suspension ni d'un moyen propre à créer un doute sérieux quant à leur légalité, ni de ce qu'ils porteraient une atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
              5. Il résulte de ce qui précède que la requête de Mme Coti ne peut, par suite, et sans qu'il soit besoin de se prononcer ni sur sa recevabilité ni sur la condition d'urgence, qu'être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme Coti est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme Sophie Coti. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
