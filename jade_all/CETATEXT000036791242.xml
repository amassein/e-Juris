<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791242</ID>
<ANCIEN_ID>JG_L_2018_04_000000415483</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791242.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 11/04/2018, 415483, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415483</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415483.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a, le 6 novembre 2017, saisi le Conseil d'Etat en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 30 octobre 2017 rejetant le compte de campagne de M. A...D..., tête de liste " En marche vers le progrès " pour les élections au conseil territorial de Saint-Martin qui se sont déroulées les 19 et 26 mars 2017. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...D...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
<br/>
              1. Par une décision du 30 octobre 2017, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. D..., candidat tête de la liste " En marche vers le progrès " élu, le 26 mars 2017,  conseiller territorial de Saint-Martin. Ce rejet est motivé par l'existence d'une minoration des dépenses d'impression à hauteur de la somme de 3 316 euros, correspondant à 15,35 % du montant des dépenses figurant dans le compte de campagne et 13,65 % du plafond des dépenses autorisées dans la circonscription, conduisant au dépassement de ce plafond à concurrence de 625 euros. La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le Conseil d'Etat, en application de l'article L 52-15 du code électoral, applicable de plein droit aux élections des conseillers territoriaux de la collectivité d'outre-mer de Saint-Martin. <br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              2. Aux termes de l'article L. 52-12 du code électoral, applicable de plein droit aux élections des conseillers territoriaux de la collectivité d'outre-mer de Saint-Martin : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. La même obligation incombe au candidat ou au candidat tête de liste dès lors qu'il a bénéficié de dons de personnes physiques conformément à l'article L. 52-8 du présent code selon les modalités prévues à l'article 200 du code général des impôts. (...) Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit. / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. (...) ". <br/>
<br/>
              3. Il résulte de l'instruction qu'une somme de 9 296 euros a été inscrite, au titre des frais d'impression, dans le compte de campagne de M.D.... Ni les explications fournies par M. D...ni les factures relatives à ces dépenses de campagne produites par l'imprimeur en complément de son attestation du 28 juillet 2017 ne remettent en cause le calcul effectué par la CNCCFP sur le fondement des pièces justificatives jointes au compte de campagne déposé le 6 mai 2017. Il résulte de ces documents que les dépenses engagées par le candidat au titre des frais d'impression, hors ceux de la campagne officielle qui ne doivent pas figurer au compte de campagne, se sont élevées à la somme totale de 12 612 euros. Le compte de campagne de M. D... présente donc une minoration de ces dépenses à hauteur de la somme de 3 316 euros, ce qui correspond à 15,35 % du montant des dépenses engagées et 13,65 % du plafond des dépenses autorisées dans la circonscription. La réintégration de cette somme a en outre pour effet de porter le montant total des dépenses à 24 913 euros, soit 625 euros de plus que ce plafond. Dès lors, c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. D...par une décision du 30 octobre 2017. Par suite, celle-ci n'a pas droit au remboursement budgétaire de l'Etat en vertu de l'article L. 52-11-1 du code électoral.<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              4. Aux termes de l'article L. 118-3 du code électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / (...) Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. (...)".  En dehors des cas de fraude, le juge de l'élection ne prononce l'inéligibilité d'un candidat dont le compte de campagne a été rejeté à bon droit que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              5. L'omission de la somme de 3 316 euros, constatée dans le compte de campagne de M. D...au titre des dépenses représente 15,35 % du montant des dépenses engagées  et 13,65 % du plafond des dépenses autorisées dans la circonscription. La réintégration de cette somme aboutit par ailleurs au dépassement de ce plafond. M.D..., expert-comptable de son état, homme politique expérimenté, conseiller territorial de Saint-Martin élu en 2007 et 2012, ne peut valablement soutenir que cette erreur provient d'une présentation imprécise du compte et que celle-ci est de pure forme. Il doit dès lors être regardé comme ayant méconnu, de manière délibérée, une règle substantielle de financement des campagnes électorales. Il a ainsi commis un manquement d'une particulière gravité aux règles de financement des campagnes électorales.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il y a lieu, en application de l'article L. 118-3 du code électoral, de déclarer M. D...inéligible à tout mandat pendant un an à compter du jour de la présente décision et de le déclarer démissionnaire d'office de son mandat de conseiller territorial.<br/>
<br/>
              7. Aux termes du quatrième alinéa de l'article LO 524 du code électoral : " La constatation par le Conseil d'Etat de l'inéligibilité d'un ou de plusieurs candidats n'entraîne l'annulation de l'élection que du ou des élus déclarés inéligibles. Le Conseil d'Etat proclame en conséquence l'élection du ou des suivants de liste ". En application de ces dispositions, il y a lieu de proclamer élue conseillère territoriale Mme C...B..., inscrite sur la liste conduite par M. D...immédiatement après le dernier élu de cette liste.<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le compte de campagne de M. D...a été rejeté à bon droit.<br/>
Article 2 : M. D...n'a pas droit au remboursement budgétaire de l'Etat en application de l'article L. 52-11-1.<br/>
Article 3 : M. A...D...est déclaré inéligible pour une durée d'un an à compter de la présente décision et démissionnaire d'office de son mandat de conseiller territorial. <br/>
Article 4 : Mme C...B...est proclamée élue en qualité de conseillère territoriale.<br/>
Article 5 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques, à M. A...D..., à la ministre des outre-mer et à Mme C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
