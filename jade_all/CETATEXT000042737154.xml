<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737154</ID>
<ANCIEN_ID>JG_L_2020_12_000000431085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737154.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 23/12/2020, 431085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431085.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée unipersonnelle Adventus Software a demandé au tribunal administratif de Pau d'annuler la décision du 31 janvier 2014 par laquelle le préfet de la région Aquitaine a rejeté les dépenses non justifiées d'actions de formation à hauteur de 75 088 euros au titre des exercices 2011 et 2012 sur le fondement de l'article L. 6362-5 du code du travail, a mis à sa charge le versement d'une somme de ce montant au Trésor public sur le fondement de l'article L. 6362-7 de ce code, ainsi que la somme de 51 235 euros sur le fondement de l'article L. 6362-7-1 du même code et la somme de 13 115 euros sur le fondement de l'article L. 6362-7-2 de ce code. Par un jugement n° 1400664 du 13 septembre 2016, le tribunal administratif de Pau a réduit de 16 555 euros la somme mise à la charge de la société Adventus Software au titre des articles L. 6362-5 et L. 6362-7 du code du travail, a, en conséquence, réformé les dispositions de la même décision relatives aux sommes dues au Trésor public et a rejeté le surplus des conclusions de la demande de la société. <br/>
<br/>
              Par un arrêt n° 16BX03658 du 1er avril 2019, la cour administrative d'appel de Bordeaux a, sur l'appel de la société Adventus Software, annulé la décision du préfet de la région Aquitaine du 31 janvier 2014 en toutes ses dispositions et réformé le jugement du tribunal administratif dans cette mesure.  <br/>
<br/>
              Par un pourvoi, enregistré le 27 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre du travail demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Adventus Software.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code du travail ; <br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme A... B..., rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite du contrôle par les services de l'Etat des activités de formation professionnelle continue menées par la société Adventus Software, le préfet de la région Aquitaine a, par décision du 31 janvier 2014, rejeté les dépenses non justifiées d'actions de formation de cet organisme à hauteur de 75 088 euros au titre des exercices 2011 et 2012 en application de l'article L. 6362-5 du code du travail, mis à sa charge le versement au Trésor public d'une somme de ce montant sur le fondement de l'article L. 6362-7 de ce code, de la somme de 51 235 euros, correspondant aux remboursements non effectués auprès de ses cocontractants, sur le fondement de l'article L. 6362-7-1 du même code, et de celle de 13 115 euros sur le fondement de l'article L. 6362-7-2 de ce code, pour avoir établi et utilisé intentionnellement des documents portant des mentions inexactes en vue d'obtenir indûment des paiements. Par un arrêt du 1er avril 2019, contre lequel la ministre du travail se pourvoit en cassation, la cour administrative d'appel de Bordeaux a annulé la décision du 31 janvier 2014 et réformé dans cette mesure le jugement du tribunal administratif de Pau, qui n'avait fait que partiellement droit aux conclusions de la société tendant à l'annulation de cette décision.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 6361-2 du code du travail, dans sa rédaction applicable au litige : " L'Etat exerce un contrôle administratif et financier sur : / 1° Les activités en matière de formation professionnelle continue conduites par : (...) / c) Les organismes de formation (...) ". Aux termes de l'article L. 6362-8 du même code, dans sa rédaction applicable : " Les contrôles en matière de formation professionnelle continue peuvent être opérés soit sur place, soit sur pièces ".   L'article L. 6362-9 de ce code prévoit que : " Les résultats du contrôle sont notifiés à l'intéressé (...) ". Aux termes de l'article L. 6362-10 du même code dans sa rédaction applicable : " Les décisions de rejet de dépenses et de versement mentionnées au présent livre prises par l'autorité administrative ne peuvent intervenir, après la notification des résultats du contrôle, que si une procédure contradictoire a été respectée ". Enfin, l'article R. 6362-1 du code du travail dispose, dans sa rédaction alors applicable, que : " Les personnes et organismes mentionnés aux articles L. 6361-1 et L. 6361-2, 1°, qui ont fait l'objet d'un contrôle sur place, sont informés de la fin de la période d'instruction par lettre recommandée avec avis de réception (...) ", l'article R. 6362-3 de ce code que : " Les résultats des contrôles prévus aux articles L. 6361-1 à L. 6361-3 sont notifiés à l'intéressé avec l'indication du délai dont il dispose pour présenter des observations écrites et demander, le cas échéant, à être entendu. (...) " et l'article R. 6362-4 du même code que : " La décision du ministre chargé de la formation professionnelle ou du préfet de région ne peut être prise qu'au vu des observations écrites et après audition, le cas échéant, de l'intéressé (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions que, si les décisions de rejet de dépenses et de versement prises par l'autorité administrative ne peuvent intervenir sans qu'une procédure contradictoire ait été respectée, la mise en oeuvre de celle-ci n'est imposée qu'après la notification des résultats du contrôle. La légalité de la décision de rejet ou de versement prise n'est ainsi pas subordonnée au respect d'une telle procédure durant la conduite des opérations de contrôle. <br/>
<br/>
              4. D'autre part, en vertu de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, alors en vigueur, dont les dispositions sont désormais reprises à l'article L. 122-1 du code des relations entre le public et l'administration, la personne contrôlée peut, lors de la mise en oeuvre de la procédure contradictoire, " se faire assister par un conseil ou représenter par un mandataire de son choix ", de même d'ailleurs que dès le déroulement du contrôle en application de l'article 6 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques, aux termes duquel : " Les avocats peuvent assister et représenter autrui devant les administrations publiques, sous réserve des dispositions législatives et réglementaires ", dès lors que cette assistance, qu'aucune disposition n'exclut, n'est pas incompatible avec son déroulement. Il n'en résulte toutefois pas que l'administration, à laquelle aucune disposition n'impose d'informer l'organisme de formation, ni du contrôle qu'elle va opérer, ni de sa faculté de se faire assister par un avocat lors de ce contrôle ou lors de la procédure contradictoire conduite après la notification de ses résultats, devrait, à peine d'irrégularité, faire droit à une demande de report du contrôle présentée par le conseil de l'organisme de formation, la circonstance que l'administration ait averti ce dernier de ce contrôle ou l'ait informé de son droit d'être assisté étant sans incidence à cet égard.<br/>
<br/>
              5. Par suite, en déduisant de ce que l'administration  avait, le 11 avril 2013, informé la société requérante du contrôle qu'elle allait mener sur place à compter du 2 mai 2013, date arrêtée avec le gérant invité à faire connaître toute indisponibilité, et de son droit à être assistée par le conseil de son choix, que l'administration avait entendu se soumettre elle-même à une procédure mettant en oeuvre les droits de la défense lors des opérations de contrôle et qu'elle aurait commis une irrégularité en ne faisant pas droit à la demande de report présentée le 25 avril 2013 par l'avocat de la société au motif de son empêchement à la date prévue, la cour a commis une erreur de droit. <br/>
<br/>
              6. Il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la ministre du travail est fondée à demander l'annulation pour ce motif de l'arrêt qu'elle attaque.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er :  L'arrêt du 1er avril 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : La présente décision sera notifiée à la ministre du travail, de l'emploi et de l'insertion et à la société à responsabilité limitée unipersonnelle Adventus Software.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
