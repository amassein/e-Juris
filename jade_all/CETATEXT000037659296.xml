<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659296</ID>
<ANCIEN_ID>JG_L_2018_11_000000420343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/92/CETATEXT000037659296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/11/2018, 420343</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP GATINEAU, FATTACCINI ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:420343.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E...D..., M. J...B..., Mme K...B..., M. I...C..., M. F...A...et Mme L...A...ont demandé au juge des référés du tribunal administratif de La Réunion, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre au maire de Saint-Leu, dans un délai de quarante-huit heures à compter de l'ordonnance à intervenir et sous astreinte de 1 500 euros par jour de retard, de dresser procès-verbal de constat des infractions au code de l'urbanisme commises par M.M..., d'adopter un arrêté interruptif de travaux et d'en transmettre copie au procureur de la République près le tribunal de grande instance de Saint-Pierre, et d'enjoindre au préfet de La Réunion, à défaut d'intervention du maire de saint-Leu dans le délai prévu par l'ordonnance, de se substituer à celui-ci en prescrivant dans un délai de quarante-huit heures et sous astreinte de 1 500 euros par jour de retard, l'interruption des travaux et de transmettre copie de son arrêté au procureur de la République près le tribunal de grande instance de Saint-Pierre. Par une ordonnance n° 1701139 du 19 mars 2018, le juge des référés du tribunal administratif de La Réunion a fait droit à leur demande et fixé respectivement à quarante-huit heures, sous astreinte de 1 000 euros par jour de retard, et vingt-quatre heures les délais impartis pour prendre les actes en cause.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 3 et 18 mai et les 9 et 24 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. H...M...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de Mme E...D...et autres une somme globale de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, A..., Stoclet, avocat de M.M..., à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme D...et autres, et à la SCP Gatineau, Fattaccini, avocat de la commune de Saint-Leu ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 23 octobre 2014, le maire de la commune de Saint-Leu (La Réunion) a délivré à M. M...un permis de construire pour l'édification d'une maison individuelle ; que, par un courrier du 7 novembre 2017, M. et MmeG..., M. et MmeA..., M. et MmeB..., M. C...et Mme D...ont notamment demandé au maire de Saint-Leu d'établir un procès-verbal de mise en conformité de la construction au permis délivré ; que, par un courrier du 21 novembre 2017, ils ont demandé au maire de Saint-Leu de dresser un procès-verbal d'infraction au code de l'urbanisme, de prendre un arrêté interruptif de travaux et d'en transmettre copie au procureur de la République ; que ces deux demandes ont été implicitement rejetées par le maire de Saint-Leu ; que Mme D...et autres ont demandé au juge des référés du tribunal administratif de La Réunion, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre au maire de Saint-Leu, dans un délai de quarante-huit heures à compter de l'ordonnance à intervenir et sous astreinte de 1 500 euros par jour de retard, de dresser procès-verbal de constat des infractions au code de l'urbanisme commises par M.M..., de prendre un arrêté interruptif de travaux et d'en transmettre copie au procureur de la République près le tribunal de grande instance de Saint-Pierre et  d'enjoindre au préfet de La Réunion, à défaut d'intervention du maire de Saint-Leu dans le délai prévu par l'ordonnance, de se substituer à celui-ci en prescrivant dans un délai de quarante-huit heures et sous astreinte de 1500 euros par jour de retard, l'interruption des travaux et de transmettre copie de son arrêté au procureur de la République près le tribunal de grande instance de Saint-Pierre ; que, par une ordonnance du 19 mars 2018, le juge des référés du tribunal administratif de La Réunion a fait droit à leur demande et fixé respectivement à quarante-huit heures, sous astreinte de 1 000 euros par jour de retard, et vingt-quatre heures les délais impartis pour prendre les actes en cause ; que M. M...demande l'annulation de cette ordonnance ; <br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              3.	Considérant que l'article L. 480-1 du code de l'urbanisme donne compétence au maire pour commissionner des agents publics aux fins de constater les infractions aux dispositions du livre IV du code de l'urbanisme ; que le maire tient de l'article L. 480-2 de ce code le pouvoir d'ordonner l'interruption des travaux ; que, lorsqu'il exerce les attributions qui lui sont confiées par ces dispositions, le maire agit comme autorité de l'Etat ; que, dès lors, la commune ne justifie pas d'un intérêt la rendant recevable à intervenir devant le juge de cassation à l'appui du pourvoi dirigé contre l'ordonnance par laquelle le juge des référés, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, a enjoint au maire de faire usage des pouvoirs qu'il tient des articles L. 480-1 et L. 480-2 du code de l'urbanisme ; <br/>
<br/>
              Sur les conclusions aux fins de non-lieu présentées par Mme D...et autres : <br/>
<br/>
              4.	Considérant que la circonstance qu'en exécution de l'injonction prononcée à son encontre par une ordonnance rendue sur le fondement de l'article L. 521-3 du code de justice administrative, le maire de Saint-Leu a pris un arrêté interruptif de travaux, dressé un procès-verbal de constat d'infraction au code de l'urbanisme et notifié ces actes au procureur de la République près le tribunal de grande instance de Saint-Pierre ne rend pas sans objet les conclusions du requérant dirigées contre cette ordonnance ; que, dès lors, les conclusions aux fins de non-lieu présentées par Mme D...et autres doivent être rejetées ; <br/>
<br/>
              Sur les conclusions de M. M...:<br/>
<br/>
              5.	Considérant, en premier lieu, que le moyen tiré de ce que la minute de l'ordonnance attaquée ne serait pas revêtue de la signature du magistrat qui l'a rendue, requise par les dispositions de l'article R. 742-5 du code de justice administrative, manque en fait ; <br/>
<br/>
              6.	Considérant, en deuxième lieu, que, saisi sur le fondement de l'article L. 521-3 du code de justice administrative d'une demande qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, notamment sous forme d'injonctions adressées à l'administration, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse ; qu'en raison du caractère subsidiaire du référé régi par l'article L. 521-3, le juge saisi sur ce fondement ne peut prescrire les mesures qui lui sont demandées lorsque leurs effets pourraient être obtenus par les procédures de référé régies par les articles L. 521-1 et L 521-2 ; que si le juge des référés, saisi sur le fondement de l'article L. 521-3, ne saurait faire obstacle à l'exécution d'une décision administrative, même celle refusant la mesure demandée, à moins qu'il ne s'agisse de prévenir un péril grave, la circonstance qu'une décision administrative refusant la mesure demandée au juge des référés intervienne postérieurement à sa saisine ne saurait faire obstacle à ce qu'il fasse usage des pouvoirs qu'il tient de l'article L. 521-3 ; que ce motif, qui répond à un moyen invoqué devant le juge des référés et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs retenus par l'ordonnance attaquée, dont il justifie légalement le dispositif ; <br/>
<br/>
              7.	Considérant, en troisième lieu, qu'en relevant que les agents communaux s'étaient bornés à rédiger un rapport d'information faisant état des déclarations qui leur avaient été faites, le juge des référés a suffisamment répondu au moyen tiré de ce que ce rapport devait être regardé comme une invitation à régulariser les travaux à laquelle il ne pouvait être fait obstacle ; qu'il n'a pas commis d'erreur de droit sur ce point ; <br/>
<br/>
              8.	Considérant, en quatrième lieu, qu'en jugeant que la condition d'urgence était satisfaite au motif que les travaux entrepris sur le fondement de l'autorisation d'urbanisme délivrée à M. M...avaient déjà été engagés, le juge des référés, dont l'ordonnance est suffisamment motivée sur ce point, s'est livré à une appréciation souveraine des faits de l'espèce qui n'est entachée ni d'erreur de droit ni de dénaturation ; <br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que M. M...n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. M...une somme globale de 3 000 euros à verser à Mme D...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention de la commune de Saint-Leu n'est pas admise. <br/>
Article 2 : Le pourvoi de M. M...est rejeté. <br/>
<br/>
Article 3 : M. M...versera à Mme D...et autres une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. H...M..., à Mme E...D..., à la commune de Saint-Leu et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). - POUVOIRS - LIMITES - ABSENCE D'OBSTACLE À L'EXÉCUTION D'UNE DÉCISION ADMINISTRATIVE, Y COMPRIS CELLE REFUSANT LA MESURE DEMANDÉE, SAUF PÉRIL GRAVE - EXISTENCE [RJ1] - DÉCISION ADMINISTRATIVE REFUSANT LA MESURE DEMANDÉE POSTÉRIEUREMENT À SA SAISINE - ABSENCE.
</SCT>
<ANA ID="9A"> 54-035-04 Si le juge des référés, saisi sur le fondement de l'article L. 521-3 du code de justice administrative (CJA), ne saurait faire obstacle à l'exécution d'une décision administrative, même celle refusant la mesure demandée, à moins qu'il ne s'agisse de prévenir un péril grave, la circonstance qu'une décision administrative refusant la mesure demandée au juge des référés intervienne postérieurement à sa saisine ne saurait faire obstacle à ce qu'il fasse usage des pouvoirs qu'il tient de l'article L. 521-3 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 février 2016,,, n°s 393540-393451, p. 13.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
