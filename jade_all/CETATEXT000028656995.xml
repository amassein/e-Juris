<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028656995</ID>
<ANCIEN_ID>JG_L_2014_02_000000373999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/65/69/CETATEXT000028656995.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 19/02/2014, 373999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373999.20140219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 373999, l'ordonnance n° 1306269 du 13 décembre 2013, enregistrée le 16 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le juge des référés du tribunal administratif de Grenoble, avant de statuer sur la demande de la commune de Thonon-les-Bains, tendant à la suspension de l'exécution de l'arrêté préfectoral du 3 octobre 2013 la rattachant à la communauté de communes des collines du Léman à compter du 1er janvier 2014, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 5210-1-2 du code général des collectivités territoriales ; <br/>
<br/>
              Vu le mémoire, enregistré le 28 novembre 2013 au greffe du tribunal administratif de Grenoble, présenté par la commune de Thonon-les-Bains, représentée par son maire, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 374289, l'ordonnance n° 1302961 du 27 décembre 2013, enregistrée le 30 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le juge des référés du tribunal administratif de Nancy, avant de statuer sur la demande de la commune de Saint-Ail, tendant à la suspension de l'exécution de l'arrêté préfectoral du 24  septembre 2013 la rattachant, ainsi que la commune de Batilly, à la communauté de communes du Pays de l'Orne à compter du 1er janvier 2014, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 5210-1-2 du code général des collectivités territoriales ; <br/>
<br/>
              Vu le mémoire, enregistré le 12 décembre 2013 au greffe du tribunal administratif de Nancy, présenté par la commune de Saint-Ail, représentée par son maire, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et ses articles 61-1 et 72 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Thonon-les-Bains et à la SCP Vincent, Ohl, avocat de la commune de Saint-Ail ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 5210-1-2 du code général des collectivités territoriales, entré en vigueur le 1er juin 2013, lorsque le préfet constate qu'une commune n'appartient à aucun établissement public de coopération intercommunale (EPCI) à fiscalité propre ou crée, au sein du périmètre d'un tel établissement existant, une enclave ou une discontinuité territoriale, il lui appartient, en principe, de rattacher par arrêté cette commune à un EPCI à fiscalité propre ; que ce rattachement a lieu après accord de l'EPCI à fiscalité propre concerné et avis de la commission départementale de la coopération intercommunale ainsi que, le cas échéant, avis du comité de massif ; qu'en cas de désaccord exprimé par l'EPCI à fiscalité propre concerné, le préfet rattache la commune conformément à son projet initial, sauf si la commission départementale de la coopération intercommunale se prononce, à la majorité des deux tiers de ses membres, en faveur d'un autre projet de rattachement, cas dans lequel le préfet est tenu de mettre en oeuvre cet autre projet ; <br/>
<br/>
              3. Considérant que les communes de Thonon-les-Bains et de Saint-Ail soutiennent que ces dispositions sont contraires au principe de libre administration des collectivités territoriales énoncé à l'article 72 de la Constitution et au droit de propriété garanti par les articles 2 et 17 de la Déclaration de 1789  ;<br/>
<br/>
              4. Considérant que l'article L. 5210-1-2 du code général des collectivités territoriales est applicable aux litiges dont sont saisis les juges des référés des tribunaux administratifs de Grenoble et Nancy ; que cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, et notamment au principe de libre administration des collectivités territoriales, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 5210-1-2 du code général des collectivités territoriales est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Thonon-les-Bains, à la commune de Saint-Ail et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil Constitutionnel, au Premier ministre, au tribunal administratif de Grenoble et au tribunal administratif de Nancy.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
