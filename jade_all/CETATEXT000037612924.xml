<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037612924</ID>
<ANCIEN_ID>JG_L_2018_11_000000406371</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/29/CETATEXT000037612924.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 14/11/2018, 406371</TITRE>
<DATE_DEC>2018-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406371</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406371.20181114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Nouvelle-Calédonie d'annuler pour excès de pouvoir la décision du 18 juin 2014 par laquelle le président de l'université de la Nouvelle-Calédonie a refusé de le réintégrer dans son emploi à la suite d'une mise en disponibilité pour convenances personnelles. Par un jugement n° 1400276 du 13 mai 2015, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 15PA03325 du 27 septembre 2016, la cour administrative d'appel de Paris a, sur appel de l'université de la Nouvelle-Calédonie, annulé ce jugement et rejeté la demande de M. B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 décembre 2016 et 29 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'université de la Nouvelle-Calédonie ;<br/>
<br/>
              3°) de mettre à la charge de l'université de la Nouvelle-Calédonie la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - l'arrêté du ministre chargé de l'enseignement supérieur du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B...et à la SCP Waquet, Farge, Hazan, avocat de l'université de la Nouvelle-Calédonie ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 4 mars 2013, le président de l'université de la Nouvelle-Calédonie a placé M. B..., professeur des universités, en position de disponibilité pour convenances personnelles ; qu'à la demande de M. B..., le tribunal administratif de Nouméa a, par un jugement du 13 mai 2015, annulé la décision du 18 juin 2014 par laquelle le président de cette université a rejeté sa demande de réintégration anticipée dans l'emploi qu'il exerçait avant sa mise en disponibilité ; que M. B... se pourvoit en cassation contre l'arrêt du 27 septembre 2016 par lequel la cour administrative d'appel de Paris a, sur appel de l'université de la Nouvelle-Calédonie, annulé ce jugement et rejeté sa demande ;<br/>
<br/>
              2.  Considérant que le premier alinéa de l'article 51 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dispose : " La disponibilité est la position du fonctionnaire qui, placé hors de son administration ou service d'origine, cesse de bénéficier, dans cette position, de ses droits à l'avancement et à la retraite " ; que son article 52 précise : " Un décret en Conseil d'Etat détermine les cas et conditions de mise en disponibilité, sa durée, ainsi que les modalités de réintégration des fonctionnaires intéressés à l'expiration de la période de disponibilité " ; qu'aux termes de l'article 49 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions de fonctionnaires de l'Etat, applicable aux professeurs des universités en vertu des articles 1er et 10 du décret du 6 juin 1984 relatif au statut des enseignants-chercheurs de l'enseignement supérieur : " (...) la réintégration est subordonnée à la vérification par un médecin agréé et, éventuellement, par le comité médical compétent, saisi dans les conditions prévues par la réglementation en vigueur, de l'aptitude physique du fonctionnaire à l'exercice des fonctions afférentes à son grade. / Trois mois au moins avant l'expiration de la disponibilité, le fonctionnaire fait connaître à son administration d'origine sa décision de solliciter le renouvellement de la disponibilité ou de réintégrer son corps d'origine. Sous réserve des dispositions du deuxième alinéa du présent article et du respect par l'intéressé, pendant la période de mise en disponibilité, des obligations qui s'imposent à un fonctionnaire même en dehors du service, la réintégration est de droit. / A l'issue de sa disponibilité, l'une des trois premières vacances dans son grade doit être proposée au fonctionnaire. S'il refuse successivement trois postes qui lui sont proposés, il peut être licencié après avis de la commission administrative paritaire. / A l'issue de la disponibilité prévue aux 1° et 2° de l'article 47 du présent décret, le fonctionnaire est, par dérogation aux dispositions de l'alinéa précédent, obligatoirement réintégré à la première vacance dans son corps d'origine et affecté à un emploi correspondant à son grade. S'il refuse le poste qui lui est assigné, les dispositions du précédent alinéa lui sont appliquées. / Le fonctionnaire qui a formulé avant l'expiration de la période de mise en disponibilité une demande de réintégration est maintenu en disponibilité jusqu'à ce qu'un poste lui soit proposé dans les conditions fixées aux deux alinéas précédents (...) " ; qu'il résulte de ces dispositions qu'un professeur des universités qui sollicite, auprès du ministre chargé de l'enseignement supérieur, sa réintégration à l'issue de la période de mise en disponibilité pour convenances personnelles, ou sa réintégration anticipée avant cette date, a droit d'être réintégré dans son corps d'origine, à l'une des trois premières vacances d'un emploi de son grade, sous réserve de la vérification de l'aptitude physique de l'intéressé à l'exercice de ses fonctions et du respect par celui-ci, pendant la période de mise en disponibilité, des obligations qui s'imposent à un fonctionnaire même en dehors du service ;<br/>
<br/>
              3.  Considérant, toutefois, que le premier alinéa de l'article L. 951-3 du code de l'éducation dispose : " Le ministre chargé de l'enseignement supérieur peut déléguer par arrêté aux présidents des universités et aux présidents ou directeurs des autres établissements publics d'enseignement supérieur, dans les conditions fixées par décret en Conseil d'Etat, tout ou partie de ses pouvoirs en matière de recrutement et de gestion des personnels titulaires, stagiaires et non titulaires de l'Etat qui relèvent de son autorité, dans la limite des emplois inscrits dans la loi de finances et attribués à l'établissement " ; qu'en application de ces dispositions, le ministre chargé de l'enseignement supérieur a, par un arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche, délégué aux présidents des universités ses pouvoirs en matière de recrutement et de gestion des personnels enseignants en ce qui concerne : " 13. La mise en disponibilité et la réintégration après mise en disponibilité " ; que sans préjudice des pouvoirs du ministre chargé de l'enseignement supérieur rappelés au point 2, les pouvoirs ainsi délégués à chaque président d'un établissement public d'enseignement supérieur doivent être regardés comme donnant compétence à ces derniers pour statuer, d'une part, sur les demandes de mise en disponibilité des personnels enseignants exerçant dans leur établissement et, d'autre part, sur les demandes de réintégration présentées par ces mêmes enseignants à l'issue de la période de mise en disponibilité ou de manière anticipée avant cette date, dès lors que cette demande de réintégration vise à occuper un poste dans leur établissement d'origine ; <br/>
<br/>
              4.  Considérant que, dans l'exercice des pouvoirs ainsi délégués, lorsqu'un enseignant-chercheur en position de disponibilité sollicite sa réintégration auprès du président de l'université dans laquelle il était affecté avant son départ, en demandant d'occuper un poste dans cette université, le président de l'université peut légalement, eu égard à l'absence de tout droit des enseignants-chercheurs en disponibilité à être réintégrés dans l'établissement où ils étaient précédemment affectés, opposer un refus à cette réintégration en raison d'un motif tiré de l'intérêt du service, notamment l'absence, dans cette université, d'emploi vacant dans le grade sur lequel il pourrait être réintégré ; <br/>
<br/>
              5.  Considérant  que  pour  juger  que  le président  de l'université de la Nouvelle-Calédonie était fondé à rejeter la demande présentée par M. B... tendant à sa réintégration anticipée à l'université de la Nouvelle-Calédonie, la cour administrative d'appel de Paris a, par une appréciation souveraine exempte de dénaturation, estimé que, compte tenu de la nature et de la notoriété des agissements de M. B... dans cette université, antérieurement à sa mise en disponibilité, sa réintégration dans cette université présentait, à la date à laquelle la demande de réintégration a été rejetée, un risque de troubles au bon fonctionnement de l'université ; que la cour a pu, sans erreur de droit ni inexacte qualification juridique des faits, juger que de tels troubles auraient porté atteinte à l'intérêt du service et justifiaient le rejet de sa demande de réintégration ;<br/>
<br/>
              6.  Considérant que la cour a pu, par suite, sans se méprendre sur la portée des écritures de l'administration et sans commettre d'erreur de droit, substituer, à la demande de l'université de la Nouvelle-Calédonie, ce motif tiré de l'intérêt du service au motif que le président de l'université avait retenu dans sa décision rejetant la demande de M. B... ;<br/>
<br/>
              7.  Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que son pourvoi doit ainsi être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              8.  Considérant que, la décision litigieuse ayant été prise au nom de l'Etat, l'université de la Nouvelle-Calédonie n'a pas qualité de partie au présent litige ; que les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce que soit mise à la charge de M. B...la somme qu'elle demande à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
            Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par l'université de la Nouvelle-Calédonie au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B..., à l'université de la Nouvelle-Calédonie et à la ministre de l'enseignement supérieur et de la recherche<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-02-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL ENSEIGNANT. - 1) DÉLÉGATION PAR LE MINISTRE CHARGÉ DE L'ENSEIGNEMENT SUPÉRIEUR À UN PRÉSIDENT D'UN ÉTABLISSEMENT PUBLIC D'ENSEIGNEMENT SUPÉRIEUR DE SA COMPÉTENCE EN MATIÈRE DE MISE À DISPONIBILITÉ ET DE RÉINTÉGRATION DU PERSONNEL ENSEIGNANT (ART. L. 951-3 DU CODE DE L'ÉDUCATION ET ARRÊTÉ DU 10 FÉVRIER 2012) - PORTÉE [RJ1] - 2) MOTIFS SUSCEPTIBLES D'ÊTRE OPPOSÉS À UNE DEMANDE DE RÉINTÉGRATION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. - 1) DÉLÉGATION PAR LE MINISTRE CHARGÉ DE L'ENSEIGNEMENT SUPÉRIEUR À UN PRÉSIDENT D'UN ÉTABLISSEMENT PUBLIC D'ENSEIGNEMENT SUPÉRIEUR DE SA COMPÉTENCE EN MATIÈRE DE MISE À DISPONIBILITÉ ET DE RÉINTÉGRATION DU PERSONNEL ENSEIGNANT (ART. L. 951-3 DU CODE DE L'ÉDUCATION ET ARRÊTÉ DU 10 FÉVRIER 2012) - PORTÉE [RJ1] - 2) MOTIFS SUSCEPTIBLES D'ÊTRE OPPOSÉS À UNE DEMANDE DE RÉINTÉGRATION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">30-02-05-01-038 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. PRÉSIDENTS D'UNIVERSITÉ. - 1) DÉLÉGATION PAR LE MINISTRE CHARGÉ DE L'ENSEIGNEMENT SUPÉRIEUR À UN PRÉSIDENT D'UNIVERSITÉ DE SA COMPÉTENCE EN MATIÈRE DE MISE À DISPONIBILITÉ ET DE RÉINTÉGRATION DU PERSONNEL ENSEIGNANT (ART. L. 951-3 DU CODE DE L'ÉDUCATION ET ARRÊTÉ DU 10 FÉVRIER 2012) - PORTÉE [RJ1] - 2) MOTIFS SUSCEPTIBLES D'ÊTRE OPPOSÉS À UNE DEMANDE DE RÉINTÉGRATION[RJ2].
</SCT>
<ANA ID="9A"> 30-01-02-01 1) En application du premier alinéa de l'article L. 951-3 du code de l'éducation, le ministre chargé de l'enseignement supérieur a, par un arrêté du 10 février 2012, délégué aux présidents des universités ses pouvoirs en matière de recrutement et de gestion des personnels enseignants en ce qui concerne : 13. La mise en disponibilité et la réintégration après mise en disponibilité . Sans préjudice des pouvoirs du ministre chargé de l'enseignement supérieur, les pouvoirs ainsi délégués à chaque président d'un établissement public d'enseignement supérieur doivent être regardés comme donnant compétence à ces derniers pour statuer, d'une part, sur les demandes de mise en disponibilité des personnels enseignants exerçant dans leur établissement et, d'autre part, compétence pour statuer sur les demandes de réintégration présentées par ces mêmes enseignants, à l'issue de la période de mise en disponibilité ou de manière anticipée avant cette date, dès lors que cette demande de réintégration vise à occuper un poste dans leur établissement d'origine.... ,,2) Dans l'exercice des pouvoirs ainsi délégués, lorsqu'un enseignant-chercheur en position de disponibilité sollicite sa réintégration auprès du président de l'université dans laquelle il était affecté avant son départ, en demandant d'occuper un poste dans cette université, le président de l'université peut légalement, eu égard à l'absence de tout droit des enseignants-chercheurs en disponibilité à être réintégrés dans l'établissement où ils étaient précédemment affectés, opposer un refus à cette réintégration en raison d'un motif tiré de l'intérêt du service, notamment l'absence, dans cette université, d'emploi vacant dans le grade sur lequel il pourrait être réintégré.</ANA>
<ANA ID="9B"> 30-02-05 1) En application du premier alinéa de l'article L. 951-3 du code de l'éducation, le ministre chargé de l'enseignement supérieur a, par un arrêté du 10 février 2012, délégué aux présidents des universités ses pouvoirs en matière de recrutement et de gestion des personnels enseignants en ce qui concerne : 13. La mise en disponibilité et la réintégration après mise en disponibilité . Sans préjudice des pouvoirs du ministre chargé de l'enseignement supérieur, les pouvoirs ainsi délégués à chaque président d'un établissement public d'enseignement supérieur doivent être regardés comme donnant compétence à ces derniers pour statuer, d'une part, sur les demandes de mise en disponibilité des personnels enseignants exerçant dans leur établissement et, d'autre part, compétence pour statuer sur les demandes de réintégration présentées par ces mêmes enseignants, à l'issue de la période de mise en disponibilité ou de manière anticipée avant cette date, dès lors que cette demande de réintégration vise à occuper un poste dans leur établissement d'origine.... ,,2) Dans l'exercice des pouvoirs ainsi délégués, lorsqu'un enseignant-chercheur en position de disponibilité sollicite sa réintégration auprès du président de l'université dans laquelle il était affecté avant son départ, en demandant d'occuper un poste dans cette université, le président de l'université peut légalement, eu égard à l'absence de tout droit des enseignants-chercheurs en disponibilité à être réintégrés dans l'établissement où ils étaient précédemment affectés, opposer un refus à cette réintégration en raison d'un motif tiré de l'intérêt du service, notamment l'absence, dans cette université, d'emploi vacant dans le grade sur lequel il pourrait être réintégré.</ANA>
<ANA ID="9C"> 30-02-05-01-038 1) En application du premier alinéa de l'article L. 951-3 du code de l'éducation, le ministre chargé de l'enseignement supérieur a, par un arrêté du 10 février 2012, délégué aux présidents des universités ses pouvoirs en matière de recrutement et de gestion des personnels enseignants en ce qui concerne : 13. La mise en disponibilité et la réintégration après mise en disponibilité . Sans préjudice des pouvoirs du ministre chargé de l'enseignement supérieur, les pouvoirs ainsi délégués à chaque président d'un établissement public d'enseignement supérieur doivent être regardés comme donnant compétence à ces derniers pour statuer, d'une part, sur les demandes de mise en disponibilité des personnels enseignants exerçant dans leur établissement et, d'autre part, compétence pour statuer sur les demandes de réintégration présentées par ces mêmes enseignants, à l'issue de la période de mise en disponibilité ou de manière anticipée avant cette date, dès lors que cette demande de réintégration vise à occuper un poste dans leur établissement d'origine.... ,,2) Dans l'exercice des pouvoirs ainsi délégués, lorsqu'un enseignant-chercheur en position de disponibilité sollicite sa réintégration auprès du président de l'université dans laquelle il était affecté avant son départ, en demandant d'occuper un poste dans cette université, le président de l'université peut légalement, eu égard à l'absence de tout droit des enseignants-chercheurs en disponibilité à être réintégrés dans l'établissement où ils étaient précédemment affectés, opposer un refus à cette réintégration en raison d'un motif tiré de l'intérêt du service, notamment l'absence, dans cette université, d'emploi vacant dans le grade sur lequel il pourrait être réintégré.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., en l'état antérieur du droit, CE, 4 février 2000, Noble, n° 185726, T. pp. 799-1021-1057.,,[RJ2] Rappr. CE, 18 novembre 1994, Ministre des affaires sociales et de la solidarité nationale c/ Mme Ciolino, n° 77047, T. p.997.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
