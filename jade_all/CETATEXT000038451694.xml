<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038451694</ID>
<ANCIEN_ID>JG_L_2019_04_000000429945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/45/16/CETATEXT000038451694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/04/2019, 429945, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429945.20190426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif d'Orléans de l'admettre au bénéfice de l'aide juridictionnelle provisoire et, sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre au directeur territorial de l'Office français de l'immigration et de l'intégration d'Orléans de la réintégrer dans le lieu d'hébergement où elle avait été admise auparavant, d'ordonner que lui soit rétabli le versement de l'allocation pour demandeur d'asile, de suspendre la décision prolongeant le délai de son transfert en Espagne et d'enjoindre au préfet de Loir-et-Cher de lui renouveler son attestation de demandeur d'asile, de s'assurer auprès des autorités espagnoles de ses conditions d'accueil avec son bébé en Espagne et, à défaut de réponse, de mettre en oeuvre la clause discrétionnaire prévue à l'article 17 du règlement (UE) n° 604/2013 du 26 juin 2013 et de lui délivrer une attestation permettant de présenter une demande d'asile à l'Office français de protection des réfugiés et apatrides et, à titre subsidiaire, d'enjoindre au préfet de Loir-et-Cher de lui fournir un hébergement d'urgence ou, à défaut, une aide financière de 100 euros par jour pour lui permettre de financer une chambre d'hôtel pour elle et son enfant. Par une ordonnance n° 1901213 du 9 avril 2019, le juge des référés du tribunal administratif d'Orléans a accordé à Mme B...le bénéfice de l'aide juridictionnelle provisoire et rejeté le surplus des conclusions de sa demande. <br/>
<br/>
              Par une requête, enregistrée le 18 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au juge des référés du Conseil d'Etat : <br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) à titre principal, de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et, à titre subsidiaire, de mettre à la charge de l'Etat la somme de 1 000 euros au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - étant une mère isolée accompagnée d'un très jeune enfant et faute que le préfet de Loir-et-Cher se soit assuré des conditions de son accueil en Espagne, elle ne peut être regardée comme étant en fuite du fait de son refus d'embarquer à destination de ce pays et l'attestation de demandeur d'asile ne pouvait lui être retirée ;<br/>
              - c'est à tort que le juge des référés du tribunal administratif d'Orléans a jugé que l'article L. 744-7 du code de l'entrée et du séjour et du droit d'asile n'était pas incompatible avec les objectifs de l'article 20 de la directive 2013/33/UE du 26 juin 2013 ;<br/>
              - le retrait du bénéfice des conditions matérielles d'accueil selon une procédure irrégulière, faute qu'elle ait bénéficié préalablement d'une information adéquate et de la possibilité de présenter des observations, porte une atteinte grave et manifestement illégale à son droit d'asile ;<br/>
              - elle se trouve dans une situation extrêmement précaire avec son bébé et n'a obtenu qu'à titre provisoire la prise en charge par le département de son hébergement à l'hôtel.<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) n° 343/2003 du Conseil ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée. <br/>
<br/>
              2. Si, d'une part, la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés, qui apprécie si les conditions prévues par l'article L. 521-2 du code de justice administrative sont remplies à la date à laquelle il se prononce, ne peut faire usage des pouvoirs qu'il tient de cet article en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              3. Il appartient, d'autre part, aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              4. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif d'Orléans que Mme A...B..., ressortissante ivoirienne née le 2 mai 1995 et mère d'un enfant né le 13 juin 2018, deux jours selon elle après son entrée en France, a, le 28 juin 2018, présenté une demande d'asile et accepté les conditions matérielles d'accueil qui lui ont été proposées, lui permettant de bénéficier d'un hébergement. Les autorités espagnoles ayant toutefois accepté le 20 septembre 2018 la prise en charge de l'intéressée, dont les empreintes avaient été relevées précédemment en Espagne, le préfet de Loir-et-Cher a décidé, le 11 octobre 2018, de transférer Mme B...vers ce pays. Le 28 mars 2019, le préfet de Loir-et-Cher a informé l'Office français de l'immigration et de l'intégration que l'intéressée avait refusé d'embarquer sur le vol prévu la veille pour l'exécution de la décision de transfert et qu'elle était déclarée en fuite, ce dont il a informé les autorités espagnoles, en leur indiquant que l'échéance du délai de transfert était portée au 19 avril 2020. Le 29 mars 2019, le directeur général de l'Office français de l'immigration et de l'intégration a mis fin au bénéfice pour Mme B...des conditions matérielles d'accueil. Mme B...a saisi le juge des référés du tribunal administratif d'Orléans d'une demande tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, à ce qu'il soit enjoint à l'Office français de l'immigration et de l'intégration de lui rétablir le bénéfice des conditions matérielles d'accueil et notamment d'un hébergement, à ce qu'il soit enjoint au préfet de Loir-et-Cher de suspendre la prolongation de son délai de transfert et de s'assurer, avant son transfert, de ses conditions d'accueil avec son enfant en Espagne et, à défaut, de la mettre à même de présenter sa demande d'asile en France et, à titre subsidiaire, à ce qu'il soit enjoint au préfet de Loir-et-Cher de lui assurer un hébergement d'urgence. Par une ordonnance du 9 avril 2019, dont Mme B...relève appel, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              5. En premier lieu, la requérante ne conteste pas sérieusement, en faisant notamment valoir qu'il était nécessaire de s'assurer préalablement des conditions de son hébergement et de sa prise en charge avec son enfant en Espagne, que son refus d'embarquer à destination de ce pays ne caractériserait pas son intention de se soustraire à l'exécution de son transfert et ne pourrait de ce fait être regardée comme une fuite au sens de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013, susceptible de fonder la prolongation du délai de son transfert, et comme une absence de respect des exigences posées par les autorités chargées de l'asile quant à ses obligations de présentation, auquel les dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile subordonnent le bénéfice des conditions matérielles d'accueil, ainsi que le permet l'article 20 de la directive 2013/33/UE du 26 juin 2013 qu'elles transposent sur ce point. Elle n'apporte ainsi en appel aucun élément de nature à infirmer l'appréciation portée, au regard des critères mentionné au point 2, par le juge des référés de première instance quant à l'absence d'atteinte grave et manifestement illégale portée à la liberté fondamentale que constitue le droit d'asile.<br/>
<br/>
              6. En second lieu, s'il résulte de l'instruction que Mme B...est accompagnée de son fils, âgé de dix mois, dont l'état de santé a nécessité une hospitalisation pendant plusieurs jours au début du mois d'avril 2019 et impose un suivi médical, il résulte des indications apportées par l'intéressée elle-même en cause d'appel que son hébergement avec son enfant est, à ce jour, assuré par le département de Loir-et-Cher, auquel incombe en vertu de l'article L. 222-5 du code de l'action sociale et des familles la prise en charge, qui inclut l'hébergement, le cas échéant en urgence, des mères isolées avec leurs enfants de moins de trois ans qui ont besoin d'un soutien matériel et psychologique, notamment parce qu'elles sont sans domicile. Dans ces conditions, et alors que l'intervention de l'Etat ne revêt qu'un caractère supplétif, dans l'hypothèse où le département n'aurait pas accompli les diligences qui lui reviennent, Mme B...n'est pas fondée à se plaindre de ce que le juge des référés du tribunal administratif d'Orléans a jugé qu'aucune méconnaissance grave et manifeste des obligations qui s'imposent à l'Etat dans la mise en oeuvre du droit à l'hébergement d'urgence ne pouvait être retenue au regard des critères mentionnés au point 3.<br/>
<br/>
              7. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de Mme B... ne peut être accueilli. La requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit dès lors être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B....<br/>
Copie en sera adressée à l'Office français de l'immigration et de l'intégration, au ministre de l'intérieur et au département de Loir-et-Cher.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
