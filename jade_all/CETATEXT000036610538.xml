<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610538</ID>
<ANCIEN_ID>JG_L_2018_02_000000412583</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610538.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 09/02/2018, 412583</TITRE>
<DATE_DEC>2018-02-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412583</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412583.20180209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Maison de chirurgie clinique Turin a demandé au tribunal administratif de Paris d'annuler l'arrêté du 4 novembre 2016 par lequel le directeur général de l'agence régionale de santé d'Ile-de-France a fixé le montant dû par l'établissement au titre de l'année 2015 en application du mécanisme de dégressivité tarifaire prévu à l'article L. 162-22-9-2 du code de la sécurité sociale.<br/>
<br/>
              Par une ordonnance n° 1700257 du 18 juillet 2017, enregistrée au secrétariat de la section du contentieux du Conseil d'Etat le 19 juillet 2017, la présidente du tribunal administratif de Paris a transmis au président de la section du contentieux du Conseil d'Etat, en application du second alinéa de l'article R. 351-3 du code de justice administrative, la requête présentée à ce tribunal par la société par actions simplifiée Maison de chirurgie clinique Turin. <br/>
<br/>
              Un mémoire, enregistré le 29 janvier 2018, a été produit par la société Maison de chirurgie clinique Turin sans le ministère d'un avocat au Conseil d'Etat et à la cour de Cassation<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 351-1 du code de l'action sociale et des familles : " Les recours dirigés contre les décisions prises par (...) le directeur général de l'agence régionale de santé (...) déterminant les dotations globales, les dotations annuelles, les forfaits annuels, les dotations de financement des missions d'intérêt général et d'aide à la contractualisation, les remboursements forfaitaires, les subventions obligatoires aux établissements de santé mentionnés à l'article L. 4383-5 du code de la santé publique, les prix de journée et autres tarifs des établissements et services sanitaires (...) de statut public ou privé (...) sont portés, en premier ressort, devant le tribunal interrégional de la tarification sanitaire et sociale ".<br/>
<br/>
              2. Aux termes de l'article L. 162-22-9-2 inséré dans le code de la sécurité sociale par l'article 41 de la loi du 23 décembre 2013 de financement de la sécurité sociale pour 2014, en vigueur à la date de l'arrêté attaqué : " L'État peut fixer, pour tout ou partie des prestations d'hospitalisation mentionnées au 1° de l'article L. 162-22, des seuils exprimés en taux d'évolution ou en volume d'activité. / Lorsque le taux d'évolution ou le volume d'activité d'une prestation ou d'un ensemble de prestations d'hospitalisation d'un établissement de santé soumis aux dispositions du premier alinéa du présent article est supérieur au seuil fixé en application du même alinéa, les tarifs mentionnés au 1° du I de l'article L. 162-22-10 applicables à la prestation ou à l'ensemble de prestations concernés sont minorés pour la part d'activité réalisée au-delà de ce seuil par l'établissement. / Un décret en Conseil d'État détermine les modalités d'application du présent article, notamment les critères pris en compte pour fixer les seuils, les modalités de mesure de l'activité et de minoration des tarifs ainsi que les conditions de mise en oeuvre des minorations après constatation du dépassement des seuils. La mesure de l'activité tient compte des situations de création ou de regroupement d'activités ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 23 décembre 2013, que, dans l'objectif de maîtrise des dépenses de santé, le législateur a entendu permettre l'instauration d'une dégressivité tarifaire pour des catégories de prestations précisément identifiées connaissant, au sein d'un établissement de santé, un taux d'évolution ou un volume d'activité importants, afin, d'une part, de tenir compte de la décroissance corrélative du coût marginal de ces prestations et, d'autre part, d'inciter les établissements concernés à maîtriser l'augmentation de leur nombre.<br/>
<br/>
              3. Il résulte de ces dispositions que les décisions par lesquelles le directeur général de l'agence régionale de santé a fixé, sur le fondement des dispositions de l'article R. 162-42-1-8 du code de la sécurité sociale alors en vigueur, le montant des sommes à récupérer auprès d'un établissement au titre de la minoration des tarifs, qui n'ont pas le caractère de sanction, se rattachent à la détermination des tarifs des établissements et services sanitaires, sociaux et médico-sociaux au sens de l'article L. 351-1 du code de l'action sociale et des familles précité.<br/>
<br/>
              4. En conséquence, les litiges relatifs à ces décisions relèvent de la compétence en premier ressort du tribunal interrégional de la tarification sanitaire et sociale.<br/>
<br/>
              5. Il y a lieu, dès lors, d'attribuer le jugement de la requête de la société Maison de chirurgie clinique Turin au tribunal interrégional de la tarification sanitaire et sociale de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
                               D E C I D E :<br/>
                               --------------<br/>
Article 1er : Le jugement de la requête de la société Maison de chirurgie clinique Turin est attribué au tribunal interrégional de la tarification sanitaire et sociale de Paris.<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Maison de chirurgie clinique Turin, à l'agence régionale de santé d'Ile-de-France, au président du tribunal interrégional de la tarification sanitaire et sociale de Paris et à la présidente du tribunal administratif de Paris.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-04-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DES JURIDICTIONS ADMINISTRATIVES SPÉCIALES. JURIDICTION ADMINISTRATIVE DE DROIT COMMUN OU JURIDICTION ADMINISTRATIVE SPÉCIALISÉE. - FIXATION DU MONTANT DES SOMMES À RÉCUPÉRER AUPRÈS D'UN ÉTABLISSEMENT AU TITRE DE LA MINORATION TARIFAIRE (ANCIEN ART. R. 162-42-8-1 DU CSS) - LITIGE RELEVANT DE LA COMPÉTENCE EN PREMIER RESSORT DU TITSS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-02-02 SÉCURITÉ SOCIALE. RELATIONS AVEC LES PROFESSIONS ET LES ÉTABLISSEMENTS SANITAIRES. RELATIONS AVEC LES ÉTABLISSEMENTS DE SANTÉ. - FIXATION DU MONTANT DES SOMMES À RÉCUPÉRER AUPRÈS D'UN ÉTABLISSEMENT AU TITRE DE LA MINORATION TARIFAIRE (ANCIEN ART. R. 162-42-8-1 DU CSS) - LITIGE RELEVANT DE LA COMPÉTENCE EN PREMIER RESSORT DU TITSS [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-05-01-03 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RÈGLES DE COMPÉTENCE. COMPÉTENCE DES JURIDICTIONS DE SÉCURITÉ SOCIALE. - FIXATION DU MONTANT DES SOMMES À RÉCUPÉRER AUPRÈS D'UN ÉTABLISSEMENT AU TITRE DE LA MINORATION TARIFAIRE (ANCIEN ART. R. 162-42-8-1 DU CSS) - LITIGE RELEVANT DE LA COMPÉTENCE EN PREMIER RESSORT DU TITSS [RJ1].
</SCT>
<ANA ID="9A"> 17-05-04-02 Les décisions par lesquelles le directeur général de l'agence régionale de santé (ARS) fixe, sur le fondement de l'ancien article R. 162-42-1-8 du code de la sécurité sociale (CSS), le montant des sommes à récupérer auprès d'un établissement au titre de la minoration des tarifs, qui n'ont pas le caractère de sanction, se rattachent à la détermination des tarifs des établissements et services sanitaires, sociaux et médico-sociaux au sens de l'article L. 351-1 du code de l'action sociale et des familles (CASF). En conséquence, les litiges relatifs à ces décisions relèvent de la compétence en premier ressort du tribunal interrégional de la tarification sanitaire et sociale (TITSS).</ANA>
<ANA ID="9B"> 62-02-02 Les décisions par lesquelles le directeur général de l'agence régionale de santé (ARS) fixe, sur le fondement de l'ancien article R. 162-42-1-8 du CSS, le montant des sommes à récupérer auprès d'un établissement au titre de la minoration des tarifs, qui n'ont pas le caractère de sanction, se rattachent à la détermination des tarifs des établissements et services sanitaires, sociaux et médico-sociaux au sens de l'article L. 351-1 du code de l'action sociale et des familles (CASF). En conséquence, les litiges relatifs à ces décisions relèvent de la compétence en premier ressort du tribunal interrégional de la tarification sanitaire et sociale (TITSS).</ANA>
<ANA ID="9C"> 62-05-01-03 Les décisions par lesquelles le directeur général de l'agence régionale de santé (ARS) fixe, sur le fondement de l'ancien article R. 162-42-1-8 du CSS, le montant des sommes à récupérer auprès d'un établissement au titre de la minoration des tarifs, qui n'ont pas le caractère de sanction, se rattachent à la détermination des tarifs des établissements et services sanitaires, sociaux et médico-sociaux au sens de l'article L. 351-1 du code de l'action sociale et des familles (CASF). En conséquence, les litiges relatifs à ces décisions relèvent de la compétence en premier ressort du tribunal interrégional de la tarification sanitaire et sociale (TITSS).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décisions du même jour, Société Clinique du Mont-Louis, n° 412585 et Société Clinique des Ormeaux, n° 414319,  inédites au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
