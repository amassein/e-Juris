<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575683</ID>
<ANCIEN_ID>JG_L_2020_11_000000429860</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575683.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/11/2020, 429860, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429860</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429860.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la commission du contentieux du stationnement payant d'annuler le titre exécutoire émis le 4 octobre 2018 par l'Agence nationale de traitement automatisé des infractions (ANTAI) en vue du recouvrement du forfait de post-stationnement d'un montant de 30 euros mis à sa charge le 6 avril 2018 par la commune de Saint-Denis ainsi que la majoration dont il est assorti. Par une ordonnance n° 19001228 du 18 février 2019, le magistrat désigné par le président de la commission du contentieux du stationnement payant a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 avril et 10 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'ANTAI la somme de 2 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B..., et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de l'Agence nationale de traitement automatisé des infractions.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'en raison de l'absence de paiement d'une redevance de stationnement constatée le 6 avril 2018, M. B... a fait l'objet d'un titre exécutoire, émis le 4 octobre 2018 par l'Agence nationale de traitement automatisé des infractions, en vue du recouvrement d'un forfait de post-stationnement d'un montant de 30 euros pour la commune de Saint-Denis, assorti d'une majoration revenant à l'Etat. M. B... se pourvoit en cassation contre l'ordonnance du 18 février 2019 par laquelle le magistrat désigné par le président de la commission du contentieux du stationnement payant a rejeté sa requête dirigée contre ce titre exécutoire.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 2333-87 du code général des collectivités territoriales qu'il appartient en principe au redevable d'un forfait de post-stationnement qui entend contester le bien-fondé de la somme mise à sa charge de saisir l'autorité administrative d'un recours administratif préalable dirigé contre l'avis de paiement et, en cas de rejet de ce recours, d'introduire une requête contre cette décision de rejet devant la commission du contentieux du stationnement payant. Toutefois, en cas d'absence de paiement de sa part dans les trois mois et d'émission, en conséquence, d'un titre exécutoire portant sur le montant du forfait de post-stationnement augmenté de la majoration due à l'Etat, il est loisible au même redevable de contester ce titre exécutoire devant la commission du contentieux du stationnement payant, qu'il ait ou non engagé un recours administratif contre l'avis de paiement et contesté au contentieux le rejet de son recours. A ce titre, s'il résulte des dispositions de l'article R. 2333-120-35 du même code que le redevable qui saisit la commission du contentieux du stationnement payant d'une requête contre un titre exécutoire n'est pas recevable à exciper de l'illégalité de l'avis de paiement du forfait de post-stationnement auquel ce titre exécutoire s'est substitué, ces mêmes dispositions ne font pas obstacle à ce que l'intéressé conteste, dans le cadre d'un litige dirigé contre le titre exécutoire, l'obligation de payer la somme réclamée par l'administration.<br/>
<br/>
              3. Il ressort des termes mêmes de l'ordonnance attaquée que, pour rejeter la requête de M. B..., le magistrat désigné par le président de la commission du contentieux du stationnement payant s'est fondé sur l'inopérance du moyen tiré de l'absence d'obligation de payer la somme réclamée par l'administration, au motif qu'il mettait ainsi en cause la légalité de l'avis de paiement auquel le titre exécutoire s'était substitué. Il résulte de ce qui a été dit ci-dessus que l'ordonnance attaquée est, sur ce point, entachée d'une erreur de droit. M. B... est, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, fondé à en demander l'annulation.<br/>
<br/>
              4. En revanche, l'Agence nationale de traitement automatisé des infractions, mise en cause pour observations, n'ayant pas qualité de partie dans la présente instance, les conclusions de M. B... tendant à ce qu'une somme soit mise à sa charge au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du magistrat désigné par le président de la commission du contentieux du stationnement payant du 18 février 2019 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la commission du contentieux du stationnement payant.<br/>
<br/>
Article 3 : Le surplus des conclusions de M. B..., présenté au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., au ministre de l'action et des comptes publics, à la commune de Saint-Denis et à l'Agence nationale de traitement automatisé des infractions.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
