<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038486933</ID>
<ANCIEN_ID>JG_L_2019_05_000000405926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/48/69/CETATEXT000038486933.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 17/05/2019, 405926, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:405926.20190517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...D..., Mme G...B...épouse D...et Mme H... A...ont demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir l'arrêté du 21 août 2013 par lequel le maire de Vanves (Hauts-de-Seine) a délivré un permis de construire une maison individuelle à M. E...F.... Par un jugement n° 1404025 du 11 décembre 2015, le tribunal administratif de Cergy-Pontoise a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 16VE00476 du 7 décembre 2016, enregistrée le 13 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 15 février 2016 au greffe de cette cour, présenté par M. et Mme D...et MmeA.... Par ce pourvoi et par un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 avril 2017 et 25 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. D...et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Vanves et de M. F...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
                           Vu : <br/>
                           - le code de l'urbanisme ;<br/>
                           - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. D...et autres, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. F...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Vanves.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 août 2013, le maire de Vanves a délivré à M. F...un permis de construire en vue de l'édification d'une maison d'habitation. Par un jugement du 11 décembre 2015, contre lequel M. D... et autres se pourvoient en cassation, le tribunal administratif de Cergy Pontoise a rejeté la demande des intéressés tendant à l'annulation pour excès de pouvoir de cette décision. <br/>
<br/>
              2.	En premier lieu, eu égard à l'argumentation dont il était saisi, le tribunal administratif, en relevant que la notice du projet architectural comportait l'ensemble des informations nécessaires à l'instruction de la demande, pour en déduire que les services compétents avaient pu se prononcer en connaissance de cause, a, sans les dénaturer, porté une appréciation souveraine sur les pièces du dossier. Son jugement est à cet égard exempt d'erreur de droit. <br/>
<br/>
              3.	En deuxième lieu, les moyens, qui ne sont ni d'ordre public ni nés du jugement attaqué, reprochant au tribunal administratif d'avoir commis une erreur de droit et une dénaturation des pièces du dossier en s'étant fondé sur la nécessité d'un raccordement séparé pour l'évacuation des eaux pluviales et usées pour écarter le moyen tiré de la méconnaissance des dispositions de l'article UD 4 du règlement du plan local d'urbanisme imposant un raccordement aux réseaux d'eau et d'électricité, procèdent d'une argumentation nouvelle en cassation et sont par suite, et en tout état de cause, inopérants. <br/>
<br/>
              4.	En troisième lieu, aux termes de l'article UD 7.2 du règlement du plan local d'urbanisme : " Dans une bande de 20 mètres comptés à partir de l'alignement ou de la limite de fait de la voie privée ou de l'emprise publique, les constructions, en tout ou partie, peuvent être implantées sur les limites séparatives. En cas de retrait, celui-ci doit respecter les distances prévues au paragraphe 7.3. / Au-delà de la bande de 20 mètres comptés à partir de l'alignement ou de la limite de fait de la voie privée ou de l'emprise publique, les constructions doivent être implantées en retrait des limites séparatives dans le respect des distances prévues au paragraphe 7.3, sauf si la construction projetée s'adosse à une construction existante en bon état et de dimensions égales ou supérieures sur le terrain voisin (...) ". Pour écarter l'argumentation excipant de la méconnaissance de ces dispositions, le tribunal administratif a relevé, au terme d'une appréciation souveraine des pièces du dossier exempte de dénaturation, que, contrairement à ce que soutenaient les requérants, il ressortait des pièces du dossier que l'intégralité de la façade ouest de la construction projetée était implantée dans la bande des 20 mètres à partir de l'alignement ou de la limite de la voie et que les imprécisions minimes qui affecteraient certaines cotes, à les supposer établies, n'étaient pas de nature à empêcher le service instructeur de procéder utilement à l'examen du projet. Eu égard à la teneur des écritures dont il était saisi, en se prononçant ainsi, le tribunal administratif n'a pas entaché son jugement d'une insuffisance de motivation. En mentionnant l'absence d'incidence d'éventuelles erreurs de cotations, le tribunal administratif s'est borné, sans commettre d'erreur de droit, à répondre à une branche de l'argumentation soulevée devant lui mettant en cause une insuffisance du dossier de demande. <br/>
<br/>
              5.	En quatrième lieu, aux termes de l'article UD 9 du règlement du plan local d'urbanisme : " L'emprise au sol des constructions ne peut excéder 50 % de la superficie du terrain (...) / Pour les terrains dont la superficie est inférieure ou égale à 200 m², l'emprise au sol des constructions peut atteindre 80 % de la superficie du terrain. ". Il résulte de ces dispositions que, pour les terrains dont la superficie est inférieure ou égale à 200 m², l'emprise au sol des constructions peut atteindre jusqu'à 80 %, et non 50 %, de la superficie, sans exiger que cela soit justifié par des circonstances particulières. Par suite, en relevant, au terme d'une appréciation souveraine des pièces du dossier, non arguée de dénaturation, que le terrain d'assiette du projet litigieux avait une superficie de 143 m² et qu'ainsi il permettait une emprise au sol de 114,4 m², pour juger que l'article UD9 n'avait pas été en l'espèce méconnu, le tribunal n'a pas entaché son jugement d'une erreur de droit. <br/>
<br/>
              6.	En cinquième lieu, aux termes de l'article UD 13.2 du règlement du plan local d'urbanisme, " (...) Pour les terrains dont la superficie est inférieure ou égale à 200 m², la surface des espaces verts doit représenter au moins 10 % de la superficie du terrain, sauf dans le secteur UDb (...) ". Le tribunal, qui a pu, en tout état de cause, légalement se fonder sur les plans joints à la demande de permis de construire modificatif pour apprécier le respect de ces dispositions, a relevé au terme d'une appréciation souveraine des pièces du dossier, exempte de dénaturation, que la surface d'espaces verts, soit 31 m², représentait au moins 10 % de la superficie du terrain. <br/>
<br/>
              7.	Il résulte de ce qui précède que M. D...et autres ne sont pas fondés à demander l'annulation du jugement qu'ils attaquent. <br/>
<br/>
              8.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Vanves ou de M. F..., qui ne sont pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. D...et autres, d'une part, la somme globale de 1 500 euros à verser à la commune de Vanves et, d'autre part, la somme globale de 1 500 euros à verser à M.F..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. D...et autres est rejeté.<br/>
Article 2 : M. D...et autres verseront, d'une part, la somme globale de 1 500 euros à la commune de Vanves et, d'autre part, la somme globale de 1 500 euros à M.F..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. C...D..., premier dénommé, pour l'ensemble des requérants, à la commune de Vanves et à M. E...F....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
