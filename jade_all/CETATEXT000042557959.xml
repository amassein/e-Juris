<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042557959</ID>
<ANCIEN_ID>JG_L_2020_11_000000434354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/55/79/CETATEXT000042557959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 23/11/2020, 434354, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434354.20201123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental de Paris de l'ordre des chirurgiens-dentistes a porté plainte contre Mme A... B... et la SELARL de chirurgiens-dentistes Agnès B... devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des chirurgiens-dentistes. Par deux décisions du 20 février 2017, la chambre disciplinaire de première instance a infligé à Mme B... et à la SELARL de chirurgiens-dentistes Agnès B... la sanction de l'interdiction d'exercer la profession de chirurgien-dentiste pour une durée de trois mois.<br/>
<br/>
              Par une décision du 8 juillet 2019, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a, sur appels de Mme B... et de la SELARL de chirurgiens-dentistes Agnès B..., refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 4124-6 du code de la santé publique et refusé de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, réformé la sanction prononcée en première instance pour l'assortir du sursis pour une durée d'un mois, dit que la sanction serait exécutée du 1er octobre au 30  novembre 2019 et rejeté le surplus des conclusions des requêtes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 6 et 17 septembre 2019 et le 3 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... et la SELARL de chirurgiens-dentistes Agnès B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs appels ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental de Paris de l'ordre des chirurgiens-dentistes la somme de 4 000 euros au titre de l'article L.  761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 56 ;<br/>
              - la directive 2000/31/CE du Parlement européen et du Conseil du 8 juin 2000, notamment son article 8 ;<br/>
              - l'arrêt n° C-339/15 du 4 mai 2017 de la Cour de justice de l'Union européenne ;<br/>
              - l'ordonnance n° C-296/18 du 23 octobre 2018 de la Cour de justice de l'Union européenne ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B... et de la SELARL de chirurgiens-dentistes Agnes B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite notamment d'un constat d'huissier du 7 décembre 2016 et de constatations effectuées par ses membres le 16 décembre 2016, le conseil départemental de Paris de l'ordre des chirurgiens-dentistes a, le 20 février 2017, saisi la chambre disciplinaire de première instance de l'ordre des chirurgiens-dentistes d'Ile-de-France d'une plainte à l'encontre de Mme B..., chirurgien-dentiste exerçant à Paris, et de la SELARL de chirurgiens-dentistes Agnès B... dont elle est la gérante. Par deux décisions du 20 février 2017, la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des chirurgiens-dentistes a infligé à Mme B... et à la SELARL de chirurgiens-dentistes Agnès B... la sanction de l'interdiction d'exercer la profession de chirurgien-dentiste pour une durée de trois mois. Par une décision du 8 juillet 2019, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a réformé la sanction prononcée en première instance pour l'assortir du sursis pour une durée d'un mois. Mme B... et la SELARL de chirurgiens-dentistes Agnès B... se pourvoient en cassation contre cette décision.<br/>
<br/>
              2. Aux termes de l'article R. 4127-215 du code de la santé publique : " La profession dentaire ne doit pas être pratiquée comme un commerce. Sont notamment interdits : / 1° L'exercice de la profession dans un local auquel l'aménagement ou la signalisation donne une apparence commerciale ; / 2° Toute installation dans un ensemble immobilier à caractère exclusivement commercial ; / 3° Tous procédés directs ou indirects de publicité ; / 4° Les manifestations spectaculaires touchant à l'art dentaire et n'ayant pas exclusivement un but scientifique ou éducatif ". <br/>
<br/>
              3. Il résulte des stipulations de l'article 56 du traité sur le fonctionnement de l'Union européenne, telles qu'interprétées par la Cour de justice de l'Union européenne dans son arrêt rendu le 4 mai 2017 dans l'affaire C -339/15, ainsi que des dispositions de l'article 8 paragraphe 1 de la directive 2000/31/CE du Parlement européen et du Conseil, du 8 juin 2000, relative à certains aspects juridiques des services de la société de l'information, et notamment du commerce électronique, dans le marché intérieur ("directive sur le commerce électronique"), telles qu'interprétées par la Cour de justice de l'Union européenne dans son ordonnance rendue le 23 octobre 2018 dans l'affaire C-296/18, qu'elles s'opposent à des dispositions réglementaires qui interdisent de manière générale et absolue toute publicité et toute communication commerciale par voie électronique, telles que celles qui figurent au 5ème alinéa de l'article R. 4127-215 du code de la santé publique. Par suite, en jugeant que les dispositions du 3° de l'article R. 4127-215 du code de la santé publique n'étaient pas incompatibles avec le droit de l'Union européenne et en retenant que Mme B... et la SELARL de chirurgiens-dentistes Agnès B... avaient commis un manquement en les méconnaissant, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a entaché sa décision d'erreur de droit. <br/>
<br/>
              4. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que Mme B... et la SELARL de chirurgiens-dentistes Agnès B... sont fondées à demander l'annulation de la décision du 8 juillet 2019 de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de Paris de l'ordre des chirurgiens-dentistes une somme globale de 3 000 euros à verser à Mme B... et à la SELARL de chirurgiens-dentistes Agnès B... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 8 juillet 2019 de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes est annulée.<br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes.<br/>
Article 3 : Le conseil départemental de Paris de l'ordre des chirurgiens-dentistes versera à Mme B... et à la SELARL de chirurgiens-dentistes Agnès B... une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme A... B..., à la SELARL de chirurgiens-dentistes Agnès B... et au conseil départemental de Paris de l'ordre des chirurgiens-dentistes.<br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
