<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042120821</ID>
<ANCIEN_ID>JG_L_2020_07_000000428881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/12/08/CETATEXT000042120821.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 15/07/2020, 428881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428881.20200715</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 15 mars 2019, 11 mai et 23 juin 2020 au secrétariat du contentieux du Conseil d'Etat, l'Association Service jésuite des réfugiés, l'Association des chrétiens pour l'abolition de la torture, l'Association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour, l'Association Centre Primo Levi, l'Association Dom'asile, le Groupe accueil et solidarité, le Groupe d'information et de soutien des immigrés et la Ligue des droits de l'homme demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre et le ministre de l'intérieur ont rejeté leur demande, reçue le 15 novembre 2018, tendant à l'adoption des mesures utiles pour assurer le plein respect de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre et au ministre de l'intérieur d'adopter les mesures utiles pour assurer le plein respect de cette directive ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2018-778 du 10 septembre 2018 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat de l'Association Service jésuite des réfugiés, l'Association des chrétiens pour l'abolition de la torture, l'Association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour, l'Association Centre Primo Levi, l'Association Dom'asile, le Groupe accueil et solidarité, le Groupe d'information et de soutien des immigrés et la Ligue des droits de l'homme.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er juillet 2020, présentée par l'association Service jésuite des réfugiés et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale prévoit à ses articles 15 et 16, au titre des dispositions générales relatives aux conditions d'accueil, les obligations pesant sur les Etats membres en ce qui concerne l'emploi et la formation professionnelle des ressortissants de pays tiers et des apatrides ayant présenté une demande de protection internationale sur laquelle il n'a pas encore été statué définitivement. Par un courrier reçu le 15 novembre 2018, l'association Service jésuite des réfugiés et sept autres associations ont saisi le Premier ministre et le ministre de l'intérieur d'une demande tendant à ce que ceux-ci prennent les mesures nécessaires, notamment en modifiant certaines dispositions réglementaires du code du travail, pour assurer la complète mise en oeuvre des articles 15 et 16 de cette directive. Elles sollicitent l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé sur leur demande.<br/>
<br/>
              Sur l'accès au marché du travail :<br/>
<br/>
              2. Selon l'article 15 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale, " 1.  Les États membres veillent à ce que les demandeurs aient accès au marché du travail dans un délai maximal de neuf mois à compter de la date d'introduction de la demande de protection internationale lorsque aucune décision en première instance n'a été rendue par l'autorité compétente et que le retard ne peut être imputé au demandeur. / 2.  Les États membres décident dans quelles conditions l'accès au marché du travail est octroyé au demandeur, conformément à leur droit national, tout en garantissant que les demandeurs ont un accès effectif à ce marché. / Pour des motifs liés à leur politique du marché du travail, les États membres peuvent accorder la priorité aux citoyens de l'Union et aux ressortissants des États parties à l'accord sur l'Espace économique européen, ainsi qu'aux ressortissants de pays tiers en séjour régulier. / 3.  L'accès au marché du travail n'est pas retiré durant les procédures de recours, lorsqu'un recours formé contre une décision négative prise lors d'une procédure normale a un effet suspensif, jusqu'au moment de la notification d'une décision négative sur le recours ".<br/>
<br/>
              3. Aux termes du premier alinéa de l'article L. 744-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction issue de la loi du 10 septembre 2018 pour une immigration maîtrisée, un droit d'asile effectif et une intégration réussie : " L'accès au marché du travail peut être autorisé au demandeur d'asile lorsque l'Office français de protection des réfugiés et apatrides, pour des raisons qui ne sont pas imputables au demandeur, n'a pas statué sur la demande d'asile dans un délai de six mois à compter de l'introduction de la demande. (...) Le demandeur d'asile est soumis aux règles de droit commun applicables aux travailleurs étrangers pour la délivrance d'une autorisation de travail. Toutefois, l'autorité administrative dispose d'un délai d'instruction de deux mois à compter de la réception de la demande d'autorisation de travail pour s'assurer que l'embauche de l'étranger respecte les conditions de droit commun d'accès au marché du travail. A défaut de notification dans ce délai, l'autorisation est réputée acquise. Elle est applicable pour la durée du droit au maintien du séjour du demandeur d'asile ". Le demandeur d'asile est alors autorisé à travailler, en vertu du 14° de l'article R. 5221-3 du code du travail, par une " autorisation provisoire de travail, (...) délivrée (...) à l'étranger salarié qui, par la nature de son séjour ou de son activité, ne relève pas du champ d'application des autorisations de travail " résultant de la nature du titre de séjour qu'il possède. En vertu de l'article R. 5221-20 du même code, pour se prononcer sur la demande d'autorisation de travail, formée par l'employeur, " le préfet prend en compte les éléments d'appréciation suivants : / 1° La situation de l'emploi dans la profession et dans la zone géographique pour lesquelles la demande est formulée, compte tenu des spécificités requises pour le poste de travail considéré, et les recherches déjà accomplies par l'employeur auprès des organismes concourant au service public de l'emploi pour recruter un candidat déjà présent sur le marché du travail ; / 2° L'adéquation entre la qualification, l'expérience, les diplômes ou titres de l'étranger et les caractéristiques de l'emploi auquel il postule ; (...) / 3° le respect par l'employeur, l'utilisateur mentionné à l'article L. 1251-1 ou l'entreprise d'accueil de la législation relative au travail et à la protection sociale ; / 4° Le cas échéant, le respect par l'employeur, l'utilisateur, l'entreprise d'accueil ou le salarié des conditions réglementaires d'exercice de l'activité considérée ; / 5° Les conditions d'emploi et de rémunération offertes à l'étranger, qui sont comparables à celles des salariés occupant un emploi de même nature dans l'entreprise ou, à défaut, conformes aux rémunérations pratiquées sur le marché du travail pour l'emploi sollicité (...) ".<br/>
<br/>
              4. En premier lieu, si l'article 15 de la directive du 26 juin 2013 exige que les demandeurs d'asile aient un accès effectif au marché du travail, au plus tard neuf mois après l'introduction de leur demande de protection internationale lorsque aucune décision en première instance n'a été rendue et que le retard ne peut leur être imputé, il laisse aux États membres la possibilité de décider des conditions dans lesquelles cet accès leur est octroyé et d'accorder une priorité, pour l'accès à ce marché, non seulement aux citoyens de l'Union et aux ressortissants des États parties à l'accord sur l'Espace économique européen, mais aussi, pour des motifs liés à leur politique du marché du travail, aux ressortissants de pays tiers en séjour régulier sur leur territoire. En soumettant les demandeurs d'asile aux règles de droit commun applicables aux travailleurs étrangers pour la délivrance d'une autorisation de travail et en prévoyant que la demande peut en être faite dès lors que l'Office français de protection des réfugiés et apatrides, pour des raisons qui ne sont pas imputables au demandeur, n'a pas statué sur la demande d'asile dans un délai de six mois, l'autorité administrative disposant alors d'un délai d'instruction de deux mois au terme duquel l'autorisation de travail est réputée acquise, le législateur n'a pas adopté de dispositions incompatibles avec l'article 15 de la directive du 26 juin 2013. Par suite, les associations requérantes ne sont, en tout état de cause, pas fondées à soutenir que l'article L. 744-11 du code de l'entrée et du séjour des étrangers et du droit d'asile méconnaîtrait les objectifs de cet article de la directive. <br/>
<br/>
              5.  En second lieu, il résulte clairement des termes du paragraphe 3 de l'article 15 de la directive du 26 juin 2013, telles qu'ils résultent du rectificatif publié au Journal officiel de l'Union européenne du 17 avril 2015 pour remplacer le mot " refusé " par " retiré ", en concordance avec la version anglaise de la directive, qu'un demandeur d'asile qui s'est vu octroyer l'accès au marché du travail doit conserver ce bénéfice en cas d'exercice d'un recours suspensif contre une décision négative prise lors d'une procédure normale, mais non que les Etats membres seraient tenus d'accorder cet accès à un demandeur qui n'en bénéficiait pas préalablement à un tel recours. Par suite, en tout état de cause, les associations requérantes ne sont pas fondées à soutenir que l'article L. 744-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ne serait pas compatible avec le paragraphe 3 de l'article 15 de la directive du 26 juin 2013 au motif qu'il se borne à prévoir que l'accès au marché du travail peut être autorisé au demandeur d'asile lorsque l'Office français de protection des réfugiés et apatrides, pour des raisons qui ne sont pas imputables au demandeur, n'a pas statué sur sa demande d'asile dans un délai de six mois et que l'autorisation de travail qui lui est délivrée est applicable pour la durée de son droit au maintien du séjour.<br/>
<br/>
              Sur l'accès aux formations professionnelles et à l'accompagnement des personnes à la recherche d'un emploi : <br/>
<br/>
              6. Selon l'article 16 de la directive du 26 juin 2013, " Les États membres peuvent autoriser l'accès des demandeurs à la formation professionnelle, que ceux-ci aient ou non accès au marché du travail. / L'accès à la formation professionnelle liée à un contrat d'emploi est subordonné à la possibilité, pour le demandeur, d'accéder au marché du travail conformément à l'article 15 ".<br/>
<br/>
              7. En premier lieu, le second alinéa de l'article L. 744-11 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que : " Le demandeur d'asile qui accède au marché du travail, dans les conditions prévues au premier alinéa du présent article, bénéficie des actions de formation professionnelle continue prévues à l'article L. 6313-1 du code du travail ". Aux termes de l'article L. 6313-1 du code du travail : " Les actions concourant au développement des compétences qui entrent dans le champ d'application des dispositions relatives à la formation professionnelle sont : / 1° Les actions de formation ; / 2° Les bilans de compétences ; / 3° Les actions permettant de faire valider les acquis de l'expérience (...) ; / 4° Les actions de formation par apprentissage (...) ". <br/>
<br/>
              8. Il résulte de la combinaison de l'article L. 744-11 du code de l'entrée et du séjour des étrangers et du droit d'asile et de l'article L. 6313-1 du code du travail que le demandeur d'asile qui accède au marché du travail bénéficie des actions de formation professionnelle continue dans les mêmes conditions que tout autre travailleur. Ces dispositions ne sont pas incompatibles avec l'article 16 de la directive du 26 juin 2013, qui donne aux Etats membres la faculté d'autoriser l'accès des personnes demandant la protection internationale à la formation professionnelle, alors même qu'elles n'auraient pas accès au marché du travail, sans les y contraindre, et impose seulement que l'accès à la formation professionnelle liée à un contrat d'emploi soit subordonné à la possibilité, pour le demandeur, d'accéder au marché du travail.<br/>
<br/>
              9. En second lieu, l'article R. 5221-47 du code du travail prévoit que : " Pour demander son inscription sur la liste des demandeurs d'emploi, le travailleur étranger doit satisfaire aux conditions d'inscription prévues par la section 1 du chapitre premier du titre premier du livre IV, et notamment à celles mentionnées aux articles R. 5411-2 et R. 5411-3 et au 5° de l'article R. 5411-6 relatives à la justification de la régularité de sa situation au regard des dispositions qui réglementent l'exercice d'activités professionnelles par les étrangers ". L'article R. 5221-48 du même code précise que : " Pour être inscrit, le travailleur étranger doit être titulaire de l'un des titres de séjour suivants : / (...) 5° (...) l'autorisation provisoire de travail mentionnée au 14° de l'article R. 5221-3, lorsque le contrat de travail, conclu avec un employeur établi en France, a été rompu avant son terme, du fait de l'employeur, pour un motif qui lui est imputable ou pour un cas de force majeure (...) ".<br/>
<br/>
              10. Il ne résulte d'aucune disposition de la directive du 26 juin 2013 que les personnes ayant présenté une demande de protection internationale devraient bénéficier, pour l'accès au placement et à l'accompagnement des demandeurs d'emploi, de dispositions différant des règles de droit commun applicables aux travailleurs étrangers. Par suite, les associations requérantes ne sont pas fondées à soutenir que les articles R. 5221-47 et R. 5221-48 du code du travail méconnaîtraient les articles 15 et 16 de la directive du 26 juin 2013.<br/>
<br/>
              11. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre de l'intérieur, que les associations requérantes ne sont pas fondées à soutenir que le Premier ministre et le ministre de l'intérieur auraient illégalement rejeté leur demande tendant à ce qu'ils prennent les mesures nécessaires, notamment en modifiant les dispositions réglementaires du code du travail qu'elles critiquent, pour assurer la pleine mise en oeuvre des dispositions des articles 15 et 16 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale. <br/>
<br/>
              12. Par suite, leurs conclusions aux fins d'injonction et leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1 : La requête de l'association Service jésuite des réfugiés et autres est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'association Service jésuite des réfugiés, représentante désignée, pour l'ensemble des associations requérantes, et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre et à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
