<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659285</ID>
<ANCIEN_ID>JG_L_2018_11_000000415811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/92/CETATEXT000037659285.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/11/2018, 415811, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415811.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 9 mai 2018, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme A...B...dirigées contre le jugement n°s 1604750, 1607783 du tribunal administratif de Lyon du 27 juin 2017 en tant seulement que ce jugement se prononce sur sa demande relative à la récupération d'un indu d'aides exceptionnelles de fin d'année.<br/>
<br/>
              Le pourvoi a été communiqué au ministre des solidarités et de la santé, qui n'a pas produit de mémoire.<br/>
<br/>
              La caisse d'allocations familiales du Rhône a produit des observations, enregistrées le 10 juillet 2018.<br/>
<br/>
<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2012-1468 du 27 décembre 2012 ;<br/>
              - le décret n° 2013-1294 du 30 décembre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de la situation de MmeB..., bénéficiaire du revenu de solidarité active, la caisse d'allocations familiales du Rhône lui a fait connaître, le 28 octobre 2015, sa décision de récupérer un indu d'un montant initial de 7 073,78 euros pour la période d'octobre 2012 à février 2015, correspondant à des trop-perçus d'allocation de revenu de solidarité active, d'aides exceptionnelles de fin d'année au titre des années 2012 et 2013 et d'allocations de logement. Par une demande enregistrée au greffe du tribunal administratif de Lyon le 25 octobre 2016, Mme B...a demandé à ce tribunal d'annuler la décision du 28 octobre 2015 en tant qu'elle lui notifie un indu au titre des aides exceptionnelles de fin d'année, la décision implicite rejetant sa demande de décharge de cet indu et la décision du 11 avril 2016 par laquelle la caisse d'allocations familiales du Rhône a refusé de lui accorder une remise gracieuse du même indu. Par une décision du 9 mai 2018, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme B...dirigées contre le jugement du tribunal administratif de Lyon du 27 juin 2017 en tant qu'il rejette cette demande. <br/>
<br/>
              2. Lorsque le juge administratif est saisi d'un recours dirigé contre une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu d'aide exceptionnelle de fin d'année, il entre dans son office d'apprécier, au regard de l'argumentation du requérant, le cas échéant, de celle développée par le défendeur et, enfin, des moyens d'ordre public, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction, la régularité comme le bien-fondé de la décision de récupération d'indu. Il lui appartient, s'il y a lieu, d'annuler ou de réformer la décision ainsi attaquée, pour le motif qui lui paraît, compte tenu des éléments qui lui sont soumis, le mieux à même, dans l'exercice de son office, de régler le litige. L'organisme débiteur du revenu de solidarité active, qui doit apprécier si le bénéficiaire satisfaisait aux conditions d'ouverture du droit à cette aide prévues par la réglementation applicable et vérifier si les délais de prescription de l'action tendant à la répétition de l'aide indûment perçue ne font pas obstacle à la récupération, ne peut être regardé comme placé en situation de compétence liée, du seul fait qu'il estime à bon droit que le bénéficiaire ne pouvait prétendre au revenu de solidarité active, lorsqu'il décide de récupérer un indu d'aide exceptionnelle de fin d'année. <br/>
<br/>
              3. Il ressort des pièces de la procédure de première instance qu'à l'appui de sa demande présentée devant le tribunal administratif de Lyon, Mme B...soutenait notamment que la décision du 28 octobre 2015 avait été prise par une autorité incompétente, n'était pas suffisamment motivée et procédait d'une erreur de droit. Or le tribunal a déduit de ce que Mme B... n'avait pas droit au revenu de solidarité active que l'administration était en situation de compétence liée pour procéder à la récupération des aides exceptionnelles de fin d'année pour les années 2012 et 2013 qu'elle avait perçues et que ces moyens étaient inopérants. Il résulte de ce qui a été dit ci-dessus que Mme B...est fondée à soutenir que le tribunal a ainsi commis une erreur de droit.<br/>
<br/>
              4. Il suit de là que Mme B...est fondée à demander l'annulation du jugement du tribunal administratif de Lyon qu'elle attaque en tant qu'il statue sur sa demande relative à la récupération d'un indu d'aides exceptionnelles de fin d'année.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du pourvoi présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 27 juin 2017 est annulé en tant qu'il statue sur la demande n° 1607783 de Mme B...relative à la récupération d'un indu d'aides exceptionnelles de fin d'année.<br/>
Article 2 : Le jugement de la demande n° 1607783 de Mme B...relative à la récupération d'un indu d'aides exceptionnelles de fin d'année est renvoyé au tribunal administratif de Lyon.<br/>
Article 3 : Les conclusions du pourvoi présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 son rejetées.<br/>
Article 4 : La présente décision sera notifiée à MmeA... B... et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la caisse d'allocations familiales du Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
