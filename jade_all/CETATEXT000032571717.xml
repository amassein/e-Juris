<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571717</ID>
<ANCIEN_ID>JG_L_2016_05_000000385305</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571717.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 385305</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385305</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385305.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, deux mémoires complémentaires et un mémoire en réplique, enregistrés les 23 octobre 2014, 29 décembre 2014, 10 décembre 2015 et 9 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société Celtipharm demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre des affaires sociales, de la santé et des droits des femmes sur sa demande du 19 juillet 2014 tendant à l'abrogation de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du Système national d'information interrégimes de l'assurance maladie ; <br/>
<br/>
              2°) d'enjoindre au ministre des affaires sociales, de la santé et des droits des femmes d'abroger cet arrêté.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 21 ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 98-1194 du 23 décembre 1998 ;<br/>
              - la loi n° 2004-806 du 9 août 2004 ;<br/>
              - la décision du 27 février 2015 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Celtipharm ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Celtipharm ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'eu égard aux moyens qu'elle invoque, la société Celtipharm doit être regardée comme demandant l'annulation pour excès de pouvoir de la décision implicite de rejet résultant du silence gardé par le ministre des affaires sociales, de la santé et des droits des femmes sur sa demande du 19 juillet 2014, tendant à l'abrogation de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du Système national d'information interrégimes de l'assurance maladie, en tant seulement qu'elle concerne les dispositions du 3° du III de l'article 4 de cet arrêté en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations prévues à son article 3 ;<br/>
<br/>
              Sur l'irrecevabilité opposée par le ministre des affaires sociales, de la santé et des droits des femmes : <br/>
<br/>
              2. Considérant que la société requérante a pour objet, notamment, en vertu de l'article III de ses statuts, " la création, le développement, la gestion et l'actualisation de bases de données ou de connaissances, dans l'univers de la pharmacie (...), la diffusion et le transit des informations afférentes en France et à l'étranger " et la " mise à disposition des informations publiques, professionnelles et commerciales sur tous sites " ; que, par suite, contrairement à ce que soutient le ministre, cette société justifie d'un intérêt lui donnant qualité pour agir contre la décision attaquée dans la mesure mentionnée au point 1 ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              3. Considérant, en premier lieu, que la loi du 23 décembre 1998 de financement de la sécurité sociale pour 1999 a prévu la création d'un système national d'information interrégimes de l'assurance maladie (SNIIRAM), devant être mis en place par les organismes gérant un régime de base d'assurance maladie, qui lui transmettent les données nécessaires ; qu'aux termes de l'article L. 161-28-1 du code de la sécurité sociale : " (...) Les modalités de gestion et de renseignement du système national d'information interrégimes de l'assurance maladie, définies conjointement par protocole passé entre au moins la Caisse nationale de l'assurance maladie des travailleurs salariés, la Caisse centrale de mutualité sociale agricole et la Caisse nationale du régime social des indépendants, sont approuvées par un arrêté du ministre chargé de la sécurité sociale (...) pris après avis motivé de la Commission nationale de l'informatique et des libertés (...) " ;  <br/>
<br/>
              4. Considérant que l'arrêté du 19 juillet 2013 du ministre des affaires sociales et de la santé relatif à la mise en oeuvre du SNIIRAM, pris après avis de la Commission nationale de l'informatique et des libertés, approuve, à son article 1er, le protocole du 8 juin 2012 et ses annexes définissant les modalités de gestion et de renseignement du système, passé entre les caisses d'assurance maladie mentionnées à l'article L. 161-28-1 du code de la sécurité sociale, et définit, à ses articles 2 à 7, les finalités du traitement ainsi mis en oeuvre, la liste des informations nécessaires, rassemblées dans une base de données nationale, les destinataires de ces informations, ainsi que les droits d'accès à ces informations, de rectification et d'opposition ; qu'à ce titre, le III de l'article 4 de cet arrêté prévoit que les destinataires des informations contenues dans le SNIIRAM sont, à raison de leurs fonctions et selon des règles d'habilitation définies par le protocole, en particulier, en vertu du 3°, certains organismes de recherche, en précisant que : " (...) Le traitement des informations (...) demandé par tout autre organisme de recherche, des universités, écoles ou autres structures d'enseignement liés à la recherche (...) est soumis à l'approbation du bureau de l'Institut des données de santé. Aucun organisme de recherche, université, école ou autre structure d'enseignement lié à la recherche poursuivant un but lucratif ne peut accéder aux informations de l'article 3. La CNIL, conformément aux dispositions du chapitre X de la loi du 6 janvier 1978 susvisée, autorise ces traitements " ;<br/>
<br/>
              5. Considérant qu'en adoptant les dispositions en cause du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013, en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3, le ministre des affaires sociales et de la santé ne s'est pas borné à approuver les modalités de gestion et de renseignement du SNIIRAM, dont les destinataires habilités à recevoir communication des données qu'il rassemble, définies conjointement par le protocole passé entre les caisses d'assurance maladie ; que, contrairement à ce que fait valoir le ministre en défense, ces dispositions ne se déduisent pas de l'annexe II du protocole, dont l'objet est seulement d'identifier, pour certains utilisateurs, l'autorité compétente pour accéder aux informations contenues dans le SNIIRAM ; qu'il a ainsi lui-même fixé une telle règle ; que, dès lors, ces dispositions ne trouvent pas leur fondement légal dans les dispositions précitées de l'article L. 161-28-1 du code de la sécurité sociale ; que si le ministre se prévaut de l'article 3.5 du protocole, qui prévoit la fixation par arrêté ministériel des destinataires des informations contenues dans le SNIIRAM, une telle clause ne saurait fonder légalement sur ce point la compétence du ministre chargé de la sécurité sociale ; qu'aucun autre texte législatif ou réglementaire, notamment la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, ne donne compétence à ce ministre pour déterminer les organismes de recherche ou d'enseignement pouvant accéder aux données du SNIIRAM ; que, dès lors, ces dispositions sont entachées d'incompétence ;<br/>
<br/>
              6. Considérant, en second lieu, que le chapitre X de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, en vigueur à la date de la décision attaquée, régit les traitements de données de santé à caractère personnel à des fins d'évaluation ou d'analyse des pratiques ou des activités de soins et de prévention ; qu'en vertu de son article 63, les données " issues des systèmes d'information des caisses d'assurance maladie, ne peuvent être communiquées à des fins statistiques d'évaluation ou d'analyse des pratiques et des activités de soins et de prévention que sous la forme de statistiques agrégées ou de données par patient constituées de telle sorte que les personnes concernées ne puissent être identifiées. / Il ne peut être dérogé aux dispositions de l'alinéa précédent que sur autorisation de la Commission nationale de l'informatique et des libertés dans les conditions prévues aux articles 64 à 66 (...) " ; qu'aux termes du premier alinéa de l'article 64 de la même loi, alors en vigueur : " Pour chaque demande, la commission vérifie les garanties présentées par le demandeur pour l'application des présentes dispositions et, le cas échéant, la conformité de sa demande à ses missions ou à son objet social. Elle s'assure de la nécessité de recourir à des données à caractère personnel et de la pertinence du traitement au regard de sa finalité déclarée d'évaluation ou d'analyse des pratiques ou des activités de soins et de prévention. Elle vérifie que les données à caractère personnel dont le traitement est envisagé ne comportent ni le nom, ni le prénom des personnes concernées, ni leur numéro d'inscription au Répertoire national d'identification des personnes physiques. En outre, si le demandeur n'apporte pas d'éléments suffisants pour attester la nécessité de disposer de certaines informations parmi l'ensemble des données à caractère personnel dont le traitement est envisagé, la commission peut interdire la communication de ces informations par l'organisme qui les détient et n'autoriser le traitement que des données ainsi réduites " ;<br/>
<br/>
              7. Considérant que ces dispositions ne restreignent pas la qualité des personnes susceptibles de solliciter, à des fins d'évaluation des pratiques de soins et de prévention, la communication d'informations issues du SNIIRAM sous forme de statistiques agrégées ou de données insusceptibles, même par recoupement avec d'autres données, d'identifier les personnes concernées ; que si elles soumettent à l'autorisation de la Commission nationale de l'informatique et des libertés, qui doit notamment vérifier les garanties présentées par le demandeur, la communication de données susceptibles de permettre une telle identification, elles n'excluent pas qu'une personne poursuivant un but lucratif puisse bénéficier d'une autorisation, dès lors que toutes les conditions en seraient remplies ; que les dispositions de l'article L. 161-28-1 du code de la sécurité sociale citées ci-dessus ne prévoient pas plus une telle exclusion ; qu'ainsi, en prévoyant que les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne pourraient accéder aux informations mentionnées à son article 3, l'arrêté du 19 juillet 2013 a ajouté une condition non prévue par la loi ; qu'il est, pour ce motif, également entaché d'illégalité ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la décision attaquée doit être annulée en tant qu'elle concerne les dispositions du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du SNIIRAM, en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations prévues à son article 3 ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              9. Considérant que l'annulation de la décision attaquée implique nécessairement que le ministre chargé de la sécurité sociale abroge les dispositions du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du SNIIRAM en ce qu'elles prévoient que les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3 ; qu'il y a lieu, par suite, d'enjoindre au ministre d'y procéder dans un délai de quatre mois à compter de la notification de la présente décision ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le ministre des affaires sociales, de la santé et des droits des femmes a refusé d'abroger l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du Système national d'information interrégimes de l'assurance maladie est annulée en tant qu'elle concerne les dispositions du 3° du III de l'article 4 de cet arrêté en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3.<br/>
Article 2 : Il est enjoint au ministre des affaires sociales et de la santé d'abroger les dispositions du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du Système national d'information interrégimes de l'assurance maladie en ce qu'elles prévoient que les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3, dans un délai de quatre mois à compter de la notification de la présente décision.<br/>
Article 3 : La présente décision sera notifiée à la société Celtipharm et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03-15 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. MINISTRE CHARGÉ DE LA SÉCURITÉ SOCIALE. - LOI CONFIANT UN POUVOIR RÉGLEMENTAIRE À UNE CONVENTION ENTRE ORGANISMES DE SÉCURITÉ SOCIALE APPROUVÉE PAR ARRÊTÉ MINISTÉRIEL - FIXATION PAR LE MINISTRE D'UNE RÈGLE NON CONTENUE DANS LA CONVENTION - INCOMPÉTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. - SNIIRAM - FIXATION DES MODALITÉS DE GESTION ET DE RENSEIGNEMENT PAR UN PROTOCOLE CONCLU PAR LES CAISSES ET APPROUVÉ PAR ARRÊTÉ MINISTÉRIEL - POSSIBILITÉ DE DÉFINIR LES DESTINATAIRES DES DONNÉES DANS LE PROTOCOLE - EXISTENCE - FIXATION PAR LE MINISTRE D'UNE RÈGLE NON CONTENUE DANS LA CONVENTION - INCOMPÉTENCE.
</SCT>
<ANA ID="9A"> 01-02-02-01-03-15 En vertu de l'article L. 161-28-1 du code de la sécurité sociale (CSS), les modalités de gestion et de renseignement du système national d'information interrégimes de l'assurance maladie (SNIIRAM) sont définies par un protocole entre les caisses nationales approuvé par un arrêté du ministre chargé de la sécurité sociale pris après avis motivé de la Commission nationale de l'informatique et des libertés (CNIL).... ,,En adoptant les dispositions du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du SNIIRAM, en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3, le ministre ne s'est pas borné à approuver les modalités de gestion et de renseignement du SNIIRAM, dont fait partie la définition des destinataires habilités à recevoir communication des données qu'il rassemble et qui sont définies conjointement par le protocole passé entre les caisses d'assurance maladie. Ces dispositions, qui ne se déduisent pas du protocole, ne sauraient non plus trouver leur fondement dans l'article 3.5 du protocole, qui prévoit la fixation par arrêté ministériel des destinataires des informations contenues dans le SNIIRAM. Incompétence du ministre.</ANA>
<ANA ID="9B"> 62-01 En vertu de l'article L. 161-28-1 du code de la sécurité sociale (CSS), les modalités de gestion et de renseignement du système national d'information interrégimes de l'assurance maladie (SNIIRAM) sont définies par un protocole entre les caisses nationales approuvé par un arrêté du ministre chargé de la sécurité sociale pris après avis motivé de la Commission nationale de l'informatique et des libertés (CNIL).... ,,En adoptant les dispositions du 3° du III de l'article 4 de l'arrêté du 19 juillet 2013 relatif à la mise en oeuvre du SNIIRAM, en vertu desquelles les organismes de recherche, universités, écoles ou autres structures d'enseignement liées à la recherche poursuivant un but lucratif ne peuvent accéder aux informations mentionnées à son article 3, le ministre ne s'est pas borné à approuver les modalités de gestion et de renseignement du SNIIRAM, dont fait partie la définition des destinataires habilités à recevoir communication des données qu'il rassemble et qui sont définies conjointement par le protocole passé entre les caisses d'assurance maladie. Incompétence du ministre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
