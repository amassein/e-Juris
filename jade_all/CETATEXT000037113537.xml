<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037113537</ID>
<ANCIEN_ID>JG_L_2018_06_000000419595</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/11/35/CETATEXT000037113537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 27/06/2018, 419595, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419595</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419595.20180627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat de l'enseignement supérieur SNESUP-FSU, à l'appui de sa requête tendant à l'annulation du jugement n° 1703016 du 14 décembre 2017 du tribunal administratif de Strasbourg ayant rejeté sa demande d'annulation de l'élection, le 13 décembre 2016, de M. A...B..., en qualité de président de l'université de Strasbourg, a produit un mémoire, enregistré le 12 février 2018 au greffe de la cour administrative d'appel de Nancy, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 18NC00333 du 6 avril 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre de la cour administrative d'appel de Nancy, avant qu'il soit statué sur la requête d'appel du syndicat de l'enseignement supérieur SNESUP-FSU, a décidé, en application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat cette question relative à la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 712-2 du code de l'éducation.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation, notamment son article L. 712-2 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du syndicat national de l'enseignement supérieur SNESUP-FSU et à la SCP Waquet, Farge, Hazan, avocat de l'université de Strasbourg ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat lui a transmis, en application de l'article 23-2 de cette même ordonnance, la question de la conformité aux droits et libertés garantis par la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 712-2 du code de l'éducation : " Le président de l'université est élu à la majorité absolue des membres du conseil d'administration parmi les enseignants-chercheurs, chercheurs, professeurs ou maîtres de conférences, associés ou invités, ou tous autres personnels assimilés, sans condition de nationalité. Son mandat, d'une durée de quatre ans, expire à l'échéance du mandat des représentants élus des personnels du conseil d'administration. Il est renouvelable une fois " ; que le troisième alinéa du même article dispose que les fonctions de président d'université " sont incompatibles avec celles de membre élu du conseil académique, de directeur de composante, d'école ou d'institut ou de toute autre structure interne de l'université et avec celles de dirigeant exécutif de tout établissement public à caractère scientifique, culturel et professionnel ou de l'une de ses composantes ou structures internes " ; qu'au soutien de sa demande d'annulation de l'élection, le 13 décembre 2016, de M. B..., en qualité de président de l'université de Strasbourg, le syndicat requérant soutient que ces dispositions méconnaissent les principes constitutionnels de laïcité et " d'indépendance de la recherche et des enseignants-chercheurs " en ce que, faute de prévoir une incompatibilité entre les fonctions de président d'université et " l'exercice concomitant d'une charge ou d'une fonction religieuse ", elles ne font pas obstacle à l'élection d'ecclésiastiques à la présidence d'universités publiques ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 10 de la Déclaration des droits de l'homme et du citoyen de 1789 : " Nul ne doit être inquiété pour ses opinions, même religieuses, pourvu que leur manifestation ne trouble pas l'ordre public établi par la loi " ; qu'aux termes de l'article 1er de la Constitution : " La France est une République indivisible, laïque, démocratique et sociale. Elle assure l'égalité devant la loi de tous les citoyens sans distinction d'origine, de race ou de religion. Elle respecte toutes les croyances (...)  " ; que le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit ; que notamment, il en résulte la neutralité de l'Etat, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion ; <br/>
<br/>
              4. Considérant qu'il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion ; que, par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions ; que, par suite, la question de la conformité au principe constitutionnel de laïcité des dispositions législatives contestées par le syndicat requérant, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              5. Considérant, en second lieu, que la circonstance que le président élu d'une université aurait la qualité de ministre d'un culte est, par elle-même, sans rapport avec les garanties qui s'attachent au respect du principe constitutionnel d'indépendance des enseignants-chercheurs ; que les dispositions litigieuses de l'article L. 712-2 du code de l'éducation prévoient au demeurant que le président de l'université est élu parmi les enseignants-chercheurs, chercheurs, professeurs ou maîtres de conférences ; que, par suite, la question tirée, par le syndicat requérant, de la violation d'un " principe d'indépendance de la recherche et des enseignants-chercheurs ", qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Nancy.<br/>
Article 2 : La présente décision sera notifiée au syndicat de l'enseignement supérieur SNESUP-FSU, à l'université de Strasbourg et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la cour administrative d'appel de Nancy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - PRINCIPE DE LAÏCITÉ - PORTÉE - 1) ACCÈS AUX FONCTIONS PUBLIQUES - ACCÈS SANS DISTINCTION DE CROYANCE ET DE RELIGION [RJ1] - 2) MINISTRE D'UN CULTE ÉLU AUX FONCTIONS DE PRÉSIDENT D'UNIVERSITÉ - QUALITÉ FAISANT OBSTACLE À L'ACCÈS À CES FONCTIONS - ABSENCE - OBLIGATIONS DANS L'EXERCICE DES FONCTIONS TENANT À L'ABSENCE DE MANIFESTATION DE SES OPINIONS RELIGIEUSES ET EN DEHORS DE CELLES-CI AU DEVOIR DE NEUTRALITÉ - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-03-07 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ DES CULTES. - PRINCIPE DE LAÏCITÉ - PORTÉE - 1) ACCÈS AUX FONCTIONS PUBLIQUES - ACCÈS SANS DISTINCTION DE CROYANCE ET DE RELIGION [RJ1] - 2) MINISTRE D'UN CULTE ÉLU AUX FONCTIONS DE PRÉSIDENT D'UNIVERSITÉ - QUALITÉ FAISANT OBSTACLE À L'ACCÈS À CES FONCTIONS - ABSENCE - OBLIGATIONS DANS L'EXERCICE DES FONCTIONS TENANT À L'ABSENCE DE MANIFESTATION DE SES OPINIONS RELIGIEUSES ET EN DEHORS DE CELLES-CI AU DEVOIR DE NEUTRALITÉ - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">30-02-05-01-038 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. PRÉSIDENTS D'UNIVERSITÉ. - PRINCIPE DE LAÏCITÉ - PORTÉE - ACCÈS AUX FONCTIONS DE PRÉSIDENT D'UNIVERSITÉ - ACCÈS SANS DISTINCTION DE CROYANCE ET DE RELIGION [RJ1] - ELECTION D'UN MINISTRE D'UN CULTE - QUALITÉ FAISANT OBSTACLE À L'ACCÈS À CES FONCTIONS - ABSENCE - OBLIGATIONS DANS L'EXERCICE DES FONCTIONS TENANT À L'ABSENCE DE MANIFESTATION DE SES OPINIONS RELIGIEUSES ET EN DEHORS DE CELLES-CI AU DEVOIR DE NEUTRALITÉ - EXISTENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-03 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. - PRINCIPE DE LAÏCITÉ - PORTÉE - 1) ACCÈS AUX FONCTIONS PUBLIQUES - ACCÈS SANS DISTINCTION DE CROYANCE ET DE RELIGION [RJ1] - 2) MINISTRE D'UN CULTE ÉLU AUX FONCTIONS DE PRÉSIDENT D'UNIVERSITÉ - QUALITÉ FAISANT OBSTACLE À L'ACCÈS À CES FONCTIONS - ABSENCE - OBLIGATIONS DANS L'EXERCICE DES FONCTIONS TENANT À L'ABSENCE DE MANIFESTATION DE SES OPINIONS RELIGIEUSES ET EN DEHORS DE CELLES-CI AU DEVOIR DE NEUTRALITÉ - EXISTENCE [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">36-07-11 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. - PRINCIPE DE LAÏCITÉ - PORTÉE - 1) ACCÈS AUX FONCTIONS PUBLIQUES - ACCÈS SANS DISTINCTION DE CROYANCE ET DE RELIGION [RJ1] - 2) MINISTRE D'UN CULTE ÉLU AUX FONCTIONS DE PRÉSIDENT D'UNIVERSITÉ - QUALITÉ FAISANT OBSTACLE À L'ACCÈS À CES FONCTIONS - ABSENCE - OBLIGATIONS DANS L'EXERCICE DES FONCTIONS TENANT À L'ABSENCE DE MANIFESTATION DE SES OPINIONS RELIGIEUSES ET EN DEHORS DE CELLES-CI AU DEVOIR DE NEUTRALITÉ - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-04-005 1) Le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit. Notamment, il en résulte la neutralité de l'État, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion.,,,2) Il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion. Par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions.</ANA>
<ANA ID="9B"> 26-03-07 1) Le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit. Notamment, il en résulte la neutralité de l'État, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion.,,,2) Il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion. Par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions.</ANA>
<ANA ID="9C"> 30-02-05-01-038 Le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit. Notamment, il en résulte la neutralité de l'État, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion.,,,Il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion. Par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions.</ANA>
<ANA ID="9D"> 36-03 1) Le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit. Notamment, il en résulte la neutralité de l'État, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion.,,,2) Il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion. Par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions.</ANA>
<ANA ID="9E"> 36-07-11 1) Le principe de laïcité figure au nombre des droits et libertés que la Constitution garantit. Notamment, il en résulte la neutralité de l'État, le respect de toutes les croyances et l'égalité de tous les citoyens devant la loi sans distinction de religion.,,,2) Il résulte ainsi du principe constitutionnel de laïcité que l'accès aux fonctions publiques, dont l'accès aux fonctions de président d'université, s'effectue sans distinction de croyance et de religion. Par suite, il ne peut, en principe, être fait obstacle à ce qu'une personne ayant la qualité de ministre d'un culte puisse être élue aux fonctions de président d'université, celle-ci étant alors tenue, eu égard à la neutralité des services publics qui découle également du principe de laïcité, à ne pas manifester ses opinions religieuses dans l'exercice de ses fonctions ainsi qu'à un devoir de réserve en dehors de l'exercice de ces fonctions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur le respect des convictions d'un agent public, CE, 28 avril 1938,,p. 379 ; CE, 8 décembre 1948,,, p. 464 ; CE, 3 mai 1950,,, p. 247 ; CE, 10 avril 2000,,n° 311888, p. 158.,,[RJ2] Rappr., sur les obligations d'un agent public, CE, avis, 3 mai 2000,,n° 217017, p.169 ; CE, 15 octobre 2003,,, n° 244428, p. 402 ; CE, 19 février 2009,,, n° 311633, T. p. 813 ; CE, 28 juillet 2017,,et autres, n°s 390740 390741 390742, à mentionner aux Tables ; rappr. Cass. Ass. Plén., 25 juin 2014, n° 13-28369, Bull. Ass. plén. 2014, n° 1.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
