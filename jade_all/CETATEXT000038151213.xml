<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151213</ID>
<ANCIEN_ID>JG_L_2019_02_000000423024</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 20/02/2019, 423024</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423024</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423024.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme C...B...épouse A...a demandé au tribunal administratif de Rouen d'annuler la décision du 16 janvier 2017 par laquelle la préfète de la Seine-Maritime a accordé le concours de la force publique pour procéder, à compter du 1er avril 2017, à son expulsion du logement qu'elle occupe au 7, place du Boulingrin à Rouen. Par un jugement n° 1701192 du 13 mars 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 8 août et 8 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée, alors même qu'elle n'aurait produit aucune défense, peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : (...) 3° Sur les litiges relatifs aux refus de concours de la force publique pour exécuter une décision de justice ; (...) ". Il résulte des termes mêmes de ces dispositions, qui ne visent que les décisions relatives aux refus de concours de la force publique, que les jugements par lesquels le tribunal administratif statue sur les litiges relatifs aux décisions octroyant le concours de la force publique pour exécuter une décision de justice sont susceptibles d'appel.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...a saisi le tribunal administratif de Rouen d'une demande tendant à l'annulation de la décision du 16 janvier 2017 par laquelle la préfète de la Seine-Maritime a accordé le concours de la force publique pour procéder, à compter du 1er avril 2017, à l'expulsion de son logement, afin d'exécuter un jugement du tribunal d'instance de Rouen du 15 mars 2016. Par jugement du 13 mars 2018, le tribunal administratif a rejeté sa demande. Il résulte de ce qui précède que ce jugement est susceptible d'appel. Il y a lieu, en conséquence, de renvoyer la requête de Mme A... à la cour administrative d'appel de Douai.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement de la requête de Mme A...est renvoyé à la cour administrative d'appel de Douai.<br/>
Article 2 : La présente décision sera notifiée à Mme C...B..., épouseA..., et au préfet de la Seine-Maritime.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - EXCLUSION - LITIGES RELATIFS AUX DÉCISIONS OCTROYANT LE CONCOURS DE LA FORCE PUBLIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - INCLUSION - LITIGES RELATIFS AUX DÉCISIONS OCTROYANT LE CONCOURS DE LA FORCE PUBLIQUE.
</SCT>
<ANA ID="9A"> 17-05-012 Il résulte des termes même de l'article L. 811-1 du code de justice administrative (CJA), qui ne vise que les décisions relatives aux refus de concours de la force publique, que les jugements par lesquels le tribunal administratif statue sur les litiges relatifs aux décisions octroyant le concours de la force publique pour exécuter une décision de justice sont susceptibles d'appel.</ANA>
<ANA ID="9B"> 17-05-015 Il résulte des termes même de l'article L. 811-1 du code de justice administrative (CJA), qui ne vise que les décisions relatives aux refus de concours de la force publique, que les jugements par lesquels le tribunal administratif statue sur les litiges relatifs aux décisions octroyant le concours de la force publique pour exécuter une décision de justice sont susceptibles d'appel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
