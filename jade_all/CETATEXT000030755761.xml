<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030755761</ID>
<ANCIEN_ID>JG_L_2015_06_000000386971</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/57/CETATEXT000030755761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 18/06/2015, 386971, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386971</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:386971.20150618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes (URFEPRA) ont demandé au juge des référés du tribunal administratif de Grenoble de suspendre l'exécution de l'arrêté du 3 octobre 2014 par lequel le préfet de l'Isère a accordé à la SNC Roybon Cottages une autorisation au titre de l'article L. 214-3 du code de l'environnement. Par une ordonnance n° 1406934 du 23 décembre 2014, le juge des référés du tribunal administratif de Grenoble a suspendu l'exécution de cet arrêté du 3 octobre 2014.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire additionnel et un mémoire en réplique, enregistrés les 7 janvier, 19 janvier, 16 mars et 21 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la SNC Roybon Cottages demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de Grenoble ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par les requérantes de première instance ;<br/>
<br/>
              3°) de mettre à la charge de la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et de l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes (URFEPRA) la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              La SNC Roybon Cottages soutient que : <br/>
              -	le juge des référés du tribunal administratif de Grenoble  a commis une erreur de droit en retenant que le moyen tiré de l'absence de saisine obligatoire de la Commission nationale du débat public était opérant alors que l'autorisation délivrée en application de l'article L. 214-3 du code de l'environnement ne relève pas des catégories d'opérations listées par l'article R. 121-1 du même code ; <br/>
              -	le juge des référés a commis une erreur de droit en tenant compte de travaux étrangers à l'autorisation litigieuse pour statuer sur le respect du seuil de 300 millions d'euros fixé à l'article R. 121-2 du code de l'environnement ; <br/>
              -	le juge des référés a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que le coût prévisionnel du projet dépassait le seuil de 300 millions d'euros fixés par l'article R. 121-2 du code de l'environnement alors que le coût des bâtiments et infrastructures visé par cette disposition est inférieure à ce montant ; <br/>
              -	le juge des référés a inexactement qualifié les faits de l'espèce ou les a dénaturés en retenant un doute sérieux sur la légalité de la décision litigieuse sur le fondement du moyen tiré de l'absence de saisine obligatoire de la Commission nationale du débat public alors que cette circonstance n'a pas caractérisé, en l'espèce, la privation d'une garantie et n'a pas exercé une influence sur le sens de la décision prise ; <br/>
              -	le juge des référés a commis une erreur de droit en vérifiant avec un même degré de contrôle le respect des dispositions de l'article L. 211-1 du code de l'environnement et celles du schéma directeur d'aménagement et de gestion des eaux de Rhône-Méditerranée ; <br/>
              -	le juge des référés a commis une erreur de droit en prononçant la suspension sur le fondement du doute sérieux relatif à l'insuffisance des mesures compensatoires sans rechercher si le juge du fond aurait pu la rectifier par des prescriptions supplémentaires ; <br/>
              -	l'ordonnance est irrégulière en ce que le juge des référés n'a pas identifié avec une précision suffisante le moyen dont il estimait qu'il créait un doute sérieux sur la légalité de l'autorisation litigieuse ; <br/>
              -	le juge des référés a entaché son ordonnance de dénaturation des pièces du dossier et d'erreur de droit en jugeant que le moyen tiré du caractère suffisant des mesures compensatoires à la destruction et à l'altération de zones humides était de nature à faire naître un doute sérieux sur la légalité de l'autorisation litigieuse ; <br/>
              -	le juge des référés a commis une erreur de droit en subordonnant la poursuite du projet à la condition d'une atteinte d'une particulière gravité portée à l'intérêt général ; <br/>
              -	le juge des référés a entaché son ordonnance de dénaturation des faits de l'espèce, d'insuffisance de motivation et de contradiction de motifs en jugeant que les conséquences de la suspension ne portaient pas à l'intérêt général une atteinte d'une particulière gravité.<br/>
<br/>
              	Par un mémoire en défense, enregistré le 9 avril 2015, la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes concluent au rejet du pourvoi et à ce que la somme de 4 000 euros soit mise à la charge de la SNC Roybon Cottages au titre de l'article L. 761-1 du code de justice administrative. Elles soutiennent qu'aucun des moyens soulevés par le pourvoi n'est fondé.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SNC Roybon Cottages et à la SCP Piwnica, Molinié, avocat de la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes (URFEPRA) ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 214-3 du code de l'environnement : " I.-Sont soumis à autorisation de l'autorité administrative les installations, ouvrages, travaux et activités susceptibles de présenter des dangers pour la santé et la sécurité publique, de nuire au libre écoulement des eaux, de réduire la ressource en eau, d'accroître notablement le risque d'inondation, de porter gravement atteinte à la qualité ou à la diversité du milieu aquatique, notamment aux peuplements piscicoles. (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Grenoble que par un arrêté du 3 octobre 2014, le préfet de l'Isère a accordé une autorisation au titre des dispositions précitées de l'article L. 214-3 du code de l'environnement à la SNC Roybon Cottages ; que par une ordonnance du 23 décembre 2014, contre laquelle la SNC Roybon Cottages se pourvoit en cassation, le juge des référés du tribunal administratif de Grenoble a suspendu l'exécution de cet arrêté sur le fondement de l'article L. 123-16 du code de l'environnement ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article L. 121-8 du code de l'environnement : " I.-La Commission nationale du débat public est saisie de tous les projets d'aménagement ou d'équipement qui, par leur nature, leurs caractéristiques techniques ou leur coût prévisionnel, tel qu'il peut être évalué lors de la phase d'élaboration, répondent à des critères ou excèdent des seuils fixés par décret en Conseil d'Etat. (...) " ; qu'aux termes de l'article R. 121-1 du même code : " I.-Lorsqu'ils répondent aux conditions prévues aux articles R. 121-2 et R. 121-3, sont soumis aux dispositions du présent chapitre les projets d'aménagement ou d'équipement d'intérêt national de l'Etat, des collectivités territoriales, des établissements publics et des personnes privées entrant dans les catégories d'opérations et de projets d'investissements suivantes : (...) 10° Equipements culturels, sportifs, scientifiques, touristiques ; (...) " ; qu'en application de l'article R. 121-2 du même code, le seuil visé au I de l'article L. 121-8, s'agissant de ces équipements, correspond à un coût des bâtiments et infrastructures supérieur à 300 millions d'euros ; <br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le juge des référés du tribunal administratif de Grenoble a estimé que le moyen tiré de ce que le projet de village de vacances Center Parcs aurait dû, eu égard à son coût prévisionnel, faire l'objet d'une saisine de la Commission nationale du débat public au titre du I de l'article L. 121-8 du code de l'environnement, était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision attaquée en précisant que n'y faisait pas obstacle la circonstance que l'autorisation litigieuse ne portait que sur certains travaux qui, pris isolément, n'atteignaient pas le seuil fixé par l'article R. 121-2 du code de l'environnement, ni celle que la SNC Roybon Cottages bénéficiait pour son projet d'un permis de construire devenu définitif ; que toutefois, la décision litigieuse a pour objet de délivrer une autorisation au titre de l'article L. 214-3 du code de l'environnement cité au point 1 et non d'autoriser un projet d'aménagement ou d'équipement au sens des dispositions de l'article R. 121-2 du même code ; que, dès lors, en retenant que le moyen tiré de l'absence de saisine de la Commission nationale du débat public était de nature à créer un doute sérieux sur la légalité de la décision litigieuse alors qu'il ne pouvait être utilement invoqué à l'encontre de cette décision eu égard à l'objet de celle-ci, le juge des référés a commis une erreur de droit ; <br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article L. 211-1 du code de l'environnement : " I. - Les dispositions des chapitres Ier à VII du présent titre ont pour objet une gestion équilibrée et durable de la ressource en eau ; cette gestion prend en compte les adaptations nécessaires au changement climatique et vise à assurer : / 1° La prévention des inondations et la préservation des écosystèmes aquatiques, des sites et des zones humides ; on entend par zone humide les terrains, exploités ou non, habituellement inondés ou gorgés d'eau douce, salée ou saumâtre de façon permanente ou temporaire ; la végétation, quand elle existe, y est dominée par des plantes hygrophiles pendant au moins une partie de l'année (...) " ; qu'en application du II de l'article R. 214-6 du même code, le dossier présenté en vue d'une autorisation au titre de l'article L. 214-3 du code de l'environnement comporte notamment un document relatif aux incidences du projet " précisant s'il y a lieu les mesures correctives ou compensatoires envisagées " ; qu'aux termes du XI de l'article L. 212-1 du même code :  " XI.-Les programmes et les décisions administratives dans le domaine de l'eau doivent être compatibles ou rendus compatibles avec les dispositions des schémas directeurs d'aménagement et de gestion des eaux. " ; qu'aux termes de l'orientation 6-B-6 du schéma directeur d'aménagement et de gestion des eaux Rhône-Méditerranée : " Après étude des impacts environnementaux, lorsque la réalisation d'un projet conduit à la disparition d'une surface de zones humides ou à l'altération de leur biodiversité, le SDAGE préconise que les mesures compensatoires prévoient dans le même bassin versant, soit la création de zones humides équivalentes sur le plan fonctionnel et de la biodiversité, soit la remise en état d'une surface de zones humides existantes, et ce à hauteur d'une valeur guide de l'ordre de 200 % de la surface perdue " ; <br/>
<br/>
              6. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le juge des référés a estimé qu'était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision litigieuse le moyen tiré de l'insuffisance des mesures compensatoires à la destruction et à l'altération de zones humides au regard des exigences fixées par l'article                     L. 211-1 du code de l'environnement et le schéma directeur d'aménagement et de gestion des eaux (SDAGE) Rhône-Méditerranée ; qu'il ressort des pièces du dossier soumis au juge des référés que l'arrêté litigieux comprend des prescriptions relatives aux mesures compensatoires supplémentaires qui sont compatibles avec le SDAGE et notamment avec le respect de la valeur guide de l'ordre de 200 % que celui-ci détermine ; que, dès lors, en retenant ce moyen comme propre à créer un doute sérieux sur la légalité de la décision litigieuse, le juge des référés a entaché son appréciation de dénaturation ; qu'il résulte de ce qui précède que la SNC Roybon Cottages est fondée à demander l'annulation de l'ordonnance attaquée ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes (URFEPRA), en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              8. Considérant que les moyens invoqués par la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'URFEPRA à l'appui de leur demande de suspension et tirés de la méconnaissance de l'article L. 121-8 du code de l'environnement s'agissant de l'absence de saisine de la Commission nationale du débat public et du non-respect des formalités prévues au II de cet article, de la méconnaissance de l'article L. 214-4 du même code compte tenu des modifications apportées au projet par le pétitionnaire après l'enquête publique, de la méconnaissance de l'article L. 122-1 du code de l'environnement s'agissant des travaux visés dans l'enquête publique, du caractère insuffisant de l'analyse de l'état initial de la zone et des milieux susceptibles d'être affectés par le projet prévue à l'article R. 122-5 du même code, de la méconnaissance de l'article L. 211-1 du code de l'environnement et du SDAGE s'agissant des mesures compensatoires à la destruction des zones humides et de la méconnaissance des orientations fondamentales du SDAGE ne paraissent pas, en l'état de l'instruction, propres à créer un doute sérieux sur la légalité de la décision contestée ; qu'il en résulte que la demande de suspension doit être rejetée ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat et de la SNC Roybon Cottages, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et de l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes la somme de 1 500 euros chacune à verser à la SNC Roybon Cottages ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Grenoble du 23 décembre 2014 est annulée.<br/>
Article 2 : La demande présentée par la Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes devant le juge des référés du tribunal administratif de Grenoble est rejetée.<br/>
Article 3 : La Fédération de la Drôme pour la pêche et la protection du milieu aquatique et l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes verseront la somme de 1 500 euros chacune à la SNC Roybon Cottages au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SNC Roybon Cottages, à la Fédération de la Drôme pour la pêche et la protection du milieu aquatique, à l'Union régionale des fédérations départementales des associations agréées de pêche et de protection du milieu aquatique de Rhône-Alpes (URFEPRA) et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée pour information au département de l'Isère.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
