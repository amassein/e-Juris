<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026198961</ID>
<ANCIEN_ID>JG_L_2012_07_000000343303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/19/89/CETATEXT000026198961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 16/07/2012, 343303, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Didier Chauvaux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:343303.20120716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés le 15 septembre et le 16 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés par la société SA Vortex, dont le siège est 37 bis, rue Greneta à Paris (75002), représentée par son président-directeur général en exercice ; la société Vortex demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions du 23 mars 2010 par lesquelles le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa candidature en vue de l'exploitation du service de radio Skyrock par voie hertzienne terrestre dans les zones de Nogent-le-Rotrou et Saint-Lô, relevant du comité technique radiophonique de Caen ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel de lui délivrer les autorisations demandées, sous astreinte de 500 euros par jour de retard à compter de la notification de la décision à intervenir, <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le moyen relatif aux conditions d'examen des candidatures : <br/>
<br/>
              1. Considérant qu'afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne terrestre qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance ; que la circonstance qu'une décision de rejet soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi du 30 septembre 1986, est sans incidence sur la légalité de cette décision ; que, dès lors, le moyen tiré de ce que les décisions de rejet des candidatures de la société Vortex à l'exploitation d'un service de radio par voie hertzienne terrestre dénommé Skyrock et relevant de la catégorie D dans les zones de Nogent-le-Rotrou et Saint-Lô, prises par le Conseil supérieur de l'audiovisuel lors de sa séance du 23 mars 2010, lui ont été notifiées par une lettre du 13 juillet 2010 alors que les décisions d'autorisation prises lors de la même séance, et que cette société n'a pas contestées, avaient été publiées au Journal officiel des 8 et 13 avril 2010, ne saurait être accueilli ;<br/>
<br/>
              Sur le moyen tiré d'une violation des dispositions de l'article 41 de la loi du 30 septembre 1986 :<br/>
<br/>
              2. Considérant qu'aux termes de cet article : " Une même personne physique ou morale ne peut, sur le fondement d'autorisations relatives à l'usage de fréquences dont elle est titulaire pour la diffusion d'un ou de plusieurs services de radio par voie hertzienne terrestre en mode analogique, ou par le moyen d'un programme qu'elle fournit à d'autres titulaires d'autorisation par voie hertzienne terrestre en mode analogique, disposer en droit ou en fait de plusieurs réseaux que dans la mesure où la somme des populations recensées dans les zones desservies par ces différents réseaux n'excède pas 150 millions d'habitants " ; <br/>
<br/>
              3. Considérant qu'à la supposer établie, la circonstance que le Conseil supérieur de l'audiovisuel aurait méconnu les dispositions précitées en délivrant des autorisations à des services contrôlés par les groupes NRJ et Lagardère Active Broadcast dans d'autres zones du ressort du comité radiophonique de Caen n'est pas opérante à l'encontre des décisions par lesquelles il a rejeté la candidature de Skyrock dans les zones de Nogent-le-Rotrou et Saint-Lô, où aucune autorisation n'a été accordée, à l'issue de l'appel à candidatures, à un service contrôlé par un de ces groupes ; <br/>
<br/>
              Sur le moyen tiré d'une violation des dispositions de l'article 29 de la loi du 30 septembre 1986 :<br/>
<br/>
              4. Considérant qu'aux termes de cet article, le Conseil supérieur de l'audiovisuel " accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence ... " ;<br/>
<br/>
              5. Considérant que, par ses communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le Conseil supérieur de l'audiovisuel, faisant usage des pouvoirs qu'il tient de ces dispositions, a déterminé cinq catégories de services en vue de l'appel à candidatures pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre ; que ces cinq catégories sont ainsi définies : services associatifs éligibles au fonds de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (C), services thématiques à vocation nationale (D), et services généralistes à vocation nationale (E) ;<br/>
<br/>
              6. Considérant qu'il ne ressort pas des pièces du dossier que le Conseil supérieur de l'audiovisuel, qui a statué, comme il y était tenu, sur l'ensemble des candidatures au cours d'une même séance, ait omis d'examiner l'intérêt particulier de chaque projet ; <br/>
<br/>
              En ce qui concerne la zone de Nogent-le-Rotrou :<br/>
<br/>
              7. Considérant que dans cette zone, où émettaient un service en catégorie A, deux services en catégorie B, un service en catégorie C, six services en catégorie D et trois services en catégorie E, le Conseil supérieur de l'audiovisuel a attribué la seule fréquence disponible à Jazz Radio, relevant de la catégorie D, " afin de compléter l'offre musicale de la zone par un format inédit " et a écarté la candidature de Skyrock, dans la même catégorie, au motif que sa " thématique rap s'adresse à une plus faible partie de la population que le candidat retenu, dans une ville de moins de 13 000 habitants où la population âgée de 15 à 29 ans a baissé entre 1999 et 2007 " ; que ce motif n'est entaché ni d'erreur de droit ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ; que la circonstance que la zone aurait comporté plus de 13 000 habitants est sans incidence sur la légalité de la décision attaquée dès lors qu'il ressort des pièces du dossier que le Conseil supérieur de l'audiovisuel n'a pas commis d'erreur de fait en ce qui concerne les caractéristiques de la population sur lesquelles il s'est fondé ;<br/>
<br/>
              En ce qui concerne la zone de Saint-Lô :<br/>
<br/>
              8. Considérant que dans cette zone, où émettaient un service en catégorie A, deux services en catégorie B, deux services en catégorie C, trois services en catégorie D, dont Fun Radio, et deux services en catégorie E, le Conseil supérieur de l'audiovisuel a attribué la seule fréquence disponible à Ouï FM, relevant de la catégorie D, " afin de compléter l'offre musicale de la zone " et a écarté, dans la même catégorie, la candidature de Skyrock " qui propose à un public jeune un format proche de celui de Fun Radio (...) et présente donc un format moins original dans la zone que le candidat retenu " ; que ce motif n'est entaché ni d'erreur de fait ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ; <br/>
<br/>
              Sur le moyen tiré d'une violation des stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              9. Considérant que les décisions attaquées, qui résultent d'une exacte application des critères posés par la loi du 30 septembre 1986, ne méconnaissent pas les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui garantit à toute personne le droit à la liberté d'expression et celui de recevoir et de communiquer des informations ou des idées ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la société Vortex n'est pas fondée à demander l'annulation des décisions du 23 mars 2010 par lesquelles le Conseil supérieur de l'audiovisuel lui a refusé l'autorisation d'exploiter le service Skyrock dans les zones de Nogent-le-Rotrou et Saint-Lô ; que doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction ainsi que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Vortex est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Vortex et au Conseil supérieur de l'audiovisuel. <br/>
Copie pour information en sera adressée à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
