<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027386266</ID>
<ANCIEN_ID>JG_L_2013_04_000000348137</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/38/62/CETATEXT000027386266.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 24/04/2013, 348137, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348137</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:348137.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 avril et 5 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Bessan (34550), représentée par son maire ; la commune de Bessan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0904359 du 1er février 2011 par lequel le tribunal administratif de Montpellier a annulé l'arrêté de son maire du 27 juillet 2009 maintenant M.  A... en disponibilité pour convenances personnelles jusqu'au 30 juin 2010 ;<br/>
<br/>
              	2°) de mettre à la charge de M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 86-68 du 13 janvier 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de la commune de Bessan et de la SCP Fabiani, Luc-Thaler, avocat de M.A..., <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la la SCP Gaschignard, avocat de la commune de Bessan et de la SCP Fabiani, Luc-Thaler, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " (...) Le fonctionnaire mis en disponibilité, soit d'office (...), soit de droit, sur demande, pour raisons familiales, est réintégré à l'expiration de sa période de disponibilité dans les conditions prévues aux premier, deuxième et troisième alinéas de l'article 67 de la présente loi. Dans les autres cas, si la durée de la disponibilité n'a pas excédé trois années, une des trois premières vacances dans la collectivité ou l'établissement d'origine doit être proposée au fonctionnaire " ; qu'aux termes de l'article 26 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité et de congé parental des fonctionnaires territoriaux : " (...) le fonctionnaire mis en disponibilité sur sa demande fait connaître à son administration d'origine sa décision de solliciter le renouvellement de la disponibilité ou de réintégrer son cadre d'emplois d'origine trois mois au moins avant l'expiration de la disponibilité. / (...) Le fonctionnaire qui a formulé avant l'expiration de la période de mise en disponibilité une demande de réintégration est maintenu en disponibilité jusqu'à ce qu'un poste lui soit proposé dans les conditions prévues à l'article 97 de la loi du 26 janvier 1984 (...) " ; qu'aux termes de l'article 97 de la loi du 26 janvier 1984 précitée : " (...) Si la collectivité ou l'établissement ne peut lui offrir un emploi correspondant à son grade, le fonctionnaire est maintenu en surnombre pendant un an. Pendant cette période, tout emploi créé ou vacant correspondant à son grade dans la collectivité ou l'établissement lui est proposé en priorité ; la collectivité (...) et le centre de gestion examinent, chacun pour ce qui le concerne, les possibilités de reclassement. (...) Au terme de ce délai, le fonctionnaire est pris en charge par le centre de gestion dans le ressort duquel se trouve la collectivité (...). / Pendant la période de prise en charge, (...) le centre (...) lui propose tout emploi vacant correspondant à son grade (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions, d'une part, qu'un fonctionnaire territorial mis en disponibilité pour convenances personnelles a le droit, à l'issue de sa période de disponibilité, d'obtenir sa réintégration sous réserve, toutefois, de la vacance d'un emploi correspondant à son grade, d'autre part, que, jusqu'à ce qu'un tel emploi lui soit proposé, ce fonctionnaire est maintenu en disponibilité, enfin, que la collectivité territoriale qui n'est pas en mesure de lui proposer un tel emploi doit saisir le centre de gestion de la fonction publique territoriale compétent afin qu'il lui propose tout emploi vacant correspondant à son grade ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A..., agent de salubrité publique de la commune de Bessan (Hérault) affecté sur l'emploi de conducteur de benne à ordures, a été, à sa demande, placé en disponibilité pour convenances personnelles pour une durée d'un an à compter du 1er juillet 2000 ; que sa disponibilité a fait l'objet de prolongations annuelles successives, d'abord à sa demande entre le 1er juillet 2001 et le 31 août 2005, puis, alors qu'il avait demandé sa réintégration, en raison de l'absence d'emploi vacant correspondant à son grade entre le 1er septembre 2005 et le 31 juin 2010 ; que la commune de Bessan se pourvoit en cassation contre le jugement du 1er février 2011 par lequel le tribunal administratif de Montpellier, faisant droit à la demande de M. A..., a annulé l'arrêté du maire du 27 juillet 2009 prolongeant la disponibilité de M. A... du 1er septembre 2009 au 30 juin 2010 ;<br/>
<br/>
              4. Considérant que, pour annuler l'arrêté du maire du 27 juillet 2009, le tribunal administratif s'est fondé sur les motifs tirés de ce que l'impossibilité pour la commune de réintégrer M. A... " n'était pas réellement établie " et de ce que la commune n'avait pas saisi le centre de gestion de la fonction publique territoriale de l'Hérault ; que le tribunal administratif a, sur le premier point, en se bornant à écarter ainsi le moyen invoqué par la commune tiré de ce qu'aucun emploi correspondant au grade " d'agent de salubrité " n'était vacant au moment de la dernière demande de réintégration présentée le 22 mai 2009 par M. A..., insuffisamment motivé son jugement et, sur le second point, dénaturé les pièces du dossier qui lui était soumis, desquelles il ressort que, le 1er octobre 2008, le maire de la commune de Bessan a saisi le centre de gestion d'une demande tendant à ce que lui soit indiqué la procédure à suivre pour inscrire M. A... à la bourse de l'emploi ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Bessan est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il y n'a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Bessan au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Fabiani Luc-Thaler, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 1er février 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Bessan est rejeté.<br/>
<br/>
Article 4 : Les conclusions présentées par la SCP Fabiani Luc-Thaler au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Bessan et à M. B... A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
