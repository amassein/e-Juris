<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032095939</ID>
<ANCIEN_ID>JG_L_2016_02_000000377195</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/09/59/CETATEXT000032095939.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 19/02/2016, 377195, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377195</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; LE PRADO ; SCP SEVAUX, MATHONNET ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:377195.20160219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme C...B...et M. A...B...ont demandé au tribunal administratif de Nantes de condamner le centre hospitalier de Saint-Nazaire à verser à Mme C... B...la somme de 131 586,10 euros, assortie des intérêts au taux légal à compter du 9 janvier 2008, en réparation des préjudices subis suite à sa prise en charge hospitalière du 13 mars au 14 juin 2001 puis, lors de son hospitalisation, du 15 au 26 juin 2001 et à M. A...B...la somme de 5 000 euros en réparation du préjudice moral subi suite à la prise en charge hospitalière de son épouse du 13 mars au 14 juin 2001 puis, lors de son hospitalisation, du 15 au 26 juin 2001. Par un jugement n° 0807350 du 21 juin 2012, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12NT02270 du 6 février 2014, la cour administrative d'appel de Nantes, sur appel de Mme et M.B..., a annulé ce jugement, condamné le centre hospitalier à verser 13 774,99 euros à M. et MmeB..., avec intérêts au taux légal à compter du 9 janvier 2008 et 19 278,02 euros à la CPAM de Loire-Atlantique et rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 7 avril et 7 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Saint-Nazaire la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
- le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. et MmeB..., à Me Le Prado, avocat du centre hospitalier de Saint-Nazaire et à la SCP Foussard, Froger, avocat de la caisse primaire d'assurance maladie de Loire-Atlantique et à la SCP Sevaux Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., diabétique, a consulté le 13 mars 2001 au centre hospitalier de Saint-Nazaire pour deux lésions du pied gauche et qu'une collection purulente en cours de constitution a été observée ; que Mme B...a regagné son domicile avec une prescription de soins infirmiers quotidiens et de suivi hospitalier hebdomadaire ; que l'extension de la nécrose du talon et l'aspect inflammatoire de la plaie ont conduit à une hospitalisation dans le même établissement du 15 au 26 juin 2001 et qu'un prélèvement bactériologique a mis en évidence la présence de deux germes, pseudomonas aeruginosa multi-résistant et staphylococcus aureus multi-sensible ; qu'en raison de complications, Mme B...a été hospitalisée, de nouveau, du 14 au 31 août 2001 et que des prélèvements bactériologiques effectués le 22 août ont permis d'identifier le même germe pseudomonas aeruginosa ainsi qu'un germe enterococcus faecalis ; que, compte tenu de la gravité de son état, Mme B...a été hospitalisée en urgence le 8 septembre et a subi une amputation partielle de la jambe gauche le 17 septembre 2001 ; que les prélèvements réalisés sur le moignon ont mis en évidence, le 11 octobre 2001, le même germe pseudomonas aeruginosa et un germe citrobacter freundi et, le 28 novembre 2001, le germe pseudomonas aeruginosa ainsi que le germe staphylococcus aureus ; que, bien que l'expert désigné par une ordonnance du 25 mars 2008 du juge des référés du tribunal administratif de Nantes ait conclu à l'absence de faute, Monsieur et Madame B...ont formé contre le centre hospitalier de Saint-Nazaire un recours indemnitaire ; que, par jugement avant dire droit du 8 juin 2011, le tribunal administratif de Nantes a écarté le caractère nosocomial de l'infection et l'existence d'une faute dans la prise en charge de Mme B...du 15 au 26 juin 2001 mais a jugé que le centre hospitalier avait commis une faute dans la prise en charge de la patiente du 13 mars au 14 juin 2001 tenant à l'absence de prélèvements bactériologiques et à l'absence d'examen par un médecin lors des visites hebdomadaires  ; que, par le même jugement, il a ordonné une expertise complémentaire afin d'évaluer la perte de chance subie par l'intéressée d'échapper aux complications qui ont conduit à l'amputation partielle de la jambe gauche ; que, par jugement du 21 juin 2012, le tribunal administratif de Nantes, retenant l'absence de lien entre les fautes commises par le centre hospitalier et l'évolution de l'état de santé de l'intéressée,  a rejeté le recours indemnitaire des époux B...; que, par un arrêt du 6 février 2014, la cour administrative d'appel de Nantes a écarté le caractère nosocomial de l'infection ainsi que les fautes relatives à l'hospitalisation du 15 au 26 juin 2001 et à l'absence de revascularisation de la jambe mais a imputé une perte de chance de 20% subie par Mme B...à diverses fautes du centre hospitalier dans sa prise en charge du 13 mars au 14 juin de la même année, à l'absence, durant cette période, de prélèvement bactériologique, à l'origine d'un retard dans la mise en oeuvre du traitement du germe multi-résistant, ainsi qu'à l'absence de tout médecin aux consultations hebdomadaires qui ont eu lieu à l'hôpital ; que M. et Mme B...se pourvoient en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que la cour, en se bornant à affirmer que les germes à l'origine de l'infection litigieuse étaient présents avant le 13 mars 2001, alors, d'une part, qu'aucune pièce du dossier qui lui était soumis n'établissait cette présence et que, d'autre part, le rapport du premier expert mentionnait que la contamination par les germes staphylococcus aureus et pseudomonas aeroginosa pouvait être d'origine hospitalière, a insuffisamment motivé son arrêt ; qu'en outre, elle a omis de prendre parti sur l'origine du germe enterococcus faecalis, décelé dès le 22 août, après huit jours d'hospitalisation, et du germe citrobacter freundi, retrouvés au niveau de la plaie et du moignon, alors qu'il était soutenu qu'ils avaient été contractés à l'hôpital ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. et Mme B...sont fondés à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Saint-Nazaire le versement aux époux B...et à la caisse primaire d'assurance maladie de la Loire-Atlantique d'une somme de 3 000 euros chacun, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 6 février 2014 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Le centre hospitalier de Saint-Nazaire versera aux époux B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le centre hospitalier de Saint-Nazaire versera à la caisse primaire d'assurance maladie de Loire Atlantique une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme C...B...et M. A...B..., au centre hospitalier de Saint-Nazaire, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurance maladie de Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
