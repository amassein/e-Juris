<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986105</ID>
<ANCIEN_ID>JG_L_2014_12_000000382204</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/61/CETATEXT000029986105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 29/12/2014, 382204</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382204</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:382204.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. A...C..., demeurant au "... ; M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1400660 du 3 juin 2014 par lequel le tribunal administratif de Pau a, à la demande de M. F...M..., d'une part, annulé son élection en qualité de conseiller municipal lors des opérations électorales qui se sont déroulées le 30 mars 2014 dans la commune d'Aignan, d'autre part, proclamé M. M...en qualité de conseiller municipal ; <br/>
<br/>
              2°) de mettre à la charge de M. M...le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte de l'instruction qu'à l'issue du second tour des opérations électorales qui se sont déroulées les 23 et 30 mars 2014 en vue de l'élection du conseil municipal de la commune d'Aignan, M. A...C...a été proclamé élu conseiller municipal au bénéfice de l'âge, conformément aux dispositions de l'article L. 253 du code électoral, avec 214 voix, à égalité avec M. F...M...; que M. C...relève appel du jugement par lequel le tribunal administratif de Pau a annulé son élection et proclamé élu M. M... ; <br/>
<br/>
              2.	Considérant que l'article R. 52 du code électoral dispose que : " Le bureau se prononce provisoirement sur les difficultés qui s'élèvent touchant les opérations électorales. / Ses décisions sont motivées. Toutes les réclamations et décisions sont inscrites au procès-verbal, les pièces qui s'y rapportent y sont annexées après avoir été paraphées par les membres du bureau. (...) " ; que, par ailleurs, aux termes de l'article R. 68 du même code : " Les pièces fournies à l'appui des réclamations et des décisions prises par le bureau, ainsi que les feuilles de pointage sont jointes au procès-verbal. / Les bulletins autres que ceux qui, en application de la législation en vigueur, doivent être annexés au procès-verbal sont détruits en présence des électeurs. " ;<br/>
              3.	Considérant qu'il résulte de l'instruction qu'au terme des opérations électorales relatives au second tour des élections municipales dans la commune d'Aignan, aucune réclamation n'a été inscrite au procès-verbal établi par l'unique bureau de vote conformément aux dispositions de l'article R. 52 du code électoral précité ; que contrairement au second alinéa de l'article R. 68 du code électoral précité, les bulletins de vote déclarés valides par le bureau de vote n'ont pas été détruits mais placés dans deux enveloppes ; que, le lendemain des opérations de vote, le maire de la commune a extrait un bulletin de l'une de ces enveloppes hors la présence de l'ensemble des membres du bureau de vote et en l'absence de tout membre de la liste d'opposition ; que M. M...a fondé sa protestation sur le caractère équivoque et donc irrégulier de ce bulletin qui, selon lui, n'aurait pas dû être pris en compte ;<br/>
<br/>
              4.	Considérant qu'en l'absence de toute indication relative à ce bulletin litigieux dans le procès verbal des opérations de vote, auquel il n'a par ailleurs pas été annexé, sa validité ne pouvait plus être utilement contestée ; qu'il en résulte, sans qu'il soit besoin d'examiner les autres moyens de la requête, que M. C...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Pau a retranché ce bulletin des suffrages qu'il avait obtenus et modifié les résultats de l'élection en proclamant élu à sa place, au deuxième tour du scrutin, M. M...; <br/>
<br/>
              5.	Considérant qu'en l'absence d'autre grief soulevé par M. M...devant le tribunal administratif, il y a lieu d'annuler le jugement du 3 juin 2014 et de valider l'élection de M. C...en qualité de conseiller municipal de la commune d'Aignan ; <br/>
<br/>
              6.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Pau du 3 juin 2014 est annulé.<br/>
<br/>
Article 2 : L'élection de M. C...en qualité de conseiller municipal de la commune d'Aignan est validée.<br/>
<br/>
Article 3 : La protestation de M. M...est rejetée.<br/>
<br/>
Article 4 : Le surplus des conclusions de la requête en appel de M. C...est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...C..., à M. F...M..., à M. D... H..., à M. F...G..., à M. K... L..., à M. J... I..., à M. E... B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-05-01-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. OPÉRATIONS ÉLECTORALES. DÉROULEMENT DU SCRUTIN. BULLETINS DE VOTE. - BULLETIN N'AYANT PAS ÉTÉ NI DÉCLARÉ NUL NI MENTIONNÉ AU PROCÈS-VERBAL DES OPÉRATIONS ÉLECTORALES - POSSIBILITÉ PAR LA SUITE D'EN CONTESTER UTILEMENT LA VALIDITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-04-05-04-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. OPÉRATIONS ÉLECTORALES. DÉPOUILLEMENT. DÉCOMPTE DES BULLETINS. - BULLETIN N'AYANT PAS ÉTÉ NI DÉCLARÉ NUL NI MENTIONNÉ AU PROCÈS-VERBAL DES OPÉRATIONS ÉLECTORALES - POSSIBILITÉ PAR LA SUITE D'EN CONTESTER UTILEMENT LA VALIDITÉ - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-08-05-02-04 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. GRIEFS. GRIEFS INOPÉRANTS. - BULLETIN N'AYANT PAS ÉTÉ NI DÉCLARÉ NUL NI MENTIONNÉ AU PROCÈS-VERBAL DES OPÉRATIONS ÉLECTORALES - POSSIBILITÉ PAR LA SUITE D'EN CONTESTER UTILEMENT LA VALIDITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 28-04-05-01-02 Lorsqu'un bulletin n'a été ni déclaré nul ni mentionné au procès-verbal des opérations électorales, et aurait dès lors dû être détruit, sa validité ne peut plus être utilement contestée.</ANA>
<ANA ID="9B"> 28-04-05-04-02 Lorsqu'un bulletin n'a été ni déclaré nul ni mentionné au procès-verbal des opérations électorales, et aurait dès lors dû être détruit, sa validité ne peut plus être utilement contestée.</ANA>
<ANA ID="9C"> 28-08-05-02-04 Lorsqu'un bulletin n'a été ni déclaré nul ni mentionné au procès-verbal des opérations électorales, et aurait dès lors dû être détruit, sa validité ne peut plus être utilement contestée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
