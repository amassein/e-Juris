<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037194512</ID>
<ANCIEN_ID>JG_L_2018_07_000000396413</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/19/45/CETATEXT000037194512.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 11/07/2018, 396413, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396413</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:396413.20180711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 26 janvier, 20 juin et 6 octobre 2016 et le 9 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe n° 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts le 24 juillet 2017 sous la référence BOIRPPM-PVBMI-20-20-10, en tant qu'il écarte l'application aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition en application de l'article 150-0 B ter du code général des impôts de l'abattement pour durée de détention prévu par l'article 150-0 D du même code et en tant qu'il ne prévoit pas l'application à ces mêmes plus-values d'un coefficient d'érosion monétaire ; <br/>
<br/>
              2°) de transmettre à la Cour de justice de l'Union européenne une question préjudicielle relative à la compatibilité des modalités d'imposition des plus-values placées en report d'imposition avec les dispositions de l'article 8 de la directive n° 2009/133/CE du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents, ainsi qu'au transfert du siège statutaire d'une SE ou d'une SCE d'un État membre à un autre ;<br/>
<br/>
              3°) à titre subsidiaire, de surseoir à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question préjudicielle dont le Conseil d'Etat l'a saisie par une décision n° 393881 du 31 mai 2016, portant sur l'interprétation de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;  <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2013-1278 du 29 décembre 2013 de finances pour 2014, notamment son article 17 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958, notamment son article 23-5 ;<br/>
              - la décision n° 2016-538 QPC du 22 avril 2016 du Conseil constitutionnel ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018, Jacob et Lassus (C-327/16 et C-421/16) ;<br/>
              - la décision du 19 juillet 2016 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B...;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article 150-0 B ter du code général des impôts dans sa version issue de l'article 18 de la loi du 29 décembre 2012 de finances rectificative pour 2012 applicable aux plus-value réalisées à compter du 14 novembre 2012 : " L'imposition de la plus-value réalisée, directement ou par personne interposée, dans le cadre d'un apport de valeurs mobilières, de droits sociaux, de titres ou de droits s'y rapportant tels que définis à l'article 150-0 A à une société soumise à l'impôt sur les sociétés ou à un impôt équivalent est reportée si les conditions prévues au III du présent article sont remplies. (imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux) ". <br/>
<br/>
              2. Il résulte de ces dispositions qu'elles ont pour seul effet de permettre, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de constater et de liquider la plus-value d'échange l'année de sa réalisation et de l'imposer l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange. Le montant de la plus-value est ainsi calculé en appliquant les règles d'assiette en vigueur l'année de sa réalisation, mais son imposition obéit aux règles de calcul de l'impôt en vigueur l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition.<br/>
<br/>
              3. En vertu du 2 de l'article 200 A du code général des impôts, dans sa rédaction applicable aux revenus perçus à compter du 1er janvier 2013 et résultant de la loi du 29 décembre 2012 de finances pour 2013, les gains nets obtenus dans les conditions prévues à l'article 150-0 A sont  pris en compte pour la détermination du revenu net global soumis au barème progressif de l'impôt sur le revenu. L'article 150-0 D du code général des impôts dispose, dans sa version issue de  la loi du 29 décembre 2013 de finances pour 2014, au deuxième alinéa de son 1, que :  " Les gains nets de cession à titre onéreux d'actions, de parts de sociétés, de droits portant sur ces actions ou parts, ou de titres représentatifs de ces mêmes actions, parts ou droits, mentionnés au I de l'article 150-0 A, ainsi que les distributions mentionnées aux 7,7 bis et aux deux derniers alinéas du 8 du II du même article, à l'article 150-0 F et au 1 du II de l'article 163 quinquies C sont réduits d'un abattement déterminé dans les conditions prévues, selon le cas, au 1 ter ou au 1 quater du présent article. (imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux) ". Ce même article définit, à son 1 ter, l'abattement pour durée de détention de droit commun et, à son 1 quater, l'abattement pour durée de détention renforcé applicable à certaines situations. Le III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 prévoit que ces dispositions s'appliquent aux gains réalisés à compter du 1er janvier 2013.<br/>
<br/>
              4. Il résulte de ce qui a été rappelé au point 2 ci-dessus que les dispositions relatives aux abattements pour durée de détention qui, ainsi que l'a d'ailleurs également jugé le Conseil constitutionnel dans la décision n° 2016-538 QPC du 22 avril 2016, constituent une règle de détermination de l'assiette des plus-values mobilières, ne peuvent dès lors s'appliquer aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition, la circonstance que la cession mettant fin à ce report intervient après le 1er janvier 2013 étant sans incidence à cet égard. <br/>
<br/>
              5. Ainsi, en prévoyant que " l'abattement pour durée de détention ne s'applique pas, notamment : (imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux) aux gains nets de cession, d'échange ou d'apport réalisés avant le 1er janvier 2013 et placés en report d'imposition dans les conditions prévues (imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux) à l'article 150-0 B ter du CGI ", les commentaires administratifs attaqués ne méconnaissent pas les dispositions qu'ils ont pour objet d'interpréter. N'est en outre pas de nature à entraîner leur annulation la circonstance qu'ils n'indiquent explicitement ni que, conformément à la réserve d'interprétation formulée par le Conseil constitutionnel au point 15 de sa décision n° 2016-538 QPC, les plus-values placées en report automatique d'imposition en application de l'article 150-0 B ter du code général des impôts demeurent.imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux <br/>
<br/>
              6. En second lieu, aux termes de l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents, ainsi qu'au transfert du siège statutaire d'une SE ou d'une SCE d'un État membre à un autre : " 1. L'attribution, à l'occasion d'une fusion, d'une scission ou d'un échange d'actions, de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé. / (imposées selon les règles d'assiette et de taux applicables à la date du fait générateur de l'imposition de la plus-value, ni que, conformément à la réserve d'interprétation formulée au point 11 de la même décision, il convient d'appliquer à l'assiette des plus-values placées en report d'imposition avant le 1er janvier 2013 qui ne font l'objet d'aucun abattement sur leur montant brut et dont le montant de l'imposition est arrêté selon des règles de taux telles que celles en vigueur à compter du 1er janvier 2013, un coefficient d'érosion monétaire pour la période comprise entre l'acquisition des titres et le fait générateur de l'imposition, dès lors que ces deux réserves d'interprétation sont sans incidence sur l'absence d'application des abattements pour durée de détention aux plus-values visées par les commentaires litigieux) 6. L'application des paragraphes 1, 2 et 3 n'empêche pas les États membres d'imposer le profit résultant de la cession ultérieure des titres reçus de la même manière que le profit qui résulte de la cession des titres existant avant l'acquisition ".<br/>
<br/>
              7. M. B...soutient qu'il résulte de ces dispositions que les plus-values d'échange résultant des opérations qui entrent dans le champ de la directive " fusions " doivent être soumises au même régime d'imposition que les plus-values réalisées lors de la cession des titres reçus lors de l'échange et que, en énonçant que l'article 150-0 B ter précité soumet une plus-value d'apport de titres opéré en 2012 à un régime de report d'imposition sans bénéfice de l'abattement pour durée de détention à l'expiration du report, les commentaires attaqués réitèreraient des dispositions qui seraient incompatibles, dans le champ de la directive " fusions " du 19 octobre 2009, avec les objectifs de son article 8 précité, ce dont il résulterait une différence de traitement constitutive d'une discrimination " à rebours ", au détriment des opérations placées hors du champ d'application territorial ou matériel de cette directive, et par suite d'une méconnaissance du principe constitutionnel d'égalité devant la loi.<br/>
<br/>
              8. Toutefois, le moyen tiré de ce que les dispositions législatives commentées par les énonciations attaquées seraient à l'origine d'une discrimination qui méconnaîtrait le principe d'égalité devant la loi ne peut être utilement soulevé qu'à l'appui d'une question prioritaire de constitutionnalité présentée dans les formes prescrites par l'article 23-5 de l'ordonnance du 7 novembre 1958 et l'article R. 771-13 du code de justice administrative. Faute d'être soulevé à l'appui d'une telle question présentée par mémoire distinct, ce moyen ne peut qu'être écarté, sans qu'il soit besoin de transmettre une question préjudicielle à la Cour de justice de l'Union européenne.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation des mots : " ainsi qu'à l'article 150-0 B ter " figurant au paragraphe 130 des commentaires administratifs publiés au BOFiP-Impôts le 24 juillet 2017 sous la référence BOIRPPM-PVBMI-20-20-10.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                       D E C I D E :<br/>
                                     --------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.  <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
