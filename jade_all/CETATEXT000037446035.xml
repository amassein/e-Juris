<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037446035</ID>
<ANCIEN_ID>JG_L_2018_09_000000411164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/44/60/CETATEXT000037446035.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 28/09/2018, 411164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411164.20180928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir la décision du ministre de l'enseignement supérieur et de la recherche et du ministre de la santé et des sports  du 11 juin 2010 lui refusant l'intégration dans le corps des maîtres de conférences des universités-praticiens hospitaliers des disciplines pharmaceutiques. Par un jugement n° 1001897 du 5 avril 2012, le tribunal administratif  a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 12DA00823 du 13 mai 2013, la cour administrative d'appel de Douai a, sur appel de M.A..., annulé ce jugement et la décision des ministres et enjoint à l'administration de procéder au réexamen de la demande d'intégration. <br/>
<br/>
              M. A...a demandé à la cour administrative d'appel d'assurer l'exécution de cet arrêt. Par un arrêt n° 16DA02300 du 30 mars 2017, la cour administrative d'appel a enjoint à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ainsi qu'à la ministre des affaires sociales et de la santé de réexaminer, dans un délai de trois mois, la demande d'intégration de M. A...et de se prononcer sur celle-ci à la date de la décision annulée en reconstituant, le cas échéant, sa carrière.<br/>
<br/>
<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 6 juin 2017 et 24 août  2018 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'enseignement supérieur, de la recherche et de l'innovation demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande d'exécution de M.A....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 2008-308 du 2 avril 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; qu'aux termes de l'article L. 911-4 du même code : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / (...) Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte (...) " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 13 mai 2013, la cour administrative d'appel de Douai a annulé la décision du ministre de l'enseignement supérieur et de la recherche et du ministre de la santé du 11 juin 2010 refusant à M. A...son intégration dans le corps des maîtres de conférences des universités-praticiens hospitaliers des disciplines pharmaceutiques et a enjoint à l'administration, en application des dispositions citées ci-dessus de l'article L. 911-1 du code de justice administrative, de procéder au réexamen de sa demande ; que, par un arrêt du 30 mars 2017 contre lequel la ministre de l'enseignement supérieur, de la recherche et de l'innovation se pourvoit en cassation, la même cour administrative d'appel, saisie sur le fondement de l'article L. 911-4 du code de justice administrative, également cité ci-dessus, a enjoint à l'administration de procéder à un nouvel examen de la demande de M. A...dans un délai de trois mois, en se plaçant à la date du refus annulé par sa précédente décision ; <br/>
<br/>
              3.	Considérant que si l'annulation d'une décision par laquelle l'autorité administrative a refusé de faire droit à une demande oblige l'administration à statuer à nouveau sur la demande dont elle demeure saisie dans le respect de l'autorité de la chose jugée, l'étendue des obligations pesant sur elle est fonction de la nature du motif de l'annulation prononcée et dépend en outre, lorsque sa décision n'est pas destinée à combler pour le passé un vide juridique, d'un éventuel changement dans les circonstances de droit et de fait qui serait survenu entre la date d'intervention de la décision initiale qui a été annulée et la date à laquelle l'administration est appelée à prendre une nouvelle décision ; que, par suite, en jugeant que la demande d'intégration de M. A...dans  le corps des maîtres de conférences des universités-praticiens hospitaliers des disciplines pharmaceutiques devait être examinée compte tenu des circonstances de droit et de fait à la date du 11 juin 2010 et donner lieu à une reconstitution de sa carrière à partir de cette date, la cour administrative d'appel a entaché son arrêt d'erreur de droit ; que la ministre de l'enseignement supérieur, de la recherche et de l'innovation est, par suite, fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5.	Considérant qu'il résulte de l'instruction que M. A...a été admis à la retraite, pour limite d'âge, à compter du 26 juillet 2014 ; qu'il en résulte que la décision annulant le refus de son intégration dans le corps des maîtres de conférences des universités-praticiens hospitaliers des disciplines pharmaceutiques ne peut plus impliquer son intégration dans ce corps ; que l'injonction faite à l'administration, par l'arrêt du 13 mai 2013 cité au point 2, de procéder au réexamen de la demande d'intégration de M.A..., n'est, dès lors, plus susceptible d'exécution ; que les conclusions de M. A...ont, par suite, perdu leur objet ;<br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, tant au titre de l'instance d'appel que de l'instance de cassation, une somme de 3 000 euros à verser à M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 30 mars 2017 est annulé.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions à fin d'exécution présentée par M. A... devant la cour administrative d'appel de Douai ;<br/>
<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à M. B...A.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
