<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029124435</ID>
<ANCIEN_ID>JG_L_2014_06_000000355675</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/12/44/CETATEXT000029124435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 23/06/2014, 355675, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355675</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355675.20140623</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 9 janvier, 10 avril et 4 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05725 du 8 décembre 2011 par lequel la cour administrative d'appel de Paris a annulé le jugement n° 0603374/2 du tribunal administratif de Paris du 16 juillet 2009 et a remis à sa charge les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle avait été assujettie au titre des années 1999 et 2001 et dont la décharge avait été prononcée par le tribunal administratif ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de Mme A...;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 27 mai et 3 juin 2014, présentées pour MmeA... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu du I de l'article 163 bis B du code général des impôts, dans sa rédaction applicable à l'impôt sur le revenu dû au titre des années 1999 et 2001, les sommes versées par l'entreprise en application d'un plan d'épargne constitué conformément aux dispositions du chapitre III du titre IV du livre IV du code du travail sont exonérées de l'impôt sur le revenu établi au nom du salarié ; qu'en vertu du II du même article, les revenus des titres détenus dans un plan d'épargne mentionné au I sont également exonérés d'impôt sur le revenu s'ils sont réemployés dans ce plan et frappés de la même indisponibilité que les titres auxquels ils se rattachent et sont définitivement exonérés à l'expiration de la période d'indisponibilité correspondante ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A..., épouse Schall, président-directeur général de la société Weecilms S.A, a bénéficié, pour les dividendes perçus au titre des années 1999 et 2001 en rémunération des titres qu'elle détenait dans le plan d'épargne de la société, de l'exonération d'impôt sur le revenu prévue par l'article 163 bis B du code général des impôts ; qu'à l'issue d'une vérification de la comptabilité de la société et du contrôle sur pièces du dossier personnel de MmeA..., l'administration fiscale a, par une notification de redressement du 5 juillet 2002, remis en cause cette exonération au motif que le plan d'épargne de la société dissimulait un montage destiné à permettre aux dividendes versés par la société d'échapper à toute imposition ; que les dividendes perçus par Mme A...ont, en conséquence, été taxés en tant que revenus distribués dans la catégorie des revenus de capitaux mobiliers ; que, par un jugement du 16 juillet 2009, le tribunal administratif de Paris a toutefois prononcé la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises à la charge de Mme A...au titre des années 1999 et 2001 au motif que cette dernière établissait que la constitution du plan d'épargne d'entreprise n'avait pas eu un but exclusivement fiscal ; que, sur recours du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, la cour administrative d'appel de Paris a, par l'arrêt attaqué du 8 décembre 2001, annulé ce jugement et remis à la charge de Mme A...les cotisations supplémentaires d'impôt sur le revenu et de contribution sociale dont le tribunal administratif avait prononcé la décharge ; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              3. Considérant, d'une part, qu'il ressort des pièces du dossier soumis au Conseil d'Etat que le moyen tiré de ce que la minute de l'arrêt attaqué ne comporterait pas les signatures requises par l'article R. 741-7 du code de justice administrative manque en fait ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des pièces du dossier soumis à la cour que celle-ci n'a omis de répondre à aucun des moyens en défense soulevés devant elle par Mme A... ; qu'en jugeant que cette dernière, en se bornant à produire une attestation de la société gestionnaire du plan d'épargne selon laquelle les retraits qu'elle avait opérés sur le plan au cours des années 2002 à 2004 avaient été soumis aux cotisations sociales par le biais du prélèvement libératoire, n'établissait pas que les dividendes en litige, correspondant aux années 1999 et 2001, auraient été doublement soumis à ces cotisations, la cour a, contrairement à ce que soutient le pourvoi, répondu au moyen soulevé par Mme A...dans ses mémoires enregistrés les 25 février et  28 mars 2011 ; <br/>
<br/>
              5. Considérant, enfin, qu'il résulte de l'article 2 de l'arrêt attaqué que celui-ci n'a pas, contrairement à ce que soutient MmeA..., remis à sa charge la majoration pour abus de droit de 80 % dont le tribunal avait prononcé la décharge, mais uniquement les cotisations supplémentaires d'impôt sur le revenu et de contribution sociale auxquelles elle a été assujettie au titre des années 1999 et 2001 ; que le moyen tiré de ce que la cour aurait statué au-delà des conclusions dont elle était saisie doit, par suite, être écarté ;<br/>
<br/>
              Sur les moyens relatifs au bien-fondé des impositions en litige :<br/>
<br/>
              6. Considérant qu'en vertu de l'article L. 443-1 du code du travail, alors applicable, le plan d'épargne d'entreprise constituait un système d'épargne collectif ouvrant aux salariés de l'entreprise la faculté de participer, avec l'aide de celle-ci, à la constitution d'un portefeuille de valeurs mobilières ; que l'article L. 443-2 de ce code, alors applicable, prévoyait que les versements annuels d'un salarié sur un plan d'épargne d'entreprise ne pouvaient excéder un quart de sa rémunération annuelle ; qu'enfin, l'article L. 443-3 du même code, alors applicable, prévoyait que les sommes recueillies par un plan d'épargne d'entreprise étaient affectées à l'acquisition de valeurs mobilières ; qu'il résulte de ces dispositions que les plans d'épargne d'entreprise avaient pour principal objet l'acquisition, par les salariés de l'entreprise, de valeurs mobilières ; que, dès lors, un plan d'épargne d'entreprise constitué par le transfert de titres acquis antérieurement à sa constitution  n'était pas conforme à cet objet ; <br/>
<br/>
              7. Considérant qu'il résulte de la combinaison des dispositions du code du travail mentionnées au point 6 et des dispositions de l'article 163 bis B du code général des impôts mentionnées au point 1 que les revenus de titres détenus dans un plan d'épargne d'entreprise dont la constitution et le fonctionnement n'étaient pas conformes aux dispositions alors applicables des articles L. 443-1 à L. 443-3 du code du travail ne pouvaient bénéficier des mesures de faveur prévues par ces dispositions ; <br/>
<br/>
              8. Considérant qu'il ressort des écritures présentées par le ministre devant la cour que celui-ci a justifié les impositions supplémentaires mises à la charge de MmeA..., au titre des années 1999 et 2001, à raison des dividendes perçus en rémunération de titres détenus dans le plan d'épargne d'entreprise de la société Weecilms, par la circonstance que ces titres n'avaient pas été acquis par cette dernière dans le cadre du plan mais étaient détenus antérieurement à la constitution du plan et avaient fait l'objet d'un simple transfert dans le plan ; que, pour faire droit à son recours, la cour, après avoir notamment relevé que le dépôt sur le plan des titres antérieurement détenus par Mme A...n'avait pas permis l'acquisition de valeurs mobilières nouvelles et n'avait pas servi à la constitution d'un portefeuille de valeurs mobilières au sens de l'article L. 443-1 du code du travail, en a déduit que les premiers juges avaient estimé à tort que le fonctionnement du plan d'épargne d'entreprise avait été conforme aux dispositions du code du travail ; <br/>
<br/>
              9. Considérant, en premier lieu, qu'en statuant comme elle l'a fait, la cour n'a pas relevé d'office un moyen mais a accueilli  un moyen du ministre tiré de la non-conformité, au regard des dispositions applicables du code du travail, de la constitution et du fonctionnement du plan d'épargne d'entreprise de la société Weecilms ; qu'elle pouvait légalement, eu égard à l'argumentation précise développée par le ministre devant elle, substituer ce motif à celui initialement opposé par l'administration dans la notification de redressement du 5 juillet 2002, tiré de ce que la constitution du plan d'épargne d'entreprise de la société Weecilms révélait un montage constitutif d'un abus de droit ; que c'est sans erreur de droit qu'elle a estimé que cette substitution de base légale ne privait Mme A...d'aucune garantie dès lors que celle-ci avait bénéficié des garanties attachées à la procédure de redressement contradictoire prévue par l'article L. 55 du livre des procédures fiscales ;<br/>
<br/>
              10. Considérant, en deuxième lieu, qu'en estimant que le plan d'épargne d'entreprise de la société Weecilms était irrégulier au regard des dispositions des articles L. 443-1 à L. 443-3 du code du travail alors applicables, dès lors que le dépôt sur le plan des titres antérieurement détenus par Mme A...n'avait pas permis l'acquisition de valeurs mobilières nouvelles et n'avait pas servi à la constitution d'un portefeuille de valeurs mobilières au sens des dispositions de l'article L. 443-1 du code du travail, la cour n'a ni dénaturé les faits ni entaché son arrêt d'erreur de droit ; que si elle s'est, à tort, fondée sur les dispositions des articles L. 443-1 à L. 443-3 du code du travail dans leur rédaction issue de la loi n° 2001-152 du 19 février 2001, alors que ces dispositions n'étaient pas applicables lors de la constitution, en 1997, du plan d'épargne d'entreprise de la société Weecilms, les motifs de son arrêt peuvent être légalement fondés sur les dispositions des articles L. 443-1 et L. 443-3 antérieures à la modification de ces articles par la loi n° 2001-152 du 19 février 2001, qui fixaient des prescriptions identiques à celles qu'a citées la cour ; qu'il y a lieu, en conséquence, de substituer ce fondement légal au fondement erroné retenu par l'arrêt attaqué ; <br/>
<br/>
              11. Considérant, en troisième lieu, que les motifs de l'arrêt attaqué par lesquels la cour a précisé que les plans d'épargne d'entreprise doivent nécessairement être alimentés par des versements provenant des revenus du travail présentent un caractère surabondant ; que le moyen tiré de ce que ces motifs seraient entachés d'erreur de droit doit, dès lors, en tout état de cause, être écarté comme inopérant ;<br/>
<br/>
              12. Considérant, en quatrième lieu, que, contrairement à ce que soutient le pourvoi, la cour n'a pas jugé que les dividendes versés dans le cadre du plan d'épargne d'entreprise ne pouvaient pas être regardés comme de l'épargne nouvelle mais seulement que la circonstance que " les dividendes produits par les titres déposés sur le plan par Mme A...épouse Schall auraient eux-mêmes servi à l'acquisition de nouveaux titres ne permet pas de considérer que le plan a fonctionné dans des conditions conformes aux dispositions précitées du code du travail " ; que son arrêt n'est entaché sur ce point d'aucune dénaturation des faits ;<br/>
<br/>
              13. Considérant, en cinquième lieu, que c'est par une appréciation souveraine exempte d'erreur de droit et de dénaturation que la cour a jugé que Mme A...n'apportait pas la preuve de ce que les dividendes en litige, correspondants aux années 1999 et 2001, auraient été doublement soumis aux cotisations sociales ;<br/>
<br/>
              14. Considérant, enfin, qu'ainsi qu'il a été indiqué au point 5, la cour n'a pas remis à la charge de Mme A...la pénalité de 80 % dont le tribunal administratif avait prononcé la décharge ; que le moyen tiré de ce que la cour aurait commis une erreur de droit en remettant cette pénalité à sa charge ne peut, par suite, qu'être écarté ; que si Mme A...soutient, par ailleurs, que la cour a commis une erreur de droit en remettant à sa charge les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre des années 1999 et 2001 alors, en premier lieu, que les sommes imposées au titre de l'année 2001 incluent à tort des dividendes versés au titre de l'année 2000 et, en deuxième lieu, que les dividendes litigieux n'ayant pas été perçus en 1999 et 2001, aucune imposition ne pouvait être mise à sa charge au titre de ces deux années, enfin, que l'administration n'a produit aucun document permettant de vérifier le bien-fondé du calcul des impositions remises à sa charge après l'abandon du fondement de l'abus de droit, ces moyens, qui sont nouveaux en cassation et ne sont pas d'ordre public, doivent être écartés comme inopérants ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
