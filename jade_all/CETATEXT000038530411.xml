<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038530411</ID>
<ANCIEN_ID>JG_L_2019_05_000000428558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/53/04/CETATEXT000038530411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 29/05/2019, 428558, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:428558.20190529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) du Breuil a demandé au tribunal administratif de Rennes de prononcer la réduction, à hauteur des sommes respectives de 1 484 et 1 553 euros, des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016 dans les rôles de la commune de Saint-Martin des champs (Finistère) et la condamnation de l'Etat à lui verser la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              La société du Breuil, à l'appui de sa demande tendant à obtenir la réduction des cotisations supplémentaires de taxe foncière sur les propriétés bâties, a produit un mémoire, enregistré le 22 novembre 2018 au greffe de ce même tribunal, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1803955 du 1er mars 2019, enregistrée le 7 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la 2ème chambre de ce tribunal, avant qu'il ne soit statué sur la demande de la société du Breuil, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 1500 du code général des impôts dans sa rédaction résultant de l'article 101 de la loi n° 2008-1443 du 30 décembre 2008.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise, la société du Breuil soutient que l'article 1500 du code général des impôts, dans sa rédaction résultant de l'article 101 de la loi n° 2008-1443 du 30 décembre 2008 applicable au litige, méconnaît le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'Homme et du citoyen de 1789.  <br/>
<br/>
              Par un mémoire, enregistré le 21 mars 2018, le ministre de l'action et des comptes publics soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies et, en particulier, que la question, qui n'est pas nouvelle, est dépourvue de caractère sérieux. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts, notamment l'article 1500 ;<br/>
              - la loi n° 2008-1443 du 30 décembre 2008, notamment son article 101 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 1500 du code général des impôts, dans sa rédaction résultant de l'article 101 de la loi de finances rectificatives pour 2008 du 20 décembre 2008 : " Les bâtiments et terrains industriels sont évalués : / - 1° selon les règles fixées à l'article 1499 lorsqu'ils figurent à l'actif du bilan de leur propriétaire ou de leur exploitant, et que celui-ci est soumis aux obligations définies à l'article 53 A ; / - 2° selon les règles fixées à l'article 1498 lorsque les conditions prévues au 1° ne sont pas satisfaites. ". <br/>
<br/>
              3. L'article 13 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 dispose que : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". Cette exigence ne serait pas respectée si l'impôt revêtait un caractère confiscatoire ou faisait peser sur une catégorie de contribuables une charge excessive au regard de leurs facultés contributives. En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              4. La société requérante soutient que les dispositions de l'article 1500 du code général des impôt, en ce qu'elles ont pour effet que la valeur locative d'un immeuble présentant un caractère industriel appartenant à une société civile immobilière ayant opté pour la taxation de ses bénéfices à l'impôt sur les sociétés, et soumise de ce fait aux obligations définies à l'article 53 A, est déterminée selon les règles de l'article 1499 du code général des impôts, tandis que la valeur locative du même immeuble est déterminée selon les règles de l'article 1498 du même code lorsque la société civile propriétaire n'a pas exercé cette option, institue entre des contribuables placés dans des situations similaires une différence de traitement constitutive d'une rupture d'égalité devant les charges publiques.<br/>
<br/>
              5. Aucune disposition ne fait obligation à une société civile immobilière relevant du 1° de l'article 8 du code général des impôts dont les parts ne sont pas inscrites à l'actif d'une personne morale passible de l'impôt sur les sociétés dans les conditions de droit commun ou d'une entreprise industrielle, commerciale, artisanale ou agricole imposable à l'impôt sur le revenu de plein droit selon un régime de bénéfice réel, de tenir elle-même une comptabilité d'engagement impliquant, en particulier, l'établissement d'un bilan annuel lorsque son activité consiste en la location de biens immobiliers dans des conditions non constitutives d'une exploitation commerciale. A défaut d'exercice par cette société de l'option pour son assujettissement à l'impôt sur les sociétés, prévue par le b du 3 de l'article 206 du code général des impôts, la valeur locative des bâtiments et terrains industriels dont elle est propriétaire est déterminée, en application des dispositions du 2° de l'article 1500 du code général des impôts, selon les règles fixées à l'article 1498 du même code.<br/>
<br/>
              6. Une telle société civile immobilière, lorsqu'elle exerce l'option pour son assujettissement à l'impôt sur les sociétés, se voit astreinte, de ce fait, aux obligations déclaratives de l'article 53 A du même code et soumise à l'obligation de tenir une comptabilité d'engagement impliquant d'inscrire à l'actif de son bilan les biens durablement productifs de revenus. Cette société remplit alors les conditions prévues par le 1° de l'article 1500 du même code et la valeur locative des immeubles industriels dont elle est propriétaire doit être déterminée selon les règles fixées à l'article 1499 de ce code. <br/>
<br/>
              7. La différence de traitement ainsi relevée par la société requérante procède toutefois non de la loi elle-même, mais de l'exercice par le contribuable de l'option pour son assujettissement à l'impôt sur les sociétés. Il en résulte que l'article 1 500 du code général des impôts ne saurait être regardé, en ce qu'il conduit, pour les sociétés ayant exercé cette option, à déterminer la valeur locative des immeubles assujettis à la taxe foncière sur les propriétés bâties selon les règles fixées à l'article 1499 de ce code, comme portant atteinte au principe d'égalité devant les charges publiques.<br/>
<br/>
              8. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société du Breuil.<br/>
Article 2 : La présente décision sera notifiée à la société civile immobilière du Breuil et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
