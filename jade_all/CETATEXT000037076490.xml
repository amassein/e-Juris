<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037076490</ID>
<ANCIEN_ID>JG_L_2018_06_000000413799</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/07/64/CETATEXT000037076490.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème chambre jugeant seule, 15/06/2018, 413799, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413799</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413799.20180615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 60 000 euros avec intérêts au taux légal en réparation du préjudice qu'il estime avoir subi du fait de l'absence de proposition de relogement. Par un jugement n° 1611196/3-3 du 16 mai 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 29 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat au profit de la SCP Delvolvé et Trichet, son avocat, qui renonce à percevoir la part contributive de l'Etat au titre de l'aide juridictionnelle, la somme de 3 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de la sécurité sociale ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été reconnu prioritaire et devant être relogé en urgence, sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation, par une décision du 7 juin 2013 de la commission de médiation du département de Paris au motif qu'il justifiait d'un hébergement continu en structure sociale depuis plus de six mois ; que, par un jugement du 16 septembre 2014, le tribunal administratif de Paris, saisi par M. A...sur le fondement du I de l'article L. 441-2-3-1 du même code, a enjoint au préfet de la région d'Ile-de-France, préfet de Paris, d'assurer son relogement ; que, le 18 juillet 2016, M. A... a demandé au tribunal de condamner l'Etat à l'indemniser du préjudice subi du fait de son absence de relogement ; qu'il se pourvoit en cassation contre le jugement du 16 mai 2017 par lequel le tribunal administratif a rejeté cette demande ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande d'indemnisation présentée par M. A..., le tribunal administratif a retenu que, résidant depuis le 31 mars 2013 dans un logement situé dans une résidence sociale et d'une surface de 14 m² ne caractérisant pas une sur-occupation, il ne justifiait pas d'un préjudice lui ouvrant droit à indemnisation ; que, cependant, il résulte de ce qui a été dit ci-dessus que, constatant que l'intéressé ne disposait que d'un hébergement dans une résidence sociale, par nature temporaire, le tribunal devait en déduire que la situation qui avait motivé la décision de la commission perdurait et que M. A... justifiait de ce seul fait de troubles dans ses conditions d'existence lui ouvrant droit à réparation dans les conditions indiquées au point 2 ; que le requérant est, par suite, fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Delvolvé et Trichet, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à cette société ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 16 mai 2017 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Delvolvé et Trichet une somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
