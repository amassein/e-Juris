<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039127761</ID>
<ANCIEN_ID>JG_L_2019_09_000000434258</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/12/77/CETATEXT000039127761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/09/2019, 434258, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434258</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:434258.20190918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 5 et 16 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension des dispositions de la section 1 du chapitre II de du " Guide pratique pour les élections des Tribunaux de commerce pour l'année 2019 ", publié au Bulletin officiel du ministère de la justice du 8 juillet 2019 ;<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors que les dispositions contestées ont un caractère impératif eu égard à leur formulation et à l'interprétation qui est faite de la loi ;<br/>
              - la condition d'urgence est remplie dès lors que la date limite de dépôt des candidatures pour les élections des tribunaux de commerce est fixée au 20 septembre 2019 à 18 heures ; <br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées, pour deux motifs : à titre principal, les dispositions contestées sont entachées d'une erreur de droit dès lors qu'elles retiennent une interprétation contraire au sens et à la portée de l'article L. 723-7 du code de commerce ; à titre subsidiaire, elles ont méconnu le champ d'application de la loi dans le temps dès lors que, d'une part, elles confèrent à la loi du 18 novembre 2016 une portée rétroactive et, d'autre part, elles sont contraires au principe général de non-rétroactivité des actes administratifs. <br/>
<br/>
              Par un mémoire en défense, enregistré le 13 septembre 2019, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient que les passages contestés du guide ne font pas grief et que, par suite, la requête est irrecevable, que la condition d'urgence n'est pas remplie dès lors que le requérant a tardé à présenter sa demande et qu'aucun des moyens soulevés n'est propre à créer un doute sérieux quant à la légalité de la décision contestée, celle-ci étant conforme à l'intention du législateur et ne comportant aucune rétroactivité illégale.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du commerce ;<br/>
              - le code de justice administrative ;	<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A... et, d'autre part, la garde des sceaux, ministre de la justice ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 septembre 2019 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
- les représentants de la ministre de la justice ;<br/>
<br/>
     et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. M. A... a effectué quatre mandats de juge consulaire au tribunal de commerce de Nanterre de 2000 à 2014. Ayant cessé d'exercer ses fonctions juridictionnelles en 2015, en application des dispositions alors en vigueur de l'article L. 723-7 du code de commerce, il a été réélu juge consulaire dans le même tribunal en 2016 pour un nouveau mandat expirant en 2019. Il conteste la légalité du " Guide pratique pour les élections des tribunaux de commerce pour l'année 2019 ", publié au Bulletin officiel du ministère de la justice du 8 juillet 2019, en tant que ce document interprète l'article L. 723-7 du code de commerce comme faisant obstacle à ce qu'il puisse se présenter aux élections qui seront organisées en octobre 2019, et demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension des dispositions de la section 1 du chapitre II de ce guide.<br/>
<br/>
              3. Les moyens invoqués par M. A... à l'appui de sa demande de suspension et tirés, en premier lieu, de ce que les dispositions contestées méconnaissent la lettre de l'article L. 723-7 du code de commerce en lui donnant une portée qu'il n'a pas, en second lieu, de ce que ces dispositions méconnaissent le champ d'application de la loi dans le temps et sont entachées d'une rétroactivité illégale, ne paraissent pas, en l'état de l'instruction, propres à créer un doute sérieux sur la légalité des dispositions contestées. Dès lors, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par M. A... tendant à ce que soit suspendue l'exécution des dispositions contestées de la section 1 du chapitre II du " Guide pratique pour les élections des tribunaux de commerce pour l'année 2019 " publié au Bulletin officiel du ministère de la justice du 8 juillet 2019 doit être rejetée. <br/>
<br/>
              4. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par M. A... soit mise à la charge de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A... et à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
