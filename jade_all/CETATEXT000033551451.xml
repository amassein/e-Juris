<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551451</ID>
<ANCIEN_ID>JG_L_2016_12_000000389036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551451.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 07/12/2016, 389036</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389036.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Par une requête et un mémoire en réplique, enregistrés sous le n° 389036 les 27 mars 2015 et 22 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, l'Union des chirurgiens de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-74 du 27 janvier 2015 relatif aux actes infirmiers relevant de la compétence exclusive des infirmiers de bloc opératoire ;<br/>
<br/>
              2°) de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle relative à la conformité à l'article 45 du traité sur le fonctionnement de l'Union européenne d'une interprétation de la directive 2005/36/CE du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles qui conduirait un Etat membre à soumettre à des exigences de titres et de diplômes différents et supplémentaires des professionnels chargés de réaliser les mêmes actes.<br/>
<br/>
<br/>
              2° Par une requête, enregistrée sous le n° 389589 le 17 avril 2015, l'Union des chirurgiens de France demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 27 janvier 2015 relatif aux actes et activités et à la formation complémentaire prévus par le décret n° 2015-74 du 27 janvier 2015 relatif aux actes infirmiers relevant de la compétence exclusive des infirmiers de bloc opératoire.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Par une requête, enregistrée sous le n° 390121 le 12 mai 2015, l'Union des chirurgiens de France demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 12 mars 2015 modifiant l'arrêté du 22 octobre 2001 relatif à la formation conduisant au diplôme d'Etat d'infirmier de bloc opératoire.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              En ce qui concerne le respect des articles L. 4311-2 et L. 4311-13 du code de la santé publique : <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4311-2 du code de la santé publique : " (...) peuvent exercer la profession d'infirmier ou d'infirmière les personnes titulaires d'un diplôme, certificat ou titre mentionné aux articles L. 4311-3 et L. 4311-4, ou titulaires des autorisations prévues aux articles L. 4311-9 et L. 4311-10 " ; qu'aux termes de l'article L. 4311-13 du même code : " Par dérogation aux dispositions de l'article L. 4311-2, peuvent accomplir des actes d'assistance auprès d'un praticien au cours d'une intervention chirurgicale les personnels aides-opératoires et aides-instrumentistes exerçant cette activité professionnelle à titre bénévole ou salarié depuis une durée au moins égale à six ans avant le 28 juillet 1999, et ayant satisfait, avant le 31 décembre 2005, à des épreuves de vérification des connaissances dans des conditions déterminées par décret en Conseil d'Etat. / L'épreuve de vérification des connaissances est destinée à autoriser exclusivement l'exercice des activités professionnelles d'aides-opératoires et d'aides-instrumentistes. / Tout employeur de personnel aide-opératoire ou aide-instrumentiste est tenu de proposer à ces personnels un plan de formation intégré dans le temps de travail, aboutissant à son maintien au sein de l'établissement, dans des conditions et des délais définis par décret " ;<br/>
<br/>
              3. Considérant que l'article R. 4311-11-1 du code de la santé publique, créé par le décret attaqué, réserve au seul infirmier de bloc opératoire, titulaire du diplôme d'Etat de bloc opératoire, les actes et activités suivants : " 1° Dans les conditions fixées par un protocole préétabli, écrit, daté et signé par le ou les chirurgiens : / a) Sous réserve que le chirurgien puisse intervenir à tout moment : / - l'installation chirurgicale du patient ; / - la mise en place et la fixation des drains susaponévrotiques ; / - la fermeture sous-cutanée et cutanée ; / b) Au cours d'une intervention chirurgicale, en présence du chirurgien, apporter une aide à l'exposition, à l'hémostase et à l'aspiration ; / 2° Au cours d'une intervention chirurgicale, en présence et sur demande expresse du chirurgien, une fonction d'assistance pour des actes d'une particulière technicité déterminés par arrêté du ministre chargé de la santé " ; <br/>
<br/>
              4. Considérant, en premier lieu, que les dispositions de l'article L. 4311-2 du code de la santé publique ne font pas obstacle à ce que le pouvoir réglementaire réserve certains actes, du fait de la spécialisation qu'ils requièrent, à des infirmiers et infirmières titulaires d'un diplôme de spécialité, obtenu par la validation d'enseignements théoriques et pratiques et de stages ou par celle des acquis de l'expérience ;  <br/>
<br/>
              5. Considérant, en second lieu, que les dispositions de l'article L. 4311-13 du code de la santé publique, éclairées par les travaux préparatoires de la loi du 27 juillet 1999 portant création d'une couverture maladie universelle dont elles sont issues, ont pour seul objet de permettre le maintien dans leur emploi des personnels aides-opératoires et aides-instrumentistes justifiant d'au moins six années d'exercice de cette activité et ayant satisfait à des épreuves de vérification des connaissances, en dérogeant aux dispositions qui définissent les actes dont l'exercice est réservé aux personnes remplissant les conditions fixées par l'article L. 4311-2 du même code pour exercer la profession d'infirmier ; qu'en permettant aux aides-opératoires et aux aides-instrumentistes d'accomplir des actes d'assistance auprès d'un chirurgien, le législateur n'a pas entendu leur attribuer la qualification d'infirmier ou d'infirmière ; que, s'il attribue une compétence exclusive aux infirmiers de bloc opératoire diplômés d'Etat pour accomplir certains actes et activités, le décret attaqué n'a ni pour objet ni pour effet de retirer aux aides-opératoires et aux aides-instrumentistes toute possibilité d'assister le chirurgien au cours des interventions chirurgicales ; qu'il n'a ainsi pas méconnu les dispositions de l'article L. 4311-13 du code de la santé publique ;<br/>
<br/>
              En ce qui concerne la compatibilité avec la directive 2005/36/CE :<br/>
<br/>
              6. Considérant qu'en vertu de la jurisprudence de la Cour de justice de l'Union européenne, il découle des stipulations du traité sur le fonctionnement de l'Union européenne qu'un Etat membre, saisi d'une demande d'autorisation d'exercer une profession dont l'accès est, selon la législation nationale, subordonné à la possession d'un diplôme ou d'une qualification professionnelle, doit prendre en considération les diplômes, certificats et autres titres que l'intéressé a acquis dans le but d'exercer cette même profession dans un autre Etat membre en procédant à une comparaison entre les compétences attestées par ces diplômes et les connaissances et qualifications exigées par les règles nationales ; qu'il n'en résulte pas que seul un système de reconnaissance automatique serait de nature à satisfaire aux exigences découlant du traité ;<br/>
<br/>
              7. Considérant que la directive 2005/36/CE du Parlement européen et du Conseil du 7 septembre 2005 relative à la reconnaissance des qualifications professionnelles dispose à son article 21, compte tenu de la coordination des conditions minimales de formation qu'elle organise, que chaque Etat membre reconnaît les titres de formation d'infirmier responsable de soins généraux visés en annexe qui sont conformes aux conditions minimales de formation qu'elle prévoit, en leur donnant, en ce qui concerne l'accès aux activités professionnelles et leur exercice, le même effet sur son territoire qu'aux titres de formation qu'il délivre ; qu'elle prévoit en revanche à son article 10 que le régime général de reconnaissance des titres de formation, selon lequel l'Etat membre d'accueil, dans lequel une profession est réglementée, est tenu de prendre en compte les qualifications acquises dans un autre Etat membre et d'apprécier si celles-ci correspondent à celles qu'il exige, s'applique aux infirmiers détenant un titre de formation spécialisée, aux fins de reconnaissance de cette spécialisation ; que ces dispositions ont été transposées par l'article L. 4311-4 du code de la santé publique, s'agissant de la profession d'infirmier anesthésiste, d'infirmier de bloc opératoire ou de puéricultrice ; <br/>
<br/>
              8. Considérant qu'en réservant certains actes et activités aux infirmiers de bloc opératoire, qui détiennent un titre de formation spécialisé, le décret attaqué n'a pas méconnu les objectifs de la directive 2005/36/CE, interprétée à la lumière des stipulations du traité, notamment de son article 45, relatif à la libre circulation des travailleurs à l'intérieur de l'Union ; que, par suite, le moyen tiré de cette méconnaissance doit être écarté, sans qu'il soit besoin de renvoyer à la Cour de justice de l'Union européenne une question relative à l'interprétation de la directive 2005/36/CE ;<br/>
<br/>
              En ce qui concerne le respect du principe d'égalité :<br/>
<br/>
              9. Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un comme l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ;<br/>
<br/>
              10. Considérant qu'en vertu des articles D. 4311-42 et D. 4311-43 du code de la santé publique, le diplôme d'Etat d'infirmier de bloc opératoire est délivré par le préfet de région aux personnes titulaires du diplôme d'Etat d'infirmier ou de sage-femme après un enseignement théorique ou pratique d'une durée totale de dix-huit mois ou après validation des acquis de l'expérience ; que le décret attaqué subordonne la réalisation des actes et activités qu'il réserve aux infirmiers de bloc opératoire diplômés d'Etat au suivi, par ces professionnels, d'une formation complémentaire ; que les infirmiers de bloc opératoire diplômés d'Etat sont ainsi dans une situation différente de celle des infirmiers diplômés d'Etat, des aides-opératoires et des aides-instrumentistes ; qu'en attribuant aux premiers une compétence exclusive pour l'accomplissement d'actes techniques lors des interventions chirurgicales, le pouvoir réglementaire, qui a entendu garantir la sécurité et la santé des patients, a introduit une différence de traitement qui est en rapport direct avec l'objet de cette réglementation et n'est pas manifestement disproportionnée au regard de la différence de situation et avec le motif d'intérêt général poursuivi ; que, dès lors, le moyen tiré de ce que le décret porterait atteinte au principe d'égalité doit être écarté ;<br/>
<br/>
              En ce qui concerne les conditions d'entrée en vigueur du décret attaqué :<br/>
<br/>
              11. Considérant qu'aux termes de l'article R. 4311-11 du code de la santé publique, issu de l'article 12 du décret du 11 février 2002 relatif aux actes professionnels et à l'exercice de la profession d'infirmier : " L'infirmier ou l'infirmière titulaire du diplôme d'Etat de bloc opératoire ou en cours de formation préparant à ce diplôme, exerce en priorité les activités suivantes : / 1° Gestion des risques liés à l'activité et à l'environnement opératoire ; / 2° Elaboration et mise en oeuvre d'une démarche de soins individualisée en bloc opératoire et secteurs associés ; / 3° Organisation et coordination des soins infirmiers en salle d'intervention ; / 4° Traçabilité des activités au bloc opératoire et en secteurs associés ; / 5° Participation à l'élaboration, à l'application et au contrôle des procédures de désinfection et de stérilisation des dispositifs médicaux réutilisables (...) / En per-opératoire, l'infirmier ou l'infirmière titulaire du diplôme d'Etat de bloc opératoire ou l'infirmier ou l'infirmière en cours de formation préparant à ce diplôme exerce les activités de circulant, d'instrumentiste et d'aide opératoire en présence de l'opérateur (...) " ; que cette priorité ainsi reconnue aux infirmiers de bloc opératoire ne faisait pas obstacle, avant l'intervention du décret attaqué, à ce que, en cas de besoin, les mêmes activités soient exercées par des infirmiers, ainsi que, dans la limite de la compétence qui leur a été reconnue par l'article L. 4311-13 du code de la santé publique, par des personnels aides-opératoires et aides-instrumentistes ; <br/>
<br/>
              12. Considérant que, sans prévoir de mesures transitoires, le décret attaqué dispose que, dès son entrée en vigueur, les actes et activités énumérés à l'article R. 4311-11-1 du code de la santé publique, lorsqu'ils ne sont pas accomplis par le chirurgien lui-même, ne peuvent être accomplis que par des infirmiers titulaires du diplôme d'Etat de bloc opératoire ; qu'en outre, en vertu du II de l'article 2 du décret attaqué, pour pouvoir réaliser ces actes et activités entre la date d'entrée en vigueur du décret attaqué et le 31 décembre 2020, les infirmiers titulaires du diplôme d'Etat de bloc opératoire en exercice à la date d'entrée en vigueur du décret et les infirmiers en cours de formation préparant au diplôme d'Etat de bloc opératoire à cette même date doivent avoir suivi, dans l'attente de son intégration à la formation préparant à ce diplôme, une formation complémentaire ; que le décret attaqué renvoie le contenu, les modalités de cette formation complémentaire et le modèle type d'attestation de formation à un arrêté du ministre chargé de la santé qui a été publié au Journal officiel de la République française le 20 février 2015 et fixe la durée de la formation à quarante-neuf heures ; qu'il en résulte que, dès l'entrée en vigueur du décret attaqué, seuls les chirurgiens et les infirmiers titulaires du diplôme d'Etat de bloc opératoire ayant suivi cette formation complémentaire peuvent réaliser les actes et activités mentionnés à l'article R. 4311-11-1 du code de la santé publique ;<br/>
<br/>
              13. Considérant que, en tant qu'il réserve aux infirmiers de bloc opératoire, à la condition qu'ils aient suivi une formation complémentaire, la possibilité d'accomplir des actes qui, jusque-là, relevaient de la compétence des seuls chirurgiens, le décret attaqué a entouré l'entrée en vigueur des nouvelles dispositions de précautions nécessaires pour garantir la sécurité et la santé des patients ; qu'en revanche, en tant qu'il réserve aux mêmes infirmiers de bloc opératoire, les actes d'aide à l'exposition, à l'hémostase et à l'aspiration au cours d'une intervention chirurgicale, qui pouvaient auparavant, au moins pour partie, être accomplis par les infirmiers voire par les aides-opératoires, tout en ne leur permettant d'accomplir ces actes qu'après avoir suivi une formation complémentaire et ce au plus tard le 31 décembre 2020, le décret qui fait peser la réalisation de ces actes, à la date de son entrée en vigueur, sur les seuls chirurgiens, puis sur un nombre dans les premiers temps limité d'infirmiers de bloc opératoire ayant reçu la formation requise, est entaché d'une erreur manifeste d'appréciation faute de prévoir des mesures transitoires, compte tenu des conséquences d'une telle entrée en vigueur immédiate sur le fonctionnement des services ; qu'il résulte de la mesure supplémentaire d'instruction diligentée par la 1ère chambre de la Section du contentieux du Conseil d'Etat que ce délai doit être fixé compte tenu de ce que les professionnels doivent suivre la formation complémentaire exigée par le décret pour accomplir ces actes ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que l'union requérante est fondée à demander l'annulation du décret qu'elle attaque en tant seulement que, en l'état du dispositif applicable, il ne diffère pas au 31 décembre 2017 l'entrée en vigueur des dispositions du b) du 1° de l'article R. 4311-11-1 du code de la santé publique ; <br/>
<br/>
              Sur la légalité de l'arrêté du 27 janvier 2015 :<br/>
<br/>
              15. Considérant que l'article 1er de l'arrêté du 27 janvier 2015, pris en application du décret du même jour, renvoie à une annexe la liste des actes d'une particulière technicité constituant la fonction d'assistance technique prévue à l'article R. 4311-11-1 du code de la santé publique et pour laquelle le décret attribue aux infirmiers de bloc opératoire diplômés d'Etat une compétence exclusive ; que l'annexe I de l'arrêté détaille, d'une part, les actes et activités mentionnés au 1° de cet article que les infirmiers de bloc opératoire peuvent réaliser, dans les conditions fixées par un protocole préétabli par le ou les chirurgiens ; qu'elle décrit, d'autre part, conformément au 2° du même article, les actes d'une particulière technicité réalisés par les infirmiers de bloc opératoire, au cours d'une intervention chirurgicale, en présence et sur demande expresse du chirurgien ; que les articles 2 à 5 de l'arrêté attaqué, ainsi que ses annexes II et III, définissent le contenu, la durée et les modalités de la formation complémentaire que les infirmiers de bloc opératoire diplômés d'Etat doivent accomplir et le modèle type d'attestation de formation ; <br/>
<br/>
              16. Considérant, en premier lieu, que l'annulation partielle du décret du 27 janvier 2015, prononcée par la présente décision, est sans incidence sur la légalité de l'arrêté attaqué ; que, par suite, l'Union des chirurgiens de France n'est pas fondée à en demander l'annulation par voie de conséquence de celle du décret ;<br/>
<br/>
              17. Considérant, en second lieu, que l'arrêté attaqué ne réserve pas aux infirmiers de bloc opératoire diplômés d'Etat d'autres actes ou activités que ceux prévus par le décret du 27 janvier 2015 ; que, par suite, pour les motifs indiqués aux points 4 et 5, le moyen tiré de la méconnaissance des dispositions des articles L. 4311-2 et L. 4311-13 du code de la santé publique doit être également écarté ;<br/>
<br/>
              18. Considérant qu'il résulte de ce qui précède que l'union requérante n'est pas  fondée à demander l'annulation de l'arrêté du 27 janvier 2015 ;<br/>
<br/>
              Sur la légalité de l'arrêté du 12 mars 2015 :<br/>
<br/>
              19. Considérant que l'arrêté du 12 mars 2015 modifie le contenu de la formation conduisant au diplôme d'Etat d'infirmier de bloc opératoire, définie par un arrêté du 22 octobre 2001, afin de prendre en compte les évolutions apportées aux actes et activités relevant de leur compétence par le décret du 27 janvier 2015 ; qu'il complète cette formation par un module de 49 heures portant sur les activités réalisées en application d'un protocole médical, conformément à l'article R. 4311-1-1 du code de la santé publique ; <br/>
<br/>
              20. Considérant, en premier lieu, que l'annulation partielle du décret du 27 janvier 2015, prononcée par la présente décision, est sans incidence sur la légalité de l'arrêté attaqué ; que, par suite, l'Union des chirurgiens de France n'est pas fondée à en demander l'annulation par voie de conséquence de celle du décret ;<br/>
<br/>
              21. Considérant, en second lieu, qu'en complétant ainsi qu'il le fait la formation initiale des infirmiers souhaitant obtenir le diplôme d'Etat d'infirmier de bloc opératoire, pour répondre aux exigences de qualification qui justifient l'attribution aux infirmiers de bloc opératoire, par le décret du 27 janvier 2015, d'une compétence exclusive pour accomplir certains actes techniques lors des interventions chirurgicales, l'arrêté du 12 mars 2015 n'a pas méconnu les dispositions des articles L. 4311-2 et L. 4311-13 du code de la santé publique ;<br/>
<br/>
              22. Considérant qu'il résulte de ce qui précède que l'union requérante n'est pas  fondée à demander l'annulation de l'arrêté du 12 mars 2015 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret du 27 janvier 2015 relatif aux actes infirmiers relevant de la compétence exclusive des infirmiers de bloc opératoire est annulé en tant seulement que, en l'état du dispositif applicable, il ne diffère pas au 31 décembre 2017 l'entrée en vigueur des dispositions du b) du 1° de l'article R. 4311-11-1 du code de la santé publique.<br/>
Article 2 : Le surplus des conclusions de la requête n° 389036 et les requêtes n° 389589 et n° 390121 de l'Union des chirurgiens de France sont rejetés. <br/>
Article 3 : La présente décision sera notifiée à l'Union des chirurgiens de France, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-04-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. ERREUR MANIFESTE. EXISTENCE. - MODALITÉS D'ENTRÉE EN VIGUEUR D'UN ACTE RÉGLEMENTAIRE - ENTRÉE EN VIGUEUR IMMÉDIATE ENTACHÉE D'UNE ERREUR MANIFESTE EU ÉGARD AUX CONSÉQUENCES SUR LE SERVICE PUBLIC HOSPITALIER.
</SCT>
<ANA ID="9A"> 01-05-04-01 Le décret attaqué dispose sans prévoir de mesures transitoires,que, dès son entrée en vigueur, les actes et activités énumérés à l'article R. 4311-11-1 du code de la santé publique, lorsqu'ils ne sont pas accomplis par le chirurgien lui-même, ne peuvent désormais être accomplis que par des infirmiers titulaires du diplôme d'Etat de bloc opératoire. En outre, ceux de ces infirmiers qui sont en exercice ou en cours de formation doivent avoir suivi une formation complémentaire, dans l'attente de son intégration à la formation préparant à ce diplôme....   ,,En tant qu'il réserve aux infirmiers de bloc opératoire certains actes accomplis au cours d'une intervention chirurgicale, qui pouvaient auparavant, au moins pour partie, être accomplis par les infirmiers voire par les aides-opératoires, tout en ne leur permettant d'accomplir ces actes qu'après avoir suivi une formation complémentaire, le décret, qui fait peser la réalisation de ces actes, à la date de son entrée en vigueur, sur les seuls chirurgiens, puis sur un nombre dans les premiers temps limité d'infirmiers de bloc opératoire ayant reçu la formation requise, est entaché d'une erreur manifeste d'appréciation faute de prévoir des mesures transitoires, compte tenu des conséquences d'une telle entrée en vigueur immédiate sur le fonctionnement des services. Annulation dans cette mesure.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
