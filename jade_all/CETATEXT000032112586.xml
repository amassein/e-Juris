<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032112586</ID>
<ANCIEN_ID>JG_L_2016_02_000000382364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/11/25/CETATEXT000032112586.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 26/02/2016, 382364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:382364.20160226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme Klépierre a demandé au tribunal administratif de Montreuil la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice 2003 ainsi que des pénalités correspondantes. Par un jugement n° 1008280 du 15 mars 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE01822 du 10 avril 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement par la société Klépierre.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 juillet et 7 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Klépierre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la SA Klepierre ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu de l'article 208 C du code général des impôts, dans sa rédaction issue de l'article 11 de la loi du 30 décembre 2002 de finances pour 2003, applicable en 2003 : " I. - Les sociétés d'investissements immobiliers cotées s'entendent des sociétés par actions cotées sur un marché réglementé français, dont le capital social n'est pas inférieur à 15 millions d'euros, qui ont pour objet principal l'acquisition ou la construction d'immeubles en vue de la location, ou la détention directe ou indirecte de participations dans des personnes visées à l'article 8 et aux 1, 2 et 3 de l'article 206 dont l'objet social est identique. / II. - Les sociétés d'investissements immobiliers cotées visées au I et leurs filiales détenues à 95 % au moins, directement ou indirectement, de manière continue au cours de l'exercice, soumises à l'impôt sur les sociétés et ayant un objet identique, peuvent opter pour l'exonération d'impôt sur les sociétés pour la fraction de leur bénéfice provenant de la location des immeubles et des plus-values sur la cession à des personnes non liées au sens du 12 de l'article 39 d'immeubles, de participations dans des personnes visées à l'article 8 ou dans des filiales soumises au présent régime." ; que les sociétés ou organismes mentionnés aux articles 206 à 208 quinquies du code général des impôts cessant, lorsqu'ils exercent l'option ainsi prévue, d'être totalement ou partiellement soumis à l'impôt sur les sociétés au taux prévu au deuxième alinéa du I de l'article 219, ils font l'objet, en vertu du deuxième alinéa du 2 de l'article 221 du même code, d'une imposition immédiate, dans les conditions prévues au 1 et 3 de l'article 201 de ce code en cas de cession ou de cessation d'une entreprise industrielle ou commerciale ; qu'aux termes du IV de l'article 219 du même code, dans sa version applicable à l'imposition en litige : " Le taux de l'impôt est fixé à 16,5 % en ce qui concerne les plus-values imposables en application du 2 de l'article 221 et du deuxième alinéa de l'article 223 F, relatives aux immeubles et parts des organismes mentionnés au dernier alinéa du II de l'article 208 C inscrits à l'actif des sociétés d'investissements immobiliers cotées et de leurs filiales qui ont opté pour le régime prévu à cet article " ;<br/>
<br/>
              2. Considérant que, pour la détermination de la plus-value latente sur les parts des sociétés détenant des immeubles immédiatement imposable en cas d'option pour le régime prévu par l'article 208 C du code général des impôts, la valeur des actifs concernés doit être appréciée, même s'ils ont vocation à être conservés par la société à la suite de cette option, comme en cas de cession, compte tenu de tous les éléments permettant d'obtenir un chiffre aussi voisin que possible de celui qu'aurait entraîné le jeu normal de l'offre et de la demande à la date où l'option est exercée ; que le contribuable peut notamment faire valoir qu'il convient de prendre en compte d'éventuelles décotes qui seraient pratiquées en cas de cession, dont il lui appartient alors de justifier la pertinence au regard du jeu normal de l'offre et de la demande ; qu'il appartient à l'administration, si elle conteste ces ajustements, d'établir que les éléments invoqués par le contribuable pour en justifier la pertinence, dans leur principe comme dans leur montant, ne sont pas fondés ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Klépierre a opté pour le régime des sociétés d'investissements immobiliers cotées prévu à l'article 208 C du code général des impôts à compter du 1er janvier 2003 ; qu'en application des dispositions combinées du deuxième alinéa du 2 de l'article 221 et du IV de l'article 219 du code général des impôts, la société a acquitté l'impôt sur les sociétés au taux de 16,5 % sur les plus-values correspondant à des parts de sociétés qu'elle détenait ; qu'à la suite d'une vérification de comptabilité, l'administration a remis en cause l'assiette de ces plus-values latentes ; que la société Klépierre se pourvoit en cassation contre l'arrêt du 10 avril 2014 par lequel la cour administrative d'appel de Versailles a rejeté son appel formé contre le jugement du tribunal administratif de Montreuil du 15 mars 2012 rejetant sa demande en décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice 2003 à raison de cette rectification ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, d'une part,  que la société Klepierre a, pour estimer l'actif net réévalué des filiales en cause, déduit de la valeur d'expertise des immeubles portés par ces sociétés les droits d'enregistrement (4,80 %) et les frais notariés (1,20 %) exigibles en cas de cession de ces immeubles, la société justifiant la décote ainsi pratiquée, qui n'était pas contestée par l'administration, par la pratique de marché consistant, dans le cadre de contrats " clés en main ", à la réfaction du prix de vente d'un immeuble à hauteur des droits d'enregistrement et frais de notariés que devra acquitter l'acquéreur ; que, d'autre part, la société Klepierre a également réduit la valeur des titres correspondant à l'actif net réévalué ainsi calculé d'une décote, d'un montant représentant 4,6 % de cet actif net réévalué, en présentant  devant l'administration puis devant les juges du fond des éléments pour justifier cette autre décote ; qu'elle a ainsi indiqué devant les juges du fond que cette décote correspondait à la fiscalité latente systématiquement pratiquée sur le marché, en cas de cession des titres d'une société immobilière, à raison de l'anticipation, par l'acquéreur, du fait qu'en cas de cession ultérieure des immeubles détenus par la société, la plus-value imposable entre ses mains inclura la plus-value latente constatée à la date d'acquisition de la société, et qu'en cas de conservation de ces immeubles, il ne pourra bénéficier au niveau de la société acquise d'un amortissement fiscalement déductible, calculé sur une base correspondant à la valeur vénale des immeubles retenue pour la détermination du prix d'acquisition des titres qu'elle a présenté, devant les juges du fond, les éléments de calcul de ce " déficit de base amortissable " invoqué sur les immeubles détenus par les filiales en cause, et précisé que le montant ainsi obtenu avait pris en compte la pratique de marché consistant à partager la décote pour fiscalité latente pour moitié entre le vendeur et l'acheteur ; que la cour a cependant fait droit à la défense de l'administration en jugeant, sans examiner ces justifications,  que la société qui exerce l'option pour le régime prévu par l'article 208 C du code général des impôts ne peut utilement se prévaloir, pour évaluer la valeur des titres, de la prise en compte, par le marché, d'une décote pour fiscalité latente ; que ce faisant, en écartant par principe l'application de cette décote sur la valeur des titres sans prendre en considération les justifications avancées par la société, non contestées par l'administration, alors que, ainsi qu'il a été dit au point 2, une telle décote sur la plus-value latente immédiatement imposable peut être prise en compte, quand bien même les actifs auraient vocation à être conservés par la société à la suite de son option pour ce régime fiscal, dès lors  que cette société est à même de la justifier au regard du jeu normal de l'offre et de la demande, la cour a commis une erreur de droit ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit  être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstance de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Klepierre au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 10 avril 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : L'Etat versera à la société Klépierre une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Klépierre et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
