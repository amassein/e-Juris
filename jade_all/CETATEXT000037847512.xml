<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847512</ID>
<ANCIEN_ID>JG_L_2018_12_000000417417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/75/CETATEXT000037847512.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/12/2018, 417417, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417417.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 20 juin 2018, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la SA Point P dirigées contre le jugement du 20 novembre 2017 du tribunal administratif de Montreuil en tant seulement que ce jugement s'est prononcé sur les conclusions tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2013 et 2014 dans les rôles de la commune de Pantin (Seine-Saint-Denis).<br/>
<br/>
              Par un mémoire en défense, enregistré le 5 septembre 2018, le ministre de l'action et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
              Par un nouveau mémoire, enregistré le 29 novembre 2018, la SA Point P reprend les conclusions de ses précédents mémoires et les mêmes moyens. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la SA Point P.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1498 du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / (...) 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales. / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ". Il résulte de ces dispositions que le juge de l'impôt, saisi d'une contestation portant sur la méthode de détermination de la valeur locative d'un bien, a l'obligation, lorsqu'il estime irrégulière la méthode d'évaluation initialement retenue par l'administration, de lui substituer la méthode qu'il juge régulière. Dans le cas où il retient une évaluation par comparaison, il doit, en outre, pour l'application des dispositions du 2° de l'article 1498 du code général des impôts, statuer d'office sur le terme de comparaison qu'il estime, par une appréciation souveraine, pertinent et dont il a vérifié la régularité, au vu des éléments dont il dispose ou qu'il a sollicités par un supplément d'instruction. Il ne lui appartient pas, en l'absence de contestation sur les éléments au dossier portant sur le terme de comparaison qu'il envisage de retenir, de vérifier d'office si ce local-type remplit l'ensemble des conditions de régularité posées par le 2° de l'article 1498.<br/>
<br/>
              2. Pour établir la valeur locative de l'ensemble immobilier litigieux situé 110 avenue du général Leclerc à Pantin (Seine-Saint-Denis), propriété de la société Point P, contestée devant lui par cette dernière au soutien de sa demande tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2013 et 2014, le tribunal administratif de Montreuil a tout d'abord relevé que l'administration avait admis en cours d'instance que la valeur locative des locaux litigieux n'avait pas été régulièrement arrêtée. Il a ensuite estimé, au vu de la proposition non contestée de l'administration, d'une part, qu'il convenait de déterminer la valeur locative de l'ensemble immobilier en litige par comparaison avec le local-type n° 55 du procès-verbal des opérations foncières de la commune du Bourget, évalué d'après le bail en cours au 1er janvier 1970 et, d'autre part, qu'il y avait lieu de majorer la valeur locative de ce terme de comparaison de dix pour cent afin de tenir compte de la différence de superficie entre les deux biens immobiliers. En se bornant à constater que le local type n° 55 avait été régulièrement évalué et était situé dans une zone géographique et dans un environnement économique comparables à ceux des locaux en litige, sans rechercher si, de par ses caractéristiques intrinsèques, il constituait un terme de comparaison pertinent, le tribunal a méconnu l'étendue de son office définie au point 1.<br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société Point P est fondée à demander l'annulation du jugement attaqué en tant que les premiers juges ont statué sur les conclusions tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles cette société a été assujettie au titre des années 2013 et 2014 dans les rôles de la commune de Pantin.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Point P au titre de l'article L. 761-1 du code de justice administrative  et de mettre à la charge de l'Etat le versement à cette dernière de la somme de 1 500 euros.<br/>
<br/>
<br/>
<br/>                         D E C I D E :<br/>
                                      --------------<br/>
<br/>
Article 1er : Le jugement du 20 novembre 2017 du tribunal administratif de Montreuil est annulé en tant qu'il s'est prononcé sur les conclusions tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles la société Point P a été assujettie au titre des années 2013 et 2014 dans les rôles de la commune de Pantin.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Montreuil.<br/>
<br/>
Article 3 : L'Etat versera à la société Point P une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Point P et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
