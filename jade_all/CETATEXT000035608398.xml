<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035608398</ID>
<ANCIEN_ID>JG_L_2017_09_000000393188</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/60/83/CETATEXT000035608398.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 22/09/2017, 393188, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393188</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393188.20170922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le groupement d'intérêt économique (GIE) Aprionis Fonctions transverses a demandé au tribunal administratif de Cergy-Pontoise de prononcer la décharge ou la réduction de la cotisation de taxe d'habitation à laquelle il a été assujetti au titre de l'année 2011 à raison d'un immeuble situé à Malakoff (Hauts-de-Seine). Par un jugement n° 1306137 du 2 juillet 2015, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 septembre et 3 décembre 2015 et le 9 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le GIE Aprionis Fonctions transverses demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat du groupement d'intérêt économique Fonctions Transverses.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que le groupement d'intérêt économique (GIE) Aprionis Fonctions transverses a demandé la réduction de la cotisation de taxe d'habitation à laquelle il a été assujetti au titre de l'année 2011 à raison de locaux à usage de bureaux qu'il utilise, pour son activité, dans un immeuble situé à Malakoff (Hauts-de-Seine). Il se pourvoit en cassation contre le jugement du 2 juillet 2015 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa demande. <br/>
<br/>
              2. Aux termes de l'article 1407 du code général des impôts : " I. La taxe d'habitation est due : /(...) 2° Pour les locaux meublés conformément à leur destination et occupés à titre privatif par les sociétés, associations et organismes privés et qui ne sont pas retenus pour l'établissement de la cotisation foncière des entreprises ; (...) / II. Ne sont pas imposables à la taxe : / 1° Les locaux passibles de la cotisation foncière des entreprises lorsqu'ils ne font pas partie de l'habitation personnelle des contribuables ; (...) ". Aux termes de l'article 1408 du même code : " I. La taxe est établie au nom des personnes qui ont, à quelque titre que ce soit, la disposition ou la jouissance des locaux imposables. (...) ". Aux termes de l'article 1415 du même code : " La taxe foncière sur les propriétés bâties, la taxe foncière sur les propriétés non bâties et la taxe d'habitation sont établies pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition ". <br/>
<br/>
              3. Aux termes de l'article 1461 du même code : " Sont exonérés de la cotisation foncière des entreprises: / (...) 8° Les associations régies par la loi du 1er juillet 1901 relative au contrat d'association, constituées conformément à l'accord du 25 avril 1996 portant dispositions communes à l'AGIRC et à l'ARRCO, et les associations et groupements d'intérêt économique contrôlés par ces associations et comptant parmi leurs membres soit au moins une fédération ou institution de retraite complémentaire régie par le titre II du livre IX du code de la sécurité sociale, soit au moins une association ou un groupement d'intérêt économique comptant parmi ses membres au moins une telle fédération ou institution, pour leurs seules opérations de gestion et d'administration réalisées pour le compte de leurs membres qui ne sont pas dans le champ d'application de la cotisation foncière des entreprises en application du I de l'article 1447 ".<br/>
<br/>
              4. Il résulte de la combinaison de ces dispositions avec celles de l'article 1407 du code général des impôts qu'un groupement d'intérêt économique satisfaisant aux conditions qu'elles posent bénéficie d'une exonération partielle de cotisation foncière des entreprises, à concurrence de la part des opérations de gestion et d'administration qu'il réalise pour le compte de ses membres placés hors du champ d'application de la cotisation foncière des entreprises dans le total de ses opérations, et n'est, symétriquement, assujetti à la taxe d'habitation, à raison des locaux pris en compte pour la détermination de l'assiette de la cotisation foncière des entreprises, que dans la proportion dans laquelle il est exonéré de cotisation foncière des entreprises.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge du fond que le GIE Aprionis Fonctions transverses, dont il est constant qu'il remplit les conditions posées par le 8° de l'article 1461 du code général des impôts, soutenait devant le tribunal administratif que les opérations de gestion et d'administration qu'il avait réalisées, au titre de la période de référence prise en compte pour l'établissement de la cotisation foncière des entreprises au titre de 2011, au profit de ceux de ses membres qui sont placés hors du champ de cet impôt représentaient 86, 22 % de l'ensemble de ses opérations. Il en déduisait qu'il ne devait être assujetti à la taxe d'habitation, à raison des locaux qu'il occupe, que dans cette même proportion, alors qu'il avait été assujetti à hauteur de 100 %. En se fondant, pour rejeter cette demande, sur le seul motif que le GIE ne contestait pas avoir bénéficié, en fait, d'une absence totale d'assujettissement à la cotisation foncière des entreprises au titre de 2011, sans rechercher si, et dans quelle proportion, il était en droit de bénéficier, en application de la règle rappelée au point 4, d'une telle exonération et si, par voie de conséquence, il était fondé à bénéficier d'une exonération partielle de taxe d'habitation, le tribunal administratif a entaché son jugement d'une erreur de droit. Par suite, sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, le GIE Aprionis Fonctions transverses est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au GIE Aprionis Fonctions transverses, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 2 juillet 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : L'Etat versera au GIE Aprionis Fonctions transverses la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée au groupement d'intérêt économique Aprionis Fonctions transverses et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
