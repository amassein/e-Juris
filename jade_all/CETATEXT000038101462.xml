<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101462</ID>
<ANCIEN_ID>JG_L_2019_02_000000418311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101462.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 06/02/2019, 418311, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418311.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 19 février et 18 décembre 2018, M. B...A...demande au Conseil d'Etat d'annuler l'arrêté du 13 décembre 2017 du ministre de la cohésion des territoires relatif au contenu de la notice d'information relative aux obligations du bailleur et aux voies de recours et d'indemnisation du locataire jointe au congé délivré par le bailleur en raison de sa décision de reprendre ou de vendre le logement, en tant que cet arrêté dispose, à l'article 2.2.3 de son annexe, qu' " il n'est pas obligatoire que la superficie du logement soit mentionnée dans le congé " délivré par le bailleur.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution ;<br/>
<br/>
              - la loi n° 65-557 du 10 juillet 1965 ;<br/>
<br/>
              - la loi n° 89-462 du 6 juillet 1989 ;<br/>
<br/>
              - la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
<br/>
              - la décision n° 418311 du 23 mai 2018 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A...;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 15 de la loi du 6 juillet 1989, dans sa rédaction issue de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " (...) Une notice d'information relative aux obligations du bailleur et aux voies de recours et d'indemnisation du locataire est jointe au congé délivré par le bailleur en raison de sa décision de reprendre ou de vendre le logement. Un arrêté du ministre chargé du logement, pris après avis de la Commission nationale de concertation, détermine le contenu de cette notice ". En application de ces dispositions, un arrêté du 13 décembre 2017 du ministre de la cohésion des territoires définit le contenu de la notice d'information relative aux obligations du bailleur et aux voies de recours et d'indemnisation du locataire jointe au congé délivré par le bailleur en raison de sa décision de reprendre ou de vendre le logement. M. A...demande au Conseil d'Etat d'annuler cet arrêté en tant qu'il dispose, à l'article 2.2.3 de son annexe, qu' " il n'est pas obligatoire que la superficie du logement soit mentionnée dans le congé " délivré par le bailleur.<br/>
<br/>
              2. D'une part, aux termes de l'article 46 de la loi du 10 juillet 1965 : " Toute promesse unilatérale de vente ou d'achat, tout contrat réalisant ou constatant la vente d'un lot ou d'une fraction de lot mentionne la superficie de la partie privative de ce lot ou de cette fraction de lot. La nullité de l'acte peut être invoquée sur le fondement de l'absence de toute mention de superficie. / Cette superficie est définie par le décret en Conseil d'Etat prévu à l'article 47./ Les dispositions du premier alinéa ci-dessus ne sont pas applicables aux caves, garages, emplacements de stationnement ni aux lots ou fractions de lots d'une superficie inférieure à un seuil fixé par le décret en Conseil d'Etat prévu à l'article 47. / Le bénéficiaire en cas de promesse de vente, le promettant en cas de promesse d'achat ou l'acquéreur peut intenter l'action en nullité, au plus tard à l'expiration d'un délai d'un mois à compter de l'acte authentique constatant la réalisation de la vente. / La signature de l'acte authentique constatant la réalisation de la vente mentionnant la superficie de la partie privative du lot ou de la fraction de lot entraîne la déchéance du droit à engager ou à poursuivre une action en nullité de la promesse ou du contrat qui l'a précédé, fondée sur l'absence de mention de cette superficie. / Si la superficie est supérieure à celle exprimée dans l'acte, l'excédent de mesure ne donne lieu à aucun supplément de prix. / Si la superficie est inférieure de plus d'un vingtième à celle exprimée dans l'acte, le vendeur, à la demande de l'acquéreur, supporte une diminution du prix proportionnelle à la moindre mesure. / L'action en diminution du prix doit être intentée par l'acquéreur dans un délai d'un an à compter de l'acte authentique constatant la réalisation de la vente, à peine de déchéance. ".<br/>
<br/>
              3. D'autre part, aux termes du premier alinéa du II de la loi du 6 juillet 1989, dans sa rédaction initiale : " Lorsqu'il est fondé sur la décision de vendre le logement, le congé doit, à peine de nullité, indiquer le prix et les conditions de la vente projetée. Le congé vaut offre de vente au profit du locataire : l'offre est valable pendant les deux premiers mois du délai de préavis ". Le I de l'article 190 de la loi du 13 décembre 2000 a complété cet alinéa par une phrase ainsi rédigée : " Les dispositions de l'article 46 de la loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis ne sont pas applicables au congé fondé sur la décision de vendre le logement. ". Le II de l'article 190 de cette loi dispose que " Sous réserve des décisions de justice passées en force de chose jugée, sont validés les congés fondés sur la décision de vendre le logement en tant qu'ils n'ont pas satisfait aux dispositions de l'article 46 mentionné au I ci-dessus ".<br/>
<br/>
              4. En premier lieu, il résulte des termes mêmes de l'article 46 de la loi du 10 juillet 1965 cité ci-dessus, dans sa rédaction issue de loi du 18 décembre 1996 améliorant la protection des acquéreurs de lots de copropriété, que l'obligation de mentionner la superficie du lot vendu qu'il prévoit ne s'applique qu'à des contrats relatifs à la cession d'un logement, tels que les promesses unilatérales de vente ou d'achat ou les contrats de vente. Cette obligation ne concerne donc pas, ainsi que l'a confirmé l'article 190 de la loi du 13 décembre 2000, le congé, fondé sur la décision de vendre le bien, que le propriétaire notifie au locataire, un tel acte, s'il ouvre au profit du locataire un droit de préemption au prix indiqué, ne résultant pas de la rencontre des volontés du bailleur et du preneur en vue d'une cession du logement à ce dernier. Dès lors, en indiquant à l'article 2.2.3 de l'annexe de son arrêté du 13 décembre 2017, qu' " il n'est pas obligatoire que la superficie du logement soit mentionnée dans le congé " délivré par le bailleur, le ministre n'a méconnu ni l'étendue de sa compétence ni l'article 46 de la loi du 10 juillet 1965.<br/>
<br/>
              5. En deuxième lieu, le Conseil d'Etat, statuant au contentieux a jugé, dans sa décision n° 418311 du 23 mai 2018, qu'il n'y avait pas lieu de renvoyer au Conseil constitutionnel la question, soulevée par M.A..., de la conformité aux droits et libertés garantis par la Constitution de l'article 190 de la loi du 13 décembre 2000. Dès lors, le requérant ne peut utilement soutenir que les dispositions contestées de l'arrêté du 13 décembre 2017, qui se bornent à reprendre les termes de la loi, méconnaîtraient le principe d'égalité devant la loi ainsi que l'objectif à valeur constitutionnelle d'intelligibilité et d'accessibilité du droit.<br/>
<br/>
              6. En troisième lieu, si M. A...fait encore valoir que l'arrêté attaqué méconnaîtrait les dispositions du II de l'article 190 de la loi du 13 décembre 2000, qui valident les congés pour vendre ne comportant pas l'indication de superficie du lot, sous réserve des décisions de justice passées en force de chose jugée avant leur entrée en vigueur, ces dispositions, contrairement à ce qu'il soutient, ne renvoient pas au juge le soin de décider de l'application de l'article 46 de la loi du 10 juillet 1965 au congé pour vendre.<br/>
<br/>
              7. En dernier lieu, le moyen tiré de ce que les dispositions critiquées de l'arrêté du 13 décembre 2017 ne permettraient pas d'assurer correctement la mise en oeuvre de l'article 46 de la loi du 10 juillet 1965 en matière de superficie est dépourvu des précisions suffisantes permettant d'en apprécier le bien-fondé. En outre, et en tout état de cause, le locataire destinataire d'un congé pour vente dispose de la partie privative en cause, dont il lui est loisible de constater ou de faire vérifier la superficie.<br/>
<br/>
              8. Il résulte de ce qui précède que la requête de M. A... doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
