<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285480</ID>
<ANCIEN_ID>JG_L_2016_10_000000397537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 20/10/2016, 397537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:397537.20161020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 1er mars et 9 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société Compagnie du Cambodge demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le second alinéa du paragraphe 190 de l'instruction BOI-BIC-PVMV-30-10 publiée au bulletin officiel des finances publiques-impôts le 12 septembre 2012 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 15 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de commerce ; <br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 6 et 17 octobre 2016, présentées pour la  société Compagnie du Cambodge.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le paragraphe 190 de l'instruction fiscale publiée le 12 septembre 2012 sous la référence BOI-BIC-PVMV-30-10, intitulé " actions d'autocontrôle ", prévoit que : " Lorsque des actions d'une société sont possédées par une ou plusieurs sociétés dont elle détient directement ou indirectement le contrôle, les droits de vote attachés à ces actions ne peuvent être exercés à l'assemblée générale de la société (C. com. art. L. 233-31). / Dès lors que ces titres sont privés de droit de vote et que la société qui les détient est elle-même détenue par la société émettrice des titres, lesdits titres ne peuvent être considérés comme des titres de participation éligibles au taux réduit d'imposition " ; que la société Compagnie du Cambodge doit être regardée comme demandant l'annulation du second alinéa de ce paragraphe ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du a ter du I de l'article 219 du code général des impôts : " (...) constituent des titres de participation les parts ou actions de sociétés revêtant ce caractère sur le plan comptable. Il en va de même des actions acquises en exécution d'une offre publique d'achat ou d'échange par l'entreprise qui en est l'initiatrice ainsi que des titres ouvrant droit au régime des sociétés mères ou, lorsque leur prix de revient est au moins égal à 22 800 000 euros, qui remplissent les conditions ouvrant droit à ce régime autres que la détention de 5 % au moins du capital de la société émettrice, si ces actions ou titres sont inscrits en comptabilité au compte de titres de participation ou à une subdivision spéciale d'un autre compte du bilan correspondant à leur qualification comptable (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 233-31 du code de commerce : " Lorsque des actions ou des droits de vote d'une société sont possédés par une ou plusieurs sociétés dont elle détient directement ou indirectement le contrôle, les droits de vote attachés à ces actions ou ces droits de vote ne peuvent être exercés à l'assemblée générale de la société. Il n'en est pas tenu compte pour le calcul du quorum " ; <br/>
<br/>
              4. Considérant qu'en excluant du bénéfice du régime des plus-values de long terme prévu au I de l'article 219 précité les titres d'autocontrôle mentionnés à l'article L. 233-31 du code de commerce, au seul motif que les droits de vote attachés à ces titres ne peuvent être exercés à l'assemblée générale de la société, alors que ni les dispositions du I de l'article 219 ni aucune autre disposition du code ne conditionnent le bénéfice de ce régime à l'exercice des droits de vote, le ministre ne s'est pas borné à expliciter la loi mais y a ajouté des dispositions nouvelles qu'aucun texte ne l'autorisait à prendre ; que la société requérante est, par suite, fondée à demander l'annulation du second alinéa du paragraphe 190 de l'instruction fiscale publiée sous la référence BOI-BIC-PVMV-30-10 ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Compagnie du Cambodge ;<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                       --------------<br/>
<br/>
Article 1er : Le second alinéa du paragraphe 190 de l'instruction fiscale publiée le 12 septembre 2012 sous la référence BOI-BIC-PVMV-30-10 est annulé. <br/>
<br/>
Article 2 : L'Etat versera à la société Compagnie du Cambodge la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Compagnie du Cambodge et au ministre de l'économie et des finances. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
