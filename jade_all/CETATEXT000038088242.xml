<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088242</ID>
<ANCIEN_ID>JG_L_2019_02_000000420338</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088242.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 01/02/2019, 420338, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420338</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420338.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Caen d'annuler la décision du 13 juillet 2016 par laquelle l'officier adjoint au commandant de la région de gendarmerie de Basse-Normandie lui a infligé la sanction de la réprimande. Par une ordonnance n° 1601863 du 23 septembre 2016, le président du tribunal administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 16NT03796 du 2 mai 2018, enregistrée le 3 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nantes a annulé cette ordonnance et a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée par Mme A...et un mémoire en réplique, enregistrés au greffe de cette cour les 23 novembre 2016 et 26 janvier 2018.<br/>
<br/>
              Par cette requête, ce mémoire en réplique et deux nouveaux mémoires, enregistrés les 9 juillet 2018 et 16 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 13 juillet 2016 par laquelle l'officier adjoint au commandant de la région de gendarmerie de Basse-Normandie lui a infligé la sanction de la réprimande ; <br/>
<br/>
              2°) d'enjoindre à la ministre des armées d'effacer de son dossier toute référence à cette sanction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeA....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 janvier 2019, présentée par Mme A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que MmeA..., capitaine de gendarmerie affectée à la compagnie de Deauville, est détachée depuis le 1er janvier 2015 comme chargée de mission au sein du bureau de la performance et de la cohérence opérationnelle de la région de gendarmerie de Basse-Normandie. Par une décision du 13 juillet 2016, l'officier adjoint au commandant de cette région a infligé à Mme A...une sanction de réprimande en raison de sa reprise de service tardive les 22 et 30 mars 2016.<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 4137-12 du code de la défense : " Lorsque les autorités militaires de premier, deuxième ou troisième niveau ne peuvent exercer leur pouvoir disciplinaire pour une durée déterminée, elles sont remplacées par une autorité militaire exerçant ce pouvoir par suppléance. Cette autorité est celle qui est prévue par les textes d'organisation de la force armée ou de la formation rattachée. A défaut, c'est le premier des subordonnés de cette autorité dans l'ordre hiérarchique qui exerce ce pouvoir ". Le moyen tiré de ce que le signataire de la décision attaquée ne disposait pas d'une délégation de signature du commandant du groupement départemental de gendarmerie du Calvados en matière de ressources humaines ne peut qu'être écarté, dès lors qu'il ressort des pièces du dossier que ce signataire exerçait le pouvoir disciplinaire sur le fondement de la suppléance prévue par les dispositions précitées du code de la défense. <br/>
<br/>
              3. En deuxième lieu, les termes de l'instruction ministérielle du 12 juin 2014 relative aux sanctions disciplinaires qu'invoque Mme A...étant dépourvus de valeur réglementaire, il ne résulte d'aucun texte ni d'aucun principe général du droit qu'une sanction disciplinaire ne pourrait pas être infligée à un militaire par son supérieur hiérarchique direct.<br/>
<br/>
              4. En troisième lieu, aux termes de l'article R. 4137-15 du code de la défense : " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit (...) sur les faits qui lui sont reprochés devant l'autorité militaire de premier niveau dont il relève. Au préalable, un délai de réflexion, qui ne peut être inférieur à un jour franc, lui est laissé pour organiser sa défense.(...) /Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner ". Il résulte de ces dispositions que le droit à la communication du dossier comporte pour le militaire intéressé celui d'en prendre copie, à moins que sa demande ne présente un caractère abusif.<br/>
<br/>
              5. D'une part, il ressort des pièces du dossier que la requérante a été informée le 15 juin 2016 de l'existence d'une demande de sanction à la suite de deux retards de trente minutes dans sa prise de poste les 23 et 30 mars précédents. Elle a été reçue par le signataire de la décision attaquée, autorité militaire de premier niveau, le 6 juillet 2016 afin de présenter ses observations. Dès lors, le moyen tiré de ce que la requérante n'a pas pu présenter ses explications devant l'autorité militaire de premier niveau compétente avant le prononcé de la sanction disciplinaire doit être écarté.<br/>
<br/>
              6. D'autre part, si Mme A...ne s'est pas vu remettre de copie de son dossier disciplinaire avant son entretien du 6 juillet 2016, il ressort des pièces du dossier qu'elle a pris connaissance de son dossier le 23 juin et que l'administration lui a remis à cette occasion un document destiné à lui permettre d'acquitter les frais de photocopie auprès de la trésorerie. La requérante a toutefois attendu, sans motif valable, le 8 juillet suivant pour effectuer son paiement et obtenir, le même jour, les copies demandées. En outre, son entretien a été reporté à plusieurs reprises afin de lui permettre de consulter son dossier et d'en prendre copie. Dans ces conditions, la circonstance que la copie du dossier n'a été remise à la requérante qu'après l'entretien préalable au prononcé de la sanction n'a pas entaché d'irrégularité la procédure de sanction.<br/>
<br/>
              Sur la légalité interne de la décision attaquée : <br/>
<br/>
              7. En premier lieu, aux termes de l'article L. 4137-2 du code de la défense : " 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre (...) ". <br/>
<br/>
              8. Il ressort des pièces du dossier qu'alors que les horaires de travail de Mme A... prévoient une pause pour déjeuner entre 12h15 et 13h30, en application d'une décision du 10 décembre 2015 du commandant de la région de gendarmerie de Basse-Normandie, la requérante a repris son service avec trente minutes de retard à deux reprises en quelques jours, les 23 et 30 mars 2016, sans justification valable et sans en prévenir sa hiérarchie, alors même qu'elle dispose d'un téléphone portable de service et qu'elle avait fait l'objet de plusieurs observations écrites sur son comportement professionnel inapproprié dans les mois précédents. Dès lors, le caractère fautif de ces retards est établi.<br/>
<br/>
              9. En deuxième lieu, eu égard aux responsabilités de MmeA..., capitaine de gendarmerie, et à la nature des manquements en cause, l'autorité disciplinaire n'a pas, dans les circonstances de l'espèce, et compte tenu du pouvoir d'appréciation dont elle disposait, pris une sanction disproportionnée en lui infligeant une réprimande, relevant du premier groupe de sanctions. La circonstance, à la supposer établie, que d'autres gendarmes ayant repris leur service avec retard n'ont pas été sanctionnés, et celle que les deux retards de la requérante n'ont pas entraîné de dysfonctionnement dans le service, sont sans incidence sur la légalité de la décision attaquée. <br/>
<br/>
              10. En dernier lieu, il ne ressort pas des pièces du dossier que la décision attaquée du 13 juillet 2016 aurait été prise en rétorsion à la saisine par la requérante de la plateforme " Stop-discri " de l'inspection générale de la gendarmerie nationale. De même, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              11. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision qu'elle attaque et que ses conclusions à fin d'injonction, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
