<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861196</ID>
<ANCIEN_ID>JG_L_2015_12_000000376914</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861196.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 23/12/2015, 376914, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376914</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376914.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 31 mars 2014, 12 janvier 2015 et 13 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société La nouvelle Aventure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la décision implicite de rejet résultant du silence gardé par Pôle emploi sur sa demande du 23 octobre 2013 tendant à ce que soient fixées, par voie réglementaire, les conditions et les modalités d'application de la prise en charge, au titre de l'annexe X, relative aux artistes du spectacle, du règlement général annexé à la convention du 6 mai 2011 relative à l'indemnisation du chômage, des heures effectuées par un artiste du spectacle vivant dans le cadre d'actions culturelles, de la phase préparatoire à la phase de représentation publique, et, d'autre part, la décision du 31 janvier 2014 par laquelle Pôle emploi s'est estimé incompétent pour examiner cette demande et l'a transmise à l'UNEDIC ;<br/>
<br/>
              2°) d'enjoindre à Pôle emploi, à titre principal, d'édicter cette réglementation, dans le délai de deux mois à compter de la décision à intervenir, sous astreinte de 150 euros par jour de retard, et, à titre subsidiaire, de réexaminer sa demande, sous la même astreinte ;<br/>
<br/>
              3°) de mettre à la charge de Pôle emploi la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Pôle emploi, et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de l'UNEDIC ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la portée des conclusions de la société La nouvelle Aventure : <br/>
<br/>
              1. La société La nouvelle Aventure demande l'annulation des décisions par lesquelles Pôle emploi, établissement public à caractère administratif, a refusé, implicitement puis de façon expresse, de faire droit à sa demande en prenant des mesures à caractère réglementaire pour préciser la nature des activités, exercées par des artistes du spectacle, susceptibles de relever de l'annexe X du règlement général annexé à la convention du 6 mai 2011 relative à l'indemnisation du chômage. D'une part, elle ne conteste pas la décision qui aurait été prise par l'Union nationale interprofessionnelle pour l'emploi dans l'industrie et le commerce (UNEDIC) à la suite de la transmission de sa demande par Pôle emploi. D'autre part, la décision du 31 janvier 2014, par laquelle le directeur général de Pôle emploi a expressément rejeté sa demande, s'est substituée à la décision par laquelle il avait implicitement rejeté cette demande. Par suite, les conclusions de la requête de la société La nouvelle Aventure doivent être regardées comme exclusivement dirigées contre la décision de Pôle emploi du 31 janvier 2014.<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 5422-20 du code du travail, les mesures d'application des dispositions législatives relatives au régime d'assurance des travailleurs involontairement privés d'emploi " font l'objet d'accords conclus entre les organisations représentatives d'employeurs et de salariés. / Ces accords sont agréés dans les conditions définies par la présente section. / En l'absence d'accord ou d'agrément de celui-ci, les mesures d'application sont déterminées par décret en Conseil d'Etat ". Aux termes de l'article L. 5427-1 du même code : " Les parties signataires de l'accord prévu à l'article L. 5422-20 confient la gestion du régime d'assurance chômage à un organisme de droit privé de leur choix. / Le service de l'allocation d'assurance est assuré, pour le compte de cet organisme, par l'institution mentionnée à l'article L. 5312-1 (...) ", c'est-à-dire Pôle emploi. Par l'article 1er de la convention du 6 mai 2011, les partenaires sociaux ont confié à l'UNEDIC la gestion du régime d'assurance chômage.<br/>
<br/>
              3. D'autre part, en vertu de l'article L. 5312-1 du code du travail, Pôle emploi est une institution nationale publique dotée de la personnalité morale et de l'autonomie financière qui " a pour mission de : (...) 4° Assurer, pour le compte de l'organisme gestionnaire du régime d'assurance chômage, le service de l'allocation d'assurance (...) 6° Mettre en oeuvre toutes autres actions qui lui sont confiées par (...) l'organisme gestionnaire du régime d'assurance chômage en relation avec sa mission (...) ". Aux termes du premier alinéa de l'article L. 5312-3 du même code, dans sa rédaction applicable à la date de la décision attaquée : " Une convention pluriannuelle conclue entre l'Etat, l'organisme gestionnaire du régime d'assurance chômage mentionné à l'article L. 5427-1 et l'institution publique mentionnée à l'article L. 5312-1 définit les objectifs assignés à celle-ci au regard de la situation de l'emploi et au vu des moyens prévisionnels qui lui sont alloués par l'organisme gestionnaire du régime d'assurance chômage et l'Etat ". Par une convention tripartite, conclue le 11 janvier 2012 pour les années 2012 à 2014, l'Etat et l'UNEDIC ont fixé les objectifs assignés à Pôle emploi au titre de la mission de service de l'allocation d'assurance chômage dont il est délégataire. Cette convention a elle-même renvoyé à une convention bilatérale, conclue le 21 décembre 2012 entre l'UNEDIC et Pôle emploi, par laquelle les conditions d'exercice de cette mission ont été précisées.<br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions que la détermination des règles applicables au régime de l'assurance chômage, notamment les critères d'attribution des allocations dues à ce titre, relève de la compétence des partenaires sociaux et que la gestion de ce régime est assurée par l'UNEDIC. Pôle emploi agit, pour le service de l'allocation d'assurance, pour le compte de cet organisme de droit privé, en vertu d'une délégation prévue par la loi et précisée par une convention tripartite qu'il conclut avec l'Etat et l'UNEDIC. Il ne dispose, à ce titre, d'aucune compétence propre lui conférant le pouvoir de prendre, par délibération de son conseil d'administration, des mesures à caractère réglementaire relatives aux conditions d'attribution de l'allocation d'assurance. <br/>
<br/>
              5. Si l'article R. 5312-6 du code du travail, dans sa rédaction applicable à la date de la décision attaquée, prévoit que le conseil d'administration de Pôle emploi délibère sur " 3° Les conditions de mise en oeuvre des dispositifs législatifs et réglementaires de la politique publique de l'emploi ", ces dispositions visent les actions confiées à cette institution par l'Etat ou par les collectivités territoriales, en vertu de l'article L. 5312-1 du même code, au titre des dispositifs en faveur de l'emploi. Elles ne concernent pas, contrairement à ce que soutient la société requérante, l'indemnisation des travailleurs privés d'emploi par le régime d'assurance.<br/>
<br/>
              6. Il suit de là que Pôle emploi, n'ayant pas compétence pour adopter les mesures réglementaires sollicitées en se substituant aux parties à la convention relative à l'indemnisation du chômage, était tenu d'opposer un refus à la demande de la société La nouvelle Aventure. Dès lors, la circonstance que l'adoption de telles mesures aurait été nécessaire au respect du principe de sécurité juridique et du principe d'égalité est sans incidence sur la légalité de la décision attaquée.<br/>
<br/>
              7. Si la société requérante fait également valoir que les services de Pôle emploi sont amenés, pour le service de l'allocation d'assurance, à interpréter les clauses de la convention relative à l'indemnisation du chômage et de son règlement annexé, il n'en résulte pas que Pôle emploi aurait adopté des dispositions règlementaires complétant ce règlement. Par suite, la société La nouvelle Aventure n'est pas fondée à soutenir que le refus opposé à sa demande violerait le droit constitutionnellement garanti à un recours effectif devant une juridiction, l'objectif d'accessibilité et d'intelligibilité de la loi, l'obligation incombant aux autorités administratives, en vertu de l'article 2 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, d'organiser un accès simple aux règles de droit qu'elles édictent, non plus que l'obligation leur incombant, sauf circonstances particulières y faisant obstacle, de publier dans un délai raisonnable les règlements qu'elles édictent. <br/>
<br/>
              8. Il résulte de ce qui précède que la société La nouvelle Aventure n'est pas fondée à demander l'annulation du refus opposé à sa demande par Pôle emploi. Dans ces conditions, il n'est pas nécessaire d'examiner la contestation par Pôle emploi de son intérêt à agir contre cette décision.<br/>
<br/>
              9. Par suite, les conclusions de la société requérante à fin d'injonction ne peuvent qu'être également rejetées. <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société La nouvelle Aventure le versement à Pôle emploi d'une somme de 1 500 euros, au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'il soit fait droit à la demande de la société La nouvelle Aventure présentée au même titre, ainsi qu'à celle présentée par l'UNEDIC qui, mise en cause pour produire des observations, n'a pas la qualité de partie dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société La nouvelle Aventure est rejetée.<br/>
Article 2 : La société La nouvelle Aventure versera à Pôle emploi une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de l'UNEDIC présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société La nouvelle Aventure, à Pôle emploi et à l'Union nationale interprofessionnelle pour  l'emploi dans l'industrie et le commerce.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
