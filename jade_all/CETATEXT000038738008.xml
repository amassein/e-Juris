<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038738008</ID>
<ANCIEN_ID>JG_L_2019_07_000000415009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/73/80/CETATEXT000038738008.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 08/07/2019, 415009, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415009.20190708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B..., néeA..., a demandé au tribunal administratif de Grenoble de condamner la commune des Houches à lui verser une somme de 15 000 euros en réparation du préjudice qu'elle estime avoir subi du fait du retard dans le versement de l'allocation d'aide au retour à l'emploi qui lui avait été illégalement refusé par une décision du maire de la commune du 12 août 2008. Par un jugement n° 1300850 du 6 octobre 2015, le tribunal a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 16LY00631 du 9 octobre 2017, enregistrée le 12 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi et le mémoire, enregistrés les 17 février et 14 novembre 2016 au greffe de cette cour, présentés par MmeB.... Par ce pourvoi, par ce mémoire et par un nouveau mémoire, enregistré au secrétariat de la section du contentieux le 3 janvier 2018, Mme B...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Grenoble ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner la commune des Houches à lui verser la somme de 15 000 euros assortie des intérêts au taux légal à compter de sa demande préalable et des intérêts capitalisés à chaque échéance annuelle ; <br/>
<br/>
              3°) de mettre à la charge de la commune des Houches la somme de 3 500 euros à verser à la SCP Matuchansky, Poupot, Valdelièvre, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les écritures de la commune des Houches, qui ont été présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, bien que l'intéressée ait été informée de l'obligation de recourir à ce ministère, doivent être écartées des débats.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme C...B..., néeA..., a été recrutée par la commune des Houches sous contrats successifs à durée déterminée, de juin 2001 à février 2003, puis en qualité de fonctionnaire stagiaire, de février 2003 à juillet 2004. Placée en congé de maladie puis en congé de maternité jusqu'en juillet 2005, et en congé parental jusqu'au 31 juillet 2007, elle a ensuite repris, pendant quelques mois, une activité professionnelle dans le secteur privé. Après avoir été involontairement privée d'emploi, elle a demandé, le 2 février 2008, à la commune des Houches le versement de l'aide au retour à l'emploi. Par décision du 12 août 2008, le maire de la commune des Houches a refusé de lui accorder le bénéfice de cette allocation. Par jugement du 6 mars 2012, devenu définitif, le tribunal administratif de Grenoble a annulé cette décision. La commune des Houches a exécuté ce jugement en versant à MmeB..., en mai 2012, le montant de l'aide au retour à l'emploi à laquelle elle pouvait prétendre pendant une durée de 700 jours, soit 23 219 euros. Mme B...a toutefois demandé à la commune de l'indemniser à hauteur de 15 000 euros en réparation du préjudice qu'elle estime avoir subi du fait de la décision illégale prise par la commune le 12 août 2008, qui a conduit à ne lui verser l'allocation qu'en 2012. Elle se pourvoit en cassation contre le jugement du 6 octobre 2015 par lequel le tribunal administratif de Grenoble a rejeté sa demande tendant à la condamnation de la commune des Houches à lui verser cette somme. <br/>
<br/>
              3. D'une part, il ressort des pièces du dossier soumis aux juges du fond, notamment du jugement du tribunal d'instance de Bonneville du 4 février 2009 statuant sur la demande de rétablissement personnel de l'intéressée, que les ressources de MmeB..., qui ne disposait d'aucun patrimoine immobilier ni bien mobilier de valeur et devait rembourser des dettes supérieures à 20 000 euros, s'élevaient alors à 1 237 euros par mois, composées pour l'essentiel de prestations familiales et de l'aide personnalisée au logement, auxquelles s'étaient ajoutées, en septembre et octobre 2008, une " aide de dépannage " de 700 euros de la caisse d'allocations familiales de la Haute-Savoie et une aide financière de l'aide sociale à l'enfance du département de la Haute-Savoie de 300 euros, alors qu'elle vivait seule avec trois enfants à sa charge et s'acquittait d'un loyer de 600 euros par mois. D'autre part, il ressort de ces mêmes pièces que le refus de lui accorder l'allocation d'aide au retour à l'emploi l'a privée illégalement de la somme de 995 euros par mois au titre de cette prestation, dont le versement n'aurait pu diminuer que de façon très limitée le montant des autres prestations dont elle bénéficiait. Dans ces conditions, en jugeant qu'il n'était pas établi que le refus fautif de la commune de lui verser l'allocation d'aide au retour à l'emploi, perçue de ce fait en 2012 seulement par l'intéressée, aurait aggravé la situation de MmeB..., au motif qu'elle connaissait déjà des difficultés financières et avait bénéficié de subsides de substitution, le tribunal administratif de Grenoble a dénaturé les pièces du dossier.<br/>
<br/>
              4. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. La décision par laquelle l'administration, rejetant une demande d'allocation, prive illégalement le demandeur d'une allocation à laquelle il avait droit est de nature à engager sa responsabilité, ou celle de l'administration pour le compte de laquelle l'allocation est versée, si elle lui a directement causé un préjudice. Si le défaut de versement de l'allocation sollicitée a vocation à être réparé par le versement de la somme due en exécution de l'annulation de la décision illégale de refus, contestée dans le délai de recours contentieux, et ne peut par suite faire l'objet de conclusions indemnitaires, en revanche, l'intéressé peut demander réparation du préjudice matériel distinct pouvant en résulter, tel que le préjudice résultant du retard dans la perception de l'allocation ou, le cas échéant, des troubles dans ses conditions d'existence. L'administration peut invoquer le fait du demandeur comme cause d'atténuation ou d'exonération de sa responsabilité.<br/>
<br/>
              7. Par un jugement du 6 mars 2012, devenu définitif, le tribunal administratif de Grenoble a annulé la décision du 12 août 2008 par laquelle le maire des Houches a refusé d'attribuer à Mme B...l'aide au retour à l'emploi, au motif qu'eu égard à la durée d'affiliation dont elle justifiait, elle remplissait les conditions ouvrant droit au bénéfice de cette allocation devant lui être versée par la commune. Par suite, la commune a commis une faute de nature à engager sa responsabilité à l'égard de MmeB....<br/>
<br/>
              8. Mme B...demande que la commune soit condamnée à lui verser la somme de 15 000 euros en réparation du préjudice financier et des troubles dans ses conditions d'existence qu'elle estime avoir subis du fait de cette faute, qui a conduit à ne lui verser l'aide au retour à l'emploi qu'en 2012, en une seule fois, au lieu de la lui verser mensuellement de 2008 à 2010.<br/>
<br/>
              9. En premier lieu, Mme B...ne peut se prévaloir utilement dans le présent contentieux indemnitaire, ainsi qu'il a été dit au point 6, du préjudice matériel résultant du seul défaut de versement de l'allocation d'aide au retour à l'emploi à laquelle elle avait droit ou des intérêts sur la somme correspondante. Elle peut en revanche utilement faire valoir que l'octroi de la somme de 23 219 euros en un seul versement en mai 2012 lui a fait perdre en 2014 son droit à l'aide personnalisée au logement calculé sur la base des ressources de l'année 2012. Il résulte néanmoins de l'instruction que la somme dont Mme B...a été ainsi privée, de l'ordre de 3 300 euros, n'excède pas le montant du surcroît de prestations sociales perçues du fait du défaut de versement en 2008 et 2009 de l'aide au retour à l'emploi, au titre, d'une part, du revenu minimum d'insertion et de secours exceptionnels au cours des mêmes années et, d'autre part, d'un montant plus élevé d'aide personnalisée au logement en 2010. Par suite, Mme B...n'est pas fondée à soutenir que le refus illégal d'octroi de l'aide au retour à l'emploi lui aurait causé un préjudice financier.<br/>
<br/>
              10. En second lieu, il résulte de l'instruction que Mme B...a eu pour seules ressources en 2008 une pension alimentaire de 180 euros et des prestations sociales inférieures d'environ 900 euros par mois à celles qu'elle aurait perçues si l'aide au retour à l'emploi lui avait été versée. Dans ces conditions, et eu égard aux éléments mentionnés au point 3, MmeB..., qui vivait seule avec trois enfants à charge, est fondée à soutenir que son foyer a connu en 2008, du fait du défaut de versement mensuel de l'aide au retour à l'emploi, une situation financière difficile engendrant des troubles dans ses conditions d'existence. Il sera fait une juste appréciation de ce préjudice en condamnant la commune des Houches, qui n'a aucunement cherché à nuire à l'intéressée contrairement à ce que celle-ci soutient, à lui verser une somme de 2 000 euros tous intérêts compris au jour de la présente décision. En revanche, si Mme B...invoque un préjudice lié à l'atteinte à sa réputation, elle ne l'établit pas.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune des Houches une somme de 1 000 euros à verser à MmeB..., sur le fondement de l'article L. 761-1 du code de justice administrative, au titre des frais qu'elle a exposés en première instance. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions de la commune des Houches présentées au même titre devant le tribunal administratif. Enfin, Mme B...a obtenu le bénéfice de l'aide juridictionnelle devant le Conseil d'Etat. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Matuchansky, Poupot, Valdelièvre, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la commune des Houches une somme de 1 500 euros à verser à cette société. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 6 octobre 2015 est annulé.<br/>
Article 2 : La commune des Houches est condamnée à verser à Mme B...une somme de 2 000 euros tous intérêts compris au jour de la présente décision. <br/>
Article 3 : La commune des Houches versera une somme de 1 000 euros à Mme B...au titre de l'article L. 761-1 du code de justice administrative et une somme de 1 500 euros à la SCP Matuchansky, Poupot, Valdelièvre, son avocat, en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Le surplus des conclusions de la demande de Mme B...et les conclusions de la commune des Houches présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à Mme C...B..., néeA..., et à la commune des Houches. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
