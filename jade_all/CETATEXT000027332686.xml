<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027332686</ID>
<ANCIEN_ID>JG_L_2013_04_000000346309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/33/26/CETATEXT000027332686.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 17/04/2013, 346309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2013:346309.20130417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er février et 28 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n°10PA05598 du 14 décembre 2010 par laquelle le président de la deuxième chambre de la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1005446 du tribunal administratif de Paris du 2 novembre 2010 rejetant sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du préfet de police du 19 février 2010 lui refusant le renouvellement de son titre de séjour, l'obligeant à quitter le territoire français dans un délai d'un mois et fixant son pays de destination et, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté et à ce qu'il soit enjoint au préfet de lui délivrer un titre de séjour dans un délai d'un mois suivant la notification de l'arrêt à venir sous peine d'astreinte à hauteur de 100 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Ortscheidt, avocat de MmeA...,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ortscheidt, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu de l'article L. 313-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, la carte de séjour temporaire peut être retirée à tout employeur titulaire de cette carte qui commet une infraction à l'article L. 341-6 du code du travail ; qu'aux termes de cet article L. 341-6 du code du travail, alors en vigueur : " Nul ne peut, directement ou par personne interposée, engager, conserver à son service ou employer pour quelque durée que ce soit un étranger non muni du titre l'autorisant à exercer une activité salariée en France (...) " ; qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1° Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2° Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ;<br/>
<br/>
              2. Considérant que pour rejeter l'appel formé par Mme A...contre le jugement du tribunal administratif de Paris du 2 novembre 2010 rejetant sa demande tendant à l'annulation pour excès de pouvoir du refus de renouvellement de titre de séjour que lui a opposé le préfet de police le 19 février 2010, le président de la deuxième chambre de la cour administrative d'appel de Paris a notamment écarté, en reprenant les motifs retenus par le jugement attaqué, le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle de l'atelier de confection qu'elle dirigeait, Mme A...a été condamnée à deux mois d'emprisonnement avec sursis pour emploi d'étrangers en situation irrégulière ; que si elle pouvait, à ce titre, faire l'objet, sur le fondement de l'article L. 313-5 du code de l'entrée et du séjour des étrangers et du droit d'asile mentionné ci-dessus, d'un retrait de la carte de séjour temporaire qui lui avait été délivrée, il ressort des pièces du dossier soumis aux juges du fond que l'intéressée était en situation régulière, vivait en France depuis 1997 avec son époux, également titulaire d'un titre de séjour, avec lequel elle avait eu trois enfants nés en France en 1999, 2001 et 2009 et scolarisés pour deux d'entre eux ; qu'eu égard à cette situation familiale ainsi qu'à la durée et aux conditions du séjour de l'intéressée sur le territoire national, l'arrêté du préfet de police lui refusant le renouvellement de sa carte de séjour a porté au respect de sa vie privée et familiale une atteinte disproportionnée par rapport aux buts en vue desquels il a été pris ; qu'ainsi, le président de la deuxième chambre de la cour a inexactement qualifié les faits de l'espèce en jugeant que cet arrêté n'avait pas porté au respect de la vie privée et familiale de Mme A...une atteinte disproportionnée ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à demander l'annulation de l'ordonnance attaquée ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que Mme A...est fondée à soutenir que c'est à tort que le tribunal administratif de Paris a jugé que l'arrêté attaqué n'avait pas porté à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des buts en vue desquels il a été pris ; qu'ainsi, Mme A...est fondée à demander l'annulation du jugement du tribunal administratif de Paris du 2 novembre 2010 en tant qu'il la concerne ainsi que l'annulation pour excès de pouvoir de l'arrêté du préfet de police du 19 février 2010 lui refusant le renouvellement de son titre de séjour et l'obligeant à quitter le territoire français ;<br/>
<br/>
              5. Considérant qu'en raison du motif qui la fonde, l'annulation de l'arrêté attaqué implique nécessairement, compte tenu de l'absence de changements de circonstances de droit ou de fait y faisant obstacle, qu'un titre de séjour portant la mention " vie privée et familiale " soit délivré à la requérante sur le fondement de l'article L. 911-1 du code de justice administrative  ; qu'il y a lieu d'enjoindre au préfet de police de délivrer ce titre à Mme A...dans un délai d'un mois à compter de la notification de cette décision ; qu'il n'y a pas lieu, en revanche, d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du président de la deuxième chambre de la cour administrative d'appel de Paris du 14 décembre 2010 est annulée. <br/>
Article 2 : Le jugement du tribunal administratif de Paris du 2 novembre 2010 est annulé en tant qu'il rejette la demande de MmeA....<br/>
Article 3 : L'arrêté du préfet de police du 19 février 2010 est annulé.<br/>
Article 4 : Il est enjoint au préfet de police de délivrer à Mme A...un titre de séjour portant la mention " vie privée et familiale " dans un délai d'un mois à compter de la notification de la présente décision.<br/>
Article 5 : L'Etat versera à Mme A...une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à Mme B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
