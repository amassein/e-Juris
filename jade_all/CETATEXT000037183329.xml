<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037183329</ID>
<ANCIEN_ID>JG_L_2018_07_000000389902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/18/33/CETATEXT000037183329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 11/07/2018, 389902, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:389902.20180711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 30 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. K...O..., Mme J...O..., M. B...F..., Mme C...F..., M. B...-C...F..., M. B...D..., M. H...G..., M. I...P..., M. E...N..., M. B...-E...N..., M. A...L...et Me M...O...demandent au Conseil d'Etat :<br/>
<br/>
              1°) de constater l'illégalité de l'article 91 de la loi sur les finances du 28 avril 1816, de l'ordonnance du 10 septembre 1817, des articles 4 et 5 de la loi n° 71-1130 du 31 décembre 1971, l'article 12 de la loi du 24 mai 1872 relative au Tribunal des conflits, des décrets n° 88-814 du 12 juillet 1988, n° 91-1125 du 28 octobre 1991, n° 78-380 du 15 mars 1978, des articles 973 et suivants du code de procédure civile, des articles R. 431-1, R. 821-3 et R. 834-3 du code de justice administrative, de l'article 17 du décret du 26 octobre 1849 portant règlement d'administration publique déterminant les formes de procédure du tribunal des conflits, ainsi que tous les règlements qui feraient application de ces textes ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir les articles 5, 32, 34, 37, 38 et 50 du décret n° 2015-233 du 27 février 2015 relatif au Tribunal des conflits et aux questions préjudicielles ;<br/>
<br/>
              3°) d'enjoindre au Premier ministre, dans le délai d'un mois à compter de la décision à intervenir, et sous astreinte de 1 000 euros par jour de retard, d'édicter des mesures transitoires relatives aux instances en cours devant le Tribunal des conflits à la date de l'entrée en vigueur du décret du 27 février 2015 ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat une somme de 6 000 euros à verser à chacun des requérants au titre des dispositions de l'article L 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi sur les finances du 28 avril 1816, notamment son article 91 ;<br/>
              - l'ordonnance du 10 septembre 1817 qui réunit, sous la dénomination d'ordre des avocats au Conseil d'Etat et à la Cour de cassation, l'ordre des avocats aux conseils et le collège des avocats à la Cour de cassation, fixe irrévocablement le nombre des titulaires et contient des dispositions pour la discipline intérieure de l'ordre ;<br/>
              - la loi n° 71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires ; <br/>
              - la loi n° 2015-177 du 16 février 2015 relative à la modernisation et à la simplification du droit et des procédures dans les domaines de la justice et des affaires intérieures, notamment son article 13 ;<br/>
              - la décision du 22 juillet 2015 par laquelle le Conseil d'Etat statuant au Contentieux a décidé de ne pas renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. O...et autres ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions tendant à l'annulation de dispositions du décret du 27 février 2015 : <br/>
<br/>
              1. En premier lieu, si l'article 34 de la Constitution réserve au législateur le soin de fixer " les règles concernant : / (...) / la procédure pénale (...) / la création de nouveaux ordres de juridiction (...) ", les dispositions relatives à la procédure à suivre devant les juridictions relèvent de la compétence réglementaire dès lors qu'elles ne concernent pas la procédure pénale et qu'elles ne mettent en cause aucune des matières réservées au législateur par l'article 34 ou d'autres dispositions constitutionnelles. Il suit de là que, contrairement à ce qui est soutenu, le Premier ministre est compétent pour réserver, ainsi qu'il l'a fait à l'article 5 du décret attaqué, aux seuls avocats au Conseil d'Etat et à la Cour de cassation la représentation des parties devant le Tribunal des conflits.<br/>
<br/>
              2. En deuxième lieu, l'obligation de représentation par un avocat au Conseil d'Etat et à la Cour de cassation, prévue à l'article 5 du décret attaqué, a pour objet tant d'assurer aux justiciables la qualité de leur défense que de concourir à une bonne administration de la justice en imposant le recours à des mandataires professionnels spécialisés, offrant des garanties de compétence particulières, compte tenu de la spécificité du Tribunal des conflits. Eu égard à l'institution par le législateur d'un dispositif d'aide juridictionnelle, cette obligation ne saurait, en tout état de cause, être regardée, contrairement à ce qui est soutenu, comme portant atteinte au droit constitutionnel des justiciables d'exercer un recours effectif devant une juridiction.<br/>
<br/>
              3. En troisième lieu, l'article 5 de la directive 98/5 du Parlement européen et du Conseil du 16 février 1998 visant à faciliter l'exercice permanent de la profession d'avocat dans un Etat membre autre que celui où la qualification a été acquise prévoit que " dans le but d'assurer le bon fonctionnement de la justice, les Etats membres peuvent établir des règles spécifiques d'accès aux cours suprêmes, tel le recours à des avocats spécialisés ". Par suite, eu égard au rôle du Tribunal des conflits, dont les décisions, aux termes de l'article 11 de la loi du 24 mai 1872 relative au Tribunal des conflits, " s'imposent à toutes les juridictions de l'ordre judiciaire et de l'ordre administratif ", le moyen tiré de ce que l'institution d'un monopole de représentation des parties devant le Tribunal des conflits au profit des avocats au Conseil d'Etat et à la Cour de cassation méconnaîtrait la directive précitée doit être écarté. <br/>
<br/>
              4. En quatrième lieu, les dispositions du décret attaqué n'ont ni pour objet ni pour effet de prévoir une limitation du nombre des avocats au Conseil d'Etat et à la Cour de cassation. Par suite, le moyen tiré de ce que le numérus clausus des avocats au Conseil d'Etat et à la Cour de cassation serait contraire au droit de l'Union européenne ne peut qu'être écarté. <br/>
<br/>
              5. En cinquième lieu, contrairement à ce qui est soutenu, il résulte des dispositions du décret attaqué, et en particulier de ses articles 32, 34, 37 et 38, que dans tous les cas de conflits d'attribution entre la juridiction administrative et la juridiction judiciaire, il appartient au Tribunal des conflits, saisi soit par l'une des juridictions, soit par l'une des parties intéressées, de désigner la juridiction compétente pour connaître du litige en cause. Par suite, le moyen tiré de ce que les dispositions des articles 32, 34, 37 et 38 du décret attaqué méconnaîtraient notamment le droit à un recours effectif, en ce qu'elles rendraient possible une situation où aucune juridiction ne prendrait en charge un litige, manque en fait.<br/>
<br/>
              6. Enfin, aux termes de l'article 50 du décret attaqué : " La date prévue au III de l'article 13 de la loi n° 2015-177 du 16 février 2015 susvisée pour l'entrée en vigueur des dispositions relatives au Tribunal des conflits est fixée au 1er avril 2015. Le présent décret entre en vigueur à la même date. / S'agissant des conflits positifs, les dispositions de l'article 13 de la loi n° 2015-177 du 16 février 2015 susvisée et celles du présent décret sont applicables aux procédures donnant lieu à un déclinatoire de compétence présenté à compter du 1er avril 2015. / Les dispositions du titre II du présent décret sont applicables aux jugements rendus à compter de la même date. " Contrairement à ce qui est soutenu, cet article prévoit des mesures transitoires applicables aux instances en cours devant le Tribunal des conflits. Par suite, le moyen tiré de l'absence de mesures transitoires applicables aux instances en cours devant le Tribunal des conflits ne peut qu'être écarté.<br/>
<br/>
              Sur les autres conclusions de la requête :<br/>
<br/>
              7. Le juge de l'excès de pouvoir ne pouvant être saisi que de conclusions tendant à l'annulation d'un acte ou d'une décision administratif, les conclusions des requérants tendant à ce que le Conseil d'Etat " constate l'illégalité " des diverses dispositions législatives et règlementaires qu'ils énumèrent ne peuvent, en tout état de cause, qu'être rejetées. <br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il y ait lieu de soumettre à la Cour de justice de l'Union européenne les questions préjudicielles soulevées dans le mémoire du requérant et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de la justice tiré du défaut d'intérêt pour agir des requérants, la requête de M. O...et autres doit être rejetée, y compris ses conclusions aux fins d'injonction, ainsi que celles tendant à l'application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. O...et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. K...O..., premier dénommé, pour l'ensemble des requérants, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
