<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028686263</ID>
<ANCIEN_ID>JG_L_2014_02_000000375618</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/62/CETATEXT000028686263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 28/02/2014, 375618, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375618</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:375618.20140228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme B...A..., élisant domicile ...BP 77412, à Lyon Cedex 07 (69347) ; la requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1400855 du 14 février 2014 par laquelle le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet du Rhône, d'une part, de procéder à l'enregistrement de sa demande d'asile et de lui remettre le dossier nécessaire aux fins la transmission de sa demande à l'Office français de protection des réfugiés et apatrides dans le cadre de la procédure normale, et, d'autre part, de lui délivrer une autorisation provisoire de séjour dans un délai de 48 heures à compter de l'ordonnance et sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
<br/>
              elle soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que le préfet a pris un arrêté de réadmission vers la Lituanie où elle est susceptible de faire l'objet de discriminations en raison de son orientation sexuelle ;<br/>
              - le préfet du Rhône a porté une atteinte grave et manifestement illégale au droit d'asile en reportant le délai de transfert à 18 mois, en refusant d'enregistrer sa demande d'asile et de lui délivrer une autorisation provisoire de séjour ;<br/>
<br/>
<br/>
                          Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 25 février 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; <br/>
              il soutient que :<br/>
              - la condition d'urgence n'est pas remplie ;<br/>
              - c'est à bon droit que le préfet a prolongé à 18 mois le délai de réadmission, dès lors que la requérante s'est soustraite au contrôle de l'autorité administrative ;<br/>
              - la requérante n'apporte pas d'éléments crédibles attestant qu'elle risque de subir des discriminations dans le traitement de sa demande d'asile en Lituanie en raison de son orientation sexuelle;<br/>
              - elle n'a pas droit à obtenir une autorisation provisoire de séjour, dès lors que sa demande relève de la responsabilité de la Lituanie ;<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement (CE) n° 343/2003 du Conseil du 18 février 2013 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 26 février 2014 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme A...;<br/>
<br/>
              - le représentant du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que, s 'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le 1° de cet article permet de refuser l'admission en France d'un demandeur d'asile, lorsque l'examen de la demande d'asile relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n°343/2003 du Conseil du 18 février 2003 ; que l'article 19 de ce règlement prévoit que le transfert du demandeur d'asile vers le pays de réadmission doit se faire dans les six mois à compter de l'acceptation de la demande de prise en charge et que ce délai peut être porté à dix-huit mois si l'intéressé " prend la fuite " ; que la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger non admis au séjour se sera soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que MmeA..., de nationalité russe, entrée en France le 18 avril 2013, a déposé le 25 avril une demande d'asile ; que le préfet du Rhône, informé de ce qu'elle était bénéficiaire d'un visa Schengen délivré par les autorités lituaniennes, a sollicité de ces autorités la prise en charge de l'intéressée, sur le fondement des dispositions des articles 9 et 17 du règlement n°343/2003 du 18 février 2003 et après avoir avisé le 30 avril 2013 Mme A...de cette démarche par une information rédigée en langue russe ; qu'au vu de l'accord donné par la Lituanie le 26 juin 2013, le préfet du Rhône a, par une décision du 3 juillet 2013, prise sur le fondement de l'article 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, refusé d'admettre Mme A...au séjour ; qu'après que Mme A...par lettre du 24 juillet a fait connaître son refus de rejoindre la Lituanie, le préfet du Rhône par une décision du 23 août 2013 a confirmé sa décision du 3 juillet et pris le 16 septembre un arrêté prescrivant la remise de l'intéressée aux autorités lituaniennes ; qu'il a, le 10 octobre, invité l'intéressée à se présenter en préfecture en vue de l'exécution d'office de la mesure de réadmission ; que Mme A...a cependant sollicité à nouveau le 31 décembre 2013 son admission provisoire au titre de l'asile, arguant de ce que les autorités françaises seraient devenues responsables du traitement de sa demande en raison de l'expiration du délai de six mois ; qu'elle relève appel de l'ordonnance par laquelle le juge des référés du tribunal administratif de Lyon a, sur le fondement de l'article L. 521-2 du code de justice administrative, rejeté sa demande tendant à ce qu'il soit enjoint au préfet du Rhône d'enregistrer sa nouvelle demande d'asile et de lui délivrer une autorisation provisoire de séjour ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces produites devant le juge des référés de première instance que Mme A...n'a pris aucune disposition pour se conformer ni à la décision préfectorale du 8 juillet 2013 confirmée le 23 août 2013, ni à celle du 16 septembre 2013 portant réadmission vers la Lituanie ; qu'elle a explicitement annoncé, par un courrier du 24 juillet 2014, son intention de se soustraire à la mesure d'éloignement ; qu'elle n'a pas communiqué son adresse d'hébergement aux services préfectoraux malgré les demandes formulées en ce sens par le préfet du Rhône dans ses lettres du 3 juillet, du 16 septembre et du 10 octobre 2013 ; qu'elle n'a pas davantage déféré à la convocation qui lui a été adressée par le préfet du Rhône le 10 octobre 2013 ; qu'elle avait été informée à plusieurs reprises qu'à défaut pour elle de déférer à l'arrêté du 16 septembre 2013, le délai pour procéder à son transfert en Lituanie serait porté à 18 mois ; que, dès lors, et ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Lyon, le préfet du Rhône n'a pas commis d'illégalité grave et manifeste en portant le délai de réadmission à 18 mois ;<br/>
<br/>
              5. Considérant que MmeA..., pour établir que sa demande d'asile ferait l'objet en Lituanie, qui est un Etat membre de l'Union européenne et partie tant à la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, complétée par le protocole de New York, qu'à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, d'un traitement contraire aux exigences de ces conventions, ne produit aucun autre document que ceux sur l'homophobie de la population lituanienne, déjà écartés à bon droit  par le juge des référés de première instance comme n'établissant pas que sa demande d'asile pourrait faire l'objet en Lituanie d'un traitement contraire aux exigences des conventions internationales auxquelles cet Etat est partie ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il y ait lieu de se prononcer sur la condition d'urgence, que Mme A...n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, qui est suffisamment motivée, le juge des référés du tribunal administratif de Lyon a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative ; qu'il y a lieu, par suite, et sans qu'il y ait lieu d'admettre Mme A... au bénéfice de l'aide juridictionnelle, de rejeter sa requête, y compris ses conclusions tendant à l'application des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
 Article 1 : Mme A...n'est pas admise au bénéfice de l'aide juridictionnelle à titre provisoire.<br/>
<br/>
 Article 2 : La requête de Mme A...est rejetée. <br/>
<br/>
 Article 3 : La présente ordonnance sera notifiée à Mme B...A...et au ministre de   l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
