<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027300322</ID>
<ANCIEN_ID>JG_L_2013_04_000000362009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/30/03/CETATEXT000027300322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/04/2013, 362009, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:362009.20130412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 11NC00946 du 5 avril 2012, enregistré le 20 août 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative d'appel de Nancy, avant de statuer sur la requête de la caisse primaire d'assurance maladie de l'Aube et de la caisse primaire d'assurance maladie de la Haute-Marne tendant, d'une part, à l'annulation du jugement n° 0901124 du 14 avril 2011 du tribunal administratif de Châlons-en-Champagne en tant qu'il a rejeté les conclusions de la caisse primaire d'assurance maladie de la Haute-Marne tendant à la condamnation du centre hospitalier de Troyes à lui payer une somme de 163 280,85 euros assortie des intérêts légaux en remboursement de ses débours et une somme de 980 euros au titre des frais de gestion, d'autre part, au prononcé de cette condamnation, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question suivante : <br/>
<br/>
              Les conventions de mutualisation conclues entre différentes caisses d'assurance maladie, qui constituent des mandats de gestion, permettent-elles à la caisse désignée comme gestionnaire d'agir au contentieux et de rechercher la responsabilité du tiers responsable sur le fondement des dispositions de l'article L. 376-1 du code de la sécurité sociale en lieu et place de la caisse d'affiliation désignée par ce texte ' <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu la loi n° 2004-810 du 13 août 2004 ;<br/>
<br/>
              Vu la loi n° 2006-1640 du 21 décembre 2006 ;<br/>
<br/>
              Vu la loi n° 2010-1594 du 20 décembre 2010 ;<br/>
<br/>
              Vu la loi n° 2012-1404 du 17 décembre 2012 ;<br/>
	Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes, <br/>
              - les observations de Me Le Prado, avocat du centre hospitalier général de Troyes et de Me Foussard, avocat de la caisse nationale de l'assurance maladie des travailleurs salariés,<br/>
<br/>
- les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              - La parole ayant été à nouveau donnée à Me Le Prado, avocat du centre hospitalier général de Troyes et à Me Foussard, avocat de la caisse nationale de l'assurance maladie des travailleurs salariés ;<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
              Sur les dispositions législatives applicables :<br/>
<br/>
              1. L'article L. 376-1 du code de la sécurité sociale dispose que : " Lorsque, sans entrer dans les cas régis par les dispositions législatives applicables aux accidents du travail, la lésion dont l'assuré social ou son ayant droit est atteint est imputable à un tiers, l'assuré ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé, conformément aux règles du droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre. / Les caisses de sécurité sociale sont tenues de servir à l'assuré ou à ses ayants droit les prestations prévues par le présent livre, sauf recours de leur part contre l'auteur responsable de l'accident dans les conditions ci-après. / Les recours subrogatoires des caisses contre les tiers s'exercent poste par poste sur les seules indemnités qui réparent des préjudices qu'elles ont pris en charge, à l'exclusion des préjudices à caractère personnel (...) ". Il résulte de l'article L. 122-1 du même code que c'est en principe le directeur de la caisse primaire d'assurance maladie qui décide des actions en justice dirigées contre les tiers responsables de dommages causés à l'assuré social affilié à la caisse et qui représente alors celle-ci en justice. Le directeur peut donner mandat à cet effet à certains agents de la caisse ou à un agent d'un autre organisme de sécurité sociale. <br/>
<br/>
              2. Toutefois, l'article L. 221-3-1 du code de la sécurité sociale, issu de la loi du 13 août 2004 relative à l'assurance maladie et modifié par la loi du 21 décembre 2006 de financement de la sécurité sociale pour 2007, dispose que le directeur général de la caisse nationale de l'assurance maladie des travailleurs salariés " est notamment chargé pour ce qui concerne la gestion de la caisse nationale et du réseau des caisses régionales, locales et de leurs groupements : (...) 3° De prendre les mesures nécessaires à l'organisation et au pilotage du réseau des caisses du régime général ; il peut notamment définir les circonscriptions d'intervention des organismes locaux, prendre les décisions prévues aux articles L. 224-11, L. 224-12, L. 224-13 et L. 281-2, et confier à certains organismes, à l'échelon national, interrégional, régional ou départemental, la charge d'assumer certaines missions, notamment celles mentionnées au II de l'article L. 216-2-1 ". <br/>
<br/>
              3. L'article L. 216-2-1 du code de la sécurité sociale, dans sa rédaction initiale issue de la loi du 21 décembre 2006 de financement de la sécurité sociale pour 2007, a prévu à son I que les conseils d'administration de la caisse nationale d'assurance vieillesse des travailleurs salariés, de la caisse nationale des allocations familiales et de l'agence centrale des organismes de sécurité sociale " définissent les orientations relatives à l'organisation du réseau des organismes relevant de la branche concernée. / Pour l'application de ces orientations, le directeur de l'organisme national peut confier à un ou plusieurs organismes de la branche la réalisation de missions ou d'activités relatives à la gestion des organismes, au service des prestations et au recouvrement. / Les modalités de mise en oeuvre sont fixées par convention établie entre l'organisme national et les organismes locaux ou régionaux. Les directeurs signent la convention, après avis des conseils d'administration des organismes locaux ou régionaux concernés ". Aux termes du II du même article : " Pour les missions liées au service des prestations, l'organisme désigné peut, pour le compte des autres organismes locaux ou régionaux, participer à l'accueil et à l'information des bénéficiaires, servir des prestations, procéder à des vérifications et enquêtes administratives concernant leur attribution et exercer les poursuites contentieuses afférentes à ces opérations. Il peut également, pour ces mêmes missions, se voir attribuer certaines compétences d'autres organismes locaux ou régionaux ". L'article 39 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011 a complété la première phrase du troisième alinéa du I, pour prévoir que les modalités de mise en oeuvre seraient désormais fixées par convention établie entre l'organisme national et les organismes locaux ou régionaux " sauf en ce qui concerne le traitement des litiges et des contentieux y afférents ainsi que de leurs suites qui sont précisés par décret ". Enfin, l'article 68 de la loi du 17 décembre 2012 de financement de la sécurité sociale pour 2013 a complété la première phrase du II pour préciser qu'au titre de l'exercice des poursuites contentieuses prévues par ces dispositions, l'organisme peut " notamment agir en demande et en défense devant les juridictions ".<br/>
<br/>
              Sur la portée de ces dispositions pour les branches vieillesse et famille et pour l'activité de recouvrement : <br/>
<br/>
              4. Il résulte des dispositions législatives précitées, éclairées par les travaux parlementaires qui ont précédé leur adoption, que le législateur a entendu permettre qu'un organisme de sécurité sociale du régime général puisse agir en justice pour le compte d'un autre organisme de la même branche, lorsque le contentieux est lié au service des prestations. La loi du 17 décembre 2012 s'est bornée à apporter une clarification sur ce point. Les modalités de mise en oeuvre de ce mécanisme doivent être fixées, depuis l'intervention de la loi du 20 décembre 2010, par décret.<br/>
<br/>
              Sur la portée de ces dispositions pour les branches maladie et accidents du travail :<br/>
<br/>
              5. Les dispositions combinées de l'article L. 221-3-1 et du II de l'article L. 216-2-1 du code de la sécurité sociale permettent au directeur général de la caisse nationale de l'assurance maladie des travailleurs salariés de confier à une caisse primaire la charge d'agir en justice pour le compte de la caisse d'affiliation de l'assuré dans tous les contentieux liés au service des prestations d'assurance maladie. A ce titre, une caisse peut se voir confier la mission d'exercer, pour le compte d'une ou plusieurs autres caisses, le recours subrogatoire prévu par les dispositions de l'article L. 376-1 du même code à l'encontre du tiers responsable de l'accident, un tel recours tendant au remboursement des prestations servies à l'assuré à la suite de l'accident. La décision prise en ce sens par le directeur général de la caisse nationale de l'assurance maladie des travailleurs salariés peut, le cas échéant, être formalisée dans un document, signé également par les caisses locales concernées, qui détermine les modalités concrètes de sa mise en oeuvre. <br/>
<br/>
              6. Le présent avis sera notifié à la cour administrative d'appel de Nancy, à la caisse primaire d'assurance maladie de l'Aube, à la caisse primaire d'assurance maladie de la Haute-Marne, au centre hospitalier général de Troyes, à la caisse nationale de l'assurance maladie des travailleurs salariés et à la ministre des affaires sociales et de la santé. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">62-01-01-01-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. RÉGIME DE SALARIÉS. RÉGIME GÉNÉRAL. ASSURANCE MALADIE. - POSSIBILITÉ POUR LE DG DE LA CNAM DE CONFIER À UNE CAISSE PRIMAIRE LA CHARGE D'AGIR EN JUSTICE POUR LE COMPTE DE LA CAISSE D'AFFILIATION DE L'ASSURÉ (ART. L. 221-1-1 ET II DE L'ART. L. 216-2-1 DU CSS) - EXISTENCE - POSSIBILITÉ D'EXERCER DANS CE CADRE UN RECOURS SUBROGATOIRE AU NOM D'UNE AUTRE CAISSE - EXISTENCE - FORMALISATION DANS UN DOCUMENT SIGNÉ PAR LES CAISSES LOCALES CONCERNÉES - FACULTÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-01-01-01-01-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. RÉGIME DE SALARIÉS. RÉGIME GÉNÉRAL. ASSURANCE MALADIE. CAISSE NATIONALE. - POSSIBILITÉ POUR LE DG DE LA CNAM DE CONFIER À UNE CAISSE PRIMAIRE LA CHARGE D'AGIR EN JUSTICE POUR LE COMPTE DE LA CAISSE D'AFFILIATION DE L'ASSURÉ (ART. L. 221-1-1 ET II DE L'ART. L. 216-2-1 DU CSS) - EXISTENCE - POSSIBILITÉ D'EXERCER DANS CE CADRE UN RECOURS SUBROGATOIRE AU NOM D'UNE AUTRE CAISSE - EXISTENCE - FORMALISATION DANS UN DOCUMENT SIGNÉ PAR LES CAISSES LOCALES CONCERNÉES - FACULTÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-01-01-01-01-03 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. RÉGIME DE SALARIÉS. RÉGIME GÉNÉRAL. ASSURANCE MALADIE. CAISSES PRIMAIRES. - POSSIBILITÉ POUR LE DG DE LA CNAM DE CONFIER À UNE CAISSE PRIMAIRE LA CHARGE D'AGIR EN JUSTICE POUR LE COMPTE DE LA CAISSE D'AFFILIATION DE L'ASSURÉ (ART. L. 221-1-1 ET II DE L'ART. L. 216-2-1 DU CSS) - EXISTENCE - POSSIBILITÉ D'EXERCER DANS CE CADRE UN RECOURS SUBROGATOIRE AU NOM D'UNE AUTRE CAISSE - EXISTENCE - FORMALISATION DANS UN DOCUMENT SIGNÉ PAR LES CAISSES LOCALES CONCERNÉES - FACULTÉ.
</SCT>
<ANA ID="9A"> 62-01-01-01-01 Les dispositions combinées de l'article L. 221-3-1 et du II de l'article L. 216-2-1 du code de la sécurité sociale (CSS) permettent au directeur général (DG) de la caisse nationale de l'assurance maladie des travailleurs salariés (CNAM) de confier à une caisse primaire la charge d'agir en justice pour le compte de la caisse d'affiliation de l'assuré dans tous les contentieux liés au service des prestations d'assurance maladie. A ce titre, une caisse peut se voir confier la mission d'exercer, pour le compte d'une ou plusieurs autres caisses, le recours subrogatoire prévu par les dispositions de l'article L. 376-1 du même code à l'encontre du tiers responsable de l'accident, un tel recours tendant au remboursement des prestations servies à l'assuré à la suite de l'accident. La décision prise en ce sens par le DG de la CNAM peut, le cas échéant, être formalisée dans un document, signé également par les caisses locales concernées, qui détermine les modalités concrètes de sa mise en oeuvre.</ANA>
<ANA ID="9B"> 62-01-01-01-01-01 Les dispositions combinées de l'article L. 221-3-1 et du II de l'article L. 216-2-1 du code de la sécurité sociale (CSS) permettent au directeur général (DG) de la caisse nationale de l'assurance maladie des travailleurs salariés (CNAM) de confier à une caisse primaire la charge d'agir en justice pour le compte de la caisse d'affiliation de l'assuré dans tous les contentieux liés au service des prestations d'assurance maladie. A ce titre, une caisse peut se voir confier la mission d'exercer, pour le compte d'une ou plusieurs autres caisses, le recours subrogatoire prévu par les dispositions de l'article L. 376-1 du même code à l'encontre du tiers responsable de l'accident, un tel recours tendant au remboursement des prestations servies à l'assuré à la suite de l'accident. La décision prise en ce sens par le DG de la CNAM peut, le cas échéant, être formalisée dans un document, signé également par les caisses locales concernées, qui détermine les modalités concrètes de sa mise en oeuvre.</ANA>
<ANA ID="9C"> 62-01-01-01-01-03 Les dispositions combinées de l'article L. 221-3-1 et du II de l'article L. 216-2-1 du code de la sécurité sociale (CSS) permettent au directeur général (DG) de la caisse nationale de l'assurance maladie des travailleurs salariés (CNAM) de confier à une caisse primaire la charge d'agir en justice pour le compte de la caisse d'affiliation de l'assuré dans tous les contentieux liés au service des prestations d'assurance maladie. A ce titre, une caisse peut se voir confier la mission d'exercer, pour le compte d'une ou plusieurs autres caisses, le recours subrogatoire prévu par les dispositions de l'article L. 376-1 du même code à l'encontre du tiers responsable de l'accident, un tel recours tendant au remboursement des prestations servies à l'assuré à la suite de l'accident. La décision prise en ce sens par le DG de la CNAM peut, le cas échéant, être formalisée dans un document, signé également par les caisses locales concernées, qui détermine les modalités concrètes de sa mise en oeuvre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
