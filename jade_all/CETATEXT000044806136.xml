<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806136</ID>
<ANCIEN_ID>JG_L_2021_12_000000436058</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806136.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 436058, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436058</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436058.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Mediapost a demandé au tribunal administratif d'Orléans d'annuler la décision implicite par laquelle la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté son recours hiérarchique formé contre la décision de l'inspectrice du travail de la 18ème section de l'unité territoriale d'Indre-et-Loire de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Centre-Val de Loire du 11 décembre 2015 refusant d'autoriser le licenciement de M. B... C.... Par un jugement n° 1602593 du 14 juin 2018, le tribunal administratif d'Orléans a annulé les décisions de l'inspectrice du travail et de la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>
              Par un arrêt n° 18NT03031 du 17 septembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. C... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un pourvoi sommaire rectificatif et un mémoire complémentaire, enregistrés les 18 et 26 novembre 2019 et le 18 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Mediapost la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Meier-Bourdeau, Lecuyer et associés, avocat de M. C... et à la SCP Célice, Texidor, Perier, avocat de la société Mediapost ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... a été recruté en qualité de distributeur de journaux et prospectus par la société Mediapost. Son contrat de travail stipule qu'il doit utiliser son véhicule personnel pour les besoins de son activité professionnelle et que l'impossibilité d'utiliser son véhicule personnel est susceptible de constituer un motif réel et sérieux justifiant la rupture du contrat de travail. En février 2015, M. C... a été victime, dans le cadre de son travail, d'un accident de la circulation ayant entraîné l'indisponibilité de son véhicule. Ayant été temporairement réaffecté sur un autre emploi au sein de l'entreprise, M. C... a refusé de faire l'acquisition d'un nouveau véhicule. Le 14 octobre 2015, la société Mediapost a demandé l'autorisation de licencier M. C..., en invoquant l'impossibilité pour M. C... de remplir ses obligations contractuelles du fait de l'absence de disposition d'un véhicule et le trouble objectif au fonctionnement de l'entreprise en résultant. Par une décision du 11 décembre 2015, l'inspectrice du travail de la 18ème section de l'unité territoriale d'Indre-et-Loire de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Centre Val de Loire a refusé d'autoriser le licenciement de M. C.... Par un jugement du 14 juin 2018, le tribunal administratif d'Orléans a annulé cette décision ainsi que la décision implicite de la ministre chargée du travail ayant rejeté le recours hiérarchique de la société Mediapost. Par un arrêt du 17 septembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel de M. C... contre ce jugement. M. C... se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient dans l'intérêt de l'ensemble des travailleurs qu'ils représentent d'une protection exceptionnelle. Lorsque le licenciement de l'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est motivée par un acte ou un comportement du salarié qui ne constitue pas une faute, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits en cause sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise, eu égard à la nature de ses fonctions et à l'ensemble des règles applicables au contrat de travail de l'intéressé. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que pour rejeter la demande de la société Mediapost tendant à la délivrance d'une autorisation aux fins de licencier M. C... pour trouble objectif dans le fonctionnement de l'entreprise, l'inspecteur du travail a estimé que le non-respect de la clause du contrat de travail de M. C... exigeant la possession d'un véhicule personnel ne caractérisait pas en l'espèce un trouble objectif au fonctionnement de l'entreprise. Or il ressort des termes mêmes de l'arrêt attaqué que pour juger illégale cette décision et la décision du ministre chargé du travail rejetant le recours hiérarchique de la société, la cour administrative d'appel de Nantes s'est fondée sur le caractère " non pertinent " des motifs pour lesquels M. C... refusait d'acquérir un nouveau véhicule. En statuant ainsi alors que, saisie de la légalité des décisions de l'inspecteur du travail et du ministre chargé du travail, il lui appartenait seulement de se prononcer sur la régularité de ces décisions et sur le bien-fondé de leurs motifs, la cour a méconnu son office de juge de l'excès de pouvoir. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. C... est fondé à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Mediapost une somme de 3 000 euros à verser à M. C... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 17 septembre 2019 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : La société Mediapost versera à M. C... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B... C... et à la société Mediapost. <br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Françoise Tomé, conseillère d'Etat-rapporteure. <br/>
<br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Françoise Tomé<br/>
                 Le secrétaire :<br/>
                 Signé : M. D... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
