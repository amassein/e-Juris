<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037612952</ID>
<ANCIEN_ID>JG_L_2018_11_000000419164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/29/CETATEXT000037612952.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/11/2018, 419164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419164.20181114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Grenoble d'enjoindre au directeur du centre pénitentiaire de Saint-Quentin Fallavier de le placer en quartier d'isolement. Par une ordonnance n° 1801253 du 6 mars 2018, le juge des référés du tribunal administratif de Grenoble a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés le 21 mars 2018, le 3 avril 2018, le 10 juillet 2018 et le 16 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-146 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ".<br/>
<br/>
              2.	Il ressort des pièces du dossier soumis au juge des référés que M.A..., condamné à une peine de huit années de réclusion criminelle et en détention depuis le 18 mars 2016, a été placé à l'isolement, à sa demande, du 4 avril au 17 août 2016 puis du 17 août 2016 au 13 janvier 2017, en raison de risques pour sa sécurité liés au motif de sa condamnation et à son état suicidaire. L'administration pénitentiaire a décidé, le 13 janvier 2017, la mainlevée de sa mise à l'isolement. M. A...a fait l'objet de périodes d'hospitalisation à l'unité hospitalière spécialement aménagée du centre hospitalier Le Vinatier et au centre hospitalier de Roanne pour soins psychiatriques au cours de l'année 2017, la dernière à compter du 7 décembre 2017. Il a été admis au centre pénitentiaire de Saint-Quentin Fallavier le 12 février 2018, en provenance du centre pénitentiaire de Roanne. M. A...se pourvoit en cassation contre l'ordonnance du 6 mars 2018 par laquelle le juge des référés du tribunal administratif de Grenoble a rejeté sa demande tendant à ce qu'il soit enjoint à l'administration pénitentiaire de le placer de nouveau à l'isolement.<br/>
              3.	Aux termes de l'article 22 de la loi du 24 novembre 2009 pénitentiaire : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. Ces restrictions tiennent compte de l'âge, de l'état de santé, du handicap et de la personnalité de la personne détenue ". Aux termes de l'article L. 726-1 du code de procédure pénale : " Toute personne détenue, sauf si elle est mineure, peut être placée par l'autorité administrative, pour une durée maximale de trois mois, à l'isolement par mesure de protection ou de sécurité soit à sa demande, soit d'office. Cette mesure ne peut être renouvelée pour la même durée qu'après un débat contradictoire, au cours duquel la personne concernée, qui peut être assistée de son avocat, présente ses observations orales ou écrites. L'isolement ne peut être prolongé au-delà d'un an qu'après avis de l'autorité judiciaire. (...). ". Aux termes de l'article R. 57-7-73 de ce code : " Tant pour la décision initiale que pour les décisions ultérieures de prolongation, il est tenu compte de la personnalité de la personne détenue, de sa dangerosité ou de sa vulnérabilité particulière, et de son état de santé. / L'avis écrit du médecin intervenant dans l'établissement est recueilli préalablement à toute proposition de renouvellement de la mesure au-delà de six mois et versé au dossier de la procédure. ". <br/>
<br/>
              4.	Eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis-à-vis de l'administration pénitentiaire, il appartient à celle-ci, et notamment aux directeurs des établissements pénitentiaires, en leur qualité de chefs de service, de prendre les mesures propres à protéger leur vie ainsi qu'à leur éviter tout traitement inhumain ou dégradant afin de garantir le respect effectif des exigences découlant des principes rappelés notamment par les articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Le droit au respect de la vie ainsi que le droit de ne pas être soumis à des traitements inhumains ou dégradants constituent des libertés fondamentales au sens des dispositions de l'article L. 521-2 du code de justice administrative. Lorsque la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes ou les expose à être soumises, de manière caractérisée, à un traitement inhumain ou dégradant, portant ainsi une atteinte grave et manifestement illégale à ces libertés fondamentales, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut, au titre de la procédure particulière prévue par l'article L. 521-2, prescrire toutes les mesures de nature à faire cesser la situation résultant de cette carence.<br/>
<br/>
              5.	En premier lieu, c'est sans erreur de droit ni contradiction de motifs que le juge des référés du tribunal administratif de Grenoble s'est fondé, pour estimer que le centre pénitentiaire de Saint-Quentin Fallavier n'avait pas commis d'atteinte grave et manifestement illégale aux droits de M. A...en ne faisant pas droit immédiatement à sa demande de placement en isolement, malgré sa vulnérabilité et son état de santé psychologique, sur la circonstance que le placement en quartier d'isolement n'était pas le seul moyen d'assurer sa sécurité et son intégrité physique. <br/>
<br/>
              6.	En deuxième lieu, si M. A...fait état des attestations médicales de deux médecins du centre hospitalier de Roanne, en date des 18 et 24 janvier 2018, puis d'un médecin du centre hospitalier Le Vinatier, en date du 28 février 2018, qui exposent que son état de santé psychique n'est pas compatible avec un maintien en détention ordinaire ou préconisent un placement à l'isolement, l'administration a fait valoir les risques que comportait pour le détenu la mise à l'isolement, la limitation de la durée de la mise à l'isolement qui résulte des dispositions citées au point 3, la nécessité d'assurer à l'intéressé une détention moins contraignante lui permettant de participer à des activités de formation ou de travail et enfin la possibilité d'aménager le régime de détention ordinaire en prévoyant notamment des modalités de surveillance adaptées à sa situation. Eu égard à ces éléments, le juge des référés n'a pas dénaturé les faits de l'espèce en écartant l'existence d'une atteinte grave et manifestement illégale aux droits de M.A.... Le moyen tiré de la méconnaissance des articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit, par voie de conséquence, être écarté.<br/>
<br/>
              7.	Il résulte de ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Grenoble, après avoir constaté l'absence d'atteinte grave et manifestement illégale à une liberté fondamentale, a rejeté sa demande. Par suite, sans qu'il soit besoin d'examiner les moyens du pourvoi dirigés contre les énonciations de l'ordonnance attaquée statuant sur l'urgence, le pourvoi de M. A...doit être rejeté, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
