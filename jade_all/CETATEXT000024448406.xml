<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448406</ID>
<ANCIEN_ID>JG_L_2011_07_000000349624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448406.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 26/07/2011, 349624</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Suzanne von Coester</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Julien Boucher</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n°s 0913570 et 1007274 du 23 mai 2011, enregistrée le 25 mai 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la première section du tribunal administratif de Paris, avant qu'il soit statué sur les demandes de M. A tendant à la réduction des cotisations d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2006 et 2007, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 2° de l'article 196 du code général des impôts ;<br/>
<br/>
              Vu le mémoire, enregistré le 30 mars 2011 au greffe du tribunal administratif de Paris, présenté par M. Mohammad Baber A, demeurant ..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; il soutient que les dispositions de l'article 196 du code général des impôts méconnaissent les droits de la famille, de l'enfant et de l'adoption consacrés aux alinéas 10, 11, 13 et 14 du Préambule de la Constitution du 27 octobre 1946 ; qu'elles méconnaissent également l'article 55 de la Constitution, en ce qu'elles sont incompatibles avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de la convention internationale des droits de l'enfant ; qu'elles méconnaissent les principes d'égalité devant la loi, d'égalité devant l'impôt et d'égalité devant les charges publiques garantis par l'article 1er de la Constitution et par les articles 1er et 13 de la Déclaration des droits de l'homme et du citoyen ; que ces dispositions législatives méconnaissent enfin les principes fondamentaux reconnus par les lois de la République en vertu desquels la tutelle est une charge personnelle et d'ordre public ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Suzanne von Coester, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Julien Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant que, dans cette hypothèse, le Conseil d'Etat est régulièrement saisi et se prononce sur le renvoi de la question prioritaire de constitutionnalité telle qu'elle a été soulevée dans le mémoire distinct produit devant la juridiction qui la lui a transmise, dans la limite des dispositions dont la conformité à la Constitution a fait l'objet de la transmission ; que si, dans son ordonnance du 23 mai 2011, le président de la première section du tribunal administratif de Paris a motivé la transmission au Conseil d'Etat de la question de la conformité à la Constitution du 2° de l'article 196 du code général des impôts par le caractère non dépourvu de sérieux du moyen tiré de la violation du principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, le Conseil d'Etat est saisi de l'ensemble des motifs d'inconstitutionnalité invoqués dans le mémoire produit devant le tribunal administratif à l'encontre de la disposition législative contestée ;<br/>
<br/>
              Considérant que M. A soutient que le 2° de l'article 196 du code général des impôts, qui dispose que sont considérés comme étant à la charge du contribuable, à la condition qu'ils n'aient pas de revenus distincts, les enfants qu'il a recueillis à son propre foyer, est contraire aux principes d'égalité devant la loi et d'égalité devant les charges publiques respectivement garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, en ce qu'il exclut les enfants ne résidant pas au domicile du contribuable dont celui-ci assume l'entière charge financière ; que, toutefois, le principe d'égalité devant la loi ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; que, dans le but de prendre en compte la situation et les charges de famille du contribuable, le législateur a pu, sans méconnaître le principe d'égalité devant la loi, établir une distinction, pour le mécanisme du quotient familial prévoyant la division du revenu imposable en un certain nombre de parts, entre les enfants recueillis par un contribuable à son foyer et les autres enfants à sa charge ; qu'en retenant une telle condition de résidence des enfants au foyer du contribuable, le législateur s'est fondé sur un critère objectif et rationnel en fonction des buts qu'il s'est assignés, qui ne fait pas peser de charge excessive, au regard de leurs facultés contributives, sur les contribuables assumant la charge financière d'enfants autres que ceux recueillis à leur foyer ; que, par suite, les moyens tirés de la violation des principes d'égalité devant la loi et d'égalité devant les charges publiques ne présentent pas un caractère sérieux ;<br/>
<br/>
              Considérant, s'agissant des autres motifs invoqués par M. A, que la circonstance qu'un contribuable supporte des dépenses afférentes à l'entretien d'un enfant sans pour autant bénéficier d'un avantage fiscal à ce titre ne saurait, en tout état de cause, constituer une méconnaissance des principes fondamentaux reconnus par les lois de la République qui s'appliqueraient à la tutelle et des droits de la famille, de l'enfant et de l'adoption consacrés aux alinéas 10, 11, 13 et 14 du Préambule de la Constitution du 27 octobre 1946 et interprétés au regard de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de la convention internationale des droits de l'enfant ; qu'ainsi aucun de ces moyens ne présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Mohammad Baber A, à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement et au Premier ministre.<br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Paris.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-06 PROCÉDURE. - TRANSMISSION AU CE - ETENDUE DE SA SAISINE (QUANT AUX MOYENS) - ENSEMBLE DES MOYENS INVOQUÉS DANS LE MÉMOIRE DEVANT LA JURIDICTION QUI A TRANSMIS LA QUESTION [RJ1].
</SCT>
<ANA ID="9A"> 54-10-06 Lorsqu'une juridiction relevant du Conseil d'Etat (CE) lui a transmis une question prioritaire de constitutionnalité (QPC) celui-ci examine cette dernière, dans la limite des dispositions dont la conformité à la Constitution a fait l'objet de la transmission. Il est en revanche saisi de l'ensemble des motifs d'inconstitutionnalité invoqués dans le mémoire produit devant cette juridiction à l'encontre des dispositions législatives en question, nonobstant la circonstance que la juridiction du fonds ait motivé le renvoi par le sérieux d'un de ces motifs en particulier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la limitation de la saisine aux seules dispositions législatives transmises, CE, 26 novembre 2010, Cachard, n° 342958, à mentionner aux Tables. Rappr., pour la neutralité de la décision de transmission de la QPC au regard des motifs d'inconstitutionnalité soulevés, CE, 24 septembre 2010, Decurey, n° 341685, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
