<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030064096</ID>
<ANCIEN_ID>JG_L_2015_01_000000383899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/06/40/CETATEXT000030064096.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/01/2015, 383899, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383899.20150107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
            Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Cergy-Pontoise d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision de l'Assistance publique - Hôpitaux de Paris du 18 juin 2014 de ne pas donner suite à sa demande d'emploi en situation de cumul emploi-retraite. Par une ordonnance n°s 1407350, 1407504 du 14 août 2014, le juge des référés du tribunal administratif de Cergy-Pontoise a suspendu l'exécution de cette décision.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, enregistré le 22 août 2014 au secrétariat du contentieux du Conseil d'Etat, l'Assistance publique - Hôpitaux de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 3 et 4 de cette ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 14 août 2014 ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. D'une part, aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public : " (...) doivent être motivées les décisions qui : / (...) - refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir ; / - refusent une autorisation, sauf lorsque la communication des motifs pourrait être de nature à porter atteinte à l'un des secrets ou intérêts protégés par les dispositions des deuxième à cinquième alinéas de l'article 6 de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public (...) ".<br/>
<br/>
              3. D'autre part, en vertu du 7° de l'article L. 161-22 du code de la sécurité sociale, un médecin en retraite peut, à compter de l'âge légal ou réglementaire de départ à la retraite, accomplir à sa demande des vacations dans des établissements de santé ou dans des établissements ou services sociaux et médico-sociaux en cumulant sa pension et la rémunération de ces vacations, dans la limite d'un plafond. Ces dispositions ouvrent une faculté de cumul de la rémunération d'une activité professionnelle avec une pension, qui n'est subordonnée à aucune autorisation administrative, mais ne confèrent pas au praticien retraité un droit à la poursuite d'une activité professionnelle auprès de son ancien employeur.<br/>
<br/>
              4. Il résulte de ce qui précède que la décision par laquelle l'Assistance publique - Hôpitaux de Paris n'a pas donné suite à la demande de vacations de Mme B...à compter du 19 juillet 2014, date de son départ à la retraite après prolongation de son activité au-delà de la limite d'âge sur le fondement de l'article 135 de la loi du 9 août 2004, ne constituait pas le refus d'une autorisation ni même le refus d'un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir. Par suite, le juge des référés du tribunal administratif de Cergy-Pontoise a commis une erreur de droit en jugeant que la décision litigieuse refusait à Mme B...le bénéfice d'une autorisation au sens de la loi du 11 juillet 1979 et que, dès lors, le moyen tiré de son absence de motivation était de nature à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              5. Il suit de là que l'Assistance publique - Hôpitaux de Paris est fondée à demander l'annulation des articles 3 et 4 de l'ordonnance qu'elle attaque. L'erreur de droit qui l'entache suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de statuer sur la demande de suspension présentée par MmeB....<br/>
<br/>
              7. Les moyens invoqués par Mme B...à l'appui de sa demande de suspension, tirés du défaut de motivation de la décision litigieuse, de l'absence d'instruction de sa demande, de la méconnaissance du principe du contradictoire, du défaut de motivation de l'avis de la commission médicale d'établissement, de la méconnaissance des dispositions du 6° de l'article R. 6144-40 du code de la santé publique en l'absence de consultation du comité technique d'établissement et de l'incompétence de l'auteur de l'acte en l'absence de délégation de signature, ne sont pas, en l'état de l'instruction, propres à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              8. L'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par Mme B... devant le juge des référés du tribunal administratif de Cergy-Pontoise doit être rejetée. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par Mme B...devant le juge des référés. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Assistance publique - Hôpitaux de Paris au même titre tant devant le juge des référés que devant le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 3 et 4 de l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 14 août 2014 sont annulés.<br/>
Article 2 : La demande présentée par Mme B...devant le juge des référés du tribunal administratif de Cergy-Pontoise est rejetée.<br/>
Article 3 : Les conclusions de l'Assistance publique - Hôpitaux de Paris présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Assistance publique - Hôpitaux de Paris et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
