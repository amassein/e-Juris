<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028243792</ID>
<ANCIEN_ID>JG_L_2013_11_000000357306</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/24/37/CETATEXT000028243792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 26/11/2013, 357306, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357306</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:357306.20131126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 mars et 31 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre communal d'action sociale (CCAS) de Clermont-Ferrand représenté par son représentant légal en exercice domicilié... ; le CCAS demande au Conseil d'Etat d'annuler le jugement n° 1100016 du 21 décembre 2011 par lequel le tribunal administratif de Clermont-Ferrand a, d'une part, annulé la décision du 8 juin 2010 par laquelle le président du CCAS de Clermont-Ferrand a refusé l'attribution de la nouvelle bonification indiciaire aux fonctionnaires territoriaux exerçant leurs fonctions au sein de l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) " Les jardins de la Charme " ainsi que le rejet du recours gracieux dirigé contre cette décision, d'autre part, enjoint au CCAS de Clermont-Ferrand de réexaminer, au regard de leur situation individuelle, le droit au bénéfice de la nouvelle bonification indiciaire de chaque fonctionnaire territorial exerçant ses fonctions au sein de l'EHPAD dans le délai de deux mois à compter de la notification du jugement ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 2006-780 du 3 juillet 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat du Centre communal d'action sociale (CCAS) de Clermont-Ferrand et à la SCP Waquet, Farge, Hazan, avocat du syndicat CGT des fonctionnaires territoriaux de la mairie et du CCAS de Clermont-Ferrand ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que, par lettre du 8 septembre 2010, le syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand a demandé au président du centre communal d'action sociale (CCAS) de Clermont-Ferrand de rapporter sa décision du 8 juin 2010 refusant l'attribution de la nouvelle bonification indiciaire aux fonctionnaires territoriaux exerçant leurs fonctions au sein de l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) " Les jardins de la Charme " ; que, par une décision du 8 novembre 2010 et pour les mêmes motifs que ceux exposés dans sa décision du 8 juin 2010, le président du CCAS de Clermont-Ferrand a rejeté ce recours gracieux ; qu'il ressort de la demande présentée devant le tribunal administratif de Clermont-Ferrand et des pièces qui y étaient jointes, que le syndicat a entendu contester ces deux décisions ; que, par suite, le défaut de communication au CCAS de Clermont-Ferrand du dernier mémoire présenté par le syndicat, qui ne contenait pas de conclusions nouvelles, n'a pas entaché d'irrégularité la procédure suivie devant le tribunal administratif ;<br/>
<br/>
              2. Considérant, en deuxième lieu, qu'il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles du 2° de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des fonctionnaires autres que ceux qui concernent l'entrée au service, la discipline ou la sortie de service ; que, contrairement à ce qui est soutenu par le CCAS, la contestation par le syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand du refus d'attribuer la nouvelle bonification indiciaire aux fonctionnaires territoriaux exerçant leurs fonctions au sein de l'EHPAD " Les jardins de la Charme " doit être regardée comme relative à la situation individuelle de ces agents ; que, par suite, le moyen tiré de la méconnaissance du 2° de l'article R. 222-13 et de l'article R. 811-1 du code de justice administrative ne peut qu'être écarté ;<br/>
<br/>
              3. Considérant, en troisième lieu, qu'aux termes de l'article 1er du décret du 3 juillet 2006 portant attribution de la nouvelle bonification indiciaire à certains personnels de la fonction publique territoriale exerçant dans des zones à caractère sensible : " Les fonctionnaires territoriaux exerçant à titre principal les fonctions mentionnées en annexe au présent décret soit dans les zones urbaines sensibles dont la liste est fixée par le décret du 26 décembre 1996 susvisé, soit dans les services et équipements situés en périphérie de ces zones et assurant leur service en relation directe avec la population de ces zones, (...) bénéficient de la nouvelle bonification indiciaire. " ; qu'il résulte des dispositions précitées qu'a droit à une nouvelle bonification indiciaire le fonctionnaire territorial qui exerce ses fonctions à titre principal au sein d'une zone urbaine sensible ou dans un service situé en périphérie d'une telle zone, sous réserve, dans ce second cas, que l'exercice des fonctions assurées par l'agent concerné le place en relation directe avec des usagers résidant dans cette zone urbaine sensible ; qu'il est constant que l'EHPAD " Les jardins de la Charme " se situe dans une zone urbaine sensible figurant sur la liste fixée par le décret du 26 décembre 1996 ; qu'en jugeant que les dispositions de l'article 1er du décret du 3 juillet 2006 ne subordonnaient pas l'attribution de la nouvelle bonification indiciaire, pour les fonctionnaires territoriaux exerçant leurs fonctions au sein de cet EHPAD, à l'accomplissement d'un service en relation directe avec la population de la zone dans laquelle se situe l'établissement, le tribunal administratif n'a, contrairement à ce qui est soutenu, commis aucune erreur de droit ;<br/>
<br/>
              4. Considérant, en quatrième lieu, que le moyen tiré de la méconnaissance du principe d'égalité de traitement n'est assorti d'aucune précision permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              5. Considérant, en cinquième lieu, que le CCAS de Clermont-Ferrand ne peut utilement contester le bien-fondé du jugement qu'il attaque en invoquant, pour la première fois devant le juge de cassation, le moyen qui n'est pas d'ordre public et n'est pas né de ce jugement, tiré de ce que les décisions dont l'annulation est demandée seraient légalement justifiées par un autre motif ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le CCAS de Clermont-Ferrand n'est pas fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge du CCAS de Clermont-Ferrand le versement au syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand de la somme de 3 000 euros au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Le pourvoi du centre communal d'action sociale de Clermont-Ferrand est rejeté.<br/>
<br/>
Article 2 : Le centre communal d'action sociale de Clermont-Ferrand versera au syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au centre communal d'action sociale de Clermont-Ferrand et au syndicat CGT des fonctionnaires et agents territoriaux de la mairie et du CCAS de Clermont-Ferrand.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
