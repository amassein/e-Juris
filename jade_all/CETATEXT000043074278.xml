<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074278</ID>
<ANCIEN_ID>JG_L_2021_01_000000436639</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074278.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 26/01/2021, 436639, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436639</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436639.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) LPL82 a demandé au juge des référés du tribunal administratif de Toulouse d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution, d'une part, de la décision INTV-MCQ-2019-16 du 13 juin 2019 de la directrice générale de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) définissant les conditions applicables en France pour le programme de l'Union européenne " lait et fruits à l'école " à partir de la rentrée de l'année scolaire 2019/2020 et, d'autre part, de la décision du 6 juin 2019 de la directrice générale de FranceAgriMer par laquelle elle a été informée de la clôture de l'agrément qui lui avait été accordé le 8 janvier 2019 en tant que gestionnaire de ce programme, d'enjoindre à FranceAgriMer, d'une part, de maintenir jusqu'à la fin de l'année scolaire 2022/2023 l'application de la décision INTV-RMPS-2017-63 du 10 octobre 2017 définissant les conditions applicables en France pour ce programme à partir de la rentrée de l'année scolaire 2017/2018 et, d'autre part, de maintenir son agrément dans ses conditions initiales et, enfin, de condamner FranceAgriMer à lui verser 208 280 euros en réparation du préjudice matériel et moral qu'elle estime avoir subi.<br/>
<br/>
              Par une ordonnance n° 1906094 du 25 novembre 2019, le juge des référés du tribunal administratif de Toulouse a suspendu l'exécution de la décision du 6 juin 2019, a enjoint à FranceAgriMer de maintenir l'agrément de la SAS LPL82 accordé le 8 janvier 2019 et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, un nouveau mémoire, une note en délibéré et un nouveau mémoire, enregistrés respectivement au secrétariat de la section du contentieux les 10 et 26 décembre 2019, le 7 janvier 2020, le 6 février 2020 et le 22 décembre 2020, FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle lui est défavorable ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de la SAS LPL82 la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés de produits agricoles, et abrogeant les règlements (CEE) n° 922/72, (CEE) n° 234/79, (CE) n° 1037/2001 et (CE) n° 1234/2007 du Conseil ;<br/>
              - le règlement délégué (UE) 2017/40 de la Commission du 3 novembre 2016 complétant le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil en ce qui concerne l'aide de l'Union pour la fourniture de fruits et de légumes, de bananes et de lait dans les établissements scolaires et modifiant le règlement délégué (UE) n° 907/2014 de la Commission ;<br/>
              - la loi n° 2018-938 du 30 octobre 2018 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de la société FranceAgriMer et à la SCP Baraduc, Duhamel, Rameix, avocat de la société LPL82 ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 janvier 2021, présentée par la société LPL 82.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / (...) La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Toulouse que, par une décision individuelle du 8 janvier 2019 de la directrice générale de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer), la société par actions simplifiée (SAS) LPL82 a été agréée, à partir du deuxième trimestre de l'année scolaire 2018/2019 commençant le 1er janvier 2019, en qualité de gestionnaire du volet " fruit et légumes " du programme d'aide de l'Union européenne intitulé " lait et fruits à l'école ". Cet agrément a été accordé dans le cadre de la stratégie de la France notifiée à la Commission européenne le 1er août 2017 pour la période 2017/2018-2022/2023 et sur le fondement de la décision réglementaire INTV-RMPS-2017-63 du 10 octobre 2017 de la directrice générale de FranceAgriMer relative à la mise en oeuvre de ce dispositif d'aide. Toutefois, par une lettre du 6 juin 2019, notifiée le 14 juin 2019, la directrice générale de FranceAgriMer a informé la SAS LPL82 de l'évolution de la stratégie nationale pour ce programme d'aide conduisant à réserver l'accès à l'aide aux organismes supportant le coût de la restauration collective dans les établissements scolaires, ce qui en excluait les opérateurs intervenant en qualité de fournisseurs. En conséquence, la SAS LPL82 était informée, d'une part, de la fin de son agrément en qualité de gestionnaire du volet " fruit et légumes " à compter de la fin de l'année scolaire en cours et, d'autre part, de la faculté qui lui était ouverte d'intervenir dorénavant auprès des organismes précités en qualité de fournisseur référencé. L'évolution de la stratégie s'est traduite par une nouvelle décision réglementaire INTV-MCQ-2019-16 du 13 juin 2019 de la directrice générale de FranceAgriMer. FranceAgriMer se pourvoit en cassation contre les articles 1 et 2 de l'ordonnance du 25 novembre 2019 par lesquels le juge des référés du tribunal administratif de Toulouse a suspendu l'exécution de la décision du 6 juin 2019 portant fin de l'agrément de la SAS LPL82 et lui a enjoint de reprendre le fonctionnement de celui-ci.<br/>
<br/>
              3. Aux termes de l'article 23 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 : " 1. Une aide de l'Union est octroyée en faveur de : / a) la distribution aux enfants, dans les établissements scolaires visés à l'article 22, de produits des secteurs des fruits et des légumes, des fruits et des légumes transformés et des bananes (...) / 2. Les États membres souhaitant participer au programme élaborent au préalable, au niveau national ou régional, une stratégie pour sa mise en oeuvre. (...) ". Aux termes du 4 de l'article 2 du règlement délégué (UE) 2017/40 de la Commission du 3 novembre 2016 : " Un État membre peut modifier sa stratégie. L'État membre notifie à la Commission sa nouvelle stratégie dans un délai de deux mois à compter de la modification (...) ". Aux termes du 2 de l'article 5 du même règlement : " L'État membre sélectionne les demandeurs d'aide parmi les organismes suivants : / a) les établissements scolaires ; / b) les autorités scolaires ; / c) les fournisseurs ou distributeurs de produits ; / d) les organisations agissant au nom d'un ou de plusieurs établissements scolaires ou autorités scolaires et instituées spécifiquement aux fins de la gestion et de la réalisation de l'une des activités visées au paragraphe 1 ; / e) tout autre organisme public ou privé chargé de la gestion et de la réalisation de toute activité visée au paragraphe 1 ". Enfin, aux termes de l'article 6 du même règlement : " 1. Les demandeurs d'aide doivent être agréés par l'autorité compétente de l'État membre sur le territoire duquel se trouve l'établissement scolaire auquel les produits sont fournis et/ou distribués (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions que les Etats membres ont la faculté de modifier leur stratégie nationale, notamment pour déterminer, parmi les catégories de demandeurs visées par le règlement délégué (UE) 2017/40, celles qui pourront être agréées en vue de bénéficier de l'aide. Par suite, les opérateurs concernés, qui ne disposent d'aucun droit au maintien de la stratégie nationale en vigueur au moment où ils ont été agréés et qui ne se sont, alors, engagés à aucune contrepartie autre que celle de fournir les denrées alimentaires correspondant à leur agrément au cours du trimestre où celui-ci leur a été accordé, sont en mesure de prévoir que cette stratégie, en particulier en tant qu'elle détermine l'accès direct à l'aide, est susceptible d'être adaptée.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés que si la lettre du 6 juin 2019 mentionnée au point 2, qui constitue une décision faisant grief en tant qu'elle met fin à l'agrément dont bénéficiait la SAS LPL82, a été prise avant la décision réglementaire du 13 juin 2019 précitée, ainsi d'ailleurs qu'avant la décision individuelle du 14 juin 2019 renouvelant cet agrément à compter du troisième trimestre de l'année scolaire 2018/2019, elle n'a produit d'effet, compte tenu de ses termes mêmes, qu'à la fin de l'année scolaire en cause, c'est-à-dire après ces décisions. Par ailleurs, la décision réglementaire du 13 juin 2019, qui ne s'est appliquée qu'à compter de l'année scolaire 2019/2020 n'a modifié les modalités d'agrément que pour l'avenir et, eu égard à ce qui a été dit au point 4, n'a porté atteinte ni à des situations juridiquement constituées ni aux principes de sécurité juridique et de confiance légitime. Dès lors et sans que puisse y faire obstacle la circonstance que les décisions individuelles accordant et renouvelant l'agrément ne précisaient pas le terme de celui-ci et que ni ces décisions ni la décision réglementaire du 10 octobre 2017 n'indiquaient que l'agrément pouvait ne pas être renouvelé en cas de modification de la réglementation, le juge des référés du tribunal administratif de Toulouse, en jugeant que le moyen qu'il a regardé comme tiré de l'application rétroactive de la décision réglementaire du 13 juin 2019, était de nature à créer un doute sérieux quant à la légalité de la décision individuelle du 6 juin 2019 a commis une erreur de droit. Par suite et sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, FranceAgriMer est fondé à demander l'annulation des articles 1er et 2 de l'ordonnance qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Les moyens soulevés par la SAS LPL82, tirés de ce que la décision contestée est entachée d'illégalité rétroactive, porte atteinte à des droits acquis et méconnaît les principes de sécurité juridique et de protection de la confiance légitime, ne sont pas propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision du 6 juin 2019. Par suite, et sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par FranceAgriMer et sur la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative, les conclusions tendant à la suspension de l'exécution de cette décision, ainsi, par voie de conséquence, que les conclusions aux fins d'injonction présentées par la SAS LPL82 doivent être rejetées.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de FranceAgriMer, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que la SAS LPL82 demande à ce titre. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SAS LPL82 le versement à FranceAgriMer d'une somme de 3 000 euros au titre de la demande qu'elle a présentée en première instance et en cassation.   <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'ordonnance du 25 novembre 2019 du juge des référés du tribunal administratif de Toulouse sont annulés. <br/>
Article 2 : Les conclusions présentées par la SAS LPL82 devant le juge des référés du tribunal administratif de Toulouse tendant, d'une part, à la suspension de l'exécution de la décision du 6 juin 2019 de la directrice générale de FranceAgriMer et, d'autre part, à ce qu'il soit enjoint à celle-ci de maintenir son agrément ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : La SAS LPL82 versera à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) et à la société par actions simplifiée LPL82.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
