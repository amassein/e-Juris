<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546723</ID>
<ANCIEN_ID>JG_L_2021_05_000000435650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546723.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 27/05/2021, 435650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435650.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lyon de condamner la commune de Villeurbanne à lui verser la somme de 3 382 euros. Par un jugement n° 1709240 du 19 juillet 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 30 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Rocheteau, Uzan-Sarano, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la route ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;  <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B... et à la SARL Didier-Pinet, avocat de la commune de Villeurbanne et de la société Groupama Rhônes-Alpes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La mise en fourrière des véhicules est prévue par l'article L. 325-1 du code de la route, dont le premier alinéa dispose que, pour différents motifs fixés par cet article, notamment les infractions aux dispositions de ce même code, les véhicules peuvent, à la demande et sous la responsabilité du maire ou de l'officier de police judiciaire compétent, être mis en fourrière et, le cas échéant, aliénés ou livrés à la destruction. L'article R. 325-22 du même code dispose, s'agissant de la notification de la mise en fourrière prévue par l'article R. 325-21, que : " I.- Cette notification s'effectue par lettre recommandée avec demande d'accusé de réception, dans le délai maximal de cinq jours ouvrables suivant la mise en fourrière du véhicule. / II.- Cette notification comporte les mentions obligatoires suivantes : / (...) / 5° Mise en demeure au propriétaire de retirer son véhicule avant l'expiration d'un délai : / a) De dix jours à compter de la date de notification pour un véhicule à livrer à la destruction ; / b) De quinze jours à compter de la date de notification pour un véhicule à remettre à l'administration chargée des domaines en vue de son aliénation ; / 6° Avertissement au propriétaire que son absence de réponse dans les délais impartis vaudra abandon de son véhicule et que ledit véhicule sera, dans les conditions prévues par décret, soit remis à l'administration chargée des domaines en vue de son aliénation, soit livré à la destruction ; / (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., propriétaire d'un véhicule mis en fourrière pour stationnement irrégulier par les services de la commune de Villeurbanne (Rhône), s'est vu notifier une mise en demeure de retirer son véhicule avant le 14 novembre 2016. Ayant pris contact avec les services municipaux, elle a toutefois reçu l'assurance verbale de pouvoir récupérer son véhicule jusqu'au 31 décembre 2016. Lorsqu'elle s'est présentée le 3 janvier 2017, il lui a été indiqué que ce véhicule avait été détruit, destruction dont il est apparu qu'elle avait eu lieu dès le 23 décembre 2016. Mme B... se pourvoit en cassation contre le jugement du 19 juin 2019 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à ce que la commune de Villeurbanne soit condamnée à lui verser la somme de 3 832 euros en réparation du préjudice matériel et des troubles dans les conditions d'existence ayant résulté, pour elle, de l'absence de restitution de son véhicule.<br/>
<br/>
              3. En premier lieu, en jugeant qu'il n'était pas établi, au vu des simples allégations de la requérante, qu'elle se serait rendue une première fois à la fourrière le 28 décembre 2016, le tribunal administratif s'est livré à une appréciation souveraine des pièces du dossier qui lui était soumis, sans les dénaturer. Il n'a pas commis d'erreur de droit en ne faisant pas usage de ses pouvoirs d'instruction pour établir la réalité de ce déplacement et n'a pas méconnu, ce faisant, le principe d'égalité des armes garanti par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              4. En second lieu, en retenant que Mme B... n'était venue, sans s'en justifier, rechercher son véhicule qu'après le 31 décembre 2016, date à partir de laquelle elle savait qu'elle prenait le risque qu'il soit détruit, pour en déduire que, s'il résultait de l'instruction que l'administration avait, contrairement à l'engagement pris, détruit son véhicule dès le 23 décembre, cette circonstance était sans lien direct avec l'impossibilité pour la requérante de le récupérer, le tribunal administratif a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Le pourvoi de Mme B... doit, par suite, être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A... B..., à la commune de Villeurbanne et à la société Groupama Rhône-Alpes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
