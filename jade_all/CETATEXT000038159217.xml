<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038159217</ID>
<ANCIEN_ID>JG_L_2019_02_000000417209</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/92/CETATEXT000038159217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/02/2019, 417209</TITRE>
<DATE_DEC>2019-02-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417209</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417209.20190218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 janvier et 11 avril 2018, la Confédération générale du travail - Force ouvrière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-1551 du 10 novembre 2017 relatif aux modalités d'approbation par consultation des salariés de certains accords d'entreprise ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention internationale du travail n° 87 de 1948 concernant la liberté syndicale et la protection du droit syndical adoptée à San Francisco lors de la trente-et-unième session de la conférence internationale du travail, ratifiée le 28 juin 1951 ;<br/>
              - la convention internationale du travail n° 98 de 1949 concernant l'application des principes du droit d'organisation et de négociation collective adoptée à Genève lors de la trente-deuxième session de la conférence internationale du travail, ratifiée le 26 octobre 1951 ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2017-1340 du 15 septembre 2017 ;<br/>
              - l'ordonnance n° 2017-1385 du 22 septembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la Société Confédération générale du travail Force ouvrière.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 2232-23-1 du code du travail, dans sa rédaction applicable à la date du décret attaqué, issue de l'article 8 de l'ordonnance du 22 septembre 2017 relative au renforcement de la négociation collective : " I. - Dans les entreprises dont l'effectif habituel est compris entre onze et moins de cinquante salariés, en l'absence de délégué syndical dans l'entreprise ou l'établissement, les accords d'entreprise ou d'établissement peuvent être négociés, conclus et révisés : / 1° Soit par un ou plusieurs salariés expressément mandatés par une ou plusieurs organisations syndicales représentatives dans la branche ou, à défaut, par une ou plusieurs organisations syndicales représentatives au niveau national et interprofessionnel, étant membre ou non de la délégation du personnel du comité social et économique. A cet effet, une même organisation ne peut mandater qu'un seul salarié ; / 2° Soit par un ou des membres de la délégation du personnel du comité social et économique. / Les accords ainsi négociés, conclus et révisés peuvent porter sur toutes les mesures qui peuvent être négociées par accord d'entreprise ou d'établissement sur le fondement du présent code. / II. - La validité des accords ou des avenants de révision conclus avec un ou des membres de la délégation du personnel du comité social et économique, mandaté ou non, est subordonnée à leur signature par des membres du comité social et économique représentant la majorité des suffrages exprimés lors des dernières élections professionnelles. / La validité des accords ou des avenants de révision conclus avec un ou plusieurs salariés mandatés, s'ils ne sont pas membres de la délégation du personnel du comité social et économique, est subordonnée à leur approbation par les salariés à la majorité des suffrages exprimés, dans des conditions déterminées par décret et dans le respect des principes généraux du droit électoral ". <br/>
<br/>
              2. Le décret attaqué du 10 novembre 2017 modifie trois articles de la partie réglementaire du livre II de la deuxième partie du code du travail, relatifs aux modalités de consultation des salariés en vue de l'approbation de certains accords d'entreprise ou d'établissement, pour les rendre applicables, notamment, aux accords ou avenants de révision conclus avec un ou plusieurs salariés mandatés non membres de la délégation du personnel du comité social et économique dans les entreprises dont l'effectif habituel est compris entre onze et moins de cinquante salariés, en l'absence de délégué syndical dans l'entreprise ou l'établissement, dans les conditions prévues à l'article L. 2232-23-1 du code du travail mentionné ci-dessus. La Confédération générale du travail - Force ouvrière demande l'annulation pour excès de pouvoir de ce décret au motif que l'article L. 2232-23-1 du code du travail, qui en constitue la base légale, évincerait les organisations syndicales de la négociation des accords d'entreprise concernés faute d'instituer une priorité entre la négociation avec des salariés élus et la négociation avec les salariés mandatés, de limiter le champ de la négociation avec les salariés non mandatés et d'imposer à l'employeur d'informer les organisations syndicales représentatives dans la branche de l'engagement d'une négociation sur un tel accord et que, pour ces raisons, il méconnaîtrait les articles 3 et 11 de la convention internationale du travail n° 87 et l'article 4 de la convention internationale du travail n° 98.<br/>
<br/>
              3. D'une part, aux termes de l'article 3 de la convention internationale du travail n° 87 concernant la 'liberté syndicale et la protection du droit syndical : " 1. Les organisations de travailleurs et d'employeurs ont le droit d'élaborer leurs statuts et règlements administratifs, d'élire librement leurs représentants, d'organiser leur gestion et leur activité, et de formuler leur programme d'action. / 2. Les autorités publiques doivent s'abstenir de toute intervention de nature à limiter ce droit ou à en entraver l'exercice légal ". Ces stipulations créent des droits dont les particuliers peuvent directement se prévaloir. Toutefois, les dispositions de l'article L. 2232-23-1 du code du travail, qui ont pour objet, dans les entreprises dont l'effectif habituel est compris entre onze et quarante-neuf salariés, de permettre qu'un accord d'entreprise puisse être négocié, conclu ou révisé malgré l'absence de délégué syndical dans l'entreprise ou l'établissement, ne portent nullement atteinte à la liberté d'organisation des organisations syndicales que ces stipulations garantissent.<br/>
<br/>
              4. D'autre part, l'article 11 de la même convention prévoit que : " Tout membre de l'Organisation internationale du travail pour lequel la présente convention est en vigueur s'engage à prendre toutes mesures nécessaires et appropriées en vue d'assurer aux travailleurs et aux employeurs le libre exercice du droit syndical " et l'article 4 de la convention internationale du travail n° 98 concernant l'application des principes du droit d'organisation et de négociation collective que : " Des mesures appropriées aux conditions nationales doivent, si nécessaire, être prises pour encourager et promouvoir le développement et l'utilisation les plus larges de procédures de négociation volontaire de conventions collectives entre les employeurs et les organisations d'employeurs d'une part, et les organisations de travailleurs d'autre part, en vue de régler par ce moyen les conditions d'emploi ". Ces stipulations requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers et sont, par suite, dépourvues d'effet direct. Dès lors, le syndicat requérant ne peut utilement les invoquer à l'appui de ses conclusions.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la Confédération générale du travail - Force ouvrière n'est pas fondée à demander l'annulation du décret qu'elle attaque. Sa requête doit ainsi être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la Confédération générale du travail - Force ouvrière est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Confédération générale du travail - Force ouvrière, au Premier ministre et à la ministre du travail. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACCORDS INTERNATIONAUX. APPLICABILITÉ. - CONVENTION INTERNATIONALE DU TRAVAIL N° 87 CONCERNANT LA LIBERTÉ SYNDICALE ET LA PROTECTION DU DROIT SYNDICAL - ARTICLE 3 - EFFET DIRECT [RJ1].
</SCT>
<ANA ID="9A"> 01-01-02-01 L'article 3 de la convention internationale du travail n° 87 de 1948 concernant la liberté syndicale et la protection du droit syndical, adoptée à San Francisco lors de la trente-et-unième session de la conférence internationale du travail, crée des droits dont les particuliers peuvent directement se prévaloir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 décembre 2011, Fédération générale autonome des fonctionnaires, n° 341670, inédit au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
