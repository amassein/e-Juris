<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713940</ID>
<ANCIEN_ID>JG_L_2019_07_000000420987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713940.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 01/07/2019, 420987, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2019:420987.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A...et Mme C...A...ont porté plainte contre M. E... D...devant la chambre disciplinaire de première instance de Basse-Normandie de l'ordre des médecins. Le conseil départemental de la Manche de l'ordre des médecins s'est associé à la plainte. Par une décision n° 749 du 18 novembre 2016, la chambre disciplinaire de première instance a infligé à M. D...la sanction de radiation du tableau de l'ordre des médecins. <br/>
<br/>
              Par une décision n° 13041 du 27 mars 2018, la chambre disciplinaire nationale de l'ordre des médecins, sur appel de M.D..., a réformé la décision de la chambre disciplinaire de première instance et infligé à l'intéressé la sanction d'interdiction d'exercer la médecine pendant trois mois, à compter du 1er septembre 2018. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 mai et 28 août 2018 et le 14 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. D... ;<br/>
<br/>
              3°) de mettre à la charge de M. D...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de MmeA..., à la SCP Zribi et Texier, avocat de M. D... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite du décès de M.A..., son épouse et sa fille ont porté plainte contre son dermatologue, M. D..., devant le conseil départemental de la Manche de l'ordre des médecins. Statuant sur cette plainte, à laquelle le conseil départemental s'était associé, la chambre disciplinaire de première instance de Basse-Normandie de l'ordre des médecins a, par une décision du 18 novembre 2016, infligé à M. D...la sanction de radiation du tableau de l'ordre en retenant trois fautes disciplinaires. Premièrement, le fait qu'en traitant le mélanome dont souffrait M. A... il avait méconnu l'obligation d'" assurer personnellement au patient des soins consciencieux, dévouées et fondés sur les données acquises de la science, en faisant appel, s'il y a lieu, à l'aide de tiers compétents " prévue par l'article R. 4127-32 du code de la santé publique. Deuxièmement, le fait qu'il avait, à cette même occasion, méconnu l'obligation selon laquelle " Le médecin doit à la personne qu'il examine, qu'il soigne ou qu'il conseille une information loyale, claire et appropriée sur son état, les investigations et les soins qu'il lui propose " prévue par l'article R. 4127-35 du même code. Enfin, le fait qu'en ayant produit, pour sa défense devant la chambre disciplinaire, des documents qu'il avait falsifiés, il avait méconnu les principes de moralité et de probité rappelés par l'article R. 4127-3 du même code.<br/>
<br/>
              2. Sur appel de M.D..., la chambre disciplinaire nationale, tout en retenant les mêmes fautes, a abaissé la sanction prononcée en première instance en la ramenant, par sa décision du 27 mars 2018, à une interdiction d'exercer la médecine pendant trois mois. MmeA..., fille du défunt et co-auteur de la plainte, se pourvoit en cassation contre cette décision.<br/>
<br/>
              3. L'article L. 4123-2 du code de la santé publique dispose que : " Lorsqu'une plainte est portée devant le conseil départemental, son président en accuse réception à l'auteur, en informe le médecin, le chirurgien-dentiste ou la sage-femme mise en cause et les convoque dans un délai d'un mois à compter de la date d'enregistrement de la plainte en vue d'une conciliation. En cas d'échec de celle-ci, il transmet la plainte à la chambre disciplinaire de première instance avec l'avis motivé du conseil dans un délai de trois mois à compter de la date d'enregistrement de la plainte, en s'y associant, le cas échéant. (...) ". Par ailleurs, le VI de l'article L. 4122-3 du même code dispose que : " Peuvent faire appel, outre l'auteur de la plainte et le professionnel sanctionné, le ministre chargé de la santé, le directeur général de l'agence régionale de santé, le procureur de la République, le conseil départemental ou territorial et le Conseil national de l'ordre intéressé ". Ces dispositions confèrent à l'auteur d'une plainte la qualité de partie à l'instance disciplinaire introduite par sa plainte. MmeA..., qui avait ainsi qualité de partie en défense devant la chambre disciplinaire nationale a, par suite, qualité pour se pourvoir en cassation contre la décision du 27 mars 2018. Il résulte du dispositif de cette décision et de ce qui a été dit au point 2 qu'elle justifie d'un intérêt à en demander l'annulation.<br/>
<br/>
              4. Si le choix de la sanction relève de l'appréciation des juges du fond au vu de l'ensemble des circonstances de l'espèce, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu, dès lors, être légalement prise.<br/>
<br/>
              5. Les peines disciplinaires encourues par les médecins sont, aux termes de l'article L. 4124-6 du code de la santé publique : " (...) 1° L'avertissement ;/ 2° Le blâme ;/ 3° L'interdiction temporaire avec ou sans sursis ou l'interdiction permanente d'exercer une, plusieurs ou la totalité des fonctions de médecin, de chirurgien-dentiste ou de sage-femme, conférées ou rétribuées par l'Etat, les départements, les communes, les établissements publics, les établissements reconnus d'utilité publique ou des mêmes fonctions accomplies en application des lois sociales ;/ 4° L'interdiction temporaire d'exercer avec ou sans sursis ; cette interdiction ne pouvant excéder trois années ;/ 5° La radiation du tableau de l'ordre./ (...) ". <br/>
<br/>
              6. Il résulte des termes, non contestés, de la décision attaquée de la chambre disciplinaire nationale, que M.D..., qui avait procédé à deux reprises, à l'exérèse d'une lésion cutanée dont souffrait M.A..., n'a, alors que l'analyse biologique avait chaque fois révélé qu'il s'agissait d'un mélanome malin, informé son patient ni de la nature de sa maladie, ni de sa gravité, ni de la nécessité d'un suivi médical régulier. La chambre disciplinaire a également retenu que M. D...n'avait pas pris contact avec le médecin traitant de M.A..., ni prescrit le " bilan d'extension " prévu par les recommandations de bonne pratique de la Haute autorité de santé, ni proposé la tenue d'une réunion pluridisciplinaire. Elle a enfin relevé que, pour tenter d'établir le contraire, l'intéressé avait produit devant la chambre disciplinaire de première instance des documents qu'il avait falsifiés. En infligeant à M.D..., au vu de ce comportement, une interdiction d'exercer la médecine pendant trois mois, la chambre disciplinaire nationale a retenu une sanction qui, à supposer même, comme le soutient l'intéressé, qu'un suivi régulier du mélanome n'aurait pas évité l'issue létale de la maladie, est, par son insuffisance, hors de proportion avec les fautes commises. <br/>
<br/>
              7. Mme A...est, par suite, fondée à demander l'annulation de la décision qu'elle attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. D...la somme de 3 000 euros que demande la requérante au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soient mises à la charge de MmeA..., qui n'est pas la partie perdante dans la présente instance, la somme que demande, au même titre, M. D.... <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des médecins du 27 mars 2018 est annulée.<br/>
Article  2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 3 : M. D...versera à Mme A...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. D...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5  : La présente décision sera notifiée à Mme C...A...et à M. E...D....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins, au conseil départemental de la Manche de l'ordre des médecins et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-004-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES POURVOIS. - DÉCISION DE LA CHAMBRE DISCIPLINAIRE NATIONALE DE L'ORDRE DES MÉDECINS - 1) QUALITÉ DE PARTIE À L'INSTANCE DISCIPLINAIRE DE L'AUTEUR DE LA PLAINTE - EXISTENCE [RJ1] - 2) CONSÉQUENCE - INTÉRÊT DE L'AUTEUR DE LA PLAINTE POUR FORMER UN POURVOI CONTRE CETTE DÉCISION - EXISTENCE, EN FONCTION DU DISPOSITIF [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-05 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. VOIES DE RECOURS. - DÉCISION DE LA CHAMBRE DISCIPLINAIRE NATIONALE DE L'ORDRE DES MÉDECINS - 1) QUALITÉ DE PARTIE À L'INSTANCE DISCIPLINAIRE DE L'AUTEUR DE LA PLAINTE - EXISTENCE [RJ1] - 2) CONSÉQUENCE - INTÉRÊT DE L'AUTEUR DE LA PLAINTE POUR FORMER UN POURVOI CONTRE CETTE DÉCISION - EXISTENCE, EN FONCTION DU DISPOSITIF [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-05-01-03 PROFESSIONS, CHARGES ET OFFICES. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES DEVANT LE CONSEIL D'ETAT. POUVOIRS DU JUGE. CONSEIL D'ÉTAT JUGE DE CASSATION. - DÉCISION DE LA CHAMBRE DISCIPLINAIRE NATIONALE DE L'ORDRE DES MÉDECINS - 1) QUALITÉ DE PARTIE À L'INSTANCE DISCIPLINAIRE DE L'AUTEUR DE LA PLAINTE - EXISTENCE [RJ1] - 2) CONSÉQUENCE - INTÉRÊT DE L'AUTEUR DE LA PLAINTE POUR FORMER UN POURVOI CONTRE CETTE DÉCISION - EXISTENCE, EN FONCTION DU DISPOSITIF [RJ2].
</SCT>
<ANA ID="9A"> 54-08-02-004-01 1) L'article L. 4123-2 du code de la santé publique (CSP) et le VI de l'article L. 4122-3 du même code confèrent à l'auteur d'une plainte la qualité de partie à l'instance disciplinaire introduite par sa plainte.... ,,2) Requérant portant une plainte devant le conseil départemental de l'ordre des médecins. Praticien relevant appel de la sanction infligée par la chambre disciplinaire de première instance. Chambre disciplinaire nationale retenant les mêmes griefs mais abaissant le quantum de la sanction prononcée....  ,,Le requérant, qui avait ainsi qualité de partie en défense devant la chambre disciplinaire nationale a, par suite, qualité pour se pourvoir en cassation contre la décision. Il résulte du dispositif de cette décision qu'il justifie d'un intérêt à en demander l'annulation.</ANA>
<ANA ID="9B"> 55-04-01-05 1) L'article L. 4123-2 du code de la santé publique (CSP) et le VI de l'article L. 4122-3 du même code confèrent à l'auteur d'une plainte la qualité de partie à l'instance disciplinaire introduite par sa plainte.... ,,2) Requérant portant une plainte devant le conseil départemental de l'ordre des médecins. Praticien relevant appel de la sanction infligée par la chambre disciplinaire de première instance. Chambre disciplinaire nationale retenant les mêmes griefs mais abaissant le quantum de la sanction prononcée....  ,,Le requérant, qui avait ainsi qualité de partie en défense devant la chambre disciplinaire nationale a, par suite, qualité pour se pourvoir en cassation contre la décision. Il résulte du dispositif de cette décision qu'il justifie d'un intérêt à en demander l'annulation.</ANA>
<ANA ID="9C"> 55-05-01-03 1) L'article L. 4123-2 du code de la santé publique (CSP) et le VI de l'article L. 4122-3 du même code confèrent à l'auteur d'une plainte la qualité de partie à l'instance disciplinaire introduite par sa plainte.... ,,2) Requérant portant une plainte devant le conseil départemental de l'ordre des médecins. Praticien relevant appel de la sanction infligée par la chambre disciplinaire de première instance. Chambre disciplinaire nationale retenant les mêmes griefs mais abaissant le quantum de la sanction prononcée....  ,,Le requérant, qui avait ainsi qualité de partie en défense devant la chambre disciplinaire nationale a, par suite, qualité pour se pourvoir en cassation contre la décision. Il résulte du dispositif de cette décision qu'il justifie d'un intérêt à en demander l'annulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., avant l'intervention de la loi n° 2002-303 du 4 mars 2002, sur l'absence de qualité de partie dans l'instance disciplinaire de l'auteur de la plainte, CE, 19 octobre 1979,,, n° 12176, p. 383 ; CE, 15 avril 1983,,, n° 36503, T. pp. 606-848.,,[RJ2] Ab. jur., CE, Section, 3 mars 1989,,, n° 84716, p. 68. Rappr., sur l'intérêt à se pourvoir en cassation en cas de rejet de la plainte par la juridiction ordinale d'appel, CE, 9 février 1977,,, n° 92835, p. 80. Cf. CE, Section, décision du même jour,,, n°s 411263-411302, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
