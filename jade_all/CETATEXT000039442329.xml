<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442329</ID>
<ANCIEN_ID>JG_L_2019_11_000000435785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/23/CETATEXT000039442329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 20/11/2019, 435785</TITRE>
<DATE_DEC>2019-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435785.20191120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
               Vu la procédure suivante :<br/>
<br/>
               M. B... A... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative d'ordonner à l'Etat, d'une part, de suspendre l'exécution de la décision du 4 octobre 2019 par laquelle le directeur de la maison d'arrêt de Grasse l'a placé à l'isolement et, d'autre part, toutes mesures nécessaires à la sauvegarde du droit de ne pas subir des traitements inhumains ou dégradants et du droit au respect de sa vie privée et familiale. Par une ordonnance n° 1904959 du 18 octobre 2019, le juge des référés du tribunal administratif de Nice a rejeté sa requête. <br/>
<br/>
               Par une requête et un mémoire en réplique, enregistrés les 5 et 14 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
               1°) d'annuler l'ordonnance du 18 octobre 2019 et de faire droit à sa demande de première instance ; <br/>
<br/>
               2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
               Il soutient que : <br/>
               - l'ordonnance contestée est insuffisamment motivée faute de répondre au moyen tiré de l'atteinte grave et manifestement illégale à une liberté fondamentale ; <br/>
               - la minute est entachée d'irrégularité faute de comporter les signatures prévues par l'article R. 741-8 du code de justice administrative ;<br/>
               - la condition d'urgence est remplie dès lors, d'une part, que l'urgence doit être présumée et, d'autre part, que sa mise à l'isolement a de graves conséquences sur sa santé mentale et physique, lesquelles sont accrues par la durée de cette mesure ;<br/>
               - l'exécution de la décision litigieuse porte une atteinte grave et manifestement illégale, au regard des articles R. 57-7-63 et R. 57-7-73 du code de procédure pénale, à son droit de ne pas subir des traitements inhumains ou dégradants ainsi qu'à son droit au respect de la vie privée dès lors, en premier lieu, qu'il n'a plus aucun contact avec ses codétenus, en deuxième lieu, qu'il n'a pu consulter le médecin qu'une seule fois depuis le 4 octobre 2019 alors même qu'il avait fait état de velléités suicidaires, en troisième lieu, que la décision contestée n'est fondée sur aucun élément de fait de nature à établir un risque d'incidents graves et, en quatrième lieu, qu'il n'a pas été tenu compte de sa personnalité, de sa dangerosité, de sa vulnérabilité particulière ou de son état de santé.<br/>
<br/>
               Par un mémoire en défense, enregistré le 14 novembre 2019, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient qu'aucun des moyens soulevés par le requérant n'est fondé.<br/>
<br/>
<br/>
<br/>
               Vu les autres pièces des dossiers ;<br/>
<br/>
               Vu :<br/>
               - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
               - le code de procédure pénale ; <br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique, d'une part, M. A... et, d'autre part, la garde des sceaux, ministre de la justice ;<br/>
<br/>
               Vu le procès-verbal de l'audience publique du 15 novembre 2019 à 15 heures au cours de laquelle ont été entendus :<br/>
<br/>
               - Me Feschotte-Desbois, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A..., ainsi que les représentants de ce dernier ;<br/>
<br/>
               - les représentants de la garde des sceaux, ministre de la justice ; <br/>
               et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit :<br/>
<br/>
               1. Il résulte de l'instruction que M. A..., qui est maintenu en détention provisoire depuis le 5 avril 2017 à la maison d'arrêt de Grasse, a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative d'enjoindre à l'Etat, d'une part, de suspendre l'exécution de la décision du 4 octobre 2019 par laquelle le directeur de la maison d'arrêt de Grasse a prolongé le placement à l'isolement dont il a été l'objet depuis le 18 juillet 2019 et, d'autre part, de prendre toutes mesures nécessaires à la sauvegarde de son droit à ne pas subir des traitements inhumains ou dégradants et au respect de sa vie privée et familiale. Par une ordonnance n° 1904959 du 18 octobre 2019, le juge des référés du tribunal administratif de Nice a rejeté sa requête. M. A... relève appel de cette ordonnance. <br/>
<br/>
               2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
               3. Aux termes du premier alinéa de l'article 726-1 du code de procédure pénale : " Toute personne détenue, sauf si elle est mineure, peut être placée par l'autorité administrative, pour une durée maximale de trois mois, à l'isolement par mesure de protection ou de sécurité soit à sa demande, soit d'office. Cette mesure ne peut être renouvelée pour la même durée qu'après un débat contradictoire, au cours duquel la personne concernée, qui peut être assistée de son avocat, présente ses observations orales ou écrites. L'isolement ne peut être prolongé au-delà d'un an qu'après avis de l'autorité judiciaire ". Aux termes du troisième alinéa du même article : " Lorsqu'une personne détenue est placée à l'isolement, elle peut saisir le juge des référés en application de l'article L. 521-2 du code de justice administrative ". Il résulte de ces dernières dispositions, éclairées par les travaux préparatoires de la loi pénitentiaire du 24 novembre 2009 dont elles sont issues, que le législateur, en faisant explicitement référence à la possibilité pour les personnes détenues de saisir le juge des référés du tribunal administratif sur le fondement de l'article L. 521-2 du code de justice administrative, n'a pas entendu faire obstacle à ce que ce juge puisse également être saisi d'une demande de suspension de l'exécution des décisions plaçant d'office à l'isolement ou prolongeant cet isolement sur le fondement de l'article L. 521-1 du même code.<br/>
<br/>
               4. En distinguant les deux procédures prévues par les articles L. 521-1 et L. 521-2 du code de justice administrative, le législateur a entendu répondre à des situations différentes. Les conditions auxquelles est subordonnée l'application de ces dispositions ne sont pas les mêmes, non plus que les pouvoirs dont dispose le juge des référés. En particulier, le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article.<br/>
<br/>
               5. Si, eu égard à son objet et à ses effets sur les conditions de détention, la décision plaçant d'office à l'isolement une personne détenue ainsi que les décisions prolongeant éventuellement un tel placement, prises sur le fondement de l'article 726-1 du code de procédure pénale, créent en principe, sauf à ce que l'administration pénitentiaire fasse valoir des circonstances particulières, une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, puisse ordonner la suspension de leur exécution s'il estime remplie l'autre condition posée par cet article, il appartient, en revanche, à la personne détenue qui saisit le juge des référés sur le fondement de l'article L. 521-2 du même code de justifier de circonstances particulières caractérisant, au regard notamment de son état de santé ou des conditions dans lesquelles elle est placée à l'isolement, la nécessité, pour elle, de bénéficier à très bref délai, du prononcé d'une mesure de sauvegarde sur le fondement de ce dernier article.<br/>
<br/>
               6. Aux termes de l'article R. 57-7-62 du code de procédure pénale : " La mise à l'isolement d'une personne détenue, par mesure de protection ou de sécurité, qu'elle soit prise d'office ou sur la demande de la personne détenue, ne constitue pas une mesure disciplinaire. / La personne détenue placée à l'isolement est seule en cellule. / Elle conserve ses droits à l'information, aux visites, à la correspondance écrite et téléphonique, à l'exercice du culte et à l'utilisation de son compte nominatif. / Elle ne peut participer aux promenades et activités collectives auxquelles peuvent prétendre les personnes détenues soumises au régime de détention ordinaire, sauf autorisation, pour une activité spécifique, donnée par le chef d'établissement. / Toutefois, le chef d'établissement organise, dans toute la mesure du possible et en fonction de la personnalité de la personne détenue, des activités communes aux personnes détenues placées à l'isolement. / La personne détenue placée à l'isolement bénéficie d'au moins une heure quotidienne de promenade à l'air libre. ". Aux termes de l'article R. 57-7-63 du même code : " La liste des personnes détenues placées à l'isolement est communiquée quotidiennement à l'équipe de l'unité de consultation et de soins ambulatoires de l'établissement. / Le médecin examine sur place chaque personne détenue au moins deux fois par semaine et aussi souvent qu'il l'estime nécessaire. / Ce médecin, chaque fois qu'il l'estime utile au regard de l'état de santé de la personne détenue, émet un avis sur l'opportunité de mettre fin à l'isolement et le transmet au chef d'établissement. ".<br/>
<br/>
               7. Pour justifier de l'urgence qu'il y aurait à suspendre à très bref délai, sur le fondement de l'article L. 521-2 du code de justice administrative, l'exécution de la décision du 4 octobre 2019 qui a prolongé le placement à l'isolement dont il fait l'objet depuis le 18 juillet 2019, M. A... soutient que les conditions de son placement à l'isolement, ainsi que la durée de celui-ci, affectent gravement son état de santé physique et psychique et provoquent chez lui des velléités suicidaires. Toutefois, il résulte de l'instruction que l'intéressé, d'une part, reçoit deux fois par semaine la visite du médecin de la maison d'arrêt qui, à aucun moment, n'a jugé incompatible ses conditions de détention avec son état de santé et, d'autre part, ne produit en appel aucun élément tangible, à l'appui de ses allégations, qui démontrerait l'existence d'une situation médicale préoccupante. Par ailleurs, sa mise à l'isolement, qui est conforme aux dispositions de l'article R. 57-7-62 du code de procédure pénale, ne l'empêche, ni de recevoir des visites au parloir, ni de continuer à bénéficier de la promenade et d'activités culturelles ou sportives. Par suite, M. A... n'établit pas l'existence de circonstances particulières justifiant qu'il soit ordonné à très bref délai, sur le fondement de l'article L. 521-2 du code de justice administrative, une mesure de sauvegarde d'une liberté fondamentale. <br/>
<br/>
               8. Il résulte de ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Nice, dont l'ordonnance est suffisamment motivée et qui en a signé la minute en application de l'article R. 742-5 du code de justice administrative, a considéré que la condition d'urgence n'était pas remplie. Dès lors, et sans qu'il soit besoin de se prononcer sur l'existence alléguée d'une atteinte grave et manifestement illégale à une liberté fondamentale, il y a lieu de rejeter la requête, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A... et à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - EXÉCUTION DES JUGEMENTS. - EXÉCUTION DES PEINES. - SERVICE PUBLIC PÉNITENTIAIRE. - RECOURS EN RÉFÉRÉ CONTRE UNE MESURE DE PLACEMENT D'UN DÉTENU À L'ISOLEMENT OU DE PROLONGATION DE CETTE MESURE (ART. 726-1 DU CPP) - PRÉSOMPTION D'URGENCE EN RÉFÉRÉ SUSPENSION - EXISTENCE [RJ2] - PRÉSOMPTION D'URGENCE EN RÉFÉRÉ LIBERTÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-03-02 PROCÉDURE. - PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. - RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). - CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. - URGENCE. - PRÉSOMPTION - ABSENCE [RJ1] - MESURE DE PLACEMENT D'UN DÉTENU À L'ISOLEMENT OU DE PROLONGATION DE CETTE MESURE (ART. 726-1 DU CPP).
</SCT>
<ANA ID="9A"> 37-05-02-01 Si, eu égard à son objet et à ses effets sur les conditions de détention, la décision plaçant d'office à l'isolement une personne détenue ainsi que les décisions prolongeant éventuellement un tel placement, prises sur le fondement de l'article 726-1 du code de procédure pénale (CPP), créent en principe, sauf à ce que l'administration pénitentiaire fasse valoir des circonstances particulières, une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), puisse ordonner la suspension de leur exécution s'il estime remplie l'autre condition posée par cet article, il appartient, en revanche, à la personne détenue qui saisit le juge des référés sur le fondement de l'article L. 521-2 du même code de justifier de circonstances particulières caractérisant, au regard notamment de son état de santé ou des conditions dans lesquelles elle est placée à l'isolement, la nécessité, pour elle, de bénéficier à très bref délai du prononcé d'une mesure de sauvegarde sur le fondement de ce dernier article.</ANA>
<ANA ID="9B"> 54-035-03-03-02 Il appartient à la personne détenue qui saisit le juge des référés sur le fondement de l'article L. 521-2 du code de justice administrative (CJA) de justifier de circonstances particulières caractérisant, au regard notamment de son état de santé ou des conditions dans lesquelles elle est placée à l'isolement, la nécessité, pour elle, de bénéficier à très bref délai du prononcé d'une mesure de sauvegarde sur le fondement de ce dernier article.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du référé suspension, CE, 7 juin 2019, Mme Madani, n° 426772, p.199....[RJ2] Cf. CE, 7 juin 2019, Mme Madani, n° 426772, p. 199.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
