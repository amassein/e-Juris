<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253955</ID>
<ANCIEN_ID>JG_L_2018_07_000000408149</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253955.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 26/07/2018, 408149, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408149</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408149.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Les Hauts du Golf a demandé au tribunal administratif de Bordeaux de condamner solidairement la commune de La Teste-de-Buch et l'Etat à lui verser la somme de 18 232 048,10 euros, en réparation des préjudices qu'elle estime avoir subis et tenant à l'impossibilité de réaliser un projet de lotissement. Par un jugement n° 1304060 du 5 novembre 2015, le tribunal administratif a fait partiellement droit à sa demande en condamnant la commune de La Teste-de-Buch à lui verser la somme de 892 895,33 euros et en condamnant l'Etat et la commune de La Teste-de-Buch solidairement à lui verser la somme de 49 192,43 euros.<br/>
<br/>
              Par un arrêt n° 15BX04252, 16BX00037, 16BX00352 du 20 décembre 2016, la cour administrative d'appel de Bordeaux a, sur appels de la société Les Hauts du Golf et de la commune de La Teste-de-Buch, annulé la condamnation de la commune de La Teste-de-Buch à lui verser la somme de 892 895,33 euros et condamné l'Etat à garantir cette commune de la moitié de la condamnation à verser la somme de 49 192,43 euros.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 février 2017, 19 mai 2017 et 2 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la société Les Hauts du Golf demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de la commune de La Teste-de-Buch et de l'Etat la somme de 5 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 86-2 du 3 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société Les Hauts du Golf et à la SCP Piwnica, Molinié, avocat de la commune de la Teste-de-Buch ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Les Hauts du Golf, qui envisageait la réalisation d'un lotissement au lieudit Le Laurey Ouest, situé sur le territoire de la commune de La Teste-de-Buch, a notamment obtenu une autorisation de défricher délivrée par le préfet de la Gironde le 22 octobre 1998, deux certificats d'urbanisme positifs délivrés les 14 janvier 1998 et 26 juillet 1999 par le maire de La Teste-de-Buch, puis une autorisation de lotir délivrée le 26 octobre 1999 au nom de la commune et portant sur 20 lots ; que cette autorisation de lotir est devenue caduque le 26 avril 2001 ; qu'après avoir acquis, le 3 mars 2000, le terrain d'assiette nécessaire à la réalisation de son projet, la société Les Hauts du Golf a obtenu un nouveau certificat d'urbanisme positif le 1er décembre 2000 ; que, le 30 octobre 2001, le maire de La Teste-de-Buch a toutefois refusé de lui délivrer une nouvelle autorisation de lotir portant sur 90 lots ; que le tribunal administratif de Bordeaux a annulé ce refus par un jugement du 26 juin 2003 ; que, le 24 octobre 2003, le préfet de la Gironde a délivré à la société une nouvelle autorisation de défricher ; qu'au terme d'une transaction avec la société approuvée par une délibération du conseil municipal du 30 mars 2006 et signée le 24 avril 2006, le maire de La Teste-de-Buch lui a délivré une autorisation de lotir le 14 septembre 2006 ; que, par trois jugements rendus les 15 mai et 17 juillet 2008, le tribunal administratif de Bordeaux a annulé la délibération du 30 mars 2006, l'autorisation de lotir du 14 septembre 2006 et l'autorisation de défrichement du 24 octobre 2003 ; que, par une décision du 14 novembre 2011, le Conseil d'Etat, statuant au contentieux a rejeté les pourvois formés par la société Les Hauts du Golf contre les deux arrêts de la cour administrative d'appel de Bordeaux du 7 septembre 2009 confirmant ces jugements ;<br/>
<br/>
              2. Considérant que la société Les Hauts du Golf a alors demandé au tribunal administratif de Bordeaux de condamner solidairement la commune de La Teste-de-Buch et l'Etat à l'indemniser des préjudices qu'elle estimait avoir subis du fait de l'impossibilité de réaliser son projet ; que, par un jugement du 5 novembre 2015, le tribunal administratif de Bordeaux a fait partiellement droit à sa demande en condamnant la commune de La Teste-de-Buch à lui verser la somme de 892 895,33 euros au titre du préjudice résultant de la perte de valeur vénale de son terrain et en condamnant solidairement l'Etat et la commune à lui verser la somme de 49 192,43 euros correspondant à la moitié des frais liés à des travaux de terrassement et à la réalisation d'études, de dossiers et de diagnostics et exposés à la suite de la délivrance de l'autorisation de défrichement du 24 octobre 2003 et de l'autorisation de lotir du 14 septembre 2006,  jusqu'au jugement du 15 mai 2008 annulant ces autorisations ; que, par un arrêt du 20 décembre 2016, contre lequel la société Les Hauts du Golf se pourvoit en cassation, la cour administrative d'appel de Bordeaux a réformé ce jugement en annulant la condamnation de la commune de La Teste-de-Buch à lui verser la somme de 892 895,33 euros ; <br/>
<br/>
              Sur la responsabilité de la commune :<br/>
<br/>
              En ce qui concerne la délivrance des certificats d'urbanisme :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 410-1 du code de l'urbanisme, dans sa rédaction applicable à la date des certificats litigieux : " Le certificat d'urbanisme indique, en fonction du motif de la demande, si, compte tenu des dispositions d'urbanisme et des limitations administratives au droit de propriété applicables à un terrain, ainsi que de l'état des équipements publics existants ou prévus, et sous réserve de l'application éventuelle des dispositions législatives et réglementaires relatives aux zones d'aménagement concerté, ledit terrain peut :/ a) Etre affecté à la construction ;/ b) Etre utilisé pour la réalisation d'une opération déterminée, notamment d'un programme de construction défini en particulier par la destination des bâtiments projetés et leur superficie de plancher hors oeuvre... " ; qu'aux termes de l'article L. 146-6 du même code, dans sa rédaction applicable à la même date, issue de la loi du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral : " Les documents et décisions relatifs à la vocation des zones ou à l'occupation et à l'utilisation des sols préservent les espaces terrestres et marins, sites et paysages remarquables ou caractéristiques du patrimoine naturel et culturel du littoral, et les milieux nécessaires au maintien des équilibres biologiques. (...) " ;<br/>
<br/>
              4. Considérant, en premier lieu, que la société requérante soulevait en appel un moyen unique tiré de la délivrance fautive, par la commune de La Teste-de-Buch, de plusieurs certificats d'urbanisme illégaux et imprécis ; que la cour, qui n'était pas tenue de répondre à l'ensemble des arguments, a suffisamment motivé son arrêt sur ce point ; que la requérante n'est, dès lors, pas fondée à demander pour ce motif l'annulation de l'arrêt de la cour en tant qu'il rejette ses conclusions tendant à l'engagement de la responsabilité de la commune au titre de la délivrance du certificat d'urbanisme du 14 janvier 1998 ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que, pour écarter la responsabilité de la commune au titre de la délivrance du certificat d'urbanisme du 26 juillet 1999 pour le préjudice résultant de l'acquisition du terrain, le 3 mars 2000, au prix d'un terrain constructible, la cour administrative d'appel a retenu que ce certificat n'était pas illégal dès lors que le classement de ce terrain en zone constructible n'était, à la date de délivrance des certificats litigieux, en contradiction avec aucune protection instituée ou en cours ; qu'en retenant ainsi que seule l'intégration du terrain en cause au réseau Natura 2000 en 2005 lui avait conféré son caractère d'espace remarquable au sens des dispositions précitées de l'article L. 146-6 du code de l'urbanisme, alors que cette qualification s'apprécie au seul regard des caractéristiques du terrain, la cour a commis une erreur de droit ; qu'en se fondant également, pour juger que la délivrance de ce même certificat n'était pas fautive, sur la circonstance qu'il ne revêtait qu'un caractère informatif, alors que l'information délivrée sur la constructibilité du terrain présentait un caractère erroné, la cour a également commis une erreur de droit ; qu'enfin, en écartant tout lien entre la délivrance du certificat d'urbanisme du 26 juillet 1999 et le préjudice résultant de la perte de valeur vénale du terrain, au motif que la société requérante était, à la date d'achat du terrain en mars 2000, dépourvue de toute assurance suffisante sur la constructibilité des parcelles, alors que l'objet du certificat d'urbanisme était de fournir une telle assurance, la cour administrative d'appel de Bordeaux a inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'en jugeant que le certificat d'urbanisme positif du 1er décembre 2000 n'était pas entaché d'illégalité dès lors qu'il mentionnait l'opposabilité de la " loi littoral ", alors que ce certificat déclarait constructible un terrain compris dans un espace remarquable au sens de l'article L. 146-6 du code de l'urbanisme, la cour administrative d'appel de Bordeaux a commis une erreur de droit ;<br/>
<br/>
              En ce qui concerne le refus d'autorisation de lotir opposé le 30 octobre 2001 :<br/>
<br/>
              7. Considérant que, par un jugement du 26 juin 2003 devenu définitif, le tribunal administratif de Bordeaux a annulé le refus d'autorisation de lotir opposé le 30 octobre 2001 à la société requérante et retenu que cette annulation n'impliquait pas nécessairement la délivrance par le maire d'une nouvelle autorisation ; que, dès lors, en jugeant qu'il n'existait pas de lien direct entre les préjudices invoqués, tenant à l'impossibilité pour la société requérante de réaliser son projet, et l'illégalité fautive du refus opposé par la commune, la cour administrative d'appel de Bordeaux s'est livrée à une appréciation souveraine des faits exempte de dénaturation ;<br/>
<br/>
              En ce qui concerne la conclusion et l'exécution du protocole transactionnel du 24 avril 2006 :<br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 30 mars 2006, le conseil municipal de La Teste-de-Buch a approuvé la signature par le maire d'un protocole transactionnel prévoyant notamment que la commune délivrerait à la société requérante une autorisation de lotir conforme à ses demandes et se désisterait de l'appel contre le jugement du tribunal administratif de Bordeaux du 26 juin 2003 ; que cette signature est intervenue le 24 avril 2006 ; que, par un jugement du 15 mai 2008, confirmé par un arrêt de la cour administrative d'appel de Bordeaux du 7 septembre 2009, le tribunal administratif de Bordeaux a annulé la délibération du 30 mars 2006 et enjoint au maire de saisir le tribunal administratif aux fins d'annulation du protocole ; que la société requérante ne peut utilement soutenir que la cour aurait inexactement qualifié les faits de l'espèce en ne retenant pas que la commune aurait commis une faute en ne se conformant pas à cette injonction de saisir le juge du contrat pour obtenir la résolution du protocole, dès lors que, en tout état de cause, le jugement du tribunal administratif faisait obstacle à la poursuite de l'exécution de cette convention et que la faute alléguée ne pouvait avoir aucun lien direct avec les préjudices invoqués  ;<br/>
<br/>
              9. Considérant que la cour administrative d'appel de Bordeaux a jugé fautives tant la délibération du conseil municipal du 30 mars 2006 autorisant le maire à signer le protocole transactionnel que l'autorisation de lotir délivrée le 14 septembre 2006 en application de ce protocole ; qu'il résulte implicitement mais nécessairement de ces motifs qu'elle a également regardé comme fautive la signature du protocole par le maire ; qu'ainsi le moyen tiré de ce que la cour aurait inexactement qualifié et dénaturé les faits de l'espèce en ne retenant aucune faute de la commune sur ce point manque en fait ; que la cour n'a pas commis d'erreur de qualification juridique ni dénaturé les faits de l'espèce en jugeant que la responsabilité extracontractuelle de la commune ne pouvait être engagée à raison de promesses non tenues, dès lors que le maire s'était, conformément au protocole, désisté de l'appel qu'il avait introduit contre le jugement du 26 juin 2003, qu'il avait délivré une autorisation de lotir et qu'il n'avait fait aucune promesse sur l'aboutissement du projet ;<br/>
<br/>
              En ce qui concerne la responsabilité sans faute :<br/>
<br/>
              10. Considérant qu'aux termes de l'article L. 160-5 du code de l'urbanisme, dans sa version applicable aux faits de l'espèce : " N'ouvrent droit à aucune indemnité les servitudes instituées par application du présent code en matière de voirie, d'hygiène et d'esthétique ou pour d'autres objets et concernant, notamment, l'utilisation du sol, la hauteur des constructions, la proportion des surfaces bâties et non bâties dans chaque propriété, l'interdiction de construire dans certaines zones et en bordure de certaines voies, la répartition des immeubles entre diverses zones. / Toutefois, une indemnité est due s'il résulte de ces servitudes une atteinte à des droits acquis ou une modification à l'état antérieur des lieux déterminant un dommage direct, matériel et certain ; cette indemnité, à défaut d'accord amiable, est fixée par le tribunal administratif, qui doit tenir compte de la plus-value donnée aux immeubles par la réalisation du plan d'occupation des sols rendu public ou du plan local d'urbanisme approuvé ou du document qui en tient lieu " ; que ces dispositions instituent un régime spécial d'indemnisation, exclusif de l'application du régime de droit commun de la responsabilité sans faute de l'administration pour rupture de l'égalité devant les charges publiques ; qu'elles ne font, toutefois, pas obstacle à ce que le propriétaire dont le bien est frappé d'une servitude prétende à une indemnisation dans le cas exceptionnel où il résulte de l'ensemble des conditions et circonstances dans lesquelles la servitude a été instituée et mise en oeuvre, ainsi que de son contenu, que ce propriétaire supporte une charge spéciale et exorbitante, hors de proportion avec l'objectif d'intérêt général poursuivi ;<br/>
<br/>
              11. Considérant que, pour écarter l'existence d'une telle charge, la cour administrative d'appel de Bordeaux a retenu que le caractère inconstructible du terrain d'assiette trouvait son fondement dans les dispositions de l'article L. 146-6 du code de l'urbanisme, applicables à l'ensemble du littoral, que l'application de ces dispositions ne pouvait être regardée comme ayant eu pour effet de modifier l'état antérieur des lieux et que la société requérante ne pouvait se prévaloir d'aucun droit acquis né de l'autorisation de lotir du 14 septembre 2006 ou du protocole transactionnel signé le 24 avril 2006, lesquels étaient illégaux ; qu'elle n'a, sur ce point, pas commis d'erreur de qualification juridique ;<br/>
<br/>
              Sur la responsabilité de l'Etat :<br/>
<br/>
              En ce qui concerne l'exercice par le préfet du contrôle de légalité :<br/>
<br/>
              12. Considérant que les carences de l'Etat dans l'exercice du contrôle de légalité des actes des collectivités territoriales ne sont susceptibles d'engager la responsabilité de l'Etat que si elles présentent le caractère d'une faute lourde ; qu'en retenant qu'il ne résultait pas de l'instruction que les services préfectoraux auraient commis une telle faute en s'abstenant de déférer au tribunal administratif les actes de la commune, notamment la délibération du 30 mars 2006 autorisant le maire à signer le protocole transactionnel et l'autorisation de lotir du 14 septembre 2006, la cour administrative d'appel de Bordeaux n'a ni dénaturé les faits soumis à son appréciation, ni commis d'erreur de qualification juridique ; <br/>
<br/>
              13. Considérant que la société requérante soutenait également devant la cour administrative d'appel que les services de l'Etat avaient commis une faute lourde en s'abstenant de déférer au tribunal administratif les délibérations du 6 octobre 2011 approuvant le plan local d'urbanisme (PLU) de la commune de La Teste-de-Buch et du 24 juin 2013 approuvant le schéma de cohérence territoriale (SCOT) du bassin d'Arcachon, qui ne prévoyaient pas l'inconstructibilité des terrains litigieux ; que, cependant, les préjudices qu'invoquait la société consistaient en des dépenses exposées par elle antérieurement à ces délibérations ou étaient liés à l'achat du terrain litigieux le 3 mars 2000 ; que le motif tiré de ce de tels préjudices n'étaient pas directement liés à la faute invoquée, qui conduit à rejeter les conclusions présentées devant les juges du fond au titre de cette abstention des services de l'Etat, aurait pu être retenu par eux alors même qu'il n'était pas invoqué en défense et ne comporte l'appréciation d'aucune circonstance de fait ; il doit être substitué au motif retenu par l'arrêt attaqué ;<br/>
<br/>
              En ce qui concerne les autorisations de coupe et de défrichement :<br/>
<br/>
              14. Considérant que la société requérante soulevait en appel un moyen unique tiré de la délivrance fautive, par le préfet, d'autorisations de défrichement, de coupe et d'abattage illégales ; que la cour, qui n'était pas tenue de répondre à l'ensemble des arguments, a suffisamment motivé son arrêt sur ce point ;<br/>
<br/>
              15. Considérant qu'il résulte de ce qui a été dit au point 5 qu'en retenant que le préfet de la Gironde n'avait commis aucune faute en délivrant une autorisation de défrichement par un arrêté du 22 octobre 1998, au motif qu'à cette date, le terrain d'assiette du projet n'avait pas encore été identifié comme pouvant constituer un espace protégé au titre de la loi " littoral ", la cour administrative d'appel de Bordeaux a commis une erreur de droit ;<br/>
<br/>
              16. Considérant qu'il résulte des termes de l'arrêt attaqué que la cour a regardé comme fautive la délivrance par le préfet de la Gironde de l'autorisation de défrichement du 24 octobre 2003, dès lors que cette autorisation avait fait l'objet d'une annulation contentieuse par une décision de justice passée en force de chose jugée ; que, si elle a par ailleurs retenu que la société requérante ne précisait pas en quoi la délivrance de cette autorisation constituerait une faute de nature à engager la responsabilité de l'État, c'est par l'effet d'une erreur de plume qui, pour regrettable qu'elle soit, n'entache pas son arrêt de contradiction de motifs ;<br/>
<br/>
              Sur les partages de responsabilité :<br/>
<br/>
              17. Considérant que c'est sans dénaturer les faits de l'espèce ni commettre d'erreur de qualification juridique que la cour administrative d'appel de Bordeaux a vu une faute d'imprudence dans le fait, pour une société constituée de promoteurs locaux au fait des enjeux urbanistiques du Bassin d'Arcachon et de professionnels avertis connaissant les aléas juridiques pesant sur des programmes immobiliers de grande ampleur, de s'être volontairement affranchie de la condition suspensive de la promesse de vente liée à l'obtention d'une autorisation de lotir définitive en acquérant le 3 mars 2000 le terrain d'assiette sans bénéficier d'une telle autorisation de lotir définitive, en signant avec la commune un protocole d'accord transactionnel contraire aux règles d'urbanisme et en ne s'assurant pas au préalable de la régularité de son projet dans un secteur sensible, en dépit des réserves émises et des recours contentieux pendants ; qu'elle n'a pas davantage dénaturé ou inexactement qualifié les faits en retenant qu'eu égard aux fautes commises par les différentes parties qu'elle retenait, il y avait lieu d'exonérer la commune et l'Etat de leur responsabilité à hauteur de 50 % ;<br/>
<br/>
              Sur la détermination des préjudices :<br/>
<br/>
              18. Considérant que la cour administrative d'appel de Bordeaux, qui a condamné la commune et l'Etat à rembourser 50 % des frais d'étude et de travaux engagés inutilement par la société, a omis de statuer sur des demande de remboursement correspondant à un montant de 177 003,60 euros, pour lesquels la société présentait des justificatifs pour la première fois en appel ; qu'elle a, sur ce point, entaché son arrêt d'insuffisance de motivation ;<br/>
<br/>
              19. Considérant que la cour n'a, en tout état de cause, pas commis d'erreur de droit en retenant, par une motivation suffisante, que le préjudice commercial dont la société se prévalait présentait un caractère purement éventuel et n'était donc pas indemnisable ;<br/>
<br/>
              20. Considérant qu'il résulte de tout ce qui précède que l'arrêt attaqué doit être annulé en tant seulement qu'il statue sur la responsabilité de la commune de La Teste-de-Buch au titre de la délivrance fautive des certificats d'urbanisme positifs des 26 juillet 1999 et 1er décembre 2000, sur la responsabilité de l'Etat dans la délivrance fautive de l'autorisation de défrichement du 14 janvier 1998 ainsi que, par suite, en tant qu'il se prononce sur les préjudices de perte de valeur vénale du terrain d'assiette et de frais d'études et de travaux engagés en vain ;<br/>
<br/>
              21. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de La Teste-de-Buch et de l'Etat une somme de 3 000 euros chacun à verser à la société Les Hauts du Golf au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Les Hauts du Golf qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 20 décembre 2016 est annulé en tant qu'il statue sur la responsabilité de la commune de La Teste-de-Buch au titre de la délivrance des certificats d'urbanisme positifs des 26 juillet 1999 et 1er décembre 2000, sur la responsabilité de l'Etat au titre de la délivrance de l'autorisation de défrichement du 14 janvier 1998 et sur les préjudices de perte de valeur vénale du terrain d'assiette et de frais d'études et de travaux engagés en vain.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux dans la limite de la cassation prononcée.<br/>
<br/>
Article 3 : La commune de La Teste-de-Buch et l'Etat verseront à la société Les Hauts du Golf une somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de la société Les Hauts du Golf est rejeté.<br/>
<br/>
Article 5 : Les conclusions de la commune de La Teste-de-Buch présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la société Les Hauts du Golf, à la commune de La Teste-de-Buch, au ministre de la cohésion des territoires et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
