<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028700156</ID>
<ANCIEN_ID>JG_L_2014_03_000000375179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/70/01/CETATEXT000028700156.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 07/03/2014, 375179, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:375179.20140307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1103890 du 30 janvier 2014, enregistré le 5 février 2014 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Cergy-Pontoise, avant de statuer sur la demande de M. A... B...tendant à l'annulation de l'arrêté du 12 novembre 2010 du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales le titularisant dans le corps des gardiens de la paix en tant que cet arrêté ne reprend pas son ancienneté acquise en qualité de militaire, ensemble les arrêtés du 26 mars et du 20 août 2012, a transmis au Conseil d'Etat, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du huitième alinéa de l'article L. 4139-14 du code de la défense combinées avec celles de l'article L. 4139-1 du même code ;<br/>
<br/>
              Vu les mémoires, enregistrés les 17 et 18 octobre 2013 au greffe du tribunal administratif de Cergy-Pontoise, présentés pour M.B..., demeurant..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 mars 2014, présentée pour M. B... ; <br/>
<br/>
              Vu la Constitution, notamment son préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de la défense et notamment ses articles L. 4139-1 à L. 4139-4 et L. 4139-14 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4139-1 du code de la défense, dans sa rédaction applicable au litige : " La demande de mise en détachement du militaire lauréat d'un concours de l'une des fonctions publiques civiles ou d'accès à la magistrature est acceptée, sous réserve que l'intéressé ait accompli au moins quatre ans de services militaires, ait informé son autorité d'emploi de son inscription au concours et ait atteint le terme du délai pendant lequel il s'est engagé à rester en position d'activité à la suite d'une formation spécialisée ou de la perception d'une prime liée au recrutement ou à la fidélisation. / Sous réserve des dispositions de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique relative au statut de la magistrature, le militaire lauréat de l'un de ces concours est titularisé et reclassé, dans le corps ou le cadre d'emploi d'accueil dans des conditions équivalentes, précisées par décret en Conseil d'Etat, à celles prévues pour un fonctionnaire par le statut particulier de ce corps ou de ce cadre d'emploi. (...) " ; qu'aux termes de l'article L. 4139-14 du même code : " La cessation de l'état militaire intervient d'office dans les cas suivants : (...) 8° Lors de la titularisation dans une fonction publique, ou dès la réussite à un concours de l'une des fonctions publiques pour les militaires ne bénéficiant pas du détachement prévu au premier alinéa de l'article L. 4139-1, dans les conditions prévues à la section 1 du présent chapitre " ;<br/>
<br/>
              3. Considérant que M. B...a saisi le tribunal administratif de Cergy-Pontoise d'une demande d'annulation de l'arrêté du 12 novembre 2010 du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales le titularisant dans le corps des gardiens de la paix en tant que cet arrêté ne reprend pas son ancienneté acquise en qualité de militaire ; que M. B...soutient que les dispositions citées ci-dessus du code de la défense, sur lesquelles l'administration s'est fondée pour lui refuser toute reprise d'ancienneté, introduisent une inégalité de traitement entre les militaires lauréats d'un concours de la fonction publique civile selon qu'ils sont ou non placés en position de détachement à la suite de leur réussite à un concours et sont ainsi contraires au principe d'égalité garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              4. Considérant toutefois que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un ou l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; que les militaires lauréats d'un concours de l'une des fonctions publiques civiles ou d'accès à la magistrature, qui ne sont pas placés en situation de détachement, soit parce qu'ils n'en remplissent pas les conditions relatives à leur durée d'engagement dans l'armée et à l'information préalable de l'administration sur leur candidature au concours, soit parce qu'ils ont omis d'en formuler la demande, ne sont pas dans la même situation que les militaires, lauréats du même concours, qui ont sollicité ce détachement et remplissent les conditions pour l'obtenir ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la question soulevée par M.B..., qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y pas lieu, par suite, de la transmettre au Conseil constitutionnel ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Cergy-Pontoise.<br/>
Article 2 : La présente décision sera notifiée à M. A... B....<br/>
Copie en sera adressée pour information au Conseil constitutionnel, au ministre de l'intérieur, au ministre de la défense, au Premier ministre et au tribunal administratif de Cergy-Pontoise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
