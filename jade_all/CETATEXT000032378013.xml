<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032378013</ID>
<ANCIEN_ID>JG_L_2016_04_000000386762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/80/CETATEXT000032378013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 06/04/2016, 386762, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386762.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1205265 du 16 décembre 2014, enregistrée le 29 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Lille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le syndicat Force Ouvrière de la préfecture du Nord.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Lille le 6 septembre 2012, le syndicat Force Ouvrière de la préfecture du Nord demande au Conseil d'Etat d'annuler la décision du 16 juillet 2012 par laquelle le ministre de l'intérieur a rejeté sa demande tendant à ce que le régime indemnitaire des agents de la préfecture du Nord soit aligné sur celui des agents des préfectures de la région Ile-de-France. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2008-1533 du 22 décembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le syndicat Force Ouvrière de la préfecture du Nord demande l'annulation de la décision du 16 juillet 2012 par laquelle le ministre de l'intérieur a rejeté sa demande tendant à ce que le régime indemnitaire des agents de la préfecture du Nord, qu'il s'agisse de la prime de fonctions et de résultats pour les agents qui en bénéficiaient, d'une part, ou des " taux moyens d'objectifs " pour les autres agents, d'autre part, soit aligné sur celui des agents des préfectures d'Ile-de-France ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du décret du 22 décembre 2008 relatif à la prime de fonctions et de résultats : " Les fonctionnaires appartenant à des corps de la filière administrative ou détachés sur un emploi fonctionnel de cette filière peuvent percevoir une prime de fonctions et de résultats, dans les conditions fixées par le présent décret ... " ; qu'aux termes de l'article 2 du même décret : " La prime de fonctions et de résultats comprend deux parts : / - une part tenant compte des responsabilités, du niveau d'expertise et des sujétions spéciales liées aux fonctions exercées ; / - une part tenant compte des résultats de la procédure d'évaluation individuelle prévue par la réglementation en vigueur et de la manière de servir. " ; qu'aux termes de l'article 4 du même décret : " Un arrêté du ministre chargé de la fonction publique et du ministre chargé du budget ainsi que, le cas échéant, du ministre intéressé fixe pour chaque grade ou emploi, dans la limite d'un plafond : / - les montants annuels de référence de la part pouvant être attribuée au titre de la fonction ; / - les montants annuels de référence de la part liée aux résultats de la procédure d'évaluation individuelle et à la manière de servir. " ; qu'aux termes de l'article 5 du même décret : " Les montants individuels de la part fonctionnelle et de la part liée aux résultats de l'évaluation et à la manière de servir sont respectivement déterminés comme suit : / I. - S'agissant de la part fonctionnelle, l'attribution individuelle est déterminée par application au montant de référence d'un coefficient multiplicateur compris dans une fourchette de 1 à 6 au regard des responsabilités, du niveau d'expertise et des sujétions spéciales liées à la fonction exercée ... " ;<br/>
<br/>
              3. Considérant, d'une part, que la demande du syndicat requérant doit être regardée, en ce qui concerne la prime de fonctions et de résultats, comme tendant à ce que le coefficient multiplicateur prévu à l'article 5 du décret du 22 décembre 2008 fixé pour les agents en fonctions dans les préfectures de la région Ile-de-France soit également appliqué aux agents en fonctions à la préfecture du Nord ; que, toutefois, eu égard à l'intérêt général qui s'attache à ce que les agents publics soient répartis sur le territoire en fonction des besoins de la population et des nécessités du service, le ministre de l'intérieur pouvait, sans méconnaître le principe d'égalité de traitement entre agents d'un même corps, prévoir un coefficient multiplicateur différent selon le lieu d'affectation des agents et retenir un coefficient plus élevé pour les agents affectés en région Ile-de-France, afin de remédier par cette incitation financière aux déséquilibres constatés dans les demandes d'affectation et les vacances d'emploi au détriment de cette région ; qu'au regard de ce motif, que le syndicat requérant, qui se borne à comparer le nombre d'agents de préfecture par habitant dans le Nord et en région parisienne, ne critique pas utilement, il ne ressort pas des pièces du dossier que le refus du ministre de l'intérieur de faire droit à la demande du syndicat requérant serait entaché d'erreur manifeste d'appréciation ;<br/>
<br/>
              4. Considérant, d'autre part, s'agissant des " taux moyens d'objectifs ", qui au demeurant ne sauraient constituer légalement, pour chacune des catégories de personnel concernées, que des données moyennes indicatives destinées à faciliter, pour les chefs de service et les gestionnaires, la gestion des rémunérations dont ils assurent le paiement, que le ministre pouvait également, pour les mêmes motifs que ceux énoncés au point 3, indiquer des montants différents selon le lieu d'affectation des agents ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en tout état de cause, la requête du syndicat Force Ouvrière de la préfecture du Nord doit être rejetée ;<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête du syndicat Force Ouvrière de la préfecture du Nord est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat Force Ouvrière de la préfecture du Nord et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
