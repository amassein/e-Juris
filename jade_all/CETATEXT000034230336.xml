<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230336</ID>
<ANCIEN_ID>JG_L_2017_03_000000392792</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230336.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 20/03/2017, 392792</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392792</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392792.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du 6 décembre 2010 par laquelle le maire de Neuilly-sur-Seine a refusé de procéder au renouvellement de son contrat ainsi que la décision du 1er février 2011 par laquelle le maire a  rejeté son recours gracieux et a refusé de lui accorder un contrat à durée indéterminée. Par un jugement n° 1102603 du 28 janvier 2014, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 14VE00924 du 18 juin 2015, la cour administrative d'appel de Versailles a rejeté l'appel de MmeB....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 août et 20 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) subsidiairement, de renvoyer à la Cour de justice de l'Union européenne la question préjudicielle suivante : l'article 2 de la directive 1999/70/CE du 28 juin 1999 implique-t-il que le juge national soit tenu de requalifier en contrat à durée indéterminée un ensemble de contrats à durée déterminée conclus de façon abusive pour pourvoir aux besoins permanents d'une collectivité publique ou le litige peut-il se résoudre exclusivement en dommages et intérêts '<br/>
<br/>
              4°) de mettre à la charge de la commune de Neuilly-sur-Seine la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 1999/70/CE du Conseil du 28 juin 1999 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - la loi n° 2005-843 du 26 juillet 2005 ;<br/>
              - la loi n° 2009-372 du 3 août 2009 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de MmeB..., et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Neuilly-sur-Seine ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a été recrutée le 8 janvier 1999 par la commune de Neuilly-sur-Seine en qualité de rédacteur non titulaire au sein des services techniques pour une durée d'un an, sur un emploi vacant à temps plein ; que son contrat a été renouvelé annuellement ; qu'à compter du 12 janvier 2008, elle a été recrutée dans les mêmes conditions au sein de la direction des affaires culturelles ; que ce contrat a été renouvelé à deux reprises ; que, toutefois, par lettre du 6 décembre 2010, le maire lui a fait savoir que, à son terme, le 11 janvier 2011, son contrat ne serait pas renouvelé ; que, par un jugement du 28 janvier 2014, le tribunal administratif de Cergy-Pontoise a rejeté le recours pour excès de pouvoir que Mme B...avait formé contre cette décision ; que ce jugement a été confirmé par la cour administrative d'appel de Versailles par un arrêt du 18 juin 2015, contre lequel Mme B...se pourvoit en cassation ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 1er de la directive 1999/70/CE du Conseil de l'Union Européenne du 28 juin 1999 concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée : " La présente directive vise à mettre en oeuvre l'accord cadre sur le travail à durée déterminée, figurant en annexe, conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) " ; qu'aux termes de l'article 2 de cette directive : " Les États membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive au plus tard le 10 juillet 2001 ou s'assurent, au plus tard à cette date, que les partenaires sociaux ont mis en place les dispositions nécessaires par voie d'accord, les États membres devant prendre toute disposition nécessaire leur permettant d'être à tout moment en mesure de garantir les résultats imposés par la présente directive. (...) " ; qu'en vertu des stipulations de la clause 5 de l'accord-cadre annexé à la directive, relative aux mesures visant à prévenir l'utilisation abusive des contrats à durée déterminée : " 1. Afin de prévenir les abus résultant de l'utilisation de contrats ou de relations de travail à durée déterminée successifs, les États membres, après consultation des partenaires sociaux, conformément à la législation, aux conventions collectives et pratiques nationales, et/ou les partenaires sociaux, quand il n'existe pas des mesures légales équivalentes visant à prévenir les abus, introduisent d'une manière qui tienne compte des besoins de secteurs spécifiques et/ou de catégories de travailleurs, l'une ou plusieurs des mesures suivantes : a) des raisons objectives justifiant le renouvellement de tels contrats ou relations de travail ; b) la durée maximale totale de contrats ou relations de travail à durée déterminée successifs ; c) le nombre de renouvellements de tels contrats ou relations de travail. 2. Les États membres, après consultation des partenaires sociaux et/ou les partenaires sociaux, lorsque c'est approprié, déterminent sous quelles conditions les contrats ou relations de travail à durée déterminée : a) sont considérés comme "successifs" ; b) sont réputés conclus pour une durée indéterminée " ;<br/>
<br/>
              3.	Considérant qu'il résulte des dispositions de cette directive, telles qu'elles ont été interprétées par la Cour de justice de l'Union européenne, qu'elles imposent aux États membres d'introduire de façon effective et contraignante dans leur ordre juridique interne, s'il ne le prévoit pas déjà, l'une au moins des mesures énoncées aux a) à c) du paragraphe 1 de la clause 5, afin d'éviter qu'un employeur ne recoure de façon abusive au renouvellement de contrats à durée déterminée ; que lorsque l'État membre décide de prévenir les renouvellements abusifs en recourant uniquement aux raisons objectives prévues au a), ces raisons doivent tenir à des circonstances précises et concrètes de nature à justifier l'utilisation de contrats de travail à durée déterminée successifs ; qu'il ressort également de l'interprétation de la directive retenue par la Cour de justice de l'Union européenne que le renouvellement de contrats à durée déterminée afin de pourvoir au remplacement temporaire d'agents indisponibles répond, en principe, à une raison objective au sens de la clause citée ci-dessus, y compris lorsque l'employeur est conduit à procéder à des remplacements temporaires de manière récurrente, voire permanente, alors même que les besoins en personnel de remplacement pourraient être couverts par le recrutement d'agents sous contrats à durée indéterminée ; que, dès lors que l'ordre juridique interne d'un État membre comporte, dans le secteur considéré, d'autres mesures effectives pour éviter et, le cas échéant, sanctionner l'utilisation abusive de contrats de travail à durée déterminée successifs au sens du point 1 de la clause 5 de l'accord, la directive ne fait pas obstacle à l'application d'une règle de droit national interdisant, pour certains agents publics, de transformer en un contrat de travail à durée indéterminée une succession de contrats de travail à durée déterminée qui, ayant eu pour objet de couvrir des besoins permanents et durables de l'employeur, doivent être regardés comme abusifs ;<br/>
<br/>
              4.	Considérant qu'il résulte des dispositions de l'article 3 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans leur rédaction issue de la loi du 3 août 2009, applicable au litige, que les collectivités territoriales de plus de 2000 habitants ne peuvent recruter par contrat à durée déterminée des agents non titulaires que, d'une part, en vertu des premier et deuxième alinéas de cet article, en vue d'assurer des remplacements momentanés ou d'effectuer des tâches à caractère temporaire ou saisonnier définies à ces alinéas et, d'autre part, dans le cadre des dérogations au principe selon lequel les emplois permanents sont occupés par des fonctionnaires, énoncées aux quatrième, cinquième et sixième alinéas du même article, lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer certaines fonctions, ou lorsque, pour des emplois de catégorie A, la nature des fonctions ou les besoins des services le justifient ; qu'aux termes des septième et huitième alinéas du même article : " Les agents recrutés conformément aux quatrième, cinquième, et sixième alinéas sont engagés par des contrats à durée déterminée, d'une durée maximale de trois ans. Ces contrats sont renouvelables, par reconduction expresse. La durée des contrats successifs ne peut excéder six ans. / Si, à l'issue de la période maximale de six ans mentionnée à l'alinéa précédent, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée " ; <br/>
<br/>
              5.	Considérant que ces dispositions se réfèrent ainsi, s'agissant de la possibilité de recourir à des contrats à durée déterminée, à des " raisons  objectives ", de la nature de celles auxquelles la directive renvoie ; qu'elles ne font nullement  obstacle à ce qu'en cas de renouvellement abusif de contrats à durée déterminée, l'agent concerné puisse se voir reconnaître un droit à l'indemnisation du préjudice éventuellement subi lors de l'interruption de la relation d'emploi, évalué en fonction des avantages financiers auxquels il aurait pu prétendre en cas de licenciement s'il avait été employé dans le cadre d'un contrat à durée indéterminée ; que, dès lors, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que ces dispositions ne sont pas incompatibles avec les objectifs de la directive du 28 juin 1999 ; <br/>
<br/>
              6.	Considérant que, si Mme B...avait soulevé devant la cour administrative d'appel un moyen tiré de ce qu'elle entrait dans le cadre du I de l'article 15 de la loi du 26 juillet 2005 portant diverses mesures de transposition du droit communautaire à la fonction publique et aurait dû, par application de ces dispositions, bénéficier de la transformation de son contrat en contrat à durée indéterminée, la cour, qui a retenu qu'elle avait été recrutée pour remplacer des agents momentanément indisponibles et n'était donc pas titulaire de contrats entrant dans les catégories énoncées aux quatrième, cinquième et sixième alinéas de l'article 3 de la loi du 26 janvier 1984, seuls régis par les dispositions du I de l'article 15 de la loi du 26 juillet 2005, a nécessairement écarté ce moyen ; que, par suite, Mme B...n'est pas fondée à soutenir que la cour a entaché son arrêt d'une insuffisance de motivation ;<br/>
<br/>
              7.	Considérant que la cour administrative d'appel a, par une appréciation  souveraine des pièces du dossier qu'elle n'a pas dénaturées, estimé que le recrutement de Mme B... avait été effectué pour faire face au remplacement momentané d'agents et n'entrait pas dans le cadre de l'une des hypothèses prévues aux quatrième, cinquième et sixième alinéa de l'article 3 de la loi du 26 janvier 1984 dans sa rédaction alors en vigueur, pour lesquelles le législateur avait prévu, en cas de contrats successifs d'une durée cumulée supérieure à six ans, l'obligation que le renouvellement ultérieur intervienne par une décision expresse et pour une durée indéterminée ; qu'elle a pu, sans commettre d'erreur de droit, juger que la nature des fonctions exercées, le type d'organisme employeur ainsi que le nombre et la durée cumulée des contrats en cause ne donnaient pas à eux seuls à l'intéressée un droit au renouvellement de son contrat et qu'en l'absence de tout texte instaurant un droit au renouvellement des contrats à durée déterminée ou à leur transformation en contrat à durée indéterminée, le refus de renouvellement qui avait été opposé à Mme B...n'était entaché d'aucune illégalité ;<br/>
<br/>
              8.	Considérant qu'il résulte de tout ce qui précède, sans qu'il y ait lieu de renvoyer à la Cour de justice de l'Union européenne une question préjudicielle, que le pourvoi de Mme B...doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Neuilly-sur-Seine qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Neuilly-sur-Seine au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la commune de Neuilly-sur-Seine présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la commune de Neuilly-sur-Seine.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-085 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DIRECTIVE 1999/70/CE DU 28 JUIN 1999 SUR LE TRAVAIL À DURÉE DÉTERMINÉE - OBLIGATION DE PRÉVENTION DES RENOUVELLEMENTS ABUSIFS DE CDD À LA CHARGE DES ETATS MEMBRES - COMPATIBILITÉ DE L'ARTICLE 3 DE LA LOI DU 26 JANVIER 1984 PORTANT DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - ARTICLE 3 DE LA LOI DU 26 JANVIER 1984 - COMPATIBILITÉ AVEC LA DIRECTIVE 1999/70/CE DU 28 JUIN 1999 SUR LE TRAVAIL À DURÉE DÉTERMINÉE OBLIGEANT LES ETATS MEMBRES À PRÉVENIR LES RENOUVELLEMENTS ABUSIFS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 15-05-085 Directive 1999/70/CE du 28 juin 1999 obligeant les Etats membres à prévenir les renouvellements abusifs de contrats de travail à durée déterminée (CDD).... ,,L'article 3 de la loi n° 84-53 du 24 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale subordonne la conclusion et le renouvellement de contrats à durée déterminée à la nécessité de remplacer des fonctionnaires temporairement ou partiellement indisponibles. Il se réfère ainsi à une raison objective de la nature de celles auxquelles la directive renvoie.... ,,Ces dispositions ne font pas obstacle à ce qu'en cas de renouvellement abusif de contrats à durée déterminée, l'agent concerné puisse se voir reconnaître un droit à l'indemnisation du préjudice éventuellement subi lors de l'interruption de sa relation d'emploi, évalué en fonction des avantages financiers auxquels il aurait pu prétendre en cas de licenciement s'il avait été employé dans le cadre d'un contrat à durée indéterminée.... ,,Ces dispositions ne sont donc pas incompatibles avec les objectifs de la directive 1999/70/CE du 28 juin 1999.</ANA>
<ANA ID="9B"> 36-07-01-03 Directive 1999/70/CE du 28 juin 1999 obligeant les Etats membres à prévenir les renouvellements abusifs de contrats de travail à durée déterminée.... ,,L'article 3 de la loi n° 84-53 du 24 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale subordonne la conclusion et le renouvellement de contrats à durée déterminée à la nécessité de remplacer des fonctionnaires temporairement ou partiellement indisponibles. Il se réfère ainsi à une raison objective de la nature de celles auxquelles la directive renvoie.... ,,Ces dispositions ne font pas obstacle à ce qu'en cas de renouvellement abusif de contrats à durée déterminée, l'agent concerné puisse se voir reconnaître un droit à l'indemnisation du préjudice éventuellement subi lors de l'interruption de sa relation d'emploi, évalué en fonction des avantages financiers auxquels il aurait pu prétendre en cas de licenciement s'il avait été employé dans le cadre d'un contrat à durée indéterminée.... ,,Ces dispositions ne sont donc pas incompatibles avec les objectifs de la directive 1999/70/CE du 28 juin 1999.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour des dispositions équivalentes régissant la fonction publique hospitalière, CE, 20 mars 2015, Mme Julie, n° 371664, T. pp. 583-722-732.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
