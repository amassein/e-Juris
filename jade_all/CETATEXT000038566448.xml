<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566448</ID>
<ANCIEN_ID>JG_L_2019_06_000000423696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566448.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 05/06/2019, 423696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423696.20190605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Chambre FNAIM du Grand Paris, la Chambre nationale des propriétaires, la Fédération nationale de l'immobilier (FNAIM), l'Union des syndicats de l'immobilier (UNIS) et le Syndicat national des professionnels de l'immobilier (SNPI), d'une part, l'association Union nationale de la propriété immobilière (UNPI) Paris et Mme A...B..., d'autre part, ont demandé au tribunal administratif de Paris d'annuler l'arrêté du 25 juin 2015 par lequel le préfet de la région d'Ile-de-France, préfet de Paris a fixé les loyers de référence, les loyers de référence majorés et les loyers de référence minorés dans la commune de Paris. L'association " Bail à part - Tremplin pour le logement " a demandé au tribunal d'annuler ce même arrêté du 25 juin 2015 en tant que cet arrêté ne concerne pas l'ensemble des communes de l'agglomération parisienne, mais seulement la commune de Paris. L'association UNPI et Mme B...ont, en outre, demandé au tribunal administratif d'annuler les arrêtés du 20 juin 2016 et du 21 juin 2017, par lesquels le préfet de la région d'Ile-de-France, préfet de Paris a fixé les loyers de référence, les loyers de référence majorés et les loyers de référence minorés dans la commune de Paris. <br/>
<br/>
              Par un jugement nos 1511828,1513696,1514241,1612832,1711728 du 28 novembre 2017, le tribunal administratif a annulé les arrêtés du 25 juin 2015, du 20 juin 2016 et du 21 juin 2017.<br/>
<br/>
              Par un arrêt nos 17PA03805,17PA03808,18PA00339,18PA00340 du 26 juin 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par le ministre de la cohésion des territoires contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 28 août 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de la cohésion des territoires demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 89-462 du 6 juillet 1989 ; <br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le décret n° 2013-392 du 10 mai 2013 ; <br/>
              - le décret n° 2014-1334 du 5 novembre 2014 ; <br/>
              - le décret n° 2015-650 du 10 juin 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat de Mme B...et de l'association Union nationale de la propriété immobilière et à la SCP Spinosi, Sureau, avocat de la Chambre nationale des propriétaires, de la Fédération nationale de l'immobilier, de l'Union des syndicats de l'immobilier, du Syndicat national des professionnels de l'immobilier et de la Chambre FNAIM du Grand Paris ;<br/>
<br/>
              Vu la note en délibéré et le mémoire soulevant une question prioritaire de constitutionnalité, enregistrés le 16 mai 2019 après la clôture de l'instruction, présentés par Mme B... et l'association Union nationale de la propriété immobilière ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 16 de la loi du 6 juillet 1989 tendant à améliorer les rapports locatifs et portant modification de la loi n° 86-1290 du 23 décembre 1986, dans sa rédaction issue de la loi n° 2014-366 du 24 mars 2014, antérieure à sa modification par la loi n° 2018-1021 du 23 novembre 2018 : " Des observatoires locaux des loyers peuvent être créés à l'initiative des collectivités territoriales, des établissements publics de coopération intercommunale à fiscalité propre compétents en matière d'habitat ou de l'Etat. Ces observatoires ont notamment pour mission de recueillir les données relatives aux loyers sur une zone géographique déterminée et de mettre à la disposition du public des résultats statistiques représentatifs sur ces données ". Aux termes du I de l'article 17 de cette même loi, dans sa rédaction issue de la loi du 24 mars 2014 : " Les zones d'urbanisation continue de plus de 50 000 habitants où il existe un déséquilibre marqué entre l'offre et la demande de logements, entraînant des difficultés sérieuses d'accès au logement sur l'ensemble du parc résidentiel existant, qui se caractérisent notamment par le niveau élevé des loyers, le niveau élevé des prix d'acquisition des logements anciens ou le nombre élevé de demandes de logement par rapport au nombre d'emménagements annuels dans le parc locatif social, sont dotées d'un observatoire local des loyers prévu à l'article 16 de la présente loi. Un décret fixe la liste des communes comprises dans ces zones./ Dans ces zones, le représentant de l'Etat dans le département fixe chaque année, par arrêté, un loyer de référence, un loyer de référence majoré et un loyer de référence minoré, exprimés par un prix au mètre carré de surface habitable, par catégorie de logement et par secteur géographique./ Les catégories de logement et les secteurs géographiques sont déterminés en fonction de la structuration du marché locatif constatée par l'observatoire local des loyers./ Chaque loyer de référence est égal au loyer médian calculé à partir des niveaux de loyers constatés par l'observatoire local des loyers selon les catégories de logement et les secteurs géographiques./ Chaque loyer de référence majoré et chaque loyer de référence minoré sont fixés respectivement par majoration et par minoration du loyer de référence./ Les compétences attribuées au représentant de l'Etat dans le département par le présent article sont exercées, dans la région d'Ile-de-France, par le représentant de l'Etat dans la région./ Le loyer de référence majoré est égal à un montant supérieur de 20 % au loyer de référence./ Le loyer de référence minoré est égal au loyer de référence diminué de 30 % ".<br/>
<br/>
              2.	Aux termes de l'article 1er du décret du 10 juin 2015 relatif aux modalités de mise en oeuvre du dispositif d'encadrement du niveau de certains loyers et modifiant l'annexe à l'article R. 366-5 du code de la construction et de l'habitation : " Les communes comprises dans les zones mentionnées au I de l'article 17 de la loi du 6 juillet 1989 susvisée sont celles qui figurent sur la liste annexée au décret du 10 mai 2013 susvisé ". Aux termes de l'article 2 de ce décret : " Les catégories de logement et les secteurs géographiques mentionnés au I de l'article 17 de la loi du 6 juillet 1989 susvisée sont déterminés selon les modalités suivantes : (...) 2° Les secteurs géographiques délimitent des zones homogènes en termes de niveaux de loyer constatés sur le marché locatif ".<br/>
<br/>
              3.	Il appartenait au préfet de mettre en oeuvre, dans un délai raisonnable, le dispositif d'encadrement des loyers dans les zones définies au I de l'article 17 de la loi du 17 juillet 1989. Il ne pouvait toutefois fixer des loyers de référence qu'à la condition de disposer des données relatives aux niveaux des loyers constatés par l'observatoire local des loyers, après avoir, le cas échéant, suscité la création d'une telle structure et obtenu ses premières séries de données. Dans le cas où les données nécessaires n'étaient disponibles que pour une partie seulement des secteurs géographiques inclus dans une zone, il était possible au préfet, sans que le principe d'égalité y fasse obstacle, de mettre en oeuvre le dispositif dans cette partie des secteurs géographiques, dès lors que l'application de la loi dans ces seuls secteurs n'était pas de nature à créer un risque sérieux de distorsion vis-à-vis du marché immobilier des secteurs limitrophes, susceptible de compromettre l'objectif poursuivi par le législateur.<br/>
<br/>
              4.	Il ressort des pièces du dossier soumis aux juges du fond que l'autorité administrative disposait dès 2015, pour identifier les catégories de logement et secteurs géographiques sur le territoire de la commune de Paris, des travaux réalisés, avant même l'entrée en vigueur de la loi du 24 mars 2014, par l'Office des loyers de l'agglomération parisienne. Les données relatives aux autres communes de la zone n'étaient, en revanche, pas disponibles à cette date, l'agrément de cet office n'ayant été étendu au territoire de la zone d'urbanisation qui englobe Paris et 412 communes des départements limitrophes que le 29 juin 2016. Il ressort par ailleurs des pièces du dossier soumis aux juges du fond que la commune de Paris a connu une augmentation particulièrement sensible des loyers, en valeur absolue comme en valeur relative, depuis l'année 2000. En outre, au regard des spécificités du marché locatif à Paris, et notamment de l'attractivité de la capitale qui constitue par elle-même un facteur de renchérissement des loyers sur son territoire, la fixation des loyers de références dans les quatorze secteurs définis au sein du territoire de Paris n'était pas de nature à créer un risque sérieux de distorsion vis-à-vis du marché immobilier des secteurs limitrophes. Dans ces conditions, il était possible au préfet de la région Ile-de-France, préfet de Paris de mettre en oeuvre, ainsi qu'il a été dit au point 3, le dispositif d'encadrement des loyers du I de l'article 17 de la loi du 17 juillet 1989 pour la seule commune de Paris à la date à laquelle il a pris les arrêtés contestés. Par suite, en jugeant que le préfet avait méconnu l'article 17 de la loi du 6 juillet 1989 en ne fixant, dans un premier temps, les loyers de référence, les loyers de référence majorés et les loyers de référence minorés que dans la commune de Paris, la cour administrative d'appel a commis une erreur de droit qui justifie l'annulation de son arrêt.<br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, le versement d'une somme au titre des frais exposés par Mme B...et l'association UNPI d'une part, et par la chambre FNAIM du Grand Paris, la FNAIM, l'UNIS, le SNPI et la chambre nationale des propriétaires d'autre part, et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 26 juin 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées par Mme B...et l'association UNPI d'une part, et par la chambre FNAIM du Grand Paris, la FNAIM, l'UNIS, le SNPI et la chambre nationale des propriétaires d'autre part, au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, à la Chambre FNAIM du Grand Paris et à l'association Union nationale de la propriété immobilière, premiers défendeurs dénommés, et à l'association  " Bail à part - Tremplin pour le logement ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
