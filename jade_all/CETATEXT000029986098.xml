<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029986098</ID>
<ANCIEN_ID>JG_L_2014_12_000000377293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/60/CETATEXT000029986098.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 29/12/2014, 377293, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:377293.20141229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 9 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense ; le ministre de la défense demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12/04244 du 4 février 2014 par lequel la cour régionale des pensions de Versailles a annulé le jugement n° 05/00052 du 8 avril 2008 du tribunal départemental des pensions de Paris faisant droit à la demande de Mme D...A...tendant à la décristallisation de sa pension de réversion, s'est déclarée incompétente pour connaître de la demande et a renvoyé l'affaire devant le tribunal administratif de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le code civil ; <br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 59-1454 du 26 décembre 1959 ;<br/>
<br/>
              Vu la loi n° 79-1102 du 21 décembre 1979 ;<br/>
<br/>
              Vu la loi n° 2002-1576 du 30 décembre 2002 ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
<br/>
              Vu la décision n° 2010-1 QPC du 28 mai 2010 du Conseil constitutionnel ;<br/>
<br/>
              Vu la décision n° 2010-108 QPC du 25 mars 2011 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...C..., ressortissant sénégalais ayant servi dans l'armée française du 16 janvier 1947 au 1er octobre 1953, a été admis par arrêté du 8 juillet 1980 à compter du 1er janvier 1975 au bénéfice d'une pension militaire d'invalidité, consistant en une indemnité personnelle et viagère en application des dispositions de l'article 71 de la loi du 26 décembre 1959 de finances pour 1960 ; que M. C...est décédé le 23 avril 1998 ; que sa veuve, Mme D...A..., ressortissante sénégalaise, a sollicité le bénéfice d'une pension de réversion du chef de son époux décédé par une demande reçue le 2 août 2004 ; que, par arrêté du 21 mars 2005, le ministre de la défense a accordé à MmeA..., avec jouissance rétroactive à compter du 1er janvier 2002, une pension de réversion calculée sur le fondement des dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002 ; que, par une demande formée le 17 août 2005, Mme A...a contesté cette décision ; que, par un jugement du 8 avril 2008, le tribunal départemental des pensions de Paris a fait droit à sa demande et enjoint à l'administration de réviser ses droits à pension ; que, toutefois, par un arrêt du 25 mars 2010, la cour régionale des pensions de Paris a infirmé ce jugement ; que Mme A...s'est pourvue en cassation devant le Conseil d'Etat qui, par une décision du 3 octobre 2012, a annulé l'arrêt de la cour régionale des pensions de Paris et a renvoyé l'affaire devant la cour régionale des pensions de Versailles ; que, par un arrêt du 4 février 2014, celle-ci a annulé le jugement du tribunal départemental des pensions de Paris, s'est déclarée incompétente pour connaître de la demande de Mme A...et a renvoyé l'affaire devant le tribunal administratif de Paris ; que le ministre se pourvoit contre ce dernier arrêt ;<br/>
<br/>
              2.	Considérant que si le contentieux des pensions mixtes de retraite et d'invalidité, prévues par le code des pensions civiles et militaires de retraite, relève de la compétence du juge administratif de droit commun, sous réserve des questions relatives à l'existence, à l'origine médicale et au degré de l'invalidité qui doivent être tranchées par la juridiction des pensions, les contestations soulevées par l'application du livre Ier, à l'exception des chapitres I et IV du titre VII, et du livre II du code des pensions militaires d'invalidité et des victimes de la guerre relèvent de la compétence des juridictions de pensions, en vertu des dispositions de l'article L. 79 de ce code ; qu'il en va de même pour les litiges relatifs à l'indemnité viagère annuelle prévue par l'article 71 de la loi du 26 décembre 1959 portant loi de finances pour 1960 lorsque cette indemnité remplace une pension militaire d'invalidité ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, lors de son décès le 23 avril 1998, M.C..., de nationalité sénégalaise, était titulaire d'une indemnité viagère annuelle prévue par l'article 71 de la loi du 26 décembre1959 qui remplaçait une pension militaire d'invalidité ; que la pension de réversion dont est titulaire Mme A... du chef de son époux décédé revêt le même caractère ; que la contestation formée par Mme A...relève ainsi de la compétence des juridictions de pensions ; qu'il s'ensuit qu'en annulant d'office pour incompétence le jugement du 8 avril 2008 du tribunal départemental des pensions de Paris statuant sur la demande de MmeA..., la cour régionale des pensions de Versailles a entaché son arrêt d'une erreur de droit ; que son arrêt doit, par suite, être annulé ;<br/>
<br/>
              4.	Considérant qu'il incombe au Conseil d'Etat, en vertu du second alinéa de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              5.	Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation " ; qu'aux termes du deuxième alinéa de son article 62 : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause " ; qu'enfin, aux termes du troisième alinéa du même article : " Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles " ;<br/>
<br/>
              6.	Considérant qu'il résulte des dispositions précitées de l'article 62 de la Constitution qu'une disposition législative déclarée contraire à la Constitution sur le fondement de l'article 61-1 n'est pas annulée rétroactivement mais abrogée pour l'avenir à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision ; que, par sa décision n° 2010-108 QPC en date du 25 mars 2011, le Conseil constitutionnel a jugé que " si, en principe, la déclaration d'inconstitutionnalité doit bénéficier à l'auteur de la question prioritaire de constitutionnalité et la disposition déclarée contraire à la Constitution ne peut être appliquée dans les instances en cours à la date de la publication de la décision du Conseil constitutionnel, les dispositions de l'article 62 de la Constitution réservent à ce dernier le pouvoir tant de fixer la date de l'abrogation et reporter dans le temps ses effets que de prévoir la remise en cause des effets que la disposition a produits avant l'intervention de cette déclaration " ; <br/>
<br/>
              7.	Considérant que, lorsque le Conseil constitutionnel, après avoir abrogé une disposition déclarée inconstitutionnelle, use du pouvoir que lui confèrent les dispositions précitées, soit de déterminer lui-même les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause, soit de décider que le législateur aura à prévoir une application aux instances en cours des dispositions qu'il aura prises pour remédier à l'inconstitutionnalité constatée, il appartient au juge, saisi d'un litige relatif aux effets produits par la disposition déclarée inconstitutionnelle, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, cette disposition, dans les conditions et limites fixées par le Conseil constitutionnel ou le législateur ; <br/>
<br/>
              8.	Considérant que, par sa décision n° 2010-1 QPC du 28 mai 2010, le Conseil constitutionnel a déclaré contraires à la Constitution les dispositions de l'article 68 de la loi du 30 décembre 2002 de finances rectificative pour 2002, à l'exception de celles de son paragraphe VII ; qu'il a jugé que : " afin de permettre au législateur de remédier à l'inconstitutionnalité constatée, l'abrogation des dispositions précitées prendra effet à compter du 1er janvier 2011 ; afin de préserver l'effet utile de la présente décision à la solution des instances actuellement en cours, il appartient, d'une part, aux juridictions de surseoir à statuer jusqu'au 1er janvier 2011 dans les instances dont l'issue dépend de l'application des dispositions déclarées inconstitutionnelles et, d'autre part, au législateur de prévoir une application des nouvelles dispositions à ces instances en cours à la date de la présente décision " ;<br/>
<br/>
              9.	Considérant que, à la suite de cette décision, l'article 211 de la loi du 29 décembre 2010 de finances pour 2011 a défini de nouvelles dispositions pour le calcul des pensions militaires d'invalidité, des pensions civiles et militaires de retraite et des retraites du combattant servies aux ressortissants des pays ou territoires ayant appartenu à l'Union française ou à la Communauté ou ayant été placés sous le protectorat ou sous la tutelle de la France et abrogé plusieurs dispositions législatives, notamment celles de l'article 71 de la loi du 26 décembre 1959 portant loi de finances pour 1960 ; que, par ailleurs, son paragraphe VI prévoit que " le présent article est applicable aux instances en cours à la date du 28 mai 2010, la révision des pensions prenant effet à compter de la date de réception par l'administration de la demande qui est à l'origine de ces instances " ; qu'enfin, aux termes du XI du même article : " Le présent article entre en vigueur au 1er janvier 2011 " ;<br/>
<br/>
              10.	Considérant que, comme il a été dit, le Conseil constitutionnel a jugé qu'il appartenait au législateur de prévoir une application aux instances en cours à la date de sa décision des dispositions qu'il adopterait en vue de remédier à l'inconstitutionnalité constatée ; que l'article 211 de la loi de finances pour 2011 ne se borne pas à déterminer les règles de calcul des pensions servies aux personnes qu'il mentionne, mais abroge aussi des dispositions qui définissent, notamment, les conditions dans lesquelles est ouvert le droit à une pension de réversion ; qu'ainsi, alors même qu'il mentionne seulement la " révision des pensions ", le paragraphe VI de l'article 211 précité doit être regardé comme s'appliquant aussi aux demandes de pension de réversion ;<br/>
<br/>
              11.	Considérant qu'ainsi qu'il a été dit ci-dessus, les dispositions de l'article 71 de la loi du 26 décembre 1959 et celles de l'article 68 de la loi du 30 décembre 2002, qui définissaient, à la date du jugement attaqué, les conditions dans lesquelles un droit à pension de réversion était ouvert à la veuve d'un ayant droit étranger, ont été abrogées à compter du 1er janvier 2011, les premières par l'article 211 de la loi de finances pour 2011, les secondes par la décision du Conseil constitutionnel du 28 mai 2010 ; qu'en application du VI de l'article 211 de la loi de finances pour 2011, dont la portée a été précisée ci-dessus, il y a lieu d'écarter ces dispositions législatives pour statuer sur le droit à pension de réversion de Mme A...à compter de la date de réception de sa demande par l'administration, soit à compter du 2 août 2004 ;<br/>
<br/>
              Sur la période postérieure au 6 novembre 2012 : <br/>
<br/>
              12.	Considérant qu'il résulte de l'instruction qu'il a été procédé à la révision de la pension de réversion de Mme A...et aux rappels d'arrérages correspondants à compter du 6 novembre 2012, en application des dispositions de l'article 211 de la loi du 29 décembre 2010 ; que Mme A...a ainsi bénéficié, pour la période postérieure à cette date, du rétablissement du taux de droit commun, conformément à sa demande ; que, par suite, ses conclusions tendant à la revalorisation du montant de sa pension de retraite sont devenues sans objet dans cette mesure ; que, dès lors, il n'y a pas lieu d'y statuer ; <br/>
<br/>
              Sur la période comprise entre le 2 août 2004 et le 6 novembre 2012 : <br/>
<br/>
              13.	Considérant que les dispositions de l'article 71 de la loi du 26 décembre 1959 de finances pour 1960 ainsi que celles de l'article 14 de la loi de finances rectificative pour 1979 du 21 décembre 1979 et celles de l'article 68 de la loi du 30 décembre 2002, qui définissaient, à la date de la décision attaquée, les conditions dans lesquelles un droit à pension de réversion était ouvert et liquidé à la veuve d'un ayant droit étranger, ont été abrogées à compter du 1er janvier 2011, les premières par l'article 211 de la loi du 29 décembre 2010, les secondes par la décision du Conseil constitutionnel du 28 mai 2010 ; qu'en application du VI de l'article 211 de la loi du 29 décembre 2010, il y a lieu d'écarter ces dispositions législatives pour statuer sur le droit à pension de réversion de Mme A...sur la période courant à compter de la date de réception de sa demande par l'administration, soit à compter du 2 août 2004 ; qu'il résulte par ailleurs du II et du IV de ce même article que les indices et la valeur du point d'indice servant au calcul des pensions servies aux conjoints survivants des titulaires d'une pension militaire de retraite sont désormais égaux aux indices et à la valeur du point d'indice applicables aux prestations de même nature servies aux ressortissants français en application du code des pensions militaires d'invalidité et des victimes de la guerre ; que ces règles sont applicables pour le calcul de la pension de Mme A...qui a, par suite, droit à ce que le taux de sa pension de réversion soit ainsi calculé pour la période comprise entre le 2 août 2004 et le 6 novembre 2012 dans les conditions qui viennent d'être rappelées ;<br/>
<br/>
              Sur la période antérieure au 2 août 2004 :<br/>
<br/>
              14.	Considérant que si Mme A...soutient qu'elle a droit à une pension de réversion à compter du 23 avril 1998, date de décès de son époux, il résulte du I de l'article 68 de la loi du 30 décembre 2002 que : " Les prestations servies en application des articles (...) 71 de la loi de finances pour 1960 (...) sont calculées dans les conditions prévues aux paragraphes suivants " ; que le II de ce même article prévoit que les prestations sont calculées en fonction des parités relatives de pouvoir d'achat entre la France et l'Etat de résidence lors de la liquidation initiale des droits à réversion ; qu'aux termes du VI du même article : " Les prestations servies en application des textes visés au I peuvent faire l'objet, à compter du 1er janvier 2002 et sur demande, d'une réversion. L'application du droit des pensions aux intéressés et la situation de famille sont appréciées à la date d'effet des dispositions visées au I pour chaque Etat concerné " ; qu'il résulte de ces dispositions que le droit à la réversion d'une pension militaire d'invalidité versée à un ressortissant sénégalais en application des dispositions combinées du I de l'article 71 de la loi du 26 décembre 1959 et de l'article 14 de la loi du 21 décembre 1979 ne saurait être reconnu pour une période antérieure au 1er janvier 2002, alors même que le décès du titulaire du droit à pension serait intervenu avant cette date ;<br/>
<br/>
              15.	Considérant toutefois que Mme A...soutient que les dispositions du VI de l'article 68 de la loi du 30 décembre 2002 sont incompatibles avec les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention, en ce qu'elles font obstacle à ce que les droits à réversion soient ouverts à une date antérieure au 1er janvier 2002 ; <br/>
<br/>
              16.	Considérant qu'aux termes de l'article 1er du premier protocole additionnel à cette convention : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes " ; qu'aux termes de l'article 14 de cette convention : " La jouissance des droits et libertés reconnus dans la présente convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation " ;<br/>
<br/>
              17.	Considérant, d'une part, que le code des pensions militaires d'invalidité et des victimes de la guerre prévoit que la pension servie à un ayant droit est, en principe, réversible, notamment au profit de sa veuve ; qu'ainsi qu'il a été dit, Mme A...est, depuis le 23 avril 1998, veuve d'un militaire titulaire d'une pension concédée en application de ce code ; que, par suite, si la loi applicable exclut pour elle, sur le seul fondement d'un critère relatif à la nationalité du titulaire de la pension, le bénéfice d'une pension de réversion à compter de cette date, MmeA..., qui remplit la condition d'être veuve d'un titulaire d'une pension, peut se prévaloir d'un droit patrimonial, qui doit être regardé comme un bien au sens des stipulations précitées de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, et peut demander au juge d'écarter l'application des dispositions du VI de l'article 68 de la loi du 30 décembre 2002 en invoquant leur incompatibilité avec les stipulations de l'article 14 de la convention ;<br/>
<br/>
              18.	Considérant, d'autre part, qu'une distinction entre des personnes placées dans une situation analogue est discriminatoire, au sens des stipulations de l'article 14 de la convention, si elle n'est pas assortie de justifications objectives et raisonnables, c'est-à-dire si elle ne poursuit pas un objectif d'utilité publique ou si elle n'est pas fondée sur des critères objectifs et rationnels en rapport avec les buts de la loi ; que les pensions de retraite constituent, pour les militaires et agents publics, des allocations pécuniaires destinées à leur assurer, ou à assurer à leurs ayants cause, des conditions matérielles de vie en rapport avec la dignité des fonctions précédemment exercées par ces militaires et agents ; que la différence de situation existant entre des ayants cause d'anciens militaires et agents publics de la France, selon que ceux-ci ont la nationalité française ou sont ressortissants d'Etats devenus indépendants, ne justifie pas, eu égard à l'objet des pensions de réversion, une différence de traitement ; que cette différence de traitement ne peut être regardée comme reposant sur un critère en rapport avec l'objectif de la loi du 30 décembre 2002 ; que les dispositions du VI de l'article 68 de cette loi étant, de ce fait, incompatibles avec les stipulations précitées de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il y a lieu d'en écarter l'application au présent litige ;<br/>
<br/>
              19.	Considérant toutefois qu'aux termes de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre, dont se prévaut le ministre de la défense et des anciens combattants : " Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la troisième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages, afférents à l'année au cours de laquelle la demande a été déposée et aux trois années antérieures " ; que Mme A...ayant déposé sa demande de pension de réversion le 2 août 2004, les droits de celle-ci au rappel des arrérages de sa pension se limitent, en tout état de cause, à la période postérieure au 1er janvier 2001 ;<br/>
<br/>
              20.	Considérant qu'il résulte de ce qui précède que Mme A...a droit à une pension de réversion conformément aux dispositions prévues par l'article 211 de la loi de finances pour 2011 à compter du 1er janvier 2001 ; qu'en revanche, le surplus de sa demande doit être rejeté ;<br/>
<br/>
              Sur les intérêts et leur capitalisation :<br/>
<br/>
              21.	Considérant que Mme A...a demandé le versement des intérêts sur les rappels d'arrérages de la pension qui lui ont été illégalement refusés ; qu'il y a lieu de faire droit à ces conclusions, à compter de la réception, par l'administration, de sa première demande de concession de sa pension du 2 août 2004, pour les arrérages dus à cette date, puis au fur et à mesure de l'échéance des arrérages ; qu'elle a simultanément demandé la capitalisation des intérêts afférents à ces arrérages ; qu'à cette date, il n'était pas dû au moins une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande, seulement à compter du 2 août 2005 et à chaque échéance annuelle à compter de cette dernière date ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Versailles du 4 février 2014 et le jugement du tribunal départemental des pensions de Paris du 8 avril 2008 sont annulés.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de Mme A...tendant à la condamnation de l'Etat à lui verser le rappel d'arrérages de sa pension au titre de la période postérieure au 6 novembre 2012.<br/>
Article 3 : L'Etat versera à Mme A...une pension de réversion du chef de son époux décédé à compter du 1er janvier 2001 dans les conditions fixées par la présente décision.<br/>
Article 4 : Les arrérages versés pour la période postérieure au 1er janvier 2001 porteront intérêts au taux légal à compter de la réception, par l'administration, de la demande du 2 août 2004 et seront capitalisés au 2 août 2005 et à chaque échéance annuelle ultérieure.<br/>
Article 5 : Le surplus des conclusions de la demande de Mme A...présentée devant le tribunal départemental des pensions de Paris est rejeté.<br/>
Article 6 : La présente décision sera notifiée au ministre de la défense et à Mme D...A..., veuveC.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
