<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027647265</ID>
<ANCIEN_ID>JG_L_2013_06_000000358329</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/64/72/CETATEXT000027647265.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 28/06/2013, 358329, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358329</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Michel Bart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:358329.20130628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>  Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 6 avril 2012 et le  3 juillet 2012, présentés pour M. B... A..., demeurant au... ; M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 10015626 du 9 février 2012 par laquelle la Cour nationale du droit d'asile a rejeté sa demande tendant à l'annulation de la décision du 25 juin 2010 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile et à ce que lui soit reconnu le statut de réfugié ou, à défaut, la protection subsidiaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève relative au statut des réfugiés ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Michel Bart, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...A...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que M.A..., de nationalité irakienne, a sollicité le 12 février 2009 son admission au bénéfice de l'asile auprès de l'Office français de protection des réfugiés et apatrides qui la lui a refusé, par décision du 25 juin 2010, sur le fondement de la clause d'exclusion prévue au b) du F de l'article 1er de la convention de Genève relative au statut des réfugiés ; que, saisie par M.A..., la Cour nationale du droit d'asile a confirmé, par décision du 9 février 2012, le rejet de la demande d'asile sur le fondement du 2° du A de l'article 1er de la même convention en jugeant non fondées les craintes de persécution alléguées en cas de retour de l'intéressé dans son pays ; que M. A...se pourvoit en cassation contre cette décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'en vertu du 2° du A de l'article 1er de la convention de Genève relative au statut des réfugiés, la qualité de réfugié est notamment reconnue à toute personne : " qui (...) craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de son pays " ; qu'aux termes du F de l'article 1er de cette même convention : " Les dispositions de cette convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : / (...) b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiées (...) ; " ; d'autre part, qu'aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié (...) et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes (...) b) La torture ou des peines ou traitements inhumains ou dégradants (...) " ; et qu'aux termes de l'article L. 712-2 de ce même code : " La protection subsidiaire n'est pas accordée à une personne s'il existe des raisons sérieuses de penser : (...) b) Qu'elle a commis un crime grave de droit commun (...) ; "<br/>
<br/>
              3. Considérant que si, pour soutenir que la procédure menée devant la Cour nationale du droit d'asile a été irrégulière, M. A...invoque la méconnaissance du principe du contradictoire et des droits de la défense qu'aurait constitué le fait pour la cour d'avoir fondé sa décision sur l'appréciation de la réalité des craintes de persécution qu'il encourrait en cas de retour en Irak alors que l'Office français de protection des réfugiés et apatrides n'a jamais contesté la réalité de ces craintes et n'a fondé sa décision de rejet de sa demande d'admission au statut de réfugié que sur le seul fondement de la clause d'exclusion prévue au b) du F de l'article 1er de la convention de Genève, il est constant que le recours ouvert aux personnes prétendant à la qualité de réfugié a le caractère d'un recours de plein contentieux et implique pour la Cour nationale du droit d'asile, dès lors qu'elle est saisie d'une contestation d'une décision prise par l'Office français de protection des réfugiés et apatrides, de se prononcer elle-même sur le droit des intéressés à la qualité de réfugié au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue ; que les parties sont à même de discuter devant elle la matérialité des faits qu'ils allèguent ; que la circonstance que l'Office français de protection des réfugiés et apatrides ait, avant de statuer sur la demande, regardé les craintes alléguées comme établies n'interdit nullement à la cour d'apprécier souverainement les faits qui lui sont soumis, sans qu'il soit nécessaire, lorsqu'elle les estime non établis, d'en prévenir les parties, dès lors que par nature la saisine de la cour a pour objet de lui soumettre ces faits pour qu'elle détermine s'ils sont susceptibles de fonder la protection demandée ; qu'ainsi le moyen tiré de ce que la Cour nationale du droit d'asile a méconnu le principe du contradictoire, les droits de la défense et le droit à recours effectif constitutionnellement garanti ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant qu'en jugeant, après avoir indiqué de manière circonstanciée les différents éléments qui l'ont conduite à la solution retenue, parmi lesquels figurait, parmi d'autres, la possibilité pour M. A...de se placer sous la protection de son mouvement en cas de retour en Irak, que les craintes qu'énonçait ce dernier n'apparaissaient fondées ni au regard de la convention de Genève, ni au regard de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif à la protection subsidiaire, la Cour nationale du droit d'asile n'a pas entaché sa décision de contradiction pour avoir estimé établi le passé du requérant, mais non fondées ses craintes en cas de retour,  a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et ne leur a pas donné une qualification juridique erronée, la seule circonstance que l'OFPRA n'avait pas porté la même appréciation étant sans incidence sur ce point ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile contestée ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Office français de protection des réfugiés et apatrides qui n'est pas, dans la présente instance, la partie perdante ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                               --------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Monsieur B... A...A...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
