<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367635</ID>
<ANCIEN_ID>JG_L_2021_10_000000450998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 20/10/2021, 450998, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450998.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. N... U... a demandé au tribunal administratif de Marseille d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 dans la commune de Digne-les-Bains (Alpes-de-Haute-Provence) en vue de l'élection des conseillers municipaux et communautaires. Par un jugement n° 2004918 du 23 février 2021, le tribunal administratif a fait droit à sa protestation et a annulé ces opérations électorales.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 23 mars et le 10 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme AK... AU..., M. V... B..., Mme H... AS..., M. P... D..., Mme Y... AR..., M. R... AD..., Mme Q... AO..., M. I... AA..., Mme AG... AT..., M. T... F..., Mme K... AN..., M. T... AN..., Mme AI... E..., M. W... AC..., Mme AI... O..., M. T... G..., Mme X... AF..., M. AE... AJ..., Mme AM... AP..., Mme AH... AQ..., M. T... J..., Mme AB... AL... et M. Z... L... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) à titre subsidiaire, de désigner toute personne de son choix afin d'auditionner les électeurs ayant voté dans le bureau n° 3 sous le n° 617, dans le bureau n° 6 sous le n° 756, dans le bureau n° 7 sous le n° 1051, dans le bureau n° 8 sous les numéros 176, 211, 333, 574, dans le bureau n° 10 sous le n° 826, dans le bureau n° 11 sous les numéros 249, 305, 508 et 771, dans le bureau n° 12 sous le n° 718, ou d'inviter ces électeurs à présenter des observations orales devant la juridiction, s'agissant du point de savoir si elles ont personnellement voté et émargé lors du scrutin des 15 mars et 28 juin 2020 ;<br/>
<br/>
              3°) de mettre à la charge de M. U... le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 septembre 2021, présentée par Mme AU... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
<br/>
              1. A l'issue du second tour des élections municipales et communautaires qui s'est déroulé le 28 juin 2020 en vue de l'élection des conseillers municipaux et communautaires de la commune de Digne-les-Bains (Alpes-de-Haute-Provence), la liste " Ambitions pour Digne-les-Bains " conduite par Mme AU... a obtenu 1 699 voix, soit 32,49 % des suffrages exprimés, tandis que la liste " Terre dignoise - devoir d'agir ", conduite par M. U... a obtenu 1 696 voix, soit 32,43 % des suffrages exprimés. Par un jugement du 23 février 2021, le tribunal administratif de Marseille a fait droit à la protestation de M. U... et annulé les opérations électorales du 15 mars et du 28 juin 2020, au motif que quatorze suffrages des opérations électorales du 28 juin 2020 étaient irréguliers, excédant l'écart des voix entre les deux listes arrivées en tête. Mme AU... et autres relèvent appel de ce jugement. <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 741-7 du code de justice administrative : " Dans les tribunaux administratifs et les cours administratives d'appel, la minute de la décision est signée par le président de la formation de jugement, le rapporteur et le greffier d'audience ". Il ressort des pièces de la procédure que la minute du jugement comporte la signature de la présidente de la 7ème chambre du tribunal administratif de Marseille, du rapporteur et du greffier. Il s'ensuit que le moyen tiré de ce que le jugement serait irrégulier faute de comporter les signatures requises manque en fait.<br/>
<br/>
              3. En deuxième lieu, en relevant que les signatures de treize électeurs, qu'il a précisément identifiés, présentaient des différences significatives inexpliquées entre les deux tours, et en en déduisant que les suffrages correspondants étaient irréguliers et devaient être retranchés du nombre des suffrages obtenus par la liste conduite par Mme AU..., le tribunal administratif a suffisamment motivé son jugement, contrairement à ce qui est soutenu. <br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              En ce qui concerne le vote de Mme AP... :<br/>
<br/>
              4. Aux termes de l'article R. 76-1 du code électoral : " Au fur et à mesure de la réception des volets de procuration, le maire inscrit sur un registre ouvert à cet effet les noms et prénoms du mandant et du mandataire, le nom et la qualité de l'autorité qui a dressé l'acte de procuration et la date de son établissement ainsi que la durée de validité de la procuration. Le registre est tenu à la disposition de tout électeur requérant. / Le défaut de réception par le maire du volet d'une procuration fait obstacle à ce que le mandataire participe au scrutin ".<br/>
<br/>
              5. Il résulte de l'instruction qu'une procuration de Mme M... en faveur de Mme AP..., établie en temps utile au commissariat de police de Digne-les-Bains, le mercredi 24 juin 2020, a été adressée par erreur à la mairie de Mulhouse, commune de naissance A... la mandante. Sollicité par le service des élections de la commune de Digne-les-Bains le jour du scrutin, le commissariat de police a transmis une copie du registre des procurations, et confirmé l'erreur d'expédition. Il a par ailleurs été fait mention au procès-verbal du bureau de vote n° 4 de ces éléments et de la décision prise d'autoriser Mme AP... à voter au nom de Mme M.... Mme AU... est ainsi fondée à soutenir que ce vote ne pouvait être regardé, dans les circonstances de l'espèce, comme irrégulier.<br/>
<br/>
              En ce qui concerne la régularité des émargements du second tour de scrutin :<br/>
<br/>
              6. Aux termes du troisième alinéa de l'article L. 62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement ". Aux termes du deuxième alinéa de l'article L. 64 de ce code : " Lorsqu'un électeur se trouve dans l'impossibilité de signer, l'émargement prévu par le troisième alinéa de l'article L. 62-1 est apposé par un électeur de son choix qui fait suivre sa signature de la mention suivante : l'électeur ne peut signer lui-même ". Aux termes de l'article R. 76 de ce code : " A la réception d'une procuration dont la validité n'est pas limitée à un seul scrutin, le maire inscrit sur la liste électorale, à l'encre rouge, à côté du nom du mandant, celui du mandataire (...) / Les indications portées à l'encre rouge sur la liste électorale sont reproduites sur la liste d'émargement. / A la réception d'une procuration valable pour un seul scrutin, le maire porte ces indications sur la liste d'émargement seulement (...) ". Il résulte de ces dispositions, destinées à assurer la sincérité des opérations électorales, que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf cas d'impossibilité dûment mentionnée sur la liste d'émargement. Ainsi, la constatation d'un vote par l'apposition, sur la liste d'émargement, soit d'une croix, soit d'une signature qui présente des différences manifestes entre les deux tours de scrutin sans qu'il soit fait mention d'un empêchement de l'électeur de signer lui-même ou d'un vote par procuration, ne peut être regardée comme garantissant l'authenticité de ce vote.<br/>
<br/>
              7. Il résulte de l'examen des listes d'émargement et des justificatifs produits devant le Conseil d'Etat que, sur les treize émargements jugés irréguliers par le tribunal administratif, si les signatures des électeurs ayant voté sous le n° 756 dans le bureau n° 6, sous le n° 176 dans le bureau n° 8, sous le n° 333 dans le bureau n° 9, sous les n°s 249 et 771 dans le bureau n° 11 et sous le n° 718 dans le bureau n° 12 présentent des différences manifestes entre les deux tours de scrutin, la signature figurant sur la liste d'émargement pour le second tour est, pour chacun, identique à celle figurant sur la copie de la carte nationale d'identité produite à l'appui de l'attestation par laquelle ils assurent être l'auteur de leur vote. Par ailleurs, s'agissant de l'électeur ayant voté sous le n° 508 dans le bureau n° 11, la signature figurant sur la liste d'émargement pour le second tour est identique à celles figurant sur sa carte d'électeur et sur l'attestation par laquelle il assure être l'auteur de son vote. Il y a lieu, par suite, de regarder les sept suffrages mentionnés ci-dessus comme régulièrement exprimés, contrairement à ce qu'a jugé le tribunal administratif. Il en va de même, compte tenu des justifications produites devant le Conseil d'Etat, relatives notamment au grand âge de l'électeur en cause, s'agissant de l'électeur ayant voté sous le n° 211 dans le bureau n° 8.<br/>
<br/>
              8. En revanche, il résulte de l'examen des listes d'émargement et des justificatifs produits devant le Conseil d'Etat que les signatures de l'électeur ayant voté sous le n° 1 051 dans le bureau n° 7 présentent des différences qui ne font l'objet d'aucune justification. En ce qui concerne l'électeur ayant voté sous le n° 826 dans le bureau n° 10, les signatures figurant sur sa carte d'identité et sur l'attestation par laquelle il assure être l'auteur de son vote ne correspondent pas au paraphe figurant sur la liste d'émargement du second tour. S'agissant de l'électeur ayant voté sous le n° 574 dans le bureau n°9, si la signature figurant sur la liste d'émargement du second tour est celle de sa mère, il n'est pas établi que cette dernière ait disposé d'une procuration. L'électeur ayant voté sous le n° 305 dans le bureau n° 11, qui n'a apposé qu'un paraphe sur la liste d'émargement du second tour, a indiqué dans une sommation interpellative d'huissier du 29 avril 2021 qu'il ne souvenait pas " d'avoir signé comme ça ". Enfin, en ce qui concerne l'électeur ayant voté sous le n° 617 dans le bureau n° 3, si le paraphe apposé sur la liste d'émargement du second tour est identique à celui figurant sur l'attestation par laquelle il assure être l'auteur de son vote, ce paraphe ne correspond pas à la signature figurant sur sa carte d'identité. Il s'ensuit que les justifications produites par les requérants pour contester le caractère irrégulier de ces cinq autres suffrages ne sont pas suffisamment probantes pour retenir que c'est à tort que le tribunal administratif a estimé que ces suffrages avaient été irrégulièrement exprimés.<br/>
<br/>
              9. Il appartient au juge de l'élection de tirer les conséquences des irrégularités commises au cours du scrutin, en rectifiant, le cas échéant, les résultats de l'élection. Lorsqu'il est impossible de déterminer sur quelle liste ou en faveur de quel candidat s'est portée la voix à retrancher ou à ajouter aux suffrages exprimés, le juge de l'élection procède au calcul des résultats qui seraient constatés dans chacune des hypothèses, en vérifiant si la liste ou le candidat arrivé en tête conserve la majorité des suffrages.<br/>
<br/>
              10. Il résulte de ce qui est dit aux points 4 à 9 que cinq suffrages irréguliers doivent être hypothétiquement déduits du nombre de suffrages obtenus par la liste conduite par Mme AU..., arrivée en tête au second tour. Ce nombre est supérieur à l'écart de trois voix seulement qui sépare le nombre de voix obtenues par cette liste et la liste conduite par M. U....<br/>
<br/>
              11. Le motif énoncé au point 10 justifie à lui seul le dispositif du jugement attaqué, qui n'est entaché d'aucune contradiction de motifs. Par suite, et sans qu'il y ait lieu d'ordonner les mesures d'instruction sollicitées, les requérants ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif a annulé les opérations électorales des 15 mars et 28 juin 2020 dans la commune de Digne-les-Bains. <br/>
<br/>
              Sur les conclusions de la requête tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. U..., qui, dans la présente instance, n'est pas la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par M. U....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
<br/>
Article 1er : La requête de Mme AU... et autres est rejetée.<br/>
Article 2 : Les conclusions de M. U... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme AK... AU..., première requérante dénommée, à M. N... U... et au ministre de l'intérieur.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Thalia Breton, auditrice-rapporteure. <br/>
<br/>
<br/>
              Rendu le 20 octobre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Thalia Breton<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... S...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
