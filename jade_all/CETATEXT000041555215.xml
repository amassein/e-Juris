<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041555215</ID>
<ANCIEN_ID>JG_L_2020_02_000000428625</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/55/52/CETATEXT000041555215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/02/2020, 428625</TITRE>
<DATE_DEC>2020-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428625</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:428625.20200207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au juge des référés du tribunal administratif de Melun, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté du maire de Bussy-Saint-Georges du 26 décembre 2018 portant retrait de l'arrêté du 31 août 2018 par lequel il l'avait détachée dans l'emploi fonctionnel de directeur général des services ainsi que des arrêtés du même jour par lesquels il lui avait attribué la nouvelle bonification indiciaire et diverses indemnités et d'enjoindre à la commune du Bussy-Saint-Georges de la réintégrer dans l'emploi de directrice générale des services sous astreinte de 350 euros par jour de retard et, d'autre part, de suspendre l'exécution des décisions des 26 décembre 2018 et 10 janvier 2019 par lesquelles le maire l'a réintégrée dans le grade d'attaché territorial pour exercer les fonctions de responsable du pôle administratif, juridique et financier des services techniques. <br/>
<br/>
              Par une ordonnance nos 1900775, 1901053 du 19 février 2019, le juge des référés du tribunal administratif a rejeté ces demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 18 mars 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Bussy-Saint-Georges la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le décret n° 87-1101 du 30 décembre 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de Mme A... B... et à la SCP Colin-Stoclet, avocat de la commune de Bussy-Saint-Georges ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par arrêté du 31 août 2018, le maire de Bussy-Saint-Georges a nommé Mme A... B... directrice générale des services de la commune en la détachant dans cet emploi et lui a attribué, par arrêtés du même jour, la nouvelle bonification indiciaire afférente à cet emploi ainsi que diverses indemnités. Par un arrêté du 26 décembre 2018, pris au motif que le détachement ainsi prononcé était illégal, le maire a retiré l'ensemble de ces actes et a réintégré Mme B... dans le corps des attachés territoriaux, pour l'affecter aux fonctions de responsable du pôle administratif, juridique et financier des services techniques, correspondant à une fiche de poste qu'il lui a remise le 10 janvier 2019. Mme B... se pourvoit en cassation contre l'ordonnance du 19 février 2019 par laquelle le juge des référés du tribunal administratif de Melun a rejeté ses demandes tendant à la suspension, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'exécution de l'arrêté du 26 décembre 2018 et des décisions prononçant sa réintégration et sa nouvelle affectation.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Les emplois de directeur général des services des communes de 2 000 habitants et plus sont régis, aux termes du 1 du I de son article 1er, par le décret du 30 décembre 1987 portant dispositions statutaires particulières à certains emplois administratifs de direction des collectivités territoriales et des établissements publics locaux assimilés. Son article 4 prévoit que les fonctionnaires nommés dans ces emplois, lorsqu'ils ne font pas l'objet d'un recrutement direct en application de l'article 47 de la loi du 26 janvier 1984, " sont placés en position de détachement dans les conditions et suivant les règles statutaires prévues pour cette position dans leur cadre d'emplois, corps ou emploi d'origine " et " sont classés à l'échelon de l'emploi fonctionnel comportant un indice égal ou, à défaut, immédiatement supérieur à celui dont ils bénéficiaient dans leur grade ". L'article 5 du même décret précise que par dérogation à l'article 4, les fonctionnaires ainsi détachés " qui ont, en application de l'article 53 de la loi du 26 janvier 1984, précédemment occupé soit un emploi identique au nouvel emploi dans lequel ils sont nommés, soit un autre de ces emplois affecté d'une échelle indiciaire identique ou moins favorable, sont classés à un indice égal ou, à défaut, immédiatement supérieur à celui dont ils bénéficiaient dans l'emploi précédemment occupé, dès lors que leur nomination dans ce nouvel emploi intervient dans un délai au plus égal à un an ". Enfin, aux termes de l'article 7 de ce décret : " Seuls les fonctionnaires de catégorie A peuvent être détachés dans un emploi de : / 1. Directeur général des services d'une commune de 2 000 à 40 000 habitants ; (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions que la seule condition statutaire à remplir pour être détaché dans un emploi de directeur général des services d'une commune de 2 000 à 40 000 habitants est celle, fixée par l'article 7 du décret du 30 décembre 1987, d'être fonctionnaire de catégorie A. Les articles 4 et 5 de ce décret ont uniquement pour objet de déterminer à quel échelon de l'emploi de détachement l'intéressé est classé, en fonction de l'indice dont il bénéficiait dans son grade ou, si les conditions posées à l'article 5 sont remplies, dans l'emploi fonctionnel qu'il occupait précédemment.<br/>
<br/>
              5. Pour écarter tout doute sérieux quant à la légalité de l'arrêté du 26 décembre 2018, le juge des référés du tribunal administratif de Melun a relevé que Mme B... n'avait occupé précédemment aucun emploi fonctionnel et s'est fondé sur les dispositions des articles 4 et 5 du décret du 30 décembre 1987 pour déduire de l'indice qu'elle détenait dans le grade des attachés territoriaux qu'elle ne pouvait être détachée dans l'emploi de directeur général des services. En statuant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que Mme B... était, à la date du 31 août 2018, détachée dans l'emploi de directrice générale adjointe des services de la commune de Bussy-Saint-Georges, emploi fonctionnel mentionné à l'article 53 de la loi du 26 janvier 1984, et qu'elle remplissait en tant qu'attachée territoriale l'unique condition posée par les dispositions citées au point 3 pour le détachement dans un emploi de directeur général des services d'une commune de 2 000 à 40 000 habitants, il a dénaturé les pièces de ce dossier et méconnu la portée des dispositions du décret du 30 décembre 1987.<br/>
<br/>
              6. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, Mme B... est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension de l'arrêté de retrait du 26 décembre 2018 :<br/>
<br/>
              8. En premier lieu, d'une part, l'article L. 242-1 du code des relations entre le public et l'administration dispose que : " L'administration ne peut abroger ou retirer une décision créatrice de droits de sa propre initiative ou sur la demande d'un tiers que si elle est illégale et si l'abrogation ou le retrait intervient dans le délai de quatre mois suivant la prise de cette décision ". <br/>
<br/>
              9. Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de cette décision ou s'il a privé les intéressés d'une garantie. Il en résulte qu'une décision créatrice de droits, entachée d'un vice qui n'a pas été susceptible d'exercer une influence sur le sens de cette décision et qui n'a pas privé les intéressés d'une garantie, ne peut être tenue pour illégale et ne peut, en conséquence, être retirée ou abrogée par l'administration de sa propre initiative ou sur la demande d'un tiers, même dans le délai de quatre mois suivant la prise de cette décision.<br/>
<br/>
              10. D'autre part, conformément à l'article 30 de la loi du 26 janvier 1984, dans sa rédaction applicable au litige, et à l'article 27 du décret du 13 janvier 1986, le détachement d'un agent dans l'emploi de directeur général des services d'une commune de plus de 2 000 habitants doit intervenir après consultation préalable de la commission administrative paritaire compétente.<br/>
<br/>
              11. Il est constant, ainsi que l'a relevé le contrôle de légalité, que l'arrêté portant détachement de Mme B... dans l'emploi de directrice générale des services de la commune de Bussy-Saint-Georges a été pris avant que la commission administrative paritaire se prononce sur le projet de détachement. Mme B... soutient certes que l'arrêté de détachement n'était pas illégal de ce fait, dès lors que, d'une part, la consultation préalable de la commission administrative paritaire constituerait, selon elle, une garantie au bénéfice du seul agent que l'on se propose de nommer et que la décision lui étant en l'espèce favorable elle ne pouvait être regardée comme ayant été privée de cette garantie et que, d'autre part, en tout état de cause, la consultation de la commission administrative paritaire postérieurement à la décision a régularisé la procédure. Toutefois, lorsque la loi ou le règlement prévoit la consultation préalable d'une commission administrative paritaire avant une décision de détachement, cette consultation constitue une garantie au bénéfice de l'ensemble des agents candidats à ce détachement ou susceptibles de l'être. Le défaut de cette consultation préalable ne peut en outre être regardé comme régularisé par la consultation de la commission administrative paritaire après la décision que dans les hypothèses où la loi ou le règlement permettent expressément de déroger au caractère préalable de la consultation, hypothèses dans lesquelles il n'est ni établi ni même allégué que l'on se soit trouvé en l'espèce.<br/>
<br/>
              12. Ainsi, en l'état de l'instruction, le moyen tiré de ce que l'arrêté du 26 décembre 2018 serait illégal comme ayant prononcé le retrait d'une décision qui n'était pas entachée d'illégalité n'apparaît pas de nature à faire naître un doute sérieux quant à la légalité de cet arrêté. <br/>
<br/>
              13. En deuxième lieu, aux termes du dixième alinéa de l'article 53 de la loi du 26 janvier 1984 : " Il ne peut être mis fin aux fonctions des agents occupant les emplois " tels que, notamment, celui de directeur général des services d'une commune de plus de 2 000 habitants, " sauf s'ils ont été recrutés directement en application de l'article 47, qu'après un délai de six mois suivant soit leur nomination dans l'emploi, soit la désignation de l'autorité territoriale. La fin des fonctions [de ces] agents [...] est précédée d'un entretien de l'autorité territoriale avec les intéressés et fait l'objet d'une information de l'assemblée délibérante et du Centre national de la fonction publique territoriale ou du centre de gestion ; la fin des fonctions de ces agents prend effet le premier jour du troisième mois suivant l'information de l'assemblée délibérante ". Ces dispositions ne trouvent toutefois à s'appliquer que dans le cas où le fonctionnaire est déchargé de ses fonctions et pas dans le cas où le détachement est retiré au motif qu'il est entaché d'illégalité. Mme B... ne peut, dès lors, utilement invoquer la méconnaissance de ces dispositions.<br/>
<br/>
              14. En dernier lieu, la circonstance que l'arrêté détachant Mme B... dans l'emploi de directrice générale des services ait mentionné, par l'effet d'une erreur matérielle, un indice inférieur à celui auquel elle devait être classée par application des dispositions mentionnées au point 3 est sans incidence sur la légalité de la décision par laquelle le maire a prononcé le retrait de cet arrêté pour le motif exposé précédemment.<br/>
<br/>
              15. Il résulte de tout ce qui précède qu'aucun moyen soulevé par Mme B... n'est, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de l'arrêté du 26 décembre 2018. Par suite, sans qu'il soit besoin de se prononcer sur l'urgence, ses conclusions tendant à la suspension de l'exécution de cet arrêté doivent être rejetées, de même que ses conclusions tendant au prononcé d'une injonction.<br/>
<br/>
              Sur la demande de suspension des décisions portant réintégration et affectation de Mme B... :<br/>
<br/>
              16. Il ressort des éléments versés au dossier que si le retrait de l'emploi fonctionnel de directrice générale des services, dont découle de plein droit sa réintégration dans le grade d'attachée territoriale, a entraîné pour Mme B... une perte de responsabilités et de rémunération, les fonctions auxquelles elle a ensuite été affectée, avec la rémunération afférente, sont au nombre de celles correspondant à son grade et qu'il appartenait à la commune de lui proposer dans la mesure des emplois disponibles. Une telle affectation ne saurait, pour les mêmes motifs, être regardée comme une sanction. Dès lors, Mme B... n'est pas fondée à soutenir que son affectation dans ces fonctions préjudicierait, par elle-même, de manière suffisamment grave à ses intérêts pour que soit caractérisée une situation d'urgence.<br/>
<br/>
              17. Par suite, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par la commune de Bussy-Saint-Georges non plus que sur les moyens soulevés, les conclusions de Mme B... tendant à la suspension de l'exécution des décisions par lesquelles elle a été réintégrée dans son grade pour exercer les fonctions de responsable du pôle administratif, juridique et financier des services techniques doivent être rejetées.<br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              18. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Bussy-Saint-Georges qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B... une somme au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : L'ordonnance du 19 février 2019 du juge des référés du tribunal administratif de Melun est annulée.<br/>
<br/>
Article 2 : Les demandes présentées par Mme B... devant le tribunal administratif de Melun sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Bussy-Saint-Georges au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A... B... et à la commune de Bussy-Saint-Georges.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. - DÉCISION CRÉATRICE DE DROITS ENTACHÉE D'UN VICE DANTHONISABLE [RJ1] - IMPOSSIBILITÉ POUR L'ADMINISTRATION DE RETIRER OU D'ABROGER CETTE DÉCISION.
</SCT>
<ANA ID="9A"> 01-09 Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de cette décision ou s'il a privé les intéressés d'une garantie. Il en résulte qu'une décision créatrice de droits, entachée d'un vice qui n'a pas été susceptible d'exercer une influence sur le sens de cette décision et qui n'a pas privé les intéressés d'une garantie, ne peut être tenue pour illégale et ne peut, en conséquence, être retirée ou abrogée par l'administration de sa propre initiative ou sur la demande d'un tiers, même dans le délai de quatre mois suivant la prise de cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rapp., CE, Assemblée, 23 décembre 2011, M.,et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
