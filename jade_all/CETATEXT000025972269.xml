<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025972269</ID>
<ANCIEN_ID>JG_L_2012_06_000000330075</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/97/22/CETATEXT000025972269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 04/06/2012, 330075, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330075</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>Mme Anne Berriat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:330075.20120604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 juillet et 27 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE AGGREKO FRANCE, dont le siège est 5, rue Boole, à Saint-Michel-sur-Orge (91240), représentée par son gérant ; la SOCIETE AGGREKO FRANCE demande au Conseil d'Etat d'annuler l'arrêt n° 07VE02423 du 7 mai 2009 par lequel la cour administrative d'appel de Versailles a, d'une part, annulé le jugement du 10 mai 2007 par lequel le tribunal administratif de Versailles l'avait déchargée des prélèvements auxquels elle avait été assujettie en application de l'article 125 A du code général des impôts au titre des années 1998 et 1999 et, d'autre part, remis ces prélèvements à sa charge ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 mai 2012, présentée pour la SOCIETE AGGREKO FRANCE ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu la convention fiscale entre la France et les Pays-Bas du 16 mars 1973 ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Berriat, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les observations de la SCP Lesourd, avocat de la SOCIETE AGGREKO FRANCE,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              - La parole ayant été à nouveau donnée à la SCP Lesourd, avocat de la SOCIETE AGGREKO FRANCE ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SOCIETE AGGREKO FRANCE a servi en 1998 et en 1999 des intérêts d'emprunt à sa société mère, la société de droit néerlandais Aggreko Finance BV, dont elle soutenait qu'ils étaient versés en remboursement de deux prêts accordés le 31 mars 1995 par la société Aggreko Finance BV pour financer sa filiale établie en France ; qu'elle a fait l'objet d'une vérification de comptabilité en 2001 à l'issue de laquelle l'administration fiscale a soumis ces intérêts au prélèvement libératoire institué par le III de l'article 125 A du code général des impôts ; que, par un jugement du 10 mai 2007, le tribunal administratif de Versailles a déchargé la SOCIETE AGGREKO FRANCE des impositions en cause au motif que la législation fiscale française, combinée avec les stipulations de l'article 11 de la convention fiscale conclue entre la France et les Pays-Bas le 16 mars 1973, qui plafonnent le taux d'imposition des intérêts à 10 %, était incompatible avec les articles 43 et 48 du traité instituant la Communauté européenne ; que la SOCIETE AGGREKO FRANCE se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Versailles a annulé ce jugement pour erreur de droit et remis ces prélèvements à sa charge ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 131 quater du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige: " Les produits des emprunts contractés hors de France par des personnes morales françaises avec l'autorisation du ministre de l'économie, des finances et de la privatisation sont exonérés du prélèvement prévu au paragraphe III de l'article 125 A " ; que la cour administrative d'appel n'a pas commis d'erreur de droit en relevant, au terme d'une appréciation souveraine des pièces du dossier qui lui était soumis, exempte de dénaturation, que les clauses des contrats néerlandais produits par la SOCIETE AGGREKO FRANCE ne prévoyaient pas le versement d'intérêts d'emprunt remplissant les conditions posées par l'article 131 quater du code général des impôts ;<br/>
<br/>
              Considérant, en second lieu, qu'en vertu des stipulations des articles 43, 49 et 56 du traité instituant la Communauté européenne alors en vigueur, les restrictions aux libertés d'établissement, de prestation de services et de mouvement des capitaux à l'intérieur de l'Union européenne sont interdites ; <br/>
<br/>
              Considérant qu'aux termes de l'article 125 A du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I. (...) les personnes physiques qui bénéficient d'intérêts (...), dont le débiteur est domicilié ou établi en France, peuvent opter pour leur assujettissement à un prélèvement qui libère les revenus auxquels il s'applique de l'impôt sur le revenu. / La retenue à la source éventuellement opérée sur ces revenus est imputée sur le prélèvement. / Celui-ci est effectué par le débiteur ou par la personne qui assure le paiement des revenus. (...) / III. Le prélèvement est obligatoirement applicable aux revenus visés ci-dessus qui sont encaissés par des personnes n'ayant pas en France leur domicile fiscal ; la même disposition s'applique aux revenus qui sont payés hors de France ou qui sont encaissés par des personnes morales n'ayant pas leur siège social en France (...) " ;<br/>
<br/>
              Considérant qu'en jugeant que la différence de traitement établie par la législation française entre les sociétés bénéficiaires d'intérêts, consistant en l'application de techniques d'imposition différentes selon que celles-ci sont établies en France, et de ce fait assujetties à l'impôt sur les sociétés et à l'imposition forfaitaire annuelle, ou dans un autre Etat membre, ce qui les rend redevables du prélèvement institué par le III de l'article 125 A du code général des impôts, concerne des situations qui ne sont pas objectivement comparables dès lors, d'une part, que la France agit, dans le premier cas, en sa qualité d'Etat de résidence des sociétés concernées et, dans le second cas, en tant qu'Etat de la source des intérêts, d'autre part, que le versement d'intérêts par une société résidente à une autre société résidente et le versement d'intérêts par une société résidente à une société non résidente donnent lieu à des impositions distinctes, fondées sur des bases juridiques différentes et, enfin, que ces différentes techniques d'imposition reflètent la différence des situations dans lesquelles se trouvent ces sociétés au regard du recouvrement de l'impôt, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant que les possibles différences d'assiette résultant de la non-déductibilité des charges sont sans incidence à cet égard, compte tenu de l'écart entre les taux d'imposition applicables aux intérêts versés aux sociétés résidentes, soumis à l'impôt sur les sociétés et ceux correspondant aux prélèvements sur les intérêts versés aux sociétés non résidentes, plafonnés à seulement 10 % des intérêts en vertu de l'article 11 de la convention fiscale conclue entre la France et les Pays-Bas ;<br/>
<br/>
              Considérant que, dans le cas d'une retenue à la source effectuée sur des intérêts versés à une société non résidente déficitaire, le décalage dans le temps entre la perception à la source et l'impôt établi à l'encontre d'une société déficitaire établie en France au titre de l'exercice où ses résultats redeviennent bénéficiaires procède d'une technique différente d'imposition, de sorte que le désavantage de trésorerie qui en résulte ne peut être regardé, à lui seul, comme constituant une différence de traitement caractérisant une restriction aux libertés d'établissement, de prestation de services et de mouvement des capitaux ;<br/>
<br/>
              Considérant que, par suite, la société requérante n'est pas fondée à soutenir que la cour administrative d'appel aurait méconnu les stipulations du traité instituant la Communauté européenne ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE AGGREKO FRANCE n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la SOCIETE AGGREKO FRANCE est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE AGGREKO FRANCE et au ministre de l'économie, des finances et du commerce extérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
