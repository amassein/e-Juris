<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569445</ID>
<ANCIEN_ID>JG_L_2020_02_000000432131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569445.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 12/02/2020, 432131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Recours en interprétation</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432131.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 19 juin 2019 au secrétariat du contentieux du Conseil d'Etat, Mme D... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'interpréter sa décision n° 400711, 400712 en date du 6 novembre 2017 par laquelle, après avoir annulé l'arrêt du 14 avril 2016 de la cour administrative d'appel de Versailles, il a mis à la charge de Mme B... et de M. et Mme C... le versement à la commune de Massy et à la société Paris Sud Aménagement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) de mettre à la charge de la commune de Massy la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme B..., et à la SCP Foussard, Froger, avocat de la commune de Massy ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Par l'article 3 de sa décision du 6 novembre 2017, rendue sur la requête de la commune de Massy et de la société Paris Sud Aménagement, le Conseil d'Etat, statuant au contentieux, a mis à la charge de Mme B... et de M. et Mme C... le versement à la commune de Massy et à la société Paris Sud Aménagement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. Mme B... demande, par la voie d'un recours en interprétation, que cette décision soit interprétée en ce que M. et Mme C... et elle sont ensemble débiteurs de la somme totale de 2 500 euros à verser à la commune de Massy et à la société Paris Sud Aménagement, et non ensemble débiteurs de la somme de 2 500 euros à verser à chacun des deux créanciers ni débiteurs séparément de la somme de 2 500 euros à verser à chacun des deux créanciers.<br/>
<br/>
              2.	Un recours en interprétation d'une décision juridictionnelle n'est recevable que s'il émane d'une partie à l'instance ayant abouti au prononcé de la décision dont l'interprétation est sollicitée et dans la seule mesure où il peut être valablement argué que cette décision est obscure ou ambiguë.<br/>
<br/>
              3.	Aux termes des visas de la décision, les conclusions de la commune de Massy tendaient au versement de la somme de 4 000 euros à la charge solidaire de M. et Mme C... et de Mme B... et les conclusions de la société Paris Sud Aménagement tendaient au versement de la somme de 4 000 euros à la charge solidaire de Mme A..., de M. et Mme C... et de Mme B.... Les motifs de la décision admettent de faire droit à ces demandes en mettant " solidairement à la charge de Mme B... et de M. et Mme C... la somme de 2 500 euros à verser à la commune de Massy et à la société Paris Sud Aménagement au titre de l'article L. 761-1 du code de justice administrative ". <br/>
<br/>
              4.	Ainsi, l'article 3, qui est dépourvu de toute obscurité ou ambiguïté en décidant le versement de la somme totale de 2 500 euros à la commune de Massy et à la société Paris Sud Aménagement, soit 1 250 euros à chacune par Mme B... et M. et Mme C..., solidairement tenus à ces deux dettes, soit 625 euros versés par chacun des débiteurs à chacun des créanciers, ne peut, au vu des visas et des motifs, être entendu comme prévoyant le versement de la somme de 2 500 euros par chacun des débiteurs à chacun des créanciers ou le versement de la somme de 2 500 euros à chacun des créanciers par les débiteurs solidaires. En conséquence, le recours en interprétation présenté par Mme B... est irrecevable.<br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Massy qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B... est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme D... B..., à la commune de Massy, à la société Paris Sud Aménagement et à M. et Mme C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
