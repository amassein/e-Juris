<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631760</ID>
<ANCIEN_ID>JG_L_2018_11_000000411084</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 21/11/2018, 411084, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411084</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Marie-Laure Denis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411084.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Groupe ornithologique du Roussillon a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté n° 2013-275-0002 du 2 octobre 2013 par lequel le préfet des Pyrénées-Orientales a, d'une part, attribué à l'association communale de chasse agréée de Llo un plan de chasse pour le prélèvement d'un spécimen de l'espèce grand tétras pour la saison 2013-2014 et, d'autre part, fixé les conditions générales de chasse. Par un jugement n° 1305601 du 6 mars 2015, le tribunal administratif de Montpellier a annulé l'arrêté préfectoral du 2 octobre 2013.<br/>
<br/>
              Par un arrêt n° 15MA01921 du 30 mars 2017, la cour administrative d'appel de Marseille a, rejeté l'appel formé par la ministre de l'écologie, du développement durable et de l'énergie contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés respectivement le 31 mai 2017 et le 25 août 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de la transition écologique et solidaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la directive 2009/147/CE du 30 novembre 2009 du Parlement européen et du Conseil concernant la conservation des oiseaux sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Laure Denis, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'association Groupe ornithologique du Roussillon.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 2 octobre 2013, le préfet des Pyrénées-Orientales a attribué au président de l'association communale de chasse agrée de Llo un plan de chasse pour le prélèvement d'un spécimen de l'espèce grand tétras pour la saison de chasse 2013/2014 et a fixé les conditions générales de la chasse. Le tribunal administratif de Montpellier a, à la demande du Groupe Ornithologique du Roussillon, annulé cet arrêté. Le ministre chargé de la chasse se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes de l'article 7 de la directive 2009/147/CE du 30 novembre 2009 concernant la conservation des oiseaux sauvages : " Les États membres prennent toutes les mesures nécessaires pour maintenir ou adapter la population de toutes les espèces d'oiseaux visées à l'article 1er à un niveau qui corresponde notamment aux exigences écologiques, scientifiques et culturelles, compte tenu des exigences économiques et récréationnelles ". Aux termes de l'article 7 de cette directive : " 1. En raison de leur niveau de population, de leur distribution géographique et de leur taux de reproductivité dans l'ensemble de la Communauté, les espèces énumérées à l'annexe II peuvent faire l'objet d'actes de chasse dans le cadre de la législation nationale. Les États membres veillent à ce que la chasse de ces espèces ne compromette pas les efforts de conservation entrepris dans leur aire de distribution (...). / 4. Les États membres s'assurent que la pratique de la chasse (...) telle qu'elle découle de l'application des mesures nationales en vigueur, respecte les principes d'une utilisation raisonnée et d'une régulation équilibrée du point de vue écologique des espèces d'oiseaux concernées, et que cette pratique soit compatible, en ce qui concerne la population de ces espèces, notamment des espèces migratrices, avec les dispositions découlant de l'article 2. / Ils veillent en particulier à ce que les espèces auxquelles s'applique la législation sur la chasse ne soient pas chassées pendant la période nidicole ni pendant les différents stades de reproduction et de dépendance (...) ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 420-1 du code de l'environnement : " La gestion durable du patrimoine faunique et de ses habitats est d'intérêt général. La pratique de la chasse, activité à caractère environnemental, culturel, social et économique, participe à cette gestion et contribue à l'équilibre entre le gibier, les milieux et les activités humaines en assurant un véritable équilibre agro-sylvo-cynégétique. / Le principe de prélèvement raisonnable sur les ressources naturelles renouvelables s'impose aux activités d'usage et d'exploitation de ces ressources. Par leurs actions de gestion et de régulation des espèces dont la chasse est autorisée ainsi que par leurs réalisations en faveur des biotopes, les chasseurs contribuent au maintien, à la restauration et à la gestion équilibrée des écosystèmes en vue de la préservation de la biodiversité. Ils participent de ce fait au développement des activités économiques et écologiques dans les milieux naturels, notamment dans les territoires à caractère rural ". Aux termes de l'article L. 425-6 du même code, dans sa rédaction en vigueur à la date de l'arrêté préfectoral contesté : " Le plan de chasse détermine le nombre minimum et maximum d'animaux à prélever sur les territoires de chasse. Il tend à assurer le développement durable des populations de gibier et à préserver leurs habitats, en conciliant les intérêts agricoles, sylvicoles et cynégétiques (...) ".<br/>
<br/>
              4. Il résulte de la combinaison de ces dispositions, et notamment de celles de la directive 2009/147/CE du 30 novembre 2009, éclairées par la jurisprudence de la Cour de justice des Communautés européennes, devenue la Cour de justice de l'Union européenne qu'elles poursuivent un objectif d'intérêt général de protection des espèces d'oiseaux sauvages qui doit être concilié, en vertu de l'article 2 de la directive, avec des exigences économiques et récréationnelles. En vertu de l'article 7 de la directive, les espèces concernées peuvent en principe faire l'objet d'actes de chasse, dès lors qu'ils ne compromettent pas les efforts de conservation entrepris dans leur aire de distribution et qu'ils respectent les principes d'une utilisation raisonnée et d'une régulation équilibrée du point de vue écologique des espèces d'oiseaux concernées.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond qu'en relevant que l'effectif de grands tétras mâles a connu une diminution de l'ordre de 70 % à l'échelle de l'ensemble des Pyrénées françaises entre 1960 et 2009, que l'effectif de l'espèce demeure inférieur au seuil critique de cinq cents unités à la date de l'arrêté attaqué dans le département des Pyrénées-Orientales et même inférieur à cent dans l'unité de gestion " Canigou-Puigmal-Carança " dont relève l'association de chasse agréée de Llo, alors que l'indice de reproduction est insuffisant pour assurer la conservation favorable de l'espèce à court et moyen terme dans son aire de répartition naturelle, s'agissant d'un oiseau sédentaire, la cour administrative d'appel a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation. <br/>
<br/>
              6. Compte tenu de ces constatations souveraines, en jugeant que le prélèvement, ne serait-ce que d'un seul spécimen, était de nature à compromettre les efforts de conservation de l'espèce, qui ne peuvent être regardés comme suffisants pour empêcher une diminution sensible des effectifs de grand tétras susceptible de conduire, à terme, à la disparition de l'espèce au sein de l'unité de gestion " Canigou-Puigmal-Carança " comme au sein de l'ensemble du département des Pyrénées-Orientales, la cour n'a pas commis les erreurs de droit reprochées. <br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi du ministre de la transition écologique et solidaire doit être rejeté. <br/>
<br/>
              8.  Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3000 euros qui sera versée à l'association Groupe ornithologique du Roussillon au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre d'Etat, ministre de la transition écologique et solidaire est rejeté.<br/>
Article 2 : L'Etat versera à l'association Groupe ornithologique du Roussillon une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de la transition écologique et solidaire et à l'association Groupe ornithologique du Roussillon. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
