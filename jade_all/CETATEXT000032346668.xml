<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032346668</ID>
<ANCIEN_ID>JG_L_2016_03_000000375529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/34/66/CETATEXT000032346668.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème SSR, 30/03/2016, 375529, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:375529.20160330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par action simplifiée Bureau européen d'assurance hospitalière (BEAH) a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir la décision en date du 30 octobre 2012 par laquelle le directeur du centre hospitalier de Perpignan a rejeté sa demande de communication des pièces du marché d'assurance de responsabilité civile conclu avec la société hospitalière d'assurances mutuelles (SHAM), ainsi que ses annexes, et d'enjoindre au directeur du centre hospitalier de Perpignan de lui communiquer, sous astreinte, ces documents. <br/>
<br/>
              Par un jugement n° 1204882 du 17 décembre 2013, le tribunal administratif de Montpellier a, d'une part, prononcé un non-lieu partiel à statuer et, d'autre part, annulé la décision du directeur du Centre hospitalier de Perpignan en tant qu'elle refuse de communiquer au BEAH le formulaire de réponse financière produit par la SHAM dans le cadre de la passation du marché public de prestations d'assurance responsabilité civile qui lui a été attribué.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 17 février 2014, 19 mai 2014 et 10 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Perpignan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes présentées par le Bureau européen d'assurance hospitalière en première instance ;<br/>
<br/>
              3°) de mettre à la charge du Bureau européen d'assurance hospitalière la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier de Perpignan et à la SCP Lyon-Caen, Thiriez, avocat du bureau européen d'assurance hospitalière ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le Bureau européen d'assurance hospitalière (BEAH) s'est porté candidat dans le cadre d'un appel d'offres ouvert lancé par le centre hospitalier de Perpignan pour l'assurance responsabilité civile de l'établissement ; que par courrier du 29 mars 2012, le BEAH a été informé que son offre n'avait pas été retenue ; qu'il a sollicité la communication des pièces relatives à ce marché ainsi que ses annexes ; que, sur saisine du BEAH, la commission d'accès aux documents administratifs a rendu un avis favorable à la communication de ces documents ; que le 30 octobre 2012, le directeur du centre hospitalier de Perpignan a confirmé son refus de communiquer le marché de prestation d'assurance responsabilité civile conclu avec la société hospitalière d'assurances mutuelles (SHAM) et ses annexes ; que par jugement du 17 décembre 2013, le tribunal administratif de Montpellier, après avoir relevé un non lieu à statuer partiel sur la demande de communication, a annulé la décision du 30 octobre 2012 en tant qu'elle refuse de communiquer au BEAH le formulaire de réponse financière produit par la SHAM et a enjoint au centre hospitalier de produire ce document ; que le pourvoi en cassation du centre hospitalier de Perpignan doit être regardé comme dirigé contre le jugement en tant qu'il fait droit à la demande du BEAH, c'est-à-dire contre ses articles 2 à 4 et en tant qu'il a rejeté sa demande présentée au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal alors en vigueur : " Le droit de toute personne à l'information est précisé et garanti par les dispositions des chapitres Ier, III et IV du présent titre en ce qui concerne la liberté d'accès aux documents administratifs. / Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, directives, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions et décisions. (...) " ; qu'aux termes de l'article 2 de cette même loi : " Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent titre. / Le droit à communication ne s'applique qu'à des documents achevés. " ; que le II de l'article 6 de cette loi dispose que : " II.-Ne sont communicables qu'à l'intéressé les documents administratifs : / - dont la communication porterait atteinte à la protection de la vie privée, au secret médical et au secret en matière commerciale et industrielle (...) " ; que ces dispositions sont aujourd'hui codifiées aux articles L. 311-1 à L. 311-6 du code des relations entre le public et l'administration ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées que les marchés publics et les documents qui s'y rapportent, y compris les documents relatifs au contenu des offres, sont des documents administratifs au sens des dispositions de l'article 1er de la loi du 17 juillet 1978 ; que, saisis d'un recours relatif à la communication de tels documents, il revient aux juges du fond d'examiner si, par eux-mêmes, les renseignements contenus dans les documents dont il est demandé la communication peuvent, en affectant la concurrence entre les opérateurs économiques, porter atteinte au secret industriel et commercial et faire ainsi obstacle à cette communication en application des dispositions du II de l'article 6 de la loi du 17 juillet 1978 ; qu'au regard des règles de la commande publique, doivent ainsi être regardés comme communicables, sous réserve des secrets protégés par la loi, l'ensemble des pièces du marché ; que dans cette mesure, si notamment l'acte d'engagement, le prix global de l'offre et les prestations proposées par l'entreprise attributaire sont en principe communicables,  le bordereau unitaire de prix de l'entreprise attributaire, en ce qu'il reflète la stratégie commerciale de l'entreprise opérant dans un secteur d'activité et qu'il est susceptible, ainsi, de porter atteinte au secret commercial, n'est quant à lui, en principe, pas communicable ; <br/>
<br/>
              4. Considérant que pour juger communicable le formulaire de réponse financière de la SHAM, attributaire du marché relatif à l'assurance responsabilité civile du centre hospitalier de Perpignan, le tribunal a estimé que cette communication ne pouvait porter atteinte à la concurrence en se fondant notamment sur le fait que le marché n'était pas susceptible d'être renouvelé à brève échéance par le centre hospitalier ; que ce faisant, alors qu'en lui-même le bordereau de prix unitaire d'un marché est, ainsi qu'il a été dit, en principe susceptible d'affecter la concurrence entre les entreprises intervenant dans un même secteur d'activité et ainsi de porter atteinte au secret commercial, le tribunal administratif a entaché son jugement d'une erreur de droit ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le centre hospitalier de Perpignan est fondé à demander l'annulation des articles 2 à 4 du jugement attaqué et en tant qu'il met à sa charge une somme en application des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans cette mesure, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que la communication du prix détaillé de l'offre de l'attributaire d'un marché dans le secteur des assurances, qui relève de la stratégie commerciale de l'assureur et peut en révéler les principaux aspects, est susceptible de porter atteinte au secret commercial ; qu'en l'absence de circonstances particulières relatives à l'offre retenue par le centre hospitalier, c'est sans erreur de droit ni de qualification juridique que le directeur du centre hospitalier de Perpignan a estimé ne pouvoir communiquer ce document demandé ; qu'il suit de là, que le BEAH n'est pas fondé à solliciter l'annulation de la décision qu'il attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier de Perpignan qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Bureau européen d'assurance hospitalière la somme de 5 000 euros à verser au centre hospitalier de Perpignan, pour l'ensemble de la procédure, au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les articles 2 à 4 du jugement du 17 décembre 2013 du tribunal administratif de Montpellier et l'article 5 en tant qu'il met une somme à la charge du centre hospitalier de Perpignan sur le fondement de l'article L. 761-1 du code de justice administrative sont annulés.<br/>
<br/>
Article 2 : Les conclusions de la demande du Bureau européen d'assurance hospitalière tendant à l'annulation du refus de communication du formulaire de réponse financière de l'attributaire du marché d'assurance responsabilité civile du centre hospitalier de Perpignan ainsi que celles tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La société Bureau européen d'assurance hospitalière versera au centre hospitalier de Perpignan la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Perpignan et à la SAS Bureau européen d'assurance hospitalière.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-02 Droits civils et individuels. Accès aux documents administratifs. Accès aux documents administratifs au titre de la loi du 17 juillet 1978. Droit à la communication. Documents administratifs communicables.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39 Marchés et contrats administratifs.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
