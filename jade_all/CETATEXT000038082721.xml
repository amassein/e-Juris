<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038082721</ID>
<ANCIEN_ID>JG_L_2019_01_000000420797</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/27/CETATEXT000038082721.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 30/01/2019, 420797, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420797</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:420797.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 18 mai 2018, enregistré le 22 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Pau, avant de statuer sur les demandes de M. A...B...tendant, d'une part, à la condamnation de l'Etat à réparer les préjudices qui résulteraient, selon lui, de l'absence de prise en compte de certains de ses services pour le calcul de bonifications d'ancienneté au titre de l'avantage spécifique d'ancienneté et, d'autre part, à l'annulation de la décision du directeur départemental des finances publiques des Hautes-Pyrénées du 17 février 2017 ayant partiellement régularisé sa situation, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir quel est le délai de recours applicable aux décisions implicites de rejet d'une demande indemnitaire préalable, postérieurement à la modification des articles R. 421-1 et R. 421-3 du code de justice administrative par le décret du 2 novembre 2016 portant modification du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2015-1145 du 15 septembre 2015 ;<br/>
              - le décret n° 2016-1480 du 2 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes de l'article R. 421-1 du code de justice administrative, dans sa rédaction issue du décret du 2 novembre 2016 portant modification du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée (...) ". S'agissant du délai de recours contre les décisions implicites, l'article R. 421-2 du même code dispose, dans sa rédaction issue du décret de modification du code de justice administrative du 15 septembre 2015   : " Sauf disposition législative ou réglementaire contraire, dans les cas où le silence gardé par l'autorité administrative sur une demande vaut décision de rejet, l'intéressé dispose, pour former un recours, d'un délai de deux mois à compter de la date à laquelle est née une décision implicite de rejet (...) ". Cette dernière règle comporte toutefois deux exceptions, fixées par l'article R. 421-3 du même code, qui prévoit, dans sa rédaction issue du décret du 2 novembre 2016, que seule une décision expresse est de nature à faire courir le délai de recours contentieux " (...) 1° Dans le contentieux de l'excès de pouvoir, si la mesure sollicitée ne peut être prise que par décision ou sur avis des assemblées locales ou de tous autres organismes collégiaux ", ainsi que " 2° Dans le cas où la réclamation tend à obtenir l'exécution d'une décision de la juridiction administrative ". Ce même décret du 2 novembre 2016 a, par son article 10, supprimé à cet article R. 421-3 une troisième exception, qui prévoyait que le délai de recours de deux mois ne courait qu'à compter d'une décision expresse " en matière de plein contentieux ". <br/>
<br/>
              2. La question soumise au Conseil d'Etat par le tribunal administratif de Pau, sur le fondement de l'article L. 113-1 du code de justice administrative, est relative aux conditions d'entrée en vigueur de cette dernière suppression, qui a pour effet de soumettre au droit commun, pour la naissance du délai de recours, les décisions implicites dont la contestation relève du plein contentieux, en particulier les refus tacitement opposés par l'administration à une demande indemnitaire.<br/>
<br/>
              3. L'article 35 du décret du 2 novembre 2016, qui fixe les conditions de son entrée en vigueur, dispose que : " I. - Le présent décret entre en vigueur le 1er janvier 2017. / II. - Les dispositions des articles 9 et 10 (...) sont applicables aux requêtes enregistrées à compter de cette date ". La question posée conduit ainsi à distinguer entre les décisions implicites relevant du plein contentieux qui sont nées à compter du 1er janvier 2017 et celles qui sont nées avant cette date.<br/>
<br/>
              4. S'agissant des décisions implicites relevant du plein contentieux qui sont nées à compter du 1er janvier 2017, date de l'entrée en vigueur du décret du 2 novembre 2016, la nouvelle règle selon laquelle, sauf dispositions législatives ou réglementaires qui leur seraient propres, le délai de recours de deux mois court à compter de la date où elles sont nées, leur est applicable.<br/>
<br/>
              5. S'agissant, en revanche, des décisions nées avant le 1er janvier 2017, les dispositions citées au point 3 n'ont pas pour objet et n'auraient pu légalement avoir pour effet de déroger au principe général du droit selon lequel, en matière de délai de procédure, il ne peut être rétroactivement porté atteinte aux droits acquis par les parties sous l'empire des textes en vigueur à la date à laquelle le délai a commencé à courir. <br/>
<br/>
              6. A ce titre, lorsque, avant le 1er janvier 2017, une personne s'était vu tacitement opposer un refus susceptible d'être contesté dans le cadre d'un recours de plein contentieux, ce recours n'était enfermé, en l'état des textes en vigueur, dans aucun délai, sauf à ce que cette décision de refus soit, sous forme expresse, régulièrement notifiée à cette personne, un délai de recours de deux mois courant alors à compter de la date de cette notification. <br/>
<br/>
              7. Il s'ensuit que, s'agissant des refus implicites nés avant le 1er janvier 2017 relevant du plein contentieux, le décret du 2 novembre 2016 n'a pas fait - et n'aurait pu légalement faire - courir le délai de recours contre ces décisions à compter de la date à laquelle elles sont nées.<br/>
<br/>
              8. Toutefois, les dispositions du II de l'article 35 du décret du 2 novembre 2016, citées au point 3, qui prévoient l'application de l'article 10 de ce décret à " toute requête enregistrée à compter " du 1er janvier 2017, ont entendu permettre la suppression immédiate, pour toutes les situations qui n'étaient pas constituées à cette date, de l'exception à la règle de l'article R. 421-2 du code de justice administrative dont bénéficiaient les matières de plein contentieux.<br/>
<br/>
              9. Or la réglementation applicable jusqu'à l'entrée en vigueur, le 1er janvier 2017, du décret du 2 novembre 2016, ne créait pas de droit acquis à ce que tout refus tacite antérieur reste, en matière de plein contentieux, indéfiniment susceptible d'être contesté. Elle conférait seulement aux intéressés le droit à ce que le délai de recours contre un tel refus ne courre qu'à compter du moment où, ainsi qu'il a été dit, ce refus était explicitement et régulièrement porté à leur connaissance.<br/>
<br/>
              10. Un délai de recours de deux mois court, par suite, à compter du 1er janvier 2017, contre toute décision implicite relevant du plein contentieux qui serait née antérieurement à cette date.<br/>
<br/>
              11. Cette règle doit toutefois être combinée avec les dispositions de l'article L. 112-6 du code des relations entre le public et l'administration, aux termes desquelles, sauf en ce qui concerne les relations entre l'administration et ses agents, les délais de recours contre une décision tacite de rejet ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception prévu par l'article L. 112-3 du même code ne lui a pas été transmis ou que celui-ci ne porte pas les mentions prévues à l'article R. 112-5 de ce code et, en particulier, dans le cas où la demande est susceptible de donner lieu à une décision implicite de rejet, la mention des voies et délais de recours.<br/>
<br/>
              12. Ainsi, sous réserve qu'aient été respectées les règles rappelées au point précédent, les recours de plein contentieux dirigés contre une décision implicite de rejet née antérieurement au 1er janvier 2017 ne sont recevables que dans un délai franc de deux mois à compter de la date d'entrée en vigueur du décret du 2 novembre 2016, soit jusqu'au 2 mars 2017.<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Pau, à M. A... B..., à la garde des sceaux, ministre de la justice et au Premier Ministre.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - DÉCRET DU 2 NOVEMBRE 2016 SOUMETTANT AU DROIT COMMUN DE LA NAISSANCE DU DÉLAI DE RECOURS LES DÉCISIONS IMPLICITES DONT LA CONTESTATION RELÈVE DU PLEIN CONTENTIEUX, RENDU APPLICABLE AUX REQUÊTES ENREGISTRÉES À COMPTER DU 1ER JANVIER 2017 - 1) DÉCISIONS IMPLICITES NÉES À COMPTER DU 1ER JANVIER 2017 - APPLICABILITÉ DE CETTE RÈGLE - EXISTENCE - 2) DÉCISIONS IMPLICITES NÉES ANTÉRIEUREMENT AU 1ER JANVIER - A) APPLICABILITÉ DU NOUVEAU DÉLAI DE RECOURS À COMPTER DE LA NAISSANCE DE CES DÉCISIONS - ABSENCE [RJ1] - B) APPLICABILITÉ DU NOUVEAU DÉLAI À COMPTER DU 1ER JANVIER 2017 - EXISTENCE [RJ2], SAUF LORSQUE L'ACCUSÉ DE RÉCEPTION PRÉVU PAR L'ARTICLE L.112-3 DU CRPA N'A PAS ÉTÉ TRANSMIS OU QUE CELUI-CI NE PORTE PAS LA MENTION DES VOIES ET DÉLAIS DE RECOURS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. - DÉCISIONS IMPLICITES DONT LA CONTESTATION RELÈVE DU PLEIN CONTENTIEUX - DÉCRET DU 2 NOVEMBRE 2016 SOUMETTANT CES DÉCISIONS AU DROIT COMMUN DE LA NAISSANCE DU DÉLAI DE RECOURS, RENDU APPLICABLE AUX REQUÊTES ENREGISTRÉES À COMPTER DU 1ER JANVIER 2017 - CONSÉQUENCES - 1) DÉCISIONS IMPLICITES NÉES À COMPTER DU 1ER JANVIER 2017 - APPLICABILITÉ DE CETTE RÈGLE - EXISTENCE - 2) DÉCISIONS IMPLICITES NÉES ANTÉRIEUREMENT AU 1ER JANVIER - A) APPLICABILITÉ DU NOUVEAU DÉLAI DE RECOURS À COMPTER DE LA NAISSANCE DE CES DÉCISIONS - ABSENCE [RJ1] - B) APPLICABILITÉ DU NOUVEAU DÉLAI À COMPTER DU 1ER JANVIER 2017 - EXISTENCE [RJ2], SAUF LORSQUE L'ACCUSÉ DE RÉCEPTION PRÉVU PAR L'ARTICLE L.112-3 DU CRPA N'A PAS ÉTÉ TRANSMIS OU QUE CELUI-CI NE PORTE PAS LA MENTION DES VOIES ET DÉLAIS DE RECOURS.
</SCT>
<ANA ID="9A"> 01-08-01 1) La nouvelle règle, issue du décret n° 2016-1480 du 2 novembre 2016, selon laquelle, sauf dispositions législatives ou règlementaire qui leur seraient propres, le délai de recours de deux mois court à compter de la date où les décisions implicites relevant du plein contentieux sont nées, est applicable à ces décisions nées à compter du 1er janvier 2017.... ...2) a) S'agissant des refus implicites nés avant le 1er janvier 2017 relevant du plein contentieux, le décret du 2 novembre 2016 n'a pas fait - et n'aurait pu légalement faire - courir le délai de recours contre ces décisions à compter de la date à laquelle elles sont nées.... ...b) Toutefois, les dispositions du II de l'article 35 du décret du 2 novembre 2016, qui prévoient l'application de la nouvelle règle à toute requête enregistrée à compter du 1er janvier 2017, ont entendu permettre la suppression immédiate, pour toutes les situations qui n'étaient pas constituées à cette date, de l'exception à la règle de l'article R.421-2 du code de justice administrative (CJA) dont bénéficiaient les matières de plein contentieux. Un délai de recours de deux mois court, par suite, à compter du 1er janvier 2017, contre toute décision implicite relevant du plein contentieux qui serait née antérieurement à cette même date. Cette règle doit toutefois être combinée avec les dispositions de l'article L. 112-6 du code des relations entre le public et l'administration (CRPA), aux termes desquelles, sauf en ce qui concerne les relations entre l'administration et ses agents, les délais de recours contre une décision tacite de rejet ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception prévu par l'article L.112-3 du même code ne lui a pas été transmis ou que celui-ci ne porte pas les mentions prévues à l'article R. 112-5 de ce code et, en particulier, dans le cas où la demande est susceptible de donner lieu à une décision implicite de rejet, la mention des voies et délais de recours.</ANA>
<ANA ID="9B"> 54-01-07-02 1) La nouvelle règle, issue du décret n° 2016-1480 du 2 novembre 2016, selon laquelle, sauf dispositions législatives ou règlementaire qui leur seraient propres, le délai de recours de deux mois court à compter de la date où les décisions implicites relevant du plein contentieux sont nées, est applicable à ces décisions nées à compter du 1er janvier 2017.... ...2) a) S'agissant des refus implicites nés avant le 1er janvier 2017 relevant du plein contentieux, le décret du 2 novembre 2016 n'a pas fait - et n'aurait pu légalement faire - courir le délai de recours contre ces décisions à compter de la date à laquelle elles sont nées.... ...b) Toutefois, les dispositions du II de l'article 35 du décret du 2 novembre 2016, qui prévoient l'application de la nouvelle règle à toute requête enregistrée à compter du 1er janvier 2017, ont entendu permettre la suppression immédiate, pour toutes les situations qui n'étaient pas constituées à cette date, de l'exception à la règle de l'article R.421-2 du code de justice administrative (CJA) dont bénéficiaient les matières de plein contentieux. Un délai de recours de deux mois court, par suite, à compter du 1er janvier 2017, contre toute décision implicite relevant du plein contentieux qui serait née antérieurement à cette même date. Cette règle doit toutefois être combinée avec les dispositions de l'article L. 112-6 du code des relations entre le public et l'administration (CRPA), aux termes desquelles, sauf en ce qui concerne les relations entre l'administration et ses agents, les délais de recours contre une décision tacite de rejet ne sont pas opposables à l'auteur d'une demande lorsque l'accusé de réception prévu par l'article L.112-3 du même code ne lui a pas été transmis ou que celui-ci ne porte pas les mentions prévues à l'article R. 112-5 de ce code et, en particulier, dans le cas où la demande est susceptible de donner lieu à une décision implicite de rejet, la mention des voies et délais de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 janvier 1975, Sieur,, n° 89274, p. 22,,[RJ2] Rappr. CE, 21 janvier 2015, Société EURL 2B, n° 382902, p. 3.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
