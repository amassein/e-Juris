<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034971194</ID>
<ANCIEN_ID>JG_L_2017_06_000000404151</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/11/CETATEXT000034971194.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 19/06/2017, 404151, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404151</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404151.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 6 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Techstar demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction fiscale intitulée " TFP - Taxe sur les surfaces commerciales " publiée sous la référence BOI-TFP-TSC-20160406 au Bulletin officiel des finances publiques-Impôts en tant qu'elle précise les notions de " contrôle direct et indirect " et " d'enseigne commerciale " en matière de taxe sur les surfaces commerciales ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 72-657 du 13 juillet 1972 ; <br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés dans sa version issue de la loi du 4 août 2008 de modernisation de l'économie : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. Ne sont pas considérés comme magasins de commerce de détail les établissements de commerce de gros dont la clientèle est composée de professionnels pour les besoins de leur activité ou de collectivités. Lorsque ces établissements réalisent à titre accessoire des ventes à des consommateurs pour un usage domestique, ces ventes constituent des ventes au détail qui sont soumises à la taxe dans les conditions de droit commun. / Toutefois, le seuil de superficie de 400 mètres carrés ne s'applique pas aux établissements contrôlés directement ou indirectement par une même personne et exploités sous une même enseigne commerciale lorsque la surface de vente cumulée de l'ensemble de ces établissements excède 4 000 mètres carrés. ".<br/>
<br/>
              2. La société Techstar demande l'annulation du paragraphe 140 de l'instruction fiscale intitulée " TFP - Taxe sur les surfaces commerciales ", publiée sous la référence BOI-TFP-TSC-20160406 au Bulletin officiel des finances publiques-Impôts, dont l'objet est de préciser les modalités de détermination, de déclaration et de recouvrement de la taxe sur les surfaces commerciales, en tant qu'il interdirait aux entreprises exploitant, sur une surface inférieure à 400 mètres carrés, une enseigne commerciale de réseau en dehors de tout lien juridique et capitalistique avec la société tête de ce réseau, autres que les entreprises franchisées, de bénéficier de l'exonération de taxe sur les surfaces commerciales prévue par l'article 3 de la loi du 13 juillet 1972. Elle demande également l'annulation du paragraphe 150 de cette instruction en tant qu'il définirait la notion d'enseigne commerciale sans tenir compte de la dimension immatérielle qui s'attache nécessairement à cet élément essentiel du fonds de commerce.<br/>
<br/>
              3. En premier lieu, le paragraphe 140 de l'instruction attaquée indique que " de manière générale, il ressort des dispositions du code de commerce " que les filiales et les succursales sont regardées comme contrôlées par la société qui les détient, mais que les contrats de franchise ne sont " en principe pas suffisants pour constituer un contrôle du franchisé par le franchiseur ". Contrairement à ce que soutient la requérante, l'instruction, qui ne présente pas sur ce point un caractère exhaustif, ne réserve pas et ne saurait légalement pas avoir pour effet de réserver aux seules entreprises franchisées le bénéfice de l'exonération prévue par l'article 3 de la loi du 13 juillet 1972 pour les entreprises exploitant des surfaces de vente de moins de 400 mètres carrés sous une enseigne de réseau en dehors de tout lien juridique et capitalistique, au sens de l'article L. 233-3 du code de commerce, avec la société tête de ce réseau. Par suite, l'instruction fiscale n'a pas, sur ce point, méconnu les dispositions de l'article 3 de la loi du 13 juillet 1972 citées au point 1 ci-dessus.<br/>
<br/>
              4. En second lieu, aux termes du paragraphe 150 de cette instruction : " En application de l'article L. 581-3 du code de l'environnement, constitue une enseigne toute inscription, forme ou image apposée sur un immeuble et relative à une activité qui s'y exerce. / Dans le même sens, la Cour de cassation définit une enseigne comme le sigle qui identifie le lieu d'une exploitation commerciale (Cass. com., arrêts du 13 janvier 2009 pourvois n° 07-19056 et 07-19571). / Par ailleurs, l'utilisation par des établissements d'une enseigne constituée à partir d'un signe commun, protégé en application de l'article L.711-4 du code de la propriété intellectuelle, est suffisante pour remplir la condition d'enseigne commune. / L'ajout au signe commun d'une précision, relative par exemple à la clientèle visée ou à la localisation géographique de l'établissement, ne peut permettre de considérer qu'il s'agit d'une enseigne distincte, dès lors que le signe suffit à la clientèle pour rattacher l'établissement à une chaîne de distribution donnée. / Par la décision n° 2010-58 QPC du 18 octobre 2010, le Conseil Constitutionnel a jugé que le deuxième alinéa de l'article 3 de la loi n° 72-657 de la loi du 13 juillet 1972 qui prévoit que le seuil de superficie de 400 m² ne s'applique pas aux établissements contrôlés directement ou indirectement par une même personne et exploités sous une même enseigne commerciale lorsque la surface de vente cumulée de l'ensemble de ces établissements excède 4 000 m², est conforme à la Constitution. ".<br/>
<br/>
               5. Ce paragraphe 150 de l'instruction attaquée, s'il fait ainsi référence à la définition matérielle que retient de la notion d'enseigne commerciale l'article L. 581-3 du code de l'environnement, indique également que l'enseigne est un sigle qui identifie le lieu d'une exploitation commerciale et que l'utilisation comme enseigne d'un signe commun protégé sur le fondement des dispositions de l'article L. 711-4 du code de la propriété intellectuelle suffit à établir l'utilisation d'une enseigne commune. En apportant ces précisions relatives à la notion d'enseigne commerciale, ce paragraphe de l'instruction n'a pas non plus méconnu les dispositions de l'article 3 de la loi du 13 juillet 1972. <br/>
<br/>
              6. Il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par le ministre, que la société Techstar n'est pas fondée à demander l'annulation de l'instruction qu'elle attaque. Par suite, sa requête doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Techstar est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Techstar, au ministre de l'économie et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
