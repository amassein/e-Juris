<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255205</ID>
<ANCIEN_ID>JG_L_2014_07_000000367473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 16/07/2014, 367473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367473.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 8 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Sympadis, dont le siège est rue du Sablon, ZAC de la Prairie des Moulins, à Sainte-Jamme-sur-Sarthe (72380), représentée par son président directeur général en exercice ; la société Sympadis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 963 T du 17 janvier 2013 par laquelle la Commission nationale d'aménagement commercial lui a refusé l'autorisation préalable requise en vue de créer un ensemble commercial de 4 115 m², à Saint-Pavace (Sarthe), comportant un hypermarché Super U de 3 500 m², un local de 20 m², une galerie marchande de 595 m², comprenant 6 cellules de moins de 300 m² chacune ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur l'intervention de l'association des exploitants du centre commercial des Fontenelles :<br/>
<br/>
              1. Considérant que l'association des exploitants du centre commercial des Fontenelles a intérêt au rejet de la requête ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              2. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce et précisés à l'article R. 752-7 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ; <br/>
<br/>
              3. Considérant, en premier lieu, que, pour refuser l'autorisation sollicitée, la commission nationale s'est fondée sur les dispositions de l'article L. 111-1-4 du code de l'urbanisme ; qu'en vertu du principe de l'indépendance des législations du commerce et de l'urbanisme, la commission nationale ne pouvait légalement fonder sa décision sur la méconnaissance par la société pétitionnaire des règles fixées par l'article L. 111-1-4 du code de l'urbanisme, intégrées à l'article AUa du règlement du plan local d'urbanisme de la commune qui interdit toute construction et installation dans une bande de 75 m de part et d'autre de l'axe de la voie ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des pièces du dossier que son terrain d'assiette est situé dans le tissu aggloméré de la RD 313, à proximité d'une zone d'habitation et d'équipements publics, et qu'il ne présente pas de caractéristique naturelle remarquable ; que la circonstance invoquée que le terrain est vierge de toute construction ne peut suffire à établir que le projet méconnaîtrait l'objectif d'aménagement du territoire ; que, dans ces conditions, il ne contribue pas à l'étalement urbain et ne méconnaît pas l'objectif d'aménagement du territoire ;<br/>
<br/>
              5. Considérant, en troisième lieu, que, la circonstance que l'ensemble commercial sera desservi par une seule ligne de bus n'est pas de nature à compromettre l'objectif de développement durable eu égard à l'importance moyenne du projet ; que l'insertion du projet, eu égard notamment à la nature de son environnement constitué d'une rocade et d'une zone pavillonnaire et aux caractéristiques du projet, est satisfaisante ; que, dès lors, en estimant que le projet méconnaîtrait l'objectif de développement durable, la commission nationale a fait une inexacte application des dispositions rappelées ci-dessus ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la requérante est fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Sympadis qui n'est pas, dans la présente instance, la partie perdante ; que dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à la société Sympadis au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'intervention de l'association des exploitants du centre commercial des Fontenelles est admise.<br/>
<br/>
Article 2 : La décision de la Commission nationale d'aménagement commercial du 17 janvier 2013 est annulée.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Sympadis au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la société de distribution Sarthoise, la société Molière et la société Couldis au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Sympadis, à la société de distribution Sarthoise, à la société Molière, à la société Couldis et à l'association des exploitants du centre commercial des Fontenelles. <br/>
Copie en sera adressée pour information à la Commission nationale d'aménagement commercial,<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
