<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042381867</ID>
<ANCIEN_ID>JG_L_2020_09_000000426290</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/18/CETATEXT000042381867.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 28/09/2020, 426290</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426290</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426290.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. CA... CB..., M. AO... Z..., Mme AR... BA..., M. G... BW..., M. AX... AA..., M. CI... AC..., Mme CC... AZ..., M. CI... BD..., Mme DC... BL..., Mme CG... L..., M. CW... AF..., Mme CV... H..., M. AP... M..., M. BJ... Y..., Mme AN... AH..., M. D... AI..., Mme CT... BH..., M. BO... B..., Mme BB... A..., M. W... AK..., Mme AG... BN..., M. K... CQ..., Mme DB... BY..., M. BU... AL..., Mme AD... DD..., Mme CV... CU..., M. CO... AM..., Mme U... AT..., M. E... CR..., Mme BT... CE..., M. BS... CF..., Mme AG... BG..., Mme CN... O..., M. BF... P..., Mme BK... N..., M. AE... AQ..., M. BP... Q..., Mme BR... CX..., M. AU... R..., Mme I... BQ..., M. CK... CS..., Mme CH... BC..., Mme AV... AS..., Mme BZ... F..., M. J... T..., Mme CJ... BV..., M. CP... CZ..., Mme CY... CD..., M. AJ... DA..., Mme BX... X..., M. C... V..., Mme BB... BM..., M. AY... AW..., Mme S... AB..., M. BI... CL..., Mme CN... BE..., M. CA... CM... et la société civile immobilière " Prince " ont demandé au tribunal administratif de Nancy d'annuler les titres de recette, lettres de relance et mises en demeure émis à leur encontre par la commune de Cutry pour le recouvrement des frais de démolition d'immeubles frappés d'une procédure de péril. Par un jugement n° 1602544 du 4 juillet 2017, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17NC02051-17NC02052 du 16 octobre 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. CB... et autres contre ce jugement et dit n'y avoir lieu de statuer sur leur demande de sursis à exécution de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 17 décembre 2018, 13 mars 2019, 8 novembre 2019 et 31 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. CB... et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette leur appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Cutry la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ; <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. et Mme CB... et à la SCP Ohl, Vexliard, avocat de la commune de Cutry.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. CB... et les autres requérants ont conclu des contrats de vente en l'état futur d'achèvement portant sur les lots d'un ensemble immobilier situé dans la commune de Cutry (Meurthe-et-Moselle). Les constructions sont toutefois restées inachevées en raison d'une défaillance du promoteur, placé en liquidation judiciaire le 1er avril 2010. Au vu du péril que faisaient courir ces constructions, le maire de Cutry a pris le 18 juin 2013, sur le fondement de l'article L. 511-2 du code de la construction et de l'habitation, des arrêtés enjoignant aux requérants de faire procéder à la démolition de leurs immeubles et, faute d'exécution de leur part, a fait procéder d'office à ces travaux, dont il a mis le coût à la charge des requérants en émettant à leur encontre, entre le 9 et le 17 juin 2016, des avis de sommes à payer, puis des lettres de relance et enfin, à compter du 18 août 2016, des mises en demeure de payer. M. CB... et autres ont demandé au tribunal administratif de Nancy d'annuler les titres de recette, lettres de relance et mises en demeure de payer émis contre eux. Par un jugement du 4 juillet 2017, le tribunal administratif a rejeté leur demande. Ils se pourvoient en cassation contre l'arrêt du 16 octobre 2018 par lequel la cour administratif d'appel de Nancy a rejeté leur appel contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 511-2 du code de la construction et de l'habitation, dans sa rédaction applicable aux faits de l'espèce : " I. - Le maire, à l'issue d'une procédure contradictoire dont les modalités sont définies par décret en Conseil d'Etat, met le propriétaire de l'immeuble menaçant ruine, (...), en demeure de faire dans un délai déterminé, selon le cas, les réparations nécessaires pour mettre fin durablement au péril ou les travaux de démolition, ainsi que, s'il y a lieu, de prendre les mesures indispensables pour préserver les bâtiments contigus. Si l'état du bâtiment, ou d'une de ses parties, ne permet pas de garantir la sécurité des occupants, le maire peut assortir l'arrêté de péril d'une interdiction d'habiter et d'utiliser les lieux qui peut être temporaire ou définitive. Les dispositions des articles L. 521-1 à L. 521-4 sont alors applicables. (...) III. - Sur le rapport d'un homme de l'art, le maire constate la réalisation des travaux prescrits ainsi que leur date d'achèvement et prononce la mainlevée de l'arrêté de péril et, le cas échéant, de l'interdiction d'habiter et d'utiliser les lieux. (...) IV. - Lorsque l'arrêté de péril n'a pas été exécuté dans le délai fixé, le maire met en demeure le propriétaire d'y procéder dans un délai qu'il fixe et qui ne peut être inférieur à un mois.  A défaut de réalisation des travaux dans le délai imparti, le maire, par décision motivée, fait procéder d'office à leur exécution. (...) Lorsque la commune se substitue au propriétaire défaillant et fait usage des pouvoirs d'exécution d'office qui lui sont reconnus, elle agit en lieu et place des propriétaires, pour leur compte et à leurs frais. (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 1601-3 du code civil : " La vente en l'état futur d'achèvement est le contrat par lequel le vendeur transfère immédiatement à l'acquéreur ses droits sur le sol ainsi que la propriété des constructions existantes. Les ouvrages à venir deviennent la propriété de l'acquéreur au fur et à mesure de leur exécution ; l'acquéreur est tenu d'en payer le prix à mesure de l'avancement des travaux. Le vendeur conserve les pouvoirs de maître de l'ouvrage jusqu'à la réception des travaux ".  <br/>
<br/>
              4. Si elles prévoient que l'acquéreur d'un bien vendu en vertu d'un contrat de vente en l'état futur d'achèvement devient immédiatement propriétaire du terrain et des constructions existantes et propriétaire des ouvrages à venir au fur et à mesure de leur construction, les dispositions précitées ne peuvent avoir pour effet de lui transférer, avant la date de réception des travaux, les obligations de réparation ou de démolition incombant à la personne propriétaire d'un immeuble menaçant ruine, pour l'application des dispositions de l'article L. 511-2 du code de la construction et de l'habitation, dès lors que, jusqu'à cette date, il ne dispose pas des pouvoirs de maître de l'ouvrage. <br/>
<br/>
              5. Par suite, en se bornant à relever que M. CB... et autres avaient acquis en l'état futur d'achèvement les immeubles ayant fait l'objet des arrêtés de péril pris par le maire de Cutry, pour en déduire qu'ils avaient la qualité de propriétaires de ces immeubles pour l'application des dispositions de l'article L. 511-2 du code de la construction et de l'habitation, sans rechercher si la  réception des travaux de construction de ces immeubles avait eu lieu, la cour administrative d'appel de Nancy a commis une erreur de droit. M. CB... et autres sont dès lors fondés, sans qu'il soit besoin de se prononcer sur l'autre moyen de leur pourvoi, à demander l'annulation de son arrêt en tant qu'il rejette leur appel.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. CB... et autres, qui ne sont pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Cutry la somme de 4 000 euros à verser solidairement à M. CB... et aux autres requérants.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 16 octobre 2018 de la cour administrative d'appel de Nancy est annulé en tant qu'il rejette l'appel de M. CB... et autres.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Nancy. <br/>
Article 3 : La commune de Cutry versera la somme de 4 000 euros, solidairement, à M. CB... et aux autres requérants, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Cutry au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. CA... CB..., premier requérant dénommé, et à la commune de Cutry. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-02-02-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA SÉCURITÉ. IMMEUBLES MENAÇANT RUINE. CHARGE DES TRAVAUX ET RESPONSABILITÉ. - PROPRIÉTAIRE DE L'IMMEUBLE MENAÇANT RUINE AU SENS DE L'ARTICLE L. 511-2 DU CCH - NOTION - ACQUÉREUR D'UN BIEN VENDU EN VEFA (ART. 1601-3 DU CODE CIVIL) - EXCLUSION AVANT LA DATE DE RÉCEPTION DES TRAVAUX [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-001-02 POLICE. POLICES SPÉCIALES. - PROPRIÉTAIRE DE L'IMMEUBLE MENAÇANT RUINE AU SENS DE L'ARTICLE L. 511-2 DU CCH - NOTION - ACQUÉREUR D'UN BIEN VENDU EN VEFA (ART. 1601-3 DU CODE CIVIL) - EXCLUSION AVANT LA DATE DE RÉCEPTION DES TRAVAUX [RJ1].
</SCT>
<ANA ID="9A"> 135-02-03-02-02-02-02 S'ils prévoient que l'acquéreur d'un bien vendu en vertu d'un contrat de vente en l'état futur d'achèvement (VEFA) devient immédiatement propriétaire du terrain et des constructions existantes et propriétaire des ouvrages à venir au fur et à mesure de leur construction, les articles L. 511-2 du code de la construction et de l'habitation (CCH) et 1601-3 du code civil ne peuvent avoir pour effet de lui transférer, avant la date de réception des travaux, les obligations de réparation ou de démolition incombant à la personne propriétaire d'un immeuble menaçant ruine, pour l'application de l'article L. 511-2 du CCH, dès lors que, jusqu'à cette date, il ne dispose pas des pouvoirs de maître de l'ouvrage.</ANA>
<ANA ID="9B"> 49-05-001-02 S'ils prévoient que l'acquéreur d'un bien vendu en vertu d'un contrat de vente en l'état futur d'achèvement (VEFA) devient immédiatement propriétaire du terrain et des constructions existantes et propriétaire des ouvrages à venir au fur et à mesure de leur construction, les articles L. 511-2 du code de la construction et de l'habitation (CCH) et 1601-3 du code civil ne peuvent avoir pour effet de lui transférer, avant la date de réception des travaux, les obligations de réparation ou de démolition incombant à la personne propriétaire d'un immeuble menaçant ruine, pour l'application de l'article L. 511-2 du CCH, dès lors que, jusqu'à cette date, il ne dispose pas des pouvoirs de maître de l'ouvrage.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du transfert des risques, Cass. civ. 3e, 11 octobre 2000, SCI le Lion de Belfort, n° 98-21.826, Bull. civ. III n° 163.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
