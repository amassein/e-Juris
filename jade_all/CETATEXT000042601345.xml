<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042601345</ID>
<ANCIEN_ID>JG_L_2020_11_000000435627</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/60/13/CETATEXT000042601345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 30/11/2020, 435627, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435627</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:435627.20201130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 28 octobre 2019 et le 20 janvier 2020 au secrétariat du contentieux du Conseil d'État, M. et Mme B... A... demandent au Conseil d'État :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir les énonciations des paragraphes 180, 190, 210, 480, 520, 620 à 760, 810 et 820 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - impôts le 2 juillet 2015 et le 4 mars 2016 sous la référence BOI-RPPM-PVBMI-30-10-60 ; <br/>
<br/>
              2°) à titre subsidiaire, de saisir la Cour de justice de l'Union européenne d'une question préjudicielle portant sur la compatibilité du II de l'article 150-0 B ter du code général des impôts avec les objectifs poursuivis par l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009 ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 90/434/CEE du Conseil du 23 juillet 1990 ;<br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2012-1510 du 29 décembre 2012 ;<br/>
- l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018 Jacob et Lassus (C-327/16 et C-421/16) ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 18 septembre 2019 <br/>
			 C-662/18 et C-672/18) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. et Mme A... demandent l'annulation des paragraphes nos 180, 190, 210, 480, 520, 620 à 760, 810 et 820 des commentaires administratifs publiés les 2 juillet 2015 et 4 mars 2016 au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-30-10-60, par lesquels l'administration fiscale a donné son interprétation des dispositions de l'article 150-0 B ter du code général des impôts prévoyant, d'une part, les modalités de détermination des plus-values placées en report d'imposition en cas d'apport de titres à une société soumise à l'impôt sur les sociétés et, d'autre part, l'imposition, entre les mains du donataire, des plus-values placées en report d'imposition à la suite d'une opération d'apport lorsque les titres remis en rémunération de l'apport ont fait l'objet d'une donation ou d'un don manuel et ont été cédés par le donataire avant l'expiration d'un délai de dix-huit mois, ainsi que les obligations déclaratives incombant aux contribuables concernés.<br/>
<br/>
              2. Aux termes de l'article 8 de la directive du Conseil du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents, ainsi qu'au transfert du siège statutaire d'une SE ou d'une SCE d'un État membre à un autre : " 1. L'attribution, à l'occasion d'une fusion, d'une scission ou d'un échange d'actions, de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé. / (...) 6. L'application des paragraphes 1, 2 et 3 n'empêche pas les États membres d'imposer le profit résultant de la cession ultérieure des titres reçus de la même manière que le profit qui résulte de la cession des titres existant avant l'acquisition (...) ".<br/>
<br/>
              3. Aux termes de l'article 150-0 B ter du code général des impôts, dans sa rédaction en vigueur à la date de publication des commentaires administratifs dont la légalité est contestée par M. et Mme A... : " I.- L'imposition de la plus-value réalisée, directement ou par personne interposée, dans le cadre d'un apport de valeurs mobilières, de droits sociaux, de titres ou de droits s'y rapportant tels que définis à l'article 150-0 A à une société soumise à l'impôt sur les sociétés ou à un impôt équivalent est reportée si les conditions prévues au III du présent article sont remplies. Le contribuable mentionne le montant de la plus-value dans la déclaration prévue à l'article 170. / (...) Il est mis fin au report d'imposition à l'occasion : / 1° De la cession à titre onéreux, du rachat, du remboursement ou de l'annulation des titres reçus en rémunération de l'apport ; / 2° De la cession à titre onéreux, du rachat, du remboursement ou de l'annulation des titres apportés, si cet événement intervient dans un délai, décompté de date à date, de trois ans à compter de l'apport des titres. Toutefois, il n'est pas mis fin au report d'imposition lorsque la société bénéficiaire de l'apport cède les titres dans un délai de trois ans à compter de la date de l'apport et prend l'engagement d'investir le produit de leur cession, dans un délai de deux ans à compter de la date de la cession et à hauteur d'au moins 50 % du montant de ce produit : (...) / II.- En cas de transmission par voie de donation ou de don manuel des titres mentionnés au 1° du I du présent article, le donataire mentionne, dans la proportion des titres transmis, le montant de la plus-value en report dans la déclaration prévue à l'article 170 si la société mentionnée au 2° du même I est contrôlée par le donataire dans les conditions prévues au 2° du III. Ces conditions sont appréciées à la date de la transmission, en tenant compte des droits détenus par le donataire à l'issue de celle-ci. / La plus-value en report est imposée au nom du donataire et dans les conditions prévues à l'article 150-0 A : / 1° En cas de cession, d'apport, de remboursement ou d'annulation des titres dans un délai de dix-huit mois à compter de leur acquisition ; / 2° Ou lorsque l'une des conditions mentionnées au 2° du I du présent article n'est pas respectée. Le non-respect de l'une de ces conditions met fin au report d'imposition dans les mêmes conditions que celles décrites au même 2°. L'intérêt de retard prévu à l'article 1727, décompté de la date de l'apport des titres par le donateur, est applicable. / La durée de détention à retenir par le donataire est décomptée à partir de la date d'acquisition des titres par le donateur. Les frais afférents à l'acquisition à titre gratuit sont imputés sur le montant de la plus-value en report. / Le 1° du présent II ne s'applique pas en cas d'invalidité correspondant au classement dans la deuxième ou troisième des catégories prévues à l'article L. 341-4 du code de la sécurité sociale, de licenciement ou de décès du donataire ou de son conjoint ou partenaire lié par un pacte civil de solidarité soumis à une imposition commune ".<br/>
<br/>
              4. En premier lieu, il résulte des arrêts rendus par la Cour de justice de l'Union européenne le 22 mars 2018 dans les affaires C-327/16 et C-421/16 et le 18 septembre 2019 dans les affaires C-662/18 et C-672/18 qu'une mesure qui consiste à constater la plus-value issue de l'opération d'échange de titres et à reporter le fait générateur de l'imposition de cette plus-value jusqu'à l'année au cours de laquelle intervient l'évènement mettant fin à ce report d'imposition, constitue uniquement une " technique " qui, tout en permettant de sauvegarder la compétence fiscale des États membres et, partant, leurs intérêts financiers, conformément au paragraphe 6 de l'article 8 de la directive du Conseil du 18 octobre 2009, respecte le principe de neutralité fiscale établi par le paragraphe 1 de l'article 8 de cette directive, en ce qu'elle conduit à ce que l'opération d'échange de titres ne donne lieu, par elle-même, à aucune imposition de ladite plus-value. Les requérants ne sont, par suite, pas fondés à soutenir que les commentaires attaqués, en tant qu'ils rappellent que les plus-values d'apport de titres sont placées, en vertu des dispositions précitées de l'article 150-0 B ter du code général des impôts, en report d'imposition lorsque le contribuable contrôle la société bénéficiaire de l'apport réitéreraient une règle contraire au principe de neutralité fiscale résultant de la directive du Conseil du 18 octobre 2009.<br/>
<br/>
              5. En deuxième lieu, les dispositions de l'article 8 de la directive citées au point 2, dès lors qu'elles ne sont relatives qu'au traitement fiscal du gain résultant, pour l'associé d'une société apporteuse ou absorbée, de l'attribution de titres de la société bénéficiaire ou absorbante en rémunération de l'apport de titres qui lui a été consenti, sont sans incidence sur la possibilité, lorsque les titres reçus lors de l'opération d'échange ont fait ultérieurement l'objet d'une donation, de soumettre à l'impôt la plus-value correspondante entre les mains du donataire dans les conditions prévues par les dispositions de l'article 150-0 A du code général des impôts auxquelles renvoient celles du  II de l'article 150-0 B ter du même code. Par suite, les requérants ne peuvent utilement soutenir que le principe de neutralité fiscale résultant du paragraphe 1 de l'article 8 de la directive du Conseil du 18 octobre 2009 ferait, par lui-même, obstacle à ce que la plus-value placée en report d'imposition puisse être imposée entre les mains du donataire des titres en cas de survenance de l'un des évènements mentionnés aux 1° ou au 2° du II de l'article 150-0 B ter du code général des impôts.<br/>
<br/>
              6. En troisième lieu, il résulte de l'arrêt rendu le 18 septembre 2019 par la cour de justice de l'Union européenne dans les affaires C-662/18 et C-672/18 que les dispositions précitées des paragraphes 1 et 6 de l'article 8 de la directive du Conseil du 18 octobre 2009 doivent être interprétées en ce sens que, dans le cadre d'une opération d'échange de titres, elles requièrent que soit appliqué à la plus-value afférente aux titres échangés et placée en report d'imposition ainsi qu'à celle issue de la cession des titres reçus en échange le même traitement fiscal, au regard du taux d'imposition et de l'application d'un abattement fiscal pour tenir compte de la durée de détention des titres, que celui que se serait vu appliquer la plus-value qui aurait été réalisée lors de la cession des titres existant avant l'opération d'échange, si cette dernière n'avait pas eu lieu. Ces dispositions n'ont toutefois pas pour objet de régir, ainsi qu'il a été dit au point 5, les modalités d'imposition, entre les mains du donataire, de la plus-value en report grevant des titres qui, après avoir été reçus lors d'une opération échange, ont fait ultérieurement l'objet d'une donation. Par suite, les requérants ne sont pas fondés à soutenir que les commentaires attaqués réitéreraient des dispositions législatives méconnaissant le principe de neutralité découlant du paragraphe 1 de l'article 8 de la directive du Conseil du 18 octobre 2009.<br/>
<br/>
              7. En quatrième et dernier lieu, si le paragraphe 1 de l'article 15 de la directive du 18 octobre 2009 dispose qu' " Un Etat membre peut refuser d'appliquer tout ou partie des dispositions des articles 4 à 14 ou d'en retirer le bénéfice lorsqu'une des opérations visées à l'article 1er : / a) a comme objectif ou comme un de ses objectifs principaux la fraude ou l'évasion fiscale (...) ", les dispositions de cette directive n'ont, ainsi qu'il a été dit au point 5, ni pour objet ni pour effet de régir les modalités d'imposition, entre les mains du donataire, de la plus-value en report grevant des titres qui, après avoir été reçus lors d'une opération échange, ont fait ultérieurement l'objet d'une donation. Par suite, les requérants ne peuvent utilement soutenir que les commentaires attaqués réitéreraient des dispositions de la loi fiscale instituant des modalités d'imposition allant au-delà des mesures qu'un Etat-membre est autorisé à prendre pour les motifs de lutte contre la fraude et l'évasion fiscales énoncés à l'article 15 de cette même directive.<br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il y ait lieu de saisir la cour de justice de l'Union européenne à titre préjudiciel, la requête de M. et Mme A... doit être rejetée, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : La requête de M. et Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A... et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
