<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035568851</ID>
<ANCIEN_ID>JG_L_2017_09_000000413682</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/56/88/CETATEXT000035568851.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/09/2017, 413682, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-09-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413682</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:413682.20170912</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 24 août 2017 au secrétariat de la section du contentieux du Conseil d'Etat, la Caisse nationale des barreaux français demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2017-887 du 9 mai 2017 relatif à l'organisation financière de certains régimes de sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le décret contesté porte atteinte de manière grave et immédiate à ses intérêts ;<br/>
              - il existe a un doute sérieux sur la légalité du décret contesté ;<br/>
              - le décret contesté est entaché d'incompétence dès lors qu'il est intervenu dans le champ de la compétence du législateur, d'une part, en ce qu'il entérine le choix de capitalisation des réserves des régimes de retraite complémentaire et uniformise l'organisation financière des régimes de retraite complémentaire et, d'autre part, en ce qu'il limite l'autonomie de gestion des régimes de retraite complémentaire ;<br/>
              - il est entaché d'incompétence et d'un vice de procédure dès lors qu'il n'est pas établi que toutes ses dispositions ont été soumises à l'avis du Conseil d'Etat, ni que toutes les caisses de retraites concernées ont été consultées ; <br/>
              - il est entaché d'une première erreur de droit en ce qu'il méconnaît, d'une part, le principe d'autonomie de gestion des caisses chargées de la gestion des régimes de retraite complémentaire du fait de la précision, du nombre et du rigorisme des contraintes qu'il institue dans la gestion des régimes complémentaires de retraite et, d'autre part, le respect des droits de la défense du fait de l'insuffisance d'encadrement dans le passage du régime complémentaire au régime simplifiée ; <br/>
              - il est entaché d'une seconde erreur de droit en ce qu'il méconnaît le principe d'égalité ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce qu'il crée un régime non adapté aux objectifs d'intérêt général de maintien de la capacité des régimes de retraite à payer les pensions sur le moyen et long terme et de solidarité.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il en va ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers et que, en cas d'annulation, ses effets pourraient être effacés par une réparation pécuniaire ; qu'il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
              3. Considérant que le décret n° 2017-887 du 9 mai 2017 abroge le décret n° 2002-1314 du 25 octobre 2002 et modifie des dispositions du livre VI du code de la sécurité sociale principalement issues de ce décret, en vue de fixer des règles d'organisation financière et de gestion des actifs de différentes caisses de sécurité sociale gérant des régime de retraite de base et complémentaires ; que la Caisse nationale des barreaux français (CNBF), soumise à ces dispositions, demande au juge des référés du Conseil d'Etat de suspendre l'exécution des dispositions du décret du 9 mai 2017 ;  <br/>
<br/>
              4. Considérant que les dispositions litigieuses imposent aux caisses qui y sont soumises diverses contraintes d'organisation ou de contrôle dans le domaine de la gestion financière ; qu'il est ainsi fait obligation à leurs administrateurs de suivre une formation relative aux questions actuarielles et financières, qu'elles sont tenues de soumettre à leur conseil d'administration ou à une commission des placements certaines décisions d'achat ou de vente ou encore de mettre en place une fonction de contrôle des risques et des procédures de gestion de ceux-ci ; que la définition d'une politique de pilotage du ou des régimes gérés se traduit, en particulier, selon l'article R. 623-6 du code de la sécurité sociale, par l'adoption, chaque année, d'un document prévisionnel comportant différentes analyses, dont des scénarios d'évolution des perspectives économique et financières sur un horizon à quarante ans ; que le conseil d'administration est contraint d'adopter tous les trois ans au minimum, en application de l'article R. 623-9 du code de la sécurité sociale, un document relatif à la politique de placement et de gestion des risques ; qu'est également imposée la production de rapports de gestion et l'établissement, tous les trimestres au minimum, d'une vue transparente de l'actif détenu ; <br/>
<br/>
              5. Considérant que le décret contesté définit également des règles, limites et ratios de placement des actifs par catégorie, selon deux régimes, le régime auquel sont soumis les organismes qui ont adopté un document relatif à la politique de placement et de gestion des risques approuvé par la tutelle et un régime dit " simplifié ", offrant moins de possibilités, dans le cas contraire ; que le décret qui renvoie d'ailleurs pour la mise en oeuvre de certaines de ses dispositions à l'intervention d'arrêtés interministériels fixe au 1er janvier 2018 son entrée en vigueur ; qu'il comporte toutefois des dispositions permettant aux caisses de conserver, au-delà de cette date, des actifs qui ne seraient pas conformes au nouveau régime ;<br/>
<br/>
              6. Considérant que pour soutenir qu'il y a urgence à suspendre l'exécution de ce texte, la CNBF fait valoir, de manière générale, le coût économique et social important de la mise en oeuvre  des nouvelles règles de placement des actifs ; <br/>
<br/>
              7. Considérant toutefois qu'il existe pour les ressortissants de la caisse requérante, un intérêt à la mise en place des diverses mesures d'organisation ou de contrôle dans le domaine de la gestion financière mentionnées au point 4, dont nombre ont été préconisées par l'inspection générale des affaires sociales à la suite du contrôle effectué en 2013 de quelques-uns des organismes de retraite intéressés et s'imposent au demeurant déjà à eux pour certains actifs ; que, par ailleurs, la CNBF n'établit pas en quoi la mise en oeuvre des nouvelles règles de placement des actifs, au demeurant assortie de dispositions transitoires, lui causerait un préjudice grave et immédiat constitutif d'une situation d'urgence ; qu'il en résulte que l'urgence à suspendre les dispositions litigieuses n'est pas établie ; que, par suite, la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, doit dès lors être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Caisse nationale des barreaux français est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la Caisse nationale des barreaux français et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
