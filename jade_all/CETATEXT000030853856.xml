<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853856</ID>
<ANCIEN_ID>JG_L_2015_06_000000371212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/38/CETATEXT000030853856.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 29/06/2015, 371212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:371212.20150629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société de fait Pelamourgues et Valadier Escurial a demandé au tribunal administratif de Paris de lui accorder la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier 2006 au 31 décembre 2007. Par un jugement n° 1101672 du 15 mai 2012, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12PA03268 du 28 mai 2013, la cour administrative d'appel de Paris a, sur l'appel de la société, réduit le chiffre d'affaires retenu par l'administration au titre des deux années en litige et déchargé la société Pelamourgues et Valadier Escurial des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier 2006 au 30 juin 2007 en conséquence de cette réduction de la base d'imposition.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 août et 13 novembre 2013 et le 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, la société Pelamourgues et Valadier Escurial demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire intégralement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Pelamourgues et Valadier Escurial ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société de fait Pelamourgues et Valadier Escurial exploite un bar-brasserie dans le troisième arrondissement de Paris ; que cette société a fait l'objet d'une vérification de comptabilité portant sur les exercices clos en 2006 et 2007 ; qu'à l'issue de cette vérification, l'administration a écarté comme non probante la comptabilité de la société et reconstitué son chiffre d'affaires pour les exercices 2006 et 2007 en utilisant la méthode dite " des vins " ; que le vérificateur a reconstitué les ventes de vin estimées sur la période vérifiée, ces ventes étant calculées à partir des achats et de la variation des stocks physiques et des prix unitaires, en intégrant un abattement de 5 % pour tenir compte des vins non vendus en terrasse et en salle : que, pour reconstituer les recettes totales sur la période vérifiée, il a appliqué à ces ventes un coefficient multiplicateur égal à l'inverse du ratio entre ventes de vin en terrasse et en bar et recettes totales ; que  ce ratio a été calculé à partir du dépouillement des notes établies pendant 37 journées des mois de novembre 2008 à janvier 2009 ; que l'administration a procédé en conséquence au rehaussement de la base imposable à la taxe sur la valeur ajoutée de la société résultant des minorations de recettes qu'elle avait constatées et aux rappels de taxe sur la valeur ajoutée correspondants ; que, par un arrêt du 28 mai 2013, la cour administrative d'appel de Paris a jugé que le chiffre d'affaires retenu par l'administration pour les deux années en litige devait être réduit en appliquant aux ventes de vins estimées sur la période vérifiée un abattement de 7 % pour les vins non vendus et en prenant en compte les repas comprenant un verre de vin du 1er janvier 2006 au 31 décembre 2007 au lieu de la période du 1er janvier 2006 au 30 juin 2007 ; qu'elle a déchargé la société des rappels de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2006 au 31 décembre 2007 en conséquence de cette réduction de la base d'imposition ; que la société Pelamourgues et Valadier Escurial se pourvoit en cassation contre l'article 4 de l'arrêt ;<br/>
<br/>
              2. Considérant que, pour écarter le moyen tiré de ce que la méthode de reconstitution des recettes de la société utilisée par l'administration était radicalement viciée dès lors que le calcul du coefficient multiplicateur était fondé sur des données postérieures à la période en litige, la cour a jugé que cette méthode était justifiée par les insuffisances de la comptabilité de la société qui ne comprenait aucune pièce justificative des recettes en salle et en terrasse du restaurant ; qu'en statuant de la sorte, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              3. Considérant, en revanche, que, d'une part, en écartant le moyen tiré de la minoration du prix de vente de certains vins pour déterminer le calcul du ratio entre les recettes des ventes de vin et le montant total des ventes réalisées au motif que cette minoration était favorable à la société alors que, compte tenu de la méthode de reconstitution dite " des vins ", une augmentation du prix de vente des vins entraîne une diminution du coefficient multiplicateur et, par voie de conséquence, une diminution des recettes reconstituées, la cour a commis une erreur de droit ; que, d'autre part, en écartant le moyen tiré de la minoration du prix de vente des vins vendus avec certains  repas, au cours de la période vérifiée, au motif que cette minoration était favorable à la société, alors que les vins vendus dans le cadre de ces formules venaient en déduction des recettes estimées des ventes de vin, ce qui entraînait, par voie de conséquence, une diminution des recettes reconstituées, la cour a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'article 4 de son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la société de fait Pelamourgues et Valadier Escurial au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : L'article 4 de l'arrêt de la cour administrative d'appel de Paris du 28 mai 2013 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera 1 500 euros à la société Pelamourgues et Valadier au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société de fait Pelamourgues et Valadier et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
