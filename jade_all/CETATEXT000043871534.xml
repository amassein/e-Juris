<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043871534</ID>
<ANCIEN_ID>JG_L_2021_05_000000452778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/15/CETATEXT000043871534.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/05/2021, 452778, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452778.20210526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de l'arrêté du 7 mars 2021 par lequel le préfet de Mayotte l'a obligé à quitter le territoire français sans délai et a interdit son retour pendant une durée d'un an et, d'autre part, d'enjoindre au préfet de Mayotte de lui délivrer une autorisation provisoire de séjour dans l'attente du réexamen de sa demande de titre de séjour. Par une ordonnance n° 2100575 du 9 mars 2021, le juge des référés du tribunal administratif de Mayotte a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 19 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;  <br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance contestée est entachée d'irrégularité dès lors qu'il atteste de l'intensité de ses liens personnels et familiaux à Mayotte, notamment par l'attestation de concubinage avec sa compagne de nationalité française ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit au respect de sa vie privée et familiale en méconnaissance de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi qu'à l'intérêt supérieur de son enfant de nationalité française, dont il assure l'éducation et l'entretien, garanti par l'article 3-1 de la convention internationale relative aux droits de l'enfant. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la convention internationale relative aux droits de l'enfant ; <br/>
              - le code de l'entrée du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte des pièces du dossier que le préfet de Mayotte a pris, le 7 mars 2021, à l'encontre de M. A..., né le 13 mai 1995 aux Comores, un arrêté par lequel il l'a obligé à quitter le territoire français sans délai et a interdit son retour pendant une durée d'un an. M. A... a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de cet arrêté et, d'autre part, d'enjoindre au préfet de Mayotte de lui délivrer une autorisation provisoire de séjour dans l'attente du réexamen de sa demande de titre de séjour. Par une ordonnance du 9 mars 2021, le juge des référés a rejeté sa demande. M. A... relève appel de cette ordonnance.<br/>
<br/>
              3. Pour rejeter la demande de M. A..., le juge des référés du tribunal administratif de Mayotte a relevé, en ce qui concerne la mesure d'éloignement vers les Comores dont il fait l'objet, qu'il n'était pas fondé à soutenir que cette mesure porte une atteinte grave et manifestement illégale à son droit au respect de sa vie privée et familiale ou à l'intérêt supérieur de son enfant dès lors que, d'une part, il ne démontre pas de l'ancienneté de son séjour à Mayotte ni de l'intensité des liens personnels et familiaux qu'il y aurait développés, les pièces qu'il a produites, essentiellement composées d'avis d'impôt sur le revenu de 2014 à 2021 et quelques factures dont la plus ancienne est datée du 11 novembre 2016, ne suffisant pas pour ce faire et, d'autre part, il ne justifie pas contribuer, de manière stable et continue, à l'entretien et à l'éducation de son enfant de nationalité française. M. A... n'apporte en appel aucun élément susceptible d'infirmer l'appréciation retenue par le juge des référés de première instance.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. A... ne peut être accueilli. Sa requête ne peut dès lors qu'être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
