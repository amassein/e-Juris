<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546720</ID>
<ANCIEN_ID>JG_L_2021_05_000000433863</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 27/05/2021, 433863</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433863</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433863.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 29 juin 2020, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme D... F..., de M. A... B... et de M. E... F... dirigées contre l'arrêt n° 17BX00912, 18BX03314, 18BX03325 de la cour administrative d'appel de Bordeaux du 25 juin 2019 en tant seulement que cet arrêt, pour évaluer le préjudice subi par Melle C... B... du fait de son besoin d'assistance par une tierce personne, retient un taux horaire de 13 euros et en tant qu'il statue sur l'indemnisation des frais exposés pour l'achat et la construction d'un logement adapté à son handicap.<br/>
<br/>
              Par un mémoire en défense enregistré le 31 août 2020, le centre hospitalier de Libourne conclut au rejet du pourvoi et à ce que la somme de 3 500 euros soit mise à la charge de Mme F..., de M. B... et de M. F... au titre de l'article L. 761-1 du code de justice administrative. Il soutient que les moyens du pourvoi ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique enregistré le 23 septembre 2020, Mme F... et autres reprennent les conclusions de leur pourvoi, par les mêmes moyens.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP L. Poulet, Odent, avocat de Mme F... et autres, et à Me Le Prado, avocat du centre hospitalier de Libourne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :  <br/>
<br/>
              1. Par un arrêt du 25 juin 2019, la cour administrative d'appel de Bordeaux a condamné le centre hospitalier de Libourne à verser diverses indemnités à Mme F..., M. Fredoux et M. Montillaud à raison des fautes commises par cet établissement de santé lors de la naissance de leur fille et petite-fille C..., atteinte d'une infirmité motrice cérébrale sévère. Par une décision du 29 juin 2020, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de Mme F... et autres dirigées contre cet arrêt, d'une part en tant qu'il fixe le montant de l'indemnité due au titre des frais d'assistance par une tierce personne en se fondant sur un taux horaire de 13 euros et, d'autre part, en tant qu'il statue sur l'indemnisation des frais exposés pour assurer à C... B... un logement adapté à son handicap.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il fixe le montant des frais d'assistance :<br/>
<br/>
              2. Lorsque le juge administratif indemnise dans le chef de la victime d'un dommage corporel la nécessité de recourir à l'aide d'une tierce personne, il détermine le montant de l'indemnité réparant ce préjudice en fonction des besoins de la victime et des dépenses nécessaires pour y pourvoir. Il doit à cette fin se fonder sur un taux horaire déterminé, au vu des pièces du dossier, par référence, soit au montant des salaires des personnes à employer augmentés des cotisations sociales dues par l'employeur, soit aux tarifs des organismes offrant de telles prestations, en permettant le recours à l'aide professionnelle d'une tierce personne d'un niveau de qualification adéquat et sans être lié par les débours effectifs dont la victime peut justifier. Il n'appartient notamment pas au juge, pour déterminer cette indemnisation, de tenir compte de la circonstance que l'aide a été ou pourrait être apportée par un membre de la famille ou un proche de la victime.<br/>
<br/>
              3. Par suite, en retenant, sur la seule base d'une référence au montant du salaire minimum brut augmenté des cotisations sociales dues par l'employeur, un taux horaire de 13 euros pour déterminer le montant de l'indemnité due à la jeune C... au titre de son besoin d'assistance par une tierce personne, sans tenir compte, ainsi qu'il ressortait des pièces du dossier qui lui était soumis, de ce qu'une assistance adaptée à sa situation de handicap s'élevait un coût plus d'une fois et demie supérieur au montant retenu, la cour a méconnu les règles énoncées au point précédent.<br/>
<br/>
              4. Mme F... et autres sont, par suite, fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant que celui-ci fixe le montant de ces frais d'assistance en retenant un taux horaire de 13 euros.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les frais liés à l'adaptation du logement :<br/>
<br/>
              5. Il résulte des termes de l'arrêt attaqué que, pour rejeter les conclusions de Mme F... tendant à l'indemnisation de divers frais liés à la construction d'un nouveau logement, la cour administrative d'appel, après avoir relevé que l'intéressée soutenait se trouver dans l'impossibilité d'aménager le logement dont elle était locataire ou d'en louer un autre qui soit adapté aux besoins de sa fille, a jugé qu'en tout état de cause, en cas d'achat ou de construction d'un logement adapté, seuls les frais exposés pour aménager un tel logement conformément aux besoins de l'enfant étaient susceptibles d'être indemnisés.<br/>
<br/>
              6. En statuant ainsi, alors que, outre les dépenses d'aménagement du logement rendues nécessaires par le handicap de l'enfant, d'autres dépenses nées d'une décision d'achat ou de construction d'un logement sont, dès lors qu'une telle décision est imposée par le handicap de l'enfant et dans la mesure où ces dépenses visent à répondre à ses besoins, susceptibles d'être regardées comme étant en lien direct avec la faute de l'établissement de santé et comme devant, par suite, faire l'objet d'une indemnisation, la cour a commis une erreur de droit. <br/>
<br/>
              7. Mme F... et autres sont, par suite, fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant que celui-ci statue sur l'indemnisation des frais exposés pour la construction d'un logement adapté au handicap de sa fille.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge des requérants, qui ne sont pas dans la présente instance la partie perdante, une somme que demande, à ce titre, le centre hospitalier de Libourne. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Libourne une somme de 1 000 euros, chacun, à verser à Mme F..., M. Frédoux et M. Montillaud au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 25 juin 2019 est annulé en tant que, pour évaluer les frais d'assistance par une tierce personne, il retient un taux horaire de 13 euros et en tant qu'il statue sur l'indemnisation des frais exposés pour la construction d'un logement adapté.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le centre hospitalier de Libourne versera à Mme F..., à M. B... et à M. F... la somme de 1 000 euros, chacun, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme D... F..., première requérante dénommée, et au centre hospitalier de Libourne.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - PRÉJUDICE TENANT À LA NÉCESSITÉ DE RECOURIR À L'AIDE D'UNE TIERCE-PERSONNE - EVALUATION D'ESPÈCE SUR LA BASE DES PIÈCES DU DOSSIER [RJ1].
</SCT>
<ANA ID="9A"> 60-04-03 Lorsque le juge administratif indemnise dans le chef de la victime d'un dommage corporel la nécessité de recourir à l'aide d'une tierce personne, il détermine le montant de l'indemnité réparant ce préjudice en fonction des besoins de la victime et des dépenses nécessaires pour y pourvoir.,,,Il doit à cette fin se fonder sur un taux horaire déterminé, au vu des pièces du dossier, par référence, soit au montant des salaires des personnes à employer augmentés des cotisations sociales dues par l'employeur, soit aux tarifs des organismes offrant de telles prestations, en permettant le recours à l'aide professionnelle d'une tierce personne d'un niveau de qualification adéquat et sans être lié par les débours effectifs dont la victime peut justifier.,,,Il n'appartient notamment pas au juge, pour déterminer cette indemnisation, de tenir compte de la circonstance que l'aide a été ou pourrait être apportée par un membre de la famille ou un proche de la victime.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 25 mai 2018, Mme,, n° 393827, T. 903-911.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
