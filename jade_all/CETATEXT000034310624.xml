<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034310624</ID>
<ANCIEN_ID>JG_L_2017_03_000000399506</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/31/06/CETATEXT000034310624.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 29/03/2017, 399506, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399506</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399506.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, et deux mémoires en réplique enregistrés les 4 mai, 27 mai et 3 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Layher demande au Conseil d'Etat, dans le dernier état de ses écritures :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir le paragraphe n° 130 de l'instruction publiée sous la référence BOI-IS-AUT-30-20160302 en tant qu'il exclut du bénéfice de l'exonération les distributions réalisées entre sociétés d'un même groupe lorsqu'il ne relève pas du régime de l'intégration fiscale même si la condition de détention du capital de 95 % fixée par l'article 223 A du code général des impôts est remplie ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler pour excès de pouvoir l'intégralité du paragraphe n° 130 de l'instruction BOI-IS-AUT-30-20160302 ;<br/>
<br/>
              3°) d'enjoindre à l'administration, sur le fondement de l'article L. 911-1 du code de justice administrative, soit d'appliquer l'instruction BOI-IS-AUT-30-20160302 corrigée aux distributions effectuées avant le 1er janvier 2017 entre sociétés détenues à 95 % ou plus, soit d'appliquer l'exonération prévue au 1° du I de l'article 235 ter ZCA du code général des impôts aux sociétés d'un même groupe n'ayant pas opté pour le régime de l'intégration fiscale et au sein duquel le seuil de participation est égal ou supérieur à 95 % ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 62 ;<br/>
              - la décision du Conseil constitutionnel n° 2016-571 QPC du 30 septembre 2016 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 février 2017, présentée par la société Layher. <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Layher demande, dans le dernier état de ses écritures, à titre principal, l'annulation pour excès de pouvoir du paragraphe n° 130 de l'instruction publiée sous la référence BOI-IS-AUT-30-20160302, qui prévoit notamment " que sont exclus de l'assiette de la contribution additionnelle les montants distribués entre sociétés d'un même groupe au sens de l'article 223 A du code général des impôts ", en tant que ce paragraphe exclut du bénéfice de l'exonération les distributions réalisées entre sociétés du même groupe lorsqu'il ne relève pas du régime de l'intégration fiscale, même si la condition de détention de 95 % du capital fixée par l'article 223 A du code général des impôts est remplie. Elle demande, à titre subsidiaire, l'annulation de l'intégralité du paragraphe n° 130 de cette instruction. Elle demande également que soit enjoint au ministre sur le fondement de l'article L. 911-1 du code de justice administrative, soit d'appliquer l'instruction BOI-IS-AUT-30-20160302 corrigée aux distributions effectuées avant le 1er janvier 2017 entre sociétés détenues à 95 % ou plus, soit d'appliquer l'exonération prévue au 1° du I de l'article 235 ter ZCA du code général des impôts aux sociétés d'un même groupe n'ayant pas opté pour le régime de l'intégration fiscale et au sein duquel le seuil de participation est égal ou supérieur à 95 %.<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction issue de la loi du 29 décembre 2015 de finances rectificative pour 2015, en vigueur à la date de l'instruction attaquée : " Les sociétés ou organismes français ou étrangers passibles de l'impôt sur les sociétés en France (...) sont assujettis à une contribution additionnelle à cet impôt au titre des montants qu'ils distribuent au sens des articles 109 à 117 du présent code. / La contribution est égale à 3 % des montants distribués. Toutefois, elle n'est pas applicable : / 1° Aux montants distribués entre sociétés du même groupe au sens de l'article 223 A ou de l'article 223 A bis, y compris pour les montants mis en paiement par une société du groupe au cours du premier exercice dont le résultat n'est pas pris en compte dans le résultat d'ensemble si la distribution a lieu avant l'événement qui entraîne sa sortie du groupe (...) ".<br/>
<br/>
              3. Par sa décision n° 2016-571 QPC du 30 septembre 2016, le Conseil constitutionnel a déclaré contraires à la Constitution les mots " entre sociétés du même groupe au sens de l'article 223 A " figurant au 1° du paragraphe I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction résultant de la loi du 29 décembre 2015 de finances rectificative pour 2015. Il a reporté au 1er janvier 2017 les effets de l'abrogation ainsi prononcée. A compter de cette date, les mots " entre sociétés du même groupe au sens de l'article 223 A " figurant au 1° du paragraphe I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction résultant de la loi du 29 décembre 2015, que l'instruction attaquée a pour objet de commenter, ne sont plus en vigueur.<br/>
<br/>
              4. Le Conseil constitutionnel ayant ainsi différé jusqu'au 1er janvier 2017 les effets de la déclaration d'inconstitutionnalité qu'il prononçait, la société requérante n'est pas fondée à s'en prévaloir à l'appui de son recours pour excès de pouvoir contre l'instruction qu'elle attaque. <br/>
<br/>
              5. Toutefois, les juridictions administratives et judiciaires, à qui incombe le contrôle de la compatibilité des lois avec le droit de l'Union européenne ou les engagements internationaux de la France, peuvent déclarer que des dispositions législatives incompatibles avec le droit de l'Union ou ces engagements sont inapplicables au litige qu'elles ont à trancher. Il appartient, par suite, au juge du litige, s'il n'a pas fait droit aux conclusions d'une requête en tirant les conséquences de la déclaration d'inconstitutionnalité d'une disposition législative prononcée par le Conseil constitutionnel, d'examiner, dans l'hypothèse où un moyen en ce sens est soulevé devant lui, s'il doit écarter la disposition législative en cause du fait de son incompatibilité avec une stipulation conventionnelle ou, le cas échéant, une règle du droit de l'Union européenne. <br/>
<br/>
              6. Aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes ". Aux termes de l'article 14 de cette convention : " La jouissance des droits et libertés reconnus dans la présente convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions politiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation ". Une distinction entre des personnes placées dans une situation analogue est discriminatoire, au sens de ces stipulations, si elle n'est pas assortie de justifications objectives et raisonnables, c'est-à-dire si elle ne poursuit pas un objectif d'utilité publique ou si elle n'est pas fondée sur des critères objectifs et rationnels en rapport avec les buts de la loi.<br/>
<br/>
              7. Le 1° du paragraphe I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction résultant de la loi du 29 décembre 2015, applicable jusqu'au 31 décembre 2016, exonère de la contribution additionnelle à l'impôt sur les sociétés au titre des montants distribués les distributions réalisées entre sociétés du même groupe fiscalement intégré au sens de l'article 223 A du code général des impôts. Sont ainsi exclues du bénéfice de cette exonération les distributions réalisées entre sociétés d'un même groupe dès lors que celui-ci ne relève pas du régime de l'intégration fiscale, même si la condition de détention de 95 %  du capital fixée par l'article 223 A est remplie. Il en résulte, lorsque la condition de détention est satisfaite, une différence de traitement entre les sociétés d'un même groupe qui réalisent, en son sein, des distributions, selon que ce groupe relève ou non du régime de l'intégration fiscale. <br/>
<br/>
              8. D'une part, en instituant la contribution additionnelle à l'impôt sur les sociétés au titre des montants distribués, le législateur a poursuivi un objectif de rendement, qui ne constitue pas, en lui-même, un objectif d'utilité publique de nature à justifier, lorsque la condition de détention est satisfaite, la différence de traitement instituée entre les sociétés d'un même groupe réalisant, en son sein, des distributions, selon que ce groupe relève ou non du régime de l'intégration fiscale.<br/>
<br/>
              9. D'autre part, la contribution instituée par l'article 235 ter ZCA du code général des impôts est un impôt autonome, distinct de l'impôt sur les sociétés. L'exonération instituée par les dispositions contestées est donc sans lien avec le régime de l'intégration fiscale, qui ne concerne que l'impôt sur les sociétés et n'a pas pour objet d'exonérer de cet impôt les sociétés membres d'un groupe. Par conséquent, lorsque la condition de détention est satisfaite, les sociétés d'un même groupe réalisant, en son sein, des distributions sont placées, au regard de l'objet de la contribution, dans la même situation, que ce groupe relève ou non du régime de l'intégration fiscale. Ce critère de distinction ne présente ainsi pas un caractère objectif et rationnel en rapport avec les buts de la loi.<br/>
<br/>
              10. Il résulte de ce qui précède que le 1° du paragraphe I de l'article 235 ter ZCA du code général des impôts, dans sa rédaction résultant de la loi du 29 décembre 2015, crée une différence de traitement, qui ne repose pas sur des justifications objectives et raisonnables, entre les distributions selon qu'elles sont réalisées entre sociétés d'un même groupe qui relève ou non du régime de l'intégration fiscale prévu à l'article 223 A du code général des impôts, alors même que la condition de détention de 95 % du capital fixée par cet article est remplie. Cette différence de traitement qui résulte du 1° du I de l'article 235 ter ZCA, que l'instruction attaquée a pour objet de commenter, est, par suite, incompatible avec les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales combinées avec l'article premier de son premier protocole additionnel. <br/>
<br/>
              11. Les dispositions du 1° du I de l'article 235 ter ZCA étant, dans leur ensemble, incompatibles avec ces stipulations conventionnelles, les énonciations du paragraphe n° 130 de l'instruction attaquée, relatives aux distributions entre sociétés d'un même groupe fiscalement intégré et qui prescrit d'en faire application, ne peuvent être regardées comme divisibles. Il convient, dès lors, de rejeter les conclusions principales de la société Layher tendant à l'annulation de ces énonciations en tant qu'elles excluent du bénéfice de l'exonération les distributions réalisées entre sociétés d'un même groupe lorsqu'il ne relève pas du régime de l'intégration fiscale même si la condition de détention de 95 % du capital fixée par l'article 223 A du code général des impôts est remplie. Il y a lieu, en revanche, de faire droit, sans qu'il soit besoin d'examiner l'autre moyen de sa requête, à ses conclusions subsidiaires tendant à l'annulation de l'intégralité du paragraphe n° 130 de l'instruction BOI-IS-AUT-30-20160302. <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              12. Aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ".<br/>
<br/>
              13. L'annulation prononcée au point 11 de la présente décision implique seulement que l'administration ne fasse plus application du paragraphe n° 130 de l'instruction en litige, dès lors qu'il a été annulé dans son intégralité. Il y a lieu, par suite, de rejeter les conclusions à fin d'injonction présentées par la société Layher.<br/>
<br/>
              Sur les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Layher au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le paragraphe n° 130 de l'instruction BOI-IS-AUT-30-20160302 est annulé.<br/>
Article 2 : L'Etat versera à la société Layher une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la SAS Layher et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
