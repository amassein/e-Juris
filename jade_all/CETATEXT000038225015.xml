<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038225015</ID>
<ANCIEN_ID>JG_L_2019_03_000000414248</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/50/CETATEXT000038225015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 12/03/2019, 414248, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414248</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:414248.20190312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rennes d'annuler la décision du 1er février 2013 du directeur général du centre hospitalier universitaire (CHU) de Rennes en tant qu'elle met fin à ses fonctions de chef de service des urgences médico-chirurgicales et de condamner le CHU de Rennes à lui verser la somme de 190 000 euros en réparation de ces préjudices moral et psychologique. Par un jugement n° 1301138 du 12 mars 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT02039 du 12 mai 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par M.A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 septembre et 12 décembre 2017 et 8 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du CHU de Rennes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. A...et à la SCP Waquet, Farge, Hazan, avocat du centre hospitalier régional universitaire de Rennes.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 1er février 2013, abrogeant une décision du 17 janvier 2013 qui avait la même portée, le directeur général du centre hospitalier universitaire (CHU) de Rennes a mis fin aux fonctions de chef du service des urgences médico-chirurgicales adultes exercées par M. A..., professeur des universités praticien hospitalier, depuis le 1er septembre 2011. Le tribunal administratif de Rennes a rejeté la demande que M. A...a présentée contre cette décision. La cour administrative d'appel de Nantes a rejeté l'appel de M. A...contre ce jugement en retenant, d'une part, que ses conclusions tendant à l'annulation de la décision mettant fin à ses fonctions ne sont pas fondées et, d'autre part, que ses conclusions indemnitaires sont irrecevables en l'absence de demande préalable de nature à lier le contentieux. M. A...se pourvoit en cassation contre l'arrêt dont, eu égard aux moyens qu'il soulève, il doit être regardé comme demandant l'annulation en tant seulement qu'il a rejeté ses conclusions tendant à l'annulation de la décision mettant fin à ses fonctions.<br/>
<br/>
              2. En vertu de l'article R. 6164-5 du code de la santé publique, il peut être mis fin, dans l'intérêt du service, aux fonctions de responsable de structure interne, service ou unité fonctionnelle dans les centres hospitaliers et les centres hospitalo-universitaires par décision du directeur, à son initiative, après avis du président de la commission médicale d'établissement et du chef de pôle.<br/>
<br/>
              3. Aux termes de l'article 65 de la loi du 22 avril 1905 : " Tous les fonctionnaires civils et militaires, tous les employés et ouvriers de toutes administrations publiques ont droit à la communication personnelle et confidentielle de toutes les notes, feuilles signalétiques et tous autres documents composant leur dossier, soit avant d'être l'objet d'une mesure disciplinaire ou d'un déplacement d'office, soit avant d'être retardé dans leur avancement à l'ancienneté ". En vertu de ces dispositions, un agent public faisant l'objet d'une mesure prise en considération de sa personne, qu'elle soit ou non justifiée par l'intérêt du service, doit être mis à même de demander la communication de son dossier, en étant averti en temps utile de l'intention de l'autorité administrative de prendre la mesure en cause. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, à la suite d'un entretien tenu avec M. A...le 9 janvier 2013 en présence du président de la commission médicale d'établissement, le directeur général lui a adressé le 10 janvier suivant un courrier par lequel il lui indiquait qu'il lui confirmait les termes de son entretien de la veille selon lesquels il était mis fin à ses fonctions de chef de service. Le même jour, un message a été adressé à l'ensemble des chefs de service et chefs de pôle de l'établissement pour leur faire part de cette décision en leur indiquant qu'un chef de service intérimaire prendrait ses fonctions dès le 14 janvier. Après recueil des avis du président de la commission médicale d'établissement et du chef de pôle qu'imposaient les dispositions de l'article R. 6164-5 du code de la santé publique rappelées ci-dessus, une décision retirant à M. A...ses fonctions de chef de service a été signée le 17 janvier. Par une nouvelle décision du 1er février, la décision du 17 janvier a été abrogée et le retrait des fonctions de chef de service de M. A...a été prononcé à nouveau.<br/>
<br/>
              5. La cour administrative d'appel a, par l'arrêt attaqué, jugé que, par l'entretien tenu le 9 janvier et le courrier du 10 janvier, M. A...avait été informé dans un délai suffisant de la mesure envisagée à son encontre et ainsi mis à même de demander la communication de son dossier avant la décision intervenue le 1er février. Il ressort toutefois des pièces du dossier soumis aux juges du fond que compte tenu des termes fermes et définitifs employés par l'administration lors de l'entretien du 9 janvier, confirmés par un courrier à l'intéressé le lendemain et repris dans un courriel destiné aux médecins de l'hôpital, le directeur général ne s'est pas borné à indiquer à M. A...qu'il envisageait de lui retirer ses fonctions de chef de service, mais lui a fait part de sa décision en ce sens, dissuadant l'intéressé de présenter utilement ses observations avant la formalisation de cette décision par les documents signés les 17 janvier et 1er février. En retenant que la décision attaquée avait été prise au terme d'une procédure régulière, la cour administrative d'appel a donc entaché son appréciation d'une dénaturation qui justifie, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'annulation de son arrêt en tant qu'il a rejeté les conclusions tendant à l'annulation de la décision mettant fin aux fonctions de M.A.... <br/>
              . <br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans cette mesure en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Ainsi qu'il a été dit ci-dessus, c'est au terme d'une procédure irrégulière que le directeur général du CHU de Rennes a mis fin le 17 janvier 2013, par une décision abrogée et réitérée avec une motivation complétée le 1er février 2013, aux fonctions de M.A..., chef du service des urgences médico-chirurgicales adultes. Il suit de là que M. A...est fondé à soutenir que c'est à tort que par le jugement attaqué, le tribunal administratif de Rennes a rejeté sa requête tendant à l'annulation de la décision du 1er février 2013. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHU de Rennes la somme de 6 000 euros à verser à M. A...en application des dispositions de l'article L. 761-1 du code de justice administrative au titre des instances de premier ressort, d'appel et de cassation.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n°15NT02039 du 12 mai 2017 de la cour administrative d'appel de Nantes est annulé en tant qu'il a rejeté les conclusions de M. A...tendant à l'annulation de la décision du 1er février 2013 mettant fin à ses fonctions de chef de service des urgences médico-chirurgicales adultes.<br/>
Article 2 : Le jugement du 12 mars 2015 du tribunal administratif de Rennes est annulé en tant qu'il a rejeté les conclusions de M. A...tendant à l'annulation de la décision du 1er février 2013 mettant fin à ses fonctions de chef de service des urgences médico-chirurgicales adultes. <br/>
Article 3 : La décision du 1er février 2013 mettant fin aux fonctions de chef de service des urgences médico-chirurgicales adultes exercées par M. A...est annulée.<br/>
Article 4 : Le centre hospitalier universitaire de Rennes versera à M. A...la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au centre hospitalier universitaire de Rennes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
