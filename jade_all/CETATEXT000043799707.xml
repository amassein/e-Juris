<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799707</ID>
<ANCIEN_ID>JG_L_2021_07_000000433578</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799707.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/07/2021, 433578, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433578</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433578.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Montreuil de prononcer la décharge ou, à titre subsidiaire, la réduction, de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2011, ainsi que des pénalités correspondantes. Par un jugement no 1509294 du 19 décembre 2016, ce tribunal a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 17VE00546 du 27 juin 2019, la cour administrative d'appel de Versailles, statuant sur l'appel formé par M. B... contre ce jugement, a prononcé une décharge partielle des impositions en litige, correspondant à une réduction de 10 % des revenus taxés entre ses mains, a prononcé la décharge de la pénalité pour activité occulte à laquelle il avait été assujetti et a rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi enregistré le 13 août 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'État d'annuler cet arrêt, en tant qu'il a partiellement accueilli l'appel de M. B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de la comptabilité de la société à responsabilité limitée Aldès, établie en France et qui y exerce une activité de construction immobilière, dont M. B..., domicilié en Suisse, est associé et gérant minoritaire, l'administration fiscale a estimé que des sommes qui avaient été facturées à cette société au cours de l'année 2011 par la société de droit suisse Algiluc, qui détient 99,8% de son capital, avaient en réalité rémunéré des prestations d'assistance administrative et de gestion que lui avaient rendues M. B..., par ailleurs administrateur de la société Algiluc et détenteur, avec des membres de sa famille, de son capital. En application des dispositions du II de l'article 155 A du code général des impôts, l'administration a imposé les sommes en cause entre les mains de M. B..., dans la catégorie des bénéfices non commerciaux et, estimant qu'il avait exercé en France une activité non commerciale occulte d'assistance administrative et de gestion, l'a assujetti à la pénalité prévue au c) de l'article 1728 du même code. Par un jugement du 19 décembre 2016, le tribunal administratif de Montreuil a rejeté la demande de M. B... tendant à la décharge de la cotisation supplémentaire d'impôt sur le revenu mise à sa charge au titre de l'année 2011 ainsi que de cette pénalité. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 27 juin 2019 par lequel la cour administrative d'appel de Versailles, faisant partiellement fait droit à l'appel formé par M. B... contre ce jugement, a prononcé une décharge partielle des impositions en litige, correspondant à une réduction d'assiette de 10% résultant de la taxation des rémunérations en litige dans la catégorie des traitements et salaires, ainsi que la décharge de la pénalité pour activité occulte.<br/>
<br/>
              2. Aux termes de l'article 155 A du code général des impôts : " I. Les sommes perçues par une personne domiciliée ou établie hors de France en rémunération de services rendus par une ou plusieurs personnes domiciliées ou établies en France sont imposables au nom de ces dernières : - soit, lorsque celles-ci contrôlent directement ou indirectement la personne qui perçoit la rémunération des services ; - soit, lorsqu'elles n'établissent pas que cette personne exerce, de manière prépondérante, une activité industrielle ou commerciale, autre que la prestation de services ; - soit, en tout état de cause, lorsque la personne qui perçoit la rémunération des services est domiciliée ou établie dans un Etat étranger ou un territoire situé hors de France où elle est soumise à un régime fiscal privilégié au sens mentionné à l'article 238 A. II. Les règles prévues au I ci-dessus sont également applicables aux personnes domiciliées hors de France pour les services rendus en France (...) ". Les prestations dont la rémunération est ainsi susceptible d'être imposée entre les mains de la personne qui les a effectuées correspondent à un service rendu pour l'essentiel par elle et pour lequel la facturation par une personne domiciliée ou établie hors de France ne trouve aucune contrepartie réelle dans une intervention propre de cette dernière, permettant de regarder ce service comme ayant été rendu pour son compte. Ces dispositions ne dispensent pas l'administration, pour soumettre cette rémunération à l'impôt sur le revenu entre les mains de la personne ayant rendu les services, de faire application des règles de taxation relatives à la catégorie de revenus dont elle relève. La détermination de cette catégorie ne saurait dépendre que de l'analyse des relations existant entre la personne qui a rendu pour l'essentiel les services facturés et le bénéficiaire de ces services.   <br/>
<br/>
               3. La cour administrative d'appel, par des motifs non contestés de son arrêt, a jugé que les prestations d'assistance administrative et de gestion, prévues par une convention conclue entre la société Aldès et la société de droit suisse Algiluc, avaient été à bon droit regardées par l'administration comme ayant en réalité été rendues à la société Aldès par M. B..., au moyen de locaux et de matériels mis à sa disposition par cette société et que les sommes en litige devaient être regardées comme la rémunération de ces prestations, taxables entre les mains de M. B... en application des dispositions du II de l'article 155 A du code général des impôts. <br/>
<br/>
              4. En premier lieu, après avoir relevé que M. B... exerçait les fonctions de gérant minoritaire de la société Aldès et que les prestations administratives, comptables et juridiques qu'il lui rendait pour les besoins de son activité de promotion immobilière et qui avaient donné lieu aux rémunérations en litige se rattachaient à l'exercice de ces fonctions, la cour administrative d'appel a jugé que les sommes en litige étaient taxables dans la catégorie des traitements et salaires. En statuant ainsi, la cour, qui a porté sur les faits de l'espèce une appréciation souveraine non entachée de dénaturation, n'a pas commis d'erreur de droit, dès lors qu'à supposer que les prestations effectuées par M. B... excédaient les actes de gestion courante de la société Aldès, elles ne pouvaient, eu égard à leur nature et aux conditions de leur fourniture relevées par le juge du fond, être regardées comme caractérisant l'exercice d'une activité libérale distincte des fonctions de gérant. <br/>
<br/>
              5. En second lieu, dès lors qu'elle a ainsi jugé à bon droit que les sommes en litige, versées par la société Aldès à M. B..., rémunéraient ses fonctions de gérant et non des prestations rendues dans l'exercice d'une activité non commerciale distincte de ces fonctions, la cour n'a entaché son arrêt ni d'erreur de droit, ni d'erreur de qualification juridique des faits en jugeant que les conditions de mise en oeuvre de la pénalité pour activité occulte prévue par les dispositions du c) de l'article 1728 du code général des impôts n'étaient pas réunies.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le ministre n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il a partiellement accueilli l'appel formé par M. B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
