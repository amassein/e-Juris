<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520618</ID>
<ANCIEN_ID>JG_L_2020_11_000000428494</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520618.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/11/2020, 428494</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428494.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 28 février, 16 mai, 25 novembre 2019 et les 6 février et 27 août 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Compiègne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-1328 du 28 décembre 2018 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, et des collectivités de Saint-Barthélemy, de Saint-Martin, et de Saint-Pierre-et-Miquelon, en tant qu'il fixe sa population ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de faire procéder par l'Institut national de la statistique et des études économiques (INSEE) à la répartition entre les deux communes des habitants de l'immeuble situé au 40, rue d'Amiens à Compiègne / 2, avenue Octave-Butin à Margny-lès-Compiègne, en fonction du critère relatif à l'utilisation des services publics ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 51-711 du 7 juin 1951 ;<br/>
              - la loi n° 2002-276 du 27 février 2002 ; <br/>
              - le décret n° 2003-485 du 5 juin 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet, Odent, avocat de la commune de Compiègne ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La commune de Compiègne demande l'annulation pour excès de pouvoir du décret du 28 décembre 2018 authentifiant les chiffres des populations de métropole, des départements d'outre-mer de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, et des collectivités de Saint-Barthélemy, de Saint-Martin, et de Saint-Pierre-et-Miquelon, en tant que ce décret, en renvoyant aux valeurs figurant dans les tableaux consultables sur le site internet de l'Institut national de la statistique et des études économiques (INSEE), arrête à 41 660 habitants le chiffre de sa population totale, sans y inclure une partie des habitants d'un immeuble implanté à la fois sur son territoire au 40, rue d'Amiens et sur celui de la commune de Margny-lès-Compiègne au 2, avenue Octave-Butin.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 2151-1 du code général des collectivités territoriales : " I. - Les personnes prises en compte dans les catégories de population définies ci-dessous sont les personnes résidant dans les logements d'une commune, celles résidant dans les communautés telles que définies aux V et VI du présent article, les personnes sans abri et les personnes résidant habituellement dans des habitations mobiles. / II. - Les catégories de population sont : / 1. La population municipale ; / 2. La population comptée à part ; / 3. La population totale, qui est la somme des deux précédentes. / III. - La population municipale d'une commune, mentionnée au 1 du II du présent article, comprend : / 1. Les personnes ayant leur résidence habituelle sur le territoire de la commune. (...) ". <br/>
<br/>
              3. D'autre part, aux termes de l'article 1er de la loi du 7 juin 1951 sur l'obligation, la coordination et le secret en matière de statistiques : " I. Le service statistique public comprend l'Institut national de la statistique et des études économiques et les services statistiques ministériels. / Les statistiques publiques regroupent l'ensemble des productions issues : / - des enquêtes statistiques dont la liste est arrêtée chaque année par un arrêté du ministre chargé de l'économie ; / - de l'exploitation, à des fins d'information générale, de données collectées par des administrations, des organismes publics ou des organismes privés chargés d'une mission de service public. / La conception, la production et la diffusion des statistiques publiques sont effectuées en toute indépendance professionnelle (...) ". Aux termes de l'article 156 de la loi du 27 février 2002 relative à la démocratie de proximité : " I.- Le recensement de la population est effectué sous la responsabilité et le contrôle de l'Etat. / (...) / III.- La collecte des informations est organisée et contrôlée par l'Institut national de la statistique et des études économiques. / (...) / VII.- Pour établir les chiffres de la population, l'Institut national de la statistique et des études économiques utilise les informations collectées dans chaque commune au moyen d'enquêtes de recensement exhaustives ou par sondage, les données démographiques non nominatives issues des fichiers administratifs, notamment sociaux et fiscaux, que l'institut est habilité à collecter à des fins exclusivement statistiques, ainsi que les résultats de toutes autres enquêtes statistiques réalisées en application de l'article 2 de la loi n° 51-711 du 7 juin 1951 précitée. / (...) / VIII.- Un décret authentifie chaque année les chiffres des populations de métropole, des départements d'outre-mer, de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, des circonscriptions administratives et des collectivités territoriales. / IX.- Les informations relatives à la localisation des immeubles, nécessaires à la préparation et à la réalisation des enquêtes de recensement, sont librement échangées entre l'Institut national de la statistique et des études économiques, les communes et les établissements publics de coopération intercommunale intéressés. / (...) ". Aux termes de l'article 21 du décret du 5 juin 2003 relatif au recensement de la population : " Les enquêtes de recensement concernent les logements, à l'exception des logements de fonction dans les communautés. (...) ". Aux termes de l'article 26 du même décret : " Les informations de localisation mentionnées au IX de l'article 156 de la loi du 27 février 2002 susvisée sont les suivantes : / 1. En ce qui concerne les immeubles bâtis, les coordonnées géographiques, le type et le nom de la voie, le numéro dans la voie, un complément d'adresse si celui-ci est nécessaire, le type d'immeuble, la date de construction, la date d'entrée dans le répertoire d'immeubles localisés, la date de dernière modification (ou de destruction), l'aspect du bâti, le nombre de logements, le nombre d'étages, le nombre de communautés, le nombre d'établissements, le nombre d'équipements urbains ; / 2. En ce qui concerne le logement, l'immeuble auquel ce logement appartient, l'étage, la position dans l'étage, le numéro de porte ou toute autre indication topographique et le nom de l'occupant principal ". <br/>
<br/>
              4. En premier lieu, il résulte des dispositions citées au point 3 qu'il appartient à l'INSEE, dans le respect des textes régissant la statistique et le recensement, de déterminer, sous le contrôle du juge de l'excès de pouvoir, les méthodes sur la base desquelles sont établis les résultats du recensement. Dès lors, le directeur général de l'INSEE était compétent pour déterminer le critère de rattachement à la population totale d'une commune des personnes résidant dans les locaux d'habitation inclus dans un immeuble implanté sur les territoires de plusieurs communes limitrophes.<br/>
<br/>
              5. En second lieu, pour l'application des dispositions citées aux points 2 et 3, l'INSEE peut légalement, en vue de garantir la fiabilité des enquêtes de recensement qu'il organise et la stabilité des unités statistiques objectives qu'il utilise à cette fin, recenser les personnes habitant dans un immeuble dont l'emprise s'étend sur le territoire de plusieurs communes parmi la population totale d'une seule commune, celle sur le territoire de laquelle est située l'entrée de l'immeuble sous l'adresse qui la répertorie, sans retenir ni le critère de l'utilisation des services publics mis en oeuvre pour les personnes résidant dans une communauté au sens de l'article R. 2151-1 du code général des collectivités territoriales, ni celui de l'inscription sur les listes électorales qui ne détermine pas le lieu de la résidence habituelle, ni celui de l'adresse figurant dans les registres fiscaux auxquels n'ont pas accès les personnes concourant à la préparation et à la réalisation des enquêtes de recensement. Lorsque l'immeuble dont l'emprise s'étend sur le territoire de plusieurs communes comporte plusieurs entrées situées sur les territoires de différentes communes, il appartient à l'INSEE de déterminer laquelle de ces entrées constitue l'entrée principale de l'immeuble afin de rattacher l'immeuble dans son ensemble à la commune où est située cette entrée principale. A cette fin, l'INSEE peut légalement retenir le critère, objectif et stable, de l'entrée par laquelle les piétons accèdent à l'immeuble et, si l'accès piétonnier est possible par différentes entrées, celui de l'entrée par laquelle s'effectue la desserte de l'immeuble par les services publics.<br/>
<br/>
              6. Ainsi et alors qu'il est en l'espèce constant que l'entrée de l'immeuble en cause est située sur le territoire de la commune de Margny-lès-Compiègne, le moyen tiré de ce que le décret attaqué serait entaché d'illégalité en ce qu'il n'a pas tenu compte des personnes résidant dans cet immeuble pour fixer le chiffre de la population totale de la commune de Compiègne doit être écarté.<br/>
<br/>
              7. Il résulte de ce qui précède que la commune de Compiègne n'est pas fondée à demander l'annulation pour excès de pouvoir du décret attaqué. Ses conclusions tendant à ce qu'il soit enjoint au Premier ministre de faire procéder par l'INSEE à la répartition entre les deux communes des habitants de l'immeuble situé au 40, rue d'Amiens à Compiègne / 2, rue Octave-Butin à Margny-lès-Compiègne ne peuvent qu'être rejetées en conséquence, de même que celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la commune de Compiègne est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Compiègne et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Premier ministre, à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et au ministre des outre-mer. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-01-05 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. IDENTITÉ DE LA COMMUNE. POPULATION DE LA COMMUNE. - RECENSEMENT - PERSONNES HABITANT DANS UN IMMEUBLE DONT L'EMPRISE S'ÉTEND SUR LE TERRITOIRE DE PLUSIEURS COMMUNES [RJ1] - MÉTHODE [RJ2] - 1) RATTACHEMENT À LA POPULATION D'UNE SEULE COMMUNE - LÉGALITÉ - 2) CRITÈRES - A) CAS D'UN IMMEUBLE COMPORTANT UNE ENTRÉE UNIQUE - LIEU DE CETTE ENTRÉE SOUS L'ADRESSE QUI LA RÉPERTORIE - LÉGALITÉ - B) CAS D'UN IMMEUBLE COMPORTANT DES ENTRÉES DANS DIFFÉRENTES COMMUNES - I) ENTRÉE PRINCIPALE - II) CRITÈRES - ACCÈS PIÉTONNIER OU ENTRÉE DE DESSERTE PAR LES SERVICES PUBLICS - LÉGALITÉ.
</SCT>
<ANA ID="9A"> 135-02-01-01-05 1) Pour l'application, d'une part, de l'article R. 2151-1 du code général des collectivités territoriales (CGCT), d'autre part, de l'article 1er de la loi n° 51-711 du 7 juin 1951, de l'article 156 de la loi n° 2002-276 du 27 février 2002 et des articles 21 et 26 du n° 2003-485 du 5 juin 2003, l'Institut national de la statistique et des études économiques (INSEE) peut légalement, en vue de garantir la fiabilité des enquêtes de recensement qu'il organise et la stabilité des unités statistiques objectives qu'il utilise à cette fin, recenser les personnes habitant dans un immeuble dont l'emprise s'étend sur le territoire de plusieurs communes parmi la population totale d'une seule commune.,,,2) a) Cette commune peut légalement être celle sur le territoire de laquelle est située l'entrée de l'immeuble sous l'adresse qui la répertorie, sans retenir ni le critère de l'utilisation des services publics mis en oeuvre pour les personnes résidant dans une communauté au sens de l'article R. 2151-1 du CGCT, ni celui de l'inscription sur les listes électorales qui ne détermine pas le lieu de la résidence habituelle, ni celui de l'adresse figurant dans les registres fiscaux auxquels n'ont pas accès les personnes concourant à la préparation et à la réalisation des enquêtes de recensement.,,,b) i) Lorsque l'immeuble dont l'emprise s'étend sur le territoire de plusieurs communes comporte plusieurs entrées situées sur les territoires de différentes communes, il appartient à l'INSEE de déterminer laquelle de ces entrées constitue l'entrée principale de l'immeuble afin de rattacher l'immeuble dans son ensemble à la commune où est située cette entrée principale.,,,ii) A cette fin, l'INSEE peut légalement retenir le critère, objectif et stable, de l'entrée par laquelle les piétons accèdent à l'immeuble et, si l'accès piétonnier est possible par différentes entrées, celui de l'entrée par laquelle s'effectue la desserte de l'immeuble par les services publics.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du recensement de la population relevant de communautés, CE, 5 juillet 2010, Commune de Saint-Servais, n° 325723, p. 242 ; CE, 26 avril 2013, Commune des Gonds, n° 357221, T. p. 461.,,[RJ2] Rappr., s'agissant du contrôle restreint exercé par le juge sur la méthode de recensement, CE, 29 juin 2011, Commune de la Ville-aux-Dames, n° 337138, T.  pp. 800-1101 ; CE, 29 juin 2011, Communauté de communes de l'Etempois Sud Essonne et commune d'Etampes, n°s 337068 337069, T. pp. 800-1101.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
