<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041982571</ID>
<ANCIEN_ID>JG_L_2020_06_000000434117</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/25/CETATEXT000041982571.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 09/06/2020, 434117</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434117</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434117.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au juge des référés du tribunal administratif de La Réunion d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 11 octobre 2018 par lequel le maire de la commune de Saint-Pierre a décidé de ne pas renouveler l'autorisation dont elle bénéficiait pour l'occupation de l'emplacement E6 du port de plaisance de la commune par son bateau " La Désirade ". Par une ordonnance n° 1900366 du 16 août 2019, le juge des référés a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 et 17 septembre 2019 et le 27 février 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Pierre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code des transports ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
- le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Saint-Pierre et à la SCP Le Bret-Desaché, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté n° 157/2017, le maire de la commune de Saint-Pierre a autorisé en 2017 Mme B... à occuper l'emplacement E 6 du port de la commune, pour son bateau " La Désirade ". Cette autorisation, accordée sans limitation de durée, a été tacitement renouvelée au titre de l'année 2018. Par une décision du 11 octobre 2018 prise en application de l'article 5 de l'arrêté d'autorisation relatif à sa reconduction, le maire de la commune n'a pas renouvelé cette autorisation à compter de sa prochaine échéance, soit à compter du 1er janvier 2019, et a demandé à Mme B... de libérer son emplacement à cette date. Mme B... a demandé au juge des référés du tribunal administratif de La Réunion d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette décision. La commune se pourvoit en cassation contre l'ordonnance du 16 août 2019 par laquelle ce juge des référés a fait droit à cette demande.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : "  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : (...) 4° Retirent ou abrogent une décision créatrice de droits ; (...) 7° Refusent une autorisation, sauf lorsque la communication des motifs pourrait être de nature à porter atteinte à l'un des secrets ou intérêts protégés par les dispositions du a au f du 2° de l'article L. 311-5 (...) ".<br/>
<br/>
              4. La décision par laquelle l'autorité gestionnaire du domaine public rejette une demande de délivrance d'une autorisation unilatérale d'occupation du domaine public constitue un refus d'autorisation au sens du 7° de l'article L. 211-2 précité du code des relations entre le public et l'administration et doit par suite être motivée en application de ces dispositions. En revanche, la décision par laquelle l'autorité gestionnaire du domaine public met fin à une autorisation unilatérale d'occupation du domaine public, délivrée à titre précaire et révocable, notamment la décision de ne pas renouveler, à la prochaine échéance, une autorisation tacitement renouvelable constitue une abrogation de cette autorisation.  Les dispositions précitées du 4° de l'article L. 211-2 du code des relations entre le public et l'administration n'imposent pas qu'une telle décision soit motivée, sauf dans le cas particulier où elle devrait être regardée comme ayant créé des droits au profit de son bénéficiaire.<br/>
<br/>
              5. Pour ordonner la suspension de l'exécution de la décision du 11 octobre 2018 du maire de la commune de ne pas renouveler l'autorisation d'occupation du domaine public portuaire à son échéance annuelle, soit le 1er janvier 2019, le juge des référés du tribunal administratif de La Réunion a considéré que le moyen tiré de ce que cette décision, qu'il a regardée comme un refus d'autorisation, n'était pas motivée était propre à créer, en l'état de l'instruction, un doute sérieux quant à sa légalité. En statuant ainsi, alors que, ainsi qu'il a été dit ci-dessus, cette décision procédait à l'abrogation d'une autorisation d'occupation domaniale non créatrice de droits et n'avait donc pas à être motivée, le juge des référés a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que la commune de Saint-Pierre est fondée à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Comme il a été dit ci-dessus, le moyen tiré du défaut de motivation de la décision du 11 octobre 2018 n'est pas motivée n'est pas propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de cette décision. Il en résulte que la demande de Mme B... tendant à la suspension, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'exécution de cette décision ne peut qu'être rejetée. <br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B... la somme que la commune de Saint-Pierre demande au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre et sur le fondement de l'article 37 de la loi du 10 juillet 1991 à la charge de la commune de Saint-Pierre qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1900366 du 16 août 2019 du juge des référés du tribunal administratif de La Réunion est annulée. <br/>
Article 2 : La demande de Mme B... présentée devant le juge des référés du tribunal administratif de La Réunion est rejetée. <br/>
Article 3 : Les conclusions présentées par la commune de Saint-Pierre et par Mme B... au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Pierre et à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
