<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528052</ID>
<ANCIEN_ID>JG_L_2016_05_000000380687</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528052.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 11/05/2016, 380687, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380687</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:380687.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête sommaire et un mémoire complémentaire, enregistrés les 27 mai et 26 août 2014 au secrétariat du contentieux du Conseil d'Etat, l'organisme à vocation sanitaire (OVS) Porc Bretagne, l'OVS Porc Poitou-Charentes, l'association régionale porcine de promotion sanitaire (AREPSA), l'OVS Porc Pays de la Loire, l'organisation sanitaire porcine Région Centre, l'OVS Porc Normandie, l'OVS Porc Midi-Pyrénées - ASAMIP, l'association nationale sanitaire porcine (ANSP), l'organisation sanitaire régionale avicole Nord-Pas-de-Calais Picardie Champagne Ardenne, l'organisation sanitaire avicole Bretagne (OSAB), l'organisation sanitaire avicunicole Région centre, l'organisation sanitaire avicole et cunicole Région Pays de la Loire, l'organisation sanitaire avicole Auvergne (OSAA) et l'organisation sanitaire avicole Rhône Alpes demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 31 mars 2014 portant reconnaissance des organismes à vocation sanitaire dans le domaine animal ou végétal ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à chaque association requérante de la somme de 2 000 euros, au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code rural et de la pêche maritime ; <br/>
              - l'ordonnance n° 2011-862 du 22 juillet 2011 ; <br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'ordonnance du 22 juillet 2011 relative à l'organisation de l'épidémiosurveillance, de la prévention et de la lutte contre les maladies animales et végétales et aux conditions de délégation de certaines tâches liées aux contrôles sanitaires et phytosanitaires a complété le code rural et de la pêche maritime, notamment ses articles L. 201-1 et suivants, aux fins de définir une nouvelle organisation en matière de santé animale et végétale, principalement fondée sur la constitution de réseaux de surveillance et de prévention des dangers sanitaires ; qu'en vue de permettre à l'Etat de s'appuyer sur des organismes compétents dans le domaine sanitaire et de mettre à la disposition des organisations professionnelles les outils nécessaires, le législateur a habilité l'autorité administrative à reconnaître des organismes à vocation sanitaire  ou des organisations vétérinaires à vocation technique ou des associations sanitaires régionales regroupant les organismes participant à ces réseaux, en leur confiant des missions de surveillance et de prévention, étendues le cas échéant à des missions de lutte contre les dangers sanitaires ; qu'en application de ces dispositions législatives, le décret du 30 juin 2012 relatif à la reconnaissance des organismes à vocation sanitaire, des organisations vétérinaires à vocation technique, des associations sanitaires régionales ainsi qu'aux conditions de délégations de missions liées aux contrôles sanitaires a introduit dans le code rural et de la pêche maritime un article R. 201-12 renvoyant à un arrêté du ministre chargé de l'agriculture  la reconnaissance des organismes à vocation sanitaire, à raison d'un seul par domaine d'activité, animal ou végétal, pour une région donnée ; qu'eu égard à la contestation qu'ils soulèvent, l'OVS Porc Bretagne et autres doivent être regardés comme demandant l'annulation pour excès de pouvoir de l'arrêté ministériel du 31 mars 2014 en tant seulement qu'il porte reconnaissance  des organismes à vocation sanitaire dans le domaine animal ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du gouvernement : "  A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions (...) peuvent signer au nom du ministre ou du secrétaire d'Etat et par délégation l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : (...) les directeurs d'administration centrale (...) " ; que M. A...B...a été nommé directeur général de l'alimentation au ministère de l'agriculture par un décret du 9 février 2012, publié le lendemain au Journal officiel de la République française ; que, dès lors et contrairement à ce que soutiennent l'OVS Porc Bretagne et autres, M. B...justifiait d'une délégation régulière du ministre chargé de l'agriculture lui permettant de signer l'arrêté attaqué ;  <br/>
<br/>
              3.  Considérant que, pour soutenir que l'arrêté attaqué est entaché d'un défaut d'impartialité de son auteur, l'OVS Porc Bretagne et autres se bornent à faire état de contacts qui auraient été pris, préalablement à son édiction, entre la Fédération nationale de groupements de défense sanitaire (FNGDS) et les services du ministère de l'agriculture ; qu'il ne ressort pas des pièces du dossier que ces contacts préliminaires, justifiés par l'instruction du dossier par l'administration, sont de nature à caractériser un quelconque manquement à l'obligation d'impartialité s'imposant à celle-ci ; que, par suite, ce moyen doit être écarté ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 201-12 du code rural et de la pêche maritime, " les organismes à vocation sanitaire mentionnés à l'article L. 201-9 dont l'objet social est d'exercer leurs activités sur l'ensemble du territoire d'une région peuvent être reconnus par arrêté du ministre chargé de l'agriculture pour le domaine animal ou le domaine végétal./ Un seul organisme à vocation sanitaire peut être reconnu par domaine d'activité pour une région donnée. Un organisme à vocation sanitaire régional peut comporter des sections départementales. " ; qu'aux termes de l'article R. 201-13 du même code, " la reconnaissance d'un organisme à vocation sanitaire est subordonnée au respect des conditions suivantes : / 1° Avoir pour objet principal la protection de l'état sanitaire des animaux, des aliments pour animaux, des denrées alimentaires d'origine animale ou des végétaux et produits végétaux ; / 2° Accepter l'adhésion de plein droit de tout propriétaire ou détenteur d'animaux ou de végétaux entrant dans le champ d'intervention de l'organisme ; / 3° Justifier d'un fonctionnement garantissant la représentation équilibrée des adhérents ; / 4° Employer des personnes disposant de compétences techniques dans le domaine animal ou végétal, garanties notamment par une formation initiale dans les domaines vétérinaire ou phytosanitaire et par une mise à jour de leurs connaissances ; / 5° Disposer de moyens permettant d'assurer une gestion comptable séparée pour l'exercice de chacune de leurs activités ; / 6° Justifier, pour le domaine concerné, l'exercice d'actions sanitaires sur l'aire d'intervention considérée ; / 7° Disposer d'un système de permanence et de diffusion de l'information, mobilisable en cas de crise sanitaire, pour les dangers sanitaires de première et de deuxième catégorie ; / 8° Présenter des garanties d'indépendance et d'impartialité, notamment vis-à-vis des intérêts économiques particuliers des adhérents. " ; <br/>
<br/>
              5. Considérant que, si l'OVS Porc Bretagne et autres soutiennent que certains des organismes ayant fait l'objet, en vertu de l'arrêté attaqué, d'une reconnaissance en qualité d'OVS ne justifieraient pas d'une expérience antérieure, acquise dans chacun des types d'élevage animal, une telle exigence n'est pas prévue par les dispositions réglementaires précitées ; que celles-ci imposent en revanche qu'un seul organisme soit reconnu, au titre de l'ensemble de la filière animale et pour une région donnée ; qu'il ressort des pièces du dossier que les agréments litigieux ont été attribués à des organismes ayant présenté des candidatures relevant de l'ensemble de la filière animale et pour une même région ; que si l'OVS Porc Bretagne et autres font valoir que certains organismes dont la candidature n'a pas été retenue justifiaient d'une plus grande expérience que ceux qui ont été agréées, le ministre chargé de l'agriculture soutient, sans être sérieusement contredit, que les organismes non retenus auxquels se réfèrent l'OVS Porc Bretagne et autres ne s'étaient portés candidats que pour un type d'élevage donné, leur candidature étant, pour ce motif, irrecevable ;<br/>
<br/>
              6. Considérant que, contrairement à ce que soutiennent l'OVS Porc Bretagne et autres, il ne ressort pas des pièces du dossier que le ministre de l'agriculture ait accordé un agrément en qualité d'OVS à des organismes ne justifiant pas des conditions d'impartialité et d'indépendance exigées par les dispositions de l'article R. 201-13 du code rural et de la pêche maritime ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'OVS Porc Bretagne et autres ne sont pas fondés à demander l'annulation de l'arrêté attaqué ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L.761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que l'article L. 761-1 du code de justice administrative fait obstacle à ce que la somme demandée par l'OVS Porc Bretagne et autres au titre de ces dispositions soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'OVS Porc Bretagne et autres est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'OVS Porc Bretagne, à l'OVS Porc Poitou-Charentes, à l'association régionale porcine de promotion sanitaire (AREPSA), à l'OVS Porc Pays de la Loire, à l'organisation sanitaire porcine Région Centre, à  l'OVS Porc Normandie, à l'OVS Porc Midi-Pyrénées - ASAMIP, à l'association nationale sanitaire porcine (ANSP), à l'organisation sanitaire régionale avicole Nord-Pas-de-Calais Picardie Champagne Ardenne, à l'organisation sanitaire avicole Bretagne (OSAB), à l'organisation sanitaire avicunicole Région Centre, à l'organisation sanitaire avicole et cunicole Région Pays de la Loire, à l'organisation sanitaire avicole Auvergne (OSAA), à l'organisation sanitaire avicole Rhône Alpes et au ministre de l'agriculture, de l'agroalimentaire et de la forêt, porte-parole du gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
