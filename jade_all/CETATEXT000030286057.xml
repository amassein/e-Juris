<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286057</ID>
<ANCIEN_ID>JG_L_2015_02_000000369898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 25/02/2015, 369898, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369898.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour Mme A...B..., demeurant ...; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de réformer la décision du 18 février 2013 par laquelle le garde des sceaux, ministre de la justice, lui a alloué une indemnité de 49 843 euros en réparation de la perte de salaire subie du 1er septembre 2007 au 9 janvier 2013, afin de porter cette somme à 267 044,26 euros ; <br/>
<br/>
              2°) de condamner l'Etat à lui verser la somme de 217 201,26 euros en réparation du préjudice financier résultant de l'éviction illégale de ses fonctions de substitut placé auprès du procureur général près la cour d'appel de Fort-de-France du 1er septembre 2007 au 9 janvier 2013 ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administratif et la somme de 35 euros au titre de l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
<br/>
              Vu la loi n° 50-407 du 3 avril 1950 ;<br/>
<br/>
              Vu le décret n° 2003-1284 du 26 décembre 2003 ;<br/>
<br/>
              Vu l'arrêté du 3 mars 2010 pris en application du décret n° 2003-1284 du 26 décembre 2003 relatif au régime indemnitaire de certains magistrats de l'ordre judiciaire ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que, par une décision du 1er octobre 2010, le Conseil d'Etat, statuant au contentieux, a annulé la décision du 11 octobre 2007 du garde des sceaux, ministre de la justice, prononçant à l'encontre de Mme B...la sanction d'exclusion définitive de l'Ecole nationale de la magistrature, l'arrêté du même ministre du 13 octobre 2007 mettant fin à ses fonctions d'auditeur de justice à compter du 30 octobre 2007 ainsi que le décret du Président de la République du 16 novembre 2007 rapportant le décret du 18 juillet 2007 la nommant substitut placé auprès du procureur général près la cour d'appel de Fort-de-France ; que, par un arrêté du 29 janvier 2013, le garde des sceaux, ministre de la justice, a procédé à la reconstitution de la carrière de Mme B...et l'a nommée, à compter du 1er septembre 2007, substitut placé auprès du procureur général près la cour d'appel de Fort-de-France ; que, par une décision du 18 février 2013, le garde des sceaux, ministre de la justice, lui a alloué une somme de 49 843 euros en réparation de la perte de salaire subie du 1er septembre 2007 au 9 janvier 2013 à raison de l'éviction illégale de ses fonctions de magistrat ; que Mme B... demande au Conseil d'Etat d'annuler cette décision en tant qu'elle limite à cette somme le montant de l'indemnité qui lui est due et de condamner l'Etat à lui verser la somme de 217 201,26 euros afin d'assurer la réparation intégrale de son préjudice financier ;<br/>
<br/>
              2. Considérant qu'en vertu des principes généraux qui régissent la responsabilité des personnes publiques, un agent public irrégulièrement évincé a droit à la réparation intégrale du préjudice qu'il a effectivement subi du fait de la mesure illégalement prise à son encontre, y compris au titre de la perte des rémunérations auxquelles il aurait pu prétendre s'il était resté en fonctions ; que, pour l'évaluation du montant de l'indemnité due, doit être prise en compte, outre la nature et la gravité des illégalités affectant la mesure d'éviction et, le cas échéant, les fautes commises par l'intéressé, la perte du traitement ainsi que celle des primes et indemnités dont celui-ci avait, pour la période en cause, une chance sérieuse de bénéficier, à l'exception de celles qui, eu égard à leur nature, à leur objet et aux conditions dans lesquelles elles sont versées, sont seulement destinées à compenser des frais, charges ou contraintes liés à l'exercice effectif des fonctions ; qu'il y a lieu de déduire, le cas échéant, le montant des rémunérations professionnelles que l'agent a pu se procurer au cours de la période d'éviction ; <br/>
<br/>
              3. Considérant que doivent être pris en compte, au titre de l'évaluation de la perte de revenus subie par Mme B...du fait de la mesure d'éviction du service illégalement prise à son encontre, les sommes correspondant aux traitements qu'elle aurait dû percevoir ainsi que celles correspondant à la part forfaitaire et à la part modulable de la prime prévue par le décret du 26 décembre 2003 relatif au régime indemnitaire de certains magistrats de l'ordre judiciaire, dont elle avait une chance sérieuse de bénéficier ; que, s'agissant de l'évaluation du préjudice subi à raison de la perte de la prime forfaitaire, il y a lieu de tenir compte du taux de prime applicable aux magistrats placés en vertu de l'arrêté du 3 mars 2010 pris en application du décret du 26 décembre 2003 relatif au régime indemnitaire de certains magistrats judiciaires et, s'agissant, de la perte de la prime modulable, du taux moyen prévu par le même arrêté ; qu'en revanche, la majoration prévue par la loi du 3 avril 1950 concernant les conditions de rémunération et les avantages divers accordés aux fonctionnaires en service dans les départements de la Martinique, de la Guadeloupe, de la Guyane et de la Réunion, ne peut être prise en compte, dès lors qu'elle est seulement destinée à compenser des charges liées à l'exercice effectif des fonctions ; qu'à la suite d'une mesure d'instruction, l'administration, qui n'est pas contredite sur ce point, a évalué la perte de traitement et des parts forfaitaire et modulable de la prime prévue par le décret du 26 décembre 2003 durant la période en cause à 207 068,45 euros ; <br/>
<br/>
              4. Considérant, d'une part, qu'il résulte de l'instruction que la mesure d'éviction du service prise à l'encontre de Mme B...a été motivée par l'utilisation frauduleuse répétée, au cours des mois de juin et juillet 2007, de la carte bancaire d'un magistrat auprès duquel elle avait effectué un stage dans le cadre de sa formation à l'Ecole nationale de la magistrature ; que ces faits, qui ont entraîné la condamnation de l'intéressée à une peine de deux mois d'emprisonnement avec sursis ainsi que l'adoption à son encontre, par un arrêté du garde des sceaux du 18 octobre 2012, d'une sanction disciplinaire de déplacement d'office, sont constitutifs d'un grave manquement à la probité, à la dignité et à l'honneur ainsi qu'aux devoirs qui s'attachent à la fonction de magistrat ; que, d'autre part, l'annulation par le Conseil d'Etat de la mesure d'éviction du service prononcée à son encontre était motivée par l'irrégularité de la procédure de retrait de la nomination de magistrat, alors qu'aurait dû être suivie la procédure disciplinaire, et par l'illégalité de la décision d'exclusion définitive de l'Ecole nationale de la magistrature prise alors que l'intéressée avait déjà acquis la qualité de magistrat judiciaire ; que, compte tenu tant du comportement de l'intéressée que de la nature des illégalités entachant la mesure d'éviction prise à son encontre, il sera fait une juste appréciation de son préjudice au titre des pertes de rémunérations en l'évaluant à la somme de 103 534,22 euros ; que Mme B...a continué à percevoir, pendant la période d'éviction du service, des rémunérations versées par l'Ecole nationale de la magistrature qui s'élèvent à 109 661,05 euros ; qu'elle s'est également vu verser la somme de 49 843 euros au titre de la réparation de son préjudice financier ; qu'il suit de là que les sommes déjà perçues par l'intéressée excèdent le préjudice qu'elle a subi ; que, par suite, et sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le ministre, les conclusions présentées par Mme B...tendant à la majoration de l'indemnité accordée par l'administration ne peuvent qu'être rejetées ; <br/>
<br/>
              Sur les conclusions présentées par Mme B...au titre des articles L. 761-1 et R. 761 1 du code de justice administrative :<br/>
<br/>
              5. Considérant que les dispositions des articles L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu de laisser la contribution pour l'aide juridique à Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B...est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
