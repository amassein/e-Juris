<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030322719</ID>
<ANCIEN_ID>JG_L_2015_03_000000384004</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/32/27/CETATEXT000030322719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 06/03/2015, 384004, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384004</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384004.20150306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1401343 du 22 août 2014, enregistrée le 28 août 2014 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Besançon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de Mme B...A...;<br/>
<br/>
              Vu la requête, enregistrée le 20 août 2014 au greffe du tribunal administratif de Besançon, présentée par Mme B...A..., demeurant... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le point 3 du chapitre 5 de l'instruction n° RH2A/2013/12/7317 du 18 décembre 2013 du directeur général des finances publiques relative aux mutations et premières affectations des cadres B et C de l'année 2014 ; <br/>
<br/>
              2°) d'annuler toutes les mutations vers les départements ultramarins à compter du 1er septembre 2014 si elles ont été obtenues en raison de la priorité définie par cette instruction ; <br/>
<br/>
              3°) d'enjoindre à la direction générale des finances publiques de prononcer son affectation dans le département de la Réunion ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution et notamment son article 1er ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur les conclusions dirigées contre les instructions des 18 décembre 2013 et du 18 décembre 2014 du directeur général des finances publiques relative aux mutations et premières affectations des cadres B et C au titre des années 2014 et 2015 : <br/>
<br/>
              1. Considérant que si aucun principe général non plus qu'aucune règle ne s'oppose à ce que la publication d'un acte intéressant les personnels prenne la forme d'une mise en ligne de cette décision sur l'intranet de l'administration à laquelle ils appartiennent, ce mode de publicité n'est susceptible de faire courir le délai de recours contentieux à l'égard des agents et des groupements représentatifs du personnel qu'à la condition, d'une part, que l'information ainsi diffusée puisse être regardée, compte tenu notamment de sa durée, comme suffisante et, d'autre part, que le mode de publicité par voie électronique et les effets juridiques qui lui sont attachés aient été précisés par un acte réglementaire ayant lui-même été régulièrement publié ; qu'ainsi la seule circonstance, invoquée par le ministre de l'économie, que l'instruction du 18 décembre 2013 a été diffusée sur le site intranet de la direction générale des finances publiques n'est ainsi pas de nature à établir que cette publication aurait fait courir le délai de recours contentieux ; que, par ailleurs, cette instruction, qui édicte, dans les développements critiqués par la requérante, les conditions dans lesquelles les fonctionnaires de catégorie B et C de la direction générale des finances publiques originaires d'un département d'outre-mer bénéficient d'une priorité de mutation pour l'année 2014, revêt un caractère impératif ; qu'il suit de là que le ministre n'est pas fondé à soutenir que la requête serait irrecevable ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 60 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction applicable à la date de l'instruction litigieuse : " L'autorité compétente procède aux mouvements des fonctionnaires après avis des commissions administratives paritaires. (...) / Dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille. Priorité est donnée aux fonctionnaires séparés de leur conjoint pour des raisons professionnelles, aux fonctionnaires séparés pour des raisons professionnelles du partenaire avec lequel ils sont liés par un pacte civil de solidarité (...), aux fonctionnaires handicapés (...) et aux fonctionnaires qui exercent leurs fonctions, pendant une durée et selon des modalités fixées par décret en Conseil d'Etat, dans un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles. Priorité est également donnée aux fonctionnaires placés en situation de réorientation professionnelle (...) " ; <br/>
<br/>
              3. Considérant que les dispositions du point 3 du chapitre 5 des instructions du 18 décembre 2013 et du 18 décembre 2014 instaurent une priorité au bénéfice des agents originaires d'un département d'outre-mer pour une mutation vers leur département d'origine ; qu'elles fixent ainsi une règle de nature statutaire relative aux conditions de mutation des agents, que le ministre des finances et des comptes publics ne tenait d'aucun texte, notamment pas des statuts des agents concernés, le pouvoir d'édicter ; que, par suite, Mme A...est fondée à soutenir que ces dispositions ont été prises par une autorité incompétente et, sans qu'il soit besoin d'examiner l'autre moyen de sa requête sur ce point, à en demander l'annulation ;<br/>
<br/>
              Sur les autres conclusions de la requête :<br/>
<br/>
              4. Considérant que les conclusions tendant à l'annulation de toutes les mutations vers les départements ultramarins à compter du 1er septembre 2014 n'étant pas assorties de précisions suffisantes, elles ne peuvent en tout état de cause qu'être rejetées ; <br/>
              5. Considérant que la présente décision n'implique pas, par elle-même, à ce qu'il soit enjoint au ministre des finances et des comptes publics de prononcer l'affectation de Mme A...dans le département de la Réunion ; que, par suite, les conclusions à fin d'injonction doivent être rejetées ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que Mme A...est seulement fondée à demander l'annulation des dispositions du point 3 du chapitre 5 des instructions du 18 décembre 2013 et du 18 décembre 2014 du ministre des finances et des comptes publics ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les dispositions du point 3 du chapitre 5 de l'instruction n° RH2A/2013/12/7317 du 18 décembre 2013 du ministre des finances et des comptes publics relative aux mutations et premières affectations des cadres B et C de l'année 2014 sont annulées.<br/>
Article 2 : Les dispositions du point 3 du chapitre 5 de l'instruction n° RH2A/2014/12/7331 du 18 décembre 2014 du ministre des finances et des comptes publics relative aux mutations et premières affectations des cadres B et C de l'année 2015 sont annulées.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
