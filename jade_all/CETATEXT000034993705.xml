<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993705</ID>
<ANCIEN_ID>JG_L_2017_06_000000402420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 22/06/2017, 402420, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402420.20170622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 12 août et 14 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la Section française de l'Observatoire international des prisons demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir de l'arrêté du 9 juin 2016 portant création de traitements de données à caractère personnel relatifs à la vidéoprotection de cellules de détention ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ; <br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - la loi n° 2016-987 du 21 juillet 2016 ;<br/>
              - la décision du 8 février 2017 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Section française de l'Observatoire international des prisons ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique : <br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public, <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Section française de l'Observatoire international des prisons ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par un arrêté du 9 juin 2016 pris sur le fondement de l'article 26 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, le garde des sceaux, ministre de la justice, a autorisé la mise en oeuvre par la direction de l'administration pénitentiaire de traitements de données à caractère personnel relatifs aux systèmes de vidéoprotection de cellules de détention au sein des établissements pénitentiaires ayant pour finalité " le contrôle sous vidéoprotection de cellules de détention dans lesquelles sont affectées les personnes placées sous main de justice, faisant l'objet d'une mesure d'isolement, dont l'évasion ou le suicide pourraient avoir un impact important sur l'ordre public eu égard aux circonstances particulières à l'origine de leur incarcération et l'impact de celles-ci sur l'opinion publique " ; que la Section française de l'Observatoire international des prisons demande l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              2.	Considérant que, postérieurement à l'intervention de l'arrêté attaqué, l'article 9 de la loi du 21 juillet 2016 prorogeant l'application de la loi n° 55-385 du 3 avril 1955 relative à l'état d'urgence et portant mesures de renforcement de la lutte antiterroriste, insérant un article 58-1 à la loi du 24 novembre 2009 pénitentiaire et un article 716-1 A au code de procédure pénale, a défini les conditions dans lesquelles la direction de l'administration pénitentiaire peut mettre en oeuvre des traitements de données à caractère personnel relatifs aux systèmes de vidéosurveillance de cellules de détention au sein des établissements pénitentiaires, ayant pour finalité " le contrôle sous vidéosurveillance des cellules de détention dans lesquelles sont affectées les personnes placées sous main de justice, faisant l'objet d'une mesure d'isolement, dont l'évasion ou le suicide pourraient avoir un impact important sur l'ordre public eu égard aux circonstances particulières à l'origine de leur incarcération et à l'impact de celles-ci sur l'opinion publique " ; que si, depuis l'entrée en vigueur de ces dispositions, l'arrêté attaqué du 9 juin 2016 a cessé de produire effet, il ressort des pièces du dossier qu'il a reçu exécution avant cette date ; que le recours pour excès de pouvoir formé par la Section française de l'Observatoire international des prisons contre cet arrêté conserve, dans cette mesure, un objet ; <br/>
<br/>
              3.	Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article L. 212-1 du code des relations entre le public et l'administration : " Toute décision prise par une administration comporte la signature de son auteur ainsi que la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci " ; qu'il ressort des pièces versées au dossier que l'original de l'arrêté attaqué a été signé par le garde des sceaux, ministre de la justice et comporte les mentions prévues par les dispositions précitées de l'article L. 212-1 ; <br/>
<br/>
              4.	Considérant, en deuxième lieu, que, par décision du 8 février 2017, le Conseil d'Etat, statuant au contentieux, a jugé qu'il n'y avait pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Section française de l'Observatoire international des prisons, mettant en cause l'article 58-1 de la loi du 24 novembre 2009 pénitentiaire et de l'article 716-1 A du code de procédure pénale ; que, dès lors, le moyen tiré de ce que l'arrêté attaqué serait illégal parce que ces dispositions législatives méconnaîtraient les droits et libertés garantis par la Constitution ne peut qu'être écarté ;<br/>
<br/>
              5.	Considérant, en troisième lieu, que les dispositions de l'article 58-1 de la loi du 24 novembre 2009 pénitentiaire et de l'article 716-1 A du code de procédure pénale, issues de la loi du 21 juillet 2016, sont dépourvues de caractère rétroactif et n'étaient, en conséquence, pas en vigueur à la date à laquelle l'arrêté attaqué a été pris ; que l'incompatibilité d'une disposition législative avec les stipulations d'un traité ne peut être utilement invoquée à l'appui de conclusions dirigées contre un acte réglementaire que si ce dernier a été pris pour son application ou si elle en constitue la base légale ; qu'il résulte de ce qui vient d'être dit que les dispositions législatives mises en cause, qui n'étaient pas en vigueur à la date de l'arrêté attaqué, ne peuvent en constituer le fondement ; que, par suite, l'association requérante ne peut, à l'appui de ses conclusions tendant à l'annulation de l'arrêté, utilement soutenir, par la voie de l'exception, que ces dispositions législatives méconnaîtraient les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que la Section française de l'Observatoire international des prisons n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la Section française de l'Observatoire international des prisons est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Section française de l'Observatoire international des prisons et au ministre d'Etat, garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
