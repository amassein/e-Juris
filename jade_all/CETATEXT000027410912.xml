<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410912</ID>
<ANCIEN_ID>JG_L_2013_05_000000348818</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410912.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 15/05/2013, 348818, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348818</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; LE PRADO ; SCP ROGER, SEVAUX ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:348818.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 avril et 27 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier universitaire de Nantes dont le siège est 5 allée de l'Ile Gloriette, BP 100 à Nantes (44035) ; le centre hospitalier universitaire demande au Conseil d'Etat d'annuler l'arrêt n° 10NT00704-10NT00853 du 24 février 2011 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre le jugement n° 0702702 du 11 février 2010 du tribunal administratif de Nantes le condamnant à verser 30 000 euros à M. A...B...et 57 688,36 euros à la caisse primaire d'assurance maladie de Nantes en réparation des conséquences dommageables d'une infection nosocomiale ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 avril 2013 présentée pour la caisse primaire d'assurance maladie de Loire-Atlantique ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Le Prado, avocat du centre hospitalier universitaire de Nantes, de la SCP Potier de la Varde, Buk Lament, avocat de M. B..., de Me Foussard, avocat de la caisse primaire d'assurance maladie de Loire-Atlantique et de la SCP Roger, Sevaux, avocat de l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Le Prado, avocat du centre hospitalier universitaire de Nantes, à la SCP Potier de la Varde, Buk Lament, avocat de M. B..., à Me Foussard, avocat de la caisse primaire d'assurance maladie de Loire-Atlantique et à la SCP Roger, Sevaux, avocat de l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a subi le 17 septembre 2002 au centre hospitalier universitaire de Nantes un pontage fémoro-tibial destiné à remédier à une thrombose des artères fémorale et poplitée gauches ; que, le 8 novembre 2002, en raison d'une dégradation de son état de santé liée à une infection par un staphylocoque doré, il a dû être amputé au tiers moyen de la jambe gauche ; que, par un jugement rendu le 11 février 2010, le tribunal administratif de Nantes, estimant que M. B...avait contracté une infection nosocomiale et que la preuve d'une cause étrangère n'était pas apportée, a condamné le centre hospitalier universitaire à lui verser une indemnité de 30 000 euros et à rembourser à la caisse primaire d'assurance maladie de Loire-Atlantique les frais exposés par elle pour un montant de 56 762,36 euros ; que le centre hospitalier universitaire de Nantes se pourvoit en cassation contre l'arrêt du 24 février 2011 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 1142-1 du code de la santé publique : " Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute.  / Les établissements, services et organismes susmentionnés sont responsables des dommages résultant d'infections nosocomiales, sauf s'ils rapportent la preuve d'une cause étrangère " ; que ces dispositions font peser sur l'établissement de santé la responsabilité des infections nosocomiales qu'elles soient exogènes ou endogènes, à moins que la preuve d'une cause étrangère soit rapportée ; <br/>
<br/>
              3. Considérant que la cour administrative d'appel de Nantes a estimé, en se fondant notamment sur le rapport d'expertise, que l'infection, apparue dans un délai inférieur à deux mois sur le matériel prothétique posé lors du pontage effectué le 17 septembre 2002, était la conséquence de cette intervention ; qu'en déduisant de cette appréciation, exempte de dénaturation, que l'infection présentait un caractère nosocomial, la cour, dont l'arrêt est suffisamment motivé sur ce point, n'a pas commis d'erreur de droit ; qu'en jugeant ensuite que le centre hospitalier universitaire de Nantes n'apportait pas la preuve d'une cause étrangère en se bornant à invoquer le caractère endogène du germe responsable de l'infection ainsi que la forte dépendance à l'alcool et au tabac et les problèmes artériels graves de M.B..., elle n'a pas davantage entaché son arrêt d'insuffisance de motivation, de dénaturation ni d'erreur de droit ;<br/>
<br/>
              4. Considérant que, dès lors que les soins à l'origine de l'infection nosocomiale avaient été dispensés avant l'entrée en vigueur, le 1er janvier 2003, des dispositions de l'article L. 1142-1-1 du code de la santé publique issues de la loi du 30 décembre 2002, qui prévoient que la réparation des conséquences les plus graves des infections nosocomiales est assurée au titre de la solidarité nationale par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, la cour administrative d'appel s'est à bon droit abstenue de faire application de ces dispositions ; que, n'ayant pas été saisie d'un moyen tiré de leur méconnaissance, elle n'avait pas à motiver son arrêt sur ce point ;  <br/>
<br/>
              5. Considérant, toutefois, qu'en mettant à la charge du centre hospitalier universitaire de Nantes la réparation intégrale des préjudices résultant de l'amputation subie par M.B..., sans répondre au moyen, invoqué devant elle par le centre hospitalier universitaire de Nantes, selon lequel, compte tenu de la pathologie que l'intervention du 17 septembre 2002 avait pour objet de tenter de juguler, l'infection contractée par l'intéressé à la suite de cette intervention n'avait entraîné pour lui que la perte d'une chance d'échapper à cette amputation, la cour administrative d'appel de Nantes a insuffisamment motivé son arrêt ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le centre hospitalier universitaire de Nantes est fondé à demander l'annulation de l'arrêt du 24 février 2011 en tant qu'il se prononce sur l'étendue des droits à réparation de M. B...et de la caisse primaire d'assurance maladie de Loire-Atlantique ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce que des sommes soient mises à ce titre à la charge du centre hospitalier universitaire de Nantes qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 24 février 2011 est annulé en tant qu'il se prononce sur l'étendue des droits à réparation de M. B...et de la caisse primaire d'assurance maladie de Loire-Atlantique.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes dans la limite de la cassation prononcée.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi du centre hospitalier universitaire de Nantes est rejeté. <br/>
<br/>
Article 4 : Les conclusions présentées par M.B..., par l'ONIAM et par la caisse primaire d'assurance maladie de Loire-Atlantique au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au centre hospitalier universitaire de Nantes, à M. A... B..., à la caisse primaire d'assurance maladie de Loire-Atlantique et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
