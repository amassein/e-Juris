<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043908919</ID>
<ANCIEN_ID>JG_L_2021_07_000000452878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/90/89/CETATEXT000043908919.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/07/2021, 452878</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:452878.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2101950 du 21 mai 2021, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Rennes avant de statuer sur les conclusions de la demande présentée par M. B... A... a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Le délai de quinze jours prévu au I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu, depuis le 1er mai 2021, l'article L. 614-5 du même code, est-il un délai franc ou un délai non franc ' <br/>
<br/>
              2°) Si ce délai ne constitue pas un délai franc, quelle conséquence doit-on tirer sur la recevabilité de la requête, et alors que la mention des délais et voies de recours figurant dans la notification au requérant lui-même de l'arrêté attaqué se borne à mentionner sans autre précision, l'existence d'un délai de quinze jours, de l'information figurant sur le site de l'administration française " service public.fr " et selon laquelle ce délai est un délai franc '<br/>
<br/>
              Des observations, enregistrées le 27 juin 2021, ont été présentées pour M. A....<br/>
<br/>
              Des observations, enregistrées le 7 juillet 2021, ont été présentées par le ministre de l'intérieur.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - Les conclusions de M. Philippe Ranquet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes du I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile en vigueur à la date de la décision contestée dans le cadre du litige ayant donné lieu à la demande d'avis, et dont les dispositions sont reprises, depuis le 1er mai 2021, à l'article L. 614-5 du même code : " L'étranger qui fait l'objet d'une obligation de quitter le territoire français sur le fondement des 1°, 2°, 4° ou 6° du I de l'article L. 511-1 et qui dispose du délai de départ volontaire mentionné au premier alinéa du II du même article L. 511-1 peut, dans un délai de quinze jours à compter de sa notification, demander au président du tribunal administratif l'annulation de cette décision, ainsi que l'annulation de la décision mentionnant le pays de destination et de la décision d'interdiction de retour sur le territoire français qui l'accompagnent le cas échéant (...) ". <br/>
<br/>
              2. L'article L. 511-1 du même code dispose que : " L'autorité administrative peut obliger à quitter le territoire français un étranger non ressortissant d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse et qui n'est pas membre de la famille d'un tel ressortissant au sens des 4° et 5° de l'article L. 121-1, lorsqu'il se trouve dans l'un des cas suivants : / 1° Si l'étranger ne peut justifier être entré régulièrement sur le territoire français, à moins qu'il ne soit titulaire d'un titre de séjour en cours de validité ; / 2° Si l'étranger s'est maintenu sur le territoire français au-delà de la durée de validité de son visa ou, s'il n'est pas soumis à l'obligation du visa, à l'expiration d'un délai de trois mois à compter de son entrée sur le territoire sans être titulaire d'un premier titre de séjour régulièrement délivré ; (...) / 4° Si l'étranger n'a pas demandé le renouvellement de son titre de séjour temporaire ou pluriannuel et s'est maintenu sur le territoire français à l'expiration de ce titre ; / (...) 6° Si la reconnaissance de la qualité de réfugié ou le bénéfice de la protection subsidiaire a été définitivement refusé à l'étranger ou si l'étranger ne bénéficie plus du droit de se maintenir sur le territoire français en application des articles L. 743-1 et L. 743-2, à moins qu'il ne soit titulaire d'un titre de séjour en cours de validité. Lorsque, dans l'hypothèse mentionnée à l'article L. 311-6, un refus de séjour a été opposé à l'étranger, la mesure peut être prise sur le seul fondement du présent 6° ". Ces dispositions sont reprises, depuis le 1er mai 2021, aux 1°, 2° et 4° de l'article L. 611-1 de ce code. <br/>
<br/>
              3. Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai. <br/>
<br/>
              4. Par suite, alors que les dispositions citées précédemment ne s'y opposent pas, le délai de recours de quinze jours prévu au I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu, depuis le 1er mai 2021, l'article L. 614-5 du même code, présente le caractère d'un délai franc.<br/>
<br/>
              5. Lorsque le délai expire un samedi, un dimanche ou un jour férié ou chômé, il y a lieu, par application des règles définies à l'article 642 du code de procédure civile, d'admettre la recevabilité d'une demande présentée le premier jour ouvrable suivant.<br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Rennes, à M. B... A... et au ministre de l'intérieur. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. - OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - DÉLAI DE QUINZE JOURS POUR CONTESTER UNE OQTF PRISE EN APPLICATION DES 1°, 2° OU 4° DE L'ARTICLE L. 611-1 DU CESEDA ET ASSORTIE D'UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 614-5 DU CESEDA) - CARACTÈRE FRANC - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - DÉLAI DE QUINZE JOURS POUR CONTESTER UNE OQTF PRISE EN APPLICATION DES 1°, 2° OU 4° DE L'ARTICLE L. 611-1 DU CESEDA ET ASSORTIE D'UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 614-5 DU CESEDA) - CARACTÈRE FRANC - EXISTENCE.
</SCT>
<ANA ID="9A"> 335-03-03 Le I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), repris, depuis le 1er mai 2021, à l'article L. 614-5 du même code, prévoit que l'étranger qui fait l'objet d'une obligation de quitter le territoire français (OQTF) sur le fondement des 1°, 2°, 4° ou 6° du I de l'article L. 511-1, repris aux 1°, 2° et 4° de l'article L. 611-1, et qui dispose d'un délai de départ volontaire, peut, dans un délai de quinze jours à compter de sa notification, demander l'annulation notamment de cette décision au président du tribunal administratif.......Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai. ......Par suite, et alors que les dispositions mentionnées ci-dessus ne s'y opposent pas, ce délai de quinze jours doit être regardé comme un délai franc.</ANA>
<ANA ID="9B"> 54-01-07 Le I bis de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), repris, depuis le 1er mai 2021, à l'article L. 614-5 du même code, prévoit que l'étranger qui fait l'objet d'une obligation de quitter le territoire français (OQTF) sur le fondement des 1°, 2°, 4° ou 6° du I de l'article L. 511-1, repris aux 1°, 2° et 4° de l'article L. 611-1, et qui dispose d'un délai de départ volontaire, peut, dans un délai de quinze jours à compter de sa notification, demander l'annulation notamment de cette décision au président du tribunal administratif.......Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai. ......Par suite, et alors que les dispositions mentionnées ci-dessus ne s'y opposent pas, ce délai de quinze jours doit être regardé comme un délai franc.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
