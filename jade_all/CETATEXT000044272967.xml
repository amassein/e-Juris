<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044272967</ID>
<ANCIEN_ID>JG_L_2021_10_000000450886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/27/29/CETATEXT000044272967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 29/10/2021, 450886, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450886.20211029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F... B... a demandé au tribunal administratif de Paris, d'une part, d'annuler les opérations électorales qui se sont déroulées dans le 5ème arrondissement de Paris les 15 mars et 28 juin 2020 pour l'élection des conseillers de Paris et des conseillers d'arrondissement et, d'autre part, de prononcer l'inéligibilité de Mme D... C....<br/>
<br/>
              Par un jugement n° 2009458 du 18 février 2021, le tribunal administratif de Paris a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 19 mars et 19 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) de prononcer l'inéligibilité de Mme C... ;<br/>
<br/>
              4°) de mettre à la charge de Mme C... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. A l'issue du second tour des opérations électorales qui se s'est déroulé le 28 juin 2020 dans le 5ème arrondissement de la Ville de Paris en vue de l'élection des conseillers de Paris et des conseillers de cet arrondissement, la liste conduite par Mme C..., maire sortante, a obtenu 8 457 voix, soit 51,91% des suffrages exprimés et la liste conduite par Mme G... a obtenu 7 832 voix soit 48,08% des suffrages exprimés. M. B... relève appel du jugement du 18 février 2021 par lequel le tribunal administratif de Paris a rejeté sa protestation dirigée contre ces opérations électorales.<br/>
<br/>
              2. Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) ".<br/>
<br/>
              3. En premier lieu, il résulte de l'instruction que ni le contenu, ni la périodicité ou les modalités de diffusion des deux numéros de septembre et décembre 2019 du " Journal du 5ème arrondissement ", qui étaient disponibles dans le hall de la mairie d'arrondissement et distribués dans certains commerces par des agents municipaux, ne sauraient les faire regarder comme des documents de propagande électorale. Par suite, contrairement à ce que soutient M. B..., ils n'ont pas revêtu le caractère d'un avantage consenti à Mme C... par la mairie du 5ème arrondissement en violation des dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              4. En deuxième lieu, il ne résulte pas de l'instruction que les masques de protection mis à la disposition de la mairie du 5ème arrondissement par la Ville de Paris et le conseil régional d'Ile-de-France dans le cadre de la lutte contre l'épidémie de covid-19 auraient été distribués dans des conditions de nature à faire regarder cette distribution comme une opération de propagande électorale. En particulier, si M. B... produit en appel deux " tweets " envoyés par deux colistiers de Mme C... mentionnant une opération de distribution de masques de protection aux usagers des transports publics, ces messages n'établissent pas l'utilisation de cette distribution de masques à des fins de propagande électorale.<br/>
<br/>
              5. En troisième lieu, il résulte de l'instruction que le message adressé par courriel aux parents d'élèves de l'arrondissement le 5 mai 2020 avait pour objet de les informer des mesures sanitaires mises en place pour garantir la protection des élèves à quelques jours de la réouverture des écoles. Si ce courriel rappelle également l'existence de dispositifs sanitaires et sociaux de nature à répondre aux besoins des habitants de l'arrondissement dans le cadre de la crise sanitaire, il ne saurait être regardé, alors d'ailleurs qu'il ne comporte aucune référence aux échéances électorales, comme un document de propagande électorale. Par suite, M. B... n'est pas fondé à soutenir que l'utilisation du fichier des parents d'élèves pour adresser ce courriel doit être regardée comme un avantage en nature consenti par la mairie du 5ème arrondissement en violation des dispositions de l'article 52-8 du code électoral. <br/>
<br/>
              6. Enfin, la circonstance que la brochure de la caisse des écoles du 5ème arrondissement, diffusée en octobre 2019, contient dans son éditorial une phrase laissant entendre que Mme C... serait, en qualité de présidente de la caisse des écoles, à l'origine du " choix de l'excellence " fait pour la gestion des cantines situées dans l'arrondissement, ne confère pas à ce document, dont la diffusion ne présente aucun caractère exceptionnel et dont le contenu n'évoque ni le scrutin à venir ni les actions conduites par la mairie du 5ème arrondissement, le caractère d'un document de propagande électorale. M. B... n'est dès lors pas fondé à soutenir que sa diffusion constituerait un avantage en nature prohibé par les dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa protestation.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme C..., qui n'est pas la partie perdante dans la présente instance, la somme demandée par M. B.... Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par Mme C....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par Mme C... au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. F... B..., à Mme D... C... et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : M. Denis Piveteau, président de chambre, présidant ; M. Olivier Yeznikian, conseiller d'Etat et Mme Flavie Le Tallec, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 29 octobre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Denis Piveteau<br/>
 		La rapporteure : <br/>
      Signé : Mme Flavie Le Tallec<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
