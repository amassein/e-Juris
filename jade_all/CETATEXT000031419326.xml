<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031419326</ID>
<ANCIEN_ID>JG_L_2015_11_000000373677</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/41/93/CETATEXT000031419326.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 02/11/2015, 373677, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373677</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373677.20151102</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...et M. D...B...ont demandé au tribunal administratif de  Strasbourg de condamner le syndicat intercommunal d'assainissement de l'Altenbach à les indemniser des préjudices qu'ils ont subis à la suite des travaux de pose d'une canalisation d'assainissement. Par un jugement n° 1101987 du 3 octobre 2013, le tribunal administratif n'a fait que partiellement droit à leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 décembre 2013 et 3 mars 2014 au secrétariat du contentieux du Conseil d'Etat, MM. A...et B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 9 de ce jugement rejetant le surplus de leur demande d'indemnisation ;<br/>
<br/>
              2°) d'annuler l'article 3 de ce jugement en tant qu'il fixe le versement des intérêts à taux légal à compter du 28 décembre 2010 ;<br/>
<br/>
              3°) de mettre à la charge du syndicat intercommunal d'assainissement de l'Altenbach le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de MM. A...et B...et à la SCP Lévis, avocat du syndicat intercommunal d'assainissement de l'Altenbach ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 9 août 2006, le préfet du Haut-Rhin a institué sur le territoire de la commune de Buschwiller une servitude de passage sur fonds privés pour la pose d'une canalisation au profit du syndicat intercommunal d'assainissement de l'Altenbach ; qu'à la fin de 2006, ce syndicat a fait procéder à des travaux de pose de cette canalisation sur des parcelles appartenant respectivement à M. A...et M.B..., qui ont demandé en vain à ce syndicat d'être indemnisés à raison des désordres provoqués par leur mauvaise finition et se pourvoient en cassation contre le jugement du 3 octobre 2013 du tribunal administratif de Strasbourg, en tant qu'il n'a fait que partiellement droit à leur demande d'indemnisation en limitant celle-ci à la remise en état du sol autour d'un regard situé sur la parcelle de M.A... et qu'il a fixé le point de départ du calcul des intérêts des sommes devant être versées à ce dernier à raison des frais d'expertise à compter du 28 décembre 2010 ;<br/>
<br/>
              2. Considérant, d'une part, qu'alors que les requérants se bornaient à faire valoir que les travaux avaient entraîné une érosion de la berge qui avait fait apparaître un bloc de béton dans le ruisseau de l'Altenbach sur le côté droit de la parcelle de M.B...,  le tribunal a porté sur les éléments du dossier qui lui était soumis une appréciation souveraine exempte de dénaturation en relevant que la présence de ce bloc ne présentait pas de lien avec les travaux au motif que l'expertise mentionnait qu'il était le vestige d'une ancienne écluse ;  que s'il n'a pas suivi les conclusions de l'expertise en estimant que l'affaissement des parcelles au droit de la berge, qu'il a qualifié de léger, n'était pas imputable aux travaux, il n'a pas dénaturé les pièces du dossier dès lors que l'expertise ne justifiait pas du lien de causalité entre les travaux réalisés et l'affaissement des berges et que l'existence de crues fréquentes, également relevées par le rapport, pouvait en être à l'origine ; qu'enfin, il a estimé par une appréciation souveraine des faits qu'en l'absence d'état  des lieux préalable aux travaux, aucun élément ne permettait d'imputer l'apparition d'ornières sur le chemin d'exploitation de M. B...aux travaux d'assainissement, en relevant notamment que ce chemin était emprunté par des tracteurs ; qu'il a suffisamment motivé son jugement sur l'ensemble des dommages restant en litige ; <br/>
<br/>
              3. Considérant, d'autre part, que les requérants ont demandé au tribunal de leur accorder des intérêts au taux légal à compter du 28 décembre 2010 à raison des frais d'expertise avancés par M.A... ; que le tribunal était ainsi tenu par l'étendue des conclusions dont il était saisi ; que, dès lors, il n'a pas commis d'erreur de droit en fixant à la date de leur réclamation préalable le point de départ des intérêts dus à M. A...à raison des frais d'expertise qu'il avait avancés et non à la date de versement effectif de ces sommes ;<br/>
<br/>
              4. Considérant, en revanche, que le tribunal n'a pas statué sur les conclusions tendant à  l'indemnisation portant sur les désordres résultant du défaut de scellement du tampon du regard situé sur la parcelle de M. A... ; que, dès lors, il y a lieu d'annuler dans cette mesure le jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, et notamment des conclusions de l'expertise en date du 12 mars 2010, que la parcelle de M. A...comporte un regard dont le tampon n'est pas scellé ; qu'un tel désordre a pour cause les travaux de pose de la canalisation d'assainissement ; que cette expertise a évalué le coût de scellement de ce tampon à la somme de 240 euros hors taxes ; qu'il suit de là que M. A...est fondé à demander le versement par le syndicat de cette somme avec application de la taxe sur la valeur ajoutée correspondante ; que cette somme portera intérêts au taux légal à compter du 28 décembre 2010, date de la réclamation préalable ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat intercommunal d'assainissement de l'Altenbach la somme globale de 500 euros à verser à MM. A...etB..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                               D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : L'article 9 du jugement du 3 octobre 2013 est annulé en tant qu'il statue sur les conclusions de M. A...tendant à son indemnisation à raison du défaut de scellement du tampon du regard sur la parcelle lui appartenant.<br/>
<br/>
Article 2 : Le syndicat intercommunal de l'Altenbach est condamné à verser à M. A...la somme de 240 euros avec application de la taxe sur la valeur ajoutée. La somme portera portant intérêts au taux légal à compter du 28 décembre 2010. <br/>
<br/>
Article 3 : Le syndicat intercommunal de l'Altenbach versera conjointement à MM. A...et B...une somme globale de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. C...A..., à M. D...B...et au syndicat intercommunal d'assainissement de l'Altenbach.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
