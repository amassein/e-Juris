<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039017831</ID>
<ANCIEN_ID>JG_L_2019_08_000000433451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/01/78/CETATEXT000039017831.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/08/2019, 433451, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-08-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:433451.20190822</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'arrêté du 25 juillet 2019 par lequel le ministre de l'intérieur a renouvelé pour une durée de trois mois à compter du 4 août 2019 la mesure individuelle de contrôle administratif et de surveillance prise à son encontre le 3 mai 2019. Par une ordonnance n° 1908203 du 31 juillet 2019, le tribunal administratif de Montreuil a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 août et 17 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit à ses conclusions de première instance ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative à verser à son conseil en vertu de l'article 34 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que sa situation économique, sociale, administrative et familiale est rendue précaire par la mesure contestée qui l'empêche d'exercer l'emploi de livreur auprès d'une société qui est prête à l'embaucher ;<br/>
              - le ministre ne fait valoir aucune circonstance de nature à remettre en cause la présomption d'urgence posée par la jurisprudence ;<br/>
              - la circonstance qu'il est actuellement incarcéré pour avoir enfreint les obligations de l'arrêté en litige ne permet pas d'écarter la condition d'urgence mais justifie au contraire qu'il soit statué en urgence sur la demande de suspension ;<br/>
              - les faits allégués par le ministre ne peuvent être rapportés par la seule production de notes blanches qui, en l'espèce, comportent des informations imprécises, subjectives, erronées ou fondées sur des extrapolations ; <br/>
              - l'arrêté attaqué porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir ainsi qu'à son droit au respect de sa vie privée et familiale ;<br/>
              - le ministre ne démontre pas que la mesure est justifiée, nécessaire, et proportionnée et n'établit pas la réalité et l'actualité de la menace terroriste qu'il représenterait au regard des exigences posées par les dispositions de l'article L. 228-1 et suivants du code de la sécurité intérieure pour l'édiction de mesures individuelles de contrôle administratif et de surveillance ;<br/>
              - il n'est pas établi par les pièces fournies par le ministre qu'il a effectivement appartenu à l'organisation Jundallah comme cela est seulement allégué par la note blanche ;<br/>
              - il n'appartient à aucune organisation islamiste et ne représente aucun danger en France ainsi que le démontrent les procédures judiciaires et les perquisitions déjà effectuées notamment à son domicile ; <br/>
              - il n'a jamais adopté un comportement fuyant à l'égard du personnel féminin en prison mais a au contraire participé à des activités volontaires organisées par ces personnels ;<br/>
              - sa pratique de la boxe ou d'autres sports en prison ne s'apparente pas à un entraînement de combat ;<br/>
              - ses lectures de prison n'ont qu'une portée religieuse et ne permettent pas de le regarder comme soutenant ou adhérant à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes ;<br/>
              - les personnes rencontrées en prison dont les noms ont été repris dans l'interdiction formulée sur le fondement de l'article L. 228-5 du code de la sécurité intérieure, l'ont été dans le cadre contraint de la détention en cellule, en cours ou en promenade ;<br/>
              - les autres personnes rencontrées en dehors de la prison, l'ont été sur les marchés ou dans le cadre de voisinage sans qu'il soit établi qu'il aurait entretenu des liens suivis avec celles-ci ou que ces liens seraient susceptibles de révéler un danger ; <br/>
              - trois personnes dont les noms figurent dans la liste de l'arrêté sont inconnues de lui ;<br/>
              - les mesures contenues dans l'arrêté sont, au regard de son droit au respect de sa vie privée et familiale et de sa vie professionnelle, disproportionnées dès lors que les interdictions de déplacement ne lui permettent pas retrouver du travail et font obstacle à ce qu'il subvienne aux besoins de sa famille, notamment de ses deux jeunes enfants. <br/>
<br/>
              Par un mémoire en défense, enregistré le 16 août 2019, le ministre de l'intérieur conclut au rejet de la requête. <br/>
<br/>
              Il soutient que ;<br/>
              - la requête est irrecevable dès lors, d'une part, qu'elle ne tend pas à l'annulation de l'ordonnance et, d'autre part, qu'il n'appartient pas au juge du référé liberté de prononcer l'annulation de l'arrêté ;<br/>
              - la condition d'urgence n'est plus remplie compte tenu du placement en détention provisoire de l'intéressé jusqu'au 12 septembre ;<br/>
              - la note blanche produite en cours d'instance, reposant sur des faits précis et circonstanciés qui ne sont pas sérieusement remis en cause, a une valeur probante suffisante ;<br/>
              - il existe des raisons sérieuses de penser que le comportement de l'intéressé constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics compte tenu de son appartenance initiale au groupe jihadiste Jundullah, de son parcours de délinquant multirécidiviste, du comportement agressif adopté pendant son incarcération, de ses relations habituelles avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, de son soutien et de son adhésion à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes au regard du contenu de sa bibliothèque personnelle en prison ;<br/>
              - il n'y a pas d'atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code pénal ;<br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 2019-222 du 23 mars 2019, et notamment son article 65 ;<br/>
              - la loi n° 2017-1510 du 30 octobre 2017 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et, d'autre part, le ministre de l'intérieur ;<br/>
              Vu le procès-verbal de l'audience publique du mardi 20 août 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Texier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ; <br/>
              - le représentant du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale ". <br/>
<br/>
              2. Aux termes, en premier lieu, de l'article L. 228-1 du code de sécurité intérieure : "Aux seules fins de prévenir la commission d'actes de terrorisme, toute personne à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace d'une particulière gravité pour la sécurité et l'ordre publics et qui soit entre en relation de manière habituelle avec des personnes ou des organisations incitant, facilitant ou participant à des actes de terrorisme, soit soutient, diffuse, lorsque cette diffusion s'accompagne d'une manifestation d'adhésion à l'idéologie exprimée, ou adhère à des thèses incitant à la commission d'actes de terrorisme ou faisant l'apologie de tels actes peut se voir prescrire par le ministre de l'intérieur les obligations prévues au présent chapitre ". <br/>
<br/>
              3. Aux termes, en deuxième lieu, de l'article L. 228-2 du code de sécurité intérieure, dans sa rédaction résultant de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme de la justice, entrée en vigueur au 1er juillet 2019 : " Le ministre de l'intérieur peut, après en avoir informé le procureur de la République antiterroriste et le procureur de la République territorialement compétent, faire obligation à la personne mentionnée à l'article L. 228-1 de : / 1° Ne pas se déplacer à l'extérieur d'un périmètre géographique déterminé, qui ne peut être inférieur au territoire de la commune. La délimitation de ce périmètre permet à l'intéressé de poursuivre une vie familiale et professionnelle et s'étend, le cas échéant, aux territoires d'autres communes ou d'autres départements que ceux de son lieu habituel de résidence ; / 2° Se présenter périodiquement aux services de police ou aux unités de gendarmerie, dans la limite d'une fois par jour, en précisant si cette obligation s'applique les dimanches et jours fériés ou chômés ; / 3° Déclarer son lieu d'habitation et tout changement de lieu d'habitation. / Les obligations prévues aux 1° à 3° du présent article sont prononcées pour une durée maximale de trois mois à compter de la notification de la décision du ministre. Elles peuvent être renouvelées par décision motivée, pour une durée maximale de trois mois, lorsque les conditions prévues à l'article L. 228-1 continuent d'être réunies. Au-delà d'une durée cumulée de six mois, chaque renouvellement est subordonné à l'existence d'éléments nouveaux ou complémentaires. La durée totale cumulée des obligations prévues aux 1° à 3° du présent article ne peut excéder douze mois. Les mesures sont levées dès que les conditions prévues à l'article L. 228-1 ne sont plus satisfaites. / Toute décision de renouvellement des obligations prévues aux 1° à 3° du présent article est notifiée à la personne concernée au plus tard cinq jours avant son entrée en vigueur. La personne concernée peut demander au président du tribunal administratif ou au magistrat qu'il délègue l'annulation de la décision dans un délai de quarante-huit heures à compter de sa notification. Il est statué sur la légalité de la décision au plus tard dans un délai de soixante-douze heures à compter de la saisine du tribunal. Dans ce cas, la mesure ne peut entrer en vigueur avant que le juge ait statué sur la demande. / L'audience est publique. Elle se déroule sans conclusions du rapporteur public. Lorsque la présence du requérant à l'audience est susceptible de méconnaître les obligations résultant de la mesure de surveillance, le requérant peut solliciter un sauf-conduit pour s'y rendre. Le sauf-conduit n'est pas délivré si le déplacement du requérant constitue une menace pour la sécurité et l'ordre publics. / La personne soumise aux obligations prévues aux 1° à 3° du présent article peut, dans un délai de deux mois à compter de la notification de la décision, ou à compter de la notification de chaque renouvellement lorsqu'il n'a pas été fait préalablement usage de la faculté prévue au sixième alinéa, demander au tribunal administratif l'annulation de cette décision. Le tribunal administratif statue dans un délai de quinze jours à compter de sa saisine. Ces recours, dont les modalités sont fixées au chapitre III ter du titre VII du livre VII du code de justice administrative, s'exercent sans préjudice des procédures prévues au sixième alinéa du présent article ainsi qu'aux articles L. 521-1 et L. 521-2 du même code ". <br/>
<br/>
              4. Aux termes, en troisième lieu, de l'article L. 228-5 du code de sécurité intérieure, dans sa rédaction résultant de la loi du 23 mars 2019 de programmation 2018-2022 et de réforme de la justice, entrée en vigueur au 1er juillet 2019 : " Le ministre de l'intérieur peut, après en avoir informé le procureur de la République antiterroriste et le procureur de la République territorialement compétent, faire obligation à toute personne mentionnée à l'article L. 228-1, y compris lorsqu'il est fait application des articles L. 228-2 à L. 228-4, de ne pas se trouver en relation directe ou indirecte avec certaines personnes, nommément désignées, dont il existe des raisons sérieuses de penser que leur comportement constitue une menace pour la sécurité publique. / L'obligation mentionnée au premier alinéa du présent article est prononcée pour une durée maximale de six mois à compter de la notification de la décision du ministre. Au-delà d'une durée cumulée de six mois, le renouvellement est subordonné à l'existence d'éléments nouveaux ou complémentaires. La durée totale cumulée de l'obligation prévue au premier alinéa du présent article ne peut excéder douze mois. L'obligation est levée dès que les conditions prévues à l'article L. 228-1 ne sont plus satisfaites. / Toute décision de renouvellement est notifiée à la personne concernée au plus tard cinq jours avant son entrée en vigueur. La personne concernée peut demander au président du tribunal administratif ou au magistrat qu'il délègue l'annulation de la décision dans un délai de quarante-huit heures à compter de sa notification. Il est statué sur la légalité de la décision au plus tard dans un délai de soixante-douze heures à compter de la saisine du tribunal. Dans ce cas, la mesure ne peut entrer en vigueur avant que le juge ait statué sur la demande. / L'audience est publique. Elle se déroule sans conclusions du rapporteur public. Lorsque la présence du requérant à l'audience est susceptible de méconnaître les obligations résultant de la mesure de surveillance, le requérant peut solliciter un sauf-conduit pour s'y rendre. Le sauf-conduit n'est pas délivré si le déplacement du requérant constitue une menace pour la sécurité et l'ordre publics. / La personne soumise à l'obligation mentionnée au premier alinéa du présent article peut, dans un délai de deux mois à compter de la notification de la décision, ou à compter de la notification de chaque renouvellement lorsqu'il n'a pas été fait préalablement usage de la faculté prévue au troisième alinéa, demander au tribunal administratif l'annulation de cette décision. Le tribunal administratif statue dans un délai d'un mois à compter de sa saisine. Ces recours, dont les modalités sont fixées au chapitre III ter du titre VII du livre VII du code de justice administrative, s'exercent sans préjudice des procédures prévues au troisième alinéa du présent article ainsi qu'aux articles L. 521-1 et L. 521-2 du même code ".<br/>
<br/>
              5. M. B..., né le 8 juillet 1991, de nationalité algérienne, a fait l'objet d'un arrêté du 3 mai 2019, pris sur le fondement des dispositions des articles L. 228-1 à L. 228-7 du code de la sécurité intérieure, par lequel le ministre de l'intérieur a prononcé à son encontre une mesure individuelle de contrôle administratif et de surveillance. Cet arrêté comportait, en premier lieu, pour une durée de trois mois, une mesure d'assignation à résidence qui, prise en application des dispositions du 1° au 3° de l'article L. 228-2 du code de sécurité intérieure, d'une part, lui interdisait de se déplacer à l'extérieur du territoire de la commune de Drancy, d'autre part, lui imposait de se présenter une fois par jour tous les jours de la semaine, weekend et jours fériés compris, au commissariat de police de Drancy, et, enfin, lui faisait obligation de déclarer son lieu d'habitation et tout changement de lieu d'habitation. Cet arrêté comportait, en second lieu, à son article 6, pour une durée de six mois, en application des dispositions de l'article L. 228-5 du même code, l'interdiction de se trouver en relation directe ou indirecte avec treize personnes nommément désignées par l'arrêté. Par une ordonnance n° 1905702 du 28 mai 2019 devenue définitive, le juge des référés du tribunal administratif de Montreuil, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de suspension formulée par l'intéressé. Par un nouvel arrêté du 25 juillet 2019 dont l'entrée en vigueur a été fixée au 4 août 2019, le ministre de l'intérieur a, en premier lieu, renouvelé, pour une durée de trois mois, les obligations fondées sur les dispositions de l'article L. 228-2 et, en second lieu, maintenu l'interdiction fondée sur les dispositions de l'article L. 228-5 du code de la sécurité intérieure, de se trouver en relation directe ou indirecte avec treize personnes nommément désignées, en confirmant les dispositions l'article 6 de l'arrêté du 3 mai 2019 pour la période de trois mois restant à courir. Par une ordonnance n° 1908203 du 31 juillet 2019, le juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté la demande de suspension formulée par M. B... à l'encontre de l'arrêté du 25 juillet 2019. Ce dernier relève appel de cette ordonnance.<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              6. Eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prise par l'autorité administrative en application des articles L. 228-1 et L. 228-2 du code de la sécurité intérieure, porte, en principe et par<br/>
elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde. <br/>
<br/>
              7. Aux termes de l'article L. 228-7 du code de la sécurité intérieure : " Le fait de se soustraire aux obligations fixées en application des articles L. 228-2 à L. 228-5 est puni de trois ans d'emprisonnement et de 45 000 euros d'amende ".<br/>
<br/>
              8. Aux termes de l'article 111-5 du code pénal : " Les juridictions pénales sont compétentes pour interpréter les actes administratifs, réglementaires ou individuels et pour en apprécier la légalité lorsque, de cet examen, dépend la solution du procès pénal qui leur est soumis ".<br/>
<br/>
              9. Il résulte de l'instruction que M. B... se trouve, à la date de la présente ordonnance, incarcéré depuis le 9 août 2019 au centre pénitentiaire de Meaux en détention provisoire dans l'attente de son jugement en comparution immédiate qui, initialement prévu le 13 août, a été reporté au 12 septembre 2019. Il lui est reproché de s'être trouvé le 7 août 2019 à La Courneuve, où il a été interpellé, en violation de l'interdiction qui lui avait été faite de se déplacer en dehors du territoire de Drancy en application de l'article L. 228-2 du code de la sécurité intérieure, par l'arrêté du ministre de l'intérieur du 25 juillet 2019 entré en vigueur le 4 août 2019 et qui fait objet de la présente demande de suspension. <br/>
<br/>
              10. Eu égard à ce qui précède, M. B... ne fait plus actuellement l'objet de restrictions de liberté résultant directement de l'arrêté du 25 juillet 2019 du ministre de l'intérieur mais d'une mesure privative de liberté qui procède d'une décision de l'autorité judiciaire. Si, selon les informations communiquées au juge des référés du Conseil d'Etat, les poursuites pénales sont exercées à l'encontre de M. B..., en application de l'article L. 228-7 du code de la sécurité intérieure, et trouvent leur fondement dans la violation présumée par ce dernier de certaines obligations prévues par l'arrêté ministériel du 25 juillet 2019, il appartient, le cas échéant, à la juridiction pénale de se prononcer sur la légalité de l'arrêté en mettant en oeuvre les dispositions de l'article 111-5 du code pénal, et ce, sans préjudice de l'exercice des recours en annulation spécialement aménagés dont l'intéressé dispose auprès du juge administratif sur le fondement des dispositions des articles L. 228-2 et L. 228-5 du code de la sécurité intérieure, rappelées aux points 3 et 4 ci-dessus - voies de recours que l'intéressé n'a, au demeurant, pas jusqu'à présent exercées.<br/>
<br/>
              11. Dès lors, cette situation d'incarcération, aussi longtemps qu'elle demeure, ne justifie pas, dans les circonstances de l'espèce, que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, prononce dans de très brefs délais, une mesure provisoire et conservatoire de sauvegarde de la liberté fondamentale d'aller et venir ou celle tenant au droit au respect de la vie privée et familiale protégé par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Il s'en suit, et sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par le ministre en défense, que M. B... n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Montreuil a rejeté sa demande tendant à la suspension des effets de l'arrêté du 25 juillet 2019. Par suite, sa requête d'appel doit être rejetée y compris les conclusions présentées sur le fondement des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, la demande d'admission au bénéfice de l'aide juridictionnelle provisoire étant également rejetée.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
