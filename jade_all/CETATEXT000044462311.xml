<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044462311</ID>
<ANCIEN_ID>JG_L_2021_12_000000433754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/23/CETATEXT000044462311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 08/12/2021, 433754, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433754.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              La société Café de Flore a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir la décision du 19 avril 2017 par laquelle l'inspecteur du travail de l'unité territoriale de Paris a refusé de l'autoriser à licencier M. H... I..., d'autre part, de l'autoriser à licencier M. I.... Par un jugement n° 1717385 du 12 juin 2018, le tribunal administratif a annulé cette décision et rejeté le surplus des conclusions de la société Café de Flore.<br/>
<br/>
              Par un arrêt n° 18PA02660 du 20 juin 2019, la cour administrative d'appel de Paris a, sur appel de M. I..., annulé ce jugement et rejeté les conclusions présentées par la société Café de Flore tant en première instance qu'en appel. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 août et 31 octobre 2019 et le 9 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société Café de Flore demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de M. I... la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la société Café de Flore ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la recevabilité du mémoire de la ministre du travail, de l'emploi et de l'insertion :<br/>
<br/>
              1. Si la ministre du travail, de l'emploi et de l'insertion a produit un mémoire en observations après la clôture de l'instruction, l'instruction a été rouverte et ce mémoire a été communiqué aux parties afin de leur permettre d'y répliquer, ainsi que l'a d'ailleurs fait la société Café de Flore. Dès lors, il n'y a pas lieu d'écarter ce mémoire des débats. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la société Café de Flore a demandé l'autorisation de licencier pour motif disciplinaire M. I..., salarié protégé, lui reprochant divers manquements dans l'accomplissement de ses fonctions. Cette demande a été rejetée le 21 décembre 2016 par l'inspecteur du travail de l'unité territoriale de Paris en raison de l'irrégularité entachant la convocation du salarié à l'entretien préalable prévu par l'article L. 1232-2 du code du travail. La société Café de Flore a engagé une nouvelle procédure et saisi l'administration d'une nouvelle demande d'autorisation de licenciement, fondée sur les mêmes griefs. Par une décision du 15 février 2017, l'inspecteur du travail, s'il a estimé que deux des trois premiers griefs reprochés à M. I... étaient établis et fautifs, a néanmoins refusé d'autoriser le licenciement du salarié au motif que le quatrième grief invoqué par la société Café de Flore devait être regardé comme la dénonciation par M. I... de faits de harcèlement moral, laquelle ne peut légalement, en application des articles L. 1152-2 et L. 1152-3 du code du travail, fonder une mesure de licenciement. La société a alors saisi l'administration d'une troisième demande d'autorisation de licenciement, fondée sur les deux seuls griefs que l'inspecteur du travail avait estimé établis et fautifs dans sa décision du 15 février 2017. Par une décision du 19 avril 2017, l'inspecteur du travail a toutefois rejeté cette nouvelle demande, au motif que les faits reprochés au salarié, survenus le 11 novembre 2016, devaient être regardés comme prescrits. Par un jugement du 12 juin 2018, le tribunal administratif de Paris a annulé la décision du 19 avril 2017. Par un arrêt du 20 juin 2019, la cour administrative d'appel de Paris a annulé ce jugement et rejeté la demande de la société Café de Flore tendant à l'annulation de la décision du 19 avril 2017. La société Café de Flore se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              3. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle. Lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail, et le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi. <br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Paris a jugé que la décision du 19 avril 2017 était, en l'absence de circonstance de droit ou de fait nouvelle, purement confirmative de la décision du 15 février 2017, dès lors que la troisième demande d'autorisation de licenciement présentée par la société Café de Flore avait le même objet que la demande rejetée par la décision du 15 février 2017. En statuant ainsi, et en rejetant comme irrecevable la demande de la société Café de Flore tendant à l'annulation de la décision du 19 avril 2017, alors que la troisième demande d'autorisation de licenciement, si elle avait le même objet que la précédente, n'était pas fondée sur la même cause juridique dès lors que l'employeur n'invoquait plus que deux des griefs initiaux, la cour administrative d'appel a inexactement qualifié les faits et commis une erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Café de Flore est fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société Café de Flore à l'encontre de l'Etat, dès lors que, faute d'avoir interjeté appel du jugement du tribunal administratif de Paris du 12 juin 2018, l'Etat n'est plus partie à l'instance et y a seulement la qualité d'observateur.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 20 juin 2019 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées par la société Café de Flore au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la société Café de Flore et à M. H... I....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
              Délibéré à l'issue de la séance du 19 novembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; Mme A... P..., Mme E... O..., présidentes de chambre ; M. B... N..., M. L... J..., Mme K... M..., Mme C... G..., M. Damien Botteghi, conseillers d'Etat et Mme Françoise Tomé, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 8 décembre 2021.<br/>
<br/>
      La présidente : <br/>
      Signé : Mme Christine Maugüé<br/>
 		La rapporteure :<br/>
      Signé : Mme Françoise Tomé<br/>
      La secrétaire :<br/>
      Signé : Mme D... F...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
