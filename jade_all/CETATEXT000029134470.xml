<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029134470</ID>
<ANCIEN_ID>JG_L_2014_06_000000358330</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/13/44/CETATEXT000029134470.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 24/06/2014, 358330, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358330</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:358330.20140624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 avril et 26 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant... ; Mme B... demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 10MA00995 du 9 février 2012 de la cour administrative d'appel de Marseille en tant que, par cet arrêt, la cour, après avoir annulé le jugement n° 0803760 du tribunal administratif de Nîmes du 14 janvier 2010, n'a fait que partiellement droit à ses conclusions tendant à la condamnation de la commune de Bonnieux à lui verser, d'une part, la somme de 294 000 euros ou, à titre subsidiaire, la somme de 66 050,37 euros, en réparation des préjudices résultant pour elle d'une promesse non tenue de rendre la parcelle B 1932 constructible, d'autre part, la somme de 30 000 euros en réparation des dommages permanents résultant de la présence d'un stade à proximité de sa propriété ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Bonnieux la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau-Corlay-Marlange, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;  <br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'elle attaque, Mme B... soutient que la cour a insuffisamment motivé son arrêt en ne répondant pas à son argumentation tirée de ce que, lors de l'acquisition, par voie d'adjudication, de la parcelle B 1932, elle pouvait légitimement s'attendre à bénéficier d'un terrain constructible, compte tenu des engagements pris en ce sens par le maire de la commune ; qu'elle a méconnu le principe du caractère contradictoire de la procédure en relevant d'office et sans en avoir au préalable informé les parties le moyen tiré de ce qu'elle avait commis une imprudence de nature à atténuer la responsabilité de la commune ; qu'elle a dénaturé les faits en relevant qu'elle avait enchéri à 66 000 euros le prix d'un terrain dont la mise à prix initiale avait été fixée à 35 200 euros, sans qu'aucune autre enchère ait été faite, alors que le jugement d'adjudication mentionnait qu'il y avait eu diverses offres ; qu'elle a entaché son arrêt d'une contradiction de motifs en relevant à la fois qu'elle n'était pas " avertie des procédures administratives et des règles de droit applicables en urbanisme " et qu'elle avait néanmoins commis une imprudence en " donnant foi " à un engagement " manifestement illégal " ; qu'elle a commis une erreur de droit en jugeant qu'en donnant foi à un engagement manifestement illégal, elle avait commis une imprudence de nature à atténuer la responsabilité de la commune ; qu'elle a dénaturé les faits en retenant que les nuisances résultant de la proximité du stade municipal pouvaient être raisonnablement déduites de la présence de cet équipement et de ses conditions normales d'exploitation, alors qu'il résultait d'un constat d'huissier du 24 août 2008 qu'elle subissait un dommage anormal de nature à engager la responsabilité de la commune sur le fondement de la rupture d'égalité devant les charges publiques ;   <br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il se prononce sur les conclusions de Mme B...tendant à la condamnation de la commune de Bonnieux à lui verser la somme de 294 000 euros ou, à titre subsidiaire, la somme de 66 050,37 euros, en réparation des préjudices résultant d'une promesse non tenue de rendre constructible la parcelle B 1932 ; qu'en revanche, s'agissant des conclusions du pourvoi dirigées contre l'arrêt en tant qu'il se prononce sur les conclusions de Mme B... tendant à la condamnation de la commune de Bonnieux à lui verser la somme de 30 000 euros en réparation des dommages permanents résultant de la présence du stade municipal à proximité de sa propriété, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de Mme B...tendant à l'annulation de l'arrêt de la cour administrative de Marseille en tant qu'il statue sur ses conclusions tendant à la condamnation de la commune de Bonnieux à lui verser la somme de 294 000 euros ou, à titre subsidiaire, la somme de 66 050,37 euros, en réparation des préjudices résultant pour elle d'une promesse non tenue de rendre la parcelle B 1932 constructible sont admises.<br/>
<br/>
		Article 2 : Le surplus des conclusions du pourvoi de Mme B...n'est pas admis. <br/>
<br/>
		Article 3 : La  présente décision sera notifiée à Mme A...B....<br/>
		Copie en sera adressée pour information à la commune de Bonnieux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
