<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445630</ID>
<ANCIEN_ID>JG_L_2015_03_000000372318</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 18/03/2015, 372318, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372318</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372318.20150318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lille de prononcer la réduction de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1991. Par un jugement n° 0900573 du 2 février 2012, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12DA00531 du 11 juillet 2013, la cour administrative d'appel de Douai a accordé à M. A...une réduction de la cotisation supplémentaire d'impôt sur le revenu correspondant à la différence entre le montant de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1991 et celui résultant de la réduction du montant d'une plus-value, et réformé en ce sens le jugement du tribunal administratif de Lille. <br/>
<br/>
              Par un pourvoi, enregistré le 20 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12DA00531 du 11 juillet 2013 de la cour administrative d'appel de Douai ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel présenté par M. A...et de remettre à sa charge l'imposition dont la cour a prononcé la décharge.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. B...A... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 196-1 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement (...) c) De la réalisation de l'événement qui motive la réclamation. (...) " ; que seuls doivent être regardés comme constituant le point de départ du délai ainsi prévu les événements qui sont de nature à exercer une influence sur le bien-fondé de l'imposition, soit dans son principe, soit dans son montant ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... a cédé en 1991 les parts sociales de la SARL " Le Mabuse " qu'il détenait à hauteur de 50 % et qui étaient restées en indivision entre lui et son ex-épouse en l'absence de partage après leur divorce le 24 mars 1988 ; qu'à l'issue d'un contrôle sur pièces, l'administration a réintégré dans ses revenus imposables de l'année 1991 la plus-value réalisée à l'occasion de cette cession à hauteur de 411 060 francs (62 665,70 euros), qu'il n'avait pas déclarée ; que M. A...a demandé la réduction de cette imposition, par réclamations du 27 octobre 2006 et du 19 mai 2008, en faisant valoir qu'il avait été procédé, respectivement, par un acte notarié du 31 mai 2005, à la liquidation partielle de la communauté des biens entre les ex-époux et à la répartition entre eux du prix de vente des parts de la SARL " Le Mabuse " et, par un acte notarié du 16 novembre 2007, à la liquidation totale de la communauté et au partage des parts sociales de la SCI A...; que le ministre chargé du budget se pourvoit en cassation contre l'arrêt du 11 juillet 2013 par lequel la cour administrative d'appel de Douai a réformé le jugement du tribunal administratif de Lille du 2 février 2012 rejetant la demande de M. A...et accordé à celui-ci la décharge des impositions en litige pour le montant résultant de la différence entre celui de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 1991 et celui résultant de la réduction du montant de la plus-value litigieuse à concurrence de 205 530 francs (31 332,85 euros) ;<br/>
<br/>
              3. Considérant qu'en relevant que l'acte notarié du 16 novembre 2007 -  qui, au demeurant, ne portait pas sur le partage des parts sociales de la SARL " Le Mabuse ", lequel avait été opéré par l'acte du 31 mai 2005 - avait eu pour effet de liquider totalement la communauté de biens entre M. A... et son ex-épouse et constituait, par suite, un événement motivant une réclamation, au sens des dispositions du c) de l'article R 196-1 du livre des procédures fiscales, alors que cet acte n'était pas de nature à exercer une influence sur le bien-fondé de l'imposition, ni dans son principe, ni dans son montant, la cour administrative d'appel a commis une erreur de droit ; que, dès lors, pour ce motif, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que la réclamation présentée par M. A...le 19 mai 2008, tendant à la réduction de la cotisation supplémentaire d'impôt sur le revenu auquel il avait été assujetti au titre de l'année 1991, mise en recouvrement le 31 décembre 1997, est tardive au regard des dispositions du a) de l'article R. 196-1 du livre des procédures fiscales ; qu'il résulte de ce qui vient d'être dit que l'acte notarié dont il se prévaut ne peut constituer un événement de nature à rouvrir le délai de réclamation, sur le fondement du c) du même article ; que sa réclamation du 19 mai 2008 était, dès lors, irrecevable ; que, par suite, M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille a rejeté sa demande ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 11 juillet 2013 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : La requête présentée par M. A...devant la cour administrative d'appel de Douai et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
