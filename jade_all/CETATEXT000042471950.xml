<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042471950</ID>
<ANCIEN_ID>JG_L_2020_10_000000434882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/19/CETATEXT000042471950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 23/10/2020, 434882, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434882.20201023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de La Réunion d'annuler la décision par laquelle le maire du Tampon a rejeté sa demande du 7 décembre 2015 tendant à sa nomination en tant que stagiaire au titre du dispositif de titularisation institué par les articles 13 et suivants de la loi du 12 mars 2012 et de condamner la commune du Tampon à lui verser une indemnité d'un montant de 9 000 euros en réparation de son préjudice moral et financier. Par une ordonnance n° 1600242 du 7 avril 2017, le président de la 2ème chambre du tribunal administratif de La Réunion a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX01876 du 24 juin 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B... contre cette ordonnance.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 septembre et 26 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Tampon la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B... et à la SCP Gaschignard, avocat de la commune du Tampon ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. M. B..., agent contractuel de la commune du Tampon, a demandé au maire, par courrier du 7 décembre 2015, de procéder à sa nomination en tant que stagiaire dans le cadre du programme pluriannuel d'accès à l'emploi titulaire défini par une délibération du 26 juin 2013, et de lui allouer une indemnité en réparation du préjudice subi du fait de sa non-titularisation. Il a demandé au tribunal administratif de La Réunion d'annuler la décision par laquelle le maire du Tampon a rejeté sa demande et de condamner la commune du Tampon à lui verser une indemnité d'un montant de 9 000 euros en réparation de son préjudice moral et financier. Par l'arrêt attaqué du 24 juin 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B... contre l'ordonnance du 7 avril 2017 par laquelle le président de la 2ème chambre du tribunal administratif de La Réunion a rejeté sa demande.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 412-1 du code de justice administrative : " La requête doit, à peine d'irrecevabilité, être accompagnée, sauf impossibilité justifiée, de l'acte attaqué ou, dans le cas mentionné à l'article R. 421-2, de la pièce justifiant de la date de dépôt de la réclamation. / Cet acte ou cette pièce doit, à peine d'irrecevabilité, être accompagné d'une copie ".<br/>
<br/>
              3. D'autre part, en vertu de l'article L. 5 du code de justice administrative, l'instruction des affaires est contradictoire. Aux termes du premier alinéa de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser ". Selon l'article R. 611-1 de ce code, la requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties dans des conditions définies notamment par l'article R. 611-3. Aux termes de cet article : " Les décisions prises pour l'instruction des affaires sont notifiées aux parties (...). La notification peut être effectuée au moyen de lettres simples. / Toutefois, il est procédé aux notifications de la requête, des demandes de régularisation, des mises en demeure, des ordonnances de clôture, des décisions de recourir à l'une des mesures d'instruction prévues aux articles R. 621-1 à R. 626-3 ainsi qu'à l'information prévue à l'article R. 611-7 au moyen de lettres remises contre signature ou de tout autre dispositif permettant d'attester la date de réception (...) ". <br/>
<br/>
              4. Il résulte des dispositions citées au point précédent qu'il appartient au juge administratif d'inviter l'auteur d'une requête entachée d'une irrecevabilité susceptible d'être couverte en cours d'instance à la régulariser et qu'il doit être procédé à cette invitation par lettre remise contre signature ou par tout autre dispositif permettant d'attester la date de réception. La communication au requérant par lettre simple d'un mémoire en défense soulevant une fin de non-recevoir ne saurait, en principe, dispenser le juge administratif de respecter l'obligation ainsi prévue, à moins qu'il ne soit établi par ailleurs que le mémoire en défense a bien été reçu par l'intéressé.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que, pour rejeter la demande de M. B..., le président de la 2ème chambre du tribunal administratif de La Réunion s'est fondé sur la fin de non-recevoir soulevée par la commune du Tampon tirée de ce que M. B... n'aurait pas joint à sa requête une copie de l'acte attaqué ou la pièce justifiant de la date de dépôt de la réclamation conformément aux dispositions citées au point 2 de l'article R. 412-1 du code de justice administrative. Il n'est pas contesté que, alors que le mémoire en défense qui invoquait cette fin de non-recevoir avait été adressé par lettre simple à M. B..., qui n'a pas produit de réplique et ne pouvait, de ce fait, être réputé avoir reçu ce mémoire, le tribunal administratif de La Réunion n'a pas invité le demandeur à régulariser sa demande, dans les formes prévues par le deuxième alinéa de l'article R. 611-3 du code de justice administrative. Par suite, la cour administrative d'appel de Bordeaux a commis une erreur de droit en estimant que le président de la 2ème chambre du tribunal administratif de La Réunion avait pu régulièrement rejeter, comme manifestement irrecevable la demande présentée par M. B....<br/>
<br/>
              6. Il résulte de ce qui précède que M. B... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune du Tampon la somme de 3 000 euros à verser à M. B..., au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 juin 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : La commune du Tampon versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et à la commune du Tampon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
