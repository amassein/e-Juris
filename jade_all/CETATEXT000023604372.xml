<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023604372</ID>
<ANCIEN_ID>JG_L_2011_02_000000316727</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/60/43/CETATEXT000023604372.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 11/02/2011, 316727</TITRE>
<DATE_DEC>2011-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316727</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Bruno  Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Roger-Lacan Cyril</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:316727.20110211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 juin et 1er septembre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean Paul A, demeurant... ;  M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07BX00935 - 07BX00936 du 1er avril 2008 par lequel la cour administrative d'appel de Bordeaux, sur le recours du ministre de l'écologie et du développement durable, d'une part, a annulé l'article 1er du jugement n° 0401594 du 22 février 2007 par lequel le tribunal administratif de Pau a modifié l'article 5 de l'arrêté du préfet des Hautes-Pyrénées du 15 juin 2004 renouvelant l'autorisation d'exploiter la micro-centrale appartenant à M. A, située sur la Neste, à Hèches, en disposant que le débit à maintenir dans la rivière (débit réservé) ne devra pas être inférieur à 1,1 m3 par seconde ou au débit naturel du cours d'eau en amont de la prise si celui-ci est inférieur à ce chiffre, d'autre part, a rejeté la demande de M. A devant le tribunal administratif de Pau tendant à la modification du débit réservé à maintenir dans la rivière La Neste ;<br/>
<br/>
              2°) statuant au fond, de rejeter la requête présentée par le ministre de l'écologie devant la cour administrative d'appel de Bordeaux, d'annuler l'arrêté du préfet des Hautes-Pyrénées du 15 juin 2004 et, à titre subsidiaire, de modifier l'article 5 de cet arrêté en prévoyant que le débit minimal de la micro-centrale qui lui appartient ne pourra être inférieur à 0,6 m3  par seconde ou au débit naturel du cours d'eau en amont de la prise si celui-ci est inférieur à ce chiffre ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 8 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, en premier lieu, que par l'arrêt en date du 1er avril 2008 contre lequel M. A se pourvoit,  la cour administrative d'appel de Bordeaux a, d'une part, statué sur les conclusions à fin d'annulation du jugement du 22 février 2007 par lequel le tribunal administratif de Pau a modifié l'arrêté du préfet des Hautes-Pyrénées du 15 juin 2004 renouvelant l'autorisation d'exploiter la micro-centrale qu'il exploite sur le cours de la Neste, à Hèches, et, d'autre part, jugé qu'il n'y avait pas lieu de statuer sur les conclusions tendant à obtenir le sursis à l'exécution du même jugement ; que, dès lors, la circonstance que la cour aurait omis de viser ou d'analyser une fin de non recevoir opposée à ces dernières conclusions est, en tout état de cause, sans incidence sur la régularité de l'arrêt attaqué ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le schéma directeur d'aménagement et de gestion des eaux (SDAGE) Adour Garonne a fixé pour la Neste le niveau du " débit de crise " à 2 m3 par seconde ; que ce débit  correspond au niveau d'écoulement d'eau en deçà duquel sont mises en péril l'alimentation en eau potable et la survie des espèces présentes dans le milieu ; qu'ainsi, contrairement à ce que soutient le requérant, la cour a pu relever, sans commettre de dénaturation, que le tribunal administratif avait méconnu les dispositions du SDAGE en fixant à 1,1 m3 le débit minimal de la micro-centrale ; qu'en procédant à une telle appréciation, la cour n'a pas commis d'erreur de droit sur la nature du rapport des autorisations administratives délivrées dans le domaine de l'eau avec les dispositions du SDAGE, qui, aux termes de l'article L. 212-1 du code de l'environnement, est un rapport de compatibilité, et non de conformité ; <br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes des dispositions de l'article L. 432-5 du code de l'environnement applicables en l'espèce : " Tout ouvrage à construire dans le lit d'un cours d'eau doit comporter des dispositifs maintenant dans ce lit un débit minimal garantissant en permanence la vie, la circulation et la reproduction des espèces qui peuplent les eaux au moment de l'installation de l'ouvrage ainsi que, le cas échéant, des dispositifs empêchant la pénétration du poisson dans les canaux d'amenée et de fuite. / Ce débit minimal ne doit pas être inférieur au dixième du module du cours d'eau au droit de l'ouvrage correspondant au débit moyen interannuel, évalué à partir des informations disponibles portant sur une période minimale de cinq années, ou au débit à l'amont immédiat de l'ouvrage, si celui-ci est inférieur. " ; que la cour administrative d'appel n'a pas entaché son arrêt d'une erreur de droit en jugeant que le débit minimal ainsi défini, dès lors qu'il doit garantir en permanence la vie, la circulation et la reproduction des espèces qui peuplent les eaux au moment de l'installation de l'ouvrage, ne peut être composé que d'eaux s'écoulant naturellement au droit de l'ouvrage ; qu'en retenant, sur le fondement de ces dispositions, un débit minimal égal à 2,7 m3, ainsi que l'avait fait le préfet, la cour administrative d'appel s'est livrée à une appréciation souveraine de la valeur du module du cours d'eau au droit de l'ouvrage appartenant à M. A, qui n'est pas susceptible d'être discutée devant le juge de cassation ; que la cour n'était pas tenue de faire usage de ses pouvoirs de juge de plein contentieux pour fixer la valeur du débit minimal à un niveau au plus égal à celle du " débit de crise " fixée par le SDAGE, qui répond à des critères différents de ceux qui s'attachent à la définition du débit minimal ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les conclusions présentées par M. A doivent être rejetées, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : Le pourvoi de M. A est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Jean Paul A et à la ministre de l'écologie, du développement durable, du transport et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-01-01 EAUX. RÉGIME JURIDIQUE DES EAUX. RÉGIME JURIDIQUE DES COURS D'EAU. - 1) DÉBIT MINIMAL (ART. L. 432-5 DU CODE DE L'ENVIRONNEMENT ALORS EN VIGUEUR) - RÈGLES DE DÉTERMINATION - 2) POUVOIRS DU JUGE DE PLEIN CONTENTIEUX - OBLIGATION DE FIXER LA VALEUR DU DÉBIT MINIMAL À UN NIVEAU AU PLUS ÉGAL À CELLE DU DÉBIT DE CRISE FIXÉ PAR LE SDAGE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">27-05-05 EAUX. GESTION DE LA RESSOURCE EN EAU. SCHÉMAS DIRECTEURS ET SCHÉMAS D'AMÉNAGEMENT ET DE GESTION DES EAUX. - POUVOIRS DU JUGE DE PLEIN CONTENTIEUX - OBLIGATION DE FIXER LA VALEUR DU DÉBIT MINIMAL À UN NIVEAU AU PLUS ÉGAL À CELLE DU DÉBIT DE CRISE FIXÉ PAR LE SDAGE - ABSENCE.
</SCT>
<ANA ID="9A"> 27-01-01 1) Aux termes des dispositions de l'article L. 432-5 du code de l'environnement : Tout ouvrage à construire dans le lit d'un cours d'eau doit comporter des dispositifs maintenant dans ce lit un débit minimal garantissant en permanence la vie, la circulation et la reproduction des espèces qui peuplent les eaux au moment de l'installation de l'ouvrage ainsi que, le cas échéant, des dispositifs empêchant la pénétration du poisson dans les canaux d'amenée et de fuite. / Ce débit minimal ne doit pas être inférieur au dixième du module du cours d'eau au droit de l'ouvrage correspondant au débit moyen interannuel, évalué à partir des informations disponibles portant sur une période minimale de cinq années, ou au débit à l'amont immédiat de l'ouvrage, si celui-ci est inférieur. Le débit minimal ainsi défini ne peut être composé que d'eaux s'écoulant naturellement au droit de l'ouvrage.,,2) Le juge du fond n'était pas tenu de faire usage de ses pouvoirs de juge de plein contentieux pour fixer la valeur du débit minimal à un niveau au plus égal à celle du débit de crise fixée par le schéma directeur d'aménagement et de gestion des eaux (SDAGE), qui répond à des critères différents de ceux qui s'attachent à la définition du débit minimal.</ANA>
<ANA ID="9B"> 27-05-05 Le juge du fond n'était pas tenu de faire usage de ses pouvoirs de juge de plein contentieux pour fixer la valeur du débit minimal à un niveau au plus égal à celle du débit de crise fixée par le schéma directeur d'aménagement et de gestion des eaux (SDAGE), qui répond à des critères différents de ceux qui s'attachent à la définition du débit minimal.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
