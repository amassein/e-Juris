<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701526</ID>
<ANCIEN_ID>JG_L_2017_10_000000398233</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701526.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 02/10/2017, 398233, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398233</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398233.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Laboratoires Ineldea a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir la lettre du directeur départemental de la protection des populations des Alpes-Maritimes du 28 juillet 2011 portant " pré-injonction ", la décision du même directeur du 30 novembre 2011 portant injonction de mettre en conformité avec la réglementation applicable l'étiquetage des produits de la gamme " Pediakid " ainsi que la décision du 27 janvier 2012 rejetant le recours gracieux formé à l'encontre de cette mesure. Par un jugement n° 1201267 du 29 octobre 2013, le tribunal administratif de Nice a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13MA05126 du 28 janvier 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé contre ce jugement par les sociétés Laboratoires Ineldea et Veadis.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 mars 2016, 24 juin 2016 et 31 mars 2017 au secrétariat du contentieux du Conseil d'Etat, les sociétés Laboratoires Ineldea et Veadis demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat les dépens ainsi que la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CE) n° 178/2002 du Parlement européen et du Conseil du 28 janvier 2002 ; <br/>
              - la directive 2002/46/CE du Parlement européen et du Conseil du 10 juin 2002 ;<br/>
              - le règlement (CE) n° 1924/2006 du Parlement européen et du Conseil du 20 décembre 2006 ;<br/>
              - le règlement (CE) n° 834/2007 du Conseil du 28 juin 2007 ; <br/>
              - le code de la consommation ;<br/>
              - le décret n° 2006-352 du 20 mars 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Leduc, Vigand, avocat de la SAS Laboratoires Ineldea et de la SAS Veadis.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une inspection menée lors de la mise sur le marché par les sociétés Laboratoires Ineldea et Veadis de treize compléments alimentaires, le directeur départemental de la protection des populations des Alpes-Maritimes a formulé plusieurs observations, le 28 juillet 2011, puis enjoint à ces sociétés, le 30 novembre 2011, sur le fondement des articles L. 141-1 et L. 218-5 du code de la consommation, de lui présenter pour le 9 janvier 2012 un échéancier en vue de mettre en conformité l'étiquetage et la publicité de onze de ces compléments alimentaires en en retirant les mentions faisant état d'un mode de production biologique et les allégations nutritionnelles et de santé interdites. Après avoir formé un recours gracieux, rejeté le 27 janvier 2012, la société Laboratoires Ineldea a demandé au tribunal administratif de Nice d'annuler la " pré-injonction " du directeur départemental de la protection des populations des Alpes-Maritimes du 28 juillet 2011, l'injonction du 30 novembre 2011 ainsi que le rejet de son recours gracieux. Elle se pourvoit, conjointement avec la société Veadis, contre l'arrêt du 28 janvier 2016 par lequel la cour administrative d'appel de Marseille a rejeté leur appel formé contre le jugement du tribunal administratif de Nice du 29 octobre 2013 rejetant ces demandes.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 2 du règlement du Parlement européen et du Conseil du 28 janvier 2002 établissant les principes généraux et les prescriptions générales de la législation alimentaire, instituant l'Autorité européenne de sécurité des aliments et fixant des procédures relatives à la sécurité des denrées alimentaires : " Aux fins du présent règlement, on entend par " denrée alimentaire " (ou "aliment "), toute substance ou produit, transformé, partiellement transformé ou non transformé, destiné à être ingéré ou raisonnablement susceptible d'être ingéré par l'être humain. / (...) / Le terme " denrée alimentaire " ne couvre pas : / a) les aliments pour animaux ; / b) les animaux vivants à moins qu'ils ne soient préparés en vue de la consommation humaine ; / c) les plantes avant leur récolte ; / d) les médicaments au sens des directives 65/65/CEE et 92/73/CEE du Conseil ; / e) les cosmétiques au sens de la directive 76/768/CEE du Conseil ; / f) le tabac et les produits du tabac au sens de la directive 89/622/CEE du Conseil ; / g) les stupéfiants et les substances psychotropes au sens de la Convention unique des Nations unies sur les stupéfiants de 1961 et de la Convention des Nations unies sur les substances psychotropes de 1971 ; / h) les résidus et contaminants ". La directive du Parlement européen et du Conseil du 10 juin 2002 relative au rapprochement des législations des Etats membres concernant les compléments alimentaires, transposée en droit français par le décret du 20 mars 2006 relatif aux compléments alimentaires, indique à son article 1er qu'elle " concerne les compléments alimentaires commercialisés comme des denrées alimentaires et présentés comme tels " et définit à son article 2 les compléments alimentaires auxquels elle s'applique comme des " denrées alimentaires dont le but est de compléter le régime alimentaire normal et qui constituent une source concentrée de nutriments ou d'autres substances ayant un effet nutritionnel ou physiologique seuls ou combinés, commercialisés sous forme de doses (...) ". Il résulte clairement de ces dispositions que la directive du 10 juin 2002, si elle soumet les compléments alimentaires qu'elle régit à des dispositions particulières destinées à répondre à certaines de leurs spécificités, n'a pas pour effet de les soustraire à l'application des dispositions prévues, pour l'ensemble des denrées alimentaires, par le règlement du 28 janvier 2002.<br/>
<br/>
              3. D'une part, le j de l'article 2 du règlement du 28 juin 2007 relatif à la production biologique et à l'étiquetage des produits biologiques prévoit, pour l'application de ce texte, s'agissant des termes " denrée alimentaire ", que " les définitions figurant dans le règlement (CE) n° 178/2002 du Parlement européen et du Conseil du 28 janvier 2002 établissant les principes généraux et les prescriptions générales de la législation alimentaire, instituant l'Autorité européenne de sécurité des aliments et fixant des procédures relatives à la sécurité des denrées alimentaires s'appliquent (...) ". Le 2 de l'article 1er de ce règlement précise que : " Le présent règlement s'applique aux produits agricoles ci-après, y compris les produits de l'aquaculture, lorsqu'ils sont mis sur le marché ou destinés à être mis sur le marché : / (...) / b) produits agricoles transformés destinés à l'alimentation humaine (...) ". Il résulte clairement de ces dispositions, de même que des objectifs d'information des consommateurs, de transparence du marché et de promotion du recours aux ingrédients biologiques poursuivis par le règlement, que celui-ci s'applique à l'ensemble des denrées alimentaires, dès lors qu'elles incorporent des produits agricoles, au sens de l'article 2 du règlement du 28 janvier 2002, y compris aux compléments alimentaires. <br/>
<br/>
              4. D'autre part, le point 2 de l'article 1er du règlement du Parlement européen et du Conseil du 20 décembre 2006 concernant les allégations nutritionnelles et de santé portant sur les denrées alimentaires prévoit que celui-ci " s'applique aux allégations nutritionnelles et de santé formulées dans les communications à caractère commercial, qu'elles apparaissent dans l'étiquetage ou la présentation des denrées alimentaires ou la publicité faite à leur égard, dès lors que les denrées alimentaires en question sont destinées à être fournies en tant que telles au consommateur final (...) " et le point 5 de cet article précise qu'il s'applique " sans préjudice (...) de la directive 2002/46/CE ". En outre, l'article 2 de ce règlement se réfère, tout comme celui du règlement du 28 juin 2007, à la définition des " denrées alimentaires " figurant à l'article 2 du règlement du 28 janvier 2002 précité. Enfin, si son article 7 prévoit que : " Dans le cas des compléments alimentaires, les informations nutritionnelles sont fournies conformément à l'article 8 de la directive 2002/46/CE ", ce renvoi à la directive du 10 juin 2002 a pour objet, non d'exclure les compléments alimentaires du champ d'application du règlement, mais de préciser les modalités selon lesquelles s'applique, pour les compléments alimentaires, l'obligation de fournir les informations nutritionnelles lorsqu'une allégation de santé ou une allégation nutritionnelle est faite. Il en résulte clairement que le règlement du 20 décembre 2006 s'applique à l'ensemble des denrées alimentaires, y compris aux compléments alimentaires.<br/>
<br/>
              5. Il résulte de ce qui précède que c'est sans erreur de droit et en donnant aux faits de l'espèce leur exacte qualification juridique que la cour a jugé applicables aux compléments alimentaires commercialisés par les sociétés requérantes les règlements du 28 juin 2007 en ce qui concerne l'utilisation dans l'étiquetage de termes faisant référence à la production biologique et du 20 décembre 2006 en ce qui concerne les allégations nutritionnelles et de santé.<br/>
<br/>
              6. En deuxième lieu, aux termes de l'article L. 141-1 du code de la consommation, dans sa rédaction alors en vigueur : " I.- Sont recherchés et constatés, dans les conditions fixées par les articles L. 450-1 à L. 450-4, L. 450-7, L. 450-8, L. 470-1 et L. 470-5 du code de commerce, les infractions ou manquements prévus aux dispositions suivantes du présent code : (...) 2° Les sections 1, 2, 3, 8, 9 et 12 du chapitre Ier du titre II du livre Ier ; (...) V. - Les agents habilités à constater les infractions ou manquements aux obligations mentionnées aux I, II et III peuvent, après une procédure contradictoire, enjoindre au professionnel, en lui impartissant un délai raisonnable, de se conformer à ces obligations, de cesser tout agissement illicite ou de supprimer toute clause illicite ". Aux termes du I de l'article L. 121-1 alors en vigueur, figurant dans la section 1 du chapitre Ier du titre II du livre Ier du code de la consommation : " Une pratique commerciale est trompeuse si elle est commise dans l'une des circonstances suivantes : (...) 2° Lorsqu'elle repose sur des allégations, indications ou présentations fausses ou de nature à induire en erreur et portant sur l'un ou plusieurs des éléments suivants : (...) b) Les caractéristiques essentielles du bien ou du service, à savoir : ses qualités substantielles, sa composition, ses accessoires, son origine, sa quantité, son mode et sa date de fabrication, les conditions de son utilisation et son aptitude à l'usage, ses propriétés et les résultats attendus de son utilisation, ainsi que les résultats et les principales caractéristiques des tests et contrôles effectués sur le bien ou le service ". De telles pratiques commerciales trompeuses peuvent notamment résulter de la présence de mentions figurant sur l'étiquetage ou dans la publicité relative à des denrées alimentaires en méconnaissance de textes tels que les règlements du 20 décembre 2006 concernant les allégations nutritionnelles et de santé portant sur les denrées alimentaires et du 28 juin 2007 relatif à la production biologique et à l'étiquetage des produits biologiques. En conséquence, la cour, qui a regardé comme une pratique commerciale trompeuse la mention d'allégations nutritionnelles et de santé ne correspondant pas aux prescriptions du règlement du 20 décembre 2006 et la valorisation, dans des conditions contraires au règlement du 28 juin 2007, de la production biologique de certains ingrédients des compléments alimentaires en cause, n'a pas commis d'erreur de droit ni méconnu son office en jugeant que l'administration pouvait légalement faire usage de son pouvoir d'injonction pour faire cesser ces pratiques sur le fondement du V de l'article L. 141-1 du code de la consommation, alors même que cette disposition ne mentionne pas ces deux règlements.<br/>
<br/>
              7. En troisième lieu, aux termes de l'article L. 218-5 du code de la consommation, dans sa rédaction alors en vigueur : " Lorsque les agents mentionnés à l'article L. 215-1 constatent qu'un lot n'est pas conforme à la réglementation en vigueur, ces agents peuvent en ordonner la mise en conformité, dans un délai qu'ils fixent. Si la mise en conformité n'est pas possible, le préfet (...) peut ordonner l'utilisation à d'autres fins, la réexpédition vers le pays d'origine ou la destruction des marchandises dans un délai qu'il fixe (...) ". Il ne ressort nullement de ces dispositions, contrairement à ce qui est soutenu, qu'elles ne s'appliqueraient qu'à une absence de conformité à la réglementation concernant la substance même d'un produit et non à une absence de conformité à la réglementation concernant son étiquetage. C'est ainsi sans erreur de droit que la cour a jugé que la décision du 30 novembre 2011 enjoignant aux sociétés requérantes de mettre en conformité l'étiquetage des compléments alimentaires en cause pouvait légalement se fonder, outre sur le V l'article L. 141-1 précité du code de la consommation, sur l'article L. 218-5 de ce code.<br/>
<br/>
              8. En quatrième lieu, d'une part, la cour n'a pas commis d'erreur de droit en tenant compte, pour apprécier, dans les circonstances de l'espèce, le caractère raisonnable du délai laissé aux sociétés requérantes à compter de la lettre du 30 novembre 2011 pour satisfaire à l'injonction qui leur était faite, de ce qu'une procédure contradictoire préalable avait précédé ce délai, ayant permis des échanges et éclaircissements entre ces sociétés et l'administration. D'autre part, c'est sans dénaturer les pièces du dossier ou les faits de l'espèce qu'elle a estimé qu'au regard de la teneur de l'injonction, tendant principalement à la production d'un échéancier, assorti de documents explicatifs ou justificatifs, ce délai pouvait être regardé comme raisonnable.<br/>
<br/>
              9. En dernier lieu, à supposer même que les sociétés requérantes puissent être regardées comme ayant, devant la cour, soulevé un moyen tiré de ce qu'elles avaient mis l'étiquetage des produits litigieux en conformité avec les exigences énoncées dans la lettre du 30 novembre 2011, cette circonstance postérieure à la décision attaquée était sans incidence sur la légalité de celle-ci. La cour pouvait, dès lors, sans irrégularité s'abstenir de répondre à ce moyen, qui était inopérant.<br/>
<br/>
              10. Il résulte de tout ce qui précède que les sociétés requérantes ne sont pas fondées à demander l'annulation de l'arrêt attaqué. Dans ces conditions, leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent être accueillies.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Laboratoires Ineldea et Veadis est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Laboratoires Ineldea, à la société Veadis et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
