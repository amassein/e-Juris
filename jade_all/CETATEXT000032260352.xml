<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260352</ID>
<ANCIEN_ID>JG_L_2016_03_000000394547</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260352.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/03/2016, 394547, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394547</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:394547.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...B..., candidat avec Mme F...E...aux élections qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Niort 1 (Deux-Sèvres) en vue de l'élection des conseillers départementaux, a demandé au tribunal administratif de Poitiers d'annuler ces opérations électorales. Par une autre protestation, Mme E...a demandé au tribunal d'annuler ces mêmes opérations électorales et de déclarer M. D...G...et Mme H... A...inéligibles pour une durée de trois ans. Par un jugement nos 1500857, 1500862 du 14 octobre 2015, le tribunal administratif de Poitiers a annulé ces opérations électorales et rejeté le surplus des conclusions de MmeE....<br/>
<br/>
              Par une requête, enregistrée le 13 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A... et M. G... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de valider les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Niort 1 ;<br/>
<br/>
              3°) de mettre à la charge de Mme E...et de M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 52-1 du code électoral : " (...) A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin (...) ". Aux termes de l'article L. 52-8 du même code : " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) ".<br/>
<br/>
              2. En premier lieu, il résulte de l'instruction, d'une part, que le numéro d'avril 2015 de la revue municipale " Vivre à Niort " inclut une tribune signée par tous les élus de la majorité municipale, dont M. G...et MmeA..., qui dénonce en termes polémiques la situation dans laquelle la ville se trouvait un an plus tôt, lors du changement de majorité et, d'autre part, que ce même numéro d'avril comporte, en remplacement de l'habituel éditorial du maire, un encart de quatre pages intitulé " Un an d'actions, la lettre du maire ", dressant un bilan flatteur de l'action du maire et des élus de sa majorité depuis leur élection, avec une tonalité clairement électorale. Cet encart reprend, parfois dans les mêmes termes, certains de thèmes développés, lors de la campagne pour les élections départementales, par M. G...et Mme A..., lesquels ont au demeurant insisté, lors de cette campagne, sur leur qualité de conseillers municipaux et sur le soutien que leur accordait le maire de Niort.<br/>
<br/>
              3. En deuxième lieu, il résulte de l'instruction que le numéro d'avril 2015 du magazine " Vivre à Niort ", accompagné de la lettre du maire qui était datée du 30 mars, a été distribué dans un grand nombre de boîtes aux lettres du canton de Niort 1 le vendredi 27 et le samedi 28 mars, veille et avant-veille du second tour des élections départementales, alors que les jours habituels de diffusion de ce magazine étaient les lundis, mardis et mercredis et que le document du service de la communication de la ville de Niort intitulé " Chronogramme du Vivre à Niort n° 246 Avril 2015 ", daté du 12 février 2015, indiquait pour le numéro d'avril 2015, conformément à cette pratique habituelle, une distribution prévue le lundi 30 mars, le mardi 31 mars et le mercredi 1er avril.<br/>
<br/>
              4. Il résulte de ce qui précède que la publication litigieuse a constitué, comme l'a jugé le tribunal administratif, une campagne de promotion publicitaire des réalisations et de la gestion de la commune de Niort, sur le territoire de laquelle s'est déroulé le scrutin litigieux, prohibée par l'article L. 52-1 du code électoral, et que sa diffusion a constitué un don consenti par une personne morale, en méconnaissance de l'article L. 52-8 du même code.<br/>
<br/>
              5. Compte tenu de l'écart de voix, qui était seulement de 18 suffrages, soit 0,3 % des 5 700 suffrages exprimés, la distribution de cette publication a été de nature, dans les circonstances de l'espèce, à altérer les résultats du scrutin.<br/>
<br/>
              6. Il suit de là que M. G...et Mme A...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Poitiers a annulé les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Niort 1.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de Mme E...et de M.B..., qui ne sont pas, dans la présente instance, la partie perdante, le versement de la somme que réclament M. G...et Mme A...au titre des frais exposés par eux et non compris dans les dépens. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de ces derniers le versement de la somme que réclament Mme E...et de M. B...au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. G...et Mme A...est rejetée.<br/>
Article 2 : Les conclusions de Mme E...et de M. B...tendant à ce qu'une somme soit mise à la charge de M. G...et Mme A...sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme H...A..., à M. D...G..., à Mme F...E...et à M. C...B....<br/>
Copie en sera adressée au ministre de l'intérieur et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
