<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028036188</ID>
<ANCIEN_ID>JG_L_2013_10_000000352563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/03/61/CETATEXT000028036188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/10/2013, 352563, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352563.20131004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 9 septembre et 8 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Montpellier, représentée par son maire ; la commune de Montpellier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande d'édiction du décret, prévu à l'article L. 2333-84 du code général des collectivités territoriales, fixant le régime des redevances d'occupation provisoire du domaine public communal par des chantiers de travaux relatifs aux ouvrages de transport et de distribution d'électricité et de gaz et aux lignes ou canalisations particulières d'énergie électrique et de gaz, et d'indemnisation du préjudice résultant de l'absence de ce décret ;<br/>
<br/>
              2°) d'enjoindre au Premier Ministre d'édicter ce décret dans un délai de deux mois à compter de la notification de la décision à intervenir, sous astreinte de 1 500 euros par jour de retard ;<br/>
<br/>
              3°) de condamner l'Etat à lui verser une indemnité de 236 713,21 euros avec les intérêts de droit à compter du 9 mai 2011 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 53-661 du 1er août 1953 ;<br/>
<br/>
              Vu la loi n° 96-142 du 21 février 1996 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP <br/>
Lyon-Caen, Thiriez, avocat de la Commune De Montpellier ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que, par une délibération du 20 décembre 2005, le conseil municipal de la commune de Montpellier a institué une redevance d'occupation provisoire du domaine public communal par des chantiers de travaux, dont elle a ensuite actualisé le tarif par des délibérations annuelles successives ; que, sur la base de ces délibérations, la commune a émis des titres de paiement à l'encontre des sociétés Electricité Réseau Distribution France (ERDF) et Gaz Réseau Distribution France (GRDF) ; que, par cinq jugements des 9 juin 2010 et 8 avril 2011, le tribunal administratif de Montpellier, faisant droit à la demande des deux sociétés, a annulé ces titres de paiement au motif qu'en l'absence du décret, prévu à l'article L. 2333-84 du code général des collectivités territoriales, fixant le régime des redevances d'occupation du domaine public communal par des chantiers de travaux relatifs aux ouvrages de transport et de distribution d'électricité et de gaz et aux lignes ou canalisations particulières d'énergie électrique et de gaz, les délibérations du conseil municipal instituant cette redevance étaient dépourvues de base légale ; que, par lettre reçue le 9 mai 2011, la commune a demandé au Premier ministre d'édicter le décret prévu à l'article L. 2333-84 du code général des collectivités territoriales et de l'indemniser du préjudice qu'elle estime avoir subi du fait de l'absence de ce décret ; que la commune demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande, de lui enjoindre d'édicter ce décret dans un délai de deux mois, sous astreinte de 1 500 euros par jour de retard, et de condamner l'Etat à lui verser une indemnité de 236 713,21 euros en réparation du préjudice subi ;<br/>
<br/>
              Sur les conclusions d'excès de pouvoir :<br/>
<br/>
              2. Considérant qu'en vertu de l'article 21 de la Constitution, le Premier ministre " assure l'exécution des lois " et " exerce le pouvoir réglementaire ", sous réserve de la compétence conférée au Président de la République pour les décrets en Conseil des ministres par l'article 13 de la Constitution ; que l'exercice du pouvoir réglementaire comporte non seulement le droit, mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle ;<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 2333-84 du code général des collectivités territoriales, issu de la loi du 21 février 1996 relative à la partie législative du code général des collectivités territoriales et reprenant des dispositions qui figuraient antérieurement aux articles L. 374-4 et L. 375-7 du code des communes : " Le régime des redevances dues aux communes en raison de l'occupation de leur domaine public par les ouvrages de transport et de distribution d'électricité et de gaz et par les lignes ou canalisations particulières d'énergie électrique et de gaz, ainsi que pour les occupations provisoires de leur domaine public par les chantiers de travaux, est fixé par décret en Conseil d'Etat sous réserve des dispositions des premier et deuxième alinéas de l'article unique de la loi n° 53-661 du 1er août 1953 fixant le régime des redevances dues pour l'occupation du domaine public par les ouvrages de transport et de distribution d'électricité et de gaz, par les lignes ou canalisations particulières d'énergie électrique et de gaz. " ;<br/>
<br/>
              4. Considérant que si, en l'absence de réglementation particulière, toute autorité gestionnaire du domaine public est compétente pour délivrer des permissions d'occupation provisoire de ce domaine et pour fixer le tarif de la redevance due en contrepartie, en tenant compte des avantages de toute nature que le permissionnaire est susceptible de retirer de cette occupation, le législateur, par les dispositions précitées, a confié au pouvoir réglementaire le soin de fixer, par décret en Conseil d'Etat, un régime particulier de redevances d'occupation du domaine public communal par les ouvrages de transport et de distribution d'électricité et de gaz et par les lignes ou canalisations particulières d'énergie électrique et de gaz, ainsi que par des chantiers de travaux relatifs à de tels ouvrages ; que, si des décrets sont intervenus pour définir le régime des redevances dues aux communes en raison de l'occupation de leur domaine public par les ouvrages de transport et de distribution d'énergie électrique et de gaz, il n'en est pas de même pour les redevances dues en raison de l'occupation provisoire de ce domaine public par les chantiers de travaux afférents à ces ouvrages ; que l'instauration d'un tel régime particulier de redevances d'occupation du domaine public communal constituait, pour le pouvoir réglementaire, une obligation en vue d'assurer une application effective des dispositions précitées de l'article L. 2333-84 du code général des collectivités territoriales ; que, pour instaurer un tel régime, un décret était nécessaire ; qu'il ne ressort pas des pièces du dossier que son élaboration se serait heurtée à des difficultés particulières ; que le refus implicite du Premier ministre d'édicter ce décret est intervenu alors qu'était dépassé le délai raisonnable pour son édiction ; que, par suite, ce refus est entaché d'illégalité ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la commune de Montpellier est fondée, pour ce motif, à demander l'annulation pour excès de pouvoir de la décision attaquée en tant qu'elle a trait à l'édiction du décret prévu à l'article L. 2333-84 du code général des collectivités territoriales ;<br/>
<br/>
              Sur les conclusions à fins d'injonction :<br/>
<br/>
              6. Considérant que l'annulation de la décision attaquée implique nécessairement que le Premier ministre prenne le décret litigieux ; qu'il y a lieu pour le Conseil d'Etat de lui enjoindre de prendre ce décret dans un délai de quatre mois à compter de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la commune ;<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit au point 4 qu'en l'absence du décret litigieux, le régime prévu particulier prévu par l'article L. 2333-84 du code général des collectivités territoriales en ce qui concerne les redevances dues aux communes en raison de l'occupation provisoire de leur domaine public par les chantiers de travaux afférents aux ouvrages de transport et de distribution d'énergie électrique et de gaz n'a pu entrer en vigueur ; que, par suite, la commune de Montpellier, en tant qu'autorité gestionnaire de son domaine public, était compétente pour instaurer une redevance d'occupation de ce domaine par des chantiers de travaux relatifs aux ouvrages de transport et de distribution d'électricité et de gaz et aux lignes ou canalisations particulières d'énergie électrique et de gaz ; que, dès lors, le préjudice dont se prévaut la commune et qui, selon elle, correspond à la somme du produit des titres de paiement dont ERDF et GRDF ont obtenu l'annulation ou qu'elle a retirés et des frais de justice qu'elle a supportés devant le tribunal administratif de Montpellier, n'a pas de lien de causalité direct avec l'absence d'édiction du décret prévu par la loi ; que, par suite, les conclusions indemnitaires de la commune doivent être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la commune de Montpellier, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite de rejet par le Premier ministre de la demande d'édiction du décret prévu à l'article L. 2333-84 du code général des collectivités territoriales présentée par la commune de Montpellier est annulée.<br/>
Article 2 : Il est enjoint au Premier ministre de prendre, dans un délai de quatre mois à compter de la notification de la présente décision, le décret, prévu à l'article L. 2333-84 du code général des collectivités territoriales, fixant le régime des redevances d'occupation provisoire du domaine public communal par des chantiers de travaux relatifs aux ouvrages de transport et de distribution d'électricité et de gaz et aux lignes ou canalisations particulières d'énergie électrique et de gaz.<br/>
Article 3 : L'Etat versera à la commune de Montpellier une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la commune de Montpellier et au Premier ministre.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
