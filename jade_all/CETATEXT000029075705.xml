<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029075705</ID>
<ANCIEN_ID>JG_L_2014_06_000000360740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/07/57/CETATEXT000029075705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 11/06/2014, 360740, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360740.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 4 juillet et 4 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. G... I..., demeurant..., Mme B...D..., demeurant ...et M. A... F..., demeurant... ; M. I... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 4 mai 2012 portant nomination des vice-présidents des chambres régionales des comptes d'Aquitaine - Poitou-Charentes, d'Auvergne - Rhône-Alpes, de Nord-Pas-de-Calais - Picardie et de Provence-Alpes-Côte d'Azur ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 28 mai 2014, présentée pour M. L...J... ;<br/>
<br/>
              Vu le code des juridictions financières, modifié notamment par la loi n° 2012-347 du 12 mars 2012 et le décret n° 2012-255 du 23 février 2012 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de M. G...I...et autres, et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. L...J...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, dans sa rédaction issue de l'article 95 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique, l'article L. 212-3 du code des juridictions financières prévoit que les chambres régionales des comptes comptant au moins quatre sections disposent d'un vice-président, qui est un conseiller référendaire à la Cour des comptes ; qu'aux termes de l'article L. 221-2 du même code, dans sa rédaction issue de la même la loi : " L'emploi de président de chambre régionale des comptes est pourvu par un conseiller maître ou un conseiller référendaire à la Cour des comptes. L'emploi de vice-président de chambre régionale des comptes est pourvu par un conseiller référendaire à la Cour des comptes. / Les nominations sont prononcées, à la demande des magistrats intéressés, par décret du Président de la République, sur proposition du premier président de la Cour des comptes après avis du conseil supérieur de la Cour des comptes et du Conseil supérieur des chambres régionales des comptes. / Peuvent se porter candidats à ces emplois les magistrats de la Cour des comptes ainsi que les présidents de section de chambre régionale des comptes inscrits sur une liste d'aptitude établie à cet effet par le Conseil supérieur des chambres régionales des comptes. (...) " ; <br/>
<br/>
              2. Considérant que l'article R. 212-1 du code des juridictions financières, dans sa rédaction résultant du décret du 23 février 2012 relatif au siège et au ressort des chambres régionales des comptes, a redéfini les sièges et ressorts des chambres régionales des comptes ; qu'en vertu des dispositions de l'article R. 212-6 du même code, dans sa rédaction résultant du même décret, disposent d'au moins quatre sections les chambres régionales des comptes d'Aquitaine - Poitou-Charentes, d'Auvergne - Rhône-Alpes, d'Ile-de-France, de Nord-Pas-de-Calais - Picardie et de Provence-Alpes-Côte d'Azur ; qu'il résulte ainsi de ces dispositions qu'à compter de leur entrée en vigueur, fixée, par l'article 4 du décret du 23 février 2012, au 2 avril 2012, devaient être pourvus, non plus le seul emploi de vice-président de la chambre régionale des comptes d'Ile-de-France, mais cinq emplois de vice-président ; qu'en application de ces dispositions, le décret attaqué procède à la nomination des vice-présidents des chambres régionales des comptes d'Aquitaine - Poitou-Charentes, d'Auvergne - Rhône-Alpes, de Nord-Pas-de-Calais - Picardie et de Provence-Alpes-Côte d'Azur ;<br/>
<br/>
              3. Considérant, en premier lieu, que la circonstance que le décret attaqué ne vise ni les textes législatifs sur le fondement desquels il a été pris, ni les avis du conseil supérieur de la Cour des comptes et du Conseil supérieur des chambres régionales des comptes sur les nominations litigieuses, est sans incidence sur sa légalité ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article R. 221-2 du code des juridictions financières : " La liste d'aptitude à l'emploi de président ou de vice-président de chambre régionale des comptes est établie chaque année par le Conseil supérieur des chambres régionales des comptes ; sa validité est de douze mois à compter de sa publication au Journal officiel de la République française " ; que le décret attaqué a été pris à la suite de l'appel à candidatures effectué en 2011 en vue de pourvoir les emplois de président et de vice-président de chambre régionale des comptes susceptibles d'être vacants en 2012 ; que la liste des présidents de section de chambre régionale des comptes jugés aptes à être nommés à ces fonctions a été établie par le Conseil supérieur des chambres régionales des comptes le 8 décembre 2011 ; qu'en vertu des dispositions citées ci-dessus, elle demeurait valable pour une durée de douze mois ; que si, à la date à laquelle elle a été établie, les quatre nouveaux emplois de vice-président de chambre régionale des comptes n'avaient pas encore été créés par les textes cités au point 2, ces nouvelles dispositions n'ont pas eu pour effet de modifier la nature des emplois auxquels les magistrats de la Cour des comptes et les présidents de section de chambre régionale des comptes intéressés s'étaient portés candidats, ceux-ci n'ayant, au demeurant, pas connaissance des emplois qui seraient effectivement vacants en 2012 lorsqu'ils ont fait acte de candidature ; que, dans ces conditions, la circonstance que les autorités compétentes n'aient pas procédé à un nouvel appel à candidatures ni établi une nouvelle liste d'aptitude après l'entrée en vigueur des nouvelles dispositions citées ci-dessus n'a privé d'aucune garantie les personnes susceptibles de se porter candidates à ces emplois et n'a pas entaché d'illégalité le décret attaqué ;<br/>
<br/>
              5. Considérant, en troisième lieu, que douze présidents de section de chambre régionale des comptes ont été inscrits sur la liste d'aptitude établie par le Conseil supérieur des chambres régionales des comptes le 8 décembre 2011 ; que, contrairement à ce qui est soutenu, il ne ressort pas des pièces du dossier que l'établissement de cette liste n'aurait pas été précédé d'un examen approfondi de la valeur professionnelle de chacun des candidats ; que, dix jours avant la séance, ont été communiqués aux membres du Conseil le curriculum vitae de chaque candidat,  l'avis du président de la chambre au sein de laquelle il était affecté, ainsi qu'un tableau synthétique indiquant la date de nomination des candidats dans le grade de président de section, leur âge et le nombre des candidatures qu'ils avaient déjà présentées ; qu'ainsi les membres du Conseil ont été suffisamment informés pour se prononcer sur chaque candidature ; qu'il ressort du procès-verbal de la séance du Conseil du 8 décembre 2011 que, pour sélectionner les candidats, ont été pris en compte les critères de sélection suivants : " appréciations des notateurs et ancienneté dans le grade (...) capacités managériales et d'encadrement ; sens relationnel, qualités diplomatiques et aptitude des candidats à représenter la juridiction face aux élus et interlocuteurs institutionnels " ; que les candidats ont ainsi été sélectionnés au regard de critères précis, sans qu'il ressorte des pièces du dossier que, comme le soutiennent les requérants, ces critères auraient été ensuite modifiés ou appliqués de façon variable aux différents candidats ; que si trois candidats qui ne figuraient pas sur la liste établie l'année précédente ont été inscrits sur la liste d'aptitude pour l'année 2012, il ne ressort pas des  pièces du dossier qu'un tel choix serait entaché d'une erreur manifeste d'appréciation ; qu'enfin, la circonstance que les requérants aient formé un recours contre la liste d'aptitude est par elle-même sans incidence sur la légalité du décret attaqué ; qu'il résulte de ce qui précède que le moyen tiré, en ses différentes branches, de ce que l'irrégularité de la liste d'aptitude pour l'année 2012 entacherait d'illégalité le décret attaqué ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que l'avis du Conseil supérieur des chambres régionales des comptes ne lie pas l'autorité investie du pouvoir de nomination ; que si la nomination de M. E...en qualité de vice-président de la chambre régionale des comptes de Nord-Pas-de-Calais - Picardie a fait l'objet d'un avis défavorable du Conseil, il ne ressort pas des pièces du dossier que, compte tenu, d'une part, des attributions confiées aux  vice-présidents de chambres régionales des comptes et des conditions dans lesquelles ils exercent leurs fonctions et, d'autre part, des qualifications et de l'expérience de M.E..., la nomination de l'intéressé serait entachée d'une erreur manifeste d'appréciation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. I...et autres est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. G... I..., premier requérant dénommé, au Premier ministre, à MM. C...N..., L...J..., H...K...et M...E.... Les autres requérants seront informés de la présente décision par Me Foussard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée pour information à la Cour des comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
