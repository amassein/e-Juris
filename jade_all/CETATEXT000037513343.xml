<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037513343</ID>
<ANCIEN_ID>JG_L_2018_10_000000400779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/51/33/CETATEXT000037513343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 22/10/2018, 400779</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:400779.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de Corse-du-Sud a demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir la décision du 21 juillet 2013 par laquelle le maire de Serra-di-Ferro a tacitement délivré à M. A...B...un permis de construire pour l'édification d'une maison d'habitation individuelle au lieudit "Poliscono" à Serra-di-Ferro. Par un jugement n° 1301016 du 4 juillet 2014, le tribunal administratif a annulé le permis de construire attaqué. <br/>
<br/>
              Par un arrêt n° 14MA03957 du 18 avril 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juin et 20 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. B... a déposé le 22 mars 2013 une demande de permis de construire en vue de la construction d'une maison d'habitation et d'une piscine sur le territoire de la commune littorale de Serra-di-Ferro (Corse-du-Sud). Le 2 avril suivant, cette demande a été transmise par la commune aux services de la préfecture de Corse-du-Sud. Le 8 avril, la commune a indiqué au pétitionnaire qu'il convenait de compléter son dossier de demande par certaines pièces et l'a informé qu'un permis de construire tacite ne pourrait naître que dans un délai de deux mois à compter de la réception de ces pièces. L'intéressé a transmis ces pièces à la commune le 21 mai. Par arrêté du 24 juillet 2013, transmis au préfet de la Corse-du-Sud, le maire a pris un arrêté refusant expressément de délivrer le permis de construire sollicité. Lors d'une réunion du 18 septembre 2013, le maire a toutefois informé le préfet que M. B...pouvait prétendre être titulaire d'un permis tacite définitif, faute, à supposer que l'arrêté de refus du 24 juillet 2013 puisse être regardé comme valant retrait du permis tacite, que la commune ait procédé à une notification régulière de cet arrêté. Le 16 octobre 2013, le préfet a formé sans succès un recours gracieux en vue du retrait du permis de construire. Il a alors saisi le tribunal administratif de Bastia le 4 décembre 2013 d'un déféré aux fins d'annulation du permis. Par un jugement du 4 juillet 2014, le tribunal administratif de Bastia a annulé ce permis. Par un arrêt du 18 avril 2016, contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté l'appel de l'intéressé.  <br/>
<br/>
              Sur les moyens relatifs aux irrégularités qui auraient été commises par le tribunal administratif :  <br/>
<br/>
              2. D'une part, aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites  par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R.611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". L'article R. 611-10 du même code dispose que : " Sous l'autorité du président de la chambre à laquelle il appartient, le rapporteur fixe, eu égard aux circonstances de l'affaire, le délai accordé aux parties pour produire leurs mémoires. Il peut demander aux parties, pour être jointes à la procédure contradictoire, toutes pièces ou tous documents utiles à la solution du litige ". Le requérant a soutenu devant la cour que le jugement du tribunal administratif était irrégulier au motif que le tribunal s'était fondé sur des pièces transmises, à la demande de ce dernier, par le préfet ainsi que sur un mémoire qui ne lui avaient pas été communiqués. La cour a toutefois relevé que le tribunal administratif ne s'était fondé que sur les pièces produites à l'appui du déféré déposé le 4 décembre 2013 et communiquées au pétitionnaire et qu'il ne s'était, en revanche, pas fondé sur les éléments du mémoire, non communiqué, du préfet du 17 juin 2014. En déduisant de ces constatations que le tribunal administratif n'avait pas méconnu le principe du caractère contradictoire de la procédure juridictionnelle, la cour n'a entaché son arrêt d'aucune erreur de droit au regard des dispositions ci-dessus rappelées du code de justice administrative.<br/>
<br/>
              3. D'autre part, le requérant a soutenu devant la cour que le tribunal administratif avait irrégulièrement soulevé d'office le moyen, qui n'était pas d'ordre public, tiré de ce que le dossier de demande de permis de construire transmis au préfet n'était pas complet. La cour a relevé, pour écarter ce moyen, que le tribunal administratif s'était, en réalité, borné à répondre à la fin de non-recevoir soulevée devant lui par M. B...et tirée de la tardiveté du déféré du préfet, en recherchant, pour déterminer le point de départ du délai applicable, si l'entier dossier de demande de permis de construire avait bien été transmis. En se prononçant ainsi, la cour a suffisamment motivé son arrêt et n'a pas commis d'erreur de droit. <br/>
<br/>
              Sur les moyens relatifs à la recevabilité du déféré préfectoral :<br/>
<br/>
              4. D'une part, aux termes de l'article L. 2131-6 du code général des collectivités territoriales : " Le représentant de l'Etat dans le département défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission (...) ". Parmi les actes mentionnés par l'article L. 2131-2 de ce code figure, au 6° : " Le permis de construire et les autres autorisations d'utilisation du sol et le certificat d'urbanisme délivrés par le maire ". L'article R. 423-23 du code de l'urbanisme fixe à deux mois le délai d'instruction de droit commun pour les demandes de permis de construire portant sur une maison individuelle. L'article R. 424-1 du même code prévoit que, à défaut d'une décision expresse dans le délai d'instruction, le silence gardé par l'autorité compétente vaut permis de construire. Aux termes de l'article L. 424-8 du code de l'urbanisme : " Le permis tacite et la décision de non-opposition à une déclaration préalable sont exécutoires à compter de la date à laquelle ils sont acquis ". L'article R. 423-7 du même code dispose que : " Lorsque l'autorité compétente pour délivrer le permis ou pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est le maire au nom de la commune, celui-ci transmet un exemplaire de la demande ou de la déclaration préalable au préfet dans la semaine qui suit le dépôt ". D'autre part, aux termes de l'article R. 423-38 du même code : " Lorsque le dossier ne comprend pas les pièces exigées (...), l'autorité compétente, dans le délai d'un mois à compter de la réception ou du dépôt du dossier en mairie, adresse au demandeur (...) une lettre recommandée avec demande d'avis de réception (...) indiquant, de façon exhaustive les pièces manquantes ".<br/>
<br/>
              5. S'il résulte des dispositions de l'article L. 424-8 du code de l'urbanisme rappelées ci-dessus qu'un permis de construire tacite est exécutoire dès qu'il est acquis, sans qu'il y ait lieu de rechercher s'il a été transmis au représentant de l'Etat, les dispositions de cet article ne dérogent pas à celles de l'article L. 2131-6 du code général des collectivités territoriales, en vertu desquelles le préfet défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission. Figurent au nombre de ces actes les permis de construire tacites. Une commune doit être réputée avoir satisfait à l'obligation de transmission, dans le cas d'un permis de construire tacite, si elle a transmis au préfet l'entier dossier de demande, en application de l'article R. 423-7 du code de l'urbanisme. Le délai du déféré court alors à compter de la date à laquelle le permis est acquis ou, dans l'hypothèse où la commune ne satisfait à l'obligation de transmission que postérieurement à cette date, à compter de la date de cette transmission. Lorsque, en application de l'article R. 423-38 du même code, la commune invite le pétitionnaire à compléter son dossier de demande, la transmission au préfet de l'entier dossier implique que la commune lui transmette les pièces complémentaires éventuellement reçues en réponse à cette invitation.<br/>
<br/>
              6. En premier lieu, il résulte de ce qui précède qu'en jugeant que la commune n'avait pas transmis au préfet l'entier dossier de demande faute de lui avoir adressé les pièces complémentaires reçues du demandeur en réponse à l'invitation qui lui avait été faite de compléter ce dossier et en en déduisant que cette transmission incomplète avait fait obstacle au déclenchement du délai du déféré à la date de naissance du permis tacite, de sorte que le déféré n'était pas tardif, la cour, qui a porté sur les pièces du dossier une appréciation souveraine exempte de dénaturation, n'a pas commis d'erreur de droit. En ne recherchant pas si les pièces manquantes étaient nécessaires à l'exercice du contrôle de légalité ni si le préfet pouvait les demander de sa propre initiative, la cour, qui a suffisamment motivé son arrêt eu égard à l'argumentation dont elle était saisie, n'a pas commis d'erreur de droit dès lors que ces circonstances sont sans incidence sur l'obligation pesant sur la commune de transmettre au préfet l'entier dossier de demande. <br/>
<br/>
              7. En deuxième lieu, il ressort des énonciations de l'arrêt attaqué que si la cour a relevé que la lettre du 8 avril 2013 par laquelle les services de la commune ont demandé au pétitionnaire les pièces manquantes, qui a eu pour conséquence de modifier le délai d'instruction de la demande et le délai à l'issue duquel est né le permis tacite, n'avait pas été transmise au préfet, elle s'est fondée, pour juger recevable le déféré du préfet, ainsi qu'il a été dit, sur le caractère incomplet du dossier de demande de permis de construire transmis au préfet. Par suite, les moyens  tirés de ce que la cour aurait méconnu, au prix d'une erreur de droit, les dispositions des articles R. 423-38, R. 423-39 et R. 423-42 du code de l'urbanisme en jugeant que le maire était tenu de transmettre cette lettre et entaché son arrêt sur ce point d'insuffisance de motivation doivent, en tout état de cause, être écartés.<br/>
<br/>
              8. En troisième et dernier lieu, en relevant que la seule mention " vu les pièces fournies en date du 21 mai 2013 " figurant dans les visas de l'arrêté du 24 juillet 2013, qui était dépourvue de toute précision, ne pouvait valablement permettre au préfet de constater l'existence d'un permis de construire tacite, la cour n'a, en tout état de cause, pas entaché son arrêt d'une erreur de qualification juridique des faits de l'espèce.<br/>
<br/>
              Sur les moyens relatifs à l'application des dispositions du code de l'urbanisme relatives à la protection du littoral :<br/>
<br/>
              9. En vertu des dispositions du premier alinéa du I de l'article L. 146-4 du code de l'urbanisme, dans sa rédaction en vigueur : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 3 janvier 1986 dont elles sont issues, que les constructions peuvent être autorisées dans les communes littorales en continuité avec les agglomérations et villages existants, c'est-à-dire avec les zones déjà urbanisées caractérisées par un nombre et une densité significatifs de constructions, mais qu'aucune construction ne peut, en revanche, être autorisée, même en continuité avec d'autres, dans les zones d'urbanisation diffuse éloignées de ces agglomérations et villages. Il ressort des pièces du dossier soumis aux juges du fond qu'en jugeant que le terrain d'assiette du projet litigieux, qui est situé dans la continuité d'une vingtaine de constructions, ne peut, eu égard à l'implantation diffuse de ces dernières, être regardé comme se situant dans la continuité d'un centre urbain existant, au sens des dispositions précitées du I de l'article L. 146-4 du code de l'urbanisme, la cour, qui a suffisamment motivé son arrêt sur ce point, a porté une appréciation souveraine sur les pièces du dossier, qui est exempte de dénaturation. <br/>
<br/>
              10. Il résulte de tout ce qui précède que le pourvoi de M. B...doit être rejeté. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que M. B...demande au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
Copie en sera adressée à la commune de Serra-di-Ferro.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-015-02-02 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. CONTRÔLE DE LA LÉGALITÉ DES ACTES DES AUTORITÉS LOCALES. DÉFÉRÉ PRÉFECTORAL. DÉLAI DU DÉFÉRÉ. - PERMIS DE CONSTRUIRE TACITE - POINT DE DÉPART DU DÉLAI DU DÉFÉRÉ - DATE À LAQUELLE LE PERMIS EST ACQUIS OU DATE DE LA TRANSMISSION DE L'ENTIER DOSSIER, ET NOTAMMENT DES PIÈCES COMPLÉMENTAIRES REÇUES LE CAS ÉCHÉANT, SI ELLE EST POSTÉRIEURE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. - PERMIS DE CONSTRUIRE TACITE - 1) OBLIGATION DE TRANSMISSION AU REPRÉSENTANT DE L'ETAT RÉPUTÉE SATISFAITE DÈS TRANSMISSION DE L'ENTIER DOSSIER DE LA DEMANDE [RJ1], ET NOTAMMENT DES PIÈCES COMPLÉMENTAIRES REÇUES LE CAS ÉCHÉANT - POINT DE DÉPART DU DÉLAI DU DÉFÉRÉ PRÉFECTORAL - 2) DATE À LAQUELLE LE PERMIS EST ACQUIS OU DATE DE LA TRANSMISSION DE L'ENTIER DOSSIER SI ELLE EST POSTÉRIEURE [RJ1].
</SCT>
<ANA ID="9A"> 135-01-015-02-02 S'il résulte des dispositions de l'article L. 424-8 du code de l'urbanisme qu'un permis de construire tacite est exécutoire dès qu'il est acquis, sans qu'il y ait lieu de rechercher s'il a été transmis au représentant de l'Etat, les dispositions de cet article ne dérogent pas à celles de l'article L. 2131-6 du code général des collectivités territoriales (CGCT), en vertu desquelles le préfet défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission. Figurent au nombre de ces actes les permis de construire tacites. Une commune doit être réputée avoir satisfait à l'obligation de transmission, dans le cas d'un permis de construire tacite, si elle a transmis au préfet l'entier dossier de demande, en application de l'article R. 423-7 du code de l'urbanisme. Le délai du déféré court alors à compter de la date à laquelle le permis est acquis ou, dans l'hypothèse où la commune ne satisfait à l'obligation de transmission que postérieurement à cette date, à compter de la date de cette transmission. Lorsque, en application de l'article R. 423-38 du même code, la commune invite le pétitionnaire à compléter son dossier de demande, la transmission au préfet de l'entier dossier implique que la commune lui transmette les pièces complémentaires éventuellement reçues en réponse à cette invitation.</ANA>
<ANA ID="9B"> 68-03-02 1) S'il résulte  de l'article L. 424-8 du code de l'urbanisme qu'un permis de construire tacite est exécutoire dès qu'il est acquis, sans qu'il y ait lieu de rechercher s'il a été transmis au représentant de l'Etat, les dispositions de cet article ne dérogent pas à celles de l'article L. 2131-6 du code général des collectivités territoriales (CGCT), en vertu desquelles le préfet défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission. Figurent au nombre de ces actes les permis de construire tacites. Une commune doit être réputée avoir satisfait à l'obligation de transmission, dans le cas d'un permis de construire tacite, si elle a transmis au préfet l'entier dossier de demande, en application de l'article R. 423-7 du code de l'urbanisme.... ,,2) Le délai du déféré court alors à compter de la date à laquelle le permis est acquis ou, dans l'hypothèse où la commune ne satisfait à l'obligation de transmission que postérieurement à cette date, à compter de la date de cette transmission. Lorsque, en application de l'article R. 423-38 du même code, la commune invite le pétitionnaire à compléter son dossier de demande, la transmission au préfet de l'entier dossier implique que la commune lui transmette les pièces complémentaires éventuellement reçues en réponse à cette invitation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 décembre 2014, Ministre de l'égalité des territoires et du logement c/ Commune de Mollans-sur-Ouvèze, n° 373681, T. pp. 536-537-904. Rappr., pour une décision de non-opposition à déclaration préalable, CE, 23 octobre 2013, SARL Prestig'Immo, n° 344454, T. pp. 457-881.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
