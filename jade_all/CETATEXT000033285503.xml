<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285503</ID>
<ANCIEN_ID>JG_L_2016_10_000000401242</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/55/CETATEXT000033285503.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/10/2016, 401242</TITRE>
<DATE_DEC>2016-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401242</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401242.20161019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Nouvelle-Calédonie de condamner l'Etat à leur verser la somme de 32 768 970 F CFP en réparation des préjudices subis du fait de leur évacuation des terres de la tribu d'Unia le 14 juillet 2010.<br/>
<br/>
              Par un jugement n° 1400384 du 17 septembre 2015, le tribunal administratif de Nouvelle-Calédonie a condamné l'Etat à leur verser la somme de 10 600 000 F CFP.<br/>
<br/>
              Par un arrêt n° 15PA04600 du 20 juin 2016, la cour administrative d'appel de Paris a rejeté la demande de sursis à exécution de ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 6 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la ministre des outre-mer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au titre du sursis à exécution, de surseoir à l'exécution du jugement n° 1400384 du 17 septembre 2015.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la cour administrative d'appel de Paris que, par un jugement du 17 septembre 2015, le tribunal administratif de Nouvelle-Calédonie a condamné l'Etat, sur le fondement de la responsabilité sans faute, à verser à M. et Mme A...la somme de 10 600 000 F CFP en réparation des préjudices subis par ceux-ci du fait de leur évacuation, avec d'autres membres du clanA..., des terres de la tribu d'Unia le 14 juillet 2010.<br/>
<br/>
              2. Aux termes de l'article R. 811-16 du code de justice administrative : " Lorsqu'il est fait appel par une personne autre que le demandeur en première instance, la juridiction peut, à la demande de l'appelant, ordonner sous réserve des dispositions des articles R. 533-2 et R. 541-6 qu'il soit sursis à l'exécution du jugement déféré si cette exécution risque d'exposer l'appelant à la perte définitive d'une somme qui ne devrait pas rester à sa charge dans le cas où ses conclusions d'appel seraient accueillies. ". Aux termes de l'article R. 811-17 du même code : " Dans les autres cas, le sursis peut être ordonné à la demande du requérant si l'exécution de la décision de première instance attaquée risque d'entraîner des conséquences difficilement réparables et si les moyens énoncés dans la requête paraissent sérieux en l'état de l'instruction. ".<br/>
<br/>
              3. Pour rejeter les conclusions à fin de sursis à exécution présentées par le ministre des outre-mer sur le fondement à la fois de l'article R. 811-16 et de l'article R. 811-17 du code de justice administrative, la cour administrative d'appel a considéré qu'il ne résultait pas de l'instruction que l'exécution du jugement du 17 septembre 2015 risquerait d'exposer l'Etat à la perte définitive d'une somme qui ne devrait pas rester à sa charge ni qu'elle risquerait d'entraîner des conséquences difficilement réparables au cas où ses conclusions tendant à la réformation, en appel, du jugement seraient accueillies. En relevant que le ministre s'est borné à faire état du montant élevé de la condamnation de première instance et à soutenir de manière générale que certains membres du clanA..., auquel appartiennent les requérants, seraient insolvables et n'auraient plus d'adresse fixe, la cour ne s'est pas méprise sur la portée des écritures dont elle était saisie. En estimant que ces seuls éléments ne permettaient pas de considérer que les conditions posées par les articles R. 811-16 et R. 811-17 du code de justice administrative étaient remplies, pour ce qui concerne les requérants, la cour n'a pas commis d'erreur de droit et s'est livrée à une appréciation souveraine des faits qui lui étaient soumis exempte de dénaturation.<br/>
<br/>
              4. Si le ministre soutient également que la cour a commis une erreur de droit en ne tenant pas compte, pour l'application de l'article R. 811-17 précité, de l'impossibilité de récupérer les intérêts légaux dus à raison du retard mis par l'Etat à exécuter le jugement de première instance, en cas de restitution de la somme versée en exécution de ce jugement dans l'hypothèse où il serait annulé en appel, de telles conséquences ne résulteraient pas de l'exécution du jugement de première instance mais du retard mis à l'exécuter. Dès lors, et en tout état de cause, elles ne sont pas de celles qui sont susceptibles d'être invoquées à l'appui de conclusions tendant au sursis à exécution d'un jugement. <br/>
<br/>
              5. Il résulte des dispositions citées au point 2 que le sursis à exécution ne peut être ordonné sur le fondement de l'article R. 811-17 du code de justice administrative qu'à la double condition que l'exécution de la décision attaquée risque d'entraîner des conséquences difficilement réparables et que les moyens énoncés dans la requête paraissent sérieux en l'état de l'instruction. La cour ayant retenu que la première condition énoncée à cet article n'était pas remplie, elle a pu, sans erreur de droit, rejeter la demande sans se prononcer sur le caractère sérieux des moyens énoncés dans la requête présentée devant elle.<br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de la ministre des outre-mer doit être rejeté.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la ministre des outre-mer est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la ministre des outre-mer. <br/>
Copie en sera adressée à M. et Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
