<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043878477</ID>
<ANCIEN_ID>JG_L_2021_07_000000437847</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/84/CETATEXT000043878477.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/07/2021, 437847, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437847</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437847.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés le 21 janvier 2020 et les 11 mai et 7 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société Coriolis Télécom demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP) rejetant sa demande de mise en demeure et de sanction à l'encontre de la société THD Bretagne ;<br/>
<br/>
              2°) d'enjoindre à l'ARCEP de mettre en demeure la société THD Bretagne de formuler une offre d'accès dans le respect des conditions tarifaires conforme aux lignes directrices de l'ARCEP de décembre 2015, sous astreinte journalière de 1 000 euros par jour de retard à compter de l'expiration d'un délai de trois mois à partir de la notification de la décision du Conseil d'Etat, sur le fondement des articles L. 911-1 et suivants du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société THD Bretagne la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des postes et des communications électroniques ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Briard, avocat de la société Coriolis Télécom ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 36-11 du code des postes et des communications électroniques, dans sa version applicable au litige : " L'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse peut, soit d'office, soit à la demande du ministre chargé des communications électroniques d'une collectivité territoriale ou d'un groupement de collectivités territoriales, d'une organisation professionnelle, d'une association agréée d'utilisateurs ou d'une personne physique ou morale concernée, sanctionner les manquements qu'elle constate de la part des exploitants de réseau, des fournisseurs de services de communications électroniques, des fournisseurs de services de communication au public en ligne ou des gestionnaires d'infrastructures d'accueil. Ce pouvoir de sanction est exercé dans les conditions suivantes : / I. - En cas de manquement par un exploitant de réseau, par un fournisseur de services de communications électroniques, un fournisseur de services de communication au public en ligne ou un gestionnaire d'infrastructures d'accueil : / - aux dispositions législatives et réglementaires au respect desquelles l'Autorité a pour mission de veiller ou aux textes et décisions pris en application de ces dispositions ; (...) l'exploitant, le fournisseur ou le gestionnaire est mis en demeure par l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse de s'y conformer dans un délai qu'elle détermine. " Aux termes du I. de l'article L. 36-8 du même code : " En cas de refus d'accès ou d'interconnexion, d'échec des négociations commerciales ou de désaccord sur la conclusion ou l'exécution d'une convention d'interconnexion ou d'accès à un réseau de communications électroniques, l'Autorité de régulation des communications électroniques et des postes peut être saisie du différend par l'une des parties (...) Sa décision est motivée et précise les conditions équitables, d'ordre technique et financier, dans lesquelles l'interconnexion ou l'accès doivent être assurés. " <br/>
<br/>
              2.	L'article L. 1425-1 du code général des collectivités territoriales dispose : " I. - Pour l'établissement et l'exploitation d'un réseau, les collectivités territoriales et leurs groupements, (...) peuvent, (...) établir et exploiter sur leur territoire des infrastructures et des réseaux de communications électroniques, au sens des 3° et 15° de l'article L. 32 du code des postes et des communications électroniques. (...) / Une collectivité territoriale ou un groupement de collectivités territoriales peut déléguer à un syndicat mixte incluant au moins une région ou un département tout ou partie de la compétence relative à un ou plusieurs réseaux de communications électroniques (...) /. III. - L'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse est saisie, dans les conditions définies à l'article L. 36-8 du code des postes et communications électroniques, de tout différend relatif aux conditions techniques et tarifaires d'exercice d'une activité d'opérateur de communications électroniques ou d'établissement, de mise à disposition ou de partage des réseaux et infrastructures de communications électroniques visés au I. (...) VI. - Les collectivités territoriales et leurs groupements permettent l'accès des opérateurs de communications électroniques aux infrastructures et aux réseaux de communications électroniques mentionnés au premier alinéa du I, dans des conditions tarifaires objectives, transparentes, non discriminatoires et proportionnées et qui garantissent le respect du principe de libre concurrence sur les marchés des communications électroniques ainsi que le caractère ouvert de ces infrastructures et de ces réseaux. (...) Après consultation publique, l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse adopte des lignes directrices portant sur les conditions tarifaires d'accès aux réseaux ouverts au public à très haut débit en fibre optique permettant de desservir un utilisateur final. Elles sont mises à jour en tant que de besoin. (...) / Les collectivités territoriales et leurs groupements mentionnés au premier alinéa du I communiquent à l'autorité, au moins deux mois avant leur entrée en vigueur, les conditions tarifaires d'accès à leurs réseaux à très haut débit en fibre optique ouverts au public permettant de desservir un utilisateur final. (...) Lorsqu'elle estime que les conditions tarifaires soulèvent des difficultés au regard du présent VI, l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse émet un avis, qui peut être rendu public, invitant la collectivité territoriale ou le groupement concerné à les modifier. Elle le communique sans délai au ministre chargé des communications électroniques ".<br/>
<br/>
              3.	Il ressort des pièces du dossier que la société THD Bretagne a été chargée par le syndicat mixte Mégalis Bretagne, maître d'ouvrage du réseau d'initiative publique (RIP) Bretagne fibre optique créé sur le fondement de l'article L. 1425-1 du code général des collectivités territoriales, de la gestion technique et commerciale de ce réseau. Par une décision n° 2018-1560-RDPI du 11 décembre 2018, l'ARCEP, réglant, sur le fondement de l'article L. 36-8 du code des postes et communications électroniques, un différend entre la société Coriolis Télécom et la société THD Bretagne relatif à l'accès à ce réseau, a enjoint à cette dernière de proposer à la société Coriolis Télécom une offre d'accès de gros précisant les conditions techniques et tarifaires. La société Coriolis Télécom a demandé, le 20 juin 2019, à l'ARCEP de mettre en demeure et de sanctionner la société THD Bretagne pour non-respect de la décision du 11 décembre 2018, sur le fondement de l'article L. 36-11 du code des postes et des communications électroniques, au motif allégué que la décision du 11 décembre 2018 imposait implicitement mais nécessairement à la société THD Bretagne de proposer des conditions tarifaires conformes aux " lignes directrices relatives à la tarification de l'accès aux réseaux à très haut débit en fibre optique déployés par l'initiative publique ", édictées par l'ARCEP en décembre 2015. La société Coriolis Télécom demande l'annulation de la décision implicite de rejet opposée à sa demande. <br/>
<br/>
              4.	En premier lieu, aux termes de l'article D. 595 du code des postes et des communications électroniques : " Au vu du dossier d'instruction, l'Autorité, après en avoir délibéré en formation de règlement des différends, de poursuite et d'instruction, peut mettre en demeure la personne en cause : (...) 2° En cas de manquement aux dispositions mentionnées au I de l'article L. 36-11 (...) ". La décision implicite rejetant la demande, adressée par la société Coriolis Télécom à l'ARCEP et tendant à ce que soit adressée une mise en demeure et que soit prise une sanction à l'égard de la société THD Bretagne, doit être regardée comme ayant été prise par la formation de règlement des différends, de poursuite et d'instruction, compétente pour en connaître, à l'issue du délai de deux mois courant à compter de sa réception par l'Autorité. Par suite, le moyen tiré de ce que la décision litigieuse serait entachée d'incompétence ne peut qu'être écarté.<br/>
<br/>
              5.	En second lieu, les " lignes directrices relatives à la tarification de l'accès aux réseaux à très haut débit en fibre optique déployés par l'initiative publique ", édictées par l'ARCEP en décembre 2015 au titre du VI de l'article L. 1425-1 du code général des collectivités territoriales, ont pour seul objet de guider l'action des collectivités territoriales et de leurs groupements en exposant une méthode d'élaboration des niveaux tarifaires pouvant être proposés aux opérateurs commerciaux et ne fixent pas, contrairement à ce que soutient la société requérante, de norme à caractère général s'imposant aux collectivités territoriales. Par suite, la société Coriolis Télécom n'est pas fondée à soutenir que la décision de règlement de différend du 11 décembre 2018 aurait eu pour effet, implicitement mais nécessairement, d'imposer à la société THD Bretagne de lui proposer un niveau déterminé de conditions tarifaires conformes à ces lignes directrices, un échec des négociations commerciales entre les deux sociétés sur ces tarifs pouvant, le cas échéant, justifier une nouvelle demande de règlement des différends en application des dispositions de l'article L. 36-8 du code des postes et des communications électroniques. Dès lors, le moyen tiré de ce que l'ARCEP aurait été tenue de mettre en demeure et de sanctionner la société THD Bretagne pour ne pas avoir respecté la décision n° 2018-1560-RDPI du 11 décembre 2018 ne peut qu'être écarté. <br/>
<br/>
              6.	Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par la société THD Bretagne, que la requête de la société Coriolis Télécom doit être rejetée, y compris, par voie de conséquence, ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu en revanche de mettre à la charge de la société Coriolis Télécom la somme de 3 000 euros à verser à ce titre à la société THD Bretagne.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société Coriolis Télécom est rejetée.<br/>
Article 2 : La société Coriolis Télécom versera à la société THD Bretagne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société Coriolis Télécom, à la société THD Bretagne et à l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
