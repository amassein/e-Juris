<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815796</ID>
<ANCIEN_ID>JG_L_2019_07_000000417902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815796.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/07/2019, 417902</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:417902.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... a demandé au tribunal administratif de Lyon d'annuler la décision du 7 décembre 2012 par laquelle le directeur des Hospices civils de Lyon a prononcé sa mise à la retraite d'office pour invalidité, d'enjoindre à cet établissement de la replacer dans la situation administrative dans laquelle elle se trouvait avant l'intervention de la décision contestée, de se prononcer sur l'imputabilité au service de sa maladie et de procéder à son reclassement sur un emploi adapté à son état de santé, dans un délai d'un mois à compter de l'annulation de la décision, sous astreinte de 50 euros par jour de retard et de mettre à la charge des Hospices civils de Lyon la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. Par un jugement n° 1301198 du 2 décembre 2015, le tribunal administratif de Lyon a partiellement fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 16LY00588 du 5 décembre 2017, la cour administrative d'appel de Lyon a, sur appel des Hospices civils de Lyon, annulé ce jugement et rejeté la demande présentée par Mme A... devant le tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 7 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt.<br/>
<br/>
              2°) de mettre à la charge des Hospices civils de Lyon  la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
              - le décret n° 88-386 du 19 avril 1988 ;<br/>
              - le décret n° 89-376 du 8 juin 1989 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de Mme A... et à la SCP Ohl, Vexliard, avocat des Hospices civils de Lyon.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme A... a demandé au tribunal administratif de Lyon d'annuler la décision du 7 décembre 2012 par laquelle le directeur des Hospices civils de Lyon a prononcé sa mise à la retraite d'office pour invalidité, d'enjoindre à cet établissement de la replacer dans la situation administrative dans laquelle elle se trouvait avant l'intervention de cette décision, de se prononcer sur l'imputabilité au service de sa maladie et de procéder à son reclassement sur un emploi adapté à son état de santé. Par un jugement du 2 décembre 2015, le tribunal administratif a fait droit à sa demande. Mme A... se pourvoit en cassation contre l'arrêt du 5 décembre 2017 par lequel la cour administrative d'appel de Lyon a, sur appel des Hospices civils de Lyon, annulé ce jugement et rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 16 de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière : " La commission de réforme doit être saisie de tous témoignages, rapports et constatations propres à éclairer son avis.  Elle peut faire procéder à toutes mesures d'instructions, enquêtes et expertises qu'elle estime nécessaires. Dix jours au moins avant la réunion de la commission, le fonctionnaire est invité à prendre connaissance, personnellement ou par l'intermédiaire de son représentant, de son dossier, dont la partie médicale peut lui être communiquée, sur sa demande, ou par l'intermédiaire d'un médecin ; il peut présenter des observations écrites et fournir des certificats médicaux. La commission entend le fonctionnaire, qui peut se faire assister d'un médecin de son choix. Il peut aussi se faire assister par un conseiller ". En vertu des dispositions de l'article 3 du même arrêté, la commission de réforme comprend " 1. Deux praticiens de médecine générale, auxquels est adjoint, s'il y a lieu, pour l'examen des cas relevant de sa compétence, un médecin spécialiste qui participe aux débats mais ne prend pas part aux votes [...] ".<br/>
<br/>
              3. Il résulte des dispositions précitées que, dans le cas où il est manifeste, eu égard aux éléments dont dispose la commission de réforme, que la présence d'un médecin spécialiste de la pathologie invoquée est nécessaire pour éclairer l'examen du cas du fonctionnaire, l'absence d'un tel spécialiste est susceptible de priver l'intéressé d'une garantie et d'entacher ainsi la procédure devant la commission d'une irrégularité justifiant l'annulation de la décision attaquée. Par suite, en s'abstenant de rechercher s'il ressortait manifestement des éléments dont elle disposait que la présence d'un médecin spécialiste en neurologie était nécessaire lors du passage de Mme A... devant la commission de réforme, la cour administrative d'appel a entaché son arrêt d'une erreur de droit qui en justifie l'annulation, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des Hospices civils de Lyon la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par les Hospices civils de Lyon au même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 5 décembre 2017 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les Hospices civils de Lyon verseront à Mme A... une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions des Hospices civils de Lyon présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A... et aux Hospices civils de Lyon.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-04 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS MÉDICAUX. - COMMISSIONS DE RÉFORME DES AGENTS DE LA FONCTION PUBLIQUE TERRITORIALE ET DE LA FONCTION PUBLIQUE HOSPITALIÈRE - PRÉSENCE D'UN MÉDECIN SPÉCIALISTE DE LA PATHOLOGIE INVOQUÉE, DANS LES CAS OÙ IL EST MANIFESTE QU'ELLE EST NÉCESSAIRE POUR ÉCLAIRER L'EXAMEN DU CAS DE L'AGENT - CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. - FONCTION PUBLIQUE TERRITORIALE ET FONCTION PUBLIQUE HOSPITALIÈRE - CONSULTATION DE LA COMMISSION DE RÉFORME - PRÉSENCE D'UN MÉDECIN SPÉCIALISTE DE LA PATHOLOGIE INVOQUÉE, DANS LES CAS OÙ IL EST MANIFESTE QU'ELLE EST NÉCESSAIRE POUR ÉCLAIRER L'EXAMEN DU CAS DE L'AGENT - CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-07-04 Il résulte des articles 3 et 16 de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière que, dans les cas où il est manifeste, au vu des éléments dont dispose la commission de réforme, que la présence d'un médecin spécialiste de la pathologie invoquée par un agent est nécessaire pour éclairer l'examen de son cas,  l'absence d'un tel spécialiste doit être regardée comme privant l'intéressé d'une garantie et comme entachant la procédure devant la commission d'une irrégularité justifiant l'annulation de la décision attaquée.</ANA>
<ANA ID="9B"> 36-07-10 Il résulte des articles 3 et 16 de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière que, dans les cas où il est manifeste, au vu des éléments dont dispose la commission de réforme, que la présence d'un médecin spécialiste de la pathologie invoquée par un agent est nécessaire pour éclairer l'examen de son cas,  l'absence d'un tel spécialiste doit être regardée comme privant l'intéressé d'une garantie et comme entachant la procédure devant la commission d'une irrégularité justifiant l'annulation de la décision attaquée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, M. Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
