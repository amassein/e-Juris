<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028681378</ID>
<ANCIEN_ID>JG_L_2014_03_000000362227</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/13/CETATEXT000028681378.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 03/03/2014, 362227, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362227</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362227.20140303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'association Forum citoyen pour la responsabilité sociale des entreprises, dont le siège est 4, rue Jean Lantier à Paris (75001), représentée par son président ; l'association Forum citoyen pour la responsabilité sociale des entreprises demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur son recours gracieux tendant à la modification du décret n° 2012-557 du 24 avril 2012 relatif aux obligations de transparence des entreprises en matière sociale et environnementale, ainsi que ce décret ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu la loi n° 2010-788 du 12 juillet 2010 ;<br/>
<br/>
              Vu la loi n° 2010-1249 du 22 octobre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>	Sur la légalité externe du décret attaqué :<br/>
<br/>
              	1. Considérant que le décret attaqué n'est pas, en raison de son caractère réglementaire, au nombre des décisions administratives dont la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public exige l'énoncé des motifs de droit et de fait sur lesquels il est fondé ; que, par suite, le moyen tiré de la méconnaissance de l'obligation de motivation prévue par cette loi doit être écarté ; <br/>
<br/>
              	Sur la légalité interne du décret attaqué :<br/>
<br/>
              	2. Considérant, en premier lieu, qu'aux termes du cinquième alinéa de l'article L. 225-102-1 du code de commerce, le rapport annuel présenté par le conseil d'administration ou le directoire d'une société à son assemblée générale en vertu de l'article         L. 225-102 du même code " comprend (...) des informations sur la manière dont la société prend en compte les conséquences sociales et environnementales de son activité ainsi que sur ses engagements sociétaux en faveur du développement durable et en faveur de la lutte contre les discriminations et de la promotion des diversités. Un décret en Conseil d'Etat établit deux listes précisant les informations visées au présent alinéa ainsi que les modalités de leur présentation, de façon à permettre une comparaison des données, selon que la société est ou non admise aux négociations sur un marché réglementé." ; qu'il résulte de ces dispositions qu'en insérant, à l'article R. 225-105-1 du même code, des dispositions prévoyant deux listes précisant les informations requises selon que la société est ou non admise aux négociations sur un marché réglementé, le décret attaqué n'a pas, contrairement à ce qui est soutenu, instauré une distinction illégale entre différentes catégories de sociétés ; <br/>
<br/>
              	3. Considérant, en deuxième lieu, qu'en vertu des dispositions du I de l'article R. 225-105-1 du code de commerce issues du décret attaqué, l'ensemble des sociétés auxquelles ce décret s'applique mentionnent, dans leur rapport annuel, les informations sociales suivantes : " a) Emploi : / - l'effectif total et la répartition des salariés par sexe, par âge et par zone géographique ; / - les embauches et les licenciements ; / - les rémunérations et leur évolution ; / b) Organisation du travail : / - l'organisation du temps de travail ; / c) Relations sociales : / - l'organisation du dialogue social, notamment les procédures d'information et de consultation du personnel et de négociation avec celui-ci ; / - le bilan des accords collectifs ; / d) Santé et sécurité : / - les conditions de santé et de sécurité au travail ; / - le bilan des accords signés avec les organisations syndicales ou les représentants du personnel en matière de santé et de sécurité au travail ; / e) Formation : / - les politiques mises en oeuvre en matière de formation ; / - le nombre total d'heures de formation ; / f) Egalité de traitement : / - les mesures prises en faveur de l'égalité entre les femmes et les hommes ; / - les mesures prises en faveur de l'emploi et de l'insertion des personnes handicapées ; / - la politique de lutte contre les discriminations " ; qu'en vertu du II du même article, les sociétés dont les titres sont admis aux négociations sur un marché réglementé mentionnent également, dans leur rapport annuel, les informations sociales suivantes : " b) Organisation du travail : / - l'absentéisme ; / d) Santé et sécurité : / - les accidents du travail, notamment leur fréquence et leur gravité, ainsi que les maladies professionnelles ; / g) Promotion et respect des stipulations des conventions fondamentales de l'Organisation internationale du travail relatives : / - au respect de la liberté d'association et du droit de négociation collective ; / - à l'élimination des discriminations en matière d'emploi et de profession ; / - à l'élimination du travail forcé ou obligatoire ; / - à l'abolition effective du travail des enfants " ; que, contrairement à ce qui est soutenu, les dispositions législatives citées au point 2 n'imposaient pas au pouvoir réglementaire de prévoir la présence dans le rapport annuel d'autres informations que celles, au demeurant diverses et pertinentes, qui viennent d'être mentionnées ; que le moyen tiré du caractère insuffisant de ces informations ne peut qu'être écarté ; <br/>
<br/>
              	4. Considérant, en troisième lieu, qu'aux termes du sixième alinéa de l'article L. 225-102-1 du code de commerce, dans sa rédaction issue de l'article 225 de la loi du 12 juillet 2010 portant engagement national pour l'environnement : " Les institutions représentatives du personnel et les parties prenantes participant à des dialogues avec les entreprises peuvent présenter leur avis sur les démarches de responsabilité sociale, environnementale et sociétale des entreprises en complément des indicateurs présentés " ; que ces dispositions ont été abrogées par les dispositions de l'article 32 de la loi du 22 octobre 2010 de régulation bancaire et financière, entrées en vigueur le 24 octobre 2010 ; que, dès lors, l'association requérante n'est pas fondée à soutenir que le décret attaqué aurait méconnu les dispositions de l'article L. 225-102-1 du code de commerce précitées en ne prévoyant pas un dispositif destiné à en assurer la mise en oeuvre ; <br/>
<br/>
              	5. Considérant, en quatrième lieu, qu'aux termes du septième alinéa de l'article L. 225-102-1 du code de commerce : " Les informations sociales et environnementales figurant ou devant figurer au regard des obligations légales et réglementaires font l'objet d'une vérification par un organisme tiers indépendant, selon des modalités fixées par décret en Conseil d'Etat. (...) " ;  qu'aux termes du I de l'article R. 225-105-2 du même code, issu du décret attaqué : " L'organisme tiers indépendant appelé à vérifier, en application du septième alinéa de l'article L. 225-102-1, les informations devant figurer, en vertu de son cinquième alinéa, dans le rapport présenté par le conseil d'administration ou le directoire de la société est désigné, selon le cas, par le directeur général ou le président du directoire, pour une durée qui ne peut excéder six exercices, parmi les organismes accrédités à cet effet par le Comité français d'accréditation (COFRAC) ou par tout autre organisme d'accréditation signataire de l'accord de reconnaissance multilatéral établi par la coordination européenne des organismes d'accréditation. / L'organisme tiers indépendant est soumis aux incompatibilités prévues à l'article L. 822-11 " ; que l'organisme tiers chargé de vérifier les informations mentionnées dans le rapport annuel est désigné parmi des organismes accrédités à cet effet et soumis à des exigences déontologiques par les dispositions de l'article L. 822-11 du code de commerce auxquelles renvoie l'article R. 225-105-2 ; que le moyen tiré de ce que les dispositions réglementaires attaquées n'assureraient pas l'indépendance de l'organisme vérificateur, en méconnaissance des dispositions de l'article L. 225-102-1 du code de commerce ne peut être acueilli ; <br/>
<br/>
              	6. Considérant, en dernier lieu, qu'en vertu du huitième alinéa de l'article L. 225-102-1 du code de commerce, l'obligation, qui résulte des dispositions citées au point 5, de procéder à une vérification des informations sociales et environnementales figurant dans le rapport annuel par un organisme tiers indépendant " s'applique à partir de l'exercice qui a été ouvert après le 31 décembre 2011 pour les entreprises dont les titres sont admis aux négociations sur un marché réglementé " et " à partir de l'exercice clos au 31 décembre 2016 pour l'ensemble des entreprises concernées par le présent article " ; que, par suite, l'association requérante n'est pas fondée à soutenir qu'en différant l'entrée en vigueur des dispositions de l'article R. 225-105-2 du code de commerce au 31 décembre 2016 pour les entreprises autres que celles dont les titres sont admis aux négociations sur un marché réglementé, le décret attaqué aurait méconnu les dispositions législatives qu'il a pour objet de mettre en oeuvre ; <br/>
<br/>
              	7. Considérant qu'il résulte de tout ce qui précède que l'association Forum citoyen pour la responsabilité sociale des entreprises n'est pas fondée à demander l'annulation du décret et de la décision qu'elle attaque ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : La requête de l'association Forum citoyen pour la responsabilité sociale des entreprises est rejetée. <br/>
 Article 2 : La présente décision sera notifiée à l'association Forum citoyen pour la responsabilité sociale des entreprises, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
