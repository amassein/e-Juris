<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029851707</ID>
<ANCIEN_ID>JG_L_2014_12_000000359769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/85/17/CETATEXT000029851707.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 05/12/2014, 359769, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-12-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2014:359769.20141205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B... A...et la SCI les Rosiers ont demandé au tribunal administratif de Grenoble :<br/>
              - d'annuler pour excès de pouvoir la décision du 11 février 2002 par laquelle le maire de Scionzier (Haute-Savoie) a exercé le droit de préemption urbain de la commune sur un bien immobilier appartenant à la SCI les Rosiers ;<br/>
              - d'enjoindre à la commune de Scionzier, d'une part, de s'abstenir de revendre à un tiers le bien préempté et, d'autre part, de restituer ce bien à son propriétaire initial sous astreinte de 100 euros par jour de retard ;<br/>
              - de condamner la commune de Scionzier à verser à la SCI les Rosiers une somme de 61 000 euros et à M. A... une somme de 10 000 euros en réparation des préjudices qu'ils estiment avoir subis en raison de cette décision de préemption, assorties des intérêts et de la capitalisation des intérêts.<br/>
<br/>
              Par un jugement n°s 0703254, 0704555 du 30 juin 2011, le tribunal administratif de Grenoble a :<br/>
              - annulé la décision du 11 février 2002 ;<br/>
              - condamné la commune de Scionzier à verser à la SCI les Rosiers la somme de 60 979,60 euros et M. A... la somme de 1 000 euros, assorties des intérêts et de la capitalisation des intérêts ;<br/>
              - rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un arrêt n°s 11LY01985, 11LY01989 du 27 mars 2012, la cour administrative d'appel de Lyon, saisie par la commune de Scionzier, a :<br/>
              - dit n'y avoir pas lieu de statuer sur les conclusions à fin de sursis à exécution présentées par la commune de Scionzier ;<br/>
              - annulé le jugement du tribunal administratif de Grenoble du 30 juin 2011 en tant qu'il condamne la commune de Scionzier à verser une indemnité à M. A... ;<br/>
              - rejeté la demande indemnitaire présentée au tribunal administratif de Grenoble par M. A... ;<br/>
              - rejeté le surplus des conclusions de l'appel et de la requête à fin de sursis à exécution de la commune.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 mai 2012, 29 août 2012 et 17 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Scionzier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n°s 11LY01985, 11LY01989 de la cour administrative d'appel de Lyon du 27 mars 2012 en tant qu'il rejette partiellement son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à celles des conclusions de son appel qui ont été rejetées par la cour ;<br/>
<br/>
              3°) de mettre à la charge de la SCI les Rosiers et de M. A... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
            Vu :<br/>
<br/>
            - les autres pièces du dossier ;<br/>
<br/>
            - le code général des collectivités territoriales ;<br/>
<br/>
            - le code de l'urbanisme ;<br/>
<br/>
            - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
            - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Scionzier et à la SCP Didier, Pinet, avocat de la SCI les Rosiers et de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne " ;<br/>
<br/>
              2. Considérant que la communication aux parties du sens des conclusions, prévue par les dispositions de l'article R. 711-3 du code de justice administrative, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré ; qu'en conséquence, à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure devant la cour administrative d'appel de Lyon que les parties ont été mises en mesure de savoir, par l'intermédiaire du système informatique de suivi de l'instruction, avant la tenue de l'audience au cours de laquelle leur affaire allait être examinée, que le rapporteur public conclurait à l'irrecevabilité des conclusions de l'appel incident de M. A... et de la SCI Les Rosiers et à l'annulation partielle du jugement du tribunal administratif de Grenoble du 30 juin 2011, qui, après les avoir jointes, avait statué tant sur la demande à fin d'annulation que sur la demande indemnitaire de la société et de son gérant ; qu'eu égard aux caractéristiques du litige, en omettant de préciser sur quelle partie du jugement du tribunal administratif porteraient ses conclusions proposant l'annulation partielle de celui-ci, le rapporteur public n'a pas mis les parties en mesure de connaître l'ensemble des éléments du dispositif de la décision qu'il comptait proposer à la formation de jugement d'adopter ; que la commune de Scionzier, qui peut utilement se prévaloir de cette irrégularité, est, par suite, fondée à soutenir que l'arrêt qu'elle attaque a été rendu au terme d'une procédure irrégulière ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la commune de Scionzier est fondée à demander l'annulation de l'arrêt attaqué, en tant qu'il rejette le surplus des conclusions de son appel contre le jugement du tribunal administratif de Grenoble du 30 juin 2011 ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation ainsi prononcée ;<br/>
<br/>
              6. Considérant que, par une décision du 11 février 2002, le maire de Scionzier a exercé le droit de préemption urbain de la commune sur un bien immobilier situé 13 avenue de la Route Blanche et mis en vente par la SCI les Rosiers, ayant pour gérant M. B... A... ; que la SCI les Rosiers et M. A... ont saisi le tribunal administratif de Grenoble d'une demande tendant à l'annulation de cette décision puis d'une demande tendant à voir engager la responsabilité de la commune ; qu'eu égard à l'étendue de la cassation prononcée, le juge d'appel est de nouveau saisi des conclusions par lesquelles la commune de Scionzier demande l'annulation du jugement du tribunal administratif de Grenoble du 30 juin 2011 en tant qu'il annule la décision du 11 février 2002 et la condamne à verser à la SCI les Rosiers une indemnité de 60 979,60 euros, en réparation des préjudices résultant de l'illégalité de cette décision ; <br/>
<br/>
              Sur la régularité du jugement du tribunal administratif de Grenoble et la recevabilité des demandes de première instance :<br/>
<br/>
              7. Considérant, en premier lieu, que le tribunal a jugé à bon droit que la SCI les Rosiers, propriétaire du bien immobilier qui a fait l'objet de la décision de préemption, justifiait en cette qualité d'un intérêt pour agir à l'encontre de cette décision ; qu'ayant ainsi admis la recevabilité du recours pour excès de pouvoir dont il était saisi en ce qu'il émanait de la société, le tribunal n'a pas, contrairement à ce que soutient la commune, entaché son jugement d'irrégularité en s'abstenant de se prononcer sur l'intérêt pour agir de M. A... ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ; que la circonstance que le requérant a eu connaissance d'une décision est sans incidence sur l'application de ces dispositions ; qu'il ressort des pièces du dossier que la décision attaquée du 11 février 2002, dont l'article R. 213-8 du code de l'urbanisme imposait la notification au propriétaire du bien préempté, a été notifiée sans indication des voies et délais de recours ; que, par suite, c'est à bon droit que le tribunal a écarté la fin de non-recevoir tirée de la tardiveté de sa requête ; <br/>
<br/>
              9. Considérant, en troisième lieu, qu'il résulte des dispositions des articles R. 431-2 et R. 431-3 du code de justice administrative que les conclusions indemnitaires dirigées contre une collectivité territoriale figurent au nombre de celles qui, en première instance, peuvent être présentées sans recourir au ministère d'un avocat ; que, par suite, le tribunal a, à bon droit, admis la recevabilité de la demande de la SCI les Rosiers tendant à la condamnation de la commune au versement d'indemnités en réparation des préjudices subis, présentée sans le recours au ministère d'un avocat ;<br/>
<br/>
              10. Considérant, en dernier lieu, que le tribunal n'a pas statué au-delà des conclusions dont il était saisi en condamnant la commune de Scionzier à verser à la SCI les Rosiers une indemnité de 60 979,60 euros ;<br/>
<br/>
              Sur la légalité de la décision du 11 février 2002 :<br/>
<br/>
              11. Considérant que l'article L. 213-2 du code de l'urbanisme prévoit que le silence du titulaire du droit de préemption pendant deux mois à compter de la réception de la déclaration d'intention d'aliéner vaut renonciation à l'exercice du droit de préemption ; qu'aux termes du premier alinéa de l'article L. 2131-1 du code général des collectivités territoriales, dans sa rédaction applicable à la date de la décision attaquée : " Les actes pris par les autorités communales sont exécutoires de plein droit dès qu'il a été procédé à leur publication ou à leur notification aux intéressés ainsi qu'à leur transmission au représentant de l'Etat dans le département ou à son délégué dans l'arrondissement " ; que l'article L. 2131-2 du même code prévoit que cette obligation de transmission vaut également pour les décisions prises par délégation du conseil municipal en application de l'article L. 2122-22 ; qu'au nombre de ces dernières décisions figurent les décisions de préemption ; <br/>
<br/>
              12. Considérant qu'il résulte des dispositions mentionnées ci-dessus de l'article L. 213-2 du code de l'urbanisme que les propriétaires qui ont décidé de vendre un bien susceptible de faire l'objet d'une décision de préemption doivent savoir de façon certaine, au terme du délai de deux mois imparti au titulaire du droit de préemption pour en faire éventuellement usage, s'ils peuvent ou non poursuivre l'aliénation entreprise ; que, dans le cas où le titulaire du droit de préemption décide de l'exercer, les mêmes dispositions, combinées avec celles précitées du code général des collectivités territoriales, imposent que la décision de préemption soit exécutoire au terme du délai de deux mois, c'est-à-dire non seulement prise mais également notifiée au propriétaire intéressé et transmise au représentant de l'Etat ; que la réception de la décision par le propriétaire intéressé et le représentant de l'Etat dans le délai de deux mois, à la suite respectivement de sa notification et de sa transmission, constitue, par suite, une condition de la légalité de la décision de préemption ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier que la commune a reçu le 15 décembre 2001 la déclaration d'intention d'aliéner le bien situé 13 avenue de la Route Blanche ; que la décision par laquelle le maire de Scionzier, auquel le conseil municipal avait délégué l'exercice du droit de préemption urbain, a préempté ce bien a été transmise à la sous-préfecture de Bonneville le 1er mars 2002 seulement ; qu'ainsi, cette décision était illégale ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que la commune de Scionzier n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a annulé la décision de son maire du 11 février 2002 ;<br/>
<br/>
              Sur la responsabilité de la commune de Scionzier :<br/>
<br/>
              15. Considérant qu'aux termes du 1er alinéa de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis " ; qu'en vertu de l'article 6 de la même loi, si les autorités administratives ne peuvent renoncer à opposer la prescription qui découle de cette loi, les créanciers des personnes publiques entrant dans son champ peuvent toutefois " être relevés en tout ou en partie de la prescription, à raison de circonstances particulières et notamment de la situation du créancier " ; que, s'agissant des créanciers des communes, cette décision doit être prise par délibération motivée du conseil municipal, approuvée par l'autorité compétente pour approuver le budget de la commune ; qu'aux termes du 1er alinéa de l'article 7 de la même loi : " L'administration doit, pour pouvoir se prévaloir, à propos d'une créance litigieuse, de la prescription prévue par la présente loi, l'invoquer avant que la juridiction saisie du litige au premier degré se soit prononcée sur le fond " ; qu'enfin, aux termes de l'article 8 de la même loi : " La juridiction compétente pour connaître de la demande à laquelle la prescription est opposée, en vertu de la présente loi, est compétente pour statuer sur l'exception de prescription " ;<br/>
<br/>
              16. Considérant qu'il résulte de ces dispositions que l'administration ne peut renoncer à opposer la prescription, sauf à en relever le créancier selon la procédure ou pour les motifs qu'elles prévoient ; que ces dispositions ne déterminent pas l'autorité ayant qualité pour l'opposer ni ne régissent les formes dans lesquelles cette autorité peut l'invoquer devant la juridiction du premier degré ; que ni ces dispositions, ni aucun élément tenant à la nature de la prescription ne font obstacle à ce que celle-ci soit opposée par une personne ayant reçu de l'autorité compétente une délégation ou un mandat à cette fin ; que l'avocat, à qui l'administration a donné mandat pour la représenter en justice et qui, à ce titre, est habilité à opposer pour la défense des intérêts de cette dernière toute fin de non-recevoir et toute exception, doit être regardé comme ayant été également mandaté pour opposer l'exception de prescription aux conclusions du requérant tendant à la condamnation de cette administration à l'indemniser ; que, par suite, c'est à tort que le tribunal a jugé que l'exception de prescription quadriennale n'avait pas été valablement opposée, au motif qu'elle l'avait été sous la seule signature de l'avocat de la commune ;<br/>
<br/>
              17. Considérant que lorsqu'est demandée l'indemnisation du préjudice résultant de l'illégalité d'une décision administrative, le fait générateur de la créance doit être rattaché non à l'exercice au cours duquel la décision a été prise mais à celui au cours duquel elle a été valablement notifiée ; qu'il résulte de l'instruction que la créance dont se prévalait la SCI Les Rosiers trouve sa source dans la décision de préemption du 11 février 2002, dont le notaire chargé de la vente du bien litigieux a reçu notification le 12 février 2002 ; que ce notaire ayant signé la déclaration d'intention d'aliéner, il devait être regardé comme le mandataire de la SCI ; que la déclaration mentionnait que les décisions du titulaire du droit de préemption devaient être notifiées à l'adresse de ce mandataire et non à celle du propriétaire ; que la circonstance que cette notification n'était pas accompagnée de la mention des voies et délais de recours, si elle faisait obstacle à ce que le délai de recours contentieux puisse être opposé à une demande tendant à l'annulation de la décision de préemption, était sans incidence pour l'application de la loi du 31 décembre 1968 ; qu'ainsi, la décision de préemption doit être regardée comme ayant été valablement notifiée à la SCI les Rosiers le 12 février 2002 ; que, par suite, la prescription de la créance de cette société était acquise le 3 juillet 2007, quand elle a saisi pour la première fois la commune d'une demande d'indemnisation de son préjudice ; que, dès lors, la commune de Scionzier était fondée à opposer, sous la signature de son avocat, l'exception de prescription quadriennale à la créance dont la SCI les Rosiers se prévalait devant le tribunal ; <br/>
<br/>
              18. Considérant qu'il résulte de ce qui précède que la commune de Scionzier est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a écarté l'exception de prescription quadriennale et l'a condamnée à payer à la SCI les Rosiers la somme de 60 979,60 euros en réparation des préjudices subis du fait de l'illégalité de la décision du 11 février 2002 ; que son jugement doit être annulé dans cette mesure ; <br/>
<br/>
              19. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Scionzier, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI les Rosiers et de M. A... la somme que la commune de Scionzier demande au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : L'article 4 de l'arrêt de la cour administrative d'appel de Lyon du 27 mars 2012 est annulé. <br/>
<br/>
Article 2 : Les conclusions indemnitaires présentées par la SCI les Rosiers devant le tribunal administratif de Grenoble sont rejetées.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Grenoble du 30 juin 2011 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 4 : Les conclusions d'appel de la commune de Scionzier tendant à l'annulation de l'article 1er du jugement du tribunal administratif de Grenoble du 30 juin 2011 sont rejetées.<br/>
<br/>
Article 5 : Les conclusions de la commune de Scionzier ainsi que celles de la SCI les Rosiers et de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la commune de Scionzier, à la SCI les Rosiers et à M. B... A.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-02 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. COMPÉTENCE POUR OPPOSER LA PRESCRIPTION. - FACULTÉ DE FAIRE OPPOSER LA PRESCRIPTION QUADRIENNALE PAR UN MANDATAIRE DE L'AUTORITÉ COMPÉTENTE - EXISTENCE - INCLUSION DE CETTE FACULTÉ DANS LE MANDAT DE REPRÉSENTATION EN JUSTICE DE L'AVOCAT - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - PRÉJUDICE RÉSULTANT DE L'ILLÉGALITÉ D'UNE DÉCISION ADMINISTRATIVE - 1) EXERCICE AUQUEL RATTACHER LA CRÉANCE POUR DÉTERMINER LE POINT DE DÉPART DE LA PRESCRIPTION - DATE DE LA NOTIFICATION DE CETTE DÉCISION - 2) CIRCONSTANCE QUE LA NOTIFICATION NE COMPORTE PAS LA MENTION DES VOIES ET DÉLAIS DE RECOURS PRÉVUE PAR LES TEXTES - ABSENCE D'INCIDENCE SUR LE DÉCLENCHEMENT DU DÉLAI DE PRESCRIPTION [RJ2].
</SCT>
<ANA ID="9A"> 18-04-02-02 Il résulte des dispositions de loi n° 68-1250 du 31 décembre 1968 que l'administration ne peut renoncer à opposer la prescription, sauf à en relever le créancier selon la procédure ou pour les motifs qu'elles prévoient. Ces dispositions ne déterminent pas l'autorité ayant qualité pour l'opposer ni ne régissent les formes dans lesquelles cette autorité peut l'invoquer devant la juridiction du premier degré. Ni ces dispositions, ni aucun élément tenant à la nature de la prescription ne font obstacle à ce que celle-ci soit opposée par une personne ayant reçu de l'autorité compétente une délégation ou un mandat à cette fin. En particulier, l'avocat, à qui l'administration a donné mandat pour la représenter en justice et qui, à ce titre, est habilité à opposer pour la défense des intérêts de cette dernière toute fin de non-recevoir et toute exception, doit être regardé comme ayant été également mandaté pour opposer l'exception de prescription aux conclusions du requérant tendant à la condamnation de cette administration à l'indemniser.</ANA>
<ANA ID="9B"> 18-04-02-04 1) Lorsqu'est demandée l'indemnisation du préjudice résultant de l'illégalité d'une décision administrative, le fait générateur de la créance doit être rattaché non à l'exercice au cours duquel la décision a été prise mais à celui au cours duquel elle a été valablement notifiée.... ,,2) La circonstance que cette notification n'ait pas été accompagnée de la mention des voies et délais de recours, si elle fait obstacle, en vertu des textes applicables, à ce que le délai de recours contentieux puisse être opposé à une demande tendant à l'annulation de la décision en cause, est sans incidence pour l'application de la loi n° 68-1250 du 31 décembre 1968.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, Section, 29 juillet 1983, Ville de Toulouse, n° 23828, p. 312., ,,[RJ2] Cf. CE, 31 janvier 2000, Gonon, n° 191800, T. p. 917.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
