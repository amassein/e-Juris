<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664479</ID>
<ANCIEN_ID>JG_L_2021_06_000000431832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664479.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/06/2021, 431832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:431832.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fédération Départementale des Chasseurs du Gard a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir la lettre du président de la Fédération Nationale des Chasseurs du 21 juillet 2015 tendant à la communication aux fédérations départementales de chasseurs de la grille tarifaire nationale approuvée par délibération de la commission nationale d'indemnisation des dégâts de gibier du 10 mars 2015 et arrêtant les motifs et taux applicables à la procédure de réduction des indemnisations des dégâts commis aux cultures par le grand gibier lorsque la victime des dégâts a une part de responsabilité dans la commission de ceux-ci. Par une ordonnance n° 1502971 du 30 septembre 2015, le président du tribunal administratif de Nîmes a transmis la demande au tribunal administratif de Cergy-Pontoise sur le fondement de l'article R. 351-3 du code de justice administrative. Par une ordonnance n° 1508468 du 12 juillet 2017, le président de la 1ère chambre du tribunal administratif de Cergy-Pontoise a, sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, rejeté cette demande comme étant manifestement irrecevable.<br/>
<br/>
              Par un arrêt n° 17VE02941 du 18 juin 2019, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Marseille a, d'une part, sur appel de la Fédération Départementale des Chasseurs du Gard, annulé cette ordonnance, d'autre part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée par la Fédération Départementale des Chasseurs du Gard.<br/>
<br/>
              Par cette requête et un mémoire, enregistrés les 12 septembre 2017 et 23 novembre 2018 au greffe de la cour administrative d'appel de Marseille, et un autre mémoire, enregistré le 13 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération Départementale des Chasseurs du Gard demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la lettre du président de la Fédération Nationale des Chasseurs du 21 juillet 2015;<br/>
<br/>
              2°) de mettre à la charge de la Fédération Nationale des Chasseurs la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération Départementale des Chasseurs du Gard et à la SCP Waquet, Farge, Hazan, avocat de la Fédération Nationale des Chasseurs ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une lettre du 21 juillet 2015, le président de la Fédération Nationale des Chasseurs a transmis aux Fédérations Départementales des Chasseurs la délibération de la commission nationale d'indemnisation des dégâts de gibier du 10 mars 2015 adoptant la grille nationale de référence fixant les motifs et les taux applicables à la procédure de réduction des indemnisations des dégâts commis aux cultures par le grand gibier s'il est constaté que la victime des dégâts a une part de responsabilité dans la commission de ceux-ci. La Fédération Départementale des Chasseurs du Gard a demandé au tribunal administratif de Nîmes, puis de Cergy-Pontoise, d'annuler pour excès de pouvoir cette lettre. Par une ordonnance du 12 juillet 2017, le président de la 1ère chambre du tribunal administratif de Cergy-Pontoise, sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, a rejeté cette demande comme étant manifestement irrecevable, faute d'être dirigée contre un acte faisant grief. Par un arrêt du 18 juin 2019, la cour administrative d'appel de Versailles a annulé cette ordonnance et transmis au Conseil d'Etat la requête de la Fédération Départementale des Chasseurs du Gard.<br/>
<br/>
              2. D'une part, il ressort des pièces du dossier que la lettre du président de la Fédération Nationale des Chasseurs du 21 juillet 2015 a non seulement pour objet de transmettre aux Fédérations Départementales des Chasseurs la grille adoptée par la commission nationale d'indemnisation des dégâts de gibier, dont la charge financière relève de ces fédérations, mais également de préciser la date de l'entrée en vigueur de cette grille. Par suite, cette lettre du président de la Fédération Nationale des Chasseurs doit être regardée comme susceptible d'avoir des effets notables sur les tiers et, par suite, comme pouvant être déférée au juge de l'excès de pouvoir.<br/>
<br/>
              3. D'autre part, en vertu de l'article R. 426-5 du code de l'environnement, dans sa version alors en vigueur, " la commission nationale d'indemnisation des dégâts de gibier élabore une grille nationale de référence, fixe les motifs et les taux applicables à la procédure de réduction d'indemnisation " des dégâts commis aux cultures par le grand gibier. Aux termes de l'article L. 421-14 du code de l'environnement, dans sa rédaction applicable au litige : " L'association dénommée Fédération Nationale des Chasseurs regroupe l'ensemble des fédérations départementales, interdépartementales et régionales des chasseurs dont l'adhésion est constatée par le paiement d'une cotisation obligatoire. / Elle assure la représentation des fédérations départementales, interdépartementales et régionales des chasseurs à l'échelon national. / Elle est chargée d'assurer la promotion et la défense de la chasse, ainsi que la représentation des intérêts cynégétiques. Elle coordonne l'action des fédérations départementales, interdépartementales et régionales des chasseurs. / (...) Elle gère, dans les conditions fixées par décret en Conseil d'Etat, un fonds dénommé Fonds cynégétique national assurant, d'une part, une péréquation entre les fédérations départementales des chasseurs en fonction de leurs ressources et de leurs charges et, d'autre part, la prévention et l'indemnisation des dégâts de grand gibier par les fédérations départementales des chasseurs. (...) ".<br/>
<br/>
              4. Par suite, en fixant la date d'entrée en vigueur de la grille transmise aux fédérations départementales, le président de la Fédération Nationale des Chasseurs a entendu en fixer les conditions d'application aux dossiers en cours d'instruction alors qu'il ne disposait d'aucune compétence à cette fin. La Fédération Départementale des Chasseurs du Gard est ainsi fondée à soutenir que la lettre qu'il attaque édicte, dans le silence des textes, une règle nouvelle entachée d'incompétence. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la Fédération Départementale des Chasseurs du Gard est fondée à demander l'annulation de la lettre du président de la Fédération Nationale des Chasseurs qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fédération Nationale des Chasseurs la somme de 1 000 euros à verser à la Fédération Départementale des Chasseurs du Gard au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de la Fédération Départementale des Chasseurs du Gard qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La lettre du président de la Fédération Nationale des Chasseurs du 21 juillet 2015 est annulée.<br/>
Article 2 : La Fédération Nationale des Chasseurs versera à la Fédération Départementale des Chasseurs du Gard une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : Les conclusions présentées par la Fédération Nationale des Chasseurs au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4: La présente décision sera notifiée à la Fédération Départementale des Chasseurs du Gard et à la Fédération Nationale des Chasseurs. <br/>
Copie en sera adressée à la ministre de la transition écologique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
