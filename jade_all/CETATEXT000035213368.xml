<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035213368</ID>
<ANCIEN_ID>JG_L_2017_07_000000410766</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/21/33/CETATEXT000035213368.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 17/07/2017, 410766, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410766</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410766.20170717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire distinct, enregistrés les 12 et 14 avril 2017 au greffe du tribunal administratif de Rouen, M. et Mme A...B...ont demandé, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, à ce tribunal de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution des dispositions du c) du 1° du V de l'article 151 septies A du code général des impôts.<br/>
<br/>
              Par une ordonnance n° 1701218 du 15 mai 2017, le tribunal administratif de Rouen a transmis cette question au Conseil d'Etat et sursis à statuer sur la requête de M. et Mme B..., jusqu'à la réception de la décision du Conseil d'Etat ou, le cas échéant, jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil d'Etat se prononce sur le renvoi de la question au Conseil constitutionnel dans un délai de trois mois, sans qu'il lui appartienne, dans ce cadre, de statuer sur la régularité de la décision juridictionnelle qui lui a transmis la question.<br/>
<br/>
              2. En vertu des mêmes dispositions, le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité par le Conseil d'Etat à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. Aux termes de l'article 151 septies A du code général des impôts : " V.-1. L'indemnité compensatrice versée à un agent général d'assurances exerçant à titre individuel par la compagnie d'assurances qu'il représente à l'occasion de la cessation du mandat bénéficie du régime mentionné au I si les conditions suivantes sont réunies : a) Le contrat dont la cessation est indemnisée doit avoir été conclu depuis au moins cinq ans au moment de la cessation ; b) L'agent général d'assurances fait valoir ses droits à la retraite à la suite de la cessation du contrat ; c) L'activité est intégralement poursuivie par un nouvel agent général d'assurances exerçant à titre individuel et dans le délai d'un an. / 2. (...) ".<br/>
<br/>
              4. Ces dispositions sont applicables au litige et elles n'ont pas déjà été déclarées conformes à la Constitution dès lors que, comme il l'a précisé au point 4 de sa décision n° 2016-587 QPC du 14 octobre 2016, le Conseil constitutionnel ne s'est prononcé, dans cette décision, que sur les mots " dans les mêmes locaux " qui figuraient, avant leur abrogation, au c) du 1° du V de l'article 151 septies du code général des impôts.<br/>
<br/>
              5. M. et Mme B...font valoir que, en subordonnant le bénéfice de l'exonération qu'elles prévoient à la condition que l'activité soit reprise par un agent général d'assurances exerçant cette activité à titre individuel, les dispositions du c) du 1° du V de l'article 151 septies A du code général des impôts méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques en créant une différence de traitement entre les agents généraux d'assurances pour lesquels cette condition n'est pas remplie et les professionnels qui bénéficient des régimes d'exonération prévus par les articles 151 septies, 151 septies A et 238 quindecies du code général des impôts. M. et Mme B...soutiennent que cette différence de traitement n'est pas justifiée par une différence de situation en rapport avec l'objet de la loi et ne repose pas sur un critère objectif et rationnel. La question prioritaire de constitutionnalité ainsi soulevée par M. et Mme B...présente, notamment au regard du principe d'égalité devant la loi, un caractère sérieux et il y a donc lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du c) du 1° du V de l'article 151 septies A du code général des impôts, en tant qu'elles subordonnent le bénéfice de l'exonération qu'elles prévoient à la condition que l'activité soit reprise par un agent général d'assurances exerçant cette activité à titre individuel, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Rouen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
