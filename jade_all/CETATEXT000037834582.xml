<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834582</ID>
<ANCIEN_ID>JG_L_2018_12_000000408616</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 19/12/2018, 408616, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408616</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408616.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 3 mars et 18 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 4 avril 2016 par laquelle le conseil académique de l'université de Nice Sophia Antipolis a décidé de ne pas retenir sa candidature sur le poste n° 23 PR 1391 "Géographie physique, climatologie" ;<br/>
<br/>
              2°) d'enjoindre à l'université de Nice Sophia Antipolis d'annuler le recrutement en cours et de reprendre la procédure de recrutement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 novembre 2018, présentée par M. B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 9-3 du décret du 6 juin 1984 portant statut particulier des enseignants-chercheurs : " Par dérogation à l'article 9-2, le conseil académique ou l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1, en formation restreinte, examine les candidatures à la mutation et au détachement des personnes qui remplissent les conditions prévues aux articles 60 et 62 de la loi du 11 janvier 1984 susvisée, sans examen par le comité de sélection. Si le conseil académique retient une candidature, il transmet le nom du candidat sélectionné au conseil d'administration. Lorsque l'examen de la candidature ainsi transmise conduit le conseil d'administration à émettre un avis favorable sur cette candidature, le nom du candidat retenu est communiqué au ministre chargé de l'enseignement supérieur. L'avis défavorable du conseil d'administration est motivé. / Lorsque la procédure prévue au premier alinéa n'a pas permis de communiquer un nom au ministre chargé de l'enseignement supérieur, les candidatures qui n'ont pas été retenues par le conseil académique ou qui ont fait l'objet d'un avis défavorable du conseil d'administration sont examinées avec les autres candidatures par le comité de sélection selon la procédure prévue à l'article 9-2 " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que M.B..., professeur des universités titulaire en géographie à l'université Paris VIII, a fait acte de candidature sur un poste de professeur d'université ouvert à l'université de Nice Sophia Antipolis sur un poste de géographie physique et climatologie intitulé 23 PR ID 286 (n° 4391) en demandant à bénéficier des dispositions, citées ci-dessus, de l'article 9-3 du décret du 6 juin 1984 ; que, par sa délibération du 4 avril 2016, le conseil académique de l'université a décidé de ne pas retenir sa candidature et a, en conséquence, transmis celle-ci au comité de sélection constitué pour le recrutement à ce poste ; que M. B...demande l'annulation de cette décision ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'aucun texte ni aucun principe n'impose que, dans le cadre de la procédure définie par l'article 9-3 du décret du 6 juin 1984, citée au point 1, le conseil académique soit tenu de désigner deux rapporteurs chargés d'établir un rapport de présentation sur le profil du candidat et son adéquation au profil du poste ; que M. B...n'est, par suite, pas fondé à soutenir que la décision qu'il attaque serait, pour ce motif, entachée d'irrégularité ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que, contrairement à ce que soutient M.B..., la seule circonstance qu'un comité de sélection ait été constitué pour le poste litigieux par deux délibérations du conseil d'administration de l'université des 9 février et 15 mars 2016, soit antérieurement à la décision litigieuse du conseil académique, et que ce comité avait débuté ses travaux à la date de cette décision, est sans incidence sur la légalité de celle-ci ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'en estimant que le profil d'enseignant-chercheur de M.B..., dont il ressort des pièces du dossier qu'il est un karstologue dont le champ de recherches prédominant relève de la géographie physique, n'était pas en adéquation avec le profil du poste mis au concours, lequel supposait notamment des compétences de recherche et d'enseignement relevant, non seulement de la géographie physique mais également de la géographie humaine en lien avec d'autres disciplines connexes, le conseil académique, qui a suffisamment motivé sa décision, n'a pas fait une inexacte application des dispositions, citées ci-dessus, de l'article 9-3 du décret du 6 juin 1984 ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non recevoir soulevée par l'Université de Nice Sophia-Antipolis, M. B...n'est pas fondé à demander l'annulation de la délibération qu'il attaque ; que ses conclusions aux fins d'injonction ne peuvent, par suite, qu'être également rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à l'Université de Nice Sophia-Antipolis.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
