<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043677312</ID>
<ANCIEN_ID>JG_L_2021_06_000000453236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/73/CETATEXT000043677312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/06/2021, 453236, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453236.20210611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 3, 9 et 10 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... et l'association " La France insoumise " demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner toutes mesures utiles afin de faire cesser l'atteinte grave et manifestement illégale portée à la liberté d'expression, à la liberté de réunion et au pluralisme des courants de pensées et d'opinions ; <br/>
<br/>
              2°) d'ordonner la suspension de l'exécution des dispositions du 9° du III de l'article 3 du décret n° 2020-1310 du 29 octobre 2020 en ce qu'elles limitent à 50 personnes les réunions électorales organisées en plein air hors des établissements recevant du public et ce au plus tard avant le 13 juin 2021 ; <br/>
<br/>
              3°) d'enjoindre au Premier ministre de modifier, dans un délai de huit jours à compter de la notification de la présente ordonnance, en application de l'article L. 3131-15 du code de la santé publique, les dispositions du 9° du III de l'article 3 du décret du 29 octobre 2020, en prenant les mesures strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu applicables, pour encadrer les réunions électorales ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Les requérantes soutiennent que : <br/>
              - la condition d'urgence est satisfaite eu égard, en premier lieu, au contexte dans lequel intervient la mesure contestée, dès lors les élections régionales se dérouleront le 20 juin 2021, en période de crise sanitaire, en deuxième lieu, à l'imminence de la réunion électorale qu'elles ont prévue le 13 juin 2021 ainsi que des autres rassemblements organisés par " La France insoumise " ou par les autres partis politiques français et, en dernier lieu, au caractère manifestement inadapté et disproportionné de l'interdiction contestée ; <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - l'interdiction contestée méconnaît la liberté d'expression et de communication, la liberté de réunion et, son corollaire, le droit pour un parti politique de tenir réunion, ainsi que le caractère pluraliste de l'expression des courants de pensée et d'opinion ;  <br/>
              - les dispositions contestées constituent, en elles-mêmes, une restriction à la liberté d'expression et à la liberté de réunion dès lors qu'elles limitent drastiquement le nombre de participants aux réunions électorales qui ont vocation à être organisées sur la voie publique au cours des prochains jours dans le cadre de la campagne des élections régionales, qui se tiendront les 20 et 27 juin 2021 sur le territoire national ; <br/>
              - une telle restriction n'est ni adaptée, ni nécessaire, ni proportionnée à l'objectif de sauvegarde de la santé publique poursuivi dès lors que, en premier lieu, l'amélioration de la situation sanitaire et l'évolution des connaissances scientifiques ne permettent plus de regarder l'interdiction contestée comme justifiée, en particulier s'agissant des réunions électorales organisées sur la voie publique dans le respect des gestes barrière et du port du masque, en deuxième lieu, à ce jour, aucun cluster n'a été identifié à la suite de rassemblements sur la voie publique respectant des gestes barrière, en troisième lieu, les dispositions contestées introduisent une différence de traitement injustifiée entre, d'une part, les réunions électorales organisées sur la voie publique qui sont soumises au respect d'une limite de 50 personnes maximum, et, d'autre part, les autres exceptions à l'interdiction de rassemblement, qui, soit sont permises sans limitation du nombre de personnes, soit, s'agissant des rassemblements organisés dans les établissements recevant du public dans lesquels l'accueil du public n'est pas interdit, permettent l'accueil d'un public assis dans la limite de 1 000 personnes ; <br/>
              - l'interdiction contestée méconnaît le principe du caractère pluraliste de l'expression des courants de pensée et d'opinion et sont entachées d'un doute quant à leurs finalités réelles dès lors que, en ne permettant des réunions sans limitation du nombre de participants à 50 personnes que dans certains établissements recevant du public, elles favorisent les partis politiques dotés des ressources financières suffisantes pour ce faire et, partant, privilégient les partis politiques ayant obtenu les meilleurs résultats aux élections législatives de 2017, les autres partis étant contraints à des rassemblements en extérieur dans la limite de 50 personnes maximum.  <br/>
<br/>
              Par un mémoire en défense, enregistré le 8 juin 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte grave et manifestement illégale n'a été portée à une liberté fondamentale.  <br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - loi n° 2021-689 du 31 mai 2021 ; <br/>
              - le décret n° 2021-699 du 1er juin 2021 ; <br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A... et l'association " La France insoumise " et, d'autre part, le Premier ministre et le ministre des solidarités et de la santé ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 10 juin 2021, à 12 heures 30 ; <br/>
<br/>
              - Me Cyrille Lesourd, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ; <br/>
<br/>
              - le représentant des requérants ; <br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus du covid-19 prises dans le cadre de l'état d'urgence sanitaire régi par les articles L. 3131-12 et suivants du code de la santé publique ont été remplacées, dans le cadre de la loi du 31 mai 2021 relative à la gestion de la sortie de la crise sanitaire, par celles du décret du 1er juin 2021 qui les adapte à la situation sanitaire actuelle.<br/>
<br/>
              3. Aux termes de l'article 3 de ce décret, " I. - Tout rassemblement, réunion ou activité sur la voie publique ou dans un lieu ouvert au public, qui n'est pas interdit par le présent décret, est organisé dans des conditions de nature à permettre le respect des dispositions de l'article 1er. / II. - Les organisateurs des manifestations sur la voie publique mentionnées à l'article L. 211-1 du code de la sécurité intérieure adressent au préfet de département sur le territoire duquel la manifestation doit avoir lieu, sans préjudice des autres formalités applicables, une déclaration contenant les mentions prévues à l'article L. 211-2 du même code, en y précisant, en outre, les mesures qu'ils mettent en oeuvre afin de garantir le respect des dispositions de l'article 1er du présent décret. / Sans préjudice des dispositions de l'article L. 211-4 du code de la sécurité intérieure, le préfet peut en prononcer l'interdiction si ces mesures ne sont pas de nature à permettre le respect des dispositions de l'article 1er. / III. - Les rassemblements, réunions ou activités sur la voie publique ou dans un lieu ouvert au public autres que ceux mentionnés au II mettant en présence de manière simultanée plus de dix personnes sont interdits. / Ne sont pas soumis à cette interdiction : / 1° Les rassemblements, réunions ou activités à caractère professionnel ; / 2° Les services de transport de voyageurs ; / 3° Les établissements recevant du public dans lesquels l'accueil du public n'est pas interdit en application du présent décret ; / 4° Les cérémonies funéraires organisées hors des établissements mentionnés au 3°, dans la limite de 50 personnes ; / 5° Les cérémonies publiques mentionnées par le décret du 13 septembre 1989 susvisé ; / 6° Les visites guidées organisées par des personnes titulaires d'une carte professionnelle ; / 7° Les compétitions et manifestations sportives soumises à une procédure d'autorisation ou de déclaration, dans la limite, pour les compétitions qui ne sont pas organisées au bénéfice des sportifs professionnels ou de haut niveau, de 50 sportifs par épreuve ; / 8° Les évènements accueillant du public assis, dans la limite de 1 000 personnes, organisés sur la voie publique ou dans un lieu ouvert au public. Une distance minimale d'un siège est laissée entre les sièges occupés par chaque personne ou chaque groupe jusqu'à six personnes venant ensemble ou ayant réservé ensemble ; / 9° Les réunions électorales organisées en plein air hors des établissements mentionnés au 3°, dans la limite de 50 personnes. "<br/>
<br/>
              4. Mme A..., première candidate dénommée de la liste présentée par le parti politique " La France insoumise " aux élections régionales en Ile-de-France qui doivent se dérouler le 20 et le 27 juin 2021, a décidé d'organiser le 13 juin prochain une réunion électorale sur la voie publique dans une commune de Seine-Saint-Denis. Sur la foi d'un courrier d'information du préfet du département lui rappelant exactement les dispositions applicables, elle a déduit que cette réunion ne pourrait rassembler légalement plus de 50 personnes, la privant d'utilité. Sur le fondement de l'article L. 521-2, Mme A..., et l'association politique " La France Insoumise " demandent, dans le dernier état de leurs écritures, au juge des référés du Conseil d'Etat de suspendre le 9° du III de l'article 3 du décret du 1er juin 2021 et d'enjoindre au Premier Ministre de modifier ces dispositions pour les adapter à la situation sanitaire.<br/>
<br/>
              5. La liberté de manifestation, et d'expression publique de l'opinion, tout particulièrement lorsqu'elles s'exercent dans le cadre d'une campagne électorale où elles participent à la formation des suffrages, à l'initiative des partis politiques mettant en oeuvre la mission constitutionnelle qui leur est confiée par l'article 3 de la constitution à fin de concourir à l'expression du suffrage, en exerçant cette activité librement, aux termes de la garantie donnée par le même article, constituent autant de libertés fondamentales. Leur exercice doit toutefois être concilié avec l'objectif de valeur constitutionnelle de protection de la santé, et plus généralement les exigences de l'ordre public. Les mesures que peuvent prendre à cette fin les pouvoirs publics doivent être nécessaires, adaptées et proportionnées à l'objectif poursuivi.<br/>
<br/>
              6. En temps normal, toute manifestation sur la voie publique, y compris les réunions électorales, sauf celles conformes aux usages locaux, doivent être déclarées, en application des articles L. 211-1 et suivants du code de la sécurité intérieure. Les dispositions de l'article 3 du décret du 1er juin 2021 ajoutent à cette exigence l'obligation d'informer l'autorité des mesures qui seront prises pour assurer le respect des mesures de protection de la santé publique définies à l'article 1 du même décret. Le 9° du III du même article 3 écarte cette obligation dans le cas où la manifestation envisagée ne réunirait que moins de 50 personnes. Il résulte des débats à l'audience que cet assouplissement, loin d'interdire toute réunion de plus de 50 personnes, qui demeurent possible en respectant les exigences du II de l'article 3, a été conçu par les pouvoirs publics dans le but de faciliter la tenue de réunions de petite taille qui ne leur paraissait pas, par l'effectif considéré, pouvoir contribuer de manière significative à la diffusion de l'épidémie. La suspension demandée de cette mesure aggraverait donc la contrainte sur la liberté d'organiser des manifestations par les partis politiques et ne serait donc aucunement de nature à préserver les libertés fondamentales objets de restrictions au regard de la crise sanitaire.<br/>
<br/>
              7. La modification de ces dispositions, demandée par voie de conséquence de leur suspension, porterait sur une élévation du seuil de participation en donnant le bénéfice. Si d'autres concentrations de personnes sur la voie publique sont autorisées à titre dérogatoire sans seuil de participation par le III du même article 3, il s'agit de catégories différentes qui justifient des règles distinctes de celles critiquées. Les réunions à caractère professionnel sont par définition placées sous le contrôle d'un ou plusieurs employeurs légalement tenus d'assurer la protection des salariés et des participants, alors qu'un parti politique n'exerce aucune autorité de cette nature sur les personnes se rendant librement à une manifestation. Lorsque d'autres catégories bénéficiant, depuis le 7 juin notamment, des seuils de participations plus élevées, notamment les cérémonies funéraires ou les compétitions sportives, ces seuils sont limitatifs, contrairement à celui fixé pour les réunions électorales, qui peuvent se tenir en respectant les formalités prévues au II de l'article 3 du décret du 1er juin 2021. S'il est soutenu que les grandes formations politiques disposent des moyens d'organiser des réunions dans des établissements recevant du public, alors que la faiblesse des moyens de partis plus petits le leur interdirait, cette circonstance, à la supposer établie, ne résulte nullement des dispositions attaquées. Quant au seuil de 50 participants, aucun des éléments résultant de l'instruction et des débats ne permet de le regarder comme disproportionné au regard des objectifs poursuivis et du contexte.<br/>
<br/>
              8. Il résulte de ce qui précède que, en l'absence d'une atteinte manifestement illégale à une liberté fondamentale, il n'y pas matière à faire usage des pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative. La requête de Mme A... et de l'association politique " La France insoumise " ne peut donc qu'être rejetée, y compris en tant qu'elle tend à ce que l'Etat leur verse une somme d'argent sur le fondement de l'article L. 761-1 du code de justice administrative dont les dispositions, dès lors que l'Etat n'est pas la partie perdante, y font obstacle.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... et de l'association politique " La France insoumise " est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A... et à l'association " La France insoumise ", ainsi qu'au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
