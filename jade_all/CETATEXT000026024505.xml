<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026024505</ID>
<ANCIEN_ID>JG_L_2012_05_000000329025</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/02/45/CETATEXT000026024505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 22/05/2012, 329025</TITRE>
<DATE_DEC>2012-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329025</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:329025.20120522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 juin et 14 septembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour le SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS (SDIS) DE LA NIEVRE, dont le siège est rue du Colonel Rimeilho à Varennes-Vauzelles (58640) ; le SDIS DE LA NIEVRE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06LY02484 du 14 avril 2009 par lequel la cour administrative d'appel de Lyon a, d'une part, annulé le jugement du tribunal administratif de Dijon du 12 octobre 2006 en ce qu'il a rejeté la demande de M. A...B...tendant à l'annulation pour excès de pouvoir de la décision du 27 septembre 2005 par laquelle le président du conseil d'administration du SDIS a mis fin à son activité de sapeur-pompier volontaire en tant qu'elle a rétroagi au 29 juin 2004, d'autre part, annulé la décision contestée en tant qu'elle a rétroagi à cette date ;<br/>
<br/>
              2°) de mettre à la charge de M. B...le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 99-1039 du 10 décembre 1999 ;<br/>
<br/>
              Vu l'arrêté du 6 mai 2000 fixant les conditions d'aptitude médicale des sapeurs-pompiers professionnels et volontaires et les conditions d'exercice de la médecine professionnelle et préventive au sein des services départementaux d'incendie et de secours ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat du SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS DE LA NIEVRE,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat du SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS DE LA NIEVRE ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 8 du décret du 10 décembre 1999 relatif aux sapeurs-pompiers volontaires : " Les sapeurs-pompiers volontaires sont engagés pour une période de cinq ans, tacitement reconduite. / (...) Le maintien et le renouvellement de l'engagement sont subordonnés à la vérification selon les modalités définies par arrêté du ministre chargé de la sécurité civile, des conditions d'aptitude physique et médicale de l'intéressé correspondant aux missions qui lui sont confiées " ; que l'article 44 du même décret dispose que : "  L'autorité territoriale peut résilier d'office l'engagement du sapeur-pompier volontaire : / 1° S'il ne satisfait plus à l'une des conditions prévues à l'article 6 après mise en oeuvre, le cas échéant, des dispositions de l'article 39 (...) " ; que l'article 6 du même décret subordonne l'engagement d'un sapeur-pompier volontaire à des conditions d'aptitude physique et médicale définies par arrêté du ministre chargé de la sécurité civile et correspondant aux missions effectivement confiées aux sapeurs-pompiers volontaires ; qu'aux termes de l'article 24 de l'arrêté du 6 mai 2000 fixant les conditions d'aptitude médicale des sapeurs-pompiers professionnels et volontaires et les conditions d'exercice de la médecine professionnelle et préventive au sein des services départementaux d'incendie et de secours : " En cas d'inaptitude médicale aux activités de sapeur-pompier volontaire, et après confirmation de cet état par le médecin-chef, ce dernier peut proposer au directeur départemental du service d'incendie et de secours la poursuite d'une activité adaptée, en précisant notamment les postes ou missions incompatibles avec son état de santé. / La confirmation de l'inaptitude ou de l'aptitude à poursuivre le service avec une activité adaptée doit faire l'objet, dans le délai maximum de deux mois, d'un examen du dossier du sapeur-pompier volontaire concerné par les membres de la commission d'aptitude aux fonctions de sapeur-pompier volontaire (...) " ; qu'il résulte de ces dispositions que la seule déclaration d'inaptitude médicale par la commission d'aptitude aux fonctions de sapeur-pompier volontaire n'est pas de nature à entraîner par elle-même, sans que l'autorité territoriale n'ait à porter aucune appréciation, la résiliation d'office de l'engagement du sapeur-pompier volontaire ;<br/>
<br/>
              Considérant que les décisions administratives ne peuvent légalement disposer que pour l'avenir ; que si l'annulation d'une décision ayant illégalement évincé un agent public oblige l'autorité compétente à réintégrer l'intéressé à la date de son éviction et à prendre rétroactivement les mesures nécessaires pour reconstituer sa carrière et le placer dans une position régulière, ladite autorité, lorsqu'elle reprend à la suite d'une nouvelle procédure une mesure d'éviction, le cas échéant sur un autre fondement, ne peut légalement donner à sa décision un effet rétroactif ; qu'il n'en va autrement que lorsque cette autorité, n'ayant à porter aucune appréciation sur les faits de l'espèce, est tenue de mettre un terme aux fonctions de l'intéressé à une date antérieure à sa décision ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après l'annulation par le tribunal administratif de Dijon de sa décision du 6 février 2004 par laquelle il avait décidé de ne pas renouveler l'engagement de M.B..., qui arrivait à son terme le 31 mars 2004, le président du conseil d'administration du SDIS a, par une décision du 27 septembre 2005 prise à la suite d'une nouvelle procédure, mis fin à l'engagement de M.  B...en qualité de sapeur-pompier volontaire à compter du 29 juin 2004, soit la date à laquelle la commission médicale d'aptitude a constaté l'inaptitude médicale définitive de l'intéressé ; qu'il résulte de ce qui précède qu'en jugeant que si l'annulation de la décision du 6 février 2004 ne faisait pas obstacle à ce qu'il soit mis fin à l'activité de l'intéressé pour inaptitude médicale définitive, l'arrêté prononçant la résiliation de son engagement ne pouvait légalement prendre effet à une date antérieure à sa notification, la cour administrative d'appel de Lyon a porté sur les pièces du dossier une appréciation souveraine exempte de dénaturation et n'a pas entaché son arrêt d'erreur de droit, dès lors que le président du conseil d'administration du SDIS n'était pas tenu de prendre une telle décision sans avoir à porter aucune appréciation sur les faits de l'espèce ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi du SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS DE LA NIEVRE doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS DE LA NIEVRE est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée au SERVICE DEPARTEMENTAL D'INCENDIE ET DE SECOURS DE LA NIEVRE et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. RÉTROACTIVITÉ. RÉTROACTIVITÉ ILLÉGALE. - MESURE D'ÉVICTION D'UN FONCTIONNAIRE À COMPTER DE LA DATE D'UNE MESURE D'ÉVICTION ANTÉRIEURE ANNULÉE PAR LE JUGE DE L'EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. - DÉCISION D'ÉVICTION D'UN AGENT PUBLIC - ANNULATION - NOUVELLE MESURE D'ÉVICTION APRÈS ANNULATION - POSSIBILITÉ DE CONFÉRER À CETTE MESURE UNE PORTÉE RÉTROACTIVE - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - COMPÉTENCE LIÉE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-13-02 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. EFFETS DES ANNULATIONS. - ANNULATION D'UNE DÉCISION AYANT ILLÉGALEMENT ÉVINCÉ UN AGENT PUBLIC - NOUVELLE MESURE D'ÉVICTION APRÈS ANNULATION - POSSIBILITÉ DE CONFÉRER À CETTE MESURE UNE PORTÉE RÉTROACTIVE - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - COMPÉTENCE LIÉE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-06-07-005 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. EFFETS D'UNE ANNULATION. - ANNULATION D'UNE DÉCISION AYANT ILLÉGALEMENT ÉVINCÉ UN AGENT PUBLIC - NOUVELLE MESURE D'ÉVICTION APRÈS ANNULATION - POSSIBILITÉ DE CONFÉRER À CETTE MESURE UNE PORTÉE RÉTROACTIVE - 1) PRINCIPE - ABSENCE [RJ1] - 2) EXCEPTION - COMPÉTENCE LIÉE.
</SCT>
<ANA ID="9A"> 01-08-02-02 L'administration ne peut légalement, lorsqu'elle prend à l'encontre d'un agent public une nouvelle mesure d'éviction du service à la suite de l'annulation d'une première mesure par le juge de l'excès de pouvoir, donner à sa décision un effet rétroactif de sorte que la seconde éviction prenne effet à compter de la date de la première. Il n'en va autrement que lorsque cette autorité, n'ayant à porter aucune appréciation sur les faits de l'espèce, est tenue de mettre un terme aux fonctions de l'intéressé à une date antérieure à sa décision.</ANA>
<ANA ID="9B"> 36-10 1) Si l'annulation d'une décision illégale d'éviction d'un agent public oblige l'autorité compétente à réintégrer l'intéressé à la date de son éviction et à reconstituer rétroactivement sa carrière, cette autorité, lorsqu'elle prend, à la suite d'une nouvelle procédure, une nouvelle mesure d'éviction, ne peut légalement donner à sa décision un effet rétroactif. 2) Il en va toutefois autrement lorsque cette autorité, n'ayant à porter aucune appréciation sur les faits de l'espèce, est tenue de mettre un terme aux fonctions de l'intéressé à une date antérieure à sa décision.</ANA>
<ANA ID="9C"> 36-13-02 1) Si l'annulation d'une décision illégale d'éviction d'un agent public oblige l'autorité compétente à réintégrer l'intéressé à la date de son éviction et à reconstituer rétroactivement sa carrière, cette autorité, lorsqu'elle prend, à la suite d'une nouvelle procédure, une nouvelle mesure d'éviction, ne peut légalement donner à sa décision un effet rétroactif. 2) Il en va toutefois autrement lorsque cette autorité, n'ayant à porter aucune appréciation sur les faits de l'espèce, est tenue de mettre un terme aux fonctions de l'intéressé à une date antérieure à sa décision.</ANA>
<ANA ID="9D"> 54-06-07-005 1) Si l'annulation d'une décision illégale d'éviction d'un agent public oblige l'autorité compétente à réintégrer l'intéressé à la date de son éviction et à reconstituer rétroactivement sa carrière, cette autorité, lorsqu'elle prend, à la suite d'une nouvelle procédure, une nouvelle mesure d'éviction, ne peut légalement donner à sa décision un effet rétroactif. 2) Il en va toutefois autrement lorsque cette autorité, n'ayant à porter aucune appréciation sur les faits de l'espèce, est tenue de mettre un terme aux fonctions de l'intéressé à une date antérieure à sa décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 27 mai 1977, Loscos, n° 93920, p. 249.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
