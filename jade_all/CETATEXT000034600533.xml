<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600533</ID>
<ANCIEN_ID>JG_L_2017_05_000000396669</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600533.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 05/05/2017, 396669, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396669</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2017:396669.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1601204 du 28 janvier 2016, enregistrée au secrétariat du contentieux le 2 février 2016, la présidente du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête que M. A...B...avait présentée à ce tribunal.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif le 22 janvier 2016, M. B...demande :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions, révélées par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 24 novembre 2015, par lesquelles les ministres de la défense et de l'intérieur lui ont refusé l'accès aux données susceptibles de le concerner et figurant dans les traitements automatisés de données de la direction de la protection et de la sécurité de la défense (DPSD), devenue direction du renseignement et de la sécurité de la défense (DRSD), et du service du renseignement territorial ;<br/>
<br/>
              2°) subsidiairement de procéder à la rectification des informations erronées le concernant susceptibles de figurer dans ces traitements.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ; <br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 mars 2017, présentée par le ministre de la défense ;<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. A...B..., et d'autre part, le ministre de l'intérieur, le ministre de la défense et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
              - le rapport de Mme Prada Bordenave, conseiller d'Etat, <br/>
              - et, hors la présence des parties, les conclusions de Mme Emmanuelle <br/>
Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 41 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications. / Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'État, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant. / Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi ". Aux termes de l'article 88 du décret du 20 octobre 2005 pris pour l'application de cette loi : " Aux termes de ses investigations, la commission constate, en accord avec le responsable du traitement, celles des informations susceptibles d'être communiquées au demandeur dès lors que leur communication ne met pas en cause les finalités du traitement, la sûreté de l'État, la défense ou la sécurité publique. Elle transmet au demandeur ces informations (...) Lorsque le responsable du traitement s'oppose à la communication au demandeur de tout ou partie des informations le concernant, la commission l'informe qu'il a été procédé aux vérifications nécessaires. / La commission peut constater en accord avec le responsable du traitement, que les informations concernant le demandeur doivent être rectifiées ou supprimées et qu'il y a lieu de l'en informer. En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. Lorsque le traitement ne contient aucune information concernant le demandeur, la commission informe celui-ci, avec l'accord du responsable du traitement. / En cas d'opposition du responsable du traitement, la commission se borne à informer le demandeur qu'il a été procédé aux vérifications nécessaires. / La réponse de la commission mentionne les voies et délais de recours ouverts au demandeur ".<br/>
<br/>
              2. L'article 26 de la loi du 6 janvier 1978 dispose que : " I. Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique ; (...) / L'avis de la commission est publié avec l'arrêté autorisant le traitement. / II. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement. / III. Certains traitements mentionnés au I et au II peuvent être dispensés, par décret en Conseil d'Etat, de la publication de l'acte réglementaire qui les autorise ; pour ces traitements, est publié, en même temps que le décret autorisant la dispense de publication de l'acte, le sens de l'avis émis par la commission (...) ". <br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure, issu de la loi du 24 juillet 2015 relative au renseignement, dispose que : " Le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, pour les traitements ou parties de traitements intéressant la sûreté de l'Etat dont la liste est fixée par décret en Conseil d'Etat " . L'article R. 841-2 du même code prévoit que : " Relèvent des dispositions de l'article L. 841-2 du présent code les traitements ou parties de traitements automatisés de données à caractère personnel intéressant la sûreté de l'Etat autorisés par les actes réglementaires ou dispositions suivants :/  (...)3° Décret autorisant la mise en oeuvre par la direction de la protection et de la sécurité de la défense d'un traitement automatisé de données à caractère personnel dénommé SIREX (...) ". <br/>
              En ce qui concerne les conclusions de M. B...concernant le fichier du renseignement territorial :<br/>
<br/>
              4. Le fichier du renseignement territorial ne relève pas des dispositions précitées de l'article R. 841-2 du code de justice administrative. Le contentieux de l'accès aux données contenues dans ce fichier relève du tribunal administratif de Paris. Selon les dispositions de l'article R. 351-4 du code de justice administrative : " Lorsque tout ou partie des conclusions dont est saisi (...) le Conseil d'Etat relève de la compétence d'une juridiction administrative, (...) le Conseil d'Etat, (...) , est compétent, nonobstant les règles de répartition des compétences entre juridictions administratives (....) pour constater qu'il n'y a pas lieu de statuer sur tout ou partie des conclusions ". Ainsi, alors même que la formation spécialisée du Conseil d'Etat n'est pas compétente pour connaître de conclusions concernant l'accès au  fichier du renseignement territorial elle peut néanmoins constater que celles-ci ont perdu leur objet. Or, le ministre de l'intérieur indique sans être contredit que M. B...a été informé par la Commission nationale de l'informatique et des libertés (CNIL) que le fichier du renseignement territorial ne contenait aucune information le concernant. Par suite, les conclusions dirigées par ce dernier contre le refus de lui communiquer les informations susceptibles de le concerner contenues dans ce fichier sont devenues sans objet et il n'y a plus lieu d'y statuer. <br/>
<br/>
              En ce qui concerne les conclusions de la requête de M. B...concernant le fichier de la DRSD : <br/>
<br/>
              5. L'article L. 773-8 du code de justice administrative, issu de la loi du 24 juillet 2015, dispose que : " Lorsqu'elle traite des requêtes relatives à la mise en oeuvre de l'article 41 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article <br/>
R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              6. Il ressort des pièces du dossier que M. B...a saisi la CNIL afin de pouvoir accéder aux données le concernant qui seraient contenues dans le fichier de la direction de la protection et de la sécurité de la défense (DPSD), devenue la direction du renseignement et de la sécurité de la défense (DRSD). La CNIL a désigné, en application de l'article 41 de la loi du 6 janvier 1978 précité, deux membres pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par une lettre du 24 novembre 2015, la présidente de la CNIL a informé M. B...de ce qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans apporter à l'intéressé d'autre information. Saisi par M.B..., le tribunal administratif de Paris a transmis au Conseil d'Etat le recours pour excès de pouvoir dirigé contre le refus, révélé par ce courrier du 24 novembre 2015, du ministre de la défense de lui donner accès aux mentions susceptibles de le concerner et figurant dans le fichier litigieux. M. B...demande, en outre, que les éléments inexacts le concernant qui seraient contenus dans ce fichier soient retirés.<br/>
<br/>
              7. Le ministre de la défense et la CNIL ont communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments relatifs à la situation de l'intéressé. Le ministre de la défense a, en outre, communiqué les actes réglementaires autorisant la création du fichier litigieux.<br/>
<br/>
              8. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales.<br/>
<br/>
              9. La formation spécialisée a procédé à l'examen de l'acte réglementaire autorisant la création du fichier de la DRSD ainsi que des éléments fournis par le ministre de la défense et la CNIL. Cet examen s'est déroulé selon les modalités décrites au point précédent et a révélé que des données concernant M. B...figuraient illégalement dans ce fichier. Par suite, il y a lieu, en l'espèce, d'ordonner l'effacement de ces données.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de M. B...concernant le fichier du renseignement territorial.<br/>
Article 2 : Il est enjoint au ministre de la défense (direction du renseignement et de la sécurité de la défense) de procéder à l'effacement des données concernant M. B...illégalement contenues dans le traitement de données de la direction du renseignement et de la sécurité de la défense.<br/>
Article 3 : La présente décision sera notifiée à M. A...B..., au ministre de la défense et au ministre de l'intérieur.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT - LOI N° 2015-912 DU 24 JUILLET 2015 - CONTRÔLE JURIDICTIONNEL [RJ1] - CONSTAT PAR LA FORMATION SPÉCIALISÉE QUE DES DONNÉES FIGURENT ILLÉGALEMENT DANS UN FICHIER - INJONCTION DE PROCÉDER À L'EFFACEMENT DES DONNÉES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-03 POLICE. ÉTENDUE DES POUVOIRS DE POLICE. - TECHNIQUES DE RENSEIGNEMENT (LOI N° 2015-912 DU 24 JUILLET 2015) - CONTRÔLE JURIDICTIONNEL - OFFICE DE LA FORMATION SPÉCIALISÉE - TRAITEMENTS AUTOMATISÉS INTÉRESSANT LA SÛRETÉ DE L'ETAT [RJ1] - CONSTAT PAR LA FORMATION SPÉCIALISÉE QUE DES DONNÉES FIGURENT ILLÉGALEMENT DANS UN FICHIER - INJONCTION DE PROCÉDER À L'EFFACEMENT DES DONNÉES.
</SCT>
<ANA ID="9A"> 26-07-06 Formation spécialisée ayant procédé à l'examen de l'acte réglementaire autorisant la création du fichier litigieux et des éléments fournis par le ministre et la Commission nationale de l'informatique et des libertés (CNIL). Cet examen a révélé que des données concernant le requérant figuraient illégalement dans le fichier litigieux. Par suite, il y a lieu d'ordonner l'effacement de ces données.</ANA>
<ANA ID="9B"> 49-03 Formation spécialisée ayant procédé à l'examen de l'acte réglementaire autorisant la création du fichier litigieux et des éléments fournis par le ministre et la Commission nationale de l'informatique et des libertés (CNIL). Cet examen a révélé que des données concernant le requérant figuraient illégalement dans le fichier litigieux. Par suite, il y a lieu d'ordonner l'effacement de ces données.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur l'office de la formation spécialisée, CE, formation spécialisée, 19 octobre 2016, M.,, n° 400688, p. 430.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
