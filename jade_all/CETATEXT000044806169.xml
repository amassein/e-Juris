<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806169</ID>
<ANCIEN_ID>JG_L_2021_12_000000440376</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806169.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/12/2021, 440376, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440376</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Bruno Delsol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440376.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le numéro 440376, par une requête enregistrée le 4 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société B... Avocat Victimes et Préjudices et M. C... B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-356 du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le numéro 440976, par une requête sommaire et un mémoire complémentaire, enregistrés les 2 juin et 2 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. P... M..., M. I... N..., Mme Z... V..., Mme X... G... et Mme L... E... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-356 du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le numéro 442327, par une requête et un mémoire, enregistrés le 31 juillet 2020 et le 15 avril 2021 au secrétariat du contentieux du Conseil d'Etat, Mme W... A..., M. AA... U... et Mme Y... O... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-356 du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              4° Sous le numéro 442361, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 août et 2 novembre 2020 et le 13 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association La Quadrature du Net demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-356 du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust " ; <br/>
<br/>
              2°) d'enjoindre au garde des sceaux, ministre de la justice, de supprimer les données recueillies depuis l'entrée en vigueur du décret attaqué, sous astreinte de 1 024 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 096 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              5° Sous le numéro 442935, par une requête enregistrée le 18 août 2020 au secrétariat du contentieux du Conseil d'Etat, l'association APF France handicap, la Fédération nationale des victimes d'attentats et d'accidents collectifs et l'Union nationale des associations de familles de traumatisés crâniens et de cérébro-lésés demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-356 du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros à verser à chacune des associations requérantes au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Delsol, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société B... Avocat Victimes et Préjudices et de M. B... et à la SCP Foussard, Froger, avocat de M. M..., de M. N..., de Mme V..., de Mme G... et de Mme E... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes de la société B... Avocat Victimes et Préjudices et M. B..., de M. M... et autres, de Mme A... et autres, de l'association La Quadrature du Net et de l'association APF France handicap et autres sont dirigées contre le même décret du 27 mars 2020 portant création d'un traitement automatisé de données à caractère personnel dénommé " DataJust ". Il y a lieu de les joindre pour statuer par une même décision.<br/>
<br/>
              2. Le décret attaqué autorise le garde des sceaux, ministre de la justice, à mettre en œuvre, pour une durée de deux ans, un traitement ayant pour finalité, aux termes de son article 1er, " le développement d'un algorithme devant servir à : / 1° La réalisation d'évaluations rétrospectives et prospectives des politiques publiques en matière de responsabilité civile ou administrative ; / 2° L'élaboration d'un référentiel indicatif d'indemnisation des préjudices corporels ; / 3° L'information des parties et l'aide à l'évaluation du montant de l'indemnisation à laquelle les victimes peuvent prétendre afin de favoriser un règlement amiable des litiges ; / 4° L'information ou la documentation des juges appelés à statuer sur des demandes d'indemnisation des préjudices corporels. ". Son article 2 prévoit que les données " sont extraites des décisions de justice rendues en appel entre le 1er janvier 2017 et le 31 décembre 2019 par les juridictions administratives et les formations civiles des juridictions judiciaires dans les seuls contentieux portant sur l'indemnisation des préjudices corporels ". Le même article fixe la liste des catégories de données concernées et précise qu'elles peuvent comprendre celles mentionnées à l'article 6 de la loi du 6 janvier 1978. Le décret prévoit que le droit d'information et le droit d'opposition des personnes dont les données sont collectées ne s'appliqueront pas. <br/>
<br/>
              Sur les interventions :<br/>
<br/>
              3. M. J... et autres d'une part, l'Union nationale des associations agréées d'usagers du système de santé d'autre part, justifient d'un intérêt suffisant à l'annulation du décret attaqué. Leurs interventions sont ainsi recevables.<br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              4. En premier lieu, aux termes de l'article 34 de la Constitution : " La loi fixe les règles concernant : les droits civiques et les garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques ". La loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés régit le traitement des données personnelles notamment dans le cadre du règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, dit règlement général sur la protection des données dit  " RGPD ". La création et la mise en œuvre d'un tel traitement sont subordonnées au respect de l'ensemble des garanties applicables prévues par cette loi, en particulier les principes, énumérés à son article 4, de licéité, tel qu'il est précisé à son article 5, de loyauté, de limitation des finalités, de minimisation des données, d'exactitude, de limitation de la conservation, d'intégrité et de confidentialité. <br/>
<br/>
              5. Le décret attaqué se borne à autoriser la collecte de données nécessaires au développement d'un algorithme en matière d'indemnisation du préjudice corporel sans déroger à la loi du 6 janvier 1978. Il n'a ni pour objet, ni pour effet de fixer des règles relatives aux garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques. Le moyen tiré de l'incompétence du Premier ministre pour adopter le décret attaqué, y compris en ce qu'il exclut l'exercice des droits d'information et d'opposition des personnes dont les données personnelles sont collectées, et alors même qu'initialement le gouvernement avait préparé un projet d'article législatif instituant un référentiel d'indemnisation, ne peut, en conséquence, qu'être écarté.<br/>
<br/>
              6. En deuxième lieu, il ressort des pièces versées au dossier par le ministre de la justice que le décret attaqué ne contient pas de disposition qui diffèrerait à la fois du projet initial du Gouvernement et du texte adopté par le Conseil d'Etat. Par suite, les requérants ne sont pas, en tout état de cause, fondés à soutenir que les règles gouvernant l'examen par le Conseil d'Etat des projets de décret auraient été méconnues.<br/>
<br/>
              7. En troisième lieu, les requérants se prévalent de l'irrégularité de l'avis de la Commission nationale de l'informatique et des libertés (CNIL). Toutefois, d'une part, la seule circonstance que cet avis ne comporte que la signature de sa présidente ne suffit pas à établir qu'il n'aurait pas été rendu en formation plénière dès lors qu'aucune disposition ni aucun principe n'impose d'autres signatures ni ne prévoit de mentions obligatoires devant assortir l'avis. Il s'ensuit que lorsque l'avis a la forme d'une " délibération " et mentionne qu'il a été rendu par la Commission, il ne saurait été regardé, en l'absence d'élément contraire, comme émanant de la seule présidente de la commission. D'autre part, il ressort des pièces du dossier qu'ont été publiés au Journal officiel du 29 mars 2020 le décret attaqué et la délibération n° 2020-002 du 9 janvier 2020 portant avis de la CNIL. Contrairement à ce que soutiennent les requérants, la circonstance que l'avis de la CNIL ait été publié après la signature du décret attaqué est, en tout état de cause, sans incidence sur la légalité de ce dernier. Il s'ensuit que le moyen tiré de l'irrégularité de l'avis de la CNIL doit être écarté. <br/>
<br/>
              8. En cinquième lieu, le moyen soulevé par l'association La Quadrature du Net et tiré de ce que tous les avis légalement requis n'auraient pas été recueillis n'est pas assorti des précisions suffisantes permettant d'en apprécier le bien-fondé. <br/>
<br/>
              9. En dernier lieu, l'article 22 de la Constitution dispose que " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution. ". S'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte. Le décret attaqué n'appelant aucune mesure réglementaire ou individuelle d'exécution de la part de la ministre des solidarités et de la santé, de la ministre de l'enseignement supérieur, de la recherche et de l'innovation, ni du ministre de l'intérieur, le moyen tiré du défaut de contreseing de ces ministres ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              10. Aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui ". Aux termes de l'article 8 de la charte des droits fondamentaux de l'Union européenne : " 1. Toute personne a droit à la protection des données à caractère personnel la concernant. / 2. Ces données doivent être traitées loyalement, à des fins déterminées et sur la base du consentement de la personne concernée ou en vertu d'un autre fondement légitime prévu par la loi. Toute personne a le droit d'accéder aux données collectées la concernant et d'en obtenir la rectification. (...) ". Le traitement en litige relève du règlement du 27 avril 2016 dit règlement général sur la protection des données. Il est soumis à l'article 4 I de la loi du 6 janvier 1978 qui exige que " Les données à caractère personnel doivent être : / 1° Traitées de manière loyale et licite (...) ; 2° Collectées pour des finalités déterminées, explicites et légitimes, et ne pas être traitées ultérieurement d'une manière incompatible avec ces finalités. (...) / 3° Adéquates, pertinentes et au regard des finalités pour lesquelles elles sont collectées, limitées à ce qui est nécessaire ou, pour les traitements relevant des titres III et IV, non excessives (...) ; / 5° Conservées sous une forme permettant l'identification des personnes concernées pendant une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées ". Il résulte de l'ensemble de ces textes que l'ingérence dans l'exercice du droit de toute personne au respect de sa vie privée que constituent la collecte, la conservation et le traitement, par une autorité publique, de données à caractère personnel, ne peut être légalement autorisée que si elle répond à des finalités légitimes et que le choix, la collecte et le traitement des données sont effectués de manière adéquate et proportionnée au regard de ces finalités.<br/>
<br/>
              En ce qui concerne les finalités du traitement :<br/>
<br/>
              11. En premier lieu, la formulation, par l'article 1er du décret attaqué, des finalités tendant à " la réalisation d'évaluations rétrospectives et prospectives des politiques publiques en matière de responsabilité civile ou administrative " ainsi qu'à la création d'un référentiel d'indemnisation est suffisamment précise. Le moyen tiré de ce qu'elle méconnaitrait l'article 4 précité de la loi du 6 janvier 1978 au motif qu'elle ne serait ni " déterminée ", ni " explicite" doit dès lors être écarté.  <br/>
<br/>
              12. En deuxième lieu, les requérants contestent les finalités de ce traitement aux motifs que l'algorithme qu'il a pour objet de développer serait tout à la fois contraire aux principes de l'individualisation et de la réparation intégrale des préjudices, inutile à raison de l'existence d'autres outils ayant la même finalité, et biaisé faute pour le traitement de prendre en compte les indemnités amiables ainsi que l'évolution du droit. Toutefois, il ressort des pièces du dossier que le traitement autorisé par le décret attaqué a pour objet la mise au point d'un algorithme destiné à l'élaboration d'un référentiel indicatif d'indemnisation des préjudices corporels ayant vocation à être utilisé pour évaluer ces préjudices dans le cadre du règlement tant amiable que juridictionnel des litiges. Il tend ainsi à assurer un accès plus facile à la jurisprudence sur l'indemnisation des préjudices corporels afin de garantir l'accessibilité et la prévisibilité du droit. Au surplus et à ce stade, ce traitement, dont la durée est réduite à deux ans, est limité à la phase de développement d'un outil d'intelligence artificielle, n'a qu'un caractère expérimental et n'a pas vocation, à ce stade, à être mis à la disposition des magistrats ou des parties. Il s'ensuit que le moyen tiré de ce que les finalités poursuivies par le traitement ne seraient pas légitimes doit être écarté.<br/>
<br/>
              13. En dernier lieu, en vertu du b) du 1 de l'article 5 du RGPD, les données dont la collecte et le traitement avaient déjà été prévues pour d'autres finalités ne doivent pas être traitées ultérieurement d'une manière incompatible avec celles-ci. Le point 4 de son article 6 précise à cet égard que " le responsable du traitement (...) tient compte, entre autres: a) de l'existence éventuelle d'un lien entre les finalités pour lesquelles les données à caractère personnel ont été collectées et les finalités du traitement ultérieur envisagé; b) du contexte dans lequel les données à caractère personnel ont été collectées, en particulier en ce qui concerne la relation entre les personnes concernées et le responsable du traitement; c) de la nature des données à caractère personnel, en particulier si le traitement porte sur des catégories particulières de données à caractère personnel, en vertu de l'article 9, ou si des données à caractère personnel relatives à des condamnations pénales et à des infractions sont traitées, en vertu de l'article 10; d) des conséquences possibles du traitement ultérieur envisagé pour les personnes concernées; e) de l'existence de garanties appropriées, qui peuvent comprendre le chiffrement ou la pseudonymisation ". <br/>
<br/>
              14. En l'espèce, si le décret attaqué prévoit que les données sont extraites des décisions de justice transmises par la Cour de cassation et le Conseil d'Etat parmi les décisions qu'ils ont été autorisés à collecter pour les besoins de leur activité juridictionnelle, les fins prévues par le décret attaqué sont liées aux finalités poursuivies par la collecte initiale dès lors que sont en cause, dans les deux cas, le règlement des litiges. Le décret prévoit en outre que les données relatives aux parties sont pseudonymisées. Le traitement prévu qui ne fait pas l'objet d'un déploiement n'est enfin pas susceptible d'avoir d'incidence sur l'indemnisation personnelle de ces dernières. Il s'ensuit que le nouveau traitement n'est en tout état de cause pas incompatible avec les finalités initiales au sens des dispositions rappelées au point précédent.<br/>
<br/>
              En ce qui concerne les données collectées :<br/>
<br/>
              Quant aux principes de minimisation et d'exactitude des données : <br/>
<br/>
              15. Les requérants ne peuvent utilement soutenir que le champ des données collectées serait insuffisant pour permettre la mise en œuvre d'un instrument opérationnel, ni que le décret serait, par suite, entaché d'une erreur manifeste d'appréciation.<br/>
<br/>
              16. La circonstance que les décisions annulées en cassation ne sont pas exclues de celles qui seront recueillies ne suffit pas à porter atteinte au principe d'exactitude des données collectées.  Par ailleurs, le traitement d'un grand nombre de décisions étant nécessaire à la fiabilité de l'algorithme, le décret attaqué ne porte pas atteinte au principe de minimisation des données.   <br/>
<br/>
              Quant aux données de santé : <br/>
<br/>
              17. L'article 9 du RGPD et l'article 6 de la loi du 6 janvier 1978 interdisent le traitement de données dites sensibles, dont font partie les données de santé, sauf, notamment, aux termes du g) du paragraphe 2 de l'article 9 du RGPD, lorsqu'il est nécessaire pour des motifs d'intérêt public important sur la base d'un droit proportionné à l'objectif d'intérêt public poursuivi.  <br/>
<br/>
              18. Il ressort des pièces du dossier qu'eu égard à son objet portant sur le développement d'un algorithme devant faciliter l'accès à la jurisprudence, le traitement autorisé par le décret attaqué répond à une nécessité justifiée par des motifs d'intérêt public important. Par ailleurs, aux termes de l'article 2 du décret attaqué, les noms et prénoms des personnes physiques parties aux instances ayant fait l'objet des décisions de justice rendues en appel entre le 1er janvier 2017 et le 31 décembre 2019 à partir desquelles les données nécessaires à la mise au point de l'algorithme sont extraites, doivent être occultés préalablement à leur transmission au secrétariat général du ministère de la justice. Compte-tenu de cette pseudonymisation des données collectées relatives aux parties aux décisions juridictionnelles objet du traitement, la collecte des données de santé sur les préjudices corporels subis, qui au demeurant n'est pas susceptible d'avoir d'incidence sur les personnes concernées dont le préjudice a déjà été indemnisé, ne saurait être regardée comme disproportionnée au regard de l'objectif d'intérêt public poursuivi.  <br/>
<br/>
              Quant aux autres données : <br/>
<br/>
              19. D'une part, si les requérants critiquent l'adéquation et la pertinence de la collecte des données relatives aux dépenses de santé, à la situation financière des victimes et des responsables, aux honoraires des médecins et experts ainsi qu'aux infractions et condamnations pénales auxquelles les arrêts civils et administratifs peuvent faire référence, ces éléments, qui font l'objet, ainsi qu'il a été dit, d'une pseudonymisation lorsqu'ils concernent les parties à l'instance, sont nécessaires à l'évaluation de l'indemnisation des préjudices corporels. <br/>
<br/>
              20. D'autre part, si sont aussi collectées les données d'identité des personnes physiques mentionnées dans les décisions de justice, autres que les parties, l'article 2 du décret attaqué dispose que  " Il est interdit de sélectionner dans le traitement une catégorie particulière de personnes à partir de ces seules données. " Ces données ne feront ainsi pas l'objet d'un traitement et il ressort des pièces du dossier que leur recueil répond à des contraintes techniques. Comme les autres données à caractère personnel, elles seront effacées au plus tard au bout de deux ans en vertu de l'article 4 du décret attaqué et, conformément à son article 3, elles ne seront accessibles qu'à un nombre restreint d'agents précisément désignés, à raison de leurs attributions et dans la limite du besoin d'en connaître.<br/>
<br/>
              21. Enfin, dès lors que les décisions de justice font l'objet d'une pseudonymisation des données personnelles des parties à l'instance, la collecte de leur numéro a uniquement pour objet de répondre aux demandes des personnes dont les données ont été collectées tendant à l'exercice de leur droit d'accès, de rectification ou de limitation. <br/>
<br/>
              22. Il résulte de ce qui précède que le choix, la collecte et le traitement des données enregistrées dans le traitement autorisé par le décret attaqué sont effectués de manière adaptée, nécessaire et proportionnée au regard des finalités poursuivies par le traitement et ne portent pas atteinte à l'article 8 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              En ce qui concerne les restrictions apportées aux droits des personnes dont les données sont traitées:<br/>
<br/>
              Quant au consentement :<br/>
<br/>
              23. Selon le point 1 de l'article 6 du RGPD, le traitement de données sans le consentement de la personne peut être autorisé lorsque, notamment, le traitement est nécessaire à l'exécution d'une mission d'intérêt public ou relevant de l'exercice de l'autorité publique. Selon le dernier alinéa du point 3 de ce même article, le droit appliqué doit répondre à un objectif d'intérêt public et être proportionné à l'objectif légitime poursuivi.<br/>
<br/>
              24. Dès lors que le traitement autorisé par le décret attaqué a pour objet, afin de faciliter l'accès à la jurisprudence, la mise au point d'un algorithme destiné à l'élaboration d'un référentiel indicatif d'indemnisation des préjudices corporels pouvant être utilisé pour évaluer ces préjudices dans le cadre du règlement tant amiable que juridictionnel des litiges, il est nécessaire à l'exécution d'une mission d'intérêt public au sens de l'article 6 du RGPD. Par ailleurs, ainsi qu'il a été dit plus haut, les données relatives aux parties seront pseudonymisées. Le moyen tiré de l'illégalité de l'absence de consentement des personnes dont les données sont collectées doit par suite être écarté. <br/>
<br/>
              Quant au droit d'information : <br/>
<br/>
              25. Il résulte des dispositions du b) du point 5 de l'article 14 du RGPD que l'obligation d'informer la personne concernée par la collecte des données ne s'applique pas notamment lorsque et dans la mesure où la fourniture de telles informations se révèle impossible ou exigerait des efforts disproportionnés. Dans ce cas, le responsable du traitement doit rendre les informations publiquement disponibles. <br/>
<br/>
              26. Eu égard au grand nombre des décisions juridictionnelles à traiter, il ne peut pas être soutenu que l'information individuelle de chaque personne concernée n'exigerait pas d'efforts disproportionnés. Ainsi, le moyen tiré de ce que l'exclusion du droit à l'information serait excessive ou méconnaitrait l'article 8 de la Charte des droits fondamentaux de l'Union européenne doit être écarté. Si les dispositions citées au point précédent mettent à la charge du responsable du traitement l'obligation de rendre l'information publiquement disponible, elles n'imposent pas que l'acte portant création du traitement rappelle cette obligation ni qu'il détermine les modalités de sa mise en œuvre. <br/>
<br/>
              Quant au droit d'opposition : <br/>
<br/>
              27. Le 1 de l'article 23 du RGPD prévoit que le droit national peut apporter des limitations au droit d'opposition reconnu par son article 21 lorsqu'une telle limitation respecte l'essence des libertés et droits fondamentaux et qu'elle constitue une mesure nécessaire et proportionnée dans une société démocratique pour garantir, notamment, des objectifs importants d'intérêt public général. <br/>
<br/>
              28. En premier lieu, contrairement à ce que soutiennent les requérants, les dispositions susrappelées n'imposent pas l'intervention d'une loi pour autoriser l'exclusion du droit d'opposition aux données faisant l'objet d'un traitement. En l'espèce, son application a été écartée par une disposition expresse de l'acte instaurant le traitement conformément à ce que prévoit l'article 56 de la loi du 6 janvier 1978. <br/>
<br/>
              29. En second lieu, la dérogation au droit d'opposition est justifiée par la nécessité de disposer d'une base de données d'indemnisation aussi représentative et complète que possible pour répondre à des finalités qui, ainsi qu'il a été dit au point 12, sont légitimes.  <br/>
<br/>
              Quant aux droits d'accès, de rectification et de limitation :  <br/>
<br/>
              30. L'article 6 du décret attaqué prévoit que les droits d'accès, de rectification et de limitation s'exercent auprès du ministre de la justice dans les conditions prévues respectivement aux articles 15, 16 et 18 du RGPD. Si les requérants soutiennent que, faute d'avoir été informées de la collecte de leurs données, les personnes concernées ne seront pas en mesure d'exercer ces droits, ce moyen doit être écarté dès lors que le responsable du traitement est tenu, ainsi qu'il a été rappelé au point 25, d'assurer l'information publique nécessaire sur la collecte des données. <br/>
<br/>
              En ce qui concerne les destinataires et la sécurité des données collectées :<br/>
<br/>
              31. L'article 3 du décret attaqué dispose : " Seuls ont accès, à raison de leurs attributions et dans la limite du besoin d'en connaître, aux données à caractère personnel et informations enregistrées dans le présent traitement : / 1° Les agents du ministère de la justice affectés au service chargé des développements informatiques du secrétariat général du ministère de la justice, individuellement désignés par le secrétaire général ; / 2° Les agents du bureau du droit des obligations individuellement désignés par le directeur des affaires civiles et du sceau ". Ainsi, les personnes ayant accès aux données collectées sont limitativement énumérées. Le décret interdit en outre à ces agents de les utiliser à d'autres fins que celles prévues. Par ailleurs et ainsi qu'il a déjà été dit, ils ne recevront que des données pseudonymisées s'agissant des parties à l'instance. Par suite, les requérants ne peuvent pas soutenir que des agents auraient un accès trop large à leurs données, ni que le secret médical serait méconnu.<br/>
<br/>
              32. Les conditions du respect des règles de sécurité des données relèvent des obligations du responsable du traitement et ne peuvent dès lors être utilement invoquées contre l'acte d'autorisation. <br/>
<br/>
              33. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre de la justice dans la requête 440976, que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent. Par suite, les conclusions à fin d'injonction présentées par l'association La Quadrature du Net ne peuvent qu'être rejetées, ainsi que celles présentées par les requérants au titre de l'article L. 761-1 du code de justice administrative et celles présentées au même titre par l'Union nationale des associations agréées d'usagers du système de santé, qui au surplus n'est pas partie dans la présente instance.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de M. J... et autres et l'intervention de l'Union nationale des associations agréées d'usagers du système de santé sont admises.   <br/>
Article 2 : Les requêtes de la société B... Avocat Victimes et Préjudices et autre, de M. M... et autres, de Mme A... et autres, de l'association APF France Handicap et autres et de l'association La Quadrature du Net sont rejetées.<br/>
Article 3 : Les conclusions présentées par l'Union nationale des associations agréées d'usagers du système de santé au titre de l'article L 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société B... Avocat Victimes et Préjudices, à M. P... M..., à Mme W... A... née D... et à l'association APF France Handicap, premiers dénommés chacun en ce qui le concerne pour l'ensemble des requérants, à l'association La Quadrature du Net, au Premier ministre, au garde des sceaux, ministre de la justice, à M. AB... J..., premier dénommé pour l'ensemble des intervenants venus au soutien de la requête de la société B... Avocat Victimes et Préjudices, et à l'Union nationale des associations agréées d'usagers du système de santé.  <br/>
<br/>
<br/>
Délibéré à l'issue de la séance du 17 décembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. Q... K..., M. Frédéric Aladjidi, présidents de chambre ; Mme S... F..., M. T... H..., Mme Anne Egerszegi, conseillers d'Etat et M. Bruno Delsol, conseiller d'Etat-rapporteur.<br/>
<br/>
              Rendu le 30 décembre 2021<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
<br/>
 		Le rapporteur : <br/>
      Signé : M. Bruno Delsol<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme R... AC...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
