<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105101</ID>
<ANCIEN_ID>JG_L_2013_10_000000347038</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 21/10/2013, 347038, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347038</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:347038.20131021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 25 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la Confédération paysanne, dont le siège est 104 rue Robespierre à Bagnolet (93170) ; la Confédération paysanne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 22 décembre 2010 portant homologation de l'avenant n° 15 à l'accord interprofessionnel du 10 février 1976 portant création d'une cotisation au bénéfice du Centre national interprofessionnel de l'économie laitière <br/>
(CNIEL) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Vu l'arrêt C-677/11 du 30 mai 2013 de la Cour de justice de l'Union européenne ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, Auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article L. 632-12 du code rural et de la pêche maritime : " Les accords nationaux ou régionaux conclus dans le cadre de l'organisation interprofessionnelle constituée entre les producteurs de lait, les groupements coopératifs agricoles laitiers et les industries de transformation du lait par les organisations les plus représentatives de ces professions peuvent être homologués par arrêtés conjoints du ministre de l'agriculture et du ministre chargé de l'économie. " ; qu'en application de ces dispositions et de celles des articles 1er et 3 du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, M. A...B..., nommé directeur général des politiques agricole, agroalimentaire et des territoires au ministère de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire par un décret du 1er décembre 2010, publié au Journal officiel du 2 décembre 2010, a donné délégation de signature à M. C...D..., ingénieur en chef des ponts, des eaux et des forêts, dans la limite des attributions de la sous-direction des produits et marchés, par une décision du 3 décembre 2010, publiée au Journal officiel du 8 décembre 2010 ; que l'arrêté attaqué relevant des attributions de cette sous-direction, M. C... D...était dès lors compétent pour le signer au nom du ministre de l'agriculture ; que Mme F...E..., nommée directrice générale de la concurrence, de la consommation et de la répression des fraudes par un décret du 16 avril 2009, publié au Journal officiel du 17 avril 2009, avait de ce fait qualité pour le signer au nom du ministre chargé de l'économie ; que le moyen tiré de l'incompétence des signataires de l'arrêté attaqué doit, par suite, être écarté ;<br/>
<br/>
              2. Considérant, en deuxième lieu, qu'aux termes de l'article L. 632-13 du code rural et de la pêche maritime : " L'organisation interprofessionnelle est habilitée à prélever sur tous les producteurs et transformateurs de lait des cotisations résultant des accords homologués selon la procédure fixée à l'article L. 632-12 et dont le montant maximal doit être approuvé par le ministre de l'agriculture après avis du ministre chargé de l'économie. " ; que ces dispositions ont pour objet de permettre à l'organisation interprofessionnelle laitière de prélever, sur tous les membres des professions la constituant, des cotisations résultant des accords homologués selon les modalités fixées par l'article L. 632-12 précité du même code ; que ces cotisations sont perçues par un organisme de droit privé ; qu'elles tendent au financement d'activités menées, en faveur de leurs membres et dans le cadre défini par le législateur, par l'organisation interprofessionnelle laitière ; que ces cotisations sont acquittées par les membres de cette organisation ; que, par suite, ainsi que l'a jugé le Conseil constitutionnel lorsqu'il s'est prononcé sur les cotisations volontaires obligatoires de droit commun, dont le régime est analogue, dans sa décision n° 2011-221 QPC du 17 février 2012, elles ne constituent pas des impositions de toutes natures ; qu'ainsi, doivent être écartés les moyens tirés de ce que l'arrêté attaqué aurait été pris par des autorités incompétentes et serait entaché d'illégalité dès lors que la cotisation volontaire obligatoire qu'il institue constituerait une taxe fiscale dont l'assiette, le taux et les modalités de recouvrement devraient être fixées par la loi ;<br/>
<br/>
              3. Considérant, en troisième lieu, qu'aux termes de l'article L. 632-3 du code rural et de la pêche maritime : " Les accords conclus dans le cadre d'une organisation interprofessionnelle reconnue peuvent être étendus, pour une durée déterminée, en tout ou partie, par l'autorité administrative compétente dès lors qu'ils prévoient des actions communes ou visant un intérêt commun conformes à l'intérêt général et compatibles avec la législation de l'Union européenne. " ; que ni cet article, ni les autres dispositions législatives et réglementaires de ce même code ne prévoient l'obligation pour les accords homologués de mentionner avec précision les actions financées par une contribution volontaire obligatoire ; que si l'" instruction conjointe pour l'extension et l'homologation des accords conclus par les interprofessions agricoles et aquacoles " du ministre chargé de l'agriculture et du ministre chargé de l'économie du 15 mai 2007 prévoit que les interprofessions détaillent de manière exhaustive l'objet des accords, notamment lorsqu'il porte sur des actions financées par une contribution volontaire obligatoire, elle n'exige pas que le détail de ces actions soit mentionné dans le texte de l'accord lui-même, mais seulement que le dossier de demande d'homologation soit accompagné d'un certain nombre de pièces, notamment d'une note explicative de la mesure et d'un budget prévisionnel aussi détaillé que possible indiquant, pour chaque année, les actions financées par cette contribution ainsi que, si ces documents n'ont pas déjà été transmis annuellement, les comptes financiers de l'interprofession (bilan et compte de résultat) faisant ressortir clairement les actions financées par cette contribution au titre de l'exercice précédent ;<br/>
<br/>
              4. Considérant qu'en l'espèce, l'avenant n° 15 à l'accord du 10 février 1976, homologué par l'arrêté attaqué se réfère à " l'ensemble des missions de l'interprofession laitière en matière de recherche collective et de promotion des produits laitiers " et qu'il ressort des pièces du dossier que la demande d'homologation de l'avenant était accompagnée d'une note intitulée " stratégie de l'interprofession laitière pour les années 2011-2012-2013 " détaillant les actions que le CNIEL envisageait de financer par la contribution volontaire obligatoire prélevée sur le fondement de cet avenant ; que, dès lors, contrairement à ce que soutient la Confédération paysanne, l'accord homologué par l'arrêté attaqué ne méconnaît sur ce point, en tout état de cause, ni les dispositions précitées de l'article L. 632-3 du code rural ni les exigences de l'instruction précitée du 15 mai 2007 ;<br/>
<br/>
              5. Considérant, en quatrième lieu, que si la Confédération paysanne soutient que, contrairement à ce que prévoit l'instruction précitée du 15 mai 2007, les producteurs n'auraient pas la possibilité de répercuter le montant de la contribution volontaire obligatoire sur les prix de vente des produits qu'ils commercialisent puisque l'accord du 10 février 1976 portant création d'une cotisation au bénéfice du CNIEL stipule dans son article 2 que la cotisation " est déduite mensuellement des sommes versées aux producteurs en rémunération de leurs livraisons de lait et de crème ", le prélèvement de cette contribution par les transformateurs ne porte, en tout état de cause, pas atteinte à la liberté contractuelle régissant la fixation du prix de vente du lait et de la crème entre les producteurs et les transformateurs ;<br/>
<br/>
              6. Considérant que dans l'arrêt C-677/11 du 30 mai 2013 par lequel la Cour de justice de l'Union européenne s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que le paragraphe 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne doit être interprété en ce sens que la décision d'une autorité nationale étendant à l'ensemble des professionnels d'une filière agricole un accord qui institue une cotisation dans le cadre d'une organisation interprofessionnelle reconnue par l'autorité nationale et la rend ainsi obligatoire en vue de permettre la mise en oeuvre d'actions de communication, de promotion, de relations extérieures, d'assurance qualité, de recherche et de défense des intérêts du secteur concerné ne constitue pas un élément d'une aide d'Etat ; qu'il résulte de l'interprétation ainsi donnée par la Cour que la cotisation interprofessionnelle instaurée par l'avenant à l'accord interprofessionnel que la décision litigieuse n'est pas constitutive d'une aide d'Etat au sens du paragraphe 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne ; qu'il suit de là que la requérante n'est pas fondée à soutenir que la décision d'homologation litigieuse est irrégulière dès lors, d'une part, qu'elle n'a pas fait l'objet de la notification à la Commission exigée par l'article 108 du traité sur le fonctionnement de l'Union européenne et, d'autre part, que les règles communautaires de passation des marchés publics n'ont pas été respectées pour la sélection des prestataires de services chargés de mettre en oeuvre les actions financées par la cotisation volontaire obligatoire ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la Confédération paysanne n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Centre national interprofessionnel de l'économie laitière au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Confédération paysanne est rejetée. <br/>
Article 2 : Les conclusions présentées par le Centre national interprofessionnel de l'économie laitière au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la Confédération paysanne au ministre de l'agriculture, de l'agroalimentaire et de la forêt, au ministre de l'économie, des finances et au Centre national interprofessionnel de l'économie laitière.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
