<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043053022</ID>
<ANCIEN_ID>JG_L_2021_01_000000448696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/05/30/CETATEXT000043053022.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 21/01/2021, 448696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:448696.20210121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Alpes-Maritimes d'enregistrer sa demande d'asile et de lui délivrer l'attestation correspondante dans un délai de huit jours à compter de la notification de l'ordonnance à intervenir. <br/>
<br/>
              Par une ordonnance n° 2004961 du 7 décembre 2020, le juge des référés du tribunal administratif de Nice a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 15 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'enjoindre au préfet des Alpes-Maritimes de procéder à l'enregistrement de sa demande d'asile et de lui délivrer l'attestation de demandeur d'asile dans un délai de cinq jours à compter de la notification de l'ordonnance, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, elle se trouve dans l'impossibilité de déposer sa demande d'asile alors même que la France est, eu égard à l'expiration du délai de six mois à compter de la date d'acceptation par l'Espagne de reprise en charge, responsable de l'examen de sa demande et, d'autre part, l'Office français de l'immigration et de l'intégration lui a notifié son intention de suspendre le bénéfice des conditions matérielles d'accueil ;<br/>
              - le délai de six mois étant expiré, il est porté une atteinte grave et manifestement illégale au droit d'asile dès lors que le refus du préfet des Alpes-Maritimes d'enregistrer sa demande d'asile fait obstacle à l'examen de sa demande, à la délivrance d'une attestation de demande d'asile ainsi qu'à la possibilité de solliciter le statut de réfugié ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté personnelle et au droit au respect de la vie privée ;<br/>
              - le préfet des Alpes-Maritimes a commis une erreur de droit, d'une part, en la déclarant en fuite au sens de l'article 29 du règlement " Dublin III ", alors qu'il était parfaitement informé, dès le 2 octobre 2020, de l'endroit où elle se trouvait et des raisons de son absence à l'aéroport, liée à l'opération que devait subir l'un de ses fils et, d'autre part, en n'apportant pas la preuve de l'information de l'Espagne de la prolongation du délai de transfert, avant l'expiration de celui-ci.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003, tel que modifié par le règlement (UE) n° 118/2014 du 30 janvier 2014 ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2003 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes du second alinéa de l'article L. 523-1 du code de justice administrative : " Les décisions rendues en application de l'article L. 521-2 sont susceptibles d'appel devant le Conseil d'Etat dans les quinze jours de leur notification (...) ". Aux termes de l'article 6 du décret du 18 novembre 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif : " Lorsqu'une partie est représentée par un avocat, la notification prévue à l'article R. 751-3 du code de justice administrative est valablement accomplie par l'expédition de la décision à son mandataire (...) ".<br/>
<br/>
              3. Il ressort des pièces de la procédure que l'ordonnance rendue le 7 décembre 2020 par le juge des référés du tribunal administratif de Nice a été notifiée à l'avocat de Mme A... le 22 décembre 2020, au moyen de l'application Télérecours, ainsi qu'à l'intéressée le 28 décembre 2020. L'appel de Mme A... n'ayant été enregistré au secrétariat du contentieux du Conseil d'Etat que le 15 janvier 2021, soit après l'expiration du délai de quinze jours imparti, il est manifestement tardif et, par suite, irrecevable.<br/>
<br/>
              4. Il résulte de tout ce qui précède que la requête de Mme A..., y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
Copie en sera adressée au préfet des Alpes-Maritimes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
