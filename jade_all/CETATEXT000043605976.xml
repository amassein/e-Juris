<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043605976</ID>
<ANCIEN_ID>JG_L_2021_05_000000452293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/60/59/CETATEXT000043605976.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/05/2021, 452293, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452293.20210517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté d'expulsion pris à son encontre par le préfet des Bouches-du-Rhône le 27 décembre 2017 ainsi que de la décision de placement en centre de rétention administrative prise à son encontre par le même préfet le 29 avril 2021 et, d'autre part, d'enjoindre au préfet des Bouches du Rhône de le remettre en liberté et de lui délivrer une autorisation provisoire de séjour. <br/>
<br/>
              Par une ordonnance n° 2103820 du 5 mai 2021, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 5 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. Kaba demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance attaquée est entachée d'irrégularité faute d'avoir pris en compte son mémoire complémentaire et récapitulatif du 2 mai 2021 ; <br/>
              - la condition d'urgence est satisfaite, malgré la prolongation de 28 jours décidée par le juge judiciaire, dès lors qu'il a été placé en centre de rétention administrative le 30 avril 2021 en vue de son expulsion, laquelle peut intervenir à tout moment ;<br/>
              - il est porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir, au respect de sa vie privée et familiale, à sa liberté personnelle et à son droit de séjourner et de demeurer sur le territoire français en qualité de citoyen français ;<br/>
              - l'arrêté d'expulsion, qui ne lui a pas été notifié, est entaché d'illégalité dès lors que, d'une part, il a été pris par une autorité incompétente et que, d'autre part, il est français. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 521-2 du code de justice administrative dispose que : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. En premier lieu, l'article R. 742-2 du code de justice administrative, applicable en matière de référé en vertu de l'article R. 522-11 du même code, prévoit que : " Les ordonnances mentionnent le nom des parties, l'analyse des conclusions ainsi que les visas des dispositions législatives ou réglementaires dont elles font application (...) ". Il résulte de ces dispositions que, si une ordonnance de référé doit en principe porter mention, outre du nom des parties et des dispositions législatives et réglementaires dont elle fait application, de l'ensemble des mémoires produits avant la clôture de l'instruction, l'omission du visa d'un tel mémoire n'est pas, par elle-même, de nature à l'entacher d'irrégularité lorsque celui-ci ne comporte pas de conclusions nouvelles. Ces dispositions n'ont en outre pas pour effet d'imposer au juge des référés d'analyser ou de viser, dans sa décision, les moyens développés par les parties à l'appui de leurs conclusions, auxquels le juge devra toutefois répondre, en tant que de besoin, au titre de la motivation de son ordonnance. <br/>
<br/>
              3. Il ressort des pièces de la procédure devant le juge des référés du tribunal administratif de Marseille que le mémoire " complémentaire et récapitulatif " de M. Kaba, enregistré le 2 mai 2021 au greffe du tribunal, ne comportait pas de conclusions nouvelles. Par suite, l'absence de mention de ce mémoire dans les visas de l'ordonnance attaquée n'est pas, en elle-même, de nature à en entraîner son annulation.<br/>
<br/>
              4. En second lieu, M. Kaba a demandé au juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, la suspension de l'exécution de l'arrêté d'expulsion pris à son encontre le 27 décembre 2017 par le préfet des Bouches du Rhône ainsi que de la décision du même préfet en date du 29 avril 2021 de le placer en rétention dans l'attente de l'exécution de l'arrêté d'expulsion.<br/>
<br/>
              5. D'une part, en application des dispositions des articles L. 551-1 et L. 551-2 du code de l'entrée et du séjour des étrangers en France et du droit d'asile, désormais reprises à l'article L. 741-1 du même code, le placement en rétention d'un étranger peut être ordonné, par l'autorité administrative, lorsque cet étranger, faisant l'objet d'un arrêté d'expulsion, ne peut quitter immédiatement le territoire français. Le maintien en centre de rétention affecte la liberté individuelle de la personne qui en fait l'objet. Pour cette raison, au terme d'un délai de quarante-huit heures en centre de rétention, l'article L. 742-1 du même code, reprenant les dispositions de l'article L. 552-1, prévoit que le juge des libertés et de la détention du tribunal de grande instance est saisi aux fins de prolongation de la rétention. Il résulte de ces dispositions que la décision de l'autorité administrative ordonnant le placement en rétention ne peut produire effet que pendant quarante-huit heures et, qu'au terme de ce délai, seule une décision de l'autorité judiciaire peut maintenir un étranger en rétention, sans l'intervention d'aucune autorité administrative. Ainsi, l'étranger maintenu en rétention, par ordonnance du juge des libertés et de la détention, ne peut utilement contester au-delà de ce terme l'arrêté par lequel le préfet l'a placé en rétention. Par suite, saisi dans ces conditions, sur le fondement de l'article L. 521-2 du code de justice administrative, de conclusions dirigées contre la décision préfectorale, le juge des référés doit constater qu'elles sont devenues sans objet. <br/>
<br/>
              6. Si M. Kaba a demandé au juge des référés la suspension de l'exécution de la décision du préfet des Bouches du Rhône en date du 29 avril 2021 de le placer en rétention dans l'attente de l'exécution de l'arrêté d'expulsion du 27 décembre 2017, toutefois, par une ordonnance du 2 mai 2021, le juge des libertés et de la détention a ordonné la prolongation du maintien en rétention de l'intéressé jusqu'au 30 mai 2021. Ainsi M. Kaba ne demeure placé en rétention que par l'effet de cette ordonnance, confirmée par le magistrat délégué par le premier président de la cour d'appel d'Aix en Provence le 3 mai 2021. Par suite, les conclusions par lesquelles M. Kaba a demandé au juge des référés du tribunal administratif de Marseille de suspendre l'exécution de son maintien en rétention et d'enjoindre au préfet de mettre fin à son placement en rétention étaient devenues sans objet postérieurement à la date du 2 mai 2021.<br/>
<br/>
              7. D'autre part, à supposer même que l'arrêté d'expulsion en date du 27 décembre 2017 n'ait pas été régulièrement notifié à M. Kaba le 26 septembre 2018, ce dernier se borne à en contester le bien-fondé au motif qu'il posséderait la nationalité française. Toutefois, il n'apporte aucun élément probant permettant de justifier de ce qu'il disposerait de cette nationalité. Il n'établit dès lors pas, en tout état de cause, l'existence d'une atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête d'appel de M. Kaba dirigée contre l'ordonnance du juge des référés du tribunal administratif de Marseille est manifestement dénuée de pertinence et peut être rejetée sur le fondement de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. Kaba est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Mohamed Kaba. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
