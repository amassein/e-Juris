<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031355821</ID>
<ANCIEN_ID>JG_L_2015_10_000000363879</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/58/CETATEXT000031355821.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 22/10/2015, 363879, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363879</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP ROUSSEAU, TAPIE ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:363879.20151022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. G...E..., demeurant ... ; M. E...demande au Conseil d'Etat : <br/>
<br/>
              1°) de condamner les héritiers de Me Guinard, décédé, à lui verser une indemnité de 150 000 euros en réparation du préjudice qu'il estime avoir subi en raison de la faute commise par ce dernier en formant tardivement un recours gracieux contre un état exécutoire émis à son encontre ;<br/>
<br/>
              2°) de mettre hors de cause Me D...A...; <br/>
<br/>
              3°) de mettre à la charge des héritiers de Me F...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ; <br/>
<br/>
              Vu l'ordonnance du 10 septembre 1817, notamment son article 13 modifié par le décret n° 2002-76 du 11 janvier 2002 ;  <br/>
<br/>
              Vu l'arrêté du 13 novembre 1973 portant homologation des modifications apportées au statut du personnel administratif de l'assemblée permanente des chambres de commerce et d'industrie, des chambres régionales de commerce et d'industrie et des chambres de commerce et d'industrie ; <br/>
<br/>
              Vu l'arrêté du 26 mars 1991 portant approbation des modifications apportées par la commission paritaire nationale au statut du personnel administratif des chambres de commerce et d'industrie ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.E..., à la SCP Didier, Pinet, avocat de Me D...A...et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que M. E...a occupé, du 1er février 1986 au 25 mars 1991, les fonctions de secrétaire général de la chambre régionale de commerce et d'industrie de Languedoc-Roussillon puis, du 26 mars 1991 au 30 juin 1996, date de son licenciement, les fonctions de directeur général ; que la chambre régionale de commerce et d'industrie a émis à son encontre, le 5 juillet 1999, un état exécutoire en vue du recouvrement forcé de la somme de 864 345,05 francs, correspondant aux avantages injustifiés dont il avait bénéficié au titre de ses fonctions successives ; que Me Guinard, avocat au Conseil d'Etat et à la Cour de cassation, a formé au nom de M.E..., par lettre recommandée avec accusé de réception adressée le 10 septembre 1999 au président de la chambre, un recours gracieux contre cet état exécutoire ; que ce recours n'est toutefois parvenu à son destinataire que le 14 septembre 1999 ; que, par une ordonnance du 28 décembre 2004, le président de la troisième chambre du tribunal administratif de Montpellier a rejeté la requête présentée par Me Guinard pour M. E... contre l'état exécutoire du 5 juillet 1999, au motif que le recours gracieux contre cette décision avait été formé tardivement ; que, par un arrêt du 20 mai 2008, devenu définitif, la cour administrative d'appel de Marseille a rejeté la requête formée par MeA..., successeur de M. Guinard, contre cette ordonnance ; que M. E...demande, en application de l'article 13 de l'ordonnance du 10 septembre 1817 modifiée relative aux avocats au Conseil d'Etat et à la Cour de cassation, de condamner, à titre principal, Me A...en sa qualité de successeur de Me Guinard, à titre subsidiaire, M.B..., héritier de Me Guinard, en réparation du préjudice qu'il estime avoir subi du fait de la perte de chance d'obtenir l'annulation de l'état exécutoire litigieux ; <br/>
<br/>
              2. Considérant qu'en s'abstenant de faire les diligences nécessaires pour que le président de la chambre de commerce et d'industrie soit saisi du recours gracieux contre le titre exécutoire litigieux dans le délai de recours, Me Guinard a commis une faute de nature à engager sa responsabilité professionnelle à l'égard de M. E...; <br/>
<br/>
              3. Considérant, toutefois, que M. E...n'est fondé à demander l'engagement de la responsabilité professionnelle de Me Guinard en réparation du préjudice qu'il estime avoir subi du fait de la présentation tardive de son recours gracieux contre l'état exécutoire du 5 juillet 1999 que dans la mesure où celle-ci a entraîné pour lui la perte d'une chance sérieuse d'obtenir l'annulation de cet acte ; <br/>
<br/>
              4. Considérant que M. E...soutient, en premier lieu, que le courrier du président de la chambre régionale de commerce et d'industrie du Languedoc-Roussillon accompagnant l'état exécutoire litigieux serait insuffisamment motivé et entaché d'erreur de droit ; que ce courrier d'accompagnement est toutefois sans incidence sur la légalité de l'état exécutoire contesté ; <br/>
<br/>
              5. Considérant, en second lieu, que ni les dispositions de l'arrêté du 13 novembre 1973 portant homologation des modifications apportées au statut du personnel administratif de l'assemblée permanente des chambres de commerce et d'industrie, des chambres régionales de commerce et d'industrie et des chambres de commerce et d'industrie, ni celles de l'arrêté du 26 mars 1991 portant approbation des modifications apportées par la commission paritaire nationale au statut du personnel administratif des chambres de commerce et d'industrie, n'autorisent un secrétaire général ou un directeur général à bénéficier d'avantages au titre de frais dépourvus de lien avec le service ; qu'il résulte de l'instruction que les sommes réclamées par la chambre régionale de commerce et d'industrie ne présentaient aucun lien avec le service ; que, par suite, la présentation tardive du recours gracieux contre l'état exécutoire du 5 juillet 1999 n'a pas pu entraîner pour M. E...la perte d'une chance sérieuse d'en obtenir l'annulation ; <br/>
<br/>
              6. Considérant qu'il résulte ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par MeA..., que la requête de M. E...doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. E...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à M. G...E..., M. C...B...et à Me D...A....<br/>
 Copie en sera adressée pour information au conseil de l'Ordre des avocats au Conseil d'Etat et à la Cour de cassation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
