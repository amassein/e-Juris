<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028908387</ID>
<ANCIEN_ID>JG_L_2014_05_000000376083</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/83/CETATEXT000028908387.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 07/05/2014, 376083, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376083</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:376083.20140507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 et 14 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1401171 du 20 février 2014 par laquelle le juge des référés du tribunal administratif de Marseille a rejeté sa demande tendant, d'une part, à la suspension de l'exécution de la décision par laquelle le maire de la commune de Digne-les-Bains a refusé de reconnaître l'imputabilité de sa maladie au service et l'a placé en congé de maladie ordinaire et, d'autre part, à ce qu'il soit enjoint à l'autorité compétente de lui accorder le régime des accidents de service sous astreinte de 70 euros par jour de retard à compter du septième jour suivant la décision à intervenir ;<br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions présentées devant le juge des référés du tribunal administratif de Marseille ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Digne-les-Bains le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 87-602 du 30 juillet 1987 ;<br/>
<br/>
              Vu le décret n° 92-1194 du 4 novembre 1992 ;<br/>
<br/>
              Vu l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, avocat de M. B...et à la SCP Hémery, Thomas-Raquin, avocat de la commune de Digne-les-Bains ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'aux termes de l'article L. 522-1 de ce code : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale. Lorsqu'il lui est demandé de prononcer les mesures visées aux articles L. 521-1 et L. 521-2, de les modifier ou d'y mettre fin, il informe sans délai les parties de la date et de l'heure de l'audience publique (...) " ; qu'aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M.B..., adjoint technique stagiaire de la commune de Digne-les-Bains, a saisi le tribunal administratif de Marseille, le 14 février 2014, d'une demande tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution de la décision du 13 novembre 2013 par laquelle le maire de la commune de Digne-les-Bains a refusé de reconnaître l'imputabilité de sa maladie au service et l'a placé en congé de maladie ordinaire ; que M. B...se pourvoit en cassation contre l'ordonnance du 20 février 2014 par laquelle le juge des référés du tribunal administratif de Marseille a, sur le fondement de l'article L. 522-3 du code de justice administrative, rejeté sa demande ; <br/>
<br/>
              3. Considérant qu'en vertu du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, le fonctionnaire en activité a droit à des congés de maladie dont la durée totale peut atteindre un an en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions, avec l'intégralité de son traitement pendant une durée de trois mois, puis un traitement réduit de moitié pendant les neuf mois suivants ; que le second alinéa de ce 2° précise toutefois que si la maladie provient d'un accident survenu dans le service ou à l'occasion de l'exercice de ses fonctions, l'intéressé conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service et qu'il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident ; qu'en vertu de l'article 7 du décret du 4 novembre 1992 fixant les dispositions communes applicables aux fonctionnaires stagiaires de la fonction publique territoriale, les dispositions du 2° de l'article 57 de la loi du 26 janvier 1984 sont applicables aux fonctionnaires stagiaires de la fonction publique territoriale ;<br/>
<br/>
              4. Considérant qu'en estimant que la demande de suspension présentée par le requérant serait par elle-même sans incidence sur sa situation alors qu'il ressortait des pièces du dossier, d'une part, que le requérant était dans une situation d'une grande précarité financière ayant justifié la mise en oeuvre d'une procédure de surendettement et, d'autre part, que l'exécution de la décision dont la suspension était demandée avait pour conséquence de le priver de la moitié de son traitement, le juge des référés du tribunal administratif de Marseille a dénaturé les pièces du dossier ; que l'ordonnance attaquée doit être, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, annulée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de statuer sur la demande de suspension de M. B...;<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              6. Considérant, d'une part, comme il l'a été dit au point 4, qu'il ressort des pièces du dossier, en premier lieu, que M. B...se trouve dans une situation de grande précarité financière et, en second lieu, que la réduction de moitié de son traitement lui cause un préjudice grave et immédiat ; que si la commune de Digne-les-Bains soutient que M. B...bénéficie d'une garantie de maintien de son traitement dans le cadre d'un contrat conclu avec la mutuelle nationale territoriale, il n'est pas établi que cette garantie couvrirait la totalité de son traitement ; que, dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie ; <br/>
<br/>
              7. Considérant, d'autre part, qu'aux termes de l'article 16 du décret du 30 juillet 1987 pris pour l'application de la loi n° 84-53 du 26 janvier 1984 et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux : " Sous réserve du deuxième alinéa du présent article, la commission de réforme (...) est obligatoirement consultée dans tous les cas où un fonctionnaire demande le bénéfice des dispositions de l'article 57 (2°, 2e alinéa) de la loi du 26 janvier 1984 susvisée. (...) / Lorsque l'administration est amenée à se prononcer sur l'imputabilité au service d'une maladie ou d'un accident, elle peut, en tant que de besoin, consulter un médecin expert agréé. / La commission de réforme n'est pas consultée lorsque l'imputabilité au service d'une maladie ou d'un accident est reconnue par l'administration. / (...) " ; qu'aux termes de l'article 14 de l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière : " Le secrétariat de la commission de réforme convoque les membres titulaires et l'agent concerné au moins quinze jours avant la date de la réunion. ( ...) " ; qu'aux termes de l'article 16 du même arrêté : " Dix jours au moins avant la réunion de la commission, le fonctionnaire est invité à prendre connaissance, personnellement ou par l'intermédiaire de son représentant, de son dossier, dont la partie médicale peut lui être communiquée, sur sa demande, ou par l'intermédiaire d'un médecin ; il peut présenter des observations écrites et fournir des certificats médicaux. / La commission entend le fonctionnaire, qui peut se faire assister d'un médecin de son choix. Il peut aussi se faire assister par un conseiller " ;<br/>
<br/>
              8. Considérant que les moyens tirés de ce que la décision contestée est irrégulière en ce que elle a été prise avant l'avis de la commission de réforme et que le requérant n'a été ni informé de ce qu'il pouvait prendre connaissance de son dossier ni convoqué devant cette commission sont de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de l'acte attaqué ; <br/>
<br/>
              9. Considérant, dès lors, que M. B...est fondé à demander la suspension de l'exécution de la décision du 13 novembre 2013 par laquelle le maire de la commune de Digne-les-Bains a refusé de reconnaître l'imputabilité de sa maladie au service et l'a placé en congé de maladie ordinaire ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              10. Considérant que cette mesure de suspension implique, d'une part, que l'administration statue à nouveau sur la demande de reconnaissance de l'imputabilité au service de la maladie de M. B...; qu'elle implique, d'autre part, que l'administration rétablisse provisoirement son plein traitement au requérant ; qu'elle n'implique pas nécessairement, en revanche, que l'administration reconnaisse à l'intéressé le bénéfice du régime des accidents de service ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par M. B...; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. B..., qui n'est pas la partie perdante dans la présente instance, la somme réclamée par la commune de Digne-les-Bains ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Digne-les-Bains, en application des mêmes dispositions, le versement à M. B... de la somme de 4 500 euros, pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Marseille du 20 février 2014 est annulée. <br/>
Article 2 : L'exécution de la décision du 13 novembre 2013 par laquelle le maire de la commune de Digne-les-Bains a refusé de reconnaître l'imputabilité de la maladie de M. B... au service et l'a placé en congé de maladie ordinaire est suspendue. <br/>
Article 3 : Il est enjoint au maire de la commune de Digne-les-Bains de procéder au réexamen de la demande de M. B...et de rétablir pour celui-ci son plein traitement à titre provisoire. <br/>
Article 4 : La commune de Digne-les-Bains versera à M. B...la somme de 4 500 euros au titre de l'article L. 761 1 du code de justice administrative. <br/>
Article 5 : Le surplus des conclusions de la demande présentée par M. B...devant le juge des référés du tribunal administratif de Marseille est rejeté. <br/>
Article 6 : La présente décision sera notifiée à M. A...B...et à la commune de Digne-les-Bains.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
