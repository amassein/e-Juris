<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030704432</ID>
<ANCIEN_ID>JG_L_2015_06_000000384698</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/70/44/CETATEXT000030704432.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 03/06/2015, 384698, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384698</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384698.20150603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 23 septembre 2014 et 3 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 46550 GEND/DPMGN/SDAP/BCHAN du 28 juillet 2014 par laquelle le ministre de la défense lui a infligé la sanction de " blâme du ministre " ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur, dans un délai d'un mois à compter de la notification de la décision à intervenir, de retirer de tous les dossiers administratifs du requérant toute pièce relative à la sanction qui lui a été infligée, de les détruire et d'en donner attestation ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que M. B...A..., lieutenant-colonel de gendarmerie, coordonnateur, au moment des faits litigieux du centre de coopération policière franco-brésilien installé à Saint-Georges de l'Oyapock (Guyane), créé dans le cadre de l'accord de partenariat et de coopération en matière de sécurité publique signé par la France et le Brésil le 12 mars 1997, demande l'annulation pour excès de pouvoir de la décision du 28 juillet 2014 par laquelle le ministre de la défense lui a infligé la sanction de " blâme du ministre " ; <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4137-15 du code de la défense : " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit, seul ou accompagné d'un militaire en activité de son choix sur les faits qui lui sont reprochés devant l'autorité militaire de premier niveau dont il relève. Au préalable, un délai de réflexion, qui ne peut être inférieur à un jour franc, lui est laissé pour organiser sa défense. / Lorsque la demande de sanction est transmise à une autorité militaire supérieure à l'autorité militaire de premier niveau, le militaire en cause peut également s'expliquer par écrit sur ces faits auprès de cette autorité supérieure. L'explication écrite de l'intéressé ou la renonciation écrite à l'exercice du droit de s'expliquer par écrit est jointe au dossier transmis à l'autorité militaire supérieure. / Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner. " ; qu'aux termes de l'article R. 4137-16 du même code : " Lorsqu'un militaire a commis une faute ou un manquement, il fait l'objet d'une demande de sanction motivée qui est adressée à l'autorité militaire de premier niveau dont il relève, même si elle émane d'une autorité extérieure à la formation. / L'autorité militaire de premier niveau entend l'intéressé, vérifie l'exactitude des faits, et, si elle décide d'infliger une sanction disciplinaire du premier groupe, arrête le motif correspondant à la faute ou au manquement et prononce la sanction dans les limites de son pouvoir disciplinaire. / Si l'autorité militaire de premier niveau estime que la gravité de la faute ou du manquement constaté justifie soit une sanction disciplinaire du premier groupe excédant son pouvoir disciplinaire, soit une sanction du deuxième ou troisième groupe, la demande de sanction est adressée à l'autorité militaire de deuxième niveau dont relève l'autorité militaire de premier niveau même si le militaire fautif a changé de formation administrative durant cette période. " ; qu'aux termes de l'article R. 4137-17 : " Lorsque l'autorité militaire de deuxième niveau qui reçoit une demande de sanction du premier groupe estime que cette sanction est justifiée, elle inflige une telle sanction. Si la sanction disciplinaire du premier groupe envisagée excède son pouvoir disciplinaire, elle transmet la demande de sanction à l'autorité compétente. / Cette autorité est l'autorité militaire de troisième niveau dont relève le militaire s'il s'agit d'un militaire du rang, le ministre de la défense s'il s'agit d'un officier, d'un sous-officier ou s'il s'agit d'un militaire du rang ne relevant d'aucune autorité militaire de troisième niveau. " ; qu'en vertu de l'article R. 4137-25 du code de la défense, seul le ministre de la défense est compétent pour prononcer à l'égard d'un officier la sanction d'un " blâme du ministre " ;<br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions précitées imposent que le militaire ayant fait l'objet d'une demande de sanction puisse présenter ses explications écrites et être entendu avant que ne statue l'autorité militaire compétente pour le sanctionner ; que M. A... ne soutient ni même n'allègue qu'il a été privé de cette garantie et qu'il n'a pu, en particulier, présenter ses explications devant les autorités militaires de premier et deuxième niveau ; que M. A...soutient qu'il n'a pas eu connaissance d'un troisième avis de l'autorité militaire de premier niveau et d'un avis de l'autorité militaire de deuxième niveau, pourtant visés par la décision contestée ; que toutefois, d'une part, ces avis sont simplement constitutifs des actes par lesquels l'autorité militaire initialement saisie transmet la demande de sanction à l'autorité militaire supérieure compétente pour prendre la sanction susceptible d'être infligée à l'intéressé, compte tenu de la nature des faits ou du comportement qui lui sont reprochés, aucune disposition du code de la défense n'imposant au demeurant leur communication au militaire ; que, d'autre part, il ressort des pièces du dossier que ces avis ne comportaient aucun élément qui n'aurait pas été porté à sa connaissance ; que, par suite, le moyen tiré de l'irrégularité de la procédure ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, en second lieu, que si M. A...soutient que le message " évènement grave " envoyé par le commandant du groupement de Gendarmerie de la Guyane à la direction générale de la Gendarmerie Nationale ainsi que certaines pièces du procès-verbal n° 448/2013 issu de l'enquête judiciaire ne lui ont pas été communiqués, il ne ressort pas des pièces du dossier que ces éléments étaient au nombre des pièces et documents au vu desquels il était envisagé de le sanctionner et qu'ils auraient dû en conséquence être portés à sa connaissance en application des dispositions précitées de l'article R. 4137-15 du code de la défense ; qu'il ressort en outre des pièces du dossier que, contrairement à ce que soutient M. A..., le Procureur de la République près le tribunal de grande instance de Cayenne a, dès le 26 juin 2013, soit avant la demande de sanction de l'autorité militaire de premier niveau du 13 août 2013, autorisé, sur le fondement des dispositions de l'article R. 156 du code de procédure pénale, la transmission à celle-ci des procès-verbaux issus de l'enquête judiciaire concernant un incident impliquant M. A... et deux de ses amies brésiliennes ; que, par suite et en tout état de cause, M. A...n'est pas fondé à soutenir que ces procès-verbaux ont été irrégulièrement recueillis par l'autorité militaire de premier niveau qui a proposé de le sanctionner ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre " ; <br/>
<br/>
              6. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le lieutenant-colonel A...était, au moment des faits qui lui sont reprochés, coordonnateur français du centre de coopération policière franco-brésilien installé à Saint-Georges de l'Oyapock, dont une des missions est de lutter contre l'immigration clandestine ; qu'il est constant que M. A... fréquentait publiquement une étrangère en situation irrégulière et que des photos du couple prises dans le logement de fonction de M.A..., ont été diffusées sur internet ; que M. A... fréquentait également une jeune fille de quinze ans, étrangère dont la régularité de la situation n'était pas établie, et avec laquelle il avait reconnu un " flirt " ; que M. A...a également été impliqué dans un incident violent en pleine rue opposant la jeune femme et la jeune mineure qu'il fréquentait ; que, d'une part, en estimant que les faits reprochés au requérant, qui ont eu pour effet de jeter le discrédit sur le centre de coopération franco-brésilien dont M. A...était, pour la France, le coordonnateur, et plus particulièrement sur sa mission de lutte contre l'immigration irrégulière, constituaient des fautes de nature à justifier une sanction et relevaient des manquements à l'honneur, à la probité ou aux bonnes moeurs qui, en vertu du second alinéa de l'article R. 4137-23 du code de la défense, sont exclus de l'effacement d'office prévu au premier alinéa du même article au 1er janvier de la cinquième année suivant celle au cours de laquelle les sanctions ont été prononcées, l'autorité investie du pouvoir disciplinaire ne les a pas inexactement qualifiés ; que, d'autre part, eu égard à la nature de ces faits, à la méconnaissance qu'ils traduisent, de la part de M.A..., des responsabilités éminentes qui étaient les siennes et compte tenu de ce qu'ils ont porté sérieusement atteinte à la dignité et à la crédibilité de ses fonctions, M. A...ne peut soutenir que le ministre de la défense aurait pris une sanction disproportionnée en décidant de prononcer à son égard un " blâme du ministre " ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
