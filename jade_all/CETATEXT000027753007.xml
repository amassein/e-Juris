<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027753007</ID>
<ANCIEN_ID>JG_L_2013_07_000000366203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/30/CETATEXT000027753007.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 25/07/2013, 366203, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:366203.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 20 février 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300236 du 5 février 2013 par laquelle le juge des référés du tribunal administratif d'Orléans a rejeté, en application de l'article L. 522-3 du code de justice administrative, sa demande tendant à la suspension de la décision du 10 décembre 2012 de la délégation territoriale Ouest du Conseil national des activités privées de sécurité lui refusant la délivrance d'une carte professionnelle relative aux activités de sécurité ; <br/>
<br/>
              2°) statuant en référé, d'ordonner la suspension de l'exécution de la décision du 10 décembre 2012 ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité intérieure ; <br/>
<br/>
              Vu la loi n° 83-629 du 12 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ; <br/>
<br/>
              Vu le décret n° 2011-1919 du 22 décembre 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de Mme A...et à Me Blondel, avocat du Conseil national des activités privées de sécurité ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 633-3 du code de la sécurité intérieure, dont les dispositions sont issues de l'article 33-7 de la loi du 12 juillet 1983 réglementant les activités de sécurité privée : " Tout recours contentieux formé par une personne physique ou morale à l'encontre d'actes pris par une commission régionale d'agrément et de contrôle est précédé d'un recours administratif préalable devant la Commission nationale d'agrément et de contrôle, à peine d'irrecevabilité du recours contentieux. " ; qu'aux termes de l'article 10 du décret du 22 décembre 2011 relatif au Conseil national des activités privées de sécurité et modifiant certains décrets portant application de la loi n° 83-629 du 12 juillet 1983 : " La Commission nationale d'agrément et de contrôle : / (...) / 2° Statue sur les recours administratifs préalables formés à l'encontre des décisions des commissions régionales et interrégionales, sur le fondement de l'article 33-7 de la loi du 12 juillet 1983 susvisée " ; qu'aux termes de l'article 29 du même décret : " Le recours administratif préalable obligatoire devant la Commission nationale d'agrément et de contrôle prévu à l'article 33-7 de la loi du 12 juillet 1983 susvisée peut être exercé dans les deux mois de la notification, par la commission régionale ou interrégionale d'agrément et de contrôle, de la décision contestée. Cette notification précise les délais et les voies de ce recours. " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 21 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Sauf dans les cas où un régime de décision implicite d'acceptation est institué dans les conditions prévues à l'article 22, le silence gardé pendant plus de deux mois par l'autorité administrative sur une demande vaut décision de rejet./ Lorsque la complexité ou l'urgence de la procédure le justifie, des décrets en Conseil d'Etat prévoient un délai différent. " ; <br/>
<br/>
              3. Considérant qu'en l'absence de dispositions particulières dérogeant à l'article 21 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, le silence gardé pendant deux mois par la Commission nationale d'agrément et de contrôle du Conseil national des activités privées de sécurité sur le recours administratif préalable dont elle est saisie en vertu de l'article L. 633-3 du code de la sécurité intérieure donne naissance à une décision de rejet qui est susceptible de recours ; que la suspension d'une décision prise par une commission régionale d'agrément et de contrôle peut être demandée au juge des référés, alors même que la contestation de cette décision est soumise à l'exercice d'un recours administratif préalable devant la Commission nationale d'agrément et de contrôle, dès lors que l'intéressé justifie devant le juge des référés de l'introduction de ce recours préalable ; que c'est à tort que le juge des référés a opposé à la demande de suspension dont il était saisi la circonstance qu'elle n'était pas assortie d'une requête distincte à fin d'annulation de la décision litigieuse et l'a considérée, pour ce motif, comme manifestement irrecevable, alors que la requérante avait justifié de la saisine de la Commission nationale d'agrément et de contrôle ;<br/>
<br/>
              4. Considérant qu'en raison des pouvoirs conférés à la Commission nationale d'agrément et de contrôle par l'article L. 633-3 du code de la sécurité intérieure, les décisions par lesquelles elle rejette, implicitement ou expressément, les recours introduits devant elle se substituent aux décisions des commissions régionales ou interrégionales d'agrément et de contrôle ; que, par suite, lorsqu'un requérant a présenté au juge des référés une demande tendant à la suspension de l'exécution de la décision d'une commission régionale ou interrégionale d'agrément et de contrôle refusant de lui accorder une carte professionnelle et qu'il a également saisi de ce refus, comme il en a l'obligation, la Commission nationale d'agrément et de contrôle, il lui appartient, lorsqu'est intervenue une décision implicite ou explicite de rejet par cette commission, à peine d'irrecevabilité de sa demande de suspension, de présenter contre cette dernière décision, d'une part, de nouvelles conclusions tendant à sa suspension, d'autre part, une requête tendant à son annulation ; que l'intervention de cette décision postérieurement à l'introduction du pourvoi contre l'ordonnance du juge des référés du tribunal administratif relative à la demande de suspension de l'exécution de la décision de la commission régionale ou interrégionale d'agrément et de contrôle rend sans objet le pourvoi dirigé contre cette ordonnance ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme A...a saisi la Commission nationale d'agrément et de contrôle auprès du Conseil national des activités privées de sécurité par lettre recommandée avec accusé de réception en date du 18 janvier 2013, reçue le 24 janvier suivant ; qu'il résulte de ce qui a été énoncé au point 3 qu'une décision implicite de rejet est née le 24 mars 2013 du silence gardé pendant plus de deux mois par la Commission nationale d'agrément et de contrôle sur le recours administratif préalable de Mme A...; que cette décision est née postérieurement à l'introduction du pourvoi formé, le 20 février 2013, contre l'ordonnance du juge des référés du tribunal administratif d'Orléans en date du 5 février 2013 ; qu'il résulte de ce qui a été énoncé au point 4 que les conclusions du pourvoi en cassation dirigées contre cette ordonnance sont devenues sans objet ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme que Mme A...demande au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
  Article 1er : Il n'y a pas lieu de statuer sur le pourvoi de MmeA.... <br/>
<br/>
  Article 2 : Les conclusions présentées pour Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
  Article 3 : La présente décision sera notifiée à Mme B...A..., au Conseil national des activités privées de sécurité et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
