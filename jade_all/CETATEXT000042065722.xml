<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065722</ID>
<ANCIEN_ID>JG_L_2020_06_000000421146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065722.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/06/2020, 421146, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421146.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              	M. et Mme A... B... ont demandé au tribunal administratif de Grenoble de prononcer la réduction des cotisations primitives d'impôt sur le revenu auxquelles ils ont été assujettis au titre de l'année 2012. Par un jugement n° 1400593 du 3 mars 2016, le tribunal administratif de Grenoble a rejeté leur demande.<br/>
<br/>
              	Par un arrêt n° 16LY01487 du 3 avril 2018, la cour administrative d'appel de Lyon a réformé ce jugement et réduit la cotisation d'impôt sur le revenu de M. et Mme B... au titre de l'année 2012 à concurrence de la somme de 11 398 euros. <br/>
<br/>
              	Par un pourvoi et deux mémoires en réplique, enregistrés les 1er juin et 30 octobre 2018 et le 11 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              	1°) d'annuler les articles 1 à 3 de cet arrêt ;<br/>
<br/>
              	2°) réglant l'affaire au fond, de rejeter les conclusions de la requête d'appel de M. et Mme B....<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - la loi n° 2011-1977 du 28 décembre 2011 ;<br/>
              - le décret n° 2012-547 du 23 avril 2012 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de M. et Mme A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. et Mme B... ont déclaré, au titre de leurs revenus de l'année 2012, une réduction d'impôt à raison d'investissements réalisés en outre-mer d'un montant de 81 581 euros. Toutefois, pour l'établissement de leur imposition, l'administration fiscale a plafonné cette réduction d'impôt à hauteur de la somme de 69 713 euros sur le fondement de l'article 199 undecies D du code général des impôts. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 3 avril 2018 par lequel la cour administrative d'appel de Lyon a réformé le jugement du 3 mars 2016 du tribunal administratif de Grenoble rejetant la demande des contribuables tendant à la réduction de leur imposition et a réduit la cotisation d'impôt sur le revenu de M. et Mme B... au titre de l'année 2012 à concurrence de la somme de 11 398 euros. <br/>
<br/>
              2. L'article 199 undecies D du code général des impôts, dans sa rédaction issue du décret du 23 avril 2012 pris pour l'application de l'article 83 de la loi du 28 décembre 2011 de finances pour 2012, dispose que : " I. _ 1. La somme des réductions d'impôt sur le revenu mentionnées aux articles 199 undecies A, 199 undecies B et 199 undecies C et des reports de ces réductions d'impôts, dont l'imputation est admise pour un contribuable au titre d'une même année d'imposition, ne peut excéder un montant de 40 000 &#128; comprenant dans l'ordre d'imputation suivant : / a) La somme de la réduction d'impôt sur le revenu mentionnée à l'article 199 undecies C et des reports de cette réduction d'impôt, dans la limite de 40 000 &#128; ; / b) La somme des réductions d'impôt sur le revenu mentionnées aux articles 199 undecies A et 199 undecies B et des reports de ces réductions d'impôt, dans la limite de 30 600 &#128; diminuée du montant dont l'imputation a été effectuée au a du présent 1. (...) ". En application du IV de l'article 83 de la loi du 28 décembre 2011 de finances pour 2012, les nouveaux plafonds de réduction ou de crédit d'impôt admis en imputation sont applicables à compter de l'imposition des revenus de l'année 2012 pour les dépenses payées à compter du 1er janvier 2012. Le b) du 1 du I de l'article 199 undecies D du code général des impôts, dans sa version immédiatement antérieure applicable pour les dépenses payées à compter du 1er janvier 2011, prévoyait l'imputation de " la somme des réductions d'impôt sur le revenu mentionnées aux articles 199 undecies A et 199 undecies B et des reports de ces réductions d'impôt, dans la limite de 36 000 euros diminuée du montant dont l'imputation a été effectuée au a du présent 1 ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont notamment déclaré, au titre de l'année 2012, d'une part, une réduction d'impôt résultant d'un investissement, entrant dans le champ de l'article 199 undecies B du code général des impôts, réalisé outre-mer en 2012, et, d'autre part, un report de réduction d'impôt résultant d'un investissement, entrant dans le champ du même article, réalisé outre-mer en 2011. Pour faire application des plafonds institués par l'article 199 undecies D du code général des impôts, cité au point 2, il convenait d'abord de prendre en compte les dépenses payées en 2012 au titre des investissements outre-mer auxquelles il fallait faire application du nouveau plafond de 30 600 euros institué par la loi du 28 décembre 2011 de finances pour 2012. Il y avait lieu ensuite de tenir compte du report de réduction d'impôt résultant des dépenses payées avant le 1er janvier 2012 auquel il fallait appliquer l'ancien plafond de 36 000 euros. Il s'ensuit qu'en jugeant que lorsque le montant d'un plafond a été modifié, les différents plafonds doivent être successivement appliqués du plus récent au plus ancien, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que le ministre de l'action et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                        D E C I D E :<br/>
                        --------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à M. et Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à M. et Mme A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
