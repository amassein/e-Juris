<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675195</ID>
<ANCIEN_ID>JG_L_2020_03_000000421184</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/51/CETATEXT000041675195.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 02/03/2020, 421184</TITRE>
<DATE_DEC>2020-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421184</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421184.20200302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de la Polynésie française d'annuler l'arrêté du 23 août 2017 par lequel le maire de Papeete a retiré son arrêté du 15 mai 2017 lui attribuant une pension de réversion complémentaire. Par un jugement n° 1700357 du 27 mars 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18PA01742 du 31 mai 2018, enregistrée le 4 juin 2018 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le recours, enregistré le 22 mai 2018 au greffe de cette cour, présenté par Mme A.... Par ce pourvoi et un nouveau mémoire, enregistré au secrétariat du contentieux du Conseil d'Etat le 23 octobre 2018, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Papeete la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme A... s'est mariée à titre posthume le 26 avril 2012 avec M. C..., agent de la commune de Papeete, décédé le 26 juillet 2011. Mme A... se pourvoit en cassation contre le jugement du 27 mars 2018 par lequel le tribunal administratif de la Polynésie française a rejeté sa demande tendant à l'annulation de l'arrêté du 23 août 2017 par lequel le maire de Papeete a retiré son arrêté du 15 mai 2017 lui accordant le bénéfice d'une pension de réversion.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Contrairement à ce que soutient Mme A..., le tribunal administratif, en se fondant sur les dispositions de l'article 227 du code civil pour juger que son mariage avait été dissous par le décès de son conjoint, n'a pas relevé d'office un moyen mais s'est borné à apporter une réponse au moyen de la requête dont il était saisi. Dès lors, le moyen, soulevé devant le juge de cassation, tiré de ce que le jugement attaqué aurait été rendu sans que soit respectée la procédure prévue à l'article R. 611-7 du code de justice administrative ne peut, en tout état de cause, qu'être écarté. <br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              3. D'une part, aux termes de l'article 171 du code civil : " Le Président de la République peut, pour des motifs graves, autoriser la célébration du mariage en cas de décès de l'un des futurs époux, dès lors qu'une réunion suffisante de faits établit sans équivoque son consentement. / Dans ce cas, les effets du mariage remontent à la date du jour précédant celui du décès de l'époux (...) ". Aux termes de l'article 227 du même code : " Le mariage se dissout : / 1° Par la mort de l'un des époux ; (...) ". Les articles 171 et 227 du code civil, qui fixent des règles relatives à l'état des personnes, s'appliquent de plein droit en Polynésie française, en vertu du 4° de l'article 7 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française. Il résulte de leurs dispositions combinées qu'un mariage célébré à titre posthume doit être regardé comme ayant été contracté à la date du jour précédant le décès du conjoint et cesse de produire effet le jour du décès.<br/>
<br/>
              4. D'autre part, l'article 5 de la délibération de la commune de Papeete du 25 février 1970 créant un régime de retraite en faveur du personnel municipal prévoit que, en cas de décès d'un employé municipal retraité, une pension de réversion, égale à 65 % de la retraite qu'il percevait en vertu de ce régime de retraite, est attribuée au conjoint survivant à la condition que " le mariage ait eu lieu depuis au moins deux ans ". <br/>
<br/>
              5. En premier lieu, il découle des termes mêmes de l'article 5 de la délibération du 25 février 1970 que la condition mise par cet article au bénéfice de la pension de réversion du régime de retraite du personnel municipal de la commune de Papeete impose que le mariage ait duré au moins deux ans avant le décès de l'employé municipal, et non qu'un délai de deux ans sépare la date du mariage de la date de demande de réversion. <br/>
<br/>
              6. En deuxième lieu, aucune disposition n'écarte l'application de la condition fixée par l'article 5 de la délibération du 25 février 1970 aux mariages célébrés sur le fondement de l'article 171 du code civil. Il résulte de ce qui a été dit au point 3 que, dès lors que, par application des articles 171 et 227 du code civil, un tel mariage prend effet la veille du décès du conjoint et cesse de produire effet le jour du décès, cette condition fait obstacle à ce qu'une personne ayant été autorisée à épouser à titre posthume un agent de la commune de Papeete retraité puisse percevoir la pension de réversion prévue par l'article 5 de cette délibération.<br/>
<br/>
              7. Il résulte de ce qui précède que les moyens d'erreur de droit soulevés par Mme A... à l'encontre du jugement rendu par le tribunal administratif ne peuvent qu'être écartés.<br/>
<br/>
              8. Quant aux moyens tirés de la méconnaissance, par l'article 5 de la délibération du 25 février 1970, de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi que de l'article premier du protocole additionnel à cette convention, ils sont nouveaux en cassation et ne peuvent, en conséquence, qu'être écartés comme inopérants.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le pourvoi de Mme A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A... et à la commune de Papeete. <br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-04 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. QUESTIONS DIVERSES RELATIVES À L`ÉTAT DES PERSONNES. - MARIAGE POSTHUME - PORTÉE TEMPORELLE.
</SCT>
<ANA ID="9A"> 26-01-04 Il résulte des articles 171 et 227 du code civil combinés qu'un mariage célébré à titre posthume doit être regardé comme ayant été contracté à la date du jour précédant le décès du conjoint et cesse de produire effet le jour du décès.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
