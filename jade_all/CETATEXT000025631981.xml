<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025631981</ID>
<ANCIEN_ID>JG_L_2012_04_000000350952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/63/19/CETATEXT000025631981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 04/04/2012, 350952, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Christine Maugüé</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Didier-Roland Tabuteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par le SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE (SNIASS), dont le siège est 5, rue de Crimée à Paris (75019) ; le syndicat requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-70 du 19 janvier 2011 fixant les conditions de désignation des inspecteurs et contrôleurs des agences régionales de santé et relatif au contrôle des établissements et services médico-sociaux et de certains lieux de vie et d'accueil, ainsi que la décision du 17 mai 2011 par laquelle la secrétaire générale des ministères chargés des affaires sociales a rejeté son recours gracieux formé contre ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu le code de la santé publique, notamment son article L. 1435-7 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu l'ordonnance n° 2010-177 du 23 février 2010 ;<br/>
<br/>
              Vu la loi n° 2011-940 du 10 août 2011 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier-Roland Tabuteau, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 1435-7 du code de la santé publique : " Le directeur général de l'agence régionale de santé peut désigner, parmi les personnels de l'agence respectant des conditions d'aptitude technique et juridique définies par décret en Conseil d'Etat, des inspecteurs et des contrôleurs pour remplir, au même titre que les agents mentionnés à l'article L. 1421-1, les missions prévues à cet article " ; que les agences régionales de santé sont des personnes morales de droit public ; que les missions de police administrative dévolues par ces agences aux inspecteurs désignés en leur sein sur le fondement de ces dispositions le sont pour le compte de leurs employeurs ; qu'ainsi, contrairement à ce que soutient le SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE, les dispositions de l'article L. 1435-7 du code de la santé publique n'ont pas pour effet de permettre à des personnes de droit privé d'exercer des missions de police administrative ; que ni l'article 12 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ni aucun autre principe constitutionnel n'exige, contrairement à ce que soutient le syndicat requérant, que les missions de police administrative comportant l'exercice de prérogatives de puissance publique ne soient confiées par des personnes publiques qu'à des fonctionnaires ou à des agents liés à elles par des contrats de droit public ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 1435-7 du code de la santé publique porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              Considérant que l'article 22 de la Constitution dispose que les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ; que l'exécution du décret litigieux, qui fixe les conditions d'aptitude juridique et technique que doivent remplir les inspecteurs et les contrôleurs désignés en application de l'article L. 1435-7 du code de la santé publique cité ci-dessus, n'implique nécessairement l'intervention d'aucune mesure individuelle ou réglementaire que le ministre de la justice aurait compétence pour signer ou contresigner ; que, par suite, le moyen tiré de ce que ce ministre aurait dû contresigner le décret attaqué doit être écarté ;<br/>
<br/>
              Considérant que le moyen tiré de ce que le comité technique paritaire du ministère chargé des affaires sociales n'aurait pas été consulté manque en fait ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              Considérant que l'ordonnance du 23 février 2010 a été ratifiée par la loi du 10 août 2011 ; qu'ainsi, le moyen tiré, par la voie de l'exception, de l'illégalité des dispositions de l'ordonnance du 23 février 2010 est inopérant ;<br/>
<br/>
              Considérant que les agents de droit privé, en fonctions dans les agences régionales de santé en application de l'article L. 1432-9 du code de la santé publique, exercent leurs missions dans le respect des principes de l'action administrative ; que, par suite, et alors même que ces agents ne seraient pas soumis aux mêmes règles déontologiques ou aux mêmes procédures disciplinaires que les agents de droit public effectuant les mêmes missions, le décret litigieux a pu, sans erreur manifeste d'appréciation, ne pas les exclure du nombre des agents susceptibles d'être désignés par le directeur général de l'agence régionale de santé en qualité d'inspecteur ou de contrôleur sur le fondement de l'article L. 1435-7 du code de la santé publique ;<br/>
<br/>
              Considérant qu'en fixant la condition que les agents de droit privé des agences régionales de santé doivent, pour être désignés en qualité d'inspecteur, être au moins titulaires d'une licence ou d'un diplôme ou titre classé au moins au niveau II, sans restreindre expressément la liste de ces diplômes et titres à des domaines en rapport avec les activités d'inspection, le décret litigieux n'a eu ni pour objet ni pour effet d'autoriser la désignation, comme inspecteur, d'une personne titulaire de n'importe quel diplôme ou titre de niveau II ; que le SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE ne saurait dès lors soutenir que le décret est, pour ce motif, entaché d'une erreur manifeste d'appréciation ; <br/>
<br/>
              Considérant, enfin, que, compte tenu de la formation initiale et de l'expérience des agents susceptibles d'être désignés en qualité d'inspecteur, le décret attaqué a pu, sans erreur manifeste d'appréciation, prévoir une formation spécifique n'excédant pas 120 heures pour les agents de droit privé désignés en qualité d'inspecteur ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE n'est pas fondé à demander l'annulation du décret qu'il attaque ; que, par suite, les conclusions qu'il présente sur le fondement de l'article L. 761-1 du code de justice administrative doivent être également rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE.<br/>
Article 2 : La requête du SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE est rejetée.<br/>
Article 3 : La présente décision sera notifiée au SYNDICAT NATIONAL DES INSPECTEURS DE L'ACTION SANITAIRE ET SOCIALE, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
