<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023494614</ID>
<ANCIEN_ID>JG_L_2011_01_000000335708</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/49/46/CETATEXT000023494614.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 28/01/2011, 335708, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335708</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ ; SCP COUTARD, MAYER, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:335708.20110128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 janvier et 15 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant..., ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06PA03524 du 9 novembre 2009 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation de l'ordonnance du 27 juillet 2006 par laquelle le président de la cinquième section du tribunal administratif de Paris avait rejeté sa demande tendant à l'annulation de l'arrêté du bureau du Sénat du 8 novembre 2005 modifiant le règlement intérieur du Sénat ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'ordonnance n° 58-1100 du 17 novembre 1958, modifiée notamment par la loi n° 2003-710 du 1er août 2003 ;<br/>
<br/>
              Vu le décret n° 2010-148 du 16 février 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Coutard, Mayer, Munier-Apaire, avocat de M. A... B...et de la SCP Lyon-Caen, Fabiani, Thiriez, avocat du Sénat, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Mayer, Munier-Apaire, avocat de M. A...B...et à la SCP Lyon-Caen, Fabiani, Thiriez, avocat du Sénat ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 8 de l'ordonnance du 17 novembre 1958 relative au fonctionnement des assemblées parlementaires, dans sa rédaction issue de l'article 60 de la loi du 1er août 2003 : " L'Etat est responsable des dommages de toute nature causés par les services des assemblées parlementaires. / Les actions en responsabilité sont portées devant les juridictions compétentes pour en connaître. / Les agents titulaires des services des assemblées parlementaires sont des fonctionnaires de l'Etat dont le statut et le régime de retraite sont déterminés par le bureau de l'assemblée intéressée (...) La juridiction administrative est appelée à connaître de tous litiges d'ordre individuel concernant ces agents, et se prononce au regard des principes généraux du droit et des garanties fondamentales reconnues à l'ensemble des fonctionnaires civils et militaires de l'Etat visées à l'article 34 de la Constitution. La juridiction administrative est également compétente pour se prononcer sur les litiges individuels en matière de marchés publics. / Dans les instances ci-dessus visées, qui sont les seules susceptibles d'être engagées contre une assemblée parlementaire, l'Etat est représenté par le président de l'assemblée intéressée, qui peut déléguer cette compétence aux questeurs (...) "<br/>
<br/>
              Sur la question de constitutionnalité :<br/>
<br/>
              Considérant qu'aux termes de l'article 7 du décret du 16 février 2010 portant application de la loi organique n° 2009-523 du 10 décembre 2009 relative à l'application de l'article 61-1 de la Constitution : " Le présent décret entre en vigueur le 1er mars 2010. Dans les instances en cours, une question prioritaire de constitutionnalité doit, pour être recevable, être présentée sous la forme d'un mémoire distinct et motivé produit postérieurement à cette date. (...) " ; que, par suite, faute d'avoir été présenté selon ces modalités après le 1er mars 2010, le moyen tiré par M. B...de ce que les dispositions de l'article 8 de l'ordonnance du 17 novembre 1958 citées ci-dessus portent atteinte aux droits et libertés garantis par la Constitution est irrecevable ;<br/>
<br/>
              Sur la régularité et le bien fondé de l'arrêt attaqué :<br/>
<br/>
              Considérant que, si les dispositions de l'article 8 de l'ordonnance du 17 novembre 1958 citées ci-dessus n'ouvrent pas de voie d'action directe aux agents des services du Sénat à l'encontre des décisions du bureau du Sénat en matière statutaire, ceux-ci peuvent contester ces décisions et les dispositions du règlement intérieur dont il leur est fait application, par voie d'exception, à l'occasion des litiges relatifs à leur situation individuelle qu'ils portent devant la juridiction administrative ; que dès lors, M. B...n'est pas fondé à soutenir que la cour administrative d'appel de Paris a commis une erreur de droit en jugeant que ces dispositions ne méconnaissent pas à cet égard les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales garantissant le droit à un procès équitable et le droit de former un recours contentieux ;<br/>
<br/>
              Considérant que M.B..., architecte des bâtiments principal du Sénat, demandait devant les juges du fond, à titre principal, l'annulation de l'arrêté du 8 novembre 2005 du bureau du Sénat modifiant des dispositions du règlement intérieur relatives au personnel technique du service de l'architecture, des bâtiments et des jardins, notamment les dispositions relatives au recrutement, à l'appellation et aux attributions de l'architecte en chef ; que, pour rejeter sa demande, la cour administrative d'appel de Paris a retenu qu'une demande, telle que la sienne, qui tend à l'annulation de dispositions générales et impersonnelles, ne présente pas le caractère d'un litige d'ordre individuel, quand bien même ces dispositions ne porteraient que sur un seul emploi pour lequel un seul agent serait concerné ; qu'elle n'a, ce faisant, commis aucune erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte des dispositions citées plus haut de l'article 8 de l'ordonnance du 17 novembre 1958 que, si la juridiction administrative est compétente pour apprécier la légalité des dispositions du règlement intérieur d'une assemblée parlementaire relatives au statut du personnel lorsque celle-ci est contestée, par voie d'exception, à l'appui d'un recours dont le juge administratif est saisi par un agent titulaire de cette assemblée dans le cadre d'un litige d'ordre individuel, les agents des assemblées parlementaires ne sont pas recevables à contester de telles dispositions par voie d'action ; que la demande de M. B...n'était donc pas recevable ; que ce motif doit être substitué à celui, tiré de l'incompétence de la juridiction administrative pour en connaître, qu'a retenu l'arrêt de la cour administrative d'appel de Paris, dont il justifie légalement le dispositif ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt du 9 novembre 2009 de la cour administrative d'appel de Paris, lequel est suffisamment motivé ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande M.  B...au titre des frais exposés par lui et non compris dans les dépens ; qu'il y a lieu, en revanche, sur le fondement des mêmes dispositions, de mettre à la charge de M. B...le versement à l'Etat d'une somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : M. B...versera à l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au président du Sénat.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">52-03 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. PARLEMENT. - IRRECEVABILITÉ DE CONCLUSIONS DIRIGÉES CONTRE LE RÈGLEMENT INTÉRIEUR D'UNE ASSEMBLÉE PARLEMENTAIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTE INSUSCEPTIBLE DE RECOURS PAR VOIE D'ACTION - RÈGLEMENT INTÉRIEUR D'UNE ASSEMBLÉE PARLEMENTAIRE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-05 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE CASSATION. - SUBSTITUTION DE MOTIFS - POSSIBILITÉ DE SUBSTITUER UN MOTIF D'IRRECEVABILITÉ AU MOTIF ERRONÉ D'INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ3].
</SCT>
<ANA ID="9A"> 52-03 Il résulte des dispositions de l'article 8 de l'ordonnance n° 58-1100 du 17 novembre 1958 que les agents des assemblées parlementaires ne sont pas recevables à contester, par voie d'action, les dispositions du règlement intérieur d'une assemblée parlementaire.</ANA>
<ANA ID="9B"> 54-01-01-02 Il résulte des dispositions de l'article 8 de l'ordonnance n° 58-1100 du 17 novembre 1958 que les dispositions du règlement intérieur d'une assemblée parlementaire ne peuvent pas être contestées par voie d'action.</ANA>
<ANA ID="9C"> 54-07-05 Le juge de cassation peut, pour confirmer le dispositif de rejet contesté par le pourvoi, substituer un motif d'irrecevabilité au motif erroné d'incompétence de la juridiction administrative retenu par le juge du fond.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la compétence de la juridiction administrative pour apprécier la légalité d'un tel règlement par voie d'exception dans le cadre d'un litige d'ordre individuel concernant un agent titulaire d'une assemblée parlementaire, CE, 19 janvier 1996, Escriva, n° 148631, p. 10.,,[RJ2] Comp., s'agissant de la possibilité pour un agent titulaire d'une assemblée parlementaire de contester un tel règlement par voie d'exception à l'occasion d'un litige individuel porté devant le juge administratif, CE, 19 janvier 1996, Escriva, n° 148631, p. 10.,,[RJ3] Comp., pour la possibilité de substituer en cassation le motif d'incompétence de la juridiction administrative au motif erroné retenu par les juges du fond, CE, 6 décembre 1967, Mme Veuve Lemaître, n° 71063, T. p. 920.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
