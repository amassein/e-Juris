<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041735772</ID>
<ANCIEN_ID>JG_L_2020_03_000000424958</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/73/57/CETATEXT000041735772.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 18/03/2020, 424958</TITRE>
<DATE_DEC>2020-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424958</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424958.20200318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 19 octobre 2018, l'association CCDELI38 Support, Mme D... A..., M. F... C... et Mme E... B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre et le ministre des solidarités et de la santé ont rejeté leur demande tendant à l'abrogation de l'arrêté du ministre des affaires sociales, du ministre d'Etat, chargé des départements et territoires d'outre-mer, du ministre de l'économie et des finances et du ministre de l'agriculture du 9 mars 1966 fixant les tarifs d'honoraires des praticiens et auxiliaires médicaux applicables en l'absence de convention pour les soins dispensés aux assurés sociaux ;<br/>
<br/>
              2°) d'enjoindre au ministre des solidarités et de la santé et au Premier ministre, sur le fondement de l'article L. 911-1 du code de justice administrative, d'abroger cet arrêté ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme 1 500 euros, à verser à chacun d'eux, au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2011/24/UE du Parlement européen et du Conseil du 9 mars 2011 ;<br/>
              - la directive 2018/958 du 28 juin 2018 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - loi n° 64-707 du 10 juillet 1964 ;<br/>
              - le décret n° 60-451 du 12 mai 1960 ;<br/>
              - le décret n° 66-21 du 7 janvier 1966 ;<br/>
              - le décret n° 67-792 du 19 septembre 1967 ;<br/>
              - le décret n° 85-1353 du 17 décembre 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'association CCDELI38 Support, Mme A..., M. C... et Mme B... ont saisi le Premier ministre et le ministre des solidarités et de la santé d'une demande tendant à l'abrogation de l'arrêté du 9 mars 1966 fixant les tarifs d'honoraires des praticiens et auxiliaires médicaux applicables en l'absence de convention pour les soins dispensés aux assurés sociaux, qui doit être regardée comme tendant à l'abrogation de cet arrêté en tant qu'il s'applique aux chirurgiens-dentistes. Ils demandent l'annulation pour excès de pouvoir du refus implicite opposé à leur demande.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 243-2 du code des relations entre le public et l'administration : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé ". L'effet utile de l'annulation pour excès de pouvoir du refus d'abroger un acte réglementaire illégal réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour l'autorité compétente, de procéder à l'abrogation de cet acte afin que cessent les atteintes illégales que son maintien en vigueur porte à l'ordre juridique. Il s'ensuit que, dans l'hypothèse où un changement de circonstances a fait cesser l'illégalité de l'acte réglementaire litigieux à la date à laquelle il statue, le juge de l'excès de pouvoir ne saurait annuler le refus de l'abroger. A l'inverse, si, à la date à laquelle il statue, l'acte réglementaire est devenu illégal en raison d'un changement de circonstances, il appartient au juge d'annuler ce refus d'abroger pour contraindre l'autorité compétente à procéder à son abrogation.<br/>
<br/>
              Sur la base légale de l'arrêté litigieux :<br/>
<br/>
              3. Aux termes de l'article L. 162-9 du code de la sécurité sociale : " Les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, les sages-femmes et les auxiliaires médicaux sont définis par des conventions nationales conclues entre l'Union nationale des caisses d'assurance maladie et une ou plusieurs des organisations syndicales nationales les plus représentatives de chacune de ces professions (...) ". En vertu de l'article L. 162-14-1 du même code, ces conventions définissent notamment : " les tarifs des honoraires, rémunérations et frais accessoires dus aux professionnels par les assurés sociaux en dehors des cas de dépassement autorisés par la convention pour (...) les chirurgiens-dentistes ". Aux termes de l'article L. 162-12 du même code, en vigueur à la date de la présente décision : " A défaut de convention ou en l'absence d'adhésion personnelle à la convention type, les tarifs servant de base au remboursement des honoraires des chirurgiens-dentistes, des sages-femmes et des auxiliaires médicaux sont fixés par arrêtés interministériels ". Enfin, aux termes de l'article R. 162-7 du même code : " Des arrêtés conjoints du ministre chargé de la sécurité sociale, du ministre chargé de la santé, du ministre chargé de l'agriculture et du ministre chargé du budget fixent les tarifs servant de base au remboursement des honoraires à défaut de convention ou en l'absence d'adhésion personnelle ". Par les dispositions de l'article L. 162-12, le législateur a entendu confier à des arrêtés le soin de fixer les tarifs servant de base au remboursement aux assurés sociaux des honoraires des chirurgiens-dentistes et des autres professionnels entrant dans son champ d'application non seulement en l'absence d'adhésion à la convention type mentionnée à l'article L. 162-11 du code de la sécurité sociale, mais également en l'absence d'adhésion à la convention nationale mentionnée à l'article L. 162-9 du même code. <br/>
<br/>
              4. Il résulte du point 2 que lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'abroger un acte réglementaire, le juge de l'excès de pouvoir est conduit à apprécier la légalité de cet acte au regard des règles applicables à la date de sa décision. S'agissant des règles relatives à la détermination de l'autorité compétente pour édicter un acte réglementaire, leur changement ne saurait avoir pour effet de rendre illégal un acte qui avait été pris par une autorité qui avait compétence pour ce faire à la date de son édiction. Un tel changement a, en revanche, pour effet de faire cesser l'illégalité dont était entaché un règlement édicté par une autorité incompétente dans le cas où ce changement a conduit, à la date à laquelle le juge statue, à investir cette autorité de la compétence pour ce faire.<br/>
<br/>
              5. Les ministres chargés de la sécurité sociale, de la santé, de l'agriculture et du budget tiennent désormais de l'article L. 162-12 du code de la sécurité sociale compétence pour fixer les tarifs servant de base au remboursement des honoraires des chirurgiens-dentistes à défaut d'adhésion à la convention régissant les rapports entre les organismes d'assurance maladie et ces praticiens. Il suit de là que les requérants ne peuvent utilement exciper de l'incompétence dont serait entaché le décret du 12 mai 1960, modifié par le décret du 7 janvier 1966, relatif aux soins médicaux dispensés aux assurés sociaux, sur le fondement duquel l'arrêté du 9 mars 1966 a été adopté.<br/>
<br/>
              Sur le respect du droit à la protection de la santé et du principe de libre choix du praticien :<br/>
<br/>
              6. D'une part, en vertu du onzième alinéa du Préambule de la Constitution de 1946, la Nation " garantit à tous, notamment à l'enfant, à la mère et aux vieux travailleurs, la protection de la santé ". D'autre part, en vertu du premier alinéa de l'article L. 1110-8 du code de la santé publique : " Le droit du malade au libre choix de son praticien (...) est un principe fondamental de la législation sanitaire ".<br/>
<br/>
              7. Par les dispositions de l'article L. 162-12 du code de la sécurité sociale, le législateur a prévu qu'une tarification particulière, dite tarif d'autorité, s'appliquerait en vue du remboursement des frais exposés par les patients qui ont décidé de ne pas recourir, comme ils ont la possibilité de le faire, aux soins d'un praticien conventionné. Ce tarif ne concerne que ceux des assurés sociaux qui, alors que seule une très faible proportion de praticiens n'a pas adhéré à la convention, ont néanmoins choisi de consulter un praticien non conventionné. Dans l'intérêt général, les ministres peuvent prévoir une différence importante avec les tarifs conventionnels en vue de rendre plus attractif l'exercice dans le cadre conventionnel, par lequel les praticiens s'engagent au respect des obligations fixées par la convention en vue de garantir un meilleur accès aux soins. Ainsi, en maintenant ce tarif au niveau prévu par l'arrêté litigieux, les ministres compétents n'ont méconnu ni les exigences qui découlent du Préambule de la Constitution du 27 octobre 1946 en matière de protection de la santé ni le principe du libre choix du praticien par le malade. Par suite, les moyens tirés de la méconnaissance du principe constitutionnel de protection de la santé et du principe de libre choix du praticien par le patient doivent être écartés.<br/>
<br/>
              Sur le respect de la liberté d'exercice et de la liberté contractuelle des praticiens :<br/>
<br/>
              8. En vertu de l'article L. 162-15 du code de la sécurité sociale, la convention régissant les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes s'applique aux praticiens " qui s'installent en exercice libéral ou qui souhaitent adhérer à la convention pour la première fois s'ils en font la demande " et aux autres chirurgiens-dentistes " tant qu'ils n'ont pas fait connaître à la caisse primaire d'assurance maladie qu'ils ne souhaitent plus être régis par ces dispositions ". Si le maintien à un niveau très faible des tarifs servant de base au remboursement des honoraires des chirurgiens-dentistes qui n'exercent pas dans le cadre de la convention peut inciter un grand nombre de praticiens à choisir un exercice conventionnel, il n'en résulte, contrairement à ce que soutiennent les requérants, ni une atteinte à leur liberté d'exercice, ni une atteinte à leur liberté contractuelle.<br/>
<br/>
              Sur le respect du droit de l'Union européenne :<br/>
<br/>
              9. En premier lieu, aux termes du premier alinéa de l'article 56 du traité sur le fonctionnement de l'Union européenne : " Dans le cadre des dispositions ci-après, les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui du destinataire de la prestation ". Les chirurgiens-dentistes établis dans un autre Etat de l'Union européenne qui exécutent en France, de manière temporaire et occasionnelle, des actes de leur profession dans le respect des dispositions des articles L. 4112-7 et R. 4112-9 à R. 4112-12 du code de la santé publique peuvent, comme les praticiens établis en France, choisir d'exercer soit dans le cadre de la convention régissant les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, soit en dehors de ce cadre. Par suite, les requérants ne sont pas fondés à soutenir que l'arrêté litigieux porterait atteinte à la libre prestation des services à l'intérieur de l'Union européenne. <br/>
<br/>
              10. En deuxième lieu, aux termes de l'article 102 de ce traité : " Est incompatible avec le marché intérieur et interdit, dans la mesure où le commerce entre États membres est susceptible d'en être affecté, le fait pour une ou plusieurs entreprises d'exploiter de façon abusive une position dominante sur le marché intérieur ou dans une partie substantielle de celui-ci (...) ". Contrairement à ce que soutiennent les requérants, l'arrêté litigieux, dont il n'est pas établi qu'il affecterait le commerce entre les Etats membres, n'a en tout état de cause ni pour objet ni pour effet de permettre à certains chirurgiens-dentistes d'exploiter de façon abusive une position dominante sur le marché intérieur ou dans une partie substantielle de celui-ci. <br/>
<br/>
              11. En troisième lieu, les requérants soutiennent que le tarif applicable aux chirurgiens-dentistes non conventionnés serait contraire, selon leurs termes, à la " directive européenne 2016/0404 ". Ils se réfèrent ainsi à une proposition de directive, devenue la directive (UE) 2018/958 du Parlement européen et du Conseil du 28 juin 2018 relative à un contrôle de proportionnalité avant l'adoption d'une nouvelle réglementation de professions. Cette directive s'applique à l'adoption de nouvelles dispositions limitant l'accès à des professions réglementées ou leur exercice et à la modification de telles dispositions. Par suite, elle ne saurait être utilement invoquée à l'encontre de l'arrêté litigieux.<br/>
<br/>
              12. En dernier lieu, si le niveau des tarifs servant de base au remboursement des honoraires des chirurgiens-dentistes non conventionnés rend plus attractif, pour les assurés sociaux, de s'adresser à un praticien conventionné, il ne résulte pas de cette seule circonstance que l'arrêté en litige devrait être regardé comme une mesure contraire aux règles du traité sur le fonctionnement de l'Union européenne relatives à la concurrence. <br/>
<br/>
              Sur le respect du principe d'égalité :<br/>
<br/>
              13. Le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que la différence de traitement qui en résulte soit, dans l'un comme l'autre cas, en rapport direct avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier.<br/>
<br/>
              14. En premier lieu, il résulte du tableau figurant au 2° du II du A de l'annexe de l'arrêté litigieux que les tarifs applicables au remboursement des soins donnés par les chirurgiens-dentistes qui n'adhèrent pas à une convention doivent être regardés, en application, d'une part, du règlement (CE) n° 1103/97 du Conseil du 17 juin 1997 fixant certaines dispositions relatives à l'introduction de l'euro et, d'autre part, du décret du 19 septembre 1967 relatif à l'entrée en vigueur des dispositions de la loi du 10 juillet 1964 portant réorganisation de la région parisienne, comme fixés à 0,40 euros pour une consultation dans la zone comprenant la Ville de Paris, les départements des Hauts-de-Seine, de la Seine-Saint-Denis, du Val-de-Marne, une partie qu'il précise des départements de l'Essonne, des Yvelines et du Val-d'Oise, ainsi que les communes qu'il énumère des agglomérations de Lyon et Marseille, et à 0,32 euros pour les autres départements et localités de métropole. La modulation tarifaire ainsi opérée, initialement destinée à tenir compte des conditions particulières d'exercice des chirurgiens-dentistes dans ces agglomérations importantes et à leur voisinage, est en rapport direct avec l'objet de l'arrêté et ne peut être regardée comme manifestement disproportionnée au regard de la différence de situation existant entre professionnels selon leur lieu d'exercice. <br/>
<br/>
              15. En deuxième lieu, si les tarifs fixés par l'arrêté litigieux diffèrent de ceux que détermine la convention définissant les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, les praticiens conventionnés et les praticiens non conventionnés relèvent, du fait d'un choix librement exercé, de deux régimes différents, conduisant les premiers à soumettre leur exercice professionnel au respect d'un ensemble étendu d'obligations à l'égard des organismes et des assurés sociaux. Par suite, les praticiens non conventionnés et les assurés sociaux qui choisissent de s'adresser à eux ne sont pas dans une situation comparable à celle des praticiens conventionnés et de leurs patients. Les requérants ne peuvent ainsi utilement soutenir que l'arrêté litigieux méconnaîtrait le principe d'égalité entre chirurgiens-dentistes selon qu'ils sont ou non conventionnés et entre patients selon qu'ils s'adressent à un chirurgien-dentiste conventionné ou non.<br/>
<br/>
              16. En dernier lieu, aux termes de l'article 7, paragraphe 4 de la directive 2011/24/UE du Parlement européen et du Conseil du 9 mars 2011 relative à l'application des droits des patients en matière de soins de santé transfrontaliers, c'est-à-dire dispensés ou prescrits dans un Etat membre autre que l'Etat membre d'affiliation : " Les coûts des soins de santé transfrontaliers sont remboursés ou payés directement par l'État membre d'affiliation à hauteur des coûts qu'il aurait pris en charge si ces soins de santé avaient été dispensés sur son territoire, sans que le remboursement excède les coûts réels des soins de santé reçus (...) ". En vertu de ces dispositions, le remboursement de prestations transfrontalières assurées par des chirurgiens-dentistes dans un autre Etat de l'Union européenne au profit de patients affiliés à l'assurance maladie en France est assuré sur la base du tarif applicable pour des prestations équivalentes, en vertu de la convention régissant les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, quelles que soient les relations de ces praticiens avec le système d'assurance maladie de l'Etat dans lequel ils exercent. Toutefois, il n'en résulte pas, alors que les praticiens exerçant en France ont le choix de se placer ou non dans le cadre conventionnel, que les ministres chargés de la santé et de la sécurité sociale auraient été tenus, pour assurer le respect du principe d'égalité, de fixer à un niveau plus élevé les tarifs applicables en l'absence d'adhésion à la convention nationale.<br/>
<br/>
              17. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par le ministre des solidarités et de la santé, que les requérants ne sont pas fondés à demander l'annulation de la décision implicite refusant d'abroger, en tant qu'il fixe les tarifs applicables aux chirurgiens-dentistes qui n'ont pas adhéré à une convention, l'arrêté du 9 mars 1966 fixant les tarifs d'honoraires des praticiens et auxiliaires médicaux applicables en l'absence de convention pour les soins dispensés aux assurés sociaux. Leurs conclusions aux fins d'injonction et leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de l'association CCDELI38 Support, de Mme A..., de M. C... et de Mme B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée, pour l'ensemble des requérants, à l'association CCDELI38 Support, représentante désignée, et au ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
