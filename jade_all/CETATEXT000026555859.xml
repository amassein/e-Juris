<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555859</ID>
<ANCIEN_ID>JG_L_2012_10_000000346610</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 29/10/2012, 346610</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346610</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346610.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 février et 2 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société France Télécom, dont le siège est 6, place d'Alleray à Paris (75015) ; elle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05781 du 16 décembre 2010 par lequel la cour administrative d'appel de Paris, après avoir annulé le jugement n° 0618382 du 23 juillet 2009 par lequel le tribunal administratif de Paris a rejeté la demande de la Société des Autoroutes du Nord et de l'Est de la France (SANEF) tendant à ce que soit mise à la charge de la société France Telecom le paiement de redevances d'occupation du domaine public autoroutier concédé à la SANEF pour les années 1998 à 2002, l'a condamnée à verser à la SANEF la somme de 138 294,60 euros hors taxes assortie des intérêts en paiement de ces redevances ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la SANEF la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 octobre 2012, présentée pour la société France Télécom ;<br/>
<br/>
              Vu le code civil ; <br/>
<br/>
              Vu le code de la voirie routière ; <br/>
<br/>
              Vu le code du domaine de l'Etat ;<br/>
<br/>
              Vu le code des postes et télécommunications ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de la société France Télécom, et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société des Autoroutes du Nord et de l'Est de la France (SANEF),<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de la société France Télécom, et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société des Autoroutes du Nord et de l'Est de la France (SANEF) ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 48 du code du domaine de l'Etat, alors en vigueur : " Les redevances, droits et produits périodiques du domaine public ou privé de l'Etat, recouvrés par le service des domaines en vertu des lois, décrets, arrêtés généraux ou particuliers ou décisions administratives, suivant des tarifs uniformes ou variables, sont soumis à la prescription quinquennale édictée par l'article 2277 du code civil (...) " ; qu'aux termes de l'article 2277 du code civil, applicable au litige : " Se prescrivent par cinq ans les actions en paiement : Des salaires ; / Des arrérages des rentes perpétuelles et viagères et de ceux des pensions alimentaires ; / Des loyers, des fermages et des charges locatives ; / Des intérêts des sommes prêtées / et généralement de tout ce qui est payable par année ou à des termes périodiques plus courts. / Se prescrivent également par cinq ans les actions en répétition des loyers, des fermages et des charges locatives " ; <br/>
<br/>
              2. Considérant que, pour écarter l'exception soulevée devant elle, par laquelle la société France Télécom soutenait qu'était prescrite, en application de l'article 2277 du code civil, l'action en paiement de redevances d'occupation du domaine public autoroutier engagée à son encontre par la Société des Autoroutes du Nord et de l'Est de la France (SANEF) au titre des années 1998 à 2002, la cour administrative d'appel de Paris a d'abord constaté que le Conseil d'Etat statuant au contentieux avait, par la décision n° 189191 du 21 mars 2003, annulé les dispositions réglementaires du code des postes et télécommunications fixant les barèmes de redevances maximum pouvant être exigées des occupants du domaine public et prévoyant leur versement annuel ; qu'elle a ensuite relevé qu'à la date du 23 décembre 2003, à laquelle la SANEF a adressé sa facture à France Télécom, aucune autre disposition ou clause contractuelle expresse ne définissait, pour la période correspondant aux années 1998 à 2002, les modalités d'émission et de recouvrement des redevances d'occupation du domaine public autoroutier dues par les opérateurs de télécommunications ; qu'elle en a déduit que la créance dont se prévalait la SANEF ne pouvait être regardée comme " payable par année " au sens de l'article 2277 du code civil, de sorte qu'elle ne pouvait se voir opposer l'exception de prescription quinquennale prévue par cette disposition ; qu'en statuant ainsi, par un arrêt suffisamment motivé, la cour, qui n'a pas entendu subordonner l'application de la prescription quinquennale prévue à l'article 2277 du code civil à l'existence d'une stipulation expresse mais a seulement constaté qu'il n'existait en l'espèce aucun élément permettant de regarder la créance de la SANEF comme ayant un caractère périodique au sens de cet article, n'a pas commis d'erreur de droit ;  <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la société France Télécom n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que, par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SANEF, qui n'est pas dans la présente instance la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société France Télécom le versement à la SANEF d'une somme de 3 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la société France Télécom est rejeté.<br/>
<br/>
Article 2 : La société France Télécom versera à la SANEF la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société France Télécom et à la Société des Autoroutes du Nord et de l'Est de la France.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-04 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. REDEVANCES. - ACTION EN PAIEMENT DE REDEVANCES D'OCCUPATION DU DOMAINE PUBLIC AUTOROUTIER ENGAGÉE PAR LA SOCIÉTÉ CONCESSIONNAIRE À L'ENCONTRE D'UN OPÉRATEUR DE TÉLÉCOMMUNICATIONS - ANNULATION DES DISPOSITIONS RÉGLEMENTAIRES FIXANT LES BARÈMES DE REDEVANCES MAXIMUM POUVANT ÊTRE EXIGÉES DES OCCUPANTS DU DOMAINE PUBLIC ET PRÉVOYANT LEUR VERSEMENT ANNUEL [RJ1] - ABSENCE D'AUTRE DISPOSITION OU CLAUSE CONTRACTUELLE EXPRESSE DÉFINISSANT, POUR LA PÉRIODE EN CAUSE, LES MODALITÉS D'ÉMISSION ET DE RECOUVREMENT DE CES REDEVANCES - CONSÉQUENCE - APPLICATION DE LA PRESCRIPTION QUINQUENNALE PRÉVUE POUR LES CRÉANCES « PAYABLES PAR ANNÉE » AU SENS DE L'ARTICLE 2277 DU CODE CIVIL - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">51-02 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. - REDEVANCES D'OCCUPATION DU DOMAINE PUBLIC AUTOROUTIER DUES PAR UN OPÉRATEUR DE TÉLÉCOMMUNICATIONS - ACTION EN PAIEMENT ENGAGÉE PAR LA SOCIÉTÉ CONCESSIONNAIRE D'AUTOROUTES - ANNULATION DES DISPOSITIONS RÉGLEMENTAIRES FIXANT LES BARÈMES DE REDEVANCES MAXIMUM POUVANT ÊTRE EXIGÉES DES OCCUPANTS DU DOMAINE PUBLIC ET PRÉVOYANT LEUR VERSEMENT ANNUEL [RJ1] - ABSENCE D'AUTRE DISPOSITION OU CLAUSE CONTRACTUELLE EXPRESSE DÉFINISSANT, POUR LA PÉRIODE EN CAUSE, LES MODALITÉS D'ÉMISSION ET DE RECOUVREMENT DE CES REDEVANCES - CONSÉQUENCE - APPLICATION DE LA PRESCRIPTION QUINQUENNALE PRÉVUE POUR LES CRÉANCES « PAYABLES PAR ANNÉE » AU SENS DE L'ARTICLE 2277 DU CODE CIVIL - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">71-02-03 VOIRIE. RÉGIME JURIDIQUE DE LA VOIRIE. OCCUPATIONS PRIVATIVES DE LA VOIE PUBLIQUE. - DOMAINE PUBLIC AUTOROUTIER - REDEVANCES D'OCCUPATION DUES PAR UN OPÉRATEUR DE TÉLÉCOMMUNICATIONS - ACTION EN PAIEMENT ENGAGÉE PAR LA SOCIÉTÉ CONCESSIONNAIRE D'AUTOROUTES - ANNULATION DES DISPOSITIONS RÉGLEMENTAIRES FIXANT LES BARÈMES DE REDEVANCES MAXIMUM POUVANT ÊTRE EXIGÉES DES OCCUPANTS DU DOMAINE PUBLIC ET PRÉVOYANT LEUR VERSEMENT ANNUEL [RJ1] - ABSENCE D'AUTRE DISPOSITION OU CLAUSE CONTRACTUELLE EXPRESSE DÉFINISSANT, POUR LA PÉRIODE EN CAUSE, LES MODALITÉS D'ÉMISSION ET DE RECOUVREMENT DE CES REDEVANCES - CONSÉQUENCE - APPLICATION DE LA PRESCRIPTION QUINQUENNALE PRÉVUE POUR LES CRÉANCES « PAYABLES PAR ANNÉE » AU SENS DE L'ARTICLE 2277 DU CODE CIVIL - ABSENCE.
</SCT>
<ANA ID="9A"> 24-01-02-01-01-04 Action en paiement de redevances d'occupation du domaine public autoroutier engagée par une société concessionnaire d'autoroutes à l'encontre d'un opérateur de télécommunications au titre des années 1998 à 2002. Dès lors que le Conseil d'Etat statuant au contentieux a, par une décision du 21 mars 2003, annulé les dispositions réglementaires du code des postes et télécommunications fixant les barèmes de redevances maximum pouvant être exigées des occupants du domaine public et prévoyant leur versement annuel, et qu'à la date à laquelle la société concessionnaire a adressé sa facture à l'opérateur de télécommunications, aucune autre disposition ou clause contractuelle expresse ne définissait, pour la période en cause, les modalités d'émission et de recouvrement des redevances d'occupation du domaine public autoroutier dues par les opérateurs de télécommunications, la créance dont se prévalait la société concessionnaire ne pouvait être regardée comme « payable par année » au sens de l'article 2277 du code civil et ne pouvait, en l'absence de tout élément permettant de la regarder la créance comme ayant un caractère périodique au sens de cet article, se voir opposer l'exception de prescription quinquennale prévue par cette disposition.</ANA>
<ANA ID="9B"> 51-02 Action en paiement de redevances d'occupation du domaine public autoroutier engagée par une société concessionnaire d'autoroutes à l'encontre d'un opérateur de télécommunications au titre des années 1998 à 2002. Dès lors que le Conseil d'Etat statuant au contentieux a, par une décision du 21 mars 2003, annulé les dispositions réglementaires du code des postes et télécommunications fixant les barèmes de redevances maximum pouvant être exigées des occupants du domaine public et prévoyant leur versement annuel, et qu'à la date à laquelle la société concessionnaire a adressé sa facture à l'opérateur de télécommunications, aucune autre disposition ou clause contractuelle expresse ne définissait, pour la période en cause, les modalités d'émission et de recouvrement des redevances d'occupation du domaine public autoroutier dues par les opérateurs de télécommunications, la créance dont se prévalait la société concessionnaire ne pouvait être regardée comme « payable par année » au sens de l'article 2277 du code civil et ne pouvait, en l'absence de tout élément permettant de la regarder la créance comme ayant un caractère périodique au sens de cet article, se voir opposer l'exception de prescription quinquennale prévue par cette disposition.</ANA>
<ANA ID="9C"> 71-02-03 Action en paiement de redevances d'occupation du domaine public autoroutier engagée par une société concessionnaire d'autoroutes à l'encontre d'un opérateur de télécommunications au titre des années 1998 à 2002. Dès lors que le Conseil d'Etat statuant au contentieux a, par une décision du 21 mars 2003, annulé les dispositions réglementaires du code des postes et télécommunications fixant les barèmes de redevances maximum pouvant être exigées des occupants du domaine public et prévoyant leur versement annuel, et qu'à la date à laquelle la société concessionnaire a adressé sa facture à l'opérateur de télécommunications, aucune autre disposition ou clause contractuelle expresse ne définissait, pour la période en cause, les modalités d'émission et de recouvrement des redevances d'occupation du domaine public autoroutier dues par les opérateurs de télécommunications, la créance dont se prévalait la société concessionnaire ne pouvait être regardée comme « payable par année » au sens de l'article 2277 du code civil et ne pouvait, en l'absence de tout élément permettant de la regarder la créance comme ayant un caractère périodique au sens de cet article, se voir opposer l'exception de prescription quinquennale prévue par cette disposition.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 mars 2003, Syndicat intercommunal de la périphérie de Paris pour l'électricité et les réseaux, n° 189191, p. 144.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
