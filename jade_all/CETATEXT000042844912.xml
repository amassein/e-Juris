<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844912</ID>
<ANCIEN_ID>JG_L_2020_12_000000441210</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/49/CETATEXT000042844912.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/12/2020, 441210, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441210</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:441210.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>1° Sous le n° 441210, par une requête et deux nouveaux mémoires, enregistrés les 15 juin, 9 et 15 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des indépendants solidaires demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2020-621 du 22 mai 2020 procédant au regroupement et à la mise en cohérence des dispositions du code de la sécurité sociale applicables aux travailleurs indépendants (décrets simples).<br/>
<br/>
              2° Sous le n° 442025, par une requête et deux nouveaux mémoires, enregistrés le 21 juillet et les 5 et 14 décembre 2020, M. A... B... demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2020-621 du 22 mai 2020 procédant au regroupement et à la mise en cohérence des dispositions du code de la sécurité sociale applicables aux travailleurs indépendants (décrets simples).<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 79/7/CEE du Conseil du 19 décembre 1978 ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2017-1836 du 30 décembre 2017 ;<br/>
              - la loi n° 2018-1203 du 22 décembre 2018 ;<br/>
              - l'ordonnance n° 2018-470 du 12 juin 2018 ; <br/>
              - le décret n° 2019-718 du 5 juillet 2019 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8, et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par l'article 15 de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018, le législateur a supprimé le régime social des indépendants à compter du 1er janvier 2018 pour rattacher ces travailleurs au régime général d'assurance maladie et autorisé le Gouvernement à prendre par ordonnances, dans les conditions prévues à l'article 38 de la Constitution, " les mesures relevant du domaine de la loi visant à modifier toute disposition législative, afin d'assurer la cohérence des textes au regard des dispositions du présent article et le respect de la hiérarchie des normes, de regrouper les dispositions qui le justifient dans le livre VI du code de la sécurité sociale et d'abroger les dispositions, codifiées ou non, devenues sans objet ". Sur le fondement de cette habilitation, le Gouvernement a adopté l'ordonnance du 12 juin 2018 procédant au regroupement et à la mise en cohérence des dispositions du code de la sécurité sociale applicables aux travailleurs indépendants. Cette ordonnance a été complétée, s'agissant des dispositions réglementaires relevant respectivement du décret en Conseil d'Etat et du décret simple, par les décrets des 5 juillet 2019 et 22 mai 2020 procédant au regroupement et à la mise en cohérence des dispositions du code de la sécurité sociale applicables aux travailleurs indépendants. Par deux requêtes qu'il y a lieu de joindre, l'Union nationale des indépendants solidaires et M. B... demandent l'annulation pour excès de pouvoir du second de ces décrets.<br/>
<br/>
              2. En vertu d'une jurisprudence constante de la Cour de justice, rappelée encore récemment dans son arrêt de grande chambre du 11 juin 2020, C262/18 P et C271/18 P, Commission européenne et République slovaque, le droit de l'Union européenne ne porte pas atteinte à la compétence des États membres pour aménager leur système de sécurité sociale. Il appartient ainsi, en l'absence d'une harmonisation au niveau européen, à la législation de chaque État membre de déterminer les conditions du droit ou de l'obligation de s'affilier à un régime de sécurité sociale et le mode de financement de ce régime. Les États membres doivent cependant, dans l'exercice de cette compétence, respecter le droit de l'Union européenne, notamment, s'agissant des entreprises, les règles de concurrence résultant du traité sur le fonctionnement de l'Union européenne. Pour évaluer si une activité exercée dans le cadre d'un régime de sécurité sociale est dépourvue de caractère économique, il convient de procéder à une appréciation globale du régime en cause en prenant en considération les circonstances qu'il poursuit un objectif social, qu'il met en oeuvre le principe de solidarité, que l'activité exercée n'a aucun but lucratif et qu'elle est contrôlée par l'Etat, en examinant, en particulier, si et dans quelle mesure le régime en cause peut être considéré comme mettant en oeuvre le principe de solidarité et si l'activité des organismes d'assurance qui le gèrent est soumise à un contrôle de l'Etat.<br/>
<br/>
              3. En premier lieu, si l'Union nationale des indépendants solidaires soutient que, pour respecter le droit de l'Union européenne, un régime légal de sécurité sociale doit, en outre, couvrir l'ensemble de la population active, cette exigence ne résulte d'aucun texte ni d'aucun principe. En particulier, l'union requérante ne peut utilement invoquer la directive 79/7/CEE du Conseil du 19 décembre 1978 relative à la mise en oeuvre progressive du principe de l'égalité de traitement entre hommes et femmes en matière de sécurité sociale, qui s'applique notamment aux régimes légaux assurant une protection contre les risques maladie, invalidité et vieillesse et dont l'article 2 précise qu'elle s'applique à la population active, dès lors que le seul objet de cette directive est d'assurer la mise en oeuvre progressive du principe de l'égalité de traitement entre hommes et femmes en matière de sécurité sociale. Par suite, l'union requérante n'est pas fondée à soutenir que le décret attaqué ni, en tout état de cause, l'ordonnance du 12 juin 2018 et le décret du 5 juillet 2019 méconnaîtraient le droit de l'Union européenne, et en particulier la directive 79/7/CEE du Conseil du 19 décembre 1978, en ce qu'ils comportent des dispositions spécifiques aux seuls travailleurs indépendants.<br/>
<br/>
              4. En deuxième lieu, 1a loi du 22 décembre 2018 de financement de la sécurité sociale pour 2019 a modifié l'article L. 622-3 du code de la sécurité sociale, applicable aux travailleurs indépendants, pour ne plus subordonner le bénéfice des prestations en espèces à la condition que l'assuré soit à jour de ses cotisations annuelles mais disposer que : " Pour bénéficier du règlement des prestations en espèces au titre de l'assurance maladie et maternité pendant une durée déterminée, les personnes mentionnées à l'article L. 611-1 doivent justifier, dans des conditions fixées par décret, d'une période minimale d'affiliation ainsi que du paiement d'un montant minimal de cotisations. / Le revenu d'activité pris en compte pour le calcul de ces prestations est celui correspondant à l'assiette sur la base de laquelle l'assuré s'est effectivement acquitté, à la date de l'arrêt de travail, des cotisations mentionnées à l'article L. 621-1 ". Les dispositions du deuxième alinéa de l'article D. 622-7 du même code, issues du b) du 17° du II de l'article 1er du décret attaqué, prévoient que lorsque le travailleur indépendant assuré social ne s'est pas acquitté de l'intégralité de ses cotisations d'assurance maladie au titre des années civiles servant de base au calcul des indemnités journalières de maladie, et sauf si la caisse lui a accordé des délais de paiement dont il respecte les échéances, il convient d'appliquer le rapport entre le montant des cotisations acquittées et le montant des cotisations dues pour déterminer le revenu à prendre en compte dans le calcul de la prestation. Contrairement à ce que soutiennent les requérants, il ne résulte pas, en tout état de cause, de cet abattement, rendu nécessaire par la circonstance que les travailleurs indépendants cotisent pour eux-mêmes, que le régime d'assurance maladie des travailleurs indépendants, dont les caractéristiques doivent être appréciées dans leur ensemble, ne mettrait pas en oeuvre le principe de solidarité mentionné au point 2. Au demeurant, la mise en oeuvre de ce principe n'est pas une condition de la conformité d'un régime au droit de l'Union européenne mais seulement un critère destiné à apprécier sa soumission ou non aux règles de concurrence résultant du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              5. En dernier lieu, l'article L. 635-1 du code de la sécurité sociale prévoit que les travailleurs indépendants bénéficient d'un régime de retraite complémentaire obligatoire, qui leur assure " l'acquisition et le versement d'une pension exprimée en points ", le montant annuel de la pension individuelle de droit direct servie étant " obtenu par le produit du nombre total de points porté au compte de l'intéressé par la valeur de service du point ", et dont les charges sont couvertes par des cotisations. Enfin, il prévoit que : " L'équilibre financier du régime est assuré par ses seules ressources. Un décret détermine les règles de pilotage du régime, et notamment les conditions dans lesquelles le Conseil de la protection sociale des travailleurs indépendants formule à échéance régulière, au ministre chargé de la sécurité sociale, des règles d'évolution des paramètres permettant de respecter des critères de solvabilité ". Le 50° du II de l'article 1er du décret attaqué modifie l'article D. 635-9 du même code, relatif au régime complémentaire d'assurance vieillesse des travailleurs indépendants, pour substituer au conseil d'administration de la caisse nationale du régime social des indépendants, supprimée, le Conseil de la protection sociale des travailleurs indépendants mentionné à l'article L. 612-1 du même code et pour prévoir que la délibération de ce conseil, intervenant tous les six ans pour la mise en oeuvre des dispositions de l'article L. 635-1 du code de la sécurité sociale, porte non plus sur les règles d'évolution " des valeurs du revenu de référence et de service du point " applicables pour les six années suivantes mais sur celles " des valeurs du revenu de référence servant à la détermination du nombre de points inscrits au compte enregistrant les droits acquis par les assurés d'une part et des valeurs de service d'autre part " applicables pour les six années suivantes. M. B... ne critique pas ces dispositions, qui se bornent à tirer les conséquences de la création du Conseil de la protection sociale des travailleurs indépendants et à expliciter la notion de " revenu de référence ", sans en modifier la portée, mais celles de l'article D. 635-9, non modifiées par le décret attaqué, qui prévoient une délibération tous les six ans sur les règles d'évolution des valeurs du revenu de référence et des valeurs de service du point, lesquelles sont ensuite insérées dans le règlement du régime, approuvé par arrêté du ministre chargé de la sécurité sociale en vertu des articles L. 635-3 et D. 635-5 du code de la sécurité sociale. Ce faisant, il ne critique pas utilement les dispositions du décret attaqué.  <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: Les requêtes de l'Union nationale des indépendants solidaires et de M. B... sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à l'Union nationale des indépendants solidaires, à M. A... B... et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
