<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032432022</ID>
<ANCIEN_ID>JG_L_2016_04_000000375796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/43/20/CETATEXT000032432022.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/04/2016, 375796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:375796.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société Virojanglor a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution sur cet impôt auxquelles elle a été assujettie au titre des années 2004 à 2008 ainsi que des pénalités correspondantes. Par un jugement n° 1002761 du 19 mai 2011, le tribunal administratif de Montreuil a rejeté sa première demande présentée au titre des années 2004 à 2006. Par un jugement n° 1207783, ce même tribunal a rejeté sa seconde demande présentée au titre des années 2007 et 2008.<br/>
<br/>
              Par deux arrêts n° 11VE02615 du 19 décembre 2013 et n° 13VE02525 du 27 mai 2014, la cour administrative d'appel de Versailles a rejeté les appels formés par la société Virojanglor contre ces deux jugements. <br/>
<br/>
              1° Sous le n° 375796, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 26 février 2014, 26 mai 2014 et 7 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Virojanglor demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE02615 du 19 décembre 2013 de la cour administrative d'appel de Versailles ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 383067, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 25 juillet 2014, 24 octobre 2014 et 1er juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société Virojanglor demande au Conseil d'Etat d'annuler l'arrêt n° 13VE02225 du 27 mai 2014 de la cour administrative d'appel de Versailles.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Virojanglor ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois visés ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 38 du code général des impôts, dont les dispositions sont applicables à l'impôt sur les sociétés en vertu de l'article 209 du même code : " 1. Sous réserve des dispositions des articles 33 ter, 40 à 43 bis et 151 sexies, le bénéfice imposable est le bénéfice net, déterminé d'après les résultats d'ensemble des opérations de toute nature effectuées par les entreprises, y compris notamment les cessions d'éléments quelconques de l'actif, soit en cours, soit en fin d'exploitation./ 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés./ " ; qu'aux termes de l'article 39 du même code : " 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant (...) notamment : 1°) Les frais généraux de toute nature, (...) le loyer des immeubles dont l'entreprise est locataire (...) / 2° (...) les amortissements réellement effectués par l'entreprise, dans la limite de ceux qui sont généralement admis d'après les usages de chaque nature d'industrie, de commerce ou d'exploitation (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Virojanglor a pris à bail, à compter du 1er avril 2004, pour une duré de douze ans, des locaux d'une superficie de 2 760 m², comprenant 2400 m² d'entrepôts et 360 m² de bureaux situés en Seine-Saint-Denis ; que le contrat de bail stipulait, outre un loyer annuel de 98 650 euros hors taxes, le versement d'une somme de 350 000 euros, qualifiée de " droit d'entrée " dans le contrat de bail ; que la société l'a comptabilisée en immobilisation incorporelle et a pratiqué un amortissement linéaire sur six ans ; que l'administration fiscale a remis en cause la déduction des annuités d'amortissement pratiquées au titre des années 2004 à 2008, estimant que ce " droit d'entrée ", qui devait être regardé comme un élément incorporel inscrit à l'actif du bilan, ne pouvait donner lieu à amortissement ;<br/>
<br/>
              4. Considérant qu'un élément incorporel de l'actif immobilisé ne peut faire l'objet d'une dotation annuelle à un compte d'amortissements que s'il est normalement prévisible, dès sa création ou son acquisition, que ses effets bénéfiques sur l'exploitation de l'entreprise prendront fin nécessairement à une date déterminée ; que, lorsque tel n'est pas le cas, l'entreprise peut seulement constituer à la clôture de chaque exercice, comme pour tout autre élément d'actif, une provision pour dépréciation correspondant à la différence entre la valeur comptable de l'élément d'actif et sa valeur probable de réalisation ;<br/>
<br/>
              5. Considérant qu'en jugeant que le " droit d'entrée " litigieux ne pouvait donner lieu à aucun amortissement, sans rechercher si certains de ses éléments  pouvaient faire l'objet d'un tel amortissement, alors qu'il résultait des stipulations du contrat de bail qui lui était soumis que la somme de 350 000 euros avait été versée en contrepartie non seulement de la durée exceptionnelle de douze années du bail mais aussi de la renonciation du bailleur à sa faculté de résiliation pendant six ans, avantage indépendant du caractère renouvelable du contrat et dont les effets bénéfiques sur l'exploitation cesseraient à une date prévisible, la cour administrative d'appel a commis une erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que la société Virojanglor est fondée à demander l'annulation des arrêts qu'elle attaque ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Virojanglor au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Les arrêts de la cour administrative d'appel de Versailles des 19 décembre 2013 et 27 mai 2014 sont annulés. <br/>
Article 2 : Les affaires sont renvoyées à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société Virojanglor une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société Virojanglor et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
