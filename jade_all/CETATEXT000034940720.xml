<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034940720</ID>
<ANCIEN_ID>JG_L_2017_06_000000395676</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/07/CETATEXT000034940720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 14/06/2017, 395676, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395676</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395676.20170614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme DPF Investissements a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 2005 et 2006, de la cotisation supplémentaire de contribution additionnelle à cet impôt à laquelle elle a été assujettie au titre de l'année 2005, ainsi que des pénalités correspondantes. Par un jugement n° 1201016 du 11 mars 2014, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14NC00849 du 29 octobre 2015, la cour administrative d'appel de Nancy  a rejeté l'appel formé par la société DPF Investissements contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 décembre 2015, 30 mars 2016 et 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la société DPF Investissements demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la société DPF Investissements.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société anonyme DPF Investissements, société mère d'un groupe fiscalement intégré, a comptabilisé au titre de l'exercice 2002 une provision pour dépréciation des titres qu'elle détenait dans sa filiale, la société Champagne Prin père et fils, et constitué au titre du même exercice une provision pour dépréciation du compte courant ouvert à son nom dans les écritures de cette même filiale, dont elle a accru le montant au titre de l'exercice suivant. Le 1er janvier 2004, la société anonyme Bourg du Midi, membre de ce groupe, a absorbé la société Champagne Prin père et fils, dont elle a repris la dénomination et adopté la forme de société par actions simplifiée. Au titre des exercices 2004, 2005 et 2006, la société DPF Investissements a, d'une part, totalement ou partiellement repris dans ses écritures la provision pour dépréciation de titres ainsi que la provision pour dépréciation de compte courant et, d'autre part, neutralisé ces reprises par la déduction du montant correspondant du résultat d'ensemble du groupe. L'administration a remis en cause cette neutralisation, en estimant que la société DPF Investissements n'était pas fondée à neutraliser sur le fondement des dispositions des articles 223 B et 223 D du code général des impôts les déductions de provisions correspondant à une filiale qui avait été dissoute le 1er janvier 2004 et était ainsi sortie du groupe. La société DPF Investissements a contesté les rectifications résultant de la remise en cause de ces déductions. Par un jugement du 11 mars 2014, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande. Par un arrêt du 29 octobre 2015, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société DPF Investissements contre ce jugement. Cette société se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article 39 du code général des impôts : " 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant, sous réserve des dispositions du 5, notamment : / (...) 5° Les provisions constituées en vue de faire face à des pertes ou charges nettement précisées et que des événements en cours rendent probables, à condition qu'elles aient été effectivement constatées dans les écritures de l'exercice (...) / Les provisions qui, en tout ou en partie, (...) deviennent sans objet au cours d'un exercice ultérieur sont rapportées aux résultats dudit exercice (...) ". Aux termes de l'article 223 B du même code : " Le résultat d'ensemble est déterminé par la société mère en faisant la somme algébrique des résultats de chacune des sociétés du groupe (...). / Il est majoré du montant des dotations complémentaires aux provisions constituées par une société après son entrée dans le groupe, à raison des créances qu'elle détient sur d'autres sociétés du groupe ou des risques qu'elle encourt du fait de telles sociétés (...). Celui-ci est également minoré du montant des provisions rapportées en application du seizième alinéa du 5° du 1 de l'article 39 qui correspondent aux dotations complémentaires non retenues en application du premier alinéa si les sociétés citées aux deux premières phrases de cet alinéa sont membres du groupe ou, s'agissant des provisions mentionnées à la première phrase, d'un même groupe créé ou élargi dans les conditions prévues aux c, d ou e du 6 de l'article 223 L au titre de l'exercice au cours duquel ces provisions sont rapportées (...) ". Aux termes de l'article 223 D du même code : " La plus-value nette ou la moins-value nette à long terme d'ensemble est déterminée par la société mère en faisant la somme algébrique des plus-values ou des moins-values nettes à long terme de chacune des sociétés du groupe, déterminées et imposables selon les modalités prévues aux articles 39 duodecies à 39 quindecies et 217 bis. / (...) Le montant des dotations complémentaires aux provisions constituées par une société après son entrée dans le groupe à raison des participations détenues dans d'autres sociétés du groupe est ajouté à la plus-value nette à long terme d'ensemble ou déduit de la moins-value nette à long terme d'ensemble. En cas de cession entre sociétés du groupe de titres éligibles au régime des plus ou moins-values à long terme, les dotations aux provisions pour dépréciation de ces titres effectuées postérieurement à la cession sont également ajoutées à la plus-value nette à long terme d'ensemble ou retranchées de la moins-value nette à long terme d'ensemble, à hauteur de l'excédent des plus-values ou profits sur les moins-values ou pertes afférent à ces mêmes titres, qui n'a pas été pris en compte, en application du premier alinéa de l'article 223 F, pour le calcul du résultat ou de la plus ou moins-value nette à long terme d'ensemble. Lorsque, en application du deuxième alinéa de l'article 223 F, la société mère comprend dans la plus ou moins-value nette à long terme d'ensemble la plus ou moins-value non prise en compte lors de sa réalisation, la fraction de la provision qui n'a pas été retenue en application de la deuxième phrase du présent alinéa, ni rapportée en application du dix-septième alinéa du 5° du 1 de l'article 39, est, selon le cas, retranchée de la plus-value nette à long terme d'ensemble ou ajoutée à la moins-value nette à long terme d'ensemble. Le montant des provisions rapportées en application de la première phrase du dix-septième alinéa du 5° du 1 de l'article 39 qui correspondent aux dotations complémentaires non retenues en application du présent alinéa est déduit de la plus-value nette à long terme d'ensemble ou ajouté à la moins-value nette à long terme d'ensemble si les sociétés citées aux deux premières phrases de cet alinéa sont membres du groupe ou, s'agissant des provisions mentionnées à la première phrase, d'un même groupe créé ou élargi dans les conditions prévues aux c, d ou e du 6 de l'article 223 L au titre de l'exercice au cours duquel les provisions sont rapportées ".<br/>
<br/>
              3. La société DPF Investissements soutenait devant la cour administrative d'appel que la neutralisation de la reprise des provisions constituées en 2002 et 2003, pour déterminer le résultat du groupe au titre des années 2005 et 2006, ne donnait pas lieu à une déduction indue et ne pouvait, par suite, être refusée. Pour écarter ce moyen, la cour a jugé qu'il résultait des dispositions de l'article 223 D précitées que les dettes de la société Champagne Prin père et fils à l'égard de la société DPF Investissements, pour lesquelles les provisions avaient été constituées, avaient nécessairement été prises en compte pour déterminer le montant des plus-values ou des moins-values réalisées à l'occasion de l'absorption de la société Champagne Prin père et fils par la société Bourg du Midi au cours de l'exercice 2004 et que, par suite, c'était à bon droit que l'administration avait refusé la neutralisation de la reprise de ces provisions. En statuant ainsi, alors que, d'une part, les dispositions de l'article 223 D s'appliquent pour la neutralisation de la reprise des provisions pour dépréciation de titres mais sont sans incidence sur la neutralisation de la reprise de provisions pour dépréciation de créances, laquelle est régie par l'article 223 B précité, et que, d'autre part, il ne ressort pas des pièces du dossier soumis aux juges du fond que les provisions litigieuses aient été constituées uniquement à raison des dettes de la société Champagne Prin père et fils à l'égard de sa société mère, la cour a insuffisamment motivé son arrêt et, par suite, méconnu les dispositions de l'article 223 D du code général des impôts. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société DPF Investissements est fondée à demander l'annulation de l'arrêt attaqué. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société DPF Investissements, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                      --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 29 octobre 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : L'Etat versera à la société DPF Investissements une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société anonyme DPF Investissements et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
