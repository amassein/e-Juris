<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926967</ID>
<ANCIEN_ID>JG_L_2021_07_000000451093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/07/2021, 451093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451093.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation électorale enregistrée le 3 juillet 2020, M. F... D... a demandé au tribunal administratif d'Orléans d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des membres du conseil municipal de Vernouillet (Eure-et-Loir) et de prononcer l'inéligibilité de M. A... C... pour une durée de trois ans. <br/>
<br/>
              Par jugement n° 2002197 du 25 février 2021, ce tribunal a rejeté cette protestation.<br/>
<br/>
              Par une requête enregistrée le 25 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales ;<br/>
<br/>
              3°) de prononcer l'inéligibilité de M. C... pour une durée de trois ans ;<br/>
<br/>
              4°) de mettre à la charge de M. C... et des membres de sa liste une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des élections municipales qui se sont déroulées les 15 mars et 28 juin 2020 dans la commune de Vernouillet (Eure-et-Loir) en vue de la désignation des conseillers municipaux et communautaires, la liste conduite par M. A... C..., premier adjoint sortant, a recueilli 1'535 voix, soit 46,69 % des suffrages exprimés, et obtenu vingt-cinq sièges au conseil municipal et huit sièges au conseil communautaire, la liste conduite par M. F... D... a recueilli 1 382 voix, soit 42,04 % des suffrages exprimés, et obtenu sept sièges au conseil municipal et deux sièges au conseil communautaire et la liste conduite par Mme B... E... a recueilli 226 voix, soit 6,87 % des suffrages exprimés, et obtenu un siège au conseil municipal. M. D... relève appel du jugement du 25 février 2021 par lequel le tribunal administratif d'Orléans a rejeté sa protestation tendant à l'annulation de ces opérations électorales et à ce que M. C... soit déclaré inéligible.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. En premier lieu, il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus d'ordonner la communication des mémoires en défense des conseillers municipaux dont l'élection est contestée aux auteurs des protestations, ni des autres mémoires ultérieurement enregistrés et qu'il appartient seulement aux parties, si elles le jugent utile, de prendre connaissance de ces défenses et mémoires ultérieurs au greffe du tribunal administratif. Ainsi, le défaut de communication de ces mémoires n'entache pas la décision juridictionnelle d'irrégularité, même s'ils contiennent des éléments nouveaux. <br/>
<br/>
              3. Il est constant que M. D... et son conseil ont été informés de la possibilité de suivre l'état de l'instruction sur l'application Sagace. Le requérant doit ainsi être regardé comme ayant été informé de l'enregistrement du mémoire en défense présenté par M. C... le 12 février 2021, lequel a été tenu à sa disposition au greffe du tribunal administratif d'Orléans. M. D... n'est, par suite, fondé à soutenir ni que le jugement aurait été rendu en méconnaissance du principe du caractère contradictoire de la procédure juridictionnelle, ni qu'il aurait été porté atteinte au droit à un procès équitable garanti par les stipulations de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              Sur les griefs présentés dans le mémoire complémentaire du 10 février 2021 :<br/>
<br/>
              4. En vertu des dispositions de l'articles R. 119 du code électoral, les réclamations contre les opérations électorales doivent être consignées au procès-verbal ou être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. En l'espèce, le délai de recours contentieux contre les opérations électorales du second tour des élections municipales et communautaires, qui s'est tenu le 28 juin 2020, expirait le 3 juillet à dix-huit heures.<br/>
<br/>
              5. Il résulte de ce qui précède que les griefs soulevés dans le mémoire complémentaire produit par M. D... le 10 février 2021, tirés de ce que M. C... aurait procédé à des affichages non autorisés en méconnaissance de l'article L. 51 du code électoral, diffusé tardivement des éléments nouveaux de polémique électorale et participé en sa qualité de premier adjoint au maire à des actions de promotion publicitaire des réalisations de l'équipe municipale sortante en méconnaissance des dispositions de l'article L. 52-1 du même code, qui ont été  présentés pour la première fois après l'expiration du délai de recours contentieux et qui ne constituent pas le développement de griefs soulevés dans la protestation initiale, sont, ainsi que l'a jugé le tribunal administratif, irrecevables.<br/>
<br/>
              Sur le grief tiré de l'utilisation des moyens de la commune à des fins de propagande électorale :<br/>
<br/>
              6. Aux termes de l'article aux termes de l'article L. 52-8 du code électoral : " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. Les personnes morales, à l'exception des partis et groupements politiques ainsi que des établissements de crédit ou sociétés de financement ayant leur siège social dans un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, ne peuvent ni consentir des prêts à un candidat, ni lui apporter leur garantie pour l'obtention de prêts (...) ". <br/>
<br/>
              7. En premier lieu, si l'équipe de campagne de M. C... a réalisé la photographie de groupe utilisée pour la profession de foi de la liste " Vernouillet notre ville " dans les locaux de la commune, il résulte de l'instruction que les modalités de prêt des salles communales ont été définies par la mairie avant le début de la campagne électorale afin de garantir à chacun des candidats la possibilité de bénéficier, sur sa demande, de locaux de la municipalité. Par suite, M. D..., qui ne soutient pas que sa propre liste aurait été privée de cette possibilité, n'est pas fondé à soutenir que l'utilisation à cette occasion d'une salle municipale par M. C... aurait constitué un don d'une personne morale prohibé par les dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              8. En deuxième lieu, si M. C... a pris part à la mise en oeuvre d'opérations liées à la gestion par la commune de l'épidémie de covid-19, notamment par la distribution de masques aux résidents de maisons de retraite et la mise en place d'une chaine téléphonique pour le portage de repas et de médicaments à domicile, il n'est pas établi que les actions ainsi menées avec des moyens mis à disposition par la commune de Vernouillet auraient en réalité constitué, du fait d'une valorisation excessive de l'action de l'équipe municipale sortante et en particulier de son premier adjoint, un élément de la campagne électorale de ce dernier. Il résulte au contraire de l'instruction, d'une part, que la commune a proposé à l'ensemble des élus de s'associer à la distribution des masques et établi un planning à cet effet et, d'autre part, que M. D... et plusieurs conseillers municipaux d'opposition ont participé à ces actions. Par suite, le grief tiré de ce que M. C... aurait utilisé à des fins électorales les moyens mis en oeuvre par la commune dans le cadre de la gestion de la crise sanitaire ne peut qu'être écarté. <br/>
<br/>
              9. En troisième lieu, si M. C... a organisé le 24 juin 2020, dans le cadre de sa campagne électorale, une réunion de quartier à laquelle le médiateur de la commune a assisté, il résulte de l'instruction que ce dernier y a participé en sa seule qualité d'électeur de la commune, en dehors de ses horaires de travail, sans qu'il soit établi qu'il aurait fait état de sa qualité et ainsi exercé des pressions sur les électeurs. Par suite, M. D... n'est fondé à soutenir ni que la participation du médiateur de la commune à cette réunion aurait méconnu les dispositions de l'article L. 52-8 du code électoral, ni qu'elle aurait porté atteinte à la sincérité du scrutin.<br/>
<br/>
              Sur le grief tiré de la méconnaissance des dispositions de l'article R. 57 du code électoral :<br/>
<br/>
              10. Aux termes de l'article L. 97 du code électoral : " Ceux qui, à l'aide de fausses nouvelles, bruits calomnieux ou autres manoeuvres frauduleuses, auront surpris ou détourné des suffrages, déterminé un ou plusieurs électeurs à s'abstenir de voter, seront punis d'un emprisonnement d'un an et d'une amende de 15 000 euros. " Selon l'article R. 57 du même code : " Le président du bureau de vote constate publiquement et mentionne au procès-verbal l'heure d'ouverture et l'heure de clôture du scrutin. / Aucun vote ne peut être reçu après la déclaration de clôture. Toutefois, un électeur ayant pénétré dans la salle de vote avant l'heure de clôture du scrutin peut déposer son bulletin dans l'urne ou faire enregistrer son suffrage par la machine à voter après cette heure ".<br/>
<br/>
              11. Si M. D... soutient que la présidente du bureau de vote n° 5, colistière de M. C..., aurait, peu avant l'heure de fermeture de ce bureau, informé les personnes présentes dans la file d'attente qu'elle mettrait fin aux opérations de vote à 18h00 précises, ce qui aurait eu, selon le protestataire, pour effet de faire renoncer plusieurs personnes à exprimer leur suffrage, il résulte de l'instruction qu'aucune observation ou réclamation n'a été consignée au procès-verbal mentionnant que des électeurs auraient été dissuadés ou empêchés de voter. Il résulte au contraire de deux attestations que si le portail d'entrée de l'école Gérard Philippe, qui abritait le bureau de vote n° 5, a été fermé à 18h00, tel n'a pas été le cas de la porte d'entrée du bureau de vote de sorte que toutes les électeurs présents à cette heure dans la cour de l'école ont pu effectivement voter. Par suite, le grief tiré de la méconnaissance de l'article R. 57 du code électoral ne peut qu'être écarté, tout comme celui tiré de ce que ces faits seraient constitutifs d'une manoeuvre destinée à inciter certains électeurs à s'abstenir de voter, réprimée par les dispositions de l'article L. 97 du même code.<br/>
<br/>
              Sur le grief tiré de la méconnaissance des dispositions des articles L. 67 et R. 67 du code électoral :<br/>
<br/>
              12. Aux termes de l'article L. 67 du code électoral : " Tout candidat ou son représentant dûment désigné a le droit de contrôler toutes les opérations de vote, de dépouillement des bulletins et de décompte des voix, dans tous les locaux où s'effectuent ces opérations, ainsi que d'exiger l'inscription au procès-verbal de toutes observations, protestations ou contestations sur lesdites opérations, soit avant la proclamation du scrutin, soit après. Les modalités d'application du présent article sont déterminées par un décret en Conseil d'Etat ". Aux termes de l'article R. 67 du code électoral : " Immédiatement après la fin du dépouillement, le procès-verbal des opérations électorales est rédigé par le secrétaire dans la salle de vote, en présence des électeurs. / Il est établi en deux exemplaires, signés de tous les membres du bureau. / Les délégués des candidats ou listes en présence sont obligatoirement invités à contresigner ces deux exemplaires. / Dès l'établissement du procès-verbal, le résultat est proclamé en public par le président du bureau et affiché en toutes lettres par ses soins dans la salle de vote ".<br/>
<br/>
              13. Il résulte de l'instruction que le procès-verbal des opérations de vote du bureau de vote n° 7 a été rédigé non pas dans la salle de vote elle-même, ainsi que le prescrivent les dispositions de l'article R. 67 du code électoral citées au point précédent, mais dans une pièce adjacente, comme l'indiquent les mentions portées sur ce procès-verbal par deux personnes présentes dans le bureau de vote. Toutefois, si M. D... soutient que la présidente du bureau de vote aurait interdit à tous les assesseurs et aux délégués des différentes listes de pénétrer dans cette pièce, il résulte des mentions d'une attestation établie par la présidente du bureau de vote et cosignée par la secrétaire de celui-ci que sa porte était restée ouverte. M. D... ne conteste pas que le procès-verbal y a été rédigé sous le regard de deux délégués de sa liste. Il n'est par ailleurs ni établi, ni même allégué que le procès-verbal comporterait des erreurs ou ne reflèterait pas les résultats exacts du dépouillement du scrutin dans le bureau de vote concerné. Dans ces conditions, et pour regrettable que soit cette méconnaissance des dispositions de l'article R. 67 du code électoral, cet incident ne peut être regardé comme constitutif d'une manoeuvre ayant porté atteinte à la sincérité du scrutin. <br/>
<br/>
              Sur les conclusions tendant au prononcé de l'inéligibilité de M. C... :<br/>
<br/>
              14. Dès lors qu'il ne résulte pas de ce qui précède que M. C... se serait livré à des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin, M. D... n'est pas fondé à demander qu'il soit déclaré inéligible sur le fondement de l'article L. 118-4 du code électoral. <br/>
<br/>
              Sur les conclusions relatives aux frais exposés et non compris dans les dépens :<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C... et de ses colistiers qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de M. D... à ce même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : Les conclusions de M. C... présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. F... D..., à M. A... C..., premier dénommé pour l'ensemble des élus de la liste " Vernouillet notre ville " et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
