<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038466951</ID>
<ANCIEN_ID>JG_L_2019_05_000000418823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/69/CETATEXT000038466951.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 13/05/2019, 418823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418823.20190513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée au secrétariat du contentieux du Conseil d'Etat le 6 mars 2018, le Syndicat National des Praticiens Hospitaliers Anesthésistes-Réanimateurs élargi (SNPHARE) demande au Conseil d'Etat :<br/>
<br/>
              1° d'annuler la décision implicite de rejet née du silence gardé par la ministre des solidarités et de la santé sur la demande qu'il a adressée le 6 novembre 2017 tendant, d'une part, à ce que soient abrogées la circulaire n° DGOS/RH3/2013/129 du 29 mars 2013 relative à l'incidence des congés pour raison de santé et l'instruction n° DGOS/RH3/DGCS/2013/354 du 1er octobre 2013 relative à l'incidence du congé de maternité, du congé d'adoption, du congé de paternité et du congé parental sur le report des congés annuels des personnels médicaux des établissements publics de santé et, d'autre part, à ce que soit pris un décret en Conseil d'Etat insérant, dans le statut des praticiens hospitaliers, le droit au report, pendant une durée de deux ans, des congés annuels non pris ainsi que le droit à l'indemnisation des congés non pris en cas de cessation d'activité ;<br/>
<br/>
              2° d'enjoindre au Premier ministre de prendre un décret prévoyant de telles règles en application de l'article L. 6152-6 du code de la santé publique dans un délai de 3 mois à compter de la décision à intervenir sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3° de mettre à la charge de l'Etat une somme de 2 500 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, par un courrier du 6 novembre 2017, le Syndicat National des Praticiens Hospitaliers Anesthésistes-Réanimateurs élargi (SNPHARE) a demandé, d'une part, l'abrogation de la circulaire du ministre chargé de la santé du 29 mars 2013 relative à l'incidence des congés pour raison de santé sur le report des congés annuels des personnels médicaux et de l'instruction du même ministre du 1er octobre 2013 relative à l'incidence du congé de maternité, du congé d'adoption, du congé de paternité et du congé parental sur le report des congés annuels des personnels médicaux des établissements publics de santé et, d'autre part, que soit pris un décret en Conseil d'Etat insérant, dans le statut des praticiens hospitaliers, le droit au report, pendant une durée de deux ans, de congés annuels non pris ainsi que le droit à l'indemnisation des congés non pris en cas de cessation d'activité. Le SNPHARE demande l'annulation pour excès de pouvoir du rejet implicite de cette demande.<br/>
<br/>
              2. Aux termes de l'article L. 6152-6 du code de la santé publique : " Sont déterminées par décret en Conseil d'Etat les mesures réglementaires prévues aux articles L. 6152-1, L. 6152-4 et, en tant que de besoin, les modalités d'application des autres dispositions du présent chapitre et de l'article L. 6152-1-1 ". Aux termes de l'article L. 6152-1 du même code : " Le personnel des établissements publics de santé comprend, outre les agents relevant de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, les personnels enseignants et hospitaliers mentionnés à l'article L. 952-21 du code de l'éducation et les personnels mentionnés à l'article L. 6147-9 qui y exercent : / 1° Des médecins, des odontologistes et des pharmaciens dont le statut, qui peut prévoir des dispositions spécifiques selon que ces praticiens consacrent tout ou partie de leur activité à ces établissements, est établi par voie réglementaire (...) ".<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              En ce qui concerne le refus d'abroger la circulaire du 29 mars 2013 et l'instruction du 1er octobre 2013 : <br/>
<br/>
              3. Il ressort des termes mêmes des circulaires litigieuses qu'elles ont pour objet d'énoncer des règles en matière de report du droit au congé annuel non pris par le personnel médical absent en raison des différentes catégories de congés qu'elles visent. De telles règles, qui commandent le bénéfice du droit à congé annuel pour les agents qu'un congé de maladie, un congé de maternité, un congé de paternité, un congé d'adoption ou un congé parental ont placés dans l'impossibilité d'exercer ce droit au cours de l'année au titre de laquelle il est né, présentent un caractère statutaire et ne peuvent, en conséquence, être édictées que par décret en Conseil d'Etat en application des dispositions des articles L. 6152-1 et L. 6152-6 du code de la santé publique. Dans ces conditions, les circulaires litigieuses sont entachées d'incompétence et le syndicat requérant est fondé à demander l'annulation du refus opposé par le ministre chargé de la santé à sa demande tendant à leur abrogation.<br/>
<br/>
              4. Au demeurant, si les circulaires litigieuses se présentent comme ayant pour objet d'expliciter la portée du droit de l'Union européenne tel qu'il a été précisé par la jurisprudence de la Cour de justice de l'Union européenne, elles se livrent à une inexacte interprétation de cette jurisprudence. En effet, d'une part, les circulaires contestées ne permettent de reporter les congés annuels non pris en raison des différentes catégories de congés qu'elles visent que jusqu'au 31 décembre de l'année suivant celle au titre de laquelle ils sont dus, prévoyant ainsi une période de report d'une durée égale à celle de la période de référence, alors que la Cour de justice de l'Union européenne, par son arrêt C-214/10 du 22 novembre 2011, relatif à l'incidence des seules absences pour raison de santé, a dit pour droit que les congés annuels non pris pour cause de maladie doivent pouvoir être reportés sur une période qui doit " dépasser substantiellement la durée de la période de référence pour laquelle elle est accordée ". D'autre part, les circulaires contestées prévoient qu'un congé non pris ne donne lieu à aucune indemnité compensatrice contrairement à ce que la Cour de justice a jugé par son arrêt C-350/06 et C-520/06 du 20 janvier 2009.<br/>
<br/>
              En ce qui concerne le refus de modifier le statut des praticiens hospitaliers :<br/>
<br/>
              5. Aucune des dispositions du code de la santé publique relatives au statut des praticiens hospitaliers ne porte ni sur le droit au report des congés annuels ni sur l'indemnisation des congés annuels non pris. S'il incombe à l'autorité hiérarchique de veiller au respect des exigences du droit de l'Union européenne, et s'il est loisible au Premier ministre de fixer en la matière des règles statutaires adoptées par décret en Conseil d'Etat, il ne résulte ni de ce qui précède ni des éléments avancés par l'argumentation de la requête que le Premier ministre serait tenu de compléter sur les points en débat le statut des praticiens hospitaliers. Le syndicat requérant n'est, dès lors, pas fondé à demander l'annulation de la décision rejetant sa demande tendant à l'adoption d'un décret en Conseil d'Etat insérant, dans le statut des praticiens hospitaliers, le droit au report, pendant une durée de deux ans, des congés annuels non pris ainsi que le droit à l'indemnisation des congés non pris en cas de cessation d'activité.<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              6. L'exécution de la présente décision n'appelle pas nécessairement l'intervention d'un décret relatif au report des congés de maladie. Par suite, les conclusions à fin d'injonction de la requête, qui tendent à ce qu'il soit enjoint au Premier ministre de prendre un tel décret, ne peuvent qu'être rejetées.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite de la ministre des solidarités et de la santé est annulée en tant qu'elle a refusé d'abroger la circulaire du 29 mars 2013 et l'instruction du 1er octobre 2013.<br/>
<br/>
Article 2 : L'Etat versera la somme de 2 500 euros au Syndicat National des Praticiens Hospitaliers Anesthésistes-Réanimateurs élargi (SNPHARE) au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Syndicat National des Praticiens Hospitaliers Anesthésistes-Réanimateurs élargi (SNPHARE), au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
