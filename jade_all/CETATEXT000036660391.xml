<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036660391</ID>
<ANCIEN_ID>JG_L_2018_02_000000404602</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/03/CETATEXT000036660391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 28/02/2018, 404602, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404602</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:404602.20180228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La province Sud de Nouvelle-Calédonie a demandé au juge des référés du tribunal administratif de Nouvelle-Calédonie d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à M. A...B..., ainsi qu'à tous occupants de son chef, de retirer tous les biens lui appartenant ou amenés par lui et d'évacuer la parcelle du domaine public maritime qu'il occupe selon elle illégalement dans un délai de deux mois, sous astreinte de 10 000 francs CFP par jour de retard à compter de l'expiration de ce délai, faute de quoi il y sera procédé d'office et à ses frais par la province Sud, au besoin avec le concours de la force publique. Par une ordonnance n° 1600352 du 5 octobre 2016, le juge des référés a partiellement fait droit à cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 20 octobre 2016, 4 novembre 2016 et 12 février 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la demande de la province Sud de Nouvelle-Calédonie ;<br/>
<br/>
              3°) de mettre à la charge de la province Sud de Nouvelle-Calédonie la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - la loi du pays n° 2001-017 du 11 janvier 2002 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la province Sud de Nouvelle-Caledonie ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 février 2018, présentée par la province Sud de Nouvelle-Caledonie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la province Sud de Nouvelle-Calédonie a demandé au juge des référés du tribunal administratif de Nouvelle-Calédonie d'enjoindre, sur le fondement de l'article L. 521-3 du code de justice administrative, à M. A...B..., ainsi qu'à tous occupants de son chef, de retirer tous les biens lui appartenant ou amenés par lui et d'évacuer la parcelle du domaine public maritime qu'il occupe, selon elle illégalement, sur le territoire de la commune de Moindou  dans un délai de deux mois, sous astreinte de 10 000 francs CFP par jour de retard à compter de l'expiration de ce délai, faute de quoi il y sera procédé d'office et à ses frais par la province Sud, au besoin avec le concours de la force publique. Par une ordonnance du 5 octobre 2016, le juge des référés a partiellement fait droit à cette demande. M. B...se pourvoit en cassation contre cette ordonnance. La province Sud de Nouvelle-Calédonie, par la voie du pourvoi incident, demande l'annulation de cette ordonnance en tant qu'elle n'a pas fait droit à ses conclusions relatives aux biens immobiliers appartenant à M.B....<br/>
<br/>
              2. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". En vertu du premier alinéa de l'article L. 521-3-1 du même code : " La condition d'urgence prévue à l'article L. 521-3 n'est pas requise en cas de requête relative à une occupation non autorisée de la zone des cinquante pas géométriques ".<br/>
<br/>
              3. En se bornant à relever qu'il résultait de l'instruction et notamment du procès-verbal de constat établi le 15 février 2016 par un agent assermenté de la province Sud de Nouvelle-Calédonie que la parcelle occupée par M. B...se trouvait incluse dans la zone des cinquante pas géométriques, sans répondre à l'argumentation de ce dernier qui contestait que la totalité de la parcelle en litige se trouvait dans cette zone et sans se prononcer sur la valeur probante de la vue aérienne produite par l'intéressé à l'appui de sa contestation et comportant des indications de distance tendant à montrer que des habitations visées par la demande de la province Sud se trouvaient au-delà de la limite de la zone des cinquante pas géométriques, le juge des référés du tribunal administratif de Nouvelle-Calédonie a insuffisamment motivé son ordonnance. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens, que M. B...est fondé à demander l'annulation de l'ordonnance qu'il attaque. Cette annulation prive d'objet les conclusions du pourvoi incident de la province Sud de Nouvelle-Calédonie.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la province Sud de Nouvelle-Calédonie en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de l'instruction, et n'est pas contesté, que M. B... a quitté la parcelle en litige qu'il occupait. Par suite, la demande de la province Sud tendant à ce que soit ordonnée son expulsion est devenue sans objet. Dès lors, il n'y a plus lieu d'y statuer.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M.B..., d'une part, et par la province Sud, d'autre part, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 5 octobre 2016 du juge des référés du tribunal administratif de Nouvelle-Calédonie est annulée.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions du pourvoi incident de la province Sud de Nouvelle-Calédonie.<br/>
Article 3 : Il n'y a pas lieu de statuer sur la demande présentée par la province Sud de Nouvelle-Calédonie devant le juge des référés du tribunal administratif de Nouvelle-Calédonie.<br/>
Article 4 : Les conclusions de M. B...et de la province Sud de Nouvelle-Calédonie présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A... B...et à la province Sud de Nouvelle-Calédonie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
