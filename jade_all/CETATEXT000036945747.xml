<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945747</ID>
<ANCIEN_ID>JG_L_2018_05_000000408614</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945747.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème chambre jugeant seule, 25/05/2018, 408614, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408614</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408614.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 29 mai 2013, par lequel la maire d'Aix-en-Provence a nommé M. D... en qualité d'attaché territorial stagiaire ainsi que la décision implicite de rejet résultant du silence gardé par la maire d'Aix-en-Provence sur sa demande du 17 juin 2013 tendant au retrait de cette nomination et à une nouvelle saisine de la commission administrative paritaire après détermination des critères permettant d'évaluer la valeur professionnelle des agents mis à la disposition des organisations syndicales. Par un jugement n° 1306484 du 22 décembre 2014, le tribunal administratif de Marseille a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 15MA00980 du 27 décembre 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune d'Aix-en-Provence  contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 3 mars 2017, 6 juin 2017 et 26 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la commune d'Aix-en-Provence  demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. C...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-1099 du 30 décembre 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la commune d'Aix-en-Provence et à la SCP Thouvenin, Coudray, Grevy, avocat de M. C....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D...a été inscrit sur la liste d'aptitude établie après avis de la commission administrative paritaire de la commune d'Aix-en-Provence du 11 avril 2013 puis nommé attaché territorial stagiaire par arrêté de la maire du 29 mai 2013.  M.C..., éducateur territorial des activités physiques et sportives figurant sur la liste des candidats susceptibles d'être promus attaché territorial soumise à cette commission, a demandé à la maire d'Aix-en-Provence de retirer l'arrêté de nomination de M. D... et de saisir à nouveau la commission administrative paritaire. M. C...a saisi le tribunal administratif de Marseille de la décision implicite de rejet née du silence gardé sur sa demande. Par jugement du 22 décembre 2014, celui-ci a annulé l'arrêté du 29 mai 2013. La commune d'Aix-en-Provence se pourvoit en cassation contre l'arrêt du 26 décembre 2016 par lequel la cour administrative de Marseille a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. Pour demander l'annulation du jugement qu'elle attaquait, la commune d'Aix-en-Provence soutenait notamment que le tribunal administratif avait à tort écarté la fin de non-recevoir qu'elle avait soulevée devant lui, tirée du défaut d'intérêt donnant qualité pour agir à M. C.... La cour ne s'est pas prononcée sur ce moyen, qui n'était pas inopérant. Par suite, son arrêt doit être annulé.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              Sur la fin de non-recevoir tirée du défaut d'intérêt pour agir de M.C... :<br/>
<br/>
              4. Les fonctionnaires appartenant à une administration publique ont qualité pour déférer à la juridiction administrative les nominations illégales faites dans cette administration, lorsque celles-ci sont de nature à leur porter préjudice en retardant irrégulièrement leur avancement ou en leur donnant pour cet avancement des concurrents qui ne satisfont pas aux conditions exigées par les lois ou règlements. M.C..., éducateur territorial des activités physiques et sportives qui figurait, fût-ce en 75ème position, sur la liste des 168 agents susceptibles d'être promus dans le cadre d'emplois des attachés territoriaux soumise à la commission administrative paritaire, avait vocation à  être nommé dans ce cadre d'emploi. Il justifiait ainsi, contrairement à ce que soutient la commune, d'un intérêt lui donnant qualité pour demander l'annulation de la nomination de M.D.... Par suite, la fin de non-recevoir opposée par la commune d'Aix-en-Provence doit être écartée.<br/>
<br/>
              Sur le fond :<br/>
<br/>
              5. Aux termes de l'article 39 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " En vue de favoriser la promotion interne, les statuts particuliers fixent une proportion de postes susceptibles d'être proposés au personnel appartenant déjà à l'administration ou à une organisation internationale intergouvernementale, non seulement par voie de concours, selon les modalités définies au 2° de l'article 36, mais aussi par la nomination de fonctionnaires ou de fonctionnaires internationaux, suivant l'une des modalités ci-après : / (...) 2° Inscription sur une liste d'aptitude établie après avis de la commission administrative paritaire compétente, par appréciation de la valeur professionnelle et des acquis de l'expérience professionnelle des agents ". Aux termes de l'article 3 du décret du 30 décembre 1987 portant statut particulier du cadre d'emplois des attachés territoriaux : " Le recrutement en qualité d'attaché intervient après inscription sur les listes d'aptitude établies : / (...) 2° En application des dispositions du 2° de l'article 39 de [la] loi [du 26 janvier 1984] ". Aux termes de l'article 5 du même décret : " Peuvent être inscrits sur la liste d'aptitude prévue au 2° de l'article 3 ci-dessus : / 1° Les fonctionnaires territoriaux qui justifient de plus de cinq années de services effectifs accomplis en qualité de fonctionnaire territorial de catégorie B en position d'activité ou de détachement ; / 2° Les fonctionnaires territoriaux de catégorie B qui ont exercé les fonctions de directeur général des services des communes de 2 000 à 5 000 habitants pendant au moins deux ans (...) ". Aux termes de l'article 1er de ce décret : " Les attachés territoriaux constituent un cadre d'emplois administratif de catégorie A au sens de l'article 5 de la loi n° 84-53 du 26 janvier 1984 susvisée. / Ce cadre d'emplois comprend les grades d'attaché, d'attaché principal, de directeur territorial. ". Aux termes de l'article 2 du même décret : " Les membres du cadre d'emplois exercent leurs fonctions sous l'autorité des directeurs généraux des services des départements et des régions, des secrétaires généraux ou secrétaires des communes ou des directeurs d'établissements publics et, le cas échéant, des directeurs généraux adjoints des départements et des régions, des secrétaires généraux adjoints des communes, des directeurs adjoints des établissements publics ou des administrateurs territoriaux en poste dans la collectivité ou l'établissement.  / Ils participent à la conception, à l'élaboration et à la mise en oeuvre des politiques décidées dans les domaines administratif, financier, économique, sanitaire, social, culturel, de l'animation et de l'urbanisme. Ils peuvent ainsi se voir confier des missions, des études ou des fonctions comportant des responsabilités particulières, notamment en matière de gestion des ressources humaines, de gestion des achats et des marchés publics, de gestion financière et de contrôle de gestion, de gestion immobilière et foncière et de conseil juridique. Ils peuvent également être chargés des actions de communication interne et externe et de celles liées au développement, à l'aménagement et à l'animation économique, sociale et culturelle de la collectivité. Ils exercent des fonctions d'encadrement et assurent la direction de bureau ou de service ".<br/>
<br/>
              6. D'une part, il ressort des pièces du dossier qu'une liste de 168 agents de la commune d'Aix-en-Provence remplissant les conditions pour être promus attaché territorial a été soumise à la commission administrative paritaire qui s'est réunie le 11 avril 2013 et que, lors des débats, la maire a précisé que M.D..., qui figurait en 50ème position du classement opéré par cette liste sur la base de la valeur professionnelle et l'ancienneté des agents, méritait une promotion " au regard des heures de travail fourni et de ce qu'il a subi lors de la précédente municipalité, même s'il ne figure pas en début de liste ". D'autre part, il ressort de la fiche de poste de M. D... que ce dernier était chargé de missions, relevant habituellement de la catégorie C, consistant à conduire les élus sur différents sites, à entretenir et nettoyer son véhicule, à organiser les déplacements du maire, à assurer la logistique liée à ces déplacements et à renseigner les administrés durant ses déplacements, alors que les fonctions des deux agents dont l'inscription sur la liste d'aptitude a été débattue au cours de la réunion de la commission du 11 avril 2013 consistaient respectivement en la direction d'un service comprenant 50 agents et en la gestion des relations institutionnelles et financières avec les crèches, gérant notamment un budget annuel de 6 millions d'euros ainsi qu'une délégation de service public concernant environ 400 personnes. Il résulte de ce qui précède qu'en inscrivant M. D... sur la liste d'aptitude établie à la suite de la commission administrative paritaire puis en procédant à sa nomination dans le cadre d'emploi des attachés territoriaux, alors même que ce dernier  contribuerait ainsi qu'il est soutenu, dans l'exercice de ses missions, à la bonne coordination entre les services de la commune, la maire de la commune d'Aix-en-Provence a commis une erreur manifeste d'appréciation de la valeur et de l'expérience professionnelle de l'intéressé au regard des dispositions de l'article 39 de la loi du 26 janvier 1984. Il résulte de ce qui précède que la commune d'Aix-en-Provence n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a prononcé l'annulation de l'arrêté attaqué. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Aix-en-Provence la somme de 3 000 euros à verser à M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.C..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 27 décembre 2016 est annulé.<br/>
<br/>
Article 2 : La requête présentée par la commune d'Aix-en-Provence devant la cour administrative d'appel de Marseille est rejetée.<br/>
<br/>
Article 3 : Les conclusions de la commune d'Aix-en-Provence présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La commune d'Aix-en-Provence versera à M. C...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune d'Aix-en-Provence et à M. B... C....<br/>
Copie en sera adressée à M. A...D....  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
