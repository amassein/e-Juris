<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233084</ID>
<ANCIEN_ID>JG_L_2017_12_000000380438</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/30/CETATEXT000036233084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 18/12/2017, 380438</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380438</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:380438.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...et Mme C...D..., épouseB..., ont demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir l'arrêté du 9 juillet 2010 par lequel le maire de Lambres-lez-Douai a refusé de leur délivrer un permis de construire pour la construction d'une habitation. Par un jugement n° 1006810 du 20 septembre 2012, le tribunal administratif de Lille a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12DA01691 du 25 mars 2014, la cour administrative d'appel de Douai a rejeté l'appel formé contre ce jugement du tribunal administratif de Lille par M. et Mme B....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 mai et 19 août 2014 et le 2 août 2016 au secrétariat du contentieux du Conseil d'État, M. et Mme B...demandent au Conseil d'État : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Lambres-lez-Douai la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. et Mme B...et à Me Balat, avocat de la commune de Lambres-lez-Douai.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué et des pièces du dossier soumis aux juges du fond que M. et MmeB..., propriétaires d'un terrain sur le territoire de la commune de Lambres-lez-Douai (Nord), ont demandé à la commune un certificat d'urbanisme dans la perspective de la construction d'une habitation sur ce terrain ; que le maire leur a délivré le 29 novembre 2009 un certificat d'urbanisme qualifié de " négatif " précisant qu'un sursis à statuer pourrait être opposé à une demande de permis de construire qui viendrait remettre en cause l'économie générale du plan local d'urbanisme alors en cours de modification ; que par une décision du 9 juillet 2010, le maire a refusé de leur délivrer un permis de construire une habitation sur le terrain en cause, sur le fondement du plan local d'urbanisme modifié le 16 décembre 2009 ; que le 13 septembre 2010, le maire a rejeté le recours gracieux formé par les époux B...contre cette décision ; que par un jugement du 20 septembre 2012, le tribunal administratif de Lille a rejeté leur demande d'annulation pour excès de pouvoir du refus de permis de construire ; que, par un arrêt du 25 mars 2014 contre lequel M. et Mme B...se pourvoient en cassation, la cour administrative d'appel de Douai a rejeté l'appel qu'ils ont formé contre le jugement du tribunal administratif ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 410-1 du code de l'urbanisme, dans sa rédaction issue de l'ordonnance du 8 décembre 2005 relative au permis de construire et aux autorisations d'urbanisme : " Le certificat d'urbanisme, en fonction de la demande présentée : / a) Indique les dispositions d'urbanisme, les limitations administratives au droit de propriété et la liste des taxes et participations d'urbanisme applicables à un terrain ; / b) Indique en outre, lorsque la demande a précisé la nature de l'opération envisagée ainsi que la localisation approximative et la destination des bâtiments projetés, si le terrain peut être utilisé pour la réalisation de cette opération ainsi que l'état des équipements publics existants ou prévus. / Lorsqu'une demande d'autorisation ou une déclaration préalable est déposée dans le délai de dix-huit mois à compter de la délivrance d'un certificat d'urbanisme, les dispositions d'urbanisme, le régime des taxes et participations d'urbanisme ainsi que les limitations administratives au droit de propriété tels qu'ils existaient à la date du certificat ne peuvent être remis en cause à l'exception des dispositions qui ont pour objet la préservation de la sécurité ou de la salubrité publique. / Lorsque le projet est soumis à avis ou accord d'un service de l'État, les certificats d'urbanisme le mentionnent expressément. Il en est de même lorsqu'un sursis à statuer serait opposable à une déclaration préalable ou à une demande de permis. / Le certificat d'urbanisme est délivré dans les formes, conditions et délais déterminés par décret en Conseil d'État par l'autorité compétente mentionnée au a et au b de l'article L. 422-1 du présent code. " ; que l'article R. 410-12 du même code dispose : " À défaut de notification d'un certificat d'urbanisme dans le délai fixé par les articles R. 410-9 et R. 410-10, le silence gardé par l'autorité compétente vaut délivrance d'un certificat d'urbanisme tacite. Celui-ci a exclusivement les effets prévus par le quatrième alinéa de l'article L. 410-1, y compris si la demande portait sur les éléments mentionnés au b de cet article. " ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 410-1 du code de l'urbanisme ont pour effet de garantir à la personne à laquelle a été délivré un certificat d'urbanisme, quel que soit son contenu, un droit à voir sa demande de permis de construire déposée durant les dix-huit mois qui suivent, examinée au regard des dispositions d'urbanisme applicables à la date de ce certificat, à la seule exception de celles qui ont pour objet la préservation de la sécurité ou de la salubrité publique ; que, par suite, en jugeant que " les certificats d'urbanisme négatifs ne confèrent aucun droit à leur titulaire ", la cour a entaché son arrêt d'erreur de droit ; qu'ainsi, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. et Mme B...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article R. 411-1 du code de justice administrative : " La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge. / L'auteur d'une requête ne contenant l'exposé d'aucun moyen ne peut la régulariser par le dépôt d'un mémoire exposant un ou plusieurs moyens que jusqu'à l'expiration du délai de recours. " ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces de la procédure devant les juges du fond que M. et Mme B...ont présenté devant la cour administrative d'appel, dans le délai de recours, une requête d'appel qui ne constitue pas la seule reproduction littérale de leur mémoire de première instance et énonce à nouveau, de manière précise, les critiques adressées à la décision dont il ont demandé l'annulation au tribunal administratif ; qu'ainsi, elle ne méconnaît pas les exigences posées par l'article R. 411-1 du code de justice administrative ; que la fin de non-recevoir opposée par la commune de Lambres-lez-Douai doit, dès lors, être écartée ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article L. 111-7 du code de l'urbanisme, alors en vigueur et repris en substance à l'article L. 424-1 du même code : " Il peut être sursis à statuer sur toute demande d'autorisation concernant des travaux, constructions ou installations dans les cas prévus par les articles L. 111-9 et L. 111-10 du présent titre, ainsi que par les articles L. 123-6 (dernier alinéa), L. 311-2 et L. 313-2 (alinéa 2) du présent code et par l'article L. 331-6 du code de l'environnement. " ; qu'aux termes du dernier alinéa de l'article L. 123-6 du même code, dans sa rédaction applicable au litige : " A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan. " ;<br/>
<br/>
              8. Considérant qu'il résulte de la combinaison des articles L. 111-7, L. 123-6 et L. 410-1 du code de l'urbanisme que tout certificat d'urbanisme délivré sur le fondement de l'article L. 410-1 a pour effet de garantir à son titulaire un droit à voir toute demande d'autorisation ou de déclaration préalable déposée dans le délai indiqué examinée au regard des règles d'urbanisme applicables à la date de la délivrance du certificat ; que figure cependant parmi ces règles la possibilité de se voir opposer un sursis à statuer à une déclaration préalable ou à une demande de permis, lorsqu'est remplie, à la date de délivrance du certificat, l'une des conditions énumérées à l'article L. 111-7 du code de l'urbanisme ; qu'une telle possibilité vise à permettre à l'autorité administrative de ne pas délivrer des autorisations pour des travaux, constructions ou installations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d'urbanisme ; que, lorsque le plan en cours d'élaboration et qui aurait justifié, à la date de délivrance du certificat d'urbanisme, que soit opposé un sursis à une demande de permis ou à une déclaration préalable, entre en vigueur dans le délai du certificat, les dispositions issues du nouveau plan sont applicables à la demande de permis de construire ou à la déclaration préalable ;<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier qu'eu égard au degré d'avancement, à la date de délivrance du certificat d'urbanisme délivré à M. et MmeB..., du projet de modification du plan local d'urbanisme qui faisait état de la création, sur le terrain d'assiette du projet, d'un emplacement réservé, le maire pouvait légalement leur opposer, dès cette date, un sursis à statuer pour une demande de permis de construire portant sur la parcelle en cause ; que, par suite, M. et Mme B...ne sont pas fondés à soutenir, alors même que leur demande de permis de construire a été présentée dans le délai prévu à l'article L. 410-1 du code de l'urbanisme, que seules seraient applicables à cette demande les dispositions du plan local d'urbanisme en vigueur à la date de délivrance du certificat d'urbanisme qu'ils ont sollicité et ne prévoyant pas d'emplacement réservé sur leur parcelle ;<br/>
<br/>
              10. Considérant, en troisième lieu, qu'aux termes de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction alors applicable : " (...) Les plans locaux d'urbanisme comportent un règlement qui fixe, en cohérence avec le projet d'aménagement et de développement durable, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 121-1, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. / A ce titre, ils peuvent : / (...) 8° Fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts. (...) " ;<br/>
<br/>
              11. Considérant que la création d'un emplacement réservé n° 7 sur la parcelle des requérants est destinée à la réalisation d'une aire de stationnement assurant la desserte de la " plaine des jeux " qui accueille un centre de loisirs et diverses manifestations ; qu'elle se justifie par la nécessité d'améliorer l'accès des véhicules de particuliers et la sécurité des passagers des véhicules de transport en commun ; que dès lors, la création de cet emplacement réservé n'est pas entachée d'erreur manifeste d'appréciation ; que le détournement de pouvoir allégué n'est pas davantage établi ; que, par suite, le moyen tiré par voie d'exception de l'illégalité des dispositions du règlement du plan local d'urbanisme prévoyant l'emplacement réservé n° 7 doit être écarté ;<br/>
<br/>
              12. Considérant, en dernier lieu, que le projet de M. et Mme B...de construction d'une maison d'habitation n'est pas conforme à la destination de l'emplacement réservé n° 7 ; que, dans ces conditions, le maire de Lambres-lez-Douai ne pouvait que rejeter la demande de permis de construire présentée par les requérants ; que, par suite, le moyen tiré de l'inexacte application des dispositions de l'article UB11 du règlement du plan local d'urbanisme est inopérant ;<br/>
<br/>
              13. Considérant qu'il résulte de tout de ce qui précède que M. et Mme B...ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Lille a rejeté leur demande ;<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Lambres-lez-Douai, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme B...le versement à la commune de Lambres-lez-Douai d'une somme de 2 500 euros au titre des frais exposés par elle tant en appel qu'en cassation, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 25 mars 2014 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. et Mme B...devant la cour administrative d'appel de Douai est rejetée.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. et Mme B...est rejeté.<br/>
<br/>
Article 4 : M. et Mme B...verseront à la commune de Lambres-lez-Douai la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et Mme A...B...et à la commune de Lambres-lez-Douai.<br/>
		Copie en sera adressée au ministre de la cohésion des territoires.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-025-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CERTIFICAT D'URBANISME. EFFETS. - 1) DROIT CONFÉRÉ AU TITULAIRE - EXAMEN DE SA DEMANDE DE PERMIS AU REGARD DES RÈGLES D'URBANISME APPLICABLES À LA DATE DU CERTIFICAT - EXISTENCE, Y COMPRIS DANS L'HYPOTHÈSE D'UN CERTIFICAT NÉGATIF [RJ1] - 2) NOTION DE RÈGLES APPLICABLES - FACULTÉ D'OPPOSER UN SURSIS À STATUER - INCLUSION - CONSÉQUENCE [RJ2].
</SCT>
<ANA ID="9A"> 68-025-04 1) L'article L. 410-1 du code de l'urbanisme a pour effet de garantir à la personne à laquelle a été délivré un certificat d'urbanisme, quel que soit son contenu, un droit à voir sa demande de permis de construire déposée durant les dix-huit mois qui suivent, examinée au regard des dispositions d'urbanisme applicables à la date de ce certificat, à la seule exception de celles qui ont pour objet la préservation de la sécurité ou de la salubrité publique.... ,,2) Il résulte de la combinaison des articles L. 111-7, L. 123-6 et L. 410-1 du code de l'urbanisme que tout certificat d'urbanisme délivré sur le fondement de l'article L. 410-1 a pour effet de garantir à son titulaire un droit à voir toute demande d'autorisation ou de déclaration préalable déposée dans le délai indiqué examinée au regard des règles d'urbanisme applicables à la date de la délivrance du certificat. Figure cependant parmi ces règles la possibilité de se voir opposer un sursis à statuer à une déclaration préalable ou à une demande de permis, lorsqu'est remplie, à la date de délivrance du certificat, l'une des conditions énumérées à l'article L. 111-7 du code de l'urbanisme. Une telle possibilité vise à permettre à l'autorité administrative de ne pas délivrer des autorisations pour des travaux, constructions ou installations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d'urbanisme (PLU). Lorsque le plan en cours d'élaboration et qui aurait justifié, à la date de délivrance du certificat d'urbanisme, que soit opposé un sursis à une demande de permis ou à une déclaration préalable, entre en vigueur dans le délai du certificat, les dispositions issues du nouveau plan sont applicables à la demande de permis de construire ou à la déclaration préalable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour l'application de l'art. R. 600-1 du code de l'urbanisme lorsqu'est en cause un certificat d'urbanisme négatif, CE, 1er avril 2010, Mme,et M.,, n° 334113, T. p. 1022. Cf., dans l'hypothèse où le certificat d'urbanisme a été délivré sur le fondement du a) de l'article L. 410-1 du code de l'urbanisme, CE, 11 octobre 2017, M. et Mme,, n° 401878, à mentionner aux Tables., ,[RJ2] Rappr., dans l'hypothèse où un certificat d'urbanisme mentionne, alors que l'élaboration d'un nouveau document d'urbanisme a été prescrite, qu'un sursis à statuer pourra être opposé à une demande de permis et où la demande de permis est déposée postérieurement à l'entrée en vigueur du nouveau document, CE, 10 juillet 1987, Ministre de l'urbanisme c/ Foucault, n° 63010, p. 266. Cf., sur la mention relative au sursis à statuer dans le certificat d'urbanisme, CE, 3 avril 2014, Commune de Langolen, n° 362735, T. p. 904 ; CE, 11 octobre 2017, M. et Mme,, n° 401878, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
