<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648186</ID>
<ANCIEN_ID>JG_L_2021_06_000000448285</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648186.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 11/06/2021, 448285, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448285</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Luc Prévoteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448285.20210611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Sous le n° 2002459, M. E... D... a demandé au tribunal administratif de Lille de prononcer l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 en vue de l'élection des conseillers municipaux et des conseillers communautaires de la commune de Sin-le-Noble (Nord).<br/>
<br/>
              Sous le n° 2007227, la Commission nationale des comptes de campagne et des financements politiques, après avoir rejeté le compte de campagne de M. D... par une décision du 21 septembre 2020, a saisi le tribunal administratif de Lille en application de l'article L. 52-15 du code électoral.  <br/>
<br/>
              Par un jugement n°s 2002459, 2007227 du 17 décembre 2020, le tribunal administratif de Lille a, d'une part, déclaré M. D... inéligible pour une durée d'un an, annulé son élection en qualité de conseiller municipal et proclamé Mme F... A... élue, et d'autre part, rejeté la protestation électorale présentée par M. D.... <br/>
<br/>
              Par une requête enregistrée le 30 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat d'annuler ce jugement. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Prévoteau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Sin-le-Noble (Nord), la liste conduite par M. G... C... a recueilli 2 495 voix, soit 75,40 % des suffrages exprimés, obtenant ainsi trente-et-un sièges au conseil municipal et sept sièges au conseil communautaire. M. E... D..., qui conduisait la liste " Sin-le-Noble, les citoyens avant tout " ayant recueilli 9,97 % des voix, a lui-même été élu en qualité de conseiller municipal. Par une protestation formée le 19 mars 2020, M. D... a demandé au tribunal administratif de Lille d'annuler ces opérations électorales. Par ailleurs, après avoir rejeté le compte de campagne de M. D... par une décision du 21 septembre 2020, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal sur le fondement de l'article L. 52-15 du code électoral. Par un jugement du 17 décembre 2020, dont M. D... relève appel, le tribunal administratif de Lille a, d'une part, déclaré M. D... inéligible pour une durée d'un an, annulé son élection et proclamé élue Mme F... A... en qualité de conseillère municipale, et d'autre part, rejeté la protestation électorale formée par M. D....<br/>
<br/>
              2. En premier lieu, l'article L. 63 du code électoral dispose que : " L'urne électorale (...) doit, avant le commencement du scrutin, avoir été fermée à deux serrures dissemblables, dont les clefs restent, l'une entre les mains du président, l'autre entre les mains d'un assesseur tiré au sort parmi l'ensemble des assesseurs. (...) ". <br/>
<br/>
              3. Si M. D... soutient qu'en méconnaissance des dispositions précitées, le président et le vice-président du bureau de vote n° 10, qui appartenaient tous deux à la liste conduite par M. C..., maire sortant, ont chacun conservé l'une des clés de l'urne électorale, il n'est ni démontré ni même allégué que cette urne aurait été ouverte ou serait restée sans surveillance pendant la durée du scrutin. En outre, il ressort des termes du procès-verbal des opérations électorales qui se sont déroulées dans ce bureau de vote, que le scrutin a été clos à 18 h 00 et que le président a immédiatement arrêté la liste d'émargement puis ouvert l'urne. Dans ces conditions, il ne résulte pas de l'instruction que l'irrégularité dont se prévaut M. D... aurait eu pour effet de porter atteinte au secret du vote ou à la sincérité du scrutin. Il s'ensuit que cette irrégularité, à la supposer établie, n'est pas de nature à justifier l'annulation des opérations électorales qui se sont déroulées dans la commune de Sin-le-Noble. <br/>
<br/>
              4. En second lieu, d'une part, aux termes de l'article L. 52-12 du code électoral, dans sa rédaction applicable aux opérations en litige : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection (...). La même obligation incombe au candidat ou au candidat tête de liste dès lors qu'il a bénéficié de dons de personnes physiques conformément à l'article L. 52-8 du présent code selon les modalités prévues à l'article 200 du code général des impôts. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes (...). Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts (...) ". En vertu du 4° du XII de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, la date limite de dépôt du compte de campagne a été reportée au 10 juillet 2020 à 18 heures pour les listes de candidats participant seulement au 1er tour.  <br/>
<br/>
              5. D'autre part, aux termes de l'article L. 118-3 du code électoral, dans sa rédaction applicable, issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : / 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / 2° Le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales ; / 3° Le candidat dont le compte de campagne a été rejeté à bon droit. / L'inéligibilité mentionnée au présent article est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. (...) ". <br/>
<br/>
              6. Il résulte des dispositions précitées que la déclaration d'inéligibilité d'un candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12, ou dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales, ou encore dont le compte a été rejeté à bon droit, est une simple faculté dont dispose le juge de l'élection, lequel ne déclare inéligible un candidat qu'en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. Pour apprécier si le manquement justifie que le candidat soit déclaré inéligible, le juge doit prendre en compte l'ensemble des circonstances de l'espèce et, en particulier, la nature de la règle méconnue, le caractère délibéré ou non du manquement, l'existence éventuelle d'autres motifs d'irrégularité du compte ainsi que le montant des sommes en cause. <br/>
<br/>
              7. Il résulte de l'instruction et il n'est pas contesté que le compte de campagne de la liste conduite par M. D... en vue de l'élection des conseillers municipaux et communautaires de la commune de Sin-le-Noble a été rejeté par la CNCCFP en raison, d'une part, du non-respect du délai imparti pour déposer ce compte, et, d'autre part, pour défaut de présentation par un membre de l'ordre des experts-comptables et des comptables agréés. Si M. D... explique ces manquements par les graves soucis de santé dont lui-même et le mandataire financier de la liste qu'il conduisait ont été atteints, ainsi que par le manque de diligence de l'expert-comptable responsable de la présentation du compte, ni l'arrêt de travail le concernant, valable à compter du 29 juin 2020, ni les courriers médicaux datés du 2 juillet suivant, évoquant l'hospitalisation du mandataire financier au cours du mois de mai 2020, ni la lettre de ce mandataire en date du 15 septembre 2020, qui se borne à faire état de l'absence de réponses de l'expert-comptable sans même justifier de démarches à son égard, n'étaient de nature à faire obstacle à ce que M. D... s'acquitte de l'obligation de procéder au dépôt du compte dans le délai imparti et à ce que ce compte soit présenté par l'un des professionnels mentionnés à l'article L. 52-12 du code électoral. En outre, il résulte de l'instruction que M. D... a dû être mis en demeure par la CNCCFP le 11 août 2020 de déposer le compte de campagne qu'il était tenu d'établir - ce qu'il a fait le 26 août suivant - et que le formulaire utilisé pour ce faire par le requérant ne comportait aucun renseignement dans la rubrique dédiée à l'identification de l'expert-comptable chargé de mettre ce compte en état d'examen, ce qui conduit à douter qu'un expert-comptable ait été même sollicité. Dans ces conditions, en dépit du caractère limité des recettes et des dépenses dont le compte établi par M. D... fait état, ces manquements d'une particulière gravité aux prescriptions de l'article L. 52-12 du code électoral justifient qu'il ait été déclaré inéligible pendant une durée d'un an. <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. D... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lille, d'une part, a rejeté sa protestation, et, d'autre part, l'a déclaré inéligible pour une durée d'un an, annulé son élection et proclamé élue Mme A... en qualité de conseillère municipale. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. E... D..., à Mme F... A..., à M. G... C..., à M. H... B..., au ministre de l'intérieur et à la Commission nationale des comptes de campagne et des financements politiques.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
