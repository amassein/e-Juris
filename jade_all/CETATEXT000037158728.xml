<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158728</ID>
<ANCIEN_ID>JG_L_2018_07_000000415574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/87/CETATEXT000037158728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 04/07/2018, 415574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415574.20180704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 9 novembre 2017 et le 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. C...B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du conseil d'administration, statuant en formation restreinte, de l'université Lille 3, du 2 juin 2017, refusant de transmettre la liste des candidats retenus par le comité de sélection au titre du concours de recrutement d'un professeur des universités n° 4277 en psychologie sociale ainsi que le rejet opposé, par décision du 20 septembre 2017 de la présidente de l'université, à son recours contre cette délibération ;<br/>
<br/>
              2°) d'enjoindre au conseil d'administration de l'université Lille 3 de statuer à nouveau sur le recrutement au poste de professeur des universités n° 4277 et de transmettre la liste retenue ;<br/>
<br/>
              3°) de mettre à la charge de l'université Lille 3 la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire ; <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 712-3 du code de l'éducation : " (...) Sous réserve des dispositions statutaires relatives à la première affectation des personnels recrutés par concours national d'agrégation de l'enseignement supérieur, aucune affectation d'un candidat à un emploi d'enseignant-chercheur ne peut être prononcée si le conseil d'administration, en formation restreinte aux enseignants-chercheurs et personnels assimilés, émet un avis défavorable motivé (...) " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que  M. B...A..., maître de conférences à l'université de Bretagne occidentale et affecté à l'école supérieure du professorat et de l'éducation, s'est porté candidat à un poste de professeur des universités, profil " psychologie sociale ", relevant de la seizième section du Conseil national des universités que l'université Lille 3 avait ouvert au recrutement en 2017 ;  que, par une délibération du comité de sélection de l'établissement du 22 mai 2017, il a été classé en première position parmi les trois candidatures retenues ; que, par une délibération du 30 mai 2017, le conseil académique de l'université a émis un avis favorable à ce recrutement ; que, par une délibération du 2 juin 2017, le conseil d'administration de l'université, siégeant en formation restreinte, a émis un avis défavorable à la transmission de la liste des candidats à la ministre de l'enseignement supérieur, de la recherche et de l'innovation ; que M. B... A... demande l'annulation pour excès de pouvoir de cette dernière délibération, de même que du refus opposé au recours administratif qu'il avait formé contre cette délibération auprès de la présidente de l'université ; <br/>
<br/>
              4. Considérant que, pour décider de ne pas transmettre la liste des candidats retenus par le comité de sélection, le conseil d'administration s'est borné à avancer que " le candidat classé en première position ne correspond pas au profil du poste publié " ; qu'eu égard aux termes de cette motivation, celle-ci ne permet pas de connaître en quoi le profil de M. B... A... était en inadéquation avec le profil du poste ouvert au concours ; que, par suite, M. B... A...est fondé à soutenir que la décision attaquée est insuffisamment motivée et, sans qu'il soit besoin d'examiner les autres moyens de la requête, à en demander l'annulation pour excès de pouvoir ; que, par voie de conséquence, il y a lieu d'annuler également la décision de la présidente de l'université Lille 3 du 20 septembre 2017 rejetant son recours administratif contre cette délibération ; <br/>
<br/>
              5. Considérant que l'annulation de cette délibération et de cette décision implique que le conseil d'administration de l'université de Lille examine à nouveau les candidatures transmises par le comité de sélection pour le poste de professeur des universités n° 4277 et ayant recueilli un avis favorable du conseil académique ; qu'en revanche, elle n'implique pas nécessairement, comme le demande M. B...A..., que le conseil d'administration transmette à la ministre compétente la liste proposée par le comité de sélection ; qu'il y a donc seulement lieu, pour le Conseil d'Etat, d'ordonner au conseil d'administration de l'université de Lille de procéder à cet examen dans un délai de trois mois ;<br/>
<br/>
              6. Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université de Lille la somme de 3 000 euros à verser à M. B... A..., au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La délibération du conseil d'administration de l'université Lille 3 du 2 juin 2017 en ce qu'elle émet un avis défavorable au recrutement sur le poste n° 4277 et la décision de la présidente de l'université de Lille 3 du 20 septembre 2017 sont annulées.<br/>
Article 2 : Il est enjoint à l'université de Lille de réexaminer les candidatures retenues par le comité de sélection pour le poste de professeur des universités n° 4277 et ayant reçu un avis favorable du conseil académique dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : L'université de Lille versera à M. B...A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C...B...A...et à l'université de Lille.<br/>
Copie en sera adressée pour information à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
