<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036757741</ID>
<ANCIEN_ID>JG_L_2018_03_000000408994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/77/CETATEXT000036757741.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 30/03/2018, 408994</TITRE>
<DATE_DEC>2018-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408994.20180330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 3 août 2016 par laquelle la commission de médiation des Hauts-de-Seine a rejeté sa demande tendant à ce que soit reconnu le caractère prioritaire et urgent de sa demande de logement. Par une ordonnance n° 1608848 du 21 novembre 2016, le premier vice-président du tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 mars et 16 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à  sa demande et d'enjoindre à la commission de médiation, sur le fondement de l'article L. 911-1 du code de justice administrative, de reconnaître le caractère prioritaire et urgent de sa demande de logement.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              - l'arrêté du 22 janvier 2013 fixant la liste des titres de séjour prévue aux articles R. 300-1 et R. 300-2 du code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. B...A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 3 août 2016, la commission de médiation du département des Hauts-de-Seine, saisie sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation, a refusé de reconnaître comme prioritaire et urgente la demande de logement présentée par M. B...A... ; que l'intéressé a demandé au tribunal administratif de Cergy-Pontoise d'annuler cette décision ; qu'il  se pourvoit en cassation contre l'ordonnance du 21 novembre 2016 par laquelle le premier vice-président du tribunal a rejeté sa demande ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 300-1 du code de la construction et de l'habitation  : " Le droit à un logement décent et indépendant, mentionné à l'article 1er de la loi n° 90-449 du 31 mai 1990 visant à la mise en oeuvre du droit au logement, est garanti par l'Etat à toute personne qui, résidant sur le territoire français de façon régulière et dans des conditions de permanence définies par décret en Conseil d'Etat, n'est pas en mesure d'y accéder par ses propres moyens ou de s'y maintenir " ; qu'aux termes de l'article R. 300-2 de ce code : " Remplissent les conditions de permanence de la résidence en France mentionnées au premier alinéa de l'article L. 300-1 les étrangers autres que ceux visés à l'article R. 300-1 titulaires: / 1° Soit d'un titre de séjour d'une durée égale ou supérieure à un an, sous réserve que celui-ci ne soit pas périmé ; / 2° Soit d'un titre de séjour d'une durée inférieure à un an autorisant son titulaire à exercer une activité professionnelle ; / 3° Soit d'un visa d'une durée supérieure à trois mois conférant à son titulaire les droits attachés à un titre de séjour. / Un arrêté conjoint du ministre de l'intérieur et du ministre en charge du logement fixe la liste des titres de séjour concernés " ; que l'arrêté du 22 janvier 2013 visé ci-dessus, alors en vigueur, mentionne parmi les titres de séjour permettant de justifier d'une résidence permanente en France, au sens de ces dispositions, les documents suivants : " (...) 8. Récépissé délivré au titre de l'asile d'une durée de trois mois renouvelable portant la mention " reconnu réfugié, autorise son titulaire à travailler " ou " reconnu apatride, autorise son titulaire à travailler " ou " décision favorable de l'OFPRA/de la CNDA en date du... Le titulaire est autorisé à travailler " ou " a demandé la délivrance d'un premier titre de séjour ". / (...) / 12. Visa d'une durée supérieure à trois mois conférant à son titulaire les droits attachés à un titre de séjour pour une durée d'un an et portant l'une des mentions suivantes : - " vie privée et familiale " délivré pour les conjoints de ressortissants français ou pour les conjoints d'étrangers introduits au titre du regroupement familial ; (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, que selon le a) du 8° de l'article L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, la carte de résident est délivrée de plein droit, sous réserve de la régularité du séjour, au conjoint de l'étranger reconnu réfugié, si ce conjoint a été autorisé à séjourner en France au titre de la réunification familiale dans les conditions prévues à l'article L. 752-1 du même code ; que les dispositions de ce dernier article reconnaissent au ressortissant étranger qui s'est vu reconnaître la qualité de réfugié le droit d'être rejoint par son conjoint, si le mariage est antérieur à la date d'introduction de sa demande d'asile, sans conditions de durée préalable de séjour régulier, de ressources ou de logement, contrairement à ce qu'exigent, pour les autres étrangers, les dispositions relatives au regroupement familial, et prévoient que le conjoint doit solliciter, à cette fin, auprès des autorités diplomatiques et consulaires, un visa d'entrée en France pour un séjour d'une durée supérieure à trois mois  ; qu'en vertu de l'article R. 311-4 du même code, le conjoint de réfugié entré en France muni de ce visa de long séjour et qui a déposé une demande de carte de résident en application de l'article L. 314-11 déjà mentionné est mis en possession d'un récépissé de demande de titre de séjour qui l'autorise à travailler ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que la loi a entendu permettre l'installation en France des conjoints de réfugiés selon des modalités plus souples que celles de la procédure de regroupement familial ; qu'il en résulte que tant le visa de long séjour délivré au conjoint de réfugié en application de l'article L. 752-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le récépissé de demande de carte de résident qui lui est délivré en application de l'article R. 311-4 du même code répondent aux conditions posées par l'article R. 300-2 du code de la construction et de l'habitation  ; que ces documents, alors même que l'arrêté du 22 janvier 2013 omet à tort de les mentionner, doivent être regardés comme permettant à l'intéressé de justifier de sa résidence permanente en France, au sens de l'article L. 300-1 du code de la construction et de l'habitation ;   <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que  le récépissé de la demande de carte de résident de l'épouse de M. B...A..., présentée sur le fondement du a) du 8° de l'article L.314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, ne pouvait être regardé comme justifiant de la résidence permanente en France de l'intéressée, au sens des dispositions des articles L. 300-1 et R. 300-2 du code de la construction et de l'habitation, le tribunal administratif a commis une erreur de droit ; que son ordonnance doit, par suite, être annulée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que M. B... A...a demandé à la commission de médiation du département des Hauts-de-Seine de reconnaître le caractère prioritaire et urgent de sa demande de logement, en application du II de l'article L. 441-2-3 du code de la construction et de l'habitation ; que, pour lui opposer un refus par sa décision du 3 août 2016, la commission de médiation a estimé que son épouse ne remplissait pas la condition de résidence permanente en France mentionnée à l'article L. 300-1 du code de la construction et de l'habitation dès lors qu'elle n'était titulaire ni d'une carte de résident, ni d'un titre de séjour prévu par un traité ou accord international et conférant des droits équivalents à ceux attachés à la carte de résident, ni d'un titre portant la mention " vie privée et familiale " ; qu'il résulte toutefois de ce qui est dit au point 4 ci-dessus que la détention par l'épouse de M. B...A..., à la date de la décision attaquée, d'un visa de long séjour délivré en application de l'article L. 752-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dans le cadre de la procédure de réunification familiale suffisait pour faire regarder l'intéressée comme ayant une résidence permanente en France, au sens de l'article L. 300-1 du code de la construction et de l'habitation ; que la décision de refus attaquée doit, par suite, être annulée ; <br/>
<br/>
              Sur les conclusions à fins d'injonction : <br/>
<br/>
              8. Considérant que les motifs de la présente décision n'impliquent pas nécessairement que la commission de médiation reconnaisse un caractère prioritaire et urgent à la demande de logement de M. B...A... ; qu'il n'y a donc pas lieu d'enjoindre à la commission, en application des dispositions de l'article L. 911-1 du code de justice administrative, de procéder à cette reconnaissance ; qu'il y a lieu, en revanche, sur le fondement de l'article L. 911-2 du même code, de lui enjoindre de statuer à nouveau sur la demande de l'intéressé dans un délai de trois mois à compter de la notification de la présente décision au ministre de la cohésion des territoires ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 21 novembre 2016 du tribunal administratif de Cergy-Pontoise est annulée.<br/>
Article 2 : La décision du 3 août 2016 de la commission de médiation des Hauts-de-Seine est annulée.<br/>
Article 3 : Il est enjoint à la commission de médiation des Hauts-de-Seine de statuer à nouveau sur la demande de M. B...A...dans un délai de trois mois à compter de la notification de la présente décision au ministre de la cohésion des territoires.<br/>
Article 4 : La présente décision sera notifiée à M. B... A...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-05 - DALO - DOCUMENTS PERMETTANT DE JUSTIFIER D'UNE RÉSIDENCE PERMANENTE AU SENS DE L'ARTICLE L. 300-1 DU CCH - VISA DE LONG SÉJOUR DÉLIVRÉ AU CONJOINT D'UN RÉFUGIÉ (ART. L. 752-1 DU CESEDA) ET RÉCÉPISSÉ DE DEMANDE DE CARTE DE RÉSIDENT (ART. R. 311-4 DU CESEDA) - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - DOCUMENTS PERMETTANT DE JUSTIFIER D'UNE RÉSIDENCE PERMANENTE AU SENS DE L'ARTICLE L. 300-1 DU CCH - VISA DE LONG SÉJOUR DÉLIVRÉ AU CONJOINT D'UN RÉFUGIÉ (ART. L. 752-1 DU CESEDA) ET RÉCÉPISSÉ DE DEMANDE DE CARTE DE RÉSIDENT (ART. R. 311-4 DU CESEDA) - INCLUSION.
</SCT>
<ANA ID="9A"> 095-05 Il résulte du a) du 8° de l'article L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), et des articles L. 752-1 et R. 311-4 du même code que la loi a entendu permettre l'installation en France des conjoints de réfugiés selon des modalités plus souples que celles de la procédure de regroupement familial. Il en résulte que tant le visa de long séjour délivré au conjoint d'un réfugié en application de l'article L. 752-1 du CESEDA que le récépissé de demande de carte de résident qui lui est délivré en application de l'article R. 311-4 du même code répondent aux conditions posées par l'article R. 300-2 du code de la construction et de l'habitation (CCH). Ces documents, alors même que l'arrêté du 22 janvier 2013 fixant la liste des titres de séjour prévue aux articles R. 300-1 et R. 300-2 du code de la construction et de l'habitation omet à tort de les mentionner, doivent être regardés comme permettant à l'intéressé de justifier de sa résidence permanente en France, au sens de l'article L. 300-1 du CCH.</ANA>
<ANA ID="9B"> 38-07-01 Il résulte du a) du 8° de l'article L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), et des articles L. 752-1 et R. 311-4 du même code que la loi a entendu permettre l'installation en France des conjoints de réfugiés selon des modalités plus souples que celles de la procédure de regroupement familial. Il en résulte que tant le visa de long séjour délivré au conjoint d'un réfugié en application de l'article L. 752-1 du CESEDA que le récépissé de demande de carte de résident qui lui est délivré en application de l'article R. 311-4 du même code répondent aux conditions posées par l'article R. 300-2 du code de la construction et de l'habitation (CCH). Ces documents, alors même que l'arrêté du 22 janvier 2013 fixant la liste des titres de séjour prévue aux articles R. 300-1 et R. 300-2 du code de la construction et de l'habitation omet à tort de les mentionner, doivent être regardés comme permettant à l'intéressé de justifier de sa résidence permanente en France, au sens de l'article L. 300-1 du CCH.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
