<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487281</ID>
<ANCIEN_ID>JG_L_2021_12_000000459132</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 08/12/2021, 459132, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459132</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme A Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459132.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... I..., M. F... G..., Mme B... E... et Mme K... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'enjoindre à la commission de contrôle et du déroulement de la consultation, au Conseil supérieur de l'audiovisuel, à l'Autorité de régulation de la communication audiovisuelle et numérique et à la société France Télévision de suspendre la diffusion des émissions de la campagne audiovisuelle officielle appartenant aux groupements politiques Les Voix du Non 2 et Les Voix du Non 3 sur les services de radios, de télévision et internet de " Nouvelle-Calédonie La 1ère ", intitulées : " Nationalité et passeport français " diffusée les 29 novembre et 3 décembre 2021 (Voix du Non 3) ; " Allez voter ! " diffusée le 29 novembre (Voix du Non 2), 30 novembre (Voix du Non 3), 1er décembre (Voix du Non 3), 2 décembre (Voix du Non 2) et le 3 décembre 2021 (Voix du Non 2) ;  " L'enseignement " diffusée le 30 novembre 2021 (Voix du Non 2) ; " L'environnement " diffusée le 1er décembre 2021 (Voix du Non 2), " La sécurité " diffusée le 2 décembre 2021 (Voix du Non 3) ainsi que du clip de campagne " Le pouvoir d'achat " ;<br/>
<br/>
              2°) d'assortir cette injonction d'une astreinte. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de leur requête ; <br/>
              - leur requête est recevable ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, la diffusion des émissions de campagnes contestées a débuté le 29 novembre 2021, date de début de la campagne officielle et, d'autre part, le scrutin est prévu le 12 décembre 2021 ; <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ;<br/>
              - les émissions litigieuses méconnaissent le principe de sauvegarde de la dignité de la personne humaine et le principe de dignité du scrutin et sont constitutives d'un trouble à l'ordre public dès lors qu'elles mettent en scène une image dégradante et infantilisante des populations océaniennes, composante majoritaire de l'électorat indépendantiste de Nouvelle-Calédonie. <br/>
<br/>
              Par un mémoire en défense, enregistré le 6 décembre 2021, la société France Télévisions conclut au rejet de la requête. Elle soutient que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 6 décembre 2021, le Conseil supérieur de l'audiovisuel conclut au rejet de la requête. Il soutient que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée aux groupements politiques Les Voix du Non 2 et Les Voix du Non 3 qui n'ont pas produit de mémoire en défense. <br/>
              Par un nouveau mémoire enregistré le 6 décembre 2021, les groupements Les Voix du Non 2 et Les Voix du Non 3 concluent à ce qu'il soit constaté un non-lieu à statuer. Ils indiquent avoir renoncé à la diffusion des parties animées des messages contestés. <br/>
<br/>
              Par un nouveau mémoire, enregistré le 7 décembre 2021, le Conseil supérieur de l'audiovisuel conclut à ce qu'il soit constaté un non-lieu à statuer. Il confirme la cessation de la diffusion des parties animées des émissions audiovisuelles des groupements Les Voix du Non 2 et Les Voix du Non 3. <br/>
<br/>
              Par un nouveau mémoire, enregistré le 7 décembre 2021, M. I... et autres reprennent les conclusions de leur requête par les mêmes moyens. Ils font valoir que ces conclusions conservent un objet dès lors que les parties animées des émissions audiovisuelles en cause ont été diffusées le mardi 7 décembre 2021 à 19h50.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - La Constitution ; <br/>
              - la loi organique n° 99-209 du 19 mars 1999 ; <br/>
              - la loi organique n° 2018-280 du 19 avril 2018 ; <br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le décret n° 2021-866 du 30 juin 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les requérants, et d'autre part, la société France Télévisions, le Conseil supérieur de l'audiovisuel, et les groupements politiques Les Voix du Non 2 et Les Voix du Non 3 ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 6 décembre 2021, à 16 heures : <br/>
<br/>
              - Me Le Guerer, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - Me Piwnica, avocat au Conseil d'Etat et à la Cour de cassation, avocat de France Télévisions ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de La Voix du Non 2 et La Voix du Non 3 ; <br/>
<br/>
              - les représentants du Conseil supérieur de l'audiovisuel ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 7 décembre 2021 à 16 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Aux termes de l'article 4 de la Constitution : " Les partis et groupements politiques concourent à l'expression du suffrage. Ils se forment et exercent leur activité librement. Ils doivent respecter les principes de la souveraineté nationale et de la démocratie./(...)/La loi garantit les expressions pluralistes des opinions et la participation équitable des partis et groupements politiques à la vie démocratique de la Nation ".<br/>
<br/>
              3. Aux termes du IV de l'article 219 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie dans sa rédaction issue de la loi organique du 19 avril 2018 relative à l'organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie : " Les partis et groupements politiques de Nouvelle-Calédonie habilités à participer à la campagne officielle en vue de la consultation peuvent utiliser en Nouvelle-Calédonie les antennes de la société nationale chargée du service public de la communication audiovisuelle outre-mer./Trois heures d'émissions radiodiffusées et trois heures d'émissions télévisées sont mises à leur disposition./Ces temps d'antenne sont répartis, entre les partis ou groupements habilités à participer à la campagne, par accord entre les présidents des groupes au congrès, sans que cette répartition puisse conduire à octroyer à l'un de ces partis ou groupements un temps d'antenne hors de proportion avec sa représentation au congrès. A défaut d'accord constaté par la commission de contrôle, cette dernière fixe la répartition des temps d'antenne entre les partis ou groupements habilités en fonction du nombre de membres du congrès qui ont déclaré s'y rattacher, apprécié à la date à laquelle la décision de la commission de contrôle dressant la liste des partis ou groupements admis à participer à la campagne est publiée au Journal officiel de la Nouvelle-Calédonie./Le Conseil supérieur de l'audiovisuel fixe les règles concernant les conditions de production, de programmation et de diffusion des émissions relatives à la campagne officielle ouverte en vue de la consultation./Le Conseil supérieur de l'audiovisuel adresse à l'ensemble des services de radio et de télévision à vocation nationale et locale, quel que soit leur mode de diffusion par tout procédé de communication électronique en Nouvelle-Calédonie, des recommandations pour l'application des principes définis à l'article 1er de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication à compter du début de la campagne et jusqu'à la veille du scrutin à zéro heure. Durant cette période, les mêmes services de radio et de télévision veillent, sous le contrôle du Conseil supérieur de l'audiovisuel, à ce que les partis et groupements politiques bénéficient d'une présentation et d'un accès à l'antenne équitables en ce qui concerne la reproduction des déclarations et écrits émanant des représentants de chaque parti ou groupement politique./Le Conseil supérieur de l'audiovisuel délègue l'un de ses membres en Nouvelle-Calédonie à l'occasion de la campagne ".<br/>
<br/>
              Sur les pouvoirs du Conseil supérieur de l'audiovisuel : <br/>
<br/>
              4. Si, dans les émissions audiovisuelles diffusées dans le cadre de campagnes électorales, les organisations politiques s'expriment librement, il appartient au Conseil supérieur de l'audiovisuel, en vertu de la loi du 30 septembre 1986 relative à la liberté de communication et, en l'espèce, des dispositions de la loi organique citées au point 3, de s'opposer, sous le contrôle du juge, à la diffusion de propos qui seraient interdits et punis par la loi ou porteraient atteinte à la dignité de la personne humaine ainsi que de messages qui répondraient à des fins étrangères à celles en vue desquelles cet accès au service public de radio et de télévision a été prévu ou qui méconnaîtraient les conditions de production, de programmation et de diffusion qu'il aurait fixées en vertu de ces mêmes dispositions. <br/>
<br/>
              Sur la campagne audiovisuelle : <br/>
<br/>
              5. Par un décret du Président de la République du 30 juin 2021, la date de la troisième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie prévue par l'article 77 de la Constitution a été fixée au dimanche 12 décembre 2021. En vertu de l'article 4 de ce même décret, la campagne en vue de cette consultation s'est ouverte le lundi 29 novembre 2021, à zéro heure, pour s'achever le vendredi 10 décembre 2021, à minuit. Conformément à son article 6, la commission de contrôle de l'organisation et du déroulement de la consultation mentionnée au III de l'article 219 de la loi organique du 19 mars 1999 a dressé la liste des partis et groupements admis à participer à la campagne, qui a été publiée au Journal officiel de la Nouvelle-Calédonie le 23 septembre 2021. Au titre de ces partis et groupements, figurent les groupements Les Voix du Non 2 et Les Voix du Non 3. <br/>
<br/>
              6. Par décision n° 2021-1104 du 27 octobre 2021, le Conseil supérieur de l'audiovisuel a fixé les conditions de production, de programmation et de diffusion des émissions de la campagne audiovisuelle officielle de la consultation sur l'accession de la Nouvelle-Calédonie à la pleine souveraineté. En vertu de l'article 6 de cette décision, les partis et groupements politiques habilités à participer à la campagne audiovisuelle s'expriment librement au cours de leurs émissions mais ne peuvent " recourir à tout moyen d'expression portant atteinte à la dignité de la personne humaine et à la considération d'autrui " ou " ayant pour effet de tourner en dérision d'autres partis ou groupements politiques ". Ses articles 31 et 34 prévoient respectivement que les émissions télévisées de la campagne sont programmées du lundi 29 novembre au vendredi 3 décembre 2021 puis du lundi 6 décembre au vendredi 10 décembre 2021, sur les services de radio et de télévision " Nouvelle-Calédonie La 1ère  ", respectivement vers 9 heures et 20 heures, et qu'elles sont rendues accessibles, le jour même immédiatement après leur première diffusion sur le site internet de cette chaîne. <br/>
<br/>
              7. Par décision n° 2021-1146 du 10 novembre 2021, le Conseil supérieur de l'audiovisuel a fixé les dates et l'ordre de passage des émissions de la campagne audiovisuelle officielle. <br/>
<br/>
              Sur l'office du juge des référés : <br/>
<br/>
              8. Saisi sur le fondement de l'article L. 521-2 du code de justice administrative d'une requête tendant à la suspension de la diffusion d'émissions audiovisuelles dans le cadre d'une campagne électorale, le juge des référés auquel il n'appartient pas d'apprécier le contenu politique de ces émissions, peut procéder à cette suspension lorsque notamment les propos tenus et les images diffusées portent une atteinte grave et manifestement illégale à la dignité de la personne humaine. <br/>
<br/>
              Sur la requête en référé : <br/>
<br/>
              9. M. I... et autres demandent au juge des référés statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre la suspension de la diffusion des émissions audiovisuelles relatives à la campagne officielle dans le cadre de la troisième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, des groupements politiques Les Voix du Non 2 et les Voix du Non 3, sur les services de radios, de télévision et internet de " Nouvelle-Calédonie La 1ère " et intitulées : " Nationalité et Passeport français ", " Allez voter ! " , " L'enseignement ", " l'environnement " " La sécurité " ainsi que la suspension de la diffusion du clip de campagne intitulé " Le pouvoir d'achat ". <br/>
<br/>
              10. Il résulte des dernières productions qu'à la suite de l'audience de référé, afin de mettre un terme à toute polémique sur la forme et le fond des parties animées des émissions et clip mis en cause, les groupements Les Voix du Non 2 et Les Voix du Non 3 ont renoncé à la diffusion des messages contestés. Le Conseil supérieur de l'audiovisuel a pris acte de cette décision. Il s'ensuit qu'il n'y a plus lieu de statuer sur les conclusions de la requête présentée par M. I... et autres. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur la requête présentée par M. I... et autres.<br/>
Article 2 : La présente ordonnance sera notifiée à M. I..., premier dénommé, au Conseil supérieur de l'audiovisuel, à la société France Télévisions et aux groupements politiques Les Voix du Non 2 et Les Voix du Non 3. <br/>
Copie en sera adressée au Président du gouvernement de la Nouvelle-Calédonie, au Haut-commissaire de la République en Nouvelle-Calédonie et à la commission de contrôle de l'organisation et du déroulement de la consultation.<br/>
              Délibéré à l'issue de la séance du 6 décembre 2021 où siégeaient : M. C... H..., président-adjoint de la section du contentieux, présidant ; Mme A... J... et M. Damien Botteghi, conseillers d'Etat, juges des référés.<br/>
Fait à Paris, le 8 décembre 2021<br/>
Signé : Rémy H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
