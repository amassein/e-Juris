<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025912085</ID>
<ANCIEN_ID>JG_L_2012_05_000000349976</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/20/CETATEXT000025912085.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 07/05/2012, 349976, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349976</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Aymeric Pontvianne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349976.20120507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 7 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 09PA01999 du 31 mars 2011 par lequel la cour administrative d'appel de Paris a annulé le jugement n° 0519703 du 19 décembre 2008 du tribunal administratif de Paris rejetant la demande de M. C...- D...tendant à l'annulation de l'arrêté du 16 juin 1998 refusant de l'inscrire sur le registre du sceau de France comme ayant succédé au titre de comte D...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu les décrets impériaux du 1er mars 1808 confirmant la création des titres impériaux et concernant les majorats ;<br/>
<br/>
              Vu le décret du 10 janvier 1872 supprimant le conseil du sceau des titres et attribuant ses fonctions au conseil d'administration du ministère de la justice ;<br/>
<br/>
              Vu les lettres patentes du 15 juin 1812 conférant à M. G...C...-D... le titre de comte D...;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aymeric Pontvianne, Maître des requêtes en service extraordinaire,<br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de M.  C...-D...,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. C...-D... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par arrêté du 16 juin 1998, le GARDE DES SCEAUX a refusé d'inscrire sur les registres du Sceau de France M. F...C...- D...comme ayant succédé au titre de comteD..., au motif que son aïeul E...A...ne pouvait prétendre à ce titre en sa qualité d'enfant naturel reconnu mais non légitime d'G...C...comte D...;<br/>
<br/>
              Considérant qu'aux termes de l'article 35 du décret impérial du 1er mars 1808 concernant les majorats : " Le titre qu'il nous aura plu d'attacher à chaque majorat sera affecté exclusivement à celui en faveur duquel la création aura eu lieu et passera à sa descendance légitime, naturelle ou adoptive, de mâle en mâle, par ordre de primogéniture " ; que ces dispositions n'ont entendu permettre la transmission des titres nobiliaires d'Empire qu'aux seuls enfants légitimes, conçus ou adoptés par des parents unis par les liens du mariage ; que, par suite, la cour administrative d'appel de Paris a commis une erreur de droit en jugeant que ce texte ne réservait pas la transmission de tels titres aux enfants issus d'une filiation légitime ; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, le GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant, en premier lieu, que depuis la promulgation des lois constitutionnelles de 1875, nulle autorité de la République ne dispose du pouvoir de collationner, de confirmer ou de reconnaître des titres nobiliaires, qui se transmettent de plein droit et sans intervention de ces autorités ; que la seule compétence maintenue au garde des sceaux, en application du décret du 10 janvier 1872 qui a supprimé le conseil du sceau des titres et attribué les fonctions de ce conseil " en tout ce qui n'est pas contraire à la législation actuelle " au conseil d'administration établi auprès du garde des sceaux, est celle de se prononcer sur les demandes de vérification des titres de noblesse, qui le conduisent uniquement à examiner les preuves de la propriété du titre par celui qui en fait la demande ; qu'il s'ensuit que le refus d'inscription sur le registre du Sceau de France de la transmission d'un titre nobiliaire, qui ne saurait en particulier s'analyser comme l'abrogation d'une décision créatrice de droits ou le refus d'un avantage dont l'attribution constituerait un droit ou un refus d'autorisation, n'est pas au nombre des décisions administratives défavorables dont la loi du 11 juillet 1979 impose qu'elles soient obligatoirement motivées ;<br/>
<br/>
              Considérant, en deuxième lieu, que la circonstance que l'arrêté attaqué ait visé le code civil de 1812 est, en tout état de cause, sans incidence sur sa légalité ; <br/>
<br/>
              Considérant, en troisième lieu, qu'ainsi qu'il a été dit précédemment, l'article 35 du décret impérial du 1er mars 1808, rappelé par les lettres patentes délivrées le 15 juin 1812 par Napoléon Ier à la comtesse B...D...pour son fils mineur G...C...-D..., réserve la transmission des titres nobiliaires d'Empire aux seuls enfants légitimes ; que Alexandre AntoineA..., aïeul de M. F...C...-D..., n'était pas le fils légitime d'G...C...comte D...; que si le requérant soutient que son aïeul a été adopté par son père naturel avant la mort de ce dernier en 1868, il ne ressort pas des pièces versées au dossier que le comte D...aurait obtenu, à cet effet, l'autorisation préalable requise par l'article 36 du décret du 1er mars 1808 alors en vigueur ; qu'il résulte de ce qui précède que le ministre était tenu de refuser d'inscrire sur le registre du Sceau de France M. F...C...-D... dont l'aïeul, E...A..., ne peut être tenu pour le fils légitime d'G...C...comte D...;<br/>
<br/>
              Considérant, enfin, que les stipulations des articles 8 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées en matière de vérification des titres de noblesse ; <br/>
<br/>
              Considérant qu'il résulte tout de ce qui précède que M. F...C...-D... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de l'arrêté attaqué du 16 juin 1998 ; que ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 mars 2011 est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. F...C...-D... devant la cour administrative d'appel de Paris est rejetée.<br/>
<br/>
Article 3 : Le surplus des conclusions de M. F...C...-D... est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée au GARDE DES SCEAUX, MINISTRE DE LA JUSTICE ET DES LIBERTES et à M. F...C...-D....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. ABSENCE D'OBLIGATION DE MOTIVATION. - REFUS D'INSCRIPTION D'UN TITRE NOBILIAIRE SUR LE REGISTRE DU SCEAU DE FRANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-01-02 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. VÉRIFICATION DES TITRES DE NOBLESSE. - NOBLESSE D'EMPIRE - TRANSMISSION DES TITRES (ART. 35 DU DÉCRET IMPÉRIAL DU 1ER MARS 1808) - 1) TRANSMISSION AUX ENFANTS NATURELS - ABSENCE - 2) OBLIGATION DE MOTIVER UN REFUS D'INSCRIPTION SUR LE REGISTRE DU SCEAU DE FRANCE - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-03 Le refus d'inscription sur le registre du Sceau de France de la transmission d'un titre nobiliaire, qui ne saurait en particulier s'analyser comme l'abrogation d'une décision créatrice de droits ou le refus d'un avantage dont l'attribution constituerait un droit ou un refus d'autorisation, n'est pas au nombre des décisions administratives défavorables dont la loi n° 79-587 du 11 juillet 1979 impose qu'elles soient motivées.</ANA>
<ANA ID="9B"> 26-01-02 1) L'article 35 du décret impérial du 1er mars 1808 concernant les majorats n'a entendu permettre la transmission des titres nobiliaires d'Empire qu'aux seuls enfants légitimes, conçus ou adoptés par des parents unis par les liens du mariage, et non aux enfants naturels.,,2) Le refus d'inscription sur le registre du Sceau de France de la transmission d'un titre nobiliaire, qui ne saurait en particulier s'analyser comme l'abrogation d'une décision créatrice de droits ou le refus d'un avantage dont l'attribution constituerait un droit ou un refus d'autorisation, n'est pas au nombre des décisions administratives défavorables dont la loi n° 79-587 du 11 juillet 1979 impose qu'elles soient motivées.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
