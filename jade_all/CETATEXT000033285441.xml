<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285441</ID>
<ANCIEN_ID>JG_L_2016_10_000000386405</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285441.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 19/10/2016, 386405</TITRE>
<DATE_DEC>2016-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386405</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386405.20161019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'exploitation agricole à responsabilité limitée (EARL) Sapins du bocage a demandé au tribunal administratif de Caen d'annuler l'arrêté du 11 septembre 2012 du préfet de l'Orne refusant de l'autoriser à exploiter des terres agricoles d'une superficie de 3 hectares 69 ares situées sur le territoire de la commune de Moncy. Par un jugement n° 1201949 du 15 février 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NT01042 du 2 octobre 2014, la cour administrative d'appel de Nantes, faisant droit à la requête de l'EARL Sapins du bocage, a annulé ce jugement et l'arrêté du 11 septembre 2012.<br/>
<br/>
              Par un pourvoi enregistré le 12 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture, de l'agroalimentaire et de la forêt demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'EARL Sapins du bocage.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code rural et de la pêche maritime ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'EARL Sapins du bocage.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de l'Orne, saisi des demandes concurrentes de l'EARL Sapins du bocage et de l'EARL Lenormand, qui sollicitaient l'autorisation d'adjoindre à leurs exploitations respectives une surface de 3 ha 69 ares située sur le territoire de la commune de Moncy, a, le 10 mai 2011, rejeté la première et accueilli la seconde ; que, par un arrêté du 12 septembre 2012, il a rejeté une nouvelle demande de l'EARL Sapins du bocage portant sur les mêmes terres au motif que l'EARL Lenormand bénéficiait d'une priorité par application des dispositions du schéma directeur départemental des structures agricoles du département ; que le recours pour excès de pouvoir formé par l'EARL Sapins du bocage contre cet arrêté a été rejeté par un jugement du 15 février 2013 du tribunal administratif de Caen ; que le ministre de l'agriculture, de l'agroalimentaire et de la forêt se pourvoit en cassation contre l'arrêt du 2 octobre 2014 par lequel la cour administrative d'appel de Nantes a annulé ce jugement ainsi que l'arrêté préfectoral du 11 septembre 2012 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 331-3 du code rural et de la pêche maritime, dans sa rédaction en vigueur à la date de l'arrêté litigieux : " L'autorité administrative se prononce sur la demande d'autorisation en se conformant aux orientations définies par le schéma directeur départemental des structures agricoles applicable dans le département dans lequel se situe le fonds faisant l'objet de la demande. Elle doit notamment : 1° Observer l'ordre des priorités établi par le schéma départemental entre l'installation des jeunes agriculteurs et l'agrandissement des exploitations agricoles, en tenant compte de l'intérêt économique et social du maintien de l'autonomie de l'exploitation faisant l'objet de la demande. (...) " ; qu'il résulte de ces dispositions que le préfet, saisi de demandes concurrentes d'autorisation d'exploiter portant sur les mêmes terres, doit, pour statuer sur ces demandes, observer l'ordre des priorités établi par le schéma directeur départemental des structures agricoles; qu'il peut être conduit à délivrer plusieurs autorisations lorsque plusieurs candidats à la reprise relèvent du même rang de priorité et qu'aucun autre candidat ne relève d'un rang supérieur ; que la circonstance qu'une autorisation ait déjà été délivrée pour l'exploitation de certaines terres ne fait pas obstacle à la délivrance d'une autorisation portant sur les mêmes terres à un agriculteur relevant d'un rang de priorité au moins égal à celui dont relève le titulaire de la première autorisation ; que, lorsque plusieurs personnes sont autorisées à exploiter les mêmes terres, la législation sur le contrôle des structures des exploitations agricoles est sans influence sur la liberté du propriétaire des terres de choisir la personne avec laquelle il conclura un bail ;<br/>
<br/>
              3. Considérant que l'article 2 de l'arrêté du 21 décembre 2011 du préfet de l'Orne établissant le schéma directeur départemental des structures agricoles de ce département confère une priorité aux opérations visant à l'installation ou à la réinstallation d'un agriculteur sur celles qui ont pour objet de conforter la structure des installations existantes ; qu'au sein de cette seconde catégorie, il fixe un ordre de priorité entre cinq types d'opérations ; qu'il précise que : " En cas de concurrence au même rang de priorité, peuvent être pris en compte pour départager les candidats tout ou partie des critères suivants : - l'entrée d'un nouvel associé au sein de la société, jeune agriculteur remplissant les conditions d'octroi des aides à l'installation, - l'entrée d'un nouvel associé au sein de la société, jeune agriculteur bénéficiaire des aides à l'installation qui se réinstalle dans les cinq ans suivant sa date d'installation, - l'âge, - la situation familiale, - la surface déjà exploitée, - la dimension économique de l'exploitation par référence au projet agricole départemental (...), - la distance par rapport au siège de l'exploitation, - le nombre d'emplois permanents ou saisonniers, - l'entrée d'un nouvel associé qui apporte son exploitation, - la structure parcellaire des exploitations concernées, notamment dans le cas de parcelles jouxtant les bâtiments d'élevage accessibles aux animaux logés dans ces bâtiments, - l'intérêt environnemental de l'opération, - la poursuite d'une activité agricole bénéficiant de la certification du mode de production biologique " ; qu'en application de telles dispositions, il appartient au préfet, lorsque les projets de deux candidats relèvent du même type d'opérations parmi ceux qu'elles définissent pour fixer l'ordre des priorités, de déterminer au regard des critères qu'elles prévoient si l'un d'eux peut néanmoins être regardé comme prioritaire ; que si le préfet doit, sous le contrôle du juge de l'excès de pouvoir, tenir compte, pour procéder à ce départage, de l'ensemble des critères prévus à cet effet par le schéma directeur, il n'est pas tenu, dans la motivation de sa décision, de se prononcer sur chacun de ces critères mais peut se borner à mentionner ceux qu'il estime pertinents et les éléments de fait correspondants ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de l'Orne a estimé que l'EARL Sapins du bocage et l'EARL Lenormand relevaient du même rang de priorité au titre de l'agrandissement d'une exploitation existante et a mis en oeuvre, afin de les départager, les dispositions précitées du schéma directeur départemental des structures agricoles ; que, pour annuler son arrêté du 11 septembre 2012, la cour administrative d'appel a relevé que cet arrêté se bornait à se référer aux critères relatifs à la distance des terres en cause par rapport à l'exploitation et à la structure parcellaire de celle-ci, sans prendre parti sur d'autres critères, notamment ceux relatifs à la dimension économique de l'exploitation et à l'intérêt environnemental de l'opération ; qu'elle en a déduit que le préfet avait fait une inexacte application de l'article 2 du schéma directeur départemental des structures agricoles et commis une erreur d'appréciation ; qu'il résulte toutefois de ce qui a été dit au point 3 que la circonstance que l'arrêté litigieux ne mentionnait que deux des douze critères prévus par le schéma directeur départemental des structures agricoles n'était toutefois pas, par elle-même, de nature à l'entacher d'illégalité ; qu'en jugeant que cet arrêté était entaché d'une erreur d'appréciation au seul motif qu'il ne mentionnait pas les critères relatifs à la dimension économique de l'exploitation et à l'intérêt environnemental de l'opération, sans rechercher si la prise en compte de ces critères devait conduire à reconnaître à l'EARL Sapins du bocage le bénéfice d'une priorité par rapport à l'EARL Lenormand, la cour a commis une erreur de droit ; que son arrêt doit, par suite, être annulé ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 2 octobre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par l'EARL Sapins du bocage au titre de l'article L.761-1 du code de justice admnistrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'agriculture, de l'agroalimentaire et de la forêt, à l'EARL Sapins du bocage et à l'EARL Lenormand.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 Actes législatifs et administratifs. Validité des actes administratifs - Forme et procédure. Questions générales. Motivation. Motivation suffisante. Existence.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-03 Agriculture et forêts. Exploitations agricoles.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
