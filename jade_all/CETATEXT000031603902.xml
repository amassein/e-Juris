<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031603902</ID>
<ANCIEN_ID>JG_L_2015_12_000000375736</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/60/39/CETATEXT000031603902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 11/12/2015, 375736, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375736</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Célia Verot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375736.20151211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rouen d'annuler la décision par laquelle le maire de la commune de Breteuil-sur-Iton a prononcé sa radiation des cadres à compter du 24 novembre 2009. <br/>
<br/>
              Par un jugement n° 1000104 du 20 novembre 2012, le tribunal administratif de Rouen a annulé cette décision.  <br/>
<br/>
              Par un arrêt n° 13DA00081 du 10 décembre 2013, la cour administrative d'appel de Douai a rejeté l'appel formé par la commune de Breteuil-sur-Iton contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 février et 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Breteuil-sur-Iton demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - le décret n° 87-602 du 30 juillet 1987 ; <br/>
              - le code de justice administrative ; 		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Célia Verot, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de la commune de Breteuil-sur-Iton ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la commune de Breteuil-sur-Iton se pourvoit en cassation contre l'arrêt du 10 décembre 2013 par lequel la cour administrative d'appel de Douai a rejeté son appel contre le jugement du 20 novembre 2012 par lequel le tribunal administratif de Rouen a annulé la décision par laquelle son maire a notifié à M. A...sa radiation des cadres pour abandon de poste à compter du 24 novembre 2009 ; <br/>
<br/>
              2. Considérant qu'une mesure de radiation des cadres pour abandon de poste ne peut être régulièrement prononcée que si l'agent concerné a, préalablement à cette décision, été mis en demeure de rejoindre son poste ou de reprendre son service dans un délai approprié qu'il appartient à l'administration de fixer ; qu'une telle mise en demeure doit prendre la forme d'un document écrit, notifié à l'intéressé, l'informant du risque qu'il court d'une radiation des cadres sans procédure disciplinaire préalable ; que lorsque l'agent ne s'est pas présenté et n'a fait connaître à l'administration aucune intention avant l'expiration du délai fixé par la mise en demeure, et en l'absence de toute justification d'ordre matériel ou médical, présentée par l'agent, de nature à expliquer le retard qu'il aurait eu à manifester un lien avec le service, cette administration est en droit d'estimer que le lien avec le service a été rompu du fait de l'intéressé ;<br/>
<br/>
              3. Considérant que l'agent en position de congé de maladie n'a pas cessé d'exercer ses fonctions ; que, par suite, une lettre adressée à un agent à une date où il est dans une telle position ne saurait, en tout état de cause, constituer une mise en demeure à la suite de laquelle l'autorité administrative serait susceptible de prononcer, dans les conditions définies au point 2 ci-dessus, son licenciement pour abandon de poste ; que, toutefois, si l'autorité compétente constate qu'un agent en congé de maladie s'est soustrait, sans justification, à une contre-visite qu'elle a demandée en application des dispositions de l'article 15 du décret du 30 juillet 1987 pris pour l'application de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux, elle peut lui adresser une lettre de mise en demeure, respectant les exigences définies au point 2 ci-dessus et précisant en outre explicitement que, en raison de son refus de se soumettre, sans justification, à la contre-visite à laquelle il était convoqué, l'agent court le risque d'une radiation alors même qu'à la date de notification de la lettre il bénéficie d'un congé de maladie ; que si, dans le délai fixé par la mise en demeure, l'agent ne justifie pas son absence à la contre-visite à laquelle il était convoqué, n'informe l'administration d'aucune intention et ne se présente pas à elle, sans justifier, par des raisons d'ordre médical ou matériel, son refus de reprendre son poste, et si, par ailleurs, aucune circonstance particulière, liée notamment à la nature de la maladie pour laquelle il a obtenu un congé, ne peut expliquer son abstention, l'autorité compétente est en droit d'estimer que le lien avec le service a été rompu du fait de l'intéressé ;   <br/>
<br/>
              4. Considérant que la cour administrative d'appel de Douai a relevé que M. A..., adjoint technique employé au sein des services de la commune de Breteuil-sur-Iton, a été victime le 9 septembre 2009 d'un accident de service ayant occasionné une entorse du genou gauche et a été placé en congé de maladie du 13 septembre au 25 novembre 2009, qu'il s'est soustrait sans justification à deux contre-visites médicales auxquelles il a été convoqué à la demande de la commune de Breteuil-sur-Iton par lettres des 30 septembre et 27 octobre 2009 et qui devaient se dérouler les 6 octobre et 3 novembre 2009, que, le 16 novembre 2009, le maire de la commune l'a informé qu'il regardait cette absence comme irrégulière, que, par lettre recommandée du 19 novembre 2009, il l'a mis en demeure de reprendre ses fonctions le 24 novembre suivant en lui indiquant qu'il serait, à défaut, contraint de constater l'abandon de poste  et que, par lettre recommandée du 25 novembre 2009, il lui a notifié sa radiation des cadres ; que la cour a rejeté  l'appel de la commune contre le jugement du 20 novembre 2012 par lequel le tribunal administratif de Rouen a annulé cette décision de radiation, au motif, d'une part, que la circonstance que M. A...se soit soustrait sans justification à deux contre-visites demandées par la commune ne permettait pas de considérer qu'il avait rompu tout lien avec le service et, d'autre part, que la mise en demeure de reprendre son service lui avait été adressée à une date où il demeurait en position régulière de congé de maladie ; qu'en ne recherchant pas si, compte tenu du refus non justifié de l'intéressé de se soumettre à des contre-visites, la commune avait pu, en respectant les exigences définies au point 3  ci-dessus, prendre la décision litigieuse, elle a commis une erreur de droit ; que la commune de Breteuil-sur-Iton est donc fondée à demander, pour ce motif, l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 10 décembre 2013 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai. <br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la commune de Breteuil-sur-Iton.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-04 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. ABANDON DE POSTE. - CAS D'UN AGENT EN CONGÉ DE MALADIE QUI REFUSE DE SE SOUMETTRE À UNE CONTRE-VISITE.
</SCT>
<ANA ID="9A"> 36-10-04 L'agent en position de congé de maladie n'a pas cessé d'exercer ses fonctions et une lettre adressée à un agent à une date où il est dans une telle position ne saurait, en tout état de cause, constituer une mise en demeure avant licenciement pour abandon de poste [RJ1]. Toutefois, si l'autorité compétente constate qu'un agent en congé de maladie s'est soustrait, sans justification, à une contre-visite qu'elle a demandée en application de l'article 15 du décret n° 87-602 du 30 juillet 1987, elle peut lui adresser une lettre de mise en demeure de rejoindre son poste ou de reprendre son service dans un délai appoprié et précisant en outre explicitement que, en raison de son refus de se soumettre, sans justification, à la contre-visite à laquelle il était convoqué, l'agent court le risque d'une radiation sans mise en oeuvre de la procédure disciplinaire, alors même qu'à la date de notification de la lettre il bénéficie d'un congé de maladie. Si, dans le délai fixé par la mise en demeure, l'agent ne justifie pas son absence à la contre-visite à laquelle il était convoqué, n'informe l'administration d'aucune intention et ne se présente pas à elle, sans justifier, par des raisons d'ordre médical ou matériel, son refus de reprendre son poste, et si, par ailleurs, aucune circonstance particulière, liée notamment à la nature de la maladie pour laquelle il a obtenu un congé, ne peut expliquer son abstention, l'autorité compétente est en droit d'estimer que le lien avec le service a été rompu du fait de l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur l'exigence et le contenu de cette mise en demeure, CE, 11 décembre 1998, M.,, n° 147511, p. 474.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
