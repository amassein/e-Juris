<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043096243</ID>
<ANCIEN_ID>JG_L_2021_02_000000440878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/62/CETATEXT000043096243.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 03/02/2021, 440878, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:440878.20210203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Toulon d'annuler la décision du 28 juin 2016 par laquelle le ministre de la défense lui a infligé vingt jours d'arrêts ainsi que la décision implicite de rejet de son recours hiérarchique du 2 septembre 2016 contre cette décision. <br/>
<br/>
              Par une ordonnance n° 1700017 du 26 mai 2020, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Toulon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M. B....<br/>
<br/>
              Par cette requête, M. B... demande au Conseil d'Etat d'annuler la décision du 28 juin 2016 et la décision implicite de rejet de son recours hiérarchique du 2 septembre 2016 contre cette décision.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	M. A... B..., capitaine, affecté le 1er novembre 2014 à l'école de l'aviation légère de l'armée de terre (EAMAT) au Luc-en-Provence en tant qu'expert en maintenance aéromobilité, s'est vu infliger le 28 juin 2016 une sanction de vingt jours d'arrêts, aux motifs qu'il ne s'était pas présenté personnellement à son supérieur hiérarchique pour l'informer de sa reprise de service après un arrêt de travail de plusieurs mois mais s'était contenté de lui envoyer un simple SMS, qu'il avait refusé de se soumettre à une visite médicale de reprise, malgré l'ordre réitéré qui lui en avait été donné, qu'il avait fait du sport le matin sans autorisation et sans s'inscrire et qu'il avait refusé de manière réitérée de se rendre à une convocation à un entretien avec son supérieur hiérarchique. Par une décision implicite, le ministre de la défense a rejeté le recours hiérarchique du 2 septembre 2016 de l'intéressé contre cette sanction. <br/>
<br/>
              Sur la légalité externe:<br/>
<br/>
              2.	En premier lieu, aux termes de l'article R. 4137-15 du code de la défense : " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit, seul ou accompagné d'un militaire en activité de son choix sur les faits qui lui sont reprochés devant l'autorité militaire de premier niveau dont il relève. Au préalable, un délai de réflexion, qui ne peut être inférieur à un jour franc, lui est laissé pour organiser sa défense. / Lorsque la demande de sanction est transmise à une autorité militaire supérieure à l'autorité militaire de premier niveau, le militaire en cause peut également s'expliquer par écrit sur ces faits auprès de cette autorité supérieure. L'explication écrite de l'intéressé ou la renonciation écrite à l'exercice du droit de s'expliquer par écrit est jointe au dossier transmis à l'autorité militaire supérieure. / Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner ". Ces dispositions n'ont ni pour objet ni pour effet de conférer au militaire, pour lequel une sanction disciplinaire est envisagée, le droit d'obtenir de l'administration des pièces et documents autres que ceux sur lesquels l'autorité militaire entend se fonder pour prononcer sa sanction. Par suite, le moyen tiré de ce que l'administration n'a pas communiqué au requérant des documents qui ne faisaient pas partie des documents au vu desquels il était envisagé de le sanctionner ne peut qu'être écarté. <br/>
<br/>
              3.	En second lieu, contrairement à ce qui est soutenu, il ressort des énonciations de la sanction attaquée que son auteur a tenu compte des éléments exposés par le requérant dans ses observations du 13 avril 2016. <br/>
<br/>
              4.	En troisième lieu, aux termes de l'article R. 4138-3 du code de la défense : " Le commandant de la formation administrative d'affectation ou d'emploi peut, à tout moment, faire procéder à un contrôle médical du militaire placé en congé de maladie afin de s'assurer que ce congé est justifié (...) ". Le requérant ne peut utilement soutenir que l'autorité militaire de premier niveau a méconnu ces dispositions, dès lors que les décisions attaquées ne portent pas sur le contrôle médical d'un militaire placé en congé de maladie.<br/>
<br/>
              5.	En quatrième lieu, contrairement à ce que soutient le requérant, il ne ressort d'aucune pièce du dossier que l'autorité militaire aurait détourné un " rapport de l'autorité hiérarchique " rendant compte d'un entretien du militaire avec l'un de ses supérieurs pour le sanctionner en méconnaissance des règles de la procédure disciplinaire. <br/>
<br/>
              6.	En cinquième lieu, aux termes de l'article R. 4137-15 du code de la défense : " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit, seul ou accompagné d'un militaire en activité de son choix sur les faits qui lui sont reprochés devant l'autorité militaire de premier niveau dont il relève. Au préalable, un délai de réflexion, qui ne peut être inférieur à un jour franc, lui est laissé pour organiser sa défense ". Il ressort des pièces du dossier que le requérant a été entendu par l'autorité militaire de premier niveau le 11 avril 2016 et ne soutient pas, ni même n'allègue, avoir été privé de son droit d'être accompagné lors cette audition. Par suite, le moyen tiré de la méconnaissance de l'article R. 4137-15 du code de la défense doit être écarté.<br/>
<br/>
              7.	En sixième lieu, aux termes de l'article R. 4137-135 du code de la défense  " Lorsqu'il s'agit d'une sanction disciplinaire du premier groupe ou d'une sanction professionnelle portant sur l'attribution de points négatifs, le recours administratif est adressé à l'autorité militaire de premier niveau dont relève le militaire et inscrite au registre des recours. L'autorité militaire de premier niveau entend l'intéressé, qui peut se faire assister exclusivement par un militaire en activité de son choix. Si cette autorité maintient la sanction prise ou si la décision contestée excède son pouvoir disciplinaire, elle adresse directement, dans un délai de huit jours francs à partir de la date de l'inscription du recours au registre des recours, le dossier au chef d'état-major de l'armée d'appartenance de l'intéressé ou à l'autorité correspondante pour les autres forces armées et les formations rattachées. Une copie de la transmission est remise à l'autorité militaire de deuxième niveau ainsi qu'à l'intéressé ".<br/>
<br/>
              8.	Il ressort des pièces du dossier que si, dans le cadre de l'examen du recours hiérarchique du 2 septembre contre la sanction litigieuse, l'autorité militaire de premier niveau a envoyé en lettre recommandée avec accusé de réception le 9 septembre 2016 une convocation à M. B... pour un entretien le 28 septembre 2016, l'intéressé n'a pas réceptionné ce courrier lors de sa présentation le 10 septembre en raison d'un changement d'adresse non communiqué préalablement à l'administration. Dès lors, les moyens tirés de la méconnaissance de l'article R. 4137-135 du code de la défense doivent être écartés.<br/>
<br/>
              9.	En dernier lieu, la circonstance que la demande de sanction relatives aux faits survenus les 7 et 8 mars 2016 a été présentée le 25 mars 2016 par l'autorité militaire de premier niveau et la sanction prononcée le 28 juin 2016 est sans incidence sur la légalité de cette dernière, dès lors qu'aucun texte ou principe ne prévoyait à la date des faits litigieux un délai pour engager une procédure disciplinaire. Dès lors, le moyen tiré de la tardiveté de la sanction doit être rejeté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              10.	Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.<br/>
<br/>
              11.	En premier lieu, aux termes du premier alinéa de l'article L. 4122-1 du code de la défense : " Les militaires doivent obéissance aux ordres de leurs supérieurs et sont responsables de l'exécution des missions qui leur sont confiées ". Aux termes de l'article L. 4111-1 du même code : " (...) L'état militaire exige en toute circonstance (...) discipline, disponibilité, loyalisme et neutralité (...) ".<br/>
<br/>
              12.	La sanction attaquée n'a pas retenu, parmi les faits justifiant la mesure contestée, le retard du requérant lors de sa prise de poste le 7 mars 2016. Par suite le moyen tiré de ce que la matérialité de ce grief ne serait pas établie est inopérant. <br/>
<br/>
              13.	En revanche il ressort des pièces du dossier, et n'est pas contesté, que le requérant s'est contenté, lors de la reprise de son service après plusieurs mois d'absence, d'informer son supérieur hiérarchique de sa présence par un SMS, sans se présenter personnellement, qu'il a refusé, malgré l'ordre réitéré qui lui en avait été donné, de se rendre à une visite médicale pourtant obligatoire, contrairement à ce que soutient le requérant, qu'il a pratiqué du sport sans s'être rendu à cette visite, qu'il a enfin également refusé de manière réitérée de se rendre à une convocation à un entretien avec son supérieur hiérarchique. Ces faits, qui traduisent des manquements aux devoirs d'obéissance et de discipline des militaires, constituent des fautes de nature à justifier une sanction disciplinaire. <br/>
<br/>
              14.	En deuxième lieu, aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre ". En infligeant au requérant une sanction de vingt jours d'arrêts, la ministre de la défense n'a pas, eu égard à la nature des faits reprochés à l'intéressé, aux fonctions qu'il exerce et compte tenu du pouvoir d'appréciation dont elle disposait, pris une sanction disproportionnée en lui infligeant vingt jours d'arrêts.<br/>
<br/>
              15.	En troisième lieu, le requérant, qui n'a fait l'objet d'aucune autre sanction disciplinaire, ne peut utilement soutenir que l'autorité militaire aurait méconnu le principe selon lequel nul ne peut être sanctionné à plusieurs reprises pour les mêmes faits.<br/>
<br/>
              16.	En dernier lieu, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              17.	Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre des armées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
