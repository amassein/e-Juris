<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039420586</ID>
<ANCIEN_ID>JG_L_2019_11_000000417380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/42/05/CETATEXT000039420586.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 25/11/2019, 417380, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417380.20191125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1713037 du 12 janvier 2018, enregistrée le 16 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête enregistrée le 11 août 2017 au greffe de ce tribunal, présentée par Mme B... A.... <br/>
<br/>
              Par cette requête et un mémoire en réplique, enregistré le 14 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 24 mai 2017 par laquelle le conseil académique de l'université Pierre et Marie Curie a approuvé la liste des candidats retenus par le comité de sélection pour le recrutement d'un professeur des universités sur le poste n° 4384 ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir l'ensemble des décisions subséquentes relatives à la procédure de recrutement d'un professeur des universités sur le poste n° 4384 et notamment la nomination à intervenir sur ce poste ; <br/>
<br/>
              3°) de mettre à la charge de l'université Pierre et Marie Curie la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-430 du 6 juin 1984 ;<br/>
	- le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la Sorbonne Université.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que Mme A..., maître de conférences à l'université Pierre et Marie Curie, s'est portée candidate au poste n° 4384 de professeur des universités en mécanique ouvert au recrutement au sein de cette université. Mme A... demande l'annulation pour excès de pouvoir de la délibération du 24 mai 2017 par laquelle le conseil académique de l'université Pierre et Marie Curie a approuvé la liste des candidats proposée par le comité de sélection, sur laquelle elle ne figurait pas, ainsi que de l'ensemble des décisions subséquentes relatives à la procédure de recrutement d'un professeur des universités sur le poste n° 4384.<br/>
<br/>
              2. Aux termes de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection (...). / Le comité est composé d'enseignants-chercheurs et de personnels assimilés, pour moitié au moins extérieurs à l'établissement, d'un rang au moins égal à celui postulé par l'intéressé. (...) / Au vu de son avis motivé, le conseil académique (...) siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence ". <br/>
<br/>
              3. En premier lieu, Mme A... ne peut utilement soutenir que le comité de sélection aurait délibéré selon des modalités irrégulières, faute de s'être conformé aux préconisations des instructions ministérielles figurant sur le site Galaxie, dès lors que de telles instructions sont dépourvues de valeur normative. <br/>
<br/>
              4. En second lieu, il n'appartient pas au juge de l'excès de pouvoir de contrôler l'appréciation faite par un jury de la valeur des candidats. Dès lors, Mme A... ne peut davantage utilement soutenir que l'appréciation portée par le comité de sélection sur ses capacités à conduire des recherches est entachée d'erreur manifeste d'appréciation.<br/>
<br/>
              5. Il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation de la délibération du conseil académique qu'elle attaque, ni, par voie de conséquence, celle des actes subséquents relatifs à cette procédure de recrutement. Par suite, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la ministre de l'enseignement supérieur, de la recherche et de l'innovation, sa requête ne peut qu'être rejetée, y compris en ce qu'elle comporte des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Dans les circonstances de l'espèce, il n'y a pas lieu, de faire droit aux conclusions présentées au même titre par Sorbonne Université.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
<br/>
Article 2 : Les conclusions de Sorbonne Université présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.   <br/>
Article 3 : La présente décision sera notifiée à Mme B... A..., à Sorbonne Université et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
