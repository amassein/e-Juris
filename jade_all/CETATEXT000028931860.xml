<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028931860</ID>
<ANCIEN_ID>JG_L_2014_05_000000358233</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/18/CETATEXT000028931860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 12/05/2014, 358233, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358233</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Renaud Jaune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:358233.20140512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 2 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'économie et des finances ; le ministre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE01287 du 19 janvier 2012 par lequel la cour administrative d'appel de Versailles, statuant sur la requête de M. C...A...B...dirigée contre le jugement n° 0404470 du 26 avril 2007 du tribunal administratif de Versailles, l'a déchargé, en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu, de contribution sociale généralisée, de contribution pour le remboursement de la dette sociale et de prélèvement social auxquelles il a été assujetti au titre des années 1998 et 1999 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;	<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Renaud Jaune, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B...a fait l'objet d'un examen contradictoire de sa situation fiscale personnelle au titre des années 1998 et 1999, à l'issue duquel plusieurs sommes enregistrées sur ses comptes bancaires ont été taxées d'office comme revenus d'origine indéterminée ou imposées dans la catégorie des revenus mobiliers ; qu'il a contesté les impositions ainsi mises à sa charge devant le tribunal administratif de Versailles qui, par un jugement du 26 avril 2007 a prononcé un non-lieu à statuer partiel et rejeté le surplus de ses conclusions ; que la cour administrative d'appel de Versailles a confirmé ce jugement par un arrêt du 26 mars 2009, annulé par une décision du Conseil d'Etat statuant au contentieux du 23 mars 2011 ; que le ministre se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Versailles, saisie à nouveau du litige après cassation, a déchargé M. A...B..., en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu, de contribution sociale généralisée, de contribution pour le remboursement de la dette sociale et de prélèvement social auxquelles il a été assujetti au titre des années 1998 et 1999 ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions des articles L. 12 et L. 47 du livre des procédures fiscales qu'un examen contradictoire de la situation fiscale personnelle ne peut normalement s'étendre sur une période supérieure à un an à compter de la réception de l'avis de vérification prévu par les dispositions de l'article L. 47 ; que, cependant, lorsque le contribuable n'a pas produit ses relevés bancaires dans un délai de soixante jours à compter d'une demande de l'administration, le délai d'un an mentionné ci-dessus peut être prorogé des délais nécessaires à celle-ci pour obtenir ces relevés; que ces délais courent à compter du 61ème jour suivant la demande faite au contribuable par l'administration ;<br/>
<br/>
              3. Considérant qu'en jugeant, après avoir relevé que le contribuable n'avait pas usé de la faculté de produire l'intégralité de ses comptes bancaires dans le délai de 60 jours, que le délai nécessaire à l'administration pour obtenir ses relevés de comptes courait, non pas à compter du 61ème jour suivant la demande initiale de l'administration, notifiée dans l'avis d'examen de situation fiscale personnelle, mais à compter de la date postérieure, qui n'était pas établie en l'espèce par l'administration, à laquelle celle-ci a effectivement demandé les relevés de comptes aux établissements bancaires concernés, la cour a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte de l'instruction que l'avis d'examen de la situation fiscale personnelle, dans lequel l'administration demandait au contribuable de lui remettre les relevés des comptes financiers de toute nature et des comptes courants sur lesquels lui-même et les membres de son foyer fiscal avaient réalisé des opérations de nature personnelle pendant la période visée par l'avis de vérification de l'examen de situation fiscale personnelle, a été notifié au requérant le 26 juillet 2000 et que la notification de redressement lui a été adressée le 12 octobre 2001 ; que, dès lors que le M. A...B...n'a pas usé de sa faculté de produire l'intégralité de ses comptes bancaires dans le délai de soixante jours prévu par l'article L. 12 du livre des procédures fiscales et que l'administration fiscale a, en conséquence, fait valoir son droit de communication en demandant à la banque de lui transmettre les relevés de son compte, la durée de production de ces relevés, obtenus le 10 janvier 2001, soit trois mois et quatorze jours, s'ajoute à la durée maximale d'un an à compter de la notification de l'avis de vérification dont dispose l'administration fiscale pour procéder à l'examen de la situation fiscale personnelle ; que cette durée n'était dès lors pas dépassée à la date de notification du redressement du12 octobre 2001 ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes du 4ème alinéa de l'article L. 10 du livre des procédures fiscales : " Avant l'engagement d'une des vérifications prévues aux articles L. 12 et L. 13, l'administration des impôts remet au contribuable la charte des droits et obligations du contribuable vérifié ; les dispositions contenues dans la charte sont opposables à l'administration " ; que, dans sa version remise à M. A...B..., la charte des droits et obligations du contribuable vérifié, rendue opposable à l'administration par les dispositions précitées, exigeait que le vérificateur ait recherché un dialogue contradictoire avec le contribuable vérifié avant d'avoir recours à la procédure contraignante de demande de justifications prévue par l'article L. 16 du livre des procédures fiscales ; qu'il résulte de l'instruction qu'après que l'administration a engagé la procédure d'examen de situation fiscale personnelle et demandé au contribuable la production de ses comptes, celui-ci a bénéficié de trois entretiens oraux avec des membres de l'administration fiscale, respectivement le 2 octobre 2000, soit avant le recours par l'administration à la procédure contraignante de demande de justifications prévue par l'article L. 16 du livre des procédures fiscales, puis les 3 mai 2001 et 3 septembre 2001, avant l'envoi, le 15 octobre 2001, de la notification de redressement ; que, notamment lors du dernier entretien, il a pu faire valoir utilement ses observations sur l'ensemble des relevés bancaires qui avaient été communiqués à l'administration ; qu'au surplus, lui ont été adressées une demande d'éclaircissements sur ses mouvements bancaires et une mise en demeure lui permettant de prendre connaissance des motifs pour lesquels le vérificateur estimait insuffisantes les justifications qu'il donnait ; que par suite, il n'a pas été privé de la possibilité d'un dialogue contradictoire avec l'administration fiscale ;<br/>
<br/>
              7. Considérant, en troisième lieu, que les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées par le contribuable à l'encontre de la procédure suivie à son endroit, dès lors qu'une telle contestation ne porte pas sur des droits et obligations à caractère civil et qu'il n'invoque pas la méconnaissance de ces stipulations dans la fixation des pénalités exclusives de bonne foi ;<br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              8. Considérant que le contribuable ayant été régulièrement taxé d'office en ce qui concerne les revenus d'origine indéterminée sur le fondement des dispositions combinées des articles L. 16 et L. 69 du livre des procédures fiscales, il supporte la charge de la preuve de l'absence de bien-fondé ou du caractère exagéré des impositions mises à sa charge en application de l'article L. 193 du même livre ; qu'il n'établit pas, en se bornant à produire des attestations de reconnaissance de dettes dépourvues de toute autre précision, que les versements effectués à hauteur de 10 000 francs le 6 juillet 1998 et de 40 000 francs le 12 mars 1999 correspondaient effectivement à des remboursements de prêts qui lui auraient été accordés par un tiers ; qu'il ne justifie pas davantage, par les pièces qu'il produit, que les sommes qui proviendraient d'un fournisseur à hauteur de 105 000 francs seraient des remboursements de trois factures réglées par lui-même en 1997 pour le compte des sociétés PL Services et La Flèche Bleue et de la SCI Saint-Jean-de-Braye ; qu'il n'établit pas non plus que les sommes de 38 153, 72 francs et de 28 357,08 francs créditées sur son compte respectivement le 25 mai 1998 et le 8 février 1999 seraient des remboursements de frais de déplacement en provenance de deux de ces sociétés ; que le versement de la société Centrale d'emballage à hauteur de 22 500 francs crédité sur le compte bancaire du contribuable le 9 juillet 1998, dont il est soutenu qu'il correspondrait au remboursement d'une avance de trésorerie accordée à cette société pour 20 000 francs et à une erreur comptable rectifiée ensuite pour le solde, n'est pas non plus justifié ; qu'il en va de même du crédit de 225 000 francs sur le compte courant d'associé de M. A...B...dans la SCI Saint-Jean-de-Braye, dont il invoque qu'il s'agirait d'une erreur comptable mais dont la rectification, à la supposer établie, est en tout état de cause postérieure au début des opérations de contrôle ; que, dès lors, le requérant n'établit pas, par les explications qu'il fournit pour demander la décharge des rehaussements de revenus d'origine indéterminée, leur caractère non imposable et l'exagération des redressements effectués par l'administration fiscale ; <br/>
<br/>
              9. Considérant qu'aux termes du 2° du 1 de l'article 109 du code général des impôts : " Sont considérés comme revenus distribués : (...) / 2° Toutes les sommes ou valeurs mises à la disposition des associés, actionnaires ou porteurs de parts et non prélevées sur les bénéfices (...) " ; que les sommes inscrites au crédit d'un compte courant d'associé ont, sauf preuve contraire apportée par l'associé titulaire du compte, le caractère de revenus imposables dans la catégorie des revenus mobiliers ; que s'agissant de la somme de 1 525 000 francs qui figure sur son compte courant d'associé dans la société Compagim, de la somme de 500 000 francs créditée sur son compte courant d'associé dans la société Nour, de la somme de 50 000 francs portée à son compte lors de la dissolution de la société Ecole de conduite francilienne, de la somme de 170 000 francs inscrite sur son compte courant dans les comptes de la société La Flèche Bleue, de la somme de 300 000 francs inscrite à son compte courant dans la société Trans-Distri, le requérant n'apporte aucun élément de fait ou de droit de nature à remettre en cause l'appréciation portée par le tribunal administratif sur ces redressements ; qu'il y a lieu, dès lors, de rejeter ces conclusions par adoption des motifs retenus par les premiers juges ;<br/>
<br/>
              Sur les pénalités exclusives de bonne foi :<br/>
<br/>
              10. Considérant qu'aux termes de l'article 1729 du code général des impôts, dont les dispositions ont été reprises en substance au même article dans sa rédaction issue de l'ordonnance du 7 décembre 2005 relative à des mesures de simplification en matière fiscale et à l'harmonisation et l'aménagement du régime des pénalités : " Lorsque la déclaration ou l'acte mentionné à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la déclaration insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 % si la mauvaise foi de l'intéressé est établie " ; que les redressements en litige portent sur des crédits inscrits sur les comptes courants d'associé de M. A...B...dans les sociétés dont il était soit dirigeant soit associé soit l'un et l'autre ou sur des sommes en provenance de ces sociétés inscrites sur son compte personnel ; que, dès lors, il a eu nécessairement connaissance de la nature de ces crédits et de leur origine, ainsi que de la nécessité d'en justifier, le cas échéant, le caractère non imposable ; que, dans ces conditions, en se fondant sur ces circonstances, l'administration établit suffisamment l'intention d'éluder l'impôt et, par suite, le bien-fondé des pénalités contestées ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la requête de M. A...B...doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 19 janvier 2012 est annulé.<br/>
<br/>
 Article 2 : La requête présentée par M. A...B...est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. C...A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
