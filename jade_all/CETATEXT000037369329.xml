<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037369329</ID>
<ANCIEN_ID>JG_L_2018_08_000000423267</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/36/93/CETATEXT000037369329.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2018, 423267, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423267</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:423267.20180827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Landes d'enregistrer sa demande d'asile et de lui délivrer une attestation de demande d'asile en procédure normale ainsi que le dossier à présenter à l'Office français de protection des réfugiés et apatrides. Par une ordonnance n° 1801761 du 1er août 2018, le juge des référés a fait droit à sa demande. <br/>
<br/>
              Par une requête enregistrée le 16 août 2018 au secrétariat du contentieux<br/>
du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au juge des référés du<br/>
Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
<br/>
              Il soutient : <br/>
              - que la condition d'urgence n'est pas remplie, dès lors que la privation du bénéfice des conditions d'accueil subie par M. B...résulte non du refus d'enregistrer sa demande d'asile, mais du fait qu'il s'est délibérément soustrait à l'exécution de la décision de transfert ;<br/>
              - qu'aucun nouveau " routing " ne lui a été notifié et qu'il n'a pas été placé en rétention en vue de son éloignement, de sorte que qu'il ne peut se prévaloir de l'imminence d'une telle mesure ; <br/>
              - que la décision contestée ne porte pas une atteinte grave et manifestement illégale au droit d'asile, dès lors qu'un ressortissant étranger faisant l'objet d'une décision de transfert peut être regardé comme " en fuite " au sens de l'article 29 du règlement " Dublin III " lorsqu'il n'a pas déféré à une convocation en préfecture et se manifeste à l'expiration du délai de six mois pour demander l'enregistrement de sa demande d'asile ;<br/>
              - que le demandeur d'asile qui se soustrait intentionnellement à l'exécution de son transfert organisé sous la forme d'un départ contrôlé doit être regardé comme en fuite au sens de ces dispositions ; <br/>
              - que M. B...n'a jamais fait part d'aucune impossibilité matérielle de se rendre à la gare de Dax dans le cadre du " routing " prévu ; <br/>
              - que la circonstance que la procédure de prise en charge par les autorités italiennes n'ait été indiquée à l'intéressé est sans incidence sur son refus de se soumettre à la mesure d'éloignement, dès lors qu'il n'appartient pas à la France d'organiser ces modalités de prise en charge dans le pays d'accueil, dont elle n'est au demeurant pas informée ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 août 2018, M. B...conclut au rejet de la requête et à ce que la somme de 2 500 soit euros soit versée à MeC..., son avocat, en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il soutient :<br/>
              - qu'il a manifesté son intention de se soumettre à la décision de transfert et n'a pas été mis à même de le faire par l'administration ; <br/>
              - que tant l'existence d'un arrêté de transfert que le refus du préfet d'enregistrer sa demande d'asile, qui le prive du bénéfice des conditions d'accueil, caractérisent une situation d'urgence ; <br/>
              - que l'arrêté du préfet de l'Hérault du 12 juillet 2018 porte une atteinte grave et manifestement illégale au droit d'asile ;<br/>
              - qu'en effet, il ne peut être regardé comme en fuite au sens de l'article 29 du règlement " Dublin III " dès lors que cette notion implique que l'intéressé se soit soustrait systématiquement à l'exécution de la mesure de transfert, y compris en cas de départ contrôlé, alors qu'il ne s'est vu notifier qu'un seul " routing " qu'il n'a pas exécuté faute d'informations suffisantes ;  <br/>
              - qu'il ne saurait lui être reproché de s'être manifesté à l'expiration du délai de six mois, dès lors qu'il a respecté ses obligations de pointage, n'a pas demandé l'annulation de la décision de transfert et est resté en contact avec les services préfectoraux ; <br/>
              - qu'il avait un motif légitime de ne pas se rendre à la gare de Dax, dès lors qu'il était dans l'impossibilité de s'y rendre par ses propres moyens et était dans l'incertitude quant aux conditions de sa prise en charge par les autorités italiennes à son arrivée et quant à son hébergement à Paris du 4 au 5 juillet.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre d'Etat, ministre de l'intérieur et, d'autre part, M. B...; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 août 2018 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
              - les représentants du ministre de l'intérieur ;<br/>
              - Me Texier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 23 août 2018 à 18 heures ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) no 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) no 343/2003 du Conseil établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des États membres par un ressortissant d'un pays tiers ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride (refonte) ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013.<br/>
<br/>
              3. Il résulte de l'article 29 du règlement du 26 juin 2013 que le transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, cette période étant susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ". Aux termes de l'article 7 du règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003, qui n'a pas été modifié sur ce point par le règlement d'exécution (UE) n° 118/2014 de la Commission du 30 janvier 2014: " 1. Le transfert vers l'Etat responsable s'effectue de l'une des manières suivantes : a) à l'initiative du demandeur, une date limite étant fixée ; b) sous la forme d'un départ contrôlé, le demandeur étant accompagné jusqu'à l'embarquement par un agent de l'Etat requérant et le lieu, la date et l'heure de son arrivée étant notifiées à l'Etat responsable dans un délai préalable convenu : c) sous escorte, le demandeur étant accompagné par un agent de l'Etat requérant, ou par le représentant d'un organisme mandaté par l'Etat requérant à cette fin, et remis aux autorités de l'Etat responsable (...) ". Il résulte de ces dispositions que le transfert d'un demandeur d'asile vers un Etat membre qui a accepté sa prise ou sa reprise en charge, sur le fondement du règlement du 26 juin 2013, s'effectue selon l'une des trois modalités définies à l'article 7 cité ci-dessus : à l'initiative du demandeur, sous la forme d'un départ contrôlé ou sous escorte.<br/>
<br/>
              4. Il résulte clairement des dispositions mentionnées au point précédent que, d'une part, la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. D'autre part, dans l'hypothèse où le transfert du demandeur d'asile s'effectue sous la forme d'un départ contrôlé, il appartient, dans tous les cas, à l'Etat responsable de ce transfert d'en assurer effectivement l'organisation matérielle et d'accompagner le demandeur d'asile jusqu'à l'embarquement vers son lieu de destination. Une telle obligation recouvre la prise en charge du titre de transport permettant de rejoindre l'Etat responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant et si nécessaire, celle du pré-acheminement du lieu de résidence du demandeur au lieu d'embarquement. Enfin, dans l'hypothèse où le demandeur d'asile se soustrait intentionnellement à l'exécution de son transfert ainsi organisé, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013 rappelées au point 3.<br/>
<br/>
              5. M.B..., ressortissant soudanais, a déclaré être entré irrégulièrement en France le 1er novembre 2017. Il a sollicité l'asile le 10 novembre 2017 et le relevé de ses empreintes a fait apparaître qu'il avait été précédemment identifié en Italie. L'administration a saisi les autorités italiennes d'une demande de reprise en charge qui a été implicitement acceptée le 14 janvier 2018. Par deux arrêtés du 5 juin 2018, le préfet des Landes a décidé le transfert de M. B...aux autorités italiennes et l'a assigné à résidence avec obligation de se présenter trois fois par semaine à la gendarmerie de Dax. Le 3 juillet 2018, des agents de la préfecture des Landes se sont présentés sur son lieu d'hébergement pour lui remettre directement un récapitulatif de son voyage organisé pour le 5 juillet, au départ de Dax et à destination de Milan, ainsi que ses titres de transport. M. B...ne s'est pas présenté à la gare de Dax le jour de son transfert et a présenté une nouvelle demande d'enregistrement de sa demande d'asile le<br/>
14 juillet 2018. Le 30 juillet 2018, M. B... a saisi le juge des référés du tribunal administratif de Pau d'un recours sollicitant, sur le fondement de l'article L. 521-2 du code de justice administrative, l'enregistrement de sa demande d'asile en raison de l'expiration du délai de<br/>
6 mois dans lequel la France pouvait procéder à son transfert. Par une ordonnance du<br/>
1er août 2018 dont le ministre relève appel, le juge des référés a enjoint au préfet des Landes<br/>
de procéder à l'enregistrement de sa demande d'asile et de lui délivrer une attestation de demande d'asile.<br/>
<br/>
              6. Il résulte de ce qui est indiqué au point 4. que dans l'hypothèse où l'administration a respecté les obligations qui sont les siennes dans l'organisation d'un départ contrôlé et où l'intéressé s'est soustrait intentionnellement à l'exécution de ce départ, puis a demandé à nouveau l'enregistrement de sa demande après l'expiration du délai de transfert de six mois, le demandeur doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013. Il résulte de l'instruction que le transfert de M. B...a été organisé sous la forme d'un départ contrôlé, l'administration lui ayant délivré les titres de transport nécessaires et ayant réservé une nuitée d'hôtel pour assurer son hébergement pendant ce transfert, et l'intéressé ayant été convié à se présenter au poste de la police aux frontières de l'aéroport de Roissy une heure avant son embarquement, pour être accompagné lors de celui-ci. Si l'intéressé soutient que l'administration n'a pas accompli toutes les diligences qui lui incombaient lors de ce départ contrôlé, en ne prévoyant pas, malgré la demande qu'il avait formulée, qu'un agent de l'administration le conduise de son lieu d'hébergement à la gare de Dax, il ne résulte pas de l'instruction que M.B..., qui se rendait trois fois par semaine à la gendarmerie de Dax dans le cadre de son assignation à résidence, aurait été dans l'impossibilité matérielle de se rendre lui-même à cette gare. L'administration n'était donc, au cas d'espèce, pas tenue d'assurer son pré-acheminement vers celle-ci. Si M. B...soutient enfin qu'aucune information ne lui avait été délivrée, malgré sa demande en ce sens, sur les modalités d'examen de sa demande d'asile en Italie, aucune obligation ne pesait à cet égard sur les autorités françaises dont la méconnaissance aurait pu justifier qu'il refuse d'exécuter la décision de transfert. Par suite, et alors même que M. B...a respecté les termes de son assignation à résidence et a eu plusieurs contacts avec l'administration au cours de la période considérée, l'administration est fondée, en raison de l'obstruction qu'il a opposée le jour de son transfert, à estimer qu'il était en fuite pour l'application de l'article 29 du règlement cité ci-dessus. C'est, dès lors, à tort que le juge des référés du tribunal administratif de Pau estimé que M. B...n'était pas en fuite et pouvait se prévaloir du délai de six mois prévu par ces dispositions.<br/>
<br/>
              7. Il y a lieu pour le juge des référés du Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. B...à l'appui de sa demande. M. B...soutient que dès lors que l'arrêté de transfert prévoyait<br/>
lui-même, en son article 2, qu'il devait être exécuté dans les six mois suivant l'accord des autorités italiennes, cet arrêté ne pourrait plus être mis à exécution depuis l'expiration de ce délai. Cette disposition a, toutefois, été implicitement mais nécessairement abrogée par la prorogation du délai de transfert à laquelle l'administration a procédé lorsqu'elle a estimé que l'intéressé était en fuite.<br/>
<br/>
              8. Il résulte de tout ce qui précède qu'en refusant d'enregistrer la demande d'asile de M. B...au motif que le délai de transfert n'était pas expiré, l'administration n'a pas porté une atteinte grave et manifestement illégale au droit d'asile de l'intéressé. Il y a lieu, par suite, d'annuler l'ordonnance attaquée et de rejeter les conclusions présentées par M. B...sur le fondement de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              9. L'Etat n'étant pas la partie perdante dans la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font, en tout état de cause, obstacle à ce qu'une somme mise à sa charge au profit de l'avocat de M. B...en application de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du 1er août 2018 du juge des référés du tribunal administratif de Pau est annulée.<br/>
Article 2 : Les conclusions présentées par M. B...sur le fondement de l'article L. 521-2 du code de justice administrative sont rejetées. <br/>
Article 3 : Les conclusions présentées par M. B...sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente ordonnance sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
