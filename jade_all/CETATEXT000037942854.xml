<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037942854</ID>
<ANCIEN_ID>JG_L_2018_12_000000410888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/94/28/CETATEXT000037942854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2018, 410888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410888.20181231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G...J...agissant en son nom propre et au nom d'Aron et LéonardJ..., ses fils mineurs, M. O... C..., Mme B... K...épouseC..., Mme F... C...épouseA..., M. N... C..., M. M... J..., Mme H... E...épouseJ..., M. I... J...et M. D... J...ont demandé au tribunal administratif de Paris de condamner l'Assistance publique - Hôpitaux de Paris (AP-HP) à les indemniser des préjudices ayant résulté pour eux du décès de Mme L...J.... Par un jugement n° 1308579/6-2 du 27 octobre 2015, le tribunal administratif a donné acte aux requérants du désistement de leurs conclusions indemnitaires à l'exception de celles tendant à l'indemnisation des frais de garde des enfants de la victime, a condamné l'AP-HP à verser à ce titre à M. G...J...la somme de 148 216,35 euros, assortie des intérêts au taux légal à compter du 20 février 2013 et de la capitalisation de ces intérêts à compter du 20 février 2014 puis à chaque échéance annuelle, et a condamné l'AP-HP à verser à la caisse primaire d'assurance maladie de Paris la somme de 7 449,03 euros en remboursement de ses débours. <br/>
<br/>
              Par un arrêt n° 15PA04761 du 27 mars 2017, la cour administrative d'appel de Paris, statuant sur l'appel de l'AP-HP et sur l'appel incident de M.J..., a fixé l'indemnisation due par l'AP-HP au titre des frais de garde d'enfants de M. J...à la somme de 6 euros par heure après déduction des aides de la caisse d'allocations familiales et des réductions d'impôts, à raison de deux heures par jour de scolarisation et de 180 jours de scolarisation par an et condamné  l'AP-HP à verser sur cette base à M. J...la somme de 7 749 euros pour le passé et 70 % des frais restant à sa charge jusqu'en juillet 2022 pour le futur. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mai et 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. J...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il se prononce sur l'indemnisation de ses frais de garde d'enfants ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'AP-HP et de faire droit à son appel incident ; <br/>
<br/>
              3°) de mettre à la charge de l'AP-HP la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Cadin, auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. J...et à la SCP Didier, Pinet, avocat de l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 décembre 2018, présentée par M. J... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme J..., mère d'un premier enfant né en 2010, a mis au monde un second enfant le 18 novembre 2011 ; que, le 22 novembre, alors qu'elle avait quitté la maternité, elle a été victime d'un malaise et conduite à l'hôpital Bichat, dépendant de l'AP-HP ; que le médecin qui l'a examinée n'a pas correctement diagnostiqué l'anomalie du rythme cardiaque dont elle était atteinte et l'a renvoyée chez elle ; que Mme J...est décédée dans la nuit du 25 au 26 novembre 2011 ; que son mari, agissant en son nom propre et au nom de ses enfants mineurs, ainsi qu'un groupe de proches de la victime ont demandé à être indemnisés des préjudices ayant résulté pour eux de son décès ; qu'en cours d'instance, ils ont conclu avec l'AP-HP une transaction portant sur l'intégralité de leurs préjudices, à l'exception de celui résultant de l'obligation pour M. J...d'exposer des frais supplémentaires de garde d'enfants ; que, par jugement du 27 octobre 2015, le tribunal administratif de Paris a retenu que l'erreur de diagnostic commise par l'AP-HP avait entraîné une perte de chance de 70 % d'éviter le décès de Mme J...et a indemnisé le préjudice résultant pour son époux de la nécessité d'exposer des frais de garde d'enfants à hauteur de 148 216,35 euros ; qu'il a également fait droit à des conclusions de la caisse primaire d'assurance maladie (CPAM) de Paris tendant au remboursement par l'AP-HP des frais d'hospitalisation exposés pour Mme J...du 23 au 26 novembre 2011 ; que M. J...demande l'annulation de l'arrêt du 27 mars 2017 par lequel la cour administrative d'appel de Paris, statuant sur l'appel de l'AP-HP et sur son appel incident, a réformé ce jugement pour ramener l'indemnisation de ses frais de garde d'enfants à 7 749 euros pour la période antérieure à sa décision et, pour la période postérieure, à 70 % des frais restant à sa charge après déduction des aides de la caisse d'allocations familiales et des réductions d'impôts, sur la base de deux heures par jour de scolarisation et de 180 jours de scolarisation par an, jusqu'en juillet 2022 ; que l'AP-HP présente un pourvoi provoqué tendant à l'annulation de cet arrêt en tant qu'il la condamne à rembourser à la CPAM de Paris 70 % des frais d'hospitalisation de Mme J...pour la période du 23 au 26 novembre 2011; <br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              2. Considérant, en premier lieu, que pour déterminer l'ampleur du préjudice ayant résulté du décès de MmeJ..., la cour a pu, sans dénaturer les faits et pièces du dossier qui lui était soumis, retenir que celle-ci disposait de quarante-trois jours de congés annuels ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'après avoir constaté que Mme J...disposait de dix jours de congés annuels de plus que son mari, la cour administrative d'appel a retenu que son décès avait eu pour conséquence le besoin de faire garder les enfants durant dix jours supplémentaires pendant les vacances scolaires ; que la cour a relevé que M. J...indiquait que ses enfants étaient pris en charge par leurs grands-parents paternels et maternels pour la partie des vacances scolaires au cours de laquelle il devait travailler ; qu'elle a estimé qu'eu égard à la brièveté de la période en cause et au caractère usuel de l'accueil des petits-enfants par les grands-parents pendant au moins une partie des vacances scolaires, il n'y avait pas lieu de mettre à la charge de l'AP-HP une indemnité au titre de frais de garde des enfants pendant ces dix jours ; qu'en se prononçant par ces motifs, qui faisaient apparaître que l'aide apportée par les grands-parents, dans le cadre usuel des relations familiales, dispensait M. J... de confier la garde des enfants à une personne salariée pendant la période en cause, elle n'a pas commis d'erreur de droit et n'a pas entaché son arrêt d'insuffisance de motivation ; <br/>
<br/>
              4. Considérant qu'il ressort des écritures de M. J...devant la cour que celui-ci ne se prévalait pas de la nécessité de devoir exposer, hors période scolaire, des frais de garde à raison de deux heures par jour en raison du fait que son épouse avait la possibilité de prendre en charge leurs enfants plus tôt qu'il ne pouvait le faire lui-même ; qu'il ne peut, par suite, utilement soutenir que la cour aurait commis une erreur de droit et insuffisamment motivé son arrêt en s'abstenant d'indemniser ce chef de préjudice ;  <br/>
<br/>
              5. Considérant, en troisième lieu, que, pour déterminer le terme de la période de responsabilité de l'AP-HP, la cour a retenu que M. J...justifiait de la nécessité de la prise en charge de ses enfants mineurs par un adulte après l'école jusqu'à la fin de la scolarisation du cadet à l'école primaire, soit jusqu'en juillet 2022 ; qu'en se prononçant par ces motifs, qui sont suffisants et exempts de dénaturation, elle n'a pas commis d'erreur de droit, alors même que l'AP-HP, qui critiquait dans sa requête d'appel la date retenue par le tribunal administratif, demandait que ce terme soit fixé à juin 2023 ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. J... doit être rejeté ;<br/>
<br/>
              Sur le pourvoi provoqué de l'AP-HP : <br/>
<br/>
              7. Considérant que, dès lors que les conclusions du pourvoi de M. J...sont rejetées, le pourvoi provoqué par lequel l'AP-HP demande l'annulation de l'arrêt en tant qu'il la condamne à rembourser à la CPAM de Paris 70 % des frais d'hospitalisation de Mme J...ne saurait, en tout état de cause, être accueilli ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire application de ces dispositions pour mettre à la charge de M. J...la somme demandée à ce titre par l'AP-HP ; que ces mêmes dispositions font obstacle à qu'une somme soit mise à la charge de l'AP-HP qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. J...et le pourvoi provoqué de l'AP-HP sont rejetés.  <br/>
Article 2 : Les conclusions présentées par l'AP-HP au titre de dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. G...J..., à l'Assistance publique- Hôpitaux de Paris et à la caisse primaire d'assurance maladie de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
