<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036586672</ID>
<ANCIEN_ID>JG_L_2018_02_000000400003</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/58/66/CETATEXT000036586672.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 07/02/2018, 400003, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400003</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:400003.20180207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SCI du Stade a demandé au tribunal administratif de Rennes de prononcer la décharge ou, à titre subsidiaire, la réduction de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a assujettie au titre de l'année 2013 dans les rôles de la commune de Rostrenen (Côtes d'Armor). Par un jugement n° 1400941 du 23 mars 2016, le tribunal a prononcé un non-lieu à statuer à concurrence du dégrèvement accordé en cours d'instance et rejeté le surplus des conclusions de la demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 mai et 22 août 2016 et le 18 août 2017 au secrétariat du contentieux du Conseil d'Etat, la SCI du Stade demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société du Stade.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SCI du Stade est propriétaire de locaux à usage commercial situés 14 B, rue Pierre Le Balpe à Rostrenen (Côtes-d'Armor), à raison desquels elle a été assujettie à la taxe foncière sur les propriétés bâties au titre de l'année 2013. Elle se pourvoit en cassation contre l'article 2 du jugement du 23 mars 2016 par lequel le tribunal administratif de Rennes a rejeté sa demande tendant à la décharge ou, à titre subsidiaire, à la réduction de cette imposition.<br/>
<br/>
              2. En premier lieu, l'article 1516 du code général des impôts prévoit que les valeurs locatives des propriétés bâties et non bâties sont mises à jour suivant une procédure comportant la constatation annuelle des changements affectant ces propriétés. Aux termes de l'article 1517 de ce code, dans sa rédaction alors applicable : " I. 1. Il est procédé, annuellement, à la constatation des constructions nouvelles et des changements de consistance ou d'affectation des propriétés bâties et non bâties. Il en va de même pour les changements de caractéristiques physiques ou d'environnement quand ils entraînent une modification de plus d'un dixième de la valeur locative ". Pour apprécier si un immeuble a connu des changements de caractéristiques physiques ou des changements d'environnement entraînant une modification de plus d'un dixième de la valeur locative, le juge se détermine au vu des résultats de l'instruction. En écartant le moyen tiré de ce que l'administration n'avait pas procédé à la constatation annuelle des changements d'environnement de l'immeuble de la SCI du Stade au motif que la société ne l'assortissait d'aucune précision quant à l'importance de ces changements permettant d'apprécier s'ils avaient entraîné une modification de plus d'un dixième de sa valeur locative, le tribunal n'a pas méconnu les règles d'administration de la preuve.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 324 AA de l'annexe III au code général des impôts : " La valeur locative cadastrale des biens loués à des conditions anormales ou occupés par leur propriétaire, occupés par un tiers à un titre autre que celui de locataire, vacants ou concédés à titre gratuit est obtenue en appliquant aux données relatives à leur consistance - telles que superficie réelle, nombre d'éléments - les valeurs unitaires arrêtées pour le type de la catégorie correspondante. Cette valeur est ensuite ajustée pour tenir compte des différences qui peuvent exister entre le type considéré et l'immeuble à évaluer, notamment du point de vue de la situation, de la nature de la construction, de son état d'entretien, de son aménagement, ainsi que de l'importance plus ou moins grande de ses dépendances bâties et non bâties si ces éléments n'ont pas été pris en considération lors de l'appréciation de la consistance ". Pour écarter le moyen tiré de ce que l'administration n'établissait pas avoir fait application de ces dispositions, le tribunal a relevé que la valeur locative de l'immeuble dont la SCI du Stade est propriétaire avait été déterminée en appliquant un abattement de 20 % pour tenir compte de la différence de situation d'un point de vue commercial et en termes de desserte entre le local de référence et l'immeuble à évaluer et que la société n'apportait aucun élément tendant à établir que cet abattement serait insuffisant. En statuant ainsi, le tribunal n'a pas dénaturé les pièces du dossier dont il était saisi dès lors que la SCI du Stade s'était bornée à indiquer que le local de référence se situait dans une zone à plus forte valeur marchande et à produire une proposition de location d'une partie de son immeuble.<br/>
<br/>
              4. En dernier lieu, aux termes du second alinéa de l'article L. 80 A du livre des procédures fiscales : " Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente (...) ". Lorsque le contribuable invoque, sur le fondement de ces dispositions, l'interprétation d'un texte fiscal que l'administration a fait connaître par des instructions ou circulaires publiées aucune imposition, même primitive, qui serait contraire à cette interprétation, ne peut être établie. Toutefois, le refus de dégrèvement de la taxe foncière sur les propriétés bâties en cas de vacance dont tout contribuable peut demander à bénéficier en application du I de l'article 1389 du code général des impôts ne constitue pas un rehaussement des impositions mises à sa charge. Il y a lieu de substituer ce motif, qui n'implique l'appréciation d'aucune circonstance de fait, au motif par lequel le tribunal administratif a écarté l'invocation par la société requérante de la réponse ministérielle à M. A..., sénateur, publiée au Journal officiel du 22 mai 2003 et reprise au Bulletin officiel des finances publiques - Impôts sous la référence BOI-IF-TFB-50-20-30 n° 60.<br/>
<br/>
              5. Il résulte de ce qui précède que la SCI du Stade n'est pas fondée à demander l'annulation de l'arrêt attaqué. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SCI du Stade est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SCI du Stade et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
