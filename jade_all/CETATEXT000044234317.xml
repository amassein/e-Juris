<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044234317</ID>
<ANCIEN_ID>JG_L_2021_10_000000444581</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/23/43/CETATEXT000044234317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 20/10/2021, 444581</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444581</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SARL DIDIER-PINET ; SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>Mme Agnès Pic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:444581.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
              M. E... N... et Mme J... M... épouse N..., M. K... C... et Mme R... F... épouse C..., ainsi que d'autres voisins du projet ont demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir l'arrêté du 29 janvier 2015 et la décision du 22 mai 2015 par lesquels le maire d'Angers a délivré à la société Bouygues Immobilier un permis de construire vingt logements individuels sous forme de maisons jumelées et deux immeubles d'habitation collective comprenant trente-neuf logements sur une parcelle située 1, rue des Noyers, puis a rejeté leur recours gracieux formé contre cet arrêté.<br/>
<br/>
              Par un jugement n° 1506167 du 9 juin 2016, le tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16NT02765 du 8 mars 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. et Mme N... et M. et Mme C... contre ce jugement.<br/>
<br/>
              Par une décision n° 420324 du 8 novembre 2019, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 19NT04375 du 17 juillet 2020, la cour d'administrative d'appel de Nantes a rejeté la requête de M. et Mme N... et de M. et Mme C.... <br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 septembre et 18 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme N... et M. et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la commune d'Angers et de la société Bouygues Immobilier la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Pic, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier-Pinet, avocat de M. N..., de Mme M..., de M. C... et de Mme F..., à la SCP Célice, Texidor, Perier, avocat de la commune d'Angers et à la SCP Jean-Philippe Caston, avocat de la société Bouygues Immobilier ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 janvier 2015, le maire d'Angers a délivré à la société Bouygues Immobilier un permis de construire valant division et comprenant des démolitions, l'autorisant à édifier vingt logements individuels groupés par deux ou trois et deux immeubles collectifs de trente-neuf logements sur une parcelle située 1, rue des Noyers. M. et Mme N... et M. et Mme C..., voisins du projet, ont demandé l'annulation de ce permis, ainsi que du rejet de leur recours gracieux, au tribunal administratif de Nantes, qui a rejeté leur demande par un jugement du 9 juin 2016. Ils se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Nantes du 17 juillet 2020 qui, statuant sur renvoi du Conseil d'Etat après annulation de son arrêt du 8 mars 2018, a rejeté l'appel formé par les requérants contre le jugement du tribunal. <br/>
<br/>
              Sur le pourvoi : <br/>
<br/>
              2. Aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas de (...) recours contentieux à l'encontre (...) d'un permis de construire, (...) l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. (...) L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif. / La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt (...) du recours. (...) ". Ces dispositions visent, dans un but de sécurité juridique, à permettre au bénéficiaire d'une autorisation d'urbanisme, ainsi qu'à l'auteur de cette décision, d'être informés à bref délai de l'existence d'un recours gracieux ou contentieux dirigé contre elle. Si, à l'égard du titulaire de l'autorisation, cette formalité peut être regardée comme régulièrement accomplie dès lors que la notification lui est faite à l'adresse qui est mentionnée dans l'acte attaqué, la notification peut également être regardée comme régulièrement accomplie lorsque, s'agissant d'une société, elle lui est adressée à son siège social.<br/>
<br/>
              3. Pour juger irrecevable, sur le fondement des dispositions précitées, le recours contentieux formé par M. et Mme N... et M. et Mme C... contre l'arrêté du 29 janvier 2015, la cour administrative d'appel de Nantes a jugé que les requérants n'avaient pas régulièrement satisfait à leur obligation de notifier leur recours gracieux à la société titulaire de l'autorisation contestée en expédiant cette notification à l'adresse de son siège social, située à Issy-les-Moulineaux, et non à l'adresse de son établissement secondaire, située à Angers, figurant sur l'arrêté du 29 janvier 2015 et sur le panneau d'affichage du permis accordé par cet arrêté. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi, la cour a entaché son arrêt d'une erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, M. et Mme N... et M. et Mme C... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette leur requête.<br/>
<br/>
              4. Il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans cette mesure.<br/>
<br/>
              Sur les fins de non-recevoir opposées par la société Bouygues Immobilier et la commune d'Angers :<br/>
<br/>
              5. Il ressort des pièces du dossier, en premier lieu, que, contrairement à ce que soutient la société Bouygues Immobilier, bénéficiaire du permis de construire, les requérants lui ont régulièrement notifié leur appel, enregistré le 9 août 2016 au greffe de la cour administrative d'appel de Nantes, de même d'ailleurs qu'à la commune d'Angers, par un courrier qui lui a été adressé par une lettre recommandée du même jour, reçue le lendemain. Par suite, la société Bouygues immobilier n'est pas fondée à soutenir que l'appel aurait été irrecevable faute pour les requérants d'avoir satisfait à leur obligation de le notifier en application de l'article R. 600-1 du code de l'urbanisme. <br/>
<br/>
              6. Il ressort également des pièces du dossier, en deuxième lieu, et il n'est d'ailleurs pas contesté, que les intéressés ont notifié leur recours gracieux à la société Bouygues Immobilier, également dans le délai requis par l'article R. 600-1 du code de l'urbanisme, au siège social de cette société. Il résulte de ce qui a été dit au point 3 qu'une telle notification est régulière. Par suite, la société Bouygues Immobilier et la commune d'Angers ne sont pas fondées à soutenir que le recours contentieux des requérants serait irrecevable faute qu'ils aient satisfait à leur obligation de notifier leur recours gracieux dans les conditions prescrites à l'article R. 600-1 du code de l'urbanisme.<br/>
<br/>
              Sur le dossier de demande de permis de construire :<br/>
<br/>
              7. Aux termes de l'article R. 431-8 du code de l'urbanisme : " Le projet architectural comprend une notice précisant : / 1° L'état initial du terrain et de ses abords indiquant, s'il y a lieu, les constructions, la végétation et les éléments paysagers existants ; / 2° Les partis retenus pour assurer l'insertion du projet dans son environnement et la prise en compte des paysages, faisant apparaître, en fonction des caractéristiques du projet : / a) L'aménagement du terrain, en indiquant ce qui est modifié ou supprimé ; / b) L'implantation, l'organisation, la composition et le volume des constructions nouvelles, notamment par rapport aux constructions ou paysages avoisinants ; / c) Le traitement des constructions, clôtures, végétations ou aménagements situés en limite de terrain ; / d) Les matériaux et les couleurs des constructions ;  / e) Le traitement des espaces libres, notamment les plantations à conserver ou à créer ; / f) L'organisation et l'aménagement des accès au terrain, aux constructions et aux aires de stationnement ". Aux termes de l'article R. 431-10 du même code : " Le projet architectural comprend également : / a) Le plan des façades et des toitures; lorsque le projet a pour effet de modifier les façades ou les toitures d'un bâtiment existant, ce plan fait apparaître l'état initial et l'état futur; / b) Un plan en coupe précisant l'implantation de la construction par rapport au profil du terrain; lorsque les travaux ont pour effet de modifier le profil du terrain, ce plan fait apparaître l'état initial et l'état futur; / c) Un document graphique permettant d'apprécier l'insertion du projet de construction par rapport aux constructions avoisinantes et aux paysages, son impact visuel ainsi que le traitement des accès et du terrain;  / d) Deux documents photographiques permettant de situer le terrain respectivement dans l'environnement proche et, sauf si le demandeur justifie qu'aucune photographie de loin n'est possible, dans le paysage lointain. Les points et les angles des prises de vue sont reportés sur le plan de situation et le plan de masse ". <br/>
<br/>
              8. La circonstance que le dossier de demande de permis de construire ne comporterait pas l'ensemble des documents exigés par les dispositions du code de l'urbanisme, ou que les documents produits seraient insuffisants, imprécis ou comporteraient des inexactitudes, n'est susceptible d'entacher d'illégalité le permis de construire qui a été accordé que dans le cas où les omissions, inexactitudes ou insuffisances entachant le dossier ont été de nature à fausser l'appréciation portée par l'autorité administrative sur la conformité du projet à la réglementation applicable.<br/>
<br/>
              9. Il ressort des pièces du dossier, en premier lieu, que le dossier de demande de permis de construire comportait notamment un plan de masse indiquant les arbres à conserver ainsi qu'un document élaboré par un paysagiste incluant des photographies aériennes permettant de visualiser les arbres conservés ainsi que le détail de chaque arbre selon leur intérêt paysager, ces différents documents apportant les éléments nécessaires pour apprécier le caractère ornemental du parc et les projets de conservation ou d'abattage des arbres. Il n'est pas établi que la demande aurait dû comporter des informations particulières en raison de la présence sur le site d'espèces faunistiques protégées.<br/>
<br/>
              10. En deuxième lieu, la demande comportait un descriptif du projet et plusieurs photographies qui permettaient de situer celui-ci dans son environnement, ces photographies mettant en évidence le caractère urbain du secteur dans lequel le projet doit s'insérer et les caractéristiques des constructions qui y sont implantées. <br/>
<br/>
              11. En troisième lieu, le dossier de demande comportait un plan et des photographies faisant apparaître les bâtiments à démolir et leurs dimensions. Si les requérants soutiennent que la surface à démolir mentionnée dans le formulaire de demande était inexacte, les autres pièces du dossier de demande étaient de nature à éclairer suffisamment l'autorité administrative sur les caractéristiques de ces bâtiments, de telle sorte que son appréciation n'a en tout état de cause pu en être faussée.<br/>
<br/>
              12. Enfin, le dossier comportait un plan de coupe (PC 3 et 5) faisant apparaître le rehaussement du sol prévu pour les maisons 1 et 2. Les requérants ne sont ainsi pas fondés à soutenir que la demande ne mentionnait pas les opérations de remblaiement et d'exhaussement projetées.<br/>
<br/>
              13. Par suite, les requérants ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif a écarté leur moyen tiré de ce que le dossier de demande de permis de construire aurait comporté des inexactitudes ou des insuffisances de nature à fausser l'appréciation portée par l'autorité administrative. <br/>
<br/>
              Sur la méconnaissance alléguée de l'article R. 111-2 du code de l'urbanisme :<br/>
<br/>
              14. Aux termes de l'article R. 111-2 du code de l'urbanisme, alors en vigueur : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations. "<br/>
<br/>
              15. Si l'arrêté attaqué dispose en son article 4 que le raccordement au réseau électrique existant devra respecter une puissance maximale de 248 kVa triphasé, il n'est pas établi que ce raccordement et le renforcement de la puissance qu'il pourrait nécessiter impliqueraient une modification du poste de transformation voisin susceptible de comporter un risque d'explosion ou un risque tenant à une augmentation du champ électromagnétique provenant de ce poste. Par suite, c'est à bon droit que le tribunal administratif a jugé, en motivant suffisamment son jugement sur ce point, que le maire n'avait pas commis d'erreur manifeste d'appréciation en ne refusant pas le permis de construire sollicité par application de l'article R. 111-2 du code de l'urbanisme ni, en tout état de cause, méconnu le principe de précaution.<br/>
<br/>
              Sur les méconnaissances alléguées du plan d'occupation des sols :<br/>
<br/>
              16. En premier lieu, aux termes de l'article UC 3.1 du règlement du plan d'occupation des sols dans sa rédaction applicable à la date du permis de construire : " Pour être constructible, un terrain doit être desservi par une voie publique ou privée et un accès (...) ouverts à la circulation automobile, de caractéristiques proportionnées à l'importance de l'occupation ou de l'utilisation du sol envisagées et aux exigences de la sécurité et de la lutte contre l'incendie ". <br/>
<br/>
              17. Il ressort des pièces du dossier, en particulier du plan de masse, que l'accès au terrain d'assiette du projet, qui présente une largeur de 7 mètres, est située rue de la Maître-Ecole, dans une zone à vitesse limitée à 30 km/h, qui n'est ni dans un virage prononcé, ni dans une intersection, et que le mur existant dans cette rue sera supprimé, de sorte qu'il satisfait aux exigences de sécurité. Dès lors, le moyen tiré de la méconnaissance par le projet de l'article UC 3.1 du règlement du plan d'occupation des sols doit être écarté, ainsi que le tribunal administratif l'a jugé à bon droit. <br/>
<br/>
              18. En deuxième lieu, aux termes de l'article UC 11.2 du règlement du plan d'occupation des sols alors applicable : " (...) les mouvements de terre éventuellement nécessaires en raison de la configuration du sol ou du parti d'aménagement doivent rester conformes au caractère de l'environnement local (...) ". <br/>
<br/>
              19. Il ressort des pièces du dossier, en particulier des plans de masse et PC 27-A1, que le terrain d'assiette naturel présente une déclivité en bordure de la rue de la Maître-Ecole avec un point bas s'établissant au niveau 41,31 à la limite de celui-ci avec le transformateur électrique. Le remblai prévu sur l'emplacement des maisons 1 et 2 apparaissant sur le plan PC 3 et 5, justifié par la mise à niveau du terrain, s'établira à une hauteur maximale de 1,24 mètre qui, compte tenu de son caractère mesuré, demeure conforme au caractère de l'environnement local. Les requérants ne sont donc pas fondés à soutenir que c'est à tort que le tribunal administratif a écarté leur moyen tiré de ce que le projet méconnaîtrait les dispositions de l'article UC 11.2 du règlement du plan d'occupation des sols. <br/>
<br/>
              20. En troisième lieu, aux termes de l'article UC 11.6 du règlement du plan d'occupation des sols alors applicable : " Dans les ensembles de constructions et les nouveaux lotissements, le type de clôture doit faire l'objet d'un projet précis inséré au plan d'aménagement de la zone ou au règlement du lotissement ". <br/>
<br/>
              21. Il ressort des pièces du dossier que la demande de permis de construire précisait, alors même que ces éléments ne sont pas exigés par les dispositions citées au point 7, que le projet serait clôturé par un grillage de couleur verte d'une hauteur de 1,20 mètre doublée d'une haie végétalisée d'essences variées. Par suite et en tout état de cause, le projet ne méconnaît pas les dispositions de l'article UC 11.6 du règlement du plan d'occupation des sols, comme le tribunal administratif l'a jugé à bon droit.<br/>
<br/>
              22. En quatrième lieu, aux termes de l'article UC 11.7 du règlement du plan d'occupation des sols alors applicable, relatif aux immeubles existants : " La démolition de tout ou partie d'un immeuble dont la conservation présente un intérêt peut être interdite (...) ".<br/>
<br/>
              23. D'une part, il n'est pas contesté que le projet prévoit la démolition d'une grande bâtisse du XVIIIème siècle en bon état de conservation. Il ne ressort toutefois pas des pièces du dossier que ce bâtiment, qui ne bénéficie pas d'une protection au titre des monuments historiques, présente, bien qu'il figure à l'inventaire général du patrimoine culturel et qu'il soit agrémenté d'un parc paysager, un intérêt de nature à rendre manifestement illégale sa démolition. <br/>
<br/>
              24. D'autre part, les dispositions du règlement du plan d'occupation des sols mentionnées au point 22 étant de nature à permettre la protection des bâtiments dont la conservation présente un intérêt, les requérants ne sont pas fondés à soutenir que la circonstance que la bâtisse et le parc attenant n'ont pas été identifiés par ce document d'urbanisme comme présentant un tel intérêt en révélerait l'illégalité. <br/>
<br/>
              25. En cinquième lieu, le moyen tiré de ce que le projet méconnaîtrait l'article UC 4.3 du règlement du plan d'occupation des sols alors applicable, imposant que les aménagements réalisés sur le terrain garantissent l'écoulement direct et sans stagnation des eaux pluviales dans le réseau collecteur, n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              26. En sixième lieu, toutefois, aux termes de l'article UC 7 du règlement du plan d'occupation des sols dans sa rédaction alors applicable : " Marge d'isolement / 1. Toute construction non implantée sur la limite séparative doit réserver par rapport à cette limite une marge d'isolement au moins égale à 4 mètres. / 2. Toutefois, si l'environnement le justifie, cette marge peut être réduite à 2 mètres pour des constructions de faible importance telles que garages, appentis, remises, serres, dont la hauteur n'excède pas 3 mètres sans tolérance pour les pignons (...) ". Les limites séparatives s'entendent des limites entre la propriété constituant le terrain d'assiette de la construction et la ou les propriétés qui la jouxtent, quelles que soient les caractéristiques de ces propriétés, dès lors qu'il ne s'agit pas de voies ou d'emprises publiques.<br/>
<br/>
              27. Il ressort des plans PC 2 et 5 et du plan de masse PC 2 a que le bâtiment dit " Maison 1 ", qui n'est pas une construction de faible importance, est implanté à 1,95 mètre de la parcelle contiguë cadastrée section CK n° 471. Les requérants sont donc fondés à soutenir que l'arrêté autorisant le permis de construire méconnaît sur ce point les dispositions de l'article UC 7 du règlement du plan d'occupation des sols et que c'est à tort que le tribunal administratif a jugé le contraire. <br/>
<br/>
              28. En septième lieu, en outre, aux termes de l'article UC 8 du règlement du plan d'occupation des sols alors applicable, relatif à l'implantation des constructions les unes par rapport aux autres sur une même propriété : " UC 8.1 Marge d'isolement / 1. Les constructions doivent être édifiées de manière à laisser entre elles une marge d'isolement au moins égale à 4 mètres. / 2. Toutefois, si l'environnement le justifie, cette disposition peut être réduite à 2 mètres à la condition que puissent être satisfaites par ailleurs les exigences de la sécurité et de la défense contre l'incendie (...) ".  Eu égard à leur objet, et en l'absence de précision dans le règlement du plan d'occupation des sols, de telles dispositions n'ont pas pour effet d'interdire la construction de maisons jumelées ou " en bande ", qui n'ont pas de vues les unes sur les autres.<br/>
<br/>
              29. D'une part, il ressort des pièces du dossier que le projet prévoit quatorze logements destinés à être occupés séparément mais accolés deux à deux sous forme de maisons jumelées et six logements destinés à être occupés séparément mais accolés trois par trois, sans avoir de vues l'un sur l'autre. Ces groupes de deux ou trois logements constituent chacune une construction unique au sens des dispositions de l'article UC 8. D'autre part, il ressort également des pièces du dossier que les constructions correspondant aux maisons jumelées 5-6 et 7-8 et aux maisons jumelées 7-8 et 9-10 sont distantes respectivement les unes des autres, compte tenu des garages des maisons 7 et 8 qui, bien qu'implantés partiellement en décroché de l'habitation principale, sont en l'espèce partie intégrante de ces constructions, de 1,5 mètre et de 1,6 mètre. Dès lors, les requérants sont fondés à soutenir que l'arrêté autorisant le permis de construire méconnaît les dispositions de l'article UC 8 du règlement du plan d'occupation des sols pour ce qui concerne les distances entre les maisons 5-6 et 7-8 et les maisons 7-8 et 9-10 et que c'est à tort que le tribunal administratif a jugé le contraire.<br/>
<br/>
              30. Pour l'application de l'article L. 600-4-1 du code de l'urbanisme, aucun autre moyen n'est de nature à entraîner l'annulation de l'acte en litige.<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté du 29 janvier 2015 :<br/>
<br/>
              31. Aux termes de l'article L. 600-5 du code de l'urbanisme : " Sans préjudice de la mise en œuvre de l'article L. 600-5-1, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice n'affectant qu'une partie du projet peut être régularisé, limite à cette partie la portée de l'annulation qu'il prononce et, le cas échéant, fixe le délai dans lequel le titulaire de l'autorisation pourra en demander la régularisation, même après l'achèvement des travaux. Le refus par le juge de faire droit à une demande d'annulation partielle est motivé ".<br/>
<br/>
              32. Il ressort des pièces du dossier que les illégalités relevées aux points 27 et 29 n'affectent qu'une partie du projet et sont susceptibles d'être régularisées. Dans ces conditions, il y a lieu de faire application des dispositions de l'article L. 600-5 du code de l'urbanisme et de prononcer l'annulation partielle de l'arrêté du 29 janvier 2015, ainsi que du recours gracieux des intéressés, en tant qu'ils méconnaissent les articles UC 7 et UC 8 du règlement du plan d'occupation des sols pour les motifs exposés aux points 26 à 29 de la présente décision. En application de l'article L. 600-5 précité, il y a lieu, dans les circonstances de l'espèce, d'accorder au titulaire de l'autorisation un délai courant jusqu'au 31 décembre 2021 pour solliciter la régularisation du permis sur ces deux points.<br/>
<br/>
              33. Il résulte de ce qui précède que M. et Mme N... sont fondés à soutenir que c'est à tort que le tribunal administratif de Nantes a rejeté leur demande en annulation de l'arrêté du 29 janvier 2015 et de la décision du 22 mai 2015 dans la mesure énoncée au point précédent.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              34. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la commune d'Angers et de la société Bouygues Immobilier le versement à M. et Mme N... et autres d'une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'il soit mis à la charge de M. et Mme N... et autres, qui ne sont pas, dans la présente instance la partie perdante, une somme au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 2 et 3 de l'arrêt du 17 juillet 2020 de la cour administrative d'appel de Nantes sont annulés. <br/>
<br/>
Article 2 : L'arrêté du 29 janvier 2015 du maire d'Angers et la décision du 22 mai 2015 sont annulés en tant que le projet autorisé prévoit, d'une part, une implantation du bâtiment dit " Maison 1 " à une distance de 1,95 mètre de la parcelle contiguë cadastrée section CK n°471 et d'autre part, une distance de 1,5 mètre entre les implantations des maisons jumelées 5-6 et 7-8 et de 1,6 mètre entre les maisons jumelées 7-8 et 9-10. <br/>
Article 3 : Le délai accordé à la société Bouygues Immobilier pour solliciter la régularisation du permis litigieux en application de l'article L. 600-5 du code de l'urbanisme expirera le 31 décembre 2021.<br/>
Article 4 : Le jugement du 9 juin 2016 du tribunal administratif de Nantes est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 5 : La commune d'Angers et la société Bouygues Immobilier verseront chacune une somme de 1 500 euros à M. et Mme N... et autres au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Le surplus des conclusions de la requête de M. et Mme N... et autres est rejeté.<br/>
Article 7 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la commune d'Angers et par la société Bouygues Immobilier, tant devant le Conseil d'Etat que la cour administrative d'appel de Nantes, sont rejetées. <br/>
Article 8 : La présente décision sera notifiée à M. E... N... et Mme J... M... épouse N..., premiers dénommés, pour l'ensemble des requérants, à la commune d'Angers et à la société Bouygues Immobilier.<br/>
              Délibéré à l'issue de la séance du 1er octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du Contentieux, présidant ; Mme A... U..., Mme G... T..., présidentes de chambre ; M. B... S..., Mme D... I..., Mme O... Q..., M. P... L..., M. Damien Botteghi, conseillers d'Etat et Mme Agnès Pic, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
Rendu le 20 octobre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Agnès Pic<br/>
                 La secrétaire :<br/>
                 Signé : Mme V... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - OBLIGATION DE NOTIFICATION DU RECOURS. - DESTINATAIRE DE LA NOTIFICATION - ADRESSE [RJ1] - INCLUSION - SIÈGE SOCIAL DE LA SOCIÉTÉ TITULAIRE DE L'AUTORISATION.
</SCT>
<ANA ID="9A"> 68-06-01-04 L'article R. 600-1 du code de l'urbanisme vise, dans un but de sécurité juridique, à permettre au bénéficiaire d'une autorisation d'urbanisme, ainsi qu'à l'auteur de cette décision, d'être informés à bref délai de l'existence d'un recours gracieux ou contentieux dirigé contre elle. Si, à l'égard du titulaire de l'autorisation, cette formalité peut être regardée comme régulièrement accomplie dès lors que la notification lui est faite à l'adresse qui est mentionnée dans l'acte attaqué, la notification peut également être regardée comme régulièrement accomplie lorsque, s'agissant d'une société, elle lui est adressée à son siège social.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la régularité d'une notification faite à l'adresse mentionnée dans l'acte attaqué, CE, 23 avril 2003, Association Nos villages et Mme Lefaux, n° 251608, T. p. 1032 ; CE, 24 septembre 2014, M. Mauro, n° 351689, T. p. 911.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
