<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659276</ID>
<ANCIEN_ID>JG_L_2018_11_000000413512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/92/CETATEXT000037659276.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/11/2018, 413512, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413512.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 18 août et 17 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Teofarma demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du comité économique des produits de santé du 22 juin 2017, rejetant sa demande de modification du prix des spécialités Claradol 500 mg (paracétamol) comprimés effervescents (B/16), Claradol 500 mg caféine (paracétamol, caféine) comprimés quadrisécables (B/16), Claradol caféine 500 mg / 50 mg (paracétamol, caféine) comprimés effervescents sécables (B/16), Claradol codéine 500 mg / 20 mg (paracétamol, phosphate de codéine) comprimés (B/16) et Claradol 500 mg (paracétamol) comprimés sécables (B/16) ; <br/>
<br/>
              2°) d'enjoindre au comité économique des produits de santé d'accepter sa demande de modification du prix des spécialités en litige ou, subsidiairement, de statuer de nouveau sur celle-ci dans un délai d'un mois au plus à compter de la notification de la décision du Conseil d'Etat, en assortissant l'injonction d'une astreinte d'un montant de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Teofarma.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que la société Teofarma a sollicité du comité économique des produits de santé la hausse du prix des spécialités qu'elle exploite, dénommées Claradol 500 mg (paracétamol) comprimés effervescents (B/16), Claradol 500 mg caféine (paracétamol, caféine) comprimés quadrisécables (B/16), Claradol caféine 500 mg / 50 mg (paracétamol, caféine) comprimés effervescents sécables (B/16), Claradol codéine 500 mg / 20 mg (paracétamol, phosphate de codéine) comprimés (B/16) et Claradol 500 mg (paracétamol) comprimés sécables (B/16), en faisant valoir la perte financière engendrée par l'exploitation de ces médicaments compte tenu des charges d'amortissement du coût d'acquisition de leur propriété industrielle. Par une décision du 22 juin 2017, dont la société requérante demande l'annulation pour excès de pouvoir, le comité économique des produits de santé a refusé de faire droit à ces demandes.<br/>
<br/>
              2. Aux termes de l'article L. 162-16-4 du code de la sécurité sociale : " I. - Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité. La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu apportée par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament. (...) ". L'article L. 162-17-4 du même code dispose que : " En application des orientations qu'il reçoit annuellement des ministres compétents, le Comité économique des produits de santé peut conclure avec des entreprises ou groupes d'entreprises des conventions d'une durée maximum de quatre années relatives à un ou à des médicaments visés (...) aux premier et deuxième alinéas de l'article L. 162-17. (...) Ces conventions, dont le cadre peut être précisé par un accord conclu avec un ou plusieurs syndicats représentatifs des entreprises concernées, déterminent les relations entre le comité et chaque entreprise, et notamment : / 1° Le prix mentionné à l'article L. 162-16-5 de ces médicaments (...) et, le cas échéant, l'évolution de ces prix, notamment en fonction des volumes de vente (...) ". Enfin, aux termes de l'article R. 162-20 du même code, dans sa rédaction applicable à la décision attaquée : " Sans préjudice des dispositions prévues à l'article R. 162-20-2, la convention conclue en application des articles L. 162-16-1 et L. 162-17-4 entre l'entreprise exploitant le médicament et le Comité économique du médicament peut, à la demande de l'entreprise ou du comité, faire l'objet d'un avenant dans les conditions prévues par la convention, et notamment dans les cas suivants : / (...) / 4° Modification des données prises en compte pour la fixation du prix des médicaments qui font l'objet de la convention ".<br/>
<br/>
              3. L'article 16 de l'accord cadre signé le 31 décembre 2015 entre le comité économique des produits de santé et le syndicat " Les entreprises du médicament " en application de l'article L. 162-17-4 du code de la sécurité sociale précise : " Lorsqu'une entreprise envisage un arrêt de production ou de commercialisation pour une de ses spécialités pharmaceutiques répondant à un besoin thérapeutique qui ne serait plus couvert en cas de disparition du marché, l'entreprise s'engage (...) à ouvrir une discussion avec le comité sur les conditions économiques de son maintien sur le marché. / En outre, lorsque, pour une spécialité répondant à un besoin thérapeutique qui n'est couvert par aucune autre spécialité moins coûteuse, l'entreprise qui l'exploite demande une hausse du prix justifiée par les conditions financières d'exploitation de cette spécialité, il est tenu compte, le cas échéant, pour l'appréciation de cette demande, des obligations résultant du contrôle des résidus médicamenteux dans l'eau et du coût spécifique de la collecte et de l'élimination des déchets perforants des patients en auto-traitement ou d'autres obligations liées aux normes environnementales ou à la lutte contre la contrefaçon. (...) ".<br/>
<br/>
              4. Sous réserve des cas dans lesquels l'évolution du prix de la spécialité remboursable a été prévue par convention avec l'entreprise exploitant le médicament, il appartient au comité économique des produits de santé, saisi d'une demande en ce sens de l'entreprise, d'apprécier s'il y a lieu de procéder à la modification de prix sollicitée au regard notamment des critères indiqués à l'article L. 162-16-4 du code de la sécurité sociale. Le comité, après avoir examiné l'ensemble de ces critères, qui ne sont d'ailleurs pas exhaustifs, peut légalement refuser la demande qui lui est soumise en faisant usage d'un seul critère, dès lors qu'il est de nature à justifier sa décision.<br/>
<br/>
              5. Il résulte des termes mêmes de la décision attaquée du 22 juin 2017 que le comité économique des produits de santé a refusé de faire droit à la demande d'augmentation du prix des spécialités Claradol, présentée par la société Teofarma, au motif que ces spécialités ne répondaient pas aux critères des " médicaments indispensables " défini par l'article 16 de l'accord conclu le 31 décembre 2015 avec le syndicat " Les entreprises du médicament " et destiné, en application de l'article L. 162-17-4 du code de la sécurité sociale, à préciser le cadre des conventions conclues avec les entreprises ou groupes d'entreprise exploitant des spécialités pharmaceutiques. Si le comité économique des produits de santé pouvait apprécier les critères prévus par les articles L. 162-16-4 et R. 162-20 du code de la sécurité sociale au regard des conditions de leur mise en oeuvre prévues par cet accord cadre et relatives à la détermination de l'évolution du prix des médicaments, dès lors que la société exploitant la spécialité avait conclu avec lui une convention s'y référant, il ne pouvait légalement fonder son refus sur le seul motif tiré de ce que les spécialités considérées ne pouvaient être regardées comme des " médicaments indispensables ", alors que ni les dispositions précitées du code de la sécurité sociale ni, en tout état de cause, les conditions de mise en oeuvre des critères légaux prévues par l'accord cadre du 31 décembre 2015 ne restreignent la possibilité d'augmenter le prix d'une spécialité à cette seule hypothèse. Par suite, la société Teofarma est fondée à soutenir que le comité économique des produits de santé a commis une erreur de droit en se fondant sur ce motif pour rejeter la demande d'augmentation du prix des spécialités en litige.<br/>
<br/>
              6. Il résulte de ce qui précède que la société Teofarma est fondée à demander l'annulation de la décision du comité économique des produits de santé du 22 juin 2017. Le moyen retenu suffisant à entraîner cette annulation, il n'y a pas lieu d'examiner les autres moyens de la requête.<br/>
<br/>
              7. L'exécution de la présente décision implique que la demande de la société Teofarma soit réexaminée. Il y a lieu, par suite, d'enjoindre au comité économique des produits de santé de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision. Dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la société requérante.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Teofarma au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du comité économique des produits de santé du 22 juin 2017 est annulée.<br/>
Article 2 : Il est enjoint au comité économique des produits de santé de réexaminer la demande de la société Teofarma dans un délai de deux mois à compter de la présente décision. <br/>
Article 3 : L'Etat versera à la société Teofarma une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Teofarma et au comité économique des produits de santé.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé et à la section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
