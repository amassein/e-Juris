<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007446734</ID>
<ANCIEN_ID>J1XLX2004X11X000000100658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/44/67/CETATEXT000007446734.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour administrative d'appel de Paris, 4ème Chambre - Formation A, du 9 novembre 2004, 01PA00658, inédit au recueil Lebon</TITRE>
<DATE_DEC>2004-11-09</DATE_DEC>
<JURIDICTION>Cour administrative d'appel de Paris</JURIDICTION>
<NUMERO>01PA00658</NUMERO>
<SOLUTION>Satisfaction totale</SOLUTION>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4EME CHAMBRE - FORMATION A</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. le Prés RIVAUX</PRESIDENT>
<AVOCATS>BLANC</AVOCATS>
<RAPPORTEUR>Mme Marie-Sylvie  DESIRE-FOURRE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. TROUILLY</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le recours, enregistré le 16 février 2001, présenté par le MINISTRE DE L'EDUCATION NATIONALE, complété par les mémoires enregistrés les 27 décembre 2001 et 28 mars 2003  ; le MINISTRE DE L'EDUCATION NATIONALE demande à la Cour  :
<br/>
     1°) d'annuler le jugement n° 99-394 en date du 24 octobre 2000 par lequel le Tribunal administratif de Papeete a annulé le refus implicite du vice-recteur de la Polynésie française de payer à M. X un complément d'indemnité d'éloignement et a renvoyé celui-ci devant l'administration pour qu'il soit procédé au versement de ce complément, correspondant à la prise en compte d'un congé administratif intermédiaire, assorti d'intérêts  ;
<br/>
     2°) de rejeter la demande présentée par M. X devant le Tribunal administratif de Papeete  ;
<br/>
     ..................................................................................................................
<br/>
     Vu les autres pièces du dossier  ;
<br/>
     Vu le code de justice administrative  ;
<br/>
     Vu la loi n° 50-772 du 30 juin 1950 fixant les conditions d'attribution des soldes et indemnités des fonctionnaires civils et militaires relevant du ministère de la France d'outre-mer  ;
<br/>
     Vu le décret du 2 mars 1910 portant règlement sur le solde et les allocations accessoires des fonctionnaires employés et agents des services coloniaux  ;
<br/>
     Vu le décret n° 96-1028 du 27 novembre 1996 relatif à l'attribution de l'indemnité d'éloignement aux magistrats et aux fonctionnaires titulaires et stagiaires de l'Etat en service dans les territoires d'outre-mer et dans la collectivité territoriale de Mayotte  ;
<br/>
     Les parties ayant été régulièrement averties du jour de l'audience  ;
<br/>
     Après avoir entendu au cours de l'audience publique du 19 octobre 2004  :
<br/>
     - le rapport de Mme Désiré-Fourré, premier conseiller  ;
<br/>
     - et les conclusions de M. Trouilly, commissaire du gouvernement  ;
<br/>
     Considérant que le MINISTRE DE L'EDUCATION NATIONALE fait appel du jugement du Tribunal administratif de Papeete qui a annulé le refus implicite du vice-recteur de la Polynésie française de verser à M. X un complément à l'indemnité d'éloignement qui lui a été allouée au titre de son séjour sur le territoire entre 1994 et 2000 pour tenir compte de la durée du congé administratif qu'il a passé en métropole du 7 juillet au 22 août 1997  ;
<br/>
     Sur la recevabilité de l'appel  :
<br/>
     Considérant en premier lieu qu'en vertu de l'article R. 811-4 du code de justice administrative le délai d'appel contre les jugements rendus par le Tribunal administratif de Papeete est en principe de trois mois  ; que s'y ajoute, le cas échéant, en vertu de l'article R. 811-5, le délai supplémentaire de distance d'un mois prévu par l'article 643 du nouveau code de procédure civile  ; que l'article R. 751-8 du code de justice administrative dispose par ailleurs que lorsque la notification d'un jugement du Tribunal administratif de Papeete doit être faite à l'Etat, elle est adressée dans tous les cas au haut-commissaire  ; que cette notification fait courir les délais d'appel à l'encontre de l'Etat  ;
<br/>
     Considérant qu'il en résulte que le délai pour former appel au nom de l'Etat d'un jugement du Tribunal administratif de Papeete est de quatre mois  ; que le recours du MINISTRE DE L'EDUCATION NATIONALE contre le jugement en date du 24 octobre 2000 notifié le 27 octobre 2000 au haut-commissaire de la République, qui a été enregistré au greffe de la Cour le 16 février 2001, avant l'expiration du délai de quatre mois courant à compter de cette notification, n'est pas tardif  ;
<br/>
     Considérant en second lieu que si dans un courrier en date du 9 juin 1998, antérieur à l'introduction de sa demande devant le Tribunal administratif, le vice-recteur a assuré à M. X qu'il se conformerait  aux conclusions rendues par le tribunal pour déterminer les suites qu'il convient de réserver aux demandes formulées par  lui-même et d'autres collègues, cette déclaration d'intention qui se référait à un autre litige n'a pu en tout état de cause valoir acquiescement au jugement rendu ultérieurement sur la demande de M. X  ; que celui-ci n'est pas fondé à soutenir qu'elle avait à son égard le caractère d'une décision individuelle créatrice de droits et ne pouvait par suite être retirée au-delà d'un délai de quatre mois  ; qu'ainsi le recours du MINISTRE DE L'EDUCATION NATIONALE est recevable  ;
<br/>
     Sur la légalité de la décision du vice-recteur  :
<br/>
     Considérant qu'aux termes de l'article 2 de la loi du 30 juin 1950  :  Pour faire face aux sujétions financières inhérentes à l'exercice de la fonction publique dans les territoires d'outre-mer, les fonctionnaires civils recevront (...) 2° Une indemnité destinée à couvrir les sujétions résultant de l'éloignement pendant le séjour et les charges afférentes au retour (...). Elle sera fonction de la durée du séjour et de l'éloignement et versée pour chaque séjour administratif, moitié avant le départ et moitié à l'issue du séjour   ; qu'il ressort de ces dispositions que le droit de l'indemnité n'est ouvert que pour les périodes de séjour effectif sur le territoire et que, notamment, l'indemnité n'est pas due pour les périodes de congé administratif passées en dehors du territoire  ; que le MINISTRE DE L'EDUCATION NATIONALE est, par suite, fondé à soutenir que c'est à tort que, par son jugement du 7 novembre 2000, le Tribunal administratif de Papeete a annulé la décision par laquelle le vice-recteur de la Polynésie française a rejeté la demande de M. X tendant à la révision du montant de l'indemnité d'éloignement qui lui a été versée et l'a renvoyé devant l'administration pour qu'il soit procédé au versement du complément correspondant à la prise en compte de son congé administratif, et à demander l'annulation de ce jugement  ;
<br/>
     Sur les conclusions de M. X  :
<br/>
     Considérant que si M. X fait valoir, à titre subsidiaire, que le montant de l'indemnité d'éloignement telle qu'elle a été liquidée à son profit comporterait des erreurs de calcul, ses conclusions à fin de condamnation de l'Etat sur ce fondement se rapportent à un litige distinct et n'ont fait l'objet d'aucune réclamation préalable auprès de l'administration  ; qu'elles ne peuvent donc qu'être rejetées comme irrecevables  ;
<br/>
     Sur les frais irrépétibles  :
<br/>
     Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, soit condamné à payer à M. X la somme qu'il demande au titre des frais engagés par lui et non compris dans les dépens  ;
<br/>
<br/>
<br/>
     
DECIDE  :
<br/>
     Article 1er  : Le jugement du Tribunal administratif de Papeete du 24 octobre 2000 est annulé.
<br/>
     Article 2  : La demande de M. X présentée devant le Tribunal administratif de Papeete et ses conclusions d'appel incident sont rejetées.
<br/>
<br/>
<br/>
<br/>
     
3
<br/>
     
N° 01PA00658
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
