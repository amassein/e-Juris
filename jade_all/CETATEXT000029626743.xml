<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626743</ID>
<ANCIEN_ID>JG_L_2014_10_000000377056</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 24/10/2014, 377056, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377056</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:377056.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 3 avril 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-213 du 21 février 2014 portant délimitation des cantons dans le département de Loir-et-Cher ou, subsidiairement, de l'annuler en tant qu'il n'a pas fixé les chefs-lieux des nouveaux cantons.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ; <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              2. Le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de Loir-et-Cher, compte tenu de l'exigence de réduction du nombre des cantons de ce département de trente à quinze résultant de l'article L. 191-1 du code électoral.<br/>
<br/>
              I. - Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. En premier lieu, il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons. Par suite, le moyen tiré de ce que cette délimitation relevait de la compétence du législateur, dès lors qu'il s'agissait d'une refonte complète de la carte des cantons et que le Conseil d'Etat pouvait se retrouver en situation d'être à la fois " juge et partie ", ne peut  qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons. Ni le principe constitutionnel de libre administration des collectivités territoriales, ni aucune autre disposition législative ou réglementaire n'imposent une consultation des communes du département faisant l'objet d'une nouvelle délimitation des cantons. Par suite, l'absence de consultation des communes du département n'est pas de nature à entacher d'irrégularité la procédure au terme de laquelle le décret attaqué a été pris.<br/>
<br/>
              5. En troisième lieu, le requérant ne peut, en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire.<br/>
<br/>
              6. En quatrième lieu, le premier alinéa de l'article L. 3121-19 du code général des collectivités territoriales prévoit que : " Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises ". Il n'est pas contesté qu'en application de ces dispositions, le président du conseil général de Loir-et-Cher a adressé aux conseillers généraux, en vue de la réunion du conseil général, le projet de décret délimitant les nouveaux cantons du département, accompagné d'un rapport ainsi que de plusieurs annexes, comportant des cartes et des tableaux. Contrairement à ce que soutient le requérant, le rapport complété par ces documents a permis de donner aux conseillers généraux une information suffisante sur le projet de décret soumis à la délibération du conseil général et, ainsi, a répondu aux exigences fixées par l'article L. 3121-19 du code général des collectivités territoriales.<br/>
<br/>
              II. - Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne les données démographiques prises en considération :<br/>
<br/>
              7. L'article 71 du décret du 18 octobre 2013, dans sa rédaction issue de l'article 8 du décret du 6 février 2014, dont la légalité n'est pas contestée, dispose que : "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Il  est constant que les nouveaux cantons du département de Loir-et-Cher ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012. Par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas aux données démographiques les plus récentes ne peut qu'être écarté.<br/>
<br/>
              En ce qui concerne la délimitation des cantons :<br/>
<br/>
              8. En premier lieu, il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales citées au point 2 que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles. Ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, des cartes des établissements publics de coopération intercommunale, des schémas de cohérence territoriale ou des " bassins de vie " définis par l'Institut national de la statistique et des études économiques. De même, ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prendre comme critères de délimitation de ces circonscriptions électorales les limites des anciens cantons, la proximité géographique des communes ou l'absence de disparité de superficie entre cantons. <br/>
<br/>
              9. En deuxième lieu, les dispositions précitées du c) du III de l'article L. 3113-2 imposent seulement au Premier ministre de comprendre entièrement dans le même canton les communes de moins de 3 500 habitants. Il ressort des pièces du dossier que le choix de diviser la commune de Blois, dont la population municipale s'élève à 46 492 habitants, entre quatre cantons, dont la population ne s'écarte pas de plus de 4,5 % de la population moyenne par canton, est justifié par le souci de rattacher aux mêmes cantons la ville de Blois et les communes limitrophes membres de la communauté d'agglomération. Par suite, si le requérant soutient qu'il aurait été préférable de diviser Blois entre deux cantons, la délimitation retenue par le décret attaqué, qui n'est pas fondée sur des considérations arbitraires, ne peut être regardée comme entachée d'erreur manifeste d'appréciation.  <br/>
<br/>
              10. En troisième lieu, le requérant critique le choix opéré par le décret attaqué de rattacher les communes d'Yvoy-le-Marron et de Lamotte-Beuvron au canton n° 13 (La Sologne)  plutôt qu'à un autre qui lui aurait semblé préférable. Toutefois, il ne conteste pas que les délimitations opérées respectent les critères définis par l'article L. 3113-2 du code général des collectivités territoriales mais se borne à invoquer la méconnaissance de l'intercommunalité. Il ne résulte pas de la seule circonstance que les communes de la communauté de communes de la Sologne des étangs aient dû, pour respecter l'exigence tenant aux bases essentiellement démographiques de la délimitation des cantons, être réparties entre trois cantons différents, que le choix auquel le Premier ministre a procédé reposerait sur une erreur manifeste d'appréciation.<br/>
<br/>
              11. En dernier lieu,  le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              En ce qui concerne les chefs-lieux de cantons et bureaux centralisateurs :<br/>
<br/>
              12. Si l'article R. 112 du code électoral, dans sa version actuellement en vigueur, prévoit que le recensement général des votes est fait par le bureau du chef-lieu de canton, la version de cet article issue du décret du 18 octobre 2013, qui est applicable, comme le décret attaqué, à compter du prochain renouvellement général des assemblées départementales, confie ce rôle au bureau centralisateur du canton. Ainsi, la qualité de bureau centralisateur d'un canton sera, à compter de l'entrée en vigueur de ces nouvelles dispositions, dépourvue de tout lien avec celle de chef-lieu de canton. <br/>
<br/>
              13. En premier lieu, la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un bureau centralisateur sans mentionner les chefs-lieux de canton est sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département de Loir-et-Cher.<br/>
<br/>
              14. En deuxième lieu, en désignant la commune de Salbris comme bureau centralisateur du nouveau canton n° 13 (La Sologne), le décret attaqué n'a ni eu pour objet ni pour effet de procéder au transfert du siège de chefs-lieux de canton. Il suit de là que le moyen tiré de ce que cette désignation, en faisant perdre à la commune de Lamotte-Beuvron la qualité de chef-lieu de canton, ainsi que les dotations qui y sont actuellement attachées et les services publics qui y sont implantés, entacherait le décret attaqué d'erreur manifeste d'appréciation, ne peut qu'être écarté.<br/>
<br/>
              15. En troisième lieu, il ne ressort pas des pièces du dossier que la désignation du bureau centralisateur de la commune de Salbris, qui est la plus peuplée du canton n° 13, comme bureau centralisateur de ce canton, serait entachée d'erreur manifeste d'appréciation. <br/>
<br/>
              16. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation totale du décret qu'il attaque, ni son annulation en tant qu'il ne désigne pas de chefs-lieux de canton.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
