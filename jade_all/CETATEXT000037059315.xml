<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037059315</ID>
<ANCIEN_ID>JG_L_2018_06_000000404846</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/05/93/CETATEXT000037059315.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/06/2018, 404846, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404846</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:404846.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 3 novembre 2016 et les 20 juin et 14 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. D... C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 4 mai 2016 par laquelle le comité de sélection de l'Ecole centrale de Lille a émis un avis défavorable à sa candidature pour le recrutement d'un professeur des universités sur le poste n° 4028 ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la délibération du 6 juin 2016 par laquelle le conseil d'administration du même établissement a classé premier M. A...B...sur ce même poste ;<br/>
<br/>
              3°) d'annuler pour excès de pouvoir la décision implicite par laquelle le directeur du même établissement a rejeté ses recours gracieux dirigés contre ces délibérations ;<br/>
<br/>
              4°) d'annuler pour excès de pouvoir, s'il est intervenu, le décret nommant et titularisant M. B...sur ce même poste ;<br/>
<br/>
              5°) d'enjoindre à l'Ecole centrale de Lille de reprendre la procédure de recrutement à partir de la désignation des membres du comité de sélection ;<br/>
<br/>
              6°) de mettre à la charge de l'Ecole centrale de Lille la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'Ecole centrale de Lille ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. C...s'est porté candidat au poste de professeur des universités n° 4028 ouvert à l'Ecole centrale de Lille ; que par une délibération du 4 mai 2016, le comité de sélection constitué pour ce concours a émis un avis défavorable à sa candidature ; que, par une délibération du 6 juin 2016, le conseil d'administration de l'Ecole centrale de Lille siégeant en formation restreinte a retenu la liste de trois candidats transmise par le comité de sélection, parmi lesquels M. C...ne figurait pas, en classant premier M. B...; que le directeur de cet établissement a implicitement rejeté le recours formé par M. C...contre ces délibérations ; que M. C...demande l'annulation de ces délibérations, de la décision de rejet de son recours gracieux ainsi que du décret portant nomination, titularisation et affectation de M. B...sur le poste en litige ;<br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil académique ou, pour les établissements qui n'en disposent pas, du conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. / Le comité est composé d'enseignants-chercheurs et de personnels assimilés, pour moitié au moins extérieurs à l'établissement, d'un rang au moins égal à celui postulé par l'intéressé. Ses membres sont proposés par le président et nommés par le conseil académique ou, pour les établissements qui n'en disposent pas, par le conseil d'administration, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs et personnels assimilés (...) " ; qu'aux termes des dispositions de l'article 9 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : "  (...) Le comité de sélection est créé par délibération du conseil académique ou de l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs, des chercheurs et des personnels assimilés. Cette délibération précise le nombre de membres du comité, compris entre huit et vingt, et, conformément aux dispositions de l'article L. 952-6-1 du code de l'éducation, le nombre de ceux choisis hors de l'établissement et le nombre de ceux choisis parmi les membres de la discipline en cause. / Les membres du comité de sélection sont proposés par le président ou le directeur de l'établissement au conseil académique ou à l'organe compétent pour exercer les attributions mentionnées au IV de l'article L. 712-6-1, siégeant en formation restreinte aux représentants élus des enseignants-chercheurs et personnels assimilés. (...) La composition du comité de sélection est rendue publique avant le début de ses travaux " ; qu'aux termes des dispositions de l'article 9-2 du même décret : " Le comité de sélection examine les dossiers des (...) professeurs postulant à la nomination dans l'emploi (...) parmi les personnes inscrites sur la liste de qualification aux fonctions (...) de professeur des universités. Au vu de rapports pour chaque candidat présentés par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre. Les motifs pour lesquels leur candidature n'a pas été retenue sont communiqués aux candidats qui en font la demande. / Le président du comité de sélection convoque les candidats et fixe l'ordre du jour de la réunion " ;<br/>
<br/>
              3. Considérant, en premier lieu, que si les laboratoires et départements d'enseignement de l'Ecole centrale de Lille qui étaient concernés par le recrutement litigieux ont suggéré des noms de personnes susceptibles de composer le comité de sélection, seuls les noms proposés, au vu notamment de ces suggestions, par le directeur de l'Ecole centrale de Lille, ont été soumis au conseil d'administration de l'école, compétent pour exercer les attributions dévolues par la loi au conseil académique ; que, par suite, M. C...n'est pas fondé à soutenir que la proposition des membres du comité de sélection aurait méconnu les dispositions citées ci-dessus de l'article 9 du décret du 6 juin 1984 ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que la circonstance que le conseil d'administration aurait, avant le début des travaux du comité de sélection, décidé d'en modifier la composition, n'est pas, par elle-même, de nature à entacher cette composition d'irrégularité ;<br/>
<br/>
              5. Considérant, en troisième lieu, que s'il résulte des dispositions, citées ci-dessus, du dernier alinéa de l'article 9 du décret du 6 juin 1984, que la composition du comité de sélection doit être rendue publique avant le début de ses travaux, M. C...n'est pas fondé à soutenir que ces dispositions ont été méconnues dès lors que l'Ecole centrale de Lille a rendu publique cette composition, en temps utile, sur le site internet de l'établissement ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que la seule circonstance que les rapports sur la candidature de M. C...auraient été établis très peu de temps avant la réunion du 4 mai 2016, au cours de laquelle le comité de sélection a décidé de ne pas l'auditionner, n'est pas de nature à établir que les membres de ce comité n'ont pas eu une connaissance suffisante de son dossier ni que le comité s'est estimé lié par ces avis ;<br/>
<br/>
              7. Considérant, en cinquième lieu, que, contrairement à ce que soutient le requérant, qui se borne à alléguer qu'aucune réunion du comité de sélection n'a eu lieu à cette fin, il ne ressort pas des pièces du dossier que les rapporteurs des dossiers de candidature n'auraient pas été désignés par le comité, ni que ces rapporteurs ne se seraient pas vus remettre les dossiers des candidats ; que la circonstance que le procès verbal de la réunion du 31 mai 2016 du comité de sélection, au cours de laquelle celui-ci a auditionné les six candidats qu'il avait retenus, n'a pas été transmis à M.C..., n'est pas de nature à établir que cette réunion ne se serait pas tenue, ni davantage à entacher d'illégalité la délibération prise à son issue par le comité de sélection ; <br/>
<br/>
              8. Considérant, en sixième lieu, que si M. C...soutient que le comité de sélection n'a auditionné que les candidats pour lesquels les deux rapports émettaient des avis favorables, une telle circonstance, à la supposer établie, n'entache pas d'irrégularité la procédure suivie par le comité ;<br/>
<br/>
              9. Considérant, en septième lieu, que le moyen tiré de ce que les règles de télétransmission des dossiers n'auraient pas été respectées n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              10. Considérant, en huitième lieu, que la circonstance que quatre des membres du comité de sélection avaient dirigé ou été membres du laboratoire LAGIS auquel a appartenu M. C...n'est pas, par elle-même, de nature à entacher d'irrégularité la composition du comité de sélection ; que, par ailleurs, ni la circonstance qu'une décision de réorganisation de ce laboratoire a, en octobre 2010, conféré à M. C...un simple statut de " membre associé ", ni celle que cette décision a été contestée par lui devant le juge administratif, ne sont de nature à établir que la présence dans le comité de sélection d'enseignants-chercheurs appartenant à ce laboratoire méconnaissait le principe d'impartialité ;<br/>
<br/>
              11. Considérant, enfin, qu'il n'appartient pas au juge de l'excès de pouvoir de contrôler l'appréciation faite par un jury de la valeur des candidats ; que, dès lors, M. C...ne saurait utilement soutenir que l'appréciation portée par les délibérations litigieuses sur la valeur de sa candidature est entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que M. C... n'est pas fondé à demander l'annulation des délibérations qu'il attaque,  ni à demander, par voie de conséquence, celle des décisions par lesquelles le directeur de l'Ecole centrale de Lille a refusé de faire droit à son recours gracieux, et de la décision portant nomination, titularisation et affectation de M.B... ; que ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Ecole centrale de Lille sur le fondement de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C... est rejeté.<br/>
Article 2 : Le surplus des conclusions de l'Ecole centrale de Lille est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. C... et à l'Ecole centrale de Lille.<br/>
Copie en sera adressée à la ministre de la recherche, de l'enseignement supérieur et de l'innovation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
