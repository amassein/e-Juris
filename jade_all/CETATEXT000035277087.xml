<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035277087</ID>
<ANCIEN_ID>JG_L_2017_07_000000412160</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/27/70/CETATEXT000035277087.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/07/2017, 412160, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412160</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:412160.20170717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des délibérations des 31 mai, 6 et 8 juin 2017 du conseil académique de l'université Lumière Lyon 2 réuni en formation restreinte, de la décision du 9 mai 2017 rejetant sa candidature, ainsi que, le cas échéant, de toute décision de nomination prise par décret du Président de la République s'agissant du poste " sociologie du genre, sociologie de l'égalité " ouvert au sein de l'université Lumière Lyon 2 ;<br/>
<br/>
              2°) d'enjoindre à l'université Lumière Lyon 2 de reprendre la procédure de mutation conformément aux dispositions de l'article 9-3 du décret n° 84-431 du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences, à compter de la notification de l'ordonnance, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'université Lumière Lyon 2 la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable dès lors que les décisions contestées présentent un lien suffisant entre elles et qu'elle justifie d'un intérêt à agir ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, son obligation de résidence à Toulouse aura nécessairement un impact sur sa situation financière et familiale et, d'autre part, le poste pour lequel elle a candidaté a vocation à être occupé dès la rentrée de septembre 2017 ;<br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ;<br/>
              -  la décision du 9 mai 2017 est entachée d'incompétence ;<br/>
              - les délibérations contestées du conseil académique sont insuffisamment motivées ;<br/>
              - la délibération de ce conseil du 8 juin 2017 est entachée d'un vice de procédure dès lors qu'au moment où elle a été informée du rejet de sa candidature, la délibération n'avait pas encore été signée, ce qui l'a privée de la possibilité de faire valoir ses droits ;<br/>
              - elle est entachée d'une erreur manifeste d'appréciation dès lors que le conseil académique siégeant en formation restreinte a estimé que son profil n'était pas en pleine adéquation avec le poste alors qu'elle disposait des qualités requises justifiant sa nomination ;<br/>
              - les délibérations des 31 mai et 6 juin 2017 sont entachées d'une erreur de droit dès lors qu'il appartient au conseil académique de se prononcer en priorité sur l'adéquation des candidatures de professeurs, remplissant les conditions pour obtenir leur mutation pour rapprochement de conjoint, au profil de poste. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ". <br/>
<br/>
              2. Il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il en va ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers et que, en cas d'annulation, ses effets pourraient être effacés par une réparation pécuniaire. Il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue.<br/>
<br/>
              3. Mme A...est professeure des universités, en poste à l'université Toulouse 3. Quelles que soient les contraintes de sa vie professionnelle et les obligations familiales qui pèsent sur elle, eu égard notamment à l'état de santé de ses parents, les effets de ces décisions, qui se bornent à faire obstacle à la mutation qu'elle souhaitait obtenir de Toulouse à Lyon, où résident son conjoint et sa fille, pour la prochaine rentrée universitaire, ne portent pas à sa situation une atteinte suffisamment grave pour caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, une mesure de suspension soit prononcée.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que la requête de Mme A... ne peut être accueillie. Par suite,  ses conclusions à fin de suspension ainsi que celles présentées à fin d'injonction et au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
