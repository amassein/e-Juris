<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030646506</ID>
<ANCIEN_ID>JG_L_2015_05_000000385991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/65/CETATEXT000030646506.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 22/05/2015, 385991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385991.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. K...W..., Mme AQ...BF...et M. BG... AD...ont demandé au tribunal administratif de Grenoble d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 pour l'élection des conseillers municipaux dans la commune d'Annemasse (Haute-Savoie) et de suspendre les mandats de M. AG...et Mme BI....<br/>
<br/>
              Par un jugement n° 1401975 du 27 octobre 2014, le tribunal administratif de Grenoble a fait droit à leur protestation.<br/>
<br/>
              1° Sous le n° 385991, par une requête et un mémoire en réplique, enregistrés les 26 novembre 2014 et 25 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. E... AG...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M.W..., Mme BF...et M. AD...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 386005, par une requête sommaire et un mémoire complémentaire enregistrés les 26 novembre et 24 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. AZ...AR..., M. X...AF..., Mme BA...AT..., M. D... BC..., Mme U...K..., Mme G...C..., Mme H...O..., M.BK..., Mme BB...AS..., Mme AU...BD..., M. L...T..., M. AZ... BE..., M. R...AO..., M. AL...S..., M. D...AW..., M. AA... AY..., Mme Y...AM..., M. AH...N..., Mme AV...M..., M. Q...Z..., MmeBJ..., M. AE... BH..., M. AK...AP..., Mme J...F..., Mme AC...V..., Mme AB...P..., Mme B...I..., Mme A...AJ...et Mme AX... AN...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement analysé sous le numéro précédent ; <br/>
<br/>
              2°) de rejeter la protestation électorale dirigée contre les élections municipales d'Annemasse des 23 et 30 mars 2014.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 mai 2015, présentée par M.AG... ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de M.AR..., et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes de M. AG...et de M. AR... et autres sont dirigées contre le même jugement ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119, R. 120 et R. 121 du code électoral relatifs aux délais impartis pour l'instruction et le jugement des protestations dirigées contre l'élection des conseillers municipaux que, par dérogation aux dispositions de l'article R. 611-5 du code de justice administrative, le tribunal administratif n'est pas tenu de notifier aux conseillers dont l'élection est contestée les pièces jointes produites à l'appui de la protestation ; qu'il appartient seulement au tribunal administratif, une fois ces pièces enregistrées, de les tenir à la disposition des parties de sorte que celles-ci soient à même, si elles l'estiment utile, d'en prendre connaissance ; qu'ainsi les requérants, qui, en tout état de cause, ne peuvent utilement se prévaloir de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dont les stipulations ne sont pas applicables au contentieux électoral, ne sont pas fondés à soutenir que l'absence de communication, par le tribunal administratif de Grenoble, des pièces jointes à la protestation de M. W...a été de nature à entacher la régularité de son jugement ; que, par ailleurs, la circonstance que le même rapporteur public a prononcé à deux reprises ses conclusions sur cette affaire, rayée du rôle après une première audience publique, est également sans incidence sur la régularité de ce jugement ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 264 du code électoral applicable aux communes de plus de 1 000 habitants : " Une déclaration de candidature est obligatoire pour chaque tour de scrutin (...) " ; qu'aux termes de l'article L. 265 du même code : " La déclaration de candidature résulte du dépôt à la préfecture ou la sous-préfecture d'une liste répondant aux conditions fixées aux articles L. 260, L. 263 et L. 264. Il en est délivré récépissé. / Elle est faite collectivement pour chaque liste par la personne ayant la qualité de responsable de liste. (...) / Pour chaque tour de scrutin, cette déclaration comporte la signature de chaque candidat, sauf le droit pour tout candidat de compléter la déclaration collective non signée de lui par une déclaration individuelle faite dans le même délai et comportant sa signature. (...) / Récépissé ne peut être délivré que si les conditions énumérées au présent article sont remplies (...) " ; qu'enfin, aux termes de l'article L. 269 du même code : " Est nul tout bulletin établi au nom d'une liste dont la déclaration de candidature n'a pas été régulièrement enregistrée " ;<br/>
<br/>
              4. Considérant que, pour annuler l'ensemble des opérations électorales dans la commune d'Annemasse, le tribunal administratif s'est fondé sur la circonstance que Mme AI..., présentée comme candidate sur la liste conduite par M.AG..., n'était, en réalité, pas candidate à ces élections municipales et n'avait ni rempli ni signé la déclaration de candidature présentée à son nom ;<br/>
<br/>
              5. Considérant qu'il appartient au juge de l'élection, lorsqu'il constate une ou plusieurs manoeuvres de cette nature, de rechercher si, eu égard aux résultats des opérations électorales, elles ont altéré la sincérité du scrutin dans son ensemble ; que, dans l'affirmative, il lui appartient d'annuler l'intégralité des opérations électorales ; <br/>
<br/>
              6. Considérant que l'irrégularité de la composition de la liste conduite par M. AG..., qui résulte de l'instruction et n'est d'ailleurs pas sérieusement contestée, entache la validité des suffrages qu'elle a obtenus ; qu'eu égard au nombre de ces suffrages qui, tant au premier qu'au second tour de scrutin, étaient dans l'un et l'autre cas supérieurs à l'écart de voix séparant les deux autres listes arrivées en tête, respectivement conduites par M. W...et M. AR..., cette manoeuvre a été de nature à fausser le résultat des élections municipales de la commune d'Annemasse ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a annulé l'ensemble des opérations électorales ;<br/>
<br/>
              8. Considérant qu'aux termes du premier alinéa de l'article L. 118-4 du code électoral : " Saisi d'une contestation formée contre l'élection, le juge de l'élection peut déclarer inéligible, pour une durée maximale de trois ans, le candidat qui a accompli des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin " ; qu'il résulte de ces dispositions que, régulièrement saisi d'un grief tiré de l'existence de manoeuvres, le juge de l'élection peut, le cas échéant d'office, et après avoir, dans cette hypothèse, recueilli les observations des candidats concernés, prononcer une telle sanction si les manoeuvres constatées présentent un caractère frauduleux et s'il est établi qu'elles ont été accomplies par les candidats concernés et ont eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin ; qu'en l'espèce, toutefois, il n'est pas établi, en l'état de l'instruction, que l'accomplissement des manoeuvres ayant eu pour effet de porter atteinte à la sincérité du scrutin soit personnellement imputable à M.AG..., ou à MmeAG..., candidate sur la même liste ; que, dès lors, les conclusions de M. W...tendant à ce que M. et Mme AG...soient déclarés inéligibles doivent être rejetées ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. W...et autres qui ne sont pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. W...et autres au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les requêtes de M. AG...et de M. AR...et autres sont rejetées.<br/>
Article 2 : Les conclusions de M. W...et autres tendant à l'application des dispositions de l'article L. 118-4 du code électoral et de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. E... AG..., à M. K... W..., à M. AZ... AR...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
