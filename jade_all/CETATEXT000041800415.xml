<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041800415</ID>
<ANCIEN_ID>JG_L_2020_03_000000438891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/04/CETATEXT000041800415.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 05/03/2020, 438891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:438891.20200305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 20 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 21 octobre 2019 par laquelle l'Agence française de lutte contre le dopage (AFLD), d'une part, a prononcé à son encontre la sanction de l'interdiction pendant deux ans, en premier lieu, de participer à toute manifestation sportive donnant lieu à la remise de prix en argent ou en nature de même qu'aux manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un de ses membres, en deuxième lieu, de participer directement ou indirectement à l'organisation et au déroulement des manifestations sportives et des entrainements mentionnés ci-dessus, en troisième lieu, d'exercer les fonctions de personnel d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à la fédération et, en dernier lieu, d'exercer les fonctions définies à l'article L. 212-1 du code du sport et, d'autre part, a demandé à la fédération française de la course camarguaise d'annuler les résultats obtenus par lui le 16 septembre 2018, ainsi qu'entre le 16 septembre 2018 et la date de notification de la décision, avec toutes les conséquences en découlant. <br/>
<br/>
              2°) de mettre à la charge de l'AFLD la somme de 3 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              M. B... soutient que :<br/>
              - la condition d'urgence est remplie dès lors que la décision attaquée préjudicie de manière suffisamment grave et immédiate à sa situation en ce que, en premier lieu, elle l'expose à l'impossibilité d'exercer son activité sportive, en deuxième lieu, elle porte atteinte à ses intérêts financiers et, en dernier lieu, elle porte atteinte à son image et à sa réputation ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - celle-ci a été prise au terme d'une procédure irrégulière, dès lors que, premièrement, l'agent préleveur n'était pas dûment agréé et assermenté, deuxièmement, M. B... n'a pas été mis en mesure de s'assurer de l'identité entre les numéros de code des échantillons prélevés et ceux inscrits sur le procès-verbal, contrairement aux dispositions de l'article R. 232-51 du code du sport et, troisièmement, les opérations de contrôle se sont déroulées dans un cadre et des conditions inappropriés, notamment au regard du respect de la vie privée et de la confidentialité du sportif, de l'exigence de surveillance constante  des opérations par la personne chargée du contrôle  ainsi que de l'exigence de surveillance du matériel et des échantillons ;<br/>
              - la matérialité des faits ne peut être regardée comme établie dès lors qu'il a été privé à tort du bénéfice d'une analyse de l'échantillon B, en violation de l'article R. 232-64 du code du sport ;<br/>
              -  il apparaît que l'analyse de l'échantillon A n'a pas été pratiqué sur des prélèvements pouvant lui être attribués, puisqu'elle ne fait pas apparaître de traces des médicaments autorisés qu'il a absorbés peu avant l'épreuve ;<br/>
              - la sanction prononcée à son encontre présente un caractère disproportionné. <br/>
              Par un mémoire en défense, enregistré le 27 février 2020, l'AFLD conclut au rejet de la requête et à ce que soit mis à la charge de M. B... le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés par M. B... n'est propre à créer un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;<br/>
              - le décret n° 2018-6 du 4 janvier 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et, d'autre part, l'AFLD ; <br/>
<br/>
              Ont été entendus au cours de l'audience publique du 2 mars 2020 à 9 heures 30 :<br/>
<br/>
              - Me Tapie, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Agence française de lutte contre le dopage ;<br/>
<br/>
      - le représentant de l'Agence française de lutte contre le dopage ; <br/>
               et à l'issue de laquelle le juge des référés a reporté la clôture l'instruction au 4 mars à 19 heures puis au 5 mars à 12 heures.<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés le 3 et 4 mars 2020, par lequel l'Agence française de lutte contre le dopage maintient ses conclusions et ses moyens ;<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés les 4 et 5 mars 2020, par lequel M. B... maintient ses conclusions et ses moyens.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes de l'article L. 232-9 du code du sport, dans sa rédaction alors applicable : " Il est interdit à tout sportif : (...) 2° D'utiliser ou tenter d'utiliser une ou des substances ou méthodes interdites figurant sur la liste mentionnée au dernier alinéa du présent article./ L'interdiction prévue au 2° ne s'applique pas aux substances et méthodes pour lesquelles le sportif : a) Dispose d'une autorisation pour usage à des fins thérapeutiques ; (...) c) Dispose d'une raison médicale dûment justifiée./ La liste des substances et méthodes mentionnées au présent article est celle qui est élaborée en application de la convention internationale mentionnée à l'article L. 230-2 ou de tout autre accord ultérieur qui aurait le même objet et qui s'y substituerait. Elle est publiée au Journal officiel de la République française. " Aux termes du 1° de l'article L. 232-23 du même code, dans sa rédaction alors applicable, la commission des sanctions de l'Agence française de lutte contre le dopage peut prononcer à l'encontre des sportifs ayant enfreint les dispositions de l'article L. 232-9 " a) Un avertissement ;/ b) Une interdiction temporaire ou définitive de participer à toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, de même qu'aux manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un des membres de celle-ci ;/ c) Une interdiction temporaire ou définitive de participer directement ou indirectement à l'organisation et au déroulement des manifestations sportives et des entraînements mentionnés au b du présent 1° ;/ d) Une interdiction temporaire ou définitive d'exercer les fonctions définies à l'article L. 212-1 ;/ e) Une interdiction d'exercer les fonctions de personnel d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à la fédération./ La sanction prononcée à l'encontre d'un sportif peut être complétée par une sanction pécuniaire dont le montant ne peut excéder 45 000 &#128;. Elle est complétée par une décision de publication nominative de la sanction, dans les conditions fixées par l'article L. 232-23-3-1. " Aux termes de l'article L. 232-23-3-3 du même code, dans sa rédaction alors applicable : " I.- La durée des mesures d'interdiction mentionnées au 1° du I de l'article L. 232-23 à raison d'un manquement à l'article L. 232-9 : (...) b) Est de deux ans lorsque ce manquement est consécutif à l'usage ou à la détention d'une substance spécifiée. Cette durée est portée à quatre ans lorsque l'Agence française de lutte contre le dopage démontre que le sportif a eu l'intention de commettre ce manquement. / II.- Les substances spécifiées et les substances non spécifiées mentionnées au I, dont l'usage ou la détention sont prohibés par l'article L. 232-9, sont celles qui figurent à l'annexe I à la convention internationale mentionnée à l'article L. 230-2. " Enfin, aux termes de l'article L. 232-23-3-10 du même code, dans sa rédaction alors applicable : " La durée des mesures d'interdiction prévues aux articles L. 232-23-3-3 à L. 232-23-3-8 peut être réduite par une décision spécialement motivée lorsque les circonstances particulières de l'affaire le justifient au regard du principe de proportionnalité. "<br/>
<br/>
              3.  Il résulte de l'instruction que M. B..., qui pratique la course camarguaise en qualité de raseteur, a été soumis à un contrôle anti-dopage à l'occasion d'une manche de la compétition dite " Trophée des As " qui s'est déroulée à Mouriès le 16 septembre 2018. L'analyse de l'échantillon A de ses urines a révélé la présence de corticoïdes, plus précisément du 6-hydroxy triamcinolone acétonide, métabolite de la triamcinolone acétonide, à une concentration estimée à 37 nanogrammes par millilitres. La triamcinolone fait partie de la liste des substances spécifiées en application du décret du 4 janvier 2018 portant publication de l'amendement à l'annexe I de la convention internationale contre le dopage dans le sport, adopté à Paris le 15 novembre 2017, alors applicable. L'intéressé a été convoqué le 15 février 2019 devant la commission d'appel de discipline antidopage de la fédération française de la course camarguaise, le délai imparti à l'organe disciplinaire de première instance par les textes alors applicables étant expiré. La commission d'appel de la fédération n'ayant pas été en mesure, elle non plus, de se prononcer dans le délai alors fixé par l'article L. 232-21 du code du sport, la procédure disciplinaire engagée par la fédération française de la course camarguaise à l'encontre de M. B... s'est poursuivie de plein droit devant l'Agence française de lutte contre le dopage (AFLD), qui a été saisie du dossier en l'état, par l'effet des dispositions du III de l'article 37 de l'ordonnance n° 2018-1178 du 19 décembre 2018. Par courrier du 7 mai 2019, l'AFLD a informé le sportif de l'existence d'un manquement présumé. Le 19 juin 2019, le secrétaire général de l'AFLD a adressé à M. B... une proposition d'entrée en voie de composition administrative, qui a été expressément refusée par le celui-ci. Par courrier du 16 juillet 2019, le collège de l'AFLD a notifié à M. B... les griefs retenus contre lui. Convoqué par une lettre recommandée du 23 septembre 2019, dont il accusé réception le lendemain, à la séance de la commission des sanctions de l'Agence du 21 octobre 2019, M. B... n'a pas présenté d'observations écrites et n'était ni présent ni représenté à cette séance. Par une décision du 21 octobre 2019, notifiée le 4 janvier 2020, la commission des sanctions de l'AFLD, d'une part, a prononcé à son encontre la sanction de l'interdiction pendant deux ans, en premier lieu, de participer à toute manifestation sportive donnant lieu à la remise de prix en argent ou en nature de même qu'aux manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un de ses membres, en deuxième lieu, de participer directement ou indirectement à l'organisation et au déroulement des manifestations sportives et des entrainements mentionnés ci-dessus, en troisième lieu, d'exercer les fonctions de personnel d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à la fédération et, en dernier lieu, d'exercer les fonctions définies à l'article L. 212-1 du code du sport et, d'autre part, a demandé à la fédération française de la course camarguaise d'annuler les résultats obtenus par lui le 16 septembre 2018, ainsi qu'entre le 16 septembre 2018 et la date de notification de la décision, avec toutes les conséquences en découlant. M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision.<br/>
<br/>
              4. En premier lieu, contrairement à ce qui est soutenu, l'agent qui a effectué le contrôle anti-dopage de M. B... était agréé et assermenté.<br/>
<br/>
              5. En deuxième lieu, M. B... a reconnu, en apposant sa signature, sans émettre d'observations, sur le procès-verbal de contrôle, que les informations fournies dans ce document et les numéros d'échantillons étaient exacts, et que le prélèvement s'était déroulé en conformité avec les procédures applicables.<br/>
<br/>
              6. En troisième lieu, il n'apparaît pas que les opérations de contrôle se soient déroulées dans des conditions ne permettant pas de garantir que l'échantillon A d'urine ultérieurement analysé a bien été prélevé sur M. B....<br/>
<br/>
              7. En quatrième lieu, si M. B... a initialement demandé l'analyse de l'échantillon B à ses frais, comme le lui permettaient les dispositions de l'article 232-64 du code du sport, il n'a, en définitive, pas donné suite à cette demande.<br/>
<br/>
              8. En cinquième lieu, M. B... fait valoir que l'échantillon A aurait dû faire apparaître des traces de deux médicaments qui lui avaient été prescrits peu avant la compétition et que, par suite, cet échantillon ne peut provenir de ses urines. Toutefois, il résulte de l'instruction que l'absence de traces de ces médicaments dans l'échantillon A n'est, en tout état de cause, compte tenu de leur composition et de leur mode d'administration, pas incompatible avec l'affirmation de M. B... selon laquelle il aurait effectivement suivi un tel traitement. De plus, aucune trace de ces médicaments n'est apparue dans les échantillons des autres sportifs prélevés.<br/>
              9. En dernier lieu, l'intéressé n'apporte aucune explication utile sur les raisons susceptibles d'expliquer la présence de corticoïdes dans ses urines ni ne fait état de circonstances particulières permettant de regarder, dans les circonstances de l'espèce, la sanction prononcée à son encontre comme disproportionnée.<br/>
<br/>
              10 Il résulte de tout ce qui précède qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée. Par suite, et sans qu'il soit besoin d'examiner la condition d'urgence, la requête de M. B... doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par l'Agence française de lutte contre le dopage.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : Les conclusions de l'Agence française de lutte contre le dopage tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à M. A... B... et à l'Agence française de lutte contre le dopage<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
