<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032698993</ID>
<ANCIEN_ID>JG_L_2016_06_000000388754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/69/89/CETATEXT000032698993.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 08/06/2016, 388754</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:388754.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...D...et Mme C...D...ont demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir, d'une part, la délibération du conseil municipal de Massy (Essonne) du 31 mai 2007 approuvant le lancement des procédures préalables à la cession du terrain situé 80 rue Marx Dormoy à Massy et autorisant le maire à demander la sortie du lot n° 5 de la copropriété horizontale située 80 rue Marx Dormoy et 8/10 chemin des Sablons et à purger le droit de rétrocession à l'égard des anciens propriétaires des lots de la copropriété verticale constituant l'immeuble inachevé situé 80 rue Marx Dormoy et, d'autre part, la délibération du conseil municipal de Massy du 2 avril 2009 autorisant son maire à signer les actes de scission de la copropriété des 78/80 rue Marx Dormoy et du 8/10 chemin des Sablons. <br/>
<br/>
              Par un jugement n°s 0706961, 0711841 et 0905066 du 11 octobre 2010, le tribunal administratif de Versailles a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 13VE01448 du 30 décembre 2014, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Versailles a rejeté l'appel formé contre ce jugement par Mme C...D...et Mme E...A...néeD..., reprenant, à la suite du décès de M. B...D..., l'instance engagée par ce dernier.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mars 2015, 16 juin 2015 et 18 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, Mme D...et Mme A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Versailles du 30 décembre 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Massy la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - la loi n° 65-557 du 10 juillet 1965 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme D...et de MmeA..., et à Me Le Prado, avocat de la commune de Massy ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par un jugement du 11 octobre 2010, le tribunal administratif de Versailles a rejeté la demande de M. B...D...et Mme C...D... tendant à l'annulation de deux délibérations des 31 mai 2007 et 2 avril 2009 par lesquelles le conseil municipal de la commune de Massy a autorisé les procédures préalables à la cession de la parcelle cadastrée I 568, située 80 rue Marx Dormoy à Massy, qui appartenait à une copropriété horizontale, puis autorisé son maire à signer l'acte de scission de cette copropriété ; que, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Versailles a, par l'arrêt attaqué du 30 décembre 2014, rejeté l'appel formé contre ce jugement par Mme C...D...et Mme E...A...qui, à la suite du décès de M. B...D..., a repris l'instance engagée par ce dernier ; <br/>
<br/>
              2. Considérant que les décisions juridictionnelles doivent porter par elles-mêmes la preuve de leur régularité ; que la mention de l'audition des parties ou de leur mandataire lors de l'audience, prévue par l'article R. 741-2 du code de justice administrative, s'impose à peine de nullité ; que l'arrêt attaqué, qui mentionne par erreur le nom du rapporteur public pour dénommer le mandataire qui a présenté des observations pour les requérantes lors de l'audience devant la cour administrative d'appel, sans qu'aucune autre mention ne permette d'identifier ce mandataire, ne permet pas de vérifier que celui-ci avait qualité pour présenter ces observations et n'apporte pas par lui-même la preuve de sa régularité ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme D...et Mme A...sont fondées à en demander l'annulation ;<br/>
<br/>
              3. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              Sur la régularité des convocations des membres du conseil municipal : <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 2121-10 du code général des collectivités territoriales, dans sa rédaction applicable à la date des délibérations attaquées : " Toute convocation est faite par le maire. Elle indique les questions posées à l'ordre du jour. Elle est mentionnée au registre des délibérations, affichée ou publiée. Elle est adressée par écrit, sous quelque forme que ce soit, au domicile des conseillers municipaux, sauf s'ils font le choix d'une autre adresse " ; qu'aux termes de l'article L. 2121-12 du même code : " Dans les communes de 3 500 habitants et plus, une note explicative de synthèse sur les affaires soumises à délibération doit être adressée avec la convocation aux membres du conseil municipal. / (...) / Le délai de convocation est fixé à cinq jours francs (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que les convocations aux réunions du conseil municipal, accompagnées des notes explicatives de synthèse, doivent être envoyées aux conseillers municipaux à leur domicile personnel, sauf s'ils ont expressément fait le choix d'un envoi à une autre adresse, laquelle peut être la mairie, et qu'il doit être procédé à cet envoi en respectant un délai de cinq jours francs avant la réunion ; <br/>
<br/>
              6. Considérant, d'une part, qu'il ressort des mentions du registre des délibérations du conseil municipal de la commune de Massy que la convocation aux séances des 31 mai 2007 et 2 avril 2009 a été adressée aux conseillers municipaux respectivement les 25 mai 2007 et 27 mars 2009, soit dans le respect du délai de cinq jours francs prévu par l'article L. 2121-12 du code général des collectivités territoriales ; que si les requérantes contestent que les convocations aient été faites dans les délais légaux, elles n'assortissent leurs allégations d'aucun élément circonstancié ; que par suite, ces allégations ne sauraient conduire à remettre en cause les mentions factuelles précises du registre des délibérations, qui, au demeurant, font foi jusqu'à preuve contraire ; qu'elles ne sont, dès lors, pas fondées à soutenir que le délai prévu par l'article L. 2121-12 aurait été méconnu ; qu'en outre si les " lettres circulaires " de convocation produites par la commune ne mentionnent pas le nom et l'adresse de chacun des élus, cette seule circonstance ne saurait suffire à caractériser une méconnaissance des dispositions de l'article L. 2121-10 du code général des collectivités territoriales ; <br/>
<br/>
              7. Considérant, d'autre part, qu'en réponse à une mesure d'instruction diligentée par la cour, la commune a produit, outre les deux " lettres circulaires " mentionnées au point précédent, l'ordre du jour des deux séances du conseil municipal et deux documents se présentant comme les " notes " des séances du conseil municipal du 31 mai 2007 et du 2 avril 2009 ; que si ces documents se présentent comme des avant-projets de délibération réservant, point par point, la décision à prendre par le conseil municipal, ils comportent, pour chacun des points inscrits à l'ordre du jour dont, pour la première séance, " l'approbation du lancement de la procédure de cession du terrain sis 80 rue Marx Dormoy " et, pour la seconde, " l'approbation de l'acte de scission de la copropriété du 80 rue Marx Dormoy et des 8/10 chemin des Sablons ", un exposé du maire résumant l'objet, les motifs et le cadre juridique des délibérations soumises au conseil municipal ; que ces documents, dont les requérantes ne contestent l'envoi effectif aux conseillers municipaux par aucun élément suffisamment étayé, doivent être regardés  comme des notes explicatives, jointes aux convocations conformément aux prévisions de l'article L. 2121-12 du code général des collectivités territoriales ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que les requérantes ne sont pas fondées à soutenir que les membres du conseil municipal de Massy ont été irrégulièrement convoqués aux séances des 31 mai 2007 et 2 avril 2009 au cours desquelles ont été adoptées les délibérations litigieuses ;  <br/>
<br/>
              Sur le caractère suffisant de l'information donnée aux conseillers municipaux : <br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 2121-13 du code général des collectivités territoriales : " Tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération " ; <br/>
<br/>
              10. Considérant, en premier lieu, que la délibération du 31 mai 2007 avait pour objet d'autoriser les procédures préalables à la cession de la parcelle cadastrée I 568, située 80 rue Marx Dormoy à Massy, et, à cet effet, d'une part, d'autoriser le maire à effectuer les démarches requises pour que la copropriété horizontale à laquelle appartenait ce terrain soit scindée et, d'autre part, d'approuver " la purge du droit de rétrocession " à l'égard des anciens propriétaires de l'immeuble inachevé qui y avait été édifié et qui constituait lui-même une copropriété verticale, c'est-à-dire d'autoriser le maire à informer ces anciens propriétaires de la décision d'aliéner afin de leur permettre d'exercer leur droit de rétrocession, en application des articles L. 12-6 et R. 12-6 du code de l'expropriation pour cause d'utilité publique, alors en vigueur ; <br/>
<br/>
              11. Considérant, d'une part, que les requérantes ne peuvent utilement se prévaloir, pour contester la régularité de la délibération du conseil municipal, des dispositions de l'article 28 de la loi du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis, qui sont applicables aux assemblées générales des syndicats de copropriété ; qu'en tout état de cause, à la date de cette première délibération, l'assemblée générale du syndicat de la copropriété horizontale, qui n'avait pas encore été saisie, ne pouvait avoir fixé aucune condition matérielle, juridique ou financière à la scission, dont le conseil municipal aurait dû être informé ; <br/>
<br/>
              12. Considérant, d'autre part, que la circonstance qu'il n'a pas été rappelé aux conseillers municipaux que la construction édifiée par l'ancien propriétaire du terrain empiète sur la parcelle voisine des requérantes, n'est pas de nature à caractériser, compte tenu de l'objet de la délibération attaquée et alors que la commune de Massy avait elle-même versé aux requérantes l'indemnisation qui leur était due par l'ancien propriétaire, un défaut d'information des conseillers municipaux susceptible d'entacher la légalité de la délibération attaquée ;<br/>
<br/>
              13. Considérant, en second lieu, que la délibération du 2 avril 2009 avait pour objet d'autoriser le maire de la commune à signer l'acte de scission de la copropriété horizontale ; que le conseil municipal a été dûment informé de l'approbation donnée à cet effet par l'assemblée générale de cette copropriété réunie le 27 juin 2007 ; qu'il ne ressort pas des pièces du dossier que l'assemblée générale aurait subordonné son approbation à des conditions matérielles, juridiques ou financières dont le conseil municipal n'aurait pas été informé ; <br/>
<br/>
              14. Considérant qu'en mentionnant que le lot n° 5 est lui-même subdivisé en lots 101 à 160, la délibération du 2 avril 2009 s'est bornée à rappeler la configuration particulière de la parcelle dont le conseil municipal était dûment informé, celle-ci constituant l'un des lots d'une copropriété horizontale et supportant un immeuble qui avait lui-même constitué une copropriété verticale avant son acquisition par la commune ; que c'est ainsi à tort que les requérantes affirment qu'une opération de division foncière n'aurait pas été portée à la connaissance du conseil municipal ;<br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède que les requérantes ne sont pas fondées à soutenir que les délibérations litigieuses ont été adoptées dans des conditions méconnaissant l'article L. 2121-13 du code général des collectivités territoriales ;<br/>
<br/>
              Sur la légalité interne des délibérations attaquées : <br/>
<br/>
              16. Considérant que la circonstance, rappelée au point 12,  que la construction édifiée par l'ancien propriétaire de la parcelle empiète sur la parcelle voisine des requérantes n'est pas de nature à révéler l'existence d'une erreur manifeste d'appréciation ; <br/>
<br/>
              17. Considérant que les allégations des requérantes selon lesquelles les délibérations litigieuses ont été prises en méconnaissance des dispositions du code de l'urbanisme ne sont assorties d'aucune précision permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les fins de non-recevoir opposées par la commune aux demandes de première instance, que Mme D...et Mme A...ne sont pas fondées à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté les demandes tendant à l'annulation des délibérations des 31 mai 2007 et 2 avril 2009 du conseil municipal de Massy ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              19. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme D...et de Mme A...une somme de 1 500 euros chacune à verser à la commune de Massy au titre de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Massy, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 30 décembre 2014 est annulé.<br/>
Article 2 : La requête présentée par Mme D...et Mme A...devant la cour administrative d'appel de Versailles est rejetée.<br/>
Article 3 : Mme D...et Mme A...verseront une somme de 1 500 euros chacune à la commune de Massy au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de Mme D...et Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme C...D..., à Mme E...A...née D...et à la commune de Massy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-01-01-01 Collectivités territoriales. Commune. Organisation de la commune. Organes de la commune. Conseil municipal. Fonctionnement. Convocation.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-04 Procédure. Instruction. Preuve.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
