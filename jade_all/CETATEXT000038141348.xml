<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038141348</ID>
<ANCIEN_ID>JG_L_2019_02_000000416043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/14/13/CETATEXT000038141348.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/02/2019, 416043</TITRE>
<DATE_DEC>2019-02-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416043.20190218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Melun :<br/>
              - d'annuler les décisions implicites par lesquelles le président du conseil départemental du Val-de-Marne a rejeté ses recours administratifs formés contre la décision de la caisse d'allocations familiales du Val-de-Marne du 19 novembre 2014 de récupérer un trop-perçu de revenu de solidarité active pour la période du 1er juillet 2013 au 31 octobre 2014, ainsi que contre les décisions de la même caisse du 2 décembre 2014 de mettre fin à son droit au revenu de solidarité active et de récupérer un trop-perçu de 1 530,96 euros pour la période du 1er octobre au 1er décembre 2014 ;<br/>
              - d'enjoindre au département du Val-de-Marne de le rétablir rétroactivement dans ses droits au revenu de solidarité active ;<br/>
              - de prononcer la décharge des sommes restant à payer au titre de ces indus ;<br/>
              - de lui accorder la remise totale de cette dette.<br/>
<br/>
              Par un jugement n° 1603327 du 28 septembre 2017, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et par un nouveau mémoire, enregistrés les 28 novembre 2017 et 17 janvier 2018 au secrétariat du contentieux du Conseil d'État, M. B...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département du Val-de-Marne la somme de 3 000 euros à verser à la SCP Lesourd, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après un contrôle de sa situation par la caisse d'allocations familiales du Val-de-Marne, M.B..., allocataire du revenu de solidarité active depuis le 1er juin 2011, a été informé par une lettre du 19 novembre 2014 de la décision de cette caisse de récupérer un trop-perçu d'un montant de 9 036,84 euros pour la période du 1er juillet 2013 au 31 octobre 2014, ainsi que, par deux lettres de la même caisse du 2 décembre 2014, des décisions de récupérer un autre trop-perçu de 1 530,96 euros pour la période du 1er octobre au 1er décembre 2014 et de mettre fin à son droit au revenu de solidarité active. Par un jugement du 28 septembre 2017, contre lequel M. B... se pourvoit en cassation, le tribunal administratif de Melun a rejeté sa demande tendant à l'annulation des décisions implicites de rejet nées du silence gardé par le président du conseil départemental du Val-de-Marne sur ses recours administratifs formés contre la décision du 19 novembre 2014 et les deux décisions du 2 décembre 2014.<br/>
<br/>
              Sur le jugement en tant qu'il statue sur les décisions de récupération d'indu :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 262-16 du code de l'action sociale et des familles : " Le service du revenu de solidarité active est assuré, dans chaque département, par les caisses d'allocations familiales et, pour leurs ressortissants, par les caisses de mutualité sociale agricole ". Aux termes de l'article L. 262-40 de ce code, dans sa rédaction applicable au litige : " Pour l'exercice de leurs compétences, le président du conseil général, les représentants de l'Etat et les organismes chargés de l'instruction et du service du revenu de solidarité active demandent toutes les informations nécessaires à l'identification de la situation du foyer : / 1° Aux administrations publiques, et notamment aux administrations financières ; / 2° Aux collectivités territoriales ; / 3° Aux organismes de sécurité sociale, de retraite complémentaire et d'indemnisation du chômage ainsi qu'aux organismes publics ou privés concourant aux dispositifs d'insertion ou versant des rémunérations au titre de l'aide à l'emploi. / Les informations demandées, que ces administrations, collectivités et organismes sont tenus de communiquer, doivent être limitées aux données nécessaires à l'instruction du droit au revenu de solidarité active, à sa liquidation et à son contrôle ainsi qu'à la conduite des actions d'insertion. / (...) / Les organismes chargés de son versement réalisent les contrôles relatifs au revenu de solidarité active selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale. (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 114-19 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Le droit de communication permet d'obtenir, sans que s'y oppose le secret professionnel, les documents et informations nécessaires : / 1° Aux agents des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations servies par lesdits organismes (...) ". Aux termes de l'article L. 114-21 du même code : " L'organisme ayant usé du droit de communication en application de l'article L. 114-19 est tenu d'informer la personne physique ou morale à l'encontre de laquelle est prise la décision de supprimer le service d'une prestation ou de mettre des sommes en recouvrement, de la teneur et de l'origine des informations et documents obtenus auprès de tiers sur lesquels il s'est fondé pour prendre cette décision. Il communique, avant la mise en recouvrement ou la suppression du service de la prestation, une copie des documents susmentionnés à la personne qui en fait la demande ". <br/>
<br/>
              4. Il résulte de ces dispositions que les caisses d'allocations familiales et les caisses de mutualité sociale agricole, chargées du service du revenu de solidarité active, réalisent les contrôles relatifs à cette prestation d'aide sociale selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale, au nombre desquels figurent le droit de communication instauré par l'article L. 114-19 du code de la sécurité sociale au bénéfice des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations qu'ils servent, ainsi que les garanties procédurales qui s'attachent, en vertu de l'article L. 114-21 du même code, à l'exercice de ce droit par un organisme de sécurité sociale. Il incombe ainsi à l'organisme ayant usé du droit de communication, avant la suppression du service de la prestation ou la mise en recouvrement, d'informer l'allocataire à l'encontre duquel est prise la décision de supprimer le droit au revenu de solidarité active ou de récupérer un indu de revenu de solidarité active, tant de la teneur que de l'origine des renseignements qu'il a obtenus de tiers par l'exercice de son droit de communication et sur lesquels il s'est fondé pour prendre sa décision. Cette obligation a pour objet de permettre à l'allocataire, notamment, de discuter utilement leur provenance ou de demander que les documents qui, le cas échéant, contiennent ces renseignements soient mis à sa disposition avant la récupération de l'indu ou la suppression du service de la prestation, afin qu'il puisse vérifier l'authenticité de ces documents et en discuter la teneur ou la portée. Les dispositions de l'article L. 114-21 du code de la sécurité sociale instituent ainsi une garantie au profit de l'intéressé. Toutefois, la méconnaissance de ces dispositions par l'organisme demeure sans conséquence sur le bien-fondé de la décision prise s'il est établi qu'eu égard à la teneur du renseignement, nécessairement connu de l'allocataire, celui-ci n'a pas été privé, du seul fait de l'absence d'information sur l'origine du renseignement, de cette garantie.<br/>
<br/>
              5. Pour écarter le moyen de M. B...tiré de ce qu'il n'avait été informé ni de la teneur ni de l'origine des documents et informations obtenus par la caisse d'allocations familiales en vertu du droit de communication, le tribunal administratif de Melun a relevé que l'intéressé avait été informé au cours du contrôle de la mise en oeuvre du droit de communication par la caisse d'allocations familiales du Val-de-Marne, ainsi que de la teneur des informations qu'elle avait recueillies. En statuant ainsi, sans rechercher si la caisse avait également indiqué à M. B...l'origine des informations recueillies ou, à défaut, s'il était établi qu'eu égard à la teneur du renseignement, il n'avait pas été privé, du seul fait de l'absence d'information sur l'origine du renseignement, de la garantie instaurée par l'article L. 114-21 du code de la sécurité sociale, le tribunal a commis une erreur de droit. <br/>
<br/>
              6. Par suite, M. B...est fondé à demander l'annulation du jugement du tribunal administratif de Melun en tant qu'il rejette ses conclusions dirigées contre les décisions des 19 novembre et 2 décembre 2014 de récupération d'indus de revenu de solidarité active. Ce moyen suffisant à entraîner l'annulation du jugement dans cette mesure, il n'est pas nécessaire d'examiner les autres moyens du pourvoi dirigés contre la même partie du jugement attaqué.<br/>
<br/>
              Sur le jugement en tant qu'il statue sur la fin du droit au revenu de solidarité active :<br/>
<br/>
              7. Aux termes de l'article L. 262-41 du code de l'action sociale et des familles : " Lorsqu'il est constaté par le président du conseil départemental ou les organismes chargés de l'instruction des demandes ou du versement du revenu de solidarité active, à l'occasion de l'instruction d'une demande ou lors d'un contrôle, une disproportion marquée entre, d'une part, le train de vie du foyer et, d'autre part, les ressources qu'il déclare, une évaluation forfaitaire des éléments de train de vie, hors patrimoine professionnel dans la limite d'un plafond fixé par décret, est effectuée. Cette évaluation forfaitaire est prise en compte pour la détermination du droit au revenu de solidarité active. / Les éléments de train de vie à prendre en compte, qui comprennent notamment le patrimoine mobilier ou immobilier, hors patrimoine professionnel dans la limite d'un plafond fixé par décret, sont ceux dont le foyer a disposé au cours de la période correspondant à la déclaration de ses ressources, en quelque lieu que ce soit, en France ou à l'étranger, et à quelque titre que ce soit ". Les dispositions des articles R. 262-74 et suivants du même code précisent les éléments à prendre en considération, la procédure à suivre et le seuil à partir duquel une disproportion marquée peut être constatée. <br/>
<br/>
              8. Ces dispositions sont seules applicables lorsque, constatant une disproportion marquée entre le train de vie et les ressources déclarées par un demandeur ou un bénéficiaire du revenu de solidarité active, le président du conseil départemental ou les organismes chargés de l'instruction des demandes ou du versement de l'allocation, entendent déterminer  son droit au revenu de solidarité active en fonction des éléments de train de vie de son foyer. Elles ne font pas obstacle, lorsqu'un demandeur ou un bénéficiaire du revenu de solidarité active s'est rendu coupable de fraude ou de fausse déclaration et que l'autorité administrative est, en outre, en mesure d'établir qu'il ne peut prétendre au bénéfice de l'allocation ou qu'il n'est pas possible, même après avoir usé du droit de communication, faute de connaître le montant exact des ressources des personnes composant le foyer, de déterminer s'il pouvait ou non bénéficier de l'allocation pour la période en cause, à ce qu'elle mettre fin à cette prestation et, sous réserve des délais de prescription, décide de récupérer les sommes qui ont ainsi été indûment versées à l'intéressé. <br/>
<br/>
              9. Le tribunal administratif de Melun a relevé, au terme d'une appréciation souveraine exempte de dénaturation, que M. B...n'avait déclaré ni sa qualité de gérant d'une société ni certaines sommes versées sur son compte bancaire et qu'il n'était pas possible, faute de connaître le montant exact des ressources des personnes composant le foyer, de déterminer s'il pouvait bénéficier du revenu de solidarité active. Il résulte de ce qui précède qu'il n'a pas commis d'erreur de droit en en déduisant que la caisse d'allocations familiales était en droit de mettre fin au droit de M. B...au revenu de solidarité active. <br/>
<br/>
              10. Par suite, M. B...n'est pas fondé à demander l'annulation du jugement du tribunal administratif de Melun en tant qu'il statue sur la décision du 2 décembre 2014 mettant fin à son droit au revenu de solidarité active.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              11.	M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Lesourd, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département du Val-de-Marne une somme de 1 500 euros à verser à cette SCP.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 28 septembre 2017 est annulé en tant qu'il rejette les conclusions de M. B...relatives aux décisions des 19 novembre et 2 décembre 2014 de récupération d'indus de revenu de solidarité active.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Melun dans la mesure de la cassation prononcée.<br/>
Article 3 : Le département du Val-de-Marne versera à la SCP Lesourd, avocat de M.B..., une somme de 1 500 euros au titre des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au département du Val-de-Marne.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - DROIT DE COMMUNICATION OUVERT AUX ORGANISMES CHARGÉS DU SERVICE DE L'ALLOCATION (ART. L. 114-19 DU CSS) [RJ1] - MÉCONNAISSANCE DE L'OBLIGATION D'INFORMER L'ALLOCATAIRE DE LA TENEUR ET DE L'ORIGINE DES RENSEIGNEMENTS UTILISÉS (ART. L. 114-21 DU CSS) - ABSENCE DE CONSÉQUENCE LORSQUE L'ALLOCATAIRE, CONNAISSANT NÉCESSAIREMENT LE RENSEIGNEMENT, N'A PAS ÉTÉ PRIVÉ D'UNE GARANTIE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - POUVOIRS DES ORGANISMES CHARGÉS DU SERVICE DE L'ALLOCATION - 1) DISPROPORTION MARQUÉE ENTRE LE TRAIN DE VIE ET LES RESSOURCES DÉCLARÉES - EVALUATION FORFAITAIRE DES ÉLÉMENTS DE TRAIN DE VIE (ART. L. 262-41 DU CASF) - 2) FRAUDE OU FAUSSE DÉCLARATION, D'UNE PART, ET IMPOSSIBILITÉ POUR L'INTÉRESSÉ DE PRÉTENDRE À L'ALLOCATION OU IMPOSSIBILITÉ DE DÉTERMINER SI L'INTÉRESSÉ POUVAIT OU NON Y PRÉTENDRE, MÊME APRÈS AVOIR USÉ DU DROIT DE COMMUNICATION, FAUTE DE CONNAÎTRE LE MONTANT EXACT DES RESSOURCES, D'AUTRE PART - FIN DU SERVICE DE LA PRESTATION ET, SOUS RÉSERVE DE LA PRESCRIPTION, RÉPÉTITION DE L'INDU [RJ3].
</SCT>
<ANA ID="9A"> 04-02-06 Il résulte des articles L. 262-16 et L. 262-40 du code de l'action sociale et des familles (CASF) et L. 114-19 et L. 114-21 du code de la sécurité sociale (CSS) que les caisses d'allocations familiales (CAF) et les caisses de mutualité sociale agricole (MSA), chargées du service du revenu de solidarité active (RSA), réalisent les contrôles relatifs à cette prestation d'aide sociale selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale, au nombre desquels figurent le droit de communication instauré par l'article L. 114-19 du CSS au bénéfice des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations qu'ils servent, ainsi que les garanties procédurales qui s'attachent, en vertu de l'article L. 114-21 du même code, à l'exercice de ce droit par un organisme de sécurité sociale.... ...Il incombe ainsi à l'organisme ayant usé du droit de communication, avant la suppression du service de la prestation ou la mise en recouvrement, d'informer l'allocataire à l'encontre duquel est prise la décision de supprimer le droit au RSA ou de récupérer un indu de RSA tant de la teneur que de l'origine des renseignements qu'il a obtenus de tiers par l'exercice de son droit de communication et sur lesquels il s'est fondé pour prendre sa décision. Cette obligation a pour objet de permettre à l'allocataire, notamment, de discuter utilement leur provenance ou de demander que les documents qui, le cas échéant, contiennent ces renseignements soient mis à sa disposition avant la récupération de l'indu ou la suppression du service de la prestation, afin qu'il puisse vérifier l'authenticité de ces documents et en discuter la teneur ou la portée. L'article L. 114-21 du CSS institue ainsi une garantie au profit de l'intéressé.... ...Toutefois, la méconnaissance de ces dispositions par l'organisme demeure sans conséquence sur le bien-fondé de la décision prise s'il est établi qu'eu égard à la teneur du renseignement, nécessairement connu de l'allocataire, celui-ci n'a pas été privé, du seul fait de l'absence d'information sur l'origine du renseignement, de cette garantie.</ANA>
<ANA ID="9B"> 04-02-06 1) Les articles L. 262-41 et R. 262-74 du code de l'action sociale et des familles (CASF) sont seuls applicables lorsque, constatant une disproportion marquée entre le train de vie et les ressources déclarées par un demandeur ou un bénéficiaire du revenu de solidarité active (RSA), le président du conseil départemental ou les organismes chargés de l'instruction des demandes ou du versement de l'allocation, entendent déterminer son droit au RSA en fonction des éléments de train de vie de son foyer.... ...2) Elles ne font pas obstacle, lorsqu'un demandeur ou un bénéficiaire du RSA s'est rendu coupable de fraude ou de fausse déclaration et que l'autorité administrative est, en outre, en mesure d'établir qu'il ne peut prétendre au bénéfice de l'allocation ou qu'il n'est pas possible, même après avoir usé du droit de communication, faute de connaître le montant exact des ressources des personnes composant le foyer, de déterminer s'il pouvait ou non bénéficier de l'allocation pour la période en cause, à ce qu'elle mette fin à cette prestation et, sous réserve des délais de prescription, décide de récupérer les sommes qui ont ainsi été indûment versées à l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur son application au RSA, CE, 20 juin 2018, Département des Bouches du Rhône, n° 409189 409193, à mentionner aux Tables.,,[RJ2] Rappr., en matière fiscale (art. L. 76 B du LPF), CE, 17 mars 2016, Ministre des finances et des comptes publics c/ M. Monsterleet, n° 381908, p. 75.,,[RJ3] Cf., en précisant, CE, 14 mars 2003, M.,, n° 246873, p. 123 ; CE, 31 mars 2017, Département de la Moselle c/ M.,, n° 395646, p. 114.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
