<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030788038</ID>
<ANCIEN_ID>JG_L_2015_06_000000389124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/80/CETATEXT000030788038.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 26/06/2015, 389124</TITRE>
<DATE_DEC>2015-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:389124.20150626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Am'Tech Médical a demandé au juge des référés du tribunal administratif de Paris l'annulation, sur le fondement de l'article L. 551-1 du code de justice administrative, de la procédure de passation des lots n°s 1, 2 et 3 d'un marché public lancé par l'Assistance Publique-Hôpitaux de Paris (AP-HP) ayant pour objet des prestations de contrôle de qualité externe d'équipements d'imagerie et de radiothérapie. <br/>
<br/>
              Par une ordonnance n° 1503139/3/5 du 16 mars 2015, le tribunal administratif de Paris a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 31 mars, 15 avril et 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, l'AP-HP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la société Am'Tech Médical le versement de la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Assistance Publique-Hôpitaux de Paris, et à la SCP Ortscheidt, avocat de la société Am'Tech Médical ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public. / Le juge est saisi avant la conclusion du contrat. " ; qu'aux termes de l'article L. 551-2 de ce code : " I. - Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que l'Assistance Publique-Hôpitaux de Paris (AP-HP) a lancé en août 2014 une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché public de services ayant pour objet des prestations de contrôle de qualité externe d'équipements d'imagerie et de radiothérapie ; que le cahier des clauses techniques particulières du marché prévoyait que, pour les lots n°s 1 à 3, les offres étaient sélectionnées en fonction d'un critère relatif à la qualité de la prestation, pondéré à hauteur de 60 %, et d'un critère relatif au prix, pondéré à hauteur de 40 % ; qu'en application de ce même cahier, le critère relatif à la qualité des prestations a été évalué à partir de réponses fournies par les candidats dans un document intitulé " cadre de réponse technique " joint à leur offre et à partir d'une composante de leur offre, consistant en l'accomplissement d'un essai de contrôle de qualité externe sur des équipements de l'AP-HP ainsi que du rapport d'analyse établi par les candidats après l'accomplissement de cet essai ; que la société Am'Tech Médical, dont les offres présentées  pour les lots n°s 1 à 3 ont été rejetées, a saisi le juge des référés du tribunal administratif de Paris d'une requête tendant à l'annulation de la procédure de passation des ces trois lots ; que, par une ordonnance du 16 mars 2015, le juge des référés a fait droit à la demande de la société ;<br/>
<br/>
              3. Considérant que, pour annuler la procédure litigieuse, le juge des référés a jugé que l'obligation imposée aux candidats par le pouvoir adjudicateur d'accomplir, dans le cadre de la présentation de leur offre, et sous le contrôle du pouvoir adjudicateur, un essai des prestations faisant l'objet du marché, afin de permettre l'évaluation de la qualité technique de leur offre, n'était autorisée par aucune disposition du code des marchés publics relative aux appels d'offres ouverts ; qu'en jugeant que l'AP-HP avait ainsi manqué à ses obligations de publicité et de mise en concurrence, sans rechercher si l'obligation qu'elle avait prévue était prohibée par une disposition du code des marchés publics ou les principes de la commande publique, le juge des référés a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'AP-HP est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Am'Tech Médical ;<br/>
<br/>
              5. Considérant, en premier lieu, que l'article 49 du code des marchés publics dispose : " le pouvoir adjudicateur peut exiger que les offres soient accompagnées d'échantillons, de maquettes ou de prototypes concernant l'objet du marché " et qu'aux termes du I de l'article 59 du code des marchés publics : " Il ne peut y avoir négociation avec les candidats. Il est seulement possible de demander aux candidats de préciser ou de compléter la teneur de leur offre " ; que ni les dispositions de l'article 49, ni aucune autre disposition ou principe n'interdisaient à l'AP-HP d'exiger des candidats la réalisation d'essais dans le cadre de la présentation de leur offre ; qu'il ne résulte pas de l'instruction et qu'il n'est d'ailleurs pas allégué par la société requérante que les essais auxquels ont été soumis les candidats auraient donné lieu à une négociation avec le pouvoir adjudicateur et à une modification de leur offre, en méconnaissance des dispositions du I de l'article 59 du code des marchés publics ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il ne résulte pas de l'instruction que la confidentialité des offres présentées par les différents candidats aurait été violée par l'organisation des essais, lesquels se sont déroulés séparément pour chacun des candidats sans qu'un candidat ne puisse connaître la teneur de l'offre de ses concurrents ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'aux termes de l'article R. 5212-29 du code de la santé publique : " Le contrôle de qualité externe des dispositifs médicaux est réalisé par des organismes agréés à cet effet par décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (...) L'agrément est accordé pour une durée de cinq ans renouvelable, en fonction des garanties d'indépendance et de compétence présentées, de l'expérience acquise dans le domaine considéré et des moyens dont l'organisme dispose pour exécuter les tâches pour lesquelles il est agréé " ; que l'agrément dont doivent être titulaires les candidats au marché en cause en vertu des dispositions mentionnées ci-dessus, ne saurait faire obstacle à ce que le pouvoir adjudicateur les soumette, en vue de la sélection des offres, à un essai ayant pour objet d'apprécier la qualité de leur prestation ; <br/>
<br/>
              8. Considérant, en quatrième lieu, qu'il résulte de l'instruction que le courrier de notification du rejet de son offre à la société requérante indique, pour l'évaluation des essais,  le nombre de points attribués, d'une part à l'essai lui-même, d'autre part, aux réponses au questionnaire remis au candidat à l'occasion de cet essai ; que le pouvoir adjudicateur s'est ainsi borné à détailler la méthode de notation adoptée pour l'évaluation des essais ; que la société requérante ne peut prétendre que ce courrier révèlerait une pondération de sous-critères portant sur les essais et les questionnaires qui n'aurait pas été porté à sa connaissance dans les documents de la consultation ; <br/>
<br/>
              9. Considérant, en cinquième lieu, qu'il ne résulte pas de l'instruction que la société requérante n'aurait pas bénéficié du délai de cinq jours prévu par les documents de la consultation pour la rédaction des rapports d'analyse des essais et que le principe d'égalité de traitement des candidats aurait ainsi été méconnu ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la demande présentée par la société Am'Tech Médical devant le  juge des référés du tribunal administratif de Paris ne peut qu'être rejetée ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que le versement d'une somme soit mis à la charge de l'AP-HP qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, au titre des mêmes dispositions, de mettre à la charge de la société Am'Tech Médical le versement à l'AP-HP de la somme de 4 500 euros au titre des frais exposés par cette dernière et non compris dans les dépens pour la procédure suivie devant le Conseil d'Etat et le tribunal administratif de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 16 mars 2015 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 2 : La demande présentée par la société Am'Tech Médical devant le juge des référés du tribunal administratif de Paris et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Am'Tech Médical versera une somme de 4 500 euros à l'Assistance Publique-Hôpitaux de Paris au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Assistance Publique-Hôpitaux de Paris (AP-HP) et à la société Am'Tech Médical. <br/>
Copie en sera transmise pour information à la société Apave Parisienne, à la société Cibio Médical et la société Medi-Qual.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - OBLIGATION SPÉCIFIQUE IMPOSÉE PAR LE POUVOIR ADJUDICATEUR AUX CANDIDATS POUR LA PRÉSENTATION DE LEUR OFFRE - 1) CONFORMITÉ AUX RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE - CRITÈRE - OBLIGATION EXPRESSÉMENT AUTORISÉE PAR LE CODE DES MARCHÉS PUBLICS - ABSENCE - OBLIGATION NON PROHIBÉE PAR LE CODE OU PAR LES PRINCIPES DE LA COMMANDE PUBLIQUE - EXISTENCE - 2) ESPÈCE - RÉALISATION D'ESSAIS - LÉGALITÉ - EXISTENCE - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. APPEL D'OFFRES. - OBLIGATION SPÉCIFIQUE IMPOSÉE PAR LE POUVOIR ADJUDICATEUR AUX CANDIDATS POUR LA PRÉSENTATION DE LEUR OFFRE - 1) CONFORMITÉ AUX RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE - CRITÈRE - OBLIGATION EXPRESSÉMENT AUTORISÉE PAR LE CODE DES MARCHÉS PUBLICS - ABSENCE - OBLIGATION NON PROHIBÉE PAR LE CODE OU PAR LES PRINCIPES DE LA COMMANDE PUBLIQUE - EXISTENCE - 2) ESPÈCE - RÉALISATION D'ESSAIS - LÉGALITÉ - EXISTENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 39-02-005 1)  Pouvoir adjudicateur imposant une obligation spécifique aux candidats pour la présentation de leur offre. Pour en apprécier la conformité aux règles de publicité et de mise en concurrence, il convient de rechercher, non pas si cette obligation spécifique est expressément autorisée par une disposition  du code des marchés publics, mais si elle est prohibée par une disposition du même code ou par les principes de la commande publique.,,,2) En l'espèce, pouvoir adjudicateur ayant imposé aux candidats d'accomplir, dans le cadre de la présentation de leur offre, et sous son contrôle, un essai des prestations faisant l'objet du marché, afin de permettre l'évaluation de la qualité technique de leur offre. Ni les dispositions de l'article 49 du code des marchés publics, ni aucune autre disposition ou principe n'interdisaient une telle exigence, dès lors que les essais réalisés n'ont donné lieu ni à une négociation avec le pouvoir adjudicateur ni à une modification de leur offre en méconnaissance des dispositions du I de l'article 59 du même code.</ANA>
<ANA ID="9B"> 39-02-02-03 1)  Pouvoir adjudicateur imposant une obligation spécifique aux candidats pour la présentation de leur offre. Pour en apprécier la conformité aux règles de publicité et de mise en concurrence, il convient de rechercher, non pas si cette obligation spécifique est expressément autorisée par une disposition  du code des marchés publics, mais si elle est prohibée par une disposition du même code ou par les principes de la commande publique.,,,2) En l'espèce, pouvoir adjudicateur ayant imposé aux candidats d'accomplir, dans le cadre de la présentation de leur offre, et sous son contrôle, un essai des prestations faisant l'objet du marché, afin de permettre l'évaluation de la qualité technique de leur offre. Ni les dispositions de l'article 49 du code des marchés publics, ni aucune autre disposition ou principe n'interdisaient une telle exigence, dès lors que les essais réalisés n'ont donné lieu ni à une négociation avec le pouvoir adjudicateur ni à une modification de leur offre en méconnaissance des dispositions du I de l'article 59 du même code.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
