<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244604</ID>
<ANCIEN_ID>JG_L_2019_03_000000410628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244604.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 18/03/2019, 410628, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:410628.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 17 mai et 17 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'association UFC-Que Choisir demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 17 mars 2017 par laquelle le comité de règlement des différends et des sanctions (CoRDiS) de la Commission de régulation de l'énergie (CRE) a refusé de donner suite à la demande de sanction qu'elle avait formée à l'encontre de la société ENEDIS ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de l'association UFC-Que Choisir ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 134-25 du code de l'énergie : " Le comité de règlement des différends et des sanctions peut soit d'office, soit à la demande (...) d'une association agréée d'utilisateurs (...), sanctionner les manquements mentionnés aux titres Ier et II du présent livre et aux livres III et IV qu'il constate de la part des gestionnaires de réseaux publics de transport ou de distribution d'électricité (...), dans les conditions fixées aux articles suivants ". Aux termes du premier alinéa de l'article R. 134-30 du même code : " Pour chaque affaire, le président du comité de règlement des différends et des sanctions désigne un membre de ce comité chargé, avec le concours des agents de la Commission de régulation de l'énergie, de l'instruction. Le cas échéant, ce membre adresse la mise en demeure prévue à l'article L. 134-26 et notifie les griefs. Il peut ne pas donner suite à la saisine ". <br/>
<br/>
              2. Il appartient au comité de règlement des différends et des sanctions de la Commission de régulation de l'énergie, investi par les dispositions de l'article L. 134-25 du code de l'énergie d'un pouvoir de sanction qu'il peut exercer de sa propre initiative ou à la suite d'une plainte, de décider, lorsqu'il est saisi par un tiers de faits de nature à motiver la mise en oeuvre de ce pouvoir, et après avoir procédé à leur examen, des suites à donner à la plainte. Il dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de la gravité des manquements allégués au regard de la législation ou de la réglementation que la Commission est chargée de faire appliquer, du sérieux des indices relatifs à ces faits, de la date à laquelle ils ont été commis, du contexte dans lequel ils l'ont été et, plus généralement, de l'ensemble des intérêts généraux dont la Commission a la charge. La décision que prend le comité, ou le cas échéant celui de ses membres qui a été chargé de l'instruction de l'affaire en application de l'article R. 134-30 du code de l'énergie, lorsqu'il refuse de donner suite à une saisine, a le caractère d'une décision administrative que le juge de l'excès de pouvoir peut censurer en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.<br/>
<br/>
              3. Par lettre du 25 juin 2014, l'association UFC-Que Choisir a saisi le CoRDiS d'une demande de sanction à l'encontre de la société Electricité Réseau Distribution France (ERDF), désormais dénommée ENEDIS, en application de l'article L. 134-25 du code de l'énergie. Elle demande l'annulation pour excès de pouvoir de la décision du 17 mars 2017 par laquelle le membre désigné par le président du CoRDiS en application de l'article R. 134-30 du code de l'énergie a refusé de donner suite à cette demande. <br/>
<br/>
              4. Il ressort, en premier lieu, des pièces du dossier que, par une décision du 29 février 2016, le président du CoRDiS a régulièrement désigné M. A...pour instruire la demande de l'association UFC-Que Choisir. Par suite, le moyen tiré de l'incompétence de l'auteur de la décision attaquée ne peut qu'être écarté.<br/>
<br/>
              5. En deuxième lieu, l'association requérante soutient, en se prévalant des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que le membre du CoRDiS qui avait instruit sa demande de sanction ne pouvait, sans entacher sa décision d'un défaut d'impartialité, se prononcer lui-même sur les suites à y donner. Toutefois, la décision par laquelle le membre désigné dans les conditions prévues à l'article R. 134-30 du code de l'énergie décide, au vu de l'instruction, qu'il n'y a pas lieu à mise en demeure ou à notification de griefs ne présente pas le caractère d'une sanction et ne peut conduire au prononcé d'une sanction. Dès lors, les stipulations de l'article 6 de la convention ne peuvent être utilement invoquées à son encontre. Le moyen soulevé ne peut donc qu'être écarté comme inopérant. Au demeurant, et en tout état de cause, la circonstance que ce soit la même personne qui, après avoir été chargée de l'instruction de la plainte, décide, au vu de cette instruction, qu'il n'y a pas lieu de donner suite à la saisine ne saurait, par elle-même, traduire un manquement à l'impartialité.<br/>
<br/>
              6. En troisième lieu, il ressort des pièces du dossier que l'association UFC-Que Choisir a demandé au CoRDiS de mettre la société ENEDIS en demeure de modifier ses statuts afin que l'interdiction qu'ils imposent aux membres du directoire d'exercer une responsabilité directe ou indirecte dans la gestion d'activités de production ou de fourniture d'électricité soit étendue à l'ensemble des personnels exerçant une fonction de direction au sein de la société. <br/>
<br/>
              7. Toutefois, si l'article L. 111-66 du code de l'énergie, qui transpose en droit interne les objectifs de l'article 26, paragraphe 2, sous a) de la directive du 13 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité, prévoit que : " Les responsables de la gestion de la société gestionnaire d'un réseau de distribution ne peuvent avoir de responsabilité directe ou indirecte dans la gestion d'activités de production ou de fourniture d'électricité ou de gaz ", ces dispositions n'imposent pas aux sociétés gestionnaires de réseaux de distribution d'inclure dans leurs statuts une clause réitérant l'interdiction de cumul qu'elles énoncent entre la responsabilité de la gestion des activités de distribution et la gestion des activités de production ou de fourniture. Une telle obligation ne découle pas davantage de l'article L. 111-65 du code de l'énergie, qui prévoit seulement que les statuts d'une société gestionnaire d'un réseau de distribution de gaz ou d'électricité doivent comporter des dispositions propres à concilier l'indépendance d'action des responsables de la gestion du réseau et la préservation des droits des actionnaires. Par suite, l'association requérante n'est pas fondée à soutenir que le membre désigné par le président du CoRDiS aurait commis une erreur de droit en estimant qu'aucun manquement aux obligations découlant de l'article L. 111-66 du code de l'énergie, interprété conformément à celles de l'article 26, paragraphe 2, sous a) de la directive du 13 juillet 2009, ne pouvait être reproché à ENEDIS à raison du contenu de ses statuts.<br/>
<br/>
              8. Il ressort, en quatrième lieu, des pièces du dossier que l'association UFC-Que Choisir a demandé au CoRDiS de sanctionner la politique de versement de dividendes de la société ENEDIS à sa société-mère EDF ainsi que la convention par laquelle elle a confié la gestion de sa trésorerie à la société EDF, au motif que ces pratiques seraient constitutives de subventions croisées. <br/>
<br/>
              9. Mais, d'une part, l'article 31, paragraphe 3 de la directive du 3 juillet 2009 concernant des règles communes pour le marché intérieur de l'électricité prévoit que, lorsque ces activités ne sont pas exercées par des entreprises distinctes, les entreprises d'électricité tiennent des comptes séparés pour chacune de leurs activités de transport et de distribution, en vue d'éviter les discriminations, les subventions croisées et les distorsions de concurrence. L'article 37, paragraphe 1, sous f) confie à l'autorité de régulation nationale la mission de " faire en sorte qu'il n'y ait pas de subventions croisées entre les activités de transport, de distribution et de fourniture ". Les articles L. 111-84 et L. 111-86 du code de l'énergie transposent ces objectifs en droit interne. Aux termes de l'article L. 111-84 du code de l'énergie, dans sa rédaction applicable à la date de la décision attaquée : " Electricité de France ainsi que les entreprises locales de distribution tiennent une comptabilité interne qui doit permettre de distinguer la fourniture aux consommateurs finals ayant exercé leur droit de choisir librement leur fournisseur et la fourniture aux consommateurs finals n'ayant pas exercé ce droit et d'identifier, s'il y a lieu, les revenus provenant de la propriété des réseaux publics de distribution. Lorsque la gestion des réseaux de distribution n'est pas assurée par une entité juridiquement distincte, ces opérateurs tiennent un compte séparé au titre de cette activité. ". Aux termes de l'article L. 111-86 du même code : " Les règles d'imputation, les périmètres comptables et les principes déterminant les relations financières entre les différentes activités, qui sont proposés par les opérateurs concernés pour mettre en oeuvre la séparation comptable prévue à l'article L. 111-84, ainsi que toute modification ultérieure de ces règles, périmètres ou principes sont approuvées par la Commission de régulation de l'énergie. La Commission de régulation de l'énergie veille à ce que ces règles, ces périmètres et ces principes ne permettent aucune discrimination, subvention croisée ou distorsion de concurrence ". Il résulte de l'ensemble de ces dispositions que l'interdiction des subventions croisées en tant que telle ne vaut que pour l'exercice simultané, au sein d'une entité unique, d'activités de transport, de distribution et de fourniture et trouve sa traduction dans les obligations de séparation comptable.<br/>
<br/>
              10. D'autre part, les pratiques de versement de dividendes et de gestion de sa trésorerie mises en oeuvre par la société ENEDIS sont sans lien avec l'obligation de séparation comptable entre l'activité de fourniture aux tarifs réglementés et celle de la fourniture à des conditions de marché qui s'impose à Electricité de France et aux entreprises locales de distribution en vertu de l'article L. 111-84 du code de l'énergie. Par ailleurs, l'obligation de tenir un compte séparé au titre de l'activité de gestion des réseaux de distribution prévue par cet article pour ces mêmes fournisseurs, lorsqu'une telle activité n'est pas exercée par une entreprise distincte, est sans objet à l'égard de la société ENEDIS, qui est une entité juridique distincte de la société EDF.<br/>
<br/>
              11. Il résulte, enfin, de l'article L. 134-25 du code de l'énergie cité au point 1 que le CoRDiS n'est compétent pour sanctionner, à la demande d'une association agréée d'utilisateurs, que les manquements mentionnés aux titres Ier et II du livre Ier et aux livres III et IV du code de l'énergie. L'association requérante n'est ainsi pas fondée à soutenir que le membre désigné par le président du CoRDiS aurait commis une erreur de droit en ne recherchant pas si les pratiques dénoncées constituaient un abus de position dominante prohibé par l'article L. 420-2 du code de commerce.<br/>
<br/>
              12. Par suite, l'association requérante, qui ne soutient pas que les pratiques dénoncées seraient constitutives d'un manquement à une autre disposition du code de l'énergie relevant de la compétence du CoRDiS, n'est pas fondée à soutenir que le membre désigné par le président du CoRDiS aurait commis une erreur de droit en refusant de donner suite à sa demande de sanction sur ce point.<br/>
<br/>
              13. En dernier lieu, l'article 26, paragraphe 2, sous c) de la directive du 13 juillet 2009 prévoit que le gestionnaire de réseau de distribution dispose des ressources nécessaires, tant humaines que techniques, matérielles et financières pour exécuter ses tâches. Aux termes du 1° de l'article L. 111-61 du code de l'énergie, le gestionnaire " assure l'exploitation, l'entretien et, sous réserve des prérogatives des collectivités et des établissements mentionnés au septième alinéa du I de l'article L. 2224-31 du code général des collectivités territoriales, le développement des réseaux de distribution d'électricité ou de gaz de manière indépendante vis-à-vis de tout intérêt dans des activités de production ou de fourniture d'électricité ou de gaz ". Ces dispositions n'interdisent pas à un gestionnaire de réseau, lorsqu'il est membre d'un groupe verticalement intégré, de recourir aux services de sa société-mère pourvu que le service lui soit fourni aux conditions de marché selon des modalités ne portant pas atteinte à son indépendance. <br/>
<br/>
              14. Or d'une part, il ressort des pièces du dossier que la convention du 8 avril 2009 par laquelle la société ENEDIS a confié la gestion de sa trésorerie à la société EDF interdit à cette dernière de s'ingérer dans la gestion et la disposition des fonds de sa filiale. En estimant que cette convention ne portait pas atteinte à l'indépendance de la société ENEDIS garantie par les dispositions précitées du 1° de l'article L. 111-61 du code de l'énergie aux motifs qu'elle a été conclue aux conditions du marché et que l'association requérante n'établissait pas que la société EDF donnerait des instructions à sa filiale dans le cadre de l'application de cette convention, le membre désigné par le président du CoRDiS ne s'est pas fondé sur des faits matériellement inexacts et n'a pas commis d'erreur de droit. <br/>
<br/>
              15. D'autre part, il ressort des termes non contestés de la décision attaquée que l'association requérante a mis en cause les conventions conclues dans le domaine des systèmes d'information au seul motif que la société ENEDIS recevait dans ce cadre des instructions de la part de la société EDF. Toutefois, la note de gouvernance du 14 février 2014, qui définit les modalités de prise de décision concernant les systèmes d'information au sein du groupe EDF ne s'applique pas aux filiales régulées du groupe telles que la société ENEDIS. Il s'ensuit qu'en refusant de donner suite à la demande de l'association requérante au motif que la société ENEDIS déterminait, depuis l'intervention de cette note, ses orientations en matière de systèmes d'information en toute indépendance, le membre désigné par le président du CoRDiS ne s'est pas fondé sur des faits matériellement inexacts et n'a pas entaché sa décision d'erreur de droit. <br/>
<br/>
              16. Il résulte de tout ce qui précède que l'association UFC-Que Choisir n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision qu'elle attaque et que sa requête, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association UFC Que Choisir est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association UFC Que Choisir et à la Commission de régulation de l'énergie. <br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - PORTÉE [RJ1] - INSTRUCTION DES PLAINTES DEVANT LE CORDIS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-06-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). CHAMP D'APPLICATION. - EXCLUSION - DÉCISION D'UN MEMBRE DU CORDIS, APRÈS AVOIR INSTRUIT UNE DEMANDE DE SANCTION, DE LA CLASSER SANS SUITE (ART. R. 134-30 DU CODE DE L'ÉNERGIE).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">29-01 ENERGIE. OPÉRATEURS. - 1) OBLIGATION POUR LES SOCIÉTÉS GESTIONNAIRES DE RÉSEAUX DE DISTRIBUTION D'INCLURE DANS LEURS STATUTS UNE CLAUSE INTERDISANT LE CUMUL ENTRE LA RESPONSABILITÉ DE LA GESTION DES ACTIVITÉS DE DISTRIBUTION ET LA GESTION DES ACTIVITÉS DE PRODUCTION OU DE FOURNITURE - ABSENCE - 2) INTERDICTION DES SUBVENTIONS CROISÉES (ART. L. 111-84 ET L. 111-86 DU CODE DE L'ÉNERGIE) - PORTÉE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">29-06-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. COMMISSION DE RÉGULATION DE L'ÉNERGIE. - 1) POUVOIR DE SANCTION DU CORDIS (ART. L. 134-25 DU CODE DE L'ÉNERGIE) - PORTÉE [RJ2] - 2) REFUS DU CORDIS DE DONNER SUITE À UNE DEMANDE DE SANCTION (ART. R. 134-33 DU CODE DE L'ÉNERGIE) - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR CETTE DÉCISION [RJ3].
</SCT>
<ANA ID="9A"> 01-04-03 La circonstance que ce soit le même membre du CoRDIS désigné dans les conditions prévues à l'article R. 134-30 du code de l'énergie qui, après avoir été chargé de l'instruction de la plainte, décide, au vu de cette instruction, qu'il n'y a pas lieu de donner suite à la saisine ne saurait, par elle-même, traduire un manquement à l'impartialité.</ANA>
<ANA ID="9B"> 26-055-01-06-01 La décision par laquelle le membre du CoRDIS désigné dans les conditions prévues à l'article R. 134-30 du code de l'énergie décide, au vu de l'instruction, qu'il n'y a pas lieu à mise en demeure ou à notification de griefs ne présente pas le caractère d'une sanction et ne peut conduire au prononcé d'une sanction. Dès lors, les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées à son encontre.</ANA>
<ANA ID="9C"> 29-01 1) L'article L. 111-66 du code de l'énergie, qui transpose en droit interne les dispositions de l'article 26, paragraphe 2, sous a) de la directive 2009/72/CE du 13 juillet 2009, n'impose pas aux sociétés gestionnaires de réseaux de distribution d'inclure dans leurs statuts une clause interdisant le cumul entre la responsabilité de la gestion des activités de distribution et la gestion des activités de production ou de fourniture, pourvu que le respect d'une telle interdiction soit assuré en pratique. Une telle obligation ne découle pas davantage de l'article L. 111-65 du code de l'énergie, qui prévoit seulement que les statuts d'une société gestionnaire d'un réseau de distribution de gaz ou d'électricité doivent comporter des dispositions propres à concilier l'indépendance d'action des responsables de la gestion du réseau et la préservation des droits des actionnaires.... ...2) Il résulte des articles L. 111-84 et L. 111-86 du code de l'énergie, lesquels transposent les articles 31, paragraphe 3, et 37, paragraphe 1, sous f), de la directive 2009/72/CE du 13 juillet 2009 que l'interdiction des subventions croisées en tant que telle ne vaut que pour l'exercice simultané, au sein d'une entité unique, d'activités de transport, de distribution et de fourniture et trouve sa traduction dans les obligations de séparation comptable.</ANA>
<ANA ID="9D"> 29-06-01 1) Il appartient au comité de règlement des différends et des sanctions de la Commission de régulation de l'énergie (CRE), investi par les dispositions de l'article L. 134-25 du code de l'énergie d'un pouvoir de sanction qu'il peut exercer de sa propre initiative ou à la suite d'une plainte, de décider, lorsqu'il est saisi par un tiers de faits de nature à motiver la mise en oeuvre de ce pouvoir, et après avoir procédé à leur examen, des suites à donner à la plainte. Il dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de la gravité des manquements allégués au regard de la législation ou de la réglementation que la Commission est chargée de faire appliquer, du sérieux des indices relatifs à ces faits, de la date à laquelle ils ont été commis, du contexte dans lequel ils l'ont été et, plus généralement, de l'ensemble des intérêts généraux dont elle a la charge.... ...2) La décision que prend le comité, ou le cas échéant celui de ses membres qui a été chargé de l'instruction de l'affaire en application de l'article R. 134-30 du code de l'énergie, lorsqu'il refuse de donner suite à une saisine, a le caractère d'une décision administrative que le juge de l'excès de pouvoir peut annuler en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 29 avril 1949,,, n° 82790, p. 188.,,[RJ2] Rappr., s'agissant de l'ACAM, CE, Section, 30 novembre 2007,,, n° 293952, p. 459 ; s'agissant de l'ARCEP, CE, 4 juillet 2012, Association française des opérateurs de réseaux et services de télécommunications, n°s 334062 347163, T. p. 887.,  ,[RJ3] Cf., CE, 7 février 2018, Société Ateliers de construction mécanique de Marigny, n° 399683, p. 17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
