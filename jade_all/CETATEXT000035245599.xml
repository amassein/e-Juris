<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245599</ID>
<ANCIEN_ID>JG_L_2017_07_000000411070</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245599.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 19/07/2017, 411070, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411070</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:411070.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, un nouveau mémoire et un mémoire en réplique, enregistrés les 31 mai, 29 juin et 4 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation, pour excès de pouvoir, de la décision du 14 mars 2017 par laquelle le vice-président du Conseil d'Etat a adopté la charte de déontologie de la juridiction administrative, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 131-4 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2016-483 du 20 avril 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.  Considérant que l'article L. 131-4, introduit dans le code de justice administrative par l'article 12 de la loi du 20 avril 2016 relative à la déontologie et aux droits et obligations des fonctionnaires, dispose que : " Le vice-président du Conseil d'Etat établit, après avis du collège de déontologie de la juridiction administrative, une charte de déontologie énonçant les principes déontologiques et les bonnes pratiques propres à l'exercice des fonctions de membre de la juridiction administrative ".<br/>
<br/>
              3.  Considérant qu'à l'appui de sa requête dirigée contre la décision du 14 mars 2017 par laquelle le vice-président du Conseil d'Etat a adopté la charte de déontologie de la juridiction administrative, M. A... soutient que les dispositions citées ci-dessus sont contraires au principe d'impartialité des juridictions et au droit d'exercer un recours juridictionnel effectif en ce qu'elles confient au vice-président du Conseil d'Etat l'établissement d'une charte de déontologie de la juridiction administrative dont la légalité ne peut être contestée par la voie du recours pour excès de pouvoir que devant le Conseil d'Etat, statuant en premier et dernier ressort ; <br/>
<br/>
              4.  Considérant que la décision dont M. A... demande l'annulation a été prise sur le fondement de cet article L. 131-4 du code de justice administrative qui est, ainsi, applicable au présent litige ; que cet article n'a pas déjà été déclaré conforme à la Constitution par le Conseil constitutionnel ; qu'enfin, le moyen tiré de ce qu'en confiant au vice-président du Conseil d'Etat l'établissement de la charte de déontologie, il méconnaîtrait, compte tenu des compétences du Conseil d'Etat en matière contentieuse, le principe d'impartialité des juridictions et le droit à un recours juridictionnel effectif garantis par les dispositions de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, soulève une question qui peut être regardée comme nouvelle au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que, par suite, sans qu'il soit besoin pour le Conseil d'Etat d'en examiner le caractère sérieux, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 131-4 du code de justice administrative est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur la requête de M. A... jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
Article 3 : La présente décision sera notifiée à M. B... A...et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
