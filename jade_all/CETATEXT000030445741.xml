<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445741</ID>
<ANCIEN_ID>JG_L_2015_03_000000388355</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445741.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/03/2015, 388355, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388355</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:388355.20150309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Toulon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner au maire de Plan-d'Aups-Sainte-Baume de faire cesser l'atteinte portée à son droit à exercer ses fonctions sans subir de harcèlement moral. Par une ordonnance n° 1500395 du 12 février 2015, le juge des référés du tribunal administratif de Toulon a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 1er mars 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1500395 du 12 février 2015 par laquelle le juge des référés du tribunal administratif de Toulon a rejeté sa demande ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Plan-d'Aups-Sainte-Baume la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et les entiers dépens.<br/>
<br/>
<br/>
              elle soutient que : <br/>
<br/>
              - le juge des référés du tribunal administratif de Toulon a dénaturé les pièces du dossier en considérant, d'une part, que les attestations produites étaient insusceptibles de constituer un début de commencement de preuve de ce que des propos racistes ont été tenus par le maire et, d'autre part, que le maire avait un projet de suppression de l'emploi de Mme A... alors que sa volonté était en réalité de l'inciter à abandonner ses fonctions ; <br/>
              - la condition d'urgence est remplie, dès lors qu'elle encourt un risque grave et imminent pour sa santé en raison de la reprise de son travail ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit fondamental à ne pas être soumis à un harcèlement moral.<br/>
<br/>
<br/>
              Vu les pièces complémentaires produites le 5 mars 2015 par MmeA... ;<br/>
<br/>
              Par un mémoire en défense enregistré le 5 mars 2015, la commune de Plan-d'Aups-Sainte-Baume conclut au rejet de la requête et en outre à ce qu'il soit mis à la charge de Mme A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par Mme A...ne sont pas fondés et, en outre, que ;<br/>
              - la requérante n'apporte pas de commencements de preuve de l'existence d'un harcèlement moral ; <br/>
              - Mme A...n'a pas de droit acquis au maintien dans le poste de secrétaire de mairie.<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n°84-53 du 26 janvier 1984 ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme B...A..., d'autre part, la commune de Plan-d'Aups-Sainte-Baume ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 6 mars 2015 à 10 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme A...;<br/>
<br/>
              - Me Matuchansky, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la commune de Plan-d'Aups-Sainte-Baume ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 6 quinquiès de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel. " ; que le droit de ne pas être soumis à un harcèlement moral constitue pour un agent une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              3. Considérant que Mme A...a été recrutée par la commune de Plan-d'Aups-Sainte-Baume en septembre 2013 par voie de mutation en qualité de technicien principal 2ème classe, puis promue à la 1ère classe de son grade et affectée à l'emploi de secrétaire générale de mairie ; qu'il résulte de l'instruction et des échanges lors de l'audience, qu'à la suite des élections municipales qui se sont déroulées en mars 2014, un nouveau maire a été élu ; que, cependant, les attestations produites par Mme A... ne permettent pas d'établir que des propos racistes auraient été tenus à son encontre par le nouveau maire ; que par ailleurs l'existence d'un projet du nouveau maire de supprimer le poste de secrétaire de mairie pour des motifs budgétaires est étayée par les pièces du dossier ; qu'enfin il ne résulte pas de l'instruction et des échanges de l'audience que si le maire a procédé à certains aménagements des tâches exercées par Mme A... et envisageait pour elle une nouvelle affectation, elle aurait été privée dans l'intervalle de ses prérogatives professionnelles et fait l'objet de pressions destinées à l'inciter à abandonner ses fonctions ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il n'est pas établi qu'il ait été porté une atteinte grave et manifestement illégale au droit de Mme A...de ne pas être soumise à un harcèlement moral ; que, par suite, la requête de Mme A... doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, de mettre à la charge de Mme A...la somme de 2 000 euros au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : Mme A...versera, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, une somme de 2 000 euros à la commune de Plan-d'Aups-Sainte-Baume.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme B... A...et à la commune de Plan-d'Aups-Sainte-Baume.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
