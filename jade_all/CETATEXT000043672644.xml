<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672644</ID>
<ANCIEN_ID>JG_L_2021_06_000000449358</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672644.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 15/06/2021, 449358, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449358</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449358.20210615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              Le médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône et la caisse primaire d'assurance maladie des Bouches-du-Rhône ont porté plainte contre M. B... A... devant la section des assurances sociales de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur - Corse de l'ordre des médecins. Par une décision du 12 février 2019, la section des assurances sociales de la chambre disciplinaire de première instance a infligé à M. A... la sanction de l'interdiction de donner des soins aux assurés sociaux pour une durée d'un an, dont six mois assortis du sursis.<br/>
<br/>
              Par une décision du 3 décembre 2020, la section des assurances sociales du Conseil national de l'ordre des médecins a rejeté l'appel formé par M. A... contre cette décision, décidé que la partie ferme de la sanction sera exécutée du 1er mars au 31 août 2021 et ordonné qu'elle soit publiée dans les locaux de la caisse primaire d'assurance maladie des Bouches-du-Rhône.<br/>
<br/>
              1° Sous le numéro 449361, par un pourvoi enregistré le 3 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le numéro 449358, par une requête et un mémoire en réplique enregistrés les 3 février et 14 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de la même décision de la section des assurances sociales du Conseil national de l'ordre des médecins ;<br/>
<br/>
              2°) de mettre à la charge de la caisse primaire d'assurance maladie des Bouches-du-Rhône la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament - Robillot, avocat de M. A... et à la SCP Boutet-Hourdeaux, avocat de la caisse primaire d'assurance maladie de Marseille ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Le pourvoi par lequel M. A... demande l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 3 décembre 2020 et sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins qu'il attaque, M. A... soutient qu'elle est entachée : <br/>
              - d'insuffisance de motivation et de dénaturation des écritures en ce que, pour écarter le moyen tiré de l'irrégularité de la décision de la section des assurances sociales de la chambre disciplinaire de première instance faute d'avoir été signée par son président, elle se borne à affirmer que l'ampliation de cette décision n'a pas à être signée ;<br/>
              - d'erreur de droit en ce qu'elle juge que les conditions dans lesquelles s'est déroulée l'enquête préalable sont sans incidence sur la recevabilité et sur la régularité de la plainte ainsi que sur la régularité de la procédure, sans rechercher si les irrégularités qu'il a relevées, relatives notamment au recueil des témoignages et à l'établissement d'une attestation, n'ont pas été de nature à affecter la valeur probante des éléments produits devant la section des assurances sociales de la chambre disciplinaire de première instance ;<br/>
              - de dénaturation des pièces du dossier et d'erreur de droit en ce que pour retenir le grief tiré de la facturation d'actes fictifs, elle se fonde d'une part sur la circonstance qu'il n'a fourni aucune iconographie exploitable lors de l'enquête préalable alors que les iconographies qu'il a produites mentionnent l'identité du patient et la date de l'examen et d'autre part sur l'absence de compte-rendu alors que cette circonstance, à la supposer établie, ne permet pas de démontrer que les actes litigieux n'ont pas été réalisés ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle juge que dans les dossiers 24 et 25, les médecins ont attesté du caractère fictif des actes litigieux ;<br/>
              - d'erreur de droit et de dénaturation des pièces du dossier en ce que pour retenir le grief tiré de la facturation d'actes fictifs dans les dossiers 24, 25, 27, 29, 33, 40, 42 et 43, elle affirme " qu'il semble très peu probable que le praticien ait pu (...) voir " les patients alors qu'il ressort des pièces du dossier que ces patients l'ont bien consulté ;<br/>
              - de dénaturation des pièces du dossier en ce que pour retenir le grief tiré de la facturation indue d'actes fictifs pour les dossiers 3, 22 et 23, elle se fonde sur la circonstance que les trois patients ont attesté ne pas avoir reçu les soins qu'il a facturés alors que la patiente du dossier n° 3 a été placée en unité de surveillance continue, acte qu'il a facturé selon la bonne cotation, et que les autres patients, traités pour des pathologies graves, peuvent avoir oublié les actes qu'il a pratiqués ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle estime que les attestations qu'il a produites en appel, notamment pour les dossiers 1, 5, 6, 8, 9, 13, 14, 16 et 19, sont imprécises et incomplètes alors qu'elles précisent les dates auxquelles il s'est rendu au domicile des patients pour effectuer une consultation et que dans deux d'entre elles, les patients des dossiers 20 et 22 réfutent les déclarations qu'ils ont formulées lors de l'enquête préalable ;<br/>
              - d'erreur de droit et d'insuffisance de motivation en ce qu'elle retient le grief tiré de l'absence de compte rendu d'actes facturés, sans rechercher si cette absence était susceptible de constituer un manquement de nature à justifier une sanction disciplinaire ;<br/>
              - de dénaturation des pièces du dossier en ce qu'elle juge que dans 30 dossiers, il n'a pas fourni les comptes rendus des examens complémentaires facturés durant l'hospitalisation des patients alors que pour les dossiers 9 et 28 il a produit des comptes rendus pour les anomalies relevées par la caisse primaire d'assurance maladie des Bouches-du-Rhône ;<br/>
              - d'insuffisance de motivation et de dénaturation des pièces du dossier en ce qu'elle retient le grief tiré de la facturation indue d'actes cotés YYY015 sans expliquer en quoi les dispositions de la classification commune des actes médicaux ont été méconnues et alors même que les patients concernés ont fait l'objet d'une surveillance continue qu'il est impossible, plusieurs années après leur hospitalisation, d'établir, notamment par la production des tracés de monitoring ;<br/>
              - d'erreur de droit et de dénaturation des pièces du dossier en ce qu'elle retient le grief tiré de facturations irrégulières au regard de l'article 2 bis de la nomenclature générale des actes professionnels, relatives à la majoration forfaitaire transitoire (MPC) au motif que celle-ci ne peut s'appliquer qu'à des consultations au cabinet du médecin spécialiste, l'article précité n'excluant pas le recours à cette majoration pour des consultations effectuées dans une clinique.<br/>
<br/>
              Il soutient en outre qu'elle lui inflige une sanction hors de proportion avec la gravité des fautes retenues.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi de M. A... n'étant pas admis, les conclusions de sa requête tendant à ce qu'il soit sursis à l'exécution de la décision attaquée sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. A... tendant à ce qu'il soit sursis à l'exécution de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 3 décembre 2020.<br/>
Article 3 : Les conclusions de la requête de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à la caisse primaire d'assurance maladie des Bouches-du-Rhône et au médecin-conseil, chef de service de l'échelon local du service médical des Bouches-du-Rhône.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
