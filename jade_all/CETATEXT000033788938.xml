<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788938</ID>
<ANCIEN_ID>JG_L_2016_12_000000387229</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/12/2016, 387229, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387229</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; LE PRADO ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387229.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...a demandé au tribunal administratif de Marseille de condamner l'assistance publique des hôpitaux de Marseille (APHM) et la commune de Marseille à réparer les préjudices résultant de sa prise charge par les marins-pompiers et l'APHM le 7 juillet 2005. Par un jugement avant dire droit n° 0906616 du 30 novembre 2010, le tribunal administratif a ordonné une expertise afin d'évaluer les préjudices du demandeur. Par un nouveau jugement n° 0906616 du 11 décembre 2012, le tribunal administratif a condamné l'APHM à payer diverses sommes à M. B...en réparation de ses préjudices.<br/>
<br/>
              Par un arrêt n° 13MA00553 du 20 novembre 2014, la cour administrative d'appel de Marseille, statuant sur appel de M. B...et sur appel incident de l'APHM, a rejeté les conclusions d'appel de M. B...et réduit le montant des sommes mises à la charge de l'APHM.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 19 janvier et 17 avril 2015 et le 29 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er, 2 et 5 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'APHM et de la commune de Marseille la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M.B..., à la SCP Célice, Soltner, Texidor, Perier, avocat de la commune de Marseille et à Me Le Prado, avocat de l'APHM.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 7 juillet 2005, M. B...a fait une chute dans la cage d'escalier de son immeuble à Marseille ; que les marins-pompiers l'ont pris en charge et transporté aux urgences de l'hôpital de la Conception à Marseille ; qu'il a ensuite été admis au service de neurologie de l'hôpital de la Timone ; qu'en dépit d'une intervention pratiquée le jour même dans cet établissement, il est atteint d'une tétraplégie définitive ; qu'il a recherché devant le tribunal administratif de Marseille la responsabilité de la commune de Marseille au titre de sa prise en charge par les marins-pompiers et celle de l'Assistance publique des hôpitaux de Marseille (APHM) au titre de sa prise en charge à l'hôpital de la Conception ; que, par un jugement avant dire droit du 30 novembre 2010, le tribunal administratif a retenu l'existence de fautes commises par les marins-pompiers et par le service public hospitalier et ordonné une expertise collégiale afin de déterminer si ces fautes avaient entraîné la perte d'une chance de conserver des séquelles moins lourdes ; que, par un jugement du 11 décembre 2012, le tribunal a estimé que les fautes des marins-pompiers n'avaient pas eu d'incidence sur les séquelles de l'intéressé mais que celles qui avaient été commises à l'hôpital de la Conception avaient entraîné une perte de chance évaluée à 17 % ; qu'il a, en conséquence, condamné l'APHM à verser à M. B...et à la caisse primaire d'assurance maladie des indemnités calculées sur cette base et rejeté les conclusions dirigées contre la commune de Marseille ; que, par un arrêt du 20 novembre 2014, la cour administrative d'appel de Marseille a confirmé le jugement en tant qu'il écartait la responsabilité de la commune de Marseille et fixait à 17 % le taux de la perte de chance imputable au service public hospitalier, mais réduit les sommes mises à la charge de l'APHM, en excluant notamment une indemnisation au titre des frais liés à l'assistance d'une tierce personne, dont elle a estimé qu'ils n'auraient pas été moins importants en l'absence de faute ; que M. B...et, par la voie d'un pourvoi incident, l'APHM demandent l'annulation de cet arrêt ;<br/>
<br/>
              Sur le pourvoi principal de M.B... :<br/>
<br/>
              2. Considérant, en premier lieu, que, pour fixer à 17 % le taux de la perte de chance imputable aux fautes commises lors de la prise en charge de M. B...à l'hôpital de la Conception, le tribunal administratif de Marseille a, dans son jugement du 11 décembre 2012, tenu compte, d'une part, du fait que la chute subie par l'intéressé avait par elle-même entraîné des séquelles, définitivement acquises dès avant la prise en charge, qui représentaient 50 % du dommage final et, d'autre part, de l'indication donnée par les experts selon laquelle les fautes commises avaient entraîné, par rapport à une prise en charge adaptée, une perte de chance de 30 % ; que, pour écarter le moyen soulevé devant elle par M.B..., selon lequel la perte de chance avait été sous-évaluée, notamment au regard du taux avancé par les experts, la cour administrative d'appel a  jugé que " le collège de trois experts a estimé une perte de chance de 30 % imputable à ce retard thérapeutique sans préciser davantage, en indiquant que ce pourcentage correspond au surcroît d'incapacité de M. B...du fait de sa prise en charge fautive, sans tenir le raisonnement probabiliste qui s'impose pour déterminer le taux de perte de chance " ; qu'en confirmant le taux de perte de chance retenu par les premiers juges, qui reposait sur le taux de 30 % avancé par les experts combiné avec une évaluation de la part du dommage irréversiblement acquise au moment de la prise en charge, alors qu'elle estimait que ce taux de 30 % ne pouvait être retenu eu égard à la méthode qui avait conduit les experts à l'avancer, la cour n'a pas tiré les conséquences nécessaires de ses propres constatations ; <br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes de l'article R. 811-6 du code de justice administrative : " Par dérogation aux dispositions du premier alinéa de l'article R. 811-2, le délai d'appel contre un jugement avant-dire-droit, qu'il tranche ou non une question au principal, court jusqu'à l'expiration du délai d'appel contre le jugement qui règle définitivement le fond du litige " ; que l'arrêt attaqué relève que " le jugement avant-dire-droit du 30 novembre 2010 devenu définitif a limité la faute des marins-pompiers aux manipulations sans précaution du patient lors de leur arrivée sur place immédiatement après sa chute " et en déduit que M. B... n'est pas recevable à invoquer, dans le cadre de son appel contre le jugement du 11 décembre 2012, d'autres fautes qui auraient été commises par les marins-pompiers et auraient pu avoir une incidence sur le dommage ; qu'en retenant que le jugement avant dire droit était devenu définitif, alors qu'en application des dispositions précitées de l'article R. 811-6 du code de justice administrative, le délai d'appel contre ce jugement courait jusqu'à l'expiration du délai d'appel contre le jugement réglant définitivement le fond du litige et qu'un appel avait été formé dans ce délai, la cour a commis une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel qu'il attaque ; <br/>
<br/>
              Sur le pourvoi incident de l'assistance publique des hôpitaux de Marseille :<br/>
<br/>
              5. Considérant que l'annulation, sur le pourvoi de M. B..., de l'arrêt du 20 novembre 2014 prive d'objet le pourvoi incident présenté par APHM contre le même arrêt ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'APHM et de la commune de Marseille le versement à M.B..., au titre de l'article L. 761-1 du code de justice administrative, de la somme de 2 000 euros chacune ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt n°13MA00553 du 20 novembre 2014 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi incident de l'Assistance publique des hôpitaux de Marseille.<br/>
Article 4 : L'Assistance publique des hôpitaux de Marseille et la commune de Marseille verseront chacune une somme de 2 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune de Marseille présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à M. A...B..., à l'Assistance publique des hôpitaux de Marseille et à la commune de Marseille. Copie en sera adressée à la caisse primaire d'assurance maladie des Bouches-du-Rhône. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
