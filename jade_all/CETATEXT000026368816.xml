<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026368816</ID>
<ANCIEN_ID>JG_L_2012_09_000000357839</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/36/88/CETATEXT000026368816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 12/09/2012, 357839, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-09-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357839</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Aymeric Pontvianne</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2012:357839.20120912</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les mémoires, enregistrés les 18 et 29 juin, les 2, 4 et 23 juillet et les 13 et 27 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par l'association France Nature Environnement, dont le siège est au 10, rue Barbier au Mans (72000), représentée par M. Raymond Léost, administrateur, et par l'association Agir pour les paysages, dont le siège est 22, rue Meyrueis à Montpellier (34000), représentée par son président en exercice ; France Nature Environnement et Agir pour les paysages demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation pour excès de pouvoir du décret n° 2012-118 du 30 janvier 2012 relatif à la publicité extérieure, aux enseignes et aux préenseignes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du deuxième et du troisième alinéas de l'article L. 581-9, de l'article L. 581-14-2, de l'article L. 581-18 ainsi que de l'article L. 120-1 du code de l'environnement ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aymeric Pontvianne, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du deuxième alinéa de l'article L. 581-9 du code de l'environnement, dans sa rédaction, applicable à la date du décret du 30 janvier 2012, résultant de la loi du 12 juillet 2010 portant engagement national pour l'environnement : " Peuvent être autorisés par arrêté municipal, au cas par cas, les emplacements de bâches comportant de la publicité et, après avis de la commission départementale compétente en matière de nature, de paysages et de sites, l'installation de dispositifs publicitaires de dimensions exceptionnelles liés à des manifestations temporaires. Les conditions d'application du présent alinéa sont déterminées par le décret mentionné au premier alinéa. " ; qu'aux termes du troisième alinéa du même article : " L'installation des dispositifs de publicité lumineuse autres que ceux qui supportent des affiches éclairées par projection ou par transparence est soumise à l'autorisation de l'autorité compétente. " ; que l'article L. 581-14-2 du même code, résultant de la loi du 12 juillet 2010, prévoit que " Les compétences en matière de police de la publicité sont exercées par le préfet. Toutefois, s'il existe un règlement local de publicité, ces compétences sont exercées par le maire au nom de la commune. Dans ce dernier cas, à défaut pour le maire de prendre les mesures prévues aux articles L. 581-27, L. 581-28 et L. 581-31 dans le délai d'un mois suivant la demande qui lui est adressée par le représentant de l'Etat dans le département, ce dernier y pourvoit en lieu et place du maire. " ; qu'aux termes du premier alinéa de l'article L. 581-18 du même code, dans sa rédaction résultant de la loi du 12 juillet 2010 : " Un décret en Conseil d'Etat fixe les prescriptions générales relatives à l'installation et à l'entretien des enseignes en fonction des procédés utilisés, de la nature des activités ainsi que des caractéristiques des immeubles où ces activités s'exercent et du caractère des lieux où ces immeubles sont situés. Ce décret fixe également des prescriptions relatives aux enseignes lumineuses afin de prévenir ou limiter les nuisances lumineuses mentionnées au chapitre III du présent titre. " ; <br/>
<br/>
              3. Considérant, d'autre part, que l'article L. 120-1 du code de l'environnement, résultant de la loi du 12 juillet 2010, définit les conditions et limites dans lesquelles le principe de participation du public prévu à l'article 7 de la Charte de l'environnement est applicable aux décisions réglementaires de l'Etat et de ses établissements publics ; <br/>
<br/>
              4. Considérant que les associations France Nature Environnement et Agir pour les paysages soutiennent, à l'appui du recours pour excès de pouvoir qu'elles ont formé contre le décret du 30 janvier 2012 relatif à la publicité extérieure, aux enseignes et aux préenseignes, que ces différentes dispositions de la partie législative du code de l'environnement portent atteinte aux droits et libertés garantis par la Constitution ;<br/>
<br/>
              5. Considérant que les dispositions précitées des articles L. 581-9, L. 581-14-2 et L. 581-18 du code de l'environnement, sur le fondement desquelles ont été prises certaines des dispositions du décret attaqué, sont applicables au litige ; que sont, de même, applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958, les dispositions de l'article L. 120-1 du code de l'environnement qui définissent les conditions dans lesquelles le décret attaqué a été soumis à participation du public ; <br/>
<br/>
              6. Considérant que ces différentes dispositions du code de l'environnement n'ont pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel ;<br/>
<br/>
              7. Considérant qu'à l'appui des questions prioritaires de constitutionnalité soulevées, les associations requérantes se prévalent, notamment, des termes des articles 1er, 3 et 7 de la Charte de l'environnement ; que ce moyen soulève une question présentant un caractère sérieux ; qu'il y a lieu, par suite, de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité relatives aux deuxième et troisième alinéas de l'article L. 581-9, de l'article L. 581-14-2, du premier alinéa de l'article L. 581-18 et de l'article L. 120-1 du code de l'environnement, dans leur rédaction résultant de la loi du 12 juillet 2010 ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les questions de la conformité à la Constitution des deuxième et troisième alinéas de l'article L. 581-9, de l'article L. 581-14-2, du premier alinéa de l'article L. 581-18 et de l'article L. 120-1 du code de l'environnement sont renvoyées au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête des associations France Nature Environnement et Agir pour les paysages jusqu'à ce que le Conseil constitutionnel ait tranché les questions de constitutionnalité ainsi soulevées.<br/>
<br/>
Article 3 : La présente décision sera notifiée aux associations France Nature Environnement et Agir pour les paysages, au ministre de l'économie et des finances et au ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>
              Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
