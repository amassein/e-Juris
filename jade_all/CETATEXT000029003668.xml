<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003668</ID>
<ANCIEN_ID>JG_L_2014_05_000000359738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003668.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 28/05/2014, 359738</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; BALAT</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359738.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 mai et 28 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Compagnie des Bateaux Mouches, dont le siège est Port de la Conférence, à Paris (75008) ; elle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 2 à 4 de l'arrêt n° 11PA00557 du 15 mars 2012 par lesquels la cour administrative d'appel de Paris, après avoir rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0521100 du 2 décembre 2010 du tribunal administratif de Paris en tant qu'il a rejeté ses conclusions tendant à ce qu'il soit enjoint à l'établissement public Voies Navigables de France de lui restituer, avec intérêts de droit et capitalisation des intérêts, la somme de 155 449,12 euros qu'elle avait versée sur le fondement d'un titre exécutoire émis le 17 octobre 2002 et annulé par décision n° 305417 du 30 juillet 2010 du Conseil d'Etat statuant au contentieux, d'autre part, à ce qu'il soit enjoint, au besoin sous astreinte, à l'établissement public Voies Navigables de France de lui restituer cette somme avec intérêts de droit et capitalisation des intérêts, a fait droit à l'appel incident de l'établissement public Voies navigables de France et l'a condamnée à verser à celui-ci la somme de 129 157,02 euros correspondant au reliquat non acquitté de la somme mentionné dans ce titre exécutoire ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel incident de Voies Navigables de France ; <br/>
<br/>
              3°) de mettre à la charge de Voies navigables de France la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 90-1168 du 29 décembre 1990 ;<br/>
<br/>
              Vu le décret n° 60-441 du 26 décembre 1960 ;<br/>
<br/>
              Vu le décret n° 91-696 du 18 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 91-797 du 20 août 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la Compagnie des Bateaux Mouches et à Me Balat, avocat de Voies navigables de France ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le conseil d'administration de l'établissement public Voies Navigables de France a adopté, le 3 octobre 2001, une délibération fixant les tarifs des péages pour le transport de passagers pour l'année 2002 ; que l'établissement a adressé, le 24 octobre 2002, à la Compagnie des Bateaux Mouches un titre exécutoire comportant un avis des sommes à payer daté du 17 octobre 2002 pour des péages dus pour l'année 2002 ; que, par une décision n° 305417 du 30 juillet 2010, le Conseil d'Etat statuant au contentieux a annulé cet état exécutoire au motif que la délibération fixant les tarifs n'avait pas fait l'objet d'une publication régulière ; que la société requérante n'ayant, pour l'exécution de ce titre, versé à l'établissement public qu'une somme de 155 449,12 euros, Voies Navigables de France a émis, le 29 novembre 2005, un nouvel état exécutoire d'un montant de 129 157,02 euros, correspondant au reliquat non versé par la Compagnie des Bateaux Mouches ; que celle-ci se pourvoit en cassation contre l'arrêt du 15 mars 2012 de la cour administrative d'appel de Paris en tant que, faisant droit à l'appel incident de l'établissement public contre le jugement du 2 décembre 2010 du tribunal administratif de Paris qui avait annulé l'état exécutoire du 29 novembre 2005, elle l'a condamnée, sur le fondement de son enrichissement sans cause, à verser à Voies Navigables de France la somme de 129 157, 02 euros ;<br/>
<br/>
              2. Considérant qu'en vertu du I de l'article 124 de la loi du 29 décembre 1990 de finances pour 1991, l'établissement public qui a pris le nom de A...en application du décret du 18 juillet 1991 pris pour l'application de ces dispositions et portant statut de Voies Navigables de France s'est vu confier l'exploitation, l'entretien, l'amélioration, l'extension des voies navigables et de leurs dépendances et la gestion du domaine de l'Etat nécessaire à l'accomplissement de ses missions ; que ces dispositions prévoient également que l'établissement public perçoit à son profit, notamment, des redevances et droits fixes sur les personnes publiques ou privées pour l'usage d'une partie du domaine public ; qu'aux termes du III de l'article 124 de la même loi, applicable aux faits de l'espèce : " Les transporteurs de marchandises ou de passagers (...) sont assujettis, dans des conditions fixées par décret en Conseil d'Etat, à des péages perçus au profit de l'établissement public lorsqu'ils naviguent sur le domaine public qui lui est confié (...) Le montant de ces péages est fixé par l'établissement (...) " ; qu'aux termes de l'article 2 du décret du 20 août 1991 relatif aux recettes instituées au profit de Voies Navigables de France par l'article 124 de la loi de finances pour 1991, dans sa rédaction applicable au litige : " Pour le transport public de personnes réalisé à l'intérieur des limites du domaine confié à Voies Navigables de France, le transporteur acquitte un péage pour tout parcours effectué en utilisant le réseau fluvial. Les tarifs du péage sont fonction des sections de voies navigables empruntées par le transporteur, des caractéristiques du bateau, de la durée d'utilisation des voies du réseau et du trajet (...) " ; que l'article 3 de ce décret prévoit que ces péages peuvent être établis sous la forme de forfaits calculés selon la durée, la période d'utilisation du réseau, la portion du réseau emprunté et les caractéristiques du bateau ; qu'aux termes de l'article 6 du même décret, dans sa rédaction applicable au litige : " (...) Les péages prévus aux articles 2 et 3 sont recouvrés par l'établissement public, qui délivre au transporteur de passagers ou au propriétaire du bateau un récépissé constatant le règlement des péages dus. / Les transporteurs mentionnés à l'article 2 du présent décret (...) produisent chaque année une déclaration de flotte au plus tard le 1er février. Cette déclaration précise le nombre et les caractéristiques des bateaux concernés. En cas de défaut de déclaration ou de déclaration inexacte, le président de l'établissement public établit avant le 1er avril un état qui se substitue à la déclaration et poursuit le recouvrement. (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que Voies Navigables de France a la faculté d'adresser aux transporteurs de marchandises ou de passagers naviguant sur le domaine public fluvial, lesquels sont dans une situation unilatérale et réglementaire, des états exécutoires en vue d'obtenir le recouvrement des péages dus par ces derniers au titre de leur usage du réseau fluvial ; qu'eu égard à la nature et à l'objet de ces redevances pour services rendus, qui constituent la rémunération des prestations fournies à des usagers du domaine public fluvial, l'inopposabilité de la délibération fixant les tarifs des péages résultant de ce qu'elle n'avait pas fait l'objet d'une mesure de publicité suffisante ne saurait avoir pour effet de décharger les usagers ayant ainsi contesté les montants de péages mis à leur charge de toute obligation de payer une redevance en contrepartie du service dont ils ont effectivement bénéficié ; que, dès lors, Voies Navigables de France peut, lorsque la juridiction administrative annule un état exécutoire au seul motif que les tarifs sur lesquels il se fondait n'étaient pas opposables, faute d'avoir fait l'objet d'une mesure de publicité suffisante, émettre de nouveaux états exécutoires au titre des périodes en litige après avoir régulièrement publié ces tarifs ; <br/>
<br/>
              4. Considérant qu'en relevant, pour condamner la Compagnie des Bateaux Mouches à verser à Voies Navigables de France la somme de 129 157,02 euros, que l'établissement était fondé à se prévaloir, devant le juge administratif, d'un enrichissement sans cause de la Compagnie des Bateaux Mouches en raison de l'utilité que celle-ci aurait retirée de sa navigation sur le domaine public fluvial au titre de l'année 2002, alors que la voie de droit mentionnée ci-dessus lui était ouverte et que, dès lors, celle de l'enrichissement sans cause ne pouvait l'être, la cour administrative d'appel de Paris a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la Compagnie des Bateaux Mouches est fondée à demander l'annulation des articles 2, 3 et 4 de l'arrêt qu'elle attaque ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de qui vient d'être dit que Voies Navigables de France n'est pas fondé à se prévaloir d'un enrichissement sans cause de la Compagnie des Bateaux Mouches pour demander, par la voie de l'appel incident, l'annulation du jugement attaqué ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Voies Navigables de France le versement à la Compagnie des Bateaux Mouches de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Compagnie des Bateaux Mouches qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                  --------------<br/>
<br/>
 Article 1er : Les articles 2, 3 et 4 de l'arrêt de la cour administrative d'appel de Paris du 15 mars 2012 sont annulés. <br/>
<br/>
 Article 2 : L'appel incident de Voies Navigables de France est rejeté. <br/>
<br/>
 Article 3 : Voies Navigables de France versera à la Compagnie des Bateaux Mouches une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
 Article 4 : Les conclusions de Voies Navigables de France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
 Article 5 : La présente décision sera notifiée à la Compagnie des Bateaux Mouches et à l'établissement public Voies Navigables de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">65-04-01 TRANSPORTS. TRANSPORTS FLUVIAUX. CONDITIONS FINANCIÈRES. - PÉAGES - DÉLIBÉRATIONS DE VNF FIXANT LES TARIFS - INOPPOSABILITÉ À DÉFAUT DE PUBLICITÉ SUFFISANTE - 1) EFFET - DÉCHARGE DES USAGERS AYANT CONTESTÉ LES MONTANTS DE PÉAGES MIS À LEUR CHARGE DE TOUTE OBLIGATION DE PAYER UNE REDEVANCE EN CONTREPARTIE DU SERVICE RENDU - ABSENCE [RJ1] -  2) VOIES DE DROIT OUVERTES À VNF DANS UN TEL CAS - A) FACULTÉ, EN CAS D'ANNULATION PAR LE JUGE D'UN ÉTAT EXÉCUTOIRE POUR INOPPOSABILITÉ DES TARIFS, D'ÉMETTRE DE NOUVEAUX ÉTATS EXÉCUTOIRES AU TITRE DES PÉRIODES EN LITIGE APRÈS AVOIR RÉGULIÈREMENT PUBLIÉ CES TARIFS - EXISTENCE [RJ1] - B) FACULTÉ D'INVOQUER L'ENRICHISSEMENT SANS CAUSE DES USAGERS - ABSENCE, EU ÉGARD AU CARACTÈRE SUBSIDIAIRE D'UNE TELLE VOIE DE DROIT [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-04-02 TRANSPORTS. TRANSPORTS FLUVIAUX. VOIES NAVIGABLES DE FRANCE. - DÉLIBÉRATIONS TARIFAIRES - INOPPOSABILITÉ À DÉFAUT DE PUBLICITÉ SUFFISANTE - 1) EFFET - DÉCHARGE DES USAGERS AYANT CONTESTÉ LES MONTANTS DE PÉAGES MIS À LEUR CHARGE DE TOUTE OBLIGATION DE PAYER UNE REDEVANCE EN CONTREPARTIE DU SERVICE RENDU - ABSENCE [RJ1] -  2) VOIES DE DROIT OUVERTES À VNF DANS UN TEL CAS - A) FACULTÉ, EN CAS D'ANNULATION PAR LE JUGE D'UN ÉTAT EXÉCUTOIRE POUR INOPPOSABILITÉ DES TARIFS, D'ÉMETTRE DE NOUVEAUX ÉTATS EXÉCUTOIRES AU TITRE DES PÉRIODES EN LITIGE APRÈS AVOIR RÉGULIÈREMENT PUBLIÉ CES TARIFS - EXISTENCE [RJ1] - B) FACULTÉ D'INVOQUER L'ENRICHISSEMENT SANS CAUSE DES USAGERS - ABSENCE, EU ÉGARD AU CARACTÈRE SUBSIDIAIRE D'UNE TELLE VOIE DE DROIT [RJ2].
</SCT>
<ANA ID="9A"> 65-04-01 Voies Navigables de France (VNF) a la faculté d'adresser aux transporteurs de marchandises ou de passagers navigant sur le domaine public fluvial, lesquels sont dans une situation unilatérale et réglementaire, des états exécutoires en vue d'obtenir le recouvrement des péages dus par ces derniers au titre de leur usage du réseau fluvial.... ,,1) Eu égard à la nature et à l'objet de ces redevances pour services rendus, qui constituent la rémunération des prestations fournies à des usagers du domaine public fluvial, l'inopposabilité de la délibération fixant les tarifs des péages résultant de ce qu'elle n'avait pas fait l'objet d'une mesure de publicité suffisante ne saurait avoir pour effet de décharger les usagers ayant ainsi contesté les montants de péages mis à leur charge de toute obligation de payer une redevance en contrepartie du service dont ils ont effectivement bénéficié.... ,,2) a) Dès lors, VNF peut, lorsque la juridiction administrative annule un état exécutoire au seul motif que les tarifs sur lesquels il se fondait n'étaient pas opposables, faute d'avoir fait l'objet d'une mesure de publicité suffisante, émettre de nouveaux états exécutoires au titre des périodes en litige après avoir régulièrement publié ces tarifs.,,,b) En revanche, VNF n'est pas fondé à se prévaloir, devant le juge administratif, d'un enrichissement sans cause de l'usager en raison de l'utilité que celui-ci aurait retirée de sa navigation sur le domaine public fluvial au titre de la période en litige, alors que la voie de droit mentionnée au a) lui est ouverte et que, dès lors, celle de l'enrichissement sans cause ne peut l'être.</ANA>
<ANA ID="9B"> 65-04-02 Voies Navigables de France (VNF) a la faculté d'adresser aux transporteurs de marchandises ou de passagers navigant sur le domaine public fluvial, lesquels sont dans une situation unilatérale et réglementaire, des états exécutoires en vue d'obtenir le recouvrement des péages dus par ces derniers au titre de leur usage du réseau fluvial.... ,,1) Eu égard à la nature et à l'objet de ces redevances pour services rendus, qui constituent la rémunération des prestations fournies à des usagers du domaine public fluvial, l'inopposabilité de la délibération fixant les tarifs des péages résultant de ce qu'elle n'avait pas fait l'objet d'une mesure de publicité suffisante ne saurait avoir pour effet de décharger les usagers ayant ainsi contesté les montants de péages mis à leur charge de toute obligation de payer une redevance en contrepartie du service dont ils ont effectivement bénéficié.... ,,2) a) Dès lors, VNF peut, lorsque la juridiction administrative annule un état exécutoire au seul motif que les tarifs sur lesquels il se fondait n'étaient pas opposables, faute d'avoir fait l'objet d'une mesure de publicité suffisante, émettre de nouveaux états exécutoires au titre des périodes en litige après avoir régulièrement publié ces tarifs.,,,b) En revanche, VNF n'est pas fondé à se prévaloir, devant le juge administratif, d'un enrichissement sans cause de l'usager en raison de l'utilité que celui-ci aurait retirée de sa navigation sur le domaine public fluvial au titre de la période en litige, alors que la voie de droit mentionnée au a) lui est ouverte et que, dès lors, celle de l'enrichissement sans cause ne peut l'être.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, 28 avril 2014, Mme,et autres, n° 357090, à publier au Recueil.,,[RJ2] Cf. CE, Section, 27 juillet 1984, Commune de la Teste de Buch, n° 34860, p. 282.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
