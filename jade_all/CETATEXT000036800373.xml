<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800373</ID>
<ANCIEN_ID>JG_L_2018_04_000000407899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/03/CETATEXT000036800373.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 13/04/2018, 407899</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407899.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...D...a porté plainte contre Mme B... C...devant la chambre disciplinaire de première instance des Antilles-Guyane de l'ordre des médecins. Par une décision du 9 août 2016, la chambre disciplinaire de première instance a infligé à Mme C...la sanction d'interdiction d'exercer la médecine pendant une durée de trois mois. <br/>
<br/>
              Par une ordonnance n° 13342 du 16 décembre 2016, le président de la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par Mme C... contre cette décision et a précisé que la sanction d'interdiction d'exercer la médecine s'exécuterait à compter du 1er mai 2017.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 13 février et 24 août 2017, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins et de Mme D... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              -  le code de procédure civile ;<br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de MmeC..., à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de Mme D...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1.  Considérant qu'il ressort des pièces du dossier soumis à la chambre disciplinaire nationale de l'ordre des médecins que, par une décision du 9 août 2016, la chambre disciplinaire de première instance des Antilles-Guyane de l'ordre des médecins a infligé à Mme C... la sanction d'interdiction d'exercer la médecine pendant une durée de trois mois ; que Mme C...se pourvoit en cassation contre l'ordonnance du 16 décembre 2016 par laquelle le président de la chambre disciplinaire nationale a rejeté son appel comme manifestement irrecevable, au motif que sa requête était tardive ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 4126-44 du code de la santé publique : " Le délai d'appel est de trente jours à compter de la notification de la décision. (...) " ; que l'article 643 du code de procédure civile, rendu applicable à l'instance disciplinaire des médecins par les dispositions combinées des articles R. 4126-25 et R. 4126-43 du code de la santé publique, dispose : " Lorsque la demande est portée devant une juridiction qui a son siège en France métropolitaine, les délais (...) d'appel (...) sont augmentés de : / 1. Un mois pour les personnes qui demeurent (...) à la Martinique (...) " ; <br/>
<br/>
              3.  Considérant qu'il ressort des pièces du dossier soumis au juge du fond que MmeC..., qui demeure en Martinique et bénéficie à ce titre du délai de distance d'un mois prévu par l'article 643 du code de procédure civile, a reçu le 10 août 2016 la notification de la décision de la chambre disciplinaire de première instance ; qu'ainsi, en application des dispositions citées au point précédent, le délai qui lui était imparti pour former appel de cette décision expirait, compte tenu de la prolongation du délai d'appel par le délai de distance, le lundi 10 octobre 2016 à minuit ; que la requête de MmeC..., enregistrée postérieurement à cette date, a été postée à Marseille le samedi 8 octobre 2016 ; qu'ainsi, elle n'a pas été expédiée en temps utile pour parvenir au greffe de la chambre disciplinaire nationale avant l'expiration du délai d'appel ; que, dans ces conditions, le président de la chambre disciplinaire nationale n'a pas entaché son ordonnance d'erreur de droit en rejetant comme tardive cette requête; qu'il s'ensuit que le pourvoi de Mme C... doit être rejeté ;<br/>
<br/>
              4.  Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme D..., qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre Mme C...  ; que, le Conseil national de l'ordre des médecins n'ayant pas la qualité de partie dans la présente instance, ces mêmes dispositions font obstacle à ce que soient mises à sa charge la somme que demande, à leur titre, MmeC... ; qu'enfin, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de Mme C... une somme de 3 000 euros à verser au même titre à Mme D...  ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme C...est rejeté.<br/>
Article 2 : Les conclusions présentées par Mme C...et par le Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Mme C...versera à Mme D...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme C...et à Mme A... D....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-05 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. VOIES DE RECOURS. - APPEL CONTRE LA DÉCISION D'UNE CHAMBRE DISCIPLINAIRE DE PREMIÈRE INSTANCE DE L'ORDRE DES MÉDECINS - DÉLAI DE TRENTE JOURS - PROLONGATION PAR UN DÉLAI DE DISTANCE D'UN MOIS POUR LES PERSONNES DEMEURANT OUTRE-MER - COMPUTATION.
</SCT>
<ANA ID="9A"> 55-04-01-05 Aux termes de l'article R. 4126-44 du code de la santé publique (CSP), le délai d'appel contre la décision d'une chambre disciplinaire de première instance de l'ordre des médecins est de trente jours à compter de la notification. Ce délai d'appel est prolongé d'un mois par le délai de distance prévu par l'article 643 du code de procédure civile, rendu applicable à l'instance disciplinaire des médecins par les dispositions combinées des articles R. 4126-25 et R. 4126-43 du CSP.,,,Décision de la chambre disciplinaire de première instance des Antilles-Guyane, notifiée à la requérante, qui réside en Martinique, le 10 août 2016. Compte tenu de la prolongation du délai d'appel par le délai de distance, le délai qui lui était imparti pour former appel de cette décision expirait lundi 10 octobre 2016 à minuit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
