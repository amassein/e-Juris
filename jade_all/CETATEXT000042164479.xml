<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164479</ID>
<ANCIEN_ID>JG_L_2020_07_000000441466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164479.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 11/07/2020, 441466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441466.20200711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 26 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... C... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du Premier ministre du 20 juin 2020 maintenant l'interdiction de la pratique des sports de combat, telle que révélée dans le communiqué de presse du même jour ;<br/>
<br/>
              2°) d'enjoindre à l'administration de procéder à la mise en cohérence des protocoles sanitaires des pratiques sportives.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le maintien de l'interdiction litigieuse empêche la pratique normale des sports de combat et crée une situation d'inégalité entre les pratiquants de sports collectifs ;<br/>
              - il est porté une atteinte grave et manifestement illégale au principe d'égalité, à la liberté de réunion et à la liberté d'association, dès lors notamment que, d'une part, les pratiquants de sports collectifs où les contacts sont codifiés peuvent reprendre leurs activités tandis que les pratiquants de sports de combat ne peuvent le faire et, d'autre part, la décision attaquée empêche les associations de fonctionner correctement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le décret n° 2020-548 du 11 mai 2020 ;<br/>
              - le décret n° 2020-663 du 31 mai 2020 ;<br/>
              - le décret n° 2020-724 du 14 juin 2020 ;<br/>
              - le décret n° 2020-759 du 21 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. La circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée n'est pas de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulière requise par l'article L. 521-2 est satisfaite, en prenant en compte la situation du requérant et les intérêts qu'il entend défendre mais aussi l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration.<br/>
<br/>
              3. M. B... C... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la décision du Premier ministre du 20 juin 2020 maintenant l'interdiction de la pratique des sports de combat, telle que révélée dans le communiqué de presse du même jour, et d'enjoindre à l'administration de procéder à la mise en cohérence des protocoles sanitaires des pratiques sportives.<br/>
<br/>
              4. Pour justifier de l'urgence à ordonner de telles mesures, M. B... C... A... soutient que le maintien de l'interdiction litigieuse empêche la pratique normale des sports de combat et crée une situation d'inégalité entre les pratiquants de sports de combat et les pratiquants de sports collectifs, dont les activités ont été autorisées à reprendre avec des mesures de prévention adaptées.<br/>
<br/>
              5. Toutefois, eu égard, d'une part, aux circonstances exceptionnelles au vu desquelles la décision attaquée a été prise et qui ont conduit le législateur à déclarer l'état d'urgence sanitaire pour une durée de deux mois, puis à le proroger jusqu'au 10 juillet 2020 inclus, d'autre part, à l'intérêt public qui s'attache aux mesures consistant à n'autoriser la reprise de la pratique des activités sportives, et notamment des sports de combat, qui conduisent à des contacts étroits entre sportifs, que de façon progressive pour lutter contre la reprise de la propagation du virus du covid-19 pendant la période de déconfinement, et, enfin, à la conciliation entre les droits et libertés et l'objectif de valeur constitutionnelle de protection de la santé, M. B... C... A..., qui ne fait état d'aucune circonstance particulière de nature à justifier une intervention à très bref délai du juge des référés, ne remplit pas la condition d'urgence requise par l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              6. Par suite, il y a lieu de rejeter la requête de M. B... C... A..., selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... C... A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... A... B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
