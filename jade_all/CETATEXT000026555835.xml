<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555835</ID>
<ANCIEN_ID>JG_L_2012_10_000000330063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555835.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 29/10/2012, 330063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:330063.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, du ministre du budget, des comptes publics et de la réforme de l'Etat ; le ministre du budget, des comptes publics et de la réforme de l'Etat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NT02224 du 25 mai 2009 par lequel la cour administrative d'appel de Nantes a rejeté son recours tendant à l'annulation du jugement n° 04-3253 du 10 avril 2008 du tribunal administratif de Rennes en tant qu'il a fait droit à la demande de la SAS PLG Finances tendant à la réduction des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt mises à sa charge au titre des exercices 1999 et 2000 pour ce qui concerne le mode de comptabilisation du coût d'acquisition de distributeurs doseurs par la SAS  ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son recours ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SAS PLG Finances,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la SAS PLG Finances ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS , qui a pour activité la vente en gros de produits d'hygiène et de matériels de nettoyage, met gratuitement à la disposition de ses clients des distributeurs-doseurs qui permettent d'utiliser les produits d'hygiène qu'elle fournit pour des machines industrielles telles que lave-linge ou lave-vaisselle, en contrepartie d'un approvisionnement exclusif du client auprès d'elle ; que, pendant la durée de leur mise en dépôt auprès de la clientèle, la SAS  reste propriétaire de ces distributeurs ; qu'à la suite d'une vérification de comptabilité, l'administration fiscale a estimé que les distributeurs-doseurs de produits d'hygiène ainsi que la valeur de la main-d'oeuvre nécessaire à la mise en place de ces équipements, comptabilisés en tant que charges par la société, correspondaient à des immobilisations et que, par suite, les sommes correspondantes devaient être réintégrées dans les bases imposables à l'impôt sur les sociétés au titre de l'exercice clos les 31 décembre 1999 et 2000 ; qu'à la suite du rejet de ses réclamations contentieuses dirigées contre les impositions supplémentaires mises à sa charge à ce titre, la SAS  a saisi le juge de l'impôt du litige l'opposant à l'administration fiscale ; que le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt du 25 mai 2009 par lequel la cour administrative d'appel de Nantes a rejeté son recours tendant à l'annulation du jugement du 10 avril 2008 du tribunal administratif de Rennes en tant qu'il a fait droit à la demande de la SAS PLG Finances tendant à la réduction des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt mises à sa charge à raison de la réintégration dans les bases imposables à l'impôt sur les sociétés au titre de l'exercice clos les 31 décembre 1999 et 2000 des sommes correspondant à l'acquisition et à la valeur de la main d'oeuvre nécessaire à la mise en place de distributeurs doseurs ; que par un pourvoi incident formé le 5 mars 2010, la SAS PLG demande l'annulation de l'arrêt en tant que la cour administrative d'appel de Nantes a rejeté ses conclusions tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt mises à sa charge au titre de l'exercice 1999 en raison du refus de l'administration d'admettre la déduction de cotisations de retraite complémentaire ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 39 du code général des impôts, applicable en vertu de l'article 209 du même code pour la détermination des bénéfices passibles de l'impôt sur les sociétés : " 1. Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant (...) 1° Les frais généraux de toute nature, les dépenses de personnel et de main- d'oeuvre (...) " ; qu'aux termes de l'article 38 du même code, également applicable à la détermination des bénéfices passibles de l'impôt sur les sociétés : " (...) 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés. (...) " ;<br/>
<br/>
              3. Considérant que la documentation de base 4 C-221 du 30 octobre 1997, dont la SAS  s'est prévalue sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales, prévoit que " certains biens de faible valeur peuvent être admis en charges au titre de l'exercice d'acquisition dès lors que leur utilisation ne constitue pas pour l'entreprise l'objet même de son activité " ; qu'elle autorise notamment les entreprises " à comprendre parmi leurs charges immédiatement déductibles le prix d'acquisition des matériels et outillages d'une valeur unitaire hors taxe n'excédant pas 2 500 francs " en précisant que " Les matériels et outillages pouvant bénéficier de cette tolérance sont ceux qui répondent à la définition du matériel et de l'outillage à inscrire aux comptes 2154 et 2155 du plan comptable de 1982. Il s'agit en fait de l'ensemble des objets, instruments et machines avec ou par lesquels : - on extrait, transforme ou façonne les matériels ou fournitures, - on fournit les services qui sont l'objet même de la profession exercée (...) " ;<br/>
<br/>
              4. Considérant, en premier lieu, que la cour, qui ne s'est pas contentée de relever que les matériels litigieux avaient été régulièrement comptabilisés aux comptes 2154 et 2155 mais a également recherché s'ils servaient à fournir les services qui sont l'objet même de la profession exercée, a suffisamment motivé son arrêt ;<br/>
<br/>
              5. Considérant, en second lieu, que la cour a estimé par une appréciation souveraine des faits de l'espèce, sans entacher son arrêt d'inexactitude matérielle, que les distributeurs-doseurs litigieux permettaient d'utiliser les produits d'entretien fournis par l'entreprise pour des machines industrielles ; qu'elle en a exactement déduit que ces matériels concouraient à la réalisation de son activité de vente de produits d'hygiène et d'entretien, sans toutefois constituer l'objet même de celle-ci, et que, dès lors que leur valeur unitaire n'excédait pas les limites fixées par la tolérance administrative évoquée ci-dessus et qu'ils étaient régulièrement comptabilisés aux comptes 2154 et 2155, ils entraient dans les prévisions de la doctrine précitée ; qu'il suit de là, sans qu'il soit besoin d'examiner la recevabilité des moyens soulevés par le ministre, que ce dernier n'est pas fondé à soutenir que la cour aurait méconnu la portée  de la documentation de base 4 C-221 du 30 octobre 1997 et fait une inexacte application de l'article L. 80 A du livre des procédures fiscales en jugeant que les premiers juges étaient fondés à faire droit à la demande de la SAS  de réduction des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre des exercices 1999 et 2000 ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre du budget, des comptes publics et de la fonction publique n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              Sur le pourvoi incident de la SAS PLG Finances :<br/>
<br/>
              7. Considérant que, parmi les dépenses de personnel mentionnées au 1° du 1 de l'article 39 précité du code général des impôts, qui doivent avoir été engagées dans l'intérêt direct de l'entreprise, figurent les cotisations versées par l'entreprise au titre d'un régime de retraite, dans la mesure où le régime en vertu duquel ces cotisations sont versées s'applique de plein droit à l'ensemble du personnel ou à certaines catégories de celui-ci ; qu'il résulte de ces dispositions que, si le contrat de retraite complémentaire souscrit par un employeur s'applique de plein droit à la totalité ou à une catégorie déterminée de ses salariés, caractérisant de ce fait un régime collectif et impersonnel, une société est fondée à déduire de son résultat imposable les cotisations ou les primes versées en exécution de ce contrat ;<br/>
<br/>
              8. Considérant que la cour a relevé dans les motifs de son arrêt, d'une part, que la SAS , représentée par M. , qui, avec son épouse, dirigeait l'entreprise et en a détenu le capital jusqu'en 1996, a souscrit en 1987 un contrat de retraite complémentaire au profit de certains salariés, limitant le bénéfice de ce régime à l'ensemble des cadres ayant un coefficient supérieur à 550 suivant la convention collective du commerce de gros, puis à compter de 1999, aux cadres ayant une ancienneté minimum de 25 ans et, d'autre part, que le contrat a été résilié au 31 décembre 1999 après le départ de M. et Mme  de la société ; que, par une appréciation souveraine de ces éléments de faits, exempte de dénaturation, la cour a jugé que les bénéficiaires de ce contrat, apparemment déterminés selon des critères objectifs et impersonnels, avaient, en réalité, été définis de manière à ne viser que les époux   ; que les juges du fond, qui n'ont pas jugé, contrairement à ce qui est soutenu, que la circonstance qu'un nombre réduit de personnes bénéficieraient du régime de retraite complémentaire serait par elle-même de nature à retirer à celui-ci tout caractère collectif et impersonnel, n'ont pas inexactement qualifié les faits de l'espèce ni commis d'erreur de droit en déduisant de ce qui précède que ce contrat ne pouvait être regardé comme instaurant un régime collectif et impersonnel, de sorte que l'administration avait pu refuser à bon droit la déduction, au titre de l'exercice 1999, des cotisations y afférentes ; qu'il s'ensuit que la SAS PLG Finances n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions de la SAS PLG Finances présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à la SAS PLG Finances, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat et le pourvoi incident de la SAS PLF Finances sont rejetés. <br/>
Article 2 : L'Etat versera à la SAS PLF Finances une somme de 3 000 euros  au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la SAS PLG Finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
