<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375635</ID>
<ANCIEN_ID>JG_L_2020_09_000000429047</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/09/2020, 429047, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429047</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429047.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Marseille de condamner l'agence régionale de santé de Provence-Alpes-Côte d'Azur à lui verser la somme de 18 150 euros en réparation du préjudice résultant de l'absence d'indemnisation des astreintes qu'il a effectuées dans le cadre de la permanence des soins en établissement de santé. Par un jugement n° 1705967 du 17 décembre 2018, le tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 19MA00755 du 21 mars 2019, enregistrée le lendemain au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 16 février 2019 au greffe de cette cour, présenté par M. A.... Par ce pourvoi et par deux nouveaux mémoires, enregistrés les 5 juin 2019 et 6 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'agence régionale de santé de Provence-Alpes-Côte d'Azur la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et de l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2016-41 du 26 janvier 2016 ;<br/>
              - les arrêtés des 16 janvier 2012 et 18 juin 2013 relatifs aux montants et aux conditions de versement de l'indemnité forfaitaire aux médecins libéraux participant à la mission de permanence des soins en établissement de santé ;<br/>
              - l'arrêté du 27 février 2012 fixant la nature des charges relatives à la permanence des soins en établissement de santé financées par le fonds d'intervention régional en application de l'article R. 6112-28 du code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 1434-9 du code de la santé publique, dans sa rédaction applicable au litige : " Le schéma régional d'organisation des soins fixe, en fonction des besoins de la population, par territoire de santé : / (...) 4° Les missions de service public assurées par les établissements de santé (...) ". Aux termes de l'article L. 6112-1 du même code, dans sa rédaction applicable : " Les établissements de santé peuvent être appelés à assurer, en tout ou partie, une ou plusieurs des missions de service public suivantes : / 1° La permanence des soins (...) ". En vertu du neuvième alinéa de l'article L. 6112-2 du même code, dans sa rédaction applicable, le contrat pluriannuel d'objectifs et de moyens conclu entre l'agence régionale de santé et chaque établissement de santé en application de l'article L. 6114-1 du même code précise les obligations auxquelles est assujettie toute personne assurant ou contribuant à assurer une ou plusieurs des missions de service public mentionnées à l'article L. 6112-2 et, le cas échéant, les modalités de calcul de la compensation financière de ces obligations.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le groupement de coopération sanitaire Axium-Rambot a été autorisé par l'agence régionale de santé de Provence-Alpes-Côte d'Azur à exercer les activités interventionnelles sous imagerie médicale, par voie endovasculaire, en cardiologie mentionnées au 11° de l'article R. 6122-25 du code de la santé publique et a été érigé, en application de l'article L. 6133-7 du même code, en établissement de santé privé, relevant du d) de l'article L. 162-22-6 du code de la sécurité sociale. Le schéma régional d'organisation des soins pour les années 2012 à 2016, arrêté par le directeur général de l'agence régionale de santé, agissant au nom de l'Etat en vertu de l'article L. 1432-2 du code de la santé publique, et demeuré en vigueur jusqu'à la publication, dans la région, du projet régional de santé prévu par la loi du 26 janvier 2016 de modernisation de notre système de santé, dispose que le groupement assurerait une mission de service public de permanence des soins pour la spécialité de cardiologie interventionnelle. Le contrat pluriannuel d'objectifs et de moyens conclu le 31 juillet 2012 pour une durée de cinq ans entre l'agence régionale de santé, agissant au nom de l'Etat, et le groupement, demeuré en vigueur jusqu'à son échéance en vertu de l'article 99 de la loi du 26 janvier 2016, a précisé les obligations auxquelles ce dernier est assujetti au titre de cette mission. M. B... A..., anesthésiste-réanimateur exerçant à titre libéral au sein du groupement, a demandé au tribunal administratif de Marseille de condamner l'agence régionale de santé de Provence-Alpes-Côte d'Azur à lui verser la somme de 8 000 euros en réparation du préjudice résultant de l'absence d'indemnisation des astreintes qu'il a effectuées dans le cadre de la permanence des soins de cardiologie interventionnelle entre le 30 mai et le 18 décembre 2016, demande qu'il a portée à 18 150 euros en cours d'instance, compte tenu des astreintes assurées en 2017 et 2018. Par un jugement du 17 décembre 2018 contre lequel il se pourvoit en cassation, le tribunal administratif de Marseille a écarté l'argumentation qu'il présentait au regard des dispositions du schéma régional d'organisation des soins applicable jusqu'en 2017 et rejeté sa demande.<br/>
<br/>
              3. D'une part, aux termes de l'article L. 1435-8 du code de la santé publique : " Un fonds d'intervention régional finance, sur décision des agences régionales de santé, des actions, des expérimentations et, le cas échéant, des structures concourant : / (...) 3° A la permanence des soins (...) / Les financements alloués aux établissements de santé (...) au titre du fonds d'intervention régional ainsi que les engagements pris en contrepartie sont inscrits et font l'objet d'une évaluation dans le cadre des contrats pluriannuels d'objectifs et de moyens mentionnés " à l'article L. 6114-1 du même code. Aux termes de l'article R. 6112-28 de ce code, puis de l'article R. 6111-49 qui en a repris les dispositions, dans leur rédaction applicable au litige : " La participation des établissements à la permanence des soins peut être prise en charge financièrement par le fonds d'intervention régional mentionné à l'article L. 1435-8. / En outre, pour sa participation à la permanence des soins assurée par un établissement mentionné au d de l'article L. 162-22-6 du code de la sécurité sociale, le médecin libéral qui exerce une spécialité médicale répertoriée dans le contrat mentionné au neuvième alinéa de l'article L. 6112-2 et selon les conditions fixées par ce contrat peut être indemnisé par le fonds d'intervention régional. / Les ministres de la santé et de la sécurité sociale arrêtent : (...) / 2° Les conditions d'indemnisation des médecins mentionnés au deuxième alinéa ". Par l'arrêté du 27 février 2012 fixant la nature des charges relatives à la permanence des soins en établissement de santé financées par le fonds d'intervention régional en application de l'article R. 6112-28 du code de la santé publique, les ministres chargés de la santé et de la sécurité sociale ont prévu que pouvait être prise en charge à ce titre la rémunération ou l'indemnisation des médecins hors structures d'urgence pour l'accueil suivi de la prise en charge des patients la nuit, pendant le week-end, à l'exception du samedi matin, et les jours fériés. Ils ont fixé les conditions de cette indemnisation par leur arrêté du 16 janvier 2012 puis du 18 juin 2013 relatifs aux montants et aux conditions de versement de l'indemnité forfaitaire aux médecins libéraux participant à la mission de permanence des soins en établissement de santé, dont l'article 2 précise que les engagements, obligations et modalités de mise en oeuvre de la mission de service public définis au contrat pluriannuel d'objectifs et de moyens sont exécutés au sein de l'établissement dans le cadre de contrats tripartites d'accomplissement de la mission de service public de permanence des soins en établissement de santé conclus entre l'agence régionale de santé, l'établissement et les médecins qui s'engagent à y participer.<br/>
<br/>
              4. D'autre part, en vertu de l'article R. 6123-132 du code de la santé publique, l'autorisation d'exercer les activités interventionnelles sous imagerie médicale, par voie endovasculaire, en cardiologie n'est accordée, pour les actes portant sur les cardiopathies de l'adulte autres que les actes de rythmologie interventionnelle, de stimulation multisites et de défibrillation, " que si le demandeur s'engage à les pratiquer vingt-quatre heures sur vingt-quatre tous les jours de l'année et à assurer la permanence des soins (...) ". Il résulte en outre de l'article D. 6124-181 du même code, relatif aux conditions techniques de fonctionnement de ces activités, qu'à la demande du médecin qui prescrit ou qui réalise l'acte interventionnel, sous imagerie médicale, par voie endovasculaire, en cardiologie " un médecin anesthésiste-réanimateur est en mesure d'intervenir lors de la prescription et de la réalisation de l'acte ", y compris en cas d'urgence.<br/>
<br/>
              5. Il résulte de ces dispositions que la permanence des soins de cardiologie interventionnelle implique l'organisation d'un service d'astreintes non seulement pour les médecins cardiologues mais également pour les médecins anesthésistes-réanimateurs. Le tribunal administratif de Marseille a ainsi commis une erreur de droit en jugeant que le directeur général de l'agence régionale de santé de Provence-Alpes-Côte d'Azur, qui avait prévu, dans le schéma régional d'organisation des soins pour les années 2012 à 2016, la participation de médecins cardiologues à la mission de permanence des soins de cardiologie interventionnelle assurée par le groupement de coopération sanitaire Axium-Rambot, avait pu, sans méconnaître les dispositions de l'article D. 6124-181 du code de la santé publique, s'abstenir de prévoir celle de médecins anesthésistes-réanimateurs. Il a, en outre, insuffisamment motivé son jugement en ne recherchant pas, ainsi qu'il y était invité par le requérant, si le contrat pluriannuel d'objectifs et de moyens conclu entre l'agence régionale de santé et le groupement n'était pas entaché d'une semblable illégalité dans la définition, sur le fondement de l'article L. 6114-1 du code de la santé publique, des modalités de calcul de la compensation financière de la participation à la permanence des soins.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le jugement attaqué doit être annulé.<br/>
<br/>
              7. Si les agences régionales de santé sont, aux termes de l'article L. 1432-1 du code de la santé publique, des établissements publics distincts de l'Etat, les compétences qui leur sont confiées par l'article L. 1431-2 de ce code, parmi lesquelles l'établissement du schéma régional d'organisation des soins et la conclusion avec les établissements de santé des contrats pluriannuels d'objectifs et de moyens mentionnés à l'article L. 6114-1 du même code, sont, en vertu de l'article L. 1432-2 de ce code, exercées par leur directeur général au nom de l'Etat, sauf lorsqu'elles ont été attribuées à une autre autorité au sein de ces agences. Par suite, les conclusions de M. A..., qui tendent à ce qu'une somme soit mise à la charge de l'agence régionale de santé de Provence-Alpes-Côte d'Azur au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont mal dirigées et ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 17 décembre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 : Les conclusions présentées par M. A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., au ministre des solidarités et de la santé et à l'agence régionale de santé de Provence-Alpes-Côte d'Azur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
