<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448282</ID>
<ANCIEN_ID>JG_L_2011_07_000000324126</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/82/CETATEXT000024448282.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 28/07/2011, 324126, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324126</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:324126.20110728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 janvier et 14 avril 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE LA GARDE (Var), représentée par son maire ; la COMMUNE DE LA GARDE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06MA02519 du 13 novembre 2008 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé le jugement n° 0502027 du 1er juin 2006 du tribunal administratif de Nice rejetant la demande de M. Jean-Pierre A et Mme Chantal B dirigée contre la décision du maire de La Garde en date du 29 avril 2002 fixant le montant de leur participation au programme d'aménagement d'ensemble de la zone d'aménagement concerté de La Planquette et le titre exécutoire émis, en exécution de cette décision, le 23 décembre 2004 par la trésorerie principale de La Valette et, d'autre part, fait droit à la demande présentée par M. A et Mme B devant le tribunal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M. A et Mme B devant la cour administrative d'appel de Marseille ;<br/>
<br/>
              3°) de mettre à la charge de M. A et Mme B le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE LA GARDE et de la SCP Waquet, Farge, Hazan, avocat de M. A et de Mme B, <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE LA GARDE et à la SCP Waquet, Farge, Hazan, avocat de M. A et de Mme B ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la COMMUNE DE LA GARDE a institué un programme d'aménagement d'ensemble de la zone d'aménagement concerté de La Planquette par une délibération du 6 novembre 1992, qui indique les équipements publics à réaliser dans ce cadre et détermine les modalités de calcul de la participation des constructeurs au financement de ces équipements prévue par l'article L. 332-9 du code de l'urbanisme ; que, par des délibérations du 12 mars 1993 et du 25 septembre 1997, la COMMUNE DE LA GARDE a procédé à une nouvelle répartition, entre les constructeurs, du financement des dépenses de réalisation des équipements prévus, à partir de nouveaux critères ; que, par un arrêt du 13 novembre 2008 se fondant sur l'illégalité des délibérations instituant la participation en litige, la cour administrative d'appel de Marseille a d'une part, annulé le jugement du 1er juin 2006 du tribunal administratif de Nice rejetant la demande de M. A et Mme B dirigée contre les dispositions du permis de construire qui leur avait été délivré par le maire de La Garde le 29 avril 2002 en tant qu'elles fixaient le montant de leur participation ainsi que contre le titre exécutoire émis, par voie de conséquence, le 23 décembre 2004 par la trésorerie principale de La Valette, et fait droit à leur demande de décharge ; que la commune se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Considérant, en premier lieu, que, contrairement à ce que soutient la commune, la cour n'était pas tenue de répondre à l'ensemble des arguments présentés à l'appui des moyens de la requête dont elle était saisie ; que, par suite, le moyen tiré de l'insuffisance de motivation de l'arrêt attaqué peut être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article L. 332-9 du code de l'urbanisme : "Dans les secteurs de la commune où un programme d'aménagement d'ensemble a été approuvé par le conseil municipal, il peut être mis à la charge des constructeurs tout ou partie du coût des équipements publics réalisés pour répondre aux besoins des futurs habitants ou usagers des constructions à édifier dans le secteur concerné. Lorsque la capacité des équipements programmés excède ces besoins, seule la fraction du coût proportionnelle à ces besoins peut être mise à la charge des constructeurs. Lorsqu'un équipement doit être réalisé pour répondre aux besoins des futurs habitants ou usagers des constructions à édifier dans plusieurs opérations successives devant faire l'objet de zones d'aménagement concerté ou de programmes d'aménagement d'ensemble, la répartition du coût de ces équipements entre différentes opérations peut être prévue dès la première, à l'initiative de l'autorité publique qui approuve l'opération. / Dans les communes où la taxe locale d'équipement est instituée, les constructions édifiées dans ces secteurs sont exclues du champ d'application de la taxe. / Le conseil municipal détermine le secteur d'aménagement, la nature, le coût et le délai prévus pour la réalisation du programme d'équipements publics. Il fixe, en outre, la part des dépenses de réalisation de ce programme qui est à la charge des constructeurs, ainsi que les critères de répartition de celle-ci entre les différentes catégories de constructions (...)" ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions que la délibération du conseil municipal instituant un plan d'aménagement d'ensemble et mettant à la charge des constructeurs une participation au financement des équipements publics à réaliser doit identifier avec précision les aménagements prévus ainsi que leur coût prévisionnel et déterminer la part de ce coût mise à la charge des constructeurs, afin de permettre le contrôle du bien-fondé du montant de la participation mise à la charge de chaque constructeur ; que ces dispositions impliquent également, afin de permettre la répartition de la participation entre les constructeurs, que la délibération procède à une estimation quantitative des surfaces dont la construction est projetée à la date de la délibération et qui serviront de base à cette répartition ; qu'il appartient enfin au conseil municipal de modifier en tant que de besoin les critères de calcul de la participation des constructeurs pour tenir compte d'éventuels écarts constatés entre les programmes d'équipements publics et leur réalisation effective, ainsi qu'entre les prévisions de constructions privées et leur réalisation effective ;<br/>
<br/>
              Considérant que la cour n'a pas dénaturé les faits qui lui étaient soumis en relevant que les termes des délibérations du conseil municipal de la COMMUNE DE LA GARDE ne permettaient pas de connaître la nature et le nombre des aménagements paysagers à réaliser et des salles de sport à construire et que ces délibérations ne procédaient pas à une estimation quantitative de la surface des immeubles dont la construction était envisagée ; qu'en se fondant sur ces circonstances pour en déduire que ces délibérations ne permettaient pas la vérification du bien-fondé des conditions de répartition des dépenses d'aménagement entre les constructeurs conformément aux dispositions précitées de l'article L. 332-9 du code de l'urbanisme et pour annuler en conséquence les dispositions du permis de construire du 29 avril 2002 mettant à la charge de M. A et Mme B une participation aux dépenses d'aménagement de la zone de La Planquette ainsi que le titre exécutoire du 23 décembre 2004, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, toutefois, en troisième lieu, que, contrairement à ce que soutiennent en défense M. A et Mme B, le moyen soulevé par la COMMUNE DE LA GARDE tiré de ce que la cour a commis une erreur de droit en fixant le point de départ des intérêts moratoires au 14 avril 2005, date de saisine du tribunal administratif de Nice, est né de l'arrêt attaqué et peut donc être soulevé pour la première fois devant le juge de cassation ; qu'en retenant cette date comme point de départ des intérêts moratoires, alors que les requérants n'avaient pas encore payé la participation en litige, la cour a entaché son arrêt d'erreur de droit ; que, dès lors, son arrêt doit être annulé en tant qu'il fixe le point de départ des intérêts moratoires au 14 avril 2005 ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de M. A et Mme B, qui ne sont pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la COMMUNE DE LA GARDE et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la COMMUNE DE LA GARDE le versement à M. A et Mme B de la somme globale de 1 500 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 13 novembre 2008 est annulé en tant qu'il fixe le point de départ des intérêts moratoires au 14 avril 2005.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la COMMUNE DE LA GARDE est rejeté.<br/>
Article 4 : La COMMUNE DE LA GARDE versera à M. A et Mme B une somme globale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE LA GARDE, à M. Jean-Pierre A et à Mme Chantal B.<br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
