<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043511401</ID>
<ANCIEN_ID>J0_L_2021_05_000001902866</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/51/14/CETATEXT000043511401.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 1ère chambre, 11/05/2021, 19VE02866, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-11</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>19VE02866</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BEAUJARD</PRESIDENT>
<AVOCATS>SELARL MDMH</AVOCATS>
<RAPPORTEUR>Mme Odile  DORION</RAPPORTEUR>
<COMMISSAIRE_GVT>M. MET</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       Par une ordonnance du 16 juin 2016, le président de la section du contentieux du Conseil d'Etat a transmis au tribunal administratif de Cergy-Pontoise la demande de M. A... C... tendant à l'annulation de la décision du 21 avril 2016 par laquelle le ministre de la défense a, après avis de la commission de recours des militaires, partiellement rejeté sa demande de révision de sa notation au titre de l'année 2015.<br/>
<br/>
       Par un jugement n° 1606005 du 6 juin 2019, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête, enregistrée le 5 août 2019, M. C..., représenté par Me Maumont, avocat, demande à la cour :<br/>
<br/>
       1° d'annuler le jugement attaqué ;<br/>
<br/>
       2° d'annuler la décision du 21 avril 2016 du ministre de la défense en tant qu'elle ne lui donne pas entière satisfaction ;<br/>
<br/>
       3° d'enjoindre à la ministre des armées d'établir un nouveau bulletin de notation au titre de l'année 2015, conforme à ses mérites ;<br/>
<br/>
       4° de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Il soutient que :<br/>
       - sa notation est entachée d'erreur de fait et d'erreur manifeste d'appréciation en ce qu'elle fait apparaître, par rapport à ses notations antérieures qui étaient élogieuses, une soudaine régression inexpliquée ; l'appréciation littérale présente un caractère subjectif, ainsi que le reconnaît la ministre des armées dans la décision contestée ; les incidents invoqués ne justifient pas un tel abaissement de sa notation ; celle-ci fait suite à des rapports hiérarchiques injustes et des poursuites disciplinaires iniques qui révèlent un acharnement de sa hiérarchie à son encontre ; les appréciations littérales et l'évaluation de ses qualités sont entachées de contrariété en ce qui concerne tant l'évaluation des compétences que la qualité des services rendus ;<br/>
       - sa notation procède d'un détournement de pouvoir, dès lors qu'il a subi des agissements excédant l'exercice normal du pouvoir hiérarchique et constitutifs de harcèlement moral et que l'abaissement de sa notation traduit la volonté de le sanctionner.<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu : <br/>
       - le code de la défense ;<br/>
       - les décrets n° 2020-1404 et n° 2020-1405 du 18 novembre 2020 ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de Mme B... ;<br/>
       - et les conclusions de M. Met, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. M. C..., militaire de carrière depuis le 1er avril 1984, sous-officier de l'armée de terre depuis le 1er juillet 1988, adjudant-chef depuis le 1er janvier 2008, a été affecté au détachement Terre Antilles du 33ème régiment d'infanterie de marine (RIMa) basé en Martinique, du 2 novembre 2012 au 1er août 2015. Il relève appel du jugement du 6 juin 2019 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa demande d'annulation de la décision du 21 avril 2016 de la ministre des armées statuant, après avis de la commission de recours des militaires, sur sa demande de révision de sa notation au titre de l'année 2015, en tant qu'elle n'a que partiellement fait droit à sa demande.<br/>
<br/>
       2. Aux termes de l'article L. 4135-1 du code de la défense : " Les militaires sont notés au moins une fois par an. La notation est traduite par des notes et des appréciations qui sont obligatoirement communiquées chaque année aux militaires. (...) ". Aux termes de l'article R. 4135-1 du même code : " La notation est une évaluation par l'autorité hiérarchique des qualités morales, intellectuelles et professionnelles du militaire, de son aptitude physique, de sa manière de servir pendant une période déterminée et de son aptitude à tenir dans l'immédiat et ultérieurement des emplois de niveau plus élevé ". L'article R. 4135-2 de ce code précise que : " La notation est traduite : / 1° Par des appréciations générales, qui doivent notamment comporter les appréciations littérales données par l'une au moins des autorités chargées de la notation ; / 2° Par des niveaux de valeur ou par des notes chiffrées respectivement déterminés selon une échelle ou selon une cotation définie, dans chaque armée ou formation rattachée, en fonction des corps qui la composent. (...) ". Il résulte de ces dispositions que la notation d'un militaire constitue une appréciation par l'autorité hiérarchique des qualités et des aptitudes dont il a fait preuve pendant la période de notation.<br/>
<br/>
       3. En premier lieu, M. C... fait valoir qu'au regard des appréciations très favorables portées sur sa manière de servir les années antérieures, sa notation au titre de l'année 2015 traduit un revirement soudain et inexpliqué, et que ce bulletin de notation est empreint d'appréciations subjectives, ainsi que le confirme, selon lui, la révision partielle portant sur la formulation de certaines appréciations littérales. Il ressort toutefois des pièces du dossier qu'au cours de la période de notation du 1er juin 2014 au 31 mai 2015, M. C... a été reçu au rapport hiérarchique de son commandant d'unité à raison de trois incidents survenus en août et octobre 2014 et qu'il a fait l'objet d'une première sanction, le 9 janvier 2015, de sept jours d'arrêts assortis d'un sursis de six mois, pour avoir, en décembre 2014, manqué à son devoir de réserve en diffusant des articles polémiques, et d'une seconde sanction le 25 juin 2015, de sept jours d'arrêts révoquant le sursis précédent pour avoir, en mai 2015, divulgué des informations confidentielles relatives aux procédures de notation en cours. M. C... n'a pas contesté la première sanction et les recours formés par l'intéressé contre la seconde sanction ont été rejetés. Ces faits étaient de nature à justifier qu'aient notamment été jugées perfectibles ses compétences en matière d'adhésion à l'institution, de discipline, de rigueur formelle, de jugement, de capacité à convaincre, d'esprit de cohésion, et de maîtrise de soi. L'appréciation littérale révisée de l'autorité notant au premier degré, selon laquelle " impliqué dans sa fonction, l'ADC C... fait preuve de polyvalence et de disponibilité, qualités nécessaires dans l'environnement complexe du régiment. Pédagogue confirmé, il a pris en charge la formation secourisme et entretient de bons rapports avec le soutien, facilitant grandement la prise en compte des besoins du 33. En revanche, il fait preuve d'un défaut de stabilité dans ses rapports hiérarchiques, provoquant une certaine confusion dans la compagnie ", salue les qualités professionnelles de l'intéressé et tient compte, dans des termes particulièrement mesurés, des incidents survenus au cours de la période de notation. Par suite, et alors même que la qualité de service rendus appréciée en B " Très bon " au titre de l'année précédente, a été rétrogradée en C " Bon " au titre de l'année en litige, et que ses compétences ont été jugées " perfectibles " sur plusieurs critères, la décision attaquée ne peut être regardée comme entachée d'erreur de fait, ni d'une erreur manifeste d'appréciation.<br/>
<br/>
       4. En second lieu, si M. C... a pu avoir des relations tendues avec sa hiérarchie, il ne ressort pas de la décision attaquée, ni des autres pièces du dossier, qu'il ait été victime d'agissements excédant l'exercice normal du pouvoir hiérarchique, de nature à caractériser un détournement de pouvoir ou de procédure, des faits de harcèlement ou une sanction déguisée.<br/>
<br/>
       5. Il résulte de ce qui précède que M. C... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa demande. Il s'ensuit que sa requête doit être rejetée, y compris ses conclusions afin d'injonction et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       DECIDE :<br/>
<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
2<br/>
N° 19VE02866<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-06-01 Fonctionnaires et agents publics. Notation et avancement. Notation.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
