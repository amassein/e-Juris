<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752999</ID>
<ANCIEN_ID>JG_L_2013_07_000000364363</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 25/07/2013, 364363, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364363</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:364363.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Lynet, dont le siège est ZAC de l'Aumônerie à Saint-Jean d'Angely (17400), représentée par son président directeur général en exercice ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1474 T du 11 septembre 2012 par laquelle la Commission nationale d'aménagement commercial a accordé à la SCI TVB La Grenoblerie l'autorisation préalable requise en vue de créer un magasin spécialisé dans la vente d'articles de bricolage, d'articles de jardinage (hors végétaux) et de matériaux de construction d'une surface de vente de 7 700 m², au sein de la zone de La Grenoblerie à Saint-Jean d'Angely (Charente-Maritime) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 juin 2013, présentée par la société Lynet ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la légalité externe de l'autorisation attaquée :<br/>
<br/>
              1. Considérant qu'aux termes du quatrième alinéa de l'article R. 752-51 du code de commerce : " Le commissaire du gouvernement recueille les avis des ministres intéressés, qu'il présente à la commission nationale " ; qu'aux termes du deuxième alinéa de l'article R. 752-16 du même code : " Pour les projets d'aménagement commercial, l'instruction des demandes est effectuée conjointement par les services territorialement compétents chargés du commerce ainsi que ceux chargés de l'urbanisme et de l'environnement. " ;<br/>
<br/>
              2. Considérant qu'il résulte de la combinaison de ces dispositions que les ministres intéressés, au sens de l'article R. 752-51 du code de commerce, sont ceux qui ont autorité sur les services chargés d'instruire les demandes, soit les ministres en charge du commerce, de l'urbanisme et de l'environnement ; qu'en l'espèce, il ressort des pièces du dossier que les avis de ces ministres, qui sont signés par des personnes dûment habilitées à cet effet, ont bien été présentés à la commission ; qu'en particulier, M. A...bénéficiait d'une délégation de signature du directeur général de la compétitivité, de l'industrie et des services à l'effet de signer tous actes, arrêtés et décisions, marchés ou conventions, dans la limite des attributions du service du tourisme, du commerce, de l'artisanat et des services, qui n'était pas devenue caduque du fait du changement du ministre sous l'autorité duquel était placée la direction générale concernée ; que le moyen tiré de ce que l'avis d'autres ministres n'a pas été recueilli est inopérant ;<br/>
<br/>
              Sur l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes du troisième alinéa de l'article 1er de la loi du 27 décembre 1973 : " Les pouvoirs publics veillent à ce que l'essor du commerce et de l'artisanat permette l'expansion de toutes les formes d'entreprises, indépendantes, groupées ou intégrées, en évitant qu'une croissance désordonnée des formes nouvelles de distribution ne provoque l'écrasement de la petite entreprise et le gaspillage des équipements commerciaux et ne soit préjudiciable à l'emploi " ; qu'aux termes de l'article L. 750-1 du code de commerce, dans sa rédaction issue de la loi du 4 août 2008 de modernisation de l'économie : " Les implantations, extensions, transferts d'activités existantes et changements de secteur d'activité d'entreprises commerciales et artisanales doivent répondre aux exigences d'aménagement du territoire, de la protection de l'environnement et de la qualité de l'urbanisme. Ils doivent en particulier contribuer au maintien des activités dans les zones rurales et de montagne ainsi qu'au rééquilibrage des agglomérations par le développement des activités en centre-ville et dans les zones de dynamisation urbaine. / Dans le cadre d'une concurrence loyale, ils doivent également contribuer à la modernisation des équipements commerciaux, à leur adaptation à l'évolution des modes de consommation et des techniques de commercialisation, au confort d'achat du consommateur et à l'amélioration des conditions de travail des salariés " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 752-6 du même code, issu de la même loi du 4 août 2008 : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d'évaluation sont : / 1° En matière d'aménagement du territoire : / a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; / b) L'effet du projet sur les flux de transport ; / c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; / 2° En matière de développement durable : / a) La qualité environnementale du projet ; / b) Son insertion dans les réseaux de transports collectifs. " ; <br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce ; <br/>
<br/>
              6. Considérant, en premier lieu, s'agissant de l'objectif d'aménagement du territoire, qu'il ressort des pièces du dossier que le projet prendra place dans la zone commerciale de la Grenoblerie en cours de développement, dans laquelle il contribuera à diversifier l'offre commerciale, améliorant ainsi le confort d'achat des consommateurs sans porter atteinte à l'animation de la vie urbaine ; que le flux additionnel de transport induit par le projet demeurera limité, étant en partie commun aux autres activités implantées dans les zones de la Sacristinerie et de la Grenoblerie ; <br/>
<br/>
              7. Considérant, en second lieu, s'agissant de l'objectif de développement durable, qu'il ressort des pièces du dossier que le projet fait l'objet d'aménagements paysagers et se caractérise par l'implantation de nombreux arbres ; que les autorisations d'aménagement commercial et les autorisations délivrées en application du code de l'urbanisme relèvent de législations distinctes et sont régies par des procédures indépendantes, en sorte que la société requérante ne saurait utilement se prévaloir de l'insuffisance du traitement architectural de la construction au regard de la dérogation demandée aux dispositions de l'article L. 111-1-4 du code de l'urbanisme ; que le traitement des eaux usées comme des eaux pluviales est prévu ; que des cheminements particuliers permettront d'assurer l'accès sécurisé du site aux cyclistes et aux piétons ; que si le magasin n'est pas desservi par les réseaux de transport en commun et par des pistes cyclables, cette circonstance ne justifie pas, à elle seule, le refus de l'autorisation sollicitée ; que, dès lors, la commission n'a pas fait une inexacte application des dispositions précitées du code de commerce en confirmant l'autorisation que la commission départementale avait accordée ;<br/>
<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la requête présentée par la société Lynet doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la société Lynet, en application des mêmes dispositions, la somme de 3 000 euros à verser à la société TVB La Grenoblerie au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête présentée par la société Lynet est rejetée.<br/>
Article 2 : La société Lynet versera à la société TVB La Grenoblerie la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Lynet, à la société TVB La Grenoblerie et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
