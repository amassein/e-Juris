<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998390</ID>
<ANCIEN_ID>JG_L_2014_12_000000364774</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/83/CETATEXT000029998390.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/12/2014, 364774, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364774</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Frédéric Béreyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364774.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
       La section française de l'observatoire international des prisons (OIP) a demandé au tribunal administratif de Versailles d'annuler la décision implicite par laquelle le directeur de la maison d'arrêt de Fleury-Mérogis a rejeté sa demande du 30 juillet 2007 tendant à ce qu'il soit immédiatement mis fin à l'utilisation des quartiers disciplinaires de cet établissement. Par un jugement n° 0711008 du 8 juillet 2010, le tribunal administratif de Versailles a rejeté les conclusions de première instance.<br/>
<br/>
       Faisant partiellement droit, par les articles 1er, 2 et 4 d'un arrêt n° 10VE03029 du 23 octobre 2012, à l'appel interjeté par la section française de l'OIP, la cour administrative d'appel de Versailles a, respectivement, annulé le jugement n° 0711008 du tribunal administratif de Versailles, dit n'y avoir plus lieu de statuer sur les conclusions aux fins d'annulation en tant qu'elles portaient sur la période postérieure à l'année 2008 et rejeté le surplus de ces conclusions. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
       Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 décembre 2012, 25 mars 2013 et 13 février 2014 au secrétariat du contentieux du Conseil d'Etat, la section française de l'OIP demande au Conseil d'Etat :<br/>
<br/>
       1°) d'annuler les articles 2 et 4 de l'arrêt de la cour administrative d'appel de Versailles ;<br/>
<br/>
       2°) réglant, dans cette mesure, l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment ses articles 3 et 8 ; <br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Béreyziat, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la section française de l'observatoire international des prisons.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que l'association dénommée section française de l'observatoire international des prisons a demandé au directeur de la maison d'arrêt de Fleury-Mérogis, par un courrier daté du 30 juillet 2007, que soit immédiatement mis fin à l'utilisation des quartiers disciplinaires de cet établissement respectivement réservés à la détention des hommes et des femmes ; que cette demande a fait naître une décision implicite de rejet ; que l'association a contesté cette décision pour excès de pouvoir devant le tribunal administratif de Versailles, qui a rejeté sa demande par un jugement du 8 juillet 2010 ; que faisant partiellement droit à l'appel formé par l'association contre ce jugement, par les articles 1er à 4 d'un arrêt du 23 octobre 2012, la cour administrative d'appel de Versailles a, respectivement, annulé ce jugement, dit n'y avoir plus lieu de statuer sur les conclusions aux fins d'annulation en tant qu'elles portaient sur la période postérieure à l'année 2008, mis à la charge de l'Etat certains frais et dépens et rejeté le surplus des conclusions d'excès de pouvoir ; que l'association se pourvoit en cassation contre les articles 2 et 4 de cet arrêt ; <br/>
<br/>
              Sur les conclusions relatives au quartier disciplinaire réservé aux hommes :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que ceux des locaux visés par la demande de la section française de l'observatoire international des prisons qui étaient réservés aux hommes ont cessé de recevoir des détenus en cours d'instance ; que le pourvoi de l'association requérante, qui ne peut plus donner lieu à aucune mesure d'exécution sur ce point, est ainsi devenu sans objet en tant qu'il se rapporte au quartier disciplinaire réservé aux hommes de la maison d'arrêt de Fleury-Mérogis ; qu'il n'y a, dès lors, plus lieu d'y statuer dans cette mesure ; <br/>
<br/>
              Sur les conclusions relatives au quartier disciplinaire réservé aux femmes :<br/>
<br/>
              3. Considérant, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que le nouveau quartier disciplinaire livré après travaux à la maison d'arrêt de Fleury-Mérogis était et demeure exclusivement réservé à la détention des hommes ; qu'il suit de là qu'en se fondant sur la seule livraison de ce nouveau quartier pour juger qu'avait été mis fin à l'utilisation de l'ensemble des locaux visés par la demande de l'association et en déduire, notamment, qu'étaient pour partie privées d'objet celles des conclusions en excès de pouvoir qui portaient sur le quartier disciplinaire réservé aux femmes, la cour a entaché son arrêt de dénaturation et d'erreur de droit ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des pièces du dossier soumis aux juges du fond qu'au soutien des conclusions rejetées par la cour, l'association requérante avait fait valoir que la décision litigieuse méconnaissait les stipulations des articles 3 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'il ressort des énonciations de l'arrêt attaqué que, pour écarter ce moyen, la cour s'est fondée sur la circonstance que la décision litigieuse revêtait un caractère réglementaire ; que la cour en a déduit que cette décision n'était pas susceptible, en elle-même, de porter atteinte à une liberté individuelle ; qu'en statuant ainsi, la cour a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'association requérante est fondée à demander l'annulation des articles 2 et 4 de l'arrêt qu'elle attaque, en tant qu'ils se rapportent au quartier disciplinaire de la maison d'arrêt de Fleury-Mérogis réservé à la détention des femmes ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer immédiatement sur les conclusions à fin d'annulation présentées par la section française de l'OIP devant le tribunal administratif de Versailles ;<br/>
<br/>
              7. Considérant que l'association requérante soutient, sans être contredite sur ce point, que la décision implicite de rejet prise par le directeur de la maison d'arrêt de Fleury-Mérogis est intervenue le 6 octobre 2007 ; que cette décision a notamment eu pour objet et pour effet de permettre, depuis cette date, le placement de détenues dans le quartier disciplinaire de cette maison d'arrêt réservé aux femmes, selon les conditions prévues à l'article D. 251-3 du code de procédure pénale, aux termes duquel, dans sa rédaction alors applicable : " La mise en cellule disciplinaire prévue par les articles D. 251 (5°) et D. 251-1-2 consiste dans le placement du détenu dans une cellule aménagée à cet effet et qu'il doit occuper seul. La sanction emporte pendant toute sa durée la privation d'achats en cantine prévue à l'article D. 251 (3°) ainsi que la privation des visites et de toutes les activités sous réserve des dispositions de l'article D. 251-1-2 relatifs aux mineurs de plus de seize ans. Toutefois, les détenus placés en cellule disciplinaire font une promenade d'une heure par jour dans une cour individuelle. La sanction n'emporte en outre aucune restriction à leur droit de correspondance écrite. (...). Pour les détenus majeurs, la durée de la mise en cellule disciplinaire ne peut excéder quarante-cinq jours pour une faute disciplinaire du premier degré, trente jours pour une faute disciplinaire du deuxième degré, et quinze jours pour une faute disciplinaire du troisième degré " ; <br/>
<br/>
              8. Considérant, d'une part, qu'aux termes de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Nul ne peut être soumis à la torture ni à des peines ou traitements inhumains ou dégradants " ; qu'il en résulte, comme en dispose l'article D. 189 du code de procédure pénale, dans sa version applicable au litige, et comme le rappelle désormais l'article 22 de la loi du 24 novembre 2009, que tout prisonnier a droit à être détenu dans des conditions conformes à la dignité humaine, de sorte que les modalités d'exécution des mesures prises ne le soumettent pas à une épreuve qui excède le niveau inévitable de souffrance inhérent à la détention ; qu'en raison de la situation d'entière dépendance des personnes détenues vis-à-vis de l'administration pénitentiaire, l'appréciation du caractère attentatoire à la dignité des conditions de détention dépend notamment de la vulnérabilité de ces personnes, appréciée compte tenu de leur âge, de leur état de santé, de leur handicap et de leur personnalité, ainsi que de la nature et de la durée des manquements constatés et des motifs susceptibles de justifier ces manquements eu égard aux exigences qu'impliquent le maintien de la sécurité et du bon ordre dans les établissements pénitentiaires, la prévention de la récidive et la protection de l'intérêt des victimes ;<br/>
<br/>
              9. Considérant, d'autre part, qu'il ressort des pièces du dossier qu'à la demande de la section française de l'OIP, un expert a été désigné le 24 janvier 2007 par le juge des référés du tribunal administratif de Versailles, sur le fondement de l'article R. 531-1 du code de justice administrative, aux fins de constater l'état des cellules et des parties communes des quartiers disciplinaires en cause ; qu'il ressort de l'ensemble des pièces versées au dossier et notamment du rapport établi le 16 juillet 2007 par l'expert ainsi désigné, dont les constatations corroborent au demeurant les rapports établis aux mois de juin 2004, décembre 2005 et novembre 2007 par trois sénateurs à l'issue de l'exercice du droit de visite que les intéressés tenaient de leurs mandats parlementaires, que les parties communes ainsi que les cellules encore affectées du quartier disciplinaire en cause se trouvaient, à la date du 6 octobre 2007, dans un état d'ensemble particulièrement dégradé ; qu'une partie de ces locaux, y compris les sanitaires, étaient sujets à des infiltrations, pouvant conduire à leur inondation totale ou partielle en cas de pluie ; que, dans la très grande majorité de ces cellules, la configuration du système d'aération ne permettait pas d'assurer une ventilation satisfaisante des locaux ; que, dans la très grande majorité d'entre elles, ni l'éclairage naturel ni même la lumière artificielle n'étaient suffisants pour permettre aux détenus de lire et travailler sans altérer leur vue ; qu'enfin, l'administration pénitentiaire n'effectuait pas toutes les diligences nécessaires au maintien de la propreté des parties communes, notamment des sanitaires, locaux techniques et espaces réservés à la promenade des détenues ; <br/>
<br/>
              10. Considérant qu'il résulte des points 7 à 9 qui précèdent que, dans les circonstances particulières de l'espèce, l'exécution de la décision litigieuse du directeur de la maison d'arrêt de Fleury-Mérogis pouvait avoir pour effet d'exposer les détenues les plus vulnérables, au sens exposé au point 8, ou celles sanctionnées par les mises en cellule disciplinaire les plus longues, à des épreuves physiques et morales, d'ailleurs contraires aux règles d'hygiène et de salubrité prescrites par les articles D. 349 à D. 351 du code de procédure pénal, qui, même rapportées aux motifs susceptibles de les justifier et notamment aux exigences qu'impliquait le maintien de la sécurité et du bon ordre dans l'établissement en cause, excédaient le niveau inévitable de souffrance inhérent à la détention et étaient, dès lors, attentatoires à la dignité des intéressées ; <br/>
<br/>
              11. Considérant qu'il suit de là que l'association requérante est fondée à demander, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens soulevés en première instance et en appel, l'annulation de la décision qu'elle attaque, en tant qu'elle porte sur le quartier disciplinaire de la maison d'arrêt de Fleury-Mérogis réservé à la détention des femmes ;<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 500 euros à verser à la section française de l'OIP, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la section française de l'OIP, en tant qu'elles portent sur le quartier disciplinaire réservé aux hommes de la maison d'arrêt de Fleury-Mérogis.<br/>
Article 2 : Les articles 2 et 4 de l'arrêt de la cour administrative d'appel de Versailles du 23 octobre 2012 sont annulés, en tant qu'ils portent sur le quartier disciplinaire de cette maison d'arrêt réservé aux femmes.<br/>
Article 3 : Est annulée, en tant qu'elle porte sur le quartier disciplinaire réservé aux femmes, la décision implicite par laquelle le directeur de la maison d'arrêt de Fleury-Mérogis a rejeté la demande en date du 30 juillet 2007, présentée par la section française de l'OIP et tendant à ce que soit immédiatement mis fin au placement de détenus dans les deux quartiers disciplinaires de cet établissement.<br/>
Article 4 : L'Etat versera à la section française de l'OIP une somme de 2 500 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la section française de l'observatoire international des prisons et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
