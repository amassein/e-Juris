<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983394</ID>
<ANCIEN_ID>JG_L_2015_07_000000381979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983394.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 31/07/2015, 381979, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:381979.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif d'Amiens d'annuler la décision du 27 décembre 2011 par laquelle le recteur de l'académie d'Amiens a refusé de lui accorder le bénéfice d'une retraite anticipée avec jouissance immédiate de pension en sa qualité de père de trois enfants et à ce qu'il soit enjoint à l'administration de lui servir cette pension majorée de la bonification pour enfant. Par un jugement n° 1200616 du 25 juin 2013, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13DA01476 du 19 juin 2014, enregistré le 30 juin 2014 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Douai a transmis au Conseil d'Etat le pourvoi, enregistré le 2 septembre 2013 au greffe de cette cour, présenté par M. B.... Par ce pourvoi et par deux nouveaux mémoires, enregistrés les 30 septembre 2014 et 2 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ; <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le code des pensions civiles et militaires de retraite ; <br/>
              - la loi n° 2003-775 du 21 août 2003 ;<br/>
              - la loi n° 2004-1485 du 30 décembre 2004 ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - l'arrêt C-173/13 du 17 juillet 2014 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond, que, le 4 novembre 2011, M. B..., fonctionnaire de l'éducation nationale et père de trois enfants, a saisi son administration d'une demande de départ anticipé à la retraite avec jouissance immédiate de son droit à pension, à compter du 1er décembre 2012, sur le fondement du 3° du I de l'article L. 24 du code des pensions civiles et militaires de retraite. Cette demande a été rejetée par une décision du 27 décembre 2011 du recteur de l'académie d'Amiens au motif que l'intéressé ne remplissait pas les conditions posées par ces dispositions. M. B...a saisi le tribunal administratif d'Amiens de conclusions tendant à ce qu'il saisisse la Cour de justice de l'Union européenne d'une question préjudicielle sur la conformité au droit de l'Union européenne des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite et, après annulation de la décision contestée, à ce qu'il soit enjoint à l'administration de faire droit à sa demande de départ anticipé à la retraite et de lui accorder le bénéfice du b de l'article L. 12 du même code relatif à la bonification pour enfant. Sa demande a été rejetée par un jugement du tribunal administratif d'Amiens du 25 juin 2013. M.B..., dont le pourvoi a été transmis au Conseil d'Etat par un arrêt du 19 juin 2014 de la cour administrative d'appel de Bordeaux, se pourvoit en cassation contre ce jugement.<br/>
<br/>
              En ce qui concerne la bonification pour enfant :<br/>
<br/>
              2. Aux termes de l'article L. 12 du code des pensions civiles et militaires de retraite, dans sa rédaction issue de l'article 52 la loi du 9 novembre 2010 applicable au litige : " Aux services effectifs s'ajoutent, dans les conditions déterminées par un décret en Conseil d'Etat, les bonifications ci-après : / (...) b) Pour chacun de leurs enfants légitimes et de leurs enfants naturels nés antérieurement au 1er janvier 2004, pour chacun de leurs enfants dont l'adoption est antérieure au 1er janvier 2004 et, sous réserve qu'ils aient été élevés pendant neuf ans au moins avant leur vingt-et-unième anniversaire, pour chacun des autres enfants énumérés au II de l'article L. 18 dont la prise en charge a débuté antérieurement au 1er janvier 2004, les fonctionnaires et militaires bénéficient d'une bonification fixée à un an, qui s'ajoute aux services effectifs, à condition qu'ils aient interrompu ou réduit leur activité dans des conditions fixées par décret en Conseil d'Etat ". En vertu du 1° de l'article R. 13 du même code, dans sa version applicable au litige, le bénéfice des dispositions précitées du b de l'article L. 12 du même code est subordonné à une interruption d'activité d'une durée continue au moins égale à deux mois dans le cadre d'un congé pour maternité, d'un congé pour adoption, d'un congé parental, d'un congé de présence parentale, ou d'une disponibilité pour élever un enfant de moins de huit ans.<br/>
<br/>
              3. Aux termes de l'article 141 du traité instituant la Communauté européenne, devenu l'article 157 du traité sur le fonctionnement de l'Union européenne : " 1. Chaque État membre assure l'application du principe de l'égalité des rémunérations entre travailleurs masculins et travailleurs féminins pour un même travail ou un travail de même valeur. / 2. Aux fins du présent article, on entend par rémunération, le salaire ou traitement ordinaire de base ou minimum, et tous autres avantages payés directement ou indirectement, en espèces ou en nature, par l'employeur au travailleur en raison de l'emploi de ce dernier. L'égalité de rémunération, sans discrimination fondée sur le sexe, implique : / a) que la rémunération accordée pour un même travail payé à la tâche soit établie sur la base d'une même unité de mesure ; / b) que la rémunération accordée pour un travail payé au temps soit la même pour un même poste de travail. / (...) 4. Pour assurer concrètement une pleine égalité entre hommes et femmes dans la vie professionnelle, le principe de l'égalité de traitement n'empêche pas un État membre de maintenir ou d'adopter des mesures prévoyant des avantages spécifiques destinés à faciliter l'exercice d'une activité professionnelle par le sexe sous-représenté ou à prévenir ou compenser des désavantages dans la carrière professionnelle ". Il résulte de ces dispositions, telles qu'interprétées par la Cour de justice de l'Union européenne, que le principe d'égalité des rémunérations s'oppose non seulement à l'application de dispositions qui établissent des discriminations directement fondées sur le sexe mais également à l'application de dispositions qui maintiennent des différences de traitement entre travailleurs masculins et travailleurs féminins sur la base de critères non fondés sur le sexe dès lors que ces différences de traitement ne peuvent s'expliquer par des facteurs objectivement justifiés et étrangers à toute discrimination fondée sur le sexe et qu'il y a discrimination indirecte en raison du sexe lorsque l'application d'une mesure nationale, bien que formulée de façon neutre, désavantage en fait un nombre beaucoup plus élevé de travailleurs d'un sexe par rapport à l'autre. Par un arrêt du 17 juillet 2014, la Cour de justice de l'Union européenne, statuant sur renvoi préjudiciel de la cour administrative d'appel de Lyon, a estimé que l'article 141 doit être interprété en ce sens que, sauf à pouvoir être justifié par des facteurs objectifs étrangers à toute discrimination fondée sur le sexe, tels qu'un objectif légitime de politique sociale, et à être propre à garantir l'objectif invoqué et nécessaire à cet effet, un régime de bonification de pension tel que celui résultant des articles L. 12 et R. 13 du code des pensions civiles et militaires de retraite, en tant qu'elles prévoient la prise en compte du congé de maternité dans les conditions ouvrant droit à l'octroi de la bonification en cause, introduirait une différence de traitement entre les travailleurs féminins et les travailleurs masculins contraire à cet article. Elle a cependant rappelé que, s'il lui revenait de donner des " indications de nature à permettre à la juridiction nationale de statuer ", il revient exclusivement au juge national, seul compétent pour apprécier les faits et pour interpréter la législation nationale, de déterminer si et dans quelle mesure les dispositions concernées sont justifiées par de tels facteurs objectifs.<br/>
<br/>
              4. Si, pendant son congé de maternité, la femme fonctionnaire ou militaire conserve légalement ses droits à avancement et à promotion et qu'ainsi la maternité est normalement neutre sur sa carrière, il ressort néanmoins de l'ensemble des pièces produites devant le juge du fond et des données disponibles qu'une femme ayant eu un ou plusieurs enfants connaît, de fait, une moindre progression de carrière que ses collègues masculins et perçoit en conséquence une pension plus faible en fin de carrière. Les arrêts de travail liés à la maternité contribuent à empêcher une femme de bénéficier des mêmes possibilités de carrière que les hommes. De plus, les mères de famille ont dans les faits plus systématiquement interrompu leur carrière que les hommes, ponctuellement ou non, en raison des contraintes résultant de la présence d'un ou plusieurs enfants au foyer. Ainsi, selon les données d'une étude statistique du service des retraites de l'État produite par le ministre des finances et des comptes publics, si une femme fonctionnaire sans enfant perçoit à la fin de sa carrière une pension moyenne supérieure de 2,6 % à celle des hommes également sans enfant, les femmes avec enfants perçoivent en moyenne des pensions inférieures à celles des hommes ayant le même nombre d'enfants. Ces écarts entre les pensions perçues par les femmes et les hommes s'accroissent avec le nombre d'enfants. Les pensions des femmes fonctionnaires, rapportées à celles des hommes, sont ainsi inférieures de 9,8 % pour un enfant, de 11,5 % pour deux enfants, de 13,3 % pour trois enfants et de 23 % pour quatre enfants. Si la bonification par enfant était supprimée, les écarts passeraient à 12,7 % pour un enfant, 17,3 % pour deux enfants, 19,3 % pour trois enfants et à près de 30 % pour quatre enfants. Le niveau de la pension ainsi constaté des femmes ayant eu des enfants résulte d'une situation passée, consécutive à leur déroulement de carrière, qui ne peut être modifiée au moment de la liquidation. Cette bonification n'a pas pour objet et ne pouvait avoir pour effet de prévenir les inégalités sociales dont ont été l'objet les femmes mais de leur apporter, dans une mesure jugée possible, par un avantage de retraite assimilé à une rémunération différée au sens de l'article 157 du traité sur le fonctionnement de l'Union européenne, une compensation partielle et forfaitaire des retards et préjudices de carrière manifestes qui les ont pénalisées.<br/>
<br/>
              5. Par la loi du 21 août 2003, le législateur a modifié les dispositions sur le fondement desquelles ont été prises les dispositions litigieuses, en ne maintenant le bénéfice automatique de la bonification que pour les femmes fonctionnaires et militaires mères d'enfants nés avant le 1er janvier 2004. Ce faisant, le législateur a entendu maintenir à titre provisoire, en raison de l'intérêt général qui s'attache à la prise en compte de cette situation et à la prévention des conséquences qu'aurait la suppression du b de l'article L. 12 du code des pensions civiles et militaires de retraite sur le niveau des pensions servies aux assurées dans les années à venir, ces dispositions destinées à compenser des inégalités normalement appelées à disparaître.<br/>
<br/>
              6. Dans ces conditions, la différence de traitement dont bénéficient indirectement les femmes mères d'enfants nés avant le 1er janvier 2004 par le bénéfice systématique de la bonification pour enfant tel qu'il découle de la prise en compte du congé maternité, en application des dispositions combinées du b de l'article L. 12 et de l'article R. 13 du code des pensions civiles et militaires de retraite, est objectivement justifiée par un objectif légitime de politique sociale, elle est propre à garantir cet objectif et nécessaire à cet effet. Par suite, les dispositions en cause ne méconnaissent pas le principe d'égalité tel que défini à l'article 157 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              En ce qui concerne le départ anticipé à la retraite :<br/>
<br/>
              7. Aux termes du 3° du I de l'article L. 24 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable au litige en vertu des dispositions transitoires prévues à l'article 44 de la loi du 9 novembre 2010 : " I. - La liquidation de la pension intervient : / (...) 3° Lorsque le fonctionnaire civil est parent de trois enfants vivants, ou décédés par faits de guerre, ou d'un enfant vivant, âgé de plus d'un an et atteint d'une invalidité égale ou supérieure à 80 %, à condition qu'il ait, pour chaque enfant, interrompu son activité dans des conditions fixées par décret en Conseil d'Etat. Sont assimilées à l'interruption d'activité mentionnée à l'alinéa précédent les périodes n'ayant pas donné lieu à cotisation obligatoire dans un régime de retraite de base, dans des conditions fixées par décret en Conseil d'Etat. Sont assimilés aux enfants mentionnés au premier alinéa les enfants énumérés au II de l'article L. 18 que l'intéressé a élevés dans les conditions prévues au III dudit article ". En vertu des I et II de l'article R. 37 du même code, applicable au litige, le bénéfice des dispositions précitées du 3° du I de l'article L. 24 est subordonné à une interruption d'activité d'une durée continue au moins égale à deux mois dans le cadre d'un congé pour maternité, d'un congé pour adoption, d'un congé parental, d'un congé de présence parentale, ou d'une disponibilité pour élever un enfant de moins de huit ans. Par l'arrêt déjà cité du 17 juillet 2014, la Cour de justice de l'Union européenne a estimé, conformément à cette jurisprudence, que l'article 141 doit être interprété en ce sens que, sauf à pouvoir être justifié par des facteurs objectifs étrangers à toute discrimination fondée sur le sexe, tels qu'un objectif légitime de politique sociale, et à être propre à garantir l'objectif invoqué et nécessaire à cet effet, un régime de départ anticipé à la retraite tel que celui résultant des dispositions des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite, en tant qu'elles prévoient la prise en compte du congé maternité dans les conditions ouvrant droit au bénéfice en cause introduirait également une différence de traitement entre les travailleurs féminins et les travailleurs masculins contraire à cet article.<br/>
<br/>
              8. Cependant, ainsi qu'il a été dit au point 3 de la présente décision, la Cour de justice de l'Union européenne a rappelé que, s'il lui revenait de donner des " indications de nature à permettre à la juridiction nationale de statuer ", il revient exclusivement au juge national, qui est seul compétent pour apprécier les faits et pour interpréter la législation nationale, de déterminer si et dans quelle mesure les dispositions concernées sont justifiées par de tels facteurs objectifs. Par la loi du 9 novembre 2010, le législateur a modifié les dispositions sur le fondement desquelles a été prise la décision attaquée, en procédant à une extinction progressive de la mesure pour les parents de trois enfants. Ce faisant, le législateur a entendu non pas prévenir les inégalités de fait entre les hommes et les femmes fonctionnaires et militaires dans le déroulement de leur carrière et leurs incidences en matière de retraite telles qu'exposées au point 4, mais compenser à titre transitoire ces inégalités normalement appelées à disparaître. Dans ces conditions, la disposition litigieuse relative au choix d'un départ anticipé avec jouissance immédiate, prise, pour les mêmes motifs que la bonification pour enfant prévue par les dispositions combinées des articles L. 12 et R. 37, afin d'offrir, dans la mesure du possible, une compensation des conséquences de la naissance et de l'éducation d'enfants sur le déroulement de la carrière d'une femme, en l'état de la société française d'alors, est objectivement justifiée par un objectif légitime de politique sociale, qu'elle est propre à garantir cet objectif et nécessaire à cet effet. Par suite, les dispositions en cause ne méconnaissent pas le principe d'égalité des rémunérations tel que défini à l'article 157 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B...et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
