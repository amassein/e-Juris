<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044097080</ID>
<ANCIEN_ID>JG_L_2021_09_000000432650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/09/70/CETATEXT000044097080.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 23/09/2021, 432650</TITRE>
<DATE_DEC>2021-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432650.20210923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société La Place Gambetta a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir l'arrêté du 22 août 2016 par lequel le maire de Bordeaux a refusé de lui délivrer un permis de construire pour la modification de la toiture et l'aménagement en logement des combles d'un immeuble situé 46 place Gambetta, ainsi que la décision implicite par laquelle le préfet de la région Nouvelle-Aquitaine a rejeté son recours du 12 octobre 2016 et d'enjoindre à la commune de Bordeaux de lui délivrer le permis sollicité ou, à défaut, d'instruire à nouveau sa demande et de lui délivrer un certificat de permis de construire tacite. Par un jugement n° 1604556 du 1er février 2018, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18BX01301 du 14 mai 2019, la cour administrative d'appel de Bordeaux a, sur appel de la société La Place Gambetta, annulé ce jugement ainsi que l'arrêté du 22 août 2016 et enjoint au maire de Bordeaux de délivrer à la société La Place Gambetta le certificat de permis de construire tacite du 6 juillet 2016.   <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Bordeaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société La Place Gambetta ;<br/>
<br/>
              3°) de mettre à la charge de la société La Place Gambetta la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code du patrimoine ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la commune de Bordeaux et à la SCP Waquet, Farge, Hazan, avocat de la société La Place Gambetta ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire de Bordeaux a constaté, le 23 juin 2013, que la société civile La Place Gambetta avait fait réaliser, sans permis de construire, des travaux sur un immeuble dont elle est propriétaire au 46, place Gambetta, consistant en l'aménagement d'un logement sous les combles de l'immeuble et une modification de la toiture à l'arrière du bâtiment pour l'installation d'une structure métallique de type " chien assis ", la pose de deux fenêtres de toit et la création d'une terrasse. Par des décisions du 18 novembre 2013 puis du 22 août 2016, le maire de Bordeaux a refusé de délivrer à la société un permis de construire de régularisation. Le tribunal administratif de Bordeaux a rejeté la demande de la société tendant à l'annulation de cette dernière décision. La commune de Bordeaux se pourvoit en cassation contre l'arrêt du 14 mai 2019 par lequel la cour administrative d'appel de Bordeaux a annulé la décision attaquée et lui a enjoint de délivrer à la société un certificat de permis construire tacite. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 151-43 du code de l'urbanisme : " Les plans locaux d'urbanisme comportent en annexe les servitudes d'utilité publique affectant l'utilisation des sols et figurant sur une liste dressée par décret en Conseil d'Etat ". L'annexe à laquelle renvoie l'article R. 151-51 du même code comporte, dans la liste des servitudes d'utilité publique affectant l'utilisation du sol, au titre des servitudes relatives à la conservation du patrimoine : " a) Monuments historiques et sites patrimoniaux remarquables : immeubles classés et inscrits au titre des monuments historiques en application de l'article L. 621-1 du code du patrimoine ". Aux termes de l'article L. 152-7 du même code : " Après l'expiration d'un délai d'un an à compter, soit de l'approbation du plan local d'urbanisme soit, s'il s'agit d'une servitude d'utilité publique nouvelle définie à l'article L. 151-43, de son institution, seules les servitudes annexées au plan ou publiées sur le portail national de l'urbanisme prévu à l'article L. 133-1 peuvent être opposées aux demandes d'autorisation d'occupation du sol ". D'autre part, aux termes de l'article L. 621-27 du code du patrimoine : " L'inscription au titre des monuments historiques est notifiée aux propriétaires et entraînera pour eux l'obligation de ne procéder à aucune modification de l'immeuble ou partie de l'immeuble inscrit, sans avoir, quatre mois auparavant, avisé l'autorité administrative de leur intention et indiqué les travaux qu'ils se proposent de réaliser. / Lorsque les constructions ou les travaux envisagés sur les immeubles inscrits au titre des monuments historiques sont soumis à permis de construire, à permis de démolir, à permis d'aménager ou à déclaration préalable, la décision accordant le permis ou la décision de non-opposition ne peut intervenir sans l'accord de l'autorité administrative chargée des monuments historiques. ". L'article R. 621-8 du même code prévoit que " La décision de classement de l'immeuble est notifiée par le préfet de région au propriétaire. Celui-ci est tenu d'en informer les affectataires ou occupants successifs. ". Enfin, par exception à ce que prévoit l'article R.* 424-1 du code de l'urbanisme, en vertu duquel, à défaut de notification d'une décision expresse dans le délai d'instruction d'une demande de permis de construire, le silence gardé par l'autorité compétente pour délivrer le permis vaut permis de construire tacite, l'article R.* 424-2 de ce code précise que, lorsque la demande de permis de construire porte sur un immeuble inscrit au titre des monuments historiques, le défaut de notification d'une décision expresse dans le délai d'instruction, qui est de cinq mois, vaut décision implicite de rejet.<br/>
<br/>
              3. Il résulte de ces dispositions que lorsqu'une servitude d'utilité publique affectant l'utilisation des sols, telle la servitude affectant les immeubles classés ou inscrits au titre des monuments historiques, n'est pas annexée à un plan local d'urbanisme, elle n'est, en principe, pas opposable à une demande d'autorisation d'occupation des sols. Toutefois, lorsque le propriétaire d'un immeuble classé ou inscrit aux monuments historiques s'est vu notifier cette inscription en application de l'article R. 621-8 du code du patrimoine, cette servitude lui est opposable alors même qu'elle ne serait pas annexée au plan local d'urbanisme et sa demande de permis de construire, de démolir ou d'aménager portant sur cet immeuble relève en conséquence, conformément à l'article R.* 424-2 du code de l'urbanisme, de la procédure dérogatoire prévue pour ces demandes par les dispositions précitées de l'article L. 621-27 du code du patrimoine, d'où il résulte que le silence gardé par l'administration à l'issue du délai d'instruction fait naître une décision implicite de rejet de la demande.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que les façades et toitures de l'immeuble pour lequel la société La Place Gambetta a sollicité un permis de construire sont inscrites au titre des monuments historiques depuis un arrêté du 15 novembre 1927, que ce classement avait été notifié au propriétaire de cet immeuble conformément à l'article R. 621-8 du code du patrimoine mais que cette servitude n'a pas été annexée au plan local d'urbanisme de Bordeaux Métropole. Il résulte de ce qui a été dit au point 3, qu'en déduisant de l'absence d'inscription de cette servitude en annexe du plan local d'urbanisme que, faute de réponse à sa demande de permis de construire dans le délai de trois mois prévu par l'article R.* 424-1 du code de l'urbanisme, la société était devenue titulaire d'un permis de construire tacite, alors qu'un refus tacite était né à l'issu du délai de cinq mois en application de l'article R.* 424-2 du même code, la cour administrative d'appel de Bordeaux a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la commune de Bordeaux est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société La Place Gambetta la somme de 3 000 euros à verser à la commune de Bordeaux au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Bordeaux qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 14 mai 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : Le jugement de l'affaire est renvoyé à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La société La Place Gambetta versera une somme de 3 000 euros à la commune de Bordeaux au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société La Place Gambetta présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Bordeaux et à la société La Place Gambetta.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-02 MONUMENTS ET SITES. - MONUMENTS HISTORIQUES. - TRAVAUX SUR LES MONUMENTS HISTORIQUES. - SERVITUDE AFFECTANT UN IMMEUBLE CLASSÉ OU INSCRIT NON ANNEXÉE AU PLU - 1) PRINCIPE - INOPPOSABILITÉ, PASSÉ UN DÉLAI D'UN AN (ART. L. 152-7 DU CODE DE L'URBANISME) - 2) A) EXCEPTION - OPPOSABILITÉ À L'ÉGARD DU PROPRIÉTAIRE DE L'IMMEUBLE LORSQU'ELLE LUI A ÉTÉ NOTIFIÉE - B) CONSÉQUENCE - DEMANDE DE PERMIS DE CONSTRUIRE RELEVANT DE LA PROCÉDURE DÉROGATOIRE - SILENCE VALANT DÉCISION IMPLICITE DE REJET [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">41-01-06 MONUMENTS ET SITES. - MONUMENTS HISTORIQUES. - PLANS D'URBANISME ET LÉGISLATION SUR LES MONUMENTS HISTORIQUES. - SERVITUDE AFFECTANT UN IMMEUBLE CLASSÉ OU INSCRIT NON ANNEXÉE AU PLU - 1) PRINCIPE - INOPPOSABILITÉ, PASSÉ UN DÉLAI D'UN AN (ART. L. 152-7 DU CODE DE L'URBANISME) - 2) A) EXCEPTION - OPPOSABILITÉ À L'ÉGARD DU PROPRIÉTAIRE DE L'IMMEUBLE LORSQU'ELLE LUI A ÉTÉ NOTIFIÉE - B) CONSÉQUENCE - DEMANDE DE PERMIS DE CONSTRUIRE RELEVANT DE LA PROCÉDURE DÉROGATOIRE - SILENCE VALANT DÉCISION IMPLICITE DE REJET [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PERMIS DE CONSTRUIRE. - PROCÉDURE D'ATTRIBUTION. - DEMANDE PORTANT SUR UN IMMEUBLE CLASSÉ OU INSCRIT AU TITRE DES MONUMENTS HISTORIQUES - SERVITUDE NON ANNEXÉE AU PLU - 1) PRINCIPE - INOPPOSABILITÉ, PASSÉ UN DÉLAI D'UN AN (ART. L. 152-7 DU CODE DE L'URBANISME) - 2) A) EXCEPTION - OPPOSABILITÉ À L'ÉGARD DU PROPRIÉTAIRE DE L'IMMEUBLE LORSQU'ELLE LUI A ÉTÉ NOTIFIÉE - B) CONSÉQUENCE - DEMANDE DE PERMIS DE CONSTRUIRE RELEVANT DE LA PROCÉDURE DÉROGATOIRE - SILENCE DE L'ADMINISTRATION VALANT REFUS [RJ1].
</SCT>
<ANA ID="9A"> 41-01-02 1) Il résulte des articles L. 151-43, L. 152-7 et R. 151-51 du code de l'urbanisme que, lorsqu'une servitude d'utilité publique affectant l'utilisation des sols, telle la servitude affectant les immeubles classés ou inscrits au titre des monuments historiques, n'est pas annexée à un plan local d'urbanisme (PLU), elle n'est, en principe, pas opposable à une demande d'autorisation d'occupation des sols. ......2) a) Toutefois, lorsque le propriétaire d'un immeuble classé ou inscrit aux monuments historiques s'est vu notifier cette inscription en application de l'article R. 621-8 du code du patrimoine, cette servitude lui est opposable alors même qu'elle ne serait pas annexée au PLU.......b) Sa demande de permis de construire, de démolir ou d'aménager portant sur cet immeuble relève en conséquence, conformément à l'article R.* 424-2 du code de l'urbanisme, de la procédure dérogatoire prévue pour ces demandes par l'article L. 621-27 du code du patrimoine, d'où il résulte que le silence gardé par l'administration à l'issue du délai d'instruction fait naître une décision implicite de rejet de la demande.</ANA>
<ANA ID="9B"> 41-01-06 1) Il résulte des articles L. 151-43, L. 152-7 et R. 151-51 du code de l'urbanisme que, lorsqu'une servitude d'utilité publique affectant l'utilisation des sols, telle la servitude affectant les immeubles classés ou inscrits au titre des monuments historiques, n'est pas annexée à un plan local d'urbanisme (PLU), elle n'est, en principe, pas opposable à une demande d'autorisation d'occupation des sols. ......2) a) Toutefois, lorsque le propriétaire d'un immeuble classé ou inscrit aux monuments historiques s'est vu notifier cette inscription en application de l'article R. 621-8 du code du patrimoine, cette servitude lui est opposable alors même qu'elle ne serait pas annexée au PLU.......b) Sa demande de permis de construire, de démolir ou d'aménager portant sur cet immeuble relève en conséquence, conformément à l'article R.* 424-2 du code de l'urbanisme, de la procédure dérogatoire prévue pour ces demandes par l'article L. 621-27 du code du patrimoine, d'où il résulte que le silence gardé par l'administration à l'issue du délai d'instruction fait naître une décision implicite de rejet de la demande.</ANA>
<ANA ID="9C"> 68-03-02 1) Il résulte des articles L. 151-43, L. 152-7 et R. 151-51 du code de l'urbanisme que, lorsqu'une servitude d'utilité publique affectant l'utilisation des sols, telle la servitude affectant les immeubles classés ou inscrits au titre des monuments historiques, n'est pas annexée à un plan local d'urbanisme (PLU), elle n'est, en principe, pas opposable à une demande d'autorisation d'occupation des sols. ......2) a) Toutefois, lorsque le propriétaire d'un immeuble classé ou inscrit aux monuments historiques s'est vu notifier cette inscription en application de l'article R. 621-8 du code du patrimoine, cette servitude lui est opposable alors même qu'elle ne serait pas annexée au PLU.......b) Sa demande de permis de construire, de démolir ou d'aménager portant sur cet immeuble relève en conséquence, conformément à l'article R.* 424-2 du code de l'urbanisme, de la procédure dérogatoire prévue pour ces demandes par l'article L. 621-27 du code du patrimoine, d'où il résulte que le silence gardé par l'administration à l'issue du délai d'instruction fait naître une décision implicite de rejet de la demande.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
