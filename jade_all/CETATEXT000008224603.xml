<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008224603</ID>
<ANCIEN_ID>JG_L_2006_07_000000292360</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/22/46/CETATEXT000008224603.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 13/07/2006, 292360, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2006-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>292360</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stahl</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2006:292360.20060713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 avril 2006 au secrétariat du contentieux du Conseil d'Etat, présentée pour Mlle A...B..., demeurant... ; Mlle B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 30 mars 2006 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant, d'une part, à la suspension de l'exécution de la décision du 9 janvier 2006 du préfet de la Seine-Saint-Denis rejetant sa demande de renouvellement de titre de séjour, et, d'autre part, à ce qu'il soit enjoint au préfet de réexaminer sa demande dans un délai de huit jours et de lui délivrer un récépissé de demande de renouvellement de titre de séjour en qualité d'étudiante ou une autorisation provisoire de séjour sous astreinte ;<br/>
<br/>
              2°) statuant en référé, de prononcer la suspension de la décision du 9 janvier 2006 par laquelle le préfet de la Seine-Saint-Denis a rejeté sa demande de renouvellement de titre de séjour ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment son article 8 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'aile, notamment son article L. 313-7 ;<br/>
<br/>
              Vu le code de justice administrative notamment ses articles L. 521-1 et R. 742-2 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, Auditeur,  <br/>
<br/>
              - les observations de la SCP Boulloche, avocat de MlleB..., <br/>
<br/>
              - les conclusions de M. Jacques-Henri Stahl, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ces effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'aux termes de l'article R. 742-2 du même code : " Les ordonnances mentionnent le nom des parties, l'analyse des conclusions ainsi que les visas des dispositions législatives ou réglementaires dont elles font application " ;<br/>
<br/>
              Considérant qu'il appartient au juge des référés qui rejette une demande tendant à la suspension de l'exécution d'une décision administrative au motif qu'il n'est pas fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cette décision, d'analyser soit dans les visas de son ordonnance, soit dans les motifs de celle-ci, les moyens développés au soutien de la demande de suspension, afin, notamment, de mettre le juge de cassation en mesure d'exercer son  contrôle ;<br/>
<br/>
              Considérant que, dans l'analyse de l'argumentation présentée par Mlle B... au soutien de ses conclusions tendant à ce que soit suspendue l'exécution de la décision en date du 9 janvier 2006, l'ordonnance attaquée s'est limitée à énoncer que la requérante faisait état de son parcours universitaire et de l'incidence de son état de santé sur ce dernier ; que le juge des référés s'est borné, dans ses motifs, à énoncer qu'aucun des moyens invoqués n'était de nature à faire naître un doute sérieux quant à la légalité de la décision attaquée, sans davantage préciser quels étaient ces moyens, alors que Mlle B... soutenait notamment que la décision avait été prise par une autorité incompétente et que le préfet de la Seine-Saint-Denis avait fait une inexacte application des dispositions de l'article L. 313-7 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de la requête, Mlle B... est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée par Mlle B... ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que Mlle B..., de nationalité iranienne, est entrée sur le territoire  français le 5 octobre 2001 afin d'y suivre des études de " Langues, littératures et civilisations étrangères ", spécialisation " Langues iraniennes ", à l'université Paris III ; que sa carte de séjour temporaire " étudiant " a été renouvelée jusqu'au 25 septembre 2005 ; qu'elle a obtenu une licence en 2002 et, après un redoublement, une maîtrise en 2004, avant d'interrompre ses études pendant l'année universitaire 2004/2005 pour raisons de santé ; que, faute de satisfaire aux exigences académiques posées à l'inscription en troisième cycle, elle s'est inscrite au titre de l'année universitaire 2005/2006 à la préparation du diplôme universitaire de langue française (D.U.L.F) niveau 4 " intermédiaire avancé " ; que, pour estimer que les études de Mlle B... ne présentaient pas un caractère réel et sérieux, le préfet de la Seine-Saint-Denis s'est notamment fondé sur l'absence de progression dans le déroulement de son cursus ;<br/>
<br/>
              Considérant que, eu égard à la durée du séjour en France de Mlle B... et à la nature des diplômes qu'elle a obtenus, tant dans son pays d'origine qu'en France, qui requièrent une bonne maîtrise de la langue française, il n'apparaît pas que l'inscription à la préparation du DULF - niveau " intermédiaire avancé " s'inscrive dans la continuité logique des études entreprises par la requérante, âgée de 32 ans, ni conditionne l'accès aux formations universitaires à l'enseignement du français langue étrangère qu'elle envisage de suivre par la suite ; que, par suite, le moyen tiré de ce que le préfet de la Seine-Saint-Denis aurait fait une inexacte application des dispositions de l'article L. 313-7 du code de l'entrée et du séjour des étrangers et du droit d'asile n'est pas propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision attaquée ; que ne sont pas davantage propre à créer un tel doute les moyens tirés de ce que la décision du 9 janvier 2006 aurait été prise par une autorité incompétente et qu'elle méconnaîtrait les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Mlle B... n'est pas fondée à demander la suspension de la décision refusant de lui renouveler son titre de séjour ; que les conclusions à fin d'injonction dont sa demande est assortie doivent, par voie de conséquence, être rejetées, ainsi que ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 30 mars 2006 du juge des référés du tribunal administratif de Cergy-Pontoise est annulée.<br/>
<br/>
Article 2 : La demande présentée par Mlle B... et le surplus des conclusions de sa requête sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mlle A...B...et au ministre d'Etat, ministre de l'intérieur et de l'aménagement du territoire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
