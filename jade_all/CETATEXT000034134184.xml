<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034134184</ID>
<ANCIEN_ID>JG_L_2017_03_000000392446</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/13/41/CETATEXT000034134184.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 03/03/2017, 392446</TITRE>
<DATE_DEC>2017-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392446</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392446.20170303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La société Leasecom a demandé au tribunal administratif de Marseille de condamner l'Etat à lui payer la somme de 40 866,33 euros correspondant à l'indemnité de résiliation d'un contrat ayant pour objet la location de quinze photocopieurs par le tribunal de grande instance de Marseille. Par un jugement n° 0707048 du 28 juin 2011, le tribunal a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n°s 11MA03607, 12MA04630 du 11 juin 2013, la cour administrative d'appel de Marseille a, sur appel formé par le garde des sceaux, ministre de la justice, annulé ce jugement et rejeté la demande présentée par la société Leasecom devant le tribunal.<br/>
<br/>
              Par une décision n° 371130 du 5 novembre 2014, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour.<br/>
<br/>
              Par un arrêt n°s 14MA04874, 14MA04875 du 8 juin 2015, la cour administrative d'appel de Marseille, statuant sur le renvoi de l'affaire par le Conseil d'Etat, a annulé le jugement du tribunal administratif de Marseille du 28 juin 2011 et rejeté la demande présentée par la société Leasecom devant le tribunal.<br/>
<br/>
<br/>
<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 août et 9 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Leasecom demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la justice ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Leasecom.  <br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 février 2017, présentée par la société Leasecom.  <br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, le 20 novembre 2003, le tribunal de grande instance de Marseille a conclu avec la société Leasecom un contrat de location de quinze photocopieurs à compter du 1er janvier 2004 pour une durée de douze trimestres, moyennant un loyer trimestriel de 7 765,72 euros ; que, par un courrier du 27 juin 2005, le greffier en chef du tribunal a informé la société de sa décision de résilier ce contrat à compter du 31 décembre 2005 ; que, par un jugement du 28 juin 2011, le tribunal administratif de Marseille a condamné l'Etat à verser à la société Leasecom la somme de 40 866,33 euros au titre de l'indemnité contractuelle de résiliation assortie des taux d'intérêt légaux ; que l'arrêt du 11 juin 2013 par lequel la cour administrative d'appel de Marseille a, sur appel du garde des sceaux, ministre de la justice, annulé ce jugement et rejeté la demande présentée par la société Leasecom a été  annulé par le Conseil d'Etat, statuant au contentieux, par une décision du 5 novembre 2014 ; que, par un second arrêt du 8 juin 2015, contre lequel la société Leasecom se pourvoit en cassation, la cour, statuant sur le renvoi de l'affaire par le Conseil d'Etat, a annulé le jugement du 28 juin 2011 et rejeté l'ensemble des conclusions présentées par la société Leasecom ; <br/>
<br/>
              2. Considérant qu'en vertu des règles générales applicables aux contrats administratifs, la personne publique cocontractante peut toujours, pour un motif d'intérêt général, résilier unilatéralement un tel contrat, sous réserve des droits à indemnité de son cocontractant ; que, si l'étendue et les modalités de cette indemnisation peuvent être déterminées par les stipulations contractuelles, l'interdiction faite aux personnes publiques de consentir des libéralités fait toutefois obstacle à ce que ces stipulations prévoient une indemnité de résiliation qui serait, au détriment de la personne publique, manifestement disproportionnée au montant du préjudice subi par le cocontractant du fait de cette résiliation ; que si, dans le cadre d'un litige indemnitaire, l'une des parties ou le juge soulève, avant la clôture de l'instruction, un moyen tiré de l'illicéité de la clause du contrat relative aux modalités d'indemnisation du cocontractant en cas de résiliation anticipée, il appartient à ce dernier de demander au juge la condamnation de la personne publique à l'indemniser du préjudice qu'il estime avoir subi du fait de la résiliation du contrat sur le fondement des règles générales applicables, dans le silence du contrat, à l'indemnisation du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général ; que, dans l'hypothèse où le juge inviterait les parties à présenter leurs observations, en application de l'article R. 611-7 du code de justice administrative, sur le moyen soulevé d'office et tiré de l'illicéité de la clause d'indemnisation du contrat, le cocontractant de la personne publique peut, dans ses observations en réponse soumises au contradictoire, fonder sa demande de réparation sur ces règles générales applicables aux contrats administratifs ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des énonciations de l'arrêt attaqué que les conditions particulières du contrat litigieux prévoyaient qu'en cas de résiliation anticipée, quelle qu'en soit la cause, le bailleur aurait droit à une indemnité égale à tous les loyers dus et à échoir jusqu'au terme de la durée initiale de location majorée de 10 % ; que la cour administrative d'appel de Marseille, en jugeant qu'une telle indemnité, d'un montant supérieur au loyer que le tribunal de grande instance de Marseille aurait continué à verser en exécution du contrat si celui-ci n'avait pas été résilié, était manifestement disproportionnée au regard du préjudice résultant, pour la société Leasecom, des dépenses qu'elles avait exposées et du gain dont elle avait été privée, dès lors que la société ne justifiait pas de charges particulières ou de l'impossibilité de vendre ou de louer ce matériel, n'a, contrairement à ce qui est soutenu, ni commis d'erreur de droit, ni inexactement qualifié les faits ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des écritures de la société Leasecom devant les juges du fond que celle-ci s'est exclusivement prévalue, au soutien de ses conclusions indemnitaires, de la clause de résiliation prévue par le contrat ; qu'alors que la cour l'a informée de ce que l'arrêt à intervenir était susceptible d'être fondé sur un moyen relevé d'office tiré de l'illicéité de cette clause, la société Leasecom s'est bornée, dans ses observations en réponse, à contester le bien-fondé de ce moyen ; qu'en l'absence de toute demande de la société tendant à l'indemnisation des conséquences de la résiliation anticipée du contrat sur le fondement des règles générales applicables aux contrats administratifs, il résulte de ce qui a été dit au point 2 que la cour, en ne se prononçant pas sur ce point, n'a ni méconnu son office ni insuffisamment motivé son arrêt ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société Leasecom n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les conclusions qu'elle a présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Leasecom est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Leasecom et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. - JUGE DU CONTRAT SAISI D'UNE DEMANDE D'INDEMNISATION DE LA RÉSILIATION ANTICIPÉE DU CONTRAT PAR L'ADMINISTRATION POUR UN MOTIF D'INTÉRÊT GÉNÉRAL - CONTRAT PRÉVOYANT UNE CLAUSE D'INDEMNISATION - CAS OÙ LE JUGE DU CONTRAT NE PEUT FAIRE APPLICATION DE LA CLAUSE EN RAISON DE SON ILLICÉITÉ - 1) FACULTÉ POUR LE COCONTRACTANT DE DEMANDER L'INDEMNISATION DES CONSÉQUENCES DE LA RÉSILIATION ANTICIPÉE SUR LE FONDEMENT DES RÈGLES GÉNÉRALES APPLICABLES AUX CONTRATS ADMINISTRATIFS - EXISTENCE - 2) OBLIGATION POUR LE JUGE DE SE PLACER D'OFFICE SUR CE TERRAIN - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08-03 Conclusions tendant à l'indemnisation des conséquences de la résiliation pour motif d'intérêt général d'un contrat dont une clause fixait les modalités d'indemnisation du cocontractant en cas de résiliation anticipée [RJ1].,,,1) Si, dans le cadre d'un litige indemnitaire, l'une des parties ou le juge soulève, avant la clôture de l'instruction, un moyen tiré de l'illicéité de la clause du contrat relative aux modalités d'indemnisation du cocontractant en cas de résiliation anticipée, il appartient à ce dernier de demander au juge la condamnation de la personne publique à l'indemniser du préjudice qu'il estime avoir subi du fait de la résiliation du contrat par la personne publique sur le fondement des règles générales applicables, dans le silence du contrat, à l'indemnisation du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général. Dans l'hypothèse où le juge inviterait les parties, après la clôture de l'instruction, à présenter leurs observations, en application de l'article R. 611-7 du code de justice administrative, sur le moyen soulevé d'office et tiré de l'illicéité de la clause d'indemnisation du contrat, le cocontractant de la personne publique peut, dans ses observations en réponse soumises au contradictoire, fonder sa demande de réparation sur ces règles générales applicables aux contrats administratifs.,,,2) Cocontractant de l'administration s'étant uniquement prévalu, au soutien de ses conclusions indemnitaires, de la clause de résiliation prévue par le contrat, y compris après que le juge l'avait informé de ce que sa décision à intervenir était susceptible d'être fondée sur un moyen relevé d'office tiré de l'illicéité de cette clause. En l'absence de toute demande du cocontractant tendant à l'indemnisation des conséquences de la résiliation anticipée du contrat sur le fondement des règles générales applicables aux contrats administratifs, il n'appartient pas au juge de se prononcer d'office sur ce point.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les conditions de légalité d'une telle clause, CE, 4 mai 2011, Chambre de commerce et d'industrie de Nîmes, Uzès, Bagnols, Le Vigan, n° 334280, p. 205 ; CE, 22 juin 2012, Chambre de commerce et d'industrie de Montpellier (CCIM) et société aéroport de Montpellier-Méditerranée, n° 348676, T. pp. 851-954.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
