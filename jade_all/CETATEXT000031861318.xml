<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861318</ID>
<ANCIEN_ID>JG_L_2015_12_000000387213</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/13/CETATEXT000031861318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 30/12/2015, 387213, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387213</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:387213.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Le Centre hospitalier de Saint-Omer a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 19 juillet 2011 par laquelle le directeur général de l'agence régionale de santé (ARS) de Nord-Pas-de-Calais lui a infligé une sanction financière d'un montant de 100 000 euros. Par un jugement n° 1106533 du 21 mai 2013, le tribunal administratif de Lille a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 13DA01317 du 12 novembre 2014, la cour administrative d'appel de Douai a rejeté l'appel formé par le ministre des affaires sociales de la santé contre ce jugement du tribunal administratif de Lille.<br/>
<br/>
              Par un pourvoi, enregistré le 19 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des affaires sociales, de la santé et des droits des femmes demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Douai du 12 novembre 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
- les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 162-22-18 du code de la sécurité sociale dispose que : " Les établissements de santé sont passibles, après qu'ils ont été mis en demeure de présenter leurs observations, d'une sanction financière en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, d'erreur de codage ou d'absence de réalisation d'une prestation facturée. / Cette sanction est prise par le directeur général de l'agence régionale de santé, à la suite d'un contrôle réalisé sur pièces et sur place par les médecins inspecteurs de santé publique, les inspecteurs de l'agence régionale de santé ayant la qualité de médecin ou les praticiens-conseils des organismes d'assurance maladie en application du programme de contrôle régional établi par l'agence. Le directeur général de l'agence prononce la sanction après avis d'une commission de contrôle composée à parité de représentants de l'agence et de représentants des organismes d'assurance maladie et du contrôle médical. La motivation de la sanction indique, si tel est le cas, les raisons pour lesquelles le directeur général n'a pas suivi l'avis de la commission de contrôle. La sanction est notifiée à l'établissement. / (...) Les modalités d'application du présent article sont définies par décret en Conseil d'Etat ". Aux termes de l'article R. 162-42-13 du même code, dans sa rédaction applicable à la décision en litige : " La sanction envisagée et les motifs la justifiant sont notifiés à l'établissement par tout moyen permettant de déterminer la date de réception. L'établissement dispose d'un délai d'un mois pour présenter ses observations. Au terme de ce délai, le directeur général sollicite l'avis de la commission de contrôle, notamment sur le montant de la sanction. Il prononce la sanction, la notifie à l'établissement dans un délai d'un mois par tout moyen permettant de déterminer la date de réception en indiquant à l'établissement le délai et les modalités de paiement des sommes en cause ainsi que, le cas échéant, les raisons pour lesquelles il n'a pas suivi l'avis de la commission de contrôle (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions qu'une sanction financière prononcée sur le fondement de l'article L. 162-22-18 du code de la sécurité sociale doit être motivée. Pour satisfaire à cette exigence, le directeur général de l'agence régionale de santé doit indiquer, soit dans sa décision elle-même, soit par référence à un document joint ou précédemment adressé à l'établissement de santé, outre les dispositions en application desquelles la sanction est prise, les considérations de fait et les éléments de calcul sur lesquels il se fonde pour décider de son principe et en fixer le montant.<br/>
<br/>
              3. Pour juger qu'elle ne satisfaisait pas aux exigences de motivation, la cour administrative d'appel de Douai a relevé que la sanction financière infligée au Centre hospitalier de Saint-Omer le 19 juillet 2011 ne précisait pas l'énoncé des considérations de droit qui en constituent le fondement ni la nature des anomalies retenues pour chacun des dossiers en cause et jugé que la circonstance que l'établissement ait reçu précédemment un courrier qui comportait en annexe un tableau reprenant les principales données financières ayant permis le calcul du montant maximum de la sanction était sans incidence, dès lors, d'une part, que ce document n'était pas joint à la décision contestée et, d'autre part, que le montant de la sanction infligée avait été fixé après modulation, dans une grande proportion, du montant préalablement notifié. En statuant ainsi, sans rechercher si la décision litigieuse et les documents auxquels elle se référait, qui avaient été précédemment adressés à l'établissement, permettaient à ce dernier de connaître les considérations de droit et de fait au vu desquelles la sanction était prise et les éléments en fonction desquels son montant avait été finalement arrêté, la cour administrative d'appel de Douai a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre des affaires sociales, de la santé et des droits des femmes est fondé à demander l'annulation de l'arrêt qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner l'autre moyen de son pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 12 novembre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La présente décision sera notifiée à la ministre des affaires sociales, de la santé et des droits des femmes et au Centre hospitalier de Saint-Omer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
