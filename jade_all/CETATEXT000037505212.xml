<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037505212</ID>
<ANCIEN_ID>JG_L_2018_10_000000420822</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/52/CETATEXT000037505212.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 17/10/2018, 420822, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420822</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:420822.20181017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Groupe Mada a demandé au tribunal administratif de Paris de prononcer la décharge de l'amende prévue au 1 du I de l'article 1736 du code général des impôts qui lui a été infligée au titre des années 2009 à 2012. Par un jugement n° 1521127 du 12 octobre 2016, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16PA03478 du 22 mars 2018, la cour administrative d'appel de Paris a rejeté l'appel de la SARL Groupe Mada contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 22 mai et 22 août 2018 au secrétariat du contentieux du Conseil d'Etat, la SARL Groupe Mada demande au Conseil d'Etat : <br/>
              1°) d'annuler cet arrêt ; <br/>
              2°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire distinct enregistré le 25 septembre 2018, la SARL Groupe Mada a demandé au Conseil d'Etat, au soutien de son pourvoi, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil constitutionnel une question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution des dispositions du 1 du I de l'article 1736 du code général des impôts. Elle soutient que :<br/>
              - la disposition est applicable au litige, <br/>
              - si le Conseil constitutionnel a admis la conformité de cette disposition à la Constitution dans sa décision n° 2012-267 QPC du 20 juillet 2012, un changement de circonstances résultant de sa décision n° 2016-554 QPC du 22 juillet 2016 justifie un nouvel examen de cette disposition,<br/>
              - l'amende instituée par le 1 du I de l'article 1736 du code général des impôts méconnait le principe de proportionnalité des peines et est donc contraire à l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              -la Constitution, notamment son article 61-1 ; <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la décision du Conseil constitutionnel n° 2012-267 QPC du 20 juillet 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la SARL Groupe Mada  ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du 1 du I de l'article 1736 du code général des impôts, " Entraîne l'application d'une amende égale à 50 % des sommes non déclarées le fait de ne pas se conformer aux obligations prévues à l'article 240 et au 1 de l'article 242 ter et à l'article 242 ter B. L'amende n'est pas applicable, en cas de première infraction commise au cours de l'année civile en cours et des trois années précédentes, lorsque les intéressés ont réparé leur omission, soit spontanément, soit à la première demande de l'administration, avant la fin de l'année au cours de laquelle la déclaration devait être souscrite ".<br/>
<br/>
              3. Le Conseil constitutionnel a, dans les motifs et le dispositif de la décision n° 2012-267 QPC du 20 juillet 2012, déclaré conformes à la Constitution les dispositions du 1 du I de l'article 1736 du code général des impôts citées au point 2. Contrairement à ce que soutient la société requérante, l'intervention de la décision du Conseil constitutionnel n° 2016-554 QPC du 22 juillet 2016 ne caractérise pas un changement de circonstances de nature à justifier que la conformité des dispositions du 1 du I de l'article 1736 du code général des impôts aux droits et libertés garantis par la Constitution soit à nouveau examinée par le Conseil constitutionnel. Par suite, les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 font obstacle à ce que la question prioritaire de constitutionnalité soulevée par la société requérante soit renvoyée au Conseil constitutionnel.<br/>
<br/>
              Sur le pourvoi : <br/>
<br/>
              4. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              5. Pour demander l'annulation de l'arrêt qu'elle attaque, la SARL Groupe Mada soutient que la cour administrative d'appel de Paris l'a entaché d'erreur de droit en jugeant que l'administration n'était pas tenue, en application du 1 du I de l'article 1736 du code général des impôts, de l'inviter à régulariser sa situation en respectant ses obligations déclaratives avant de lui infliger une amende alors qu'était en cause une première infraction.<br/>
<br/>
              6. Ce moyen n'est pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SARL Groupe Mada. <br/>
Article 2 : Le pourvoi de la SARL Groupe Mada n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SARL Groupe Mada.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
