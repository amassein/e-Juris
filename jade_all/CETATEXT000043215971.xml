<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043215971</ID>
<ANCIEN_ID>JG_L_2021_03_000000431525</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/59/CETATEXT000043215971.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 03/03/2021, 431525, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431525</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:431525.20210303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Sauvons la forêt valbonnaise a demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 9 novembre 2018 par lequel le maire de la commune de Valbonne a délivré aux sociétés Immobilière Méditerranée, FDI Sacicap et Les Bourelles 1715 un permis de construire pour la réalisation d'un ensemble immobilier sur le site du lieu-dit Les Bourelles. Par une ordonnance n° 1900066 du 10 avril 2019, le président de la sixième chambre du tribunal administratif a, par application de l'article R. 222-1 du code de justice administrative, rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 juin et 11 septembre 2019 et le 25 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Sauvons la forêt valbonnaise demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Valbonne la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'association Sauvons la forêt valbonnaise et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société 3F sud ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 3 décembre 2015, le conseil municipal de Valbonne a décidé d'engager une procédure intégrée pour le logement emportant mise en compatibilité du plan local d'urbanisme, en application des articles L. 300-6-1 et R. 300-15 du code de l'urbanisme, afin de permettre la réalisation d'un ensemble immobilier composé de logements au lieu-dit Les Bourelles. Par une délibération du 4 octobre 2018, le conseil municipal a approuvé cette procédure. Par un arrêté du 9 novembre 2018, le maire de la commune a délivré aux sociétés Immobilière Méditerranée, FDI Sacicap et Les Bourelles 1715 un permis de construire valant division parcellaire pour la réalisation de l'opération. L'association Sauvons la forêt valbonnaise se pourvoit en cassation contre l'ordonnance du 10 avril 2019 par laquelle le président de la sixième chambre du tribunal administratif de Nice a rejeté comme irrecevable sa demande tendant à l'annulation pour excès de pouvoir de cet arrêté.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un certificat d'urbanisme, ou d'une décision relative à l'occupation ou l'utilisation du sol régie par le présent code, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant un certificat d'urbanisme, ou une décision relative à l'occupation ou l'utilisation du sol régie par le présent code. L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif (...) ". D'autre part, aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, les premiers vice-présidents des tribunaux et des cours, le vice-président du tribunal administratif de Paris, les présidents de formation de jugement des tribunaux et des cours et les magistrats ayant une ancienneté minimale de deux ans et ayant atteint au moins le grade de premier conseiller désignés à cet effet par le président de leur juridiction peuvent, par ordonnance : / (....) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier que l'association Sauvons la forêt valbonnaise a formé un recours gracieux contre la délibération du conseil municipal de Valbonne Sophia Antipolis du 4 octobre 2018 approuvant la procédure de mise en compatibilité du plan local d'urbanisme engagée sur le fondement de l'article L. 300-6-1 du code de l'urbanisme. Par l'ordonnance attaquée, le président de la sixième chambre du tribunal administratif de Nice a cependant jugé que la demande de l'association Sauvons la forêt valbonnaise tendant à l'annulation pour excès de pouvoir du permis de construire litigieux délivré le 9 novembre 2018 était irrecevable, faute pour la requérante d'avoir apporté la preuve de la notification de son recours gracieux au pétitionnaire, comme l'exige l'article R. 600-1 du code de l'urbanisme. En statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis qu'elle n'avait pas formé un tel recours à l'encontre du permis de construire litigieux, le président de la sixième chambre a dénaturé les pièces du dossier.<br/>
<br/>
              4. Si la société 3F Sud soutient en défense que la requête était en tout état de cause irrecevable, faute pour le président de l'association requérante d'avoir obtenu l'accord préalable du bureau, comme l'exigent ses statuts, il ressort en tout état de cause des pièces du dossier soumis aux juges du fond, d'une part, que ces statuts prévoient que le bureau de l'association est composé de cinq à sept membres élus parmi les membres du comité directeur, et, d'autre part, que son président a été autorisé à introduire un recours contentieux contre le permis de construire litigieux par l'unanimité des membres de ce comité. Il s'en déduit que le bureau de l'association doit être regardé comme ayant donné son accord à l'introduction de la requête, conformément à ses statuts. Le moyen tiré de ce que cette fin de non-recevoir devrait être substituée à celle retenue par l'ordonnance attaquée ne peut qu'être écarté.<br/>
<br/>
              5. Il résulte de tout ce qui précède que l'association Sauvons la forêt valbonnaise est fondée à demander l'annulation de l'ordonnance attaquée du 10 avril 2019.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Valbonne la somme de 3 000 euros à verser à l'association Sauvons la forêt valbonnaise au titre des dispositions de l'article L. 761-1 du code de justice administrative. Cette dernière n'étant pas, dans la présente instance, la partie perdante, ces dispositions font en revanche obstacle à ce qu'une somme soit mise à sa charge à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la sixième chambre du tribunal administratif de Nice du 10 avril 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nice.<br/>
Article 3 : La commune de Valbonne versera à l'association Sauvons la forêt valbonnaise une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société 3F Sud présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'association Sauvons la forêt valbonnaise, à la commune de Valbonne et à la société 3F Sud.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
