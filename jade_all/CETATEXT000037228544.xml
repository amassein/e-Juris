<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037228544</ID>
<ANCIEN_ID>JG_L_2018_07_000000417384</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/85/CETATEXT000037228544.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 18/07/2018, 417384, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417384</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417384.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Le médecin-conseil, chef de service de l'échelon local du service médical de Rouen-Elbeuf-Dieppe a porté plainte contre M. A... B... devant la section des assurances sociales de la chambre disciplinaire du conseil régional de Haute-Normandie de l'ordre des médecins. Par une décision du 19 septembre 2016, la section des assurances sociales a infligé à M. B... la sanction d'interdiction de donner des soins aux assurés sociaux pour une durée de six mois, dont trois mois assortis du sursis.<br/>
<br/>
              Par une décision du 15 novembre 2017, la section des assurances sociales du Conseil national de l'ordre des médecins a rejeté l'appel formé par M. B... contre cette décision.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 417384, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 janvier et 16 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du médecin-conseil, chef de service de l'échelon local du service médical de Rouen-Elbeuf-Dieppe et du conseil départemental de Seine-Maritime de l'ordre des médecins la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 419859, par une requête, enregistrée le 16 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, en application de l'article R. 821-5 du code de justice administrative, qu'il soit sursis à l'exécution de la même décision du 15 novembre 2017 de la section des assurances sociales du Conseil national de l'ordre des médecins ;<br/>
<br/>
              2°) de mettre à la charge du médecin-conseil, chef de service de l'échelon local du service médical de Rouen-Elbeuf-Dieppe et du conseil départemental de Seine-Maritime de l'ordre des médecins la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant que le pourvoi par lequel M. B... demande l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 15 novembre 2017 et sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2.  Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              3.  Considérant que, pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins qu'il attaque, M. B... soutient qu'elle est entachée d'insuffisance de motivation, d'erreur de droit et d'inexacte qualification juridique des faits en ce que, premièrement, elle fonde la sanction qu'elle prononce sur une analyse purement statistique de son activité, en ce que, deuxièmement, elle juge qu'il a réalisé des actes qui n'étaient pas médicalement justifiés, en ce que, troisièmement, elle juge qu'il a facturé des actes qu'il n'a pas réalisés et, enfin, en ce qu'elle juge que les erreurs de cotation commises sont fautives ; qu'elle prononce, sans la motiver suffisamment, une sanction hors de proportion avec les fautes reprochées ;<br/>
<br/>
              4.  Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ;<br/>
<br/>
              5.  Considérant que le pourvoi formé par M. B... contre la décision du 15 novembre 2017 de la section des assurances sociales du Conseil national de l'ordre des médecins n'étant pas admis, les conclusions qu'il présente aux fins de sursis à l'exécution de cette décision sont devenues sans objet ; que, le médecin-conseil, chef de service de l'échelon local du service médical de Rouen-Elbeuf-Dieppe n'étant pas la partie perdante dans la présente instance et le conseil départemental de Seine-Maritime de l'ordre des médecins n'y ayant pas la qualité de partie, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à leur charge les sommes que demande, à ce titre, M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... n'est pas admis.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. B...tendant à ce qu'il soit sursis à l'exécution de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 15 novembre 2017.<br/>
Article 3 : Le surplus des conclusions de la requête de M.B..., présenté au titre des dispositions de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A... B....<br/>
Copie en sera adressée au médecin-conseil, chef de service de l'échelon local du service médical de Rouen-Elbeuf-Dieppe.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
