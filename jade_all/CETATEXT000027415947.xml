<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027415947</ID>
<ANCIEN_ID>JG_L_2013_05_000000356489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/59/CETATEXT000027415947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 17/05/2013, 356489</TITRE>
<DATE_DEC>2013-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356489.20130517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 février et 27 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B..., demeurant... ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY00986 du 18 octobre 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0902338 du 10 février 2011 du tribunal administratif de Clermont-Ferrand rejetant sa demande tendant à l'annulation de la décision implicite de rejet du recteur de l'académie de Clermont-Ferrand née du silence gardé sur son recours gracieux formé le 10 septembre 2009 tendant à son recrutement dans le cadre d'un parcours d'accès aux carrières de la fonction publique territoriale, hospitalière et d'Etat, d'autre part, à l'annulation pour excès de pouvoir de cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP A. Bouzidi-Ph. Bouhanna au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;   <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu l'ordonnance n° 2005-901 et le décret n° 2005-902 du 2 août 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6 (...) " ; qu'aux termes de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris d'ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne " ; <br/>
<br/>
              2.  Considérant qu'il ressort des pièces du dossier que, alors qu'aucune ordonnance de clôture de l'instruction n'avait été prise et que la date de l'audience, indiquée dans l'avis adressé aux parties, était fixée au mardi 27 septembre 2011, l'unique mémoire en défense du ministre de l'éducation nationale a été enregistré au greffe de la cour administrative d'appel de Lyon le jeudi 22 septembre 2011 et communiqué par télécopie à l'avocat de Mme A... partiellement le même jour et complètement le lendemain ; que ce mémoire ne se bornait pas, contrairement à ce que soutient le ministre, à reprendre l'argumentation que l'administration avait présentée en première instance mais invitait la cour, par des développements circonstanciés sur lesquels elle s'est fondée, à écarter l'un des moyens invoqués par la requérante pour contester le motif retenu par le tribunal administratif pour rejeter sa demande ; que l'instruction ayant été close, par application des dispositions de l'article R. 613-2 du code de justice administrative, trois jours francs avant la date du 27 septembre 2011, la requérante n'a pas disposé d'un délai suffisant pour répondre au mémoire en défense du ministre de l'éducation nationale ; que, par suite,  l'arrêt du 18 octobre 2011 par lequel la cour a rejeté la requête de Mme A...a été rendu en méconnaissance du principe du caractère contradictoire de la procédure ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à en demander, pour ce motif, l'annulation ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 5 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Sous réserve des dispositions de l'article 5 bis nul ne peut avoir la qualité de fonctionnaire : / (...) / 3° Le cas échéant, si les mentions portées au bulletin n° 2 de son casier judiciaire sont incompatibles avec l'exercice des fonctions " ; qu'aux termes de l'article 22 bis de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, inséré à cette loi par l'article 3 de l'ordonnance du 2 août 2005 : " Les jeunes gens de seize à vingt-cinq ans révolus qui sont sortis du système éducatif sans diplôme ou sans qualification professionnelle reconnue et ceux dont le niveau de qualification est inférieur à celui attesté par un diplôme de fin de second cycle long de l'enseignement général, technologique ou professionnel, peuvent, à l'issue d'une procédure de sélection, être recrutés dans des emplois du niveau de la catégorie C relevant des administrations mentionnées à l'article 2 de la présente loi, par des contrats de droit public ayant pour objet de leur permettre d'acquérir, par une formation en alternance avec leur activité professionnelle, une qualification en rapport avec l'emploi dans lequel ils ont été recrutés ou, le cas échéant, le titre ou le diplôme requis pour l'accès au corps dont relève cet emploi. /( ...) /Au terme de son contrat, après obtention, le cas échéant, du titre ou du diplôme requis pour l'accès au corps, dont relève l'emploi dans lequel il a été recruté et sous réserve de la vérification de son aptitude par une commission nommée à cet effet, l'intéressé est titularisé dans le corps correspondant à l'emploi qu'il occupait. / (...) " ; qu'aux termes de l'article 2 du décret du 2 août 2005 pris pour l'application de cette ordonnance : " Les jeunes gens mentionnés à l'article 1er sont recrutés sur des emplois vacants des corps de catégorie C par des contrats de droit public dénommés Parcours d'accès aux carrières de la fonction publique territoriale, hospitalière et d'Etat. / Les jeunes gens ont, selon l'emploi sur lequel ils sont recrutés, la qualité d'agent de l'Etat ou de l'un de ses établissements publics administratifs. (...) " ;<br/>
<br/>
              5. Considérant que Mme A...a sollicité, au cours de l'année 2007, son recrutement sur un emploi public dans le cadre du contrat de droit public mentionné à l'article 22 bis de la loi du 11 janvier 1984 ; que le recteur de l'académie de Clermont-Ferrand l'a informée le 20 décembre 2007 de sa décision de la nommer à l'inspection académique de l'Allier mais a suspendu cette nomination dans l'attente de la communication de l'extrait du bulletin n° 2 de son casier judiciaire ; qu'à la suite de la réception de ce document, qui comportait la mention de faits ayant donné lieu à une condamnation pénale, il l'a informée, par lettre du 23 janvier 2008 qu'il avait décidé de ne pas la recruter, compte tenu des conditions requises pour avoir la qualité de fonctionnaire ; qu'après avoir obtenu du tribunal de grande instance de Moulins, par jugement du 13 février 2008, l'exclusion de la mention de cette condamnation au  bulletin n° 2 de son casier judiciaire, elle a formé un recours gracieux auprès du recteur aux fins de réexamen de sa demande de recrutement ; que ce recours a implicitement été rejeté ; <br/>
<br/>
              6. Considérant que Mme A...soutient, en premier lieu, que le tribunal administratif de Clermont-Ferrand ne pouvait, pour rejeter sa demande tendant à l'annulation de cette décision implicite, se fonder sur les dispositions de l'article 5 de la loi du 13 juillet 1983 alors que les jeunes gens recrutés dans le cadre de l'article 22 bis de la loi du 11 janvier 1984 n'ont pas la qualité de fonctionnaire lors du parcours d'accès aux carrières de la fonction publique et n'ont pas vocation à le devenir de façon certaine ;<br/>
<br/>
              7. Considérant toutefois qu'il résulte de la combinaison des dispositions précitées de l'article 22 bis de la loi du 11 janvier 1984 et de l'article 2 du décret du 2 août 2005 que les jeunes gens recrutés sur des emplois vacants des corps de catégorie C par un contrat de droit public dénommé " parcours d'accès aux carrières de la fonction publique territoriale, hospitalière et d'Etat ", qui ont la qualité d'agent de l'Etat lorsqu'ils sont recrutés sur des emplois de l'Etat, ont vocation à être titularisés dans le corps correspondant à l'emploi occupé ; qu'il appartient, dès lors, à l'autorité administrative de vérifier qu'ils remplissent les conditions requises pour cette titularisation, et en particulier la condition fixée par les dispositions précitées de l'article 5 de la loi du 13 juillet 1983, sans attendre le moment de leur titularisation, nonobstant la circonstance, d'une part, qu'ils n'ont pas encore la qualité de fonctionnaire à la date de cette vérification et, d'autre part, que l'un des objectifs de ce dispositif est de contribuer à l'insertion sociale de jeunes issus de milieux défavorisés ; que, par suite, contrairement à ce que la requérante soutient, le tribunal administratif n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant, en second lieu, que si les dispositions de l'article 5 de la loi du 13 juillet 1983 retiennent comme critère d'appréciation des conditions générales requises pour l'accès à la fonction publique, le fait, le cas échéant, que les mentions portées au bulletin n° 2 du casier judiciaire du candidat ne sont pas incompatibles avec l'exercice des fonctions, elles ne font pas obstacle à ce que, lorsque l'administration a légalement été informée des mentions portées sur ce bulletin et que, postérieurement à cette information, ces mentions sont supprimées, l'autorité compétente tienne compte des faits ainsi portés à sa connaissance, pour apprécier s'il y a lieu, compte tenu de la nature des fonctions auxquelles il postule, de recruter un candidat ayant vocation à devenir fonctionnaire ; que cette appréciation s'exerce sous le contrôle du juge de l'excès de pouvoir ; <br/>
<br/>
              9. Considérant que, si la requérante fait valoir, d'une part, qu'elle n'avait pas connaissance de l'antécédent judiciaire qui lui a été opposé par l'administration dès lors que ce jugement a été rendu par défaut et, d'autre part, que le tribunal correctionnel a fait droit à sa demande tendant à ce que le bulletin n° 2 de son casier judiciaire ne fasse plus mention de cette condamnation, la réalité des faits de violence commis en réunion, qu'elle ne conteste d'ailleurs pas, est établie ; que, par suite et alors même qu'à la date de la décision implicite de rejet, toute inscription au bulletin n° 2 de son casier judiciaire avait été supprimée, le recteur a pu, sans erreur de droit et sans entacher sa décision d'une erreur d'appréciation, se fonder sur ces faits, dont il a eu légalement connaissance, pour estimer qu'ils étaient incompatibles avec l'exercice des fonctions auxquelles la requérante postulait ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que Mme A... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Clermont-Ferrand a rejeté sa demande ; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à la SCP A. Bouzidi-Ph. Bouhanna, avocat de Mme A..., la somme demandée en application de ces dispositions et de l'article 37 de la loi du 10 juillet 1991 au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'arrêt du 18 octobre 2011 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
Article 2 : La requête présentée par Mme A...devant la cour administrative d'appel de Lyon et les conclusions présentées par la SCP A. Bouzidi-Ph. Bouhanna devant le Conseil d'Etat tendant à l'application des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative  sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...et au ministre de l'éducation nationale. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. - AGENTS RECRUTÉS PAR UN CONTRAT DE DROIT PUBLIC DANS LE CADRE DU DISPOSITIF DIT PACTE - AGENTS RECRUTÉS SUR DES EMPLOIS DE L'ETAT - VOCATION À LA TITULARISATION - EXISTENCE - CONSÉQUENCE - VÉRIFICATION DES CONDITIONS MISES À LA TITULARISATION DÈS LE STADE DU RECRUTEMENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-12 Il résulte de la combinaison des dispositions de l'article 22 bis de la loi n° 84-16 du 11 janvier 1984, issues de l'ordonnance n° 2005-901 du 2 août 2005, et de l'article 2 du décret n° 2005-902 du 2 août 2005 que les jeunes gens recrutés sur des emplois vacants des corps de catégorie C par un contrat de droit public dénommé parcours d'accès aux carrières de la fonction publique territoriale, hospitalière et d'Etat (PACTE), qui ont la qualité d'agent de l'Etat lorsqu'ils sont recrutés sur des emplois de l'Etat, ont vocation à être titularisés dans le corps correspondant à l'emploi occupé. Il appartient, dès lors, à l'autorité administrative de vérifier qu'ils remplissent les conditions requises pour cette titularisation, et en particulier la condition fixée par les dispositions de l'article 5 de la loi n° 83-634 du 13 juillet 1983 (absence de condamnation pénale incompatible avec l'exercice des fonctions), sans attendre le moment de leur titularisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
