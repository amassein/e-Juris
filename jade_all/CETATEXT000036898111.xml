<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036898111</ID>
<ANCIEN_ID>JG_L_2018_05_000000408371</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/89/81/CETATEXT000036898111.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 09/05/2018, 408371, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408371</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408371.20180509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 février et 23 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le département de la Sarthe demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le 10° de l'article 1er du décret n° 2017-122 du 1er février 2017 relatif à la réforme des minima sociaux ;<br/>
<br/>
              2°) de lui allouer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la charte européenne de l'autonomie locale ;<br/>
              - l'ordonnance n° 58-1360 du 29 décembre 1958 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 262-13 du code de l'action sociale et des familles : " Le revenu de solidarité active est attribué par le président du conseil départemental du département dans lequel le demandeur réside ou a, dans les conditions prévues au chapitre IV du titre VI du présent livre, élu domicile. / Le conseil départemental peut déléguer l'exercice de tout ou partie des compétences du président du conseil départemental en matière de décisions individuelles relatives à l'allocation aux organismes chargés du service du revenu de solidarité active mentionnés à l'article L. 262-16 ".  L'article L. 262-14 de ce code prévoit que : " La demande de revenu de solidarité active est déposée, au choix du demandeur, auprès d'organismes désignés par décret " et l'article L. 262-15 du même code que : " L'instruction administrative de la demande est effectuée à titre gratuit, dans des conditions déterminées par décret, par les services du département ou l'organisme chargé du service du revenu de solidarité active (...) ". Aux termes de l'article L. 262-16 du même code : " Le service du revenu de solidarité active est assuré, dans chaque département, par les caisses d'allocations familiales et, pour leurs ressortissants, par les caisses de mutualité sociale agricole ". L'article R. 262-25-5 du même code, créé par le 10° de l'article 1er du décret 1er février 2017 dont le département requérant demande l'annulation pour excès de pouvoir, dispose que : " Lorsqu'elle est déposée auprès des organismes mentionnés à l'article L. 262-16, la demande de revenu de solidarité active est réalisée soit par téléservice, soit par le dépôt d'un formulaire. L'utilisation du téléservice dispense, le cas échéant, l'usager de la fourniture de pièces justificatives dès lors que ces organismes disposent des informations nécessaires ou qu'elles peuvent être obtenues auprès des administrations, collectivités et organismes mentionnés à l'article L. 262-40 ", c'est-à-dire les administrations publiques, les collectivités territoriales, les organismes de sécurité sociale, de retraite complémentaire et d'indemnisation du chômage ainsi que les organismes publics ou privés concourant aux dispositifs d'insertion ou versant des rémunérations au titre de l'aide à l'emploi, lesquels sont tenus de communiquer les informations demandées.<br/>
<br/>
              2. Il résulte de ces dispositions que si l'exercice de la faculté, ouverte par les dispositions litigieuses, de déposer par téléservice une demande de revenu de solidarité active auprès des caisses d'allocations familiales ou de mutualité sociale agricole dispense l'usager de la fourniture de certaines pièces justificatives, d'une part, cette dispense ne vaut que pour les pièces se rapportant aux informations dont ces caisses disposent déjà ou qu'elles peuvent obtenir auprès d'autres administration ou organismes, d'autre part, elle n'a pour objet ou pour effet ni d'ouvrir un droit automatique à l'attribution du revenu de solidarité active en dispensant ces caisses, lorsqu'elles effectuent l'instruction de la demande, de vérifier que les conditions d'attribution de cette allocation sont remplies, ni d'imposer au président du conseil départemental de déléguer à ces caisses la décision d'attribution du revenu de solidarité active prise sur cette demande comme l'article L. 262-13 du code de l'action sociale et des familles le lui permet. Le département de la Sarthe n'est ainsi pas fondé à soutenir que les dispositions litigieuses priveraient le président du conseil départemental de la compétence qu'il tient de cet article L. 262-13 et seraient de nature à accroître le coût de cette prestation pour le département en lui interdisant d'écarter les demandes infondées ou frauduleuses. Il suit de là que ne peuvent qu'être écartés les moyens tirés de ce que ces dispositions, qu'aucun texte ni aucun principe n'imposaient de soumettre à l'avis du Conseil économique, social et environnemental, seraient, pour ce motif, entachées d'incompétence, violeraient le principe de libre administration des collectivités territoriales garanti par l'article 72 de la Constitution, porteraient une atteinte illégale à l'autonomie financière reconnue à ces collectivités par l'article 72-2 de la Constitution ou, en tout état de cause, par le paragraphe 1 de l'article 9 de la charte européenne de l'autonomie locale ou méconnaîtraient l'article L. 262-13 du code de l'action sociale et des familles.<br/>
<br/>
              3. Il résulte de ce qui précède que le département requérant n'est pas fondé à demander l'annulation des dispositions qu'il attaque.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font en tout état de cause obstacle à ce qu'une somme lui soit versée au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du département de la Sarthe est rejetée.<br/>
Article 2 : La présente décision sera notifiée au département de la Sarthe et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
