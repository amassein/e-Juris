<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044361920</ID>
<ANCIEN_ID>JG_L_2021_11_000000458346</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/19/CETATEXT000044361920.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/11/2021, 458346, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458346</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458346.20211116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 10 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme C... A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'arrêté du 2 juillet 2021 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière fixant les listes des praticiens ayant satisfait aux épreuves de vérification des connaissances prévues aux articles L. 4111-2-I et L. 4221-12 du code de la santé publique organisées au titre de la session 2020 en tant qu'il détermine la liste A des praticiens ayant réussi la spécialité ophtalmologie, en ce qu'il ne l'a pas déclarée admise ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, l'arrêté litigieux la prive de la possibilité d'exercer son activité de médecin, qui constitue son moyen principal de subsistance et que, en second lieu, elle doit rester en France, où réside son mari, médecin urgentiste, pour s'occuper de son fils qui fait l'objet d'un suivi pédopsychiatrique ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté litigieux dès lors que la délibération du jury sur laquelle l'arrêté s'est fondé pour établir la liste des praticiens admis dans la spécialité ophtalmologie, méconnaît l'article 24 de l'arrêté du 5 mars 2007 fixant les modalités d'organisation des épreuves de vérification des connaissances prévues au I de l'article L. 4111-2 et à l'article L. 4221-12 du code de la santé publique, en ce qu'elle fixe une note éliminatoire de 14/20, plus élevée que celle de 6/20 prévue par l'arrêté du 5 mars 2007, alors que la possibilité antérieurement prévue pour le jury de fixer une note éliminatoire plus élevée a été abrogée, et que le jury a entendu fixer une note minimale et non un seuil d'admission supérieur à celui établi par les textes ;<br/>
              - la délibération du jury est entachée d'une erreur manifeste d'appréciation dès lors que, d'une part, il apparaît que seuls vingt candidats sur soixante-dix-sept ont été admis, alors que les années précédentes tous les postes ont été pourvus et que, d'autre part, en prétendant fixer objectivement un seuil d'admission le jury a évincé des candidats qui avaient pourtant un niveau comparable à celui qui était exigé ;<br/>
              - l'arrêté litigieux est entaché de détournement de pouvoir dès lors que, d'une part, la décision du jury traduit une volonté de faire barrage aux candidats d'origine étrangère et, d'autre part, le jury a tenu des propos particulièrement scandaleux à l'égard des candidats, manifestant des réticences quant au nombre élevé de candidats étrangers.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes du I de l'article L. 4111-2 du code de la santé publique : " Le ministre chargé de la santé ou, sur délégation, le directeur général du Centre national de gestion peut, après avis d'une commission comprenant notamment des délégués des conseils nationaux des ordres et des organisations nationales des professions intéressées, choisis par ces organismes, autoriser individuellement à exercer les personnes titulaires d'un diplôme, certificat ou autre titre permettant l'exercice, dans le pays d'obtention de ce diplôme, certificat ou titre, de la profession de médecin, dans la spécialité correspondant à la demande d'autorisation, chirurgien-dentiste, le cas échéant dans la spécialité correspondant à la demande d'autorisation, ou de sage-femme. / Ces personnes doivent avoir satisfait à des épreuves anonymes de vérification des connaissances, organisées par profession et, le cas échéant, par spécialité, et justifier d'un niveau suffisant de maîtrise de la langue française. (...) Des dispositions réglementaires fixent les conditions d'organisation de ces épreuves. Le nombre maximum de candidats susceptibles d'être reçus à ces épreuves pour chaque profession et, le cas échéant, pour chaque spécialité est fixé par arrêté du ministre chargé de la santé en tenant compte, notamment, de l'évolution des nombres d'étudiants déterminés en application du deuxième alinéa du I de l'article L. 631-1 du code de l'éducation et de vérification du niveau de maîtrise de la langue française. (...) " En application de ces dispositions et d'un arrêté du ministre des solidarités et de la santé du 5 juin 2020, des épreuves de vérification des connaissances ont été organisées au titre de la session 2020, que la requérante affirme avoir passées dans la spécialité " ophtalmologie ". Elle demande au juge des référés du Conseil d'Etat, statuant en application des dispositions de l'article L. 521-1 du code de justice administrative citées ci-dessus, de suspendre l'exécution de l'arrêté du 2 juillet 2021 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière fixant les listes des personnes qui ont satisfait aux épreuves de vérification des connaissances prévues aux articles L. 4111-2-I et L. 4221-12 du code de la santé publique en tant qu'il ne l'a pas déclarée admise dans cette spécialité. <br/>
<br/>
              3. La condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              4. Pour caractériser l'urgence qui justifie selon elle la suspension de l'arrêté qu'elle conteste, la requérante se borne à soutenir que cet arrêté la prive de la possibilité d'exercer son activité de médecin en France, où elle est arrivée en décembre 2019 pour rejoindre son mari, médecin urgentiste, avec son fils qui est scolarisé et fait l'objet d'un suivi pédopsychiatrique. Cette circonstance ne saurait suffire à constituer une situation d'urgence qui justifierait la suspension de l'arrêté en cause sur le fondement de l'article L. 521-1 du code de justice administrative. La condition d'urgence posée par ces dispositions n'étant pas remplie, il convient de rejeter la requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du même code. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C... A... B....<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
Fait à Paris, le 16 novembre 2021<br/>
Signé : Guillaume Goulard<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
