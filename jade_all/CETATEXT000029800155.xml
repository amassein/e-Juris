<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800155</ID>
<ANCIEN_ID>JG_L_2014_11_000000382651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 13/11/2014, 382651, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382651.20141113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le département d'Eure-et-Loir, représenté par le président du conseil général ; le département d'Eure-et-Loir demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-231 du 24 février 2014 portant délimitation des cantons dans le département d'Eure-et-Loir ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral, notamment son article L. 191-1 ; <br/>
<br/>
              Vu le code général des collectivités territoriales, notamment ses articles L. 3113-1 et L. 3113-2 ; <br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu l'ordonnance du 23 juillet 2014 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevée par le département d'Eure-et-Loir ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général " ; <br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de l'Eure-et-Loir, compte tenu de l'exigence de réduction du nombre des cantons de ce département de vingt-neuf à quinze résultant de l'application de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la régularité de la procédure juridictionnelle devant le Conseil d'Etat :<br/>
<br/>
              3. Considérant que le moyen tiré de ce que le jugement par des membres de la section du contentieux du Conseil d'Etat des présentes requêtes dirigées contre un décret pris après avis de la section de l'intérieur du Conseil d'Etat, méconnaîtrait le droit des requérants à être jugé par un tribunal indépendant et impartial garanti par les stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté dès lors que les membres du Conseil d'Etat statuant sur ces requêtes n'ont pas pris part à la délibération de l'avis rendu sur le décret attaqué ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              En ce qui concerne la compétence de l'auteur du décret attaqué :<br/>
<br/>
              4. Considérant qu'il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au pouvoir réglementaire de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement son exécution ; que le décret attaqué du 24 février 2014, qui se limite à modifier les circonscriptions électorales du département de l'Eure-et-Loir, n'appelle aucune mesure d'exécution que le garde des sceaux, ministre de la justice serait compétent pour signer ou contresigner ; qu'il suit de là que ce décret n 'avait pas à être contresigné par le garde des sceaux, ministre de la justice ; <br/>
<br/>
              En ce qui concerne la procédure d'élaboration du décret attaqué :<br/>
<br/>
              6. Considérant qu'aucune disposition législative notamment pas l'article L. 1111-9 du code général des collectivités territoriales, ni réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des communes ou des établissements publics de coopération intercommunale à fiscalité propre, en plus de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales ; que le département d'Eure-et-Loir ne peut, par suite, soutenir que le décret attaqué serait entaché sur ce point d'un vice de procédure ; <br/>
<br/>
              7. Considérant qu'aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des élus du département, indépendamment de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales ; que la requête ne peut, à cet égard et en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, ni aux propos tenus par le ministre de l'intérieur les 13 et 26 mars 2013 devant les parlementaires, lesquels sont dépourvus de caractère réglementaire ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne le calcul de la population de chaque canton :<br/>
<br/>
              8. Considérant que si le département d'Eure-et-Loir soutient que le décret serait illégal au motif qu'il prend en compte la population municipale et non le nombre d'électeurs par commune ni la population saisonnière pour procéder au découpage cantonal, ce moyen ne peut qu'être écarté dès lors qu'aux termes de l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué, dont la légalité n'est pas contestée : " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ; que, par ailleurs, si le requérant soutient que le décret est illégal au motif que le calcul précis de la population des cantons urbains est impossible compte tenu de la méthode retenue par l'Institut national de la statistique et des études économiques, ce moyen, qui n'est pas assorti des précisions permettant d'en apprécier le bien-fondé, ne peut qu'être écarté ; <br/>
<br/>
              9. Considérant que le département soutient, par la voie de l'exception, que l'article 8 du décret n° 2014-112 du 6 février 2014 portant différentes mesures d'ordre électoral serait illégal ; que s'il soutient, en premier lieu, qu'il serait illégal faute de comporter le contreseing du ministre de l'économie, du redressement productif et du numérique et de la ministre des outre-mer, il n'appelle aucune mesure d'exécution que ces deux ministres seraient compétents pour signer ou contresigner ; qu'en deuxième lieu, la délimitation des nouvelles circonscriptions cantonales devait, conformément aux dispositions de l'article 7 de la loi du 11 juillet 1990, être effectuée au plus tard un an avant le prochain renouvellement général des conseils généraux ; que, dans les circonstances de l'espèce, eu égard, d'une part, aux délais inhérents à l'élaboration de l'ensemble des projets de décrets de délimitation des circonscriptions cantonales, à la consultation des conseils généraux et à la saisine pour avis du Conseil d'Etat, d'autre part, à la circonstance que la déclinaison à l'échelon infra-communal des chiffres de population applicables à compter du 1er janvier 2014, nécessaire à la délimitation de certains cantons, n'était pas disponible à la date à laquelle devait être entreprise la délimitation des nouvelles circonscriptions cantonales, le décret du 6 février 2014 a pu légalement prévoir que le chiffre de population municipale auquel il convenait de se référer était le chiffre authentifié par le décret n° 2012-1479 du 27 décembre 2012 et non celui que prévoit le décret n° 2013-1289 du 27 décembre 2013, qui authentifie les chiffres de population auxquels il convient, en principe, de se référer pour l'application des lois et règlements à compter du 1er janvier 2014 ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que le département d'Eure-et-Loir n'est pas fondé à soutenir que l'exigence d'une délimitation des cantons à partir de bases essentiellement démographiques a été méconnue en se référant à la population authentifiée au 1er janvier 2014, dès lors que c'est la population authentifiée au 1er janvier 2013 qui doit être prise en compte ; <br/>
<br/>
              En ce qui concerne l'atteinte au principe d'égalité devant le suffrage :<br/>
<br/>
              11. Considérant que le département ne peut utilement soulever un moyen tiré de ce que le décret attaqué n'aurait pas procédé à un rééquilibrage des écarts de population par canton d'un département à un autre dès lors que ce décret ne concerne que le département d'Eure-et-Loir ; <br/>
<br/>
              12. Considérant qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que si le requérant soutient que certains cantons ruraux tels que celui de Saint-Lubin-des-Joncherets seraient plus peuplés que des cantons urbains comme celui Chartres-2 et qu'un tel écart est de nature à pénaliser la représentation des territoires ruraux, il n'est toutefois pas allégué que la délimitation des cantons du département d'Eure-et-Loir n'aurait pas été opérée conformément aux règles posées au III de l'article L. 3113-2 du code général des collectivités territoriales, notamment que le territoire de chaque canton n'aurait pas été établi sur des bases essentiellement démographiques ; que, par suite, la seule circonstance que les zones rurales seraient, à raison de la mise en oeuvre de ces règles, moins bien représentées que les zones urbaines au sein de l'assemblée départementale est sans incidence sur la légalité du décret attaqué ; que de même, ni les dispositions de l'article L. 3113-2 du code général des collectivité territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Gouvernement de prendre comme critères de délimitation de ces circonscriptions électorales l'absence de disparité de superficie entre cantons ; que le moyen tiré de ce que la suppression des chefs-lieux de canton aurait des conséquences, notamment financières, graves sur les services publics dont la compétence est celle du conseil général, ne saurait utilement être invoqué dès lors qu'il ressort des pièces du dossier que la délimitation des cantons a été établie sur des bases essentiellement démographiques ;<br/>
<br/>
              En ce qui concerne le non respect du périmètre d'autres circonscriptions électorales ou subdivisions administratives :<br/>
<br/>
              13. Considérant que ni les dispositions précitées, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de respecter les limites des anciens cantons ; que, de même, ni les dispositions précitées, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives ou des circonscriptions judiciaires, les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou les limites des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ; que, de même encore, si l'article L. 192 du code électoral, dans sa rédaction antérieure à l'intervention de la loi précitée du 17 mai 2013, relatif aux modalités de renouvellement des conseils généraux, faisait référence aux arrondissements, aucun texte en vigueur à la date du décret attaqué ne mentionne ces arrondissements, circonscriptions administratives de l'Etat, pour la détermination des limites cantonales ; que, par suite, le département ne saurait utilement se prévaloir de ce que la délimitation de plusieurs cantons ne correspondrait pas à celle d'autres circonscriptions électorales ou à celle de subdivisions administratives ; qu'il ne ressort pas non plus des pièces du dossier qu'en ne faisant pas coïncider les limites des cantons redéfinis avec les limites des anciens arrondissements, des circonscriptions judicaires, des périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale, le Premier ministre aurait commis une erreur manifeste d'appréciation ; <br/>
<br/>
              En ce qui concerne la détermination des bureaux centralisateurs :<br/>
<br/>
              14. Considérant que la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un " bureau centralisateur " sans mentionner les chefs-lieux de canton est, en tout état de cause, sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département d'Eure-et-Loir ; <br/>
<br/>
              En ce qui concerne le détournement de pouvoir :<br/>
<br/>
              15. Considérant que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que le département d'Eure-et-Loir n'est pas fondé à demander l'annulation du décret attaqué ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête du département d'Eure-et-Loir est rejetée.<br/>
Article 2 : La présente décision sera notifiée au département d'Eure-et-Loir et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
