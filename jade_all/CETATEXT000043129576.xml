<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043129576</ID>
<ANCIEN_ID>JG_L_2021_02_000000432064</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/95/CETATEXT000043129576.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 11/02/2021, 432064, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432064</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432064.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire enregistrés les 28 juin 2019, 21 janvier et 29 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, les associations des riverains du circuit de Lédenon, d'Uzege - Pont du Gard durable et Anti-bruit de voisinage demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre de l'intérieur du 29 avril 2019 portant homologation du circuit de vitesse de Lédenon (Gard) et à ce qu'il soit mis à la charge de l'Etat la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Les associations requérantes soutiennent que l'arrêté du 29 avril 2019 :<br/>
              - est entaché d'incompétence ;<br/>
              - est entaché d'un vice de procédure substantiel au regard des articles<br/>
 R. 331-38 et suivants du code du sport ; <br/>
              - a été délivré sur la base d'un dossier de demande d'homologation incomplet ;<br/>
              - est entaché d'une erreur de droit au regard de l'article R. 331-43 du code du sport ;<br/>
              - est entaché d'une erreur de droit au regard des articles R. 1336-6 et R. 1336-7 du code de la santé publique ;<br/>
              - est entaché d'une erreur manifeste d'appréciation au regard de ces articles R. 1336-6 et R. 1336-7 du code du sport.<br/>
<br/>
<br/>
              Par un mémoire en défense enregistré le 23 décembre 2019, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés ne sont pas fondés.<br/>
<br/>
              Par deux mémoires, enregistrés les 13 août 2020 et 14 janvier 2021 la SARL Circuit de Lédenon conclut au rejet de la requête et à ce que soit mise à la charge des associations requérantes la somme de 5 000 euros au titres des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code du sport ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2013-728 du 12 août 2013 ;<br/>
              - le décret n° 2014-408 du 16 avril 2014 ;<br/>
              - le code de justice administrative et le décret n°2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme A... D..., rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 29 avril 2019, le ministre de l'intérieur a homologué le circuit de vitesse de Lédenon et fixé les conditions de son utilisation. Les associations des riverains du circuit de Lédenon, d'Uzege-Pont du Gard durable et Anti -bruit de voisinage demandent l'annulation pour excès de pouvoir de cet arrêté.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En premier lieu, il résulte des dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, que le délégué à la sécurité routière, placé sous l'autorité du ministre de l'intérieur disposait, du fait de sa nomination et eu égard aux attributions de la délégation à la sécurité routière, d'une délégation lui donnant compétence pour signer l'arrêté attaqué.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article R. 331-37 du code du sport, l'homologation des circuits sur lesquels la vitesse des véhicules peut dépasser 200 km/h est accordée par arrêté du ministre de l'intérieur, après visite sur place et avis de la Commission nationale d'examen des circuits de vitesse. L'article R. 331-38 du même code dispose que " La Commission nationale d'examen des circuits de vitesse comprend huit membres : / 1° Trois membres désignés par le ministre de l'intérieur ; / 2° Un membre désigné par le ministre chargé de l'écologie ; / 3° Un membre désigné par le ministre chargé des transports ; / 4° Un membre désigné par le ministre chargé des sports ; / 5° Un membre proposé par la Fédération française du sport automobile ; / 6° Un membre proposé par la Fédération française de motocyclisme. / Les membres de la commission et son président, choisi parmi eux, sont nommés par le ministre de l'intérieur pour un mandat de trois ans renouvelable. / Chaque titulaire a un suppléant nommé dans les mêmes conditions, qui le remplace en cas d'empêchement. (...) ". Contrairement à ce que soutiennent les associations requérantes, M. C... B... était bien membre de la Commission nationale d'examen des circuits de vitesse lorsque celle-ci a rendu son avis le 16 avril 2019, l'intéressé ayant été nommé comme suppléant au titre de la Fédération française de motocyclisme par arrêté du 26 février 2018. Par suite, le moyen tiré de ce que la Commission nationale d'examen des circuits aurait été irrégulièrement composée lorsqu'elle a rendu son avis doit être écarté.<br/>
<br/>
              4. En troisième lieu, il ne ressort pas des pièces du dossier, en particulier du procès-verbal de la réunion du 16 avril 2019, que la Commission nationale d'examen des circuits de vitesse, qui s'est rendue sur place en mai 2018, aurait rendu son avis en méconnaissance des dispositions du code du sport relatives à ses missions. <br/>
<br/>
              5. En quatrième lieu, aux termes de l'article A. 331-21-2 du code du sport : " La personne physique ou morale qui demande l'homologation d'un circuit ou le renouvellement de cette homologation, doit constituer un dossier qui comprend : (...) / 4° Les dispositions prévues pour assurer la sécurité des personnes et la tranquillité publique. / (...) La demande de renouvellement est transmise deux mois avant la date de fin de validité de l'homologation ". Si les associations requérantes font valoir que la préfecture du Gard et l'agence régionale de santé avaient relevé les insuffisances du dossier d'homologation en ce qui concerne la sécurité des personnes et la tranquillité publique, il ressort des éléments produits au cours de l'instruction que le gestionnaire du circuit a fourni dans le dossier de demande différents éléments sur ces points. Le moyen tiré de la méconnaissance de l'article A. 331-21 du code de sport doit, par suite, être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              6. Aux termes de l'article R. 1336-6 du code de la santé publique : " Lorsque le bruit mentionné à l'article R. 1336-5 a pour origine une activité professionnelle autre que l'une de celles mentionnées à l'article R. 1336-10 ou une activité sportive, culturelle ou de loisir, organisée de façon habituelle ou soumise à autorisation, l'atteinte à la tranquillité du voisinage ou à la santé de l'homme est caractérisée si l'émergence globale de ce bruit perçu par autrui, telle que définie à l'article R. 1336-7, est supérieure aux valeurs limites fixées au même article. / Lorsque le bruit mentionné à l'alinéa précédent, perçu à l'intérieur des pièces principales de tout logement d'habitation, fenêtres ouvertes ou fermées, est engendré par des équipements d'activités professionnelles, l'atteinte est également caractérisée si l'émergence spectrale de ce bruit, définie à l'article R. 1336-8, est supérieure aux valeurs limites fixées au même article. / Toutefois, l'émergence globale et, le cas échéant, l'émergence spectrale ne sont recherchées que lorsque le niveau de bruit ambiant mesuré, comportant le bruit particulier, est supérieur à 25 décibels pondérés A si la mesure est effectuée à l'intérieur des pièces principales d'un logement d'habitation, fenêtres ouvertes ou fermées, ou à 30 décibels pondérés A dans les autres cas ". Cet article a ainsi fixé, dans un but de santé et de tranquillité publiques, des valeurs limites à respecter en toute hypothèse en matière de bruit de voisinage, notamment par des activités sportives.<br/>
<br/>
              7. Il résulte des dispositions précitées de l'article R. 331-35 du code du sport et de celles de l'article R. 33119 du même code, qui habilitent les fédérations sportives délégataires à déterminer les règles techniques et de sécurité applicables aux événements et aux sites de pratique relevant des disciplines pour lesquelles elles ont obtenu délégation, qu'il leur appartient d'édicter les règles générales relatives au bruit émis par les véhicules terrestres à moteur participant à des manifestations organisées dans des lieux non ouverts à la circulation publique et, le cas échéant, au ministre de l'intérieur ou au préfet, lors de la procédure d'homologation des circuits de vitesse et d'autorisation des concentrations et manifestations, de définir les conditions d'exercice spécifiques relatives au bruit de ces manifestations. En outre, il incombe à l'exploitant du circuit de veiller au respect des valeurs limites d'émergence fixées aux articles R. 1336-7 et R. 1336-8 du code de la santé publique. L'inobservation de ces dernières dispositions est susceptible de conduire l'autorité administrative compétente à prendre, en vertu de l'article R. 1336-11 du même code, une ou plusieurs des mesures prévues à l'article L. 171-8 du code de l'environnement.<br/>
<br/>
              8. D'une part, les dispositions précitées de l'article R. 1336-6 du code de la santé publique, dont le ministre n'était pas tenu, à peine d'illégalité, de rappeler l'existence ou la teneur dans l'arrêté contesté, s'imposent à l'exploitant du circuit homologué. D'autre part, l'arrêté attaqué impose, afin de préserver la tranquillité publique, des prescriptions particulières relatives à l'utilisation du circuit, notamment la limitation du niveau sonore des véhicules autorisés à circuler sur le circuit de vitesse à 3 décibels aux valeurs, diminuées de 3 dBA, fixées par les fédérations sportives ayant reçu délégation, ainsi qu'aux modalités de mesure et de contrôle des émissions sonores des véhicules mesurés à la source et en mode dynamique. Il exige en outre la réalisation, dans un délai de 12 mois, une étude de conception et de réalisation, dans l'emprise du circuit, d'un écran acoustique orienté vers la commune de Lédenon. Il ne ressort pas des pièces du dossier que cet arrêté autoriserait le circuit à fonctionner dans des conditions qui conduiraient, de façon structurelle, au non-respect des valeurs limites d'émergence fixées aux articles R. 1336-et R.1336-8 du code de la santé publique. Par suite, les moyens tirés de ce que l'arrêté d'homologation aurait méconnu les dispositions des articles R.1336-6, R.1336-7 et R. 1336-8 du code de la santé publique doivent être écartés.<br/>
<br/>
              9. Il résulte de ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation de l'arrêté d'homologation du 29 avril 2019. Leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent, en conséquence, être rejetées. Il y a lieu, en revanche, à ce titre, dans les circonstances de l'espèce, de mettre à la charge de chacune des associations, la somme de 500 euros à verser à la SARL Circuit de Lédenon.<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			 --------------<br/>
<br/>
Article 1er : La requête de l'association des riverains du circuit de Lédenon et autres est rejetée.<br/>
Article 2 : L'association des riverains du circuit de Lédenon, l'association d'Uzege - Pont du Gard durable et l'association Anti-bruit de voisinage verseront chacune la somme de 500 euros à la SARL Circuit de Lédenon au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3: La présente décision sera notifiée à l'association des riverains du circuit de Lédenon, premier dénommé, au ministre de l'intérieur et à la SARL Circuit de Lédenon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
