<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029046216</ID>
<ANCIEN_ID>JG_L_2014_06_000000366877</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/04/62/CETATEXT000029046216.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 04/06/2014, 366877, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366877</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366877.20140604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société par actions simplifiée Convatec, dont le siège est 90, boulevard National à La Garenne Colombes (92250), représentée par son président ; la société Convatec demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 29 octobre 2012 des ministres en charge de la santé et de la sécurité sociale portant refus d'inscription sous nom de marque du pansement en fibres de carboxyméthylcellulose contenant de l'argent " Aquacel Ag " sur la liste des produits et prestations remboursables mentionnée à l'article L. 165-1 du code de la sécurité sociale, ainsi que la décision implicite de rejet résultant du silence gardé par le ministre des affaires sociales et de la santé sur son recours gracieux tendant au retrait de cette décision ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Convatec ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 165-1 du code de la sécurité sociale : " Le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel (...) est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute Autorité de santé (...) " ; qu'aux termes de l'article R. 165-2 du même code : " Les produits ou les prestations mentionnés à l'article L. 165-1 sont inscrits sur la liste prévue audit article au vu de l'appréciation du service qui en est attendu (...) / Les produits ou prestations dont le service attendu est insuffisant pour justifier l'inscription au remboursement ne sont pas inscrits sur la liste " ; qu'aux termes de l'article R. 165-16 de ce même code : " Les décisions portant refus d'inscription sur la liste prévue à l'article L. 165-1, refus de renouvellement d'inscription, radiation de la liste ou refus de modification de l'inscription, du tarif ou du prix doivent, dans la notification au fabricant ou distributeur, être motivées et mentionner les voies et délais de recours qui leur sont applicables " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que la société Convatec a sollicité l'inscription, par nom de marque, sur la liste des produits et prestations remboursables mentionnée à l'article L. 165-1 du code de la sécurité sociale cité au point 1, du pansement en fibres de carboxyméthylcellulose contenant de l'argent " Aquacel Ag " ; que la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé, mentionnée au même article L. 165-1, a rendu le 25 septembre 2012 un avis défavorable à cette demande, en estimant que l'intérêt d'ajouter de l'argent à un pansement en fibres de carboxyméthylcellulose Aquacel n'était pas établi et qu'ainsi, le service attendu du produit était insuffisant pour justifier son inscription ; qu'en adoptant les motifs de cet avis, les ministres chargés de la sécurité sociale et de la santé ont, par une décision du 29 octobre 2012, rejeté la demande de la société Convatec ; que la société Convatec demande l'annulation pour excès de pouvoir de cette décision, ainsi que de la décision implicite de rejet née du silence gardé par les ministres sur son recours gracieux exercé à la suite de cette première décision ;<br/>
<br/>
              Sur la légalité externe de la décision du 29 octobre 2012 :<br/>
<br/>
              3. Considérant qu'en indiquant avoir décidé de suivre l'avis du 25 septembre 2012 émis par la commission précitée, qui prend en compte les différents critères mentionnés à l'article R. 165-2 du code de la sécurité sociale et dont il n'est pas contesté qu'il était joint à la décision du 29 octobre 2012, après avoir d'ailleurs été communiqué à la société requérante afin qu'elle puisse produire ses observations, les ministres ont suffisamment motivé leur décision ; que, par suite, le moyen tiré de l'insuffisance de motivation de la décision du 29 octobre 2012 doit être écarté ; <br/>
<br/>
              Sur la légalité interne de la décision du 29 octobre 2012 :<br/>
<br/>
              4. Considérant que la société requérante soutient que les auteurs des décisions attaquées ont porté atteinte au principe de sécurité juridique en retenant, conformément à l'avis du 25 septembre 2012 de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé, que les pansements " Aquacel " étaient indiqués dans les plaies chroniques et aiguës sans distinction de phase, que les pansements " Aquacel " et " Aquacel Ag " étaient plus aptes à favoriser la détersion des plaies alors que les pansements " Urgotul " et " Urgotul Ag " étaient plus adaptés aux plaies en phase d'épidermisation, et que, au total, l'étude fournie par la société Convatec ne permettait pas de comparer les pansements " Aquacel Ag " et " Urgotul Ag " ni d'établir l'intérêt des pansements " Aquacel Ag " par rapport aux pansements " Aquacel " ; que, toutefois, il ressort des pièces du dossier, en premier lieu, que les pansements " Aquacel " sont, selon leur notice d'utilisation, indiqués pour les plaies exsudatives aigües et chroniques sans distinction de phase et que la nomenclature définie par l'arrêté du 16 juillet 2010 relatif à la modification de la procédure d'inscription et des conditions de prise en charge des articles pour pansements inscrits à la section 1, chapitre 3, titre Ier, de la liste prévue à l'article L. 165-1 (LPP) du code de sécurité sociale prévoit que la prise en charge des pansements en fibres de carboxyméthylcellulose est assurée pour les plaies aigües et chroniques très exsudatives sans distinction de phase, en second lieu, qu'il n'est pas établi qu'il n'existe pas de différences notables entre les propriétés physico-chimiques des pansements comparés dans l'étude fournie par la société Convatec, comme l'a également relevé la commission dans son avis, et l'efficacité de ces pansements selon les phases de cicatrisation, en troisième lieu, que l'étude comparative fournie par la société Convatec comporte des biais et des lacunes, relevés aussi par l'avis de la commission, qui empêchent de comparer valablement les pansements " Aquacel Ag " aux pansements " Urgotul Ag " et d'établir l'intérêt des pansements " Aquacel Ag " par rapport aux pansements " Aquacel " ; qu'ainsi, la société requérante n'est, en tout état de cause, pas fondée à soutenir que le principe de sécurité juridique aurait été méconnu ;<br/>
<br/>
              5. Considérant que, si la société requérante soutient que la commission a traité de manière différente sa demande d'inscription en nom de marque des pansements " Aquacel Ag " et celles présentées pour d'autres dispositifs médicaux, parmi lesquels figuraient notamment " Urgotul Ag ", " Urgotul Trio Ag ", " Ialuset Plus " et " Effidia ", il ressort toutefois des pièces du dossier  que les différences entre les avis rendus à propos de ces différents dispositifs étaient justifiées par l'évolution dans le temps des critères et des exigences méthodologiques de la commission et par les différences affectant les indications de ces dispositifs médicaux, qui n'étaient pas comparables, ainsi que l'objet même de leur évaluation ; que, par suite, la société requérante n'est pas fondée à soutenir que la décision du 29 octobre 2012 méconnaîtrait le principe d'égalité ni qu'elle serait entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
              6. Considérant que la société requérante ne saurait non plus utilement soutenir que la décision attaquée conduirait à une augmentation des dépenses à la charge de l'assurance maladie dès lors que cette décision n'est motivée que par l'insuffisance du service médical rendu par les pansements " Aquacel Ag " ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la société Convatec n'est pas fondée à demander l'annulation des décisions attaquées ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Convatec est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Convatec et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée pour information à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
