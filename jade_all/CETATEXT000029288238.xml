<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288238</ID>
<ANCIEN_ID>JG_L_2014_07_000000363141</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 23/07/2014, 363141</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363141</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363141.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er octobre 2012 et 2 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant... ; M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC00271 du 8 mars 2012 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0902020 du 9 juin 2010 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 13 novembre 2008 par laquelle le ministre de l'éducation nationale a prononcé son licenciement en fin de stage et rejeté son recours gracieux et, d'autre part, à ce qu'il soit enjoint au ministre de le réintégrer et de l'affecter dans le ressort de l'académie de Strasbourg ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 92-1189 du 6 novembre 1992 ;<br/>
<br/>
              Vu l'arrêté du 22 août 2005 relatif au certificat d'aptitude au professorat de lycée professionnel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 10 du décret du 6 novembre 1992 relatif au statut particulier des professeurs de lycée professionnel, dans sa rédaction applicable à la date des décisions litigieuses : " Les candidats reçus aux concours prévus à l'article 4 ci-dessus sont nommés professeurs de lycée professionnel stagiaires et effectuent un stage d'une durée d'un an. (.../ Au cours de cette année de stage, les professeurs de lycée professionnel stagiaires reçoivent une formation professionnelle initiale dans les instituts universitaires de formation des maîtres et subissent les épreuves du certificat d'aptitude au professorat de lycée professionnel, dont les modalités sont définies par un arrêté du ministre chargé de l'éducation. (...) Les professeurs de lycée professionnel stagiaires qui obtiennent le certificat d'aptitude au professorat de lycée professionnel sont titularisés par le recteur de l'académie dans le ressort de laquelle ils ont effectué leur stage. A titre exceptionnel, le recteur de l'académie dans le ressort de laquelle le stage a été effectué peut autoriser l'accomplissement d'une seconde année de stage. A l'issue de cette période, l'intéressé est soit titularisé par le recteur de l'académie dans le ressort de laquelle il a effectué cette seconde année, soit licencié, soit réintégré dans son grade d'origine ou dans son corps, cadre d'emplois ou emploi d'origine (...) " ; qu'aux termes de l'article 3 de l'arrêté du 22 août 2005 relatif au certificat d'aptitude au professorat de lycée professionnel, alors applicable : " Le jury académique se prononce après avoir pris connaissance des éléments du dossier de compétences du professeur stagiaire (...) " ; qu'aux termes de l'article 4 du même arrêté, alors applicable : " Après délibération, le jury établit la liste des professeurs stagiaires qui ont obtenu le certificat d'aptitude au professorat de lycée professionnel. / Les stagiaires non admis au certificat d'aptitude doivent avoir subi un entretien avec le jury ou avoir été inspectés. Le jury peut procéder à un entretien avec le stagiaire même si son dossier de compétences comporte un rapport d'inspection. / En outre, pour les stagiaires effectuant leur première année de stage qui n'ont pas été admis au certificat d'aptitude, il formule un avis sur l'intérêt, au regard de l'aptitude professionnelle, d'autoriser le stagiaire à effectuer une seconde et dernière année de stage " ; qu'aux termes de l'article 6 de ce même arrêté, alors applicable : " Le recteur arrête par section, éventuellement par option, la liste des professeurs stagiaires qui, admis au certificat d'aptitude, sont titularisés en qualité de professeur de lycée professionnel. Il arrête, par ailleurs, la liste des professeurs stagiaires autorisés à accomplir une seconde année de stage. Les professeurs stagiaires qui n'ont été ni admis au certificat d'aptitude, ni autorisés à accomplir une seconde année de stage sont, selon le cas, licenciés ou réintégrés dans leur corps et leur grade d'origine " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le jury académique se prononce à l'issue d'une période de formation et de stage ; que, s'agissant non d'un concours ou d'un examen mais d'une procédure tendant à l'appréciation de la manière de servir qui doit être faite en fin de stage, cette appréciation peut être censurée par le juge de l'excès de pouvoir en cas d'erreur manifeste ; <br/>
<br/>
              3. Considérant que, pour confirmer par l'arrêt attaqué le rejet, par le tribunal administratif de Strasbourg, de la demande de M. A...B..., professeur de lycée professionnel stagiaire qui avait accompli une année de stage à l'institut universitaire de formation des maîtres de Strasbourg, tendant à l'annulation de l'arrêté du ministre de l'éducation nationale du 13 novembre 2008 le licenciant, après que le jury académique eut refusé de lui délivrer le certificat d'aptitude au professorat de lycée professionnel, la cour administrative d'appel de Nancy a jugé qu'il n'appartenait pas au juge administratif de contrôler l'appréciation portée par le jury sur la valeur professionnelle du requérant ; qu'il résulte de ce qui vient d'être dit qu'elle a, ce faisant, commis une erreur de droit ; que, par suite, M. B...est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 8 mars 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-02-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL ENSEIGNANT. - APPRÉCIATION PORTÉE PAR LE JURY ACADÉMIQUE EN FIN DE STAGE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - EXISTENCE - DEGRÉ DU CONTRÔLE - CONTRÔLE RESTREINT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS ÉCHAPPANT AU CONTRÔLE DU JUGE. - ABSENCE - APPRÉCIATION PORTÉE PAR UN JURY ACADÉMIQUE EN FIN DE STAGE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - APPRÉCIATION PORTÉE PAR UN JURY ACADÉMIQUE EN FIN DE STAGE [RJ1].
</SCT>
<ANA ID="9A"> 30-01-02-01 Les jurys académiques, appelés notamment à se prononcer en vue de la titularisation des professeurs stagiaires nommés dans certains corps, statuent à l'issue d'une période de formation et de stage. S'agissant non d'un concours ou d'un examen mais d'une procédure tendant à l'appréciation de la manière de servir qui doit être faite en fin de stage, cette appréciation est contrôlée par le juge de l'excès de pouvoir et peut être censurée en cas d'erreur manifeste.</ANA>
<ANA ID="9B"> 54-07-02-01 Les jurys académiques, appelés notamment à se prononcer en vue de la titularisation des professeurs stagiaires nommés dans certains corps, statuent à l'issue d'une période de formation et de stage. S'agissant non d'un concours ou d'un examen mais d'une procédure tendant à l'appréciation de la manière de servir qui doit être faite en fin de stage, cette appréciation est contrôlée par le juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-07-02-04 Les jurys académiques, appelés notamment à se prononcer en vue de la titularisation des professeurs stagiaires nommés dans certains corps, statuent à l'issue d'une période de formation et de stage. S'agissant non d'un concours ou d'un examen mais d'une procédure tendant à l'appréciation de la manière de servir qui doit être faite en fin de stage, cette appréciation peut être censurée en cas d'erreur manifeste.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., en ce qu'elles soustraient cette appréciation à tout contrôle du juge de l'excès de pouvoir, CE, 10 février 2006, M. Poyet, n° 257484, inédite au Recueil ; CE, 14 novembre 2001, Ministre de l'éducation nationale c/ Chabert, n° 223506, inédite au Recueil ; CE, 29 décembre 1997, Ministre de l'éducation nationale c/ Mme Barbulesco, n° 150276, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
