<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027164324</ID>
<ANCIEN_ID>JG_L_2013_03_000000364827</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/16/43/CETATEXT000027164324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 11/03/2013, 364827</TITRE>
<DATE_DEC>2013-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364827</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364827.20130311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre de la défense, enregistré le 27 décembre 2012 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1207109 du 14 décembre 2012 par laquelle le juge des référés du tribunal administratif de Versailles, statuant en application de l'article <br/>
L. 551-1 du code de justice administrative, à la demande de la société Aeromécanic, a annulé la procédure négociée de passation du marché public de prestations de maintenance des hélicoptères " Puma " de l'armée française et a enjoint à la structure intégrée de maintien en conditions opérationnelles des matériels aéronautiques du ministère de la défense (SIMMAD), si elle entendait conclure le marché, d'en reprendre la procédure de passation au stade de l'envoi des lettres de consultation ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Aeromécanic ; <br/>
<br/>
              3°) de mettre à la charge de cette société le versement de la somme de 3 700 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 février 2013, présentée pour la société Aeromécanic ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 février 2013, présentée pour la société Sabena Technics DNR ; <br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société Aéromecanic et de la SCP Peignot, Garreau, Bauer-Violas, avocat de la société Sabena Technics DNR,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société Aéromecanic et à la SCP Peignot, Garreau, Bauer-Violas, avocat de la société Sabena Technics DNR ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; que selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence publié le 8 octobre 2011 au journal officiel de l'Union européenne et le 9 octobre 2011 au Bulletin officiel des annonces des marchés publics, la structure intégrée de maintien en conditions opérationnelles des matériels aéronautiques du ministère de la défense (SIMMAD) a, sur le fondement de la troisième partie du code des marchés publics applicable aux marchés de défense ou de sécurité, lancé une procédure de passation pour un marché négocié relatif aux visites d'entretien des hélicoptères " Puma " de l'armée française ; que l'offre de la société Aeromécanic, candidate à l'attribution du lot n° 1 du marché relatif aux hélicoptères stationnés en France métropolitaine, a été écartée au profit de l'offre de la société Sabena Technics DNR, attributaire du lot ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Versailles a, à la demande de la société Aeromécanic, sur le fondement de l'article L. 551-1 du code de justice administrative, annulé dans son intégralité la procédure de passation pour le lot n° 1 ; que le ministre de la défense se pourvoit en cassation contre cette ordonnance ; <br/>
<br/>
              3. Considérant que le mémoire présenté devant le Conseil d'Etat par la société Sabena Technics DNR, qui avait la qualité de partie en première instance et dont les conclusions tendent aux mêmes fins que le pourvoi du ministre de la défense, ont le caractère d'un pourvoi principal dirigé contre l'ordonnance du 14 décembre 2012 ; que ce pourvoi, enregistré le 8 février 2013, est tardif et, par suite, irrecevable ; <br/>
<br/>
              4. Considérant que l'article 244 du code des marchés publics dispose que : <br/>
" I. - Une lettre de consultation est envoyée simultanément à tous les candidats sélectionnés. / Cette lettre de consultation comporte au moins : 1° Les documents de la consultation ou, s'ils ne sont pas détenus par la personne soumise à la présente partie, l'adresse du service auprès duquel les documents de la consultation peuvent être immédiatement obtenus sur demande et la date limite pour présenter cette demande, ou encore les conditions d'accès à ces documents s'ils sont mis à disposition directe par voie électronique (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte des dispositions précitées que le pouvoir adjudicateur doit adresser aux candidats les documents de la consultation nécessaires à l'élaboration de leurs offres ; que, toutefois, ces dispositions n'ont ni pour objet ni pour effet d'interdire au pouvoir adjudicateur d'inviter les candidats à venir consulter sur site des documents nécessaires à l'élaboration de leurs offres mais qui ne peuvent leur être adressés en raison, notamment, de leur volume ou de leur confidentialité ; qu'ainsi le juge des référés a commis une erreur de droit en annulant la procédure de passation du marché litigieux au motif que la SIMMAD avait méconnu les dispositions du 1° du I de l'article 244 citées ci-dessus en n'adressant pas aux candidats le plan d'entretien des hélicoptères " Puma ", nécessaire à l'élaboration de leurs offres, et en leur offrant seulement la possibilité de le consulter sur place, dans les locaux de l'équipe technique interarmées à Montauban, sans rechercher si, comme le soutenait le ministre en défense, les documents constituant le plan d'entretien de ces hélicoptères ne pouvaient être communiqués directement aux candidats en raison de leur volume et des règles de confidentialité qui les protégeaient ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé au titre des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              7. Considérant, en premier lieu, que la SIMMAD n'a pas manqué à ses obligations de publicité et de mise en concurrence en n'adressant pas aux candidats le plan d'entretien des hélicoptères " Puma " de l'armée française ; qu'en effet, il résulte de l'instruction que ce plan d'entretien est constitué d'un ensemble de documents d'un volume considérable faisant obstacle à ce qu'il soit adressé aux candidats ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'en ne prenant pas l'initiative de proposer à chaque candidat une date pour la consultation du plan d'entretien des hélicoptères, la SIMMAD n'a pas méconnu les dispositions de l'article 14 du règlement de la consultation, qui prévoyaient seulement que les candidats pourraient venir consulter la documentation technique " au sein de l'Equipe Technique Interarmées à Montauban au cours de la semaine 14 " de la consultation ou solliciter un rendez-vous pendant une autre période, possibilité dont a d'ailleurs fait usage la société requérante ; <br/>
<br/>
              9. Considérant, en troisième lieu, que la société Aeromécanic, dont les représentants sont demeurés moins de trois heures sur le site interarmées de Montauban lors de leur visite du 10 juillet 2012, alors qu'ils disposaient d'une journée entière, ne peuvent soutenir qu'une unique journée de consultation de la documentation technique aurait été insuffisante pour en prendre connaissance ; <br/>
<br/>
              10. Considérant, en quatrième lieu, que ni le règlement de la consultation, ni le cahier des clauses techniques particulières du marché, ne s'opposaient à ce que les candidats adoptent, pour élaborer leurs prix, une méthode consistant à ne pas facturer deux fois une même opération utile à la réalisation de deux prestations d'entretien distinctes ; que, par suite, l'offre de la société Sabena Tehnics DNR ne peut être regardée comme irrégulière au seul motif qu'elle aurait utilisé une telle méthode ; <br/>
<br/>
              11. Considérant, en cinquième lieu, que l'article 15 du règlement de la consultation prévoyait : " conformément au code des marchés publics, chaque soumissionnaire sera reçu selon les mêmes dispositions pour présenter son offre et proposer d'éventuels axes d'amélioration. A l'issue de ces rencontres, il sera demandé à chacun des soumissionnaires de présenter une dernière et meilleure offre (...) " ; que, contrairement à ce que soutient la société Aeromécanic, il n'en résulte pas que la SIMMAD aurait dû organiser, en plus de la réunion de négociation qui s'est tenue le 13 juin 2012, une seconde rencontre avec chaque candidat préalablement au dépôt des offres ; <br/>
<br/>
              12. Considérant, en sixième lieu, que la société Aeromécanic ne peut utilement soutenir que le ministre de la défense, qui a produit la méthode de notation pour le critère du prix dans un mémoire communiqué à la société Aeromécanic, aurait manqué à ses obligations de publicité et de mise en concurrence en refusant de lui communiquer cette méthode ;  <br/>
<br/>
              13. Considérant, enfin, que la personne publique est tenue, sur le fondement des dispositions de l'article 255 du code des marchés publics de communiquer, dans les cas prévus par ces dispositions, aux candidats évincés, à leur demande et postérieurement au choix de l'attributaire du marché, les " caractéristiques et avantages relatifs " de l'offre retenue ; que la SIMMAD a refusé, sans apporter aucune justification de nature, notamment, à établir une atteinte au secret industriel et commercial, de communiquer à la société Aeromécanic les délais d'exécution et le prix global de l'offre de la société Sabena Technics DNR, qui constituaient des éléments faisant l'objet de critères de sélection des offres ; qu'elle a, en l'espèce, manqué à ses obligations de publicité et de mise en concurrence ; qu'un tel manquement est susceptible de léser la société Aeromécanic ; qu'il y a dès lors lieu, d'une part, d'enjoindre à la SIMMAD de communiquer à la société Aeromécanic, dans un délai de quinze jours à compter de la notification de la présente décision, les délais d'exécution et le prix global de l'offre de la société Sabena Technics DNR, sauf à établir qu'une telle communication porterait atteinte au secret des affaires, et, d'autre part, de surseoir à statuer sur les conclusions de la société Aeromécanic tendant à l'annulation de la procédure ; <br/>
<br/>
<br/>
              14. Considérant que si une personne publique qui n'a pas eu recours au ministère d'avocat peut néanmoins demander au juge le bénéfice de l'article L. 761-1 du code de justice administrative au titre des frais spécifiques exposés par elle à l'occasion de l'instance, elle ne saurait se borner à faire état d'un surcroît de travail de ses services et doit faire état précisément des frais qu'elle aurait exposés pour défendre à l'instance ; qu'en l'espèce, les conclusions du ministre de la défense au titre de l'article L. 761-1, qui se fondent sur la quantité de travail consacré par ses services au présent litige ainsi que sur des frais de reprographie et de déplacement dont, d'ailleurs, il ne fait pas état avec précision, ne peuvent qu'être rejetées ; qu'il y a, en revanche, lieu de surseoir à statuer sur les conclusions présentées par la société Aeromécanic au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 14 décembre 2012 du juge des référés du tribunal administratif de Versailles est annulée.<br/>
Article 2 : Le pourvoi de la société Sabena Technics DNR est rejeté.<br/>
Article 3: Il est sursis à statuer sur la requête de la société Aeromécanic jusqu'à ce que le ministre de la défense lui ait communiqué, dans un délai de quinze jours à compter de la notification de la présente décision, le montant global et les délais d'exécution de l'offre de la société Sabena Technics DNR, sauf à justifier que la communication de ces éléments porterait atteinte au secret des affaires. <br/>
Article 4 : Les conclusions du ministre de la défense présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de la défense, à la société Aeromécanic et à la société Sabena Technics DNR.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-20 ARMÉES ET DÉFENSE. - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ (TROISIÈME PARTIE DU CODE DES MARCHÉS PUBLICS) - PROCÉDURE NÉGOCIÉE - OBLIGATION D'ADRESSER AUX CANDIDATS LES DOCUMENTS DE LA CONSULTATION NÉCESSAIRES À L'ÉLABORATION DE LEURS OFFRES (1° DU I DE L'ART. 244 DU MÊME CODE) - PORTÉE - INTERDICTION DE PROPOSER UNE CONSULTATION SUR PLACE DES DOCUMENTS CONFIDENTIELS OU VOLUMINEUX - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MARCHÉS DE DÉFENSE OU DE SÉCURITÉ (TROISIÈME PARTIE DU CODE DES MARCHÉS PUBLICS) - PROCÉDURE NÉGOCIÉE - OBLIGATION D'ADRESSER AUX CANDIDATS LES DOCUMENTS DE LA CONSULTATION NÉCESSAIRES À L'ÉLABORATION DE LEURS OFFRES (1° DU I DE L'ART. 244 DU MÊME CODE) - PORTÉE - INTERDICTION DE PROPOSER UNE CONSULTATION SUR PLACE DES DOCUMENTS CONFIDENTIELS OU VOLUMINEUX - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-02-05 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. MARCHÉ NÉGOCIÉ. - MARCHÉ DE DÉFENSE OU DE SÉCURITÉ (TROISIÈME PARTIE DU CODE DES MARCHÉS PUBLICS) - OBLIGATION D'ADRESSER AUX CANDIDATS LES DOCUMENTS DE LA CONSULTATION NÉCESSAIRES À L'ÉLABORATION DE LEURS OFFRES (1° DU I DE L'ART. 244 DU MÊME CODE) - PORTÉE - INTERDICTION DE PROPOSER UNE CONSULTATION SUR PLACE DES DOCUMENTS CONFIDENTIELS OU VOLUMINEUX - ABSENCE.
</SCT>
<ANA ID="9A"> 08-20 Les dispositions du 1° du I de l'article 244 du code des marchés publics, aux termes desquelles le pouvoir adjudicateur doit adresser aux candidats les documents de la consultation nécessaires à l'élaboration de leurs offres, n'ont ni pour objet ni pour effet d'interdire à celui-ci d'inviter les candidats à venir consulter sur site des documents nécessaires à l'élaboration de leurs offres mais qui ne peuvent leur être adressés en raison, notamment, de leur volume ou de leur confidentialité.</ANA>
<ANA ID="9B"> 39-02-005 Les dispositions du 1° du I de l'article 244 du code des marchés publics, aux termes desquelles le pouvoir adjudicateur doit adresser aux candidats les documents de la consultation nécessaires à l'élaboration de leurs offres, n'ont ni pour objet ni pour effet d'interdire à celui-ci d'inviter les candidats à venir consulter sur site des documents nécessaires à l'élaboration de leurs offres mais qui ne peuvent leur être adressés en raison, notamment, de leur volume ou de leur confidentialité.</ANA>
<ANA ID="9C"> 39-02-02-05 Les dispositions du 1° du I de l'article 244 du code des marchés publics, aux termes desquelles le pouvoir adjudicateur doit adresser aux candidats les documents de la consultation nécessaires à l'élaboration de leurs offres, n'ont ni pour objet ni pour effet d'interdire à celui-ci d'inviter les candidats à venir consulter sur site des documents nécessaires à l'élaboration de leurs offres mais qui ne peuvent leur être adressés en raison, notamment, de leur volume ou de leur confidentialité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
