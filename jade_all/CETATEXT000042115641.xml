<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115641</ID>
<ANCIEN_ID>JG_L_2020_07_000000430585</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115641.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 03/07/2020, 430585, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430585</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430585.20200703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Les associations " Les Jardins des Vaîtes " et " France Nature Environnement 25-90 ", Mme E... C..., Mme D... B... F... et M. A... B... ont demandé au juge des référés du tribunal administratif de Besançon d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du préfet du Doubs du 18 mars 2019 portant dérogation à l'interdiction de détruire, altérer, dégrader des sites de reproduction ou des aires de repos de spécimens d'espèces animales protégées et de capturer ou d'enlever des spécimens animales protégées dans le cadre du projet urbain du quartier durable des Vaîtes à Besançon. Par une ordonnance n° 1900636 du 6 mai 2019, le juge des référés a suspendu l'exécution de cet arrêté.<br/>
<br/>
              1°, Sous le n° 430585, par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 9 et 27 mai 2019 et 18 février et 22 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la Société Publique Locale (SPL) Territoire 25 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de suspension présentée par l'association " Les Jardins des Vaîtes " et autres ;<br/>
<br/>
              3°) de mettre à la charge de l'association " Les Jardins des Vaîtes " et autres la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2°, Sous le n° 432446, par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 9 juillet 2019, le ministre d'Etat, ministre de la transition écologique et solidaire, demande au Conseil d'Etat d'annuler la même ordonnance.<br/>
<br/>
              Il soutient que l'ordonnance du juge des référés du tribunal administratif de Besançon qu'il attaque est entachée :<br/>
              - d'une dénaturation des pièces du dossier en ce qu'elle retient comme sérieux le moyen tiré de ce que le projet litigieux ne répond pas à une raison impérative d'intérêt public majeur ;<br/>
              - d'une erreur de droit en ce qu'elle s'abstient de rechercher si la suspension des travaux n'est pas de nature à aggraver la situation des espèces protégées présentes sur le site.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Buk Lament - Robillot, avocat de la Société Publique Locale Territoire 25, et à la SCP Zribi et Texier, avocat de la société France nature environnement 25-90 et autre ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 27 mai et 11 juin 2020, présentées par les associations " France Nature Environnement 25-90 " et " Les Jardins des Vaîtes " ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, dans le cadre de l'aménagement du secteur dit des Vaîtes, d'une superficie d'une quarantaine d'hectares, la commune de Besançon a décidé la réalisation d'un " éco-quartier " sur un terrain d'environ 23 hectares. Par les arrêtés des 27 octobre 2011, 7 mars 2014, 20 mars 2014 et 6 juillet 2016, le préfet du Doubs a déclaré d'utilité publique le projet au profit de la commune de Besançon, désigné la société publique locale (SPL) Territoire 25 en qualité de concessionnaire de l'opération, déclaré cessibles les parcelles concernées au bénéfice de cette société et prorogé de cinq années supplémentaires les effets de la déclaration d'utilité publique. Puis, par un arrêté du 18 mars 2019, il a accordé à la SPL Territoire 25, sur le fondement du 4° du I de l'article L. 411-2 du code de l'environnement, une dérogation à l'interdiction de détruire, altérer, dégrader des sites de reproduction ou des aires de repos de spécimens d'espèces animales protégées et de capturer ou enlever de tels spécimens, pour la réalisation du projet. Par une ordonnance du 6 mai 2019, le juge des référés du tribunal administratif de Besançon, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu, à la demande des associations " Les Jardins des Vaîtes " et " France Nature Environnement 25-90 " et de trois particuliers, l'arrêté du 18 mars 2019. Par deux pourvois qu'il y a lieu de joindre pour statuer par une seule décision, la SPL Territoire 25 et le ministre de la transition écologique et solidaire demandent au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur les moyens dirigés contre l'ordonnance attaquée :<br/>
<br/>
              3. Le I de l'article L. 411-1 du code de l'environnement comporte un ensemble d'interdictions visant à assurer la conservation d'espèces animales ou végétales protégées et de leurs habitats. Sont ainsi interdits en vertu du 1° du I de cet article : " La destruction ou l'enlèvement des oeufs ou des nids, la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces ou, qu'ils soient vivants ou morts, leur transport, leur colportage, leur utilisation, leur détention, leur mise en vente, leur vente ou leur achat ". Sont interdits en vertu du 2° du I du même article : " La destruction, la coupe, la mutilation, l'arrachage, la cueillette ou l'enlèvement de végétaux de ces espèces, de leurs fructifications ou de toute autre forme prise par ces espèces au cours de leur cycle biologique, leur transport, leur colportage, leur utilisation, leur mise en vente, leur vente ou leur achat, la détention de spécimens prélevés dans le milieu naturel ". Sont interdits en vertu du 3° du I du même article : " La destruction, l'altération ou la dégradation de ces habitats naturels ou de ces habitats d'espèces ". Toutefois, le 4° du I de l'article L. 411-2 du même code permet à l'autorité administrative de délivrer des dérogations à ces interdictions dès lors que sont remplies trois conditions distinctes et cumulatives tenant à l'absence de solution alternative satisfaisante, à la condition de ne pas nuire " au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle " et, enfin, à la justification de la dérogation par l'un des cinq motifs qu'il énumère limitativement, dont celui énoncé au c) qui mentionne " l'intérêt de la santé et de la sécurité publiques ", " d'autres raisons impératives d'intérêt public majeur, y compris de nature sociale ou économique " et " les motifs qui comporteraient des conséquences bénéfiques primordiales pour l'environnement ".<br/>
<br/>
              4. Il résulte de ces dispositions qu'un projet de travaux, d'aménagement ou de construction d'une personne publique ou privée susceptible d'affecter la conservation d'espèces animales ou végétales protégées et de leurs habitats ne peut être autorisé, à titre dérogatoire, que s'il répond, par sa nature et compte tenu des intérêts économiques et sociaux en jeu, tels que notamment le projet urbain dans lequel il s'inscrit, à une raison impérative d'intérêt public majeur. En présence d'un tel intérêt, le projet ne peut cependant être autorisé, eu égard aux atteintes portées aux espèces protégées appréciées en tenant compte des mesures de réduction et de compensation prévues, que si, d'une part, il n'existe pas d'autre solution satisfaisante et, d'autre part, cette dérogation ne nuit pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés que la réalisation d'un " éco-quartier " dans le secteur des Vaîtes, secteur peu urbanisé et en déprise situé à proximité du centre-ville de Besançon et faisant l'objet d'une politique de rénovation ayant notamment conduit à l'installation de deux arrêts du tramway en 2014, est un projet conduit depuis de nombreuses années par la commune de Besançon et qui a été reconnu d'utilité publique. Il ressort également des pièces du dossier soumis au juge des référés que ce projet vise à répondre aux besoins en logement existant à Besançon tout en limitant l'étalement urbain par la construction de plus de mille logements favorisant la mixité sociale ainsi que d'infrastructures et d'équipements publics correspondants. Dans ces conditions, en estimant que les besoins en logement dans la commune de Besançon, à court et moyen terme, n'étaient pas tels qu'ils permettaient de considérer que ce projet répondait à une raison impérative d'intérêt public majeur, alors qu'il ressortait en outre des pièces du dossier qui lui était soumis que les projections démographiques associées à la politique de revitalisation du centre de l'agglomération faisaient apparaître un besoin de 10.000 logements d'ici 2030, besoin que les opérations de construction actuellement en cours ne couvrent que très partiellement et que le taux de vacance global des logements existants est dans la moyenne des villes comparables, le juge des référés a dénaturé les faits sur lesquels il a fait reposer son appréciation.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que la SPL Territoire 25 et le ministre de la transition écologique et solidaire sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension dirigée contre l'arrêté du 18 mars 2019 :<br/>
<br/>
              En ce qui concerne la recevabilité de l'intervention de la SPL Territoire 25 :<br/>
<br/>
              8. La SPL Territoire 25, bénéficiaire de l'autorisation litigieuse, justifie d'un intérêt suffisant au maintien de la décision attaquée. Ainsi, son intervention est recevable.<br/>
<br/>
              En ce qui concerne les fins de non-recevoir opposées en défense :<br/>
<br/>
              9. En premier lieu, l'association " Les Jardins des Vaîtes ", qui a été créée le 1er décembre 2018, a notamment pour objet, aux termes de ses statuts, de préserver les espaces naturels, forestiers et cultivés et la biodiversité dans la zone du quartier des Vaîtes de Besançon. Contrairement à ce qui est soutenu, il ne ressort pas des pièces du dossier que son objet social aurait été modifié postérieurement à l'édiction de l'arrêté dont elle demande la suspension. Elle a produit par ailleurs le procès-verbal des réunions de son conseil d'administration des 6 et 9 avril 2019 autorisant sa présidente à agir en justice à l'encontre de l'arrêté préfectoral du 18 mars 2019. Par suite, la fin de non-recevoir tirée de ce que cette association ne justifierait ni de son intérêt pour agir ni de la qualité pour agir de sa représentante doit être écartée.<br/>
<br/>
              10. En deuxième lieu, l'association Fédération Nature Environnement 25-90 a pour objet, aux termes de ses statuts, la protection de la nature et de l'environnement dans les départements du Doubs et du Territoire de Belfort, et notamment la conservation et la restauration des " espaces, ressources, milieux et habitats naturels, terrestres et aquatiques, les espèces animales et végétales, la diversité et les équilibres fondamentaux de la biosphère, l'eau, l'air, le sol, le sous-sol, les sites et paysages (...) ". Par ailleurs, conformément à l'article 6 de ses statuts, son président dispose de la compétence exclusive pour ester en justice, en lieu et place du bureau, sous réserve de l'en informer à sa prochaine réunion, lorsqu'un délai de procédure empêche une décision du bureau avant le terme de la prochaine réunion prévue. Par suite, la fin de non-recevoir tirée de ce que cette association ne justifierait ni de son intérêt pour agir ni de la qualité pour agir de son représentant doit être écartée. <br/>
<br/>
              11. En troisième lieu, en revanche, la seule circonstance que Mme C... et M. et Mme B... soient domiciliés sur des parcelles qui jouxtent le terrain d'assiette du projet litigieux et qu'ils disposent d'une vue directe sur ce dernier n'est pas suffisante, eu égard à l'objet de l'arrêté attaqué, pour leur donner intérêt pour agir à l'encontre de ce dernier. Par suite, la demande de suspension est irrecevable en tant qu'elle émane de ces trois personnes.<br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              12. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des éléments fournis par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. <br/>
<br/>
              13. Il résulte de l'instruction que la poursuite des travaux de réalisation du projet d'aménagement en cause est susceptible de causer aux espèces protégées présentes sur le site une atteinte irréversible. Ni la circonstance qu'une partie des travaux ait déjà été réalisée ni le coût que représente pour l'aménageur et la collectivité la prolongation de leur suspension ne sont de nature, dans les circonstances de l'espèce, à justifier que les travaux soient achevés rapidement. Par suite, la condition d'urgence est, en l'espèce, remplie.<br/>
<br/>
              En ce qui concerne l'existence, en l'état de l'instruction, d'un doute sérieux quant à la légalité de l'arrêté attaqué :<br/>
<br/>
              14. Il résulte des textes cités au point 3 que toute dérogation aux interdictions mentionnées aux 1°, 2° et 3° de l'article L. 411-1 du code de l'environnement précité doit répondre à trois conditions distinctes et cumulatives, dont l'absence de solution alternative satisfaisante. <br/>
<br/>
              15. S'il résulte, en l'état de l'instruction, que le moyen tiré de ce que le projet d'aménagement du quartier des Vaîtes ne répondrait pas à une raison impérative d'intérêt public majeur n'est pas propre à créer un doute sérieux sur la légalité de l'arrêté attaqué, en revanche, le moyen tiré de ce qu'il n'est pas établi, en l'état de l'instruction, eu égard aux éléments dont les requérants ont fait état devant le juge des référés du tribunal administratif de Besançon, que le besoin de logement identifié par la commune ne pouvait pas être satisfait par des solutions alternatives permettant de limiter l'atteinte portée aux espèces protégées, est de nature à faire naître un doute sérieux sur la légalité de l'arrêté attaqué. <br/>
<br/>
              16. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur les autres moyens de la demande, que les associations " Les Jardins des Vaîtes " et " France Nature Environnement 25-90 " sont fondées à demander la suspension de l'exécution de l'arrêté qu'elles attaquent.<br/>
<br/>
              17. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la SPL Territoire 25 et par l'association " Les Jardins des Vaîtes " et autres au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1900636 du 6 mai 2019 du juge des référés du tribunal administratif de Besançon est annulée.<br/>
Article 2 : L'intervention de la SPL Territoire 25 est admise.<br/>
Article 3 : La demande est irrecevable en tant qu'elle émane de Mme C... et de M. et Mme B....<br/>
Article 4 : L'exécution de l'arrêté du 18 mars 2019 est suspendue.<br/>
Article 5 : Les conclusions présentées par la SPL Territoire 25 et par l'association " Les Jardins des Vaîtes " et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la SPL Territoire 25, à la ministre de la transition écologique et solidaire et aux associations " France Nature Environnement 25-90 " et " Les Jardins des Vaîtes ". <br/>
Copie en sera adressée à Mme E... C..., à M. A... B..., et à Mme D... B... F....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
