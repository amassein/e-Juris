<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659302</ID>
<ANCIEN_ID>JG_L_2018_11_000000424135</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/93/CETATEXT000037659302.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 28/11/2018, 424135</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424135</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:424135.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...et Mme E...A...ont demandé au juge des référés du tribunal administratif de Nancy, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la décision de limitation des traitements actifs dispensés à M. D...A..., prise par le centre hospitalier régional universitaire (CHRU) de Nancy le 5 juillet 2018. Par une ordonnance n° 1802221 du 9 août 2018,  prise sur le fondement de l'article L. 522-3 du code de justice administrative, le juge des référés du tribunal administratif de Nancy a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 et 26 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... et Mme A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la décision du Conseil constitutionnel n° 2017-632 QPC du 2 juin 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de Mme C...B...et de Mme E...A...et à Me Le Prado, avocat du centre hospitalier régional universitaire de Nancy ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M. D...A...a été victime d'un accident de la circulation le 28 février 2018, qui a occasionné d'importantes lésions encéphaliques. Compte tenu de la gravité de son état qui excluait une prise en charge neurochirurgicale, M. A...a été transféré dans le service de réanimation chirurgicale polyvalente du centre hospitalier régional universitaire (CHRU) de Nancy. Le 19 mars 2018, au vu du pronostic neurologique défavorable du patient, le service de réanimation chirurgicale du CHRU de Nancy a informé la famille de M. A...qu'il était envisagé d'engager une procédure de limitation des traitements actifs en cas de détresse vitale. Une extubation a été réalisée le 13 avril 2018, suivie du transfert du patient dans le service de neurochirurgie de l'hôpital, le 19 avril suivant. La procédure collégiale prévue par les articles L. 1110-5-1 et R. 4127-37 du code de la santé publique a ensuite été mise en oeuvre, le 3 mai 2018. Dans ce cadre, l'hôpital a fait appel à un consultant extérieur, professeur au centre hospitalier universitaire de Strasbourg, qui a émis un avis aux termes duquel la réadmission en réanimation en cas de dégradation clinique de l'état du patient ne lui paraissait pas indiquée. L'équipe médicale a rencontré les membres de la famille du patient au cours du mois de juin 2018 pour leur rendre compte de la procédure en cours et leur indiquer que l'exécution de la décision de limitation des traitements actifs en cas de détresse vitale serait suspendue le temps nécessaire à l'exercice de leur droit au recours puis elle a décidé, le 5 juillet 2018, que M. A...ne serait pas transféré en unité de prise en charge en soins critiques en cas de détresse vitale afin d'éviter toute obstination déraisonnable. Mmes B... et A...ont saisi le juge des référés du tribunal administratif de Nancy, sur le fondement de l'article L. 521-2 du code de justice administrative, le 8 août 2018, afin que soit ordonnée la suspension de l'exécution de la décision du 5 juillet 2018. Par une ordonnance du 9 août 2018, prise sur le fondement de l'article L. 522-3 du même code, contre laquelle celles-ci se pourvoient en cassation, le juge des référés du tribunal administratif de Nancy a rejeté cette demande.<br/>
<br/>
              Sur l'office du juge des référés statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) ". <br/>
<br/>
              3. Il appartient au juge des référés d'exercer ses pouvoirs de manière particulière, lorsqu'il est saisi sur le fondement de l'article L. 521-2 du code de justice administrative d'une décision, prise par un médecin sur le fondement du code de la santé publique, et conduisant à interrompre ou à ne pas entreprendre un traitement au motif que ce dernier traduirait une obstination déraisonnable, dans la mesure où l'exécution de cette décision porterait de manière irréversible une atteinte à la vie. Il doit alors, le cas échéant en formation collégiale, prendre les mesures de sauvegarde nécessaires pour faire obstacle à son exécution lorsque cette décision pourrait ne pas relever des hypothèses prévues par la loi, en procédant à la conciliation des libertés fondamentales en cause, que sont le droit au respect de la vie et le droit du patient de consentir à un traitement médical et de ne pas subir un traitement qui serait le résultat d'une obstination déraisonnable.<br/>
<br/>
              Sur le cadre juridique applicable au litige :<br/>
<br/>
              4. Il résulte des dispositions des articles L. 1110-1, L. 1110-2, L. 1110-5, L. 1110-5-1, L. 1110-5-2 et L. 1111-4 du code de la santé publique, telles qu'interprétées par la décision du Conseil constitutionnel n° 2017-632 QPC du 2 juin 2017, que la décision du médecin d'arrêter ou de ne pas mettre en oeuvre un traitement traduisant une obstination déraisonnable, conduisant au décès d'un patient hors d'état d'exprimer sa volonté, doit être notifiée à la personne de confiance désignée par celui-ci ou, à défaut, à sa famille ou ses proches, dans des conditions leur permettant d'exercer un recours en temps utile, ce qui implique en particulier que le médecin ne peut mettre en oeuvre cette décision avant que les personnes concernées, qui pourraient vouloir saisir la juridiction compétente d'un recours, n'aient pu le faire ou obtenir une décision de sa part.<br/>
<br/>
              Sur le litige en référé :<br/>
<br/>
              5. Aux termes de l'article L. 522-3 du code de justice administrative : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge des référés que la décision du 5 juillet 2018 prévoit, sans limiter ses effets dans le temps, qu'il ne serait pas fait appel à l'équipe de réanimation en cas de détresse vitale présentée par M.A.... Dès lors, toutefois, que cette dernière est susceptible de se réaliser à tout moment, le juge des référés du tribunal administratif de Nancy a entaché son ordonnance de dénaturation en rejetant la demande dont il était saisi, sur le fondement de l'article L. 522-3 du code de justice administrative, au motif que la condition d'urgence posée par l'article L. 521-2 du même code n'était pas remplie à la date à laquelle il a statué. <br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mmes B...et A...sont fondées à demander l'annulation de l'ordonnance qu'elles attaquent.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Dès lors que la décision du 5 juillet 2018, dont l'exécution n'est pas subordonnée à l'absence d'évolution favorable de l'état de santé de M.A..., ne fixe, ainsi qu'il a été dit précédemment, aucune limite à son champ d'application dans le temps, il résulte de ce qui a été dit aux points 3, 4 et 6 de la présente décision que, faisant obstacle, en cas de survenance d'une détresse vitale de l'intéressé, à l'exercice d'un recours en temps utile à son encontre, elle est susceptible d'entraîner des conséquences de nature à porter une atteinte à une liberté fondamentale qu'il appartient au juge du référé liberté de faire cesser immédiatement. Il s'ensuit qu'il y a lieu d'ordonner la suspension de l'exécution de la décision du 5 juillet 2018.<br/>
<br/>
              10. Il incombe à l'autorité médicale de permettre, dans tous les cas, aux membres de la famille de M.A..., s'ils s'y croient fondés, de saisir en temps utile le juge des référés administratif sur le fondement de l'article L. 521-2 du code de justice administrative afin qu'il puisse procéder, au vu de la situation actuelle à la date de sa décision, à la conciliation du droit au respect de la vie et du droit du patient de ne pas subir un traitement qui serait le résultat d'une obstination déraisonnable. Dès lors, il appartient à l'autorité médicale de procéder, sans que soit requise la procédure collégiale prévue par les articles L. 1110-5-1 et R. 4127-37 du code de la santé publique, à un nouvel examen de l'état de santé du patient, et si, au terme de celui-ci, elle décide de ne pas entreprendre un traitement de réanimation de M.A..., en cas de détresse vitale de celui-ci, de subordonner l'exécution de cette nouvelle décision à l'absence d'évolution favorable de la situation et, en toute hypothèse, d'en limiter le champ d'application dans le temps en retenant une durée ne pouvant excéder trois mois. Le cas échéant, au terme de ce délai, cette décision pourrait être prolongée dans les mêmes conditions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nancy du 9 août 2018 est annulée. <br/>
Article 2 : L'exécution de la décision du 5 juillet 2018 du centre hospitalier régional universitaire de Nancy est suspendue.<br/>
Article 3 : La présente décision sera notifiée à Madame C...B..., à Madame E...A..., au centre hospitalier régional universitaire de Nancy et à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-05 SANTÉ PUBLIQUE. BIOÉTHIQUE. - POSSIBILITÉ POUR UN MÉDECIN DE PRENDRE, À L'ISSUE D'UNE PROCÉDURE COLLÉGIALE, UNE DÉCISION CONDUISANT, EN CAS D'ÉVENTUELLE DÉTRESSE VITALE, À NE PAS ENTREPRENDRE UN TRAITEMENT AU MOTIF QUE CE DERNIER TRADUIRAIT UNE OBSTINATION DÉRAISONNABLE - EXISTENCE, DANS LE RESPECT DU DROIT AU RECOURS DE LA FAMILLE ET DES PROCHES [RJ1] - CONDITIONS - A) DÉCISION DONT LE CHAMP D'APPLICATION TEMPOREL NE PEUT EXCÉDER TROIS MOIS - EXISTENCE - B) NÉCESSITÉ D'UN NOUVEL EXAMEN DE L'ÉTAT DE SANTÉ AVANT TOUTE NOUVELLE DÉCISION - EXISTENCE, SANS QUE SOIT REQUISE UNE NOUVELLE PROCÉDURE COLLÉGIALE - C) EXÉCUTION DE LA DÉCISION SUBORDONNÉE À L'ABSENCE D'ÉVOLUTION FAVORABLE DE LA SITUATION DU PATIENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 61-05 Patient dans un état grave, avec d'importantes lésions céphaliques, à la suite d'un accident de la circulation. Centre hospitalier informant la famille, à l'issue de la procédure collégiale prévue aux articles L. 1110-5-1 et R. 4127-37 du code de la santé publique (CSP), de ce qu'il est envisagé d'engager une procédure de limitation des traitements actifs en cas de détresse vitale.... ...Il incombe à l'autorité médicale de permettre, dans tous les cas, aux membres de la famille du patient, s'ils s'y croient fondés, de saisir en temps utile le juge des référés administratif sur le fondement de l'article L. 521-2 du code de justice administrative (CJA) afin qu'il puisse procéder, au vu de la situation actuelle à la date de sa décision, à la conciliation du droit au respect de la vie et du droit du patient de ne pas subir un traitement qui serait le résultat d'une obstination déraisonnable. Dès lors, il appartient à l'autorité médicale de procéder, sans que soit requise la procédure collégiale déjà évoquée, à un nouvel examen de l'état de santé du patient (b), et si, au terme de celui-ci, elle décide, à nouveau, de ne pas entreprendre un traitement de réanimation du patient, en cas de détresse vitale de celui-ci, de subordonner l'exécution de cette nouvelle décision à l'absence d'évolution favorable de la situation (c) et, en toute hypothèse, d'en limiter le champ d'application dans le temps en retenant une durée ne pouvant excéder trois mois (a). Le cas échéant, au terme de ce délai, cette décision pourrait être prolongée dans les mêmes conditions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. const., 2 juin 2017, n° 2017-632 QPC ; CE, 6 décembre 2017, Union nationale des associations de familles de traumatisés crâniens et de cérébro-lésés (UNAFTC), n° 403944, p. 351.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
