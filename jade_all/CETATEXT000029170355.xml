<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029170355</ID>
<ANCIEN_ID>JG_L_2014_06_000000373227</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/17/03/CETATEXT000029170355.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/06/2014, 373227, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373227</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:373227.20140630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 8 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, la SAS Mazeau Petrin Auvergnat, la SARL Aux fins gourmets Patrick Vialette, la SARL Aux Délices et l'EURL Au feu de bois demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social sur leur demande du 2 juillet 2013 tendant à l'abrogation de l'arrêté du préfet de la Haute-Loire du 6 mars 2012 relatif à la fermeture hebdomadaire des boulangeries et points de vente de pain de ce département ; <br/>
<br/>
              2°) d'enjoindre au ministre chargé du travail d'abroger l'arrêté du préfet de la Haute-Loire du 6 mars 2012.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 3132-29 du code du travail : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos (...) ". Pour l'application de ces dispositions, la fermeture au public des établissements d'une profession ne peut légalement être ordonnée sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé. Aux termes de l'article R. 3132-22 du même code : " Lorsqu'un arrêté préfectoral de fermeture au public, pris en application de l'article L. 3132-29, concerne des établissements concourant d'une façon directe à l'approvisionnement de la population en denrées alimentaires, il peut être abrogé ou modifié par le ministre chargé du travail après consultation des organisations professionnelles intéressées. / Cette décision ne peut intervenir qu'après l'expiration d'un délai de six mois à compter de la mise en application de l'arrêté préfectoral ".<br/>
<br/>
              2. A la suite de l'accord intervenu le 16 janvier 2012 entre certains syndicats d'employeurs et de travailleurs concernés, le préfet de la Haute-Loire a, par arrêté du 6 mars 2012, prescrit la fermeture un jour par semaine des établissements, parties d'établissements, dépôts, fabricants artisanaux ou industriels, fixes ou ambulants dans lesquels s'effectue, à titre principal ou accessoire, la vente au détail ou la distribution de pain dans le département de la Haute-Loire. Les sociétés requérantes demandent l'annulation pour excès de pouvoir de la décision par laquelle le ministre a rejeté leur demande du 2 juillet 2013 tendant à l'abrogation de l'arrêté du préfet de la Haute-Loire du 6 mars 2012.<br/>
<br/>
              3. L'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date.<br/>
<br/>
              Sur l'existence d'un accord entre organisations syndicales de salariés et organisations d'employeurs : <br/>
<br/>
              4. Si, en vertu des dispositions précitées de l'article L. 3132-29 du code du travail, le préfet doit vérifier l'existence d'un accord entre une ou plusieurs organisations d'employeurs et une ou plusieurs organisations de salariés, résultant d'échanges et de discussions menées simultanément et collectivement entre les parties, les conditions de négociation de cet accord, lequel ne doit d'ailleurs pas nécessairement prendre la forme d'un document écrit et signé dans les conditions prévues au titre III du livre II de la deuxième partie du code du travail, sont par elles-mêmes sans incidence sur la légalité de l'arrêté. En l'espèce, il ressort des pièces du dossier que l'accord du 16 janvier 2012 ne procède pas de simples avis recueillis auprès des différentes organisations mais a été précédé de deux réunions organisées les 14 décembre 2011 et 16 janvier 2012 auxquelles l'ensemble des organisations intéressées ont été conviées, la fédération des entreprises de boulangeries et pâtisseries françaises et la fédération des industries de boulangerie et de pâtisserie ayant d'ailleurs participé à la première de ces réunions. Par suite, les sociétés requérantes ne sont pas fondées à soutenir que l'accord n'aurait pas été précédé de négociations et que les syndicats représentant la boulangerie industrielle et les terminaux de cuisson n'auraient pas été associés.<br/>
<br/>
              Sur l'existence d'une majorité favorable à la fermeture hebdomadaire :<br/>
<br/>
              5. En premier lieu, il ressort des pièces du dossier que l'accord du 16 janvier 2012 a été signé, pour les employeurs, par la fédération de la boulangerie et boulangerie-pâtisserie de la Haute-Loire, par la fédération nationale de l'épicerie, par le syndicat des indépendants, par la confédération nationale de la boulangerie-pâtisserie française et par la fédération nationale des détaillants en produits laitiers. Si les sociétés requérantes font valoir qu'un précédent accord, conclu le 5 mars 1998, a été jugé illégal par la cour d'appel de Riom le 26 mai 2011 au motif qu'il ne correspondait pas à la volonté de la majorité indiscutable de tous ceux qui exerçaient la profession et que la chambre syndicale de la boulangerie de la Haute-Loire ne peut prétendre représenter des boulangeries qui ne sont pas adhérentes, il ressort des pièces du dossier, d'une part, que l'accord du 5 mars 1998 avait été signé uniquement, pour les employeurs, par le syndicat départemental des maîtres artisans boulangers et boulangers pâtissiers de la Haute-Loire et que, d'autre part, la chambre syndicale de la boulangerie de la Haute-Loire n'a pas été partie à l'accord du 16 janvier 2012. Eu égard, notamment, au nombre de boulangeries et boulangeries-pâtisseries représentées par la fédération de la boulangerie et boulangerie-pâtisserie de la Haute-Loire et de commerces ayant une activité accessoire de vente de pain représentés par la fédération nationale de l'épicerie, les sociétés requérantes ne sont pas fondées à soutenir que cet accord ne correspondrait pas pour la profession, à la date de signature de l'arrêté du 6 mars 2012, à la volonté de la majorité indiscutable de tous ceux qui exercent la profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé.<br/>
<br/>
              6. En second lieu, il ne ressort pas des pièces du dossier que se serait produit, entre janvier 2012 et septembre 2013, dans l'opinion d'un nombre important des commerçants intéressés, un changement susceptible de modifier la volonté de la majorité d'entre eux, imposant au ministre de procéder à une nouvelle consultation des organisations professionnelles intéressées pour s'assurer que l'arrêté litigieux correspondait encore à la volonté de la majorité indiscutable des établissements proposant du pain à la vente dans le département à la date à laquelle est intervenue sa décision de refus d'abrogation.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la SAS Mazeau Petrin Auvergnat, la SARL Aux fins gourmets Patrick Vialette, la SARL Aux Délices et l'EURL Au feu de bois ne sont pas fondées à demander l'annulation de la décision par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté leur demande tendant à l'abrogation de l'arrêté du préfet de la Haute-Loire du 6 mars 2012. Par suite, leurs conclusions tendant à ce qu'il soit enjoint au ministre de procéder à cette abrogation ne peuvent qu'être également rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SAS Mazeau Petrin Auvergnat, de la SARL Aux fins gourmets Patrick Vialette, de la SARL Aux Délices et de l'EURL Au feu de bois est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la SAS Mazeau Petrin Auvergnat, à la SARL Aux fins gourmets Patrick Vialette, à la SARL Aux Délices, à l'EURL Au feu de bois et au ministre du travail, de l'emploi et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
