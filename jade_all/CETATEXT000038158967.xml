<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038158967</ID>
<ANCIEN_ID>J2_L_2019_02_000001801834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/89/CETATEXT000038158967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de LYON, 5ème chambre B - formation à 3, 21/02/2019, 18LY01834, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-21</DATE_DEC>
<JURIDICTION>CAA de LYON</JURIDICTION>
<NUMERO>18LY01834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre B - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. CLOT</PRESIDENT>
<AVOCATS>MBOTO Y'EKOKO NGOY</AVOCATS>
<RAPPORTEUR>M. Jean-Pierre  CLOT</RAPPORTEUR>
<COMMISSAIRE_GVT>M. LAVAL</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
       Mme C... D... a demandé au tribunal administratif de Lyon d'annuler les arrêtés du 5 septembre 2017 par lesquels le préfet de la Loire a décidé son transfert aux autorités italiennes pour l'examen de sa demande d'asile et l'a assignée à résidence dans le département de la Loire. <br/>
<br/>
       Par un premier jugement n° 1706609 du 8 septembre 2017, le magistrat désigné par le président du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
       Par un arrêt n° 17LY04136 du 6 mars 2018, la cour a annulé ce jugement du 8 septembre 2017 et renvoyé Mme D... devant le tribunal administratif.<br/>
<br/>
       Par un second jugement n° 1801635 du 3 avril 2018, le magistrat désigné par le président du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
Procédure devant la cour <br/>
<br/>
       Par une requête enregistrée le 18 mai 2018, Mme D..., représentée par Me B... A...'ekoko Ngoy, avocat, demande à la cour :<br/>
       1°) d'annuler ce jugement du magistrat désigné par le président du tribunal administratif de Lyon du 3 avril 2018 ; <br/>
       2°) d'annuler pour excès de pouvoir les décisions susmentionnées ; <br/>
<br/>
       3°) d'enjoindre au préfet d'enregistrer sa demande d'asile et de l'autoriser à séjourner en France, dans le délai de quinze jours à compter de la notification de l'arrêt à intervenir, sous astreinte de 100 euros par jour de retard ; <br/>
       4°) de mettre à la charge de l'État le paiement à son conseil d'une somme de 2 500 euros en application de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi n° 91-647 du 10 juillet 1991.<br/>
<br/>
       Elle soutient que : <br/>
       - le jugement attaqué est irrégulier dès lors qu'elle n'a pas été régulièrement convoquée à l'audience ;<br/>
       - la décision de transfert en litige a été signée par une autorité incompétente ; elle est insuffisamment motivée ; les brochures A et B remises, écrites en petits caractères, sont illisibles et méconnaissent l'obligation d'information intelligible prévue à l'article 4 du règlement n° 604/2013 du 26 juin 2013 ; elle méconnaît l'article 3.2 et l'article 17 de ce règlement ; elle méconnait son droit au respect de sa vie privée et familiale au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; elle méconnaît l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - la mesure d'assignation à résidence a été signée par une autorité incompétente ; elle est insuffisamment motivée ; elle est intervenue en méconnaissance des droits de la défense ; elle méconnaît la liberté d'aller et venir garantie par l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 66 de la Constitution du 4 octobre 1958.<br/>
<br/>
       Par un mémoire enregistré le 23 janvier 2019, le préfet de la Loire conclut au rejet de la requête.<br/>
<br/>
       Il déclare s'en rapporter à ses écritures devant le tribunal administratif.<br/>
<br/>
       Mme D... a été admise au bénéfice de l'aide juridictionnelle totale par une décision du 24 avril 2018.<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - la Charte des droits fondamentaux de l'Union européenne ;<br/>
       - le règlement (CE) no 1560/2003 de la Commission du 2 septembre 2003 modifié par le règlement (UE) n° 118/2014 de la Commission du 30 janvier 2014 ;<br/>
       - le règlement (UE) n° 603/2013 du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
       - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - la loi n° 91-647 du 10 juillet 1991 ;<br/>
       - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
       Le président de la formation de jugement ayant dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique le rapport de M. Clot, président ;<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
       1. Mme D..., ressortissante de la République démocratique du Congo née le 23 mars 1988, a déclaré être entrée sur le territoire français le 12 janvier 2017. Elle a déposé une demande d'asile à la préfecture du Rhône le 12 février 2017. Le fichier Visabio ayant révélé que les autorités italiennes lui avaient délivré un visa le 24 août 2016, valable du 1er septembre au 15 octobre 2016, ces autorités ont été saisies d'une demande visant à sa prise en charge, qu'elles ont implicitement acceptée. Le 5 septembre 2017, le préfet de la Loire a ordonné sa remise aux autorités italiennes et l'a assignée à résidence dans le département de la Loire. Par un premier jugement, du 3 avril 2018, le magistrat désigné par le président du tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de ces décisions. Par un arrêt du 6 mars 2018, la cour a annulé ce jugement, qui était irrégulier, et renvoyé l'intéressée devant le tribunal administratif. Mme D... relève appel du nouveau jugement, du 3 avril 2018, par lequel le magistrat désigné par le président du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
       Sur la régularité du jugement attaqué :<br/>
<br/>
       2. Aux termes de l'article R. 777-3 du code de justice administrative : " Sont présentés, instruits et jugés selon les dispositions des articles L. 742-4 à L. 742-6 du code de l'entrée et du séjour des étrangers et du droit d'asile et celles du présent code, sous réserve des dispositions du présent chapitre, les recours en annulation formés contre les décisions de transfert mentionnées à l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile et, le cas échéant, contre les décisions d'assignation à résidence prises en application de l'article L. 561-2 de ce code au titre de ces décisions de transfert. " <br/>
<br/>
       3. Aux termes du II de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif à la procédure contentieuse applicable aux décisions de transfert mentionnées à l'article L. 742-3 du même code : " Lorsqu'une décision de placement en rétention prise en application de l'article L. 551-1 ou d'assignation à résidence prise en application de l'article L. 561-2 est notifiée avec la décision de transfert, l'étranger peut, dans les quarante-huit heures suivant leur notification, demander au président du tribunal administratif l'annulation de la décision de transfert et de la décision d'assignation à résidence. / Il est statué sur ce recours selon la procédure et dans le délai prévus au III de l'article L. 512-1. (...) ". Le III de ce dernier article prévoit que : " (...) L'audience est publique. Elle se déroule sans conclusions du rapporteur public, en présence de l'intéressé, sauf si celui-ci, dûment convoqué, ne se présente pas. L'étranger est assisté de son conseil s'il en a un. (...) ".<br/>
<br/>
       4. Il résulte des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que, lorsqu'il est fait application de cette procédure, par dérogation à l'article R. 431-1 du code de justice administrative, les dispositions spéciales de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile imposent une convocation personnelle à l'audience du requérant, même assisté d'un avocat, dans les litiges relatifs aux arrêtés de transfert portés devant les tribunaux administratif. Dès lors, l'étranger doit, même s'il est assisté d'un avocat, être personnellement convoqué à l'audience devant le président du tribunal administratif ou le magistrat qu'il désigne.<br/>
<br/>
       5. Le jugement attaqué mentionne que " les parties ont été régulièrement averties du jour de l'audience ", à laquelle elles n'étaient ni présentes ni représentées. Cette mention, qui fait foi jusqu'à preuve contraire, est toutefois contredite par les pièces du dossier de première instance, qui ne comportent pas copie de la convocation de Mme D... à cette audience, malgré la demande adressée à cet effet au greffe du tribunal administratif. Le dossier de première instance ne comporte aucune autre pièce établissant l'existence d'une convocation écrite ou orale à l'audience de l'intéressée. Ainsi, Mme D... doit être regardée comme n'ayant pas été personnellement convoquée à l'audience devant le tribunal administratif, en méconnaissance des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Elle est, par suite, fondée à soutenir que le jugement attaqué a été rendu à l'issue d'une procédure irrégulière et à en demander, pour ce motif, l'annulation.<br/>
<br/>
       6. Il y a lieu d'évoquer et de statuer immédiatement sur la demande de Mme D... devant le tribunal administratif.<br/>
<br/>
       Sur la légalité de la décision de transfert :<br/>
<br/>
       7. Il ressort des pièces produites par le préfet devant le tribunal administratif que M. Gérard Lacroix, secrétaire général de la préfecture de la Loire, était compétent pour signer la décision en litige.<br/>
<br/>
       S'agissant de la motivation :<br/>
<br/>
       8. Aux termes de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve du second alinéa de l'article L. 742-1, l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre État peut faire l'objet d'un transfert vers l'État responsable de cet examen. / Toute décision de transfert fait l'objet d'une décision écrite motivée prise par l'autorité administrative. (...) ".<br/>
<br/>
       9. En application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, la décision de transfert dont fait l'objet un ressortissant de pays tiers ou un apatride qui a déposé auprès des autorités françaises une demande d'asile dont l'examen relève d'un autre État membre ayant accepté de le prendre ou de le reprendre en charge doit être motivée, c'est-à-dire qu'elle doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement.<br/>
<br/>
       10. Pour l'application de ces dispositions, est suffisamment motivée une décision de transfert qui mentionne le règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et comprend l'indication des éléments de fait sur lesquels l'autorité administrative se fonde pour estimer que l'examen de la demande présentée devant elle relève de la responsabilité d'un autre État membre, une telle motivation permettant d'identifier le critère du règlement communautaire dont il est fait application. <br/>
<br/>
       11. L'arrêté litigieux vise le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et indique que les autorités italiennes ont délivré à l'intéressée un visa dont la validité était expirée depuis moins de six mois à la date de sa demande d'asile, de sorte que celles-ci sont compétentes en application du 4 de l'article 12 dudit règlement. Ainsi, cet arrêté est suffisamment motivé.<br/>
<br/>
       S'agissant de l'application de l'article 4 du règlement (UE) n° 604-2013 du 26 juin 2013 :<br/>
<br/>
       12. Aux termes de l'article 4 du règlement (UE) n° 604-2013 du 26 juin 2013 : " 1. Dès qu'une demande de protection internationale est introduite au sens de l'article 20, paragraphe 2, dans un État membre, ses autorités compétentes informent le demandeur de l'application du présent règlement et notamment : / a) des objectifs du présent règlement (...) / b) des critères de détermination de l'État membre responsable, de la hiérarchie de ces critères au cours des différentes étapes de la procédure et de leur durée (...) / 2. Les informations visées au paragraphe 1 sont données par écrit, dans une langue que le demandeur comprend ou dont on peut raisonnablement supposer qu'il la comprend. Les Etats membres utilisent la brochure commune rédigée à cet effet en vertu du paragraphe 3. / Si c'est nécessaire à la bonne compréhension du demandeur, les informations lui sont également communiquées oralement, par exemple lors de l'entretien individuel visé à l'article 5 (...) ". Il résulte de ces dispositions que le demandeur d'asile auquel l'administration entend faire application du règlement du 26 juin 2013 doit se voir remettre, dès le moment où le préfet est informé de ce qu'il est susceptible d'entrer dans le champ d'application de ce règlement, et en tout état de cause en temps utile, une information complète sur ses droits, par écrit et dans une langue qu'il comprend.<br/>
<br/>
       13. Il ressort des pièces produites par le préfet de la Loire en première instance que Mme D..., qui a bénéficié d'un entretien individuel le 13 février 2017, jour de l'enregistrement de sa demande d'asile à la préfecture du Rhône, a signé le compte rendu de cet entretien accompagné de son résumé en certifiant qu'il lui a été remis le jour même et il est précisé sur ce document que l'entretien a été réalisé en français, " langue comprise ". La requérante a, en outre, attesté avoir reçu à la même date le guide d'accueil du demandeur d'asile, ainsi que les brochures d'information " J'ai demandé l'asile dans l'Union européenne - quel pays sera responsable de l'analyse de ma demande ' " et " Je suis sous procédure Dublin - Qu'est-ce que cela signifie ' ", qui constituent la brochure commune prévue par les dispositions de l'article 4 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, dit " Dublin III " et figurant en annexe au règlement (UE) du 30 janvier 2014. Dans ces conditions et en dépit d'une écriture des brochures en petits caractères, Mme D... a reçu toutes les informations requises lui permettant de faire valoir ses observations avant que ne soit prise la décision contestée. Dès lors, l'article 4 du règlement (UE) n° 604/2013 n'a pas été méconnu.<br/>
<br/>
       S'agissant du 2 de l'article 3 du règlement (UE) n° 604/2013 du 26 juin 2013 :<br/>
<br/>
       14. Aux termes du 2 de l'article 3 du règlement (UE) du 26 juin 2013 : " (...) Lorsqu'il est impossible de transférer un demandeur vers l'État membre initialement désigné comme responsable parce qu'il y a de sérieuses raisons de croire qu'il existe dans cet État membre des défaillances systémiques dans la procédure d'asile et les conditions d'accueil des demandeurs, qui entraînent un risque de traitement inhumain ou dégradant au sens de l'article 4 de la charte des droits fondamentaux de l'Union européenne, l'État membre procédant à la détermination de l'État membre responsable poursuit l'examen des critères énoncés au chapitre III afin d'établir si un autre État membre peut être désigné comme responsable. / Lorsqu'il est impossible de transférer le demandeur en vertu du présent paragraphe vers un État membre désigné sur la base des critères énoncés au chapitre III ou vers le premier État membre auprès duquel la demande a été introduite, l'État membre procédant à la détermination de l'État membre responsable devient l'État membre responsable (...) ".<br/>
<br/>
       15. Il ne ressort pas des pièces du dossier que les autorités italiennes, qui ont donné leur accord à la demande de prise en charge adressée par les autorités françaises, ne sont pas en mesure de traiter la demande d'asile de Mme D... dans des conditions conformes à l'ensemble des garanties exigées par le respect du droit d'asile et que Mme D... courrait en Italie un risque réel d'être soumise à des traitements inhumains ou dégradants, au sens de l'article 4 de la Charte des droits fondamentaux de l'Union européenne, en l'absence d'existence avérée de défaillances systémiques dans la procédure d'asile et les conditions d'accueil des demandeurs d'asile dans ce pays, au demeurant État-membre de l'Union européenne et partie tant à la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, complétée par le protocole de New York, qu'à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. La décision de transfert contestée ne méconnaît donc pas les dispositions précitées du 2. de l'article 3 du règlement (UE) du 26 juin 2013.<br/>
<br/>
       S'agissant de l'article 17 du règlement (UE) n° 604/2013 du 26 juin 2013 :<br/>
<br/>
       16. Aux termes de l'article 17 du règlement (UE) n° 604/2013 : " 1. Par dérogation à l'article 3, paragraphe 1, chaque État membre peut décider d'examiner une demande de protection internationale qui lui est présentée par un ressortissant de pays tiers ou un apatride, même si cet examen ne lui incombe pas en vertu des critères fixés dans le présent règlement. (...) ".<br/>
<br/>
       17. Si l'Italie connaît un afflux de migrants, le préfet n'a pas commis d'erreur manifeste d'appréciation en ne faisant pas usage, en l'espèce, de la clause discrétionnaire prévue au 1 de l'article 17 du règlement (UE) n° 604/2013 du 26 juin 2013 qui permet à un État d'examiner la demande d'asile d'un demandeur même si cet examen ne lui incombe pas en application des critères fixés dans ce règlement. <br/>
<br/>
       S'agissant de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : <br/>
<br/>
       18. Aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. "<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       19. Compte tenu notamment de la date de son entrée sur le territoire, Mme D... ne saurait se prévaloir d'une atteinte à sa vie privée et familiale à laquelle la décision qu'elle conteste aurait porté atteinte. <br/>
<br/>
       S'agissant de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : <br/>
<br/>
       20. Il ne ressort pas des pièces du dossier que le transfert de Mme D... vers l'Italie pourrait entraîner son renvoi en République démocratique du Congo, où elle allègue encourir des risques de subir des traitements prohibés par l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Dès lors, le moyen tiré de la méconnaissance de ces stipulations doit être écarté.<br/>
<br/>
       Sur la légalité de la décision d'assignation à résidence :<br/>
<br/>
       21. Aux termes de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " I.-L'autorité administrative peut prendre une décision d'assignation à résidence à l'égard de l'étranger qui ne peut quitter immédiatement le territoire français mais dont l'éloignement demeure une perspective raisonnable, lorsque cet étranger : (...) 1° bis Fait l'objet d'une décision de transfert en application de l'article L. 742-3 ou d'une requête aux fins de prise en charge ou de reprise en charge en application du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride (...). Les trois derniers alinéas de l'article L. 561-1 sont applicables, sous réserve que la durée maximale de l'assignation ne puisse excéder une durée de quarante-cinq jours, renouvelable une fois. (...) ". <br/>
       22. Aux termes de l'article L. 561-1 du même code : " (...) La décision d'assignation à résidence est motivée. (...) L'étranger astreint à résider dans les lieux qui lui sont fixés par l'autorité administrative doit se présenter périodiquement aux services de police ou aux unités de gendarmerie. Il doit également se présenter, lorsque l'autorité administrative le lui demande, aux autorités consulaires, en vue de la délivrance d'un document de voyage. (...) ".<br/>
<br/>
       23. Il ressort des pièces produites par le préfet devant le tribunal administratif que M. Gérard Lacroix, secrétaire général de la préfecture de la Loire, était compétent pour signer la décision en litige.<br/>
<br/>
       24. La décision en litige, qui comporte l'énoncé des éléments de droit et de fait qui en constituent le fondement, est suffisamment motivée.<br/>
<br/>
       25. En se bornant à soutenir que la décision contestée méconnaît les droits de la défense, la requérante qui, comme il a été dit, a bénéficié d'un entretien à la préfecture, ne fait état d'aucun élément pertinent qu'elle aurait été empêchée de faire valoir auprès de l'autorité administrative avant que n'intervienne la mesure d'assignation à résidence qu'elle conteste. Dès lors, ce moyen doit être écarté.<br/>
<br/>
<br/>
       26. Il ne ressort pas des pièces du dossier que cette mesure ne serait, en l'espèce, ni nécessaire, ni proportionnée. Dès lors, le moyen tiré de l'atteinte portée à la liberté d'aller et venir garantie par l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 66 de la Constitution du 4 octobre 1958 doit être écarté.<br/>
<br/>
       27. Il résulte de ce qui précède que Mme D... n'est pas fondée à demander l'annulation des décisions qu'elle conteste. Doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction et celles de son conseil tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
DÉCIDE :<br/>
Article 1er : Le jugement du magistrat désigné par le président du tribunal administratif de Lyon du 3 avril 2018 est annulé.<br/>
Article 2 : Le surplus des conclusions de Mme D... est rejeté.<br/>
Article 3 : Le présent arrêt sera notifié à Mme C... D... et au ministre de l'intérieur. Copie en sera adressée au préfet de la Loire.<br/>
Délibéré après l'audience du 31 janvier 2019 à laquelle siégeaient :<br/>
M. Clot, président de chambre,<br/>
Mme Dèche, premier conseiller,<br/>
M. Savouré, premier conseiller.<br/>
Lu en audience publique, le 21 février 2019.<br/>
8<br/>
N° 18LY01834<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
