<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983373</ID>
<ANCIEN_ID>JG_L_2015_07_000000375752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983373.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 31/07/2015, 375752, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375752.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'annuler la décision du 21 mars 2012 du président de l'Université Paris V - René Descartes et les décisions implicites de la directrice de l'Assistance Publique-Hôpitaux de Paris, du ministre de l'enseignement supérieur et de la recherche et du ministre du travail, de l'emploi et de la santé refusant de lui communiquer son dossier administratif de fonctionnaire, avec ses documents annexes, son dossier médical, ainsi que les appréciations et avis portés sur sa carrière et ses activités par ses pairs en application de son statut.<br/>
<br/>
              Par un jugement n° 1207909/6-1 du 20 décembre 2013, le tribunal administratif de Paris a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 24 février 2014 et 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1207909/6-1 du 20 décembre 2013 du tribunal administratif de Paris; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de Mme A...et à la SCP Delaporte, Briard, Trichet, avocat de l'Université paris V ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une lettre du 8 décembre 2011 adressée au président de l'Université Paris V - René Descartes avec copie à la directrice de l'Assistance Publique - Hôpitaux de Paris (AP-HP), au ministre de l'enseignement supérieur de la recherche et au ministre du travail, de l'emploi et de la santé, MmeA..., maître de conférences des universités-praticien hospitalier, a demandé la communication de son dossier administratif, de son dossier médical, ainsi que des appréciations portées par ses pairs sur sa carrière et ses activités universitaires et hospitalières. Estimant ne pas avoir obtenu de réponse, elle a saisi la commission d'accès aux documents administratifs (CADA) qui a rendu un avis le 9 février 2012. Par un jugement du 20 décembre 2013, contre lequel Mme A...se pourvoit en cassation, le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation des décisions explicites ou implicites qu'elle estime avoir été apportées à sa demande ; <br/>
<br/>
              2. L'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal dispose que sont considérés comme documents administratifs les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Aux termes de l'article 2 de cette loi : " (...) Lorsqu'une administration mentionnée à l'article 1er est saisie d'une demande de communication portant sur un document administratif qu'elle ne détient pas mais qui est détenu par une autre administration mentionnée au même article, elle la transmet à cette dernière et en avise l'intéressé (...) ". Il résulte de ces dispositions que, lorsqu'un établissement public de l'Etat chargé d'une mission de service public est saisi d'une demande de communication portant sur un document administratif qu'il ne détient pas et qu'il estime être détenu par une personne de droit privé chargée d'une mission de service public, il est tenu de la transmettre à cette dernière et d'en aviser l'intéressé ,<br/>
<br/>
              3. L'article 17 du décret du 30 décembre 2005 relatif à la liberté d'accès aux documents administratifs prévoit que le silence gardé pendant plus d'un mois par l'autorité compétente, saisie d'une demande de communication de documents en application de l'article 2 de la loi du 17 juillet 1978 cité au point 2, vaut décision de refus et que l'intéressé dispose d'un délai de deux mois à compter de la notification du refus ou de l'expiration du délai fixé au premier alinéa pour saisir la CADA. Aux termes de l'article 19 de ce décret : " (...) Le silence gardé par l'autorité mise en cause pendant plus de deux mois à compter de l'enregistrement de la demande de l'intéressé par la commission vaut confirmation de la décision de refus ". Il résulte de ces dispositions qu'à l'issue des délais qu'elles fixent, dont le premier court à compter de la date de sa réception par l'administration initialement saisie, la demande est réputée avoir été implicitement rejetée par l'administration qui détient le document en cause, que cette demande lui ait été ou non transmise. L'intéressé dispose alors d'un délai de deux mois pour demander l'annulation de cette décision devant le juge de l'excès de pouvoir, l'absence de saisine préalable de la CADA ne pouvant plus lui être opposée ;<br/>
<br/>
              4. Pour soulever d'office l'irrecevabilité de la requête de MmeA..., en tant qu'elle tendait à l'annulation des décisions implicites de rejet de la directrice de l'AP-HP, du ministre de l'enseignement supérieur et de la recherche et du ministre du travail, de l'emploi et de la santé, le tribunal administratif de Paris s'est fondé sur l'absence de décision administrative préalable de ces trois autorités qui n'auraient été saisies d'aucune demande, alors qu'il résulte des pièces du dossier qui lui était soumis que la demande de MmeA..., dont ils avaient reçu copie, leur avait été transmise le 27 février 2012 par le président de l'Université Paris V - René Descartes avec l'avis de la CADA qui, d'ailleurs, s'était prononcée sur le caractère communicable de l'ensemble des documents. Il a ainsi entaché son jugement d'une erreur de droit. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé dans cette mesure ;<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Assistance publique-Hôpitaux de Paris et de l'Etat, à parts égales, la somme de 2 400 euros à verser à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 20 décembre 2013 du tribunal administratif de Paris est annulé en tant qu'il rejette la demande d'annulation des décisions implicites de refus de la directrice de l'Assistance publique - Hôpitaux de Paris, du ministre de l'enseignement supérieur et de la recherche, et du ministre du travail, de l'emploi et de la santé.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure devant le tribunal administratif de Paris.<br/>
Article 3 : L'Assistance publique - Hôpitaux de Paris et l'Etat verseront, à parts égales, la somme de 2 400 euros à Mme A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'Université Paris V - René Descartes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A..., au président de l'Université Paris V - René Descartes, au directeur général de l'Assistance publique-Hôpitaux de Paris, à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et au ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
