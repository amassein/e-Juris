<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034293471</ID>
<ANCIEN_ID>JG_L_2017_03_000000404008</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/29/34/CETATEXT000034293471.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/03/2017, 404008, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404008</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404008.20170327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Bordeaux d'annuler la décision du 6 décembre 2013 du ministre de l'égalité des territoires et du logement et du ministre de l'écologie, du développement durable et de l'énergie rejetant sa demande tendant à l'attribution de l'indemnité temporaire de mobilité, et de condamner l'Etat à lui verser la somme de 10 000 euros au titre de cette indemnité. Par une ordonnance n° 1401938 du 16 octobre 2015, le président de la 4ème chambre du tribunal administratif de Bordeaux a condamné l'Etat à verser la somme de 6 000 euros à M. A...au titre de l'indemnité temporaire de mobilité. <br/>
<br/>
              Par une ordonnance n° 15BX04062 du 23 septembre 2016, la présidente de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 17 décembre 2015 au greffe de cette cour, présenté par le ministre de l'écologie, du développement durable et de l'énergie et le ministre de l'égalité des territoires et du logement.  <br/>
<br/>
              Par ce pourvoi, le ministre de l'écologie, du développement durable et de l'énergie et le ministre de l'égalité des territoires et du logement demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance du président de la 4ème chambre du tribunal administratif de Bordeaux du 16 octobre 2015 en tant qu'elle met à la charge de l'Etat le versement à M. A...de la somme de 6 000 euros, assortie des intérêts et de leur capitalisation ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A.affectés sur un tel emploi durant une période de référence de trois ans <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2008-369 du 17 avril 2008 ;<br/>
              - l'arrêté du 28 juillet 2009 fixant les conditions d'octroi de l'indemnité temporaire de mobilité, instituée par le décret n° 2008-369 du 17 avril 2009, aux agents du ministère de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er du décret du 17 avril 2008 portant création d'une indemnité temporaire de mobilité : " Dans les administrations de l'Etat, (...) une indemnité temporaire de mobilité peut être accordée, dans le cadre d'une mobilité fonctionnelle ou géographique, aux fonctionnaires (...) " ; qu'aux termes de l'article 2 du même décret : " (...) Le ou les emplois susceptibles de donner lieu à l'attribution d'une indemnité temporaire de mobilité sont déterminés par arrêté du ministre intéressé. Cet arrêté fixe également la période de référence pour le versement de l'indemnité dans la limite de six années, sans que cette période puisse être inférieure à trois ans " ; qu'aux termes de l'article 3 du même décret : " (...) L'indemnité est payée en trois fractions : / - une première, de 40 %, lors de l'installation du fonctionnaire dans son nouvel emploi ; / - une deuxième, de 20 %, au terme d'une durée égale à la moitié de la période de référence ; / - une troisième, de 40 %, au terme de la période de référence " ;  qu'aux termes de l'article 4 du même décret : " L'agent qui, sur sa demande, quitte l'emploi au titre duquel il perçoit l'indemnité temporaire de mobilité avant le terme de la période de référence ne pourra percevoir les fractions non encore échues de l'indemnité " ; qu'aux termes de l'article 4 de l'arrêté du 28 juillet 2009 pris pour l'application de ce décret : " Dans le cadre de la réorganisation de certains services, sont susceptibles de donner lieu à l'attribution de l'indemnité temporaire de mobilité les emplois suivants : / (...) 2° Les emplois des pôles supports intégrés et des centres de prestations comptables mutualisés " ; qu'aux termes de l'article 5 du même arrêté : " Le montant de l'indemnité temporaire de mobilité est fixé à 10 000 euros " ; qu'aux termes du second alinéa de l'article 6 de cet arrêté : " L'indemnité est versée sur une période de référence de trois ans aux agents affectés sur les emplois visés à l'article 4 du présent arrêté " ; qu'il résulte de ces dispositions que les agents des ministères chargés de l'écologie, de l'énergie, du développement durable, de la mer et du logement occupant un emploi mentionné à l'article 4 de l'arrêté précité n'ont droit à l'intégralité du montant de l'indemnité temporaire de mobilité que s'ils demeurent affectés sur un tel emploi durant une période de référence de trois ans; <br/>
<br/>
              2. Considérant qu'il résulte des énonciations de l'ordonnance attaquée que M. A... a perçu une somme de 4 000 euros correspondant à la première fraction de l'indemnité temporaire de mobilité au titre de l'emploi occupé par cet agent au " Pôle support intégré " à la direction régionale de l'environnement, de l'aménagement et du logement d'Aquitaine, à compter du 4 janvier 2010 ; que, par l'ordonnance attaquée, le président de la 4ème chambre du tribunal administratif de Bordeaux a jugé que M. A...était fondé à demander que lui soit versée l'intégralité de la prime en litige, soit la somme de 10 000 euros, et a mis à la charge de l'administration le versement de la somme de 6 000 euros ; <br/>
<br/>
              3. Considérant qu'il ne ressort pas des pièces du dossier soumis au juge du fond que M. A...ne remplissait pas les conditions requises pour percevoir l'intégralité de l'indemnité temporaire de mobilité et que les ministres n'ont pas opposé de contestation sur ce point en première instance ; que par suite, les ministres ne peuvent utilement soutenir pour la première fois en cassation que M. A...serait parti en retraite avant le terme de la période de référence de trois ans fixée par l'arrêté précité du 28 juillet 2009 et qu'il n'a donc pas droit à l'intégralité de l'indemnité en litige ni se prévaloir de ce que le juge du fond aurait méconnu ses obligations en les condamnant à payer une somme qu'ils ne doivent pas ; que les ministres ne peuvent pas davantage soutenir utilement que le président de la 4ème chambre du tribunal administratif de Bordeaux aurait commis une erreur de droit en s'abstenant de vérifier que l'intéressé remplissait la condition de durée d'affectation lui ouvrant droit au bénéfice de l'intégralité de l'indemnité litigieuse dès lors qu'il ne ressortait pas des pièces du dossier qui lui était soumis que M. A...ne la remplissait pas ; qu'il suit de là que les ministres ne sont pas fondés à demander l'annulation de l'ordonnance qu'ils attaquent ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'écologie, du développement durable et de l'énergie et du ministre de l'égalité des territoires et du logement est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, au ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales, à la ministre du logement et de l'habitat durable et à M. B...A.affectés sur un tel emploi durant une période de référence de trois ans<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
