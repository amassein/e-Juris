<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983433</ID>
<ANCIEN_ID>JG_L_2015_08_000000392124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/34/CETATEXT000030983433.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/08/2015, 392124, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-08-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:392124.20150804</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 27 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la Fédération générale autonome des fonctionnaires - Union régionale de la Réunion (FGAF Réunion) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté de la ministre des outre-mer du 9 juillet 2015 relatif à la procédure de désignation des membres du Conseil économique, social et environnemental représentant les activités économiques et sociales des départements et régions d'outre-mer, des collectivités d'outre-mer et de la Nouvelle-Calédonie ; <br/>
<br/>
              2°) d'enjoindre à la ministre des outre-mer d'inscrire la FGAF Réunion sur la liste des organisations les plus représentatives prévue à l'article 11 du décret n° 84-558 du 4 juillet 1984 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie, compte tenu de l'atteinte à ses intérêts et à ceux de ses adhérents, de l'atteinte à l'intérêt public que constitue l'égalité des citoyens et des salariés et des conséquences qu'aurait l'annulation rétroactive de l'arrêté contesté ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté, compte tenu de l'erreur manifeste commise par la ministre dans l'appréciation du poids respectif des différents syndicats de la Réunion.<br/>
<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cet arrêté ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1360 du 29 décembre 1958 ;<br/>
              - le décret n° 84-558 du 4 juillet 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / (...) La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision " ; qu'il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ;<br/>
<br/>
              2. Considérant que l'arrêté de la ministre des outre-mer du 9 juillet 2015 dresse une liste de près de cinq cents organisations professionnelles locales de Guadeloupe, de Guyane, de Martinique, de la Réunion, de Mayotte, de Saint-Barthélemy, de Saint-Martin, de Saint-Pierre-et-Miquelon, de Nouvelle-Calédonie, de Polynésie française et des îles Wallis et Futuna, appelées à être consultées par le représentant de l'Etat et à proposer la candidature d'une personnalité en vue de la désignation des onze membres du Conseil économique, social et environnemental représentant les activités économiques et sociales des départements et régions d'outre-mer, des collectivités d'outre-mer régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie ; <br/>
<br/>
              3. Considérant que, pour justifier de l'urgence qui s'attacherait à ce que soit ordonnée la suspension de l'exécution de cet arrêté, la FGAF Réunion fait valoir, en premier lieu, qu'il la prive de la possibilité de participer au processus de désignation des membres du Conseil économique, social et environnemental ; que, cependant, il n'en résulte pas une atteinte suffisamment grave et immédiate aux intérêts qu'elle entend défendre pour caractériser une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative ; que l'atteinte au principe d'égalité qu'elle invoque, si elle peut, le cas échéant, affecter la légalité de l'arrêté en cause, est en revanche sans incidence sur l'appréciation de l'urgence à en suspendre l'exécution ;<br/>
<br/>
              4. Considérant que la fédération requérante fait valoir, en second lieu, la nécessité de prévenir les conséquences qu'aurait une annulation rétroactive de l'arrêté en litige sur la désignation des membres du Conseil économique, social et environnemental représentant l'outre-mer, devant intervenir en novembre prochain ; que, toutefois, la procédure de référé engagée ne peut avoir pour objet que le prononcé de mesures provisoires, par nature insusceptibles de lever l'incertitude juridique susceptible d'affecter la légalité de l'acte en litige ; que les incidences que pourrait avoir l'annulation de cet acte ne sont pas, par elles-mêmes, de nature à caractériser une situation d'urgence justifiant que son exécution soit suspendue jusqu'à ce qu'il soit statué sur la requête tendant à son annulation ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la condition d'urgence n'est pas remplie ; qu'il y a lieu, par suite, de rejeter la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Fédération générale autonome des fonctionnaires - Union régionale de la Réunion est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la Fédération générale autonome des fonctionnaires - Union régionale de la Réunion.<br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
