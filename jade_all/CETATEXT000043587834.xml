<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587834</ID>
<ANCIEN_ID>JG_L_2021_05_000000450256</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587834.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 28/05/2021, 450256</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450256</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Cécile Nissen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450256.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 1er mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société Burger King France, la société Bertrand restauration et la société Groupe Flo demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-1766 du 30 décembre 2020 relatif aux bénéficiaires des dispositions de l'article 14 de la loi n° 2020-1379 du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire et portant sur les loyers et charges locatives ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - la loi n° 2020-1379 du 14 novembre 2020, notamment son article 14 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Nissen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société Burger King France, de la société Bertrand restauration et de la société Groupe Flo ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 14 de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire : " I. - Le présent article est applicable aux personnes physiques et morales de droit privé exerçant une activité économique affectée par une mesure de police administrative prise en application des 2° ou 3° du I de l'article 1er de la loi n° 2020-856 du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire ou du 5° du I de l'article L. 3131-15 du code de la santé publique, y compris lorsqu'elle est prise par le représentant de l'Etat dans le département en application du second alinéa du I de l'article L. 3131-17 du même code. Les critères d'éligibilité sont précisés par décret, lequel détermine les seuils d'effectifs et de chiffre d'affaires des personnes concernées ainsi que le seuil de perte de chiffre d'affaires constatée du fait de la mesure de police administrative. / II. - Jusqu'à l'expiration d'un délai de deux mois à compter de la date à laquelle leur activité cesse d'être affectée par une mesure de police mentionnée au I, les personnes mentionnées au même I ne peuvent encourir d'intérêts, de pénalités ou toute mesure financière ou encourir toute action, sanction ou voie d'exécution forcée à leur encontre pour retard ou non-paiement des loyers ou charges locatives afférents aux locaux professionnels ou commerciaux où leur activité est ou était ainsi affectée. / Pendant cette même période, les sûretés réelles et personnelles garantissant le paiement des loyers et charges locatives concernés ne peuvent être mises en oeuvre et le bailleur ne peut pas pratiquer de mesures conservatoires. / Toute stipulation contraire, notamment toute clause résolutoire ou prévoyant une déchéance en raison du non-paiement ou retard de paiement de loyers ou charges, est réputée non écrite. / III. - Le II ne fait pas obstacle à la compensation au sens de l'article 1347 du code civil. /  IV. - Le II s'applique aux loyers et charges locatives dus pour la période au cours de laquelle l'activité de l'entreprise est affectée par une mesure de police mentionnée au I. / Les intérêts ou pénalités financières ne peuvent être dus et calculés qu'à compter de l'expiration du délai mentionné au premier alinéa du II. / En outre, les procédures d'exécution qui auraient été engagées par le bailleur à l'encontre du locataire pour non-paiement de loyers ou de charges locatives exigibles sont suspendues jusqu'à la date mentionnée au même premier alinéa. (...) ".<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 30 décembre 2020 relatif aux bénéficiaires des dispositions de l'article 14 de la loi n° 2020-1379 du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire et portant sur les loyers et charges locatives : " I. - Pour l'application de l'article 14 de la loi du 14 novembre 2020 susvisée, les personnes physiques et morales de droit privé mentionnées au I du même article sont celles remplissant les critères d'éligibilité suivants : 1° Leur effectif salarié est inférieur à 250 salariés ; 2° Le montant de leur chiffre d'affaires constaté lors du dernier exercice clos est inférieur à 50 millions d'euros ou, pour les activités n'ayant pas d'exercice clos, le montant de leur chiffre d'affaires mensuel moyen est inférieur à 4,17 millions d'euros ; 3°Leur perte de chiffre d'affaires est d'au moins 50 % appréciés selon les modalités fixées au II. (...) / V. - Les conditions fixées aux 1° et 2° du I sont considérées au premier jour où la mesure de police administrative mentionnée au I de l'article 14 de la loi susvisée s'applique. Le seuil d'effectif est calculé selon les modalités prévues par le I de l'article L. 130-1 du code de la sécurité sociale et il est tenu compte de l'ensemble des salariés des entités liées lorsque l'entreprise locataire contrôle ou est contrôlée par une autre personne morale au sens de l'article L. 233-3 du code de commerce. "<br/>
<br/>
              3. Les sociétés Burger King et autres, qui exercent l'activité de restauration, demandent au Conseil d'Etat d'annuler ce décret pour excès de pouvoir. A l'appui de leur requête, elles soulèvent une question prioritaire de constitutionnalité, mettant en cause la conformité aux droits et libertés garantis par la Constitution de la dernière phrase du I de l'article 14 de la loi du 14 novembre 2020. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. A l'appui de la question prioritaire de constitutionnalité qu'elles soulèvent, les sociétés requérantes soutiennent que les dispositions du I de l'article 14 de la loi du 14 novembre 2020, d'une part, méconnaissent l'article 34 de la Constitution et sont entachées d'incompétence négative au regard de la liberté contractuelle garantie par les articles 4 et 16 de la Déclaration des droits de l'homme et du citoyen de 1789 et, d'autre part, méconnaissent le principe d'égalité protégé par les articles 1er et 6 de cette Déclaration en tant qu'elles n'ont pas défini précisément le champ d'application des mesures prévues. <br/>
<br/>
              6. En premier lieu, s'il est soutenu que le législateur, faute de définir précisément les bénéficiaires du régime mis en place, aurait méconnu l'étendue de sa compétence dans des conditions affectant la liberté contractuelle entre bailleurs et locataires, il résulte des termes mêmes de l'article 14 de la loi du 14 novembre 2020 que le législateur a expressément déterminé que les dispositions qu'il adoptait, visant à faire obstacle au versement d'intérêts, à des pénalités, à toute mesure financière, aux actions, sanctions ou voies d'exécution forcée ainsi qu'aux mesures conservatoires susceptibles d'être mises en oeuvre en raison du retard ou défaut de paiement de loyers ou charges locatives, étaient applicables à celles des personnes physiques et morales de droit privé, exerçant une activité économique affectée du fait d'une mesure de police administrative prise dans le cadre de l'état d'urgence sanitaire ou de sortie de l'état d'urgence sanitaire, qui remplissent des critères d'éligibilité tenant à leurs effectifs, à leur chiffre d'affaires et à la part de chiffre d'affaires perdue du fait des mesures de police administrative. Le législateur pouvait, sans méconnaître l'étendue de sa compétence dans des conditions affectant la liberté contractuelle, renvoyer à un décret le soin de fixer le montant des seuils d'effectifs, de chiffre d'affaires et de perte exigés pour pouvoir prétendre au bénéfice des dispositions en cause.<br/>
<br/>
              7. En second lieu, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. <br/>
<br/>
              8. Il résulte de l'article 14 de la loi du 14 novembre 2020, éclairé par les travaux parlementaires préalables à son adoption, que le législateur a entendu réserver le bénéfice de ses dispositions à celles des entreprises, particulièrement touchées par les effets de la crise sanitaire, qui sont les plus vulnérables financièrement et qui disposent, du fait de leur taille, d'un moindre pouvoir de négociation avec leur bailleur. Les critères d'éligibilité pour bénéficier des mesures en cause reposent sur des critères objectifs en rapport avec l'objet de la loi. Les sociétés requérantes ne sont, par suite, pas fondées à soutenir que les dispositions législatives qu'elles critiquent méconnaîtraient le principe d'égalité au motif qu'elles excluraient du dispositif certaines entreprises, qui ne sont pas dans une situation analogue à celles qui en bénéficient et peuvent, au demeurant, prétendre par ailleurs à d'autres dispositifs d'aide accordés indépendamment de la taille d'entreprise.<br/>
<br/>
              9. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que la dernière phrase du I de l'article 14 de la loi du 14 novembre 2020 porterait atteinte aux droits et libertés garantis par la Constitution doit être écarté. <br/>
<br/>
              Sur la légalité du décret attaqué : <br/>
<br/>
              10. En premier lieu, il ressort des mentions de l'ampliation du décret attaqué, certifié conforme par la secrétaire générale du Gouvernement, que, contrairement à ce qui est soutenu, le décret a été signé par le Premier ministre et contresigné par le ministre de l'économie, des finances et de la relance, le ministre des outre-mer ainsi que, au demeurant, par le ministre délégué auprès du ministre de l'économie, des finances et de la relance, chargé des petites et moyennes entreprises. Par suite, le moyen tiré de ce que ce décret méconnaîtrait les dispositions de l'article L. 212-1 du code des relations entre le public et l'administration, en vertu desquelles toute décision prise par une administration comporte la signature de son auteur, ne peut qu'être écarté. <br/>
<br/>
              11. En second lieu, les requérantes ne peuvent utilement soutenir que le décret attaqué porterait par lui-même atteinte au principe d'égalité en tant qu'il fait obstacle à ce que les dispositions de l'article 14 de la loi du 14 novembre 2020 s'appliquent à l'ensemble des entreprises locataires dont l'activité économique est affectée par une mesure de police administrative dans le cadre de la crise sanitaire, alors que, ainsi qu'il a été dit précédemment, le législateur a entendu réserver le bénéfice de ces dispositions aux entreprises dont les effectifs et le chiffres d'affaires ne dépassent pas un certain seuil.<br/>
<br/>
              12. Il résulte de ce qui précède que les conclusions à fin d'annulation des sociétés requérantes doivent être rejetées.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Burger King France et autres. <br/>
Article 2 : La requête de la société Burger King France et autres est rejetée.<br/>
Article 3 : La présente décision sera notifiée aux sociétés Burger King France, Bertrand restauration, et Groupe Flo et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre des outre-mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-015-03-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - PRINCIPE D'ÉGALITÉ - DISPOSITIF D'AIDE AUX ENTREPRISES LOCATAIRES AFFECTÉES PAR L'ÉTAT D'URGENCE SANITAIRE (ART. 14 DE LA LOI DU 14 NOVEMBRE 2020) - 1) CONDITIONS D'ÉLIGIBILITÉ - VULNÉRABILITÉ FINANCIÈRE DE L'ENTREPRISE ET, DU FAIT DE SA TAILLE, MOINDRE POUVOIR DE NÉGOCIATION AVEC SON BAILLEUR - RAPPORT DIRECT AVEC L'OBJET DE LA LOI - EXISTENCE - 2) CONSÉQUENCES - DIFFÉRENCE DE SITUATION ENTRE LES ENTREPRISES INCLUES ET EXCLUES DU DISPOSITIF - EXISTENCE - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-015-03-01-01-01 Article 14 de la loi n° 2020-1379 du 14 novembre 2020 protégeant certaines entreprises dont l'activité est affectée par une mesure de police administrative prise au titre de l'état d'urgence sanitaire des conséquences du retard ou du non-paiement des loyers ou charges locatives afférents à leurs locaux professionnels ou commerciaux.,,,1) Il résulte de ces dispositions, éclairées par les travaux parlementaires préalables à leur adoption, que le législateur a entendu réserver leur bénéfice à celles des entreprises, particulièrement touchées par les effets de la crise sanitaire, qui sont les plus vulnérables financièrement et qui disposent, du fait de leur taille, d'un moindre pouvoir de négociation avec leur bailleur.,,,Les critères d'éligibilité pour bénéficier des mesures en cause reposent sur des critères objectifs en rapport direct avec l'objet de la loi.,,,2) Ces dispositions excluent ainsi du dispositif certaines entreprises qui ne sont pas dans une situation analogue à celles qui en bénéficient et peuvent, au demeurant, prétendre par ailleurs à d'autres dispositifs d'aide accordés indépendamment de la taille d'entreprise.,,,Dès lors, elles ne méconnaissent pas le principe constitutionnel d'égalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
