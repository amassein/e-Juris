<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110488</ID>
<ANCIEN_ID>JG_L_2013_10_000000360731</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110488.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 23/10/2013, 360731</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360731</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Versini-Monod</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:360731.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 4 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association " Ban Asbestos France ", dont le siège est à Algues à Nant (12230), représentée par son président, l'association Ardeva Sud Est, dont le siège est 449, avenue Edouard Herriot, L'Escaillon à Toulon (83200), représentée par sa présidente, l'association Addeva 81, dont le siège est au bureau n° 5, Place d'Hautpoul à Gaillac (81600), représentée par son président, l'association Caper d'Auvergne, dont le siège est à la Maison du Peuple - Place de la Liberté à Clermont-Ferrand (63000), représentée par sa présidente, et l'Union syndicale Solidaires, dont le siège est 144, boulevard de la Villette à Paris (75019) représentée par sa déléguée générale ; l'association " Ban Asbestos France " et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le troisième alinéa de l'article 5 du décret n° 2012-639 du 4 mai 2012 relatif aux risques d'exposition à l'amiante ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros à chacune d'entre elles au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ; <br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Versini-Monod, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article R. 4412-100 du code du travail, dans sa rédaction issue du décret attaqué, dispose que : " La concentration moyenne en fibres d'amiante, sur huit heures de travail, ne dépasse pas dix fibres par litre. (...) " ; qu'aux termes de l'article 5 de ce même décret : " Le présent décret entre en vigueur le 1er juillet 2012./ (...) Toutefois, jusqu'au 1er juillet 2015, la valeur limite d'exposition professionnelle prévue à l'article R. 4412-100 du code du travail est fixée à une concentration en fibres d'amiante dans l'air inhalé de cent fibres par litre évaluée sur une moyenne de huit heures de travail " ;<br/>
<br/>
              2. Considérant que si, en application des dispositions des articles L. 4121-1 et L. 4121-2 du code du travail, l'employeur a l'obligation générale d'assurer la sécurité et la protection de la santé des travailleurs placés sous son autorité, il incombe aux autorités publiques chargées de la prévention des risques professionnels de se tenir informées des dangers que peuvent courir les travailleurs dans le cadre de leur activité professionnelle, compte tenu notamment des produits et substances qu'ils manipulent ou avec lesquels ils sont en contact, et d'arrêter, en l'état des connaissances scientifiques, au besoin à l'aide d'études ou d'enquêtes complémentaires, les mesures les plus appropriées pour limiter et si possible éliminer ces dangers ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que les autorités publiques ont saisi le 7 février 2005 l'Agence française de sécurité sanitaire  de l'environnement et du travail (AFSSET) d'une demande d'évaluation de la toxicité des fibres courtes d'amiante et des risques pour la santé humaine d'une exposition à ces fibres ainsi que de la réglementation applicable ; que le 16 mai 2007, une saisine complémentaire a été réalisée aux mêmes fins pour les fibres fines d'amiante et en vue de procéder à une évaluation des possibilités offertes par la méthode de microscopie électronique à transmission analytique (META) par rapport à la méthode de microscopie optique en phase de contraste (MOCT), recommandée par l'Organisation mondiale de la santé (OMS), pour comptabiliser les fibres d'amiante présentes dans l'air, et notamment les fibres fines ; qu'à la suite du rapport rendu par l'AFSSET le 1er février 2009, le Premier ministre a pris le décret du 4 mai 2012 qui renforce les obligations des employeurs en leur imposant notamment d'estimer le niveau d'empoussièrement correspondant à chacun des processus de travail selon trois niveaux, de respecter une valeur limite d'exposition professionnelle à l'amiante fixée à dix fibres par litre sur une moyenne de huit heures de travail, de faire appel à un organisme accrédité pour le mesurage de l'empoussièrement selon la méthode META, de mettre en place des moyens de prévention collective pour garantir le niveau d'empoussièrement le plus bas possible ainsi que des équipements de protection individuelle adaptés aux opérations à réaliser selon le niveau d'empoussièrement et de mettre en oeuvre les dispositions spécifiques aux activités d'encapsulage et de retrait d'amiante ou d'articles en contenant ainsi qu'aux interventions sur des matériaux, des équipements, des matériels ou des articles susceptibles de provoquer l'émission de fibres d'amiante ; que les associations requérantes contestent le troisième alinéa de l'article 5 de ce décret, qui prévoit une entrée en vigueur décalée de trois années pour l'abaissement de la valeur limite d'exposition à dix fibres par litre sur huit heures de travail ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que des études et enquêtes complémentaires ainsi que des adaptations techniques doivent être réalisées préalablement à l'abaissement de la valeur limite d'exposition professionnelle fixée à dix fibres par litre, appréciée en divisant le niveau d'empoussièrement mesuré avec la méthode META par le facteur de protection assignée (FPA) à chaque type d'appareil de protection respiratoire ; qu'en particulier, le passage à la méthode META et l'abaissement de la valeur limite requièrent une évaluation des facteurs de protection assignés aux différents types d'appareils de protection respiratoire, qui a d'ailleurs été confiée à l'Institut national de recherche et de sécurité pour la prévention des accidents de travail et des maladies professionnelles (INRS) et dont les résultats ne seront connus que début 2015 ; que les équipements de protection individuelle doivent faire l'objet d'adaptations techniques pour rendre possible le respect de la nouvelle norme ; qu'au surplus, pour les interventions sur des matériaux, équipements, matériels ou articles susceptibles de provoquer l'émission ponctuelle de fibres d'amiante, qui sont réalisées sur de courtes périodes par de petites entreprises relevant de différents corps de métiers, les données d'empoussièrement mesurées avec la méthode META doivent être complétées par les résultats d'autres études à réaliser durant la période transitoire contestée ; que si les nouvelles dispositions du décret attaqué poursuivent un objectif d'amélioration de la protection des travailleurs exposés à l'amiante au regard de l'état du droit applicable avant son entrée en vigueur, le Premier ministre, compte tenu de l'ensemble de ces contraintes préalables, n'a pas méconnu les obligations rappelées au point 2 en repoussant jusqu'au 1er juillet 2015 l'entrée en vigueur de la nouvelle valeur limite d'exposition professionnelle à l'amiante et en prévoyant que, dans l'intervalle, la valeur limite serait fixée à cent fibres par litre évaluée sur une moyenne de huit heures de travail ; qu'il n'a pas non plus, ce faisant, méconnu les dispositions du 11ème alinéa du Préambule de la Constitution du 27 octobre 1946 ni, en tout état de cause, celles de l'article L. 4411-1 du code du travail ;<br/>
<br/>
              5. Considérant que les requérants ne peuvent utilement soutenir qu'en édictant cette mesure transitoire, le pouvoir réglementaire aurait  méconnu les dispositions des articles L. 4121-1 et L. 4121-2 du code du travail, qui concernent les employeurs et leur imposent une obligation permanente de sécurité et de résultat ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les organisations requérantes ne sont pas fondées à demander l'annulation du troisième alinéa de l'article 5 du décret du 4 mai 2012 relatif aux risques d'exposition à l'amiante ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association " Ban Asbestos France ", de l'association A.R.D.E.V.A Sud Est, de l'association Addeva 81, de l'association le Caper d'Auvergne et de l'Union syndicale Solidaires est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'association " Ban Asbestos France ", à l'association A.R.D.E.V.A Sud Est, à l'association ADDEVA 81, à l'association le Caper d'Auvergne, à l'Union syndicale Solidaires, au Premier ministre et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-02 POLICE. POLICES SPÉCIALES. POLICE SANITAIRE (VOIR AUSSI : SANTÉ PUBLIQUE). - REPORT DE L'ENTRÉE EN VIGUEUR D'UN RÈGLEMENT DE POLICE SANITAIRE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR LA DURÉE DE CE REPORT - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - DURÉE DU REPORT DE L'ENTRÉE EN VIGUEUR D'UN RÈGLEMENT DE POLICE SANITAIRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01-01-01 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. RÈGLEMENTS SANITAIRES. - ENTRÉE EN VIGUEUR - REPORT - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR LA DURÉE DE CE REPORT - CONTRÔLE NORMAL.
</SCT>
<ANA ID="9A"> 49-05-02 Le juge de l'excès de pouvoir exerce un contrôle normal sur la durée du report de l'entrée en vigueur d'un règlement de police sanitaire.</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur la durée du report de l'entrée en vigueur d'un règlement de police sanitaire.</ANA>
<ANA ID="9C"> 61-01-01-01 Le juge de l'excès de pouvoir exerce un contrôle normal sur la durée du report de l'entrée en vigueur d'un règlement de police sanitaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
