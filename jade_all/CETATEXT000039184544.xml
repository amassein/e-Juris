<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039184544</ID>
<ANCIEN_ID>JG_L_2019_09_000000420685</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/18/45/CETATEXT000039184544.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/09/2019, 420685, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420685</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP L. POULET, ODENT</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420685.20190920</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A... a demandé au tribunal administratif de Paris d'annuler la décision du 25 août 2016 de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) l'informant de l'annulation de sa pension de réversion et de la demande de reversement des sommes indûment perçues du 15 janvier 2000 au 31 juillet 2016 pour un montant total de 68 685 euros ainsi que la décision du 9 novembre 2016 de rejet de son recours gracieux du 23 octobre 2016. Par un jugement n° 1700270 du 15 mars 2018, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mai et 13 août 2018 et 5 juin 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
              - le code civil ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
              - la loi n° 2008-561 du 17 juin 2008 ;<br/>
              - le décret n° 65-773 du 9 septembre 1965 ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le décret n° 2007-173 du 7 février 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de Mme A... et à la SCP L. Poulet, Odent, avocat de la Caisse nationale de retraites des agents des collectivités locales;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces produites devant le juge du fond qu'à la suite du décès de son époux, fonctionnaire hospitalier, le 1er novembre 1989, la Caisse nationale de retraites des agents des collectivités locales (CNRACL) a concédé à Mme A... une pension de réversion. Dans le cadre d'une enquête sur la situation des bénéficiaires de pensions, l'intéressée a, le 20 juin 2016, déclaré, sur le formulaire qui lui a été adressé et qu'elle a retourné signé, vivre en concubinage depuis le 15 janvier 2000. La CNRACL l'a informée le 25 juillet 2016 que le versement de sa pension serait interrompu à compter du mois d'août 2016. Le 25 août 2016, elle a été informée de l'annulation de sa pension de réversion et de ce que les sommes indûment perçues du 15 janvier 2000 au 31 juillet 2016 lui seraient réclamées ultérieurement. En réponse à un recours gracieux, la CNRACL, par un courrier du 9 novembre 2016, lui a confirmé l'annulation de sa pension de réversion et la récupération des arrérages indûment versés pour un montant total de 68 685 euros. Par un jugement du 15 mars 2018, le tribunal administratif de Paris a rejeté la demande de Mme A... tendant à l'annulation des décisions du 25 août et 9 novembre 2016. Mme A... demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
              2. Si, en principe, le droit à pension de réversion est régi par les dispositions en vigueur à la date du décès de l'ayant cause, la restitution des sommes payées indûment au titre d'une pension est soumise, en l'absence de disposition contraire, aux dispositions en vigueur à la date à laquelle l'autorité compétente décide de procéder à la répétition des sommes indûment versées.<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales : " Les dispositions du présent décret s'appliquent aux fonctionnaires mentionnés à l'article 2 du décret n° 2007-173 du 7 février 2007 relatif à la Caisse nationale de retraites des agents des collectivités locales et à leurs ayants cause ". L'article 2 du décret du 7 février 2007 relatif à la Caisse nationale de retraites des agents des collectivités locales dispose que : " Sont obligatoirement affiliés à la Caisse nationale de retraites des agents des collectivités locales les fonctionnaires soumis aux dispositions (...) de la loi n° 86-33 du 9 janvier 1986 (...) ". Aux termes de l'article 47 du décret du 26 décembre 2003 précité, qui reprend les dispositions de l'article 43 du décret du 9 septembre 1965 portant règlement d'administration publique relatif au régime de retraite des tributaires de la Caisse nationale de retraites des agents des collectivités locales : " Le conjoint survivant ou divorcé qui contracte un nouveau mariage ou vit en état de concubinage notoire perd son droit à pension (...) ".<br/>
<br/>
              4. L'article 59 du décret du 26 décembre 2003 précité dispose que : " (...) La restitution des sommes payées indûment au titre des pensions, de leurs accessoires, d'avances provisoires sur pensions, attribués en application des dispositions du présent décret est réglée conformément aux dispositions de l'article L. 93 du code des pensions civiles et militaires de retraite (...) ". Aux termes de l'article L. 93 du code des pensions civiles et militaires de retraite : " Sauf le cas de fraude, omission, déclaration inexacte ou de mauvaise foi de la part du bénéficiaire, la restitution des sommes payées indûment au titre des pensions, de leurs accessoires ou d'avances provisoires sur pensions, attribués en application des dispositions du présent code, ne peut être exigée que pour celles de ces sommes correspondant aux arrérages afférents à l'année au cours de laquelle le trop-perçu a été constaté et aux trois années antérieures ".<br/>
<br/>
              5. L'époux de Mme A... était fonctionnaire hospitalier, soumis aux dispositions de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière. Par suite, compte tenu, ainsi qu'il a été dit au point 2, de la date à laquelle la CNRACL a décidé de procéder à la répétition des sommes indûment versées, les dispositions relatives aux règles de prescription applicables à la pension de réversion perçue par Mme A... sont celles du décret du 26 décembre 2003 qui renvoient au code des pensions civiles et militaires de retraite.<br/>
<br/>
              6. En premier lieu, la perception par Mme A..., du 15 janvier 2000 au 31 juillet 2016, de sa pension de réversion malgré son concubinage notoire est consécutive à une absence de déclaration auprès de l'administration lors de son changement de situation. Cette omission, alors même qu'elle ne révèle aucune intention frauduleuse ou mauvaise foi, fait obstacle à l'application de la prescription prévue par l'article L. 93 du code des pensions civiles et militaires de retraite. Dès lors, Mme A... n'est pas fondée à soutenir que le tribunal administratif de Paris a commis une erreur de droit en estimant qu'elle ne pouvait bénéficier de cette prescription.<br/>
<br/>
              7. En deuxième lieu, en vertu de l'article 2224 du code civil, dans sa rédaction issue de la loi du 17 juin 2008 portant réforme de la prescription en matière civile, les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer.<br/>
<br/>
              8. La prescription quinquennale ainsi prévue ne porte que sur le délai pour exercer l'action, non sur la détermination de la créance elle-même. Ainsi, dès lors que l'action est introduite dans le délai de cinq ans à compter du jour où le titulaire du droit a connu ou aurait dû connaître les faits lui permettant de l'exercer, la seule limite à l'exercice de ce droit résulte de l'article 2232 du code civil, dans sa rédaction issue de la loi du 17 juin 2008, aux termes duquel " le report du point de départ, la suspension ou l'interruption de la prescription ne peut avoir pour effet de porter le délai de la prescription extinctive au-delà de vingt ans à compter du jour de la naissance du droit ". Cependant, en application des dispositions du II de son article 26, les dispositions de la loi du 17 juin 2008 qui réduisent la durée d'une prescription s'appliquent à compter du jour de l'entrée en vigueur de la loi, sans que la durée totale puisse excéder la durée prévue par la loi antérieure. Il en résulte que lorsque l'exercice d'une action n'était enserré, avant l'intervention de la loi du 17 juin 2008, que par la prescription trentenaire, cette prescription continue à s'appliquer.<br/>
<br/>
              9. Ainsi qu'il a été dit au point 1, il ressort des pièces du dossier soumis au juge du fond que Mme A... a déclaré, le 20 juin 2016, vivre en concubinage notoire depuis le 15 janvier 2000. Dès lors, le tribunal administratif de Paris a pu estimer, sans entacher d'erreur de droit son jugement, qui est suffisamment motivé, que l'action de la CNRACL en répétition des sommes indûment versées à Mme A... depuis le 15 janvier 2000, engagée dans le délai de cinq ans à compter de la date à laquelle elle avait été informée du changement de situation familiale de l'intéressée, n'était pas prescrite à la date des décisions attaquées des 25 août et 9 novembre 2016.<br/>
<br/>
              10. En dernier lieu, les moyens tirés de la méconnaissance du principe d'égalité, du principe de sécurité juridique, du principe de confiance légitime et de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'ont pas été invoqués devant le tribunal administratif de Paris. Ces moyens ne sont pas nés du jugement attaqué et ne sont pas d'ordre public. Par suite, Mme A... ne peut utilement les soulever pour contester le bien-fondé du jugement qu'elle attaque.<br/>
<br/>
              11. Il résulte de ce qui précède que le pourvoi de Mme A... doit être rejeté.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la CNRACL qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme C... A... et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
