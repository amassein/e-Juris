<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043834078</ID>
<ANCIEN_ID>JG_L_2021_07_000000454526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/83/40/CETATEXT000043834078.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/07/2021, 454526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454526.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de la Garenne-Colombes a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 30 juin 2021 par laquelle le président du Centre national du cinéma et de l'image animée a refusé l'autorisation de neuf séances de spectacles cinématographiques en plein air dans la commune et, d'autre part, d'enjoindre au Centre national du cinéma et de l'image animée de lui délivrer une autorisation d'organiser des séances de spectacles cinématographiques en plein air sur le fondement de l'article L. 214-6 du code du cinéma et de l'image animée, sous astreinte de 2 500 euros par jour de retard. Par une ordonnance n° 2108635 du 7 juillet 2021, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 13 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de la Garenne-Colombes demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de suspendre l'exécution de la décision du 30 juin 2021 refusant l'organisation de séances de spectacles cinématographiques en plein air ; <br/>
<br/>
              3°) d'enjoindre au Centre national du cinéma et de l'image animée de lui délivrer une autorisation d'organiser des séances de spectacles cinématographiques en plein air sur le fondement des dispositions de l'article L. 214-6 du code du cinéma et de l'image animée, sous astreinte de 2 500 euros par jour de retard à compter de l'ordonnance à intervenir ;<br/>
<br/>
              4°) de mettre à la charge du Centre national du cinéma et de l'image animée la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, elle prive la population communale d'accès à une activité culturelle de plein air dans le contexte de la réouverture de ces activités et dans une période estivale où certains ne peuvent partir en vacances et doivent bénéficier d'activités gratuites, en deuxième lieu, elle a pris soin de déposer sa demande dès le mois d'avril 2021 et a réservé des prestations auprès d'une société chargée de projections de plein air et, enfin, le contexte de sortie de crise sanitaire justifie d'organiser ces séances en plein air dans la commune qui est dépourvue de tout cinéma ;<br/>
              - la décision litigieuse porte une atteinte grave et manifestement illégale à la liberté d'accès aux oeuvres culturelles et à la libre administration des collectivités territoriales ; <br/>
              - elle méconnaît la liberté d'accès aux oeuvres culturelles dès lors que, d'une part, la programmation des séances de plein air ne concurrence pas les salles cinématographiques situées aux alentours au vu du nombre limité de places prévues et du public visé et, d'autre part, le projet a un intérêt social et culturel au regard de sa gratuité et des oeuvres cinématographiques choisies ;<br/>
              - elle méconnaît la libre administration des collectivités territoriales et le principe d'égalité, en ce qu'elle la prive, sans qu'un motif d'intérêt général le justifie, de la possibilité d'exercer ses compétences selon la politique publique qu'elle a décidé, quand bien même d'autres communes ne sont pas soumises à la même interdiction.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 juillet 2021, le Centre national du cinéma et de l'image animée conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée à la ministre de la culture qui n'a pas produit d'observations.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du cinéma et de l'image animée ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le représentant de la commune de la Garenne-Colombes, et d'autre part, le Centre national du cinéma et de l'image animée et la ministre de la culture ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 15 juillet 2021, à 14 heures 30 : <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la commune de la Garenne-Colombes ;<br/>
<br/>
              Me Molinié, avocat du Centre national du cinéma et de l'image animée ;<br/>
<br/>
              - les représentants du Centre national du cinéma et de l'image animée ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Aux termes de l'article L. 214-1 du code du cinéma et de l'image animée : " Sont soumises aux dispositions du présent chapitre : / (...) 6° Les séances en plein air autres que celles organisées par les exploitants d'établissements de spectacles cinématographiques dans les conditions prévues à l'article L. 212-18 ". Aux termes de l'article L. 214-6 du même code : " Les séances mentionnées au 6° de l'article L. 214-1, qui consistent dans la représentation d'oeuvres cinématographiques de longue durée, ne peuvent être organisées qu'après délivrance d'une autorisation par le président du Centre national du cinéma et de l'image animée dans des conditions fixées par décret. / Cette autorisation est accordée en tenant compte de la date de délivrance de visa d'exploitation cinématographique, du lieu et du nombre des séances, de l'intérêt social et culturel des représentations et de la situation locale de l'exploitation (...). "<br/>
<br/>
              3. Il résulte de l'instruction que la commune de la Garenne-Colombes a sollicité l'autorisation du Centre national du cinéma et de l'image animée pour organiser neuf séances gratuites de cinéma en plein air chaque samedi entre début juillet et fin août. Par une décision du 30 juin 2021, le président du Centre national du cinéma et de l'image animée a refusé d'autoriser ces séances en plein air. La commune de la Garenne-Colombes a saisi, sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Cergy-Pontoise d'une requête tendant à la suspension de l'exécution de cette décision de refus d'autorisation et à ce qu'il soit enjoint au Centre national du cinéma et de l'image animée d'autoriser l'organisation de ces séances en plein air. Par une ordonnance du 7 juillet 2021, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande. La commune de la Garenne-Colombe relève appel de cette ordonnance.<br/>
<br/>
              4. En premier lieu, il résulte de l'instruction que les cinémas situés aux alentours de la commune de la Garenne-Colombe, dont la plupart sont des établissements indépendants, ont subi des pertes de chiffre d'affaires très importantes en 2020 et depuis le début de l'année 2021 en raison des restrictions sanitaires. Si la commune de la Garenne-Colombes soutient que l'organisation de neuf séances gratuites de plein air ne présenterait pas de risque de perturbation de l'équilibre économique de ces exploitants et que les films proposés dans le cadre des séances en plein air sont des films anciens qui ne sont pas diffusés par les cinémas concernés, c'est à bon droit que le premier juge a considéré que ces pertes substantielles de chiffres d'affaires étaient de nature à caractériser l'existence d'une situation locale d'exploitation très dégradée, critère qui pouvait être pris en compte pour refuser de délivrer l'autorisation litigieuse en application des dispositions de l'article L. 214-6 du code du cinéma et des images animées citées au point 2. Par suite, la commune requérant n'est pas fondée à soutenir que c'est à tort que le juge des référés a jugé que la décision litigieuse ne portait pas une atteinte grave et manifestement illégale à la liberté d'accès aux oeuvres culturelles qu'elle invoquait.<br/>
<br/>
              5. En deuxième lieu, la circonstance que d'autres communes, au demeurant placées dans des situations différentes, auraient organisé des séances de cinéma en plein air ne peut faire regarder la décision litigieuse, qui intervient en application d'un régime d'autorisation prévu par la loi, comme portant une atteinte grave et manifestement illégale à la libre administration des collectivités territoriales.<br/>
<br/>
              6. Il résulte de ce qui précède que la commune de la Garenne-Colombes n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa requête. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de la Garenne-Colombes la somme de 2 500 euros à verser au Centre national du cinéma et des images animées en application de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du Centre national du cinéma et des images animées qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la commune de la Garenne-Colombes est rejetée.<br/>
Article 2 : La commune de la Garenne-Colombes versera au Centre national du cinéma et des images animées une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente ordonnance sera notifiée à la commune de la Garenne-Colombes et au Centre national du cinéma et de l'image animée.<br/>
Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
