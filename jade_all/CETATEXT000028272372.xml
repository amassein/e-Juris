<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028272372</ID>
<ANCIEN_ID>JG_L_2013_12_000000357335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/27/23/CETATEXT000028272372.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 04/12/2013, 357335</TITRE>
<DATE_DEC>2013-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357335.20131204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 mars et 5 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B... A...C..., demeurant... ; Mme A... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY01474 du 13 décembre 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0902763 du 12 avril 2011 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation des décisions du ministre de l'éducation nationale du 8 juin 2009 la mettant à la retraite d'office et du 26 novembre 2009 rejetant son recours gracieux contre cette décision et, d'autre part, à l'annulation pour excès de pouvoir de ces décisions ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 82-451 du 28 mai 1982 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de Mme B...A...C... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, selon l'article 5 du décret du 28 mai 1982 relatif aux commissions administratives paritaires, ces commissions " comprennent en nombre égal des représentants de l'administration et des représentants du personnel. Elles ont des membres titulaires et un nombre égal de membres suppléants " ; qu'en vertu de l'article 10 du même décret, les représentants de l'administration, titulaires et suppléants, sont nommés par arrêté du ou des ministres intéressés ou par décision de l'autorité auprès de laquelle sont placées ces commissions ; qu'en vertu des articles 15, 21 et 22 du même décret, d'une part, les sièges de représentants titulaires du personnel sont répartis entre les différentes listes de candidats présentées, sans que ces listes ne fassent mention de la qualité de titulaire ou de suppléant de chacun des candidats, en fonction de leurs résultats respectifs aux élections, les représentants titulaires étant désignés, pour chaque grade, selon l'ordre de présentation de chaque liste et, d'autre part, les sièges de représentants suppléants du personnel sont attribués à chaque liste et pour chaque grade en nombre égal aux sièges de représentants titulaires, les représentants suppléants étant désignés selon l'ordre de présentation de chaque liste, après désignation des représentants titulaires ; qu'enfin, aux termes de l'article 31 du même décret : " Les suppléants peuvent assister aux séances de la commission sans pouvoir prendre part aux débats. Ils n'ont voix délibérative qu'en l'absence des titulaires qu'ils remplacent. (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, si tout représentant suppléant de l'administration a vocation à remplacer tout représentant titulaire de l'administration qui se trouve dans l'impossibilité de participer à une séance d'une commission administrative paritaire, notamment quand elle siège en conseil de discipline, un représentant suppléant du personnel, bien qu'il ne soit pas rattaché à un représentant titulaire donné, ne peut toutefois remplacer un représentant titulaire se trouvant dans l'impossibilité de siéger que s'il a été élu sur la même liste et au titre du même grade que ce dernier ;<br/>
<br/>
              3. Considérant que, pour rejeter, par l'arrêt attaqué, la requête de Mme B... A...C..., professeur agrégé de lettres classiques en poste au lycée Anna Judic de Semur-en-Auxois, tendant à l'annulation du jugement du tribunal administratif de Dijon du 12 avril 2011 rejetant sa demande d'annulation de l'arrêté du ministre de l'éducation nationale du 8 juin 2009 la mettant à la retraite d'office, la cour administrative d'appel de Lyon a écarté le moyen tiré de ce que l'arrêté serait intervenu au terme d'une procédure irrégulière, dès lors que, lors de la séance de la commission administrative paritaire des professeurs agrégés de l'académie de Dijon, siégeant en conseil de discipline, du 31 mars 2009, des représentants suppléants de l'administration et du personnel avaient délibéré en même temps que les représentants titulaires qu'ils étaient appelés à remplacer, au motif " qu'un suppléant (...) peut remplacer tout représentant titulaire absent " ; qu'il résulte de ce qui a été dit au point 2 qu'en énonçant cette règle générale, sans prendre en compte la situation particulière des représentants titulaires du personnel, qui ne peuvent être remplacés que par des suppléants élus sur la même liste et au titre du même grade, la cour administrative d'appel a commis une erreur de droit ; que Mme A... C... est fondée, pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à Mme A... C..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 13 décembre 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera à Mme A... C... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B... A...C... et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. COMPOSITION DE L'ORGANISME CONSULTÉ. - CAP - POSSIBILITÉ POUR UN SUPPLÉANT DE REMPLACER UN TITULAIRE - REPRÉSENTANTS DE L'ADMINISTRATION - EXISTENCE, QUEL QUE SOIT LE TITULAIRE EMPÊCHÉ [RJ1] - REPRÉSENTANTS DU PERSONNEL - CONDITION - SUPPLÉANT ÉLU SUR LA MÊME LISTE ET AU TITRE DU MÊME GRADE QUE LE TITULAIRE QU'IL REMPLACE.
</SCT>
<ANA ID="9A"> 01-03-02-06 Il résulte des dispositions du décret n° 82-451 du 28 mai 1982 relatif aux commissions administratives paritaires (CAP) que si tout représentant suppléant de l'administration a vocation à remplacer tout représentant titulaire de l'administration qui se trouve dans l'impossibilité de participer à une séance d'une CAP, notamment quand elle siège en conseil de discipline, un représentant suppléant du personnel, bien qu'il ne soit pas rattaché à un représentant titulaire donné, ne peut toutefois remplacer un représentant titulaire se trouvant dans l'impossibilité de siéger que s'il a été élu sur la même liste et au titre du même grade que ce dernier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les comités techniques paritaires, CE, 8 juillet 2009, Syndicat national C Justice, n° 317423 et autres, T. p. 609-805.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
