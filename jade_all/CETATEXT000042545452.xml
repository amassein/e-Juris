<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545452</ID>
<ANCIEN_ID>JG_L_2020_11_000000427761</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545452.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/11/2020, 427761</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427761</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427761.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Evancia (Babilou) a demandé au tribunal administratif de Rennes, d'une part, d'annuler le marché conclu entre le centre communal d'action sociale (CCAS) de Saint-Malo et la société La Maison Bleue portant sur la réservation au sein d'une structure à gestion privée, pour une durée de quatre ans maximum, de vingt places en crèche destinées à l'accueil collectif d'enfants âgés de dix semaines à six ans, d'autre part, de condamner le centre communal d'action sociale de Saint-Malo à lui verser la somme de 156 835 euros en réparation du préjudice résultant de la perte de chance de se voir attribuer le marché en cause. Par un jugement n°s 1601033, 1602925 du 23 novembre 2017, le tribunal administratif de Rennes a prononcé la résiliation du marché à compter du 1er juillet 2018 et rejeté la demande indemnitaire de la société Evancia.<br/>
<br/>
              Par un arrêt n°s 17NT03800, 17NT03847, 18NT00237 du 7 décembre 2018, la cour administrative d'appel de Nantes a, sur appel du centre communal d'action sociale de Saint-Malo, de la société La Maison Bleue et de la société Evancia, annulé l'article 1er de ce jugement et rejeté la demande de la société Evancia.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 février et 6 mai 2019 et le 3 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la société Evancia demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions présentées en appel ;<br/>
<br/>
              3°) de mettre à la charge du centre communal d'action sociale de Saint-Malo et de la société La Maison Bleue la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Evancia et à la SCP Foussard, Froger, avocat du centre communal d'action sociale (CCAS) de Saint-Malo ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un avis publié le 16 juin 2015, le centre communal d'action sociale (CCAS) de Saint-Malo a engagé une consultation, selon la procédure adaptée prévue aux articles 28 et 30 du code des marchés publics alors applicable, en vue de la passation d'un marché relatif à la réservation, pendant quatre ans maximum, de vingt places en crèche pour l'accueil collectif d'enfants âgés de dix semaines à six ans. Le 14 décembre 2015, le marché a été attribué à la société La Maison Bleue. A la demande de la société Evancia (Babilou), dont l'offre, classée deuxième, a été rejetée par le centre communal d'action sociale de Saint-Malo, le tribunal administratif de Rennes a, par un jugement du 23 novembre 2017, prononcé la résiliation à compter du 1er juillet 2018 du marché conclu entre le centre communal d'action sociale de Saint-Malo et la société La Maison Bleue et rejeté la demande indemnitaire de la société Evancia. Par un arrêt du 7 décembre 2018 contre lequel la société Evancia se pourvoit en cassation, la cour administrative d'appel de Nantes a, sur l'appel de la société La Maison Bleue et du centre communal d'action sociale de Saint-Malo, annulé l'article 1er du jugement du tribunal administratif de Rennes et rejeté les demandes présentées devant ce tribunal par la société Evancia.<br/>
<br/>
              2. Pour assurer le respect des principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, l'information appropriée des candidats sur les critères d'attribution d'un marché public est nécessaire, dès l'engagement de la procédure d'attribution du marché, dans l'avis d'appel public à concurrence ou le cahier des charges tenu à la disposition des candidats. Dans le cas où le pouvoir adjudicateur souhaite retenir d'autres critères que celui du prix, il doit porter à la connaissance des candidats la pondération ou la hiérarchisation de ces critères. Il doit également porter à la connaissance des candidats la pondération ou la hiérarchisation des sous-critères dès lors que, eu égard à leur nature et à l'importance de cette pondération ou hiérarchisation, ils sont susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection et doivent en conséquence être eux-mêmes regardés comme des critères de sélection. Il n'est, en revanche, pas tenu d'informer les candidats de la méthode de notation des offres.<br/>
<br/>
              3. Le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics. Il peut ainsi déterminer tant les éléments d'appréciation pris en compte pour l'élaboration de la note des critères que les modalités de détermination de cette note par combinaison de ces éléments d'appréciation. Une méthode de notation est toutefois entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, les éléments d'appréciation pris en compte pour noter les critères de sélection des offres sont dépourvus de tout lien avec les critères dont ils permettent l'évaluation ou si les modalités de détermination de la note des critères de sélection par combinaison de ces éléments sont, par elles-mêmes, de nature à priver de leur portée ces critères ou à neutraliser leur pondération et sont, de ce fait, susceptibles de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie. Il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le règlement de la consultation du marché en cause énonçait, à son article 12, que les offres seraient évaluées à partir de deux critères constitués du " prix unitaire à la place " de crèche et de la valeur technique de l'offre, respectivement pondérés à hauteur de 40 et de 60 points. La valeur technique de l'offre devait elle-même être évaluée au regard de quatre sous-critères tirés, en premier lieu, de la qualité du projet d'établissement, dont les moyens humains mis en oeuvre, pondérée à hauteur de 25 points, en deuxième lieu, de la situation géographique au sein du périmètre défini, des facilités d'accès et de la configuration des locaux et des espaces extérieurs, pondérées à hauteur de 20 points, en troisième lieu, de la diversité et de la qualité des moyens matériels, y compris ludo-éducatifs, dédiés à la réalisation de la prestation, pondérées à hauteur de 10 points, et en quatrième lieu, des modalités de communication avec le centre communal d'action sociale, pondérées à hauteur de 5 points. Le rapport d'analyse des offres révèle en outre que, pour évaluer le sous critère relatif à la qualité du projet d'établissement, le pouvoir adjudicateur a distingué deux éléments d'appréciation, pondérés chacun à hauteur de 12,5 points, portant sur les moyens humains mis en oeuvre et sur le projet d'établissement lui-même.<br/>
<br/>
              5. En premier lieu, il ressort du rapport d'analyse des offres soumis aux juges du fond que, pour évaluer le projet d'établissement lui-même, élément d'appréciation du sous critère relatif à la qualité du projet d'établissement, le pouvoir adjudicateur a pris en compte deux éléments d'appréciation, concernant respectivement les conditions d'organisation de la structure, dont le nombre de places totales, les horaires d'ouvertures et les fermetures annuelles, et les " projets complémentaires ". Par ailleurs, s'agissant des " projets complémentaires ", quatre éléments d'appréciation ont été pris en compte, dont le " budget alimentation annuel ".<br/>
<br/>
              6. Le montant du budget consacré à l'alimentation des enfants par les candidats à un marché portant sur la réservation de places en crèche étant un élément qui permet d'apprécier, parmi d'autres, la qualité des repas servis aux enfants accueillis dans la crèche, il n'est pas sans lien avec le sous-critère de la qualité du projet d'établissement du critère de la valeur technique des offres. La circonstance que le montant du budget consacré à l'alimentation puisse également servir à l'appréciation du critère financier tiré du " prix unitaire à la place " de crèche ne fait pas obstacle à ce qu'il puisse également être pris en compte pour apprécier la valeur technique des offres dès lors que le prix des repas n'est que l'un des éléments déterminant tant le budget consacré à l'alimentation que le prix unitaire à la place de crèche. Ainsi, la cour administrative d'appel de Nantes a pu, sans commettre d'erreur de droit ni dénaturer les pièces du dossier, estimer que la prise en compte du montant du budget consacré à l'alimentation, qui n'était pas sans lien avec l'objet du marché et n'a constitué qu'un élément d'appréciation parmi d'autres de la qualité de l'alimentation proposée aux enfants, même s'il ne suffisait pas à refléter, à lui seul, la qualité diététique et gustative des repas, n'affectait pas la régularité de la méthode de notation.<br/>
<br/>
              7. En second lieu, il ressort du rapport d'analyse des offres soumis aux juges du fond que, pour apprécier les moyens humains proposés par les candidats, autre élément d'appréciation du sous critère relatif à la qualité du projet d'établissement, le centre communal d'action sociale de Saint-Malo a non seulement tenu compte des effectifs envisagés en équivalent temps plein, mais aussi du niveau de qualification des personnels et de leur expérience, du nombre d'heures de formation qui leur seraient dispensées, des modalités de leur remplacement en cas d'absence ponctuelle, de la fréquence des interventions d'un médecin et d'un psychologue, et du budget représentant la masse salariale brute, pour estimer que la société La Maison Bleue proposait " plus d'heures de formations et d'interventions de spécialistes médicaux que ses concurrents ", offrait " un mode de remplacement " plus " efficace ", et " un budget de masse salariale supérieur à celui d'Evancia (Babilou) ", ce qui justifiait de lui accorder des points supplémentaires.<br/>
<br/>
              8. La masse salariale brute des candidats à un marché portant sur la réservation de places en crèche témoigne notamment des salaires versés aux employés affectés à la mission et peut traduire leur niveau de qualification, ainsi que la mise en oeuvre d'une politique salariale liée à la qualité du travail. En outre, elle n'est pas sans lien avec les actions mises en oeuvre pour pallier les absences ponctuelles des employés et assurer la continuité du service proposé par la crèche. Ainsi, la masse salariale brute des candidats qui est un élément qui permet d'apprécier, parmi d'autres, les moyens humains qu'ils proposent de mettre en oeuvre et la qualité de leur projet d'établissement, n'est pas dépourvu de tout lien avec le critère de la valeur technique des offres. Par suite, la cour administrative d'appel de Nantes a pu estimer, sans commettre d'erreur de droit ni dénaturer les pièces du dossier, que la prise en compte de la masse salariale brute pour apprécier le sous-critère relatif à la qualité du projet d'établissement qui permettait lui-même d'évaluer le critère relatif à la valeur technique des offres n'affectait pas la régularité de la méthode de notation.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de la société Evancia doit être rejeté.<br/>
<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Evancia la somme de 3 000 euros à verser au centre communal d'action sociale de Saint-Malo, au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre communal d'action sociale de Saint-Malo qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Evancia est rejeté.<br/>
Article 2 : La société Evancia versera au centre communal d'action sociale de Saint-Malo une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Evancia et au centre communal d'action sociale (CCAS) de Saint-Malo.<br/>
Copie en sera adressée à la société La Maison Bleue.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION - RÉGULARITÉ - 1) PRINCIPES [RJ1] - APPLICATION AUX ÉLÉMENTS D'APPRÉCIATION PRIS EN COMPTE POUR NOTER LES CRITÈRES - EXISTENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) Le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics. Il peut ainsi déterminer tant les éléments d'appréciation pris en compte pour l'élaboration de la note des critères que les modalités de détermination de cette note par combinaison de ces éléments d'appréciation. Une méthode de notation est toutefois entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, les éléments d'appréciation pris en compte pour noter les critères de sélection des offres sont dépourvus de tout lien avec les critères dont ils permettent l'évaluation ou si les modalités de détermination de la note des critères de sélection par combinaison de ces éléments sont, par elles-mêmes, de nature à priver de leur portée ces critères ou à neutraliser leur pondération et sont, de ce fait, susceptibles de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie. Il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation.,,,2) Consultation engagée en vue de la passation d'un marché relatif à la réservation de places en crèche.,,,Sous-critère portant sur la qualité du projet d'établissement reposant sur plusieurs éléments d'appréciation, dont notamment le budget annuel consacré à l'alimentation.,,,Le montant du budget consacré à l'alimentation des enfants étant un élément qui permet d'apprécier, parmi d'autres, la qualité des repas, il n'est pas sans lien avec le sous-critère de la qualité du projet d'établissement du critère de la valeur technique des offres. La circonstance que le montant du budget consacré à l'alimentation puisse également servir à l'appréciation du critère financier tiré du prix unitaire à la place de crèche ne fait pas obstacle à ce qu'il puisse également être pris en compte pour apprécier la valeur technique des offres dès lors que le prix des repas n'est que l'un des éléments déterminant tant le budget consacré à l'alimentation que le prix unitaire à la place de crèche.... ,,Ainsi, la prise en compte du montant du budget consacré à l'alimentation, qui n'était pas sans lien avec l'objet du marché et n'a constitué qu'un élément d'appréciation parmi d'autres de la qualité de l'alimentation proposée aux enfants, même s'il ne suffisait pas à refléter, à lui seul, la qualité diététique et gustative des repas, n'affectait pas la régularité de la méthode de notation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 novembre 2014, Commune de Belleville-sur-Loire, n° 373362, p. 323.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
