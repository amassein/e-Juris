<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687494</ID>
<ANCIEN_ID>JG_L_2012_11_000000351163</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/74/CETATEXT000026687494.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 26/11/2012, 351163</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351163</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:351163.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 22 juillet et 18 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés par le président de l'Autorité de régulation des jeux en ligne, dont le siège est 99-101, rue Leblanc à Paris (75015) ; le président de l'Autorité de régulation des jeux en ligne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 2010/03 du 6 juin 2011 par laquelle la commission des sanctions de l'Autorité de régulation des jeux en Ligne, statuant sur la procédure de sanction engagée le 23 septembre 2010 à l'encontre de la société Betclic Enterprises Limited, a considéré que les griefs formulés ne pouvaient donner lieu au prononcé d'une sanction ; <br/>
<br/>
              2°) de prononcer une sanction à l'encontre de la société Betclic Enterprises Limited pour les griefs présentés devant la commission des sanctions ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2010-476 du 12 mai 2010 ; <br/>
<br/>
              Vu le décret n° 2010-509 du 18 mai 2010 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société Betclic Entreprises Limited,<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société Betclic Entreprises Limited ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que, le 7 juin 2010, l'Autorité de régulation des jeux en ligne (ARJEL) a délivré un agrément à la société Betclic Enterprises Limited pour l'exploitation de jeux d'argent et de hasard en ligne ; qu'à la suite d'un contrôle ponctuel réalisé par un enquêteur de sa direction des enquêtes et des contrôles, l'ARJEL a constaté que, contrairement aux prescriptions du dossier des exigences techniques établi par elle, cette société ne procédait pas à l'archivage en temps réel des données relatives à l'ouverture des comptes des joueurs et à l'acceptation des conditions générales de vente ; qu'après avoir mis en demeure la société Betclic Enterprises Limited de se mettre en conformité avec ses obligations, le collège de l'ARJEL a décidé le 3 décembre 2010 d'ouvrir une procédure de sanction à son encontre ; que, par une décision du 6 juin 2011, la commission des sanctions de l'ARJEL a refusé de prononcer une sanction à l'encontre de cette société au motif que les obligations qu'il lui était reproché d'avoir méconnues ne résultaient pas de dispositions législatives ou réglementaires applicables à son activité ; que, sur le fondement de l'article 44 de la loi du 12 mai 2010, en vertu duquel il peut, après accord du collège de l'Autorité, former un recours de pleine juridiction à l'encontre des décisions de la commission des sanctions, le président de l'ARJEL demande l'annulation de cette décision ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article 43 de la loi du 12 mai 2010 relative à l'ouverture à la concurrence et à la régulation du secteur des jeux d'argent et de hasard en ligne que la commission des sanctions de l'ARJEL peut prononcer une sanction à l'encontre d'un opérateur de jeux ou de paris en ligne agréé en cas de manquement aux obligations législatives et réglementaires applicables à son activité ; <br/>
<br/>
              3. Considérant qu'il est constant que pour un certain nombre de comptes de joueurs, des mouvements financiers ont été réalisés sans que l'ouverture de ces comptes ait été enregistrée sur le support matériel d'archivage de la société Betclic Enterprises Limited ; que, de même, les données relatives à la prise de connaissance et à l'acceptation par certains joueurs des conditions générales de vente n'ont pas été enregistrées sur ce support matériel d'archivage ;  <br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes des dispositions de l'article 38 de la loi du 12 mai 2010 : " Un contrôle permanent de l'activité des opérateurs de jeux ou de paris en ligne agréés est réalisé par l'Autorité de régulation des jeux en ligne aux fins d'assurer le respect des objectifs définis à l'article 3. A cette fin, les opérateurs mettent à la disposition permanente de l'Autorité de régulation des jeux en ligne des données portant sur : 1° L'identité de chaque joueur, son adresse et son adresse sur un service de communication au public en ligne ; 2° Le compte de chaque joueur, notamment sa date d'ouverture, et les références du compte de paiement mentionné au dernier alinéa de l'article 17 ; 3° Les événements de jeu ou de pari et, pour chaque joueur, les opérations associées ainsi que toute autre donnée concourant à la formation du solde du compte joueur ; 4° Les événements relatifs à l'évolution et à la maintenance des matériels, plates-formes et logiciels de jeux utilisés " ; qu'aux termes de l'article 31 de la même loi : " L'opérateur de jeux ou de paris en ligne titulaire de l'agrément prévu à l'article 21 est tenu de procéder à l'archivage en temps réel, sur un support matériel situé en France métropolitaine, de l'intégralité des données mentionnées au 3° de l'article 38. L'ensemble des données échangées entre le joueur et l'opérateur transitent par ce support " ; que les données relatives à l'ouverture du compte du joueur et à l'acceptation des conditions générales de vente sont au nombre des données mentionnées au 2° de l'article 38 de la loi du 12 mai 2010  et non au 3° du même article ; que, par suite, il ne résulte pas des dispositions législatives en vigueur que les opérateurs de jeux et de paris en ligne soient tenus de procéder à l'archivage de ces données en temps réel, sur un support matériel situé en France métropolitaine ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes du dernier alinéa de l'article 38 de la loi du 12 mai 2010 : " Un décret en Conseil d'Etat, pris après avis de la Commission nationale de l'informatique et des libertés, précise la liste des données que les opérateurs de jeux ou de paris en ligne sont tenus de mettre à la disposition de l'Autorité de régulation des jeux en ligne. Il précise les modalités techniques de stockage et de transmission de ces données, le délai pendant lequel l'opérateur est tenu de les archiver, ainsi que les modalités des contrôles réalisés par l'Autorité de régulation des jeux en ligne à partir de ces données " ; que ces dispositions n'ont pas pour objet et ne sauraient avoir pour effet d'autoriser le pouvoir réglementaire à étendre le champ d'application de l'obligation d'archivage prévue par l'article 31 de la loi ; que par suite, le président de l'ARJEL ne peut utilement soutenir que l'obligation d'archivage en temps réel des données relatives à l'ouverture des comptes des joueurs et à l'acceptation des conditions générales de vente résulterait du décret du 18 mai 2010 pris en application du dernier alinéa de l'article 38 de la loi du 12 mai 2010 ainsi que du dossier des exigences techniques adopté par le collège de l'Autorité sur le fondement de ce décret ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que l'obligation d'archivage en temps réel des données relatives à l'ouverture des comptes et à l'acceptation des conditions générales de vente ne découle pas de dispositions législatives ou réglementaires au sens de l'article 43 de la loi du 12 mai 2010 ; que, par suite, le président de l'ARJEL n'est pas fondé à soutenir que la commission des sanctions aurait estimé à tort qu'elle ne pouvait légalement prononcer de sanction à l'encontre de la société Betclic Enterprises Limited en raison de la méconnaissance de cette obligation ; que, dès lors, sa requête doit être rejetée ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête du président de l'Autorité de régulation des jeux en ligne est rejetée. <br/>
<br/>
Article 2 : L'Etat versera à la société Betclic Enterprises Limited une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Autorité de régulation des jeux en ligne, à la société Betclic Entreprises Limited et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - ARJEL - PRÉSIDENT FORMANT UN RECOURS DE PLEINE JURIDICTION À L'ENCONTRE DES DÉCISIONS DE LA COMMISSION DES SANCTIONS - ACTION AU NOM DE L'ETAT - CONSÉQUENCE - FRAIS AU TITRE DE L'ARTICLE L. 761-1 DU CODE DE JUSTICE ADMINISTRATIVE À LA CHARGE DE L'ETAT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63 SPORTS ET JEUX. - ARJEL - PRÉSIDENT FORMANT UN RECOURS DE PLEINE JURIDICTION À L'ENCONTRE DES DÉCISIONS DE LA COMMISSION DES SANCTIONS - ACTION AU NOM DE L'ETAT - CONSÉQUENCE - FRAIS AU TITRE DE L'ARTICLE L. 761-1 DU CJA À LA CHARGE DE L'ETAT.
</SCT>
<ANA ID="9A"> 54-06-05-11 Lorsque, sur le fondement de l'article 44 de la loi n° 2010-476 du 12 mai 2010, le président de l'Autorité de régulation des jeux en ligne (ARJEL) forme, après accord du collège de l'Autorité, un recours de pleine juridiction à l'encontre des décisions de la commission des sanctions, il agit au nom de l'Etat, à la charge duquel doivent, le cas échéant, être mis d'éventuels frais exposés en défense et non compris dans les dépens.</ANA>
<ANA ID="9B"> 63 Lorsque, sur le fondement de l'article 44 de la loi n° 2010-476 du 12 mai 2010, le président de l'Autorité de régulation des jeux en ligne (ARJEL) forme, après accord du collège de l'Autorité, un recours de pleine juridiction à l'encontre des décisions de la commission des sanctions, il agit au nom de l'Etat, à la charge duquel doivent, le cas échéant, être mis d'éventuels frais exposés en défense et non compris dans les dépens.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
