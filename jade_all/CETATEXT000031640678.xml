<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640678</ID>
<ANCIEN_ID>JG_L_2015_12_000000369248</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/06/CETATEXT000031640678.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/12/2015, 369248, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369248</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:369248.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi que des pénalités correspondantes, auxquelles ils ont été assujettis au titre des années 1999, 2000 et 2001.<br/>
<br/>
              Par un jugement n° 0813784 du 11 février 2011, le tribunal a réduit, au prorata de leurs droits dans la société civile immobilière Espace, leurs bases d'imposition à concurrence d'un montant de 33 874,12 francs, correspondant à des travaux de remise en état de façades sur un immeuble situé à Plérin, et a réduit, au prorata de leurs droits dans la société civile immobilière Tyann, leurs bases d'imposition à concurrence d'un montant de 153 161,87 francs, correspondant à des travaux d'enduits, de lavage et de rejointoyage sur un immeuble situé à Tréguier, puis les a déchargés des droits et pénalités correspondants, et a, enfin, rejeté le surplus de leur demande.<br/>
<br/>
              Par un arrêt n° 11PA01789 du 11 avril 2013, la cour administrative d'appel de Paris, sur l'appel de M. et Mme A...et sur l'appel incident du ministre de l'économie et des finances, en premier lieu a annulé le jugement en tant qu'il rejetait le surplus de la demande des épouxA..., en deuxième lieu a rejeté le surplus de cette demande après évocation, en troisième lieu a réformé le jugement en tant qu'il avait accordé à M. et Mme A...une réduction de leurs bases d'imposition correspondant aux travaux de remise en état de façades sur un immeuble situé à Plérin et qu'il avait prononcé la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi que des pénalités correspondantes et, enfin, a rejeté le surplus de la requête d'appel de M. et MmeA....<br/>
<br/>
Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 11 juin et 11 septembre 2013 et le 13 novembre 2015 au secrétariat du contentieux du Conseil d'État, M. et Mme B... A...demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit au surplus de leur demande devant le tribunal administratif de Paris ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... A...étaient associés de trois sociétés civiles immobilières " Immoglicyne ", " Trotrieux " et " St Yves ", qui ont fait l'objet de contrôles à l'issue desquels l'administration a remis en cause les déficits qu'elles avaient déclarés pour les années 1993 à 1995, ainsi que, par voie de conséquence, l'existence du déficit reportable de 3 080 329 francs que M. et Mme A... avaient déclaré au titre de l'année 1995. M. et Mme A...étaient, par ailleurs, associés des sociétés civiles immobilières " Espace " et " Tyann ", qui ont fait l'objet de contrôles sur pièces à l'issue desquels l'administration a adressé à ces sociétés au total trois notifications de redressement, se traduisant par la remise en cause des déficits déclarés par la société Espace pour les années 1996 et 1997 et par la société Tyann pour les années 1997 à 2000. Par une notification de redressement en date du 20 décembre 2002, l'administration fiscale a tiré les conséquences de ces différents contrôles sur les revenus déclarés par M. et Mme A...au titre des années 1999, 2000 et 2001 et les a, en conséquence, assujettis à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi qu'aux pénalités correspondantes. Par l'article 1er d'un jugement du 11 février 2011, le tribunal administratif de Paris a, d'une part, réduit, au prorata de leurs droits dans la société civile immobilière " Espace ", les bases d'imposition de M. et Mme A... à concurrence d'un montant de 33 874,12 francs, correspondant à des travaux de remise en état de façades sur un immeuble à Plérin, d'autre part, réduit, au prorata de leurs droits dans la société civile immobilière " Tyann ", leurs bases d'imposition à concurrence d'un montant de 153 161,87 francs, correspondant à des travaux d'enduits, de lavage et de rejointoyage sur un immeuble situé à Tréguier. Par l'article 2 de ce jugement, le tribunal a déchargé M. et Mme A...des droits et pénalités correspondant aux bases d'imposition visées à son article 1er et a, enfin, par l'article 3 du même jugement, rejeté le surplus de leur demande. M. et Mme A...se pourvoient en cassation contre l'arrêt du 11 avril 2013 par lequel la cour administrative d'appel de Paris a, après avoir annulé l'article 3 du jugement du tribunal administratif de Paris, rejeté le surplus de leur demande devant ce tribunal ainsi que le surplus de leur requête d'appel et, sur appel incident du ministre, réformé les articles 1er et 2 de ce jugement en tant qu'ils ont réduit, au prorata de leurs droits dans la société Espace, de 33 874,12 francs leurs bases d'imposition et les ont déchargés des cotisations d'impôt et pénalités correspondantes.<br/>
<br/>
              Sur les droits et pénalités afférents à la remise en cause du caractère reportable du déficit de 3 080 329 francs et sur les pénalités correspondantes :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. / (...) " ; Aux termes de l'article R. 57-1 du même livre : " La proposition de rectification prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs de la rectification envisagée. / (...) ". Il résulte de ces dispositions que l'administration doit indiquer au contribuable, dans la proposition de rectification, les motifs et le montant des rehaussements envisagés, leur fondement légal et la catégorie de revenus dans laquelle ils sont opérés, ainsi que les années d'imposition concernées. Hormis le cas où elle se réfère à un document qu'elle joint à la proposition de rectification ou à la réponse aux observations du contribuable, l'administration peut satisfaire à cette obligation en se bornant à se référer aux motifs retenus dans une proposition de rectification, ou une réponse à ses observations, consécutive à un précédent contrôle et qui a été régulièrement notifiée au contribuable, à la condition qu'elle identifie précisément la proposition ou la réponse en cause et que celle-ci soit elle-même suffisamment motivée.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que, pour rejeter le moyen tiré de ce que la notification de redressement du 20 décembre 2002 aurait été insuffisamment motivée en tant qu'elle remettait en cause le caractère reportable du déficit de 3 080 329 francs issu des années d'imposition précédant l'année 1999, la cour a tout d'abord relevé que ce document se référait à une précédente notification de redressement en date du 27 décembre 1999, laquelle faisait, elle-même, référence aux suites d'un précédent contrôle, puis a déduit de ce que M. et Mme A...ne contestaient pas la régularité de ce précédent contrôle et, donc, la motivation de la notification de redressement qui l'avait accompagné, que la notification de redressement du 20 décembre 2002 était suffisamment motivée. Il résulte de ce qui est dit au point 2 qu'en statuant ainsi la cour n'a pas méconnu les dispositions précitées des articles L. 57 et R. 57-1 du livre des procédures fiscales. <br/>
<br/>
              4. En deuxième lieu, en se fondant, pour juger que l'administration avait, à propos du report du déficit de 3 080 329 francs, établi la mauvaise foi de M. et Mme A..., sur le fait que ces derniers avaient persisté à déclarer des déficits qui avaient été auparavant remis en cause par l'administration, la cour n'a pas dénaturé les faits qui lui étaient soumis.<br/>
<br/>
              Sur les dépenses des sociétés Espace et Tyann dont la déductibilité a été refusée par l'administration :<br/>
<br/>
              En ce qui concerne les dépenses d'un montant de 33 874,12 francs, correspondant à des travaux de remise en état de façades sur un immeuble situé à Plérin :<br/>
<br/>
              5. Il ressort tant des motifs que de l'article 3 du dispositif de l'arrêt attaqué que la cour, statuant sur appel incident du ministre, a remis en cause la décharge, accordée par le tribunal administratif de Paris, des droits supplémentaires et des pénalités résultant de la réintégration des dépenses d'un montant de 33 874,12 francs concernant des travaux de remise en état de façades sur un immeuble situé à Plérin. Elle n'a en rien, contrairement à ce qui est soutenu par M. et MmeA..., remis en cause la décharge, accordée par ce même tribunal, des droits et des pénalités résultant de la réintégration des dépenses d'un montant de 153 161,87 francs, concernant des travaux d'enduits, de lavage et de rejointoyage sur un immeuble situé à Tréguier, que l'administration fiscale a au demeurant dégrevés le 16 novembre 2011. Par suite, le moyen tiré de ce qu'elle aurait commis une erreur de droit sur ce point ne peut qu'être écarté.<br/>
<br/>
              En ce qui concerne les autres dépenses relatives à des travaux des sociétés Espace et Tyann en litige devant la cour :<br/>
<br/>
              6. Aux termes de l'article 31 du code général des impôts : " I - Les charges de la propriété déductibles pour la détermination du revenu net comprennent : 1° pour les propriétés urbaines : a) les dépenses de réparation et d'entretien (...) b) les dépenses d'amélioration afférentes aux locaux d'habitation, à l'exclusion des frais correspondant à des travaux de construction, de reconstruction ou d'agrandissement (...) ". Doivent être regardés comme des travaux de construction ou de reconstruction, au sens des dispositions précitées, les travaux qui comportent la création de nouveaux locaux d'habitation, notamment dans les locaux auparavant affectés à un autre usage, ainsi que les travaux ayant pour effet d'apporter une modification importante au gros oeuvre de locaux d'habitation existants ou les travaux d'aménagement interne qui, par leur importance, équivalent à une reconstruction. Doivent être regardés comme des travaux d'agrandissement, au sens des mêmes dispositions, les travaux ayant pour effet d'accroître le volume ou la surface habitable de locaux existants.<br/>
<br/>
              7. Pour déterminer la nature déductible ou non des travaux réalisés par les sociétés civiles Espace et Tyann, il revenait à la cour de faire la distinction entre les travaux d'amélioration et les travaux de construction, reconstruction, ou agrandissement et de statuer sur le caractère dissociable ou non dissociable de l'ensemble des travaux en litige. M. et Mme A... faisaient valoir dans leurs écritures d'appel, notamment dans leur mémoire en réplique aux pages 10 et suivantes, tout en produisant les factures correspondantes que, d'une part, pour l'immeuble situé à Plérin, des travaux extérieurs de réfection de la toiture, de changement de fenêtres, de réfection de la façade, d'autre part, pour l'immeuble situé à Tréguier, des travaux concernant la réfection de la toiture, des châssis de fenêtre, des double vitrages et des joints des murs extérieurs devaient être regardés comme des travaux d'amélioration, de réparation et d'entretien. Or, il ressort des énonciations de l'arrêt attaqué que, si elle a fait état des travaux d'aménagement interne des deux immeubles, la cour n'a pas recherché si les travaux extérieurs en étaient dissociables et si les dépenses les concernant constituaient des charges déductibles des revenus fonciers des exposants. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi à ce propos, son arrêt doit être annulé en tant qu'il statue sur l'ensemble des dépenses de travaux des sociétés Espace et Tyann et sur les redressements correspondants, en droits et pénalités, de M. et MmeA..., à l'exception des dépenses visées à l'article 3 de son arrêt. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État une somme de 1 500 euros à verser à M. et Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Paris du 11 avril 2013 est annulé en tant qu'il statue sur l'ensemble des dépenses de travaux des sociétés Espace et Tyann et sur les redressements correspondants, en droits et pénalités, de M. et Mme A..., à l'exception des dépenses visées à l'article 3 de cet arrêt.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : L'État versera à M. et Mme A...une somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme B...A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
