<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928859</ID>
<ANCIEN_ID>JG_L_2016_07_000000391440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928859.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 22/07/2016, 391440, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391440.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 1er juillet 2015 et 8 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 6 mai 2015 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) l'a informé de la clôture de sa plainte relative à la date de son inscription comme demandeur d'emploi dans les fichiers de l'agence de Pôle Emploi Bourgogne ; <br/>
<br/>
              2°) d'ordonner, sous astreinte de 1 000 euros par jour de retard, à Pôle Emploi de rectifier son dossier numérique en retenant la date d'inscription du 14 février 2001 ; <br/>
<br/>
              3°) de mettre à la charge de la CNIL et de Pôle Emploi des frais irrépétibles.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : (...) 4° Elles sont exactes, complètes et, si nécessaire, mises à jour ; les mesures appropriées doivent être prises pour que les données inexactes ou incomplètes au regard des finalités pour lesquelles elles sont collectées ou traitées soient effacées ou rectifiées ". Et aux termes de l'article 40 de la même loi : " Toute personne physique justifiant de son identité peut exiger du responsable d'un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données à caractère personnel la concernant, qui sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite. /  Lorsque l'intéressé en fait la demande, le responsable du traitement doit justifier, sans frais pour le demandeur, qu'il a procédé aux opérations exigées en vertu de l'alinéa précédent ".<br/>
<br/>
              2. Il ressort des pièces du dossier que M. B...a saisi le 19 novembre 2014 la Commission nationale de l'informatique et des libertés d'une plainte à l'encontre de Pôle Emploi. Faisant usage de son droit de rectification, il exigeait d'être reconnu comme demandeur d'emploi à compter du 14 février 2001 et non du 28 août 2010. Saisie par la Commission les 18 décembre 2014 et 27 janvier 2015, la direction de Pôle Emploi Bourgogne lui a répondu le 26 février 2015, en produisant l'extrait de son dossier informatique, que ce dernier mentionnait comme première date d'inscription le 15 mai 2001. <br/>
<br/>
              3. M. B...demande toutefois, au titre de son droit à rectification, ainsi qu'il l'a confirmé à la Commission nationale de l'informatique et des libertés par courrier du 11 mars 2015, que sa première inscription comme demandeur d'emploi soit enregistrée à la date du 14 février 2001. Il ressort des pièces du dossier qu'après avoir constaté que Pôle Emploi établissait que le 15 mai 2001 correspondait à la date exacte de la première inscription du requérant, la CNIL a relevé que celui-ci ne produisait à l'appui de ses allégations qu'une attestation fournie par l'Assedic Franche-Comté-Bourgogne, de laquelle il résulte que son indemnisation avait débuté le 15 février 2002 seulement. Dans ces conditions, en décidant que l'instruction de la plainte de M. B...ne pouvait plus utilement être poursuivie et qu'il y avait dès lors lieu de procéder à la clôture du dossier, la CNIL, qui a suffisamment motivé sa décision, ne l'a entachée ni d'erreur de droit ni d'erreur manifeste d'appréciation.<br/>
<br/>
              4. Si M. B...soutient en outre que la CNIL a méconnu, d'une part, les stipulations des articles 21 et 47 de la Charte des droits fondamentaux de l'Union européenne qui garantissent le principe de non-discrimination et le principe de bonne administration et, d'autre part, celles des articles 3, 4 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui prohibent les traitements inhumains ou dégradants et le travail forcé et garantissent le respect de la vie privée, il n'assortit ce moyen d'aucune précision permettant d'apprécier le bien-fondé de ses allégations. <br/>
<br/>
              5. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision du 6 mai 2015 par laquelle la Commission nationale de l'informatique et des libertés l'a informé de la clôture de sa plainte. Sa requête doit donc être rejetée, y compris ses conclusions tendant à ce qu'il soit enjoint à Pôle Emploi, sous astreinte de 1 000 euros par jour de retard, de rectifier son dossier numérique et, en tout état de cause, celles tendant à l'octroi d'une somme au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er: La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
