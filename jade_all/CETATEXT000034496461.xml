<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034496461</ID>
<ANCIEN_ID>JG_L_2017_04_000000404888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/49/64/CETATEXT000034496461.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 26/04/2017, 404888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404888.20170426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 7 février et 2 mars 2017, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la Chambre syndicale française de la levure demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la circulaire du ministre des finances et des comptes publics du 11 mai 2016 relative à la taxe intérieure sur la consommation finale d'électricité ainsi que du refus implicite opposé par ce ministre à la demande de retrait de cette circulaire qu'elle avait présentée le 6 juillet 2016, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du dernier alinéa du d du C du 8 de l'article 266 quinquies C du code des douanes, dans sa rédaction issue de la loi du 29 décembre 2015 de finances rectificative pour 2015. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - la directive 2003/87/CE du Parlement européen et du Conseil du 13 octobre 2003 ;<br/>
              - la directive 2003/96/CE du Conseil du 27 octobre 2003 ;<br/>
              - la 2008/118/CE du Conseil du 16 décembre 2008 ;<br/>
              - la décision de la Commission européenne 2014/746/UE du 27 octobre 2014 ;<br/>
              - le code des douanes ;<br/>
              - la loi n° 2015-1786 du 29 décembre 2015 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat du ministre de l'économie et des finances ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte de ces dispositions que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. La loi du 29 décembre 2015 de finances rectificative pour 2015 a modifié les règles relatives à la taxe intérieure sur la consommation finale d'électricité prévue à l'article 266 quinquies C du code des douanes, qu'elle a renommée " contribution au service public de l'électricité ". Elle a notamment prévu, au C du 8 de cet article, l'application de plusieurs tarifs réduits, dérogeant au tarif de droit commun fixé à 22,5 euros par mégawattheure d'électricité fournie ou consommée, au bénéfice des personnes qui exploitent des installations industrielles électro-intensives, avec l'application d'une réduction supplémentaire lorsqu'elles sont exposées à un risque important de " fuite de carbone " en raison de l'impact, sur le prix de l'électricité, du mécanisme des quotas d'émission de gaz à effet de serre, au bénéfice de celles qui exploitent des installations dites hyperélectro-intensives et, enfin, au bénéfice de celles qui exercent une activité de transport de personnes et de marchandises par train, métro, tramway, câble et trolleybus. Le ministre des finances et des comptes publics a commenté ces nouvelles dispositions par une circulaire du 11 mai 2016. A l'appui de son recours tendant à l'annulation pour excès de pouvoir de cette circulaire, et plus particulièrement de ses conclusions dirigées contre son paragraphe 99, la société conteste la conformité des dispositions du d du C de l'article 266 quinquies C du code des douanes aux droits et libertés garantis par la Constitution. <br/>
<br/>
              3. Le a du C du 8 de l'article 266 quinquies C du code des douanes, dans sa rédaction issue de la loi du 29 décembre 2015, prévoit l'application de tarifs réduits de contribution au service public de l'électricité pour " les personnes qui exploitent des installations industrielles électro-intensives au sens où, au niveau de l'entreprise ou de ses sites, le montant de la taxe qui aurait été due en application du B, sans application des exonérations et exemptions, est au moins égal à 0,5 % de la valeur ajoutée ". Le d du C du 8 de cet article prévoit, en outre, l'application d'une réduction supplémentaire pour les installations mentionnées au a " qui sont exposées à un risque important de fuite de carbone en raison des coûts des émissions indirectes ". Il précise en son dernier alinéa qu' " est considérée comme exposée à un risque important de fuite de carbone en raison des coûts des émissions indirectes une installation dont l'activité relève de l'un des secteurs ou sous-secteurs mentionnés à l'annexe II de la communication 2012/ C 158/04 de la Commission relative aux lignes directrices concernant certaines aides d'Etat dans le contexte du système d'échange de quotas d'émission de gaz à effet de serre après 2012 ". <br/>
<br/>
              4. Par les dispositions contestées du d du C du 8 de l'article 266 quinquies C du code des douanes, le législateur, créant une nouvelle réduction de tarif de contribution au service public de l'électricité, dans les conditions prévues par les directives 2003/96/CE du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité et 2008/118/CE du Conseil du 16 décembre 2008 relative au régime général d'accise, a choisi de se référer, pour définir le champ d'application de cette réduction, aux lignes directrices, publiées au Journal officiel des Communautés européennes le 5 juin 2012, adoptées par la Commission européenne afin de préciser les secteurs et sous-secteurs en faveur desquels les Etats membres sont, en application du paragraphe 6 de l'article 10 bis de la directive 2003/87/CE du Parlement européen et du Conseil du 13 octobre 2003 établissant un système d'échange de quotas d'émission de gaz à effet de serre dans la Communauté, autorisés à prendre des mesures financières afin de compenser les coûts liés aux émissions de gaz à effet de serre répercutés sur les prix de l'électricité. Pour établir, à l'annexe II de ces lignes directrices, la liste de ces secteurs et sous-secteurs considérés comme exposés à un risque important de " fuite de carbone ", c'est-à-dire de délocalisation de leur production en dehors du territoire de l'Union européenne, la Commission européenne a regardé comme exposés à un risque de " fuite de carbone ", ainsi qu'il ressort de la note explicative figurant sous l'annexe II de ces lignes directrices, les secteurs et sous-secteurs dont l'intensité des échanges avec les pays tiers est supérieure à 10 % et qui subissent, du fait de la hausse du prix de l'électricité induite par les quotas d'émission de gaz à effet de serre, une augmentation de leurs coûts de production d'au moins 5 %, ce dernier chiffre pouvant, dans certains cas, être abaissé à 2,5 %.<br/>
<br/>
              5. En premier lieu, la requérante conteste les modalités d'appréciation de l'exposition à un risque important de " fuite de carbone " retenues par le dernier alinéa du d du C du 8 de l'article 266 quinquies C du code des douanes. Elle fait valoir que ces modalités, définies par référence à l'annexe II des lignes directrices mentionnées au point 4 ci-dessus, conduisent à exclure certaines productions, comme celle de levures, pourtant fortement exposées à la concurrence internationale et remplissant ainsi le critère d'intensité des échanges avec les pays tiers, mais ne satisfaisant pas au second critère, cumulatif, lié aux coûts supplémentaires induits par la mise en oeuvre du système européen d'échange de quotas d'émission de gaz à effet de serre et son impact sur le prix de l'électricité. Elle soutient que cette exclusion méconnaît les principes d'égalité devant la loi et d'égalité devant les charges publiques et que le législateur aurait dû, au regard de l'objectif recherché, faire bénéficier de la réduction de tarif en cause les productions qui, sans supporter de tels coûts, sont fortement exposées à la concurrence internationale.<br/>
<br/>
              6. La requérante fait, en particulier, valoir que le paragraphe 16 de l'article 10 bis de la directive du 13 octobre 2003 prévoit d'autres critères qui permettent de considérer un secteur ou sous-secteur comme exposé à un risque de " fuite de carbone ", en disposant que : " Nonobstant le paragraphe 15, un secteur ou sous-secteur est également considéré comme exposé à un risque important de fuite de carbone si : / a) la somme des coûts supplémentaires directs et indirects induits par la mise en oeuvre de la présente directive entraînerait une augmentation particulièrement forte des coûts de production, calculée en proportion de la valeur ajoutée brute, d'au moins 30 %; ou / b) l'intensité des échanges avec des pays tiers (...) est supérieure à 30 %. ". Elle indique qu'au regard du critère alternatif prévu au b) de ce paragraphe, la production de levures doit être considérée comme exposée au risque de " fuite de carbone ", ainsi que le confirme la décision de la Commission européenne 2014/746/UE du 27 octobre 2014 établissant, conformément à l'article 10 bis de la directive du 13 octobre 2003, la liste des secteurs et sous-secteurs considérés comme exposés à un risque important de " fuite de carbone " pour la période 2015-2019, qui mentionne les " levures de panification ". Elle fait enfin valoir que, pour la détermination des secteurs éligibles au tarif réduit bénéficiant aux installations hyperélectro-intensives, le législateur a d'ailleurs, au b du C du 8 de l'article 266 quinquies C du code des douanes, fait référence à cette décision, en prévoyant qu' " Est considérée comme hyperélectro-intensive une installation qui vérifie les deux conditions suivantes : / -sa consommation d'électricité représente plus de 6 kilowattheures par euro de valeur ajoutée ; / -son activité appartient à un secteur dont l'intensité des échanges avec des pays tiers, telle que déterminée par la Commission européenne aux fins de l'article 10 bis de la directive 2003/87/ CE du Parlement européen et du Conseil du 13 octobre 2003 établissant un système d'échange de quotas d'émission de gaz à effet de serre dans la Communauté et modifiant la directive 96/61/ CE du Conseil, est supérieure à 25 %. ".<br/>
<br/>
              7. Toutefois, le critère retenu par le législateur pour définir le champ d'application de la réduction bénéficiant aux installations industrielles électro-intensives exposées à un risque important de " fuite de carbone ", par référence à la liste établie par les lignes directrices de la Commission européenne mentionnées ci-dessus, et non à celle qu'établit sa décision du 27 octobre 2014, qui n'a au demeurant pas le même objet, a pour conséquence d'exclure de cette réduction les secteurs et sous-secteurs dans lesquels les coûts supplémentaires résultant de la hausse du prix de l'électricité induite par les quotas d'émission de gaz à effet de serre sont inférieurs à 5 %, ou dans certains cas 2,5 %, des coûts de production. La différence de traitement qui en résulte correspond à une différence de situation entre les secteurs et sous-secteurs pour lesquels ce coût est élevé et ceux pour lesquels il est faible. Cette différence est en rapport direct avec l'objet des dispositions législatives contestées qui, ainsi qu'il ressort de leurs travaux préparatoires, notamment de la présentation par le secrétaire d'Etat chargé du budget de l'amendement gouvernemental duquel elles sont issues lors de la séance à l'Assemblée nationale du 1er décembre 2015, est de ne pas pénaliser les entreprises qui, du fait de leur exposition à la concurrence internationale, " ne sont pas en mesure de répercuter sur le prix de leurs produits le coût lié au CO2 ". Dès lors, en ce qu'il exclut les secteurs et sous-secteurs pour lesquels l'adaptation au système d'échange de quotas d'émissions n'engendre pas de coûts significatifs liés à la hausse du prix de l'électricité et qui n'ont donc pas à subir, même lorsqu'ils sont exposés à la concurrence internationale, les effets de la tarification du carbone de ce fait, le critère retenu par le législateur présente un caractère objectif et rationnel. Si le législateur a retenu une définition différente des secteurs considérés comme exposés au risque de fuite de carbone pour l'application du tarif applicable aux installations hyperélectro-intensives, la requérante n'est pas fondée à soutenir qu'il aurait dû appliquer la même définition pour la réduction prévue au d du C du 8 de l'article 266 quinquies C dès lors que l'objet de ces deux réductions est différent.<br/>
<br/>
              8. En second lieu, s'il est vrai que l'appréciation des deux critères pris en compte par la Commission européenne dans ses lignes directrices du 5 juin 2012 s'opère au niveau, relativement agrégé, des secteurs ou sous-secteurs de la nomenclature des activités dans l'Union européenne et qu'ainsi, certaines installations peuvent ne pas bénéficier de la réduction prévue au d du C du 8 de l'article 266 quinquies C du code des douanes parce que leur production relève d'un secteur ou d'un sous-secteur qui, en moyenne pour l'ensemble des activités qui le composent, ne remplit pas ces deux critères, alors même qu'à leur niveau, ces installations satisferaient aux conditions de coûts supplémentaires et d'exposition à la concurrence internationale exigées, cette difficulté, qui est inhérente au degré de finesse des statistiques disponibles, n'induit pas de rupture d'égalité dès lors que le législateur s'est, en l'espèce, fondé sur un critère objectif et rationnel et que les catégories d'entreprises auxquelles il a choisi de se référer sont suffisamment homogènes.<br/>
<br/>
              9. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Chambre syndicale française des levures.<br/>
Article 2 : La présente décision sera notifiée à la Chambre syndicale française des levures et au ministre de l'économie et des finances. Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
