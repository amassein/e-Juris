<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311367</ID>
<ANCIEN_ID>JG_L_2014_07_000000376358</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 30/07/2014, 376358, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376358</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:376358.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 et 31 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre communal d'action sociale de Valenciennes, dont le siège est à l'Hôtel de Ville, Place d'Armes, BP 90339 à Valenciennes (59304) ; le centre communal d'action sociale de Valenciennes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1400794 du 27 février 2014 par laquelle le juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'avis du 22 janvier 2014 par lequel le conseil de discipline de recours propose comme sanction devant être infligée à Mme C...A..., auxiliaire de vie, l'exclusion temporaire de fonctions pour une durée de quinze jours, sans sursis ;<br/>
<br/>
              2°) de mettre à la charge de Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat du centre communal d'action sociale de Valenciennes ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le centre communal d'action sociale de Valenciennes demande l'annulation de l'ordonnance en date du 27 février 2014 par laquelle le juge des référés du tribunal administratif de Lille a rejeté sa demande tendant à la suspension de l'avis du 22 janvier 2014 par lequel le conseil de discipline de recours a proposé, en substitution à la sanction de la révocation prononcée par le conseil de discipline à l'encontre de MmeA..., auxiliaire de vie, que celle-ci fasse l'objet d'une sanction d'exclusion temporaire de fonctions de quinze jours sans sursis ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lille, notamment d'attestations, de comptes-rendus d'entretien avec les propriétaires d'un commerce ambulant et de déclarations de l'intéressée elle-même, que Mme A..., à l'occasion de courses alimentaires qu'elle effectuait pour le compte de l'une des bénéficiaires du centre communal d'action sociale de Valenciennes, MmeB..., laquelle lui remettait des chèques sans montant préétabli, utilisait ces chèques pour se faire remettre des espèces par les commerçants, en y portant des montants supérieurs aux sommes dues en règlement des courses ; qu'il ressort d'attestations précises et concordantes de Mme B...et de son fils que ces espèces n'ont jamais été remises à Mme B...; que, compte-tenu de ces éléments, le juge des référés, en jugeant que n'était pas de nature à créer un doute sérieux sur la légalité de l'avis litigieux le moyen selon lequel le conseil de discipline de recours, en retenant que les faits de détournement d'espèces par Mme A...n'étaient pas établis, aurait entaché son appréciation d'une erreur de fait, a entaché son ordonnance de dénaturation des pièces du dossier ; que le centre communal d'action sociale de Valenciennes est dès lors fondé à en demander l'annulation ; qu'il y a lieu, par suite et sans qu'il soit besoin d'examiner les autres moyens de la requête, d'annuler cette ordonnance ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par le centre communal d'action sociale de Valenciennes, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant, ainsi qu'il a été dit ci-dessus, que le moyen tiré de ce que l'avis du 22 janvier 2014 par lequel le conseil de discipline de recours a substitué à la sanction de la révocation prononcée par le conseil de discipline à l'encontre de MmeA..., une exclusion temporaire de fonctions de quinze jours, est entaché d'erreur de fait, est de nature, en l'état de l'instruction, à créer un doute sérieux sur la légalité de celui-ci ; <br/>
<br/>
              5. Considérant toutefois qu'en application de l'article 91 de la loi du 26 janvier 1984, l'autorité territoriale ne peut prononcer de sanction plus sévère que celle proposée par le conseil de discipline de recours ; que l'exécution de l'avis du conseil de discipline de recours impose alors au directeur du centre communal d'action sociale de substituer à la sanction de la révocation celle de l'exclusion temporaire de fonctions de quinze jours et de réintégrer Mme A... à l'issue de cette exclusion temporaire ; que, si la réaffectation de Mme A...auprès d'un bénéficiaire du centre communal d'action sociale de Valenciennes serait susceptible de créer des troubles eu égard aux faits qui lui sont reprochés, il ne ressort cependant pas des pièces du dossier soumis au juge des référés que le centre communal d'action sociale serait dans l'impossibilité d'affecter l'intéressée sur un poste ne comportant aucune relation avec les usagers, en attendant le règlement au fond du litige ; qu'ainsi, le centre communal d'action sociale de Valenciennes ne justifie pas de ce que la réintégration de Mme A...créerait une situation d'urgence de nature à justifier la suspension de la décision contestée ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le centre communal d'action sociale de Valenciennes n'est pas fondé à demander la suspension de l'avis du 22 janvier 2014 du conseil de discipline de recours ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le centre communal d'action sociale de Valenciennes au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 27 février 2014 du juge des référés du tribunal administratif de Lille est annulée. <br/>
Article 2 : La demande présentée par le centre communal d'action sociale de Valenciennes devant le juge des référés du tribunal administratif de Lille et ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au centre communal d'action sociale de Valenciennes, à Mme C...A...et au centre de gestion de la fonction publique territoriale du Nord.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
