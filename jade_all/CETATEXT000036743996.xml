<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036743996</ID>
<ANCIEN_ID>JG_L_2018_03_000000409565</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/39/CETATEXT000036743996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/03/2018, 409565, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409565</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409565.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B..., néeA..., a demandé au tribunal administratif de Versailles d'annuler l'arrêté du 17 février 2011 par lequel le préfet des Yvelines a abrogé son arrêté du 24 décembre 2010 portant admission de la requérante à faire valoir ses droits à la retraite à compter du 30 juin 2011, de condamner l'Etat à lui verser des dommages et intérêts en réparation d'un préjudice moral et, enfin, de modifier son titre de pension afin de prendre en compte les services compris entre le 1er juillet 2011 et le 1er juillet 2013. Par un jugement n° 1101297 du 13 avril 2015, le tribunal administratif de Versailles a annulé l'arrêté du préfet des Yvelines du 17 février 2011 et rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 15VE01843 du 30 mars 2017, enregistré le 5 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi et le nouveau mémoire, enregistrés les 12 juin 2015 et 5 décembre 2016 au greffe de cette cour, présentés par MmeB.... Par ce pourvoi, ce mémoire et un nouveau mémoire, enregistré le 2 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a rejeté ses conclusions indemnitaires ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de MmeB....<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 412-1 du code de justice administrative, dans sa version applicable au litige : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision (...) " ; qu'aux termes de l'article R. 611-7 du même code : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la chambre chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué " ; <br/>
<br/>
              2. Considérant qu'aucune fin de non-recevoir tirée du défaut de décision préalable ne peut être opposée à un requérant ayant introduit devant le juge administratif un contentieux indemnitaire à une date où il n'avait présenté aucune demande en ce sens devant l'administration lorsqu'il a formé, postérieurement à l'introduction de son recours juridictionnel, une demande auprès de l'administration sur laquelle le silence gardé par celle-ci a fait naître une décision implicite de rejet avant que le juge de première instance ne statue, et ce quelles que soient les conclusions du mémoire en défense de l'administration ; qu'en revanche, une telle fin de non-recevoir peut être opposée lorsque, à la date à laquelle le juge statue, le requérant s'est borné à l'informer qu'il avait saisi l'administration d'une demande mais qu'aucune décision de l'administration, ni explicite ni implicite, n'est encore née ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a demandé au tribunal administratif de Versailles, en premier lieu, d'annuler l'arrêté du 17 février 2011 par lequel le préfet des Yvelines a abrogé son arrêté du 24 décembre 2010 l'autorisant à faire valoir ses droits à la retraite à compter du 30 juin 2011, en deuxième lieu, de condamner l'Etat à lui verser des dommages et intérêts en réparation d'un préjudice moral et, enfin, de modifier son titre de pension afin de prendre en compte des services compris entre le 1er juillet 2011 et le 1er juillet 2013 ; que, par un mémoire enregistré au greffe du tribunal administratif le 21 février 2013, le préfet des Yvelines s'est borné à demander à la juridiction de surseoir à statuer dans l'attente d'un éventuel désistement de la requérante ; que, par une lettre recommandée avec avis de réception du 13 février 2015, le président du tribunal administratif de Versailles a informé MmeB..., en application des dispositions de l'article R. 611-7 du code de justice administrative, que le tribunal était susceptible de soulever d'office le moyen tiré de l'irrecevabilité de ses conclusions indemnitaires, qui n'ont pas été précédées d'une demande préalable, et lui a laissé un délai de quinze jours pour présenter ses observations ; que Mme B... a adressé, le 19 février 2015, une demande au préfet des Yvelines tendant au versement d'une somme de 32 500 euros à laquelle l'administration n'a pas répondu ; que, contrairement à ce que soutient la requérante, la communication faite par le tribunal le 13 février 2015 ne peut être regardée comme une invitation à régulariser ses conclusions mais constitue une simple information destinée à lui permettre de présenter ses observations sur le moyen susceptible de fonder le jugement ; qu'il résulte de ce qui a été dit au point 2 de la présente décision que le tribunal administratif de Versailles, qui n'était pas tenu d'attendre l'intervention d'une décision de l'administration, n'a pas commis d'erreur de droit en constatant, à la date de son jugement, l'absence de décision de l'administration sur la demande présentée par la requérante le 19 février 2015 et en en déduisant que les conclusions indemnitaires de celle-ci étaient irrecevables ; que, par suite, la requérante n'est pas fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il a rejeté ses conclusions indemnitaires ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme C...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
