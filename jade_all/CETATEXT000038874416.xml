<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038874416</ID>
<ANCIEN_ID>JG_L_2019_07_000000420992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/87/44/CETATEXT000038874416.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 24/07/2019, 420992, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420992.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1) Sous le n° 420992 :<br/>
<br/>
              M. A...D...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 22 novembre 2017 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une ordonnance n° 18003420 du 16 février 2018, la présidente de la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
              Par un pourvoi enregistré le 28 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat ou de l'Office français de protection des refugiés et apatrides la somme de 2 000 euros à verser à la SCP Chaisemartin et Doumic-Seiller, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              2) Sous le n° 421003 :<br/>
<br/>
              Mme B...C...épouse D...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 22 novembre 2017 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugiée ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une ordonnance n° 18003421 du 16 février 2018, la présidente de la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
              Par un pourvoi enregistré le 28 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat ou de l'Office français de protection des refugiés et apatrides la somme de 2 000 euros à verser à la SCP Chaisemartin et Doumic-Seiller, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de M. et Mme D...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Les requêtes n° 420992 et n° 421003 présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2.	Aux termes des dispositions de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides prises en application des articles L. 711-1 à L. 711-4, L. 712-1 à L. 712-3, L. 713-1 à L. 713-4, L. 723-1 à L. 723-8, L. 723-11, L. 723-15 et L. 723-16. A peine d'irrecevabilité, ces recours doivent être exercés dans le délai d'un mois à compter de la notification de la décision de l'office (...) ".<br/>
<br/>
              3.	Aux termes de l'article 9-4 de la loi du 10 juillet 1991 relative à l'aide juridique, applicable au litige : " Devant la Cour nationale du droit d'asile, le bénéfice de l'aide juridictionnelle est de plein droit, sauf si le recours est manifestement irrecevable. Si l'aide juridictionnelle est sollicitée en vue d'introduire le recours devant la cour, elle doit être demandée dans le délai de quinze jours à compter de la notification de la décision de l'office. Dans le cas contraire, l'aide juridictionnelle peut être demandée lors de l'introduction du recours, exercé dans le délai. Ces délais sont notifiés avec la décision de l'Office français de protection des réfugiés et apatrides susceptible de recours ". L'article 39 du décret du 19 décembre 1991 portant application de la loi du 10 juillet 1991 relative à l'aide juridique dispose que, lorsque l'aide juridictionnelle a été sollicitée à l'occasion d'une instance devant le Conseil d'Etat ou une juridiction administrative statuant à charge de recours devant le Conseil d'Etat avant l'expiration du délai imparti pour le dépôt du pourvoi ou des mémoires, ce délai est interrompu et " un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné ". Il résulte de ces dispositions combinées qu'une demande d'aide juridictionnelle présentée dans le délai de quinze jours à compter de la notification de la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) interrompt le délai d'un mois prévu par l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              4.	Pour rejeter les requêtes de M. D...et de MmeC..., sur le fondement de l'article R. 733-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, comme entachée d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance, la Cour nationale du droit d'asile a jugé que ces requêtes dirigées contre les décisions de l'OFPRA du 22 novembre 2017 régulièrement notifiées le 8 décembre 2017, enregistrées au secrétariat de la cour le 23 janvier 2018, soit après le 9 janvier 2018, date de l'expiration du délai de recours contentieux, étaient tardives. En statuant ainsi alors que les intéressés avaient présenté des demandes d'aide juridictionnelle le 13 décembre 2017, soit dans le délai de quinze jours prévu par l'article 9-4 de la loi du 10 juillet 1991 précité, lesquelles avaient interrompu le délai de recours contentieux, la présidente de la Cour nationale du droit d'asile a entaché les deux ordonnances attaquées d'une erreur de droit.<br/>
<br/>
              5.	Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que M. D...et Mme C...sont fondés à demander l'annulation des ordonnances attaquées.<br/>
<br/>
              6.	Il n'y a pas lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat ou de l'OFPRA la somme que demande la SCP Chaisemartin et Doumic-Seiller, avocat de M. D...et de MmeC..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les ordonnances n° 18003420 et 18003421 du 16 février 2018 de la Cour nationale du droit d'asile sont annulées.<br/>
<br/>
Article 2 : Les deux affaires sont renvoyées devant la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : Les conclusions présentées par la SCP Chaisemartin et Doumic-Seiller, avocat de M. D... et de Mme C...au titre des dispositions des articles L. 761 1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...D...et Mme B...C..., épouseD....<br/>
Copie en sera adressée à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
