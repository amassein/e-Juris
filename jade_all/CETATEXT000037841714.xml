<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037841714</ID>
<ANCIEN_ID>JG_L_2018_12_000000418523</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/17/CETATEXT000037841714.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 20/12/2018, 418523, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418523</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418523.20181220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 23 février et 22 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la société FG Concept demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2017-564 du 26 juillet 2017 par laquelle le Conseil supérieur de l'audiovisuel (CSA) l'a mise en demeure de respecter ses obligations conventionnelles de diffusion de chansons d'expression française, ensemble la décision du 29 décembre 2017 par laquelle le CSA a rejeté son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société FG Concept.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que la société FG Concept, éditeur du service " Radio FG ", a, le 2 octobre 2012 et sur le fondement de l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication, conclu avec le Conseil supérieur de l'audiovisuel (CSA) une convention aux termes de laquelle elle s'est engagée à ce qu'au moins 35 % de la totalité des chansons diffusées mensuellement entre 6 heures 30 et 22 heures 30 du lundi au vendredi et entre 8 heures et 22 heures 30 le samedi et le dimanche soient des chansons d'expression française, dont 25 % au moins du nombre total provenant de jeunes talents ; que, par deux décisions des 26 juillet et 29 décembre 2017, le CSA a mis la société FG Concept en demeure de respecter cet engagement et rejeté son recours gracieux contre cette mise en demeure ; que la société FG Concept demande l'annulation pour excès de pouvoir de ces décisions ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Considérant que les dispositions et stipulations qui confèrent au CSA le pouvoir de procéder à une mise en demeure impliquent qu'un tel acte, alors même qu'il n'entre dans aucune des catégories de décisions administratives qui doivent être motivées en application de l'article L. 211-2 du code des relations entre le public et l'administration, mentionne les faits constatés par le CSA ainsi que les obligations dont il estime qu'elles ont été méconnues et auxquelles il invite l'éditeur, le distributeur ou l'opérateur à se conformer à l'avenir ; que, contrairement à ce que soutient la société FG Concept, à qui il est au demeurant loisible de demander au CSA toute précision relative aux éléments sur lesquels il a fondé ses calculs, la mise en demeure qui lui a été adressée le 26 juillet 2017 comporte l'ensemble de ces éléments ; <br/>
<br/>
              Sur la légalité interne:<br/>
<br/>
              3. Considérant que l'article 28 de la loi du 30 septembre 1986 subordonne la délivrance de l'autorisation d'usage de la ressource radioélectrique à tout service diffusé par voie hertzienne terrestre, autre que ceux exploités par les sociétés nationales de programme, à la conclusion d'une convention passée entre le CSA au nom de l'Etat et la personne qui demande l'autorisation ; qu'aux termes du premier alinéa du 2° bis de cet article, cette convention porte notamment sur " la proportion substantielle d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France, qui doit atteindre un minimum de 40 % de chansons d'expression française, dont la moitié au moins provenant de nouveaux talents ou de nouvelles productions, diffusées aux heures d'écoute significative par chacun des services de radio autorisés par le Conseil supérieur de l'audiovisuel, pour la part de ses programmes composée de musique de variétés " ; que les alinéas suivants de ce même 2° bis prévoient, pour les radios répondant à certains critères, des règles de quotas dérogatoires portant notamment sur la programmation de nouveaux talents et de nouvelles productions ; que la loi du 7 juillet 2016 relative à la liberté de la création, à l'architecture et au patrimoine a complété le 2° bis par un alinéa introduisant un mécanisme dit de " plafonnement des rotations " ou " malus ", ainsi défini : " Dans l'hypothèse où plus de la moitié du total des diffusions d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France se concentre sur les dix oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France les plus programmées par un service, les diffusions intervenant au-delà de ce seuil ou n'intervenant pas à des heures d'écoute significative ne sont pas prises en compte pour le respect des proportions fixées par la convention pour l'application du présent 2° bis " ;<br/>
<br/>
              4. Considérant que, par une communication du 23 novembre 2016, le CSA a indiqué la méthode qu'il entendait mettre en oeuvre pour vérifier le respect, par les éditeurs de services de radio, des dispositions citées au point 3 ; qu'aux termes de cette communication : " Pour vérifier le respect des quotas, le Conseil ne prend pas en compte les diffusions des dix titres francophones intervenant au-delà de 50 % du total des titres francophones diffusés. Ces diffusions sont par conséquent retirées du sous-total des diffusions des titres francophones, c'est-à-dire du numérateur " ; qu'il résulte des dispositions du 2° bis de l'article 28 de la loi du 30 septembre 1986 que les quotas qu'elles prévoient sont calculés en proportion de l'ensemble de la programmation de chansons de variétés de chaque radio ;  que par suite, en ne prévoyant pas la soustraction des diffusions excédentaires du total des diffusions qui figure au dénominateur servant au calcul du quota, le CSA n'a pas fait une inexacte application de ces dispositions ; que la société requérante n'est, par suite, pas fondée à soutenir que les décisions qu'elle attaque auraient été prises sur le fondement d'une communication illégale ;<br/>
<br/>
              5. Considérant qu'au sens et pour l'application du 2° bis de l'article 28 de la loi du 30 septembre 1986, les termes de " musique de variétés " désignent l'ensemble de la chanson populaire et de divertissement accessible à un large public, par opposition, notamment, à l'art lyrique et au chant du répertoire savant ; qu'il ne ressort pas des pièces du dossier que le CSA aurait commis une erreur d'appréciation en retenant, pour l'application de ces dispositions, que la programmation de Radio FG, essentiellement composée de " musique électronique (lounge, undergroud, dance) ", était en totalité, pour sa partie chantée, composée de musique de variétés ;<br/>
<br/>
              6. Considérant que, se prononçant sur la conformité à la Constitution du texte adopté par le Parlement et qui allait devenir la loi du 17 janvier 1989 modifiant la loi du 30 septembre 1986 relative à la liberté de communication, le Conseil constitutionnel, par sa décision n° 88-248 DC du 17 janvier 1989, a estimé que les pouvoirs de sanction conférés par le législateur au CSA ne sont susceptibles de s'exercer qu'après mise en demeure des titulaires d'autorisation pour l'exploitation de services de communication audiovisuelle de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires, et faute pour les intéressés de respecter ces obligations ou de se conformer aux mises en demeure qui leur ont été adressées ; que c'est sous réserve de cette interprétation que les articles en cause ont été déclarés conformes à l'article 8 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 et à l'article 34 de la Constitution ; que cette réserve d'interprétation assure notamment le respect du principe de légalité des délits et des peines, consacré par l'article 8 de la Déclaration du 26 août 1789 et qui s'applique notamment devant les organismes administratifs dotés d'un pouvoir de sanction ; que le CSA ne peut, en effet, prononcer une sanction contre le titulaire de l'autorisation qu'en cas de réitération d'un comportement ayant fait auparavant l'objet d'une mise en demeure par laquelle il a été au besoin éclairé sur ses obligations ;<br/>
<br/>
              7. Considérant que si la société requérante soutient que la notion de musique de variétés est insuffisamment précise et en déduit que le CSA a méconnu le principe de légalité des délits et des peines ainsi que les stipulations de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en la mettant en demeure de respecter des obligations déterminées en fonction de cette notion, ce moyen ne saurait être accueilli dès lors, d'une part, que la notion de musique de variétés répond à la définition indiquée au point 5 et, d'autre part, que la mise en demeure attaquée indique les pourcentages de diffusion de chansons d'expression française retenus pour apprécier le respect des quotas et met ainsi son destinataire en mesure d'en vérifier l'exactitude ou, le cas échéant, de demander au CSA toute précision relative à leur mode de calcul ;<br/>
<br/>
              8. Considérant qu'aux termes du sixième alinéa du 2° bis de l'article 28 de la loi du 30 septembre 1986 : " Pour l'application des premier et quatrième alinéas du présent 2° bis, le Conseil supérieur de l'audiovisuel peut, pour les services dont les programmes musicaux constituent une proportion importante de la programmation, diminuer la proportion minimale de titres francophones, en tenant compte de l'originalité de la programmation et des engagements substantiels et quantifiés pris par la radio en matière de diversité musicale, sans que cette proportion puisse être inférieure respectivement à 35 % et 30 %. Ces engagements, applicables à l'ensemble de la programmation musicale du service aux heures d'écoute significative, portent sur le taux de nouvelles productions, qui ne peut être inférieur à 45 %, le nombre de rediffusions d'un même titre, qui ne peut être supérieur à cent cinquante par mois, ainsi que sur le nombre de titres et d'artistes diffusés et sur la diversité des producteurs de phonogrammes. Les modalités de ces engagements sont fixées par le Conseil supérieur de l'audiovisuel dans une délibération prise après consultation publique " ; que si, en application de ces dispositions, le CSA doit procéder à une consultation publique avant de définir les modalités selon lesquelles les éditeurs de services de radio souscriront des engagements substantiels et quantifiés en matière de diversité musicale, le législateur n'a pas prévu une telle formalité préalablement à la mise en oeuvre du mécanisme de " plafonnement des rotations " ou " malus " ; qu'il ne résulte pas davantage du sixième alinéa du 2° bis que l'application de ce mécanisme de " malus " soit subordonnée à celle du " bonus " susceptible d'être accordé aux éditeurs souscrivant de tels engagements ; qu'ainsi la société requérante n'est pas fondée à soutenir que le CSA aurait commis une erreur de droit ou une erreur d'appréciation en mettant en oeuvre sans délai le dispositif de " malus " ;<br/>
<br/>
              9. Considérant que chaque éditeur de services est tenu de respecter les règles de quotas de chanson francophone qui lui sont applicables en conséquence des dispositions de l'article 28 de la loi du 30 septembre 1986 et de la convention qu'il a conclue avec le CSA ; qu'un éditeur ne peut utilement exciper, à l'occasion de la contestation d'une décision prise par le CSA à son égard pour assurer ce respect, de ce que le CSA aurait conclu avec d'autres éditeurs des conventions comportant des règles de quota différentes ; que le moyen tiré de ce que la décision attaquée revêtirait un caractère discriminatoire en raison de la présence de règles de quotas plus favorables dans les conventions conclues par plusieurs autres éditeurs de services doit, par suite, être écarté ;   ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la société FG Concept n'est pas fondée à demander l'annulation des décisions qu'elle attaque  ; que sa requête doit, par suite, être rejetée, y compris les conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société FG Concept est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société FG Concept et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
