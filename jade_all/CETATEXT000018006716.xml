<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006716</ID>
<ANCIEN_ID>JG_L_2007_07_000000274479</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/67/CETATEXT000018006716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 26/07/2007, 274479, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>274479</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Yves  Salesse</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Verot</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés le 22 novembre 2004 et le 16 mars 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Marius A, demeurant ... ; M. et Mme A demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt en date du 21 septembre 2004 par lequel la cour administrative d'appel de Douai a rejeté leur requête tendant, d'une part, à l'annulation du jugement du 20 février 2001 du tribunal administratif de Rouen rejetant celles des conclusions de leur demande tendant à la réduction des cotisations supplémentaires d'impôt sur le revenu et de contribution sociale généralisée auxquelles ils ont été assujettis au titre de l'année 1993 sur lesquelles il y avait lieu de statuer et, d'autre part, à la réduction des impositions contestées ;<br/>
<br/>
              2°) statuant au fond, de leur accorder la réduction demandée ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L.761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Salesse, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de M. et Mme Marius A, <br/>
<br/>
              - les conclusions de Mlle Célia Verot, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Big Bull a conclu en 1974 avec la Sicomi Locabail un contrat de crédit-bail immobilier avec option d'achat, d'une durée de quinze ans, pour un local situé dans un centre commercial ; que ce local a été sous-loué à la société Big Crep's qui exploitait un fonds de commerce de restauration ; que le 31 mars 1987, la société Big Bull a pris la forme d'une société en nom collectif dont M. et Mme A étaient les seuls associés ; que l'option d'achat prévue au contrat de crédit-bail a été levée à l'occasion du dénouement de celui-ci et que la société en nom collectif Big Bull est ainsi devenue propriétaire en juin 1989 du local dont elle était jusque-là locataire, après le versement d'une somme de 153 798 francs ; que ce local a été revendu en janvier 1993 pour la somme de 1,5 million de francs ; que M. et Mme A ont demandé en vain la décharge des cotisations supplémentaires d'impôt sur le revenu et de contribution sociale généralisée auxquelles ils ont été assujettis à raison de la plus-value dégagée à l'occasion de cette vente ; <br/>
<br/>
              Considérant que M. et Mme A n'ont présenté, dans le délai de recours courant à l'encontre de l'arrêt attaqué, que des moyens relatifs au bien-fondé de cet arrêt ; que le moyen tiré d'une motivation insuffisante de cet arrêt, qu'ils ont soulevé après l'expiration du délai de recours, repose sur une cause juridique distincte ; que n'étant pas d'ordre public, il est, par suite, irrecevable ;<br/>
<br/>
              Considérant que si les requérants soutiennent que la cour administrative d'appel aurait commis une erreur de droit en omettant de rechercher les effets de la transformation en 1987 de la société anonyme Big Bull en une société en nom collectif dont ils étaient les associés, ce moyen ne peut qu'être rejeté dès lors que le bien n'a été acquis que deux ans après cette transformation, en juin 1989 et que, jusqu'à la date de cette acquisition, la SNC n'était titulaire que des droits issus du contrat de crédit-bail conclu par la SA Big Bull en 1974 ; <br/>
<br/>
              Considérant que si les requérants soutiennent que l'acquisition du bien en 1989 aurait généré une plus-value, ils ne peuvent invoquer le bénéfice des dispositions du IV de l'article 93 quater du code général des impôts, applicables aux seules plus-values réalisées à compter du 1er janvier 1990 ; qu'au surplus, ainsi qu'il a été dit ci-dessus, la SNC ne possédait avant l'acquisition que les droits issus du contrat de crédit-bail ;<br/>
<br/>
              Considérant que les requérants ne peuvent pas plus invoquer le bénéfice des dispositions de l'article 239 sexies du code général des impôts applicables aux acquisitions par crédit-bail de biens professionnels par une entreprise pour les besoins de son activité commerciale, alors que, en l'espèce, l'immeuble acquis par la SNC a entraîné la réalisation de revenus fonciers ; qu'ainsi, la cour administrative d'appel n'a ni commis d'erreur de droit ni dénaturé les pièces du dossier en jugeant que la plus-value réalisée en 1993 devait être calculée, en vertu de l'article 150 H du code général des impôts, par différence entre le prix de cession et le prix d'acquisition payé en 1989 ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. et Mme A ne sont pas fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Douai en date du 21 septembre 2004 ; que doivent être rejetées, par voie de conséquence, leurs conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de M. et Mme A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme Marius A et au ministre du budget, des comptes publics et de la fonction publique. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
