<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875957</ID>
<ANCIEN_ID>JG_L_2021_07_000000453908</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875957.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/07/2021, 453908, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453908</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453908.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... A... a demandé au juge des référés du tribunal administratif C..., statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution des décisions, révélées par un courrier de la directrice du centre maternel de la Croix-Rouge, mettant fin à la prise en charge de son fils B... par le service de l'aide sociale à l'enfance et portant expulsion de sa famille de la structure où elle est hébergée et d'enjoindre au préfet de la Côte-d'Or ou au département de la Côte-d'Or, à titre principal, de la maintenir, avec sa famille, dans les locaux de la Croix-Rouge actuellement occupés, dans les quarante-huit heures suivant la notification de l'ordonnance à venir, sous astreinte de 150 euros par jour de retard, à titre subsidiaire, de trouver une solution d'hébergement d'urgence adaptée sous les mêmes conditions et, sinon, de lui verser des aides financières propres à couvrir ses frais d'hébergement.<br/>
<br/>
              Par une ordonnance n° 2101625 du 22 juin 2021, le juge des référés du tribunal administratif C... a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 24 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit aux conclusions présentées devant le juge des référés du tribunal administratif C... ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat et du département de la Côte-d'Or la somme de 3 000 euros à verser à son conseil au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors qu'elle est atteinte d'une très grave pathologie, que son état s'est brutalement aggravé, qu'elle est dans l'attente du jugement du recours formé contre le refus de titre de séjour opposé par le préfet de la Côte-d'Or et l'obligation de quitter le territoire français qui lui a été faite, que son fils n'est plus pris en charge par l'aide sociale à l'enfance et qu'il va être mis fin très prochainement à l'hébergement de sa famille malgré son état de détresse avéré ;<br/>
              - l'ordonnance contestée porte une atteinte grave et manifestement illégale au droit à un hébergement d'urgence, au droit de recevoir les traitements et soins les plus appropriés et au droit de ne pas être soumis à des traitements inhumains et dégradants, compte tenu de la situation de grande détresse dans laquelle elle se trouve avec son fils et alors que ni l'absence de sollicitation des services de l'Etat ni la saturation des dispositifs d'accueil ne sont démontrés.<br/>
<br/>
              Par un mémoire en défense, enregistré le 29 juin 2021, le département de la Côte-d'Or conclut au rejet de la requête. Il soutient que les conditions fixées par l'article L. 222-5 du code de l'action sociale et des familles pour une prise en charge par le service de l'aide sociale à l'enfance ne sont plus remplies et qu'il est allé bien au-delà de ses obligations légales et ne saurait donc se voir reprocher une atteinte grave et illégale à une liberté fondamentale. <br/>
<br/>
              Par un mémoire en défense, enregistré le 30 juin 2021, le ministre des solidarités et de la santé conclut au non-lieu à statuer, dès lors qu'une solution d'hébergement a été proposée à la requérante et sa famille.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 30 juin 2021, Mme A... acquiesce aux conclusions à fin de non-lieu et maintient ses conclusions au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A..., d'autre part, le ministre des solidarités et de la santé et, enfin, le département de la Côte-d'Or ;<br/>
<br/>
              Vu la lettre informant les parties de la radiation de l'affaire du rôle de l'audience publique du 1er juillet 2021 ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Mme A..., ressortissante algérienne entrée en France en avril 2019, est actuellement hébergée avec son époux, qui l'a rejointe en 2020, et avec son fils B..., né le 6 juillet 2017, au centre parental de la Croix-Rouge française de Talant. Cet hébergement devant prendre fin le 6 juillet 2021, elle a notamment demandé au juge des référés du tribunal administratif C..., statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au département ou au préfet de la Côte-d'Or de lui maintenir le bénéfice de cet hébergement ou, à défaut, de lui trouver une solution alternative d'hébergement d'urgence, sauf à lui allouer les subsides lui permettant d'y pourvoir par elle-même. Elle fait appel de l'ordonnance du 22 juin 2021 par laquelle le juge des référés a rejeté sa demande.<br/>
<br/>
              3. Postérieurement à l'introduction de la requête de Mme A..., le service d'hébergement d'urgence des familles C... a proposé un hébergement d'urgence continu et individuel pour l'intéressée, son époux et son enfant à partir du 6 juillet 2021 et au plus tard le 8 juillet 2021, le centre parental de la Croix-Rouge s'engageant à maintenir sa prise en charge en attendant leur entrée dans ce nouvel hébergement. Dans ces conditions, les conclusions d'appel de Mme A... tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative sont, en cours d'instance, devenues sans objet. Il n'y a, dès lors, pas lieu d'y statuer.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce et sans qu'il y ait lieu d'admettre Mme A... au bénéfice de l'aide juridictionnelle provisoire, de mettre à la charge de l'Etat, à qui il appartient de mettre en oeuvre le droit à l'hébergement d'urgence, la somme de 1 500 euros à verser à l'intéressée au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur la requête de Mme A.... <br/>
Article 2 : L'Etat versera la somme de 1 500 euros à Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à Mme D... A..., au département de la Côte-d'Or et au ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
