<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043677263</ID>
<ANCIEN_ID>JG_L_2021_06_000000434363</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/72/CETATEXT000043677263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 17/06/2021, 434363, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434363</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET</AVOCATS>
<RAPPORTEUR>Mme Catherine Fischer-Hirtz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434363.20210617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 septembre et 6 décembre 2019 et le 26 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la société Butagaz demande au Conseil d'Etat :<br/>
<br/>
              1°) à titre principal, d'annuler la décision du 9 juillet 2019 par laquelle le ministre d'Etat, ministre de la transition écologique et solidaire, lui a infligé une sanction pécuniaire de 99 039 euros au titre de manquements constatés dans le cadre de ses obligations d'économies d'énergie, en application de l'article L. 222-2 du code de l'énergie ;<br/>
<br/>
              2°) à titre subsidiaire, de réformer cette décision en ramenant la sanction à un montant symbolique ou à tout le moins en réduisant substantiellement son quantum ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2015-992 du 17 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Colin-Stoclet, avocat de la société Butagaz ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que la société Butagaz est soumise, en sa qualité de fournisseur d'énergie, à des obligations d'économie d'énergie en application de l'article L. 221-1 du code de l'énergie, dont elle s'acquitte à travers la réalisation d'opérations donnant lieu à la délivrance de certificats d'économies d'énergie (CEE). La société Butagaz a mandaté la société Economie d'énergie pour la gestion et le contrôle des opérations. Par un courrier du 8 mars 2017, le ministre d'Etat, ministre de la transition écologique et solidaire a engagé auprès de la société Butagaz une procédure de contrôle au titre de 32 opérations portant sur des travaux d'isolation réalisés en 2015 et 2016 et ayant donné lieu à la délivrance de certificats pour un total de 25 456 095 kWh cumac. A l'issue du contrôle, des manquements ont été constatés pour 17 des opérations contrôlées, représentant un total de 14 803 000 kWh cumac. Par une décision du 9 juillet 2019, le ministre a prononcé en application de l'article L. 222-2 du code de l'énergie, d'une part, l'annulation de certificats d'énergie détenus par la société Butagaz pour un volume égal à celui concerné par le manquement et, d'autre part, une amende de 99 039 euros. La société demande, à titre principal, l'annulation, et à titre subsidiaire, la réformation de la décision du ministre, uniquement en tant qu'elle prononce une amende.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 221-1 du code de l'énergie dans sa rédaction applicable au litige : " Sont soumises à des obligations d'économies d'énergie : / 1° Les personnes morales qui mettent à la consommation des carburants automobiles et dont les ventes annuelles sont supérieures à un seuil défini par décret en Conseil d'Etat. / 2° Les personnes qui vendent de l'électricité, du gaz, du fioul domestique, de la chaleur ou du froid aux consommateurs finals et dont les ventes annuelles sont supérieures à un seuil défini par décret en Conseil d'Etat (...) ". Aux termes de l'article L. 222-1 du même code dans sa version applicable au litige : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements aux dispositions du chapitre Ier du présent titre ou aux dispositions réglementaires prises pour leur application ". Aux termes de l'article L. 222-2 du même code dans sa rédaction applicable au litige : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. Il peut rendre publique cette mise en demeure. / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : / 1° Prononcer à son encontre une sanction pécuniaire dont le montant est proportionné à la gravité du manquement et à la situation de l'intéressé, sans pouvoir excéder le double de la pénalité prévue au premier alinéa de l'article L. 221-4 par kilowattheure d'énergie finale concerné par le manquement et sans pouvoir excéder 2 % du chiffre d'affaires hors taxes du dernier exercice clos, porté à 4 % en cas de nouveau manquement à la même obligation ; / 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie selon les modalités prévues au premier alinéa de l'article L. 221-7 et à l'article L. 221-12 ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé (...) ". Aux termes de l'article L. 222-3 du même code : " Les sanctions sont prononcées après que l'intéressé a reçu notification des griefs et a été mis à même de consulter le dossier et de présenter ses observations, assisté, le cas échéant, par une personne de son choix ". Aux termes de l'article L. 222-5 du même code : " L'instruction et la procédure devant le ministre sont contradictoires. " Aux termes de l'article L. 222-6 du même code : " Les décisions sont motivées, notifiées à l'intéressé et publiées au Journal officiel ". Enfin, aux termes de l'article R. 222-12 du même code : " Les décisions du ministre chargé de l'énergie prononçant les sanctions prévues à l'article L. 222-2 peuvent faire l'objet d'un recours de pleine juridiction (...) devant le Conseil d'Etat (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 221-7 de ce code dans sa version applicable au litige : " Le ministre chargé de l'énergie ou, en son nom, un organisme habilité à cet effet peut délivrer des certificats d'économies d'énergie aux personnes éligibles lorsque leur action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie sur le territoire national d'un volume supérieur à un seuil fixé par arrêté du ministre chargé de l'énergie. / Sont éligibles : / 1° Les personnes mentionnées à l'article L. 221-1 (...) ". Aux termes de l'article R. 222-7 du code de l'énergie : " Le ministre chargé de l'énergie notifie au premier détenteur de certificats d'économies d'énergie la liste des opérations visées par le contrôle ou le périmètre du contrôle, qui peut être défini par l'intitulé et la référence d'une opération standardisée, la catégorie des bénéficiaires des économies d'énergie, une zone géographique correspondant à un ou plusieurs départements, une période d'engagement d'opérations d'économies d'énergie ou une période de délivrance de certificats (...) ". Aux termes de l'article R. 222-8 du même code : " Pour chaque opération d'économies d'énergie de l'échantillon mentionné à l'article R. 222-7, le ministre chargé de l'énergie établit le volume de certificats d'économies d'énergie correspondant. Si le ministre ne constate aucun manquement dans les éléments nécessaires à l'établissement de ce volume et si le volume de certificats d'économies d'énergie qu'il établit n'est pas inférieur à celui qui a été attribué, le volume de certificats d'économies d'énergie délivrés pour l'opération est confirmé. Dans tous les autres cas, il est ramené à zéro (...) / La conformité de l'échantillon s'apprécie à partir de la somme des volumes de certificats d'économies d'énergie de chacune de ses opérations, établis conformément aux dispositions des deux premiers alinéas du présent article. L'échantillon est réputé conforme si le rapport entre la somme des volumes de certificats d'économies d'énergie établis pour les opérations de l'échantillon et la somme des volumes de certificats d'économies d'énergie délivrés pour les mêmes opérations est : (...) / 2° Pour les opérations engagées à partir du 1er janvier 2013, supérieur à 95 % ". Aux termes de l'article R. 222-9 du même code : " Lorsque l'échantillon n'est pas réputé conforme, le ministre chargé de l'énergie met en demeure l'intéressé de transmettre, dans un délai d'un mois, les preuves de la conformité réglementaire des opérations d'économies d'énergie pour lesquelles des manquements ont été constatés (...) ". Aux termes de l'article R. 222-10 du même code : " Si les preuves de la conformité réglementaire mentionnées à l'article R. 222-9 ne sont pas apportées dans le délai imparti ou si les pièces produites ne permettent pas de rendre conforme l'échantillon dans les conditions prévues à l'article R. 222-8, le ministre chargé de l'énergie peut prononcer les sanctions prévues à l'article L. 222-2 (...) ".<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. La société Butagaz demande, à l'appui de sa requête, que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 222-1 et L. 222-2 du code de l'énergie, ainsi que des articles L. 221-1 à L. 221-12 du même code auxquels renvoie ledit article L. 222-1, dans leur version issue de la loi n° 2015-992 du 17 août 2015. Il soutient que ces dispositions, en ce qu'elles ne définissent pas de manière suffisamment précise les manquements aux obligations d'économies d'énergie susceptibles d'être sanctionnés en application de l'article L. 222-2, méconnaissent le principe de légalité des délits et des peines, garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              6. Toutefois, les exigences qui découlent du principe à valeur constitutionnelle de légalité des délits et des peines, appliqué en dehors du droit pénal, se trouvent satisfaites, en matière administrative, par la référence aux obligations auxquelles l'intéressé est soumis en vertu des lois et règlements en raison de l'activité qu'il exerce, de la profession à laquelle il appartient, de l'institution dont il relève ou de la qualité qu'il revêt.  Il résulte de l'article L. 222-1 du code de l'énergie que les sanctions définies à l'article L. 222-2 du même code sont prononcées par le ministre chargé de l'énergie en cas de manquement aux dispositions du chapitre Ier du titre II du livre II du même code ou aux dispositions réglementaires prises pour leur application. Les dispositions de ce chapitre définissent les modalités selon lesquelles les personnes morales qui soit mettent à la consommation des carburants automobiles soit vendent de l'électricité, du gaz, du fioul domestique, de la chaleur ou du froid aux consommateurs finals doivent s'acquitter de leurs obligations d'économies d'énergie et peuvent obtenir, à ce titre, des certificats  lorsque leur action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie sur le territoire national d'un volume supérieur à un seuil fixé par arrêté. Il suit de là que les dispositions litigieuses font référence aux obligations auxquelles les personnes morales sont soumises en raison de l'activité qu'elles exercent.<br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, laquelle n'est pas nouvelle, que le moyen tiré de ce que les dispositions des articles L. 222-1 et L. 222-2 du code de l'énergie, ainsi que des articles L. 221-1 à L. 221-12 du même code auxquels renvoie ledit article L. 222-1 portent atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              En ce qui concerne la régularité de la procédure de sanction :<br/>
<br/>
              8. En premier lieu, si le principe d'impartialité est un principe général du droit s'imposant à tous les organismes administratifs, il n'implique pas qu'il soit procédé à une séparation des fonctions d'instruction et de sanction au sein du pôle national des certificats d'économies d'énergie, qui est un service à compétence nationale placé sous l'autorité du ministre chargé de l'énergie, au nom duquel sont prononcées les décisions sanctionnant les infractions relatives aux CEE, sans d'ailleurs faire intervenir aucun organe à caractère collégial.<br/>
<br/>
              9. En second lieu, les termes des courriers adressés à la requérante par l'administration, notamment la lettre du 18 décembre 2018 la mettant en demeure de se conformer à ses obligations dans un délai d'un mois et la lettre du 27 mars 2019 l'invitant à présenter dans le même délai ses observations sur les sanctions envisagées à son encontre, ne révèlent aucun parti pris défavorable à la requérante, sans que cette dernière puisse utilement faire valoir la circonstance que ces courriers réitèrent les mêmes analyses au sujet des opérations contrôlées. La requérante n'est donc pas fondée à soutenir que la décision attaquée aurait, pour ce motif, été prise en méconnaissance du principe d'impartialité, des droits de la défense ainsi que des dispositions des articles L. 222-3 et L. 222-5 du code de l'énergie précités.<br/>
<br/>
              En ce qui concerne la légalité des textes qui constituent le fondement de la sanction :<br/>
<br/>
              10. En premier lieu, il résulte des articles R. 222-7 à R. 222-10 du code de l'énergie cités ci-dessus que, si le ministre chargé de l'énergie peut librement déterminer, parmi les opérations d'économies d'énergie ayant donné lieu à la délivrance de certificats, celles faisant l'objet du contrôle, la non-conformité de cet " échantillon " au sens de ces dispositions, est appréciée à raison de la part du volume des certificats pour lesquels des manquements ont été constatés et les résultats du contrôle ne font l'objet d'aucune extrapolation au titre des certificats non compris dans le champ du contrôle, la sanction pécuniaire susceptible d'être prononcée en application de l'article L. 222-2 du même code ne pouvant elle-même excéder un plafond défini à proportion du nombre de certificats concernés par le manquement constaté. Par suite, la société Butagaz n'est pas fondée à soutenir que les dispositions réglementaires régissant la constitution et le traitement de l'échantillonnage ne seraient pas définies de manière suffisamment précise et méconnaîtraient de ce fait le principe de légalité des délits.<br/>
<br/>
              11. En deuxième lieu, l'article L. 222-2 du code de l'énergie dispose que le ministre chargé de l'énergie peut sanctionner les manquements aux dispositions du chapitre Ier du titre II du livre II de ce code ou aux dispositions réglementaires prises pour leur application. Ce chapitre prévoit la délivrance de certificats d'économies d'énergie aux personnes éligibles lorsque leur action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie sur le territoire national d'un volume supérieur à un seuil fixé par arrêté du ministre chargé de l'énergie, l'article L. 221-12 renvoyant à un décret en Conseil d'Etat le soin de préciser ses modalités d'application. Contrairement à ce que soutient la société requérante, le pouvoir réglementaire n'a donc pas excédé les limites de l'habilitation qui lui a été ainsi donnée en fixant les modalités du contrôle de la régularité de la délivrance des certificats d'économies d'énergie et notamment, aux articles R. 222-7 à R. 222-10 du même code, celles relatives à la détermination d'un échantillon servant de base au contrôle et aux conditions dans lesquelles doit être appréciée la non-conformité de cet échantillon.  <br/>
<br/>
              12. En troisième lieu, la requérante soutient que les dispositions réglementaires des fiches standardisées d'opérations d'économies d'énergie seraient contraires au droit de de l'Union et à la directive 2006/123/CE, en tant qu'elles imposent aux prestataires des travaux d'être titulaires d'un signe de qualité répondant à certaines exigences (certification " RGE "). Toutefois, elle n'assortit pas son moyen des précisions suffisantes permettant d'en apprécier le bien-fondé.<br/>
<br/>
               En ce qui concerne les autres moyens relatifs au bien-fondé de la sanction :<br/>
<br/>
              13. En premier lieu, l'article R. 222-6 du code de l'énergie définit comme un manquement le fait pour un premier détenteur de certificats d'économies d'énergie d'avoir obtenu des certificats sans avoir respecté les dispositions de la section 2 du chapitre Ier du titre II du livre II de ce code, notamment celles relatives aux opérations standardisées mentionnées à l'article R. 221-14 ou celles relatives à la composition d'une demande de certificats d'économies d'énergie mentionnées à l'article R. 221-22 du même code. La société Butagaz n'est donc pas fondée à soutenir qu'elle ne serait tenue par aucun texte de justifier de la conformité réglementaire des opérations d'économies d'énergie pour lesquelles des manquements ont été constatés, sans pouvoir utilement faire valoir qu'elle ne réalise pas elle-même les travaux donnant lieu à la délivrance des certificats ou qu'elle ne dispose pas d'un réseau d'agents couvrant l'ensemble du territoire.<br/>
<br/>
              14. En deuxième lieu, il résulte de l'instruction que l'administration s'est fondée, pour prononcer la sanction attaquée, sur les manquements affectant dix-sept opérations d'économies d'énergie qui résultaient, pour l'une, de l'absence de réalisation des travaux d'isolation déclarés et, pour les autres, de ce qu'aucune des entreprises désignées comme sous-traitantes de la société maître d'oeuvre ADW Confort n'avait réalisé de travaux auprès des particuliers en cause. Si la requérante conteste l'existence de manquements au titre de cinq de ces opérations, elle n'apporte, ainsi qu'il appartient à elle seule de le faire, aucun élément de preuve susceptible d'étayer son affirmation, alors du reste qu'elle reconnaît dans sa plainte pour faux, usage de faux et escroquerie déposée auprès du tribunal de grande instance de Nanterre le 12 juillet 2019 qu'aucun des sous-traitants désignés n'avait effectué de travaux pour le compte de la société ADW Confort.<br/>
<br/>
              15. En troisième lieu, il résulte de l'instruction que la lettre du 9 juillet 2019 notifiant les sanctions prononcées à l'encontre de la société Butagaz fait référence à un courriel du 30 avril 2019 par lequel cette société indiquait prendre acte de la non-conformité des opérations contrôlées et accepter l'annulation du volume de CEE correspondant. Si la requérante soutient que ce courriel se rapporterait à d'autres opérations que celles faisant l'objet du contrôle, cette erreur, à la supposer avérée, est restée sans incidence sur le bien-fondé de la sanction attaquée, dès lors, d'une part, qu'il n'est pas contesté que les manquements reprochés à la société pour fonder la sanction attaquée se rapportent bien aux seules opérations faisant l'objet du présent litige et, d'autre part, que le ministre n'a fait référence à ce courriel qu'au soutien de la sanction liée à l'annulation des certificats, laquelle n'a effectivement pas été contestée par la requérante.<br/>
<br/>
              En ce qui concerne le montant de la sanction :<br/>
<br/>
              16. Si le principe de proportionnalité des peines, garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen, implique que lorsque plusieurs sanctions prononcées pour un même fait sont susceptibles de se cumuler, le montant global des sanctions éventuellement prononcées ne dépasse pas le montant le plus élevé de l'une des sanctions encourues, ce principe ne peut être utilement invoqué lorsque les sanctions sont de nature différente. La requérante n'est donc pas fondée à soutenir que la sanction pécuniaire qu'elle conteste serait contraire à ce principe au motif que le ministre aurait également prononcé une sanction de nature différente tenant à l'annulation d'un volume de certificats d'économies d'énergie égal au volume concerné par les manquements constatés.<br/>
<br/>
              17. Enfin, il résulte de l'instruction que le montant de la sanction prononcée correspond à la valeur du volume des certificats liés aux opérations ayant fait l'objet des manquements constatés. Pour contester ce montant, la requérante fait valoir qu'elle a été victime d'une escroquerie, qu'elle n'aurait tiré aucun avantage des manoeuvres frauduleuses et qu'elle avait délégué à un prestataire le soin de constituer les dossiers de demandes de CEE. Toutefois, ces circonstances, qui ne peuvent être de nature à atténuer le manquement de la société aux obligations, qui n'incombent qu'à elle seule, prescrites pour la délivrance des CEE, ne suffisent pas, compte tenu de la nature et de la gravité des manquements constatés ainsi que du nombre d'opérations concernées, et alors même que la société aurait coopéré de bonne foi avec l'administration durant la procédure de contrôle, à regarder la sanction de 99 039 euros prononcée à l'encontre de la société Butagaz comme disproportionnée. <br/>
<br/>
              18. Il résulte de tout ce qui précède que la société Butagaz n'est pas fondée à demander tant l'annulation que la réformation de la décision qu'elle attaque. Sa requête doit, par suite, être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Butagaz.<br/>
Article 2 : La requête de la société Butagaz est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la société Butagaz et à la ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
