<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461542</ID>
<ANCIEN_ID>JG_L_2018_09_000000424180</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/09/2018, 424180, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424180</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:424180.20180927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Poitiers, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au directeur de l'Office français de l'immigration et de l'intégration (OFII) de prendre les mesures nécessaires, dans un délai de 48 heures à compter de la notification de l'ordonnance à intervenir et sous astreinte de 50 euros par jour de retard, afin que le versement de l'allocation pour demandeur d'asile soit régularisé et qu'il soit maintenu en centre d'accueil. Par une ordonnance n° 1801855 du 24 août 2018, le juge des référés du tribunal administratif de Poitiers a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 14 septembre 2018 au secrétariat du contentieux, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit à sa demande de première instance ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat au profit de son conseil, la SCP Spinosi, Sureau, qui renoncera alors à percevoir la part contributive de l'Etat, le versement d'une somme de 3 000 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - le juge des référés du tribunal administratif de Poitiers a commis une erreur de droit en estimant que la condition d'urgence n'était pas remplie alors que les décisions du 24 juillet 2018 par lesquelles l'Office français de l'immigration et de l'intégration lui a retiré l'intégralité des conditions matérielles d'accueil et a prononcé sa sortie du centre d'accueil et d'orientation " Vivre au Peux ", le placent dans une situation de grande précarité ;<br/>
              - les décisions de l'OFII du 24 juillet 2018 portent une atteinte grave et manifestement illégale au droit d'asile et au droit corrélatif d'accès à des conditions matérielles d'accueil décentes dès lors qu'elles sont, d'une part, prises sur le fondement d'une fraude dont il n'est pas démontré le caractère intentionnel et, d'autre part, entachées d'une erreur manifeste d'appréciation en ce qu'elles ne prennent pas en compte sa vulnérabilité.<br/>
              Par une intervention enregistrée le 14 septembre 2018, l'association La Cimade demande au Conseil d'Etat de faire droit aux conclusions de la requête. Elle se réfère aux moyens exposés dans celle-ci et soutient, en outre, que l'introduction d'une nouvelle demande d'asile après exécution d'une décision de transfert ne constitue pas en elle-même une fraude, une dissimulation ou des déclarations mensongères. <br/>
              Par un mémoire en défense, enregistré le 20 septembre 2018, l'Office français de l'immigration et de l'intégration conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
              La requête a été transmise au ministre de l'intérieur qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2003/9/UE du Conseil du 27 janvier 2003 ;<br/>
              - la décision de la Cour de justice de l'Union européenne du 27 septembre 2012 CIMADE et GISTI, c-179/11 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, M. A... et, d'autre part, l'Office français de l'immigration et de l'intégration et le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du vendredi 21 septembre 2018 à 10 heures au cours de laquelle ont été entendus : <br/>
              - Me Gury, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
              - les représentants de l'Office français de l'immigration et de l'intégration ;<br/>
              - le représentant de l'association La Cimade ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au vendredi 21 février à 17 heures, puis à 19 heures ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Il résulte de l'instruction que M.A..., qui déclare être de nationalité soudanaise et être né le 10 octobre 1992, a présenté une première demande d'asile le 27 avril 2017 sur le territoire français qui a été enregistrée en procédure dite Dublin. Après avoir été transféré en Italie le 1er mars 2018, en application de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, il est revenu en France le 7 mars 2018. Le 4 avril 2018, il a présenté une seconde demande d'asile, également enregistrée en procédure dite Dublin. A partir du même jour, l'intéressé a bénéficié des conditions matérielles d'accueil prévues par l'article L. 744-1 du même code en étant hébergé dans un centre d'accueil et d'orientation. Toutefois, le directeur territorialement compétent de l'OFII lui a notifié son intention de lui refuser ces droits. Par deux décisions du 24 juillet 2018, prises après échange contradictoire, le même directeur a, d'une part, retiré les conditions matérielles d'accueil de M. A..., et d'autre part, ordonné par voie de conséquence sa sortie du centre d'accueil. L'intéressé relève appel de l'ordonnance du 24 août 2018, par laquelle le juge des référés du tribunal administratif de Poitiers a rejeté sa demande tendant à ce qu'il soit enjoint, en application de l'article L. 521-2 du code de justice administrative, au directeur territorial de l'OFII de prendre les mesures nécessaires, dans un délai de 48 heures et sous une astreinte de 50 euros par jour de retard, afin que le versement de l'allocation pour demandeur d'asile soit régularisé et qu'il soit maintenu en centre d'accueil. <br/>
<br/>
              Sur l'intervention de l'association La Cimade :<br/>
<br/>
              3. L'association La Cimade, qui intervient au soutien des conclusions de la requête justifie, eu égard à son objet statutaire et à la nature du litige, d'un intérêt suffisant pour intervenir dans la présente instance. Son intervention est, par suite, recevable.<br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              4. Il n'est pas contesté que le requérant ne dispose d'aucune ressource et que, depuis le 24 juillet 2018, il n'a pas d'hébergement. Ainsi, alors même qu'il est célibataire et âgé de 25 ans et qu'il ne présente pas une vulnérabilité impliquant des besoins particuliers au sens et pour l'application de l'article L. 744-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, il est fondé à soutenir que c'est à tort, que par l'ordonnance attaquée, le juge des référés du tribunal administratif de Poitiers a estimé que la condition d'urgence prévue par l'article L. 521-2 du code de justice administrative n'était pas remplie.<br/>
<br/>
              Sur l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de familiale. Il incombe au juge des référés d'apprécier, dans chaque situation, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation familiale de la personne intéressée.<br/>
<br/>
              6. Aux termes de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le bénéfice des conditions matérielles d'accueil peut être : (...) 3°Refusé si le demandeur présente une demande de réexamen de sa demande d'asile (...) ". Aux termes de l'article D. 744-34 du même code : " Le versement de l'allocation prend fin, sur demande de l'Office français de l'immigration et de l'intégration : (...) 2° A compter de la date du transfert effectif à destination de l'Etat responsable de l'examen de la demande d'asile ". Aux termes de l'article D. 744-37 du même code : " Le bénéfice de l'allocation pour demandeur d'asile peut être refusé par l'Office français de l'immigration et de l'intégration : 1° En cas de demande de réexamen de la demande d'asile ; (...) 3° En cas de fraude ".<br/>
<br/>
              7. Il résulte de ces dispositions, ainsi que de celles de la directive du Conseil du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les Etats membres, qu'elles visent à transposer et qui ont notamment été interprétées par la décision de la Cour de justice de l'Union européenne du 27 septembre 2012 CIMADE et GISTI c-179/11, que lorsqu'un demandeur d'asile a été transféré vers l'Etat responsable de l'examen de sa demande, c'est à ce dernier de lui assurer les conditions matérielles d'accueil. En cas de retour de l'intéressé en France sans que la demande n'ait été examinée et de présentation d'une nouvelle demande, l'OFII peut refuser le bénéfice de ces droits, sauf si les autorités en charge de cette nouvelle demande décident de l'examiner ou si, compte tenu du refus de l'Etat responsable d'examiner la demande précédente, il leur revient de le faire.<br/>
<br/>
              8. Il résulte de l'instruction que, dans les observations qu'il a adressées à l'OFII en réponse à l'intention de ce dernier de lui refuser le bénéfice des conditions matérielles d'accueil, le requérant a indiqué : " En Italie, j'ai été réceptionné par des policiers qui, après m'avoir fait attendre plusieurs jours, m'ont demandé de quitter les lieux sans m'avoir donné de documents ". Ni l'OFII, ni le ministre de l'intérieur qui a été appelé à présenter ses observations lors de la présente instance et à qui il appartenait de se rapprocher des autorités italiennes, n'ont présenté le moindre élément de nature à infirmer les dires du requérant selon lesquels ces autorités avaient refusé d'examiner sa demande d'asile. Dès lors et alors même que la nouvelle demande d'asile de l'intéressé n'a été enregistrée qu'en procédure dite Dublin, le refus de l'OFII de lui accorder les conditions matérielles d'accueil et la décision prise, par voie de conséquence, d'ordonner sa sortie du centre d'accueil sont entachés d'une illégalité manifeste.<br/>
<br/>
              9. Si, eu égard à la saturation actuelle du dispositif d'hébergement des demandeurs d'asile et à la situation personnelle du requérant, sa sortie du centre d'accueil, qui a été envisagée dès le premier jour où il a été accueilli, ne peut être regardée comme ayant eu pour lui des conséquences graves au sens et pour l'application de l'article L. 521-2 du code de justice administrative, il n'en va pas de même de la perte de l'allocation pour demandeur d'asile qui est prévue par l'article L. 744-9 du code de l'entrée et du séjour des étrangers et du droit d'asile et qui comprend, dans son cas, le montant additionnel versé en cas d'absence d'hébergement.<br/>
<br/>
              10. Il résulte de ce qui précède que le requérant est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Poitiers a rejeté ses conclusions tendant à ce qu'il soit enjoint au directeur de l'OFII de régulariser, à compter du 24 juillet 2018, le versement de l'allocation pour demandeur d'asile. Il y a lieu de fixer à sept jours le délai dans lequel cette injonction devra être exécutée sans l'assortir, à ce stade, de l'astreinte sollicitée.<br/>
<br/>
              Sur les frais irrépétibles : <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, et sans qu'il soit besoin d'admettre provisoirement M. A...au bénéfice de l'aide juridictionnelle, de mettre à la charge de l'Office français de l'immigration et de l'intégration, à l'encontre duquel les conclusions présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative doivent être regardées comme dirigées, la somme de 1000 euros. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association La Cimade est admise.<br/>
Article 2 : Il est enjoint à l'Office français de l'immigration et de l'intégration de procéder au versement à M. A...de l'allocation pour demandeur d'asile avec effet au 24 juillet 2018, dans un délai de sept jours à compter de la notification de la présente ordonnance.<br/>
Article 3 : L'ordonnance du juge des référés du tribunal administratif de Poitiers du 24 août 2018 est réformée en ce qu'elle a de contraire à la présente décision.<br/>
Article 4 : L'Office français de l'immigration et de l'intégration versera une somme de 1 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête de M. A...est rejeté.<br/>
Article 6 : La présente ordonnance sera notifiée à M. B...A...et à l'Office français de l'immigration et de l'intégration.<br/>
Copie en sera adressée pour information au ministre de l'intérieur et à l'association La Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
