<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036064423</ID>
<ANCIEN_ID>JG_L_2017_11_000000396595</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/06/44/CETATEXT000036064423.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/11/2017, 396595, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396595</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396595.20171120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SA Natixis  a demandé au tribunal administratif de Cergy-Pontoise de prononcer la restitution partielle des cotisations d'impôt sur les sociétés qu'elle a acquittées au titre des années 1998 et 1999. Après avoir ordonné une mesure d'instruction par un jugement avant dire droit du 13 juillet 2010, le tribunal administratif de Cergy-Pontoise, par un jugement n° 0503955 du 21 décembre 2010, a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE00684 du 17 décembre 2015, la cour administrative d'appel de Versailles a accordé à la SA Natixis la restitution des impositions correspondant à la différence entre les crédits d'impôt prévus à l'article 22 de l'accord du 30 mai 1984 signé entre la France et la Chine tels qu'ils ont été calculés initialement et ceux qui résultent de son arrêt, et réformé ce jugement en ce qu'il a de contraire.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 29 janvier 2016 et le 16 février 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'accord conclu entre la France et la Chine le 30 mai 1984 ; <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Natixis ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 10 de l'accord signé le 30 mai 1984 entre la France et la Chine en vue d'éviter les doubles impositions et de prévenir l'évasion fiscale en matière d'impôts sur le revenu : " 1. Les intérêts provenant d'un Etat contractant et payés à un résident de l'autre Etat contractant sont imposables dans cet autre Etat. 2. Toutefois, ces intérêts sont aussi imposables dans l'Etat contractant d'où ils proviennent et selon la législation de cet Etat, mais si la personne qui reçoit les intérêts en est le bénéficiaire effectif, l'impôt ainsi établi ne peut excéder 10 % du montant brut des intérêts ". L'article 22 de cet accord stipule par ailleurs que : " La double imposition est évitée de la manière suivante pour les deux Etats contractants : (...) 2. En ce qui concerne la République française : (...) b. Les revenus visés aux articles (...) 10 (...) provenant de Chine sont imposables en France, conformément aux dispositions de ces articles, pour leur montant brut. Il est accordé aux résidents de France un crédit d'impôt français correspondant au montant de l'impôt chinois perçu sur ces revenus mais qui ne peut excéder le montant de l'impôt français afférent à ces revenus. c. Aux fins de l'alinéa b et en ce qui concerne les éléments de revenus visés aux articles (...) 10 (...), le montant de l'impôt chinois perçu est considéré comme étant égal à (...) 10  % sur les intérêts (...), du montant brut de ces éléments de revenu (...) ". <br/>
<br/>
              2. Il résulte de la combinaison de ces stipulations que les résidents de France ayant reçu des intérêts de source chinoise bénéficient, lors de leur imposition en France, d'un crédit d'impôt égal au montant de l'impôt chinois perçu sur ces revenus, lequel est fixé forfaitairement à 10 % du montant brut des intérêts perçus. En jugeant que ce crédit d'impôt doit être déterminé à partir d'un montant brut des revenus reconstitué par ajout aux intérêts versés de l'impôt chinois réputé acquitté, la cour n'a pas commis une erreur de droit. Par suite, le ministre des finances et des comptes publics n'est pas  fondé à demander l'annulation de l'arrêt attaqué. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à la SA Natixis la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la SA Natixis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
