<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042043629</ID>
<ANCIEN_ID>JG_L_2020_06_000000423455</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/36/CETATEXT000042043629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 25/06/2020, 423455</TITRE>
<DATE_DEC>2020-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423455</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423455.20200625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... B... ont demandé au tribunal administratif de Toulouse d'annuler la décision implicite par laquelle le maire de Prades d'Aubrac (Aveyron) a rejeté leur demande d'attribution de terres agricoles situées, dans cette commune, au sein de la section de Born, présentée sur le fondement de l'article L. 2411-10 du code général des collectivités territoriales et d'enjoindre à la commune de leur attribuer les terres sollicitées ou de réexaminer leur demande. <br/>
<br/>
              Par un jugement n ° 1304589 du 9 mars 2016, le tribunal administratif de Toulouse a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16BX01600 du 22 juin 2018, la cour administrative d'appel de Bordeaux a rejeté l'appel qu'ils ont formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 août et 22 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Prades d'Aubrac la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. et Mme B... et à la SCP Célice, Texidor, Perier, avocat de la commune de Prades d'Aubrac ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B..., qui exploitent un élevage de bovins au hameau de Belnom, dans la commune de Prades d'Aubrac, ont demandé, le 7 mai 2012, l'attribution de terres agricoles situées dans la section de Born de cette commune, sur le fondement de l'article L. 2411-10 du code général des collectivités territoriales. Par un jugement du 9 mars 2016, le tribunal administratif de Toulouse a rejeté leur demande tendant à l'annulation de la décision implicite de rejet de cette demande par le maire de Prades d'Aubrac et à ce qu'il soit enjoint à la commune de leur attribuer les terres sollicitées ou de réexaminer leur demande. Les intéressés se pourvoient en cassation contre l'arrêt du 22 juin 2018 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'ils avaient formé contre ce jugement.<br/>
<br/>
              2. Aux termes, d'une part, des deuxième, troisième et quatrième alinéas de l'article L. 2411-10 du code général des collectivités territoriales dans leur rédaction applicable au litige  : " Les terres à vocation agricole ou pastorale propriétés de la section sont attribuées par bail rural ou par convention pluriannuelle de pâturage conclue dans les conditions prévues à l'article L. 481-1 du code rural et de la pêche maritime ou par convention de mise à disposition d'une société d'aménagement foncier et d'établissement rural au profit des exploitants agricoles ayant un domicile réel et fixe, ainsi que le siège d'exploitation sur la section. L'autorité municipale peut attribuer, le cas échéant, le reliquat de ces biens au profit d'exploitants agricoles sur la section ayant un bâtiment d'exploitation hébergeant pendant la période hivernale leurs animaux sur la section, ou à défaut au profit de personnes exploitant des biens sur le territoire de la section et résidant sur le territoire de la commune ; à titre subsidiaire, elle peut attribuer ce reliquat au profit de personnes exploitant seulement des biens sur le territoire de la section ou, à défaut, au profit des exploitants ayant un bâtiment d'exploitation sur le territoire de la commune. / Pour toutes les catégories précitées, les exploitants devront remplir les conditions prévues par les articles L. 331-2 à L. 331-5 du code rural et de la pêche maritime et celles prévues par le règlement d'attribution défini par l'autorité municipale. / Le fait de ne plus remplir les conditions énoncées ci-dessus entraîne de plein droit la résiliation des contrats ". <br/>
<br/>
              3. Aux termes, d'autre part, du I de l'article L. 331-2 du code rural et de la pêche maritime dans sa rédaction applicable au litige  : " Sont soumises à autorisation préalable les opérations suivantes : / 1° Les installations, les agrandissements ou les réunions d'exploitations agricoles au bénéfice d'une exploitation agricole mise en valeur par une ou plusieurs personnes physiques ou morales, lorsque la surface totale qu'il est envisagé de mettre en valeur excède le seuil fixé par le schéma directeur départemental des structures. (...) ". Aux termes de l'article L. 331-3 du même code dans sa rédaction applicable au litige : " L'autorité administrative se prononce sur la demande d'autorisation en se conformant aux orientations définies par le schéma directeur départemental des structures agricoles applicable dans le département dans lequel se situe le fonds faisant l'objet de la demande. Elle doit notamment : / 1° Observer l'ordre des priorités établi par le schéma départemental entre l'installation des jeunes agriculteurs et l'agrandissement des exploitations agricoles, en tenant compte de l'intérêt économique et social du maintien de l'autonomie de l'exploitation faisant l'objet de la demande ; / ... ".<br/>
<br/>
              4. Si les dispositions de l'article L. 2411-10 du code général des collectivités territoriales, citées au point 2, prévoient que l'autorisation à laquelle est soumise, le cas échéant, en vertu des dispositions du code rural et de la pêche maritime citées au point 3, l'exploitation de terres à vocation agricole ou pastorale appartenant à une section de commune par la ou les personnes qui en demandent l'attribution soit obtenue par le pétitionnaire à la date de conclusion du bail rural, de la convention pluriannuelle de pâturage ou de la convention de mise à disposition des terres en cause, elles n'exigent pas que cette autorisation soit délivrée au pétitionnaire avant que l'autorité compétente ne choisisse l'attributaire de ces terres ou ne classe les demandes d'attribution au regard des priorités qu'elles énoncent.<br/>
<br/>
              5. Par suite, après avoir relevé qu'il n'était pas contesté que l'attribution des terres agricoles et pastorales sollicitées par les requérants aurait eu pour effet un agrandissement de leur exploitation au-delà du seuil de 50 hectares fixé par le schéma départemental des structures d'Aveyron nécessitant, en vertu des dispositions précitées du I de l'article L. 331-2 du code rural et de la pêche maritime, une autorisation préalable d'exploiter, la cour a commis une erreur de droit en jugeant que le maire de la commune de Prades d'Aubrac n'avait pas méconnu l'article L. 2411-10 du code général des collectivités territoriales en rejetant la demande d'attribution de terres agricoles et pastorales présentée par les époux B... au seul motif que ces derniers n'avaient pas encore obtenu cette autorisation. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, M. et Mme B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Prades d'Aubrac et de la section de Born la somme globale de 3 000 euros à verser à M. et à Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise à la charge de M. et à Mme B..., qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt n° 16BX01600 du 22 juin 2018 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Prades d'Aubrac et la section de Born verseront à M. et Mme B... la somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la commune de Prades d'Aubrac et par la section de Born au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme A... B..., à la commune de Prades d'Aubrac et à la section de Born.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. - ATTRIBUTION DE TERRES APPARTENANT À UNE SECTION DE COMMUNE (ART. L. 2411-10 DU CGCT) - CONDITION - OBTENTION PRÉALABLE DE L'AUTORISATION D'EXPLOITER LES TERRES (I DE L'ART. L. 331-2 DU CRPM) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-02-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. INTÉRÊTS PROPRES À CERTAINES CATÉGORIES D'HABITANTS. SECTIONS DE COMMUNE. - ATTRIBUTION DE TERRES APPARTENANT À UNE SECTION DE COMMUNE (ART. L. 2411-10 DU CGCT) - CONDITION - OBTENTION PRÉALABLE DE L'AUTORISATION D'EXPLOITER LES TERRES (I DE L'ART. L. 331-2 DU CRPM) - ABSENCE.
</SCT>
<ANA ID="9A"> 03-03 Si les deuxième, troisième et quatrième alinéas de l'article L. 2411-10 du code général des collectivités territoriales (CGCT) prévoient que l'autorisation à laquelle est soumise, le cas échéant, en vertu du I de l'article L. 331-2 et de l'article L. 331-3 du code rural et de la pêche maritime (CRPM), l'exploitation de terres à vocation agricole ou pastorale appartenant à une section de commune par la ou les personnes qui en demandent l'attribution soit obtenue par le pétitionnaire à la date de conclusion du bail rural, de la convention pluriannuelle de pâturage ou de la convention de mise à disposition des terres en cause, ils n'exigent pas que cette autorisation soit délivrée au pétitionnaire avant que l'autorité compétente ne choisisse l'attributaire de ces terres ou ne classe les demandes d'attribution au regard des priorités qu'ils énoncent.</ANA>
<ANA ID="9B"> 135-02-02-03-01 Si les deuxième, troisième et quatrième alinéas de l'article L. 2411-10 du code général des collectivités territoriales (CGCT) prévoient que l'autorisation à laquelle est soumise, le cas échéant, en vertu du I de l'article L. 331-2 et de l'article L. 331-3 du code rural et de la pêche maritime (CRPM), l'exploitation de terres à vocation agricole ou pastorale appartenant à une section de commune par la ou les personnes qui en demandent l'attribution soit obtenue par le pétitionnaire à la date de conclusion du bail rural, de la convention pluriannuelle de pâturage ou de la convention de mise à disposition des terres en cause, ils n'exigent pas que cette autorisation soit délivrée au pétitionnaire avant que l'autorité compétente ne choisisse l'attributaire de ces terres ou ne classe les demandes d'attribution au regard des priorités qu'ils énoncent.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
