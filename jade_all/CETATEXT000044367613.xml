<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367613</ID>
<ANCIEN_ID>JG_L_2021_07_000000453290</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367613.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/07/2021, 453290, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453290</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:453290.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Par une requête, un mémoire complémentaire et un nouveau mémoire, enregistrés les 4 et 16 juin 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... doit être regardé comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret n° 2021-699 du 1er juin 2021 en tant qu'il soumet l'entrée sur le territoire métropolitain des ressortissants français présents au Royaume-Uni à la justification d'un résultat d'un test ou d'un examen biologique de dépistage virologique réalisé sur le territoire britannique ou irlandais moins de 48 heures avant l'embarquement et ne concluant pas à une contamination par la Covid-19.<br/>
<br/>
<br/>
               Il soutient que : <br/>
               - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de ce litige ; <br/>
               - il justifie d'un intérêt à agir contre ces dispositions ; <br/>
               - la condition d'urgence est satisfaite dès lors que, d'une part, l'accès au territoire français par les ressortissants français présents au Royaume-Uni est conditionné à la présentation d'un résultat négatif à une contamination par la Covid-19 à un test ou un examen biologique de dépistage virologique réalise´ sur le territoire britannique ou irlandais moins de 48 heures avant l'embarquement et, d'autre part, il souhaite entrer sur le territoire français le 10 juillet 2021 ; <br/>
               - il existe un doute sérieux quant à la légalité des dispositions contestées ; <br/>
               - elles ne sont pas strictement proportionnées aux risques sanitaires encourus, ni appropriées aux circonstances de temps et de lieu, en méconnaissance de l'article L. 3131-15 du code de la santé publique dès lors que la présentation d'une attestation de vaccination par les ressortissants français présents dans un pays classé en zone orange ou rouge ne permet pas, au même titre que le résultat d'un test ou d'un examen biologique de dépistage virologique, d'entrer sur le territoire français ;<br/>
               - elles méconnaissent le principe d'égalité entre les citoyens français dès lors que la réalisation d'un test ou d'un examen biologique de dépistage virologique effectué sur le territoire britannique ou irlandais est onéreux, contrairement à la vaccination ; <br/>
               - elles méconnaissent le droit pour un citoyen d'entrer sur le territoire du pays dont il a la nationalité. <br/>
<br/>
               Par un mémoire distinct, enregistré le 17 juin 2021, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. Cornut de Lafontaine de Coincy demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de la loi n° 2021-689 du 31 mai 2021. Il soutient que cet article est applicable au litige, porte atteinte aux droits et libertés garantis par la Constitution et méconnaît l'article 72-3 de la Constitution en ce qu'elle restreint l'entrée et le séjour sur le territoire par des mesures de police administrative pour les étrangers et les nationaux.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - l'ordonnance du 7 novembre 1958 ; <br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article R. 522-2 : " A peine d'irrecevabilité, les conclusions tendant à la suspension d'une décision administrative ou de certains de ses effets doivent être présentées par requête distincte de la requête à fin d'annulation ou de réformation et accompagnées d'une copie de cette dernière ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
               2. M. Cornut de Lafontaine de Coincy doit être regardé comme demandant au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret n° 2021-699 du 1er juin 2021 en tant qu'il soumet l'entrée sur le territoire métropolitain des ressortissants français présents au Royaume-Uni à la justification d'un résultat d'un test ou d'un examen biologique de dépistage virologique réalisé sur le territoire britannique ou irlandais moins de 48 heures avant l'embarquement et ne concluant pas à une contamination par la Covid-19. Toutefois, il ne ressort pas des pièces du dossier que le requérant a introduit devant le Conseil d'Etat une requête distincte en annulation. En l'absence de recours distinct sur le fond, la présente requête en référé présentée sur le fondement de l'article L. 521-1 du code de justice administrative, qui méconnaît les dispositions de l'article R. 522-1 du code précité, est manifestement irrecevable. <br/>
<br/>
               3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la transmission au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée, que la requête de M. Cornut de Lafontaine de Coincy doit être rejetée en toutes ses conclusions selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
               ------------------<br/>
<br/>
Article 1er : La requête de M. Cornut de Lafontaine de Coincy est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée M. Thibault Marie Christophe Cornut de Lafontaine de Coincy. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
