<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038971463</ID>
<ANCIEN_ID>JG_L_2019_07_000000430257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/97/14/CETATEXT000038971463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème chambre jugeant seule, 31/07/2019, 430257, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430257.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Poitiers, d'une part, d'annuler la décision du maire de Cognac du 16 décembre 2016 la suspendant de ses fonctions à compter du 19 décembre 2016 et, d'autre part, d'annuler la décision du maire de Cognac du 7 juin 2017 prononçant son exclusion temporaire de fonctions pour une durée d'un an à compter du 12 juin 2017 et d'enjoindre à la commune de la réintégrer dans ses fonctions et de reconstituer sa carrière. Par un jugement n° 1700127-1701809 du 4 juillet 2018, le tribunal administratif de Poitiers a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 18BX02852 et 18BX02853 du 21 mars 2019, la cour administrative d'appel de Bordeaux, sur appel de MmeB..., après avoir prononcé un non-lieu à statuer sur les conclusion à fin de sursis, a annulé, aux articles 2 et 3, la décision du maire de Cognac lui infligeant la sanction d'exclusion de fonctions pendant une durée d'un an, enjoint à la commune de Cognac de procéder à la reconstitution de sa carrière dans un délai de deux mois et rejeté le surplus des conclusions de MmeB....<br/>
<br/>
              1° Sous le numéro 430257, par un pourvoi enregistré le 29 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Cognac demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 et 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 430252, par une requête enregistrée le 29 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Cognac demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution des articles 2 et 3 de ce même arrêt ;<br/>
<br/>
              2°) de mettre à la charge de Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 89-677 du 18 septembre 1989 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de la commune de Cognac ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel la commune de Cognac demande l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux du 21 mars 2019 et sa requête tendant à ce qu'il soit sursis à l'exécution de ce même arrêt présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              3. Pour demander l'annulation de l'arrêt qu'elle attaque, la commune de Cognac soutient que la cour administrative d'appel de Bordeaux :<br/>
<br/>
              - l'a entaché d'irrégularité en omettant de préciser les articles du code des relations entre le public et l'administration dont il a été fait application ;<br/>
              - a commis une erreur de droit en retenant que Mme B...n'avait pas disposé du délai minimum de 15 jours exigé par les dispositions de l'article 6 du décret du 18 septembre 1989 ;<br/>
              - a commis une erreur de droit en annulant la décision de sanction d'exclusion temporaire de fonctions sans rechercher si ce vice de procédure, à le supposer établi, avait été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou avait privé l'intéressée d'une garantie. <br/>
<br/>
              4. Aucun de ces moyens n'est de nature à justifier l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par la commune de Cognac contre l'arrêt de la cour administrative d'appel de Bordeaux du 21 mars 2019 n'étant pas admis, les conclusions qu'elle présente aux fins de sursis à exécution sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Cognac n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de la commune de Cognac tendant à ce qu'il soit sursis à l'exécution des articles 2 et 3 de l'arrêt de la cour administrative d'appel de Bordeaux du 21 mars 2019.<br/>
Article 3 : La présente décision sera notifiée à la commune de Cognac. <br/>
Copie en sera adressée à MmeB....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
