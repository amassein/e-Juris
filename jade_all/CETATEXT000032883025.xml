<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032883025</ID>
<ANCIEN_ID>JG_L_2016_07_000000400945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/88/30/CETATEXT000032883025.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 04/07/2016, 400945, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:400945.20160704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté du 24 mai 2016 par lequel le ministre de l'intérieur l'a astreinte à résider dans la commune de Strasbourg, lui a fait obligation de se présenter trois fois par jour, tous les jours de la semaine, à des horaires déterminés à l'hôtel de police de cette ville, de demeurer tous les jours, entre 20 heures et 6 heures dans les locaux où elle réside et lui a interdit de se déplacer en dehors de son lieu d'assignation à résidence sans avoir préalablement obtenu un sauf-conduit établi par le préfet du Bas-Rhin et, d'autre part, d'enjoindre au ministre de l'intérieur de réexaminer sa situation en vue de lui appliquer une mesure moins contraignante. Par une ordonnance n° 1603239 du 9 juin 2016, le juge des référés du tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 24 et 29 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 et 75 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - l'ordonnance contestée est entachée d'un défaut de signature ;<br/>
              - elle est entachée d'une erreur de droit dès lors que le juge des référés du tribunal administratif de Strasbourg a méconnu son office, en ce qu'il n'a pas pris toutes les mesures nécessaires à la sauvegarde de sa liberté fondamentale ;<br/>
              - l'arrêté contesté est entaché d'une erreur de fait et d'une erreur manifeste d'appréciation dès lors qu'il n'existe aucune raison sérieuse de penser que son comportement constitue une menace pour la sécurité et l'ordre publics ;<br/>
              - il n'est ni nécessaire ni proportionné.<br/>
<br/>
<br/>
              Vu la décision du bureau d'aide juridictionnelle du Conseil d'Etat en date du 21 juin 2016 ;<br/>
<br/>
              Par un mémoire en défense enregistré le 29 juin 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MmeB..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 30 juin 2016 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de Mme B... ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au 1er juillet 2016 à 18 heures ;<br/>
              Vu les autres pièces du dossier, notamment l'arrêté du ministre de l'intérieur du 1er juillet 2016 modifiant l'arrêté litigieux ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - la loi n° 2016-162 du 19 février 2016 ; <br/>
              - la loi n° 2016-629 du 20 mai 2016 ;<br/>
              - le décret n° 2015-1475 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1476 du 14 novembre 2015 ;<br/>
              - le décret n° 2015-1478 du 14 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 521-2 du code de justice administrative, le juge des référés, saisi d'une demande en ce sens justifiée par l'urgence, peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté une atteinte grave et manifestement illégale. <br/>
<br/>
              2. En application de la loi du 3 avril 1955, l'état d'urgence a été déclaré par le décret n° 2015-1475 du 14 novembre 2015, à compter du même jour à zéro heure, sur le territoire métropolitain, prorogé pour une durée de trois mois, à compter du 26 novembre 2015, par l'article 1er de la loi du 20 novembre 2015, puis à compter du 26 février 2016 par l'article unique de la loi du 19 février 2016. En dernier lieu, la loi du 20 mai 2016 a prorogé l'état d'urgence pour une durée de deux mois à compter du 26 mai 2016. Aux termes de l'article 6 de la loi du 3 avril 1955, dans sa rédaction issue de la loi du 20 novembre 2015 : " Le ministre de l'intérieur peut prononcer l'assignation à résidence, dans le lieu qu'il fixe, de toute personne résidant dans la zone fixée par le décret mentionné à l'article 2 et à l'égard de laquelle il existe des raisons sérieuses de penser que son comportement constitue une menace pour la sécurité et l'ordre publics dans les circonscriptions territoriales mentionnées au même article 2. (...) / La personne mentionnée au premier alinéa du présent article peut également être astreinte à demeurer dans le lieu d'habitation déterminé par le ministre de l'intérieur, pendant la plage horaire qu'il fixe, dans la limite de douze heures par vingt-quatre heures. / L'assignation à résidence doit permettre à ceux qui en sont l'objet de résider dans une agglomération ou à proximité immédiate d'une agglomération. (...) / L'autorité administrative devra prendre toutes dispositions pour assurer la subsistance des personnes astreintes à résidence ainsi que celle de leur famille. / Le ministre de l'intérieur peut prescrire à la personne assignée à résidence : / 1° L'obligation de se présenter périodiquement aux services de police ou aux unités de gendarmerie, selon une fréquence qu'il détermine dans la limite de trois présentations par jour, en précisant si cette obligation s'applique y compris les dimanches et jours fériés ou chômés (...) ". Il résulte de l'article 1er du décret n° 2015-1476 du 14 novembre 2015, modifié par le décret n° 2015-1478 du même jour, que les mesures d'assignation à résidence sont applicables à l'ensemble du territoire métropolitain à compter du 15 novembre à minuit.<br/>
<br/>
              3. Il résulte de l'instruction que le ministre de l'intérieur a, par un arrêté du 24 mai 2016, astreint MmeB..., ressortissante française, à résider sur le territoire de la commune de Strasbourg avec obligation de se présenter trois fois par jour à 8 heures, 11 heures 45 et 19 heures, à l'hôtel de police de Strasbourg, tous les jours de la semaine, y compris les jours fériés ou chômés, et lui a imposé de demeurer tous les jours, de 20 heures à 6 heures dans les locaux où elle réside à Strasbourg. Cet arrêté dispose que Mme B...ne peut se déplacer en dehors de son lieu d'assignation à résidence sans avoir obtenu préalablement une autorisation écrite établie par le préfet du Bas-Rhin. Par une requête enregistrée le 24 juin 2016, Mme B...a demandé, sur le fondement de l'article L. 521-2 du code de justice administrative, au juge des référés du tribunal administratif de Strasbourg d'ordonner la suspension de l'exécution de l'arrêté du 24 mai 2016. Elle relève appel de l'ordonnance du 9 juin 2016 par laquelle le juge des référés a rejeté sa demande.<br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              4. Eu égard à son objet et à ses effets, notamment aux restrictions apportées à la liberté d'aller et venir, une décision prononçant l'assignation à résidence d'une personne, prise par l'autorité administrative en application de l'article 6 de la loi du 3 avril 1955, porte, en principe et par elle-même, sauf à ce que l'administration fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de cette personne, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, puisse prononcer dans de très brefs délais, si les autres conditions posées par cet article sont remplies, une mesure provisoire et conservatoire de sauvegarde. Le ministre de l'intérieur ne fait valoir aucune circonstance particulière conduisant à remettre en cause, au cas d'espèce, l'existence d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans la condition d'urgence particulière prévue par l'article L. 521-2 du code de justice administrative.<br/>
<br/>
               En ce qui concerne la condition tenant à l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              5. Il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, que l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le comportement de l'intéressé, compte tenu de la situation ayant conduit à la déclaration de l'état d'urgence, ou dans la détermination des modalités de l'assignation à résidence. Le juge des référés, s'il estime que les conditions définies à l'article L. 521-2 du code de justice administrative sont réunies, peut prendre toute mesure qu'il juge appropriée pour assurer la sauvegarde de la liberté fondamentale à laquelle il a été porté atteinte.<br/>
<br/>
              6. En premier lieu, il résulte de l'instruction que, sur le fondement des dispositions de l'article 6 de la loi du 3 avril 1955, le ministre de l'intérieur s'est appuyé, pour prendre la décision d'assignation à résidence litigieuse, sur les éléments mentionnés dans quatre " notes blanches " des services de renseignement versées au débat contradictoire. Il ressort de ces éléments, repris dans les motifs de l'arrêté du 24 mai 2016, et non sérieusement infirmés par les nouveaux éléments recueillis lors de l'audience, que Mme B... s'est convertie à l'islam au contact de l'un des auteurs des attentats terroristes commis le 13 novembre 2015 à Paris et à Saint-Denis, avec lequel elle entretenait une relation amoureuse avant le départ de ce dernier en Syrie. En outre, elle ne conteste pas avoir envoyé à des proches recensés sur son compte Facebook une vidéo, accessible jusqu'à la mi-octobre 2015, montrant quatre jeunes garçons vêtus de " khamis " glorifiant le jihad et appelant à partir en Syrie. Elle fait partie d'un groupe de jeunes adultes originaires de Wissembourg (Bas-Rhin) surnommés " les enfants de Daech " par des responsables de la communauté musulmane en raison de leurs velléités  de départ en Syrie, aux fins d'effectuer le jihad, deux d'entre eux ayant tenté de partir dans ce pays, en 2013 et 2014. Enfin, elle a été récemment remarquée au sein d'un groupe d'une quinzaine de personnes faisant régulièrement du prosélytisme sur les places publiques et adoptant un comportement virulent et agressif, sous la houlette de l'imam d'une mosquée fréquentée par plusieurs individus d'obédience salafiste. <br/>
<br/>
              7. Eu égard à l'ensemble des éléments ainsi recueillis au cours des échanges écrits et oraux, il n'apparaît pas, en l'état de l'instruction, qu'en prononçant l'assignation à résidence de Mme B...jusqu'au 26 juillet 2016 au motif qu'il existait de sérieuses raisons de penser que son comportement constitue une menace grave pour la sécurité et l'ordre publics, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir<br/>
              8. En second lieu, Mme B...soutient que les modalités de son assignation à résidence sont manifestement disproportionnées dès lors que l'obligation de se présenter trois fois par jour, à 8 heures, 11 heures 45 et 19 heures, à l'hôtel de police de Strasbourg, l'empêche de suivre sa formation en alternance qui a lieu sur la commune de Neudorf (Bas-Rhin). Postérieurement à l'audience, le ministre de l'intérieur a, le 1er juillet 2016, partiellement modifié ces horaires pour prendre en compte la demande de MmeB.... Toutefois, il n'apparaît pas, en l'état de l'instruction et compte tenu du comportement de l'intéressée, que ces modalités, qu'il s'agisse de celles antérieures ou postérieures au 1er juillet 2016, portent une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              9. Il résulte de ce qui précède que Mme B...n'est pas fondée à soutenir que c'est à tort, que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Strasbourg a rejeté sa demande. Sa requête doit par conséquent être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
