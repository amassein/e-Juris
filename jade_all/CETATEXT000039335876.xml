<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335876</ID>
<ANCIEN_ID>JG_L_2019_11_000000425983</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/11/2019, 425983, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425983</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:425983.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 2 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société hôtelière Paris Eiffel Suffren demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 17PA01188 de la cour administrative d'appel de Paris du 5 octobre 2018 statuant sur sa demande de condamnation de l'Etat à lui verser la somme de 2 025 068, 53 euros, augmentée des intérêts de retard, en réparation des préjudices nés de l'application du premier alinéa de l'article 15 de l'ordonnance du 21 octobre 1986, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 141-1 du code de l'organisation judiciaire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2004-1484 du 30 décembre 2004 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'ordonnance n° 86-1134 du 21 octobre 1986 ;<br/>
              - la décision n° 2013-336 QPC du Conseil constitutionnel du 1er août 2013 ;<br/>
              - l'article L. 141-1 du code de l'organisation judiciaire ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société hôtelière Paris Eiffel Suffren ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article L. 141-1 du code de l'organisation judiciaire dispose que : " L'Etat est tenu de réparer le dommage causé par le fonctionnement défectueux du service public de la justice. / Sauf dispositions particulières, cette responsabilité n'est engagée que par une faute lourde ou par un déni de justice ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la société hôtelière Paris Eiffel Suffren a demandé au tribunal administratif de Paris puis à la cour administrative d'appel de Paris de condamner l'Etat à lui verser la somme de 2 025 068,53 euros, augmentée des intérêts de retard, en réparation des préjudices qu'elle estime avoir subis du fait de l'application du premier alinéa de l'article 15 de l'ordonnance du 21 octobre 1986 relative à l'intéressement et à la participation des salariés aux résultats de l'entreprise et à l'actionnariat des salariés, devenu le premier alinéa de l'article L. 442-9 du code du travail, dans sa rédaction antérieure à la loi du 30 décembre 2004 de finances pour 2005, que le Conseil constitutionnel a déclaré contraire à la Constitution par sa décision n° 2013-336 QPC du 1er août 2013. Cette action a pour objet d'engager la responsabilité de l'Etat du fait d'une loi déclarée contraire à la Constitution par le Conseil constitutionnel et non pour faute lourde du service public de la justice. Les dispositions de l'article L. 141-1 du code de l'organisation judiciaire n'ont pas été invoquées par les parties devant la cour ni appliquées par elle et n'étaient pas susceptibles de l'être au titre des moyens qu'il lui appartenait de relever d'office. La question de leur conformité aux droits et libertés garantis par la Constitution est ainsi sans incidence sur la régularité ou le bien-fondé de l'arrêt contre lequel la société requérante se pourvoit en cassation. Par suite, ces dispositions ne sont pas applicables au litige dont le Conseil d'Etat, juge de cassation, est saisi. Elles ne seraient, en tout état de cause, pas plus susceptibles d'être applicables au litige dans l'hypothèse où le Conseil d'Etat prononcerait l'annulation de l'arrêt attaqué et règlerait l'affaire au fond, dans l'intérêt d'une bonne administration de la justice, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 141-1 du code de l'organisation judiciaire porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société hôtelière Paris Eiffel Suffren.<br/>
Article 2 : La présente décision sera notifiée à la société hôtelière Paris Eiffel Suffren et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
