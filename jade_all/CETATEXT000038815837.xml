<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815837</ID>
<ANCIEN_ID>JG_L_2019_07_000000424059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 24/07/2019, 424059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424059.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 26 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le syndicat indépendant du personnel du Conseil d'Etat et de la Cour nationale du droit d'asile (SIPCE) demande au Conseil d'Etat d'annuler pour excès de pouvoir la note du 29 mai 2018 de la présidente de la Cour nationale du droit d'asile relative à la mise en oeuvre de l'article 109 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique, en présence de demandes sérielles.<br/>
<br/>
              Le SIPCE soutient que la note attaquée :<br/>
              - méconnaît les articles R. 732-1 et R. 732-2 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - est entachée d'erreur de droit en ce qu'elle prévoit que les secrétaires d'audience délivrent les attestations de mission prévues par l'article 104 du décret du 19 décembre 1991.<br/>
<br/>
              Par un mémoire en défense, enregistré le 3 janvier 2019, la garde des sceaux, ministre de la justice, demande au Conseil d'Etat de dire qu'il n'y a pas lieu de statuer sur la requête.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article 104 du décret du 19 décembre 1991 portant application de la loi du 10 juillet 1991 relative à l'aide juridique prévoit que les sommes revenant aux avocats prêtant leur concours au titre de l'aide juridictionnelle " sont réglées sur justification de la désignation au titre de l'aide juridictionnelle et production d'une attestation de mission délivrée par le greffier en chef ou le secrétaire de la juridiction saisie ". Le même article dispose que l'attestation de mission qu'il prévoit doit mentionner " le montant de la contribution de l'Etat à la rétribution de l'avocat après, le cas échéant, application de la réduction prévue à l'article 109 (...) / ou la somme à régler à l'officier public ou ministériel après, le cas échéant, application de la réduction prévue à l'article 109 ". Enfin, l'article 109 de ce même décret dispose que : " La part contributive versée par l'Etat à l'avocat choisi ou désigné pour assister plusieurs personnes (...) dans un litige reposant sur les mêmes faits et comportant des prétentions ayant un objet similaire (...) est réduite de 30 % pour la deuxième affaire, de 40 % pour la troisième, de 50 % pour la quatrième et de 60 % pour la cinquième et s'il y a lieu pour les affaires supplémentaires ".<br/>
<br/>
              2. Par la note attaquée du 29 mai 2018, la présidente de la Cour nationale du droit d'asile a demandé aux " secrétaires d'audience " de cette cour de délivrer les attestations de mission prévues par les dispositions, citées ci-dessus, de l'article 104 du décret du 19 décembre 1991, en fixant eux-mêmes, sous le contrôle du chef de chambre, la réduction applicable à la somme à régler à l'avocat pour les affaires concernées.<br/>
<br/>
              Sur la fin de non-recevoir soulevée par la garde des sceaux, ministre de la justice :<br/>
<br/>
              3. Si, par une nouvelle note du 21 décembre 2018, la présidente de la Cour nationale du droit d'asile a abrogé la note litigieuse et l'a remplacée par de nouvelles dispositions entrées en vigueur le 1er janvier 2019, cette nouvelle note n'a pas eu pour effet de procéder au retrait de la note attaquée, qui a reçu exécution dès sa publication. La garde des sceaux, ministre de la justice n'est, par suite, pas fondée à soutenir que le litige aurait perdu son objet.<br/>
<br/>
              Sur la légalité de la note attaquée :<br/>
<br/>
              4. L'article R. 732-2 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose, dans sa rédaction en vigueur à la date de la note attaquée, que : " Le secrétariat de la Cour nationale du droit d'asile est assuré par un secrétaire général nommé par le vice-président du Conseil d'Etat sur proposition du président de la cour. / Sous l'autorité du président de la cour, le secrétaire général encadre les services de la juridiction et veille à leur bon fonctionnement. Il est assisté par des secrétaires généraux adjoints. Le président de la cour peut déléguer sa signature au secrétaire général, aux secrétaires généraux adjoints, aux fonctionnaires de catégorie A et aux agents contractuels chargés des fonctions de niveau équivalent pour (...) l'exécution des actes de procédure ".<br/>
<br/>
              5. Si les dispositions citées ci-dessus permettent au président de la Cour nationale du droit d'asile de déléguer sa signature à certains agents placés sous son autorité pour l'exécution des actes de procédure contentieuse, il résulte des termes mêmes de l'article 104 du décret du 19 décembre 1991 cité au point 1 que les attestations de mission prévues par cet article et la fixation, le cas échéant, de la réduction applicable en vertu de l'article 109 du même décret, relèvent, non de la compétence du chef de la juridiction saisie, mais de son greffier en chef ou du secrétaire de cette juridiction. La présidente de la Cour nationale du droit d'asile ne pouvait, par suite, légalement prévoir la délivrance des attestations de mission que par le secrétaire général de cette cour, ou le cas échéant par des agents ayant reçu de cette dernière délégation à cette fin.<br/>
<br/>
              6. Or, il ressort des pièces du dossier que les " secrétaires de séance " auxquels la note attaquée confie le soin de délivrer les attestations de mission ne disposaient, à la date de cette note, d'aucune délégation à cette fin émanant du secrétaire général de la Cour nationale du droit d'asile. Aucune disposition ne prévoyait d'ailleurs, à cette date, que le secrétaire général de la Cour nationale du droit d'asile puisse déléguer les compétences qui lui revenaient en propre.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le syndicat requérant est fondé à soutenir que la note litigieuse méconnaît les dispositions de l'article 104 du décret du 19 décembre 1991 et à en demander, en conséquence, l'annulation.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La note du 29 mai 2018 de la présidente de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : La présente décision sera notifiée au syndicat indépendant du personnel du Conseil d'Etat et de la Cour nationale du droit d'asile et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
