<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033163043</ID>
<ANCIEN_ID>JG_L_2016_09_000000389283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/16/30/CETATEXT000033163043.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 28/09/2016, 389283, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389283.20160928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Sous le n° 389283, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 7 avril et 20 mai 2015 et 19 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-130 du 5 février 2015 modifiant certaines dispositions du code de la sécurité intérieure (partie réglementaire) relatives aux armes et munitions en Polynésie française et en Nouvelle-Calédonie ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 389993, par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 5 mai et 5 août 2015 et les 8 mars et 18 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat des armuriers de Nouvelle-Calédonie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-130 du 5 février 2015 modifiant certaines dispositions du code de la sécurité intérieure (partie réglementaire) relatives aux armes et munitions en Polynésie française et en Nouvelle-Calédonie ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la directive 91/477/CEE du Conseil du 18 juin 1991 ;<br/>
<br/>
              - le code de la sécurité intérieure ;<br/>
<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
<br/>
              - la loi n° 2012-304 du 6 mars 2012 ;<br/>
<br/>
              - le décret n° 2013-700 du 30 juillet 2013 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du syndicat des armuriers de Nouvelle-Calédonie ;<br/>
<br/>
              Vu la note en délibéré présentée par le syndicat des armuriers de Nouvelle-Calédonie, enregistrée le 7 septembre 2016 ; <br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de M. B...et du syndicat des armuriers de Nouvelle-Calédonie sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              2. Considérant que le 4° de l'article 21 de la loi organique du 19 mars 1999 portant statut de la Nouvelle-Calédonie dispose que l'Etat est compétent en matière de " matériels de guerre, armes et munitions, poudres et substances explosives " ; que le 29° de l'article 22 de la même loi dispose que la Nouvelle-Calédonie est compétente en matière de " réglementation des activités sportives et socio-éducatives ; infrastructures et manifestations sportives et culturelles intéressant la Nouvelle-Calédonie " ; que le décret attaqué, qui édicte les dispositions règlementaires du code de la sécurité intérieure relatives aux armes et munitions applicables en Nouvelle-Calédonie, relève en application de ces dispositions de la compétence de l'Etat, alors même qu'il a aussi pour objet de réglementer la détention des armes utilisées pour la pratique sportive ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 312-1 du code de la sécurité intérieure, applicable en Nouvelle-Calédonie en vertu de l'article L. 345-1 du même code : " Nul ne peut acquérir et détenir légalement des matériels ou des armes de toute catégorie s'il n'est pas âgé de dix-huit ans révolus, sous réserve des exceptions définies par décret en Conseil d'Etat pour la chasse et les activités encadrées par la fédération sportive ayant reçu, au titre de l'article L. 131-14 du code du sport, délégation du ministre chargé des sports pour la pratique du tir " ; que ces dispositions habilitent le pouvoir réglementaire à définir le nombre maximum d'armes pouvant être détenues par des personnes mineures ;  qu'ainsi, le moyen tiré de ce que le pouvoir réglementaire n'était pas compétent pour limiter le nombre d'armes pouvant être détenues par des mineurs en Nouvelle-Calédonie doit être écarté ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 421-1 du code de l'environnement : " I. - L'organisme consultatif, dénommé Conseil national de la chasse et de la faune sauvage, placé auprès du ministre chargé de la chasse, est chargé de donner au ministre son avis sur les moyens propres à : 1° Préserver la faune sauvage ; 2° Développer le capital cynégétique dans le respect des équilibres biologiques ; 3° Améliorer les conditions d'exercice de la chasse. / II. - Le conseil est consulté sur les projets de loi et de décret modifiant les dispositions législatives et réglementaires du présent titre " ; que ces dispositions n'imposaient pas que le décret attaqué, qui ne modifie pas les dispositions réglementaires du titre II du livre IV du code de l'environnement, soit soumis à la consultation du Conseil national de la chasse et de la faune sauvage ; que le moyen tiré de ce que l'absence d'une telle consultation entacherait le décret d'illégalité doit, par suite, être écarté ;<br/>
<br/>
              5. Considérant qu'aux termes du I. de l'article 133 de la loi organique du 19 mars 1999 " I. - Le gouvernement est consulté par le haut-commissaire sur : 1° Les projets de décrets comportant des dispositions spécifiques à la Nouvelle-Calédonie ; (...) " ; que ces dispositions n'imposaient la consultation du gouvernement de Nouvelle-Calédonie que sur le projet de décret ; que le moyen tiré de ce qu'il aurait dû aussi être consulté sur des questions telles que les collections d'armes et la constitution de musées privés, qui n'est au demeurant assorti d'aucune précision, ne peut, par suite, qu'être écarté ;<br/>
<br/>
              6. Considérant, en revanche, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que s'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte ; que l'article 1er du décret attaqué et l'annexe à laquelle il renvoie ajoutent au code de la sécurité intérieure un article R. 345-1 qui rend applicable à la Nouvelle-Calédonie le cinquième alinéa du 2° de l'article R. 312-40 du même code, aux termes duquel : " Les modalités des séances contrôlées de pratique du tir sont fixées par arrêté conjoint des ministres de la défense et de l'intérieur et du ministre chargé des sports ", le cinquième alinéa de l'article R. 312-43 du même code, aux termes duquel : " Un arrêté conjoint du ministre de l'intérieur, du ministre de la défense et du ministre chargé des sports fixe le modèle type du carnet de tir et du registre journalier mentionnés aux alinéas précédents ", ainsi que le deuxième alinéa de l'article R. 315-18 du même code, aux termes duquel : " Les conditions de sécurité auxquelles doivent satisfaire les opérations de chargement, de déchargement et de transit dans les gares routières, ferroviaires, les ports et les aéroports des armes et éléments des armes classés dans ces catégories sont fixées par arrêté conjoint des ministres de l'intérieur et de la défense et des ministres chargés de l'industrie, des transports et des douanes " ; qu'ainsi, le ministre en charge des sports et le ministre en charge des transports sont chargés de l'exécution du décret attaqué et auraient dû, en vertu des dispositions de l'article 22 de la Constitution, le contresigner ; que l'omission de leur contreseing doit entraîner l'annulation de l'article 1er et de l'annexe du décret attaqué, en tant qu'ils rendent applicables à la Nouvelle-Calédonie les 3ème, 4ème et 5ème alinéas du 2° de l'article R. 312-40 du code de la sécurité intérieure, dont les dispositions ne sont pas divisibles entre elles, le cinquième alinéa de l'article R. 312-43, ainsi que le deuxième alinéa de l'article R. 315-18 du même code  ; <br/>
<br/>
              7. Considérant, en revanche, que les dispositions du décret attaqué, qui ne réglementent pas l'activité de la chasse, n'impliquaient pas qu'il fût contresigné par le ministre chargé de la chasse ; <br/>
<br/>
              Sur la légalité interne du décret attaqué : <br/>
<br/>
              8. Considérant que si M. B...soutient que le deuxième alinéa du b) du 2° de l'article 187-1 du décret du 30 juillet 2013 visé ci-dessus, inséré dans ce décret par le décret attaqué, serait inintelligible et désignerait à tort comme autorité compétente pour la Nouvelle-Calédonie le haut-commissaire de la République en Polynésie française, les erreurs dont sont entachées ces dispositions ont été corrigées par un rectificatif publié au Journal officiel le 1er août 2015 ; que ces moyens doivent, par suite, être écartés ;      <br/>
<br/>
              9. Considérant qu'il résulte des dispositions des articles R. 345-1 et R. 345-4 du code de la sécurité intérieure, issus du décret attaqué, que le 2° de l'article R. 312-40 du même code, dans sa rédaction applicable en Nouvelle-Calédonie, limite à huit armes par personne la détention d'armes relevant des 1°, 2°, 4° et 9° de la catégorie B ; que les dispositions applicables en métropole limitent la détention de telles armes à douze armes par personne ; que les requérants soutiennent que cette différence de traitement n'est pas justifiée par une différence de situation entre la Nouvelle-Calédonie et la métropole en termes de sécurité publique ; qu'il ressort toutefois des pièces du dossier que le nombre d'armes, de licences de tir et de permis de chasse par habitant et, par suite, les risques qui peuvent en résulter sont sensiblement plus élevés en Nouvelle-Calédonie qu'en métropole ; que, dans ces conditions, le Premier ministre a pu, sans méconnaître le principe d'égalité, limiter la détention des armes relevant des 1°, 2°, 4° et 9° de la catégorie B de manière plus restrictive en Nouvelle-Calédonie que sur le territoire métropolitain ; que le moyen tiré de l'atteinte au principe d'égalité doit, par suite, être écarté ;<br/>
<br/>
              10. Considérant que nul n'a droit au maintien d'une réglementation ; qu'il est ainsi loisible au pouvoir réglementaire de définir à tout moment, sous le contrôle du juge de l'excès de pouvoir, des règles de détention d'armes plus strictes que celles qui résultaient de la réglementation antérieure ;  que, compte tenu du délai de mise en conformité avec la nouvelle réglementation, fixé à six mois par le décret attaqué, les requérants ne sont pas fondés à soutenir que  le principe de sécurité juridique aurait été méconnu ; que, par ailleurs, en prévoyant que les armes dont la détention n'est plus conforme à la réglementation doivent être restituées dans un délai de six mois, le décret attaqué n'a fait qu'énoncer la conséquence qui doit nécessairement être tirée du fait que la détention a cessé d'être régulière, sans que ce dessaisissement ne constitue, s'agissant d'un bien dont la détention est subordonnée au respect de la réglementation, ni une atteinte au droit de propriété garanti par les stipulations de l'article 1er du premier protocole additionnel à la convention européenne des droits de l'homme et des libertés fondamentales susceptible de faire l'objet d'une indemnisation, ni une mesure d'une gravité disproportionnée par rapport à l'objectif poursuivi ; <br/>
<br/>
              11. Considérant que les requérants ne sauraient utilement invoquer la directive 91/477/CEE du Conseil du 18 juin 1991 relative au contrôle de l'acquisition et de la détention d'armes, qui n'est pas applicable en Nouvelle-Calédonie ; que le décret attaqué n'étant pas pris pour la mise en oeuvre du droit de l'Union européenne, ils ne peuvent utilement soutenir que ses dispositions porteraient atteinte au principe de confiance légitime ; <br/>
<br/>
              12. Considérant que le décret attaqué a notamment pour objet d'édicter les dispositions règlementaires du code de la sécurité intérieure relatives aux armes et munitions applicables en Nouvelle-Calédonie ; que ce décret crée à cette fin, d'une part, les articles R. 345-1 et R. 345-2 de ce code, qui ont pour objet de rendre applicables à ce territoire la plupart des dispositions règlementaires du même code relatives aux armes et munitions applicables en métropole, et, d'autre part, les articles R. 345-3 et R. 345-4, qui apportent des modifications à ces dispositions pour les besoins de leur application en Nouvelle-Calédonie ; qu'il était loisible au Premier ministre de retenir un mode de rédaction étendant à la Nouvelle-Calédonie les dispositions applicables en métropole en les assortissant d'adaptations, sans rédiger intégralement un nouveau titre règlementaire applicable à la seule Nouvelle-Calédonie ; que les dispositions du décret attaqué ne créent aucune ambiguïté ou contradiction qui serait source d'insécurité juridique et ne méconnaissent pas l'objectif à valeur constitutionnelle de clarté et d'intelligibilité de la norme ; <br/>
<br/>
              13. Considérant que les autres moyens soulevés par M.B...,  tirés de ce que les prescriptions du  décret attaqué méconnaîtraient les dispositions relatives aux collectionneurs d'armes et à la liberté de créer un musée, qu'elles seraient disproportionnées au regard de ce qui est nécessaire pour assurer le respect de l'ordre public et qu'elles méconnaîtraient le principe de la liberté contractuelle ne sont pas assortis des précisions permettant d'en apprécier le bien fondé ; qu'ils ne peuvent, par suite, qu'être écartés ;  <br/>
<br/>
              Sur les conséquences de l'illégalité du décret attaqué :<br/>
<br/>
              14. Considérant qu'il résulte de ce qui est indiqué au point 6 de la présente décision que le syndicat des armuriers de Nouvelle-Calédonie est fondé à demander l'annulation de l'article 1er et de l'annexe du décret attaqué en tant qu'ils rendent applicables à la Nouvelle-Calédonie les 3ème, 4ème, et 5ème alinéas du 2° de l'article R. 312-40, le 5ème alinéa de l'article R. 312-43 et le 2ème alinéa de l'article R. 315-18 du code de la sécurité intérieure ; <br/>
<br/>
              15. Considérant qu'il ne ressort pas des pièces du dossier que la disparition rétroactive de ces dispositions entraînerait des conséquences manifestement excessives, eu égard aux intérêts en présence et aux inconvénients que présenterait une limitation dans le temps des effets de leur annulation ; qu'il n'y a pas lieu, par suite, d'assortir l'annulation de ces dispositions d'une telle limitation ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes que les requérants demandent au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'article 1er et l'annexe du décret n° 2015-130 du 5 février 2015 modifiant certaines dispositions du code de la sécurité intérieure (partie réglementaire) relatives aux armes et munitions en Polynésie française et en Nouvelle-Calédonie sont annulés en tant qu'ils rendent applicables à la Nouvelle-Calédonie les 3ème, 4ème et 5ème alinéas du 2° de l'article R. 312-40, le 5ème alinéa de l'article R. 312-43 et le 2ème alinéa de l'article R. 315-18 du code de la sécurité intérieure. <br/>
Article 2 : Le surplus des conclusions des requêtes est rejeté. <br/>
Article 3 : La présente décision sera notifiée au Syndicat des armuriers de Nouvelle-Calédonie et à M. A...B..., au Premier ministre, au ministre de la défense et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
