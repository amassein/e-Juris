<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025916766</ID>
<ANCIEN_ID>JG_L_2012_05_000000336790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/67/CETATEXT000025916766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 22/05/2012, 336790</TITRE>
<DATE_DEC>2012-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Aymeric Pontvianne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336790.20120522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 février et 18 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Louis B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0701906 du 20 novembre 2009 par lequel le magistrat désigné par le président du tribunal administratif de Toulouse a rejeté sa demande tendant, d'une part, à l'annulation de la décision du directeur départemental de l'équipement de l'Aveyron du 15 janvier 2007 en tant qu'elle limite son indemnité spécifique de mobilité à 1 700 euros, d'autre part, à l'annulation de la décision implicite du ministre des transports, de l'équipement, du tourisme et de la mer rejetant son recours préalable, et enfin, à écarter l'application de l'article 3 du décret du 16 mai 2005 et de l'article 2 de l'arrêté du même jour ; <br/>
<br/>
              2°) jugeant l'affaire au fond, d'annuler les décisions précitées et de condamner l'Etat au versement d'une indemnité spécifique de mobilité de 9 000 euros avec les intérêts de droit et capitalisation ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le décret n° 2005-472 du 16 mai 2005 ;<br/>
<br/>
              Vu l'arrêté interministériel du 16 mai 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aymeric Pontvianne, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations à la SCP Masse-Dessen, Thouvenin, avocat M. B, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat M. B ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 1er du décret du 16 mai 2005 portant attribution d'une indemnité spéciale de mobilité à certains agents du ministère de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer : " Jusqu'au 31 décembre 2010, les agents publics titulaires et non titulaires ainsi que les ouvriers des parcs et ateliers du ministère de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer peuvent bénéficier (...) d'une indemnité spéciale de mobilité dès lors qu'ils doivent changer de lieu de travail en raison d'une mutation dans l'intérêt du service ou d'un déplacement d'office avec changement de résidence prononcés à l'occasion de la réorganisation de leur service (...) " ; qu'aux termes de l'article 2 du même décret, le montant de l'indemnité tient compte de l'allongement de la distance entre la résidence familiale et le nouveau lieu de travail de l'agent ; qu'aux termes de l'article 3 du même décret : " (...) Lorsque deux agents mariés, concubins ou partenaires d'un pacte civil de solidarité sont concernés au titre de la même opération par le dispositif de l'indemnité spéciale de mobilité, sans qu'ils soient astreints à un changement de résidence familiale, le premier perçoit l'indemnité dans les conditions prévues par le présent décret et son arrêté d'application, le second perçoit une indemnité d'un montant égal à 20 % de celle perçue par son conjoint, concubin ou partenaire. Le cumul des deux indemnités spéciales de mobilité ne peut cependant pas dépasser un plafond défini par l'arrêté d'application du présent décret. (...) " ; qu'aux termes de l'article 1er de l'arrêté du 16 mai 2005 fixant le montant de l'indemnité spéciale de mobilité attribuée à certains agents du ministère de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer, en cas de changement de lieu de travail de l'agent, sans changement de résidence familiale, l'indemnité est fonction croissante de la distance aller-retour entre la résidence familiale et le nouveau lieu de travail de l'agent ;  qu'aux termes de l'article 2 du même arrêté : " Le cumul des indemnités spéciales de mobilité prévu à l'article 3 du décret du 16 mai 2005 susvisé ne peut dépasser 10 700 euros. " ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que M. B, chef d'équipe d'exploitation de l'équipement, et son épouse ont été concernés, au cours de l'année 2006, par la réorganisation de la direction départementale de l'équipement de l'Aveyron, dans le cadre de la mise en oeuvre de la loi du 13 août 2004 relative aux libertés et aux responsabilités locales ; qu'à la suite de la suppression de la subdivision de Decazeville, où ils étaient affectés, ils ont été mutés, dans l'intérêt du service, l'un à Rodez, l'autre à Villefranche-de-Rouergue ; qu'une indemnité spéciale de mobilité de 9 000 euros ayant été attribuée à son épouse, M. B a reçu, par arrêté du directeur départemental du l'équipement du 15 janvier 2007, une indemnité limitée à 1 700 euros, en application de l'article 3 du décret du 16 mai 2005 et de l'article 2 de son arrêté d'application ; que M. B a contesté devant le tribunal administratif de Toulouse cette décision ainsi que la décision implicite de rejet du ministre de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer, qu'il avait saisi d'un recours préalable le 9 février 2007 ; que M. B se pourvoit contre le jugement en date du 20 novembre 2009 du tribunal administratif de Toulouse qui a rejeté sa requête, au motif que l'administration avait fait une exacte application des dispositions précitées ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que l'indemnité spéciale de mobilité a pour objet principal la compensation des charges liées à l'allongement des trajets induits par les réorganisations du service ; que dans ces conditions, la réduction de 80 % de l'indemnité versée au second membre du couple ainsi que le plafonnement de l'indemnité de résidence à 10 700 euros par couple d'agents concernés par une même opération en application des dispositions précitées trouve sa justification dans la possibilité, pour ces agents, de mutualiser tout ou partie des coûts de transport induits par l'opération de réorganisation ; que dans le cas où ces agents ont reçu deux affectations géographiques différentes et éloignées, rendant impossible une telle mutualisation, l'application des règles d'écrêtement de l'indemnité, eu égard à l'objet de l'indemnité, aboutit pour l'un d'entre eux à une différence de traitement avec un agent célibataire ou vivant en couple avec un agent non concerné par le dispositif, qui n'est justifiée par aucun objectif d'intérêt général, et ne peut être regardée comme en rapport avec l'objet de l'indemnité, méconnaissant ainsi le principe d'égalité ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le magistrat désigné par le président du tribunal administratif de Toulouse, en jugeant, après avoir relevé que les époux B, concernés par la même opération, avaient été affectés à deux résidences administratives différentes, que c'était sans méconnaître le principe d'égalité que l'administration avait fait application à M. B des dispositions de l'article 3 du décret et de l'article 2 de l'arrêté précités, a entaché, dans les circonstances de l'espèce, son jugement d'erreur de droit ; que par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, ce jugement doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant, ainsi qu'il a été dit, et comme le soutient le requérant, que le décret et l'arrêté précités ne sauraient traiter de façon différente deux agents d'un même corps placés dans une situation semblable au regard des charges liées à l'allongement des trajets induits par la restructuration, selon qu'ils ont ou non un conjoint bénéficiant de l'indemnité spéciale de mobilité, eu égard à l'objet de cette dernière ; qu'ainsi, le décret et l'arrêté précités méconnaissent le principe d'égalité en tant qu'ils n'ont pas prévu de soustraire à l'application des règles d'écrêtement les couples concernés au titre de la même opération, mais affectés, comme en l'espèce, à deux résidences administratives différentes et éloignées, sans possibilité pour les agents de mutualiser tout ou partie du coût des trajets ; que par suite, M. B est fondé à exciper de leur illégalité pour demander l'annulation, dans cette dernière mesure et dans les circonstances de l'espèce, des dispositions de l'article 3 du décret du 16 mai 2005 et de l'article 2 de l'arrêté interministériel du 16 mai 2005 ;<br/>
<br/>
              Considérant que M. B est fondé à demander, par voie de conséquence et dans les circonstances de l'espèce, l'annulation de la décision du directeur départemental de l'équipement du 15 janvier 2007 ainsi que de la décision implicite de rejet du ministre de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer ;<br/>
<br/>
              Considérant que si M. B demande le versement du plein de l'indemnité spéciale de mobilité, pour une somme de 9 000 euros, des intérêts au taux légal sur cette somme à compter de la date de réception de sa demande préalable du 9 février 2007, ainsi que la capitalisation des intérêts échus le 18 février 2010, l'état de l'instruction ne permet pas de déterminer le montant de l'indemnité qui lui due ; qu'il y a lieu seulement de le renvoyer devant le ministre de l'écologie, du développement durable, des transports et du logement pour qu'il soit procédé, à la lumière des motifs de la présente décision, à la liquidation de l'indemnité à laquelle il a droit ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Toulouse du 20 novembre 2009 est annulé.<br/>
Article 2 : L'article 3 du décret du 16 mai 2005 et l'article 2 de l'arrêté interministériel du 16 mai 2005 sont annulés en tant qu'ils ne prévoient pas de soustraire à l'application des règles d'écrêtement les couples concernés au titre de la même opération, mais affectés à deux résidences administratives différentes et éloignées, rendant impossible la mutualisation de tout ou partie du coût de leurs trajets.<br/>
<br/>
Article 3 : La décision du directeur départemental de l'équipement de l'Aveyron du 15 janvier 2007, ainsi que la décision implicite de rejet du ministre de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer, sont annulées.<br/>
<br/>
Article 4 : M. B est renvoyé devant le ministre de l'écologie, du développement durable et de l'énergie pour qu'il soit procédé, à la lumière des motifs de la présente décision, à la liquidation de l'indemnité qui lui est éventuellement due.<br/>
<br/>
Article 5 : L'Etat versera à M. B une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. Jean-Louis B et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - MÉCONNAISSANCE - INDEMNITÉ SPÉCIALE DE MOBILITÉ ATTRIBUÉE À DES AGENTS PUBLICS POUR COMPENSER LES CHARGES LIÉES À L'ALLONGEMENT DES TRAJETS INDUITS PAR DES RÉORGANISATIONS DU SERVICE - RÈGLES D'ÉCRÈTEMENT DE L'INDEMNITÉ POUR LES COUPLES CONCERNÉS PAR LA MÊME OPÉRATION - ABSENCE DE PRISE EN COMPTE DE LA SITUATION DES COUPLES AFFECTÉS À DEUX RÉSIDENCES ADMINISTRATIVES DIFFÉRENTES ET ÉLOIGNÉES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - AGENTS DU MINISTÈRE DE L'ÉQUIPEMENT, DES TRANSPORTS, DE L'AMÉNAGEMENT DU TERRITOIRE, DU TOURISME ET DE LA MER - INDEMNITÉ SPÉCIALE DE MOBILITÉ ATTRIBUÉE POUR COMPENSER LES CHARGES LIÉES À L'ALLONGEMENT DES TRAJETS INDUITS PAR DES RÉORGANISATIONS DU SERVICE (DÉCRET DU 16 MAI 2005) - RÈGLES D'ÉCRÈTEMENT DE L'INDEMNITÉ POUR LES COUPLES CONCERNÉS PAR LA MÊME OPÉRATION - ABSENCE DE PRISE EN COMPTE DE LA SITUATION DES COUPLES AFFECTÉS À DEUX RÉSIDENCES ADMINISTRATIVES DIFFÉRENTES ET ÉLOIGNÉES - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ.
</SCT>
<ANA ID="9A"> 01-04-03-01 Les dispositions du décret n° 2005-472 du 16 mai 2005 portant attribution à certains agents du ministère de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer d'une indemnité spéciale de mobilité destinée à compenser les charges liées à l'allongement des trajets induits par des réorganisations du service prévoient que, lorsque deux agents en couple sont concernés au titre de la même opération, le premier perçoit l'indemnité à taux plein et le second une indemnité d'un montant égal à 20% de celle perçue par son conjoint. Dans le cas où ces agents reçoivent deux affectations géographiques différentes et éloignées, rendant impossible toute mutualisation des frais de transport, l'application de cette règle d'écrètement de l'indemnité aboutit pour l'un d'eux à une différence de traitement qui n'est justifiée par aucun objectif d'intérêt général et qui est sans rapport avec l'objet de l'indemnité, en méconnaissance du principe d'égalité.</ANA>
<ANA ID="9B"> 36-08-03 Les dispositions du décret n° 2005-472 du 16 mai 2005 portant attribution à certains agents du ministère de l'équipement, des transports, de l'aménagement du territoire, du tourisme et de la mer d'une indemnité spéciale de mobilité destinée à compenser les charges liées à l'allongement des trajets induits par des réorganisations du service prévoient que, lorsque deux agents en couple sont concernés au titre de la même opération, le premier perçoit l'indemnité à taux plein et le second une indemnité d'un montant égal à 20% de celle perçue par son conjoint. Dans le cas où ces agents reçoivent deux affectations géographiques différentes et éloignées, rendant impossible toute mutualisation des frais de transport, l'application de cette règle d'écrètement de l'indemnité aboutit pour l'un d'eux à une différence de traitement qui n'est justifiée par aucun objectif d'intérêt général et qui est sans rapport avec l'objet de l'indemnité, en méconnaissance du principe d'égalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
