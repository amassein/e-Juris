<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036028814</ID>
<ANCIEN_ID>JG_L_2017_11_000000410117</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/02/88/CETATEXT000036028814.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 15/11/2017, 410117, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410117</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP FOUSSARD, FROGER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410117.20171115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Distribution Casino France a demandé au juge des référés du tribunal administratif d'Orléans, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 9 septembre 2015 par lequel le maire de Saint-Doulchard a délivré à la société Saint-Doulchard Distribution un permis de construire pour la réalisation d'un centre commercial sur un terrain situé au lieu-dit " champ des quatre-vingt Boisselées ", ainsi que le rejet de son recours gracieux. Par une ordonnance n° 1700664 du 12 avril 2017, le juge des référés du tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'État les 27 avril et 12 mai 2017, la société Distribution Casino France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre conjointement à la charge de la commune de Saint-Doulchard et de la société Saint-Doulchard Distribution la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
                Vu les autres pièces du dossier ;<br/>
<br/>
                Vu :<br/>
                - la loi n° 2014-626 du 18 juin 2014 ;<br/>
                - la loi n° 2015-990 du 6 août 2015 ;<br/>
                - le décret n° 2015-165 du 12 février 2015 ;<br/>
                - le code de commerce ;<br/>
                 -le code de l'urbanisme ;<br/>
                - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Distribution Casino France, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Saint-Doulchard et à la SCP Piwnica, Molinié, avocat de la société Saint-Doulchard Distribution.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumises au juge des référés que le maire de Saint-Doulchard a, par un arrêté du 9 septembre 2015, délivré un permis de construire à la société Saint-Doulchard Distribution pour la réalisation d'un centre commercial sur un terrain situé au lieu-dit " champ des quatre-vingt boisselées " ; que la société Distribution Casino France, exploitant un hypermarché sur le territoire de la même commune a, par deux requêtes distinctes enregistrées les 28 janvier et 28 février 2016 au greffe du tribunal administratif d'Orléans, demandé l'annulation de cet arrêté ainsi que la suspension de son exécution sur le fondement de l'article L. 521-1 du code de justice administrative ; que, par une ordonnance du 12 avril 2017, le juge des référés du tribunal administratif d'Orléans a rejeté la demande tendant à la suspension de l'exécution de l'arrêté au motif tiré de l'incompétence du tribunal administratif pour connaître du litige ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des mentions de l'ordonnance attaquée que le moyen tiré de l'incompétence du tribunal administratif a été soulevé au cours de l'audience publique du 10 avril 2017 ; que, dans ces circonstances et eu égard à son office, le moyen ayant été discuté au cours de l'audience, le juge des référés n'a pas méconnu le principe du caractère contradictoire de l'instruction en prononçant la clôture de l'instruction à l'issue de cette dernière ;<br/>
<br/>
              3. Considérant, en second lieu, que, d'une part, aux termes de l'article L. 600-10 du code de l'urbanisme : " Les cours administratives d'appel sont compétentes pour connaître en premier et dernier ressort des litiges relatifs au permis de construire tenant lieu d'autorisation d'exploitation commerciale prévu à l'article L. 425-4. " ; qu'en vertu des dispositions combinées de l'article 60 de la loi du 18 juin 2014 relative à l'artisanat, au commerce et aux très petites entreprises et de l'article 4 du décret du 12 février 2015 relatif à l'aménagement commercial, ces dispositions sont entrées en vigueur le 15 février 2015 ; que, d'autre part, l'article L. 425-4 du code de l'urbanisme, introduit par l'article 39 de la loi du 18 juin 2014, dispose que : " Lorsque le projet est soumis à autorisation d'exploitation commerciale au sens de l'article L. 752-1 du code de commerce, le permis de construire tient lieu d'autorisation dès lors que la demande de permis a fait l'objet d'un avis favorable de la commission départementale d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial (...) " ; que l'article 36 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques a ajouté un III à l'article 39 de la loi du 18 juin 2014 au terme duquel : " Pour tout projet nécessitant un permis de construire, l'autorisation d'exploitation commerciale, en cours de validité, dont la demande a été déposée avant le 15 février 2015 vaut avis favorable des commissions d'aménagement commercial. " ; <br/>
<br/>
              4. Considérant qu'il résulte de la combinaison de ces dispositions que, dans le cas où un projet n'aurait pas préalablement fait l'objet d'un avis favorable des commissions d'aménagement commercial mais bénéficierait d'une autorisation d'exploitation commerciale en cours de validité avant le 15 février 2015, le permis de construire tient lieu d'autorisation d'exploitation commerciale ; qu'en l'espèce, l'autorisation d'exploitation commerciale, qui a été délivrée le 2 juillet 2014 par la commission départementale d'aménagement commercial du Cher, a été demandée avant le 15 février 2015 et vaut dès lors avis favorable des commissions d'aménagement commercial ; que, par suite, le permis de construire délivré le 9 septembre 2015 à la société Saint-Doulchard Distribution tient lieu d'autorisation d'exploitation commerciale prévue à l'article L. 425-4 du code de l'urbanisme ; qu'il en résulte que le juge des référés n'a pas commis d'erreur de droit en jugeant que le litige relatif à ce permis de construire relevait de la compétence en premier et dernier ressort de la cour administrative d'appel de Nantes en application de l'article L. 600-10 du même code ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la requérante n'est pas fondée à demander l'annulation de l'ordonnance attaquée ; que son pourvoi doit par suite être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Distribution Casino France la somme demandée par la commune de Saint-Doulchard au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
          --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Distribution Casino France est rejeté.<br/>
Article 2 : Les conclusions présentées par la commune de Saint-Doulchard au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Distribution Casino France, à la commune de Saint-Doulchard et à la société Saint-Doulchard Distribution.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
