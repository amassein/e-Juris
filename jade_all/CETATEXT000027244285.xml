<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027244285</ID>
<ANCIEN_ID>JG_L_2013_03_000000350121</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/24/42/CETATEXT000027244285.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/03/2013, 350121, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350121</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350121.20130328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juin et 14 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant..., ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE02654 du 14 avril 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0708111 du 10 juillet 2009 du tribunal administratif de Versailles rejetant sa demande tendant à la désignation d'un expert et à la condamnation de l'Etat à lui verser la somme de 338 090 euros en réparation du manque à gagner et du préjudice moral causés par sa mise à la retraite anticipée ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le décret n° 86-442 du 14 mars 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de Mme B...,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., professeur d'enseignement général de collège, a été placée en disponibilité d'office le 5 mars 1995, à l'issue d'un congé de maladie de longue durée ; qu'elle a présenté, le 10 juillet 1995, une demande tendant à obtenir sa mise à la retraite anticipée pour invalidité à laquelle, après avis de la commission de réforme prévue à l'article L. 31 du code des pensions civiles et militaires de retraite en date du 28 novembre 1995, il a été fait droit par arrêté du recteur de l'académie de Versailles du 4 décembre 1995 ; que, soutenant que cette demande avait été présentée sous une contrainte psychologique et économique causée par le harcèlement dont elle aurait été victime de la part de la direction de son collège elle a, après une réclamation indemnitaire au recteur de l'académie de Versailles restée sans réponse, saisi le tribunal administratif de Versailles d'une demande tendant, d'une part, à la désignation d'un expert afin de déterminer si, à la date de l'arrêté du 4 décembre 1995 la mettant en retraite anticipée, elle était dans un état psychique justifiant cette décision et d'évaluer son préjudice et, d'autre part, à ce qu'une indemnité totale de 338 090 euros lui soit versée, au titre notamment du préjudice résultant du manque à gagner qu'elle aurait subi sur le montant de ses salaires et de sa pension de retraite ; que le tribunal administratif de Versailles a rejeté cette demande par un jugement du 10 juillet 2009 ; que Mme B... se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2. Considérant que, pour demander, devant la cour, l'annulation du jugement du tribunal administratif, Mme B... soutenait notamment que, malgré sa dépression, son état psychique à la date de sa mise en retraite anticipée n'était pas incompatible avec l'exercice de ses fonctions et, qu'ainsi, l'arrêté du 4 décembre 1995 était entaché d'illégalité ; que la cour a omis de se prononcer sur ce moyen, qui n'était pas inopérant ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 29 du code des pensions civiles et militaires de retraite : " Le fonctionnaire civil qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'une invalidité ne résultant pas du service peut être radié des cadres par anticipation soit sur sa demande, soit d'office (...) " ; qu'aux termes de l'article L. 31 du même code, la décision est prise après avis de la commission de réforme ;<br/>
<br/>
              5. Considérant, en premier lieu, que Mme B... soutient que sa demande de mise à la retraite anticipée aurait été entachée d'un vice du consentement, qui serait imputable à des faits de harcèlement qu'elle allègue avoir subis et au manque de discernement qu'ils ont pu engendrer au moment de formuler cette demande ;<br/>
<br/>
              6. Considérant que Mme B... se borne à fournir des témoignages selon lesquels elle aurait, à tort, été omise de la liste des électeurs au conseil d'administration de son établissement pendant son congé de longue maladie ; que le ministre relève, en défense, que ce fait ne saurait, en tout état de cause, caractériser un comportement de harcèlement ; qu'il résulte ainsi de l'instruction que les faits de harcèlement dont Mme B... aurait été la victime de la part de la direction du collège où elle était affectée ne sont pas établis ; que, par ailleurs, Mme B... n'établit pas un éventuel manque de discernement à l'occasion de sa demande de mise à la retraite, dont la preuve ne saurait résulter d'un certificat médical rédigé plusieurs années après les faits par un médecin généraliste dans des termes ambigus ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que Mme B... soutient, également, que sa demande de mise à la retraite anticipée a été formulée sous la contrainte économique résultant de la perspective de perte de son traitement au terme de son congé de maladie de longue durée ; que toutefois cette circonstance, qui résulte de l'application des textes statutaires, n'aurait pas été différente si Mme B... s'était abstenue de formuler sa demande de mise à la retraite anticipée, avec laquelle elle est, ainsi, dépourvue de relation ;<br/>
<br/>
              8. Considérant, en troisième lieu, que si Mme  B...soutient que c'est à tort que, par l'arrêté du 4 décembre 1995, il a été fait droit à sa demande de mise à la retraite anticipée, dans la mesure où elle n'aurait, en réalité, pas été inapte à reprendre ses fonctions, il résulte de l'instruction que cet arrêté a été pris après avis de la commission de réforme du département des Hauts-de-Seine qui, lors de sa séance du 28 novembre 1995, a conclu à son inaptitude définitive à reprendre ses fonctions ; qu'une seconde expertise de la commission de réforme le 28 mai 1996 a confirmé cette inaptitude ; que les deux certificats médicaux du 18 mars 1995 et du 6 juin 1995, qui ont un caractère très général, ne sont pas de nature à contredire les avis ainsi émis par la commission de réforme ; qu'il en va de même que les autres certificats médicaux produits par la requérante, postérieurs de plusieurs années à l'arrêté du 4 décembre 1995 ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que Mme B... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a rejeté sa demande ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 14 avril 2011 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : La requête d'appel de Mme B... est rejetée.<br/>
Article 3 : Les conclusions présentées par Mme B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme A... B...et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
