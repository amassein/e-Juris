<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285450</ID>
<ANCIEN_ID>JG_L_2016_10_000000388940</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285450.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème chambre jugeant seule, 20/10/2016, 388940, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388940</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:388940.20161020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Basse-Terre de prononcer la décharge de l'obligation de payer la taxe foncière qui a été mise à sa charge au titre des années 1999 et 2006 à 2010 par un avis à tiers détenteur du 20 janvier 2012. Par un jugement n° 1200457 du 23 décembre 2014, le tribunal administratif de Basse-Terre a fait droit à sa demande relative à la taxe foncière due au titre des années 1999, 2006 et 2007 et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n° 15BX00574 du 9 mars 2015, enregistrée le 24 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 20 février 2015 au greffe de cette cour, présenté par M. B.... Par ce pourvoi et par un nouveau mémoire, enregistré le 1er juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande :<br/>
<br/>
              1°) d'annuler l'article 2 de ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1400 du code général des impôts : " (...) toute propriété, bâtie ou non bâtie, doit être imposée au nom du propriétaire actuel (...) ". En vertu de l'article 1403 du même code : " Tant que la mutation cadastrale n'a pas été faite, l'ancien propriétaire continue à être imposé au rôle, et lui ou ses héritiers naturels peuvent être contraints au paiement de la taxe foncière, sauf leur recours contre le nouveau propriétaire ". Il résulte de ces dispositions qu'en cas de décès du propriétaire et tant que la mutation cadastrale n'a pas été faite, les héritiers du propriétaire sont chacun tenus à hauteur de leur part dans l'indivision au paiement de la taxe foncière. L'obligation de payer la taxe foncière incombant à un propriétaire indivis ne saurait excéder ses droits dans l'indivision, dès lors que la solidarité ne s'attache pas de plein droit à la qualité d'indivisaire.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le comptable public de Baie-Mahault a émis un avis à tiers détenteur en vue du paiement par M. A... B...de la totalité de la taxe foncière sur les propriétés bâties établie au titre d'un bien immobilier situé 28, rue Bel-Air à Petit-Bourg pour les années 2008 à 2010 encore en litige. M. A... B...n'est toutefois que l'un des indivisaires de la succession de M. C...B..., propriétaire du bien immobilier en cause, dont la taxe foncière est toujours établie au nom de ce dernier en l'absence de mutation cadastrale. En jugeant que la totalité de la taxe foncière de l'indivision pouvait être réclamée à M. A... B..., même si sa qualité de propriétaire indivisaire ne le rendait pas débiteur de la totalité de cette taxe, alors que seul le recouvrement de la part lui incombant pouvait être légalement poursuivi par l'administration, le tribunal a commis une erreur de droit.<br/>
<br/>
              3. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de l'article 2 du jugement attaqué. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État le versement d'une somme de 3 000 euros à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du 23 décembre 2014 du tribunal administratif de Basse-Terre est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de la Guadeloupe.<br/>
Article 3 : L'État versera une somme de 3 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
