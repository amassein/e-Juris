<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038451693</ID>
<ANCIEN_ID>JG_L_2019_04_000000429839</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/45/16/CETATEXT000038451693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/04/2019, 429839, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429839</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429839.20190425</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, d'ordonner la suspension de l'arrêté du 13 mars 2019 par lequel le président de l'université de Strasbourg a défini les modalités d'aménagement de ses examens au titre du second semestre de la troisième année de licence pour l'année universitaire 2018/2019 et, en second lieu, d'enjoindre à l'université de Strasbourg d'aménager, dans un délai de quarante-huit heures à compter de l'ordonnance à intervenir et au besoin sous astreinte, les examens prévus en avril 2019 en prévoyant une organisation des épreuves à son domicile, des sujets de substitution, l'utilisation de l'ordinateur personnel avec les logiciels utilisés en cours d'année, l'adaptation des supports, la dispense de l'épreuve d'anglais et du contrôle continu et de l'épreuve finale du cours d'analyse de la structure sociale, la soutenance du mémoire à distance et la conservation des notes durant 5 ans. Par une ordonnance n° 1902862 du 16 avril 2019, le juge des référés du tribunal administratif de Strasbourg a rejeté sa requête. <br/>
              Par une requête, enregistré le 17 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que le refus de l'université de renouveler son droit à l'aménagement des modalités de passation d'examens l'a privée du droit de passer les épreuves de première session d'examens imposée à Strasbourg ;<br/>
              - une atteinte à son droit à l'éducation est portée par le refus de l'université de Strasbourg de renouveler son droit à obtenir des aménagements pour la passation de ses examens, rendus nécessaire par sa situation d'handicap ;<br/>
              - le refus de l'université de Strasbourg d'aménager ses examens porte une atteinte grave et manifestement illégale au principe d'égal accès à l'instruction et à son corollaire le droit à l'aménagement des études et des conditions d'examen dès lors qu'il constitue une discrimination fondée sur le handicap.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 24 avril 2019, la ministre de l'enseignement supérieur, de la recherche et de l'innovation conclut au rejet de la requête. <br/>
<br/>
              Par un mémoire en défense, enregistré le 24 avril 2019, l'université de Strasbourg conclut au non-lieu à statuer. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la convention de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention des Nations Unies du 13 décembre 2006 relative aux droits des personnes handicapées ;<br/>
              - la convention internationale relative aux droits des personnes handicapées ; <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MmeB..., d'autre part, l'université de Strasbourg et la ministre de l'enseignement supérieur, de la recherche et de l'innovation ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 25 avril 2019 à 9 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Perier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B... ;<br/>
<br/>
              - la représentante de MmeB... ;<br/>
<br/>
              - Me Garreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'université de Strasbourg ;<br/>
<br/>
              - le représentant de la ministre de l'enseignement supérieur, de la recherche et de l'innovation ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. " ;<br/>
<br/>
              2. Mme B...est une étudiante en situation de handicap inscrite en 3ème année de licence de sociologie en enseignement à distance. Elle a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'arrêté du 13 mars 2019 par lequel le président de l'université de Strasbourg a défini les modalités d'aménagement de ses examens au titre du second semestre de la troisième année de licence pour l'année universitaire 2018/2019 et, d'autre part, d'enjoindre à l'université de Strasbourg d'aménager les examens prévus en avril 2019 en prévoyant une organisation des épreuves à son domicile, des sujets de substitution, l'utilisation de l'ordinateur personnel avec les logiciels utilisés en cours d'année, l'adaptation des supports, la dispense de l'épreuve d'anglais et du contrôle continu et de l'épreuve finale du cours d'analyse de la structure sociale, la soutenance du mémoire à distance et la conservation des notes durant 5 ans. Par une ordonnance du 16 avril 2019, le juge des référés du tribunal administratif de Strasbourg a rejeté sa requête. Mme B...relève appel de cette ordonnance. <br/>
<br/>
              3. Des échanges entre les parties au cours de l'instruction, poursuivis au cours de l'audience, il résulte que l'Université de Strasbourg s'est engagée à faire droit dans les meilleurs délais aux demandes de MmeB.... A ce titre, l'université a rappelé que les conservations du bénéfice des notes durant cinq ans était de droit et son bénéfice entièrement acquis à la requérante. La soutenance du mémoire à distance est regardée comme achevée et notée. La dispense de l'épreuve d'anglais est actée. Par des amendements qui seront apportés à l'arrêté du 24 avril du directeur de la composante, doyen de la faculté des sciences sociales, et au tableau qui lui est annexé, l'université organisera, dans la semaine du 29 avril, six épreuves à domicile dans les conditions matérielles adaptées résultant du certificat médical qui y est joint ; ces épreuves, portant sur des sujets adaptés aux conditions de passation des épreuves, donneront lieu à une remise des copies dans un délai de 15 jours à compter de la remise des sujets ; au vu de l'ensemble de ces engagements, Mme B...estime qu'il lui a été donné entière satisfaction. Il n'y a donc plus lieu de statuer sur l'appel qu'elle avait interjeté.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de faire droit aux demandes de Mme B...tendant à ce que l'université de Strasbourg lui verse, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, une somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : : Il n'y a pas lieu de statuer sur les conclusions de MmeB.... <br/>
Article 2 : L'université de Strasbourg versera une somme de 3 000 euros à Mme B...sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente ordonnance sera notifiée à Mme A...B..., à l'université de Strasbourg et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
