<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076957</ID>
<ANCIEN_ID>JG_L_2016_02_000000385993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076957.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 17/02/2016, 385993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:385993.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 6 mai 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la commune de Saint-Fargeau-Ponthierry dirigées contre l'arrêt de la cour administrative d'appel de Paris du 23 septembre 2014 en tant seulement qu'il se prononce sur les conclusions dirigées contre la société des Eaux de l'Essonne. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Saint-Fargeau-Ponthierry et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société des Eaux de l'Essonne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juges du fond que, par une requête enregistrée le 12 avril 2007 devant le tribunal administratif de Melun, la commune de Saint-Fargeau-Ponthierry a engagé une action en réparation à l'encontre de l'Etat et de la société Freyssinet sur le fondement de la garantie décennale des constructeurs, au titre des désordres affectant les deux châteaux d'eau de Tilly ; qu'à la suite du rapport d'expertise établi le 3 mai 2010 mettant en cause les constructeurs, l'Etat, la société Freyssinet, ainsi que le délégataire du service public de distribution d'eau potable, la société des Eaux de l'Essonne, la commune a, le 23 octobre 2010, présenté de nouvelles conclusions demandant également la condamnation de la société des Eaux de l'Essonne sur le fondement de la responsabilité contractuelle au titre des désordres affectant les deux châteaux d'eau mentionnés ci-dessus ; que la cour administrative d'appel de Paris a jugé irrecevables ces nouvelles conclusions au motif qu'elles ne présentaient pas de lien suffisant avec l'objet de l'instance en cours ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune entendait obtenir réparation d'un préjudice unique, résultant du défaut d'étanchéité des deux châteaux d'eau de Tilly ; qu'elle a, avant la clôture de l'instruction, à la suite du rapport d'expertise qui avait pour objet de constater les désordres éventuels et d'en déterminer la cause et l'imputabilité, appelé en cause la société des Eaux de l'Essonne devant les juges de première instance au titre de ce même préjudice ; que la circonstance que la demande dirigée contre la société des Eaux de l'Essonne se fonde sur une cause juridique distincte de celle de sa demande initiale ne faisait pas obstacle à ce que les deux demandes aient un lien suffisant entre elles ; que, dans ces conditions, la cour administrative d'appel de Paris a inexactement qualifié les faits qui lui étaient soumis en jugeant que la demande dirigée contre la société des Eaux de l'Essonne ne présentait pas de lien suffisant avec l'instance en cours, et a, par suite, commis une erreur de droit en rejetant pour ce motif les conclusions tendant à la condamnation de la société des Eaux de l'Essonne ; que dès lors, sans qu'il soit besoin d'examiner les autres moyens, son arrêt doit être annulé en tant qu'il statue sur ces conclusions ; <br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Saint-Fargeau-Ponthierry qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société des Eaux de l'Essonne la somme de 3 000 euros sur le fondement de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 23 septembre 2014 est annulé en tant qu'il se prononce sur les conclusions dirigées contre la société des Eaux de l'Essonne. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : La société des Eaux de l'Essonne versera à la commune de Saint-Fargeau-Ponthierry la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées sur le même fondement sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Fargeau-Ponthierry et à la société des Eaux de l'Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
