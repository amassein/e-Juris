<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815795</ID>
<ANCIEN_ID>JG_L_2019_07_000000417830</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815795.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/07/2019, 417830, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417830</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417830.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La SARL JP et P. Amoreau a demandé au tribunal administratif de Montreuil d'annuler la décision du 18 janvier 2013 par laquelle le directeur de l'Institut national de l'origine et de la qualité (INAO) a rejeté sa demande tendant à la reconnaissance d'un vin en appellation d'origine contrôlée (AOC) sous la dénomination " Le Puy ". <br/>
<br/>
              Par une ordonnance n° 1303192 du 22 avril 2013, le président du tribunal administratif de Montreuil a transmis la demande au tribunal administratif de Bordeaux.<br/>
<br/>
              Par un jugement n° 1301544 du 24 mars 2015, le tribunal administratif de Bordeaux a rejeté la demande de la SARL JP et P. Amoreau.<br/>
<br/>
              Par un arrêt n° 15BX01780 du 1er décembre 2017, la cour administrative d'appel de Bordeaux a, sur appel de la SARL JP et P. Amoreau, annulé ce jugement et rejeté la demande de cette dernière.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er février et 2 mai 2018 au secrétariat du contentieux du Conseil d'Etat, la SARL JP et P. Amoreau demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 et 3 de cet arrêt ;<br/>
<br/>
              2°) de renvoyer l'affaire devant une cour administrative d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'INAO une somme de 5 000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1234/2007 du Conseil du 22 octobre 2007 ;<br/>
              - le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la Société JP et P. Amoreau et à la SCP Didier, Pinet, avocat de l'institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL JP et P. Amoreau, producteur de vins sur le territoire de la commune de Saint-Cibard (Gironde), a demandé à l'Institut national de l'origine et de la qualité (INAO), en tant que producteur isolé, la reconnaissance de l'appellation d'origine contrôlée " Le Puy ". Par une délibération du 18 décembre 2012, la commission permanente du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie de l'INAO a rejeté sa demande. Par un jugement du 24 mars 2015, le tribunal administratif de Bordeaux a rejeté la demande de la SARL JP et P. Amoreau tendant à l'annulation de cette décision. La SARL JP et P. Amoreau se pourvoit en cassation contre l'arrêt du 1er décembre 2017 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé ce jugement, a rejeté sa demande. Eu égard aux moyens soulevés, son pourvoi doit être regardé comme tendant seulement à l'annulation des articles 2 et 3 de cet arrêt. Par la voie du pourvoi incident, l'INAO demande l'annulation du même arrêt en tant qu'il a écarté l'exception de non-lieu qu'il avait soulevée devant elle. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que les conclusions de la demande présentée par la SARL JP et P. Amoreau devant le tribunal administratif étaient dirigées contre la délibération du 18 décembre 2012 de la commission permanente du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie de l'INAO. Par une délibération du 4 septembre 2013, notifiée le 19 septembre 2013, la commission permanente du comité national des appellations d'origine relatives aux vins et aux boissons alcoolisées, et des eaux-de-vie de l'INAO a procédé, à la suite de la production d'éléments complémentaires par la société, à un nouvel examen de sa demande et décidé, avant de statuer sur cette demande, de recourir à une expertise. Cette décision devenue définitive, ne peut qu'être regardée comme ayant procédé au retrait de la décision contestée du 18 décembre 2012. Il résulte de ce qui précède qu'en ne constatant pas, après avoir annulé le jugement du tribunal administratif de Bordeaux du 24 mars 2015 et évoqué le litige, que celui-ci était privé d'objet, la cour a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les moyens du pourvoi principal, les articles 2 et 3 de son arrêt doivent être annulés.<br/>
<br/>
              3. Rien ne restant à juger eu égard à ce qui a été dit ci-dessus, il n'y a lieu pour le Conseil d'Etat ni de statuer sur le pourvoi incident de l'INAO dirigé contre les articles 2 et 3 de l'arrêt, ni de renvoyer l'affaire, ni de faire application des dispositions de l'article L. 821-2 du code de justice administrative pour régler l'affaire au fond.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la SARL JP et P. Amoreau une somme au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de l'INAO qui n'est pas pour l'essentiel, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 de l'arrêt du 1er décembre 2017 de la cour administrative d'appel de Bordeaux sont annulés.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions du pourvoi incident de l'INAO.<br/>
Article 3 : L'ensemble des conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SARL JP et P. Amoreau et à l'Institut national de l'origine et de la qualité (INAO).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
