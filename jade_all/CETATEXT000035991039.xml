<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035991039</ID>
<ANCIEN_ID>JG_L_2017_11_000000410433</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/10/CETATEXT000035991039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 08/11/2017, 410433</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410433</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410433.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Les amis de la Terre - Val d'Oise a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir l'arrêté du 30 avril 2014 par lequel le maire de Mériel a délivré à M. B...le permis d'aménager un lotissement en vue d'activités commerciales et artisanales. Par un jugement n° 1410076 du 23 décembre 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17VE00812 du 2 mai 2017, enregistrée le 10 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée par l'association Les amis de la Terre - Val d'Oise, enregistrée le 12 mars 2017 au greffe de cette cour. Par ce pourvoi et par un mémoire complémentaire, enregistré le 25 août 2017 au secrétariat du contentieux du Conseil d'Etat, cette association demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Cergy-Pontoise du 23 décembre 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Mériel et de M. A...la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2013-392 du 10 mai 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de l'association Les amis de la Terre - Val d'Oise.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              1. Aux termes de l'article R. 811-1-1 du code de justice administrative : " Les tribunaux administratifs statuent en premier et dernier ressort sur les recours contre les permis de construire ou de démolir un bâtiment à usage principal d'habitation ou contre les permis d'aménager un lotissement lorsque le bâtiment ou le lotissement est implanté en tout ou partie sur le territoire d'une des communes mentionnées à l'article 232 du code général des impôts et son décret d'application. / Les dispositions du présent article s'appliquent aux recours introduits entre le 1er décembre 2013 et le 1er décembre 2018 ". Ces dispositions ne subordonnent pas la compétence des tribunaux administratifs pour statuer en premier et dernier ressort sur les recours contre les permis d'aménager un lotissement à la destination des constructions qui ont vocation à être édifiées sur les lots qui en sont issus.<br/>
<br/>
              2. En l'espèce, tout d'abord, la commune de Mériel figure sur la liste annexée au décret du 10 mai 2013 relatif au champ d'application de la taxe annuelle sur les logements vacants instituée par l'article 232 du code général des impôts. Ensuite, il ressort des pièces de la procédure devant le tribunal administratif de Cergy-Pontoise que la demande de l'association Les amis de la Terre - Val d'Oise a été enregistrée au greffe de ce tribunal le 13 octobre 2014. Enfin, il ressort de ces mêmes pièces que cette demande tendait à l'annulation pour excès de pouvoir de l'arrêté du 30 avril 2014 par lequel le maire de Mériel a délivré à M. A...un permis d'aménagement en vue de la création d'un lotissement de onze lots à bâtir. Par suite, le jugement par lequel le tribunal a statué sur cette demande a été rendu en dernier ressort, alors même que le lotissement autorisé a pour objet la réalisation d'une zone d'activités commerciales et artisanales. La présente requête a ainsi le caractère d'un pourvoi, qui relève de la compétence du Conseil d'Etat, juge de cassation. <br/>
<br/>
              Sur les moyens du pourvoi :<br/>
<br/>
              3. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              4. Pour demander l'annulation du jugement du tribunal administratif de Cergy-Pontoise qu'elle attaque, l'association Les amis de la Terre - Val d'Oise soutient que : <br/>
              - le tribunal a dénaturé les pièces du dossier en jugeant que l'affichage du permis d'aménager était visible au sens de l'article R. 424-15 du code de l'urbanisme ;<br/>
              - il a dénaturé les pièces du dossier en jugeant que cet affichage avait été continu pendant plus de deux mois.<br/>
<br/>
              5. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association Les amis de la Terre - Val d'Oise n'est pas admis.<br/>
Article 2 : La présente décision sera notifiée à l'association Les amis de la Terre - Val d'Oise.<br/>
Copie en sera adressée à la commune de Mériel et à M.B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - SUPPRESSION TEMPORAIRE DE L'APPEL POUR LES RECOURS INTRODUITS CONTRE CERTAINS PERMIS DE CONSTRUIRE EN ZONE TENDUE (ART. R. 811-1-1 DU CJA) - APPLICATION AUX RECOURS CONTRE LES PERMIS D'AMÉNAGER UN LOTISSEMENT, QUELLE QUE SOIT LA DESTINATION DES CONSTRUCTIONS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. - SUPPRESSION TEMPORAIRE DE L'APPEL POUR LES RECOURS INTRODUITS CONTRE CERTAINS PERMIS DE CONSTRUIRE EN ZONE TENDUE (ART. R. 811-1-1 DU CJA) -  APPLICATION AUX RECOURS CONTRE LES PERMIS D'AMÉNAGER UN LOTISSEMENT, QUELLE QUE SOIT LA DESTINATION DES CONSTRUCTIONS - EXISTENCE.
</SCT>
<ANA ID="9A"> 17-05-012 L'article R. 811-1-1 du code de justice administrative (CJA) ne subordonne pas la compétence des tribunaux administratifs pour statuer en premier et dernier ressort sur les recours contre les permis d'aménager un lotissement à la destination des constructions qui ont vocation à être édifiées sur les lots qui en sont issus.</ANA>
<ANA ID="9B"> 68-06-01 L'article R. 811-1-1 du code de justice administrative (CJA) ne subordonne pas la compétence des tribunaux administratifs pour statuer en premier et dernier ressort sur les recours contre les permis d'aménager un lotissement à la destination des constructions qui ont vocation à être édifiées sur les lots qui en sont issus.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
