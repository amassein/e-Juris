<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847489</ID>
<ANCIEN_ID>JG_L_2018_12_000000412984</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/74/CETATEXT000037847489.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/12/2018, 412984, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412984</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412984.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Linpac Packaging Provence a demandé au tribunal administratif de Marseille le remboursement d'un crédit de taxe sur la valeur ajoutée d'un montant de 374 838,85 euros. Par un jugement n° 1305741 du 18 décembre 2015, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA00506 du 1er juin 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 1er août et 2 novembre 2017 et le 4 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Linpac Packaging Provence demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Linpac Packaging Provence ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Linpac Packaging Provence, qui a exercé une activité de fabrication et vente de produits d'emballages alimentaires, a présenté, le 21 décembre 2012, sur le fondement de l'article 242-0 G de l'annexe II au code général des impôts, une demande de remboursement d'un crédit de taxe sur la valeur ajoutée résultant de l'émission de deux notes d'avoir établies le même jour pour des opérations réalisées en 2008. Cette réclamation a été rejetée par l'administration fiscale. Par un jugement du 18 décembre 2015, le tribunal administratif de Marseille a rejeté la demande de remboursement du crédit de taxe en litige. La société demande l'annulation de l'arrêt du 1er juin 2017 par lequel la cour administrative d'appel de Marseille a confirmé le jugement.   <br/>
<br/>
              2. Aux termes du 1 de l'article 272 du code général des impôts : " La taxe sur la valeur ajoutée qui a été perçue à l'occasion de ventes ou de services est imputée ou remboursée dans les conditions prévues à l'article 271 lorsque ces ventes ou services sont par la suite résiliés ou annulés ou lorsque les créances correspondantes sont devenues définitivement irrecouvrables. / (...) / L'imputation ou la restitution est subordonnée à la justification, auprès de l'administration, de la rectification préalable de la facture initiale ". Aux termes de l'article 242-0 G de l'annexe II à ce code : " Lorsqu'un redevable perd cette qualité, le crédit de taxe déductible dont il dispose peut faire l'objet d'un remboursement pour son montant total ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la société Linpac Packaging Provence a déposé une demande de remboursement de crédit de taxe sur la valeur ajoutée au titre du mois de janvier 2009 qui n'a été que partiellement admise, faute notamment d'avoir justifié l'émission de deux avoirs accordés en 2008 à son principal client. Par un jugement du 2 octobre 2012, confirmé par un arrêt de la cour administrative d'appel de Marseille du 6 novembre 2014, le tribunal administratif de Marseille a rejeté la demande tendant au remboursement du reliquat de crédit de taxe non remboursé, d'un montant de 563 516 euros. La société Linpac Packaging Provence a, après le jugement du tribunal administratif, annulé les deux avoirs émis en 2008, émis deux nouvelles notes d'avoir de mêmes montants et relatifs aux mêmes opérations, annulé comptablement le reliquat de crédit de taxe non remboursé par l'enregistrement d'une charge exceptionnelle au titre de l'exercice clos le 31 décembre 2012, et demandé, sur le fondement de l'article 242-0 G de l'annexe II au code général des impôts, compte tenu de sa cessation d'activité en juin 2010, le remboursement d'un crédit de taxe sur la valeur ajoutée correspondant au montant figurant sur les deux nouveaux avoirs.<br/>
<br/>
              4. La cour a relevé que si le tableau n° 2057 de la liasse fiscale de l'exercice clos en 2011 de la SAS Linpac Packaging Provence faisait état d'une créance de 563 516 euros, la déclaration  déposée au titre de l'exercice clos en 2012 ne mentionnait plus de créance de taxe sur la valeur ajoutée, alors que le montant de 563 516 euros figurait désormais en charges exceptionnelles sous un compte 6718 intitulé " 2009 crédit TVA " et en a déduit que, ses propres écritures lui étant opposables, la société ne pouvait obtenir le remboursement qu'elle sollicitait dès lors que ce crédit avait été annulé comptablement. <br/>
<br/>
              5. Le traitement comptable d'une créance de taxe sur la valeur ajoutée, même erroné ou injustifié, comme les mentions relatives à une telle créance dans les déclarations faites au titre de l'impôt sur les sociétés, sont sans incidence sur le droit du contribuable de justifier de l'existence de cette créance auprès du Trésor public. Par suite, en jugeant que la société Linpac Packaging Provence ne pouvait demander en 2012 le remboursement du crédit de taxe sur la valeur ajoutée en litige au motif, déterminant selon elle, de l'annulation comptable de la créance par l'enregistrement d'une charge exceptionnelle au titre de l'exercice clos en 2012, la cour a commis une erreur de droit. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 1er juin 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La présente décision sera notifiée à la société Linpac Packaging Provence et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
