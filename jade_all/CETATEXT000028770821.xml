<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028770821</ID>
<ANCIEN_ID>JG_L_2014_03_000000361510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/77/08/CETATEXT000028770821.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 24/03/2014, 361510</TITRE>
<DATE_DEC>2014-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Eric Aubry</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361510.20140324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 juillet et 30 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C..., demeurant ... ; M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA01435 du 29 mai 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0902353 du 4 février 2010 par lequel le tribunal administratif de Nice a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 20 avril 2009 par laquelle le maire d'Antibes a refusé d'autoriser le transfert à son bénéfice de la licence de taxi dont est titulaire M. B...et, d'autre part, à la condamnation de la commune d'Antibes à lui verser une indemnité de 28 000 euros en réparation du préjudice financier subi du fait du versement des mensualités de la licence de taxi et à lui rembourser mensuellement le coût de la location de la licence jusqu'à ce que le maire lui donne l'autorisation de cession, soit une somme mensuelle de 3476,58 euros à compter du 1er juillet 2009 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Antibes la somme de 4 000 euros au titre des dispositions de  l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 95-66 du 20 janvier 1995 ;<br/>
<br/>
              Vu le décret n° 95-935 du 17 août 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Aubry, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. C...et à la SCP Célice, Blancpain, Soltner, avocat de la commune d'Antibes ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C...exploitait, depuis le 1er janvier 2006, en qualité de locataire, une autorisation de stationnement de taxi dont M. B...était titulaire ; que M. C...a signé avec ce dernier un compromis de cession  de cette autorisation ; que, par une décision du 20 avril 2009, le maire d'Antibes a fait connaître au conseil de l'intéressé qu'il s'opposait à la cession de cette licence ; que M. C...se pourvoit en cassation contre l'arrêt du 29 mai 2012 par lequel la cour administrative d'appel de Marseille a confirmé le jugement du 4 février 2010 du tribunal administratif de Nice qui avait rejeté sa demande tendant à l'annulation de cette décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2212-2 du code général des collectivités territoriales : " La police municipale a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques. Elle comprend notamment : / 1° Tout ce qui intéresse la sûreté et la commodité du passage dans les rues, quais, places et voies publiques (....) " ; qu'aux termes de l'article 1er de la loi du 20 janvier 1995 relative à l'accès à l'activité de conducteur et à la profession d'exploitant de taxi, alors en vigueur et dont la substance est désormais reprise à l'article L. 3121-1 du code des transports : " L'appellation de taxi s'applique à tout véhicule automobile de neuf places assises au plus, y compris celle du chauffeur, muni d'équipements spéciaux, dont le propriétaire ou l'exploitant est titulaire d'une autorisation de stationnement sur la voie publique en attente de la clientèle, afin d'effectuer à la demande de celle-ci et à titre onéreux le transport particulier des personnes et de leurs bagages " ; qu'aux termes de l'article 3 de la même loi, désormais codifié à l'article L. 3121-2 du même code : " Le titulaire d'une autorisation de stationnement a la faculté de présenter à titre onéreux un successeur à l'autorité administrative qui a délivré celle-ci. / Cette faculté est subordonnée à l'exploitation effective et continue pendant une durée de cinq ans de l'autorisation de stationnement à compter de la date de délivrance de celle-ci (...) " ; qu'aux termes de l'article 7 de la même loi, devenu l'article     L. 3121-6 du code : " Les dispositions de la présente loi ne font pas obstacle à l'exercice par les autorités administratives compétentes des pouvoirs qu'elles détiennent, dans l'intérêt de la sécurité et de la commodité de la circulation sur les voies publiques, en matière d'autorisation de stationnement. " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la cession d'une autorisation de stationnement de taxi permettant la poursuite de l'exploitation de cette activité sur le territoire d'une commune est subordonnée à une autorisation du maire ; que celui-ci peut fonder un refus d'autorisation sur un motif tiré de ce que les conditions posées par la loi ne seraient pas remplies mais aussi sur des motifs tenant à l'ordre public, notamment à la sécurité et à la commodité de la circulation sur les voies publiques ; qu'à ce titre, il est loisible au maire de prendre en considération des circonstances de nature à mettre en cause la sécurité des personnes transportées ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en estimant que la décision attaquée du maire d'Antibes pouvait être légalement fondée sur un motif d'ordre public tenant au comportement personnel de M.C..., la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant que, pour rejeter l'appel de M.C...,  la cour a estimé que la décision du maire était légalement justifiée par la nécessité de la préservation de l'ordre public, eu égard à la nature de l'agression commise par celui-ci en 2007 à l'encontre d'une cliente octogénaire se trouvant dans son taxi, qui avait donné lieu à une condamnation pénale comportant une peine d'emprisonnement assortie du sursis avec une mise à l'épreuve de deux années, et que cette décision ne portait pas une atteinte disproportionnée à la liberté du commerce et de l'industrie dès lors qu'elle ne l'empêchait pas d'exercer l'activité de conducteur de taxi ; que ce faisant, la cour, qui a suffisamment motivé son arrêt, n'a pas donné aux faits qu'elle a souverainement appréciés une inexacte qualification juridique ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. C...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de  la commune d'Antibes qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions qu'elle présente au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : Le pourvoi de M. C...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par la commune d'Antibes au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...C...et à la commune d'Antibes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA CIRCULATION ET DU STATIONNEMENT. - POUVOIR D'AUTORISATION DE CESSION D'UNE AUTORISATION DE STATIONNEMENT DE TAXI - MOTIFS DE REFUS - INCLUSION - MOTIFS TENANT À LA SÉCURITÉ ET À LA COMMODITÉ DE LA CIRCULATION SUR LES VOIES PUBLIQUES - CONSÉQUENCE - POSSIBILITÉ POUR LE MAIRE DE PRENDRE EN CONSIDÉRATION DES CIRCONSTANCES DE NATURE À METTRE EN CAUSE LA SÉCURITÉ DES PERSONNES TRANSPORTÉES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-02-01-06-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. TAXIS. POUVOIRS DES MAIRES. - POUVOIR D'AUTORISATION DE CESSION D'UNE AUTORISATION DE STATIONNEMENT DE TAXI - MOTIFS DE REFUS - INCLUSION - MOTIFS TENANT À LA SÉCURITÉ ET À LA COMMODITÉ DE LA CIRCULATION SUR LES VOIES PUBLIQUES - CONSÉQUENCE - POSSIBILITÉ POUR LE MAIRE DE PRENDRE EN CONSIDÉRATION DES CIRCONSTANCES DE NATURE À METTRE EN CAUSE LA SÉCURITÉ DES PERSONNES TRANSPORTÉES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-04-01 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. - POUVOIR D'AUTORISATION DE CESSION D'UNE AUTORISATION DE STATIONNEMENT DE TAXI - MOTIFS DE REFUS - INCLUSION - MOTIFS TENANT À LA SÉCURITÉ ET À LA COMMODITÉ DE LA CIRCULATION SUR LES VOIES PUBLIQUES - CONSÉQUENCE - POSSIBILITÉ POUR LE MAIRE DE PRENDRE EN CONSIDÉRATION DES CIRCONSTANCES DE NATURE À METTRE EN CAUSE LA SÉCURITÉ DES PERSONNES TRANSPORTÉES.
</SCT>
<ANA ID="9A"> 135-02-03-02-04 Il résulte des dispositions de l'article L. 2212-2 du code général des collectivités territoriales et des articles L. 3121-1 et suivants du code des transports dont la substance est issue de la loi n° 95-66 du 20 janvier 1995 que la cession d'une autorisation de stationnement de taxi permettant la poursuite de l'exploitation de cette activité sur le territoire d'une commune est subordonnée à une autorisation du maire. Celui-ci peut fonder un refus d'autorisation sur un motif tiré de ce que les conditions posées par la loi ne seraient pas remplies, mais aussi sur des motifs tenant à l'ordre public, notamment à la sécurité et à la commodité de la circulation sur les voies publiques. A ce titre, il est loisible au maire de prendre en considération des circonstances de nature à mettre en cause la sécurité des personnes transportées.</ANA>
<ANA ID="9B"> 14-02-01-06-02 Il résulte des dispositions de l'article L. 2212-2 du code général des collectivités territoriales et des articles L. 3121-1 et suivants du code des transports dont la substance est issue de la loi n° 95-66 du 20 janvier 1995 que la cession d'une autorisation de stationnement de taxi permettant la poursuite de l'exploitation de cette activité sur le territoire d'une commune est subordonnée à une autorisation du maire. Celui-ci peut fonder un refus d'autorisation sur un motif tiré de ce que les conditions posées par la loi ne seraient pas remplies, mais aussi sur des motifs tenant à l'ordre public, notamment à la sécurité et à la commodité de la circulation sur les voies publiques. A ce titre, il est loisible au maire de prendre en considération des circonstances de nature à mettre en cause la sécurité des personnes transportées.</ANA>
<ANA ID="9C"> 49-04-01 Il résulte des dispositions de l'article L. 2212-2 du code général des collectivités territoriales et des articles L. 3121-1 et suivants du code des transports dont la substance est issue de la loi n° 95-66 du 20 janvier 1995 que la cession d'une autorisation de stationnement de taxi permettant la poursuite de l'exploitation de cette activité sur le territoire d'une commune est subordonnée à une autorisation du maire. Celui-ci peut fonder un refus d'autorisation sur un motif tiré de ce que les conditions posées par la loi ne seraient pas remplies, mais aussi sur des motifs tenant à l'ordre public, notamment à la sécurité et à la commodité de la circulation sur les voies publiques. A ce titre, il est loisible au maire de prendre en considération des circonstances de nature à mettre en cause la sécurité des personnes transportées.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
