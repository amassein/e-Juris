<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030969391</ID>
<ANCIEN_ID>JG_L_2015_07_000000391648</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/96/93/CETATEXT000030969391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 30/07/2015, 391648, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391648</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:391648.20150730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 9 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Total marketing France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du 14 avril 2015 précisant les modalités de remboursement de la taxe intérieure de consommation sur les produits énergétiques dans le cadre des régimes visés aux articles 265 C, 265 bis et 265 nonies du code des douanes, en tant que cet arrêté subordonne, par le II de son article 8, l'exercice du droit à remboursement de l'utilisateur final bénéficiant du régime d'exonération de carburants d'aviation à la justification d'une impossibilité de s'approvisionner en carburants exonérés et de se constituer en stockage spécial ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie en ce que l'arrêté attaqué porte une atteinte grave et immédiate, d'une part, à son activité commerciale dans le secteur de l'aviation générale, de nature à remettre en cause la pérennité de sa présence sur ce segment de l'aviation, d'autre part, à la situation des petits aéroclubs et à la pérennité des entreprises d'aviation qui leur sont rattachées ;  <br/>
              - l'arrêté est entaché d'une illégalité externe en ce que, d'une part, il ne permet pas d'identifier la personne au nom duquel il a été pris, d'autre part, il a été signé par une autorité incompétente ;<br/>
              - l'article 2 du décret du 24 novembre 2014 relatif aux modalités de remboursement de certains droits et taxes perçus par l'administration des douanes, pour l'application duquel a été pris l'arrêté attaqué, est lui-même illégal en ce qu'il renvoie, sans encadrement suffisant, à un arrêté ministériel le soin de préciser les modalités d'exercice du droit à remboursement ;<br/>
              - l'arrêté contesté restreint illégalement la portée des articles 265 bis et 352 du code des douanes en ce qu'il subordonne l'exercice du droit à remboursement de la taxe intérieure de consommation sur les produits énergétiques au profit des utilisateurs finals de carburants d'aviation exonérés à des conditions de fond non prévues par la loi ;<br/>
              - l'arrêté méconnaît les règles relatives à la répétition de l'indu.   <br/>
<br/>
              Par un mémoire en défense, enregistré le 23 juillet 2015, le ministre des finances et des comptes publics conclut au rejet de la demande de suspension et à ce que la somme de 3 000 euros soit mise à la charge de la société Total marketing France en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Il soutient que :<br/>
              - la demande de suspension est irrecevable en conséquence de l'irrecevabilité de la demande d'annulation de l'arrêté attaqué, la société Total marketing France ne disposant pas d'un intérêt lui donnant qualité pour agir ;<br/>
              - la condition d'urgence n'est pas satisfaite, la société requérante ne justifiant pas que l'arrêté litigieux porterait une atteinte grave et immédiate à sa propre situation ou à un intérêt public ; <br/>
              - aucun des moyens soulevés n'est de nature à faire naître un doute sérieux sur la légalité de l'arrêté contesté.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive n° 2003/96/CE du 27 octobre 2003;<br/>
              - le code des douanes, notamment ses articles 265 bis et 352 ;<br/>
              - le décret n° 2014-1395 du 24 novembre 2014;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Total marketing France, d'autre part, le ministre des finances et des comptes publics ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 27 juillet 2015 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Total marketing France ;<br/>
<br/>
              - les représentants de la société Total marketing France ;<br/>
<br/>
              - Me Boré, avocat au Conseil d'Etat et à la Cour de cassation, avocat du ministre des finances et des comptes publics ;<br/>
<br/>
              - les représentants du ministre des finances et des comptes publics.<br/>
<br/>
              et a l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; <br/>
<br/>
              2. Considérant que le b) de l'article 14 de la directive du 27 octobre 2003 restructurant le cadre communautaire de la taxation des produits énergétiques et de l'électricité prévoit que les États membres exonèrent de la taxation " les produits énergétiques fournis en vue d'une utilisation comme carburant ou combustible pour la navigation aérienne autre que l'aviation de tourisme privée " ; que l'article 265 bis du code des douanes, dans sa rédaction issue de l'article 62 de la loi de finances rectificative pour 2007 du 25 décembre 2007 portant transposition de cette directive, prévoit que " les produits énergétiques mentionnés à l'article 265 sont admis en exonération de taxe intérieure de consommation lorsqu'ils sont destinés à être utilisés ... b) Comme carburant ou combustible à bord des aéronefs utilisés par leur propriétaire ou la personne qui en a la disposition à la suite d'une location, d'un affrètement ou à tout autre titre à des fins commerciales, notamment pour les besoins d'opérations de transport de personnes, de transport de marchandises ainsi que pour la réalisation de prestations de services à titre onéreux. L'exonération s'applique également aux aéronefs utilisés pour les besoins des autorités publiques " ; <br/>
<br/>
              3. Considérant que les utilisateurs finals de carburants d'aviation entrant dans le champ de cette disposition peuvent acquérir ces carburants en exonération de taxe, notamment en s'approvisionnant, selon des modalités exposées dans une décision administrative n°09000661 du 15 juillet 2009 publiée au bulletin officiel des douanes du 20 juillet 2009, auprès de " dépôts spéciaux de carburants d'aviation " (DSCA), établissements agréés par l'administration des douanes et placés sous son contrôle, ou en se constituant en " stockages spéciaux de carburant d'aviation " (SSCA), qui sont des dépôts autorisés par l'administration des douanes et placés sous son contrôle destinés à pourvoir aux besoins d'un unique opérateur ; qu'en vertu des dispositions de l'article 352 du code des douanes et de l'article 1er du décret du 24 novembre 2014 relatif aux modalités de remboursement de certains droits et taxes perçus par l'administration des douanes, pris pour son application, les utilisateurs qui, bien qu'entrant dans le champ de l'exonération, acquièrent des carburants d'aviation sous le régime des " droits acquittés " et supportent ainsi la taxe peuvent en demander la restitution jusqu'au 31 décembre de la deuxième année suivant celle du jour de l'émission de la facture ; que le II de l'article 8 de l'arrêté du 14 avril 2015, dont la suspension de l'exécution est demandée, prévoit toutefois que la demande de remboursement de l'utilisateur final doit être accompagnée de la justification de l'impossibilité géographique de s'approvisionner en carburant exonéré et de l'impossibilité se constituer en stockage spécial ; <br/>
              4. Considérant en premier lieu que pour justifier de l'urgence à suspendre l'exécution de l'arrêté contesté, la société Total marketing France, qui approvisionne de nombreux aérodromes de petite taille en carburants d'aviation sous le régime des " droits acquittés " et indique ne pas envisager, en raison du coût élevé de l'investissement que cela représenterait, de se porter titulaire d'un grand nombre de dépôts spéciaux de carburants d'aviation (DSCA), soutient que les conditions auxquelles l'arrêté subordonne désormais le remboursement de la taxe acquittée risquent de conduire tout ou partie de ses clients à s'implanter dans le périmètre d'autres aérodromes, sur lesquels se trouve un dépôt spécial de carburants d'aviation (DSCA), et à s'approvisionner ainsi auprès d'une société concurrente, ce qui lui causerait un préjudice grave, immédiat et irréversible ; qu'il ressort toutefois des écritures de cette société et des explications complémentaires apportées par elle lors de l'audience que le volume de carburants d'aviation qu'elle vend sous le régime des " droits acquittés " à des utilisateurs finals entrant dans le champ de l'exonération de taxe intérieure de consommation, et donc susceptible de demander la restitution de la taxe acquittée, est de l'ordre d'un millier de mètres cubes par an seulement ; que ce volume représente moins de dix pour cent des carburants qu'elle commercialise annuellement dans le secteur de l'aviation de loisir et une fraction négligeable de l'ensemble des carburants qu'elle vend ; que dans ces conditions, il n'apparait pas que la mise en oeuvre de l'arrêté soit susceptible de préjudicier à la société requérante dans des conditions caractérisant une situation d'urgence, au sens des dispositions de l'article L. 521-1 du code de justice administrative ;<br/>
<br/>
              5. Considérant en second lieu que la société requérante soutient que l'exécution de l'arrêté porterait également atteinte à un intérêt public en entraînant une modification immédiate du paysage français de l'aéronautique de loisir et un préjudice pour le tissu économique local dès lors qu'elle sera contrainte de cesser d'approvisionner les plus petits aérodromes et que de nombreuses petites entreprises à l'équilibre économique précaire devront s'exposer soit au risque d'un refus de restitution de la taxe, soit à des surcoûts pour aller s'approvisionner sur d'autres aérodromes que celui sur lequel elles sont basées ; que, toutefois, la réalité de telles conséquences immédiates de la mise en oeuvre de l'arrêté sur les petits aéroclubs et sur les entreprises qui leur sont rattachées n'apparaît pas établie, eu égard notamment aux montants de taxe en jeu pour chaque redevable et à la circonstance que ces opérateurs disposent, pour solliciter le remboursement de la taxe qu'ils ont acquittée, d'un délai qui excède celui dans lequel le Conseil d'Etat statuera, au fond, sur la demande d'annulation pour excès de pouvoir formée par la société Total marketing France contre cet arrêté ;   <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie ; qu'ainsi, sans qu'il soit besoin d'examiner si l'un au moins des moyens soulevés par la société requérante est de nature à créer, en l'état de l'instruction, un doute sérieux sur la légalité de l'arrêté litigieux, la demande de la société Total marketing France tendant à ce que soit ordonnée la suspension de son exécution doit être rejetée ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a par ailleurs pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Total marketing France le versement de la somme que l'Etat demande au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la SA Total marketing France est rejetée.<br/>
Article 2 : les conclusions du ministre des finances et des comptes publics présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à la SA Total marketing France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
