<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027697931</ID>
<ANCIEN_ID>JG_L_2013_07_000000348967</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/69/79/CETATEXT000027697931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 12/07/2013, 348967</TITRE>
<DATE_DEC>2013-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348967</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Patrick Quinqueton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:348967.20130712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 mai et 13 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Chasse-sur-Rhône, représentée par son maire ; elle demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler les articles 2 et 3 de l'arrêt n° 09LY01125 du 1er mars 2011 par lesquels la cour administrative d'appel de Lyon a rejeté son appel contre le jugement n° 0405657 du 19 mars 2009 du tribunal administratif de Grenoble qui avait déchargé les consorts B...de la participation fixée par l'autorisation de lotir du 9 février 2001 et a mis à sa charge une somme de 150 euros à verser à chacun des consorts B...sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge des consorts B...une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
				    Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 juin 2012, présentée pour la commune de Chasse-sur-Rhône ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Patrick Quinqueton, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Jacoupy, avocat de la commune de Chasse-sur-Rhône et à la SCP Barthélemy, Matuchansky, Vexliard, avocat des consortsB..., de MmeC..., néeB..., de MmeD..., épouse B...et de Mme A...née B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les consorts B...ont saisi le tribunal administratif de Grenoble d'une demande tendant à la décharge de la participation mise à leur charge sur le fondement de l'article L. 332-9 du code de l'urbanisme à l'occasion de la délivrance, le 9 février 2001, d'une autorisation de lotir par le maire de Chasse-sur-Rhône ; que, par un jugement du 19 mars 2009, le tribunal les a déchargés de cette participation ; que, par l'arrêt attaqué du 1er mars 2011, la cour administrative d'appel de Lyon, après avoir annulé le jugement en tant qu'il appliquait un taux d'intérêt majoré de cinq points à la somme à restituer, a rejeté le surplus des conclusions de la requête de la commune ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 332-9 du code de l'urbanisme, alors applicable : " Dans les secteurs de la commune où un programme d'aménagement d'ensemble a été approuvé par le conseil municipal, il peut être mis à la charge des constructeurs tout ou partie du coût des équipements publics réalisés dans l'intérêt principal des usagers des constructions à édifier dans le secteur concerné. /... / Le conseil municipal détermine le secteur d'aménagement, la nature, le coût et le délai prévus pour la réalisation du programme d'équipements publics. Il fixe, en outre, la part des dépenses de réalisation de ce programme qui est à la charge des constructeurs, ainsi que les critères de répartition de celle-ci entre les différentes catégories de constructions. Sa délibération fait l'objet d'un affichage en mairie. Une copie de cette délibération est jointe à toute délivrance de certificat d'urbanisme (...)  " ; qu'aux termes de l'article R. 332-25 du même code : " La délibération du conseil municipal ou de l'organe délibérant de l'établissement public de coopération intercommunale compétent approuvant, en application de l'article L. 332-9, un programme d'aménagement d'ensemble dans un ou plusieurs secteurs qu'elle délimite, accompagnée du document graphique faisant apparaître le ou les périmètres concernés, est affichée en mairie pendant un mois. Mention en est en outre insérée dans deux journaux régionaux ou locaux diffusés dans le département. / La délibération prend effet à compter de l'accomplissement de l'ensemble des formalités de publicité mentionnées à l'alinéa précédent (...) " ; <br/>
<br/>
              3. Considérant que la délibération du conseil municipal instituant un plan d'aménagement d'ensemble et mettant à la charge des constructeurs une participation au financement des équipements publics à réaliser doit identifier avec précision les aménagements prévus ainsi que leur coût prévisionnel et déterminer la part de ce coût mise à la charge des constructeurs, afin de permettre le contrôle du bien-fondé du montant de la participation réclamée à chaque constructeur ; que ces dispositions impliquent également, afin de permettre la répartition de la participation entre les constructeurs, que la délibération procède à une estimation quantitative des surfaces dont la construction est projetée à la date de la délibération et qui serviront de base à cette répartition ;<br/>
<br/>
              4. Considérant que la cour a relevé, d'une part, que si la délibération du conseil municipal de Chasse-sur-Rhône du 9 novembre 1993 approuvant le programme d'aménagement d'ensemble du secteur " Le lot-Soulins " mentionnait le coût total de ce programme, soit 9 213 270 francs hors taxes, et précisait que 95 % de cette somme serait mise à la charge des bénéficiaires d'autorisation de construire au prorata de la superficie hors oeuvre nette des constructions (SHON), elle ne précisait ni la superficie totale susceptible d'être réalisée dans le secteur concerné, ni un montant de participation à la charge des constructeurs par mètre carré ; qu'elle a relevé, d'autre part, que si la commune se prévalait du fait que la délibération du 9 novembre 1993 visait l'étude d'aménagement du 1er février 1993 effectuée par la direction départementale de l'équipement de l'Isère, qui mentionnait la SHON totale susceptible d'être réalisée, évaluée à 9 100 m², et un coût de 960 francs par mètre carré de SHON, la délibération n'indiquait pas s'approprier le contenu de cette étude et n'en reprenait pas les termes, qu'il ne résultait pas de l'instruction que l'étude aurait été annexée à la délibération et que la circonstance que la délibération du 9 novembre 1993 mentionnait une délibération du 26 mai 1993, qui s'approprierait le contenu de l'étude, était sans incidence ; que la cour n'a pas inexactement interprété ces actes en en déduisant que cette délibération ne permettait pas la vérification du bien-fondé des conditions de répartition des dépenses d'aménagement entre les constructeurs conformément aux dispositions de l'article L. 332-9 du code de l'urbanisme ; qu'en jugeant que cette délibération ne pouvait servir de fondement légal aux participations aux dépenses d'aménagement, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Mais considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Chasse-sur-Rhône, qui a réclamé aux lotisseurs une participation portant sur la réalisation des réseaux d'eaux usées et pluviales, les réseaux d'eau potable, d'électricité, de téléphone et de voirie et ainsi que d'espaces verts, a demandé une substitution de base légale fondée sur l'article L. 322-15 du code de l'urbanisme aux termes duquel, dans sa rédaction applicable à la date de la participation contestée : " L'autorité qui délivre l'autorisation de construire, d'aménager, ou de lotir exige, en tant que de besoin, du bénéficiaire de celle-ci la réalisation et le financement de tous travaux nécessaires à la viabilité et à l'équipement de la construction, du terrain aménagé ou du lotissement, notamment en ce qui concerne la voirie, l'alimentation en eau, gaz et électricité, les réseaux de télécommunication, l'évacuation et le traitement des eaux et matières usées, l'éclairage, les aires de stationnement, les espaces collectifs, les aires de jeux et les espaces plantés. / Les obligations imposées par l'alinéa ci-dessus s'étendent au branchement des équipements propres à l'opération sur les équipements publics qui existent au droit du terrain sur lequel ils sont implantés et notamment aux opérations réalisées à cet effet en empruntant des voies privées ou en usant de servitudes (...) " ;<br/>
<br/>
              6. Considérant, d'une part, que lorsqu'une illégalité n'entache pas le fondement légal qui a permis à l'administration d'agir, mais les motifs de sa décision, elle peut demander au juge de procéder à une substitution de motifs ; qu'il est cependant possible à l'administration, lorsqu'elle a pris une décision sur un fondement juridique erroné, de demander une substitution de base légale ;<br/>
<br/>
              7. Considérant, d'autre part, que, lorsque le juge du plein contentieux, saisi d'une demande tenant à la décharge d'une participation d'urbanisme, constate que la décision prévoyant le versement de cette participation aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui dont la méconnaissance est invoquée, il peut, le cas échéant d'office, substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la participation aurait dû lui être demandée ;<br/>
<br/>
              8. Considérant qu'ainsi qu'il a été dit au point 4, l'article L. 332-9 du code de l'urbanisme ne pouvait servir de fondement légal à la décision de mettre à la charge des consorts B... la participation en litige ; qu'en revanche, l'administration pouvait, eu égard aux travaux couverts par la participation mise à la charge des lotisseurs, présenter devant le juge statuant en plein contentieux une demande tendant à ce que soit substituée à ce fondement celui de l'article L. 322-15 du même code, dont il incombait à la cour d'apprécier le bien-fondé au regard des principes rappelés ci-dessus et de vérifier si ce nouveau fondement pouvait être légalement invoqué pour l'ensemble de ces dépenses ; que, par suite, la cour a commis une erreur de droit en jugeant, pour écarter l'argumentation de la commune, que celle-ci avait formulé une demande de substitution de motifs ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la commune de Chasse-sur-Rhône est fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune de Chasse-sur-Rhône, qui n'est pas la partie perdante dans la présente instance, la somme demandée par les consorts B... au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de ces derniers une somme à verser à la commune au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
	D E C I D E :	<br/>
--------------<br/>
<br/>
Article 1er : Les articles 2 et 3 de l'arrêt du 1er mars 2011 de la cour administrative d'appel de Lyon sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Chasse-sur-Rhône est rejeté.<br/>
Article 4 : Les conclusions des consorts B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Chasse-sur-Rhône et à Mme E...B..., épouseA..., premier défendeur dénommé. Les autres défendeurs seront informés de la présente décision par la SCP Barthélemy, Matuchansky et Vexliard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-05 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. SUBSTITUTION DE BASE LÉGALE. - 1) DISTINCTION ENTRE SUBSTITUTION DE BASE LÉGALE ET SUBSTITUTION DE MOTIFS - A) ILLÉGALITÉ N'ENTACHANT PAS LE FONDEMENT LÉGAL QUI A PERMIS À L'ADMINISTRATION D'AGIR, MAIS LES MOTIFS DE SA DÉCISION - FACULTÉ POUR L'ADMINISTRATION DE DEMANDER UNE SUBSTITUTION DE MOTIFS - EXISTENCE - B) DÉCISION PRISE SUR UN FONDEMENT JURIDIQUE ERRONÉ - FACULTÉ POUR L'ADMINISTRATION DE DEMANDER UNE SUBSTITUTION DE BASE LÉGALE - EXISTENCE - 2) POUVOIRS DU JUGE DE PLEIN CONTENTIEUX - DEMANDE DE DÉCHARGE D'UNE PARTICIPATION D'URBANISME - FACULTÉ DU JUGE DE PROCÉDER, LE CAS ÉCHÉANT D'OFFICE, À UNE SUBSTITUTION DE BASE LÉGALE - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-06 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. SUBSTITUTION DE MOTIFS. - DISTINCTION ENTRE SUBSTITUTION DE BASE LÉGALE ET SUBSTITUTION DE MOTIFS - 1) ILLÉGALITÉ N'ENTACHANT PAS LE FONDEMENT LÉGAL QUI A PERMIS À L'ADMINISTRATION D'AGIR, MAIS LES MOTIFS DE SA DÉCISION - FACULTÉ POUR L'ADMINISTRATION DE DEMANDER UNE SUBSTITUTION DE MOTIFS - EXISTENCE - 2) DÉCISION PRISE SUR UN FONDEMENT JURIDIQUE ERRONÉ - FACULTÉ POUR L'ADMINISTRATION DE DEMANDER UNE SUBSTITUTION DE BASE LÉGALE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - DEMANDE DE DÉCHARGE D'UNE PARTICIPATION D'URBANISME -  FACULTÉ DU JUGE DE PROCÉDER, LE CAS ÉCHÉANT D'OFFICE, À UNE SUBSTITUTION DE BASE LÉGALE - CONDITIONS [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - POUVOIRS DU JUGE DE PLEIN CONTENTIEUX - DEMANDE DE DÉCHARGE D'UNE PARTICIPATION D'URBANISME -  FACULTÉ DU JUGE DE PROCÉDER, LE CAS ÉCHÉANT D'OFFICE, À UNE SUBSTITUTION DE BASE LÉGALE - CONDITIONS [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-05 1)  a) Lorsqu'une illégalité n'entache pas le fondement légal qui a permis à l'administration d'agir, mais les motifs de sa décision, elle peut demander au juge de procéder à une substitution de motifs. b) Il est cependant possible à l'administration, lorsqu'elle a pris une décision sur un fondement juridique erroné, de demander une substitution de base légale.,,,2) Lorsque le juge de plein contentieux, saisi d'une demande tendant à la décharge d'une participation d'urbanisme, constate que la décision prévoyant le versement de cette participation aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui dont la méconnaissance est invoquée, il peut, le cas échéant d'office, substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la participation aurait dû lui être demandée.</ANA>
<ANA ID="9B"> 54-07-01-06 1) Lorsqu'une illégalité n'entache pas le fondement légal qui a permis à l'administration d'agir, mais les motifs de sa décision, elle peut demander au juge de procéder à une substitution de motifs. 2) Il est cependant possible à l'administration, lorsqu'elle a pris une décision sur un fondement juridique erroné, de demander une substitution de base légale.</ANA>
<ANA ID="9C"> 54-07-03 Lorsque le juge de plein contentieux, saisi d'une demande tendant à la décharge d'une participation d'urbanisme, constate que la décision prévoyant le versement de cette participation aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui dont la méconnaissance est invoquée, il peut, le cas échéant d'office, substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la participation aurait dû lui être demandée.</ANA>
<ANA ID="9D"> 68-06-04 Lorsque le juge de plein contentieux, saisi d'une demande tendant à la décharge d'une participation d'urbanisme, constate que la décision prévoyant le versement de cette participation aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui dont la méconnaissance est invoquée, il peut, le cas échéant d'office, substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la participation aurait dû lui être demandée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la possibilité pour le juge de l'excès de pouvoir de procéder à une substitution de base légale pour rejeter un recours contre la décision de mettre à la charge d'un constructeur une participation, CE, 7 juillet 2010, EARL des Noels, n° 311477, T. p. 1016-1024.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
