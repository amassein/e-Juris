<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900533</ID>
<ANCIEN_ID>JG_L_2017_06_000000399382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900533.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2017, 399382</TITRE>
<DATE_DEC>2017-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP LYON-CAEN, THIRIEZ ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399382.20170609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Entreprise Morillon Corvol Courbot (EMCC) a demandé, le 21 octobre 2010, au tribunal administratif de Nice d'annuler le marché n° 09-54 du 7 septembre 2009 conclu avec la commune de Cannes et portant sur la mise en place d'une digue sous-marine dans le cadre du programme de protection des plages de la Croisette ensemble l'avenant n° 1 à ce marché signé le 9 décembre 2009. Le 16 décembre 2010, la même société a demandé au tribunal administratif de Nice d'annuler la décision du 22 octobre 2010 de la commune de Cannes résiliant à ses torts exclusifs le marché n° 09-54. Le 4 février 2013, enfin, la société EMCC a demandé au tribunal administratif de Nice :<br/>
              - à titre principal, de condamner la commune de Cannes à lui verser la somme de 2 788 831,22 euros TTC ;<br/>
              - à titre subsidiaire, de condamner la commune de Cannes à lui verser la somme de 2 632 516,70 euros TTC au titre du solde du marché n° 09-54 et en réparation des préjudices résultant de la décision de résiliation du 22 octobre 2010 ; <br/>
              - de condamner la commune de Cannes aux dépens, en ce compris les frais de l'expertise ordonnée par ledit tribunal.<br/>
<br/>
              Par un jugement n°s 1004248, 1005134, 1300377 du 7 février 2014, le tribunal administratif de Nice a rejeté les demandes de la société EMCC, l'a condamnée à verser la somme de 1 922 413,66 euros à la commune de Cannes et a mis à sa charge les frais d'expertise d'un montant de 114 484,34 euros. <br/>
<br/>
              Par un arrêt n° 14MA01635 du 21 mars 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société EMCC contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 2 mai et 29 juin 2016 et le 1er mars 2017 au secrétariat du contentieux du Conseil d'Etat, la société EMCC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a statué sur ses conclusions indemnitaires et sur celles, incidentes, de la commune de Cannes ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Cannes la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le décret n° 76-87 du 21 janvier 1976 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Entreprise Morillon Corvol Courbot, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Cannes et à la SCP Baraduc, Duhamel, Rameix, avocat de la société Artelia Eau Environnement.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 mai 2017, présentée pour la société Entreprise Morillon Corbol Courbot.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la commune de Cannes a décidé de réaliser une digue sous-marine constituée de tubes en géotextiles pour assurer la protection des plages de la Croisette ; qu'un marché de travaux d'un montant de 1 485 088,39 euros TTC a été conclu à cette fin le 7 septembre 2009 avec la société Entreprise Morillon Corvol Courbot (EMCC) ; que, toutefois, une inspection du chantier en avril 2010 a révélé des malfaçons et des retards d'exécution ; qu'une tempête survenue le 4 mai 2010 a par ailleurs causé la destruction presque intégrale des éléments déjà posés ; qu'après avoir vainement cherché à obtenir de la société la dépose du tronçon de la digue détruit par la tempête et la reprise de l'exécution du marché, la commune de Cannes a prononcé, par décision du 22 octobre 2010, la résiliation du marché aux torts exclusifs de la société EMCC ; qu'elle a ensuite conclu avec la société Trasomar, d'une part, un marché de dépose du tronçon n° 1 de la digue, d'autre part, un marché de substitution ; que, le 27 août 2012, la commune de Cannes a notifié à la société EMCC le décompte général du marché, qui comprenait un solde de 1 922 413,66 euros TTC à son crédit ; que, par un jugement du 7 février 2014, le tribunal administratif de Nice, d'une part, a rejeté les demandes de la société EMCC tendant à l'annulation du marché conclu le 7 septembre 2009, à l'annulation de la décision de résiliation du même marché du 22 octobre 2010 et à la condamnation de la commune de Cannes à lui verser une somme de 2 788 831,22 euros et, d'autre part, a fait droit aux conclusions incidentes de la commune de Cannes en condamnant la société EMCC à lui verser la somme de 1 922 413,66 euros ; que la cour administrative d'appel de Marseille a rejeté l'appel formé par la société EMCC contre ce jugement ; que la société EMCC se pourvoit contre cet arrêt en tant qu'il a statué sur les demandes indemnitaires ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en relevant, pour juger que la commune de Cannes était fondée à prononcer la résiliation du marché aux torts de la société EMCC, que les tempêtes survenues en février et en mai 2010 ne présentaient pas un caractère exceptionnel, la cour, qui n'était pas tenue de répondre à tous les arguments de la société requérante, n'a pas entaché son arrêt d'insuffisance de motivation ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il résulte des termes mêmes de l'arrêt attaqué que le motif par lequel la cour a estimé que les dommages subis par les géotubes étaient antérieurs aux tempêtes survenues en février et en mai 2010 présente un caractère surabondant ; que, dès lors, le moyen du pourvoi dirigé contre ce motif est inopérant ;<br/>
<br/>
              4. Considérant, en troisième lieu, que la société EMCC reproche à la cour d'avoir confirmé sa condamnation à payer à la commune de Cannes la somme de 1 922 413,66 euros au titre du décompte général du marché sans répondre aux moyens qu'elle avait soulevés pour contester le montant de ce décompte, tirés de ce que la commune avait méconnu, d'une part, les stipulations de l'article 49.5 du cahier des clauses administratives générales (CCAG) Travaux, en l'empêchant de suivre l'exécution des marchés de dépose et de substitution passés avec la société Trasomar, et, d'autre part, les stipulations de l'article 10 du cahier des clauses administratives particulières (CCAP) définissant les modalités d'indemnisation du maître d'ouvrage en cas d'incapacité de l'entreprise à remplir ses obligations contractuelles ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article 46 du cahier des clauses administratives générales (CCAG) Travaux approuvé par le décret du 21 janvier 1976, alors en vigueur et applicable au marché litigieux : " (...) - 2. En cas de résiliation, il est procédé, l'entrepreneur ou ses ayants droit, tuteur, curateur ou syndic, dûment convoqués, aux constatations relatives aux ouvrages et parties d'ouvrages exécutés, à l'inventaire des matériaux approvisionnés, ainsi qu'à l'inventaire descriptif du matériel et des installations de chantier. Il est dressé procès-verbal de ces opérations ; (...) - 3. Dans les dix jours suivant la date de ce procès-verbal, la personne responsable du marché fixe les mesures qui doivent être prises avant la fermeture du chantier pour assurer la conservation et la sécurité des ouvrages ou parties d'ouvrages exécutés. Ces mesures peuvent comporter la démolition de certaines parties d'ouvrages./ A défaut d'exécution de ces mesures par l'entrepreneur dans le délai imparti par la personne responsable du marché, le maître d'oeuvre les faits exécuter d'office./Sauf dans les cas de résiliation prévus aux articles 47 et 49, ces mesures ne sont pas à la charge de l'entrepreneur " ; qu'aux termes par ailleurs de l'article 49 du même CCAG : " - 1 (...) lorsque l'entrepreneur ne se conforme pas aux dispositions du marché ou aux ordres de service, la personne responsable du marché le met en demeure d'y satisfaire, dans un délai déterminé, par une décision qui lui est notifiée par écrit (...) ; - 2. Si l'entrepreneur n'a pas déféré à la mise en demeure, une mise en régie à ses frais et risques peut être ordonnée ou la résiliation du marché peut être décidée ; (...) - 4. La résiliation du marché décidée en application du 2 ou du 3 du présent article peut être, soit simple, soit aux frais et risques de l'entrepreneur./ Dans les deux cas, les mesures prises en application du 3 de l'article 46 sont à sa charge. / En cas de résiliation aux frais et risques de l'entrepreneur, il est passé un marché avec un autre entrepreneur pour l'achèvement des travaux. Ce marché est conclu après appel d'offres avec publicité préalable ; toutefois, pour les marchés intéressant la défense ou en cas d'urgence, il peut être passé un marché négocié. - 5. L'entrepreneur dont les travaux sont mis en régie est autorisé à en suivre l'exécution sans pouvoir entraver les ordres du maître d'oeuvre et de ses représentants./ Il en est de même en cas de nouveau marché passé à ses frais et risques " ; <br/>
<br/>
              6. Considérant qu'il résulte des stipulations de l'article 49.5 du CCAG Travaux citées ci-dessus que l'entrepreneur dont le marché est résilié à ses frais et risques doit être mis à même d'user du droit de suivre les opérations exécutées par un nouvel entrepreneur dans le cadre d'un marché de substitution ; que ce droit de suivi est destiné à lui permettre de veiller à la sauvegarde de ses intérêts, les montants découlant des surcoûts supportés par le maître d'ouvrage en raison de l'achèvement des travaux par un nouvel entrepreneur étant à sa charge ; qu'en revanche il ne résulte d'aucune stipulation du CCAG Travaux que, lorsque l'entrepreneur dont le marché est résilié n'a pas exécuté les mesures de conservation et de sécurité prescrites par le pouvoir adjudicateur dans les conditions fixées par les stipulations de l'article 46 du CCAG Travaux, mesures qui peuvent comprendre la démolition des ouvrages réalisés et qui sont elles aussi à la charge de l'entrepreneur, ce dernier disposerait du droit de suivre l'exécution d'office de ces mesures ; qu'il ressort par ailleurs des pièces du dossier soumis aux juges du fond que le décompte général n'incluait aucune somme correspondant au marché de substitution conclu avec la société Trasomar pour réaliser la mise en place de la digue sous-marine ; que, dans ces conditions, le moyen soulevé par la société EMCC devant la cour administrative d'appel de Marseille, tiré de ce que la commune de Cannes aurait méconnu son droit de suivre l'exécution du marché de substitution et du marché de dépose du tronçon n° 1 de la digue conclus avec la société Trasomar, était inopérant ; que la cour n'était donc pas tenue d'y répondre ;<br/>
<br/>
              7. Considérant que le moyen tiré de la méconnaissance des stipulations de l'article 10 du CCAP était également inopérant dès lors que la décision de résiliation du marché aux torts et risques de l'entreprise a été prononcée non en raison de l'incapacité de l'entreprise à exécuter ses obligations contractuelles, mais à la suite de son refus d'exécuter un ordre de service puis une mise en demeure visant à la reprise des travaux ; qu'ainsi, la cour n'était pas non plus tenue d'y répondre ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la société EMCC n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société EMCC la somme de 3 000 euros à verser à la commune de Cannes et la même somme à verser à la société Artelia Eau et Environnement, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Entreprise Morillon Corvol Courbot est rejeté.<br/>
Article 2 : La société Entreprise Morillon Corvol Courbot versera la somme de 3 000 euros à la commune de Cannes et la même somme à la société Artelia Eau et Environnement au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Entreprise Morillon Corvol Courbot, à la commune de Cannes et à la société Artelia Eau et Environnement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. EFFETS. - MARCHÉ DE TRAVAUX - 1) DROIT DU TITULAIRE DU MARCHÉ RÉSILIÉ DE SUIVRE LES OPÉRATIONS EXÉCUTÉES PAR UN NOUVEL ENTREPRENEUR DANS LE CADRE D'UN MARCHÉ DE SUBSTITUTION - EXISTENCE - PORTÉE - 2) DROIT DU TITULAIRE DU MARCHÉ RÉSILIÉ DE SUIVRE L'EXÉCUTION D'OFFICE DES MESURES DE SAUVEGARDE PRESCRITES PAR LE POUVOIR ADJUDICATEUR - ABSENCE.
</SCT>
<ANA ID="9A"> 39-04-02-02 1) Il résulte des stipulations de l'article 49.5 du cahier des clauses administratives générales applicables aux marchés publics de travaux (CCAG travaux) que l'entrepreneur dont le marché est résilié à ses frais et risques doit être mis à même d'user du droit de suivre les opérations exécutées par un nouvel entrepreneur dans le cadre d'un marché de substitution. Ce droit de suivi est destiné à lui permettre de veiller à la sauvegarde de ses intérêts, les montants découlant des surcoûts supportés par le maître d'ouvrage en raison de l'achèvement des travaux par un nouvel entrepreneur étant à sa charge.... ,,2) En revanche, il ne résulte d'aucune stipulation du CCAG travaux que lorsque l'entrepreneur dont le marché est résilié n'a pas exécuté les mesures de conservation et de sécurité prescrites par le pouvoir adjudicateur dans les conditions fixées par les stipulations de l'article 46 du CCAG travaux, mesures qui peuvent comprendre la démolition des ouvrages réalisés et qui sont elles aussi à la charge de l'entrepreneur, ce dernier disposerait du droit de suivre l'exécution d'office de ces mesures.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
