<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028681381</ID>
<ANCIEN_ID>JG_L_2014_03_000000363971</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/13/CETATEXT000028681381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 03/03/2014, 363971, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363971</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:363971.20140303</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 novembre 2012 et 20 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA03150 du 8 juin 2012 par lequel la cour administrative d'appel de Paris a, sur la requête du préfet de police, d'une part, annulé le jugement n° 1021119 du 9 juin 2011 par lequel le tribunal administratif de Paris a annulé les décisions du préfet de police du 26 mai 2010 lui retirant son titre de séjour et lui faisant obligation de quitter le territoire français et, d'autre part, rejeté sa demande présentée devant le tribunal administratif de Paris ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article. 761-1 du code de justice administrative et de l'article 37 de la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'accord franco-algérien du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour en France des ressortissants algériens et de leurs familles ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 19 août 2008, le préfet de police a rejeté la demande de titre de séjour de M. B..., lui a fait obligation de quitter le territoire français et a fixé le pays de destination ; que, par un jugement du 31 décembre 2008, le tribunal administratif de Paris a annulé cet arrêté et a enjoint au préfet de police de délivrer à M. B...un certificat de résidence ; que, par un arrêt du 11 mars 2010, la cour administrative d'appel de Paris a annulé ce jugement et rejeté la demande de l'intéressé ; que, par un arrêté du 26 mai 2010, le préfet de police a retiré le certificat de résidence qui avait été délivré à l'intéressé pour l'exécution du jugement précité et lui a fait obligation de quitter le territoire français ; que, par un jugement du 9 juin 2011, le tribunal administratif de Paris a annulé cet arrêté ; que M. B...se pourvoit en cassation contre l'arrêt du 8 juin 2012 par lequel la cour administrative de Paris, sur la requête du préfet de police, a annulé ce jugement et rejeté la demande de l'intéressé ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale (...). / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. " ; qu'aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 : " (...) Le certificat de résidence d'un an portant la mention " vie privée et familiale " est délivré de plein droit : (...) 5) au ressortissant algérien, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la date d'intervention de l'arrêté attaqué M. B...résidait depuis huit ans sur le territoire français, et y a séjourné entre 2004 et 2007 en situation régulière  avec son épouse et leur fille, née en France en 2005 et qui y est scolarisée ; que son épouse a bénéficié d'un titre de séjour en qualité d'agent consulaire jusqu'au 31 juillet 2007, puis a été employée en qualité d'agent administratif au lycée Paul Eluard à Saint-Denis ; qu'à la date de l'arrêté litigieux le requérant bénéficiait d'un emploi stable ; qu'il n'est pas contesté que le couple est propriétaire d'un appartement et maîtrise la langue française, seule parlée par leur fille ; qu'une grande partie de la famille de M. B...est installée en France, de même qu'un frère et une soeur de son épouse, qui y séjournent régulièrement ; que la fille du requérant, qui souffre de troubles de l'audition consécutifs à un traumatisme crânien survenu en 2007 est suivie médicalement en France ; que, compte tenu de ces éléments, la cour a inexactement qualifié les faits de l'espèce en jugeant que le retrait du titre de séjour de M. B...ne portait pas une atteinte disproportionnée au droit au respect de sa vie privée et familiale ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              4. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP David Gaschignard, avocat de M. B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette SCP de la somme de 1 500 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : L'arrêt de la cour administrative d'appel de Paris du 8 juin 2012 est annulé. <br/>
 Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
 Article 3 : L'Etat versera à la SCP Gaschignard, avocat de M.B..., la somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
 Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
