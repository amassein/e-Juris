<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882247</ID>
<ANCIEN_ID>JG_L_2018_12_000000405721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/22/CETATEXT000037882247.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/12/2018, 405721, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:405721.20181226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nancy d'annuler la décision du 17 novembre 2014 par laquelle le président du conseil général de Meurthe-et-Moselle a rejeté sa demande d'aide financière présentée, pour son quatrième enfant, sur le fondement de l'article L. 222-2 du code de l'action sociale et des familles. Par un jugement n° 1500117 du 17 décembre 2015, le tribunal administratif de Nancy a rejeté sa demande.  <br/>
<br/>
              Par un arrêt n° 16NC01561 du 6 décembre 2016, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 21 juillet 2016 au greffe de cette cour, présenté par M.B.... <br/>
<br/>
              Par ce pourvoi et par un mémoire, enregistré le 13 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Nancy du 17 décembre 2015 ; <br/>
<br/>
              2°) de mettre à la charge du département de Meurthe-et-Moselle la somme de 2 500 euros, à verser à la SCP Didier, Pinet, son avocat, sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M.B..., et à la SCP Marlange, de la Burgade, avocat du département de Meurthe-et-Moselle.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision du 17 novembre 2014, le président du conseil général de Meurthe-et-Moselle a refusé de faire droit à la demande d'aide financière présentée par M.B..., pour son quatrième enfant, sur le fondement de l'article L. 222-2 du code de l'action sociale et des familles. M. B...doit être regardé comme demandant l'annulation de l'article 2 du jugement du 17 décembre 2015 par lequel le tribunal administratif de Nancy, après l'avoir admis au bénéfice de l'aide juridictionnelle provisoire, a rejeté ses conclusions à fin d'annulation de cette décision ainsi que ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
              2. Il ressort des pièces de la procédure devant le tribunal administratif que M. B... soulevait, à l'appui de ses conclusions d'annulation, un moyen tiré de l'incompétence du signataire de la décision attaquée. Le tribunal administratif a rejeté la requête de M. B...sans répondre à ce moyen. Dès lors, le tribunal a insuffisamment motivé son jugement et M. B...est fondé à demander l'annulation pour ce motif de son article 2, sans qu'il soit besoin d'examiner les autres moyens du pourvoi. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Meurthe-et-Moselle une somme de 2 000 euros à verser à la SCP Didier, Pinet au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. Les dispositions de l'article L. 761-1 du code de justice administrative font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Nancy du 17 décembre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nancy.<br/>
Article 3 : Le département de Meurthe-et-Moselle versera à la SCP Didier, Pinet une somme de 2 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : Les conclusions du département de Meurthe-et-Moselle présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au département de Meurthe-et-Moselle. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
