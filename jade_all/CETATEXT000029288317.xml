<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288317</ID>
<ANCIEN_ID>JG_L_2014_07_000000376461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 23/07/2014, 376461, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:376461.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 18 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-233 du 24 février 2014 portant délimitation des cantons dans le département du Pas-de-Calais.<br/>
<br/>
              Il soutient que le décret attaqué :<br/>
<br/>
              - a été édicté au terme d'une procédure irrégulière aux motifs qu'il n'est pas conforme au projet transmis au conseil départemental et que la " consultation s'est faite dans des conditions irrégulières " ;<br/>
<br/>
              - est insuffisamment motivé ; <br/>
<br/>
              - a été pris en retenant un critère démographique qui " n'a pas pu être appliqué avec certitude quant aux chiffres et n'a pas été respecté de façon homogène " ;<br/>
<br/>
              - est fondé sur des données démographiques erronées.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 juin 2014, le ministre de l'intérieur conclut au rejet de la requête.<br/>
<br/>
              La requête a été communiquée au Premier ministre, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 2012-1479 du 27 décembre 2012 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général. ".<br/>
<br/>
              2. Le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département du Pas-de-Calais, compte tenu de l'exigence de réduction du nombre des cantons de ce département résultant de l'article L. 191-1 du code électoral.<br/>
<br/>
              3. En premier lieu, l'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'une décision doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par cette décision. En l'espèce, il ne saurait sérieusement être soutenu que le conseil général n'a pas été saisi de l'ensemble des questions posées par le projet de modification des cantons du département, dès lors que le projet de décret qui a été soumis à sa consultation tendait au redécoupage de tous les cantons du département. Par suite, le moyen tiré de ce qu'eu égard aux modifications, à les supposer établies, qui ont été apportées au projet de délimitation des cantons à la suite de l'avis du Conseil d'Etat, le conseil général aurait dû être consulté une nouvelle fois sur le projet de décret ne peut qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, le décret, qui a le caractère d'un acte règlementaire, n'est pas au nombre des décisions qui doivent être motivées en vertu des dispositions de l'article 1er de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public. <br/>
<br/>
              5. En troisième lieu, si le requérant soutient, d'une part, que " la consultation s'est faite dans des conditions irrégulières " et, d'autre part, que le critère démographique " n'a pas pu être appliqué avec certitude quant aux chiffres et n'a pas été respecté de façon homogène ", il n'assortit pas ces moyens de précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              6. En quatrième lieu, aux termes de l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué et dont la légalité n'est pas contestée : "  (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Or, il est constant que les nouveaux cantons du département du Pas-de-Calais ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012. Par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas à la réalité démographique ne peut qu'être écarté.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête de M. B... doit être rejetée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
