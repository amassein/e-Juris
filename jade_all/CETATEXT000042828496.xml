<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828496</ID>
<ANCIEN_ID>JG_L_2020_12_000000438748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828496.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/12/2020, 438748, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438748.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :	<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 17 février et 15 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat Union nationale des syndicats autonomes des agents de la direction générale de la concurrence, de la consommation et de la répression des fraudes (UNSA-CCRF) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande du 18 octobre 2019 tendant, à titre principal, d'une part, à l'application pleine et entière du titre III du décret n° 2010-687 du 24 juin 2010 relatif à l'organisation et aux missions des services de l'Etat dans la région et les départements d'Ile-de-France et à la reconnaissance officielle du caractère interministériel de la direction départementale de la protection des population (DDPP) de Paris et d'autre part, à l'abrogation en tant qu'ils contredisent ce caractère interministériel, des arrêtés du préfet de police de Paris n° 2019-00197 du 1er mars 2019 relatif aux missions et à l'organisation de la direction des transports et de la protection du public, n° 2010-00458 du 5 juillet 2010 relatif aux missions et à l'organisation de la DDPP et n° 2010-00456 modifiant l'arrêté n° 2009-00641 du 7 août 2009 relatif aux missions et à l'organisation de la préfecture de police et, à titre subsidiaire, de constater l'abrogation implicite, au moins partielle, de l'arrêté du 6 mai 2011 du ministre de l'économie, des finances et de l'industrie relatif à l'habilitation d'agents de la DDPP de Paris en application de l'article L. 450-1 du code de commerce ;<br/>
<br/>
              2°) de faire droit à sa demande du 18 octobre 2019 ;<br/>
<br/>
              3°) d'annuler les arrêtés n° 2010-00456, n° 2010-00458 et n° 2019-00197 ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2009-1484 du 3 décembre 2009 ;<br/>
              - le décret n° 2010-687 du 24 juin 2010 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur les conclusions à fin d'annulation de la décision implicite de rejet du Premier ministre :<br/>
<br/>
              1. Le Premier ministre n'étant pas compétent pour annuler des arrêtés du préfet de police et pour constater l'abrogation d'un arrêté ministériel, il ne pouvait, en tout état de cause, faire droit à la demande du syndicat requérant. Ses conclusions tendant à l'annulation de la décision implicite de rejet née du silence gardé par le Premier ministre à la suite de son courrier du 18 octobre 2019 ne peuvent par suite qu'être rejetées.<br/>
<br/>
              Sur les conclusions à fin d'annulation des arrêtés du préfet de police des 5 juillet 2010 et 1er mars 2019 :<br/>
<br/>
              2. Le syndicat doit être regardé comme demandant au Conseil d'Etat d'annuler les arrêtés du préfet de police n° 2019-00197 du 1er mars 2019 relatif aux missions et à l'organisation de la direction des transports et de la protection du public, n° 2010-00458 du 5 juillet 2010 relatif aux missions et à l'organisation de la direction départementale interministérielle de la protection des populations de Paris (DDPP) et n° 2010-00456 modifiant l'arrêté n° 2009-00641 du 7 août 2009 relatif à l'organisation de la préfecture de police.<br/>
<br/>
              3. Si le Conseil d'Etat n'est compétent pour connaître en premier et dernier ressort des conclusions à fin d'annulation de ces arrêtés en application ni des dispositions du 2° de l'article R. 311-1 du code de justice administrative, ni d'aucune autre disposition du code de justice administrative, il est compétent, en vertu des dispositions de l'article R. 351-5 du même code, pour les rejeter si elles sont manifestement irrecevables. Tel est le cas des conclusions en annulation des arrêtés du préfet de police, enregistrées plus de deux mois après leur publication aux bulletins officiels de la ville de Paris des 13 juillet 2010 et 12 mars 2019, qui ne peuvent par suite qu'être rejetées. <br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              4. En dehors des cas prévus par les dispositions des articles L. 911-1 et suivants du code de justice administrative, il n'appartient pas au juge administratif d'adresser des injonctions à l'administration. Il en résulte que les conclusions du syndicat UNSA-CCRF tendant à ce que le Conseil d'Etat enjoigne " l'application pleine et entière du titre III du décret n° 2010-687 du 24 juin 2010 relatif à l'organisation et aux missions des services de l'Etat dans la région et les départements d'Ile-de-France et la reconnaissance officielle du caractère interministériel de la direction départementale de la protection des population (DDPP) de Paris " sont irrecevables et doivent, par suite, être rejetées.  <br/>
<br/>
              Sur les conclusions à fin de constatation de l'abrogation partielle de l'arrêté du 6 mai 2011 :<br/>
<br/>
              5. Il n'appartient pas au juge administratif de constater l'abrogation d'un acte administratif. Les conclusions présentées à cette fin par le syndicat requérant sont, par suite, irrecevables et ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat UNSA-CCRF est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat Union nationale des syndicats autonomes des agents de la direction générale de la concurrence, de la consommation et de la répression des fraudes et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
