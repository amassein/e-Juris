<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044293056</ID>
<ANCIEN_ID>JG_L_2021_11_000000449927</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/29/30/CETATEXT000044293056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 04/11/2021, 449927, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449927</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449927.20211104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 19 février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme F... A... B... demande au Conseil d'Etat d'annuler le décret du 23 décembre 2020 rapportant le décret du 21 décembre 2018 lui accordant la nationalité française.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales (...) ". Selon les dispositions de l'article 21-27 du même code, nul ne peut acquérir la nationalité française si son séjour en France est irrégulier au regard des lois et conventions relatives au séjour des étrangers en France. <br/>
<br/>
              2.	Il ressort des pièces du dossier que Mme B..., ressortissante camerounaise, qui avait déposé une demande d'acquisition de la nationalité française le 18 août 2016, a été naturalisée par décret du 21 décembre 2018, publié au Journal officiel de la République française le 23 décembre 2018. Mme B... demande l'annulation pour excès de pouvoir du décret du 23 décembre 2020 rapportant ce décret. <br/>
<br/>
              3.	En premier lieu, le moyen tiré de ce que le décret attaqué ne mentionne pas la date à laquelle il a été pris manque en fait.  <br/>
<br/>
              4.	En deuxième lieu, il ressort des pièces du dossier que le décret a été signé par le Premier ministre et contresigné par le ministre de l'intérieur. Par suite, le moyen tiré de ce que le décret attaqué aurait été signé par un auteur non identifié ou identifiable doit être écarté. <br/>
<br/>
              5.	En troisième lieu, en vertu des dispositions de l'article 63 du décret n° 93-1362 du 30 décembre 1993, les décrets rapportant un décret de naturalisation prennent effet à la date de leur signature. Le décret du 21 décembre 2018 accordant la nationalité française à Mme B... a été publié au Journal officiel de la République française le 23 décembre 2018 et le décret retirant ce décret a été pris le 23 décembre 2020. Par suite, le moyen tiré de ce que le décret attaqué serait intervenu après l'expiration du délai légal de deux ans doit être écarté. <br/>
<br/>
              6.	En quatrième lieu, contrairement à ce qui est soutenu, il résulte des termes du décret que le Premier ministre a considéré que Mme B... avait été naturalisée dès lors qu'elle remplissait les conditions requises, notamment la condition de séjour régulier en France, en vertu de titres de séjour délivrés en sa qualité de mère d'un enfant français, et non pas naturalisé " en sa qualité de parent d'enfant français " comme le mentionne le décret litigieux par une erreur de plume sans incidence sur sa légalité. <br/>
<br/>
              7.	En dernier lieu, Mme B... soutient que la situation de son enfant n'a à aucun moment influé ni sur l'examen de sa demande de naturalisation ni sur l'issue positive qui lui a été réservée. Toutefois, Mme B... ayant bénéficié de titres de séjour en raison de sa qualité de parent d'enfant français et ces titres ayant été retirés à la suite de l'annulation de la reconnaissance de paternité frauduleuse par le juge judiciaire, Mme B... ne satisfaisait pas à la date de signature du décret de naturalisation à la condition légale d'un séjour régulier en France posée par l'article 21-27 du code civil. Par suite, le moyen tiré d'une inexacte application de l'article 27-2 du code civil doit être écarté.<br/>
<br/>
              8.	Il résulte de ce qui précède que Mme B... n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 23 décembre 2020 par lequel le Premier ministre a rapporté le décret du 21 décembre 2018. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme F... A... B... et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 14 octobre 2021 où siégeaient : Mme A... E..., assesseure, présidant ; M. Jean-Yves Ollier, conseiller d'Etat et M. Clément Tonon, auditeur-rapporteur. <br/>
<br/>
              Rendu le 4 novembre 2021.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme A... E...<br/>
 		Le rapporteur : <br/>
      Signé : M. Clément Tonon<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
