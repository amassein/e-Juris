<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285469</ID>
<ANCIEN_ID>JG_L_2016_10_000000395931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285469.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème chambres réunies, 20/10/2016, 395931, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395931.20161020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Marseille de condamner la communauté urbaine Marseille Provence Métropole à lui verser la somme de 90 594,74 euros en réparation du préjudice résultant de la chute dont elle a été victime le 26 octobre 2010 dans une rue de Marseille. Le département des Bouches-du-Rhône, employeur de MmeA..., a demandé au tribunal de condamner la communauté urbaine à lui verser la somme de 103 774,67 euros assortie des intérêts.<br/>
<br/>
              Par un jugement n° 1205293 du 18 avril 2014, le tribunal administratif de Marseille a limité à 30 % la part de responsabilité incombant à la communauté urbaine et condamné celle-ci à verser d'une part à Mme A...la somme de 5 070 euros et d'autre part au département des Bouches-du-Rhône la somme de 31 113,20 euros portant intérêts. <br/>
<br/>
              Saisie d'un appel de Mme A...et d'un appel incident de la communauté urbaine, la cour administrative d'appel de Marseille a, par un arrêt n° 14MA02676 du 5 novembre 2015, annulé ce jugement et rejeté la demande de Mme A...et le surplus des conclusions des parties. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 janvier et 5 avril 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la communauté urbaine Marseille Provence Métropole la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la route ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de MmeA..., à la SCP Baraduc, Duhamel, Rameix, avocat de la métropole d'Aix Marseille Provence, et à la SCP Lyon-Caen, Thiriez, avocat du département des Bouches-du-Rhône. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a écarté toute responsabilité de la communauté urbaine Marseille Provence Métropole dans l'accident dont Mme A...a été victime le 26 octobre 2010 alors que, marchant sur un trottoir encombré d'un volume important d'immondices, à l'angle de deux rues de Marseille, elle a fait une chute sur le genou et s'est fracturée la rotule gauche ; qu'après avoir relevé que le très important tas d'immondices se trouvant à cet endroit, en raison d'une grève des éboueurs de deux semaines, était parfaitement visible et que Mme A...connaissait les lieux pour habiter à proximité, la cour a jugé que la chute de la requérante résultait exclusivement de son imprudence dès lors qu'elle aurait pu facilement contourner ce tas d'immondices en marchant sur la chaussée réservée aux automobilistes ; <br/>
<br/>
              2.	Considérant, toutefois, qu'il résulte des pièces du dossier soumis aux juges du fond que MmeA..., alors âgée de 63 ans, se bornait à marcher sur le trottoir en tentant d'éviter les détritus ; que, par ailleurs, alors que le volume des immondices répandus sur le trottoir avait atteint des proportions considérables et débordait assez largement sur la chaussée, aucune mesure de protection n'avait été mise en place à l'attention des piétons ; que, dans ces conditions, en jugeant que la chute de la requérante était uniquement imputable à son imprudence, écartant ainsi tout lien direct de causalité avec l'état de l'ouvrage public, la cour a inexactement qualifié les faits de l'espèce ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              3.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la métropole d'Aix Marseille Provence une somme de 3 000 euros à verser à Mme A..., au titre de ces dispositions ; que celles-ci font obstacle à ce qu'une somme soit mise à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 5 novembre 2015 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La métropole d'Aix Marseille Provence versera à Mme A...une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la métropole d'Aix Marseille Provence. Copie en sera adressée au département des Bouches-du-Rhône et à la caisse primaire d'assurance maladie des Bouches-du-Rhône. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
