<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025886218</ID>
<ANCIEN_ID>JG_L_2012_04_000000358099</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/88/62/CETATEXT000025886218.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/04/2012, 358099, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358099</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Arrighi de Casanova</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ (FEBEA), dont le siège est situé 137 rue de l'Université à Paris (75007), représentée par son représentant légal ; la fédération requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du directeur de l'Agence française de sécurité sanitaire des produits de santé (AFSSAPS) du 9 février 2012 prise en application de la loi n° 2011-1906 du 21 décembre 2011 de financement de la sécurité sociale pour 2012 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que la condition d'urgence est remplie ; que le laps de temps entre la publication de la décision litigieuse et son application est très court ; que le caractère tardif de la décision contestée ne laisse pas le temps aux entreprises concernées de satisfaire à ses exigences ; qu'en l'absence de déclaration ou en cas de déclaration erronée, les entreprises subiront un préjudice financier important en raison des sanctions encourues ; que l'AFSSAPS entend obtenir des informations confidentielles qui ne sont nécessaires ni à la perception de la taxe ni à la bonne exécution de ses missions ; qu'il existe un doute sérieux quant à la légalité de la décision contestée ; qu'il n'est pas justifié que le signataire de la décision attaquée bénéficiait d'une délégation régulière ; que l'agence a méconnu les usages en ne procédant pas à la consultation du syndicat représentatif de la profession ; que la décision méconnaît les prescriptions prévues par la loi du 21 décembre 2011 et le règlement CE n°1223/2009 du Parlement européen et du Conseil du 30 novembre 2009 relatif aux produits cosmétiques ; qu'en soumettant les produits cosmétiques à des dispositions plus contraignantes que les médicaments non remboursables, elle crée une rupture d'égalité de traitement entre ces deux secteurs ; qu'en permettant à l'agence de collecter des données commerciales confidentielles, cette décision porte atteinte au secret des affaires ; <br/>
<br/>
<br/>
              Vu la décision dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de la décision contestée ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le jeudi 19 avril 2012, présenté par l'AFSSAPS, qui conclut au rejet de la requête ; elle soutient que la fédération requérante n'a intérêt à agir contre la décision litigieuse qu'en tant qu'elle fixe le modèle de la déclaration relative aux produits cosmétiques ; que la condition d'urgence n'est pas remplie ; que, si la date de publication est proche de celle de transmission des déclarations, des moyens d'information et d'aide à l'établissement de ces déclarations ont été mis en place dès le 23 février 2012 ; que les amendes prévues en l'absence de déclaration ou de déclaration erronée sont des sanctions pénales qui n'ont pas de caractère automatique ; que les informations demandées ne présentent pas un caractère exorbitant au regard de ses missions ; que la balance des intérêts en cause ne sauraient être regardée comme penchant en faveur de l'urgence invoquée par la requérante ; qu'il n'existe aucun doute sérieux quant à la légalité de la décision contestée ; que son directeur a compétence pour signer la décision fixant le modèle de déclaration ; que la possession par l'agence de données précises sur le marché des produits cosmétiques est nécessaire ; que le législateur a voulu que la déclaration comporte un degré approfondi de précision par produit cosmétique ; que le modèle de déclaration a été établi selon des catégories définies à l'article 13 du règlement du 30 novembre 2009 ; qu'il n'y a pas de rupture d'égalité de traitement dès lors que les produits cosmétiques et les dispositifs médicaux non remboursables sont soumis à des règles juridiques différentes et que les informations demandées pour le secteur des produits cosmétiques sont très en retrait par rapport à celles demandées pour ces autres produits de santé ; que les données demandées sont des données agréées qui ne peuvent permettre d'identifier des secrets d'affaires ; qu'elle a le pouvoir d'accéder aux informations nécessaires à l'exercice de ses missions dans les conditions prévues par le 7° de l'article L. 5311-2 du code de la santé publique, sans que puisse lui être opposé le secret en matière industrielle et commerciale ;<br/>
<br/>
              Vu les observations, enregistrées le 19 avril 2012, présentées par le ministre du travail, de l'emploi et de la santé ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 23 avril 2012, présenté par la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que l'AFSSAPS n'ayant pas été désignée par le législateur français comme l'autorité compétente mentionnée aux articles 23 et 30 du règlement européen 1223/2009, celle-ci ne peut pas se prévaloir de ses missions ni en tirer une compétence ; que rien ne garantit qu'aucune fuite ou aucun piratage ne viendra rendre publiques des informations qui auraient dû demeurer confidentielles ; que les entreprises n'ont pas eu connaissance des obligations leur incombant avant la publication de la décision contestée ; <br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de la santé publique, modifié notamment par la loi n° 2011-1906 du 21 décembre 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ, d'autre part l'Agence française de sécurité sanitaire des produits de santé  et le ministre du travail, de l'emploi et de la santé ;  <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 24 avril 2012 à 10 h, au cours de laquelle ont été entendus :<br/>
<br/>
              - les représentants de la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ ;<br/>
<br/>
              - les représentants de l'Agence française de sécurité sanitaire des produits de santé ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au vendredi 27 avril 2012 à 12 heures 30 ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 26 avril 2012, présenté par l'AFSSAPS ; elle soutient qu'elle a reçu 281 déclarations au 31 mars 2012 ; que les dispositions de l'article L. 5121-18 du code de la santé publique ne prévoient pas que la décision contestée doit être publiée au Journal Officiel de la République ; qu'un délai raisonnable de six mois, durant lequel aucune poursuite pénale ne sera engagée, est accordé aux entreprises pour se conformer à l'obligation de déclaration ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 27 avril 2012, présenté par la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ, qui soutient que l'AFSSAPS aurait dû privilégier une publication de la décision litigieuse au Journal Officiel et que la tardiveté de la publication de cette décision est une violation caractérisée du principe de sécurité juridique ; <br/>
<br/>
<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 27 avril 2012, présenté par l'AFSSAPS  qui fait valoir que la décision litigieuse ne peut être regardée comme ayant été édictée en méconnaissance du principe de sécurité juridique ;  <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 27 avril 2012, présenté par la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ ; <br/>
<br/>
              Considérant qu'en vertu du premier alinéa de l'article L. 521-1 du code de justice administrative, le juge des référés peut ordonner la suspension de l'exécution d'un acte administratif à la condition, notamment, que l'urgence le justifie ; que tel est le cas lorsque l'exécution d'un acte porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue;<br/>
<br/>
              Considérant qu'en vertu de l'article L. 5121-18 du code de la santé publique, dans sa rédaction issue de la loi du 21 décembre 2011, les redevables de la taxe prévue à l'article 1600-0 P du code général des impôts - c'est-à-dire les personnes assujetties à la taxe sur la valeur ajoutée qui effectuent la première vente en France des produits cosmétiques définis à l'article L. 5131-1 du code de la santé publique - sont tenus d'adresser à l'Agence française de sécurité sanitaire des produits de santé (AFSSAPS), au plus tard le 31 mars de chaque année, une déclaration établie conformément au modèle fixé par décision du directeur de cette agence, fournissant des informations relatives aux ventes réalisées au cours de l'année civile précédente pour les produits cosmétiques donnant lieu au paiement de la taxe ; que la même loi a inséré dans ce code un article l. 5421-6-3 aux termes duquel : " Le fait de ne pas adresser à l'agence mentionnée à l'article L. 5311-1, au plus tard le 31 mars de l'année en cours, la déclaration mentionnée à l'article L. 5121-18 est puni de 45 000 euros d'amende " ; <br/>
<br/>
              Considérant que la décision litigieuse du 9 février 2012 qui a fixé le modèle de déclaration prévu à l'article L. 5121-18 du code de la santé publique a été publiée au bulletin officiel du ministère chargé de la santé le 15 mars suivant et mise en ligne le 22 sur le site internet du ministère alors que, ainsi qu'il a été dit, les déclarations devaient être adressées à l'agence au plus tard le 31 mars ; que, pour justifier l'urgence à suspendre l'exécution de cette décision, en tant qu'elle fixe le modèle en cause, la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ fait valoir que les entreprises redevables qui ne fourniraient pas de déclaration ou une déclaration erronée subiraient un préjudice financier important en raison de la gravité des sanctions encourues et que la mise en oeuvre de cette disposition permettrait à l'agence d'obtenir des informations confidentielles qui ne sont nécessaires ni à la perception de la taxe ni à la bonne exécution de ses missions ;<br/>
<br/>
              Considérant toutefois, d'une part, que des sanctions ne pourraient être prononcées par le juge pénal que dans la limite du montant prévu à l'article L. 5421-6-3 du code de la santé publique et sous réserve que ce juge soit saisi par le procureur de la République ; que ce dernier ne pourrait lui-même être saisi que par l'AFSSAPS ; qu'il résulte des indications fournies à l'audience par les représentants de l'agence, confirmées par écrit dans le cadre du supplément d'instruction auquel il a été procédé, qu'en raison des circonstances, un délai allant jusqu'au 1er octobre 2012 est accordé aux entreprises soumises à l'obligation de déclaration en cause, ce dont elles seront informées sur le site de l'agence, et qu'en tout état de cause, celle-ci n'entend saisir la juridiction répressive que dans les cas où les entreprises ne se mettraient pas en conformité malgré des relances ou se placeraient chaque année en position irrégulière ; que, dans ces conditions, et à supposer d'ailleurs que la méconnaissance de prescriptions figurant dans un texte de portée nationale non publié au Journal officiel puisse donner lieu à condamnation pénale, le risque de sanction invoqué par la requérante ne caractérise pas une atteinte suffisamment grave et immédiate aux intérêts qu'elle défend pour constituer une situation d'urgence ;<br/>
<br/>
              Considérant, d'autre part qu'il ne résulte ni des éléments versés au dossier, ni des débats à l'audience que les entreprises concernées seraient directement exposées, par l'effet de la décision litigieuse, à un risque de divulgation d'informations confidentielles susceptible de porter une telle atteinte à leurs intérêts ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la condition d'urgence n'est pas remplie ; qu'ainsi, et sans qu'il soit besoin d'examiner si la fédération requérante fait état de moyens propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée, les conclusions de sa requête doivent être rejetées, y compris celles qu'elle présente sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la FÉDÉRATION DES ENTREPRISES DE LA BEAUTÉ, à l'Agence française de sécurité sanitaire des produits de santé et au ministre du travail, de l'emploi et de la santé. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
