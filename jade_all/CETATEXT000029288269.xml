<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288269</ID>
<ANCIEN_ID>JG_L_2014_07_000000367860</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/07/2014, 367860, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367860</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367860.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 18 avril et le 18 juillet 2013, présentés par la SA Auchan France, dont le siège est rue de la Recherche à Villeneuve d'Ascq (59650), représentée par son président directeur général en exercice ; la SA Auchan France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n°s 1578 T - 1582T - 1585 T -1586 T du 17 janvier 2013 par laquelle la Commission nationale d'aménagement commercial a accordé à la SCCV Maisons du Parc l'autorisation préalable requise en vue de la création d'un ensemble commercial d'une surface totale de 10 659 m², comprenant un supermarché d'une surface de vente de 1 796 m², 6 moyennes surfaces spécialisées d'une surface totale de vente de 3 900 m² et 42 boutiques spécialisées, de moins de 300 m² chacune, d'une surface totale de vente de 4 963 m² à Montauban (Tarn-et-Garonne) ;<br/>
<br/>
              2°) de mettre à la charge de la SCCV Maisons du Parc la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
              .<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu l'arrêté du 21 août 2009 fixant le contenu de la demande d'autorisation d'exploitation de certains magasins de commerce de détail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la composition du dossier de demande :<br/>
<br/>
              1. Considérant qu'il ne ressort pas des pièces du dossier que la zone de chalandise du projet, laquelle a été délimitée par un temps de trajet en voiture d'environ une heure et validée par les services instructeurs de la direction générale de la compétitivité, de l'industrie et des services, serait erronée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 752-9 du code de commerce : " Pour les projets de magasins de commerce de détail, la demande précise : (...) En cas de création, la surface de vente et le secteur d'activité (...) de chacun des magasins de plus de 1 000 mètres carrés, ainsi que, le cas échéant, la surface de vente globale du projet " ; qu'il résulte de ces dispositions que le dossier de demande n'avait pas à comporter d'indication relative au secteur d'activité des magasins prévus par le projet dont la surface de vente est inférieure à 1 000 m² ; que s'agissant des autres magasins autorisés, le dossier de demande comportait les indications requises en vertu des dispositions précitées ;<br/>
<br/>
              3. Considérant que si la requérante soutient que le dossier de demande était incomplet en ce qui concerne l'impact du projet sur les flux de véhicules et le développement durable, il ressort des pièces du dossier que la commission nationale a disposé des éléments suffisants lui permettant d'apprécier la conformité du projet aux objectifs fixés par le législateur ; <br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant, en premier lieu, que si la requérante soutient que la décision attaquée méconnaît l'objectif d'aménagement du territoire en raison de son impact sur l'animation de la vie urbaine, il ressort des pièces du dossier que le projet, qui permet, d'une part, de réhabiliter un site à l'état de friche commerciale depuis 2006 dans le centre-ville de Montauban et, d'autre part, de moderniser l'offre commerciale, participe à l'animation de cette commune ; que la commission nationale pouvait tenir compte, au titre de l'impact du projet sur les flux de véhicules, de l'accessibilité du projet par les modes de déplacements doux en dépit de l'absence de pistes cyclables et d'aires de stationnement réservées aux vélos ; que l'impact du projet sur les flux de véhicules n'est pas excessif au regard des capacités d'accueil des voies de circulation ; que, contrairement aux allégations des requérants, il ne ressort pas des pièces du dossier que le parc de stationnement de l'ensemble commercial dont la réalisation est prévue dans le cadre du projet ne pourrait être utilisé par la clientèle, ni que les consommateurs ne pourraient être en mesure d'utiliser également les parcs de stationnement publics situé à sa proximité ;<br/>
<br/>
              6. Considérant, en second lieu, que si la requérante soutient que la décision attaquée méconnaît l'objectif de développement durable en raison de la mauvaise insertion du projet dans les paysages, il ressort des pièces du dossier que le site sur lequel il est implanté ne présente pas de caractéristiques naturelles ou remarquables particulières ; que, par ailleurs, comme il a été dit ci-dessus, il permettra d'améliorer l'état du site d'implantation, qui constitue aujourd'hui une friche commerciale ; qu'à cet égard, le pétitionnaire prévoit la rénovation des façades de l'ensemble immobilier à l'intérieur duquel il se trouve en partie, ainsi que la création d'un jardin de 1 000 m² de nature à améliorer la qualité paysagère du site ; <br/>
<br/>
              7. Considérant que, par suite, le moyen tiré de ce que l'autorisation méconnaîtrait les objectifs fixés à l'article L. 750-1 du code de commerce et les critères posés à l'article L. 752-6 du même code doit être écarté ; qu'il résulte de ce tout qui précède que, dès lors, la société requérante n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de la SCCV Maisons du Parc la somme que demande la société requérante au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, il y a lieu de mettre à la charge de la SA Auchan France la somme de 5 000 euros, à verser à la SCCV Maisons du Parc au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la SA Auchan France est rejetée.<br/>
Article 2 : La SA Auchan France versera la somme de 5 000 euros à la SCCV Maisons du Parc au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SA Auchan France et à la SCCV Maisons du Parc.<br/>
Copie en sera adressée pour information à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
