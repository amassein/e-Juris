<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041709649</ID>
<ANCIEN_ID>JG_L_2020_03_000000425889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/70/96/CETATEXT000041709649.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 10/03/2020, 425889</TITRE>
<DATE_DEC>2020-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425889.20200310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Caen, d'une part, d'annuler la décision du 4 novembre 2015 par laquelle la ministre de la culture et de la communication a rejeté sa demande tendant à la réparation du préjudice qu'il allègue avoir subi du fait du caractère insuffisant des montants d'indemnité spécifique qui lui ont été versés au titre des années 2014 et 2015, d'autre part, de condamner l'Etat au paiement d'une somme d'au moins 716,56 euros, à abonder d'une somme de 26,54 euros pour chaque mois écoulé depuis la date de sa demande. Par un jugement n° 1600676 du 21 juin 2017, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17NT02197 du 30 novembre 2018, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, sur le fondement de l'article R. 351-2 du code de justice administrative, la requête formée par M. B... contre ce jugement. <br/>
<br/>
              Par cette requête et deux nouveaux mémoires, enregistrés les 21 décembre 2018 et 12 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande :<br/>
<br/>
              1°) l'annulation de ce jugement ;<br/>
<br/>
              2°) à ce qu'il soit fait droit à sa demande ;<br/>
<br/>
              3°) à ce que soit mise à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 4 novembre 2015, la ministre de la culture et de la communication a rejeté les demandes des 17 février, 20 juillet et 9 octobre 2015 de M. B..., technicien supérieur principal du développement durable en position normale d'activité affecté à la direction régionale des affaires culturelles de Normandie, tendant à la réparation du préjudice résultant, selon lui, des montants d'indemnité spécifique qui lui ont été versés au titre des années 2014 et 2015 et qu'il estime insuffisants. Par un jugement du 21 juin 2017, le tribunal administratif de Caen a rejeté la demande de M. B... tendant à ce que l'Etat soit condamné au paiement d'une somme d'au moins 716,56 euros, à abonder d'une somme de 26,54 euros pour chaque mois écoulé depuis 1er janvier 2016. Par un arrêt du 30 novembre 2018, la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, sur le fondement de l'article R. 351-2 du code de justice administrative, le recours formé, devant elle, par M. B... contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 811-1 du code de justice administrative, dans sa rédaction alors applicable : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : / (...) 8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 ; / (...) / Par dérogation aux dispositions qui précèdent, en cas de connexité avec un litige susceptible d'appel, les décisions portant sur les actions mentionnées au 8° peuvent elles-mêmes faire l'objet d'un appel (...) ". En vertu de l'article R. 222-14 de ce code, le montant des indemnités visées par le 8° de l'article R. 811-1, déterminé conformément à ce que prévoit l'article R. 222-15, est fixé à 10 000 euros. <br/>
<br/>
              3. La demande d'un fonctionnaire ou d'un agent public tendant seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative. Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception, prévue à ce 8°, en vertu de laquelle le tribunal administratif statue en dernier ressort. <br/>
<br/>
              4. Il résulte des pièces du dossier soumis aux juges du fond que M. B... n'invoquait pas d'autre préjudice que l'insuffisance des sommes qui lui ont été versées en application du décret du 25 août 2003 relatif à l'indemnité spécifique de service allouée aux ingénieurs des ponts, des eaux et des forêts et aux fonctionnaires des corps techniques de l'équipement. Par suite, la demande qu'il a présentée au tribunal administratif ne peut être regardée comme une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative, quand bien même cette demande se présentait comme tendant à la réparation d'un préjudice né d'une faute de l'administration. Il s'ensuit que le jugement du tribunal administratif de Caen n'a pas été rendu en dernier ressort et que la requête de M. B..., formée contre ce jugement, ne présente pas le caractère d'un pourvoi en cassation mais d'un appel, qui ressortit à la compétence de la cour administrative d'appel de Nantes.<br/>
<br/>
              5. Il résulte de ce qui précède qu'il y a lieu, en application des dispositions de l'article R. 351-1 du code de justice administrative, d'attribuer le jugement de la requête de M. B... à la cour administrative d'appel de Nantes.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de M. B... est attribué à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au ministre de la culture et au président de la cour administrative d'appel de Nantes. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - ACTION INDEMNITAIRE AU SENS DU 8° DE L'ARTICLE R. 811-1 DU CJA - EXCLUSION - DEMANDE D'UN AGENT PUBLIC TENDANT SEULEMENT AU VERSEMENT DE RÉMUNÉRATIONS IMPAYÉES, SANS CHERCHER LA RÉPARATION D'UN PRÉJUDICE DISTINCT DU PRÉJUDICE MATÉRIEL OBJET DE CETTE DEMANDE PÉCUNIAIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - INCLUSION - DEMANDE D'UN AGENT PUBLIC TENDANT SEULEMENT AU VERSEMENT DE RÉMUNÉRATIONS IMPAYÉES SANS CHERCHER LA RÉPARATION D'UN PRÉJUDICE DISTINCT DU PRÉJUDICE MATÉRIEL OBJET DE CETTE DEMANDE PÉCUNIAIRE [RJ1].
</SCT>
<ANA ID="9A"> 17-05-012 La demande d'un fonctionnaire ou d'un agent public tendant seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative (CJA). Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception, prévue à ce 8°, en vertu de laquelle le tribunal administratif statue en dernier ressort.</ANA>
<ANA ID="9B"> 17-05-015 La demande d'un fonctionnaire ou d'un agent public tendant seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative (CJA). Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception, prévue à ce 8°, en vertu de laquelle le tribunal administratif statue en dernier ressort.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 26 février 2016,,, n° 386953, T. pp. 695-696.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
