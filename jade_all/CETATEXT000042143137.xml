<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143137</ID>
<ANCIEN_ID>JG_L_2020_07_000000436865</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143137.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 22/07/2020, 436865, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436865</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436865.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Bordeaux de condamner l'Etat à lui verser la somme de 27 755,74 euros, assortie des intérêts au taux légal, au titre de l'allocation d'aide au retour à l'emploi qu'il estime lui être due pour la période du 1er septembre 2015 au 24 août 2016, et d'enjoindre au ministre de l'éducation nationale et de la jeunesse de procéder à ce versement dans le délai d'un mois à compter de la notification du jugement. Par un jugement n° 1704337 du 3 octobre 2019, le tribunal administratif de Bordeaux a rejeté cette demande comme portée devant une juridiction incompétente pour en connaître. <br/>
<br/>
              Par une ordonnance n° 19BX04634 du 17 décembre 2019, enregistrée le 19 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M. B..., enregistrée le 2 décembre 2019 au greffe de cette cour.<br/>
<br/>
              Par cette requête et par un nouveau mémoire, enregistré le 27 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 3 octobre 2019 du tribunal administratif de Bordeaux ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance, la somme réclamée devant en outre être assortie de la capitalisation des intérêts ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la loi organique n° 2019-706 du 5 juillet 2019 ; <br/>
              - la loi n° 86-845 du 17 juillet 1986, notamment son article 1er ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jehannin, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B..., recruté par contrats à durée déterminée du 17 mars 2014 au 31 août 2015 en qualité de chargé de gestion budgétaire et financière au sein du vice-rectorat de la Polynésie française, demande l'annulation du jugement du 3 octobre 2019 par lequel le tribunal administratif de Bordeaux a rejeté comme portée devant un ordre de juridiction incompétent pour en connaître sa demande tendant à ce que l'Etat soit condamné à lui verser les sommes qu'il estime lui être dues, à l'issue de ses contrats, au titre de l'allocation d'aide au retour à l'emploi. <br/>
<br/>
              Sur l'ordre juridictionnel compétent pour connaître du litige :<br/>
<br/>
              2. En vertu de l'article 74 de la Constitution, la Polynésie française a un statut qui tient compte de ses intérêts propres au sein de la République. Ce statut est défini par une loi organique, adoptée après avis de l'assemblée délibérante. Les autres modalités de l'organisation particulière de cette collectivité sont définies et modifiées par la loi après consultation de cette assemblée. En vertu de l'article 14 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, la " fonction publique civile et militaire de l'Etat " et le " statut des autres agents publics de l'Etat " sont au nombre des matières pour lesquelles les autorités de l'Etat sont compétentes. Il résulte en outre de l'article 7 de cette même loi organique, dans sa rédaction issue de la loi organique du 5 juillet 2019 portant modification du statut d'autonomie de la Polynésie française, que les dispositions législatives et réglementaires relatives aux " agents publics de l'Etat ", qu'ils relèvent ou non d'un statut, sont applicables de plein droit en Polynésie française, sans préjudice de dispositions les adaptant à son organisation particulière. L'Etat est ainsi seul compétent pour déterminer les modalités selon lesquelles est régie la situation des agents qu'il emploie, que ceux-ci soient titulaires ou non-titulaires, le cas échéant en choisissant de les soumettre à un statut. <br/>
<br/>
              3. Aux termes de l'article 1er de la loi du 17 juillet 1986 relative aux principes généraux du droit du travail et à l'organisation et au fonctionnement de l'inspection du travail et des tribunaux du travail en Polynésie française : " La présente loi est applicable dans le territoire de la Polynésie française (...). / Elle s'applique à tous les salariés exerçant leur activité dans le territoire. / Elle s'applique également à toute personne physique ou morale qui emploie lesdits salariés. / Sauf dispositions contraires de la présente loi, elle ne s'applique pas aux personnes relevant d'un statut de droit public (...) ". Aux termes de l'article 48 de la même loi : " Les travailleurs ayant involontairement perdu leur emploi, aptes au travail, et qui sont à la recherche d'un emploi ont droit à une aide dont les modalités d'application relèvent de la réglementation territoriale ". Enfin, il résulte des articles 73, 101, 104 et 105 de cette loi que les différends qui peuvent s'élever à l'occasion du contrat de travail relèvent en Polynésie française du tribunal du travail, qui est une juridiction judiciaire. Les dispositions de la loi du 17 juillet 1986, en tant qu'elle régit la situation des salariés recrutés localement exerçant leurs activités dans les services de l'Etat ou dans ses établissements publics administratifs, n'ont pas été abrogées par celles de la loi du pays du 4 mai 2011 relative à la codification du droit du travail, adoptée par l'assemblée de la Polynésie française au titre de la compétence qu'elle tient de la loi organique du 27 février 2004. L'application de plein droit en Polynésie française des dispositions relatives aux agents publics de l'Etat résultant de l'article 7 de cette loi organique, dans sa rédaction issue de la loi organique du 5 juillet 2019, ne fait pas davantage obstacle à l'application à ces agents des dispositions de la loi du 17 juillet 1986, cette application de plein droit s'exerçant comme il a été dit sans préjudice de dispositions les adaptant à son organisation particulière.<br/>
<br/>
              4. La réserve relative aux personnes " relevant d'un statut de droit public " faite à l'article 1er de la loi du 17 juillet 1986 ne s'applique qu'à des agents régis par le titre premier du statut général des fonctionnaires de l'Etat et des collectivités territoriales. Les agents non statutaires travaillant pour le compte d'un service public à caractère administratif n'étant des agents contractuels de droit public, quel que soit leur emploi, qu'en l'absence de loi contraire, il résulte des dispositions citées au point précédent que les agents contractuels de l'Etat et de ses établissements publics administratifs employés localement relèvent du régime de droit privé résultant de la loi du 17 juillet 1986 et que tous les litiges relatifs tant à la légalité des actes pris pour la conclusion, l'exécution ou la fin des contrats des agents non statutaires de l'Etat ou de ses établissements publics, qu'à la responsabilité de leurs auteurs, relèvent des seules juridictions judiciaires, désignées par cette loi. Il en va de même des litiges relatifs aux allocations d'assurance chômage réclamées à la suite de la rupture de ces contrats.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              5. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              6. Eu égard aux griefs invoqués par M. B..., la question prioritaire de constitutionnalité qu'il soulève doit être regardée comme dirigée, non contre le quatrième, mais contre le cinquième alinéa de l'article 1er de la loi du 17 juillet 1986, aux termes duquel : " Sauf dispositions contraires de la présente loi, elle ne s'applique pas aux personnes relevant d'un statut de droit public y compris les fonctionnaires et les agents non titulaires relevant du statut de droit public adopté par délibération de l'assemblée de la Polynésie française ", en tant qu'en n'excluant pas du champ d'application de la loi les agents contractuels de l'Etat, elles ont pour effet de soumettre les litiges relatifs à la situation de ces derniers au juge judiciaire. M. B... soutient que ce faisant, ces dispositions méconnaissent le droit à un recours juridictionnel effectif, le principe d'égalité devant la loi et devant la justice et le principe d'intelligibilité des normes.<br/>
<br/>
              7. Il résulte des dispositions critiquées elles-mêmes que le législateur a entendu que les agents contractuels de l'Etat et de ses établissements publics administratifs employés localement en Polynésie française relèvent du régime de droit privé résultant de la loi du 17 juillet 1986, à la différence des agents statutaires régis par le titre premier du statut général des fonctionnaires de l'Etat et des collectivités territoriales. La compétence confiée au juge judiciaire pour connaître des litiges se rattachant à cette relation de travail, qui n'est que la conséquence du régime de droit privé auquel elle est soumise, ne peut, en tout état de cause, être regardée comme portant atteinte au droit au recours effectif ainsi qu'au principe d'égalité devant la loi et devant la justice. Par suite, les griefs soulevés par M. B... contre les dispositions critiquées, qui en tout état de cause ne méconnaissent pas l'objectif à valeur constitutionnelle d'intelligibilité de la norme, ne posent pas de question nouvelle et ne présentent pas de caractère sérieux.<br/>
<br/>
              Sur les conclusions d'appel :<br/>
<br/>
              8. Aux termes de l'article R. 351-5-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions se rapportant à un litige qui ne relève pas de la compétence de la juridiction administrative, il est compétent, nonobstant les règles relatives aux voies de recours et à la répartition des compétences entre les juridictions administratives, pour se prononcer sur ces conclusions et décliner la compétence de la juridiction administrative ".<br/>
<br/>
              9. Il résulte de ce qui a été dit au point 4 qu'il n'appartient qu'au juge judiciaire de connaître de la demande de M. B..., tendant à ce que l'Etat soit condamné à lui verser les sommes qu'il estime lui être dues, à l'issue des contrats à durée déterminée par lesquels il a été recruté par le vice-rectorat de la Polynésie française en qualité d'agent non titulaire, au titre de l'allocation d'aide au retour à l'emploi. C'est dès lors à bon droit que, par le jugement attaqué, qui n'est entaché d'aucune irrégularité, le tribunal administratif de Bordeaux a rejeté cette demande comme portée devant une juridiction incompétente pour en connaître. Il en résulte que l'appel de M. B... contre ce jugement doit être rejeté.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B....<br/>
Article 2 : La requête de M. B... est rejetée. <br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre du travail, de l'emploi et de l'insertion, au ministre des outre-mer et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
