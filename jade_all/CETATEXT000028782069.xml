<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028782069</ID>
<ANCIEN_ID>JG_L_2014_03_000000373102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/78/20/CETATEXT000028782069.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 26/03/2014, 373102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373102.20140326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 et 18 novembre 2013, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1301720 du 17 octobre 2013 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant, d'une part, à la suspension de la décision du 18 septembre 2013 par laquelle son inscription en 5ème année d'études d'odontologie à l'université de Reims - Champagne-Ardenne a été refusée et, d'autre part, à ce qu'il soit enjoint à cette université de l'inscrire en 5ème année dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              2°) de mettre à la charge de l'université de Reims - Champagne-Ardenne la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur, <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B... et à la SCP Lyon-Caen, Thiriez, avocat de l'université de Reims - Champagne-Ardenne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le pourvoi de M. B...:<br/>
<br/>
              1. Considérant qu'ainsi que le rappelle la charte des examens de l'université de Reims - Champagne-Ardenne le jury est seul compétent pour déclarer un étudiant admis aux examens et stages d'une année d'études et pour procéder au retrait d'une telle décision d'admission, si elle est illégale, dans le délai de quatre mois suivant la date à laquelle elle a été prise ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le procès-verbal d'une délibération du jury d'admission aux examens et stages de 4ème année d'études d'odontologie de l'université Reims - Champagne-Ardenne déclarant M. B...admis, signé par le président du jury, a été affiché le 16 juillet 2013 dans les locaux de l'université ; que cette décision ne pouvait être retirée que par l'autorité compétente, en l'espèce le jury chargé de désigner les étudiants admis dans l'année supérieure ; que le relevé de notes daté également du 16 juillet 2013 mentionnant M. B...comme " ajourné ", qui a été signé par la seule chef des services administratifs de l'UFR d'odontologie, n'a pu avoir pour effet de retirer cette délibération, alors même que celle-ci était présentée comme ayant un caractère " provisoire " ; que, dès lors, en estimant que le moyen tiré de ce que l'admission de M. B...prononcée par le jury le 16 juillet 2013 n'avait pas été retirée à la date de la décision du 18 septembre 2013 lui refusant le droit de s'inscrire en 5ème année d'études d'odontologie n'était pas de nature à créer un doute sérieux quant à la légalité de cette décision, le juge des référés du tribunal administratif de Châlons-en-Champagne a commis une erreur de droit ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'ordonnance du juge des référés du tribunal administratif de Châlons-en-Champagne doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application  des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée par M. B...;<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              5. Considérant aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que la délibération du jury du 16 juillet 2013 déclarant M. B...admis a été rapportée par une nouvelle délibération du jury du 18 octobre 2013 le déclarant ajourné, édictée dans le délai de quatre mois suivant l'édiction de la première décision ; que la circonstance que M. B...a reçu notification de cette décision postérieurement à l'échéance du délai de quatre mois est en tout état de cause sans incidence sur la prise d'effet de ce retrait ; que, par suite, le moyen tiré de l'existence d'une décision du jury le déclarant admis n'est pas propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision du 18 septembre 2013 ayant refusé son inscription en 5ème année ; que, par suite, et sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par l'université de Reims - Champagne-Ardenne, ses conclusions tendant à la suspension de cette décision doivent être rejetées ainsi que, par voie de conséquence, ses conclusions à fins d'injonction et d'astreinte ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              7. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'université, qui n'est pas la partie perdante dans la présente instance ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées à ce titre par l'université Reims - Champagne-Ardenne ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Châlons-en-Champagne du 17 octobre 2013 est annulée.<br/>
Article 2 : La demande de M. B...est rejetée.<br/>
Article 3 : Les conclusions de l'université Reims - Champagne-Ardenne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à l'université de Reims - Champagne-Ardenne et à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
