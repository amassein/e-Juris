<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978240</ID>
<ANCIEN_ID>JG_L_2016_02_000000390842</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978240.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 03/02/2016, 390842, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390842</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:390842.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 390842, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 juin et 8 septembre 2015 et le 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des personnels de la communication et de l'audiovisuel CFE-CGC demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2015-159 du 23 avril 2015 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a nommé Mme A...B...en qualité de présidente de France Télévisions pour une durée de cinq ans à compter du 22 août 2015 ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 390912, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 juin et 8 septembre 2015 et le 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des médias - CFDT demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2015-159 du 23 avril 2015 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a nommé Mme A...B...en qualité de présidente de France Télévisions pour une durée de cinq ans à compter du 22 août 2015 ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat du syndicat national des personnels de la communication et de l'audiovisuel - CFE-CGC et du syndicat national des médias - CFDT et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes des deux premiers alinéas de l'article 47-4 de la loi du 30 septembre 1986 relative à la liberté de communication : " Les présidents de la société France Télévisions, de la société Radio France et de la société en charge de l'audiovisuel extérieur de la France sont nommés pour cinq ans par le Conseil supérieur de l'audiovisuel, à la majorité des membres qui le composent. Ces nominations font l'objet d'une décision motivée se fondant sur des critères de compétence et d'expérience. / Les candidatures sont présentées au Conseil supérieur de l'audiovisuel et évaluées par ce dernier sur la base d'un projet stratégique " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, sur le fondement de ces dispositions, le Conseil supérieure de l'audiovisuel (CSA) a, par une délibération du 23 avril 2015, nommé MmeB... en qualité de présidente de France Télévisions pour une durée de cinq ans à compter du 22 août 2015 ; que le Syndicat national des personnels de la communication et de l'audiovisuel CFE-CGC, d'une part, et le Syndicat national des médias - CFDT, d'autre part, demandent l'annulation pour excès de pouvoir de cette délibération ; que ces requêtes sont dirigées contre la même décision et présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              3. Considérant, en premier lieu, que si les requérants soutiennent que la décision attaquée a été prise au terme d'une procédure irrégulière au motif que le président du CSA aurait décidé de manière unilatérale, avant que le collège procède au vote destiné à désigner les candidats qui seraient auditionnés, de changer les modalités de cette pré-sélection, il ressort des pièces du dossier, en particulier du procès-verbal de la séance du 15 avril 2015, que c'est l'ensemble du collège qui a délibéré en ce sens ; qu'ainsi, le moyen manque en fait ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aucune disposition législative ou réglementaire, et notamment pas l'article 47-4 de la loi du 30 septembre 1986, non plus qu'aucun principe général du droit ne faisait obligation au CSA de rendre publics les noms des personnes ayant fait acte de candidature ou ceux des candidats qu'il avait sélectionnés pour une audition ; que les requérants ne sont, dès lors, pas fondés à soutenir que la décision attaquée a été prise au terme d'une procédure irrégulière, faute pour le CSA d'avoir publié ces noms ;<br/>
<br/>
              5. Considérant, en troisième lieu, que les requérants soutiennent que le principe d'impartialité a été méconnu, au motif qu'après l'ouverture des enveloppes contenant les candidatures et avant de procéder au vote qui a permis, le 15 avril 2015, de déterminer la liste des candidats devant être auditionnés, le président du CSA aurait invité les membres du conseil supérieur à ne pas déstabiliser d'autres entreprises du secteur audiovisuel en nommant à la présidence de France Télévisions le dirigeant d'une de ces entreprises, et qu'il aurait en particulier évoqué le cas d'une des candidates, présidente de la société Média France ; qu'à supposer que le président du CSA ait effectivement tenu de tels propos lors de cette séance, cette prise de position à l'occasion des délibérations internes au collège sur le choix des candidats à auditionner ne peut être regardée comme constitutive d'une atteinte au principe d'impartialité  ; qu'en outre, contrairement à ce qui est soutenu, il ne ressort pas des pièces du dossier que le président aurait publiquement pris position en faveur ou en défaveur de l'un quelconque des candidats ; qu'ainsi, la délibération du CSA du 23 avril 2015 n'est pas entachée d'irrégularité ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que les requérants invoquent une autre atteinte au principe d'impartialité, en soutenant que MmeC..., membre du collège du CSA, aurait noué des liens avec Mme B...après le dépôt par celle-ci de sa candidature ; que toutefois, à l'appui de ce moyen, les requérants se bornent à évoquer un déjeuner qui aurait eu lieu entre ces deux personnes et à produire un article de presse postérieur à la décision attaquée ; qu'en outre, l'existence de ce déjeuner est fermement démentie par les intéressées ; qu'il en résulte que de tels liens ne peuvent être regardés comme établis en l'état du dossier ; que par suite, le moyen doit, en tout état de cause, être écarté ;<br/>
<br/>
              7. Considérant, en dernier lieu, qu'il résulte des dispositions de l'article 47-4 de la loi du 30 septembre 1986 citées au point 1 que la décision de nomination du président de France Télévisions doit être motivée et doit se fonder sur des critères de compétence et d'expérience ainsi que sur l'examen du projet stratégique du candidat retenu ; que la circonstance que le projet stratégique de Mme B...présente certaines similitudes terminologiques avec celui d'un autre candidat ne saurait, en tout état de cause, permettre à elle seule d'établir que Mme B...n'aurait pas présenté un projet personnel, qui diffère à plusieurs titres de celui de cet autre candidat ; que, pour procéder à la nomination de MmeB..., le CSA a tenu compte des compétences, notamment managériales, que cette dernière a pu développer dans le secteur des télécommunications et du numérique, avant d'examiner la pertinence de sa candidature au regard de son projet stratégique, dont l'un des axes principaux prévoyait le développement numérique des services offerts par le groupe France Télévisions ; qu'en estimant que Mme B...satisfaisait au double critère de compétence et d'expérience pour présider France Télévisions et en la nommant à ce poste par une décision qui est suffisamment motivée, le CSA n'a pas commis d'erreur manifeste d'appréciation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les fins de non-recevoir opposées par le CSA et par MmeB..., les requérants ne sont pas fondés à demander l'annulation pour excès de pouvoir de la délibération attaquée ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de chacun des requérants le versement de la somme de 1 500 euros à Mme B... au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Syndicat national des personnels de la communication et de l'audiovisuel CFE-CGC et la requête du Syndicat national des médias - CFDT sont rejetées.<br/>
<br/>
Article 2 : Le Syndicat national des personnels de la communication et de l'audiovisuel CFE-CGC et le Syndicat national des médias - CFDT verseront chacun à Mme B...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des personnels de la communication et de l'audiovisuel CFE-CGC, au Syndicat national des médias - CFDT, au Conseil supérieur de l'audiovisuel et à Mme A...B....<br/>
		Copie en sera adressée à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
