<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713412</ID>
<ANCIEN_ID>JG_L_2015_06_000000375664</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713412.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 09/06/2015, 375664, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375664</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375664.20150609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Melun d'annuler la décision née du rejet implicite par le maire du Perreux-sur-Marne de sa demande de transformation en contrat à durée indéterminée du contrat qui la liait à cette commune depuis octobre 2000, ainsi que le contrat du 27 août 2012, ayant succédé au précédent, en tant qu'il a été conclu pour une durée déterminée. Par un jugement n°1209413/14 du 20 décembre 2013, le tribunal administratif de Melun a fait droit à cette double demande.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 février 2014 et 19 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la commune du Perreux-sur-Marne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Melun ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeA... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de la commune du Perreux-sur-Marne et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un contrat d'un an conclu le 4 octobre 2000, MmeA..., psychologue, a été recrutée par la commune du Perreux-sur-Marne pour assurer des consultations au service communal de la petite enfance, à raison de huit heures par mois ; que ce contrat a fait l'objet de reconductions ininterrompues jusqu'au 31 août 2012 ; que, par lettre du 27 juillet 2012, Mme A...a demandé la transformation de son contrat à durée déterminée en un contrat à durée indéterminée sur le fondement de l'article 21 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique ; que le maire n'a pas répondu à sa demande et lui a proposé un nouveau contrat d'une année à compter du 1er septembre 2012, qu'elle a signé le 27 août 2012 ; que, sur demande de MmeA..., le tribunal administratif de Melun a annulé, d'une part, la décision née du rejet implicite de sa demande de transformation de son contrat en contrat à durée indéterminée et, d'autre part, le contrat signé le 27 août 2012 en tant qu'il a été conclu pour une durée déterminée ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions du deuxième alinéa de l'article R.811-1 du code de justice administrative, combinées avec celles du 2° de l'article R.222-13 du même code, dans leur rédaction applicable aux jugements des tribunaux administratifs rendus avant le 1er janvier 2014 que, alors même que la voie de l'appel devant la cour administrative d'appel n'est en principe pas ouverte contre ceux de ces jugements statuant sur les litiges relatifs à la situation individuelle des agents publics, les litiges concernant l'entrée au service, au nombre desquels figurent les recours dirigés contre le contrat par lequel l'administration emploie un agent, notamment par renouvellement d'un contrat précédemment conclu avec l'intéressé, sont susceptibles d'un appel ;<br/>
<br/>
              3. Considérant que la requête de la commune du Perreux-sur-Marne tend à l'annulation du jugement du 20 décembre 2013 par lequel le tribunal administratif de Melun a, à la demande de MmeA..., annulé le rejet implicite par le maire de cette commune de sa demande de transformation de son contrat en contrat à durée indéterminée ainsi que le contrat du 27 août 2012 renouvelant, pour une durée déterminée, son engagement dans les services de cette commune ; qu'il résulte de ce qui a été dit ci-dessus qu'un tel litige concerne l'entrée au service au sens des dispositions de l'article R. 811-1 du code de justice administrative ; que, par suite, la requête de la commune a le caractère d'un appel qui ne ressortit pas à la compétence du Conseil d'Etat, juge de cassation, mais à celle de la cour administrative d'appel de Paris ; qu'il y a lieu, dès lors, d'en attribuer le jugement à cette cour ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la commune du Perreux-sur-Marne est attribué à la cour administrative d'appel de Paris.<br/>
Article 2 : La présente décision sera notifiée à la commune du Perreux-sur-Marne, à Mme A...et au président de la cour administrative d'appel de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
