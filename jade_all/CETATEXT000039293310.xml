<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039293310</ID>
<ANCIEN_ID>JG_L_2019_10_000000418328</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/29/33/CETATEXT000039293310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 16/10/2019, 418328, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418328</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418328.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 26 janvier 2016 par laquelle l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. Par une décision n°16007271 du 28 novembre 2017, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 19 février 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaitre la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Office de protection des réfugiés et apatrides la somme de 2 000 euros à verser à la SCP Matuchansky, Poupot, Valdelièvre, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Mme A... soutient que la Cour nationale du droit d'asile a entaché sa décision :<br/>
              - d'erreur de droit et d'insuffisance de motivation en rejetant sa demande au seul motif qu'elle ne serait pas parvenue à se soustraire à l'emprise du réseau de prostitution, alors qu'elle avait démontré avoir entamé des démarches actives en ce sens et qu'elle était en tout état de cause victime d'un réseau de traite des êtres humains aux fins d'exploitation sexuelle ;<br/>
              - de dénaturation des pièces au dossier en relevant que les éléments produits ne suffisaient pas à établir qu'elle cherchait à s'extraire de ce réseau ;<br/>
              - de dénaturation des pièces au dossier en relevant qu'elle n'établissait pas s'exposer à des persécutions en cas de retour au Nigéria ;<br/>
              - d'erreur de droit en lui refusant le bénéfice de la protection subsidiaire en dépit des risques de traitements inhumains et dégradants auxquels elle s'exposerait en cas de retour au Nigéria.<br/>
<br/>
              Par un mémoire en défense, enregistré le 31 janvier 2019, l'Office français de protection des réfugiés et apatrides conclut au rejet du pourvoi. Il soutient qu'aucun moyen du pourvoi n'est fondé.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de Mme B... A... et à la SCP Foussard, Froger, avocat de l'Office français de protection des refugies et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Mme A... se pourvoit en cassation contre la décision du 28 novembre 2017 par laquelle la Cour nationale du droit d'asile a rejeté son recours contre la décision du 26 janvier 2016 de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile. <br/>
<br/>
              2. L'article 1er de la convention de Genève du 28 juillet 1951 précise, au 2° de son paragraphe A, que doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Un groupe social, au sens de ces stipulations, est constitué de personnes partageant un caractère inné, une histoire commune ou une caractéristique essentielle à leur identité et à leur conscience, auxquelles il ne peut leur être demandé de renoncer, ou une identité propre perçue comme étant différente par la société environnante ou par les institutions. L'appartenance à un tel groupe est un fait social objectif qui ne dépend pas de la manifestation par ses membres, ou, s'ils ne sont pas en mesure de le faire, par leurs proches, de leur appartenance à ce groupe.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que les femmes nigérianes originaires de l'Etat d'Edo, victimes d'un réseau de traite des êtres humains à des fins d'exploitation sexuelle, lorsqu'elles sont effectivement parvenues à s'extraire d'un tel réseau, partagent une histoire commune et une identité propre, perçues comme spécifiques par la société environnante dans leur pays, où elles sont frappées d'ostracisme pour avoir rompu leur serment sans s'acquitter de leur dette. Elles doivent, dans ces conditions, être regardées comme constituant un groupe social au sens des stipulations précitées de la convention de Genève.<br/>
<br/>
              4. Il ressort des énonciations de la décision attaquée qu'alors que Mme A..., de nationalité nigériane, faisait valoir qu'elle craignait des persécutions en cas de retour au Nigéria en raison de son appartenance à ce groupe social, la Cour nationale du droit d'asile s'est fondée, pour rejeter son recours, sur l'absence d'éléments suffisamment étayés concernant, d'une part, sa sortie effective du réseau et, d'autre part, les persécutions dont elle pourrait être victime en cas de retour au Nigéria.<br/>
<br/>
              5. En premier lieu, après avoir relevé que le courrier de Mme A... à la brigade de répression du proxénétisme de Paris et sa plainte adressée au procureur de la République le 17 octobre 2017 présentaient de façon particulièrement lacunaire son parcours, l'identité de sa proxénète et des autres membres du réseau ainsi que les conditions de son activité de prostitution, la Cour nationale du droit d'asile a retenu, au terme d'une appréciation souveraine des faits de l'espèce exempte de dénaturation, que les déclarations évasives et peu circonstanciées de la requérante ne permettaient pas de tenir pour établi qu'elle se serait effectivement soustraite à l'emprise d'un réseau de traite des êtres humains aux fins d'exploitation sexuelle. En recherchant, pour caractériser son appartenance au groupe social dont elle se revendiquait, si des éléments permettaient d'établir que Mme A... était effectivement parvenue à s'extraire du réseau, la Cour, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En second lieu, en retenant que Mme A... ne produisait aucun élément étayé à l'appui de ses allégations relatives aux menaces qu'elle aurait reçues et aux risques de persécutions auxquels elle serait exposée en cas de retour au Nigéria, la Cour nationale du droit d'asile n'a pas dénaturé les pièces qui lui étaient soumises. Elle n'a pas davantage commis d'erreur de droit en refusant, pour ce motif, d'accorder la protection subsidiaire à l'intéressée sur le fondement de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              7. Il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Son pourvoi doit donc être rejeté, y compris les conclusions présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B... A... et à à l'Office français de protection des réfugiés et apatrides. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-01-02-03-05 - PROSTITUÉES NIGÉRIANES ORIGINAIRES DE L'ETAT D'EDO ÉTANT EFFECTIVEMENT PARVENUES À S'EXTRAIRE D'UN RÉSEAU DE TRAITE DES ÊTRES HUMAINS - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 095-03-01-02-03-05 Les femmes nigérianes originaires de l'Etat d'Edo, victimes d'un réseau de traite des êtres humains à des fins d'exploitation sexuelle, lorsqu'elles sont effectivement parvenues à s'extraire d'un tel réseau, partagent une histoire commune et une identité propre, perçues comme spécifiques par la société environnante dans leur pays, où elles sont frappées d'ostracisme pour avoir rompu leur serment sans s'acquitter de leur dette. Elles doivent, dans ces conditions, être regardées comme constituant un groupe social au sens des stipulations du 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la notion de groupe social, CE, Assemblée, 21 décembre 2012,,, n° 334941, pp. 418-429.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
