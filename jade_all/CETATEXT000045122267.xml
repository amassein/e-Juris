<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045122267</ID>
<ANCIEN_ID>JG_L_2022_02_000000454988</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/12/22/CETATEXT000045122267.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 03/02/2022, 454988, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454988</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:454988.20220203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Ô Jardin a demandé au juge des référés du tribunal administratif de La Réunion de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 2 octobre 2020 du maire de Saint-Pierre prononçant la fermeture administrative de son établissement. Par une ordonnance n° 2100734 du 12 juillet 2021, le juge des référés a fait droit à sa demande.<br/>
<br/>
              	Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 juillet, 11 août et 2 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Pierre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la société Ô Jardin la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Saint-pierre et à la SCP L. Poulet, Odent, avocat de la société Ô Jardin.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de La Réunion que, par un arrêté du 2 octobre 2020, le maire de la commune de Saint-Pierre a prononcé la fermeture administrative d'un espace de réception exploité par la société Ô Jardin dans le quartier de Pierrefonds. La commune de Saint-Pierre se pourvoit en cassation contre l'ordonnance du 12 juillet 2021 par laquelle le juge des référés a, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, suspendu l'exécution de cet arrêté. <br/>
<br/>
              3. En jugeant que le moyen tiré de l'insuffisante motivation de l'arrêté en litige était de nature à créer un doute sérieux quant à sa légalité, au motif que le texte de cet arrêté n'indiquait pas de manière suffisamment précise la nature des risques justifiant la fermeture et la nature des aménagements ou travaux à exécuter pour y remédier, alors que cet arrêté visait, d'une part, les textes applicables à la sécurité des établissements recevant du public, d'autre part, les nombreux documents par lesquels la commission consultative départementale de sécurité et d'accessibilité et la commune avaient au préalable informé la société Ô Jardin des travaux nécessaires à sa mise en conformité avec ces textes, le juge des référés a dénaturé les pièces du dossier qui lui était soumis. Il y a lieu, par suite, d'annuler son ordonnance, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé et de statuer sur la demande de suspension présentée par la société Ô Jardin en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Pour demander la suspension de l'exécution de l'arrêté du 2 octobre 2020 contesté, la société Ô Jardin soutient :<br/>
              - que cet arrêté est insuffisamment motivé ; <br/>
              - que la mesure présente un caractère disproportionné. <br/>
<br/>
              6. Aucun de ces moyens n'est, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté. La demande de suspension de l'exécution de cet arrêté doit, par suite, être rejetée, ainsi que, par voie de conséquence, les conclusions présentées par la société Ô Jardin au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Ô Jardin la somme demandée par la commune de Saint-Pierre au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du 12 juillet 2021 du juge des référés du tribunal administratif de La Réunion est annulée. <br/>
<br/>
Article 2 : Les conclusions présentées par la société Ô Jardin devant le juge des référés sont rejetées. <br/>
<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Ô Jardin et à la commune de Saint-Pierre.<br/>
              Délibéré à l'issue de la séance du 13 janvier 2022 où siégeaient : M. Olivier Yeznikian, conseiller d'Etat, présidant ; M. Jean-Philippe Mochon, conseiller d'Etat et M. Jean-Dominique Langlais, conseiller d'Etat-rapporteur. <br/>
<br/>
Rendu le 3 février 2022.<br/>
                 Le président : <br/>
                 Signé : M. Olivier Yeznikian<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Dominique Langlais<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
