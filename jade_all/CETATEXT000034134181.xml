<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034134181</ID>
<ANCIEN_ID>JG_L_2017_02_000000408216</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/13/41/CETATEXT000034134181.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/02/2017, 408216, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408216</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:408216.20170223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 février 2017 au secrétariat du contentieux du Conseil d'Etat, l'association professionnelle des membres de l'inspection générale de la jeunesse et des sports demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension du décret du président de la République en date du 10 février 2017 nommant M. B... A... au grade d'inspecteur général de la jeunesse et des sports de deuxième classe, à compter du 11 mars 2017.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - le requête est recevable dès lors que l'association a qualité pour agir et justifie d'un intérêt à agir au regard de ses statuts et de son objet ;<br/>
              - la condition d'urgence est remplie dès lors que la nomination litigieuse est de nature à jeter le discrédit sur ce corps supérieur de contrôle ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - le dossier de candidature de M. A...est irrégulier et incomplet dès lors qu'il ne contenait pas tous les éléments exigés par l'avis d'appel à candidatures, notamment une lettre de motivation, un curriculum vitae, et une déclaration d'intérêts, adaptés aux exigences du poste ;<br/>
              -  la nomination litigieuse est entachée d'un vice de procédure de nature à altérer l'impartialité de la commission de sélection dès lors que la présentation des dossiers de candidature n'a pas été effectuée par la personne prévue par les dispositions du décret n° 2002-53 du  10 janvier 2002, et que l'inscription du nom de M. A...sur la liste présentée au ministre chargé de la jeunesse et des sports résulte d'un départage des votes irrégulier ;<br/>
              - elle est entachée d'une erreur manifeste d'appréciation dès lors que le ministre aurait dû, au regard des éléments dont il disposait, renoncer à proposer au Président de la République la nomination de M. A..., étant donné sa récente candidature au comité directeur de la fédération française de basket-ball, incompatible avec une nomination dans le corps de l'inspection générale de la jeunesse et des sports, et au regard du non-respect des obligations déontologies afférentes à ce poste.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2015-1541 du 27 novembre 2015 ;<br/>
              - le décret n° 85-344 du 18 mars 1985 ;<br/>
              - le décret n° 99-828 du 21 septembre 1999 ;<br/>
              - le décret n° 2002-53 du 10 janvier 2002 ;<br/>
              - le décret n° 2005-1795 du 30 décembre 2005 ;<br/>
              - le décret n° 2009-639 du 8 juin 2009 ;<br/>
              - le décret n° 2013-727 du 12 août 2013 ;<br/>
              - le décret n° 2016-1967 du 28 décembre 2016 ;<br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision  En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              3. La nomination contestée d'un inspecteur général de la jeunesse et des sports n'entraîne ni pour l'intérêt général ni pour les intérêts défendus par l'association professionnelle requérante de conséquences de nature à caractériser une situation d'urgence.<br/>
<br/>
              4. En l'absence d'urgence, la requête à fin de suspension présentée par l'association professionnelle des membres de l'inspection générale de la jeunesse et des sports ne peut qu'être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association professionnelle des membres de l'inspection générale de la jeunesse et des sports est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association professionnelle des membres de l'inspection générale de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
