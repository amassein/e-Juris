<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034940728</ID>
<ANCIEN_ID>JG_L_2017_06_000000398535</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/07/CETATEXT000034940728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 14/06/2017, 398535</TITRE>
<DATE_DEC>2017-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398535</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; DELAMARRE</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398535.20170614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Grenoble d'annuler la décision du 19 août 2014 par laquelle le président du conseil général de l'Isère a rejeté son recours administratif contre la décision de la caisse d'allocations familiales de l'Isère du 13 mai 2014 refusant de lui accorder le bénéfice du revenu de solidarité active. Par un jugement n° 1502782 du 5 février 2016, le magistrat désigné par le président du tribunal administratif de Grenoble a annulé la décision du 19 août 2014 et renvoyé Mme B...devant le département de l'Isère aux fins de déterminer ses droits au revenu de solidarité active à la date de sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 avril 2016, 5 juillet 2016 et 27 février 2017 au secrétariat du contentieux du Conseil d'Etat, le département de l'Isère demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Grenoble du 5 février 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par Mme B...devant le tribunal administratif de Grenoble.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat du département de l'Isère, et à Me Delamarre, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales de l'Isère, par une décision du 13 mai 2014, a refusé d'accorder à Mme A... B...le bénéfice du revenu de solidarité active au motif que son concubin exerçait une activité professionnelle en qualité de travailleur indépendant et qu'il avait réalisé un chiffre d'affaires excédant le niveau, fixé par décret, permettant le bénéfice de cette prestation. Par un jugement du 5 février 2016, le tribunal administratif de Grenoble a annulé la décision du 19 août 2014 par laquelle le président du conseil général de l'Isère a rejeté le recours administratif formé par Mme B...contre cette décision et l'a renvoyée devant le département aux fins de déterminer ses droits au revenu de solidarité active à la date de sa demande. Le département de l'Isère se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes du deuxième alinéa de l'article L. 115-2 du code de l'action sociale et des familles : " Le revenu de solidarité active, mis en oeuvre dans les conditions prévues au chapitre II du titre VI du livre II, complète les revenus du travail ou les supplée pour les foyers dont les membres ne tirent que des ressources limitées de leur travail et des droits qu'ils ont acquis en travaillant ou sont privés d'emploi. " Aux termes de l'article L. 262-2 du même code, dans sa rédaction applicable au litige : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un revenu garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du revenu garanti (...) ". Le deuxième alinéa de l'article L. 262-3 de ce code dispose que : " L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active, dans des conditions fixées par un décret en Conseil d'Etat (...) ". L'article R. 262-32 du même code précise que : " Lorsque, au sein du foyer, un des membres ou son conjoint, partenaire lié par un pacte civil de solidarité ou concubin est déjà allocataire au titre des prestations familiales, il est également le bénéficiaire au titre de l'allocation de revenu de solidarité active. / Dans le cas contraire, le bénéficiaire est celui qu'ils désignent d'un commun accord. Ce droit d'option peut être exercé à tout moment. L'option ne peut être remise en cause qu'au bout d'un an, sauf changement de situation. Si ce droit d'option n'est pas exercé, le bénéficiaire est celui qui a déposé la demande d'allocation ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 262-7 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Pour bénéficier du revenu de solidarité active, le travailleur relevant du régime mentionné à l'article L. 611-1 du code de la sécurité sociale doit n'employer, au titre de son activité professionnelle, aucun salarié et réaliser un chiffre d'affaires n'excédant pas un niveau fixé par décret. (...) ". L'article D. 262-16 du même code, alors en vigueur, prévoyait, s'agissant des travailleurs indépendants exerçant une activité commerciale, que le dernier chiffre d'affaires annuel connu, actualisé le cas échéant, ne devait pas excéder le montant fixé à l'article 50-0 du code général des impôts, soit 82 200 ou 32 900 euros hors taxes selon que l'entreprise avait une activité de vente de marchandises ou de services.<br/>
<br/>
              4. Il résulte des dispositions citées au point 2 que le revenu de solidarité active est versé en fonction des ressources dont dispose le foyer, au sein duquel le bénéficiaire est d'ailleurs, en l'absence de perception de prestations familiales, librement déterminé par les intéressés. Dès lors, les dispositions de l'article L. 262-7 du code de l'action sociale et des familles qui fixent les conditions dans lesquelles un travailleur relevant du régime social des indépendants peut " bénéficier du revenu de solidarité active " doivent nécessairement être comprises comme visant tant le bénéficiaire lui-même que son conjoint, son partenaire lié par un pacte civil de solidarité ou son concubin faisant partie du foyer.  <br/>
<br/>
              5. Par suite, en jugeant que la circonstance que le concubin de Mme B...exerçait une activité professionnelle en qualité de travailleur indépendant et qu'il avait réalisé un chiffre d'affaires excédant celui prévu par les dispositions combinées des articles L. 262-7 et D. 262-16 du code de l'action sociale et des familles n'était pas de nature à exclure la requérante du bénéfice du revenu de solidarité active, le magistrat désigné par le président du tribunal administratif de Grenoble a commis une erreur de droit. Le département de l'Isère est, en conséquence, fondé à demander l'annulation du jugement du 5 février 2016.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne à l'allocation de revenu de solidarité active, il appartient au juge administratif, eu égard tant à la finalité de son intervention dans la reconnaissance du droit à cette allocation qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé sur lesquels l'administration s'est prononcée, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative. Au vu de ces éléments, il appartient au juge administratif d'annuler ou de réformer, s'il y a lieu, cette décision en fixant alors lui-même les droits de l'intéressé, pour la période en litige, à la date à laquelle il statue ou, s'il ne peut y procéder, de renvoyer l'intéressé devant l'administration afin qu'elle procède à cette fixation sur la base des motifs de son jugement.<br/>
<br/>
              8. En premier lieu, Mme B...ne peut utilement se prévaloir, à l'appui de sa demande tendant à se voir reconnaître le bénéfice du revenu de solidarité active, des vices propres qui entacheraient la décision du 19 août 2014 par laquelle le président du conseil général de l'Isère a rejeté son recours administratif. Les moyens tirés de l'incompétence de son signataire, du caractère insuffisant de sa motivation et de l'absence de consultation de la commission de recours amiable de la caisse d'allocations familiales de l'Isère, en méconnaissance de l'article L. 262-47 du code de l'action sociale et des familles, ne peuvent donc qu'être écartés comme dépourvus d'incidence sur la solution du litige.<br/>
<br/>
              9. En second lieu, il résulte de l'instruction, et il n'est pas contesté, que le concubin de Mme B...exerçait une activité professionnelle le faisant relever du régime social des indépendants et que son chiffre d'affaires s'élevait à 104 000 euros pour la période du 1er octobre 2012 au 30 septembre 2013, dernier exercice connu au moment de sa demande, excédant ainsi le plafond défini par l'article D. 262-16 du code de l'action sociale et des familles. Par suite, en application des dispositions de l'article L. 262-7 du même code, le président du conseil général de l'Isère était en droit de rejeter la demande de revenu de solidarité active formée le 19 mars 2014 par MmeB....<br/>
<br/>
              10. Il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation de la décision du 19 août 2014 par laquelle le président du conseil général de l'Isère a confirmé le refus de lui accorder le bénéfice du revenu de solidarité active.<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de l'Isère, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 5 février 2016 est annulé.<br/>
Article 2 : La demande présentée par Mme B...devant le tribunal administratif de Grenoble est rejetée.<br/>
Article 3 : Les conclusions de Mme B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.    <br/>
Article 4 : La présente décision sera notifiée au département de l'Isère et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - DROIT À L'ALLOCATION EN FONCTION DES RESSOURCES DU FOYER - CONSÉQUENCE - DISPOSITIONS FIXANT LES CONDITIONS DANS LESQUELLES UN TRAVAILLEUR INDÉPENDANT PEUT BÉNÉFICIER DU RSA - CHAMP D'APPLICATION - INCLUSION - CONJOINT, PARTENAIRE LIÉ PAR UN PACS OU CONCUBIN FAISANT PARTIE DU FOYER.
</SCT>
<ANA ID="9A"> 04-02-06 Il résulte des articles L. 115-2, L. 262-2 et L. 262-3 du code de l'action social et des familles (CASF) que le revenu de solidarité active est versé en fonction des ressources dont dispose le foyer, au sein duquel le bénéficiaire est d'ailleurs, en l'absence de prestations familiales, librement déterminé par les intéressés. Dès lors, les dispositions de l'article L. 262-7 du CASF qui fixent les conditions dans lesquelles un travailleur relevant du régime social des indépendants peut bénéficier du revenu de solidarité active doivent nécessairement être comprises comme visant tant le bénéficiaire lui-même que son conjoint, son partenaire lié par un pacte civil de solidarité (PACS) ou son concubin faisant partie du foyer.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
