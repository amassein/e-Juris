<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806220</ID>
<ANCIEN_ID>JG_L_2021_12_000000448699</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/12/2021, 448699, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448699</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448699.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme G... E... a demandé au tribunal administratif de la Guadeloupe d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune du Lamentin (Guadeloupe) pour la désignation des conseillers municipaux et communautaires de la commune, de prononcer la suspension, sur le fondement de l'article L. 250-1 du code électoral, du mandat du maire élu jusqu'à l'organisation de nouvelles élections et de prononcer l'inéligibilité de M. D... H.... Par un jugement n° 2000286 du 17 décembre 2020, le tribunal administratif a, d'une part, annulé les opérations électorales, d'autre part, fixé le montant du remboursement forfaitaire dû à M. H... en application de l'article L. 52-11-1 du code électoral à la somme de zéro euro et, enfin, rejeté le surplus des conclusions de Mme E.... <br/>
<br/>
              I. Sous le n° 448699, par une requête et un mémoire en réplique, enregistrés les 15 janvier et le 3 mai 2021 au secrétariat du contentieux du Conseil d'Etat, Mme E... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il ne fait pas droit à ses conclusions aux fins de suspension, au titre de l'article L. 250-1 du code électoral, et d'inéligibilité de M. H... ;<br/>
<br/>
              2°) statuant en appel, de faire droit à ces conclusions ;<br/>
<br/>
<br/>
<br/>
<br/>
              3°) de mettre à la charge de M. H... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              II. Sous le n° 449874, par une requête sommaire et un mémoire complémentaire, enregistrés les 18 février et 18 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. H... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif en tant qu'il a fait droit à la protestation de Mme E... ;<br/>
<br/>
              2°) statuant en appel, de rejeter cette protestation ;<br/>
<br/>
              3°) de mettre à la charge de Mme E... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de M. H... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. A l'issue des opérations électorales auxquelles il a été procédé le 15 mars 2020 au premier tour de scrutin dans la commune du Lamentin (Guadeloupe), l'élection des conseillers municipaux et conseillers communautaires a été acquise. La liste " Ensemble, continuons la réussite " conduite par M. H..., maire sortant, a obtenu 2 764 suffrages, soit 50,04 % des suffrages exprimés. La liste " Parti socialiste guadeloupéen et autres démocrates ", conduite par M. F..., a obtenu 1 417 suffrages, soit 25,65 % suffrages exprimés, tandis que la liste " Lamentin Demain ", conduite par Mme E..., a obtenu 728 suffrages, soit 13,18 % des suffrages exprimés. Deux autres listes ont recueilli respectivement 7,22 % et 3,89 % des suffrages exprimés. Saisi d'une protestation par Mme E..., le tribunal administratif de la Guadeloupe a, par un jugement du 17 décembre 2020, d'une part, annulé les opérations électorales du 15 mars 2020, d'autre part, fixé le montant du remboursement forfaitaire dû à M. H... en application de l'article L. 52-11-1 du code électoral à la somme de zéro euro et, enfin, rejeté le surplus de la protestation. Par deux requêtes qu'il y a lieu de joindre pour statuer par une seule décision, d'une part, M. H... fait appel de ce jugement en tant qu'il a annulé les opérations électorales et fixé à zéro euro le montant du remboursement forfaitaire qui lui est dû, d'autre part, Mme E... fait appel de ce même jugement en tant qu'il a rejeté le surplus de sa protestation. <br/>
<br/>
              En ce qui concerne la requête de M. H... :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". <br/>
<br/>
              3. D'une part, il résulte de l'instruction que le numéro du bulletin municipal " Lament'innov " intitulé " 2014-2020 - 60 mois d'actions et de réalisations ", diffusé à huit mille exemplaires, comporte un éditorial du maire qui dresse un bilan avantageux des réalisations accomplies par la municipalité au cours de sa mandature. Ce numéro expose également, parfois en des termes particulièrement flatteurs, plus de cent cinquante projets ou événements réalisés par la municipalité sortante, sur une quarantaine de pages illustrées par de nombreuses photographies du maire. Bien que la pagination et le format de ce bulletin municipal soient similaires à l'édition précédente, ce numéro s'apparente, au regard de son contenu, à une édition spéciale. Si le requérant soutient que ce bulletin a été diffusé au plus tard le 26 août 2019, il ressort des pièces du dossier, et notamment des procès-verbaux des auditions des membres de la commission de contrôle des listes électorales menées par la formation de jugement du tribunal administratif de la Guadeloupe, que la diffusion de cette publication a eu lieu dans les six mois précédant les opérations électorales. Dans ces conditions, et alors même qu'il n'y est pas fait référence de manière explicite aux futures élections et à la candidature du maire, la diffusion de cette édition spéciale du bulletin municipal a constitué une campagne de promotion publicitaire contraire au second alinéa de l'article L. 52-1 du code électoral. <br/>
<br/>
              4. D'autre part, il résulte également de l'instruction que si la cérémonie de vœux organisée le 25 janvier 2020 s'est tenue dans les mêmes conditions que les années précédentes, le maire et son premier adjoint ont toutefois présenté, à cette occasion, un bilan particulièrement élogieux de l'action de l'équipe municipale sortante, évoqué les grands projets à venir en cas de réélection, tenu des propos polémiques sur la précédente mandature et souhaité expressément la réélection du maire sortant. Ces propos du maire et de son premier adjoint, eu égard à leur teneur et leur tonalité, doivent être regardés comme constitutifs d'une campagne de promotion publicitaire des réalisations de la commune en faveur de la candidature de M. H..., réalisée aux moyens des ressources de la commune.<br/>
<br/>
              5. Eu égard à l'écart de deux voix seulement séparant le nombre de suffrages recueillis par la liste proclamée victorieuse et la majorité absolue des suffrages exprimés, les irrégularités mentionnées aux points précédents sont de nature à avoir faussé les résultats du scrutin. <br/>
<br/>
              6. Il résulte de ce qui précède que M. H... n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de la Guadeloupe s'est fondé sur le moyen tiré de la méconnaissance des dispositions du second alinéa de l'article L. 52-1 du code électoral. pour annuler les opérations électorales qui se sont déroulées dans la commune du Lamentin le 15 mars 2020. <br/>
<br/>
              7. En second lieu, aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. (...) ". Si ces dispositions interdisent à toute personne morale autre qu'un parti politique de consentir des dons ou des avantages divers à un candidat, ni ces dispositions ni aucune autre disposition législative n'impliquent le rejet du compte de campagne au seul motif que le candidat a bénéficié d'un avantage au sens de ces dispositions. Il y a lieu d'apprécier, compte tenu de l'ensemble des circonstances de l'espèce, notamment de la nature de l'avantage, de son montant et des conditions dans lesquelles il a été consenti, si le bénéfice de cet avantage doit entraîner le rejet du compte.<br/>
<br/>
              8. Il résulte de l'instruction que la prise en charge du coût de conception, de publication et de diffusion des huit mille exemplaires du bulletin municipal a été assurée par une entreprise spécialisée en communication, en contrepartie d'emplacements publicitaires réservés dans ce numéro qu'elle se chargeait de commercialiser. Eu égard à la valeur d'un tel avantage, M. H... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif a réintégré une fraction du coût estimé de conception, impression et distribution de ce bulletin au titre de ses dépenses électorales, en a réintégré le montant dans son compte de campagne et, par voie de conséquence l'a rejeté et fixé à zéro euro le montant du remboursement forfaitaire en application des dispositions de l'article L. 52-11-1 du code électoral. <br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par Mme E..., que la requête de M. H... doit être rejetée. <br/>
<br/>
              En ce qui concerne la requête de Mme E... :<br/>
<br/>
              10. D'une part, aux termes de l'article L. 250-1 du code électoral : " Le tribunal administratif peut, en cas d'annulation d'une élection pour manœuvres dans l'établissement de la liste électorale ou irrégularité dans le déroulement du scrutin, décider, nonobstant appel, la suspension du mandat de celui ou de ceux dont l'élection a été annulée (...) ".<br/>
<br/>
              11. Le Conseil d'Etat statuant définitivement, par la présente décision, sur l'annulation de l'élection de M. H..., les conclusions de Mme E... tendant à la suspension du mandat de celui-ci sont devenues sans objet. Il n'y a plus lieu d'y statuer.<br/>
<br/>
              12. D'autre part, aux termes de l'article L. 118-3 du code électoral dans sa version issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral  : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible / : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / 2° Le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales ; / 3° Le candidat dont le compte de campagne a été rejeté à bon droit (...) ".  Il résulte de ces dispositions qu'en dehors des cas de fraude, le juge de l'élection ne prononce l'inéligibilité d'un candidat que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Pour déterminer si un manquement est d'une particulière gravité au sens de ces dispositions, il incombe au juge de l'élection d'apprécier, d'une part, s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales, d'autre part, s'il présente un caractère délibéré. En cas de manquement aux dispositions de l'article L. 52-8 du code électoral, il incombe, en outre, au juge de tenir compte de l'importance de l'avantage ou du don irrégulièrement consenti et de rechercher si, compte tenu de l'ensemble des circonstances de l'espèce, il a été susceptible de porter atteinte, de manière sensible, à l'égalité des candidats.<br/>
<br/>
              13. Il ne résulte pas de l'instruction, eu égard notamment à la date de parution de la publication litigieuse au début de la période de six mois prévue par l'article L. 52-1 du code électoral, à l'importance relative de l'avantage dont a bénéficié M. H... et à la circonstance que cet avantage n'a pas été de nature à porter atteinte, de manière prononcée, à l'égalité entre les candidats, que le manquement commis par le maire sortant ait revêtu, dans les circonstances de l'espèce, le caractère d'une particulière gravité requis par l'article L. 118-3 du code électoral. Par suite, Mme E... n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Guadeloupe a rejeté ses conclusions tendant à déclarer inéligible M. H.... <br/>
<br/>
              14. Il résulte de tout ce qui précède que la requête de Mme E... doit être rejetée. <br/>
<br/>
              15. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. H... et Mme E..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. H... et la requête de Mme E... sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. D... H..., à Mme G... E..., à M. A... F..., au ministre des outre-mer et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
              Délibéré à l'issue de la séance du 20 décembre 2021 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; Mme Suzanne von Coester, conseillère d'Etat et M. Bruno Bachini, maître des requêtes-rapporteur. <br/>
<br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
 		Le rapporteur : <br/>
      Signé : M. Bruno Bachini<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
