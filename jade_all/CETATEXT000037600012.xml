<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037600012</ID>
<ANCIEN_ID>JG_L_2018_11_000000417060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/00/CETATEXT000037600012.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 07/11/2018, 417060, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. B. Dacosta</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2018:417060.20181107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 janvier et 6 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat de vérifier qu'aucune technique de renseignement n'a été mise en oeuvre à son égard. <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 2 février et 16 mars 2018 et communiqués selon les modalités prévues par l'article R. 773-20 du code de justice administrative, le Premier ministre demande au Conseil d'Etat de constater qu'aucune illégalité n'a été commise sans confirmer ni infirmer la mise en oeuvre d'une technique de renseignement. <br/>
<br/>
              La Commission nationale de contrôle des techniques de renseignement a produit des observations, enregistrées le 9 mars 2018 et communiquées selon les modalités prévues à l'article R. 773-20 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 2015-912 du 24 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, M. B...A..., et d'autre part, le Premier ministre et la Commission nationale de contrôle des techniques de renseignement, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de M. Bertrand Dacosta, conseiller d'Etat,  <br/>
<br/>
              - et, hors la présence des parties, les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 821-1 du code de la sécurité intérieure : " La mise en oeuvre sur le territoire national des techniques de recueil de renseignement mentionnées au titre V du présent livre est soumise à autorisation préalable du Premier ministre, délivrée après avis de la Commission nationale de contrôle des techniques de renseignement " ; aux termes de l'article L. 833-1 du même code : " La Commission nationale de contrôle des techniques de renseignement veille à ce que les techniques de recueil de renseignement soient mises en oeuvre sur le territoire national conformément au présent livre ". L'article L. 833-4 du même code précise que " De sa propre initiative ou lorsqu'elle est saisie d'une réclamation de toute personne souhaitant vérifier qu'aucune technique de renseignement n'est irrégulièrement mise en oeuvre à son égard, la commission procède au contrôle de la ou des techniques invoquées en vue de vérifier qu'elles ont été ou sont mises en oeuvre dans le respect du présent livre. Elle notifie à l'auteur de la réclamation qu'il a été procédé aux vérifications nécessaires, sans confirmer ni infirmer leur mise en oeuvre ".<br/>
<br/>
              2. L'article L. 841-1 du code de la sécurité intérieure dispose : " Sous réserve des dispositions particulières prévues à l'article L. 854-9 du présent code, le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre des techniques de renseignement mentionnées au titre V du présent livre. / Il peut être saisi par : / 1° Toute personne souhaitant vérifier qu'aucune technique de renseignement n'est irrégulièrement mise en oeuvre à son égard et justifiant de la mise en oeuvre préalable de la procédure prévue à l'article L. 833-4 ; / 2° La Commission nationale de contrôle des techniques de renseignement, dans les conditions prévues à l'article L. 833-8. / Lorsqu'une juridiction administrative ou une autorité judiciaire est saisie d'une procédure ou d'un litige dont la solution dépend de l'examen de la régularité d'une ou de plusieurs techniques de recueil de renseignement, elle peut, d'office ou sur demande de l'une des parties, saisir le Conseil d'Etat à titre préjudiciel. Il statue dans le délai d'un mois à compter de sa saisine ". Ces dispositions s'appliquent aux techniques de renseignement mises en oeuvre à compter de la date de leur entrée en vigueur, y compris celles qui, initiées avant cette date, ont continué à être mises en oeuvre après.<br/>
<br/>
              3. Aux termes de l'article L. 773-1 du code de justice administrative : " Le Conseil d'Etat examine les requêtes présentées sur le fondement des articles L. 841-1 et L. 841-2 du code de la sécurité intérieure conformément aux règles générales du présent code, sous réserve des dispositions particulières du présent chapitre " ; aux termes de l'article L. 773-2 du même code : " Sous réserve de l'inscription à un rôle de l'assemblée du contentieux ou de la section du contentieux qui siègent alors dans une formation restreinte, les affaires relevant du présent chapitre sont portées devant une formation spécialisée (...). Dans le cadre de l'instruction de la requête, les membres de la formation de jugement et le rapporteur public sont autorisés à connaître de l'ensemble des pièces en possession de la Commission nationale de contrôle des techniques de renseignement ou des services mentionnés à l'article L. 811-2 du code de la sécurité intérieure et ceux désignés par le décret en Conseil d'Etat mentionné à l'article L. 811-4 du même code et utiles à l'exercice de leur office, y compris celles protégées au titre de l'article 413-9 du code pénal ". Aux termes de l'article L. 773-3 du même code : " Les exigences de la contradiction mentionnées à l'article L. 5 du présent code sont adaptées à celles du secret de la défense nationale (...) /. La formation chargée de l'instruction entend les parties séparément lorsqu'est en cause le secret de la défense nationale ". Aux termes de l'article L. 773-4 du même code : " Le président de la formation de jugement ordonne le huis-clos lorsque est en cause le secret de la défense nationale ". Aux termes de l'article L. 773-6 du même code : " Lorsque la formation de jugement constate l'absence d'illégalité dans la mise en oeuvre d'une technique de recueil de renseignement, la décision indique au requérant ou à la juridiction de renvoi qu'aucune illégalité n'a été commise, sans confirmer ni infirmer la mise en oeuvre d'une technique ". Aux termes de l'article L. 773-7 : " Lorsque la formation de jugement constate qu'une technique de recueil de renseignement est ou a été mise en oeuvre illégalement ou qu'un renseignement a été conservé illégalement, elle peut annuler l'autorisation et ordonner la destruction des renseignements irrégulièrement collectés. / Sans faire état d'aucun élément protégé par le secret de la défense nationale, elle informe la personne concernée ou la juridiction de renvoi qu'une illégalité a été commise. Saisie de conclusions en ce sens lors d'une requête concernant la mise en oeuvre d'une technique de renseignement ou ultérieurement, elle peut condamner l'Etat à indemniser le préjudice subi (...) ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. / Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. / Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              4. Il ressort des pièces du dossier que M. A...a saisi la Commission nationale de contrôle des techniques de renseignement (CNCTR) le 28 juillet 2017 afin de vérifier qu'aucune technique de renseignement n'était irrégulièrement mise en oeuvre à son égard. Par lettre du 27 octobre 2017, le président de la Commission a informé M. A...qu'il avait été procédé à l'ensemble des vérifications requises et que la procédure était terminée, sans apporter à l'intéressé d'autres informations. M. A...demande au Conseil d'Etat de vérifier si des techniques de renseignement ont été mises en oeuvre pour le surveiller. <br/>
<br/>
              5. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions tendant à ce qu'elle s'assure qu'aucune technique de renseignement n'est irrégulièrement mise en oeuvre à l'égard du requérant, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant fait ou non l'objet d'une telle technique. Lorsqu'il apparaît soit qu'aucune technique de renseignement n'est mise en oeuvre à l'égard du requérant, soit que cette mise en oeuvre n'est entachée d'aucune illégalité, la formation de jugement informe le requérant de l'accomplissement de ces vérifications, sans indiquer si une technique de recueil de renseignement a été mise en oeuvre à son égard. Dans le cas où une technique de renseignement est mise en oeuvre dans des conditions entachées d'illégalité, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut, par ailleurs, annuler l'autorisation et ordonner la destruction des renseignements irrégulièrement collectés.<br/>
<br/>
              6. La formation spécialisée a examiné, selon les modalités décrites au point précédent, les éléments fournis par la Commission nationale de contrôle des techniques de renseignement, qui a précisé l'ensemble des vérifications auxquelles elle avait procédé, et par le Premier ministre. A l'issue de l'examen auquel s'est livré la formation spécialisée, il y a lieu de répondre à M. A...que la vérification qu'il a sollicitée a été effectuée et que, n'ayant révélé aucune illégalité, elle n'appelle aucune mesure de la part du Conseil d'Etat. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il a été procédé à la vérification demandée par M.A....<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre et à la Commission nationale de contrôle des techniques de renseignement. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
