<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026738945</ID>
<ANCIEN_ID>JG_L_2012_12_000000348881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/73/89/CETATEXT000026738945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 07/12/2012, 348881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:348881.20121207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 mai et 2 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Tinel, dont le siège est 10 rue de Chomaget (43100), représentée par son gérant en exercice, domicilié en cette qualité audit siège : la SA Tinel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09LY02702 du 22 février 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa demande tendant, d'une part, à l'annulation du jugement n° 0800947 du 22 septembre 2009 par lequel le tribunal administratif de Clermont-Ferrand a rejeté sa demande tendant à la restitution de la taxe sur les achats de viande qu'elle a acquittée au titre de la période allant du 1er janvier 2001 au 31 octobre 2003, d'autre part, à prononcer la décharge des impositions ainsi mises à sa charge et d'en ordonner la restitution, assortie des intérêts moratoires, ou à défaut d'interroger la Cour de justice des communautés européennes sur l'interprétation de l'article 87 du traité instituant la Communauté européenne ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le code général des impôts et de le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de la SA Tinel,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de la SA Tinel ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SA Tinel a sollicité la restitution des droits de taxe sur les achats de viande qu'elle avait acquittés au titre de la période allant du 1er janvier 2001 au 31 octobre 2003 ; que l'administration fiscale a prononcé le dégrèvement de ces droits par une décision du 9 septembre 2004 ; que l'administration fiscale a en conséquence restitué les sommes perçues, augmentées des intérêts moratoires ; que toutefois, l'administration a informé la contribuable par une lettre du 15 novembre 2004, qu'elle entendait revenir sur la décision de dégrèvement et sur les remboursements indus afin de réparer une erreur commise dans la liquidation des restitutions ; qu'elle a alors adressé à cette société une proposition de rectification en date du 20 décembre 2004 ; que les droits de taxe sur les achats de viande au titre des années 2001, 2002 et 2003, augmentés des intérêts moratoires restitués par l'administration ont été mis en recouvrement par deux avis de mise en recouvrement des 17 octobre et 21 novembre 2007 ; que la SA Tinel se pourvoit en cassation contre l'arrêt du 22 février 2011 de la cour administrative d'appel de Lyon par lequel la cour a rejeté sa requête ;<br/>
<br/>
              Sur la prescription :<br/>
<br/>
              2. Considérant, d'une part, que l'article 302 bis ZD du code général des impôts applicable à la période en litige prévoit que la taxe sur les achats de viande est constatée, recouvrée et contrôlée selon les mêmes procédures que la taxe sur la valeur ajoutée ; que l'article L. 176 du livre des procédures fiscales dispose que : " pour les taxe sur le chiffre d'affaires, le droit de reprise de l'administration s'exerce jusqu'à la fin de la troisième année suivant celle au cours de laquelle la taxe est devenue exigible [...] " ; que cette disposition s'applique également dans le cas où des droits de taxe sur les achats de viande ont été dégrevés à tort ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 189 du livre des procédures fiscales : " La prescription est interrompue par la notification d'une proposition de rectification, par la déclaration ou la notification d'un procès-verbal, de même que par tout acte comportant reconnaissance de la part des contribuables et par tous les autres actes interruptifs de droit commun " ; qu'ainsi, en jugeant que le délai de reprise avait été interrompu par la notification de la proposition de rectification le 20 décembre 2004, alors même que l'administration n'était pas tenue d'y recourir, de sorte qu'elle pouvait établir une nouvelle imposition pour une période identique, la cour, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur l'application de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              4. Considérant que la cour, en jugeant que les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne faisaient pas obstacle à ce qu'après avoir prononcé un dégrèvement et restitué au contribuable les sommes qu'il avait versées, l'administration rétablisse l'imposition qui était due et réclame le reversement des sommes concernées dans le délai de reprise, n'a pas entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              Sur la qualification d'aides d'Etat :<br/>
<br/>
              5. Considérant qu'après avoir rappelé qu'il n'existait, à compter du 1er janvier 2001, aucun lien d'affectation contraignant entre la taxe sur les achats de viande et le service public de l'équarrissage ni aucun rapport entre le produit de la taxe et le montant du financement public attribué à ce service, la cour a pu, sans commettre d'erreur de droit, juger que la taxe sur les achats de viande n'entrait plus, à compter du 1er janvier 2001, dans le champ d'application des stipulations des articles 87 et 88 du traité instituant la Communauté européenne concernant les aides d'Etat et qu'elle ne constituait pas une aide d'Etat ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit ci-dessus que la SA Tinel n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes demandées par la société requérante au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SA Tinel est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SA Tinel et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
