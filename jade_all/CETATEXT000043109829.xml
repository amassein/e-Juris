<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043109829</ID>
<ANCIEN_ID>JG_L_2021_02_000000438349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/98/CETATEXT000043109829.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 08/02/2021, 438349, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; HAAS</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438349.20210208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Montpellier d'ordonner, avant dire droit, une expertise médicale aux fins d'évaluer les préjudices résultant de l'accident dont elle a été victime le 5 novembre 2013 et de condamner le département de l'Hérault à lui verser une indemnité provisionnelle de 10 000 euros à valoir sur la réparation de ses préjudices.<br/>
<br/>
              Par un jugement n° 1705046 du 28 novembre 2019, le tribunal administratif de Montpellier a condamné le département de l'Hérault à lui verser une somme de 2 674,42 euros et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n° 20MA00300 du 6 février 2020, enregistrée au secrétariat du contentieux du Conseil d'Etat le 6 février 2020, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 24 janvier 2020, au greffe de cette cour, présentée par Mme A... B....<br/>
<br/>
              Par cette requête et un nouveau mémoire, enregistré le 20 avril 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat : <br/>
<br/>
              1°) à titre principal de renvoyer le jugement de l'affaire à la cour administrative d'appel de Marseille ; <br/>
<br/>
              2°) à titre subsidiaire, d'annuler le jugement attaqué ; <br/>
<br/>
              3°) de mettre à la charge du département de l'Hérault la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de Mme B... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :  <br/>
<br/>
              1. Aux termes de l'article R. 222-13 du code de justice administrative : " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin (...) statue en audience publique et après audition du rapporteur public : (...) 7° Sur les actions indemnitaires, lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 ". Ce montant est fixé à 10 000 euros par l'article R. 222-14. Il résulte du deuxième alinéa de l'article R. 811-1 que le tribunal administratif statue en premier et dernier ressort sur les litiges mentionnés, notamment, au 7° de l'article R. 222-13.<br/>
<br/>
              2. Les actions indemnitaires dont les conclusions n'ont pas donné lieu à une évaluation chiffrée dans la requête introductive d'instance devant le tribunal administratif et ne peuvent ainsi être regardées comme tendant au versement d'une somme supérieure à 10 000 euros entrent dans le champ des dispositions du 7° de l'article R. 222-13 et du deuxième alinéa de l'article R. 811-1 du code de justice administrative. Ces dispositions ne sauraient, toutefois, trouver application quand le requérant, dans sa requête introductive d'instance, a expressément demandé qu'une expertise soit ordonnée afin de déterminer l'étendue de son préjudice ou a expressément mentionné une demande d'expertise présentée par ailleurs, en se réservant de fixer le montant de sa demande au vu du rapport de l'expert. Le jugement rendu sur une telle requête, qui doit l'être par une formation collégiale, est susceptible d'appel quel que soit le montant de la provision que le demandeur a, le cas échéant, sollicitée dans sa requête introductive d'instance comme celui de l'indemnité qu'il a chiffrée à l'issue de l'expertise, si celle-ci a été ordonnée.<br/>
<br/>
              3. Mme B... a demandé au tribunal administratif de Montpellier de condamner le département de l'Hérault à lui verser une " indemnité provisionnelle " de 10 000 euros à valoir sur la réparation des préjudices qui ont résulté de l'accident dont elle a été victime le 5 novembre 2013 ainsi que d'ordonner une expertise. Par un jugement en date du 28 novembre 2019, le tribunal administratif a limité l'évaluation de ses préjudices à la somme de 5 348,84 &#128; et rejeté la demande d'expertise.  <br/>
<br/>
              4. Ainsi qu'il a été dit, les conclusions présentées au tribunal administratif doivent être regardées comme tendant au versement d'une somme excédant le montant de 10 000 euros prévu à l'article R. 222-14 du code de justice administrative. La circonstance que dans sa requête d'appel Mme B... ait demandé en vue d'obtenir " la réparation intégrale " de son préjudice une somme inférieure à ce montant est sans incidence sur l'application des dispositions mentionnées ci-dessus. Ainsi le litige n'était pas au nombre de ceux sur lesquels le tribunal administratif statue en premier et dernier ressort. Il suit de là que la requête de Mme B... tendant à l'annulation du jugement du tribunal administratif de Montpellier doit être regardée comme un appel et ressortit à la compétence de la cour administrative d'appel de Marseille. Il y a lieu, dès lors, d'attribuer le jugement de la requête à cette juridiction.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de Mme B... est attribué à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A... B..., au département de l'Hérault et à la présidente de la cour administrative d'appel de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
