<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600537</ID>
<ANCIEN_ID>JG_L_2017_05_000000399164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 05/05/2017, 399164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:399164.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 26 avril 2015 et 18 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, l'Association des ingénieurs territoriaux de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir :<br/>
              - le décret n° 2016-200 du 26 février 2016 portant statut particulier du cadre d'emplois des ingénieurs en chef territoriaux ;<br/>
              - le décret n° 2016-202 du 26 février 2016 portant échelonnement indiciaire applicable aux ingénieurs en chef territoriaux ;<br/>
              - le décret n° 2016-204 du 26 février 2016 relatif à l'organisation de la formation initiale des élèves ingénieurs en chef territoriaux ;<br/>
              - le décret n° 2016-205 du 26 février 2016 fixant les conditions d'accès et les modalités d'organisation des concours pour le recrutement des ingénieurs en chef territoriaux ;<br/>
              - le décret n° 2016-208 du 26 février 2016 fixant les modalités d'organisation de l'examen professionnel d'accès au cadre d'emplois des ingénieurs en chef territoriaux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 4 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les fonctionnaires territoriaux appartiennent à des cadres d'emplois régis par des statuts particuliers, communs aux fonctionnaires des communes, des départements, des régions et de leurs établissements publics. / Ces statuts particuliers ont un caractère national. / Un cadre d'emplois regroupe les fonctionnaires soumis au même statut particulier, titulaires d'un grade leur donnant vocation à occuper un ensemble d'emplois. Chaque titulaire d'un grade a vocation à occuper certains des emplois correspondant à ce grade. / Le cadre d'emplois peut regrouper plusieurs grades. / Les grades sont organisés en grade initial et en grades d'avancement. / Les fonctionnaires territoriaux sont gérés par la collectivité ou l'établissement dont ils relèvent ; leur nomination est faite par l'autorité territoriale ". Aux termes de l'article 6 de la même loi, dans sa rédaction en vigueur à la date des décrets attaqués : " Les statuts particuliers sont établis par décret en Conseil d'Etat. Ils précisent notamment le classement de chaque cadre d'emplois, emploi ou corps, dans l'une des trois catégories mentionnées au premier alinéa de l'article 13 de la loi n° 83-634 du 13 juillet 1983 précitée. / L'échelonnement indiciaire applicable aux cadres d'emplois et emplois de la fonction publique territoriale est fixé par décret ".<br/>
<br/>
              2. Par le décret n° 2016-200 du 26 février 2016, le Premier ministre a créé le nouveau cadre d'emplois des ingénieurs en chef territoriaux, initialement constitué des membres du cadre d'emplois des ingénieurs territoriaux titulaires de la classe normale et de la classe exceptionnelle du grade d'ingénieur en chef. Le décret n° 2016-202 du 26 février 2016 fixe l'échelonnement indiciaire applicable aux ingénieurs en chef territoriaux. Le décret n° 2016-204 du 26 février 2016 organise la formation initiale des élèves ingénieurs en chef territoriaux. Le décret n° 2016-205 du 26 février 2016 fixe les conditions d'accès et les modalités d'organisation des concours pour le recrutement des ingénieurs en chef territoriaux. Le décret n° 2016-208 du 26 février 2016 fixe les modalités d'organisation de l'examen professionnel pour l'accès au cadre d'emplois des ingénieurs en chef territoriaux. L'Association des ingénieurs territoriaux de France demande l'annulation pour excès de pouvoir de ces décrets. <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". S'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui sont compétents pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte. Les décrets attaqués ont été contresignés par le ministre de la fonction publique, le ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales et le ministre de l'intérieur. Les décrets n° 2016-200 et n° 2016-202 ont en outre été contresignés par le ministre des finances et des comptes publics et le secrétaire d'Etat chargé du budget. Il ne ressort pas des pièces du dossier que d'autres ministres auraient été appelés à signer ou contresigner les mesures réglementaires ou individuelles que l'exécution des décrets attaqués comportait nécessairement. Dès lors, le moyen tiré du défaut de contreseing de ceux-ci doit être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              4. Si l'Association des ingénieurs territoriaux de France conclut à l'annulation de l'ensemble des dispositions des décrets attaqués, les moyens qu'elle soulève ne sont assortis de précisions permettant d'en apprécier le bien-fondé qu'en ce qui concerne les articles 7 et 21 du décret n° 2016-200 du 26 février 2016. Les conclusions de sa requête, en tant qu'elles sont dirigées contre les autres dispositions des décrets attaqués, ne peuvent par suite qu'être rejetées.<br/>
<br/>
              En ce qui concerne l'article 7 du décret n° 2016-200 du 26 février 2016 :<br/>
<br/>
              5. Aux termes de l'article 39 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " En vue de favoriser la promotion interne, les statuts particuliers fixent une proportion de postes susceptibles d'être proposés au personnel appartenant déjà à l'administration ou à une organisation internationale intergouvernementale, non seulement par voie de concours, selon les modalités définies au 2° de l'article 36, mais aussi par la nomination de fonctionnaires ou de fonctionnaires internationaux, suivant l'une des modalités ci-après : / 1° Inscription sur une liste d'aptitude après examen professionnel (...) ".<br/>
<br/>
              6. Aux termes de l'article 4 du décret n° 2016-200 : " Le recrutement en qualité d'ingénieur en chef intervient après inscription sur les listes d'aptitude établies : / 1° En application des dispositions de l'article 36 de la loi du 26 janvier 1984 précitée ; / 2° En application des dispositions du 1° de l'article 39 de ladite loi ". Aux termes de l'article 7 du décret n° 2016-200 : " I. - Peuvent être inscrits sur la liste d'aptitude prévue au 2° de l'article 4, après examen professionnel : / 1° Les membres du cadre d'emplois des ingénieurs territoriaux comptant quatre ans de services effectifs dans un grade d'avancement. Sont également pris en compte les services accomplis par ces fonctionnaires, détachés dans un ou plusieurs des emplois énumérés au 2° ci-dessous ; / 2° Les membres du cadre d'emplois des ingénieurs territoriaux comptant au moins six ans de services effectifs en position de détachement dans un ou plusieurs des emplois fonctionnels suivants (...) / II. - (...) / Le nombre de postes ouverts chaque année en application du précédent alinéa est fixé par le président du Centre national de la fonction publique territoriale, sans pouvoir excéder une proportion de 70 % du nombre de candidats admis à l'ensemble des concours mentionnés à l'article 5. Si le nombre ainsi calculé n'est pas un entier, il est arrondi à l'entier supérieur. (...) ".<br/>
<br/>
              7. D'une part, les fonctionnaires sont dans une situation statutaire et réglementaire et n'ont aucun droit au maintien de la réglementation applicable à leur emploi. Si l'article 7 du décret n° 2016-200 a subordonné à un examen professionnel l'intégration des ingénieurs territoriaux dans le nouveau cadre d'emplois des ingénieurs en chef territoriaux, cette condition nouvelle est exempte de toute méconnaissance du principe d'égalité de traitement entre les agents, lequel ne s'applique en matière statutaire qu'entre agents d'un même corps ou cadre d'emplois. En fixant le nombre de postes offerts chaque année par la voie de l'examen professionnel à 70 % au plus du nombre de candidats admis à l'ensemble des concours de recrutement en qualité d'ingénieur en chef, la disposition critiquée n'a pas méconnu la portée de l'habilitation donnée au pouvoir réglementaire.<br/>
<br/>
              8. D'autre part, lorsqu'il s'agit d'organiser au profit des agents d'un corps ou d'un cadre d'emplois déterminé l'accès par la voie de la promotion interne à un corps ou un cadre d'emplois de niveau hiérarchique supérieur, il peut être légalement dérogé au principe d'égalité de traitement applicable aux agents d'un même corps ou cadre d'emplois, lorsque l'intérêt du service dans le corps ou le cadre d'emplois de niveau hiérarchique supérieur l'exige et dès lors qu'il existe une différence de situation entre les agents. Compte tenu de la nature des fonctions et des emplois que sont appelés à occuper les membres du cadre d'emploi des ingénieurs en chef territoriaux, le pouvoir réglementaire n'a pas fait une appréciation erronée de l'intérêt du service en prévoyant des conditions d'ancienneté plus favorables pour les agents titulaires d'un grade d'avancement et en prévoyant, pour ceux des agents qui ne sont pas titulaires d'un tel grade, des conditions spécifiques tenant au type d'emploi occupé. Dès lors, le moyen tiré de ce que la disposition contestée méconnaîtrait le droit des fonctionnaires à bénéficier d'un déroulement de carrière et porterait atteinte au principe d'égalité de traitement entre agents d'un même cadre d'emplois doit être écarté.<br/>
<br/>
              En ce qui concerne l'article 21 du décret n° 2016-200 du 26 février 2016 :<br/>
<br/>
              9. L'article 21 du décret n° 2016-201 prévoit que, pour qu'ils soient nommés au grade supérieur d'ingénieur en chef hors classe, les ingénieurs en chef territoriaux doivent avoir occupé pendant au moins deux ans, au titre d'une période de mobilité au sein de la fonction publique d'Etat, de la fonction publique hospitalière ou dans une collectivité ou un établissement autre que celle ou celui qui a procédé à leur recrutement dans le cadre d'emplois des ingénieurs en chef territoriaux, un emploi correspondant au grade d'ingénieur en chef ou certains emplois de direction ou comportant des responsabilités d'encadrement, de direction de services, de conseil ou d'expertise ou de conduite de projet.<br/>
<br/>
              10. Aux termes de l'article 77 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction en vigueur à la date des décrets attaqués : " L'avancement des fonctionnaires comprend l'avancement d'échelon et l'avancement de grade ". Aux termes de l'article 79 de la même loi : " L'avancement de grade a lieu de façon continue d'un grade au grade immédiatement supérieur. Il peut être dérogé à cette règle dans les cas où l'avancement est subordonné à une sélection professionnelle. / Il a lieu suivant l'une ou plusieurs des modalités ci-après :/ 1° Soit au choix par voie d'inscription à un tableau annuel d'avancement, établi après avis de la commission administrative paritaire, par appréciation de la valeur professionnelle et des acquis de l'expérience professionnelle des agents (...) Pour les fonctionnaires relevant des cadres d'emplois de catégorie A, il peut également être subordonné à l'occupation préalable de certains emplois ou à l'exercice préalable de certaines fonctions correspondant à un niveau particulièrement élevé de responsabilité et définis par un décret en Conseil d'Etat (...) ".<br/>
<br/>
              11. D'une part, les dispositions contestées relatives à l'avancement des fonctionnaires du cadre d'emplois des ingénieurs en chef territoriaux, pour l'accès au grade d'ingénieur en chef hors classe, sont applicables également à tous les agents de ce cadre d'emplois. D'autre part, aucune disposition législative ne fait obstacle à ce que le pouvoir réglementaire édicte une condition prévoyant l'accomplissement d'une période de mobilité pour accéder au grade supérieur d'ingénieur en chef hors classe, eu égard aux emplois d'un niveau élevé de responsabilité que les agents titulaires de ce grade sont appelés à occuper. Dès lors, le moyen tiré de ce que la disposition critiquée serait illégale, au motif qu'elle méconnaitrait le principe d'égalité et ne permettrait pas de tenir compte de la valeur professionnelle et des acquis de l'expérience professionnelle des agents doit être écarté.<br/>
<br/>
              12. Il résulte de ce qui précède que l'Association des ingénieurs territoriaux de France n'est pas fondée à demander l'annulation des décrets n° 2016-200, n° 2016-202, n° 2016-204, n° 2016-205 et n° 2016-208.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme qui est demandée à ce titre par l'association requérante soit mise à la charge de l'État qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association des ingénieurs territoriaux de France est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Association des ingénieurs territoriaux de France, au Premier ministre, à la ministre de la fonction publique, au ministre de l'économie et des finances, au ministre de l'intérieur et au ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
