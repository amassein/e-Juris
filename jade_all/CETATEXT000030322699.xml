<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030322699</ID>
<ANCIEN_ID>JG_L_2015_03_000000368489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/32/26/CETATEXT000030322699.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/03/2015, 368489, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:368489.20150306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 14 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 12BX02043 du 7 mars 2013 par lequel la cour administrative d'appel de Bordeaux a annulé le jugement n° 1200171 du tribunal administratif de Toulouse du 29 juin 2012 ordonnant, à la demande du préfet de la Haute-Garonne, que l'immeuble situé 70, allée des Demoiselles à Toulouse soit libéré de tout occupant dans un délai d'un mois ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu l'ordonnance n° 2006-460 du 21 avril 2006 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond qu'un ensemble immobilier constitué de deux immeubles situés 70 allée des Demoiselles et 4 bis rue Goudouli à Toulouse, dont l'Etat est propriétaire, a été loué à compter du 1er janvier 1990 à l'Association nationale pour la formation professionnelle des adultes (AFPA) en vertu de baux successifs, dont le dernier a été conclu le 6 avril 2004 ; que, le 22 avril 2011, un collectif d'aide aux personnes mal logées est entré par effraction dans l'immeuble dont l'entrée est située 70 allée des Demoiselles, qui a été occupé à compter de cette date par plusieurs familles ; que le directeur de l'AFPA ayant sollicité la résiliation anticipée du bail, cette résiliation est intervenue le 18 octobre 2011 ; qu'à la demande du préfet de la Haute-Garonne, le tribunal administratif de Toulouse a ordonné, par un jugement du 29 juin 2012, l'expulsion de tout occupant sans droit ni titre de cet immeuble ; que, sur appel d'un des membres du collectif mentionné ci-dessus, MmeB..., la cour administrative d'appel de Bordeaux a, par un arrêt du 7 mars 2013, annulé le jugement du 29 juin 2012 et rejeté la demande de première instance du préfet au motif qu'elle avait été portée devant un ordre de juridiction incompétent pour en connaître dès lors que l'immeuble en cause ne constituait pas une dépendance du domaine public de l'Etat ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2111-1 du code général de la propriété des personnes publiques, dont la partie législative est issue de l'ordonnance du 21 avril 2006 : " Sous réserve de dispositions législatives spéciales, le domaine public d'une personne publique mentionnée à l'article L. 1 est constitué des biens lui appartenant qui sont soit affectés à l'usage direct du public, soit affectés à un service public pourvu qu'en ce cas ils fassent l'objet d'un aménagement indispensable à l'exécution des missions de ce service public " ; qu'aux termes de l'article L. 2141-1 du même code : " Un bien d'une personne publique mentionnée à l'article L. 1, qui n'est plus affecté à un service public ou à l'usage direct du public, ne fait plus partie du domaine public à compter de l'intervention de l'acte administratif constatant son déclassement " ; qu'aux termes de l'article 13 de l'ordonnance du 21 avril 2006 : " Les dispositions de la présente ordonnance sont applicables à compter du 1er juillet 2006 " ;  <br/>
<br/>
              3. Considérant qu'avant l'entrée en vigueur de la partie législative du code général de la propriété des personnes publiques, intervenue, en vertu des dispositions précitées, le 1er juillet 2006, l'appartenance d'un bien au domaine public était, sauf si ce bien était directement affecté à l'usage du public, subordonnée à la double condition qu'il ait été affecté à un service public et spécialement aménagé en vue du service public auquel il était destiné ; qu'en l'absence de toute disposition en ce sens, l'entrée en vigueur de ce code n'a pu, par elle-même, entraîner le déclassement de dépendances qui appartenaient antérieurement au domaine public et qui, depuis le 1er juillet 2006, ne rempliraient plus les conditions désormais fixées par son article L. 2111-1 ;<br/>
<br/>
              4. Considérant qu'après avoir cité les dispositions de l'article L. 2111-1 du code général de la propriété des personnes publiques, la cour administrative d'appel de Bordeaux a énoncé que, si l'immeuble situé 70 allée des Demoiselles avait été loué à compter de 1990 par l'AFPA pour les besoins du service public de l'emploi, il n'était pas établi qu'il aurait fait l'objet d'un aménagement particulier indispensable à l'exécution de cette mission et qu'il n'était plus affecté au service public du fait de la résiliation anticipée du bail ; qu'en déduisant de ces constatations que l'immeuble en cause ne constituait pas une dépendance du domaine public de l'Etat sans rechercher, d'une part, si ce bien affecté avant le 1er juillet 2006 à un service public avait fait l'objet d'un aménagement spécial, ce qui suffisait à le faire regarder  comme ayant été incorporé au domaine public avant cette date et, d'autre part, si un acte administratif intervenu postérieurement à cette date avait prononcé son déclassement, la cour a commis une erreur de droit ; que, dès lors, cet arrêt doit être annulé ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 7 mars 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
