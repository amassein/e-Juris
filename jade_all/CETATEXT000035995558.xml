<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035995558</ID>
<ANCIEN_ID>JG_L_2017_11_000000415178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/55/CETATEXT000035995558.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/11/2017, 415178</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:415178.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à la préfète du Cher de lui accorder une admission provisoire au séjour et de l'accompagner dans ses démarches nécessaires en vue de la saisine de l'Office français de protection des réfugiés et apatrides. Par une ordonnance n° 1703503 du 11 octobre 2017, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 23 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors qu'en premier lieu, il est réputé être " en fuite " depuis le 3 octobre 2017 et se trouve ainsi exclu de la structure associative qui l'accueillait, en deuxième lieu, il ne peut ni solliciter de récépissé, ni aucune aide publique durant les prochains dix-huit mois et, en dernier lieu, il est susceptible à tout moment d'être interpellé par les forces de l'ordre ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit constitutionnel d'asile, puisqu'il incombe aux autorités françaises d'enregistrer sa demande d'asile dès lors qu'est expiré le délai de six mois dont elles disposaient pour procéder à son  transfert vers l'Italie ;  <br/>
<br/>
              Par un mémoire en défense, enregistré le 30 octobre 2017, le ministre d'Etat, ministre de l'intérieur conclut au rejet de la requête. Il fait valoir que les moyens soulevés par le requérant ne sont pas fondés. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B...et, d'autre part, le ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience du mardi 31 octobre 2017 à 14 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Pinet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
<br/>
              - le représentant de M.B... ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a fixé la clôture de l'instruction au lundi 6 novembre 2017 à 13 h ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ;<br/>
<br/>
              2. Considérant qu'aux termes du paragraphe 1 de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride, le transfert du demandeur vers l'Etat membre responsable doit s'effectuer " dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation par un autre Etat membre de la requête aux fins de la prise en charge ou de reprise en charge de la personne concernée ou de la décision définitive sur le recours ou la révision lorsque l'effet suspensif est accordé conformément à l'article 27, paragraphe 3 " ; qu'aux termes du paragraphe 2 de l'article 29 : " Si le transfert n'est pas exécuté dans le délai de six mois, l'Etat membre responsable est libéré de son obligation de prendre en charge ou de reprendre en charge la personne concernée et la responsabilité est alors transférée à l'Etat membre requérant. Ce délai peut être porté à un an au maximum s'il n'a pu être procédé au transfert en raison d'un emprisonnement de la personne concernée ou à dix-huit mois au maximum si la personne concernée prend la fuite " ; <br/>
<br/>
              3. Considérant qu'aux termes du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'étranger qui a fait l'objet d'une décision de transfert mentionnée à l'article L. 742-3 peut, dans le délai de quinze jours à compter de la notification de cette décision, en demander l'annulation au président du tribunal administratif./ (...) Aucun autre recours ne peut être introduit contre la décision de transfert./ (...) Toutefois, si, en cours d'instance, l'étranger est placé en rétention en application de l'article L. 551-1 du présent code ou assigné à résidence en application de l'article L. 561-2, il est statué selon la procédure et dans le délai prévu au II du présent article " ; qu'aux termes du II de l'article L. 742-4 : " Lorsqu'une décision de placement en rétention prise en application de l'article L. 551-1 ou d'assignation à résidence prise en application de l'article L. 561-2 est notifiée avec la décision de transfert, l'étranger peut, dans les quarante-huit heures suivant leur notification, demander au président du tribunal administratif l'annulation de la décision de transfert et de la décision d'assignation à résidence./ Il est statué sur ce recours selon la procédure et dans le délai prévus au III de l'article L. 512-1./ Il est également statué selon la même procédure et dans le même délai sur le recours formé contre une décision de transfert par un étranger qui fait l'objet, en cours d'instance, d'une décision de placement en rétention ou d'assignation à résidence. Dans ce cas, le délai de soixante-douze heures pour statuer court à compter de la notification par l'administration au tribunal de la décision de placement en rétention ou d'assignation à résidence " ; qu'aux termes du second alinéa de l'article L. 742-5 : " La décision de transfert ne peut faire l'objet d'une exécution d'office ni avant l'expiration d'un délai de quinze jours ou, si une décision de placement en rétention prise en application de l'article L. 551-1 ou d'assignation à résidence prise en application de l'article L. 561-2 a été notifiée avec la décision de transfert, avant l'expiration d'un délai de quarante-huit heures, ni avant que le tribunal administratif ait statué, s'il a été saisi " ;<br/>
<br/>
              4. Considérant, enfin, qu'aux termes du I de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'autorité administrative peut prendre une décision d'assignation à résidence à l'égard de l'étranger qui ne peut quitter immédiatement le territoire français mais dont l'éloignement demeure une perspective raisonnable, lorsque cet étranger : 1° Doit être remis aux autorités compétentes d'un Etat membre de l'Union européenne en application des articles L. 531-1 ou L. 531-2 ou fait l'objet d'une décision de transfert en application de l'article L. 742-3 (...) " ; que le III de l'article L. 512-1 ouvre à l'étranger faisant l'objet d'une décision d'assignation à résidence prise en application de l'article L. 561-2 la possibilité de demander au président du tribunal administratif l'annulation de cette décision dans un délai de quarante-huit heures à compter de sa notification ;<br/>
<br/>
              5. Considérant qu'il résulte des dispositions citées aux points 2 à 4 que l'introduction d'un recours contre la décision de transfert, sur le fondement du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, par l'étranger qui n'a fait l'objet à ce stade ni d'une assignation à résidence, ni d'un placement en rétention administrative, doit être regardée comme interrompant le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 jusqu'à l'intervention du jugement du tribunal administratif ; qu'il en va de même de l'introduction d'un recours contre la décision de transfert sur le fondement du III de l'article L. 512-1, auquel renvoie le II de l'article L. 742-4, lorsqu'une décision de placement en rétention ou d'assignation à résidence est notifiée avec la décision de transfert ; <br/>
<br/>
              6. Considérant, en outre, qu'eu égard à l'objet d'une mesure d'assignation à résidence prise en application de l'article L. 561-2, qui est de permettre la mise à exécution de la décision de transfert, lorsque l'étranger auquel ont été notifiées une décision de transfert et une décision d'assignation à résidence, soit simultanément, soit successivement, ne conteste que la seconde, sur le fondement du III de l'article L. 512-1, ce recours, dès lors qu'il est introduit dans le délai de quarante-huit heures suivant la notification de la décision, fait obstacle à l'exécution de la décision de transfert jusqu'à l'intervention du jugement du tribunal administratif ; que si le tribunal administratif, saisi d'un moyen tiré de l'illégalité de la décision de transfert, l'estime recevable et fondé, ou s'il constate un changement de circonstances de droit ou de fait postérieur à l'intervention de la décision de transfert et imposant à l'administration de ne pas la mettre à exécution, il lui appartient d'en tirer les conséquences en suspendant les effets de la décision de transfert devenue, en l'état, inexécutable ; que, par suite, le recours formé, sur le fondement du III de l'article L. 512-1, contre la décision d'assignation à résidence de l'étranger faisant l'objet d'une décision de transfert doit être regardée comme interrompant également le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que M.B..., ressortissant nigérian né en 1993, a déclaré être entré sur le territoire français le 11 décembre 2016 ; qu'il s'est vu remettre, le 26 janvier 2017, une attestation de demande d'asile mentionnant que lui serait appliquée la procédure de détermination de l'Etat responsable de l'examen de sa demande, prévue par les articles L. 742-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile, dès lors que la consultation du fichier " Eurodac " avait permis d'établir que ses empreintes digitales avaient été relevées par les autorités italiennes  le 3 octobre 2016 ; qu'à la suite d'une demande de prise en charge adressée à l'Italie le 30 janvier 2017, un accord implicite est né, le 30 mars 2017, du silence gardé sur cette demande durant un délai de deux mois, faisant courir le délai de six mois dont disposaient les autorités françaises en application du paragraphe 1 de l'article 29 du règlement du 26 juin 2013 ; que, par deux arrêtés pris respectivement les 23 et 24 août 2017, et notifiés le 6 septembre, la préfète du Cher a décidé le transfert de M. B... aux autorités italiennes et l'a assigné à résidence dans le département du Cher ; que M. B...a contesté devant le tribunal administratif d'Orléans le seul arrêté décidant son assignation à résidence ; que sa demande a été rejetée par un jugement du 12 septembre 2017 ; qu'il a été informé, le 2 octobre 2017, qu'un vol à destination de Naples lui avait été réservé pour le 3 octobre 2017 à 9h25, et placé en rétention administrative dans l'attente de l'exécution de la décision de transfert ; que, M. B...s'étant opposé à ce transfert en refusant de quitter le centre de rétention administrative, l'administration lui a indiqué verbalement que le délai de six mois dont elle disposait pour procéder à son transfert avait été interrompu par son recours contre l'arrêté d'assignation à résidence et continuait donc à courir, qu'il devait être regardé comme étant " en fuite " au sens du paragraphe 2 de l'article 29 du règlement du 26 juin 2013 et que ce constat avait pour conséquence de porter le délai à dix-huit mois ; qu'à la même date du 3 octobre 2017, le ministère de l'intérieur a avisé les autorités italiennes qu'un recours avait été introduit au mois de septembre, interrompant le délai de six mois, et que M. B...avait pris la fuite ; que M. B...a saisi le juge des référés du tribunal administratif d'Orléans, sur le fondement de l'article L. 521-2 du code de justice administrative, de conclusions tendant à ce qu'il soit enjoint à la préfète du Cher de lui accorder une admission provisoire au séjour et de lui permettre de saisir l'Office français de protection des réfugiés et apatrides, en soutenant que le délai de six mois était expiré à la date du 3 octobre 2017 ; que, par une ordonnance du 11 octobre 2017, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande ; <br/>
<br/>
              8. Considérant que M. B...se borne, à l'appui de sa requête d'appel, à soutenir que le délai de six mois dont disposaient les autorités françaises pour mettre à exécution son transfert en Italie en application du paragraphe 1 de l'article 29 du règlement du 26 juin 2013 était expiré à la date à laquelle elles ont entrepris d'y procéder, au motif que le recours qu'il a formé contre l'arrêté l'assignant à résidence n'aurait pas eu un effet interruptif ; qu'il résulte de ce qui a été dit au point 6 que ce moyen doit être écarté ; que, par suite, M. B...n'est pas fondé à soutenir que c'est à tort que le juge des référé du tribunal administratif a rejeté se demande ; que sa requête doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - DÉCISION DE TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - DÉLAI DE SIX MOIS IMPARTI PAR L'ART. 29 DU RÈGLEMENT (UE) N° 604/2013 DU 26 JUIN 2013 POUR EFFECTUER CE TRANSFERT - 1) RECOURS CONTRE LA DÉCISION DE TRANSFERT (ART. L. 742-4 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE, QU'UNE DÉCISION DE PLACEMENT EN RÉTENTION OU D'ASSIGNATION À RÉSIDENCE AIT OU NON ÉTÉ NOTIFIÉE AVEC LA DÉCISION DE TRANSFERT [RJ1] - 2) RECOURS CONTRE LA DÉCISION D'ASSIGNATION À RÉSIDENCE (III DE L'ART. L. 512-1 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-02-03-03-01 - DÉCISION DE TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - DÉLAI DE SIX MOIS IMPARTI PAR L'ART. 29 DU RÈGLEMENT (UE) N° 604/2013 DU 26 JUIN 2013 POUR EFFECTUER CE TRANSFERT - 1) RECOURS CONTRE LA DÉCISION DE TRANSFERT (ART. L. 742-4 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE, QU'UNE DÉCISION DE PLACEMENT EN RÉTENTION OU D'ASSIGNATION À RÉSIDENCE AIT OU NON ÉTÉ NOTIFIÉE AVEC LA DÉCISION DE TRANSFERT [RJ1] - 2) RECOURS CONTRE LA DÉCISION D'ASSIGNATION À RÉSIDENCE (III DE L'ART. L. 512-1 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DÉCISION DE TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - DÉLAI DE SIX MOIS IMPARTI PAR L'ART. 29 DU RÈGLEMENT (UE) N° 604/2013 DU 26 JUIN 2013 POUR EFFECTUER CE TRANSFERT - 1) RECOURS CONTRE LA DÉCISION DE TRANSFERT (ART. L. 742-4 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE, QU'UNE DÉCISION DE PLACEMENT EN RÉTENTION OU D'ASSIGNATION À RÉSIDENCE AIT OU NON ÉTÉ NOTIFIÉE AVEC LA DÉCISION DE TRANSFERT [RJ1] - 2) RECOURS CONTRE LA DÉCISION D'ASSIGNATION À RÉSIDENCE (III DE L'ART. L. 512-1 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - DÉCISION DE TRANSFERT D'UN DEMANDEUR D'ASILE VERS L'ETAT MEMBRE RESPONSABLE DE L'EXAMEN DE SA DEMANDE (RÈGLEMENT DUBLIN III) - DÉLAI DE SIX MOIS IMPARTI PAR L'ART. 29 DU RÈGLEMENT (UE) N° 604/2013 DU 26 JUIN 2013 POUR EFFECTUER CE TRANSFERT - 1) RECOURS CONTRE LA DÉCISION DE TRANSFERT (ART. L. 742-4 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE, QU'UNE DÉCISION DE PLACEMENT EN RÉTENTION OU D'ASSIGNATION À RÉSIDENCE AIT OU NON ÉTÉ NOTIFIÉE AVEC LA DÉCISION DE TRANSFERT [RJ1] - 2) RECOURS CONTRE LA DÉCISION D'ASSIGNATION À RÉSIDENCE (III DE L'ART. L. 512-1 DU CESEDA) - EFFET INTERRUPTIF - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 095-02-03 1) L'introduction d'un recours contre la décision de transfert, sur le fondement du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), par l'étranger qui n'a fait l'objet à ce stade ni d'une assignation à résidence, ni d'un placement en rétention administrative, doit être regardée comme interrompant le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 jusqu'à l'intervention du jugement du tribunal administratif. Il en va de même de l'introduction d'un recours contre la décision de transfert sur le fondement du III de l'article L. 512-1 du CESEDA, auquel renvoie le II de l'article L. 742-4 de ce code, lorsqu'une décision de placement en rétention ou d'assignation à résidence est notifiée avec la décision de transfert.... ,,2) Le recours formé, sur le fondement du III de l'article L. 512-1 du CESEDA, contre la décision d'assignation à résidence de l'étranger faisant l'objet d'une décision de transfert doit être regardée comme interrompant également le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013.</ANA>
<ANA ID="9B"> 095-02-03-03-01 1) L'introduction d'un recours contre la décision de transfert, sur le fondement du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), par l'étranger qui n'a fait l'objet à ce stade ni d'une assignation à résidence, ni d'un placement en rétention administrative, doit être regardée comme interrompant le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 jusqu'à l'intervention du jugement du tribunal administratif. Il en va de même de l'introduction d'un recours contre la décision de transfert sur le fondement du III de l'article L. 512-1 du CESEDA, auquel renvoie le II de l'article L. 742-4 de ce code, lorsqu'une décision de placement en rétention ou d'assignation à résidence est notifiée avec la décision de transfert.... ,,2) Le recours formé, sur le fondement du III de l'article L. 512-1 du CESEDA, contre la décision d'assignation à résidence de l'étranger faisant l'objet d'une décision de transfert doit être regardée comme interrompant également le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013.</ANA>
<ANA ID="9C"> 15-05-045-05 1) L'introduction d'un recours contre la décision de transfert, sur le fondement du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), par l'étranger qui n'a fait l'objet à ce stade ni d'une assignation à résidence, ni d'un placement en rétention administrative, doit être regardée comme interrompant le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 jusqu'à l'intervention du jugement du tribunal administratif. Il en va de même de l'introduction d'un recours contre la décision de transfert sur le fondement du III de l'article L. 512-1 du CESEDA, auquel renvoie le II de l'article L. 742-4 de ce code, lorsqu'une décision de placement en rétention ou d'assignation à résidence est notifiée avec la décision de transfert.... ,,2) Le recours formé, sur le fondement du III de l'article L. 512-1 du CESEDA, contre la décision d'assignation à résidence de l'étranger faisant l'objet d'une décision de transfert doit être regardée comme interrompant également le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013.</ANA>
<ANA ID="9D"> 335-03-03 1) L'introduction d'un recours contre la décision de transfert, sur le fondement du I de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), par l'étranger qui n'a fait l'objet à ce stade ni d'une assignation à résidence, ni d'un placement en rétention administrative, doit être regardée comme interrompant le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013 jusqu'à l'intervention du jugement du tribunal administratif. Il en va de même de l'introduction d'un recours contre la décision de transfert sur le fondement du III de l'article L. 512-1 du CESEDA, auquel renvoie le II de l'article L. 742-4 de ce code, lorsqu'une décision de placement en rétention ou d'assignation à résidence est notifiée avec la décision de transfert.... ,,2) Le recours formé, sur le fondement du III de l'article L. 512-1 du CESEDA, contre la décision d'assignation à résidence de l'étranger faisant l'objet d'une décision de transfert doit être regardée comme interrompant également le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du cas où la décision de transfert est notifiée en même temps que la décision de placement en rétention ou que la décision d'assignation à résidence, CE, juge des référés, 4 mars 2015, M.,, n° 388180, p. 79.,,[RJ2] Rappr. CE, juge des référés, 4 mars 2015, M.,, n° 388180, p. 79.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
