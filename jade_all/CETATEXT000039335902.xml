<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335902</ID>
<ANCIEN_ID>JG_L_2019_11_000000430608</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/59/CETATEXT000039335902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 06/11/2019, 430608, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430608</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430608.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et deux mémoires en réplique, enregistrés les 10 mai, 27 juin et 11 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 22 novembre 2018 par lequel le Premier ministre lui a refusé l'acquisition de la nationalité française, ainsi que la décision implicite née du rejet du recours gracieux qu'il a déposé le 6 février 2019 contre ce décret ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de réexaminer sa demande d'acquisition de la nationalité française ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	L'article 21-2 du code civil dispose que : " L'étranger ou apatride qui contracte mariage avec un conjoint de nationalité française peut, après un délai de quatre ans à compter du mariage, acquérir la nationalité française par déclaration à condition qu'à la date de cette déclaration la communauté de vie tant affective que matérielle n'ait pas cessé entre les époux depuis le mariage et que le conjoint français ait conservé sa nationalité ". Aux termes de l'article 21-4 du même code : " Le Gouvernement peut s'opposer par décret en Conseil d'État, pour indignité ou défaut d'assimilation autre que linguistique, à l'acquisition de la nationalité française par le conjoint étranger dans un délai de deux ans à compter de la date du récépissé prévu au deuxième alinéa de l'article 26 ou, si l'enregistrement a été refusé, à compter du jour où la décision judiciaire admettant la régularité de la déclaration est passée en force de chose jugée ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. A... B..., ressortissant tunisien, a épousé une ressortissante française le 10 janvier 2009 à Lyon. Le 20 mars 2017, il a souscrit une déclaration d'acquisition de la nationalité française à raison de ce mariage. Par le décret attaqué, le Premier ministre s'est opposé à l'acquisition de la nationalité française, au motif que M. A... B... ne pouvait être regardé comme étant digne d'acquérir la nationalité française. M. A... B... demande l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              3.	En premier lieu, il ressort des pièces du dossier que M. A... B... a, entre le 1er avril 2013 et le 30 juin 2014, déclaré des revenus minorés en vue de l'obtention du revenu de solidarité active pour un montant de 5 085 euros. Pour ces faits, il a été condamné par une ordonnance d'homologation du 7 janvier 2016 du président du tribunal de grande instance de Lyon, passée en force de chose jugée, à une peine de six mois d'emprisonnement avec sursis et une amende de 1 500 euros. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la mesure d'instruction demandée, que le moyen tiré de ce que la décision attaquée repose sur des faits matériellement inexacts ne peut qu'être écarté. <br/>
<br/>
              4.	En second lieu, il ressort des pièces du dossier que les faits de fraude en vue de l'obtention du revenu de solidarité active pour lesquels M. A... B... a été condamné se sont déroulés de manière répétitive entre le 1er avril 2013 et le 30 juin 2014. Par suite, en estimant qu'à la date du décret attaqué et en raison de la nature, de la répétition et de la gravité des faits qui lui sont reprochés, M. A... B... devait être regardé comme indigne d'acquérir la nationalité française, le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 21-4 du code civil.<br/>
<br/>
              5.	Il résulte de ce qui précède que M. A... B... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 22 novembre 2018 lui refusant l'acquisition de la nationalité française.<br/>
<br/>
              6.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
