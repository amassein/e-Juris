<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043100581</ID>
<ANCIEN_ID>JG_L_2021_02_000000431131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/05/CETATEXT000043100581.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/02/2021, 431131</TITRE>
<DATE_DEC>2021-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:431131.20210205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      M. B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 29 décembre 2017 par laquelle l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile. Par une décision n°18002484 du 26 mars 2019, la Cour nationale du droit d'asile a annulé la décision de l'Office et reconnu à M. A... la qualité de réfugié. <br/>
<br/>
      Par un pourvoi sommaire et deux mémoires complémentaires enregistrés les 27 mai, 27 août et 9 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat d'annuler cette décision. <br/>
<br/>
<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
      Vu : <br/>
      - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
      - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
      - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
 Après avoir entendu en séance publique :<br/>
<br/>
 - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
 - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
      La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 733-25 du code de l'entrée et du séjour des étrangers et du droit d'asile, relatif au déroulement de l'audience devant la Cour nationale du droit d'asile : " (...) Après la lecture du rapport, et sauf si le conseil du requérant demande à présenter ses observations, la formation de jugement peut poser aux parties toute question propre à l'éclairer. / Le président de la formation de jugement donne la parole au requérant et au représentant de l'office. / Les parties peuvent présenter oralement toute observation utile propre à éclairer leurs écritures (...) ". En vertu du premier alinéa de l'article R. 733-28 du même code, concernant le jugement des recours : " La formation de jugement se prononce sur le recours, en fonction des pièces du dossier et des observations présentées oralement par les parties, dans les conditions prévues par l'article R. 733-25 ". Toutefois, aux termes de l'article R. 733-29 du même code : " Lorsque le président de la formation de jugement décide d'ordonner un supplément d'instruction, les parties sont invitées à présenter un mémoire ou des pièces complémentaires pour les seuls besoins de ce supplément d'instruction. La même formation de jugement délibère, à l'expiration du délai imparti aux parties pour produire ces éléments ou, le cas échéant, y répliquer. Ce délai ne peut excéder une durée d'un mois à compter de la date de l'audience. Les parties ne sont convoquées à une nouvelle audience que si le président de la formation de jugement estime nécessaire de les entendre présenter des observations orales sur les seuls éléments nouveaux qui auraient été produits ". Il résulte de ces dispositions que si le président de la formation de jugement de la Cour nationale du droit d'asile peut, à l'issue de l'audience publique, ordonner un supplément d'instruction, les productions des parties pour y répondre et les observations qu'elles peuvent susciter doivent intervenir dans un délai d'un mois à compter de la date de l'audience. A l'expiration de ce délai, il appartient à la formation de jugement de délibérer. Si le président de la formation de jugement entend permettre aux parties de produire de nouvelles observations au-delà de ce délai, il doit renvoyer l'affaire à une audience ultérieure. Il en va de même s'il estime nécessaire de recueillir leurs observations orales sur les éléments produits.<br/>
<br/>
              2. Il résulte des pièces de la procédure suivie devant la Cour nationale du droit d'asile qu'après l'audience tenue le 2 octobre 2018, la présidente de la formation de jugement a, en application de l'article R. 733-29, ordonné un supplément d'instruction afin de recueillir les observations de l'Office français de protection des réfugiés et apatrides sur d'éventuelles condamnations de M. A... pour des infractions commises sur le territoire français. Il ressort des termes mêmes de la décision attaquée que la présidente de la formation de jugement, après l'expiration du délai d'un mois laissé aux parties pour produire leurs observations ou pièces complémentaires, a ordonné un nouveau supplément d'instruction le 6 février 2019. En rendant sa décision le 26 mars 2019 sans tenir une nouvelle audience, la Cour a entaché la procédure d'irrégularité. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'il attaque. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: La décision de la Cour nationale du droit d'asile du 26 mars 2019 est annulée. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. B.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-02-01 - POSSIBILITÉ D'ORDONNER UN SUPPLÉMENT D'INSTRUCTION À L'ISSUE DE L'AUDIENCE PUBLIQUE SANS RENVOYER L'AFFAIRE - 1) DANS LE MOIS SUIVANT L'AUDIENCE - A) EXISTENCE [RJ1] - B) DÉLAI IMPARTI POUR Y RÉPONDRE - 2) AU-DELÀ DE CE DÉLAI - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 095-08-02-01 1) a) Il résulte des articles R. 733-25, R. 733-28 et R. 733-29 du du code de l'entrée et du séjour des étrangers et du droit d'asile que le président de la formation de jugement de la Cour nationale du droit d'asile (CNDA) peut, à l'issue de l'audience publique, ordonner un supplément d'instruction.,,,b) Les productions des parties pour y répondre et les observations qu'elles peuvent susciter doivent intervenir dans un délai d'un mois à compter de la date de l'audience.,,,A l'expiration de ce délai, il appartient à la formation de jugement de délibérer.,,,2) Si le président de la formation de jugement entend permettre aux parties de produire de nouvelles observations au-delà de ce délai, il doit renvoyer l'affaire à une audience ultérieure.,,,Il en va de même s'il estime nécessaire de recueillir leurs observations orales sur les éléments produits.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., jugeant dans le cas général qu'une mesure d'instruction après la clôture de l'instruction a pour effet de rouvrir l'instruction et commande la tenue d'une nouvelle audience, CE, 4 mars 2009, Elections cantonales de Belle-Ile-en-Mer (Morbihan), n°s 317473 317735, T. p. 896 ; CE, 7 décembre 2011, Département de la Haute-Garonne, n° 330751, T. p. 1084.,,[RJ2] Rappr., jugeant dans le cas général qu'une mesure d'instruction après la clôture de l'instruction et l'audience commande la tenue d'une nouvelle audience, CE, 4 mars 2009, n°s 317473 317735, Elections cantonales de Belle-Ile-en-Mer (Morbihan).</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
