<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158716</ID>
<ANCIEN_ID>JG_L_2018_07_000000409167</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/87/CETATEXT000037158716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 04/07/2018, 409167, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409167</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409167.20180704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir les arrêtés du 17 mai 2016 par lesquels le préfet de l'Essonne a décidé son transfert aux autorités espagnoles et l'a assigné à résidence. Par un jugement n° 1603584 du 23 mai 2016, le tribunal administratif a rejeté cette demande.  <br/>
<br/>
              Par un arrêt n° 16VE02042 du 19 janvier 2017, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 mars et 1er juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 juin 2018, présentée par le ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B..., né le 2 janvier 1997, de nationalité syrienne, est entré irrégulièrement en France en novembre 2015 pour y solliciter l'asile ; que, par deux arrêtés du 17 mai 2016, le préfet de l'Essonne a décidé sa remise aux autorités espagnoles en leur qualité d'autorités de l'Etat membre responsable de l'examen de sa demande d'asile sur le fondement du paragraphe 5 de l'article 20 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride et l'a assigné à résidence ; que M. B...demande l'annulation de l'arrêt du 19 janvier 2017 par lequel la cour administrative de Versailles a rejeté l'appel qu'il avait formé contre le jugement du tribunal administratif de Versailles ayant rejeté sa demande tendant à l'annulation de ces arrêtés ;<br/>
<br/>
              2.	Considérant que le premier paragraphe de l'article 3 du règlement du Parlement européen et du Conseil du 26 juin 2013 prévoit que la demande de protection internationale présentée par un ressortissant de pays tiers ou par un apatride " est examinée par un seul Etat membre, qui est celui que les critères énoncés au chapitre III désignent comme responsable " ; que le premier paragraphe de l'article 17 de ce règlement dispose que : " Par dérogation à l'article 3, paragraphe 1, chaque État membre peut décider d'examiner une demande de protection internationale qui lui est présentée par un ressortissant de pays tiers ou un apatride, même si cet examen ne lui incombe pas en vertu des critères fixés dans le présent règlement (...) " ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a quitté la Syrie en 2014, à l'âge de dix-sept ans ; qu'après qu'il eut transité par le Liban, l'Algérie et le Maroc, ses empreintes digitales ont été relevées par les autorités espagnoles dans l'enclave espagnole de Melilla en septembre 2015 ; qu'entré en France en novembre 2015, il y a déposé une demande d'asile ; qu'à la date des arrêtés litigieux, il résidait au domicile de son oncle et de sa tante et était dépourvu de toute attache en Espagne ; qu'en outre, il ressort des écritures du préfet devant le tribunal administratif que ce dernier s'est mépris sur l'identité de M. B... et sur sa situation, en faisant état de circonstances qui concernaient une autre personne ; que, dans les circonstances particulières de l'espèce, M. B... est fondé à soutenir que la cour administrative de Versailles a dénaturé les faits de l'espèce en jugeant que n'était pas entaché d'erreur manifeste d'appréciation le refus du préfet de faire application des dispositions dérogatoires de l'article 17 du règlement du 26 juin 2013 et à demander, pour ce motif, l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier, en particulier des éléments figurant au point 3, que le préfet, eu égard à l'âge de M.B..., à son parcours et à la circonstance que, dépourvu de toute attache en Espagne, il réside en France au domicile de son oncle et de sa tante de nationalité française, a commis une erreur manifeste d'appréciation en refusant de faire, dans les circonstances de l'espèce, application des dispositions de l'article 17 du règlement du 26 juin 2013 ; que, dès lors, M. B...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté ses conclusions tendant à l'annulation des arrêtés du 17 mai 2016 par lesquels le préfet de l'Essonne a décidé son transfert aux autorités espagnoles et l'a assigné à résidence ; <br/>
<br/>
              6.	Considérant qu'il y a lieu d'enjoindre au préfet de l'Essonne d'enregistrer la demande d'asile de M. B...dans un délai d'un mois à compter de la notification de la présente décision ;<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 19 janvier 2017 et le jugement du tribunal administratif de Versailles du 23 mai 2016 sont annulés.<br/>
Article 2 : Les arrêtés du préfet de l'Essonne du 17 mai 2016 sont annulés.<br/>
<br/>
Article 3 : Il est enjoint au préfet de l'Essonne d'enregistrer la demande d'asile de M. B...dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : L'Etat versera à M. B...la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
