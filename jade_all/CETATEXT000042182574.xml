<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042182574</ID>
<ANCIEN_ID>JG_L_2020_07_000000442171</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/18/25/CETATEXT000042182574.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/07/2020, 442171, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442171</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:442171.20200728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 24 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme F... E..., M. L... H..., Mme P... B..., née K..., Mme I... D..., Mme A... J..., Mme C... M..., née N..., et M. G... O... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de la délibération en date du 19 juin 2020 par laquelle le jury du concours interne de l'agrégation section langues vivantes étrangères allemand au titre de la session 2020 a établi la liste principale des candidats admis et notifié à Mme J... une décision de refus ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de la délibération en date du 22 juin 2020 par laquelle le jury du concours interne de l'agrégation section langues vivantes étrangères anglais au titre de la session 2020 a établi la liste principale des candidats admis et notifié à Mme E..., Mme B... et M. H... une décision de refus ;<br/>
<br/>
              3°) d'ordonner la suspension de l'exécution de la délibération en date du 22 juin 2020 par laquelle le jury de l'agrégation de lettre moderne au titre de la session 2020 a établi la liste principale des candidats admis et notifié à Mme N... une décision de refus ;<br/>
<br/>
              4°) d'ordonner la suspension de l'exécution de la délibération en date du 26 juin 2020 par laquelle le jury de l'agrégation section langue vivante étrangères espagnol au titre de la session 2020 a établi la liste principale des candidats et notifié à Mme D... une décision de refus ;<br/>
              5°) d'ordonner la suspension de l'exécution de la délibération en date du 26 juin 2020 par laquelle le jury du concours interne de l'agrégation section histoire et géographie au titre de la session 2020 a établi la liste principale des candidats admis et notifié à M. O... une décision de refus ;<br/>
<br/>
              6°) d'enjoindre au ministre de l'éducation nationale, de la jeunesse et des sports de reconvoquer les jurys du concours interne de l'agrégation au titre de la session 2020 en vue de l'organisation des épreuves orales dans des modalités similaires à celles maintenues pour certaines sections du concours externes et dans un délai de trois mois à compter de la présente ordonnance ;<br/>
<br/>
              7°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - leur requête est recevable ;<br/>
              - la condition de l'urgence est remplie eu égard, premièrement, aux conditions de préparation de ce concours, deuxièmement, à la circonstance que les candidats déclarés admis sont sur le point d'être nommés dans un emploi public en qualité de professeurs agrégés stagiaires, troisièmement, à la circonstance que les programmes pour la session 2021 ont été substantiellement modifiés ; <br/>
              - il existe un doute sérieux quant à la légalité des délibérations attaquées ;<br/>
              - elles sont entachées d'une erreur manifeste d'appréciation dès lors que l'annulation par le ministre de l'éducation nationale des oraux du concours interne de l'agrégation 2020, en premier lieu, n'était plus justifiée, à la date de son arrêté du 10 juin 2020, par l'évolution positive de la situation sanitaire, en deuxième lieu, des opportunités matérielles et technologiques garantissaient des conditions d'organisations des épreuves orales dans le respect des " gestes barrières " et des règles sanitaires, et, en dernier lieu, une décision concomitante a maintenu, en dépit du contexte sanitaire, les épreuves orales de certaines sections du concours externe de l'agrégation ;<br/>
              - elles méconnaissent le principe d'égalité de traitement entre les candidats au concours externe de l'agrégation et les candidats au concours interne.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d' une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
              2. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre, ressortit lui-même à la compétence directe du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du titre V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance, sans qu'il ait à les transmettre à la juridiction compétente.<br/>
<br/>
              3. Les requérants demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de plusieurs délibérations du jury du concours interne de l'agrégation. La contestation de la légalité de ces délibérations n'est manifestement pas au nombre de celles dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative ou d'autres dispositions.<br/>
<br/>
              4. Il suit de là qu'il y a lieu de rejeter la requête de Mme E... et autres selon la procédure prévue par l'article L. 522-3 du code précité, y compris les conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme E... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée Mme F... E..., première requérante dénommée.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
