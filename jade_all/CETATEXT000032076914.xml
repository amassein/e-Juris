<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076914</ID>
<ANCIEN_ID>JG_L_2016_02_000000380684</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 17/02/2016, 380684</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380684</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:380684.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Melun d'annuler la décision du ministre de l'intérieur du 10 juillet 2008 l'informant de la perte de validité de son permis de conduire ainsi que la décision implicite par laquelle le ministre a rejeté son recours gracieux contre cette décision. Par un jugement n° 1208126/6 du 11 mars 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14PA01664 du 14 mai 2014, enregistrée le 27 mai 2014 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté devant cette cour par M.B.... Par ce pourvoi, enregistré le 14 avril 2014 au greffe de la cour administrative d'appel de Paris, et par un mémoire complémentaire, enregistré le 26 août 2014 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'intérieur de reconstituer tout ou partie du capital de points de son permis de conduire et de le lui restituer dans un délai de quinze jours à compter de la lecture de la décision à intervenir et sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B... a commis les 22 mai 2003, 8 novembre 2003, 19 août 2006 et 1er août 2007, des infractions au code de la route ayant entraîné le retrait de seize points de son permis de conduire ; que par décision du 10 juillet 2008, le ministre chargé de l'intérieur a constaté la perte de validité de son permis à la suite de ces retraits de points et lui a enjoint de le restituer ; que, par lettre du 1er juin 2012, M. B...a présenté contre cette décision un recours gracieux que le ministre de l'intérieur a rejeté implicitement ; que l'intéressé se pourvoit en cassation contre le jugement du 11 mars 2014 par lequel le tribunal administratif de Melun a rejeté sa demande tendant à l'annulation de la décision du 10 juillet 2008 et du rejet de son recours gracieux ; <br/>
<br/>
              Sur les conclusions à fin de non lieu présentées par le ministre de l'intérieur :<br/>
<br/>
              2. Considérant que le ministre fait valoir dans son mémoire en défense qu'il a notifié le 5 septembre 2015 à M. B...une décision constatant la perte de validité de son permis de conduire pour solde de points nuls ; qu'il en déduit que sa décision du 10 juillet 2008 doit être regardée comme retirée et que le pourvoi de l'intéressé a perdu son objet en tant qu'il concerne le rejet par le tribunal administratif de Melun de ses conclusions dirigées contre cette décision ; que, toutefois, le ministre s'est borné à verser au dossier le relevé d'information intégral relatif au permis de conduire de M.B..., édité le 14 octobre 2015 ; que si ce document mentionne la notification, le 5 septembre 2015, d'une lettre " 48 SI " relative à la perte de validité du permis, il fait apparaître que les retraits de points ayant concouru à cette perte de validité sont consécutifs aux mêmes infractions que celles qui fondaient la décision du 10 juillet 2008 ; que, dans ces conditions, le ministre doit être regardé comme ayant procédé à une notification de cette décision et non comme ayant pris une décision nouvelle qui en aurait emporté le retrait ; qu'il suit de là que le pourvoi conserve son objet ; <br/>
<br/>
              Sur le pourvoi de M.B... :<br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 223-6 du code de la route dans sa rédaction issue de la loi du 5 mars 2007 : " Si le titulaire du permis de conduire n'a pas commis, dans le délai de trois ans à compter de la date du paiement de la dernière amende forfaitaire, de l'émission du titre exécutoire de la dernière amende forfaitaire majorée, de l'exécution de la dernière composition pénale ou de la dernière condamnation définitive, une nouvelle infraction ayant donné lieu au retrait de points, son permis est affecté du nombre maximal de points " ;<br/>
<br/>
              4. Considérant, d'une part, que les décisions portant retrait de points d'un permis de conduire, de même que celles qui constatent la perte de validité du permis pour solde de points nuls, ne sont opposables à son titulaire qu'à compter de la date à laquelle elles lui sont notifiées ; que, tant que le retrait de l'ensemble des points du permis ne lui a pas été rendu opposable, l'intéressé peut prétendre au bénéfice des dispositions précitées de l'article L. 223-6 du code de la route prévoyant des reconstitutions de points lorsque le titulaire du permis a accompli un stage de sensibilisation à la sécurité routière ou qu'il n'a commis aucune infraction ayant donné lieu à retrait de points pendant une certaine période ; <br/>
<br/>
              5. Considérant, d'autre part, qu'il appartient au juge administratif, saisi d'une contestation portant sur un retrait de points du permis de conduire, lequel constitue une sanction que l'administration inflige à un administré, de se prononcer sur cette contestation comme juge de plein contentieux ; qu'il en va de même lorsque le juge est saisi d'un recours contre une décision constatant la perte de validité d'un permis de conduire pour solde de points nul ; que, dans le cas où il apparaît que le solde des points était nul à la date à laquelle une telle décision est intervenue mais que, faute pour l'administration de l'avoir rendue opposable en la notifiant à l'intéressé, celui-ci a pu ultérieurement remplir les conditions pour bénéficier d'une reconstitution totale ou partielle de son capital de points, il appartient au juge de prononcer l'annulation de la décision ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B... a commis les 22 mai et 8 novembre 2003 et les 19 août 2006 et 1er août 2007 des infractions au code de la route ayant entraîné le retrait de la totalité des points de son permis de conduire, dont le solde était nul lorsqu'est intervenue la décision du 10 juillet 2008 constatant sa perte de validité ; que, toutefois, n'ayant pas reçu notification de cette décision et n'ayant pas commis d'infraction ayant entraîné retrait de points pendant trois ans à compter du 1er août 2007, date du paiement de la dernière amende forfaitaire, il s'est trouvé remplir, le 1er août 2010, les conditions prévues par les dispositions législatives précitées pour bénéficier d'une reconstitution intégrale de son capital de points ; qu'il résulte de ce qui a été dit au point 5 que le tribunal administratif de Melun a commis une erreur de droit en écartant le moyen par lequel M. B... l'invitait à constater cette reconstitution et à annuler en conséquence la décision du 10 juillet 2008 ainsi que la décision rejetant son recours gracieux présenté le 1er juin 2012 ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 11 mars 2014 du tribunal administratif de Melun est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Melun. <br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 500 euros au titre de l'article L 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-03 Police. Police générale. Circulation et stationnement. Permis de conduire. Retrait de permis.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
