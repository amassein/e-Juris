<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487239</ID>
<ANCIEN_ID>JG_L_2021_12_000000441343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/12/2021, 441343, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441343.20211213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... D... a demandé au tribunal administratif de Paris de condamner l'université Paris-Dauphine à lui verser la somme de 307 337,10 euros en réparation des préjudices qu'il estime avoir subis du fait de l'illégalité de la décision révélée par l'absence de soutenance de sa thèse à la date du 1er décembre 2014 et de la décision du 24 mars 2015 par laquelle le président de l'université a refusé de lui accorder l'autorisation de soutenir sa thèse en l'état. Par un jugement n° 1705239/1-2 du 12 février 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19PA01169 du 9 juin 2020, la cour administrative d'appel de Paris a, sur appel de M. D..., annulé ce jugement et rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et des mémoires complémentaires, enregistrés les 22 juin, 23 septembre et 4 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'université Paris Dauphine, la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
              - l'arrêté du 7 août 2006 relatif à la formation doctorale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Haas, avocat de M. D... et à la SCP Lesourd, avocat de l'universite Paris-Dauphine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., inscrit en doctorat de droit public à l'université Paris Dauphine, a déposé en septembre 2014 un projet de thèse, dans la perspective d'une soutenance le 1er décembre 2014. Après la formulation, par l'un des deux rapporteurs chargés d'examiner la thèse, d'un avis défavorable, lors d'un entretien qui s'est tenu le 20 novembre 2014, le directeur de l'école doctorale de l'université a invité l'intéressé à la modifier afin de prendre en compte les critiques qui avaient été formulées, demande qui a été réitérée par un courriel en date du 8 janvier 2015. M. D... a formé un recours le 11 janvier 2015 auprès du président de l'université Paris-Dauphine qui, par une décision du 24 mars 2015, a refusé de l'autoriser à soutenir sa thèse en l'état. M. D... a ensuite soutenu sa thèse le 17 juin 2015, et une attestation de réussite au diplôme de docteur en droit lui a été délivrée le 20 juillet 2015. Il a formé une demande indemnitaire auprès de l'université Paris-Dauphine pour solliciter la réparation des préjudices qu'il estime avoir subis en raison des décisions de l'université l'ayant mis dans l'impossibilité de soutenir sa thèse à la date du 1er décembre 2014 initialement prévue puis, à la suite du rejet implicite de cette demande par l'université, il a saisi le tribunal administratif de Paris d'une demande aux mêmes fins qui a été rejetée par un jugement du 12 février 2019. M. D... se pourvoit en cassation contre l'arrêt du 9 juin 2020 par lequel la cour administrative d'appel de Paris, sur son appel, a annulé le jugement du tribunal administratif de Paris puis rejeté sa demande.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. D... avait soulevé devant la cour administrative d'appel le moyen, opérant, selon lequel le jugement du tribunal administratif de Paris du 12 février 2019 était insuffisamment motivé faute de se prononcer sur le moyen tiré de l'incompétence de l'auteur de la décision de refus d'autorisation de soutenance, révélée par l'absence de soutenance de sa thèse à la date initialement prévue. En jugeant qu'un tel moyen, dès lors qu'il était présenté à l'encontre d'une décision révélée, était inopérant, la cour administrative d'appel de Paris a entaché son arrêt d'erreur de droit.<br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. D... est fondé à demander l'annulation de l'arrêt du 9 juin 2020 de la cour administrative d'appel de Paris qu'il attaque. <br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D... au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 9 juin 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : Les conclusions de M. D... et de l'université Paris-Dauphine présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. C... D... et à l'université Paris-Dauphine.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>
Délibéré à l'issue de la séance du 8 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
Rendu le 13 décembre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
