<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031529581</ID>
<ANCIEN_ID>JG_L_2015_11_000000373550</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/52/95/CETATEXT000031529581.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 25/11/2015, 373550, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373550</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; SCP CAPRON ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373550.20151125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Caen de condamner solidairement le centre hospitalier de la Côte fleurie et le groupement de coopération sanitaire des urgences de la Côte fleurie à lui verser les sommes de 3 587,96 euros au titre des actes qu'il a pratiqués au service des urgences géré par le groupement et pour lesquels il n'a pas été rémunéré et de 2 500 euros au titre de son préjudice moral. Par un jugement n° 1202289 du 26 septembre 2013, le tribunal administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 novembre 2013 et 28 février 2014, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge solidaire du centre hospitalier de la Côte fleurie et du groupement de coopération sanitaire des urgences de la Côte fleurie de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2010-862 du 23 juillet 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M.A..., à la SCP Waquet, Farge, Hazan, avocat du centre hospitalier de la Côte fleurie, et à la SCP Capron, avocat du groupement de coopération sanitaire des urgences de la Côte fleurie ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., médecin libéral, a demandé au tribunal administratif de Caen de condamner solidairement le centre hospitalier de la Côte fleurie et le groupement de coopération sanitaire des urgences de la Côte fleurie, créé entre le centre hospitalier et la polyclinique de Deauville pour gérer le service des urgences, à l'indemniser du préjudice résultant de l'absence de rémunération de certaines des activités de médecine d'urgence qu'il a exercées au sein du groupement de coopération sanitaire ; qu'il se pourvoit en cassation contre le jugement du tribunal administratif de Caen du 26 septembre 2013 qui a rejeté sa demande au motif, d'une part, que celle-ci n'était pas recevable en tant qu'elle était dirigée contre le groupement de coopération sanitaire, dépourvu d'existence juridique, et, d'autre part, que le centre hospitalier de la Côte fleurie n'avait pas commis de faute de nature à engager sa responsabilité ;<br/>
<br/>
              Sur le jugement en tant qu'il a rejeté les conclusions dirigées contre le groupement de coopération sanitaire des urgences de la Côte fleurie :<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 6133-1 du code de la santé publique, dans sa rédaction applicable à la date de constitution du groupement de coopération sanitaire des urgences de la Côte fleurie, un groupement de coopération sanitaire, doté de la personnalité morale, peut être constitué notamment entre des établissements de santé pour faciliter, améliorer ou développer leur activité ; qu'aux termes de l'article R. 6133-11 du code de la santé publique, dans sa rédaction en vigueur à la même date : " La convention constitutive du groupement est transmise pour approbation au directeur de l'agence régionale de l'hospitalisation de la région dans laquelle le groupement a son siège (...) / (...) / Le groupement jouit de la personnalité morale à compter de la date de publication de l'acte d'approbation mentionné au premier alinéa du présent article, au recueil des actes administratifs de la région dans laquelle le groupement a son siège ainsi que dans le recueil des actes administratifs des autres régions lorsque les membres du groupement ont leur siège dans des régions distinctes " ; qu'il résulte de ces dispositions qu'un groupement de coopération sanitaire jouit de la personnalité morale dès lors que l'acte approuvant sa convention constitutive a été publié selon les modalités prévues à l'article R. 6133-11 du code de la santé publique ; que ni la légalité de l'acte d'approbation ni la validité de la convention constitutive ainsi approuvée n'ont d'incidence sur l'acquisition de la personnalité juridique par le groupement ; <br/>
<br/>
              3. Considérant que, pour rejeter comme irrecevables les conclusions dirigées contre le groupement de coopération sanitaire des urgences de la Côte fleurie, le tribunal administratif de Caen a relevé que la convention constitutive de ce groupement, qui a fait l'objet, le 22 décembre 2008, d'une décision d'approbation du directeur de l'agence régionale de l'hospitalisation de Basse-Normandie publiée au recueil des actes administratifs du Calvados du 30 décembre 2008, n'avait été ni datée ni signée et qu'une seconde convention " annulant et remplaçant " la première, signée le 8 avril 2010, n'avait pas fait l'objet d'une approbation et d'une publication par le directeur de l'agence régionale de santé ; qu'en déduisant de ces circonstances que le groupement de coopération sanitaire des urgences de la Côte fleurie n'avait pas la personnalité morale, alors que l'acte approuvant la convention, qui n'était pas juridiquement inexistant, n'avait été ni retiré ni annulé et que ni sa légalité ni la validité de la convention constitutive ainsi approuvée n'avaient d'incidence sur l'acquisition de la personnalité juridique par le groupement, le tribunal a commis une erreur de droit ;<br/>
<br/>
              Sur le jugement en tant qu'il a rejeté les conclusions dirigées contre le centre hospitalier de la Côte fleurie :<br/>
<br/>
              4. Considérant que M. A...soutenait devant le tribunal administratif de Caen que le centre hospitalier de la Côte fleurie avait commis une faute de nature à engager sa responsabilité en participant à un groupement de coopération sanitaire constitué illégalement et dont la convention ne précisait pas suffisamment les conditions juridiques de paiement des actes médicaux aux médecins libéraux ; que si l'approbation de la convention par l'agence régionale d'hospitalisation de Basse-Normandie était susceptible, en cas d'illégalité, d'engager la responsabilité de son auteur, cette approbation ne faisait toutefois pas obstacle, par elle-même, à la reconnaissance d'un lien de causalité direct entre la faute commise, le cas échéant, par le centre hospitalier et le préjudice subi par M.A... ; que, par suite, en jugeant que la responsabilité du centre hospitalier de la Côte fleurie ne pouvait être engagée au seul motif que la convention constitutive du groupement avait été approuvée par l'agence régionale d'hospitalisation de Basse-Normandie, le tribunal a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le jugement attaqué doit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé dans sa totalité ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge solidaire du centre hospitalier de la Côte fleurie et du groupement de coopération sanitaire des urgences de la Côte fleurie une somme de 1 500 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par le centre hospitalier de la Côte fleurie ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 26 septembre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Caen.<br/>
Article 3 : Le centre hospitalier de la Côte fleurie et le groupement de coopération sanitaire des urgences de la Côte fleurie verseront une somme de 1 500 euros à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du centre hospitalier de la Côte fleurie présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M B...A..., au centre hospitalier de la Côte fleurie et au groupement de coopération sanitaire des urgences de la Côte fleurie.<br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
