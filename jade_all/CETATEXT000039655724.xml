<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039655724</ID>
<ANCIEN_ID>JG_L_2019_12_000000409245</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/65/57/CETATEXT000039655724.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 20/12/2019, 409245, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409245</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409245.20191220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Versailles de condamner la chambre de commerce et d'industrie de Paris à lui verser une indemnité de 182 087,75 euros en réparation des préjudices qu'il estime avoir subis en raison de la décision du 6 septembre 2010 par laquelle le directeur général du groupe Hautes études commerciales (HEC) Paris a mis fin à ses fonctions de professeur permanent. Par un jugement n° 1102420 du 29 décembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE00629 du 9 février 2017, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mars et 27 juin 2017 et le 21 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la chambre de commerce et d'industrie de la région Paris Ile-de-France la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 52-1311 du 10 décembre 1952 ; <br/>
              - la loi n° 83-643 du 13 juillet 1983 ;<br/>
              - le décret du 22 mai 1920 portant règlement du Conservatoire national des arts et métiers ;<br/>
              - le décret n° 2007-658 du 2 mai 2007 ;<br/>
              - le statut du personnel administratif des chambres de commerce et d'industrie, des chambres régionales de commerce et d'industrie, de l'assemblée des chambres françaises de commerce et d'industrie ; <br/>
              - la convention portant statut du corps professoral permanent du groupe HEC ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M. B... et à la SCP Rocheteau, Uzan-Sarano, avocat de la chambre de commerce et d'industrie de région Paris Ile-de-France.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a été recruté par contrat à compter du 15 septembre 1990, comme professeur permanent à temps complet, par l'Ecole des hautes études commerciales (HEC), établissement dépendant de la chambre de commerce et d'industrie de Paris. Par décret du président de la République du 20 janvier 2010, M. B... a été nommé et titularisé en qualité de professeur du Conservatoire national des arts et métiers (CNAM), afin d'y exercer à temps complet sur la chaire de développement international des entreprises. M. B... a toutefois été autorisé par HEC, au vu d'une autorisation de cumul d'activités délivrée par le CNAM, à poursuivre son activité d'enseignement à HEC jusqu'à la fin de l'année universitaire 2009/2010. Par la décision du 6 septembre 2010, le directeur général du groupe HEC a mis fin à ses fonctions d'enseignant à compter du 15 septembre 2010. M. B... estimant avoir été ainsi irrégulièrement licencié, faute pour HEC d'avoir respecté un délai de préavis et de lui avoir versé une indemnité de licenciement, a saisi le tribunal administratif de Versailles d'une demande tendant à la condamnation de la chambre de commerce et d'industrie de Paris à lui verser une indemnité de 182 087,75 euros, en réparation des préjudices qu'il estime avoir subis. Il se pourvoit en cassation contre l'arrêt du 9 février 2017 de la cour administrative d'appel de Versailles rejetant son appel formé contre le jugement du 29 décembre 2014 par lequel le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 1 du statut du personnel administratif des chambres de commerce et d'industrie, des chambres régionales de commerce et d'industrie, de l'assemblée des chambres françaises de commerce et d'industrie, dans sa rédaction alors en vigueur : " (...) le cumul d'un emploi au sein d'une Compagnie Consulaire et d'une autre activité professionnelle est interdit, sous réserve des dispositions figurant à l'Article 1 bis du présent Statut (...) ". Aux termes de l'article 1 bis de ce statut : " Par dérogation à l'interdiction de cumuler un emploi au sein d'une Compagnie Consulaire et une activité professionnelle, les agents statutaires (travaillant à temps complet ou accomplissant un service au moins égal à la moitié de la durée hebdomadaire de travail d'un collaborateur à temps complet) peuvent, à titre exceptionnel, bénéficier d'exceptions leur permettant d'exercer une activité professionnelle complémentaire dans les domaines suivants : (...) enseignement dans les domaines ressortissant de la compétence des intéressés après autorisation de la Compagnie Consulaire ". Ces deux articles sont applicables y compris aux agents occupant un emploi permanent à temps complet dans le cadre d'un contrat à durée indéterminée. S'il appartient à l'établissement public consulaire, comme d'ailleurs à toute administration, de veiller à ce que les agents publics qu'il emploie respectent les règles de cumul qui leur sont applicables, le seul fait pour un agent public non titulaire d'être dans une situation de cumul prohibée ne saurait être regardé comme une condition résolutoire du contrat qui le lie au service. Par suite, en se fondant, pour juger que la décision du 6 septembre 2010 n'était pas entachée d'illégalité, sur la circonstance que la règle posée par l'article 1 du statut du personnel administratif des chambres de commerce et d'industrie, interdisant le cumul d'un emploi au sein d'une compagnie consulaire et d'une autre activité professionnelle, constituait une condition résolutoire du contrat liant M. B... à la chambre de commerce et d'industrie de Paris, la cour administrative d'appel a entaché son arrêt d'erreur de droit, alors qu'il lui appartenait de rechercher si, en faisant part de sa décision de rejoindre le CNAM en qualité de professeur titulaire exerçant à temps complet, M. B... devait être regardé comme ayant entendu démissionner de ses fonctions de professeur permanent à HEC.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, que M. B... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la chambre de commerce et d'industrie de la région Paris Ile-de-France, venant aux droits de la chambre de commerce et d'industrie de Paris, la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mis à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande au même titre la chambre de commerce et d'industrie de région Paris Île-de- France.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 février 2017 de la cour administrative d'appel de Versailles est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La chambre de commerce et d'industrie de région Paris Ile-de-France versera à M. B... une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la chambre de commerce et d'industrie de région Paris Ile-de-France sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A... B... et à la chambre de commerce et d'industrie de région Paris Ile-de-France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
