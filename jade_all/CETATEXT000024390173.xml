<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024390173</ID>
<ANCIEN_ID>JG_L_2011_07_000000343430</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/39/01/CETATEXT000024390173.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 19/07/2011, 343430</TITRE>
<DATE_DEC>2011-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343430</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Gilles Pellissier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:343430.20110719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 22 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par la LIGUE DES DROITS DE L'HOMME, dont le siège est 138, rue Marcadet à Paris (75018) ; la LIGUE DES DROITS DE L'HOMME demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2010-835 du 21 juillet 2010 relatif à l'incrimination de l'outrage au drapeau tricolore ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gilles Pellissier, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, par le décret attaqué du 21 juillet 2010, le Premier ministre a introduit dans le code pénal un article R. 645-15 incriminant " l'outrage au drapeau tricolore " aux termes duquel : " Hors les cas prévus par l'article 433-5-1, est puni de l'amende prévue pour les contraventions de la cinquième classe le fait, lorsqu'il est commis dans des conditions de nature à troubler l'ordre public et dans l'intention d'outrager le drapeau tricolore : / 1° De détruire celui-ci, le détériorer ou l'utiliser de manière dégradante, dans un lieu public ou ouvert au public ; / 2° Pour l'auteur de tels faits, même commis dans un lieu privé, de diffuser ou faire diffuser l'enregistrement d'images relatives à leur commission. / La récidive des contraventions prévues au présent article est réprimée conformément aux articles 132-11 et 132-15 " ;<br/>
<br/>
              Considérant en premier lieu qu'il résulte des dispositions de l'article 34 de la Constitution, aux termes desquelles " La loi fixe les règles concernant : (...) la détermination des crimes et délits ainsi que les peines qui leur sont applicables ", que la détermination des contraventions et des peines qui leur sont applicables ressortit à la compétence du pouvoir réglementaire ; que la circonstance que l'incrimination d'un acte a pour effet de limiter l'exercice d'une liberté publique garantie par des dispositions constitutionnelles ne saurait, par elle-même, avoir pour conséquence de réserver au pouvoir législatif la compétence pour édicter ces contraventions, dès lors qu'elles n'ont pas pour objet de réglementer l'exercice de cette liberté mais seulement d'y apporter les limitations nécessaires à la sauvegarde de l'ordre public ;<br/>
<br/>
              Considérant en second lieu qu'il résulte tant des articles 10 et 11 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, qui reconnaissent à tout citoyen la liberté d'exprimer et de communiquer ses opinions sans autres restrictions que celles qui sont nécessaires pour assurer l'ordre public que de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantit le droit de toute personne à la liberté d'expression et prévoit que l'exercice de cette liberté peut être soumis à certaines formalités, conditions, restrictions ou sanctions prévues par la loi, dès lors qu'elles constituent des mesures nécessaires, dans une société démocratique, notamment à la défense de l'ordre public, que seules peuvent être édictées les limites à la liberté d'expression qui sont nécessaires au maintien de l'ordre public et strictement proportionnées à cet objectif ; qu'il appartient au pouvoir réglementaire, lorsqu'il édicte une incrimination qui, comme en l'espèce, peut avoir pour effet de limiter la liberté de chacun d'exprimer, quel qu'en soit le mode, ses idées, de concilier la garantie de cette liberté avec les exigences de l'ordre public ;<br/>
<br/>
              Considérant qu'en prévoyant que le fait de détruire, détériorer ou utiliser de manière dégradante le drapeau tricolore, dans un lieu public ou ouvert au public, ou de diffuser ou faire diffuser l'enregistrement d'images relatives à de tels faits, même commis dans un lieu privé, n'est passible des peines que prévoit le décret que commis " dans des conditions de nature à troubler l'ordre public et dans l'intention d'outrager le drapeau tricolore ", le pouvoir réglementaire a entendu n'incriminer que les dégradations physiques ou symboliques du drapeau susceptibles d'entraîner des troubles graves à la tranquillité et à la sécurité publiques et commises dans la seule intention de détruire, abîmer ou avilir le drapeau ; qu'ainsi ce texte n'a pas pour objet de réprimer les actes de cette nature qui reposeraient sur la volonté de communiquer, par cet acte, des idées politiques ou philosophiques ou feraient oeuvre de création artistique, sauf à ce que ce mode d'expression ne puisse, sous le contrôle du juge pénal, être regardé comme une oeuvre de l'esprit ; qu'ainsi, compte tenu de ces précisions et malgré la généralité de la définition des actes incriminés, le décret attaqué ne porte pas, contrairement à ce que soutient la LIGUE DES DROITS DE L'HOMME, une atteinte excessive à la liberté d'expression garantie par la Déclaration des droits de l'homme et du citoyen et la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'au regard des peines déjà prévues par le législateur tant dans le code pénal que dans le code de la défense pour protéger l'emblème de la République institué par l'article 2 de la Constitution d'atteintes de ce type, l'auteur du décret attaqué n'a pas fait une inexacte appréciation de la nécessité d'interdire les agissements réprimés par ce texte et des sanctions destinées à punir la contravention à ces interdictions ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la LIGUE DES DROITS DE L'HOMME n'est pas fondée à demander l'annulation du décret attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la LIGUE DES DROITS DE L'HOMME est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la LIGUE DES DROITS DE L'HOMME, au Premier ministre et au garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-06 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'EXPRESSION. - VIOLATION - ABSENCE - INCRIMINATION DE « L'OUTRAGE DU DRAPEAU TRICOLORE » (DÉCRET DU 21 JUILLET 2010).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-09 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. - VIOLATION - ABSENCE - INCRIMINATION DE « L'OUTRAGE DU DRAPEAU TRICOLORE » (DÉCRET DU 21 JUILLET 2010).
</SCT>
<ANA ID="9A"> 26-03-06 Le décret n° 2010-835 du 21 juillet 2010 relatif à l'incrimination de l'outrage au drapeau tricolore a introduit dans le code pénal un article R. 645-15 incriminant « l'outrage au drapeau tricolore ». En prévoyant que le fait de détruire, détériorer ou utiliser de manière dégradante le drapeau tricolore, dans un lieu public ou ouvert au public, ou de diffuser ou faire diffuser l'enregistrement d'images relatives à de tels faits, même commis dans un lieu privé, n'est passible des peines que prévoit le décret que lorsqu'il est commis « dans des conditions de nature à troubler l'ordre public et dans l'intention d'outrager le drapeau tricolore », le pouvoir réglementaire a entendu n'incriminer que les dégradations physiques ou symboliques du drapeau susceptibles d'entraîner des troubles graves à la tranquillité et à la sécurité publiques et commises dans la seule intention de détruire, abîmer ou avilir le drapeau. Ainsi, ce texte n'a pas pour objet de réprimer les actes de cette nature qui reposeraient sur la volonté de communiquer, par cet acte, des idées politiques ou philosophiques ou feraient oeuvre de création artistique, sauf à ce que ce mode d'expression ne puisse, sous le contrôle du juge pénal, être regardé comme une oeuvre de l'esprit. Par suite, compte tenu de ces précisions et malgré la généralité de la définition des actes incriminés, le décret ne porte pas une atteinte excessive à la liberté d'expression garantie par la Déclaration des droits de l'homme et du citoyen et la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9B"> 26-055-01-09 Le décret n° 2010-835 du 21 juillet 2010 relatif à l'incrimination de l'outrage au drapeau tricolore a introduit dans le code pénal un article R. 645-15 incriminant « l'outrage au drapeau tricolore ». En prévoyant que le fait de détruire, détériorer ou utiliser de manière dégradante le drapeau tricolore, dans un lieu public ou ouvert au public, ou de diffuser ou faire diffuser l'enregistrement d'images relatives à de tels faits, même commis dans un lieu privé, n'est passible des peines que prévoit le décret que lorsqu'il est commis « dans des conditions de nature à troubler l'ordre public et dans l'intention d'outrager le drapeau tricolore », le pouvoir réglementaire a entendu n'incriminer que les dégradations physiques ou symboliques du drapeau susceptibles d'entraîner des troubles graves à la tranquillité et à la sécurité publiques et commises dans la seule intention de détruire, abîmer ou avilir le drapeau. Ainsi, ce texte n'a pas pour objet de réprimer les actes de cette nature qui reposeraient sur la volonté de communiquer, par cet acte, des idées politiques ou philosophiques ou feraient oeuvre de création artistique, sauf à ce que ce mode d'expression ne puisse, sous le contrôle du juge pénal, être regardé comme une oeuvre de l'esprit. Par suite, compte tenu de ces précisions et malgré la généralité de la définition des actes incriminés, le décret ne porte pas une atteinte excessive à la liberté d'expression garantie par la Déclaration des droits de l'homme et du citoyen et la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
