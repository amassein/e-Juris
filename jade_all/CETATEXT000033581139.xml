<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033581139</ID>
<ANCIEN_ID>JG_L_2016_12_000000383421</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/58/11/CETATEXT000033581139.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 09/12/2016, 383421</TITRE>
<DATE_DEC>2016-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383421</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383421.20161209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Aménagement services a demandé au tribunal administratif de Nice d'annuler les décisions des 10 mars 2009 et 18 février 2010 par lesquelles le préfet des Alpes-Maritimes l'a exclue du marché d'intérêt national de Nice, avec retrait du contrat d'occupation du 15 mai 2005. Par un jugement nos 0901448, 0902851, 1001382 du 28 juin 2011, le tribunal administratif a annulé la décision du 10 mars 2009 et rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n° 11MA03606 du 2 juin 2014, la cour administrative d'appel de Marseille a, sur appel de la société Aménagement services, annulé le jugement en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 18 février 2010 et annulé cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 août et 4 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la métropole Nice-Côte d'Azur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt, en tant qu'il a annulé la décision du 18 février 2010 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par la société Aménagement services ;<br/>
<br/>
              3°) de mettre à la charge de la société Aménagement services la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le décret n° 2004-374 du 29 avril 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la Métropole Nice-Côte d'Azur et à Me Ricard, avocat de la société Aménagement services.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 novembre 2016, présentée par la société Aménagement services ;<br/>
<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis aux juges du fond que, par une convention du 15 mai 2005 signée avec la société d'économie mixte pour la construction et la gestion des marchés d'intérêt national de Nice (Sominice), la société Aménagement services a été autorisée à exploiter, dans des locaux situés dans l'enceinte du marché aux produits alimentaires, une activité de transport et de recyclage d'emballages et de palettes. Par décision du 18 février 2010, le préfet des Alpes-Maritimes, faisant usage du pouvoir de sanction prévu par l'article R. 761-19 du code de commerce, a prononcé l'exclusion de la société du marché, avec retrait du contrat d'occupation du 15 mai 2005. Par un jugement du 28 juin 2011, le tribunal administratif de Nice a rejeté la demande de la société tendant à l'annulation de la décision du 18 février 2010. Par un arrêt du 2 juin 2014, la cour administrative d'appel de Marseille, après avoir admis l'intervention de la métropole Nice-Côte d'Azur, devenue gestionnaire du marché, et avoir annulé le jugement du tribunal administratif de Nice en tant qu'il avait rejeté la demande de la société tendant à l'annulation de la sanction, a annulé cette sanction. La métropole Nice-Côte d'Azur se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. En vertu des articles L. 761-3 et R. 761-6 du code de commerce, les marchés d'intérêt national sont placés sous la tutelle des ministres chargés du commerce et de l'agriculture, qui peuvent, si l'exploitation financière d'un marché présente ou laisse prévoir un déséquilibre grave, mettre le gestionnaire en demeure de prendre les mesures nécessaires au rétablissement de l'équilibre et, si ce dernier n'a pas pris de telles mesures à l'expiration d'un délai de trois mois à compter de la date de réception de la mise en demeure, relever d'office les redevances existantes, créer des recettes nouvelles, réduire les dépenses et, d'une manière générale, prendre toutes dispositions propres à rétablir l'équilibre. En vertu de l'article 1er du décret du 29 avril 2004 relatif aux pouvoirs des préfets, à l'organisation et à l'action des services de l'Etat dans les régions et départements, le préfet de département représente, dans le département, le Premier ministre et chacun des ministres. En vertu de l'article L. 761-2 du code de commerce, à l'exception des marchés d'intérêt national dont l'Etat assure lui-même la gestion, la gestion de ces marchés est assurée par la commune sur le territoire de laquelle le marché est implanté ou par le groupement de communes intéressé, en régie ou par la désignation d'une personne morale publique ou privée. En vertu de l'article L. 761-10 de ce code, l'organisation générale des marchés d'intérêt national est déterminée par décret en Conseil d'Etat. Aux termes de l'article L. 761-11 du code : " Le préfet exerce les pouvoirs de police dans l'enceinte du marché d'intérêt national (...) ". <br/>
<br/>
              3. Aux termes de l'article R. 761-19 du même code : " Les usagers des marchés d'intérêt national peuvent faire l'objet de sanctions disciplinaires pour infraction aux règles qui régissent le marché. (...) / Ces sanctions sont : / 1° L'avertissement ; / (...) / 3° Le blâme (...) ; / 4° La suspension pour une durée qui ne peut dépasser trois mois ; / 5° L'exclusion comportant, s'il y a lieu, retrait du contrat d'occupation. / L'avertissement et le blâme sont prononcés par le gestionnaire. / La suspension et l'exclusion sont prononcées par le préfet chargé de la police du marché, après avis du conseil de discipline ". Il résulte des termes mêmes du 5° de l'article R. 761-19 que le prononcé de la sanction d'exclusion qu'il prévoit implique nécessairement, lorsque l'usager est titulaire d'un contrat d'occupation, le retrait de ce contrat, alors même qu'il a été conclu par le gestionnaire du marché. <br/>
<br/>
              4. En premier lieu, lorsqu'il est compétent pour fixer certaines règles d'exercice d'une activité, le pouvoir réglementaire l'est également pour prévoir des sanctions administratives qui, par leur objet et leur nature, sont en rapport avec cette réglementation. Le pouvoir réglementaire, compétent, sur le fondement de l'article L. 761-10 du code de commerce, pour fixer l'organisation générale des marchés d'intérêt national, l'est donc également pour définir le régime des sanctions applicables aux usagers de ces marchés en cas d'infraction aux règles qui régissent le marché.<br/>
<br/>
              5. En second lieu, si, en vertu de l'article R. 761-22 du code de commerce, l'autorisation de s'établir sur un marché d'intérêt national est donnée par son gestionnaire, il résulte de l'ensemble des dispositions mentionnées au point 2 que, dans le cas des marchés d'intérêt national dont l'Etat n'a pas conservé la gestion, le préfet, lorsqu'il prononce la sanction prévue au 5° de l'article R. 761-19 du code de commerce, avec retrait du contrat d'occupation, agit comme autorité de tutelle, pour le compte du gestionnaire du marché d'intérêt national. Dès lors, la rupture de la relation contractuelle entre le gestionnaire et l'usager concerné est réputée émaner du gestionnaire du marché, partie au contrat. En conséquence, en jugeant que l'article R. 761-19 du code de commerce était entaché d'illégalité, dès lors qu'en permettant au préfet de retirer une convention légalement conclue entre le gestionnaire du marché d'intérêt national et un usager de ce marché, il portait à la liberté de contracter une atteinte dont le principe aurait dû être prévu dans la loi, la cour a commis une erreur de droit. La métropole Nice-Côte d'Azur est, par suite, fondée à demander l'annulation des articles 2 à 5 de l'arrêt qu'elle attaque, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Aménagement services le versement à la métropole Nice Côte d'Azur de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise au même titre à la charge de la métropole Nice-Côte d'Azur qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 à 5 de l'arrêt du 2 juin 2014 de la cour administrative d'appel de Marseille sont annulés. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La société à responsabilité limitée Aménagement services versera à la métropole Nice-Côte d'Azur la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Aménagement services au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la métropole Nice-Côte d'Azur, à la société à responsabilité limitée Aménagement services, à la société d'économie mixte pour la construction et la gestion des marchés d'intérêt national de Nice, au ministre de l'intérieur et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DU RÈGLEMENT. - 1) EDICTION DES SANCTIONS ADMINISTRATIVES EN RAPPORT AVEC LA RÉGLEMENTATION D'UNE ACTIVITÉ [RJ1] - 2) APPLICATION - CAS DES SANCTIONS ADMINISTRATIVES EN RAPPORT AVEC LA RÉGLEMENTATION RELATIVE AUX MARCHÉS D'INTÉRÊT NATIONAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-05-01 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. GÉNÉRALITÉS. - RÉGLEMENTATION DES MARCHÉS D'INTÉRÊT NATIONAL - SANCTION D'EXCLUSION COMPRENANT, S'IL Y A LIEU, RETRAIT DU CONTRAT D'OCCUPATION PRONONCÉE PAR LE PRÉFET (ART. R. 761-19 DU CODE DE COMMERCE) - SANCTION PRONONCÉE POUR LE COMPTE DU GESTIONNAIRE DU MARCHÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">14-02-01-04 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. MARCHÉS D'INTÉRÊT NATIONAL. - RÉGLEMENTATION DES MARCHÉS D'INTÉRÊT NATIONAL - 1) COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR ÉDICTER DES SANCTIONS ADMINISTRATIVES EN RAPPORT AVEC CETTE RÉGLEMENTATION - EXISTENCE [RJ1] - 2) - SANCTION D'EXCLUSION COMPRENANT, S'IL Y A LIEU, RETRAIT DU CONTRAT D'OCCUPATION PRONONCÉE PAR LE PRÉFET (ART. R. 761-19 DU CODE DE COMMERCE) - SANCTION PRONONCÉE POUR LE COMPTE DU GESTIONNAIRE DU MARCHÉ.
</SCT>
<ANA ID="9A"> 01-02-01-03 1) Lorsqu'il est compétent pour fixer certaines règles d'exercice d'une activité, le pouvoir réglementaire l'est également pour prévoir des sanctions administratives qui, par leur objet et leur nature, sont en rapport avec cette réglementation.... ,,2) Le pouvoir réglementaire, compétent, sur le fondement de l'article L. 761-10 du code de commerce, pour fixer l'organisation générale des marchés d'intérêt national, l'est donc également pour définir le régime des sanctions applicables aux usagers de ces marchés en cas d'infraction aux règles qui régissent le marché.</ANA>
<ANA ID="9B"> 03-05-01 Si, en vertu de l'article R. 761-22 du code de commerce, l'autorisation de s'établir sur un marché d'intérêt national est donnée par son gestionnaire, il résulte des dispositions du code de commerce relatives aux marchés d'intérêt national ainsi que du décret n° 2004-374 du 29 avril 2004 relatif aux pouvoirs des préfets, à l'organisation et à l'action des services de l'Etat que, dans le cas des marchés d'intérêt national dont l'Etat n'a pas conservé la gestion, le préfet, lorsqu'il prononce la sanction prévue au 5° de l'article R. 761-19 du code de commerce, avec retrait du contrat d'occupation, agit comme autorité de tutelle, pour le compte du gestionnaire du marché d'intérêt national. Dès lors, la rupture de la relation contractuelle entre le gestionnaire et l'usager concerné est réputée émaner du gestionnaire du marché, partie au contrat.</ANA>
<ANA ID="9C"> 14-02-01-04 1) Lorsqu'il est compétent pour fixer certaines règles d'exercice d'une activité, le pouvoir réglementaire l'est également pour prévoir des sanctions administratives qui, par leur objet et leur nature, sont en rapport avec cette réglementation. Le pouvoir réglementaire, compétent, sur le fondement de l'article L. 761-10 du code de commerce, pour fixer l'organisation générale des marchés d'intérêt national, l'est donc également pour définir le régime des sanctions applicables aux usagers de ces marchés en cas d'infraction aux règles qui régissent le marché.,,,2) Si, en vertu de l'article R. 761-22 du code de commerce, l'autorisation de s'établir sur un marché d'intérêt national est donnée par son gestionnaire, il résulte des dispositions du code de commerce relatives aux marchés d'intérêt national ainsi que du décret n° 2004-374 du 29 avril 2004 relatif aux pouvoirs des préfets, à l'organisation et à l'action des services de l'Etat que, dans le cas des marchés d'intérêt national dont l'Etat n'a pas conservé la gestion, le préfet, lorsqu'il prononce la sanction prévue au 5° de l'article R. 761-19 du code de commerce, avec retrait du contrat d'occupation, agit comme autorité de tutelle, pour le compte du gestionnaire du marché d'intérêt national. Dès lors, la rupture de la relation contractuelle entre le gestionnaire et l'usager concerné est réputée émaner du gestionnaire du marché, partie au contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 7 juillet 2004, Ministre de l'intérieur, de la sécurité intérieure et des libertés locales c/ Benkerrou, n° 255136, p. 297.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
