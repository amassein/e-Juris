<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255232</ID>
<ANCIEN_ID>JG_L_2014_07_000000380271</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 16/07/2014, 380271, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380271</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2014:380271.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Saint-Léger-de-Balson (33113), représentée par son maire ; la commune de Saint-Léger-de-Balson demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-192 du 20 février 2014 portant délimitation des cantons dans le département de la Gironde ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la même loi du 17 mai 2013, applicable à la date du décret attaqué : " I.- Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / II. - La qualité de chef-lieu de canton est maintenue aux communes qui la perdent dans le cadre d'une modification des limites territoriales des cantons, prévue au I, jusqu'au prochain renouvellement général des conseils généraux. / III. La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants. / IV. Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              3. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Gironde, compte tenu de l'exigence de réduction du nombre des cantons de ce département de soixante-trois à trente-trois résultant de l'article L. 191-1 du code électoral ; <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              4. Considérant qu'aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des maires, des conseillers municipaux, des présidents d'établissements publics de coopération intercommunale, de la commission départementale de coopération intercommunale, des présidents de schémas de cohérence territoriale, non plus qu'à la consultation individuelle des conseillers généraux du département indépendamment de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales ; que la requête ne peut, à cet égard et en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire ; <br/>
<br/>
              5. Considérant qu'aucune disposition législative ou réglementaire ne fait en principe obstacle à ce que le préfet assiste aux débats du conseil général ; qu'il ne ressort pas des pièces du dossier que la circonstance que le préfet de la Gironde a, sur l'invitation du président du conseil général, présenté le projet de délimitation des cantons et assisté aux débats et au vote public du conseil général, aurait en l'espèce été de nature à entacher d'irrégularité la consultation du conseil général ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales prévoient que le conseil général rend son avis dans un délai de six semaines, faute de quoi son avis est réputé rendu ; que ces dispositions ne sauraient faire obstacle à ce que le conseil général émette son avis avant l'expiration de ce délai de six semaines ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              7. Considérant qu'il résulte de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton ; qu'il ne peut être apporté à ces règles que des exceptions de portée limitée, spécialement justifiées ;<br/>
<br/>
              8. Considérant que la commune requérante, qui ne conteste pas que le Premier ministre a fait application des règles qui s'imposaient à lui en vertu de ces dispositions, et quand bien même elle estimerait que le rattachement de communes à d'autres cantons aurait été préférable, n'apporte pas d'élément de nature à établir que les choix auxquels il a ainsi été procédé reposeraient sur une erreur manifeste d'appréciation ;<br/>
<br/>
              9. Considérant que la désignation comme bureau centralisateur du canton n° 15 (Les Landes de Graves) de la commune de Salles, qui est la commune la plus peuplée du nouveau canton, n'est pas entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la commune de Saint-Léger-de-Balson n'est pas fondée à demander l'annulation pour excès de pouvoir du décret qu'elle attaque ;<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la commune de Saint-Léger-de-Balson est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Saint-Léger-de-Balson. Copie en sera adressée au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
