<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220692</ID>
<ANCIEN_ID>JG_L_2018_07_000000409390</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/07/2018, 409390</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409390</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; LE PRADO ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Cédric  Zolezzi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409390.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de la Polynésie française de condamner le centre hospitalier de la Polynésie française (CHPF) à l'indemniser des préjudices subis lors de sa prise en charge le 13 août 2008 dans cet établissement. Par un jugement n° 1400415 du 10 mars 2015, le tribunal administratif a condamné le CHPF à verser la somme de 9 535 800 francs CFP à M. B...sous déduction des sommes versées à titre de provision et à verser la somme de 330 983 francs CFP à la caisse de prévoyance sociale de la Polynésie française en remboursement de ses débours. <br/>
<br/>
              Par un arrêt n° 15PA02282 du 30 décembre 2016, la cour administrative d'appel de Paris a, sur appel du CHPF, ramené la somme due à M. B...à 1 919 000 francs CFP, sous déduction de la somme de 2 090 000 francs CFP versée à titre de provision. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 30 mars, 3 juillet 2017 et 22 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du CHPF la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cédric Zolezzi, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M.B..., à Me Le Prado, avocat du centre hospitalier de la Polynésie française et à la SCP Baraduc, Duhamel, Rameix, avocat de la Caisse de prévoyance sociale de la Polynésie Française.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., né le 1er décembre 1955, qui présentait une névralgie cervico-brachiale invalidante due à une hernie discale située entre les vertèbres C6 et C7, a subi, le 13 août 2008, au Centre hospitalier de la Polynésie française (CHPF), une discectomie avec mise en place d'une cage inter-somatique ; que cette intervention a été pratiquée par erreur entre les vertèbres C7 et T1 ; que, par un jugement du 10 mars 2015, le tribunal administratif de la Polynésie française, saisi par l'intéressé, a condamné le CHPF à lui verser une indemnité de 9 535 800 francs CFP ; que, par un arrêt du 30 décembre 2016, la cour administrative d'appel de Paris a limité cette indemnisation à la somme de 1 919 000 francs CFP, sous déduction des sommes de 2 000 000 francs CFP et 90 000 francs CFP précédemment versées par le centre hospitalier à titre de provisions ;<br/>
<br/>
              2. Considérant que lorsqu'une intervention destinée à remédier à un handicap échoue parce qu'elle a été conduite dans des conditions fautives, le patient peut prétendre à une indemnisation réparant, outre les troubles liés à l'intervention inutile et ses éventuelles conséquences dommageables, les préjudices résultant de la persistance de son handicap, dans la limite de la perte de chance de guérison qu'il a subie, laquelle doit être évaluée en fonction de la probabilité du succès d'une intervention correctement réalisée ; que la circonstance qu'une intervention réparatrice demeure possible ne fait pas obstacle à l'indemnisation, dès lors que l'intéressé n'est pas tenu de subir une telle intervention, mais justifie seulement qu'elle soit limitée aux préjudices déjà subis à la date du jugement, à l'exclusion des préjudices futurs, qui ne peuvent pas être regardés comme certains à cette date et pourront seulement, le cas échéant, faire l'objet de demandes ultérieures ;<br/>
<br/>
              3. Considérant que, par des motifs non contestés par le pourvoi, la cour administrative d'appel a retenu que l'intervention fautive réalisée le 13 août 2008 au CHPF avait entraîné pour M.B..., compte tenu des chances de succès d'une intervention réalisée correctement, une perte de chance, évaluée à 95 %, de guérir de sa hernie discale ou au moins d'obtenir une amélioration de son état de santé ;<br/>
<br/>
              Sur les préjudices professionnels : <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., professeur des écoles et directeur d'un établissement d'éducation spécialisée, avait été placé, en raison des troubles liés à sa pathologie, en congé de maladie ordinaire à compter d'avril 2007, puis en congé de longue maladie à demi-traitement à compter d'avril 2008 ; qu'il faisait valoir qu'en cas de succès de l'intervention pratiquée le 13 août 2008, il aurait pu reprendre son activité professionnelle au 1er novembre 2008 et demandait en conséquence que soit mise à la charge du centre hospitalier la réparation de la perte de rémunération qu'avait entraînée pour lui, à compter de cette date, son maintien en congé à demi-traitement ; que, pour rejeter ces conclusions, la cour administrative d'appel a retenu que le placement à demi-traitement, antérieur à l'intervention fautive, ne pouvait être regardé comme étant en lien direct avec celle-ci mais résultait de la pathologie initiale, qui n'avait pas été aggravée ; qu'en se prononçant ainsi, alors qu'elle retenait par ailleurs que la faute commise avait entraîné la perte d'une chance d'amélioration de l'état de santé et que si M. B...avait été privé d'une chance de reprendre son activité professionnelle, il pouvait prétendre, dans la limite de cette perte de chance, à l'indemnisation de ses pertes de revenus, la cour a commis une erreur de droit ; <br/>
<br/>
              5. Considérant, en second lieu, que M. B...faisait par ailleurs valoir que l'échec de l'intervention avait eu pour conséquence sa mise à la retraite anticipée, intervenue le 1er septembre 2011, avant qu'il ait pu accéder au grade de professeur des écoles hors classe ; que, pour rejeter ses conclusions tendant à ce que le centre hospitalier soit condamné à lui verser une indemnité au titre d'une incidence professionnelle, la cour s'est également fondée sur l'absence de lien direct entre ce préjudice et la faute commise ; qu'en se prononçant ainsi, sans rechercher si l'intéressé avait été privé d'une chance de poursuivre son activité au-delà de l'âge de 60 ans, elle a commis une erreur de droit ; <br/>
<br/>
              Sur les autres chefs de préjudice : <br/>
<br/>
              6. Considérant que la cour administrative d'appel a ramené de 2 000 000 francs CFP à 1 500 000 francs CPF l'évaluation du préjudice subi par M. B...en raison du déficit fonctionnel permanent ; que la cour n'a pas soulevé d'office le moyen tiré du caractère excessif du montant de l'indemnisation alloué par les premiers juges, ce montant ayant été contesté devant elle par le CHPF ; que M. B...n'est, dès lors, pas fondé à soutenir qu'elle aurait ainsi commis une erreur de droit et méconnu son office en donnant une portée erronée au principe, qui revêt un caractère d'ordre public, selon lequel une personne publique ne peut jamais être condamnée à payer une somme qu'elle ne doit pas ; <br/>
<br/>
              7. Mais considérant que le taux de 95 % retenu par la cour, correspondant à une perte de chance d'amélioration de l'état de santé de l'intéressé, ne pouvait concerner l'indemnisation des troubles subis du fait de l'intervention chirurgicale et des séquelles qu'elles avaient provoquées, ces préjudices étant tout entiers en lien direct avec la faute commise ; que ce taux n'avait, par suite, pas vocation à s'appliquer au calcul des sommes dues à l'intéressé au titre du déficit fonctionnel temporaire subi à la suite de l'intervention et du déficit fonctionnel permanent résultant d'une perte de mobilité du rachis cervical qu'il avait entraînée ; qu'il ne pouvait davantage concerner le remboursement des honoraires versés au médecin qui avait assisté l'intéressé au cours des opérations d'expertise ; qu'en limitant l'indemnisation de ces chefs de préjudice à 95 % de leur montant, la cour a commis une erreur de droit ; <br/>
<br/>
              Sur la déduction d'une provision :<br/>
<br/>
              8. Considérant que la cour administrative d'appel a déduit de l'indemnisation versée à M. B...la somme de 90 000 francs CFP allouée à l'expert par l'ordonnance du 8 avril 2011 du juge des référés du tribunal administratif de la Polynésie française ; que cependant, en opérant la déduction de cette somme, qui n'avait pas été versée à M.B..., mais avait au contraire été mise à sa charge par l'ordonnance du 8 avril 2011, avant d'être mise à la charge du centre hospitalier par d'autres ordonnances, la cour administrative d'appel a dénaturé les pièces du dossier ;   <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. B...n'est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'il attaque qu'en tant, d'une part, qu'il statue sur l'indemnisation de ses préjudices professionnels, d'autre part, qu'il fixe l'indemnité due au titre du déficit fonctionnel temporaire, du déficit fonctionnel permanent et des honoraires versés à un médecin ayant assisté l'intéressé au cours des opérations d'expertise et, enfin, qu'il déduit une somme de 90 000 francs CFP de l'indemnisation allouée ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHPF le versement à M. B...d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 30 décembre 2016 est annulé en tant, d'une part, qu'il statue sur l'indemnisation des préjudices professionnels de M.B..., d'autre part, qu'il fixe l'indemnité due au titre du déficit fonctionnel temporaire, du déficit fonctionnel permanent et des honoraires versés à un médecin ayant assisté l'intéressé au cours des opérations d'expertise et, enfin, qu'il déduit une somme de 90 000 francs CFP de l'indemnisation allouée. <br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris dans la limite de la cassation prononcée à l'article 1er.<br/>
Article 3 : Le centre hospitalier de la Polynésie française versera à M. B...une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au centre hospitalier de la Polynésie française et à la caisse de prévoyance sociale de la Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. CARACTÈRE INDEMNISABLE DU PRÉJUDICE - QUESTIONS DIVERSES. - CAS D'ÉCHEC FAUTIF D'UNE INTERVENTION DESTINÉE À REMÉDIER À UN HANDICAP - 1) PRÉJUDICES LIÉS À L'INTERVENTION INUTILE ET SES CONSÉQUENCES - EXISTENCE - PRÉJUDICES RÉSULTANT DE LA PERSISTANCE DU HANDICAP - EXISTENCE, DANS LA LIMITE DE LA PERTE DE CHANCE DE GUÉRISON - 2) CIRCONSTANCE QU'UNE INTERVENTION RÉPARATRICE DEMEURE POSSIBLE - CONSÉQUENCE - LIMITATION DE L'INDEMNISATION AUX PRÉJUDICES DÉJÀ SUBIS À LA DATE DU JUGEMENT.
</SCT>
<ANA ID="9A"> 60-04-01-04 1) Lorsqu'une intervention destinée à remédier à un handicap échoue parce qu'elle a été conduite dans des conditions fautives, le patient peut prétendre à une indemnisation réparant, outre les troubles liés à l'intervention inutile et ses éventuelles conséquences dommageables, les préjudices résultant de la persistance de son handicap, dans la limite de la perte de chance de guérison qu'il a subie, laquelle doit être évaluée en fonction de la probabilité du succès d'une intervention correctement réalisée.... ...2) La circonstance qu'une intervention réparatrice demeure possible ne fait pas obstacle à l'indemnisation, dès lors que l'intéressé n'est pas tenu de subir une nouvelle intervention, mais justifie seulement qu'elle soit limitée aux préjudices déjà subis à la date du jugement, à l'exclusion des préjudices futurs, qui ne peuvent pas être regardés comme certains à cette date et pourront seulement, le cas échéant, faire l'objet de demandes ultérieures.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
