<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080570</ID>
<ANCIEN_ID>JG_L_2018_06_000000411466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/05/CETATEXT000037080570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 18/06/2018, 411466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411466.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 12 juin, 4 octobre et 23 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat des inspecteurs généraux et inspecteurs de l'administration du développement durable demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 19 avril 2017 portant nomination de Mme D...B...en qualité d'inspectrice générale de l'administration du développement durable.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 84-834 du 13 septembre 1984 ;<br/>
              - le décret n° 94-1085 du 14 décembre 1994 ;<br/>
              - le décret n° 2005-367 du 21 avril 2005 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, en premier lieu, qu'aux termes de l'article 8 de la loi du 13 septembre 1984 relative à la limite d'âge dans la fonction publique et le secteur public : " A l'exception de ceux de ces corps dont la mission le justifie et dont la liste est déterminée par décret en Conseil d'Etat, les statuts particuliers des corps d'inspection et de contrôle doivent prévoir la possibilité de pourvoir aux vacances d'emploi dans le grade d'inspecteur général ou de contrôleur général par décret en conseil des ministres sans condition autre que d'âge. (...) / (...) les nominations prononcées au titre de l'alinéa précédent ne peuvent intervenir qu'après consultation d'une commission chargée d'apprécier l'aptitude des intéressés à exercer les fonctions d'inspecteur général ou de contrôleur général en tenant compte de leurs fonctions antérieures et de leur expérience. L'avis de la commission est communiqué à l'intéressé sur sa demande. Le sens de l'avis de la commission sur les nominations prononcées est publié au Journal officiel en même temps que l'acte de nomination. La composition et les modalités de fonctionnement de la commission qui comporte des membres du corps concerné élus par leurs pairs, sont fixées par décret en Conseil d'Etat " ; qu'en vertu du III de l'article 4 du décret du 21 avril 2005 relatif au statut particulier du corps des inspecteurs généraux et inspecteurs de l'administration du développement durable, un emploi vacant sur six emplois d'inspecteurs généraux de l'administration du développement durable peut être pourvu dans les conditions fixées à l'article 8 de la loi du 13 septembre 1984 susvisée, sous la seule condition d'être âgé de quarante-cinq ans révolus ; <br/>
<br/>
              2.	Considérant que, par décret du 19 avril 2017, le Président de la République a nommé Mme D...B...en tant qu'inspectrice générale de l'administration du développement durable, sur le fondement des dispositions précédemment citées du III de l'article 4 du décret du 21 avril 2005 ; que le syndicat des inspecteurs généraux et inspecteurs de l'administration du développement durable demande l'annulation pour excès de pouvoir de ce décret ; <br/>
<br/>
              3.	Considérant, en premier lieu, qu'en vertu de l'article 2 du décret du 14 décembre 1994 relatif aux modalités de nomination au tour extérieur dans certains corps d'inspection et de contrôle de la fonction publique de l'Etat, la commission prévue au deuxième alinéa de l'article 8 de la loi du 13 septembre 1984 est présidée par un membre ou ancien membre du Conseil d'Etat ayant au moins le grade de conseiller d'Etat et comprend un magistrat ou ancien magistrat de la Cour des comptes, ayant au moins le grade de conseiller maître, le directeur général de l'administration et de la fonction publique, l'inspecteur général chargé des fonctions de chef du service d'inspection ou, s'il n'existe pas un tel emploi, un inspecteur général désigné par le ministre qui a autorité sur le corps et deux inspecteurs généraux en activité élus, pour trois ans, au scrutin uninominal à un tour par les inspecteurs généraux en position d'activité ou de détachement ; que le dernier alinéa de cet article 2 précise que le secrétariat de la commission est assuré par la direction générale de l'administration et de la fonction publique ; <br/>
<br/>
              4.	Considérant, d'une part, qu'il ressort des pièces du dossier que M. E..., inspecteur général de l'administration du développement durable, a été régulièrement désigné en qualité de membre suppléant de la commission par une décision du 4 mai 2015, signée par M. A...C..., sous-directeur de la modernisation et de la gestion statutaires à la direction des ressources humaines du ministère de l'écologie, du développement durable et de l'énergie, qui avait reçu délégation à cet effet par décision du 30 mai 2013, publiée au Journal officiel du 2 juin 2013 ; <br/>
<br/>
              5.	Considérant, d'autre part, qu'il appartenait aux services de la direction générale de l'administration et de la fonction publique, ainsi qu'ils l'ont fait, d'adresser aux membres de la commission la convocation pour la réunion qui s'est tenue le 7 avril 2017 ; qu'aucun texte n'imposant de délais particuliers pour convoquer la réunion de la commission, la circonstance que celle-ci ait été invitée à délibérer dans des délais très brefs en vue d'émettre son avis n'est pas de nature, dans les circonstances de l'espèce, à faire regarder la nomination de l'intéressée comme entachée d'irrégularité ; qu'il ressort, en outre, des pièces versées au dossier que les membres de la commission ont reçu, en temps utile, les éléments leur permettant de se prononcer sur les aptitudes de l'intéressée ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que le syndicat requérant n'est pas fondé à soutenir que la consultation de la commission prévue à l'article 8 de la loi du 13 septembre 1984 préalablement à l'intervention du décret qu'il attaque se serait déroulée dans des conditions irrégulières ; <br/>
<br/>
              7.	Considérant, en second lieu, qu'il ne ressort pas des pièces du dossier que, compte tenu tant des attributions confiées aux inspecteurs généraux de l'administration du développement durable que des qualifications et de l'expérience de MmeB..., notamment dans des collectivités territoriales et au ministère chargé du développement durable, la nomination de l'intéressée en qualité d'inspectrice générale de l'administration du développement durable soit entachée d'une erreur manifeste d'appréciation ; <br/>
<br/>
              8.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par MmeB..., que le syndicat requérant n'est pas fondé à demander l'annulation pour excès de pouvoir du décret qu'il attaque ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat des inspecteurs généraux et inspecteurs de l'administration du développement durable est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat des inspecteurs généraux et inspecteurs de l'administration du développement durable, à Mme D...B..., au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
