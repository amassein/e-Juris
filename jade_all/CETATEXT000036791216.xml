<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036791216</ID>
<ANCIEN_ID>JG_L_2018_04_000000412111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/79/12/CETATEXT000036791216.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 11/04/2018, 412111, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412111.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 15 000 euros en réparation du préjudice subi du fait de l'absence de proposition de relogement. Par un jugement n° 1520846/3-3 du 11 avril 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 4 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner l'Etat à verser à M. B...la somme de 15 000 euros avec intérêts de droit à compter du 11 juin 2015 et capitalisation des intérêts ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Delvolvé et Trichet, au titre des dispositions des articles L. 761-1 du code de justice administrative  et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...a été reconnu comme prioritaire et devant être relogé en urgence, sur le fondement de l'article L. 411-2-3 du code de la construction et de l'habitation, par une décision du 14 mars 2014 de la commission de médiation de Paris, au motif qu'il était menacé d'expulsion sans relogement ; que, par un jugement du 11 juin 2015, le tribunal administratif de Paris, saisi par M. B... sur le fondement du I de l'article L. 441-2-3-1 du même code, a enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer son relogement ; que, le 22 décembre 2015, l'intéressé a demandé au tribunal de réparer le préjudice subi du fait de l'absence de relogement ; qu'il se pourvoit en cassation contre le jugement du 11 avril 2017 par lequel le tribunal administratif a rejeté cette demande au motif que le fait d'être menacé d'expulsion ne constituait pas, en lui-même, un préjudice indemnisable ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement ; <br/>
<br/>
              3. Considérant qu'il suit de là qu'ayant constaté que le préfet n'avait pas proposé un relogement à M. B...dans le délai prévu par le code de la construction et de l'habitation à compter de la décision de la commission de médiation, le tribunal administratif de Paris ne pouvait, sans commettre une erreur de droit, juger que cette carence, constitutive d'une faute de nature à engager la responsabilité de l'Etat, ne causait à l'intéressé aucun préjudice indemnisable, alors qu'il était constant que la situation qui avait motivé la décision de la commission perdurait et que M. B...justifiait de ce fait de troubles dans ses conditions d'existence lui ouvrant droit à réparation dans les conditions indiquées au point 2 ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le jugement attaqué doit être annulé ;<br/>
<br/>
              4. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Delvolvé-Trichet, avocat de M.B..., renonce à percevoir, la somme correspondant à la part contributive de l'Etat, de mettre à la charge l'Etat la somme de 2 000 euros à verser à cette société ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 11 avril 2017 du tribunal administratif de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la SCP Delvolvé-Trichet la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10  juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
