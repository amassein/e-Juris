<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037653058</ID>
<ANCIEN_ID>JG_L_2018_11_000000425100</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/30/CETATEXT000037653058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 22/11/2018, 425100, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425100</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Edmond Honorat</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:425100.20181122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Centre Zahra France " a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'arrêté du 15 octobre 2018 par lequel le préfet du Nord a prononcé la fermeture administrative, pour une durée de six mois, du lieu de culte " Centre Zahra " à Grande-Scynthe. Par une ordonnance n° 1809278 du 19 octobre 2018, le juge des référés du tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 29 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, l'association " Centre Zahra France " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de suspendre l'exécution de l'arrêté du 15 octobre 2018 portant fermeture administrative du lieu de culte " Centre Zahra " à Grande-Scynthe ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - son appel est recevable ;<br/>
              - la condition d'urgence est remplie dès lors qu'une mesure de fermeture d'un lieu de culte prise sur le fondement de l'article L. 227-1 du code de la sécurité intérieure constitue par elle-même une atteinte à la liberté de culte d'une gravité telle qu'elle suffit à caractériser une situation d'urgence ; qu'en outre, en l'absence de lieu de culte chiite autre que le " Centre Zahra " dans un rayon de deux cents kilomètres, l'atteinte à la liberté de culte est, en l'espèce, particulièrement caractérisée ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à la liberté de culte ;<br/>
              - il est entaché d'irrégularité dès lors que sa motivation est imprécise et lacunaire ; <br/>
              - il est entaché d'une erreur de droit dès lors que, d'une part, la mise en oeuvre des dispositions de l'article L. 227-1 du code de la sécurité intérieure interprétées par le Conseil constitutionnel est subordonnée à l'existence de propos provoquant à la violence, à la haine ou à la discrimination et d'un lien entre ceux-ci et le risque de commission d'actes de terrorisme, lien qu'il incombe au préfet d'établir, et que, d'autre part, le préfet du Nord n'a pas recherché en quoi les prétendues provocations à la haine, à la violence ou à la discrimination sur lesquelles il a fondé sa décision seraient en lien avec le risque de commission d'actes de terrorisme ;<br/>
              - il est entaché d'une erreur de fait dès lors qu'ont été découvertes sur le site du centre non pas cinq, mais deux armes à feu ; <br/>
              - il est entaché d'une erreur de qualification juridique dès lors que les différents éléments sur lesquels se fonde le préfet ne sauraient constituer par eux-mêmes une provocation à la commission d'actes de terrorisme ou une apologie du terrorisme de la part des responsables du " centre Zahra " ; <br/>
              - il est entaché d'une erreur manifeste d'appréciation dès lors que, en premier lieu, aucun message justifiant le djihad armé ni aucun prêche incitant au djihad armé n'ont été diffusés dans le lieu de culte ; en deuxième lieu, il n'existe aucun exemple d'embrigadement ou de départ de fidèles du " Centre Zahra " à destination des zones de combat islamiques ; en troisième lieu, le " Centre Zahra " poursuit ses activités dans un esprit pacifique ; en quatrième et dernier lieu, les deux armes à feu découvertes relèvent d'une initiative d'autoprotection. <br/>
<br/>
              Par un mémoire en défense, enregistré le 12 novembre 2018, le ministre de l'intérieur conclut au rejet de la requête. Il soutient qu'aucun des moyens de la requête n'est fondé.<br/>
<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité intérieure ;<br/>
              - la décision n° 2017-695 QPC du 29 mars 2018 du Conseil constitutionnel ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " Centre Zahra France " et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 16 novembre 2018 à 14 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants de l'association " Centre Zahra France " ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              Sur le cadre juridique applicable au litige :<br/>
              2. Considérant qu'aux termes de l'article L. 227-1 du code de la sécurité intérieure: " Aux seules fins de prévenir la commission d'actes de terrorisme, le représentant de l'Etat dans le département ou, à Paris, le préfet de police peut prononcer la fermeture des lieux de culte dans lesquels les propos qui sont tenus, les idées ou théories qui sont diffusées ou les activités qui se déroulent provoquent à la violence, à la haine ou à la discrimination, provoquent à la commission d'actes de terrorisme ou font l'apologie de tels actes. / Cette fermeture, dont la durée doit être proportionnée aux circonstances qui l'ont motivée et qui ne peut excéder six mois, est prononcée par arrêté motivé et précédée d'une procédure contradictoire dans les conditions prévues au chapitre II du titre II du livre Ier du code des relations entre le public et l'administration. / L'arrêté de fermeture est assorti d'un délai d'exécution qui ne peut être inférieur à quarante-huit heures, à l'expiration duquel la mesure peut faire l'objet d'une exécution d'office. Toutefois, si une personne y ayant un intérêt a saisi le tribunal administratif, dans ce délai, d'une demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative, la mesure ne peut être exécutée d'office avant que le juge des référés ait informé les parties de la tenue ou non d'une audience publique en application du deuxième alinéa de l'article L. 522-1 du même code ou, si les parties ont été informées d'une telle audience, avant que le juge ait statué sur la demande " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions législatives ainsi que de l'interprétation que le Conseil constitutionnel en a donnée dans sa décision n° 2017-695 QPC du 29 mars 2018, que la mesure de fermeture d'un lieu de culte ne peut être prononcée qu'aux fins de prévenir la commission d'actes de terrorisme et que les propos tenus en ce lieu, les idées ou théories qui y sont diffusées ou les activités qui s'y déroulent doivent soit constituer une provocation à la violence, à la haine ou à la discrimination en lien avec le risque de commission d'actes de terrorisme, soit provoquer à la commission d'actes de terrorisme ou en faire l'apologie ;  <br/>
<br/>
              4. Considérant que la liberté du culte confère à toute personne le droit d'exprimer les convictions religieuses de son choix et emporte la libre disposition des biens nécessaires à l'exercice du culte, sous les réserves du respect de l'ordre public ; qu'ainsi, un arrêté prescrivant la fermeture d'un lieu de culte, qui affecte l'exercice du droit de propriété, est susceptible de porter atteinte à cette liberté fondamentale ; qu'il appartient au juge des référés de s'assurer, en l'état de l'instruction devant lui, qu'en prescrivant la fermeture d'un lieu de culte sur le fondement de l'article L. 227-1 du code de la sécurité intérieure, l'autorité administrative, opérant la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public, n'a pas porté d'atteinte grave et manifestement illégale à une liberté fondamentale, que ce soit dans son appréciation de la menace que constitue le lieu de culte ou dans la détermination des modalités de la fermeture ; <br/>
<br/>
              Sur le litige en référé :<br/>
              5. Considérant que par un arrêté du 15 octobre 2018, le préfet du Nord a prononcé la fermeture du lieu de culte " Centre Zahra " sis 1, impasse Jean-Baptiste Lebas à Grande-Synthe sur le fondement des dispositions de l'article L. 227-1 du code de la sécurité intérieure, pour une durée de six mois ; que l'association " Centre Zahra France "  relève appel de l'ordonnance du 19 octobre 2018 par laquelle le juge des référés du tribunal administratif de Lille a rejeté sa demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à la suspension de l'exécution de cet arrêté ; <br/>
              6. Considérant que l'arrêté contesté est principalement motivé par la tenue, au cours de prêches, de propos tendant à légitimer le djihad armé, s'accompagnant d'un endoctrinement de la jeunesse, la diffusion de tels propos, la mise à la disposition des fidèles fréquentant ce lieu de culte et la mise en ligne sur les sites internet de l'association requérante ou de ceux d'associations présentes sur le même lieu et avec laquelle elles ont des dirigeants communs, d'écrits comportant des passages appelant à la violence, à la haine, à la discrimination ainsi qu'à la commission d'actes de terrorisme ; <br/>
              7. Considérant qu'il résulte de l'instruction et d'une note de renseignement du 4 avril 2018, précise et circonstanciée, qui a été versée au contradictoire, que le " centre Zahra " met à disposition des fidèles ou diffuse des ouvrages, comme ceux de  M. A...ou de l'ayatollah Khomeiny, ou encore la revue " Binour ", qui comportent des incitations à la haine ou à la violence ainsi que des propos antisémites ; que, si l'association soutient que ces ouvrages sont à la disposition du public dans d'autres lieux que le " centre Zahra ", elle ne conteste pas sérieusement que ces ouvrages comportent des passages d'incitation à la haine ou à la violence et ne conteste pas utilement leur prise en compte par l'autorité administrative pour l'édiction de la mesure contestée ; qu'il n'apparaît pas non plus, contrairement à ce qui est soutenu, que les écrits ou vidéos appelant à la disparition d'Israël puissent être regardés comme exclusivement antisionistes ainsi qu'en attestent, par exemple, les commentaires présents sur les sites internet mentionnés dans la note de renseignement, ouvertement antisémites et appelant à la destruction d'Israël ; que le préfet a pu prendre en compte ces nombreux commentaires émis en réaction aux propos ou écrits ainsi diffusés afin d'établir le caractère provocateur de ceux-ci alors même que de tels commentaires sont régulièrement supprimés par les gestionnaires des sites ; <br/>
<br/>
              8. Considérant qu'il résulte également de cette note et de l'instruction qu'au travers des prêches qui y sont assurés, le " centre Zahra " légitime le djihad armé, comme peuvent en attester certains passages des prêches du 22 décembre 2017 et 5 janvier 2018, au cours desquels il est présenté comme faisant partie de l'islam, sans que cette présentation en explicite le contexte, en particulier historique ; que cette légitimation du djihad armé, associée à une glorification des actions de la branche armée du Hezbollah, organisation inscrite sur la liste des organisations terroristes établie par l'Union européenne, est présentée dans des vidéos destinées à la " jeunesse de Zahra " ; qu'est ainsi mis en scène, dans une vidéo datée du 5 novembre 2017, un combattant musulman dont l'adversaire est un soldat américain et qu'est reproduit, dans une vidéo mise en ligne le 12 février 2018, un poème glorifiant la mort en martyr, ces images pouvant être perçues par les jeunes qui en sont les destinataires comme présentant des modèles à suivre ; <br/>
<br/>
              9. Considérant qu'au vu de ces différents faits et éléments, le préfet du Nord, qui a suffisamment motivé son arrêté, a pu, sans commettre d'erreur de droit ou de fait, estimer que les propos tenus, les idées et théories diffusées par le " centre Zahra " ainsi que les activités qui s'y déroulent constituaient des provocations justifiant, en vue de prévenir la commission d'actes de terrorisme, la fermeture de ce lieu de culte sur le fondement de l'article L. 227-1 du code de la sécurité intérieure ; qu'en prenant la mesure contestée, il n'a pas porté une atteinte manifestement illégale à la liberté de culte, alors même qu'aucun autre lieu de ce culte n'existe dans la région ; que, par suite, l'association requérante n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lille a rejeté sa demande ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la requête de l'association " Centre Zahra France " doit être rejetée, y compris en ce qu'elle tend à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association " Centre Zahra France " est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association " Centre Zahra France " et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
