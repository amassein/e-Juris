<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601170</ID>
<ANCIEN_ID>JG_L_2014_10_000000367625</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/11/CETATEXT000029601170.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 01/10/2014, 367625, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367625</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Denis Combrexelle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367625.20141001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 9 avril, 24 septembre et 11 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des pharmaciens indépendants de la Réunion demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2012-1096 du 28 septembre 2012 relatif à l'approvisionnement en médicaments à usage humain. <br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - la directive 2001/83/CE du Parlement Européen et du Conseil du 6 novembre 2001 ; <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2011-2012 du 29 décembre 2011 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Denis Combrexelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 5124-17-2 du code de la santé publique, dans sa rédaction issue de la loi du 29 décembre 2011, les grossistes-répartiteurs qui assurent la distribution des médicaments " sont tenus de respecter sur leur territoire de répartition les obligations de service public déterminées par décret en Conseil d'Etat. / Ils assurent l'approvisionnement  continu du marché national  de manière à couvrir les besoins des patients  sur leur territoire de répartition ". <br/>
<br/>
              2. En premier lieu, sur le fondement de ces dispositions, le décret attaqué du 28 septembre 2012, par le 5° de son article 5, insère un nouvel alinéa au sein de l'article R. 5124-59 du même code, qui dispose que : " Les médicaments achetés par le grossiste-répartiteur ou cédés au grossiste-répartiteur sont distribués de manière à couvrir les besoins des patients en France, sur le territoire de répartition déclaré ".<br/>
<br/>
              3. Les dispositions réglementaires citées ci-dessus définissent les obligations de service public que doivent respecter les grossistes-répartiteurs, en leur imposant, au nom des exigences de santé publique, de fournir en priorité et sans rupture d'approvisionnement leur territoire de répartition, et n'ont ni pour objet ni pour effet de leur interdire toute exportation de médicaments. Par suite, elles ne placent, par elles-mêmes, ni les laboratoires qui fabriquent les médicaments ni les intermédiaires du commerce de gros de ces produits en situation d'exploiter une position dominante de façon abusive ou d'avoir des pratiques constitutives de restrictions à la libre circulation des marchandises au sein de l'Union européenne. Dès lors, le moyen tiré de ce que le décret attaqué aurait, pour ce motif, méconnu les stipulations du traité sur le fonctionnement de l'Union européenne et les directives 89/105/CEE et 2001/83/CE sur le prix et le commerce des médicaments à usage humain doit, en tout état de cause, être écarté. La circonstance que des opérateurs privés intervenant sur le marché du médicament puissent, par certaines de leurs pratiques, méconnaître ces règles, ce qui serait de nature à engager leur responsabilité, est sans incidence sur la légalité du décret attaqué.<br/>
<br/>
              4. Pour les mêmes motifs,  le syndicat requérant n'est, en tout état de cause, pas fondé à soutenir que les dispositions qu'il critique méconnaîtraient les dispositions des articles L. 245-6 du code de la sécurité sociale et L. 5121-17 et L. 5123-1 du code de la santé publique issues de la loi du 29 décembre 2011, qui reconnaissent une faculté d'exportation aux grossistes-répartiteurs autorisés.  <br/>
<br/>
              5. En deuxième lieu, il résulte des termes mêmes de l'article L. 5124-17-2 du code de la santé publique, dans sa rédaction applicable à la date du décret attaqué, que les grossistes-répartiteurs sont tenus de couvrir prioritairement les besoins des patients sur leur territoire de répartition. Contrairement à ce que soutient le syndicat requérant, il ne résulte d'aucune disposition du décret attaqué que les officines de pharmacie de la Réunion ne pourraient s'approvisionner auprès de grossistes-répartiteurs installés sur le territoire métropolitain, dès lors que leur territoire de répartition inclurait la Réunion, ou qu'il serait fait application des dispositions des deux derniers alinéas de l'article R. 5124-59 du code de la santé publique. Si le 6° de l'article 5 du décret introduit à ce même article l'obligation pour les grossistes-répartiteurs, dans le cadre du système d'astreinte inter-entreprises sur leur territoire de répartition, de livrer dans les délais et au maximum dans les huit heures les médicaments demandés par le préfet ou par le pharmacien d'officine assurant le service de garde, il ne ressort pas des pièces du dossier que le pouvoir réglementaire aurait ainsi imposé aux grossistes-répartiteurs des obligations de service public manifestement excessives.<br/>
<br/>
              6. En dernier lieu, le syndicat requérant ne peut utilement se plaindre des modalités d'organisation à la Réunion du système d'astreinte imposé aux grossistes-répartiteurs, qui sont sans incidence sur la légalité du décret attaqué.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le Syndicat des pharmaciens indépendants de la Réunion n'est pas fondé à demander l'annulation du décret qu'il attaque. Par suite, sa requête doit être rejetée, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre des affaires sociales et de  la santé.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Syndicat des pharmaciens indépendants de la Réunion est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat des pharmaciens indépendants de la Réunion, au Premier ministre et à la  ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
