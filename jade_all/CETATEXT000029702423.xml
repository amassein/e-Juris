<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029702423</ID>
<ANCIEN_ID>JG_L_2014_10_000000380089</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/24/CETATEXT000029702423.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 24/10/2014, 380089, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380089</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:380089.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 380089, la requête, enregistrée le 24 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...E..., demeurant ...-La-Clouère (86160) ; M. E...demande au Conseil d'Etat d'annuler, pour excès de pouvoir, le décret n° 2014-264 du 26 février 2014 portant délimitation des cantons dans le département de la Vienne ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 381106, la requête sommaire et le mémoire complémentaire, enregistrés les 10 juin et 27 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentés par M. G...F..., élisant domicile..., 15 rue de Vaugirard à Paris (75291) ; M. F...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler, pour excès de pouvoir la décision du 7 avril 2014 par laquelle le ministre de l'intérieur a rejeté son recours gracieux formé contre le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 3°, sous le n° 381182, la requête, enregistrée le 11 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le département de la Vienne, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, la décision du 7 avril 2014 par laquelle le ministre de l'intérieur a rejeté son recours gracieux formé contre le même décret ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de reprendre la procédure de délimitation des cantons avant les prochaines échéances électorales ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code électoral, notamment son article L. 191-1 ;<br/>
<br/>
              Vu le code général des collectivités territoriales, notamment ses articles L. 3113-1 et L. 3113-2 ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le décret n° 2013-938 du 18 octobre 2013, modifié par le décret                 n° 2014-112 du 6 février 2014 ;<br/>
<br/>
              Vu le décret n° 2013-1289 du 27 décembre 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que M.D..., sénateur de la Vienne, justifie d'un intérêt suffisant à l'annulation du décret attaqué ; qu'ainsi son intervention au soutien de la requête de M. F...est recevable ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; que ces dispositions impliquent qu'il soit procédé à une nouvelle délimitation de l'ensemble des circonscriptions cantonales, qui sera applicable à compter du prochain renouvellement général des conseils généraux, fixé, à la date de la présente décision, au mois de mars 2015 ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de la même loi du 17 mai 2013, applicable à la date du décret attaqué : " I.- Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / II. - La qualité de chef-lieu de canton est maintenue aux communes qui la perdent dans le cadre d'une modification des limites territoriales des cantons, prévue au I, jusqu'au prochain renouvellement général des conseils généraux. / III. La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants. / IV. Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              4. Considérant que le décret attaqué a, en application de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Vienne, compte tenu de l'exigence de réduction du nombre des cantons de ce département de trente-huit à dix-neuf résultant de l'article L. 191-1 du code électoral ; que, par décisions du 7 avril 2014, le ministre de l'intérieur a rejeté les recours gracieux présentés par M. F... et par le département de la Vienne ;  <br/>
<br/>
              5. Considérant que le moyen tiré de ce que le jugement par des membres de la section du contentieux du Conseil d'Etat des présentes requêtes dirigées contre un décret pris après avis de la section de l'intérieur du Conseil d'Etat, méconnaîtrait le droit des requérants à être jugé par un tribunal indépendant et impartial garanti par les stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut, en tout état de cause, qu'être écarté, dès lors que les membres du Conseil d'Etat statuant sur ces requêtes n'ont, ainsi que l'implique l'article R. 122-21-1 du code de justice administrative, pas pris part à la délibération de l'avis rendu sur le décret attaqué ;<br/>
<br/>
              Sur la légalité externe du décret du 26 février 2014 portant délimitation des cantons dans le département de la Vienne :<br/>
<br/>
              6. Considérant, en premier lieu, qu'il résulte des termes mêmes des dispositions législatives citées ci-dessus qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons ; que le moyen tiré de ce que qu'il était incompétent pour procéder à une nouvelle délimitation d'une telle ampleur ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un " bureau centralisateur " sans mentionner les chefs-lieux de canton est, en tout état de cause, sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département de la Vienne ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement son exécution ; que le décret attaqué du 26 février 2014, qui se limite à modifier les circonscriptions électorales du département de la Vienne, n'appelle aucune mesure d'exécution que le garde des sceaux, ministre de la justice serait compétent pour signer ou contresigner ; qu'il suit de là que ce décret n'avait pas à être contresigné par ce ministre ;<br/>
<br/>
              9. Considérant, en quatrième lieu, qu'aucune disposition constitutionnelle, législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des communes, des établissements publics de coopération intercommunale à fiscalité propre ou des " principaux élus " du département indépendamment de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales ; que la requête ne peut, à cet égard et en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire ;<br/>
<br/>
              10. Considérant, en cinquième lieu, qu'il n'est pas contesté qu'un projet de décret, accompagné d'un exposé des motifs rappelant les grands principes de la révision de la carte cantonale, une carte représentant les nouveaux cantons, une carte pour chaque commune fractionnée indiquant la méthode de fractionnement retenue, un tableau synthétique indiquant pour chaque canton sa population légale en 2013, le nombre de communes le composant et l'écart à la moyenne démographique départementale et un tableau par communes regroupées par nouveau canton ont été soumis au conseil général ; que sur cette base, l'assemblée départementale a été mise à même d'émettre un avis sur les modalités de mise en oeuvre de la nouvelle délimitation des cantons prévue par le législateur et de faire des propositions spécifiques pour le département de la Vienne ; que, par suite, les requérants ne sont pas fondés à soutenir que la consultation du conseil général aurait été irrégulière ;  <br/>
<br/>
              11. Considérant, en sixième lieu, que l'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'une décision doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par cette décision ; que si les limites territoriales des cantons du département de la Vienne, telles qu'elle résultent du décret attaqué, ne sont pas identiques en tous points à celles prévues dans le projet soumis pour consultation au conseil général, il ne saurait sérieusement être soutenu que l'assemblée départementale n'a pas été saisie de l'ensemble des questions posées par le projet dès lors que celui-ci tendait au redécoupage de tous les cantons du département ; que les requérants ne sont ainsi pas fondés à soutenir que le conseil général aurait dû être consulté une nouvelle fois compte tenu des modifications apportées au projet après la consultation et avant de recueillir l'avis du Conseil d'Etat ;<br/>
<br/>
              Sur la légalité interne du décret du 26 février 2014 portant délimitation des cantons dans le département de la Vienne :<br/>
<br/>
              12. Considérant que si M. F...soutient que les articles L. 3113-1,  L. 3113-2 du code général des collectivités territoriales ainsi que l'article L. 191-1 du code électoral ne seraient pas conformes à plusieurs règles et principes de valeur constitutionnelle, notamment le principe d'égalité devant le suffrage, il n'appartient pas au Conseil d'Etat, en dehors des cas et conditions où il est saisi sur le fondement de l'article 61-1 de la Constitution, de se prononcer sur un moyen tiré de la non-conformité à la Constitution de dispositions législatives ; <br/>
<br/>
              En ce qui concerne le nombre de cantons :<br/>
<br/>
              13. Considérant qu'il résulte des dispositions de L. 191-1 du code électoral, citées au point 3, que le législateur a déterminé lui-même le nombre de cantons par département ; que, par suite, il n'appartenait pas au Gouvernement de modifier ce nombre pour rééquilibrer le nombre d'habitants par canton sur l'ensemble du territoire national ; que, dès lors, le moyen tiré de ce que le décret serait illégal faute d'un tel rééquilibrage ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne les données démographiques prises en considération :<br/>
<br/>
              14. Considérant, en premier lieu, que, dans sa rédaction issue de l'article 8 du décret du 6 février 2014, l'article 71 du décret du 18 octobre 2013 prévoit que, pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2 du code général des collectivités territoriales - lequel dispose, ainsi qu'il a été dit au point 3, que la délimitation des cantons doit être faite sur des bases essentiellement démographiques - " (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret             n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ; que, par suite, les requérants ne sont pas fondés à soutenir que le décret attaqué aurait également dû prendre en considération le nombre d'électeurs par canton ; que le moyen tiré de ce que le calcul précis de la population des cantons urbains serait impossible, compte tenu de l'absence de concordance entre la méthode de comptage retenue par l'Institut national de la statistique et des études économiques (INSEE) dans les secteurs urbains et la délimitation cantonale opérée par le décret, n'est pas assorti de précision permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              15. Considérant, en deuxième lieu, que les requérants soutiennent, par la voie de l'exception, que l'article 8 du décret du 6 février 2014 qui a modifié celui du 18 octobre 2013 serait illégal en ce qu'il renvoie aux chiffres authentifiés par le décret du 27 décembre 2012 ; que, toutefois, d'abord, ce décret n'appelle aucune mesure d'exécution que le ministre chargé de l'économie ou le ministre chargé de l'outre-mer seraient compétents pour signer ou contresigner ; qu'il n'avait donc pas à être contresigné par ces ministres ; qu'ensuite, la délimitation des nouvelles circonscriptions cantonales devait, conformément aux dispositions de l'article 7 de la loi du 11 décembre 1990, être effectuée au plus tard un an avant le prochain renouvellement général des conseils généraux ; que, dans les circonstances de l'espèce, eu égard, d'une part, aux délais inhérents à l'élaboration de l'ensemble des projets de décrets de délimitation des circonscriptions cantonales, à la consultation des conseils généraux et à la saisine pour avis du Conseil d'Etat, d'autre part, à la circonstance que la détermination à l'échelon infra-communal des chiffres de population applicables à compter du 1er janvier 2014, nécessaire à la délimitation de certains cantons, n'était pas disponible à la date à laquelle devait être entreprise la délimitation des nouvelles circonscriptions cantonales, le décret du 6 février 2014 a pu légalement prévoir que le chiffre de population municipale auquel il convenait de se référer était le chiffre authentifié par le décret du 27 décembre 2012 et non celui arrêté par le décret du 27 décembre 2013, qui authentifie les chiffres de population auxquels il convient, en principe, de se référer pour l'application des lois et règlements à compter du 1er janvier 2014 ; que, par ailleurs, la circonstance que les élections municipales de 2014 aient été réalisées sur la base des chiffres issus du décret du 27 décembre 2013 n'est pas de nature à porter atteinte au principe d'égalité devant le suffrage ; que, de même, si les requérants soutiennent que les dispositions attaquées méconnaissent les dispositions du décret du 27 décembre 2013 ainsi que celles de l'article R. 25-1 du code électoral, qui déterminent les chiffres de population de référence en matière électorale, il peut, en tout état de cause, être dérogé à ces dispositions par des dispositions législatives ou règlementaires, comme le rappelle d'ailleurs l'article 3 du décret du 27 décembre 2013 ; qu'enfin, les dispositions dont l'illégalité est invoquée ne concernant que la délimitation des nouvelles circonscriptions cantonales, le moyen tiré de ce qu'elles auraient des conséquences sur l'application des règles relatives aux dépenses de campagne ne peut qu'être écarté ; que, par suite, les requérants ne sont pas fondés à soutenir que le décret du 6 février 2014 serait illégal en ce qu'il renvoie aux chiffres authentifiés par le décret du 27 décembre 2012 ni que le décret attaqué serait illégal au motif qu'il a délimité les nouveaux cantons sur la base de ces données et non de celles authentifiées par le décret du 27 décembre 2013 ;<br/>
<br/>
              16. Considérant, en troisième lieu, que la légalité d'un décret s'apprécie à la date à laquelle il a été pris ; que, par suite, le département de la Vienne ne peut utilement soutenir, à l'appui d'une demande tendant à l'annulation du décret en cause et non d'une décision ultérieure refusant de l'abroger, que le décret contesté serait devenu illégal en raison d'un changement de circonstance découlant des chiffres de la population du département issus du décret du 27 décembre 2013 ;<br/>
<br/>
              En ce qui concerne la délimitation des cantons :<br/>
<br/>
              17. Considérant, en premier lieu, qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives et des circonscriptions judiciaires, les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou les limites des " bassins de vie " définis par l'INSEE ; que, de même, si l'article L. 192 du code électoral relatif aux modalités de renouvellement des conseils généraux, faisait référence aux arrondissements, dans sa rédaction antérieure à l'intervention de la loi du 17 mai 2013, aucun texte en vigueur à la date du décret contesté ne mentionne ces arrondissements, circonscriptions administratives de l'Etat, pour la détermination des limites cantonales ; que de même, ni les dispositions de l'article L. 3113-2 du code général des collectivité territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Gouvernement de prendre comme critères de délimitation de ces circonscriptions électorales les limites des anciens cantons ; que, par suite, les requérants ne saurait utilement se prévaloir de ce que la délimitation de plusieurs cantons du département de la Vienne ne correspondrait pas à celle d'autres circonscriptions électorales ou de subdivisions administratives ;<br/>
<br/>
              18. Considérant, en deuxième lieu, que si M. F...soutient que les cantons urbains sont en moyenne moins peuplés que les cantons ruraux, il n'apporte au soutien de cette allégation aucune précision permettant d'en apprécier le bien-fondé ;<br/>
              En ce qui concerne le détournement de pouvoir :<br/>
<br/>
              19. Considérant que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              20. Considérant qu'il résulte de tour ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le ministre de l'intérieur, les requérants ne sont pas fondés à demander l'annulation du décret attaqué ni, par voie de conséquence, des décisions rejetant leurs recours gracieux, sans que les vices propres dont ces décisions serait entachées puissent être utilement invoqués ; que leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de M. D...est admise.<br/>
<br/>
Article 2 : Les requêtes de M.E..., de M. F...et du département de la Vienne sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...E..., à M. G...F..., au département de la Vienne, à M. C...D...et au ministre de l'intérieur.<br/>
      Copie en sera adressée pour information au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
