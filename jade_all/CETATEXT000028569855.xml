<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569855</ID>
<ANCIEN_ID>JG_L_2014_02_000000365583</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/98/CETATEXT000028569855.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 05/02/2014, 365583, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365583</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365583.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12VE04359 du 22 janvier 2013, enregistrée le 29 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par le ministre de l'économie et des finances ;<br/>
<br/>
              Vu le pourvoi, enregistré le 15 novembre 2012 au greffe de la cour administrative d'appel de Versailles, et le nouveau mémoire, enregistré le 30 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par le ministre délégué, chargé du budget ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1106180 du 13 juillet 2012 par lequel le tribunal administratif de Montreuil, statuant sur les demandes de la société JM Bruneau, l'a déchargée des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2008 et 2009 dans les rôles de la commune de Villebon-sur-Yvette ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rétablir la société JM Bruneau au rôle de la taxe foncière sur les propriétés bâties au titre des années 2008 et 2009 à concurrence des dégrèvements prononcés en première instance ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 janvier 2014, présentée pour la société JM Bruneau ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société JM Bruneau ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 1499 du code général des impôts : " La valeur locative des immobilisations industrielles passibles de la taxe foncière sur les propriétés bâties est déterminée en appliquant au prix de revient de leurs différents éléments, revalorisé à l'aide des coefficients qui avaient été prévus pour la révision des bilans, des taux d'intérêt fixés par décret en Conseil d' Etat (...) " ; que revêtent un caractère industriel, au sens de ces dispositions, les établissements dont l'activité nécessite d'importants moyens techniques, non seulement lorsque cette activité consiste en la fabrication ou la transformation de biens corporels mobiliers, mais aussi lorsque le rôle des installations techniques, matériels et outillages mis en oeuvre, fût-ce pour les besoins d'une autre activité, est prépondérant ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société JM Bruneau, qui exerce une activité de vente à distance de mobilier, de matériel et de fournitures de bureau, dispose d'un établissement dans la zone industrielle de Courtaboeuf à Villebon-sur-Yvette (Essonne) ; qu'à l'occasion d'un contrôle, l'administration a estimé que cet établissement devait être regardé comme industriel au sens de l'article 1499 du code général des impôts et a mis en recouvrement des cotisations supplémentaires de taxe foncière sur les propriétés bâties au titre des années 2008 et 2009 ; que, par le jugement attaqué du 13 juillet 2012, le tribunal administratif de Montreuil a déchargé la société de ces impositions ; <br/>
<br/>
              3. Considérant que, pour juger que l'établissement de la société JM Bruneau ne présentait pas un caractère industriel au sens des dispositions de l'article 1499 du code général des impôts, le tribunal administratif a relevé que les matériels, fixes et mobiles, de stockage, de levage et de distribution de marchandises ainsi que les quais de chargement présents dans les entrepôts de la société jouaient un rôle prépondérant dans l'activité de logistique mais ne jouaient pas un tel rôle dans l'activité principale de vente à distance s'exerçant dans les mêmes locaux, qui employait 433 des 686 salariés ; qu'en dissociant ainsi l'activité de logistique et l'activité administrative et de prise des commandes, sans prendre en compte la part respective des surfaces dédiées aux deux composantes de l'activité de la société pour apprécier le caractère prépondérant des moyens techniques mis en oeuvre dans l'établissement, le tribunal administratif a commis une erreur de droit ; que, par suite, le ministre est fondé à demander l'annulation du jugement attaqué en tant qu'il a prononcé la décharge des cotisations supplémentaires de taxe foncière sur les propriétés bâties au titre des années 2008 et 2009 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que la surface totale de l'établissement représente 81 039 mètres carrés, dont les trois-quarts sont dédiés à l'activité de logistique ; que, dans ses entrepôts, la société JM Bruneau dispose pour recevoir, stocker et distribuer les produits qu'elle commercialise d'un tapis roulant de 1780 mètres, de quais de chargement,  d'équipements fixes et mobiles de stockage, de levage et de distribution des marchandises comprenant notamment 130 engins de manutention et 390 remorques de préparation ; que la valeur de ces équipements est estimée à environ 9 millions d'euros ; que ces importants moyens techniques jouent un rôle prépondérant dans l'activité de logistique, qui assure le traitement de plus d'un million de commandes et emploie plus de deux cents personnes ; que si les deux tiers de l'effectif sont affectés aux activités de télévente et téléprospection, les surfaces dédiées à ces activités ainsi qu'à la gestion de l'entreprise ne représentent qu'une part minime de celle de l'ensemble de l'établissement ; que la circonstance que l'activité de logistique pourrait être sous-traitée est sans influence sur le caractère industriel de l'établissement au sens des dispositions de l'article 1499 du code général des impôts, dès lors qu'elle est le support nécessaire de l'activité de vente à distance qui s'exerce sur le même site ; qu'il résulte de ce qui précède que la société JM Bruneau n'est pas fondée à demander la décharge des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie pour les années 2008 et 2009 ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil du 13 juillet 2012 est annulé en tant qu'il a prononcé la décharge des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles a été assujettie la société JM Bruneau au titre des années 2008 et 2009.<br/>
Article 2 : La demande présentée sur ce point par la société JM Bruneau est rejetée.<br/>
Article 3 : Les conclusions présentées par la société JM Bruneau au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société JM Bruneau.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
