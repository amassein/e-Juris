<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042532343</ID>
<ANCIEN_ID>JG_L_2020_11_000000445992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/53/23/CETATEXT000042532343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/11/2020, 445992, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445992.20201112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 5 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) à titre principal, d'ordonner la suspension de l'exécution de l'article 34 du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au Premier ministre de modifier les dispositions de l'article 34 de ce décret afin de limiter le recours au télé-enseignement. <br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - les dispositions contestées portent une atteinte grave et manifestement illégale à l'égal accès à l'instruction ;<br/>
              - elles méconnaissent l'article L. 123-4 du code de l'éducation dès lors que le télé-enseignement, imposé par la fermeture des établissements universitaires, préjudicie aux étudiants souffrant de difficultés sociales, matérielles et financières, aux étudiants étrangers et aux étudiants en situation de handicap ou de fragilité psychologique et, par suite, accroît le décrochage scolaire ;<br/>
              - elles méconnaissent le principe d'égalité dès lors qu'est autorisée l'ouverture des établissements de l'enseignement secondaire et des classes préparatoires aux grandes écoles ou en spécialités " brevet de technicien supérieur ", que le télé-enseignement ne peut être considéré comme une alternative équivalente à l'enseignement en présentiel et que, d'un point de vue sanitaire, il n'existe pas de situation différente avec les établissements de l'enseignement supérieur ;<br/>
              - elles méconnaissent l'article L. 3131-16 du code de la santé publique dès lors qu'elles sont disproportionnées, inappropriées et non-nécessaires pour poursuivre l'objectif de lutte contre l'épidémie covid-19 ;<br/>
              - la condition d'urgence est satisfaite eu égard à la gravité de l'atteinte qui est portée à une liberté fondamentale. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ; <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - la loi n° 2020-856 du 9 juillet 2020 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2020-1331 du 2 novembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19: " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " l'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. (...) /La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Enfin, il résulte de l'article L. 3131-15 du même code dans sa rédaction issue de la loi du 9 juillet 2020organisant la sortie de l'état d'urgence sanitaire que " dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, activités et réunions strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ". <br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire aux articles L. 3131-12 à L. 3131-20 du code de la santé publique et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence. <br/>
<br/>
              4. Une nouvelle progression de l'épidémie a conduit le Président de la République à prendre, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique cités au point 2 ci-dessus, le décret du 14 octobre 2020 déclarant l'état d'urgence à compter du 17 octobre à 0 heure sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du même code, le décret contesté prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. Ce décret a été modifié par le décret du 2 novembre 2020.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              5. M. A... B... demande au juge des référés du Conseil d'Etat statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'ordonner la suspension de l'exécution de l'article 34 du décret du 29 octobre 2020 restreignant l'accueil des usagers dans les établissements d'enseignement supérieur mentionnés au livre VII de la troisième partie du code de l'éducation, et, à titre subsidiaire, d'enjoindre au Premier ministre de modifier les dispositions de cet article aux fins de limiter le recours au télé-enseignement. Il soutient que l'enseignement à distance porte une atteinte grave et manifestement illégale à l'égal accès à l'instruction et au principe d'égalité devant la loi. <br/>
<br/>
              6. Aux termes de l'article 34 du décret contesté modifié par le 3° du I de l'article 1er du décret du 2 novembre 2020 : " L'accueil des usagers dans les établissements d'enseignement supérieur mentionnés au livre VII de la troisième partie du code de l'éducation est autorisé aux seules fins de permettre l'accès :/ 1° Aux formations lorsqu'elles ne peuvent être effectuées à distance compte tenu de leur caractère pratique dont la liste est arrêtée par le recteur de région académique ;/ 2° Aux laboratoires et unités de recherche pour les doctorants ; 3° Aux bibliothèques et centres de documentation, sur rendez-vous ainsi que pour le retrait et la restitution de documents réservés;/ 4° Aux services administratifs, uniquement sur rendez-vous ou sur convocation de l'établissement ;/ 5° Aux services de médecine préventive et de promotion de la santé, aux services sociaux et aux activités sociales organisées par les associations étudiantes ;/ 6° Aux locaux donnant accès à des équipements informatiques, uniquement sur rendez-vous ou sur convocation de l'établissement ;/ 7° Aux exploitations agricoles mentionnées à l'article L. 812-1 du code rural et de la pêche maritime ".<br/>
<br/>
              7. En premier lieu, les restrictions d'accès, pour faire face à l'aggravation de la crise sanitaire, aux établissements d'enseignement supérieur mentionnés au livre VII de la troisième partie du code de l'éducation et la poursuite des enseignements dispensés par ces établissements à distance ne portent pas atteinte à une liberté fondamentale dont la sauvegarde est susceptible de donner lieu au prononcé de mesures sur le fondement de l'article L. 521-2 du code de justice administrative. Au demeurant, les dispositions contestées, explicitées par la circulaire de la ministre de l'enseignement supérieur, de la recherche et de l'innovation du 30 octobre 2020, prévoient notamment que les enseignements dont le caractère pratique rend impossible de les effectuer à distance restent délivrés sur site et que les étudiants ne disposant pas de l'équipement ou de la connexion nécessaires au suivi des enseignements à distance bénéficient d'un accès prioritaire aux salles de travail équipées en matériel informatique ou permettant un accès à internet. Par ailleurs, les bibliothèques et centres de documentation universitaires restent accessibles sur rendez-vous ainsi que pour le retrait et la restitution de documents réservés.<br/>
<br/>
              8. En second lieu, si certaines discriminations peuvent constituer des atteintes à une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative, eu égard aux motifs qui les inspirent ou aux effets qu'elles produisent sur l'exercice d'une telle liberté, la méconnaissance du principe d'égalité ne révèle pas, par elle-même, une atteinte de cette nature. Par suite, la circonstance que les étudiants de l'enseignement supérieur scolarisés au sein des établissements de l'enseignement secondaire puissent bénéficier d'un enseignement sur site ne peut conduire, en tout état de cause, le juge des référés à ordonner des mesures sur le fondement de l'article L. 521-2 précité. <br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition relative à l'urgence, que la requête doit être rejetée, y compris les conclusions subsidiaires aux fins d'injonction, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....  <br/>
Copie en sera adressée au Premier ministre, au ministre des solidarités et de la santé et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
