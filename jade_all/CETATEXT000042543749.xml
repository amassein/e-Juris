<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042543749</ID>
<ANCIEN_ID>JG_L_2020_11_000000445944</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/37/CETATEXT000042543749.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/11/2020, 445944, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445944</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445944.20201117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de constater que le département du Finistère, en prenant le 16 septembre 2020 la décision de mettre fin à sa prise en charge au titre de l'aide sociale à l'enfance, a porté une atteinte grave et manifestement illégale à l'exigence impérative reconnue de protéger tout enfant, et notamment au droit de celui-ci à un hébergement d'urgence et, d'autre part, d'enjoindre au département du Finistère, à titre principal, d'assurer la poursuite de sa prise en charge en famille d'accueil chez Mme A... D..., à partir de la date de la décision contestée, dans un délai de trois heures à compter de l'ordonnance et jusqu'à l'intervention de la décision de l'autorité judiciaire compétente, sous astreinte de 200 euros par jour de retard, à titre subsidiaire, d'assurer en fonction de ses besoins et de sa situation, son hébergement, sa vêture, sa nourriture et la poursuite de son éducation dans l'établissement scolaire qui l'accueille, dans le même délai et sous la même astreinte. Par une ordonnance n° 2004476 du 21 octobre 2020, le juge des référés du tribunal administratif de Rennes a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 4 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de lui accorder l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ; <br/>
<br/>
              3°) de faire droit à sa demande de première instance ;<br/>
              4°) mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors que, en tant que mineur, il entend uniquement se prévaloir des dispositions du code de l'action sociale et des familles lui permettant de bénéficier d'une prise en charge provisoire jusqu'à la décision de l'autorité judiciaire ; <br/>
              - la condition d'urgence est remplie en ce que, en premier lieu, sa minorité n'est contestée ni par le département ni par une décision d'une autorité judiciaire compétente, en deuxième lieu, il est confronté à un risque immédiat de mise en danger de sa santé ou de sa sécurité, en troisième lieu, eu égard aux dispositions de l'article R. 221-11 du code de l'action sociale et des familles, le département ne pouvait prendre une décision de fin de prise en charge et, en dernier lieu, il se trouve dans une situation de précarité et de particulière vulnérabilité en l'absence de prise en charge et de tout moyen de subsistance dès lors que la décision de sa famille d'accueil de l'héberger est révocable, provisoire et limitée par ses capacités financières, cette famille ne recevant plus aucune aide financière pour son accueil ; <br/>
              - contrairement à ce qu'a jugé le juge des référés du tribunal administratif de Rennes, il est porté une atteinte grave et manifestement illégale au droit à l'hébergement et à une prise en charge adaptée à la situation d'un mineur isolé, en méconnaissance des dispositions des articles L. 223-2, L. 226-4 et R. 221-11 du code de l'action sociale et des familles, au droit d'être entendu, au droit au recours effectif à un juge, au principe de la dignité de la personne humaine, au droit de ne pas subir des traitements inhumains et dégradants et à l'intérêt supérieur de l'enfant dès lors que la décision de fin de prise en charge contestée a été prise en vertu d'une situation qui a été directement créée par les omissions du conseil départemental, lesquelles constituent par ailleurs une carence caractérisée dans l'accomplissement de ses missions.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - le code de procédure civile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2016-840 du 24 juin 2016 ;<br/>
              - l'arrêté du 17 novembre 2016 pris en application du décret n° 2016-840 du 24 juin 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.  Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Il résulte de l'instruction que M. B..., ressortissant malien, déclarant être né le 23 octobre 2005, est arrivé à Paris en février 2019. Par un jugement en assistance éducative du 7 mars 2019, le juge pour enfants du tribunal judiciaire de Quimper a maintenu le placement de l'intéressé auprès du service de l'aide sociale à l'enfance du département du Finistère, décidé par une ordonnance de placement provisoire du procureur de la République du 27 février 2019. Par un jugement du 28 février 2020, le juge aux affaires familiales du même tribunal, statuant comme juge des tutelles, a rejeté la demande du département du Finistère tendant à bénéficier d'une délégation totale de l'autorité parentale. Par une décision du 16 septembre 2020, le département a décidé de mettre un terme, à compter du 17 septembre, à la prise en charge de M. B... au titre de l'aide sociale à l'enfance et en particulier à son hébergement en famille d'accueil. <br/>
              3. M. B... relève appel de l'ordonnance du 21 octobre 2020 par laquelle le juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au département du Finistère, à titre principal, d'assurer la poursuite de sa prise en charge en famille d'accueil, à titre subsidiaire, d'assurer en fonction de ses besoins et de sa situation, son hébergement, sa vêture, sa nourriture et la poursuite de son éducation dans l'établissement scolaire qui l'accueille. <br/>
              4. Aux termes de l'article 375 du code civil : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public. (...) / La décision fixe la durée de la mesure sans que celle-ci puisse excéder deux ans. La mesure peut être renouvelée par décision motivée. (...) ". Aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : (...) 3° A un service départemental de l'aide sociale à l'enfance ; (...) ". Aux termes du premier alinéa de l'article 375-5 du même code : " A titre provisoire mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4. ". <br/>
<br/>
              5. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, (...) / ; 3° Mener en urgence des actions de protection en faveur des mineurs mentionnés au 1° du présent article ; / 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil (...) ".<br/>
<br/>
              6. Il résulte de ces dispositions qu'il incombe aux autorités du département, le cas échéant dans les conditions prévues par la décision du juge des enfants ou par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire, de prendre en charge l'hébergement et de pourvoir aux besoins des mineurs confiés au service de l'aide sociale à l'enfance. A cet égard, une obligation particulière pèse sur ces autorités lorsqu'un mineur privé de la protection de sa famille est sans abri et que sa santé, sa sécurité ou sa moralité est en danger. Lorsqu'elle entraîne des conséquences graves pour le mineur intéressé, une carence caractérisée dans l'accomplissement de cette mission porte une atteinte grave et manifestement illégale à une liberté fondamentale. Il incombe au juge des référés d'apprécier, dans chaque cas, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              7. M. B... fait valoir que la décision du département du Finistère mettant fin à sa prise en charge au titre de l'aide sociale à l'enfance le place dans une situation de mise en danger de sa santé et de sa sécurité justifiant d'enjoindre au département de poursuivre son accueil provisoire en famille d'accueil. Toutefois, la condition d'urgence prévue par l'article L. 521-2 du code de justice administrative ne peut être regardée comme remplie dès lors, d'une part, qu'il est constant que M. B... continue à être hébergé, à titre bénévole, par sa famille d'accueil et, d'autre part, que si M. B... indique qu'il a saisi, le 12 octobre 2020, le juge des enfants aux fins de sa prise en charge au titre de l'aide sociale à l'enfance, il lui appartient de demander à ce juge, en procédure d'urgence, au besoin en renouvelant sa saisine, d'ordonner sa prise en charge provisoire par le département sur le fondement des dispositions combinées des articles 375-3 et 375-5 du même code.<br/>
<br/>
              8. Par suite, il y a lieu de rejeter la requête de M. B..., y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... B....<br/>
Copie en sera adressée au département du Finistère. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
