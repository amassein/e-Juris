<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659636</ID>
<ANCIEN_ID>JG_L_2020_12_000000430516</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 10/12/2020, 430516, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430516</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430516.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Avenir Automobile 78 a demandé au tribunal administratif de Limoges d'annuler le titre exécutoire émis à son encontre le 18 août 2014 par le président directeur général de l'Agence de services et de paiement, pour un montant de 777 000 euros, et de la décharger de l'obligation de payer résultant de ce titre. Par un jugement n° 1401783 du 23 février 2017, le tribunal administratif de Limoges a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX01303 du 5 mars 2019, rectifié par une ordonnance du 5 avril 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Avenir Automobile 78 contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 mai et 6 août 2019 au secrétariat du contentieux du Conseil d'Etat, la société Avenir Automobile 78 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence de services et de paiement la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2007-1824 du 25 décembre 2007 ;<br/>
              - le décret n° 2007-1873 du 26 décembre 2007 ;<br/>
              - l'arrêté du 26 décembre 2007 relatif aux modalités de gestion de l'aide à l'acquisition de véhicules propres ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Avenir Automobile 78 et à la SCP Buk Lament - Robillot, avocat de l'Agence de services et de paiement ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Avenir Automobile 78 a conclu une convention avec le Centre national pour l'aménagement des structures des exploitations agricoles, intégré depuis dans l'Agence de services et de paiement, aux fins de mettre en oeuvre les dispositions de l'article 8 du décret du 26 décembre 2007 instituant une aide à l'acquisition des véhicules propres et  permettant au vendeur d'un véhicule neuf de faire bénéficier son client de l'avance du montant de l'aide. Dans ce cadre, la société Avenir Automobile 78 a reçu de l'Agence des services et de paiement la somme de 777 000 euros, en remboursement de l'avance du même montant qu'elle a accordée à la société Evercar France pour l'achat de 111 véhicules électriques entre mars et juillet 2013. A la suite d'une procédure de contrôle, l'Agence de services et de paiement a exigé de la société Avenir Automobile 78 le remboursement de cette somme et émis à son encontre un titre exécutoire d'un montant de 777 000 euros.<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 26 décembre 2007  instituant une aide à l'acquisition des véhicules propres : " Une aide est attribuée par le fonds d'aide à l'acquisition de véhicules propres créé par l'article 63 de la loi n° 2007-1824 du 25 décembre 2007 de finances rectificative pour 2007 à toute personne justifiant d'un domicile ou d'un établissement en France, qui acquiert ou qui prend en location dans le cadre d'un contrat de location avec option d'achat dont la durée, pour les contrats signés après le 10 novembre 2013, ne peut être inférieure à deux ans, ou d'un contrat souscrit pour une durée d'au moins deux ans un véhicule automobile terrestre à moteur qui satisfait, à la date de sa facturation, aux conditions suivantes : (...) 4° Il n'est pas destiné à être cédé par l'acquéreur en tant que véhicule neuf ; (...) ". Aux termes de l'article 8 de ce décret : " En dehors de la procédure de paiement de droit commun, consistant dans le paiement direct au bénéficiaire, chaque vendeur ou loueur ou installateur agréé peut demander à passer avec le directeur général de l'Agence de services et de paiement une convention aux termes de laquelle l'avance de l'aide pourra être faite par le titulaire de la convention ou par son réseau, le titulaire de la convention en obtenant ensuite le remboursement par le fonds d'aide à l'acquisition de véhicules propres. Ces conventions peuvent être passées entre le directeur général de l'Agence de services et de paiement et chaque constructeur ou importateur, ou, dans le cas des départements d'outre-mer, avec le ou les représentants de chaque marque. "<br/>
<br/>
              3. Aux termes de l'article 3 de l'arrêté du 26 décembre 2007 relatif aux modalités de gestion de l'aide à l'acquisition des véhicules propres : " Dans le cas où le vendeur du véhicule neuf (...) fait, comme cela est prévu par l'article 8 du décret du 26 décembre 2007 susvisé, l'avance du montant de l'aide, il exige du bénéficiaire de l'aide les pièces justificatives nécessaires à la constitution du dossier prévu à l'article 1er ou à l'article 2 ", ces derniers articles énumérant les pièces justificatives exigées en cas de demande adressée directement à l'Agence de services et de paiement. L'article 4 du même arrêté précise que " Le Fonds d'aide à l'acquisition de véhicules propres instruit les demandes de remboursement mentionnées à l'article 3. La convention conclue avec le demandeur définit les conditions d'instruction de ces demandes et les procédures de contrôle ".<br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions que l'aide à l'acquisition de véhicules propres est subordonnée au respect de conditions d'éligibilité et à un engagement du bénéficiaire de l'aide. D'une part, au titre des conditions d'éligibilité, le demandeur doit justifier d'un domicile ou d'un établissement en France et acquérir ou prendre en location, dans les conditions précisées par le décret du 26 décembre 2007, un véhicule neuf répondant à certaines caractéristiques techniques. D'autre part, en application du 4° de l'article 1er du décret, le bénéficiaire de l'aide s'engage à ne pas destiner le véhicule acquis à la vente ou à la location en tant que véhicule neuf. Dans le cas contraire, il s'expose à une action en récupération de l'aide indûment perçue. Lorsque le montant de l'aide a été avancé à l'acheteur par le vendeur du véhicule, dans le cadre d'une convention signée sur le fondement de l'article 8 du décret cité au point 2, l'Agence de services et de paiement peut, en cas de paiement indu, ordonner le reversement par le vendeur des véhicules du montant des aides que ce dernier avait avancé et dont il avait obtenu le remboursement, s'il est établi que le vendeur n'a pas satisfait à l'obligation qui lui incombe de s'assurer de l'éligibilité du dossier de demande présenté par l'acheteur. A ce titre, il appartient au vendeur de refuser de consentir une avance de l'aide en cas de doute manifeste sur le respect par l'acheteur de la règlementation relative à l'aide, notamment s'il apparait au vendeur que l'acheteur destine les véhicules en cause à la revente comme véhicules neufs.<br/>
<br/>
              5. Il résulte des énonciations de l'arrêt attaqué que la cour a jugé que l'Agence de services et de paiement " pouvait régulièrement demander à la société Avenir Automobile 78 de lui fournir une attestation certifiant que le client, la société Evercar France, bénéficiaire de l'aide avancée, était toujours propriétaire du véhicule afin de vérifier que celui-ci n'était pas destiné à être cédé par l'acquéreur en tant que véhicule neuf " et que, au  vu des documents que la société Avenir Automobile 78 lui avait transmis, l'agence avait pu retenir " que la condition d'attribution de l'aide, prévue au 4° de l'article 1er du décret du 26 décembre 2007, n'était pas remplie dès lors que la société Evercar France, après avoir acheté les véhicules à la société Avenir Automobile 78, avait revendu ces derniers à la société Evercar Belgique en tant que véhicules neufs " et lui réclamer le reversement des aides remboursées. En statuant ainsi, alors que l'Agence de services et de paiement a demandé à la société Avenir Automobile 78 des renseignements et des pièces relatives au comportement de l'acheteur postérieurement à la vente et que la cour n'a pas caractérisé le manquement de la société Avenir Automobile 78 dans les contrôles réalisés par celle-ci au moment de la vente des véhicules et de l'avance du montant de l'aide, la cour a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société Avenir Automobile 78 est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Agence de services et de paiement la somme de 3 000 euros à verser à la société Avenir Automobile 78, au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 5 mars 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : L'Agence de services et de paiement versera la somme de 3 000 euros à la société Avenir Automobile 78 au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société Avenir Automobile 78 et à l'Agence de services et de paiement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
