<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037675259</ID>
<ANCIEN_ID>JG_L_2018_11_000000417892</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/67/52/CETATEXT000037675259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/11/2018, 417892, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417892</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BERTRAND</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417892.20181130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Mme A...B...a demandé au tribunal administratif de Montreuil d'annuler :<br/>
              - la décision implicite par laquelle le directeur de la caisse d'allocations familiales de la Seine-Saint-Denis a rejeté son recours administratif formé le 19 février 2015 contestant la retenue de 1 741,08 euros opérée sur son allocation de soutien familial ;<br/>
              - la décision du 20 janvier 2015 par laquelle la mission nationale de contrôle et d'audit des organismes de sécurité sociale a rejeté sa demande d'annulation de cette décision ; <br/>
              - la décision implicite par laquelle le président du conseil départemental de la Seine-Saint-Denis a rejeté son recours administratif formé le 11 février 2016 contestant deux retenues de 1 221,02 euros et 505,82 euros au titre d'indus de revenu de solidarité active pour les mois d'août à novembre 2015 et de septembre 2015 ; <br/>
              - la décision implicite par laquelle le président du conseil départemental de la Seine-Saint-Denis a rejeté son recours administratif formé le 2 août 2016 contestant une retenue de 2 565,91 euros opérée sur son allocation de soutien familial ;<br/>
              - la décision implicite par laquelle le président de la caisse d'allocations familiales de la Seine-Saint-Denis a rejeté son recours gracieux formé le 19 février 2015 contestant deux retenues de 2 763,44 euros et de 4 170,96 euros au titre d'indus d'aide personnalisée au logement pour les mois de mai à novembre 2011 et de janvier à décembre 2012 ; <br/>
              - la décision implicite par laquelle le président du conseil départemental de la Seine-Saint-Denis a rejeté son recours administratif formé le 19 février 2015 contestant une retenue de 1 911,65 euros au titre d'un indu de revenu de solidarité active pour les mois d'avril à mai 2011 ; <br/>
              - la décision du 22 mars 2017 de la caisse d'allocations familiales de la Seine-Saint-Denis refusant de saisir la commission de recours amiable suite à des déclarations inexactes ;<br/>
              - la décision du 3 mai 2017 du directeur de la caisse d'allocations familiales de la Seine-Saint-Denis rejetant une demande de remise de dette.<br/>
              Par un jugement nos 1609072, 1701323, 1701324, 1703788, 1705630 et 1705975 du 29 novembre 2017, le tribunal administratif de Montreuil a rejeté ses demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 janvier et 9 août 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
              2°) de renvoyer l'affaire au tribunal administratif de Montreuil ou, à titre subsidiaire, réglant l'affaire au fond, de faire droit à ses demandes en ordonnant le remboursement des retenues opérées au titre des décisions de récupération d'indus annulées, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me Bertrand, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bertrand, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur des indus de prestations familiales :<br/>
<br/>
              1. Aux termes de l'article R. 351-5-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions se rapportant à un litige qui ne relève pas de la compétence de la juridiction administrative, il est compétent, nonobstant les règles relatives aux voies de recours et à la répartition des compétences entre les juridictions administratives, pour se prononcer sur ces conclusions et décliner la compétence de la juridiction administrative ". <br/>
<br/>
              2. D'une part, M. C...a été désigné pour exercer les fonctions de rapporteur public au tribunal administratif de Montreuil à compter du 7 novembre 2016 par arrêté du vice-président du Conseil d'Etat du 17 octobre 2016, publié au Journal officiel de la République française du 25 octobre suivant. Par suite, le moyen tiré de ce que le magistrat ayant prononcé des conclusions lors de l'audience du 15 novembre 2017 n'avait pas été régulièrement nommé rapporteur public manque en fait.    <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 142-1 du code de la sécurité sociale, dans sa rédaction en vigueur : " Il est institué une organisation du contentieux général de la sécurité sociale. / Cette organisation règle les différends auxquels donnent lieu l'application des législations et réglementations de sécurité sociale et de mutualité sociale agricole, et qui ne relèvent pas, par leur nature, d'un autre contentieux, ainsi que le recouvrement mentionné au 5° de l'article L. 213-1 ". Tel est le cas, en particulier, de différends relatifs à des indus de prestations familiales, énumérées à l'article L. 511-1 du même code. Par suite, les conclusions de Mme B...relatives à de tels indus se rapportaient à un litige qui, ainsi que l'a jugé le tribunal administratif de Montreuil, ne relève pas de la compétence de la juridiction administrative.<br/>
<br/>
              4. Les conclusions de Mme B...dirigées contre le jugement du tribunal en ce qu'il statue sur des indus de prestations familiales ne peuvent, dès lors, qu'être rejetées.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur des indus de revenu de solidarité active et d'aide personnalisée au logement : <br/>
<br/>
              5. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              6. Pour demander l'annulation du jugement qu'elle attaque, Mme B...soutient que :<br/>
              - ce jugement a été rendu au terme d'une procédure irrégulière faute pour le rapporteur public d'avoir été régulièrement désigné ;<br/>
              - le jugement est entaché d'irrégularité car il ne vise pas les mémoires produits le 6 novembre 2017 dans les instances nos 1701323 et 1701324 en réponse à la communication d'un moyen d'ordre public ; <br/>
              - le tribunal a commis une erreur de droit en jugeant que les décisions du 15 juillet 2015 rejetant explicitement ses demandes subsidiaires de remise de dette s'étaient substituées aux décisions ayant implicitement rejeté ses demandes principales de décharge de l'indu.<br/>
<br/>
              7. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur les demandes de Mme B...enregistrées sous les nos 1701323 et 1701324, dirigées contre les décisions implicites rejetant ses recours préalables formés le 19 février 2015 contre, d'une part, les retenues de 2 763,44 euros et 4 170,96 euros au titre d'indus d'aide personnalisée au logement pour les mois de mai à novembre 2011 et janvier à décembre 2012 et, d'autre part, la retenue de 1 911,65 euros au titre d'un indu de revenu de solidarité active pour les mois d'avril et mai 2011. En revanche, s'agissant des conclusions dirigées contre le surplus du jugement, aucun des moyens soulevés n'est de nature à en permettre l'admission.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de Mme B...dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur des indus de prestations familiales sont rejetées.<br/>
Article 2 : Les conclusions du pourvoi de Mme B...qui sont dirigées contre le jugement attaqué en tant qu'il s'est prononcé sur ses demandes enregistrées sous les nos 1701323 et 1701324 sont admises.<br/>
Article 3 : Le surplus des conclusions du pourvoi de Mme B...n'est pas admis.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé, au département de la Seine-Saint-Denis et à la caisse d'allocations familiales de la Seine-Saint-Denis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
