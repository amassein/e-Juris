<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028139460</ID>
<ANCIEN_ID>JG_L_2013_10_000000370789</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/13/94/CETATEXT000028139460.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 29/10/2013, 370789</TITRE>
<DATE_DEC>2013-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370789</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370789.20131029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er et 16 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'office public d'habitat Val d'Oise Habitat, dont le siège est rue des Châteaux Saint Sylvère, BP 10031, à Cergy-Pontoise Cedex (95001) ; Val d'Oise Habitat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1304711 du 12 juillet 2013 du juge des référés du tribunal administratif de Cergy-Pontoise en ce que, statuant en application de l'article L. 551-1 du code de justice administrative à la demande de la société ESTB, il a annulé, à compter de l'examen des offres, la procédure de passation qu'elle a engagée en vue de l'attribution du lot n° 2A, intitulé " façades ", du marché de travaux ayant pour objet la réhabilitation et la création de logements collectifs au sein de la résidence " Les Rougettes " à Cergy-Saint-Christophe ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société ESTB ;<br/>
<br/>
              3°) de mettre à la charge de la société ESTB le versement de la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu l'ordonnance n° 2005-649 du 6 juin 2005 ;<br/>
<br/>
              Vu le décret n° 2005-1742 du 30 décembre 2005 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de Val d'Oise Habitat, et à la SCP de Nervo, Poupet, avocat de la société ESTB ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que l'établissement public Val d'Oise Habitat a engagé le 31 janvier 2013, sur le fondement de l'ordonnance du 6 juin 2005 relative aux marchés passés par certaines personnes publiques ou privées non soumises au code des marchés publics et de son décret d'application du 30 décembre 2005, une procédure adaptée de passation de deux lots d'un marché de travaux ayant pour objet la réhabilitation d'une résidence à Cergy-Saint-Christophe ; que Val d'Oise Habitat se pourvoit en cassation contre l'ordonnance du 12 juillet 2013 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a annulé la procédure de passation du lot n° 2A, relatif aux façades, à compter de l'examen des offres ;<br/>
<br/>
              3. Considérant, en premier lieu, que la méthode de notation des offres ne peut être utilement contestée devant le juge du référé précontractuel qu'en cas d'erreur de droit ou de discrimination illégale ; <br/>
<br/>
              4. Considérant que la méthode de notation du critère du prix doit permettre d'attribuer la meilleure note au candidat ayant proposé le prix le plus bas ; que le juge des référés a relevé, sans commettre d'erreur de droit ni dénaturer les pièces du dossier, que la méthode retenue par Val d'Oise Habitat pour noter le critère du prix avait pour effet d'attribuer la note la plus faible au candidat ayant présenté le prix le plus éloigné de l'estimation du coût de la prestation opérée par le maître d'oeuvre, que ce prix soit inférieur ou supérieur à l'estimation, et, ainsi, avait eu pour conséquence d'attribuer la note maximale à la société déclarée attributaire du marché, alors même que sa proposition de prix était supérieure à celle de la requérante ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que le juge des référés, en relevant que, compte tenu du prix global et des prix unitaires ainsi que des volumes des prestations proposés par la société ESTB, son offre ne pouvait, sans erreur manifeste d'appréciation, être regardée comme anormalement basse et susceptible de compromettre la bonne exécution du marché, a porté sur les faits de l'espèce une appréciation souveraine qui, dès lors qu'elle est exempte de dénaturation, ne saurait être discutée devant le juge de cassation ;  <br/>
<br/>
              6. Considérant, enfin, que le juge des référés n'a pas commis d'erreur de droit en énonçant que le manquement relevé avait affecté substantiellement la notation des offres au regard du critère du prix, et que ce manquement avait été susceptible de léser la société ESTB, nonobstant le fait que son offre ait été classée troisième, jugeant implicitement mais nécessairement que le manquement constaté était susceptible de modifier l'ensemble du classement des offres ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi présenté par Val d'Oise Habitat doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de Val d'Oise Habitat, sur le fondement des mêmes dispositions, le versement d'une somme de 3 000 euros à la société ESTB ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi présenté par Val d'Oise Habitat est rejeté. <br/>
Article 2 : Val d'Oise Habitat versera à la société ESTB une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à Val d'Oise Habitat, à la société ESTB et à la société Entreprise Negro.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - CONTESTATION DE LA MÉTHODE DE NOTATION DES OFFRES - 1) OPÉRANCE - CONDITIONS - ERREUR DE DROIT OU DISCRIMINATION ILLÉGALE [RJ1] - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-08-015-01 1) La méthode de notation des offres ne peut être utilement contestée devant le juge du référé précontractuel qu'en cas d'erreur de droit ou de discrimination illégale.,,,2) La méthode de notation du critère du prix doit permettre d'attribuer la meilleure note au candidat ayant proposé le prix le plus bas. En l'espèce, le juge des référés a relevé, sans commettre d'erreur de droit ni dénaturer les pièces du dossier, que la méthode retenue par le pouvoir adjudicateur pour noter le critère du prix avait pour effet d'attribuer la note la plus faible au candidat ayant présenté le prix le plus éloigné de l'estimation du coût de la prestation opérée par le maître d'oeuvre, que ce prix soit inférieur ou supérieur à l'estimation, et, ainsi, avait eu pour conséquence d'attribuer la note maximale à la société déclarée attributaire du marché, alors même que sa proposition de prix était supérieure à celle de la requérante.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 mars 2013, Assemblée des chambres françaises de commerce et d'industrie et mutuelle des chambres de commerce et d'industrie, n°s 364551 364603, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
