<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030064085</ID>
<ANCIEN_ID>JG_L_2015_01_000000371991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/06/40/CETATEXT000030064085.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 07/01/2015, 371991, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP COUTARD, MUNIER-APAIRE ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:371991.20150107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 9 septembre, 10 décembre 2013 et 9 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Citelum, dont le siège est 37 rue de Lyon à Paris (75012), représentée par son président directeur général en exercice ; la société Citelum demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA03639 du 13 mai 2013 par lequel la cour administrative d'appel de Paris a, d'une part, annulé le jugement  n° 1102796/6-1 du 10 juin 2011 du tribunal administratif de Paris en tant qu'il a statué sur les moyens tirés de la modification de la définition des besoins de la ville de Paris au cours du dialogue compétitif et de l'irrégularité de l'offre de la société Citelum, et, d'autre part, rejeté sa requête tendant, en premier lieu, à l'annulation du jugement du 10 juin 2011 rejetant sa demande tendant à l'annulation du contrat signé le 8 février 2011 entre la Ville de Paris et le groupement  ETDE  - Satelec - Vinci  Energies - Aximum dont la société ETDE est le mandataire, ayant pour objet le marché à performance énergétique relatif aux installations d'éclairage public, d'illumination et de signalisation lumineuse de la collectivité visant à atteindre les objectifs du plan climat parisien, en second lieu, à l'annulation du marché ou, à défaut de prononcer sa résiliation, si besoin est avec effet différé ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif et le contrat signé le 8 février 2011, subsidiairement, le résilier au besoin avec effet différé, le temps pour la ville de Paris de relancer une procédure de publicité et de mise en concurrence et d'attribuer un nouveau contrat ; <br/>
<br/>
              3°) de mettre solidairement à la charge de la ville de Paris et de la société Evesa le versement des sommes de 5 000 euros et 25 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Citelum, à Me Foussard, avocat de la ville de Paris, et à la SCP Coutard, Munier-Apaire, avocat de la société ETDE ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 1224-1 du code du travail : " Lorsque survient une modification dans la situation juridique de l'employeur, notamment par succession, vente, fusion, transformation du fonds, mise en société de l'entreprise, tous les contrats de travail en cours au jour de la modification subsistent entre le nouvel employeur et le personnel de l'entreprise. " ; que ces dispositions trouvent à s'appliquer en cas de transfert par un employeur à un autre employeur d'une entité économique autonome, conservant son identité, et dont l'activité est poursuivie et reprise par le nouvel employeur ;<br/>
<br/>
              2. Considérant que pour rejeter, par l'arrêt attaqué, la requête de la société Citelum tendant, en appel du jugement du tribunal administratif de Paris rejetant sa demande, à l'annulation du contrat signé le 8 février 2011 entre la ville de Paris et le groupement ayant pour mandataire la société ETDE, la cour administrative d'appel de Paris s'est notamment fondée sur le fait que les documents de la consultation n'avaient pas à comporter d'information sur la masse salariale du personnel à reprendre, dès lors que le marché passé par la ville de Paris n'impliquait pas la poursuite d'une activité par une entité économique autonome conservant son identité ; <br/>
<br/>
              3. Considérant qu'il ressort toutefois des pièces du dossier soumis au juge d'appel qu'en se fondant, pour écarter l'existence de toute poursuite d'activité, sur la circonstance que la société Citelum, titulaire du précédent marché d'éclairage, n'assurait pas les prestations d'exploitation des installations d'éclairage public et de signalisation lumineuse tricolore sur le territoire de Paris " intra-muros ", la cour a entaché son arrêt d'une erreur de fait ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la société Citelum est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mis à la charge de la société Citelum, qui n'est pas, dans la présente instance, la partie perdante, le versement des sommes que demande la ville de Paris et la société EVESA, venant aux droits de la société ETDE ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris et de la société EVESA le versement de la somme de 1 500 euros chacune à la société Citelum au titre des dispositions de l'article L. 761-1 du code de justice administrative et de celles de l'article R. 761-1 du même code relatives au remboursement de la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 mai 2013 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : La ville de Paris et la société EVESA verseront chacune la somme de 1 500 euros à la société Citelum en application des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la ville de Paris et la société EVESA sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Citelum, à la société EVESA et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
