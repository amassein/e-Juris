<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043089817</ID>
<ANCIEN_ID>J0_L_2021_01_000001800565</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/08/98/CETATEXT000043089817.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 1ère chambre, 26/01/2021, 18VE00565, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>18VE00565</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BEAUJARD</PRESIDENT>
<AVOCATS>CABINET HEDEOS</AVOCATS>
<RAPPORTEUR>Mme Odile  DORION</RAPPORTEUR>
<COMMISSAIRE_GVT>M. MET</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La SASU Electrodomestic Trading et Development a demandé au tribunal administratif de Lille de prononcer le remboursement de crédits de taxe sur la valeur ajoutée (TVA) au titre des mois de juin à décembre 2011 pour un montant total de 4 073 766 euros, assorti des intérêts moratoires capitalisés. Le président de la section du contentieux du Conseil d'Etat a, par ordonnance du 20 janvier 2017, attribué le jugement des demandes de la SASU Electrodomestic Trading et Development au tribunal administratif de Montreuil.<br/>
<br/>
       Par un jugement nos 1540687, 1540688, 1540689, 1540691, 1540692, 1540694, 1540699 et 1630534 du 14 décembre 2017, le tribunal administratif de Montreuil a rejeté ses demandes.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête, un mémoire et des pièces enregistrés le 14 février 2018 et les 20 janvier et 5 mars 2020, la SASU Electrodomestic Trading et Development, représentée par Me A... et Me C..., avocats, demande à la cour :<br/>
<br/>
       1° d'annuler le jugement attaqué ;<br/>
<br/>
       2° d'annuler les décisions de rejet partiel de ses demandes de remboursement de crédit de TVA ;<br/>
<br/>
       3° de prononcer le remboursement d'un crédit de taxe sur la valeur ajoutée pour un montant de 5 055 588 euros au titre de la période de juin à décembre 2011, assorti des intérêts moratoires, avec capitalisation desdits intérêts ;<br/>
<br/>
       4° de mettre à la charge de l'État la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - le jugement attaqué a dénaturé ses écritures, et est entaché d'erreur de droit, de violation de la loi et d'insuffisance de motivation ;<br/>
       - c'est à tort que les premiers juges ont estimé que l'administration avait pu, au stade de l'examen de sa demande de remboursement de crédit de TVA, laquelle constitue une réclamation contentieuse au sens de l'article L. 190 du livre des procédures fiscales, opérer une compensation, en application des dispositions de l'article L. 203 du livre des procédures fiscales, entre les crédits de TVA dont elle demande le remboursement et les rappels de TVA afférents aux mêmes périodes qui lui ont été notifiés dans le cadre de la vérification de comptabilité dont elle a fait l'objet, alors que ces rappels n'ont pas porté sur ses droits à déduction et que, n'ayant pas été mis en recouvrement, ils n'avaient pas acquis un caractère définitif ; les conditions d'une telle compensation n'étaient pas réunies dès lors que sa demande de remboursement de crédit de TVA ne constitue pas une demande de décharge ou de réduction d'une imposition et que sa créance sur le Trésor ne pouvait être compensée avec des omissions qui n'ont pas été révélées au cours de l'instruction de sa demande ;<br/>
       - les décisions d'admission partielle du 5 novembre 2015 sont insuffisamment motivées ;<br/>
       - le service vérificateur n'était pas habilité à statuer, dans la proposition de rectification et la réponse aux observations du contribuable, sur sa demande de remboursement de crédit de TVA, qui vaut réclamation contentieuse, sauf à commettre une erreur entachant d'illégalité la procédure de vérification ;<br/>
       - le service vérificateur ne pouvait légalement, par la proposition de rectification du 25 janvier 2013, réduire les crédits de TVA déclarés qui n'étaient pas remis en cause par le contrôle ;<br/>
       - en l'absence de mise en recouvrement des rappels de TVA collectés compensés illégalement par la TVA déductible dont elle demandait le remboursement, elle a été privée de la faculté de contester ces impositions supplémentaires ;<br/>
       - à titre subsidiaire, les rappels de TVA qui lui ont été notifiés à l'issue de la procédure de vérification de comptabilité ne sont pas fondés, dès lors qu'elle disposait de l'ensemble des éléments justifiant l'exonération de ses livraisons intracommunautaires ;<br/>
       - elle a droit au paiement d'intérêts moratoires depuis 2011, dans les conditions prévues par l'article L. 208 du livre des procédures fiscales.<br/>
<br/>
       .........................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu : <br/>
       - le code général des impôts et le livre des procédures fiscales,<br/>
       - les décrets n° 2020-1404 et 2020-1405 du 18 novembre 2020 ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique,<br/>
       - le rapport de Mme B...,<br/>
       - les conclusions de M. Met, rapporteur public,<br/>
       - les observations de Me C... pour la SASU Electrodomestic Trading et Development.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
       1. La SASU Electrodomestic Trading et Development, qui exerce à titre principal une activité de négoce de produits électroniques, s'approvisionne essentiellement auprès de fournisseurs indépendants français et réalise 80 % de son activité à l'international sous la forme de livraisons intra-communautaires et d'exportations exonérées de taxe sur la valeur ajoutée (TVA). Elle a saisi l'administration de plusieurs demandes de remboursement de crédits de TVA au titre des mois de juin à décembre 2011. Parallèlement, elle a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier au 31 décembre 2011 à l'issue de laquelle le service a remis en cause l'exonération pratiquée par la société sur certaines livraisons <br/>
intra-communautaires et lui a notifié les rappels de taxe correspondants. Par plusieurs décisions du 5 novembre 2015, le directeur des services fiscaux a partiellement rejeté les demandes de remboursement de crédits de TVA de la SASU Electrodomestic Trading et Development, à concurrence des rappels de TVA collectée dont elle avait fait l'objet. La société <br/>
SASU Electrodomestic Trading et Development relève appel du jugement du 14 décembre 2017 par lequel le tribunal administratif de Montreuil a rejeté ses demandes de restitution du solde de crédit de TVA déductible auquel elle estime avoir droit, au titre de la période de juin à décembre 2011, pour un montant total de 4 073 766 euros.<br/>
Sur la régularité du jugement :<br/>
       2. En premier lieu, la SASU Electrodomestic Trading et Development, qui ne précise d'ailleurs pas en quoi le jugement attaqué serait insuffisamment motivé, soutenait en première instance que le rejet de ses demandes procédait d'une compensation illégale opérée par l'administration en application de l'article L. 203 du livre des procédures fiscales alors que les conditions d'une telle compensation n'étaient pas réunies. Le Tribunal administratif de Montreuil a expressément répondu à ce moyen et écarté les autres moyens présentés en première instance. Il est par suite suffisamment motivé. <br/>
       3. En second lieu, si la SASU Electrodomestic Trading et Development fait valoir que le jugement attaqué est entaché d'erreur de droit et de dénaturation, ces moyens concernent le bien-fondé de ses demandes, et sont par suite sans  influence sur la régularité du jugement.<br/>
       Sur les conclusions tendant à l'annulation des décisions d'admission partielle des demandes de remboursement :<br/>
       4. La demande de remboursement d'un crédit de TVA présentée sur le fondement des dispositions de l'article 271 du code général des impôts constitue une réclamation au sens de l'article L. 190 du livre des procédures fiscales. Les décisions par lesquelles l'administration statue sur la réclamation du contribuable qui entend contester la créance du Trésor, en tout ou en partie, en ce qui concerne les impositions auxquelles il a été assujetti, ne constituent pas des actes détachables de la procédure d'imposition susceptibles de recours pour excès de pouvoir. Il s'ensuit que les conclusions de la requête tendant à l'annulation des décisions par lesquelles le directeur général des finances publiques a partiellement fait droit aux demandes de remboursement en litige sont irrecevables et doivent être rejetées.<br/>
Sur les demandes de remboursement des reliquats de crédit de TVA :<br/>
       5. Aux termes de l'article 271 du code général des impôts : " I. 1. La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération. (...) / IV. La taxe déductible dont l'imputation n'a pu être opérée peut faire l'objet d'un remboursement (...) ".<br/>
       6. En premier lieu, il résulte de l'instruction qu'en réponse aux demandes de remboursement de crédit de TVA présentées par la SASU Electrodomestic Trading et Development, au titre des mois de juin à décembre 2011, l'administration a, par ses décisions du 5 novembre 2015, sans remettre en cause la déductibilité de la taxe dont la société demandait le remboursement, rejeté partiellement ses demandes en limitant son droit à remboursement à la différence entre son crédit de taxe et les rappels de droits mis à sa charge par la proposition de rectification du 25 janvier 2013 et confirmés par la réponse aux observations du contribuable du 16 septembre 2013. Les moyens tirés de ce que ces décisions auraient prises par un service vérificateur incompétent et seraient insuffisamment motivées sont sans incidence sur le <br/>
bien-fondé de la demande de remboursement et manquent en tout état de cause en fait.<br/>
       7. En deuxième lieu, aux termes de l'article L. 190 du livre des procédures fiscales dans sa rédaction alors en vigueur : " Les réclamations relatives aux impôts, contributions, droits, taxes, redevances, soultes et pénalités de toute nature, établis ou recouvrés par les agents de l'administration, relèvent de la juridiction contentieuse lorsqu'elles tendent à obtenir soit la réparation d'erreurs commises dans l'assiette ou le calcul des impositions, soit le bénéfice d'un droit résultant d'une disposition législative ou réglementaire. / Relèvent de la même juridiction les réclamations qui tendent à obtenir la réparation d'erreurs commises par l'administration dans la détermination d'un résultat déficitaire ou d'un excédent de taxe sur la valeur ajoutée déductible sur la taxe sur la valeur ajoutée collectée au titre d'une période donnée, même lorsque ces erreurs n'entraînent pas la mise en recouvrement d'une imposition supplémentaire. Les réclamations peuvent être présentées à compter de la réception de la réponse aux observations du contribuable mentionnée à l'article L. 57, ou à compter d'un délai de 30 jours après la notification prévue à l'article L. 76 ou, en cas de saisine de la commission départementale ou nationale des impôts directs et des taxes sur le chiffre d'affaires, à compter de la notification de l'avis rendu par cette commission ". <br/>
       8. Il résulte de ces dispositions que le contribuable peut contester une rectification opérée par l'administration dans la détermination d'un résultat déficitaire ou d'un excédent de taxe sur la valeur ajoutée déductible sur la taxe sur la valeur ajoutée, alors même qu'aucune imposition supplémentaire n'en est résulté. Dès lors, la circonstance que les rappels de TVA notifiés à la SASU Electrodomestic Trading et Development n'ont pas été mis en recouvrement du fait de l'excédent de taxe déductible non contesté dont elle disposait, n'a pas privé l'intéressée de la faculté de contester les rectifications dont elle a fait l'objet.<br/>
       9. En troisième lieu, aux termes de l'article L. 203 du livre des procédures fiscales : " Lorsqu'un contribuable demande la décharge ou la réduction d'une imposition quelconque, l'administration peut, à tout moment de la procédure et malgré l'expiration des délais de prescription, effectuer ou demander la compensation dans la limite de l'imposition contestée, entre les dégrèvements reconnus justifiés et les insuffisances ou omissions de toute nature constatées dans l'assiette ou le calcul de l'imposition au cours de l'instruction de la demande. ".<br/>
       10.  Il résulte de l'instruction que pour rejeter partiellement la demande de remboursement de crédit de TVA présentée par la SASU Electrodomestic Trading et Development, l'administration a pris en compte les rappels de taxe qui lui avaient été notifiés, procédant des insuffisances de TVA collectée sur ses livraisons intra-communautaires, sur lesquels la TVA déductible dont elle demandait le remboursement était susceptible de s'imputer. L'administration a pu légalement, dans le cadre de l'instruction des demandes de remboursement dont elle était saisie, tirer les conséquences des rappels de droits résultant de la vérification de comptabilité, pour minorer l'excédent de taxe déductible pouvant donner lieu à remboursement en application des dispositions, citées au point 5, du IV de l'article 271 du code général des impôts. Ce faisant, le service n'a pas, contrairement à ce que soutient la société requérante, procédé à une compensation, sur le fondement de l'article L. 203 du livre des procédures fiscales, entre les droits de TVA déductible de la société, dont elle ne conteste pas le bien-fondé, et ces rectifications. La société requérante n'est par suite pas fondée à soutenir que l'administration aurait pratiqué une compensation illégale. <br/>
       11. En quatrième lieu, selon les dispositions de l'article 242-0 A de l'annexe II au code général des impôts, prises pour l'application de l'article 271 cité au point 5 : " Le remboursement de la taxe sur la valeur ajoutée déductible dont l'imputation n'a pu être opérée doit faire l'objet d'une demande des assujettis (...) ". Les dispositions de l'article 242-0 E de la même annexe précisent que : " Le crédit de taxe déductible dont le remboursement a été demandé ne peut donner lieu à imputation ; il est annulé lors du remboursement ".<br/>
       12. Ces dispositions n'ont pas pour effet d'imposer à l'administration de faire droit à une demande de remboursement de crédit de taxe déductible, alors que seul un crédit de taxe déductible non imputable peut donner lieu à remboursement. La SASU Electrodomestic Trading et Development n'est par suite pas fondée à soutenir que les rappels de taxe mis à sa charge devaient rester sans incidence sur ses excédents de TVA remboursables au titre de la même période.<br/>
       13. En dernier lieu, si la SASU Electrodomestic Trading et Development fait valoir que les rappels de TVA sur ses exportations dont l'administration a remis en cause l'exonération par la proposition de rectification du 25 janvier 2013 ne sont pas fondés, elle n'articule aucun moyen à l'encontre de ces impositions supplémentaires.<br/>
       14. Il résulte de ce qui précède que la SASU Electrodomestic Trading et Development n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté ses demandes. Il s'ensuit que sa requête doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
       DECIDE :<br/>
<br/>
<br/>
<br/>
Article 1er : La requête de la SASU Electrodomestic Trading et Development est rejetée.<br/>
<br/>
2<br/>
N° 18VE00565<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-06-02-08-03-06 Contributions et taxes. Taxes sur le chiffre d'affaires et assimilées. Taxe sur la valeur ajoutée. Liquidation de la taxe. Déductions. Remboursements de TVA.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
