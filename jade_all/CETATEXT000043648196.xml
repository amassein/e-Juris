<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648196</ID>
<ANCIEN_ID>JG_L_2021_06_000000449643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648196.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2021, 449643</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:449643.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Gimarco a demandé au juge des référés du tribunal administratif de Rouen, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la décision d'attribution du contrat de concession de services pour l'exploitation du terminal " multivrac " du Grand port maritime du Havre à la société Lorany Conseils, d'enjoindre au Grand port maritime du Havre, s'il entendait poursuivre la procédure de consultation, de reprendre la procédure au stade de l'analyse des offres après avoir éliminé la candidature de cette société et, à titre subsidiaire, d'annuler l'ensemble de la procédure de passation. Par une ordonnance n° 2100012 du 28 janvier 2021, le juge des référés a annulé la procédure de passation en litige.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et trois nouveaux mémoires, enregistrés les 12 février, 1er mars, 28 avril, 14, 21 et 26 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la société Lorany Conseils demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Gimarco ;<br/>
<br/>
              3°) de mettre à la charge de la société Gimarco la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - le code de la commande publique ;<br/>
              - le code des transports ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu les notes en délibérés, enregistrée les 26 mai et 8 juin 2021, présentées par la société Lorany Conseils ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Colin - Stoclet, avocat de la société Lorany Conseils et à la SCP Marlange, de la Burgade, avocat de la société Gimarco ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Rouen que le Grand port maritime du Havre a lancé, le 5 décembre 2019, un avis d'appel à la concurrence en vue de l'attribution d'un contrat de concession portant sur la réalisation et l'exploitation d'un terminal de vracs solides dans le port du Havre. La société Gimarco, qui s'est portée candidate à l'attribution de ce contrat, a été informée, par un courrier du 23 décembre 2020, du rejet de son offre et de ce que l'offre de la société Lorany Conseils était retenue. Par une ordonnance du 28 janvier 2021, contre laquelle la société Lorany Conseils se pourvoit en cassation, le juge des référés du tribunal administratif de Rouen a fait droit à la demande de la société Gimarco tendant à l'annulation de l'ensemble de la procédure de passation en litige.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique./ (...) Le juge est saisi avant la conclusion du contrat ".<br/>
<br/>
              3. Si la société Lorany Conseils soutient en premier lieu que le juge des référés du tribunal administratif de Rouen s'est à tort fondé sur des pièces communiquées par la société Gimarco en violation du secret des affaires, cette circonstance n'est pas de nature à entacher d'irrégularité ni d'erreur de droit son ordonnance, dès lors que ces pièces ont pu être discutées contradictoirement par les parties.<br/>
<br/>
              4. En deuxième lieu, contrairement à ce qui est soutenu, c'est sans soulever un moyen d'office que le juge des référés, saisi par la société Gimarco d'un moyen tiré de ce que la requérante ne présentait pas de garanties financières suffisantes, a relevé que les termes du rapport d'analyse des offres ne lui permettaient pas de s'assurer que le Grand port maritime du Havre avait procédé au contrôle de ses capacités financières sur la base des éléments mentionnés dans le règlement de consultation.<br/>
<br/>
              5. C'est, en troisième lieu, sans commettre d'erreur de droit et au terme d'une appréciation souveraine exempte de dénaturation que le juge des référés du tribunal administratif de Rouen a estimé que l'appréciation par le Grand port maritime du Havre des capacités financières de la société Lorany Conseils était entachée d'erreur manifeste, eu égard notamment à la situation financière de cette société et à la formalisation insuffisante des soutiens de sociétés tierces revendiqués par la requérante.<br/>
<br/>
              6. En quatrième lieu, les motifs par lesquels l'ordonnance attaquée retient, d'une part, l'existence d'un nantissement judiciaire au bénéfice de la société Ciments de la Seine sur les actions détenues par la société SMEG international dans le capital de la requérante et, d'autre part, que la candidature de la société Lorany Conseils serait fondée sur de faux renseignements relatifs à l'exploitation d'un terminal de vrac sur le port de Gand sont surabondants. La requérante ne peut par suite utilement soutenir qu'ils seraient entachés d'insuffisance de motivation, d'erreur de droit et de dénaturation.<br/>
<br/>
              7. En dernier lieu, le juge des référés du tribunal administratif de Rouen n'a pas commis d'erreur de droit ni d'erreur sur la qualification juridique des faits en estimant, d'une part, que la société Gimarco justifiait d'un intérêt à conclure le contrat, la rendant recevable à contester la procédure de passation en litige, et, d'autre part, que ses intérêts avaient été lésés par les manquements aux obligations de publicité et de mise en concurrence qu'il a retenus, sans qu'y fasse obstacle la circonstance que la divulgation d'informations confidentielles contenues dans le rapport d'analyse des offres, dans le cadre de la procédure contradictoire devant lui, était susceptible de porter atteinte à l'égalité entre les candidats dans le cadre d'une éventuelle nouvelle procédure de passation, à brève échéance, de la concession en litige. En effet, une telle circonstance, qui pourrait affecter la légalité de la nouvelle procédure susceptible d'être mise en oeuvre, est en elle-même sans incidence sur celle de la procédure contestée. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la société Lorany Conseils n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Lorany Conseils la somme de 3 000 euros à verser à la société Gimarco au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Gimarco qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Lorany Conseils est rejeté.<br/>
Article 2 : La société Lorany Conseils versera à la société Gimarco une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Lorany Conseils et à la société Gimarco.<br/>
Copie en sera adressée au Grand port maritime du Havre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - DÉCISION DU JUGE FONDÉE SUR DES PIÈCES PRODUITES PAR UNE PARTIE EN MÉCONNAISSANCE DU SECRET DES AFFAIRES - CIRCONSTANCE DE NATURE À ENTACHER LA RÉGULARITÉ OU LE BIEN-FONDÉ DE LA DÉCISION DU JUGE - ABSENCE [RJ1], DÈS LORS QUE CES PIÈCES ONT PU ÊTRE DISCUTÉES CONTRADICTOIREMENT PAR LES PARTIES [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - DÉCISION DU JUGE DU RÉFÉRÉ PRÉCONTRACTUEL FONDÉE SUR DES PIÈCES PRODUITES PAR UNE PARTIE EN MÉCONNAISSANCE DU SECRET DES AFFAIRES - CIRCONSTANCE DE NATURE À ENTACHER LA RÉGULARITÉ OU LE BIEN-FONDÉ DE LA DÉCISION DU JUGE - ABSENCE [RJ1], DÈS LORS QUE CES PIÈCES ONT PU ÊTRE DISCUTÉES CONTRADICTOIREMENT PAR LES PARTIES [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - DÉCISION DU JUGE DU RÉFÉRÉ PRÉCONTRACTUEL FONDÉE SUR DES PIÈCES PRODUITES PAR UNE PARTIE EN MÉCONNAISSANCE DU SECRET DES AFFAIRES - CIRCONSTANCE DE NATURE À ENTACHER LA RÉGULARITÉ OU LE BIEN-FONDÉ DE LA DÉCISION DU JUGE - ABSENCE [RJ1], DÈS LORS QUE CES PIÈCES ONT PU ÊTRE DISCUTÉES CONTRADICTOIREMENT PAR LES PARTIES [RJ2].
</SCT>
<ANA ID="9A"> 39-08-015-01 La circonstance que le juge du référé précontractuel s'est fondé sur des pièces communiquées en violation du secret des affaires n'est pas de nature à entacher d'irrégularité ni d'erreur de droit son ordonnance, dès lors que ces pièces ont pu être discutées contradictoirement par les parties.</ANA>
<ANA ID="9B"> 54-04-01 La circonstance que le juge du référé précontractuel s'est fondé sur des pièces communiquées en violation du secret des affaires n'est pas de nature à entacher d'irrégularité ni d'erreur de droit son ordonnance, dès lors que ces pièces ont pu être discutées contradictoirement par les parties.</ANA>
<ANA ID="9C"> 54-06-01 La circonstance que le juge du référé précontractuel s'est fondé sur des pièces communiquées en violation du secret des affaires n'est pas de nature à entacher d'irrégularité ni d'erreur de droit son ordonnance, dès lors que ces pièces ont pu être discutées contradictoirement par les parties.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un jugement fondé sur un document confidentiel soustrait à son auteur, CE, 8 novembre 1998, Election cantonale de Bruz, n° 201966, p. 345,,[RJ2] Cf., s'agissant d'un jugement fondé sur de pièces produites en méconnaissance du secret médical, CE, 2 octobre 2017, M.,, n° 399753, T. pp. 603-711-747-756-763.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
