<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037545370</ID>
<ANCIEN_ID>JG_L_2018_10_000000423118</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/54/53/CETATEXT000037545370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 12/10/2018, 423118, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423118</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:423118.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe n° 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-20-20-10 le 24 juillet 2017, en tant qu'il écarte l'application de l'abattement pour durée de détention prévu par l'article 150-0 D du code général des impôts aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition en application de l'article 150-0 B ter du code général des impôts au lieu de prescrire l'application d'un abattement global déterminé en fonction de la durée de détention décomptée depuis l'acquisition des titres remis à l'échange jusqu'à la cession des titres issus de l'échange et pratiqué sur la somme de la plus-value mise en report lors de l'échange et de la plus-value  réalisée sur la cession des titres issus de l'échange ;<br/>
<br/>
              2°) de transmettre à la Cour de justice de l'Union européenne une question préjudicielle relative à la compatibilité des dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 et du 5° du II de l'article 34 de la loi du 29 décembre 2016 de finances rectificative pour 2016 relatives aux modalités d'imposition des plus-values placées en report d'imposition avec les dispositions de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Il soutient que les commentaires administratifs litigieux :<br/>
              - sont entachés d'illégalité en ce qu'ils réitèrent les dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 et du 5° du II de l'article 34 de la loi du 29 décembre 2016 de finances rectificative pour 2016, qui méconnaissent l'article 8, paragraphe 2, de la directive " fusions " du 23 juillet 1990 en privant les plus-values d'échange de titres réalisées avant le 1er janvier 2013 des abattements pour durée de détention prévus à l'article 150-0 D du code général des impôts ;<br/>
              - sont entachés d'illégalité en ce qu'ils réitèrent une disposition législative qui, en raison de son incompatibilité avec l'article 8 de la directive " fusions " dans le champ de celle-ci,  crée entre les situations entrant dans ce champ et les situations purement internes une discrimination " à rebours " contraire au principe d'égalité devant la loi tel qu'il découle de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789. <br/>
<br/>
              Par un mémoire en défense, enregistré le 25 septembre 2018, le ministre de l'action et des comptes publics conclut au rejet de la requête. Il soutient que les moyens soulevés par le requérant ne sont pas fondés. <br/>
<br/>
              En application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que le recours pour excès de pouvoir introduit par M. B... contre les commentaires litigieux n'est recevable, compte tenu de l'intérêt dont il se prévaut, qu'en tant que ceux-ci concernent l'imposition de plus-values résultant d'opérations d'échange de titres entre sociétés établies en France, qui n'entrent pas dans le champ de la directive " fusions ".<br/>
<br/>
              Par un mémoire, enregistré le 14 août 2018 au secrétariat du contentieux du Conseil d'État, M. B... demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du III de l'article 17 de la loi n° 2013-1278 du 29 décembre 2013 de finances pour 2014 et du 5° du II de l'article 34 de la loi n° 2016-1918 du 29 décembre 2016 de finances rectificative pour 2016. <br/>
<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le Traité sur le fonctionnement de l'Union européenne, notamment son article 267 ;<br/>
              - la directive 90/434/CEE du Conseil du 23 juillet 1990 ;<br/>
              - la directive 2009/133/CE du Conseil du 19 octobre 2009 ;  <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2013-1278 du 29 décembre 2013, notamment son article 17 ; <br/>
              - la loi n° 2016-1918 du 29 décembre 2016, notamment son article 34 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958, notamment son article 23-5 ;<br/>
              - la décision n° 2016-538 QPC du 22 avril 2016 du Conseil constitutionnel ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018, Jacob et Lassus (C-327/16 et C-421/16) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
		     - les conclusions de M. Benoît Bohnert, rapporteur public. <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. B...demande l'annulation pour excès de pouvoir du paragraphe n° 130 des commentaires administratifs publiés au Bulletin officiel des finances publiques (BOFiP) - Impôts sous la référence BOI-RPPM-PVBMI-20-20-10 le 24 juillet 2017, en tant qu'il écarte l'application de l'abattement pour durée de détention prévu par l'article 150-0 D du code général des impôts aux plus-values réalisées antérieurement au 1er janvier 2013 et placées en report d'imposition en application de l'article 150-0 B ter du code général des impôts au lieu de prescrire l'application d'un abattement global déterminé en fonction de la durée de détention décomptée depuis l'acquisition des titres remis à l'échange jusqu'à la cession des titres issus de l'échange et pratiqué sur la somme de la plus-value mise en report lors de l'échange et de la plus-value  réalisée sur la cession des titres issus de l'échange.<br/>
<br/>
              2. Aux termes du paragraphe 130 des commentaires administratifs publiés le 24 juillet 2017 sous la référence BOI-RPPM-PVBMI-20-20-10 : " (...) l'abattement pour durée de détention ne s'applique pas (...) aux gains nets de cession, d'échange ou d'apport réalisés avant le 1er janvier 2013 et placés en report d'imposition dans les conditions prévues (...) à l'article 150-0 B ter du CGI (...) ".<br/>
<br/>
              3. L'intérêt dont se prévaut M. B...à l'appui de sa requête tient à la réalisation d'une opération d'échange de titres entre les sociétés Meetic et Jaïna Patrimoine, qu'il contrôle, régie par les dispositions de l'article 150-0 B ter du code général des impôts. Ces sociétés étant toutes deux établies en France, cet intérêt ne lui donne qualité pour demander l'annulation des commentaires administratifs qu'il attaque qu'en tant que ceux-ci concernent les situations internes, à l'exclusion des opérations intéressant des sociétés d'Etats membres différents, lesquelles sont seules susceptibles d'entrer dans le champ de la directive qu'il invoque.<br/>
<br/>
              4. D'une part, M. B...soutient, en soulevant une question prioritaire de constitutionnalité, que le III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 et le 5° du II de l'article 34 de la loi du 29 décembre 2016 de finances rectificative pour 2016, que réitèrent les énonciations litigieuses, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques protégés respectivement par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, en tant que les modalités d'imposition des plus-values réalisées avant le 1er janvier 2013 et placées en report d'imposition en application de l'article 150-0 B ter du code général des impôts sont plus défavorables lorsque les opérations d'échange d'actions n'entrent pas dans les prévisions de la directive " fusions " du 23 juillet 1990 que lorsqu'elles y entrent, ce dont il résulte une discrimination à rebours contraire à ces principes constitutionnels.<br/>
<br/>
              5. D'autre part, M. B...soutient que les énonciations qu'il attaque reproduisent des dispositions qui, en autorisant l'imposition de la totalité de la plus-value résultant de l'échange de titres alors que la plus-value de cession des titres reçus à l'échange bénéficie quant à elle, le cas échéant, des abattements pour durée de détention et, ainsi, en ne soumettant pas ces deux plus-values à un même régime d'imposition, sont incompatibles avec les objectifs du paragraphe 1 de l'article 8 de la directive 2009/133/CE du 19 octobre 2009.<br/>
<br/>
              Sur le premier moyen :<br/>
<br/>
              En ce qui concerne le cadre juridique :<br/>
<br/>
              6. Aux termes de l'article 61-1 de la Constitution du 4 octobre 1958 : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etatou de la Cour de cassation qui se prononce dans un délai déterminé. / Une loi organique détermine les conditions d'application du présent article. ". En vertu des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, prises pour l'application de ces dispositions constitutionnelles, le Conseil constitutionnel doit être saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              7. Le Conseil d'Etat, lorsqu'il est saisi d'un moyen contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, doit, en se prononçant par priorité sur le renvoi de la question de constitutionnalité au Conseil constitutionnel, statuer dans un délai de trois mois. En vertu des dispositions de l'article 23-7 de la même ordonnance, si le Conseil d'Etat ne s'est pas prononcé dans ce délai, la question est transmise au Conseil constitutionnel.<br/>
<br/>
              8. Aux termes de l'article 267 du Traité sur le fonctionnement de l'Union européenne : " La Cour de justice de l'Union européenne est compétente pour statuer, à titre préjudiciel: a) sur l'interprétation des traités, b) sur la validité et l'interprétation des actes pris par les institutions, organes ou organismes de l'Union. / Lorsqu'une telle question est soulevée devant une juridiction d'un des États membres, cette juridiction peut, si elle estime qu'une décision sur ce point est nécessaire pour rendre son jugement, demander à la Cour de statuer sur cette question. / Lorsqu'une telle question est soulevée dans une affaire pendante devant une juridiction nationale dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne, cette juridiction est tenue de saisir la Cour. "<br/>
<br/>
              9. Les dispositions précitées de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ne font pas obstacle à ce que le juge administratif, juge de droit commun de l'application du droit de l'Union européenne, en assure l'effectivité, soit en l'absence de question prioritaire de constitutionnalité, soit au terme de la procédure d'examen d'une telle question, soit à tout moment de cette procédure, lorsque l'urgence le commande, pour faire cesser immédiatement tout effet éventuel de la loi contraire au droit de l'Union. Le juge administratif dispose de la possibilité de poser à tout instant, dès qu'il y a lieu de procéder à un tel renvoi, en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, une question préjudicielle à la Cour de justice de l'Union européenne. Lorsque l'interprétation ou l'appréciation de la validité d'une disposition du droit de l'Union européenne détermine la réponse à la question prioritaire de constitutionnalité, il appartient au Conseil d'Etat de saisir sans délai la Cour de justice de l'Union européenne.<br/>
<br/>
              En ce qui concerne  l'interprétation des dispositions invoquées dans le présent litige :<br/>
<br/>
              10. Aux termes de l'article 8 de la directive 2009/133/CE du Conseil du 19 octobre 2009 concernant le régime fiscal commun applicable aux fusions, scissions, scissions partielles, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents, ainsi qu'au transfert du siège statutaire d'une SE ou d'une SCE d'un État membre à un autre, qui reprend les dispositions de l'article 8 de la directive 90/434/CEE du Conseil du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions des sociétés d'Etats membres différents : " 1. L'attribution, à l'occasion d'une fusion, d'une scission ou d'un échange d'actions, de titres représentatifs du capital social de la société bénéficiaire ou acquérante à un associé de la société apporteuse ou acquise, en échange de titres représentatifs du capital social de cette dernière société, ne doit, par elle-même, entraîner aucune imposition sur le revenu, les bénéfices ou les plus-values de cet associé. / (imposées selon les règles de taux applicables à la date du fait générateur de l'imposition de la plus-value) 6. L'application des paragraphes 1, 2 et 3 n'empêche pas les États membres d'imposer le profit résultant de la cession ultérieure des titres reçus de la même manière que le profit qui résulte de la cession des titres existant avant l'acquisition. "<br/>
<br/>
              11. Aux termes du I de l'article 150-0 B ter du code général des impôts dans sa version issue de l'article 18 de la loi du 29 décembre 2012 de finances rectificative pour 2012 applicable aux plus-value réalisées à compter du 14 novembre 2012 : " L'imposition de la plus-value réalisée, directement ou par personne interposée, dans le cadre d'un apport de valeurs mobilières, de droits sociaux, de titres ou de droits s'y rapportant tels que définis à l'article  150 -0  A à une société soumise à l'impôt sur les sociétés ou à un impôt équivalent est reportée si les conditions prévues au III du présent article sont remplies. (imposées selon les règles de taux applicables à la date du fait générateur de l'imposition de la plus-value) ". <br/>
<br/>
              12. Il résulte de ces dispositions du code général des impôts, selon l'interprétation constante qui en est donnée par le Conseil d'Etat, statuant au contentieux, qu'elles ont pour seul effet de permettre, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de constater et de liquider la plus-value d'échange l'année de sa réalisation et de l'imposer l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange. Le montant de la plus-value est ainsi calculé en appliquant les règles d'assiette en vigueur l'année de sa réalisation. En outre, conformément à la réserve d'interprétation formulée par le Conseil constitutionnel au point 15 de sa décision n° 2016-538 QPC, les plus-values placées en report d'imposition de plein droit en application de l'article 150-0 B ter demeurent.imposées selon les règles de taux applicables à la date du fait générateur de l'imposition de la plus-value<br/>
<br/>
               13. En vertu de l'article 200 A du code général des impôts, dans sa rédaction applicable au litige, résultant de l'article 34 de la loi du 29 décembre 2016 de finances rectificative pour 2016 : " 2. Les gains nets obtenus dans les conditions prévues à l'article 150-0 A sont pris en compte pour la détermination du revenu net global défini à l'article 158. / 2 ter. a. Les plus-values mentionnées au I de l'article 150-0 B ter sont imposables à l'impôt sur le revenu au taux égal au rapport entre les deux termes suivants : / - le numérateur, constitué par le résultat de la différence entre, d'une part, le montant de l'impôt qui aurait résulté, au titre de l'année de l'apport, de l'application de l'article 197 à la somme de l'ensemble des plus-values mentionnées au premier alinéa du présent a ainsi que des revenus imposés au titre de la même année dans les conditions de ce même article 197 et, d'autre part, le montant de l'impôt dû au titre de cette même année et établi dans les conditions dudit article 197 ; / - le dénominateur, constitué par l'ensemble des plus-values mentionnées au premier alinéa du présent a retenues au deuxième alinéa du présent a. / Pour la détermination du taux mentionné au premier alinéa du présent a, les plus-values mentionnées au même premier alinéa sont, le cas échéant, réduites du seul abattement mentionné au 1 de l'article 150-0 D. / Par dérogation, le taux applicable aux plus-values résultant d'opérations d'apport réalisées entre le 14 novembre et le 31 décembre 2012 est déterminé conformément au A du IV de l'article 10 de la loi n° 2012-1509 du 29 décembre 2012 de finances pour 2013. ".<br/>
<br/>
              14. En vertu du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014, les abattements pour durée de détention prévus au 1 ter et au 1 quater de l'article 150 - 0 D du code général des impôts, applicables aux gains nets obtenus dans les conditions prévues à l'article 150-0 A du même code et pris en compte pour la détermination du revenu net global soumis au barème progressif de l'impôt sur le revenu en vertu du 2 de l'article 200 A, s'appliquent aux gains réalisés à compter du 1er janvier 2013.<br/>
<br/>
              15. En vertu du A du IV de l'article 10 de la loi du 29 décembre 2012 de finances pour 2013, les plus-values mentionnées au I de l'article 150-0 B ter résultant d'opérations d'apport réalisées entre le 14 novembre et le 31 décembre 2012 sont imposables au taux forfaitaire de 24 % ou, lorsque l'ensemble des conditions prévues au 2 bis de l'article 200 A dans sa rédaction résultant de cette même loi sont remplies, au taux forfaitaire de 19 % fixé par ce même 2 bis. <br/>
<br/>
              16. L'interprétation des dispositions nationales et l'appréciation de leur compatibilité avec la directive du 19 octobre 2009 dépendent de la réponse aux questions de savoir si les dispositions précitées de l'article 8 de la directive doivent être interprétées en ce sens qu'elles font obstacle à ce que la plus-value réalisée à l'occasion de la cession des titres reçus à l'échange et la plus-value en report soient imposées selon des règles d'assiette et de taux distinctes et, en particulier, si elles s'opposent à ce que les abattements d'assiette destinés à tenir compte de la durée de détention des titres ne s'appliquent pas à la plus-value en report, compte tenu de ce que cette règle d'assiette ne s'appliquait pas à la date à laquelle cette plus-value a été réalisée, et s'appliquent à la plus-value de cession des titres reçus à l'échange en tenant compte de la date de l'échange et non de la date d'acquisition des titres remis à l'échange. <br/>
<br/>
              17. Les questions énoncées au point 16 présentent une difficulté sérieuse d'interprétation du droit de l'Union européenne, qui justifie qu'elles soient renvoyées à la Cour de justice de l'Union européenne.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              18. Selon la réponse qui sera donnée aux questions énoncées ci-dessus, il appartiendra au juge de l'excès de pouvoir, soit de juger que les commentaires litigieux réitèrent des dispositions devant être regardées comme incompatibles avec la directive du 19 octobre 2009 en tant qu'elles s'appliquent aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, soit de juger qu'ils réitèrent des dispositions qui ne sont pas incompatibles avec la directive, compte tenu, le cas échéant, de la possibilité d'en donner une interprétation conforme aux objectifs de celle-ci. Tant que l'interprétation de l'article 8 de la directive n'aura pas conduit le juge de l'excès de pouvoir à écarter l'application des énonciations réitérant les dispositions contestées aux plus-values d'échange d'actions entre sociétés d'Etats membres différents, aucune différence dans le traitement fiscal des opérations d'échange n'est susceptible d'en résulter au détriment des plus-value issues d'un échange d'actions entre sociétés françaises. Ainsi, en l'état, la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle, ne peut être regardée comme revêtant un caractère sérieux et il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              19. Dans le cas où, à la suite de la décision de la Cour de justice de l'Union européenne, le requérant présenterait à nouveau au Conseil d'Etat la question prioritaire de constitutionnalité invoquée, l'autorité de la chose jugée par la présente décision du Conseil d'Etat ne ferait pas obstacle au réexamen de la conformité à la Constitution des dispositions du III de l'article 17 de la loi du 29 décembre 2013 de finances pour 2014 et du 5° du II de l'article 34 de la loi du 29 décembre 2016 de finances rectificative pour 2016.<br/>
<br/>
<br/>
              Sur le second moyen :<br/>
<br/>
<br/>
              20. M. B...ne peut utilement se prévaloir, à l'appui des conclusions pour lesquelles il est recevable à agir telles que définies au point 3, de ce que les dispositions de la loi, telles que réitérées par les énonciations attaquées, seraient incompatibles avec les objectifs de l'article 8, paragraphe 2, de la directive 90/434/CE du Conseil du 23 juillet 1990, repris à l'article 8, paragraphe 1, de la directive 2009/133/CE précitée.<br/>
<br/>
              21. Il résulte de tout ce qui précède qu'il y a lieu de renvoyer à la Cour de justice de l'Union européenne les questions mentionnées au point 16 et de surseoir à statuer sur la requête de M. B.imposées selon les règles de taux applicables à la date du fait générateur de l'imposition de la plus-value <br/>
<br/>
<br/>
<br/>
<br/>
                                     D E C I D E :                                         <br/>
                                     --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B.imposées selon les règles de taux applicables à la date du fait générateur de l'imposition de la plus-value<br/>
Article 2 : Les questions suivantes sont renvoyées à la Cour de justice de l'Union européenne :<br/>
- les dispositions de l'article 8 de la directive du 19 octobre 2009 doivent-elles être interprétées en ce sens qu'elles font obstacle à ce que la plus-value réalisée à l'occasion de la cession des titres reçus à l'échange et la plus-value en report soient imposées selon des règles d'assiette et de taux distinctes '<br/>
- ces mêmes dispositions doivent-elles en particulier être interprétées en ce sens qu'elles s'opposent à ce que les abattements d'assiette destinés à tenir compte de la durée de détention des titres ne s'appliquent pas à la plus-value en report, compte tenu de ce que cette règle d'assiette ne s'appliquait pas à la date à laquelle cette plus-value a été réalisée, et s'appliquent à la plus-value de cession des titres reçus à l'échange en tenant compte de la date de l'échange et non de la date d'acquisition des titres remis à l'échange '<br/>
Article 3 : Il est sursis à statuer sur la requête de M. B... jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions énoncées à l'article 2.<br/>
Article 4 : La présente décision sera notifiée à M.A... B..., au ministre de l'action et des comptes publics et au greffe de la Cour de justice de l'Union européenne.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
