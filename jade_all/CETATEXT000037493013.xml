<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037493013</ID>
<ANCIEN_ID>JG_L_2018_10_000000415993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/30/CETATEXT000037493013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 12/10/2018, 415993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415993.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) Leroy Merlin France a demandé au tribunal administratif d'Orléans de prononcer la décharge de la taxe d'enlèvement des ordures ménagères à laquelle elle a été assujettie au titre de l'année 2013 à raison d'un immeuble sis à Tours (Indre-et-Loire). Par un jugement n° 1502550 du 19 septembre 2017, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés le 27 novembre 2017 et le 18 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la SA Leroy Merlin France demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Leroy Merlin France SA.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La SA Leroy Merlin France se pourvoit en cassation contre le jugement du 19 septembre 2017 par lequel le tribunal administratif d'Orléans a rejeté sa demande tendant à la décharge des cotisations de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre de l'année 2013 dans les rôles de la commune de Tours (Indre-et-Loire), à raison d'un immeuble dont elle est propriétaire dans cette commune.<br/>
<br/>
              2. Aux termes des dispositions du I de l'article 1520 du code général des impôts, applicable aux établissements publics de coopération intercommunale, dans sa rédaction applicable à l'imposition en cause : " Les communes qui assurent au moins la collecte des déchets des ménages peuvent instituer une taxe destinée à pourvoir aux dépenses du service dans la mesure où celles-ci ne sont pas couvertes par des recettes ordinaires n'ayant pas le caractère fiscal. (...) ". La taxe d'enlèvement des ordures ménagères n'a pas le caractère d'un prélèvement opéré sur les contribuables en vue de pourvoir à l'ensemble des dépenses budgétaires, mais a exclusivement pour objet de couvrir les dépenses exposées par la commune pour assurer l'enlèvement et le traitement des ordures ménagères et non couvertes par des recettes non fiscales. Ces dépenses sont constituées de la somme de toutes les dépenses de fonctionnement réelles exposées pour le service public de collecte et de traitement des déchets ménagers et des dotations aux amortissements des immobilisations qui lui sont affectées. Il en résulte que le produit de cette taxe et, par voie de conséquence, son taux, ne doivent pas être manifestement disproportionnés par rapport au montant de telles dépenses, tel qu'il peut être estimé à la date du vote de la délibération fixant ce taux.<br/>
<br/>
              3. Pour écarter le moyen, soulevé devant lui, tiré de ce que la délibération du 14 février 2013 par laquelle le conseil de la communauté d'agglomération Tours, compétente en matière de traitement et de collecte des ordures ménagères, avait fixé le taux de la taxe d'enlèvement des ordures ménagères pour l'année 2013 à un niveau manifestement disproportionné par rapport aux dépenses nécessaires à l'exploitation du service, le tribunal administratif a procédé à une comparaison entre le produit estimé de la taxe d'enlèvement des ordures ménagères et le montant prévisionnel des dépenses de fonctionnement et d'investissement relatives à la collecte et au traitement des déchets ménagers, diminuées des recettes non fiscales de la section de fonctionnement et des recettes d'investissement. En statuant ainsi, alors qu'il résulte de ce qui a été dit au point 2 qu'aux fins d'apprécier la légalité d'une délibération fixant le taux de la taxe d'enlèvement des ordures ménagères, il n'y a pas lieu de tenir compte des données de la section d'investissement, à l'exclusion des dotations aux amortissements qui sont également retracées en opérations d'ordre dans la section de fonctionnement, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la SA Leroy Merlin France est fondée à demander l'annulation du jugement attaqué. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la SA Leroy Merlin France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif d'Orléans est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Orléans. <br/>
Article 3 : L'Etat versera la somme de 1 000 euros à la SA Leroy Merlin France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société anonyme Leroy Merlin France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
