<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029955407</ID>
<ANCIEN_ID>JG_L_2014_12_000000385320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/95/54/CETATEXT000029955407.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 23/12/2014, 385320, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:385320.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Nextradio TV a demandé au tribunal administratif de Cergy-Pontoise de la décharger de la contribution exceptionnelle sur l'impôt sur les sociétés, prévue à l'article 235 ter ZAA du code général des impôts, à laquelle elle a été assujettie au titre des exercices 2011 et 2012. A l'appui de sa demande, elle a produit un mémoire, enregistré le 12 juin 2014, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève la question de la conformité aux droits et libertés garantis par la Constitution de l'article 235 ter ZAA du code général des impôts.<br/>
<br/>
              Par un jugement n° 1405745 du 23 octobre 2014, enregistré le 24 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Cergy-Pontoise a décidé, avant de statuer sur la demande de la société Nextradio TV, de transmettre la question au Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Nextradio TV ;<br/>
<br/>
              La société Nextradio TV a produit une note en délibéré, enregistrée le 15 décembre 2014.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 235 ter ZAA du code général des impôts, dans sa rédaction applicable lors des années d'imposition en litige : " I. - Les redevables de l'impôt sur les sociétés réalisant un chiffre d'affaires supérieur à 250 millions d'euros sont assujettis à une contribution exceptionnelle égale à une fraction de cet impôt calculé sur leurs résultats imposables, aux taux mentionnés à l'article 219, des exercices clos à compter du 31 décembre 2011 et jusqu'au 30 décembre 2013. / Cette contribution est égale à 5% de l'impôt sur les sociétés dû, déterminé avant imputation des réductions et crédits d'impôt et des créances fiscales de toute nature. / Pour les redevables qui sont placés sous le régime prévu à l'article 223 A, la contribution est due par la société mère. Elle est assise sur l'impôt sur les sociétés afférent au résultat d'ensemble et à la plus-value nette d'ensemble définis aux articles 223 B et 223 D, déterminé avant imputation des réductions et crédits d'impôt et des créances fiscales de toute nature. / Le chiffre d'affaires mentionné au premier alinéa du présent I s'entend du chiffre d'affaires réalisé par le redevable au cours de l'exercice ou de la période d'imposition, ramené à douze mois le cas échéant, et pour la société mère d'un groupe mentionné à l'article 223 A, de la somme des chiffres d'affaires de chacune des sociétés membres de ce groupe. (...) " ;<br/>
<br/>
              3. Considérant que la société Nextradio TV soutient que les dispositions de l'article 235 ter ZAA du code général des impôts relatives à la détermination du chiffre d'affaires de la société mère d'un groupe fiscalement intégré à prendre en compte pour l'appréciation du seuil de 250 millions d'euros méconnaissent le principe constitutionnel d'égalité devant les charges publiques, garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              4. Considérant que les dispositions de l'article 235 ter ZAA du code général des impôts sont applicables au litige dont est saisi le tribunal administratif de Cergy-Pontoise ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles méconnaissent le principe constitutionnel d'égalité devant les charges publiques soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article 235 ter ZAA du code général des impôts est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la société Nextradio TV et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Cergy-Pontoise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
