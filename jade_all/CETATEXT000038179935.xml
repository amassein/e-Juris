<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038179935</ID>
<ANCIEN_ID>JG_L_2019_02_000000414081</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/17/99/CETATEXT000038179935.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/02/2019, 414081, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414081</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414081.20190227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Toulon d'annuler l'arrêté du 28 octobre 2013 du ministre de la défense relatif à sa titularisation et à son reclassement dans le corps des secrétaires administratifs du ministère de la défense. Par un jugement n° 1303798 du 26 novembre 2015, le tribunal administratif de Toulon a fait droit à sa demande et a enjoint au ministre de procéder au réexamen de sa situation à compter du 1er mars 2012.<br/>
<br/>
              Par un arrêt n° 15MA04809 du 11 juillet 2017, la cour administrative d'appel de Marseille a, sur appel du ministre de la défense, annulé ce jugement puis rejeté la demande présentée par M. B...devant le tribunal administratif de Toulon. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 septembre et 7 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., major dans la marine nationale, s'est porté candidat pour l'accès à un emploi réservé au sein de la fonction publique de l'Etat sur le fondement de l'article L. 4139-3 du code de la défense. Au titre de cette procédure, il a, à compter du 1er mars 2012, été détaché en qualité de secrétaire administratif de classe normale pour une durée d'un an au sein de l'établissement logistique du commissariat des armées de Toulon, avec maintien de l'indice brut 576 dont il bénéficiait dans l'armée. Par un arrêté du 28 octobre 2013, il a été titularisé dans le corps des secrétaires administratifs de classe normale à compter du 1er mars 2013 et classé au 4ème échelon de ce grade, auquel correspond l'indice brut 359, après reprise de la moitié de ses services militaires ramenés à 5 ans et d'un an d'ancienneté au titre de l'année de stage. Il a demandé l'annulation de cet arrêté au tribunal administratif de Toulon. Par un jugement du 26 novembre 2015, ce tribunal a annulé l'arrêté du 28 octobre 2013 et enjoint au ministre de la défense de procéder au réexamen de la situation de l'intéressé à compter du 1er mars 2012. M. B...se pourvoit en cassation contre l'arrêt du 11 juillet 2017 par lequel la cour administrative d'appel de Marseille a, sur appel du ministre de la défense, annulé ce jugement et rejeté sa demande de première instance.<br/>
<br/>
              2. S'agissant, d'une part, de l'accès des militaires aux emplois réservés de la fonction publique, l'article L. 4139-3 du code de la défense prévoit, dans sa rédaction applicable au litige, que : " Le militaire, à l'exception de l'officier de carrière et du militaire commissionné, peut se porter candidat pour l'accès aux emplois réservés, sur demande agréée, dans les conditions prévues par le code des pensions militaires d'invalidité et des victimes de la guerre./ En cas d'intégration ou de titularisation, la durée des services effectifs du militaire est reprise en totalité dans la limite de dix ans pour l'ancienneté dans le corps ou le cadre d'emploi d'accueil de catégorie C. Elle est reprise pour la moitié de la durée des services effectifs dans la limite de cinq ans pour l'ancienneté dans le corps ou le cadre d'emploi de catégorie B ". <br/>
<br/>
              3. D'autre part, les militaires peuvent également demander leur détachement, éventuellement suivi d'une intégration, au sein de la fonction publique civile dans les conditions énoncées au I de l'article L. 4139-2 du même code aux termes duquel : " Le militaire, remplissant les conditions de grade et d'ancienneté peut, sur demande agréée, après un stage probatoire, être détaché, dans les conditions prévues par décret en Conseil d'Etat, pour occuper des emplois vacants et correspondant à ses qualifications au sein des administrations de l'Etat, des collectivités territoriales, de la fonction publique hospitalière et des établissements publics à caractère administratif, nonobstant les règles de recrutement pour ces emplois./ Les contingents annuels de ces emplois sont fixés par voie réglementaire pour chaque administration (...)./ Après un an de détachement, le militaire peut demander, dans les conditions fixées par décret en Conseil d'Etat, son intégration ou sa titularisation dans le corps ou le cadre d'emploi dont relève l'emploi considéré, sous réserve de la vérification de son aptitude. (...) / En cas d'intégration ou de titularisation, l'intéressé est reclassé à un échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui détenu dans le corps d'origine ". Aux termes de l'article R. 4139-20 du même code, pris pour l'application de cet article L. 4139-2 : " L'intégration est prononcée par l'autorité ayant le pouvoir de nomination dans le corps d'accueil. Le militaire est alors radié des cadres ou rayé des contrôles de l'armée active à la date de son intégration. / Le militaire est nommé à l'emploi dans lequel il a été détaché et classé dans le corps, en tenant compte, le cas échéant, des responsabilités correspondant à son emploi d'intégration, à un grade et à un échelon doté d'un indice égal ou à défaut immédiatement supérieur à celui dont il bénéficiait en qualité de militaire ".<br/>
<br/>
              4. Les dispositions de l'article L. 4139-3 du code de la défense fixent les modalités selon lesquelles la carrière antérieure du militaire qui devient fonctionnaire en étant recruté sur un emploi réservé est prise en considération pour déterminer l'ancienneté dont il bénéficie dans le corps qu'il rejoint lors de sa titularisation. Cette reprise d'ancienneté permet de déterminer, au regard des dispositions statutaires propres à chaque corps, l'échelon auquel il doit être reclassé et, par suite, l'indice qui en résulte. Ces dispositions ne prévoient pas que le reclassement dans la fonction publique d'un ancien militaire, recruté au titre de la législation sur les emplois réservés, tienne compte de l'indice détenu par l'intéressé lorsqu'il était militaire, alors même que cela est le cas pour d'autres modes d'intégration de militaires dans un emploi civil, notamment en application des articles L. 4139-2 et R. 4139-20 du code de la défense. Ainsi, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en estimant qu'il convenait de faire application, pour procéder au reclassement de M.B..., des seules dispositions citées au point 2 et non de celles citées au point 3 et invoquées par le requérant, qui ne s'appliquent pas à sa situation.<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre des armées. <br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
