<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031360883</ID>
<ANCIEN_ID>JG_L_2015_10_000000372778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/36/08/CETATEXT000031360883.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 23/10/2015, 372778</TITRE>
<DATE_DEC>2015-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:372778.20151023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Dijon de prononcer la réduction de la cotisation de taxe foncière sur les propriétés bâties à laquelle il a été assujetti au titre de l'année 2012 à raison d'une maison située 112 route de Corcelles à Marzy. Par un jugement n° 1202042 du 13 août 2013, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 14 octobre 2013, 15 janvier 2014 et 2 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne. / Lorsque l'affaire est susceptible d'être dispensée de conclusions du rapporteur public, en application de l'article R. 732-1-1, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, si le rapporteur public prononcera ou non des conclusions et, dans le cas où il n'en est pas dispensé, le sens de ces conclusions " ; qu'aux termes du deuxième alinéa de l'article R. 711-2 du même code : " L'avis d'audience (...) mentionne  également les modalités selon lesquelles les parties ou leur mandataire peuvent prendre connaissance du sens des conclusions du rapporteur public, en application du premier alinéa de l'article R. 711-3 ou, si l'affaire relève des dispositions de l'article R. 732-1-1, de la décision prise sur la dispense de conclusions du rapporteur public, en application du second alinéa de l'article R. 711-3 " ; que M. B...soutient que la procédure devant le tribunal administratif est entachée d'irrégularité, dès lors que l'application " Sagace " ne comportait, avant l'audience, aucune information sur le sens des conclusions du rapporteur public ou sur l'existence d'une dispense de conclusions ; que, toutefois, M.B..., qui avait été informé par l'avis d'audience de la possibilité de prendre connaissance de cette information auprès du greffe de la juridiction, à défaut de pouvoir y accéder par le biais de l'application " Sagace ", n'établit, ni même n'allègue, avoir présenté une demande au greffe de la juridiction après avoir constaté l'impossibilité d'obtenir cette information au moyen de l'application " Sagace " ; que, dès lors, le moyen tiré de l'irrégularité de la procédure doit être écarté ; <br/>
<br/>
              2. Considérant, en deuxième lieu, que lorsqu'une imposition est assise sur la base d'éléments qui doivent être déclarés par le redevable, l'administration ne peut établir, à la charge de celui-ci, des droits excédant le montant de ceux qui résulteraient des éléments qu'il a déclarés qu'après l'avoir, conformément au principe général des droits de la défense, mis à même de présenter ses observations ; que, cependant, ce principe n'oblige pas l'administration à répondre au contribuable dont elle a recueilli les observations avant de mettre en recouvrement les impositions concernées ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, tout d'abord, que l'administration a informé M. B... du rehaussement envisagé à raison de la mise à jour de la valeur locative de son bien par un courrier en date du 11 avril 2012, ensuite, que M. B...lui ayant demandé les raisons qui motivaient cette mise à jour, elle lui a répondu par un courrier du 10 mai 2012 et, enfin, qu'elle a mis l'imposition en litige en recouvrement le 31 août 2012, soit postérieurement aux observations présentées par M. B... le 5 juin 2012 ; que c'est sans erreur de droit et par une appréciation souveraine des pièces du dossier non entachée de dénaturation que le tribunal administratif s'est fondé sur ces éléments, antérieurs à la mise en recouvrement de l'imposition contestée, pour juger que le principe des droits de la défense n'avait pas été méconnu ; que, contrairement à ce que soutient M.B..., ce principe n'imposait pas à l'administration de répondre à sa contestation du bien-fondé des raisons motivant le rehaussement envisagé qui lui avaient été communiquées ; que, dès lors, en se fondant sur les éléments mentionnés ci-dessus, qui suffisaient à apprécier le respect du principe des droits de la défense, le tribunal administratif a suffisamment répondu à l'argumentation qui lui était soumise ; <br/>
<br/>
              4. Considérant, en troisième lieu, que le tribunal administratif, qui s'est fondé sur les résultats de l'instruction pour estimer que l'administration n'avait pas procédé à une évaluation surestimée de la valeur locative de la maison en cause dans le litige et a souverainement estimé qu'il n'y avait pas lieu de demander à l'administration fiscale de produire le document retraçant les constatations opérées par un de ses agents, n'a pas méconnu les règles relatives à la charge de la preuve ; <br/>
<br/>
              5. Considérant, en quatrième lieu, que le tribunal administratif a souverainement estimé, sans dénaturer les pièces du dossier et sans commettre d'erreur de droit, que les travaux de changement des fenêtres et volets, notamment au niveau des chiens assis du grenier, non contestés par le requérant, révélaient un aménagement du grenier ; qu'il n'a pas non plus dénaturé les pièces du dossier en estimant que le requérant ne contestait pas utilement l'affirmation de l'administration selon laquelle la maison, occupée par des locataires depuis plusieurs années, ne saurait être dépourvue de tout élément de confort, notamment de locaux d'hygiène et de sanitaires ; qu'il a déduit de ces constatations, sans commettre d'erreur de droit, que la maison pouvait être, au titre de l'année d'imposition en cause, classée dans la sixième catégorie et non plus dans la septième catégorie ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-02 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. - INFORMATION SUR LE SENS DES CONCLUSIONS DU RAPPORTEUR PUBLIC (ART. R. 711-3 DU CJA) - ABSENCE DE CETTE INFORMATION SUR L'APPLICATION SAGACE - AVIS D'AUDIENCE INDIQUANT LA POSSIBILITÉ DE S'ADRESSER AU GREFFE - ABSENCE D'IRRÉGULARITÉ [RJ1].
</SCT>
<ANA ID="9A"> 54-06-02 Une procédure ne méconnaît pas l'obligation de mettre les parties en mesure de connaître le sens des conclusions du rapporteur public (ou l'existence d'une dispense de conclusions) lorsque le requérant a été informé par l'avis d'audience de la possibilité, à défaut de pouvoir y accéder par le biais de l'application « Sagace », d'en prendre connaissance auprès du greffe de la juridiction, et qu'il n'établit pas avoir présenté une telle demande au greffe après avoir constaté l'impossibilité d'obtenir cette information au moyen de l'application « Sagace ».</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., pour un requérant mis dans l'impossibilité d'avoir accès aux conclusions, CE, 2 février 2011, Mme Marchesini, n° 330641, T. p. 656 ; 18 décembre 2009, Sté Sogedame, n° 305568 , p. 501.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
