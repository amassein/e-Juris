<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029882527</ID>
<ANCIEN_ID>JG_L_2014_12_000000382540</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/88/25/CETATEXT000029882527.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 10/12/2014, 382540, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382540</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Justine Lieber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382540.20141210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. C...D..., demeurant ... et par le groupe Drôme alternance, représenté par ses co-présidents MM.B... A...et C...D..., domicilié... ; M. D... et le groupe Drôme alternance demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 13 mai 2014 par laquelle le ministre de l'intérieur a rejeté leur demande tendant à l'abrogation du décret du 20 février 2014 portant délimitation des cantons dans le département de la Drôme, ainsi que ce décret ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu la loi n° 90-1103 du 11 décembre 1990 ; <br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ; <br/>
<br/>
              Vu le décret n° 2012-1479 du 27 décembre 2012 ; <br/>
<br/>
              Vu le décret n° 2013-1289 du 27 décembre 2013 ; <br/>
<br/>
              Vu le décret n° 2014-112 du 6 février 2014 ; <br/>
<br/>
              Vu l'ordonnance n°382540 du 29 juillet 2014 par laquelle la présidente de la 6ème sous-section de la section du contentieux du Conseil d'Etat n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. D...et le groupe Drôme alternance ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Justine Lieber, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques, d'ordre topographique, comme l'insularité, le relief, l'hydrographie ; d'ordre démographique, comme la répartition de la population sur le territoire départemental ; d'équilibre d'aménagement du territoire, comme l'enclavement, la superficie, le nombre de communes par canton ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Drôme, compte tenu de l'exigence de réduction du nombre des cantons de ce département de 36 à 18 résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              3. Considérant que le moyen tiré de ce que le jugement par des membres de la section du contentieux du Conseil d'Etat de la présente requête dirigée contre un décret pris après avis de la section de l'intérieur du Conseil d'Etat, méconnaîtrait le droit des requérants à être jugé par un tribunal indépendant et impartial garanti par les stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut, en tout état de cause, qu'être rejeté dès lors que les membres du Conseil d'Etat statuant sur ces requêtes n'ont pas pris part à la délibération de l'avis rendu sur le décret attaqué ;<br/>
<br/>
              En ce qui concerne la légalité externe du décret attaqué :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il résulte des termes mêmes des dispositions législatives citées au point 1 qu'il appartenait au pouvoir réglementaire de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons ; que le moyen tiré de ce que le Premier ministre était incompétent pour adopter le décret attaqué doit être écarté ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement son exécution ; que le décret attaqué, qui se limite à modifier les circonscriptions électorales du département de la Drôme, n'appelait aucune mesure d'exécution que le garde des sceaux, ministre de la justice, aurait été compétent pour signer ou contresigner ; que le moyen tiré du défaut de contreseing de ce ministre ne peut donc être accueilli ;<br/>
<br/>
              6. Considérant, en troisième lieu, que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons ; qu'aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à une consultation des communes et des établissements publics de coopération intercommunale du département faisant l'objet d'un remodelage des limites cantonales, pas plus qu'à la consultation individuelle des principaux élus de ce département ; que la requête ne peut, à cet égard et en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire ; <br/>
<br/>
              7. Considérant, en dernier lieu, qu'aux termes de l'article L. 3121-19 du code général des collectivités territoriales : " Douze jours au moins avant la réunion du conseil général, le président adresse aux conseillers généraux un rapport, sous quelque forme que ce soit, sur chacune des affaires qui doivent leur être soumises " ; qu'il ressort des pièces du dossier que le président du conseil général de la Drôme a convoqué, le 17 septembre 2013, les membres du conseil général en vue de délibérer sur le projet de décret lors de leur réunion du 30 septembre suivant ; que cet envoi comportait le projet de décret, l'exposé des motifs, la carte des nouveaux cantons ainsi qu'un tableau synthétique des nouveaux cantons précisant leur population et le nombre de communes ; qu'ainsi, le moyen tiré de ce que le groupe Drôme Alternance n'aurait pas été consulté sur le projet de décret manque en fait ; <br/>
<br/>
              En ce qui concerne la légalité interne du décret attaqué :<br/>
<br/>
              8. Considérant, en premier lieu, que si le requérant soutient que les critères de délimitation des cantons résultant des dispositions  de l'article L. 3121-1 et du a) du III de l'article L. 3113-2 du code général des collectivités territoriales ne seraient pas conformes à plusieurs règles et principes de valeur constitutionnelle, notamment au principe d'égalité devant le suffrage, il n'appartient pas au Conseil d'Etat, hors examen d'une question prioritaire de constitutionnalité présentée dans un mémoire distinct, de se prononcer sur la conformité de ces dispositions à la Constitution ; <br/>
<br/>
              9. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que la délimitation des cantons doit être faite sur des bases essentiellement démographiques ; qu'il en découle qu'elle doit être faite en prenant en compte non le nombre des électeurs mais le chiffre de la population ; que, par suite, le moyen tiré de ce que le décret attaqué se serait à tort fondé sur ce dernier chiffre et non sur le nombre des électeurs ne peut qu'être écarté ; <br/>
<br/>
              10. Considérant, en troisième lieu, qu'aucune disposition législative ou réglementaire n'imposait de faire coïncider les limites des cantons avec les " îlots regroupés pour l'information statistique " (IRIS) définis par l'Institut national de la statistique et des études économiques dans les zones urbaines ; que le moyen tiré de ce qu'en l'absence d'une telle coïncidence, le calcul de la population des nouveaux cantons serait faussé, n'est pas assorti de précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              11. Considérant, en quatrième lieu, que le requérant soutient, par la voie de l'exception, que l'article 8 du décret du 6 février 2014 portant différentes mesures d'ordre électoral serait illégal ; que s'il soutient, d'abord, que ce décret est illégal faute de comporter le contreseing du ministre de l'économie, du redressement productif et du numérique et de la ministre des outre-mer, il n'appelle aucune mesure d'exécution que ces deux ministres devraient signer ou contresigner ; qu'ensuite, la délimitation des nouvelles circonscriptions cantonales devait, conformément aux dispositions de l'article 7 de la loi du 11 juillet 1990, être effectuée au plus tard un an avant le prochain renouvellement général des conseils généraux ; que, dans les circonstances de l'espèce, eu égard, d'une part, aux délais inhérents à l'élaboration de l'ensemble des projets de décrets de délimitation des circonscriptions cantonales, à la consultation des conseils généraux et à la saisine pour avis du Conseil d'Etat, d'autre part, à la circonstance que la déclinaison à l'échelon infra-communal des chiffres de population applicables à compter du 1er janvier 2014, nécessaire à la délimitation de certains cantons, n'était pas disponible à la date à laquelle devait être entreprise la délimitation des nouvelles circonscriptions cantonales, le décret du 6 février 2014 a pu légalement prévoir que le chiffre de population municipale auquel il convenait de se référer était le chiffre authentifié par le décret n° 2012-1479 du 27 décembre 2012 et non celui que prévoit le décret n° 2013-1289 du 27 décembre 2013, qui authentifie les chiffres de population auxquels il convient, en principe, de se référer pour l'application des lois et règlements à compter du 1er janvier 2014 ; que, par suite, le requérant ne peut utilement soutenir que l'exigence d'une délimitation des cantons à partir de bases essentiellement démographiques a été méconnue en se référant à la population authentifiée au 1er janvier 2014, dès lors que c'est la population authentifiée au 1er janvier 2013 qui doit être prise en compte ; que, dès lors, l'exception d'illégalité ne peut qu'être écartée ; <br/>
<br/>
              12. Considérant, en cinquième lieu, qu'il résulte de ce qui a été dit au point précédent que le requérant ne peut en tout état de cause se prévaloir de ce que le canton de Dié présenterait une population inférieure de plus de 20 % à la population moyenne, s'il avait été tenu compte de la population authentifiée au 1er janvier 2014 ; <br/>
<br/>
              13. Considérant, en sixième lieu, que le requérant ne peut utilement soulever un moyen tiré de ce que le décret attaqué n'aurait pas procédé à un rééquilibrage des écarts de population par canton d'un département à un autre, dès lors que ce décret ne concerne que le département de la Drôme ;<br/>
<br/>
              14. Considérant, en septième lieu, que la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un " bureau centralisateur " sans mentionner les chefs-lieux de canton est, en tout état de cause, sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département de la Drôme ; <br/>
<br/>
              15. Considérant, en huitième lieu, qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que ni ces dispositions, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives ou des circonscriptions judiciaires, les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou encore les limites des anciens cantons ; que, de même, si l'article L. 192 du code électoral, dans sa rédaction antérieure à l'intervention de la loi précitée du 17 mai 2013, relatif aux modalités de renouvellement des conseils généraux, faisait référence aux arrondissements, aucun texte en vigueur à la date du décret attaqué ne mentionne ces arrondissements, circonscriptions administratives de l'Etat, pour la détermination des limites cantonales ; que, par suite, le requérant ne saurait utilement se prévaloir de ce que la délimitation des cantons de la Drôme ne correspondrait pas à celle d'autres circonscriptions électorales ou à celle de subdivisions administratives ; qu'il ne ressort pas non plus des pièces du dossier que, ce faisant, le Premier ministre aurait entaché le décret attaqué d'une erreur d'appréciation ; <br/>
<br/>
              16. Considérant, en neuvième lieu, que la modification des limites territoriales des cantons doit être effectuée selon les règles prévues aux III et IV de l'article L. 3113-2 du code général des collectivités territoriales, compte tenu de l'exigence de réduction du nombre de cantons résultant de l'article L. 191-1 du code électoral ; que, par suite, le requérant ne peut utilement exciper de ce que les zones rurales seraient, du fait de la mise en oeuvre de ces règles, moins bien représentées que les zones urbaines au sein de l'assemblée départementale ; <br/>
<br/>
              17. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de l'intérieur, que les requérants ne sont pas fondés à demander l'annulation pour excès de pouvoir de la décision refusant d'abroger le décret litigieux ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
            Article 1er : La requête de M. D...et du groupe Drôme alternance est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. C...D..., au groupe Drôme alternance, au ministre de l'intérieur et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
