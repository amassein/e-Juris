<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026529783</ID>
<ANCIEN_ID>JG_L_2012_10_000000345153</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/52/97/CETATEXT000026529783.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 22/10/2012, 345153, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345153</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Didier Chauvaux</PRESIDENT>
<AVOCATS>SCP RICHARD ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:345153.20121022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés le 20 décembre 2010 et le 18 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le ministre du travail, de l'emploi et de la santé ; le ministre du travail, de l'emploi et de la santé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX00460 du 19 octobre 2010 par lequel la cour administrative d'appel de Bordeaux a, d'une part, annulé le jugement n° 0802347 du 17 décembre 2009 par lequel le tribunal administratif de Poitiers avait rejeté la demande de Mme A tendant à l'annulation de la décision implicite du 30 juillet 2008 et de la décision explicite du 3 décembre 2008 du préfet de la région Poitou-Charentes refusant de l'autoriser à user du titre professionnel d'ostéopathe et a annulé, d'autre part, la décision du 30 juillet 2008 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par Mme A devant la cour administrative d'appel de Bordeaux ; <br/>
<br/>
              3°) de mettre à la charge de Mme A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002, notamment son article 75 ;<br/>
<br/>
              Vu le décret n° 2006-672 du 8 juin 2006 ; <br/>
<br/>
              Vu le décret n° 2007-435 du 25 mars 2007 ;<br/>
<br/>
              Vu le décret n°2007-437 du 25 mars 2007 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet avocat de Mme A et de la SCP Richard avocat du ministre du travail, de l'emploi et de la santé.<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet avocat de Mme A et à la SCP Richard avocat du ministre du travail, de l'emploi et de la santé ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision implicite du 30 juillet 2008 puis par une décision explicite du 3 décembre 2008, le préfet de la région Poitou-Charentes a refusé d'autoriser Mme A à user du titre professionnel d'ostéopathe ; que, par un jugement du 17 décembre 2009, le tribunal administratif de Poitiers a rejeté la demande de l'intéressée tendant à l'annulation de ces décisions ; que le ministre du travail, de l'emploi et de la santé se pourvoit en cassation contre l'arrêt du 19 octobre 2010 par lequel la cour administrative d'appel de Bordeaux a annulé ce jugement et fait droit à la demande présentée en première instance par Mme A ;<br/>
<br/>
              2. Considérant, d'une part, que les dispositions du premier alinéa de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé réservent le droit à l'usage professionnel du titre d'ostéopathe aux titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie, définie par voie réglementaire, délivrée par un établissement agréé par le ministre chargé de la santé dans des conditions fixées par décret ; que le troisième alinéa du même article dispose toutefois que : " Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe (...) s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret " ; qu'aux termes de l'article 16 du décret du 25 mars 2007 dans sa rédaction résultant du décret du 2 novembre 2007, pris pour l'application de l'article 75 de la loi du 4 mars 2002 : " I. - A titre transitoire (...) l'autorisation d'user du titre professionnel d'ostéopathe est délivrée après avis de la commission mentionnée au II : 1° Par le préfet de région du lieu d'exercice de leur activité, aux praticiens en exercice à la date de publication du présent décret justifiant de conditions de formation équivalentes à celles prévues à l'article 2 du décret n° 2007-437 du 25 mars 2007 (...) ou attestant d'une expérience professionnelle dans le domaine de l'ostéopathie d'au moins cinq années consécutives et continues au cours des huit dernières années / (...) II. - La commission mentionnée au I est présidée par le directeur régional des affaires sanitaires et sociales ou son représentant. Elle comprend quatre personnalités qualifiées titulaires et quatre personnalités qualifiées suppléantes nommées par le préfet de région choisies en raison de leurs compétences dans les domaines de la formation et de leur expérience professionnelle en santé et en ostéopathie. (...) / La commission se réunit dans les conditions fixées par le décret n° 2006-672 du 8 juin 2006 (...) " ;  qu'aux termes de l'article 9 de ce dernier décret : " Sauf urgence, les membres des commissions reçoivent, cinq jours au moins avant la date de la réunion, une convocation comportant l'ordre du jour et, le cas échéant, les documents nécessaires à l'examen des affaires qui y sont inscrites " ;<br/>
<br/>
              3. Considérant, d'autre part, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ;<br/>
<br/>
              4. Considérant que, pour annuler le refus d'autorisation opposé à Mme A, la cour administrative d'appel a accueilli un moyen tiré de ce que les membres de la commission consultative prévue à l'article 16 du décret du 25 mars 2007, qui avait émis le 6 mars 2008 un avis défavorable sur la demande d'autorisation présentée par l'intéressée, n'auraient pas reçu la convocation à cette réunion cinq jours au moins avant cette date, en méconnaissance des dispositions de l'article 9 du décret du 8 juin 2006, sans rechercher si l'irrégularité ainsi censurée avait été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision de refus prise par le préfet de région ou si elle avait privé l'intéressée d'une garantie ; qu'elle a ainsi entaché son arrêt d'erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre du travail, de l'emploi et de la santé est fondé à en demander l'annulation ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il ressort des pièces du dossier, d'une part, que lorsque la commission consultative a émis son avis défavorable sur la demande de Mme A, elle était composée des cinq membres prévus par les dispositions du II de l'article 16 du décret du 25 mars 2007 et, d'autre part, que son avis contient les motifs précis pour lesquels elle a estimé que l'intéressée ne remplissait pas les conditions de formation ou d'expérience professionnelles prévues à l'article 75 de la loi du 4 mars 2002 ; que, dans ces conditions, à supposer même que, en méconnaissance des dispositions de l'article 9 du décret du 8 juin 2006,  ses membres auraient reçu leur convocation moins de cinq jours avant la date de la réunion, il ne ressort pas des pièces du dossier qu'une telle irrégularité ait pu exercer une influence sur le refus d'autorisation opposé à Mme A ou priver l'intéressée d'une garantie ; que, par suite, le moyen tiré de la méconnaissance du délai de cinq jours prévu par l'article 9 du décret du 8 juin 2006 doit être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article 14 du décret du 8 juin 2006 relatif à la création, à la composition et au fonctionnement de commissions administratives à caractère consultatif : " Le procès-verbal de la réunion de la commission indique le nom et la qualité des membres présents (...) " ; que si le procès-verbal de la réunion du 6 mars 2008 au cours de laquelle la commission consultative a émis son avis comporte six signatures alors qu'il résulte des dispositions du II de l'article 16 du décret du 25 mars 2007 que la commission ne comporte que cinq membres et si ce procès-verbal n'indique pas le nom des membres présents, en méconnaissance des dispositions de l'article 14 du décret du 8 juin 2006, il ressort d'une pièce produite par le ministre du travail, de l'emploi et de la santé en réponse à la mesure d'instruction qui lui a été adressée, d'une part que l'un des membres a signé deux fois et d'autre part que les signatures sont celles des cinq personnes qui ont siégé et dont les noms sont ceux de personnes membres de la commission en vertu des dispositions du II de l'article 16 du décret du 25 mars 2007 et de l'arrêté pris le 31 octobre 2007 par le préfet de région en application de ces dispositions ; que, dans ces conditions, d'une part, la commission n'était pas irrégulièrement composée et, d'autre part, l'irrégularité du procès-verbal au regard des dispositions de l'article 14 du décret du 8 juin 2006 n'a exercé aucune influence sur le refus d'autorisation opposé à Mme A et n'a privé l'intéressée d'aucune garantie ; <br/>
<br/>
              8. Considérant, en troisième lieu, que les pièces produites par Mme A n'établissent pas qu'elle justifiait à la date des décisions attaquées de l'expérience professionnelle dans le domaine de l'ostéopathie " d'au moins cinq années consécutives et continues au cours des huit dernières années " exigée par les dispositions citées ci-dessus du 1° du I de l'article 16 du décret n° 2007-435 du 25 mars 2007 ;<br/>
<br/>
              9. Considérant enfin que ni le diplôme en étiopathie animale obtenu par Mme A ni les attestations relatives aux formations qu'elle a suivies, établies en termes généraux, sans description détaillée de ces formations, ne permettent d'établir que l'ensemble de ces formations remplirait la condition, posée par les dispositions citées ci-dessus du 1° du I de l'article 16 du décret du 25 mars 2007, d'être " équivalentes à celles prévues à l'article 2 du décret n° 2007-437 du 25 mars 2007 ", lequel prévoit " une formation d'au moins 2 660 heures ou trois années comportant 1 435 heures d'enseignements théoriques des sciences fondamentales et de biologie et 1 225 heures d'enseignements théoriques et pratiques de l'ostéopathie ", ces heures de formation étant réparties dans des unités de formation dans six domaines précisément définis et comportant en outre une formation sur les concepts et les techniques de l'ostéopathie ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que Mme A n'est pas fondée à demander l'annulation du jugement du 17 décembre 2009 par lequel le tribunal administratif de Poitiers a rejeté sa demande tendant à l'annulation des décisions du préfet de la région Poitou-Charentes des 30 juillet et 3 décembre 2008 refusant de l'autoriser à user du titre professionnel d'ostéopathe ;  <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que demande Mme A au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A la somme que demande le ministre du travail, de l'emploi et de la santé au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 19 octobre 2010 est annulé.<br/>
<br/>
Article 2 : La requête présentée par  Mme A devant la cour administrative d'appel de Bordeaux est rejetée.<br/>
<br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme Anja-Nina A et au ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
