<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033120106</ID>
<ANCIEN_ID>JG_L_2016_09_000000400864</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/12/01/CETATEXT000033120106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 14/09/2016, 400864, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400864</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400864.20160914</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme C...B...a, dans le cadre de l'instance relative à l'affaire n° 730 de la Cour de discipline budgétaire et financière, produit un mémoire, enregistré le 21 avril 2016 au greffe de la Cour de discipline budgétaire et financière, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par un arrêt n° 209-730-I du 21 juin 2016, enregistré le 22 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la Cour de discipline budgétaire et financière, avant qu'il soit statué sur le fond de l'affaire n° 730 relative à l'office national de l'eau et des milieux aquatiques (ONEMA), a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 312-1 et L. 314-18 du code des juridictions financières.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code des juridictions financières, notamment ses articles L. 312-1 et L. 314-18 ;<br/>
              - la décision n° 2016-550 QPC du Conseil constitutionnel du 1er juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 septembre 2016, présentée par Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant que lorsque, en application de l'article 23-2 de l'ordonnance du 7 novembre 1958, une juridiction n'a transmis au Conseil d'Etat une question prioritaire de constitutionnalité qu'en tant qu'elle porte sur les dispositions législatives à l'égard desquelles les conditions fixées par cet article lui paraissent remplies, et non sur les autres dispositions législatives contestées par cette question, le Conseil d'Etat examine la question prioritaire de constitutionnalité dans les limites de cette transmission partielle et ne se prononce pas sur les dispositions législatives exclues de la transmission ; qu'il suit de là qu'il y a lieu pour le Conseil d'Etat, eu égard à l'arrêt de transmission de la Cour de discipline budgétaire et financière du 21 juin 2016, de se prononcer sur la conformité aux droits et libertés garantis par la Constitution des seuls articles L. 314-18 et L. 312-1 du code des juridictions financières ; <br/>
<br/>
              Sur l'article L. 314-18 du code des juridictions financières :<br/>
<br/>
              3. Considérant qu'en vertu du premier alinéa de l'article L. 314-18 du code des juridictions financières : " Les poursuites devant la Cour ne font pas obstacle à l'exercice de l'action pénale et de l'action disciplinaire " ; <br/>
<br/>
              4. Considérant qu'il est soutenu que les dispositions de l'article L. 314-18 du code des juridictions financières, en autorisant l'engagement de plusieurs procédures disciplinaires susceptibles de conduire à un cumul des sanctions administratives, pénales et disciplinaires portent atteinte aux droits et libertés garantis par la Constitution et, en particulier, au principe " non bis in idem " découlant de l'article 8 de la Déclaration de 1789 ; <br/>
<br/>
              5. Considérant, en premier lieu, que s'agissant du cumul des poursuites devant la Cour de discipline budgétaire et financière et devant le juge pénal, par sa décision n° 2016-550 QPC du 1er juillet 2016, le Conseil constitutionnel a jugé que la possibilité d'un tel cumul résultait des seuls mots " de l'action pénale et " figurant au premier alinéa de l'article L. 314-18 du code des juridictions financières et que, dès lors, la question prioritaire de constitutionnalité portait sur ces seuls mots, qu'il a, dans les motifs et le dispositif de sa décision, déclaré conformes à la Constitution ; <br/>
<br/>
              6. Considérant, en second lieu, que s'agissant du cumul des poursuites devant la Cour de discipline budgétaire et financière et de l'action disciplinaire, les dispositions contestées de l'article L. 314-18 du code des juridictions financières permettent qu'une personne poursuivie devant la Cour de discipline budgétaire et financière pour l'une des infractions édictées par les articles L. 313-1 à L. 313-8 du même code, fasse également l'objet d'une action disciplinaire ; que si ces dispositions n'instituent pas, par elles-mêmes, un mécanisme de double poursuite et de double sanction, elles le rendent toutefois possible ; qu'il résulte de la jurisprudence du Conseil constitutionnel que ces cumuls éventuels de poursuites et de sanctions doivent, en tout état de cause, respecter le principe de nécessité des délits et des peines, qui implique qu'une même personne ne puisse faire l'objet de poursuites différentes conduisant à des sanctions de même nature pour les mêmes faits, en application de corps de règles protégeant les mêmes intérêts sociaux ; que les sanctions susceptibles d'être infligées ne sont pas de même nature, la Cour de discipline budgétaire et financière prononçant des amendes, tandis que les sanctions disciplinaires susceptibles d'être prononcées à l'encontre d'un agent public ne comprennent pas l'amende ; qu'en outre, les sanctions disciplinaires visent à assurer le respect par les fonctionnaires de leurs obligations statutaires, tandis que les sanctions prononcées par la Cour de discipline budgétaire et financière ont pour objet d'assurer le respect des règles applicables à la dépense publique et de protéger les deniers publics ; qu'ainsi, les sanctions en cause ne sont pas prises en application de corps de règles visant à protéger les mêmes intérêts sociaux, mais répondent à des intérêts sociaux distincts ; qu'il suit de là que le grief, en tant qu'il concerne les sanctions disciplinaires, est dépourvu de caractère sérieux ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la Cour de discipline budgétaire et financière en tant qu'elle est invoquée à l'encontre de l'article L. 314-18 du code des juridictions financières ; <br/>
<br/>
              Sur l'article L. 312-1 du code des juridictions financières : <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 312-1 du code des juridictions financières : " I.-Est justiciable de la Cour : / a) Toute personne appartenant au cabinet d'un membre du Gouvernement ; / b) Tout fonctionnaire ou agent civil ou militaire de l'Etat, des collectivités territoriales, de leurs établissements publics ainsi que des groupements des collectivités territoriales ; / c) Tout représentant, administrateur ou agent des autres organismes qui sont soumis soit au contrôle de la Cour des comptes, soit au contrôle d'une chambre régionale des comptes ou d'une chambre territoriale des comptes. / Sont également justiciables de la Cour tous ceux qui exercent, en fait, les fonctions des personnes désignées ci-dessus. / II.-Toutefois, ne sont pas justiciables de la Cour à raison des actes accomplis dans l'exercice de leurs fonctions : / a) Les membres du Gouvernement ; / b) Les présidents de conseil régional et, quand ils agissent dans le cadre des dispositions des articles L. 4132-3 à L. 4132-10, L. 4132-13, L. 4132-15, L. 4132-21, L. 4132-22, L. 4132-25, L. 4133-1, L. 4133 2, L. 4133-4 à L. 4133-8, L. 4231-1 à L. 4231-5 du code général des collectivités territoriales, les vice-présidents et autres membres du conseil régional ; / c) Le président du conseil exécutif de Corse et, quand ils agissent dans le cadre des dispositions du dernier alinéa de l'article L. 4424-4 du code général des collectivités territoriales, les conseillers exécutifs ; / c bis) Le président de l'assemblée de Guyane et, quand ils agissent par délégation de celui-ci, les vice-présidents et autres membres de l'assemblée de Guyane ; / c ter) Le président du conseil exécutif de Martinique et, quand ils agissent dans le cadre des articles L. 7224-12 et L. 7224-21 du code général des collectivités territoriales, les conseillers exécutifs ; / d) Les présidents de conseil général et, quand ils agissent dans le cadre des dispositions des articles L. 3221-3 et L. 3221-7 du code général des collectivités territoriales, les vice-présidents et autres membres du conseil général ; / e) Les maires et, quand ils agissent dans le cadre des dispositions des articles L. 2122-17 à L. 2122-20 et L. 2122-25 du code général des collectivités territoriales, les adjoints et autres membres du conseil municipal ; / f) Les présidents élus de groupements de collectivités territoriales et, quand ils agissent par délégation du président, les vice-présidents et autres membres de l'organe délibérant du groupement ; / g) Le président du gouvernement de la Nouvelle-Calédonie et, quand il agit dans le cadre des dispositions de l'article 70 de la loi organique n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie, le vice-président ; le président de l'assemblée de province et, quand ils agissent dans le cadre des dispositions de l'article 173 de la même loi organique, les vice-présidents ; / h) Le président de la Polynésie française et, quand ils agissent dans le cadre des dispositions de l'article 67 de la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française, le vice-président et les ministres ; / i) Le président du conseil général de Mayotte et, quand ils agissent dans le cadre des dispositions des articles L. 3221-3 et L. 3221-7 du code général des collectivités territoriales, les vice-présidents et autres membres du conseil général ; / j) Le président du conseil territorial de Saint-Barthélemy et, quand ils agissent dans le cadre des dispositions de l'article LO 6252-3 du même code, les vice-présidents et autres membres du conseil exécutif ; / k) Le président du conseil territorial de Saint-Martin et, quand ils agissent dans le cadre des dispositions de l'article LO 6352-3 du même code, les vice-présidents et autres membres du conseil exécutif ; / l) Le président du conseil territorial de Saint-Pierre-et-Miquelon et, quand ils agissent dans le cadre des dispositions de l'article LO 6462-8 du même code, les vice-présidents et autres membres du conseil territorial ; / m) A...'ils ne sont pas rémunérés et s'ils n'exercent pas, directement ou par délégation, les fonctions de président, les administrateurs élus des organismes de protection sociale relevant du contrôle de la Cour des comptes et agissant dans le cadre des dispositions législatives ou réglementaires ; / n) S'ils ne sont pas rémunérés et s'ils n'exercent pas les fonctions de président, les administrateurs ou agents des associations de bienfaisance assujetties au contrôle de la Cour des comptes ou d'une chambre régionale des comptes. / Les personnes mentionnées aux a à l ne sont pas non plus justiciables de la Cour lorsqu'elles ont agi dans des fonctions qui, en raison de dispositions législatives ou réglementaires, sont l'accessoire obligé de leur fonction principale (...) " ; <br/>
<br/>
              9. Considérant que l'article L. 312-1 du code des juridictions financières, qui définit le champ des personnes justiciables ou non de la Cour de discipline budgétaire et financière, est applicable au litige dont cette Cour est saisie ; que les dispositions de cet article n'ont  pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; <br/>
<br/>
              10. Considérant qu'il est soutenu que cet article méconnaît le principe d'égalité résultant de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, en ce qu'il distingue certains élus des autres citoyens et agents publics dans les possibilités de poursuite, pour des agissements susceptibles d'être sanctionnés qui sont pourtant identiques ; que le grief tiré de ce que les dispositions de l'article L. 312-1 du code des juridictions financières portent ainsi atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité, soulève une question, qui n'est pas nouvelle, mais présente un caractère sérieux ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède qu'il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la Cour de discipline budgétaire et financière seulement en tant qu'elle est invoquée à l'encontre de l'article L. 312-1 du code des juridictions financières ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 312-1 du code des juridictions financières est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 314-18 du code des juridictions financières.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme C...B..., au ministre des finances et des comptes publics, au Parquet général près la Cour de discipline budgétaire et financière ainsi qu'à la Cour de discipline budgétaire et financière. Copie en sera adressée au Premier ministre et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
