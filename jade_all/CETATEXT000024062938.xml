<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024062938</ID>
<ANCIEN_ID>JG_L_2011_05_000000332451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/06/29/CETATEXT000024062938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/05/2011, 332451</TITRE>
<DATE_DEC>2011-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:332451.20110520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B... A..., demeurant..., ; M.  A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 22 juin 2009 prononçant à son encontre la peine de la déchéance de ses fonctions de conseiller prud'homme ;<br/>
<br/>
              2°) d'ordonner son rétablissement dans l'exercice de son mandat de conseiller titulaire prud'homme ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le décret n° 2008-1281 du 8 décembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Auditeur,  <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que l'article L. 1442-13 du code du travail dispose que : " Tout conseiller prud'homme manquant gravement à ses devoirs dans l'exercice de ses fonctions est appelé devant la section ou la chambre pour s'expliquer sur les faits qui lui sont reprochés (...) " ; qu'aux termes de l'article L. 1442-14 : " Les peines applicables aux conseillers prud'hommes sont : / 1° La censure ; / 2° La suspension pour une durée ne pouvant excéder six mois ; / 3° La déchéance. / La censure et la suspension sont prononcées par arrêté ministériel. La déchéance est prononcée par décret " ; que M.  A...demande l'annulation pour excès de pouvoir du décret du 22 juin 2009 qui l'a déchu de ses fonctions de membre du collège employeur de la section " encadrement " du conseil de prud'hommes de Grenoble ;<br/>
<br/>
              Considérant, en premier lieu, que, contrairement à ce qui est soutenu, la circonstance que M.  A...avait fait l'objet d'une procédure disciplinaire, engagée le 29 mai 2008, et que cette procédure n'avait pas abouti  à ce qu'une sanction soit prononcée à son encontre ne faisait pas obstacle à ce que soit engagée une nouvelle procédure disciplinaire le 26 janvier 2009 à raison des mêmes faits ;<br/>
<br/>
              Considérant, en deuxième lieu, que si M.  A...soutient que la convocation à l'audience disciplinaire du 24 février 2009 a méconnu les dispositions de la circulaire du 28 mars 1991 relative aux droits et garanties des conseillers prud'hommes en matière disciplinaire, le Premier ministre n'était pas tenu par les prescriptions d'une telle circulaire, qui était au surplus réputée être abrogée à la date du décret attaqué dès lors qu'elle n'a jamais été publiée et n'est pas reprise sur le site internet prévu par l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires ; qu'en tout état de cause, il ressort des pièces du dossier que la convocation à l'audience disciplinaire adressée à M.  A...le 26 janvier 2009 faisait référence au prononcé d'une peine prévue à l'article L. 1442-14 du code du travail, et précisait par là-même la peine encourue par l'intéressé ; que par ailleurs, en renvoyant aux rapports du procureur de la République et des chefs de la cour d'appel, lesquels explicitaient les griefs retenus à l'encontre de M.  A...et étaient tenus à la disposition de ce dernier, cette convocation informait suffisamment l'intéressé des motifs des poursuites engagées contre lui ; enfin qu'aucune règle n'imposait la présence du procureur de la République à la réunion de l'instance disciplinaire ;<br/>
<br/>
              Considérant, en troisième lieu, que les faits qui peuvent motiver l'une des mesures prévues à l'article L. 1442-14 du code du travail ne sont pas seulement ceux qui auraient été commis dans l'exercice même de fonctions juridictionnelles ou d'administration du conseil de prud'hommes, mais aussi ceux qui, commis en dehors de ce cadre, révèlent un comportement incompatible avec les qualités attendues d'une personne investie de la fonction de juger et qui sont susceptibles de jeter le discrédit sur la juridiction à laquelle elle appartient et doivent, dès lors, être regardés comme des manquements graves au sens de l'article L. 1442-13 ; que, par suite, le décret attaqué n'est pas entaché d'erreur de droit, alors même que les faits retenus à l'encontre du requérant n'ont pas été commis par lui dans l'exercice de ses activités de conseiller prud'homme ; <br/>
<br/>
              Considérant, en quatrième lieu, que les faits de complicité de discrimination syndicale qui ont donné lieu à la condamnation pénale de M. A...par un arrêt de la cour d'appel de Lyon devenu définitif après le rejet du pourvoi en cassation formé contre lui, révèlent, eu égard à la nature même du contentieux soumis à la juridiction prud'homale, un comportement incompatible avec les obligations de neutralité et d'impartialité qui sont celles d'un conseiller prud'homme et peuvent être regardés comme des manquements graves au sens de l'article L. 1442-13 ; qu'ainsi, en se fondant, pour infliger une peine de déchéance à l'intéressé, sur ces agissements, sur la circonstance que M.  A...n'a pas fait état de cette condamnation lorsqu'il a sollicité sa réélection en décembre 2008, et sur le retentissement qu'a eu, du fait de sa publication dans des revues spécialisées, cette condamnation, susceptible de jeter le discrédit sur la juridiction prud'homale, le Premier ministre n'a pas inexactement qualifié ces faits en estimant qu'ils étaient de nature à justifier l'une des peines prévues à l'article L. 1442-14 du code du travail ; <br/>
<br/>
              Considérant, en cinquième lieu, qu'il ressort pas non plus des pièces du dossier qu'eu égard à la gravité des faits mentionnés ci-dessus, le Premier ministre aurait commis une erreur d'appréciation en prononçant à l'encontre de M.A... la peine de déchéance ; <br/>
<br/>
              Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du décret qu'il attaque ; que ses conclusions à fin d'injonction ne peuvent par suite qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : La requête de M. A...est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A..., au garde des sceaux, ministre de la justice et des libertés et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - SANCTION INFLIGÉE À UN CONSEILLER DE PRUD'HOMMES SUR LE FONDEMENT DE L'ARTICLE L. 1442-14 DU CODE DU TRAVAIL [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-01-02 TRAVAIL ET EMPLOI. INSTITUTIONS DU TRAVAIL. JURIDICTIONS DU TRAVAIL. - 1) CONSEILLERS DE PRUD'HOMMES - MESURES PRÉVUES À L'ARTICLE L. 1442-14 DU CODE DU TRAVAIL - FAITS POUVANT LES MOTIVER - FAITS COMMIS HORS DE L'EXERCICE DES FONCTIONS MAIS RÉVÉLANT UN COMPORTEMENT INCOMPATIBLE AVEC CES FONCTIONS OU JETANT LE DISCRÉDIT SUR LA JURIDICTION [RJ1] - 2) NATURE DU CONTRÔLE DU JUGE - CONTRÔLE NORMAL [RJ2].
</SCT>
<ANA ID="9A"> 54-07-02-03 Le juge exerce un contrôle normal sur la gravité de la sanction infligée à un conseiller de prud'hommes sur le fondement de l'article L. 1442-14 du code du travail.</ANA>
<ANA ID="9B"> 66-01-02 1) Les faits qui peuvent motiver l'une des mesures prévues à l'article L. 1442-14 du code du travail ne sont pas seulement ceux qui auraient été commis dans l'exercice même de fonctions juridictionnelles ou d'administration du conseil de prud'hommes, mais aussi ceux qui, commis en dehors de ce cadre, révèlent un comportement incompatible avec les qualités attendues d'une personne investie de la fonction de juger et qui sont susceptibles de jeter le discrédit sur la juridiction à laquelle elle appartient et doivent, dès lors, être regardés comme des manquements graves au sens de l'article L. 1442-13.,,2) Le juge exerce un contrôle normal sur la gravité de la sanction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 juillet 2004, M. Bource, n° 253663, T. p. 892. Rappr. CE, 17 mars 2010, Confédération Générale du Travail, n° 319785, à publier au Recueil., ,[RJ2] Rappr., pour la sanction infligée à un professionnel, CE, Section, 22 juin 2007, Arfi, n° 272650, p. 263 ; pour la sanction infligée à un magistrat du parquet, CE, 29 mai 2009, H., n° 310493, p. 207. Comp., s'agissant de la nature du contrôle des sanctions infligées aux fonctionnaires, CE, Section, 1er février 2006, Touzard, n° 271676, p. 38.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
