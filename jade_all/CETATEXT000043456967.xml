<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043456967</ID>
<ANCIEN_ID>JG_L_2021_04_000000451276</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/45/69/CETATEXT000043456967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/04/2021, 451276, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451276</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451276.20210423</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Ordre des avocats au barreau de Nantes et la Section française de l'Observatoire international des prisons (SFOIP) ont demandé au juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner toutes mesures qu'il estimera utiles afin de faire cesser les atteintes graves et manifestement illégales portées aux libertés fondamentales des personnes détenues au centre pénitentiaire de Ploemeur et, plus précisément, d'enjoindre au garde des sceaux, ministre de la justice, et à tout autre ministre ou toute autre autorité qu'il estimera utile, de mettre en oeuvre un certain nombre de mesures portant sur les conditions matérielles d'accueil des personnes détenues et la surpopulation, l'état matériel et sanitaire des cellules du quartier maison d'arrêt, du quartier disciplinaire, du quartier centre de détention et des cours de promenade, la nourriture distribuée aux personnes détenues, l'accès aux soins, le maintien des liens familiaux, le confinement en cellule et le manque d'activités, le respect de la confidentialité des échanges et les règles de fonctionnement au sein de l'établissement.<br/>
<br/>
              Par une ordonnance n° 2101070 du 17 mars 2021, le juge des référés du tribunal administratif de Rennes a enjoint à l'administration pénitentiaire de réaliser un diagnostic amiante de l'ensemble des bâtiments (murs, canalisations, réseaux, etc.) du centre pénitentiaire de Ploemeur, d'installer un système d'interphonie dans toutes les cellules du quartier maison d'arrêt, sauf impossibilité matérielle dûment justifiée, de procéder à la réalisation de travaux de mise aux normes des installations électriques, incluant notamment l'installation, dans chaque cellule, d'un nombre de prises électriques suffisant au regard du nombre de personnes hébergées en leur sein, de prendre toutes les dispositions pour s'assurer qu'aucune personne détenue au sein du quartier maison d'arrêt ne dorme sur un matelas à même le sol, d'assurer dans l'ensemble des cellules du quartier maison d'arrêt, la séparation de l'espace sanitaire du reste de l'espace de vie, de prendre toute mesure de nature à améliorer l'aération naturelle des cellules, le cas échéant par la suppression des caillebotis scellés en 2013 sur les fenêtres, de réaliser les travaux de rénovation et réhabilitation du système de ventilation de l'établissement, de prendre toute mesure de nature à assurer et à améliorer l'accès aux produits d'entretien des cellules ainsi qu'aux sacs-poubelle, devant être ramassés quotidiennement, et au papier hygiénique, de prendre toute mesure de nature à garantir aux personnes détenues au sein du quartier disciplinaire un accès aux douches, dans la mesure du possible quotidien et, en tout état de cause, dans des conditions respectueuses de l'hygiène et de l'intimité, de procéder au nettoyage des abris des cours de promenade et  rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par une requête, enregistrée le 31 mars 2021 au secrétariat du contentieux du Conseil d'Etat, le garde des sceaux, ministre de la justice, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'annuler cette ordonnance en tant qu'elle fait partiellement droit à la demande de première instance. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance est entachée d'erreur de droit dès lors, d'une part, que l'installation d'un système d'interphonie dans toutes les cellules du quartier maison d'arrêt et la mise aux normes des installations électriques sont des mesures d'ordre structurel qui ne sont pas au nombre de celles susceptibles d'être ordonnées dans le cadre de l'article L. 521-2 du code de justice administrative et, d'autre part, que le juge des référés n'a pas recherché s'il existait des mesures alternatives aux fins d'atteindre le but poursuivi ; <br/>
              - elle est entachée d'erreur d'appréciation et de dénaturation des pièces du dossier dès lors que, en premier lieu, le juge des référés a enjoint de réaliser un diagnostic amiante de l'ensemble des bâtiments du centre pénitentiaire alors même que l'établissement répond à la réglementation de droit commun en vigueur concernant la protection de la population contre les risques sanitaires liés à une exposition à l'amiante dans les immeubles bâtis, en deuxième lieu, il a enjoint de prendre, dans les meilleurs délais, toutes les dispositions pour s'assurer qu'aucune personne détenue au sein du quartier maison d'arrêt ne dorme sur un matelas à même le sol alors que l'administration pénitentiaire ne dispose d'aucun pouvoir d'appréciation en matière d'incarcération, en troisième lieu, il a ordonné de prendre toute mesure de nature à améliorer l'aération naturelle des cellules en procédant au besoin à la suppression des caillebotis scellés en 2013 sur les fenêtres alors même que les fenêtres de cellule sont dotées de deux ouvertures de quatre-vingt-dix centimètres sur vingt centimètres, que les cellules de seize mètres carrés sont dotées de deux fenêtres comprenant deux ouvertures chacune et que l'installation des caillebotis scellés est nécessaire afin de répondre à des impératifs tant de sécurité que de salubrité et, en dernier lieu, il a ordonné d'améliorer l'accès aux produits d'entretien des cellules, aux sacs-poubelle, devant être ramassés quotidiennement, et au papier hygiénique alors que la distribution de ces produits d'hygiène et d'entretien n'est pas de nature à caractériser une quelconque atteinte à la situation des personnes détenues.<br/>
<br/>
              Par un mémoire en défense, enregistré le 8 avril 2021, l'Ordre des avocats au barreau de Nantes et la SFOIP concluent au rejet de la requête et à ce qu'il soit mis à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ils demandent en outre, par la voie de l'appel incident, d'enjoindre au ministre de la justice de produire un descriptif détaillé du nombre de cellules du centre pénitentiaire, de leur superficie et du mobilier qui les équipe ainsi que du nombre de personnes qui les occupe, et de refondre le mode de calcul des places disponibles, opérationnelles et théoriques, au sein du centre pénitentiaire de Ploemeur afin de tenir compte notamment de la superficie des cellules et des exigences européennes relatives à l'espace de vie minimum dont doivent disposer les personnes détenues en cellule.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu les pièces complémentaires, enregistrées le 15 avril 2021, présentées par le garde des sceaux, ministre de la justice ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le garde des sceaux, ministre de la justice, et d'autre part, l'Ordre des avocats au barreau de Nantes et la SFOIP ;<br/>
              Ont été entendus lors de l'audience publique du 14 avril 2021, à 11 heures : <br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Ordre des avocats au barreau de Nantes et de la SFOIP ;<br/>
<br/>
              - le représentant de l'Ordre des avocats au barreau de Nantes et de la SFOIP ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 16 avril 2021 à 12 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. L'Ordre des avocats au barreau de Nantes et la Section française de l'Observatoire international des prisons (SFOIP) ont saisi le juge des référés du tribunal administratif de Rennes, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il ordonne diverses mesures pour faire cesser des atteintes graves et manifestement illégales portées aux libertés fondamentales des personnes détenues au centre pénitentiaire de Ploemeur. Par une ordonnance du 17 mars 2021, le juge des référés du tribunal administratif de Rennes a partiellement fait droit à leur demande. Le garde des sceaux, ministre de la justice, relève appel de cette ordonnance en ce qu'elle a enjoint à l'administration pénitentiaire de prendre certaines mesures. L'Ordre des avocats au barreau de Nantes et la Section française de l'Observatoire international des prisons (SFOIP) présentent des conclusions d'appel incident tendant au prononcé de nouvelles injonctions. <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              3. Aux termes de l'article 22 de la loi du 24 novembre 2009 pénitentiaire : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. Ces restrictions tiennent compte de l'âge, de l'état de santé, du handicap et de la personnalité de la personne détenue ".<br/>
<br/>
              4. Eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis-à-vis de l'administration, il appartient à celle-ci, et notamment aux directeurs des établissements pénitentiaires, en leur qualité de chefs de service, de prendre les mesures propres à protéger leur vie ainsi qu'à leur éviter tout traitement inhumain ou dégradant afin de garantir le respect effectif des exigences découlant des principes rappelés notamment par les articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Le droit au respect de la vie ainsi que le droit de ne pas être soumis à des traitements inhumains ou dégradants constituent des libertés fondamentales au sens des dispositions de l'article L. 521-2 du code de justice administrative. Lorsque la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes ou les expose à être soumises, de manière caractérisée, à un traitement inhumain ou dégradant, portant ainsi une atteinte grave et manifestement illégale à ces libertés fondamentales, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut, au titre de la procédure particulière prévue par l'article L. 521-2, prescrire toutes les mesures de nature à faire cesser la situation résultant de cette carence.<br/>
<br/>
              Sur les pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              5. Il résulte de la combinaison des dispositions des articles L. 511-1, L. 521-2 et L. 521-4 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte. Ces mesures doivent en principe présenter un caractère provisoire, sauf lorsqu'aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale. Toutefois, le juge des référés ne peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale. Eu égard à son office, il peut également, le cas échéant, décider de déterminer dans une décision ultérieure prise à brève échéance les mesures complémentaires qui s'imposent et qui peuvent également être très rapidement mises en oeuvre. Dans tous les cas, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 précité est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires. Compte tenu du cadre temporel dans lequel se prononce le juge des référés saisi sur le fondement de l'article L. 521-2, les mesures qu'il peut ordonner doivent s'apprécier en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              Sur l'appel du garde des sceaux, ministre de la justice :<br/>
<br/>
              En ce qui concerne les injonctions tendant à l'installation d'un système d'interphonie dans toutes les cellules du quartier maison d'arrêt dans les meilleurs délais sauf impossibilité matérielle dûment justifiée, et à la réalisation des travaux de mise aux normes des installations électriques incluant notamment l'installation, dans chaque cellule, d'un nombre de prises électriques suffisant au regard du nombre de personnes hébergées : <br/>
<br/>
              6. Eu égard à leur objet, les injonctions contestées portent sur des mesures d'ordre structurel, insusceptibles d'être mises en oeuvre, et, dès lors, de porter effet, à très bref délai. Par suite, elles ne sont pas au nombre des mesures d'urgence que la situation permet de prendre utilement dans le cadre des pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative. Il s'ensuit que le garde des sceaux, ministre de la justice, est fondé à soutenir qu'en prononçant les injonctions contestées, le juges des référés du tribunal administratif de Rennes a entaché son ordonnance d'une erreur de droit. <br/>
<br/>
              7. Au demeurant, en premier lieu, il résulte de l'instruction que pour pallier l'absence d'interphonie dans les cellules du quartier maison d'arrêt du centre pénitentiaire de Ploemer, la direction de l'établissement a mis en place, par note de service du 15 mars 2021, en sus de quatre rondes nocturnes, un " piquet détention " consistant, pour l'agent désigné, à déceler tout bruit anormal ou odeur suspecte dans le secteur hébergement et à se rendre sur place si nécessaire. En second lieu, il résulte de l'instruction que le centre pénitentiaire a fait l'objet en 2019 et 2020 de deux visites de vérification de ses installations électriques par le bureau Veritas ainsi que d'une visite le 21 janvier 2020 de la sous-commission départementale pour la sécurité contre le risque d'incendie et de panique dans les établissements recevant du public à l'issue desquelles aucune réserve sur le risque incendie présenté par les installations électriques notamment en cellules n'a été émise. Par ailleurs, s'il est constant que les cellules ne sont équipées, quel que soit le nombre de personnes qui l'occupent, que d'une seule prise électrique, sur laquelle sont branchées des multiprises pour l'alimentation de nombreux appareils, il a été indiqué à l'audience que la présence d'un tableau électrique par cellule permettait la disjonction de l'installation en cas de surtension et que les dégradations ou défauts constatés sur les prises électriques, appareils d'éclairage et câbles à l'intérieur des cellules faisaient l'objet de réparations en tant que de besoin  par le service technique de l'établissement, dans le cadre des opérations de maintenance. <br/>
<br/>
              En ce qui concerne l'injonction tendant à la réalisation d'un diagnostic de l'amiante sur l'ensemble des bâtiments : <br/>
<br/>
              8. Il résulte de l'instruction qu'à la suite d'un repérage de la présence d'amiante et de sa localisation réalisé entre novembre et décembre 2017 par le bureau Veritas sur l'ensemble des bâtiments du centre pénitentiaire de Ploemeur, le désamiantage de l'établissement a été inscrit au rang des opérations programmées avec, en mars 2021, le lancement, par la direction interrégionale des services pénitentiaires de Rennes, d'une consultation pour un marché d'assistance à maitrise d'ouvrage en vue de la réalisation de la mission de diagnostic, d'études et de faisabilité du programme.<br/>
<br/>
              En ce qui concerne l'injonction tendant à prendre toutes les dispositions pour s'assurer qu'aucune personne détenue au sein du quartier maison d'arrêt ne dorme sur un matelas à même le sol, y compris dans le cas où il s'agirait de répondre à une demande de certains détenus de partager la même cellule : <br/>
<br/>
              9. Ainsi que l'a relevé le juge des référés du tribunal administratif de Rennes, l'administration pénitentiaire ne dispose d'aucun pouvoir de décision en matière de mises sous écrou, qui relèvent exclusivement de l'autorité judiciaire, un centre pénitentiaire étant ainsi tenu d'accueillir, quel que soit l'espace disponible dont il dispose, la totalité des personnes mises sous écrou.<br/>
<br/>
              10. Il résulte de l'instruction qu'à la date de l'audience, deux personnes détenues dormaient, dans le quartier maison d'arrêt, sur un matelas posé à même le sol, à leur demande expresse. Au cours de cette audience, la directrice du centre pénitentiaire a confirmé qu'une cellule de trois serait proposée aux intéressés dès que possible. <br/>
<br/>
              En ce qui concerne l'injonction tendant à ce que l'administration prenne, dans les plus brefs délais, toute mesure de nature à améliorer l'aération naturelle des cellules, le cas échéant par la suppression des caillebotis scellés en 2013 sur les fenêtres et à réaliser les travaux de rénovation et réhabilitation du système de ventilation de l'établissement : <br/>
<br/>
              11. En premier lieu, il résulte de l'instruction que les fenêtres des cellules se composent d'un battant supérieur, équipé d'une vitre mobile basculant de haut en bas autour d'un axe central, et d'un battant inférieur fixe équipé de deux bandes vitrées, l'entrouverture du battant supérieur étant limitée par des caillebotis extérieurs installés en 2013 afin de répondre à des impératifs de sécurité et de salubrité. Dans ces conditions, l'amélioration de l'aération naturelle des cellules ne peut procéder que de mesures structurelles consistant en un changement des fenêtres ou des caillebotis afin de permettre une plus grande ouverture des battants. <br/>
<br/>
              12. En second lieu, il résulte de l'instruction qu'à la suite d'un état des lieux réalisé en mars 2021, le devis en vue de la réhabilitation et du remplacement de la gestion technique centralisée de la ventilation dont les dysfonctionnements ne sont pas contestés est en cours d'établissement. Dans l'attente de la réalisation de cette opération prévue pour 2022, le ministre indique que des travaux de maintenance curative pour le chauffage et la ventilation seront réalisés en mai 2021.<br/>
<br/>
              En ce qui concerne l'injonction tendant à prendre toutes les mesures de nature à assurer et à améliorer l'accès aux produits d'entretien des cellules, aux sacs poubelle et au papier hygiénique :<br/>
<br/>
              13. Il résulte de l'instruction que les poubelles sont sorties des cellules lors de la distribution du dîner en application de la note de service du 3 février 2021 et qu'un sac poubelle est distribué quotidiennement conformément à l'article 11 du règlement intérieur. Il résulte également de l'instruction que chaque arrivant reçoit deux rouleaux de papier hygiénique et des produits pour l'entretien de sa cellule ainsi qu'il ressort de la note de service du 23 mars 2021. Pour les personnes détenues sans ressources suffisantes, cette dotation est renouvelée une fois par mois pour le papier hygiénique et à la demande pour les produits d'entretien, conformément à la note de service du 25 février 2020. La distribution d'un rouleau de papier hygiénique et d'un flacon d'eau de javel est également possible sans condition de ressources à l'occasion des changements de draps, une fois tous les quinze jours, ainsi qu'il ressort de la fiche de poste de l'auxiliaire de la buanderie. Enfin, les détenus ont la possibilité d'acheter du papier hygiénique à la cantine au tarif de 15 centimes d'euro le rouleau. <br/>
<br/>
              14. Il s'ensuit que le garde des sceaux, ministre de la justice, est fondé à soutenir qu'en prononçant les injonctions faisant l'objet des points 8 à 13, le juge des référés du tribunal administratif de Rennes a entaché son ordonnance d'erreurs d'appréciation. <br/>
<br/>
              15. Il résulte de tout ce qui précède que le garde des sceaux, ministre de la justice, est fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Rennes a prononcé les injonctions contestées. <br/>
<br/>
              Sur l'appel incident de l'Ordre des avocats au barreau de Nantes et de la SFOIP : <br/>
<br/>
              16. Les requérants demandent à ce qu'il soit enjoint au garde des sceaux, ministre de la justice, de produire un descriptif détaillé du nombre de cellules du centre pénitentiaire de Ploemeur, de leur superficie et du mobilier qui les équipe ainsi que du nombre de personnes qui les occupe, et de refondre le mode de calcul des places disponibles, opérationnelles et théoriques, afin de tenir compte notamment de la superficie des cellules et des exigences européennes relatives à l'espace de vie minimum dont doivent disposer les personnes détenues en cellule.  <br/>
<br/>
              17. Compte tenu de ce qui a été dit au point 9 ci-dessus, les mesures demandées qui, par ailleurs portent sur des choix structurels de politique publique, ne sont pas au nombre de celles susceptibles d'être prises dans le cadre de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              18. Il résulte de ce qui précède que les conclusions nouvelles présentées par les requérants par la voie de l'appel incident doivent, en tout état de cause, être rejetées. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              19. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'article 1er de l'ordonnance du 17 mars 2021 du juge des référés du tribunal administratif de Rennes est annulé en ce qu'il enjoint au garde des sceaux, ministre de la justice, de : <br/>
- réaliser un diagnostic amiante de l'ensemble des bâtiments du centre pénitentiaire de Ploemeur,<br/>
- installer un système d'interphonie dans toutes les cellules du quartier maison d'arrêt, sauf impossibilité matérielle dûment justifiée,<br/>
- procéder à la réalisation de travaux de mise aux normes des installations électriques, incluant notamment l'installation, dans chaque cellule, d'un nombre de prises électriques suffisant au regard du nombre de personnes hébergées en leur sein,<br/>
- prendre toutes les dispositions pour s'assurer qu'aucune personne détenue au sein du quartier maison d'arrêt ne dorme sur un matelas à même le sol, <br/>
- prendre toute mesure de nature à améliorer l'aération naturelle des cellules, le cas échéant par la suppression des caillebotis scellés en 2013 sur les fenêtres,<br/>
- réaliser les travaux de rénovation et réhabilitation du système de ventilation de l'établissement,<br/>
- prendre toute mesure de nature à assurer et à améliorer l'accès aux produits d'entretien des cellules, ainsi qu'aux sacs-poubelle, devant être ramassés quotidiennement, et au papier hygiénique.<br/>
Article 2 : L'appel incident de l'Ordre des avocats au barreau de Nantes et de la Section française de l'Observatoire international des prisons ainsi que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : La présente ordonnance sera notifiée au garde des sceaux, ministre de la justice, à l'Ordre des avocats au barreau de Nantes et à la Section française de l'Observatoire international des prisons. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
