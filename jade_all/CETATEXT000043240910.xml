<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240910</ID>
<ANCIEN_ID>JG_L_2021_02_000000432960</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240910.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 11/02/2021, 432960, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432960</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Nissen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432960.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... E... a demandé au tribunal administratif de Lille de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux mis à sa charge au titre de l'année 2009. Par un jugement n° 1405467 du 21 décembre 2017, le tribunal administratif de Lille a prononcé un non-lieu à statuer à hauteur de la somme de 2 184 euros dégrevée en cours d'instance et rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 18DA00450 du 21 mai 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par M. E... contre ce jugement en tant qu'il rejette le surplus de sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juillet et 24 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... F..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme D... A..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi et Texier, avocat de M. E... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. E... a fait l'objet d'un examen de sa situation fiscale personnelle au titre des années 2009 et 2010 à l'issue duquel l'administration fiscale a réintégré dans son revenu imposable au titre de l'année 2009, d'une part, des revenus fonciers non déclarés pour un montant de 13 974 euros, selon la procédure de rectification contradictoire, et, d'autre part, des sommes regardées comme des revenus d'origine indéterminée pour un montant de 117 449 euros, selon la procédure de taxation d'office, et l'a en conséquence assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, assortis d'une pénalité de 40 % pour manquement délibéré. Par un jugement du 21 décembre 2017, le tribunal administratif de Lille a prononcé un non-lieu à statuer à hauteur du dégrèvement intervenu en cours d'instance et résultant de la substitution de la pénalité de 10 % pour inexactitude ou omission de déclaration à la majoration de 40 % pour manquement délibéré appliquée au rappel notifié en matière de revenus fonciers, et rejeté le surplus de la demande en décharge de ces impositions. M. E... se pourvoit en cassation contre l'arrêt du 21 mai 2019 par lequel la cour administrative d'appel de Douai a rejeté son appel contre ce jugement. <br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. E... a soutenu que la procédure était irrégulière du fait que l'administration avait exercé son droit de communication alors qu'il était incarcéré. La cour a répondu à ce moyen en jugeant que l'incarcération du contribuable ne faisait pas obstacle ce que l'administration exerce son droit de communication en application de l'article L. 83 du livre des procédures fiscales. M. E... n'est pas fondé à soutenir que la cour aurait, par ces motifs, commis une erreur de droit en s'abstenant de rechercher si l'incarcération de M. E... présentait ou non les caractères de la force majeure, dès lors que ce moyen, qui n'est pas d'ordre public, n'était pas invoqué devant elle.<br/>
<br/>
              Sur la pénalité pour manquement délibéré :<br/>
<br/>
              3. Aux termes de l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré ; / (...) ". Aux termes de l'article L. 195 A du livre des procédures fiscales : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs, de la taxe sur la valeur ajoutée et des autres taxes sur le chiffre d'affaires, des droits d'enregistrement, de la taxe de publicité foncière et du droit de timbre, la preuve de la mauvaise foi et des manoeuvres frauduleuses incombe à l'administration ".<br/>
<br/>
              4. Il résulte de ces dispositions que la pénalité pour manquement délibéré a pour seul objet de sanctionner la méconnaissance par le contribuable de ses obligations déclaratives. Pour établir ce manquement délibéré, l'administration doit apporter la preuve, d'une part, de l'insuffisance, de l'inexactitude ou du caractère incomplet des déclarations et, d'autre part, de l'intention de l'intéressé d'éluder l'impôt. Pour établir le caractère intentionnel du manquement du contribuable à son obligation déclarative, l'administration doit se placer au moment de la déclaration ou de la présentation de l'acte comportant l'indication des éléments à retenir pour l'assiette ou la liquidation de l'impôt. Si l'administration se fonde également sur des éléments tirés du comportement du contribuable pendant la vérification, la mention d'un tel motif, qui ne peut en lui-même justifier l'application d'une telle pénalité, ne fait pas obstacle à ce que le manquement délibéré soit regardé comme établi dès lors que les conditions rappelées ci-dessus sont satisfaites.<br/>
<br/>
              5. Par l'arrêt attaqué, la cour a jugé que l'administration fiscale, en faisant valoir l'absence de toute explication probante sur l'origine des sommes en cause et l'absence de collaboration de M. E... lors des opérations de contrôle, apportait la preuve qui lui incombe du caractère délibéré du manquement déclaratif reproché à M. E... en ce qui concerne les revenus d'origine indéterminée. En se fondant ainsi exclusivement sur des éléments tirés du comportement du contribuable pendant la vérification, alors qu'ils ne pouvaient en eux-mêmes, ainsi qu'il a été dit au point 4, justifier l'application de la pénalité, la cour a commis une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi relatifs à la pénalité de 40% pour manquement délibéré, M. E... est fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il statue sur cette pénalité.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à M. E... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 mai 2019 de la cour administrative d'appel de Douai est annulé en tant qu'il statue sur la pénalité pour manquement délibéré.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Douai.<br/>
Article 3 : L'Etat versera une somme de 2 000 euros à M. E... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... E... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
