<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036784467</ID>
<ANCIEN_ID>JG_L_2018_03_000000404221</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/78/44/CETATEXT000036784467.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/03/2018, 404221, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404221</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:404221.20180330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 octobre 2016 et 29 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des agents phares et balises et sécurité maritime (SNAPBSM CGT) demande au Conseil d'Etat ;<br/>
<br/>
              1°)  d'annuler pour excès de pouvoir la décision du 21 mars 2016, par laquelle le ministre de l'environnement, de l'énergie et de la mer a rejeté sa demande tendant à faire bénéficier les fonctionnaires visés à l'article 157 de la loi de finances pour 2011 de la bonification de pension de retraite allouée aux ouvriers des parcs et ateliers (OPA) ayant bénéficié de l'allocation de cessation anticipée d'activité des travailleurs de l'amiante ;<br/>
<br/>
              2°) d'enjoindre au Premier Ministre d'adopter, dans un délai qui ne saurait excéder quatre mois, un décret modifiant le décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer, en alignant la situation des personnels qu'il vise sur celles des OPA des services du ministère au regard des modalités de calcul de leurs pensions de retraite ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2010-1657 du 29 décembre 2010;<br/>
              - le décret n°2001-1269 du 21 décembre 2001 ;<br/>
              - le décret n°2004-1056 du 5 octobre 2004 ;<br/>
              - le décret n°2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2013-435 du 27 mai 2013;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du syndicat national des agents phares et balises et sécurité maritime ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par courrier du 2 décembre 2015 adressé à la directrice des ressources humaines du ministère de l'environnement, de l'énergie et de la mer, le syndicat national des agents phares et balises et sécurité maritime, après avoir estimé que les modalités de calcul de la pension de retraite des ouvriers des parcs et ateliers de l'Etat (OPA) ayant bénéficié de l'allocation spécifique de cessation anticipée d'activité des travailleurs de l'amiante (ACAATA) leur assurent un revenu de remplacement proche de 100 % du salaire perçu avant leur cessation de fonction, a demandé qu'une telle mesure soit étendue aux fonctionnaires du ministère qui ont bénéficié de la même allocation. Par lettre du 21 mars 2016, la directrice des ressources humaines a rejeté cette demande. Le syndicat demande l'annulation de cette décision et à ce qu'il soit enjoint à l'Etat de modifier le décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer en ce qui concerne les modalités de calcul des pensions de retraite des personnes qu'il vise pour les aligner sur celles dont bénéficient les OPA.<br/>
<br/>
              Sur l'exception d'incompétence opposée par le ministre :<br/>
<br/>
              2. Aux termes de l'article 157 de la loi du 29 décembre 2010 portant loi de finances pour 2011 : " Les fonctionnaires et les agents non titulaires exerçant ou ayant exercé certaines fonctions dans des établissements ou parties d'établissement de construction ou de réparation navales du ministère chargé de la mer pendant les périodes au cours desquelles y étaient traités l'amiante ou des matériaux contenant de l'amiante peuvent demander à bénéficier d'une cessation anticipée d'activité et percevoir à ce titre une allocation spécifique. /  Cette allocation ne peut se cumuler avec une pension civile de retraite. / La durée de la cessation anticipée d'activité est prise en compte pour la constitution et la liquidation des droits à pension des fonctionnaires qui sont exonérés du versement des retenues pour pension. / Un décret en Conseil d'Etat fixe les conditions d'application du présent article, notamment les conditions d'âge, de cessation d'activité ainsi que les modalités d'affiliation au régime de sécurité sociale et de cessation du régime selon l'âge de l'intéressé et ses droits à pension ". Il résulte de ces dispositions que le législateur a habilité le pouvoir réglementaire à modifier les modalités de calcul des pensions de retraite des fonctionnaires et des agents non titulaires qu'elles visent, pour en préciser les conditions d'application. Par suite, le ministre n'est pas fondé à soutenir que la juridiction administrative ne peut connaître des conclusions de la requête au motif qu'elles visent les rapports entre le pouvoir exécutif et le Parlement.<br/>
<br/>
              Sans qu'il soit besoin de se prononcer sur la recevabilité de la requête ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              3. Si le syndicat requérant soutient que la signataire de la décision attaquée n'était pas compétente pour se prononcer sur sa demande, dans la mesure où celle-ci impliquait une modification du décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer, qui a été pris pour l'application des dispositions précitées de l'article 157 de la loi de finances pour 2011, la directrice des ressources humaines de ce ministère avait compétence, en sa qualité de directrice d'administration centrale, pour refuser au nom du ministre, en  application de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, une telle modification.<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 4 du décret du 21 décembre 2001 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains ouvriers de l'Etat relevant du régime des pensions des ouvriers des établissements industriels de l'Etat : " la rémunération de référence, servant de base à la détermination du montant de l'allocation spécifique, est déterminée par la moyenne des rémunérations brutes perçues par l'ouvrier pendant les douze derniers mois de son activité, à l'exclusion de tout élément de rémunération lié à une affectation outre-mer ou à l'étranger et des indemnités ayant le caractère de remboursement de frais. Elle est revalorisée dans les mêmes conditions que les salaires des ouvriers de l'Etat en activité relevant du même département ministériel ". En vertu du premier alinéa du II de l'article 10 du même décret, dans sa rédaction issue du décret n° 2007-184 du 9 février 2007, les émoluments de base pris en compte pour la détermination du montant de la pension des bénéficiaires de l'ACAATA sont constitués, par dérogation aux dispositions de droit commun applicable aux OPA, par les éléments de la rémunération de référence revalorisée précitée, soumis à retenue pour pension.<br/>
<br/>
              5. S'il n'est pas contesté par le ministre que ces dispositions peuvent conduire à des revenus de remplacement proche de 100% du salaire perçu avant leur cessation de fonction par les OPA qui étaient rémunérés, avant de bénéficier de l'ACAATA, en fonction des salaires pratiqués dans l'industrie, cette circonstance s'explique essentiellement par les dispositions du I de l'article 42 du décret du 5 octobre 2004 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat, en vertu desquelles ces ouvriers supportent une retenue pour pension non seulement sur " la somme brute obtenue en multipliant par 1 759 le salaire horaire moyen déterminé d'après le nombre d'heures de travail effectif dans l'année et les gains y afférents constitués par le salaire proprement dit " mais aussi sur " la prime d'ancienneté, la prime de fonction, la prime de rendement ainsi que les heures supplémentaires, à l'exclusion de tout autre avantage, quelle qu'en soit la nature ".<br/>
<br/>
              6. Il est vrai, en premier lieu, ainsi que l'admet d'ailleurs le ministre, que le premier motif de la décision attaquée est entaché d'erreur de droit, en tant qu'il repose sur l'affirmation inexacte que les modalités de calcul de la pension de retraite dont l'extension est sollicitée seraient communes à l'ensemble des OPA et non pas propres à ceux qui ont bénéficié de l'ACAATA. Toutefois, le second motif de cette décision, qui est tiré de ce que la différence de traitement dont bénéficient les OPA vient de ce qu'ils cotisent durant leur carrière sur une grande partie de leurs primes, suffit à en justifier la solution.<br/>
<br/>
              7. Contrairement, en second lieu, à ce que soutient le syndicat requérant, ni le principe d'égalité, ni l'objectif qui était poursuivi par le législateur, à travers l'article 157 de la loi de finances pour 2011, et qui était, d'après les travaux préparatoires, d'établir un traitement identique entre les OPA, d'une part, et les fonctionnaires et agents non titulaires exposés au même risque, d'autre part, n'imposent au pouvoir réglementaire d'accorder à ces derniers, compte tenu de ce que leurs primes ne sont, pour l'essentiel, par soumises à retenue pour pension, le même avantage en terme de retraite, que celui qui a été mentionné au point 5.<br/>
<br/>
              8. Il résulte de ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation de la décision attaquée. Par suite, ses conclusions aux fin d'injonction et au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du syndicat national des agents phares et balises et sécurité maritime est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat national des agents phares et balises et sécurité maritime et au ministre de l'environnement, de l'énergie et de la mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
