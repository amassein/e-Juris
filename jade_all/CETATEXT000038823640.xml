<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038823640</ID>
<ANCIEN_ID>JG_L_2019_07_000000423893</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/82/36/CETATEXT000038823640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 24/07/2019, 423893, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423893</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:423893.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Immobilière Carrefour a demandé au tribunal administratif de Nice de prononcer, à titre principal, la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2013 et 2014 dans les rôles de la commune d'Antibes (Alpes-Maritimes), à raison d'un local situé au sein du centre commercial Cap 3000, à concurrence de 437 277 euros et, à titre subsidiaire, à concurrence de 308 638 euros. Cette société a également demandé au même tribunal administratif de prononcer, à titre principal, la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2014 et 2015 dans les rôles de la même commune, à raison du même local, à concurrence de 440 047 euros et, à titre subsidiaire, à concurrence de 310 595 euros. Par un jugement n° 1502118-1601882 du 4 juillet 2018, ce tribunal a rejeté ses demandes.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 septembre et 5 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Immobilière Carrefour demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant les affaires au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Immobiliere Carrefour ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 732-1-1 du code de justice administrative : <br/>
" Sans préjudice de l'application des dispositions spécifiques à certains contentieux prévoyant que l'audience se déroule sans conclusions du rapporteur public, le président de la formation de jugement ou le magistrat statuant seul peut dispenser le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience sur tout litige relevant des contentieux suivants : (...) <br/>
/ 5° Taxe d'habitation et taxe foncière sur les propriétés bâties afférentes aux locaux d'habitation et à usage professionnel au sens de l'article 1496 du code général des impôts ainsi que contribution à l'audiovisuel public (...) ". Il résulte de ces dispositions que le rapporteur public ne peut être dispensé de prononcer des conclusions dans un litige relatif à la taxe foncière sur les propriétés bâties portant sur des biens dont la valeur locative n'a pas été déterminée en application de l'article 1496 du code général des impôts. Ainsi, la dispense de conclusions permise par les dispositions de l'article R. 732-1-1 ne peut s'appliquer au jugement d'un litige portant sur l'évaluation de locaux affectés à une activité commerciale ou industrielle.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la demande de la société Immobilière Carrefour était relative à des cotisations de taxe foncière sur les propriétés bâties établies, au titre des années en litige, à raison d'un bâtiment dont la valeur locative avait été déterminée conformément aux règles de l'article 1498 du code général des impôts applicables aux locaux commerciaux et biens divers. Dès lors, le rapporteur public ne pouvait être dispensé de prononcer des conclusions sur un tel litige. Par suite, la société Immobilière Carrefour est fondée à soutenir que le jugement du 4 juillet 2018, intervenu à la suite d'une audience qui n'a pas donné lieu au prononcé de conclusions du rapporteur public, a été rendu à l'issue d'une procédure irrégulière et à demander pour ce motif, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'annulation de ce jugement. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la société Immobilière Carrefour au titre de l'article <br/>
L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 4 juillet 2018 est annulé.<br/>
<br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Nice.<br/>
Article 3 : L'Etat versera une somme de 2 000 euros à la société Immobilière Carrefour au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée Immobilière Carrefour et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
