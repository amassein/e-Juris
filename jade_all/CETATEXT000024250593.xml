<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024250593</ID>
<ANCIEN_ID>JG_L_2011_06_000000347429</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/25/05/CETATEXT000024250593.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 24/06/2011, 347429</TITRE>
<DATE_DEC>2011-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347429</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:347429.20110624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 11 et 25 mars et 7 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE ATEXO, dont le siège est 17, boulevard des Capucines à Paris (75002), représentée par son directeur ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1100474 du 25 février 2011 par laquelle le juge des référés du tribunal administratif de Bordeaux, statuant en application de l'article L. 551-1 du code de justice administrative, a rejeté sa demande tendant à l'annulation de la procédure engagée par l'association " Marchés publics d'Aquitaine " (AMPA) pour la passation d'un accord-cadre portant sur l'exploitation, l'hébergement, la maintenance et l'évolution des fonctionnalités d'un logiciel de gestion des procédures de marché des adhérents de l'association et a mis à sa charge le versement de la somme de 1 000 euros à cette association au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) de mettre à la charge de l'association " Marchés publics d'Aquitaine " le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu la loi du 1er juillet 1901 relative au contrat d'association ;<br/>
<br/>
              Vu l'ordonnance n° 2009-515 du 7 mai 2009 ;<br/>
<br/>
              Vu le décret du 26 octobre 1849 réglant les formes de procéder du Tribunal des conflits ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Foussard, avocat de la SOCIETE ATEXO et de la SCP Odent, Poulet, avocat de l'association " Marchés publics d'Aquitaine " (AMPA), <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de la SOCIETE ATEXO et à la SCP Odent, Poulet, avocat de l'association " Marchés publics d'Aquitaine " (AMPA) ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bordeaux que la SOCIETE ATEXO a, sur le fondement de ces dispositions, saisi ce juge d'une demande tendant à l'annulation de la procédure de passation engagée par l'association " Marchés publics d'Aquitaine " en vue de la conclusion d'un accord-cadre pour l'exploitation, l'évolution des fonctionnalités et l'hébergement d'un logiciel de gestion des procédures de passation des marchés des adhérents de l'association ; que, par l'ordonnance attaquée du 25 février 2011, le juge des référés du tribunal administratif de Bordeaux, retenant la compétence de la juridiction administrative, a néanmoins rejeté sa demande ; <br/>
<br/>
              Considérant qu'aux termes de l'article 35 du décret du 26 octobre 1849 : " Lorsque le Conseil d'Etat statuant au contentieux, la Cour de cassation ou toute autre juridiction statuant souverainement et échappant ainsi au contrôle tant du Conseil d'Etat que de la Cour de cassation, est saisi d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse et mettant en jeu la séparation des autorités administratives et judiciaires, la juridiction saisie peut, par décision ou arrêt motivé qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence " ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 8 du code des marchés publics : " I.- Des groupements de commandes peuvent être constitués :  1° Entre des services de l'Etat et les établissements publics de l'Etat autres que ceux ayant un caractère industriel et commercial ou entre de tels établissements publics seuls ; / 2° Entre des collectivités territoriales, entre des établissements publics locaux ou entre des collectivités territoriales et des établissements publics locaux ; (...) / 4° Entre une ou plusieurs personnes publiques mentionnées aux 1° et 2° ci-dessus et une ou plusieurs personnes morales de droit privé, ou un ou plusieurs établissements publics nationaux à caractère industriel et commercial, groupements d'intérêt public, groupements de coopération sociale ou médico-sociale ou groupements de coopération sanitaire, à condition que chacun des membres du groupement applique, pour les achats réalisés dans le cadre du groupement, les règles prévues par le présent code. / II.- Une convention constitutive est signée par les membres du groupement. / Elle définit les modalités de fonctionnement du groupement.  / Elle désigne un coordonnateur parmi les membres du groupement, ayant la qualité de pouvoir adjudicateur soumis au présent code ou à l'ordonnance du 6 juin 2005 susmentionnée. / Celui-ci est chargé de procéder, dans le respect des règles prévues par le présent code, à l'organisation de l'ensemble des opérations de sélection d'un ou de plusieurs cocontractants. / Chaque membre du groupement s'engage, dans la convention, à signer avec le cocontractant retenu un marché à hauteur de ses besoins propres, tels qu'il les a préalablement déterminés. (...) / V.- (...) Les marchés passés par un groupement au sein duquel les collectivités territoriales ou les établissements publics locaux sont majoritaires obéissent aux règles prévues par le présent code pour les collectivités territoriales. (...) VI.- Chaque membre du groupement, pour ce qui le concerne, signe le marché et s'assure de sa bonne exécution.(...) / VII.- La convention constitutive du groupement peut aussi avoir prévu que le coordonnateur sera chargé : / 1° Soit de signer et de notifier le marché ou l'accord-cadre, chaque membre du groupement, pour ce qui le concerne, s'assurant de sa bonne exécution ; / 2° Soit de signer le marché ou l'accord-cadre, de le notifier et de l'exécuter au nom de l'ensemble des membres du groupement " ; qu'une association dotée d'une personnalité morale distincte de celle de chacun de ses membres ne saurait être regardée, à l'égard de ceux-ci, ni comme un groupement de commandes défini par ces dispositions, susceptible de conclure un marché en application du code des marchés publics, ni comme le coordonnateur désigné parmi les membres du groupement qu'ils constitueraient ensemble ;<br/>
<br/>
              Considérant, en second lieu, qu'il ressort des pièces du dossier soumis au juge des référés, d'une part, que l'association " Marchés publics d'Aquitaine " a été créée à l'initiative de la région Aquitaine, de la communauté urbaine de Bordeaux et de la commune de Floirac, qu'elle rassemble plus de trois cents personnes publiques, qui lui procurent ensemble l'essentiel de ses ressources par leurs cotisations destinées à couvrir ses frais de fonctionnement, leurs contributions aux frais relatifs au site dématérialisé de gestion des procédures de passation des marchés et leurs subventions éventuelles, mais aussi quelques personnes privées, et qu'aucun des membres de l'association, eu égard aux règles d'organisation et de fonctionnement de celle-ci, n'en contrôle seul l'organisation et le fonctionnement, ni, au surplus, ne lui procure l'essentiel de ses ressources, compte tenu des règles de répartition entre les membres des contributions aux dépenses de l'association ; que, d'autre part, il ressort également des pièces du dossier soumis au juge des référés que cette association a été constituée à seule fin, selon ses statuts, de mettre à disposition de ses adhérents une plateforme dématérialisée commune de gestion des procédures de passation des marchés publics ; que l'accord-cadre faisant l'objet de la procédure de passation contestée a pour finalité de permettre l'exploitation, l'évolution des fonctionnalités et l'hébergement du logiciel de gestion des procédures de passation des marchés des adhérents de l'association et ainsi pour objet exclusif de répondre aux besoins des membres de l'association ; qu'il ne ressort pas des pièces du dossier soumis au juge des référés qu'il existerait entre l'association et ses adhérents de convention lui donnant mandat pour conduire la procédure de mise en concurrence et conclure cet accord-cadre ;<br/>
<br/>
              Considérant que le litige né de l'action de la SOCIETE ATEXO tendant à l'annulation de la procédure engagée par l'association " Marchés publics d'Aquitaine " pour la passation de cet accord-cadre présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849 ; qu'au surplus, le président du tribunal de grande instance de Bordeaux, primitivement saisi par la SOCIETE ATEXO sur le fondement de l'article 2 de l'ordonnance du 7 mai 2009 relative aux procédures de recours applicables aux contrats de la commande publique et de l'article 1441-1 du code de procédure civile, a, par une ordonnance de référé du 7 février 2011, rendue en dernier ressort, décliné la compétence des tribunaux de l'ordre judiciaire ; que, par suite, il y a lieu de renvoyer au Tribunal des conflits le soin de décider sur la question de compétence ainsi soulevée et de surseoir à toute procédure jusqu'à la décision de ce tribunal ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la SOCIETE ATEXO jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridictions compétent pour statuer sur ses conclusions.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE ATEXO et à l'association Marchés publics d'Aquitaine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. CONTRATS. - DEMANDE D'ANNULATION D'UNE PROCÉDURE DE PASSATION, PAR UNE ASSOCIATION REGROUPANT NOTAMMENT PLUS DE 300 PERSONNES PUBLIQUES, D'UN ACCORD-CADRE CONCERNANT LA GESTION DÉMATÉRIALISÉE DES MARCHÉS PUBLICS DE SES ADHÉRENTS - QUESTION DE COMPÉTENCE SOULEVANT UNE DIFFICULTÉ SÉRIEUSE JUSTIFIANT UN RENVOI AU TRIBUNAL DES CONFLITS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - CONDITIONS DANS LESQUELLES LE JUGE DU RÉFÉRÉ PRÉCONTRACTUEL EXAMINE UNE QUESTION DE COMPÉTENCE DE LA JURIDICTION - CONDITIONS DE DROIT COMMUN, ET NON CONDITIONS PARTICULIÈRES S'APPLIQUANT À D'AUTRES VOIES DE RÉFÉRÉS [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-09-04-01 PROCÉDURE. TRIBUNAL DES CONFLITS. SAISINE SUR RENVOI D'UNE JURIDICTION. DIFFICULTÉ SÉRIEUSE DE COMPÉTENCE. - DEMANDE D'ANNULATION D'UNE PROCÉDURE DE PASSATION ENGAGÉE PAR UNE ASSOCIATION REGROUPANT NOTAMMENT PLUS DE 300 PERSONNES PUBLIQUES, D'UN ACCORD-CADRE CONCERNANT LA GESTION DÉMATÉRIALISÉE DES MARCHÉS PUBLICS DE SES ADHÉRENTS.
</SCT>
<ANA ID="9A"> 17-03-02-03 Une action tendant à l'annulation de la procédure de passation, par une association regroupant notamment plus de 300 personnes publiques, d'un accord-cadre ayant pour finalité de permettre l'exploitation, l'évolution des fonctionnalités et l'hébergement du logiciel de gestion des procédures de passation des marchés des adhérents de l'association et ayant ainsi pour objet exclusif de répondre aux besoins des membres de l'association, présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849.</ANA>
<ANA ID="9B"> 39-08-015-01 Eu égard à la nature de la procédure de référé précontractuel, ne trouve pas à s'y appliquer la jurisprudence selon laquelle les demandes présentées au juge des référés doivent seulement ne pas être manifestement insusceptibles de se rattacher à un litige relevant de la compétence de la juridiction administrative.</ANA>
<ANA ID="9C"> 54-09-04-01 Une action tendant à l'annulation de la procédure de passation, par une association regroupant notamment plus de 300 personnes publiques, d'un accord-cadre ayant pour finalité de permettre l'exploitation, l'évolution des fonctionnalités et l'hébergement du logiciel de gestion des procédures de passation des marchés des adhérents de l'association et ayant ainsi pour objet exclusif de répondre aux besoins des membres de l'association, présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 26 octobre 1849.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour le référé-liberté, CE, 22 octobre 2010, Putswo, n° 335051, p. 420.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
