<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800145</ID>
<ANCIEN_ID>JG_L_2014_11_000000380253</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 13/11/2014, 380253, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380253</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:380253.20141113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-268 du 27 février 2014, modifié par décret n° 2014-351 du 19 mars 2014, portant délimitation des cantons dans le département des Vosges ;<br/>
<br/>
              2°) d'enjoindre à l'administration ministérielle compétente de procéder à une nouvelle délimitation des cantons ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département des Vosges, dont le nombre passe de trente-et-un à dix-sept, compte tenu de l'exigence de réduction du nombre des cantons de ce département résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              3. Considérant que s'il est soutenu que le décret attaqué a été élaboré sans tenir compte des données démographiques définitives les plus récentes, l'article 71 du décret du 18 octobre 2013 dans sa rédaction applicable à la date du décret attaqué et dont l'illégalité n'est pas contestée, dispose toutefois que " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ; qu'il est constant que les nouveaux cantons du département des Vosges ont été délimités sur la  base des données authentifiées par le décret du 27 décembre 2012, que par suite, le moyen tiré de ce que les données retenues par le redécoupage des cantons de ce département ne correspondraient pas à la réalité démographique ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant que si le requérant soutient que l'article 17 du décret du 27 février 2014 mentionne séparément les communes de Fontenoy-le-Château et Le Magny alors que ces deux communes avaient fusionné dans la commune de Fontenoy-le-Château par un arrêté préfectoral du 26 décembre 2012, il ressort des pièces du dossier que cette inexactitude, qui a d'ailleurs été rectifiée par l'article 18 du décret du 19 mars 2014 portant correction d'erreurs matérielles dans les décrets délimitant les cantons de divers départements, ne saurait, eu égard à sa portée limitée, être regardée comme ayant une incidence sur la légalité du décret attaqué ;<br/>
<br/>
              5. Considérant que, contrairement à ce qui est soutenu, la règle de continuité du territoire de chaque canton posée par les dispositions du b) du III de l'article L. 3113-2 du code général des collectivités territoriales n'a pas été méconnue ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la requête de M. B...doit être rejetée, y compris les conclusions présentées aux fins d'injonction ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente décision sera notifiée M. A...B...et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
