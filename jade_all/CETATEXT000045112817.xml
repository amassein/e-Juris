<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045112817</ID>
<ANCIEN_ID>JG_L_2022_01_000000449496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/11/28/CETATEXT000045112817.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 31/01/2022, 449496</TITRE>
<DATE_DEC>2022-01-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; BALAT ; SCP MELKA-PRIGENT-DRUSCH</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:449496.20220131</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. K... Y... et Mme T... Y... ainsi que M. Q... M... et Mme N... M... ont demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 5 janvier 2019 par laquelle le maire de Rillieux-la-Pape a tacitement délivré à M. U... G... et Mme J... F... un permis de construire une maison individuelle et une piscine. Par un jugement n° 1905421 du 10 décembre 2020, le tribunal administratif de Lyon a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 février, 7 mai et 20 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. Z... I..., à qui le permis litigieux a été transféré, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge solidaire de M. et Mme Y... et M. et Mme M... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. I..., à Me Balat, avocat de M. et Mme Y... et à la SCP Melka-Prigent-Drusch, avocat de la commune de Rillieux-la-Pape ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 12 avril 2018, le maire de Rillieux-la-Pape ne s'est pas opposé à la déclaration préalable de lotissement pour la division d'un terrain. Le 5 janvier 2019, le maire a tacitement délivré à M. G... et Mme F... un permis de construire une maison individuelle et une piscine sur une parcelle issue de cette division. Ce permis de construire a ensuite été transféré à M. I... le 7 janvier 2020. Par un jugement du 10 décembre 2020, contre lequel M. I... se pourvoit en cassation, le tribunal administratif de Lyon a fait droit à la demande de M. et Mme Y... et M. et Mme M..., voisins du projet, tendant à l'annulation pour excès de pouvoir de ce permis de construire au motif que, le projet autorisé étant de nature à compromettre et à rendre plus onéreux l'exécution du plan local d'urbanisme et de l'habitat de la métropole de Lyon en cours d'élaboration, le maire avait commis une erreur manifeste d'appréciation en n'opposant par un sursis à statuer à la demande de permis de construire. <br/>
<br/>
              2. D'une part, l'article L. 424-1 du code de l'urbanisme prévoit les différentes hypothèses permettant à l'autorité compétente de surseoir à statuer sur des demandes d'autorisations concernant des travaux, constructions ou installations. Parmi ces hypothèses, le deuxième alinéa de cet article renvoie notamment aux cas prévus à l'article L. 153-11 du même code, dont le troisième alinéa dispose que : " L'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 424-1, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan dès lors qu'a eu lieu le débat sur les orientations générales du projet d'aménagement et de développement durable. ". <br/>
<br/>
              3. D'autre part, aux termes du premier alinéa de l'article L. 442-14 du même code : " Lorsque le lotissement a fait l'objet d'une déclaration préalable, le permis de construire ne peut être refusé ou assorti de prescriptions spéciales sur le fondement de dispositions d'urbanisme nouvelles intervenues depuis la date de non-opposition à la déclaration préalable, et ce pendant cinq ans à compter de cette même date ". <br/>
<br/>
              4. Il résulte de l'article L. 442-14 du code de l'urbanisme que l'autorité compétente ne peut légalement surseoir à statuer, sur le fondement de l'article L. 424-1 du même code, sur une demande de permis de construire présentée dans les cinq ans suivant une décision de non-opposition à la déclaration préalable de lotissement au motif que la réalisation du projet de construction serait de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d'urbanisme. <br/>
<br/>
              5. Par suite, après avoir relevé que le maire de Rillieux-la-Pape avait, le 12 avril 2018, pris une décision de non-opposition à la déclaration préalable de lotissement, le tribunal administratif a commis une erreur de droit en jugeant que ce maire avait entaché sa décision d'illégalité en n'opposant pas, le 5 janvier 2019, soit moins de cinq ans après cette décision de non opposition, un sursis à statuer à la demande de permis de construire présentée sur une parcelle du lotissement ainsi autorisé, au motif que le projet litigieux était de nature à compromettre l'exécution du futur plan local d'urbanisme et de l'habitat de la métropole de Lyon. <br/>
<br/>
              6. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. I... est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. I..., qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme Y... et M. et Mme M... le versement à M. I... d'une somme totale de 3 000 euros au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 10 décembre 2020 du tribunal administratif de Lyon est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon. <br/>
Article 3 : M. et Mme Y... et M. et Mme M... verseront une somme totale de 3 000 euros à M. I... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. et Mme Y... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. Z... I..., à M. K... AA... et Mme T... AA... et à M. Q... M... et Mme N... M.... <br/>
Copie en sera adressée à la commune de Rillieux-la-Pape.<br/>
              Délibéré à l'issue de la séance du 17 janvier 2022 où siégeaient : Mme Christine Maugüé, président adjointe de la section du contentieux, présidant ; Mme A... W..., Mme D... V..., présidentes de chambre ; Mme B... H..., Mme P... S..., M. R... L..., M. C... O..., Mme Carine Chevrier, conseillers d'Etat et Mme Manon Chonavel, auditrice-rapporteure. <br/>
<br/>
<br/>
Rendu le 31 janvier 2022.<br/>
<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme X... E...<br/>
<br/>
<br/>
	La République mande et ordonne à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales en ce qui la concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour la secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PROCÉDURES D'INTERVENTION FONCIÈRE. - LOTISSEMENTS. - AUTORISATION DE LOTIR. - CRISTALLISATION DES RÈGLES D’URBANISME APPLICABLES (ART. L. 442-14 DU CODE DE L’URBANISME) – CONSÉQUENCE – FACULTÉ D’OPPOSER UN SURSIS À STATUER À UNE DEMANDE DE PERMIS DE CONSTRUIRE AU MOTIF QUE LA CONSTRUCTION SERAIT DE NATURE À COMPROMETTRE OU À RENDRE PLUS ONÉREUSE L'EXÉCUTION D'UN PLU – ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-025-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PERMIS DE CONSTRUIRE. - NATURE DE LA DÉCISION. - SURSIS À STATUER. - MOTIFS. - CONSTRUCTION DE NATURE À COMPROMETTRE OU À RENDRE PLUS ONÉREUSE L'EXÉCUTION D'UN PLU – MOTIF OPPOSABLE DANS LES CINQ ANS SUIVANT UNE NON-OPPOSITION À UNE DÉCLARATION PRÉALABLE DE LOTISSEMENT – ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 68-02-04-02 Il résulte de l’article L. 442-14 du code de l’urbanisme que l’autorité compétente ne peut légalement surseoir à statuer, sur le fondement de l’article L. 424-1 du même code, sur une demande de permis de construire présentée dans les cinq ans suivant une décision de non-opposition à la déclaration préalable de lotissement au motif que la réalisation du projet de construction serait de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d’urbanisme (PLU).</ANA>
<ANA ID="9B"> 68-03-025-01-01 Il résulte de l’article L. 442-14 du code de l’urbanisme que l’autorité compétente ne peut légalement surseoir à statuer, sur le fondement de l’article L. 424-1 du même code, sur une demande de permis de construire présentée dans les cinq ans suivant une décision de non-opposition à la déclaration préalable de lotissement au motif que la réalisation du projet de construction serait de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d’urbanisme (PLU).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s’agissant de la cristallisation des dispositions d’urbanisme applicables résultant de l’annulation d’un refus de permis de construire, CE, 16 juillet 2010, SARL Francimo, n° 338860, T. pp. 1019-1023-1024. Comp., s’agissant de la cristallisation des dispositions d’urbanisme applicables résultant d’un certificat d’urbanisme, CE, 10 juillet 1987, Ministre de l'urbanisme c/ Foucault, n° 63010, p. 266 ; CE, 3 avril 2014, Commune de Langolen, n° 362735, T. p. 904.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
