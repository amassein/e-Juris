<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027570854</ID>
<ANCIEN_ID>JG_L_2007_05_000000298365</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/57/08/CETATEXT000027570854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 30/05/2007, 298365, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>298365</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP PARMENTIER, DIDIER</AVOCATS>
<RAPPORTEUR>Mme Catherine  Delort</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Séners</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2007:298365.20070530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 25 octobre 2006 et 8 novembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE CORRENS (Var), représentée par son maire ; la COMMUNE DE CORRENS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 1er de l'ordonnance du 9 octobre 2006 par laquelle le juge des référés du tribunal administratif de Nice a suspendu, à la demande de M. A...B..., l'exécution de la délibération en date du 13 juillet 2006 par laquelle le conseil municipal de la COMMUNE DE CORRENS a approuvé le plan local d'urbanisme de cette commune en tant qu'elle approuve le règlement de la zone UX et en tant qu'elle procède au changement de zonage de la partie des parcelles appartenant à M. B...classée précédemment en zone UB et désormais classée en zone N ; <br/>
<br/>
              2°) statuant au titre de la procédure de référé engagée, de rejeter la demande de suspension de l'exécution de la délibération du 13 juillet 2006 ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Delort, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Ricard, avocat de la COMMUNE DE CORRENS et de la SCP Parmentier, Didier, avocat de M.B..., <br/>
<br/>
              - les conclusions de M. François Séners, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
              Considérant que, par ordonnance du 9 octobre 2006, le juge des référés du tribunal administratif de Nice a suspendu, à la demande de M.B..., l'exécution de la délibération du conseil municipal en date du 13 juillet 2006 portant approbation du plan local d'urbanisme de la commune, en tant qu'elle approuve le règlement de la zone UX et en tant qu'elle procède au changement de zonage de la partie des parcelles appartenant à M. B...classée précédemment en zone UB et désormais classée en zone N ; que la COMMUNE DE CORRENS demande l'annulation de cette ordonnance ; <br/>
<br/>
              Sans qu'il soit  besoin d'examiner les autres moyens du pourvoi : <br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : "Quand une décision administrative, même de rejet, fait l'objet d'une  requête en annulation ou en réformation, le juge des référés, saisi d'une  demande en ce sens, peut ordonner la suspension de l'exécution de cette  décision, ou de certains de ses effets, lorsque l'urgence le justifie et  qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction,  un doute sérieux quant à la légalité de la décision" ; <br/>
<br/>
              Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière  suffisamment grave et immédiate, à un intérêt public, à la situation du  requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge  des référés d'apprécier concrètement, compte tenu des justifications  fournies par le requérant, si les effets de l'acte litigieux sont de  nature à caractériser une urgence justifiant que, sans attendre le  jugement de la requête au fond, l'exécution de la décision soit  suspendue ; qu'il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque  espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à estimer que  la suspension demandée revêtait un caractère d'urgence ; <br/>
<br/>
              Considérant, en premier lieu, que pour juger établie l'urgence à suspendre la délibération portant révision du plan local d'urbanisme en tant qu'elle approuve le règlement de la zone UX, destinée à l'implantation d'activités artisanales, le juge des référés s'est borné à relever que les parcelles appartenant à M. B...sont limitrophes de cette zone ; qu'en se fondant sur cette seule circonstance et en s'abstenant de prendre en compte l'argumentation de la commune relative à l'intérêt général qui s'attache, en vue du développement économique local, à l'exécution de la délibération attaquée sur ce point, il a entaché son ordonnance d'une erreur de droit ;  <br/>
<br/>
              Considérant, en second lieu, qu'en regardant comme également remplie la condition d'urgence pour prononcer la suspension de la délibération en cause en tant qu'elle classe en zone N une partie des parcelles appartenant à M. B...sans prendre en compte le souci de préservation des espaces naturels et des paysages de la commune et de cohérence du zonage que traduit ce changement, qui était invoqué devant lui, le juge des référés a commis une autre erreur de droit ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'il y a lieu d'annuler l'ordonnance attaquée en tant que, par son article 1er, elle suspend l'exécution de la délibération du conseil municipal du 13 juillet 2006 relative au plan local d'urbanisme de Correns en ce qui concerne le règlement de la zone UX et le changement de zonage de la partie des parcelles appartenant à M. B... classée précédemment en zone UB et désormais classée en zone N ; <br/>
<br/>
              Considérant qu'en application de l'article L. 821-2 du  code de justice administrative, il y a lieu de régler l'affaire dans cette limite au titre de la procédure de référé engagée par M. B...; <br/>
<br/>
              Considérant, en premier lieu, que si M. B...fait valoir que les parcelles lui appartenant sont limitrophes de la nouvelle zone UX, sur laquelle est autorisée l'implantation d'activités artisanales, cette seule circonstance ne  suffit pas à caractériser une atteinte grave et immédiate à ses intérêts permettant de regarder comme remplie sur ce point la condition d'urgence, alors surtout que la commune souligne l'importance de la création de cette zone pour le développement économique local ; <br/>
<br/>
              Considérant, en second lieu, qu'en faisant état des projets de construction qu'il a engagés précédemment et des contentieux auxquels ils ont donné lieu par suite de l'opposition de la commune, M. B...ne montre pas en quoi le classement en zone N d'une partie de ses parcelles porte à ses intérêts une atteinte suffisamment grave et immédiate pour caractériser une situation d'urgence justifiant la suspension du nouveau plan local d'urbanisme sur ce point, alors surtout que la commune expose de son côté que ce classement traduit un parti général d'aménagement des abords du village guidé par la volonté de préserver les espaces naturels et les paysages et d'assurer la cohérence du zonage ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que doivent être rejetées les conclusions de M. B...tendant à la suspension de l'exécution de la délibération du conseil municipal de la COMMUNE DE CORRENS du 13 juillet 2006 relative au plan local d'urbanisme de cette commune en ce qui concerne le règlement de la zone UX et le changement de zonage de la partie des parcelles lui appartenant classée précédemment en zone UB et désormais classée en zone N ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la COMMUNE DE CORRENS, qui n'est pas la partie perdante dans la  présente instance, la somme demandée par M. B...au titre des frais exposés par lui et non compris dans les dépens ; qu'il y a lieu en revanche, dans les circonstances de l'espèce,  de mettre à la charge de M. B...la somme de 500 euros au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 1er et 3 de l'ordonnance du 9 octobre 2006 du juge des référés du tribunal administratif de Nice sont annulés.<br/>
Article 2 : Les conclusions de la demande de M. B...devant le juge des référés du tribunal administratif de Nice relatives au règlement de la zone UX et au classement en zone N de parcelles lui appartenant sont rejetées.<br/>
Article 3 : M. B...versera à la COMMUNE DE CORRENS la somme de 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. B...tendant à l'application de l'article <br/>
L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE CORRENS et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
