<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026631924</ID>
<ANCIEN_ID>JG_L_2012_11_000000347901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/63/19/CETATEXT000026631924.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 14/11/2012, 347901, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347901.20121114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 28 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Yazid B et Mme Samira C épouse B, demeurant ... ;  M. et Mme B demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1102866 du 8 mars 2011 par laquelle le président du tribunal administratif de Paris a rejeté leur demande tendant à ce que soit ordonné à l'Etat, sur la base de l'article L. 441-2-3-1 du code de la construction et de l'habitation, de leur attribuer un logement sous astreinte ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ; <br/>
<br/>
              Vu la loi n° 2007-290 du 5 mars 2007 ;<br/>
<br/>
              Vu le décret n° 2007-1677 du 28 novembre 2007 ; <br/>
<br/>
              Vu le décret n° 2008-1227 du 27 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. et Mme B,<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de M. et Mme B ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, introduit par l'article 9 de la loi du 5 mars 2007 instituant le droit au logement opposable et portant diverses mesures en faveur de la cohésion sociale : "Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement./ (...)/ Ce recours est ouvert à compter du 1er décembre 2008 aux personnes mentionnées au deuxième alinéa du II de l'article L. 441-2-3 et, à compter du 1er janvier 2012, aux demandeurs mentionnés au premier alinéa du même II. (...)" ; qu'aux termes de l'article R. 441-16-1 du même code, dans sa rédaction applicable aux faits de l'espèce : " A compter du 1er décembre 2008, le recours devant la juridiction administrative prévu au I de l'article L. 441-2-3-1 peut être introduit par le demandeur (...) passé un délai de trois mois après qu'il a reçu notification de la décision de la commission de médiation (...). Dans les départements d'outre-mer et, jusqu'au 1er janvier 2014, dans les départements comportant au moins une agglomération (...) de plus de 300 000 habitants, ce délai est de six mois " ; que l'article R. 778-2 du code de justice administrative, introduit dans ce code par le décret du 27 novembre 2008 relatif au contentieux du droit au logement opposable, prévoit que le demandeur dispose d'un délai de quatre mois pour saisir le tribunal administratif à compter de l'expiration du délai défini par l'article R. 441-16-1 du code de la construction et de l'habitation et que ce délai de quatre mois ne lui est opposable que s'il est mentionné dans la notification de la décision de la commission de médiation ; que l'article 3 du même décret a prévu, dans sa rédaction initiale, que, par dérogation à ces dispositions, lorsqu'un requérant se prévaut d'une décision favorable d'une commission de médiation rendue avant le 1er janvier 2009 et qu'il n'a pas bénéficié de l'information relative aux voies et délais de recours, son recours doit être introduit au plus tard le 30 avril 2009 ; que cette date a été reportée au 31 décembre 2009 par un décret du 10 avril 2009 ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 22 mai 2008, dont les intéressés ont reçu notification le 29 mai suivant, la commission de médiation de Paris a reconnu que M. et Mme B étaient prioritaires et devaient se voir attribuer un logement en urgence ; que le préfet ne leur ayant pas adressé d'offre de logement dans le délai de six mois qui lui était imparti, M. et Mme B ont introduit, le 25 février 2011, un recours devant le tribunal administratif de Paris tendant à ce que soit ordonné leur relogement sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation ; <br/>
<br/>
              3. Considérant que le président du tribunal administratif a rejeté ce recours pour irrecevabilité manifeste au motif qu'en application de l'article 3 du décret du 27 novembre 2008, il aurait dû être présenté au plus tard le 31 décembre 2009 dès lors que ses auteurs se prévalaient d'une décision de la commission de médiation rendue avant le 1er janvier 2009 ; que, toutefois, M et Mme B avaient invoqué devant le juge un moyen tiré de l'illégalité des dispositions de l'article 3 du décret du 27 novembre 2008 pour soutenir que leur recours n'était pas tardif ; que le président du tribunal administratif ne pouvait, sans entacher son jugement d'une insuffisance de motivation, rejeter ce recours sans se prononcer sur l'exception tirée de l'illégalité des dispositions dont il a fait application ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son ordonnance doit être annulée ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que la loi du 5 mars 2007 a créé, au profit des bénéficiaires d'une décision favorable de la commission de médiation à laquelle l'administration n'a pas donné suite dans les délais impartis, une voie de recours spécifique devant le juge administratif ; que si le droit d'exercer ce recours s'acquiert, en principe, à l'expiration du délai de trois ou six mois, selon le cas, fixé à l'article R. 441-16-1 du code de la construction et de l'habitation, délai dans lequel l'administration doit faire une offre de logement, il résulte expressément des dispositions précitées  de l'article L. 441-2-3-1 du code de la construction et de l'habitation que ce droit au recours ne pouvait être exercé qu'à compter du 1er décembre 2008 ; que les demandeurs à l'égard desquels le délai imparti au préfet pour attribuer un logement avait expiré avant le 1er décembre 2008 n'ont ainsi acquis le droit de former un recours devant la juridiction administrative qu'à compter de cette dernière date ; que, par suite, le décret du 27 novembre 2008 a pu légalement fixer les conditions d'exercice de ce recours applicables à ces demandeurs sans porter atteinte à des droits acquis ; qu'il résulte de ce qui précède que le recours de M. et Mme B, formé postérieurement au 31 décembre 2009, terme fixé en vertu de l'article 3 du décret du 27 novembre 2008 pour l'introduction des recours formés contre les décisions rendues par les commissions de médiation avant le 1er janvier 2009, est tardif et, par suite, irrecevable ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n°1102866 du président du tribunal administratif de Paris du 8 mars 2011 est annulée. <br/>
<br/>
Article 2 : La demande présentée par M. et Mme B devant le tribunal administratif de Paris est rejetée.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. et Mme B est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. et Mme Yazid B et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - VOIE DE RECOURS OUVERTE AU PROFIT DES BÉNÉFICIAIRES D'UNE DÉCISION FAVORABLE DE LA COMMISSION DE MÉDIATION À LAQUELLE L'ADMINISTRATION N'A PAS DONNÉ SUITE DANS LES DÉLAIS IMPARTIS - DÉCRET DU 27 NOVEMBRE 2008 FIXANT LES CONDITIONS D'EXERCICE DE CE RECOURS - ATTEINTE AUX DROITS ACQUIS DES DEMANDEURS À L'ÉGARD DESQUELS LE DÉLAI AVAIT EXPIRÉ AVANT LE 1ER DÉCEMBRE 2008 - ABSENCE, DÈS LORS QUE L'ARTICLE L. 441-2-3-1 DU CCH PRÉVOYAIT QUE CE RECOURS NE POUVAIT ÊTRE EXERCÉ QU'À COMPTER DU 1ER DÉCEMBRE 2008.
</SCT>
<ANA ID="9A"> 38-07-01 La loi n° 2007-290 du 5 mars 2007 a créé, au profit des bénéficiaires d'une décision favorable de la commission de médiation à laquelle l'administration n'a pas donné suite dans les délais impartis, une voie de recours spécifique devant le juge administratif. Si le droit d'exercer ce recours s'acquiert, en principe, à l'expiration du délai de trois ou six mois, selon le cas, fixé à l'article R. 441-16-1 du code de la construction et de l'habitation (CCH), délai dans lequel l'administration doit faire une offre de logement, il résulte expressément des dispositions de l'article L. 441-2-3-1 du CCH que ce droit au recours ne pouvait être exercé qu'à compter du 1er décembre 2008. Les demandeurs à l'égard desquels le délai imparti au préfet pour attribuer un logement avait expiré avant le 1er décembre 2008 n'ont ainsi acquis le droit de former un recours devant la juridiction administrative qu'à compter de cette dernière date. Par suite, le décret n° 2008-1227 du 27 novembre 2008 a pu légalement fixer les conditions d'exercice de ce recours applicables à ces demandeurs sans porter atteinte à des droits acquis.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
