<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037103068</ID>
<ANCIEN_ID>JG_L_2018_06_000000360352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/10/30/CETATEXT000037103068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 25/06/2018, 360352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:360352.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 19 juillet 2016 ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 90/434/CE du Conseil du 23 juillet 1990 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 99-1172 du 30 décembre 1999 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018, Marc Jacob c/ Ministre des finances et des comptes publics (C-327/16) et Ministre des finances et des comptes publics c/ Marc B...(C-421/16) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 19 juillet 2016, le Conseil d'Etat, statuant au contentieux, après avoir, par une précédente décision du 7 décembre 2015, fait droit au pourvoi du ministre tendant à l'annulation de l'arrêt du 12 avril 2012 par lequel la cour administrative d'appel de Paris, sur appel de M. B..., avait annulé le jugement du 26 mai 2011 du tribunal administratif de Paris rejetant sa demande tendant à la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il avait été assujetti au titre de l'année 2002, a sursis à statuer sur la requête de M. B...jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions qui lui étaient posées à titre préjudiciel. Dans l'arrêt du 22 mars 2018 par lequel elle s'est prononcée sur ces questions, la Cour de justice de l'Union européenne a dit pour droit que :<br/>
              - l'article 8 de la directive 90/434/CE du 23 juillet 1990 concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents (" directive fusions ") doit être interprété en ce sens qu'il ne s'oppose pas à une législation d'un Etat membre en vertu de laquelle la plus-value issue d'une opération d'échange de titres relevant de cette directive est constatée à l'occasion de cette opération, mais son imposition est reportée jusqu'à l'année au cours de laquelle intervient l'événement mettant fin à ce report d'imposition, en l'occurrence la cession des titres reçus en échange ;<br/>
              - l'article 49 du Traité sur le fonctionnement de l'Union européenne s'oppose à une législation d'un Etat membre qui, dans une situation où la cession ultérieure de titres reçus en échange ne relève pas de la compétence fiscale de cet Etat membre, prévoit l'imposition de la plus-value placée en report d'imposition à l'occasion de cette cession sans tenir compte d'une éventuelle moins-value réalisée à cette occasion, alors qu'il est tenu compte d'une telle moins-value lorsque le contribuable détenteur de titres a sa résidence fiscale dans ledit Etat membre à la date de ladite cession.<br/>
<br/>
              2. Il résulte de ce qu'a ainsi jugé la Cour de justice de l'Union européenne que M. B...est seulement fondé à soutenir que l'administration ne pouvait imposer la plus-value constatée à l'occasion de l'apport de ses titres de la société française Gemplus Associates à la société de droit luxembourgeois Mars Sun, devenue Gemplus International, et placée en report d'imposition, sans imputer sur cette plus-value la moins-value réalisée lors de la cession ultérieure des titres de la société Gemplus International reçus en échange. <br/>
<br/>
              3. Dès lors qu'est en cause le calcul de la moins-value résultant de la cession de titres de la société Gemplus International acquis lors de l'opération d'échange réalisée en décembre 1999, il y a lieu, d'une part, de retenir comme prix d'acquisition de ces titres leur valeur à la date de l'opération d'échange, soit 2,7443 euros, ainsi qu'en conviennent les parties, sans qu'il y ait lieu, contrairement à ce que soutient M. B..., de tenir compte, pour le calcul de ce prix d'acquisition, d'opérations ultérieures de souscription d'actions de la société Gemplus International, sans lien avec l'opération d'échange mentionnée ci-dessus. D'autre part, il y a lieu, contrairement à ce que soutient M.B..., de calculer la moins-value en se fondant uniquement sur ceux des titres de la société Gemplus International vendus en décembre 2002 qu'il avait acquis lors de l'opération d'échange et non sur la totalité des titres de cette société vendus le même jour.<br/>
<br/>
              4. Il résulte de l'instruction que l'administration a considéré que les titres cédés par M. B...en décembre 2002, qui représentaient 45 % de la participation alors détenue par l'intéressé dans la société Gemplus International, incluaient une proportion équivalente de titres acquis en 1999 lors de l'opération d'échange et en a déduit, sans contestation de M. B...sur ce point, que la plus-value résultant de cet échange et placée en report d'imposition devait être imposée dans cette même proportion. Compte tenu de ce qui est jugé au point 3, la moins-value à imputer sur cette plus-value, égale au produit de la différence entre le prix de cession des titres en décembre 2002 et leur valeur à la date de l'opération d'apport par un nombre de titres correspondant à 45 % de ceux reçus en 1999, s'élève à 6 032 962 euros. M. B...est par suite fondé à demander la décharge de l'imposition supplémentaire mise à sa charge à hauteur de la différence entre cette imposition supplémentaire et celle résultant de l'imposition de 45 % de la plus-value placée en report d'imposition, après imputation sur ce montant de la moins-value mentionnée ci-dessus et soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Paris n'a pas fait droit dans cette mesure à sa demande. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, le versement à M. B...de la somme de 7 000 euros au titre des frais exposés par lui devant le Conseil d'Etat et la cour administrative d'appel.<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : M. B...est déchargé de la cotisation supplémentaire d'impôt sur le revenu mise à sa charge à hauteur de la différence entre cette cotisation supplémentaire et celle résultant de l'imposition de 45 % de la plus-value placée en report d'imposition, après imputation sur ce montant d'une moins-value de 6 032 962 euros.<br/>
<br/>
Article 2 : Le jugement du 26 mai 2011 du tribunal administratif de Paris est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 7 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
