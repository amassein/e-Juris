<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626726</ID>
<ANCIEN_ID>JG_L_2014_10_000000370647</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626726.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 24/10/2014, 370647, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370647</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:370647.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B... A...a demandé au tribunal administratif d'Orléans de prononcer la décharge de la cotisation supplémentaire d'impôt sur le revenu et des pénalités correspondantes auxquelles il a été assujetti au titre de l'année 2001. Par un jugement n° 0802554 du 11 octobre 2011, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT03181 du 28 mai 2013, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
Procédure devant le Conseil d'État<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 juillet 2013, 28 octobre 2013 et 3 juillet 2014 au secrétariat du contentieux du Conseil d'État, M. A... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NT03181 du 28 mai 2013 de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur pièces, l'administration a constaté qu'avait pris fin en 2001, en application du a du I de l'article 151 octies du code général des impôts, le report d'imposition de la plus-value réalisée par M. A... à l'occasion de l'apport en société de deux fonds de commerce effectué en 1996 sous le bénéfice de cet article. Après avoir réclamé en vain contre les cotisations supplémentaires d'impôt sur le revenu et les pénalités correspondantes auxquelles il a été assujetti en conséquence, M. A... a porté le litige devant le tribunal administratif d'Orléans qui, par un jugement du 11 octobre 2011, a rejeté sa demande en décharge. M. A... se pourvoit en cassation contre l'arrêt du 28 mai 2013 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 151 octies du code général des impôts, dans sa version applicable à l'imposition en litige : " I. Les plus-values soumises au régime des articles 39 duodecies à 39 quindecies et réalisées par une personne physique à l'occasion de l'apport à une société soumise à un régime réel d'imposition de l'ensemble des éléments de l'actif immobilisé affectés à l'exercice d'une activité professionnelle ou de l'apport d'une branche complète d'activité peuvent bénéficier des dispositions suivantes : / a. L'imposition des plus-values afférentes aux immobilisations non amortissables fait l'objet d'un report jusqu'à la date de la cession à titre onéreux ou du rachat des droits sociaux reçus en rémunération de l'apport de l'entreprise ou jusqu'à la cession de ces immobilisations par la société si elle est antérieure. (...) / II. Le régime défini au I s'applique : / a. Sur simple option exercée dans l'acte constatant la constitution de la société, lorsque l'apport de l'entreprise est effectué à une société en nom collectif, une société en commandite simple, une société à responsabilité limitée dans laquelle la gérance est majoritaire ou à une société civile exerçant une activité professionnelle ; / (...) / L'option est exercée dans l'acte d'apport conjointement par l'apporteur et la société ; elle entraîne l'obligation de respecter les règles prévues au présent article. / Si la société cesse de remplir les conditions permettant de bénéficier sur simple option du régime prévu au I, le report d'imposition des plus-values d'apport peut, sur agrément préalable, être maintenu. À défaut, ces plus-values deviennent immédiatement taxables. / L'apporteur doit joindre à la déclaration prévue à l'article 170 au titre de l'année en cours à la date de l'apport et des années suivantes un état conforme au modèle fourni par l'administration faisant apparaître les renseignements nécessaires au suivi des plus-values dont l'imposition est reportée conformément au premier alinéa du a du I. Un décret précise le contenu de cet état. "<br/>
<br/>
              3. Pour confirmer le rejet de la demande prononcé par le tribunal, la cour a jugé, par une appréciation souveraine non arguée de dénaturation, que les parts sociales acquises par M. A... en contrepartie des apports en société mentionnés au point 1 n'étaient pas dissociables de son activité de restaurateur. En en déduisant que ces parts devaient être inscrites à l'actif du bilan de son entreprise, sans que le contribuable puisse valablement opposer à l'administration, au demeurant sans aucune justification comptable, la décision de gestion qu'il aurait prise de les conserver dans son patrimoine privé, la cour n'a pas, contrairement à ce que soutient M. A..., jugé que l'article 151 octies du code général des impôts faisait par lui-même obstacle à une telle conservation et n'a donc pas commis l'erreur de droit alléguée.<br/>
<br/>
              4. Aux termes de l'article 1729 du code général des impôts dans sa rédaction alors applicable : " Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 p. 100 si la mauvaise foi de l'intéressé est établie ou de 80 p. 100 s'il s'est rendu coupable de manoeuvres frauduleuses [...] ". Aux termes de l'article L. 195 A du livre des procédures fiscales : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs, [...] la preuve de la mauvaise foi et des manoeuvres frauduleuses incombe à l'administration ". Contrairement à ce que soutient le requérant, la cour n'a pas méconnu ces dispositions en jugeant, par une motivation suffisante, que l'administration devait être regardée comme apportant la preuve qui lui incombait du caractère intentionnel des manquements relevés, en faisant valoir, d'une part, qu'il ne pouvait ignorer les conditions d'octroi et de déchéance du régime de report d'imposition des plus-values prévu par l'article 151 octies du code général des impôts, pour lequel il avait expressément opté, et d'autre part, qu'il avait produit un état de suivi des plus-values en report pendant les quatre années précédant l'année au titre de laquelle les impositions litigieuses ont été mises à sa charge.<br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
