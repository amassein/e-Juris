<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225098</ID>
<ANCIEN_ID>JG_L_2014_07_000000359394</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/50/CETATEXT000029225098.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 11/07/2014, 359394</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359394</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359394.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 14 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX01401 du 13 mars 2012 de la cour administrative d'appel de Bordeaux rejetant son appel contre le jugement n° 0801048, 0801697, 0801698, 0801699 du 8 avril 2010 par lequel le tribunal administratif de Pau a, d'une part, annulé l'article 5 de l'arrêté n° SA0800430 du préfet du Gers du 29 février 2008 ainsi que ses arrêtés du même jour ordonnant l'euthanasie des chiennes " Thémis " et " Moonshka " appartenant respectivement à M. A...et à Mme D...et, d'autre part, condamné l'Etat à verser une indemnité d'un montant de 300 euros à M. A...en réparation du préjudice résultant de la perte de son animal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu l'arrêté du 21 avril 1997 complétant les dispositions de l'article 1er du décret n° 96-596 du 27 juin 1996 relatif à la lutte contre la rage ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de M. A...et de Mlle D...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 223-6 du code rural, dans sa rédaction en vigueur à la date des décisions contestées devant les juges du fond, autorise le préfet à prendre, en cas de simple suspicion de maladie réputée contagieuse, un arrêté de mise sous surveillance des animaux se trouvant sur le territoire du département et à mettre en oeuvre les mesures énumérées aux 1° à 7° de l'article L. 223-8 du même code ; qu'aux termes de cet article, dans sa rédaction en vigueur à la même date : " Après la constatation de la maladie, le préfet statue sur les mesures à mettre en exécution dans le cas particulier. / Il prend, s'il est nécessaire, un arrêté portant déclaration d'infection remplaçant éventuellement un arrêté de mise sous surveillance. / Cette déclaration peut entraîner, dans le périmètre qu'elle détermine, l'application des mesures suivantes : / (...) 8° L'abattage des animaux malades ou contaminés ou des animaux ayant été exposés à la contagion, ainsi que des animaux suspects d'être infectés ou en lien avec des animaux infectés dans les conditions prévues par l'article L. 223-6 " ; qu'aux termes du deuxième alinéa de l'article L. 223-9 du même code, dans sa rédaction alors en vigueur : " Les animaux suspects de rage et ceux qu'ils auraient pu contaminer, hormis le cas où ils se trouvent déjà soumis à des mesures de police sanitaire par l'effet d'un arrêté portant déclaration d'infection pris par application de l'article L. 223-8, sont placés, par arrêté du préfet, sous la surveillance des services vétérinaires. Cet arrêté peut entraîner l'application des mesures énumérées aux 1°, 5°, 7° et 8° de l'article L. 223-8 " ; que les quatrième et cinquième alinéas du même article précisent que : " Les carnivores ayant été en contact avec un animal reconnu enragé sont abattus. (...) / L'abattage des animaux suspects et de ceux qu'ils auraient pu contaminer de rage peut être ordonné, dans tous les cas, si ces animaux se montrent dangereux ou si le respect des mesures de police sanitaire qui leur sont applicables ne peut être ou n'est pas assuré (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de l'ensemble de ces dispositions que le préfet peut, dans les conditions fixées au deuxième alinéa de l'article L. 223-9, ordonner l'abattage des animaux suspects de rage ou atteints par cette maladie sans avoir pris préalablement un arrêté de déclaration d'infection en application de l'article L. 223-8 dès lors que cette mesure est proportionnée au risque que les animaux en cause présentent pour la santé publique ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 février 2008, le préfet du Gers a placé sous surveillance les animaux susceptibles d'être infectés par la rage se trouvant sur le territoire de la commune de Montestruc-sur-Gers ; que, par l'article 5 de cet arrêté, il a ordonné l'euthanasie des chiens non identifiés ou non valablement vaccinés contre la rage ayant été en contact entre le 20 octobre et le 12 novembre 2007 avec un chien nommé " Gamin ", suspect de rage ; que, par deux décisions du même jour, il a ordonné l'euthanasie des chiennes nommées " Thémis " et " Moonshka ", appartenant respectivement à M. A...et à MmeD... ; qu'à la demande des intéressés, après l'exécution des mesures ainsi ordonnées, le tribunal administratif de Pau a, par un jugement du 8 avril 2010, annulé l'article 5 du premier arrêté ainsi que les décisions ordonnant l'euthanasie des deux chiennes et condamné l'Etat à verser à M. A...la somme de 300 euros ; que par l'arrêt attaqué du 13 mars 2012, la cour administrative d'appel de Bordeaux a rejeté le recours du ministre chargé de l'agriculture contre ce jugement  au motif que l'édiction d'un arrêté de mise sous surveillance en cas de simple suspicion de maladie réputée contagieuse ne permet pas au préfet de prescrire légalement, dans les conditions prévues au 8° de l'article L. 223-8 du code rural, l'abattage des animaux suspects d'être infectés ou en lien avec des animaux infectés ; <br/>
<br/>
              4. Considérant qu'en statuant ainsi, alors que, comme il a été dit au point 2, les dispositions du deuxième alinéa de l'article L. 223-9 de ce code, dont il a été fait application par les décisions attaquées, autorisent le préfet à ordonner l'abattage des animaux suspects de rage ou atteints par cette maladie dès lors qu'il a pris un arrêté plaçant ces animaux sous la surveillance des services vétérinaires, la cour a commis une erreur de droit ; que, par suite, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article R. 223-25 du code rural, dans sa rédaction applicable à la date des décisions attaquées : " Est considéré comme : / 1° Animal reconnu enragé tout animal pour lequel un diagnostic de rage a été établi par un organisme ou un laboratoire agréé par le ministre chargé de l'agriculture ou le ministre chargé de la santé ; / 2° Animal suspect de rage : / a) Tout animal sensible à la rage qui présente des symptômes évoquant la rage et non susceptibles d'être rattachés de façon certaine à une autre maladie ;/ b) Ou tout animal sensible à la rage qui, en quelque lieu que ce soit, a mordu ou griffé une personne ou un animal, sans raison apparente et contrairement à son comportement habituel. (...) / 3° Animal contaminé de rage : / a) Tout animal sensible à la rage qui, au cours d'une période définie par un arrêté du ministre chargé de l'agriculture, a été mordu ou griffé par un animal reconnu enragé ; / b) Ou tout carnivore qui, au cours d'une période dont la durée est définie par un arrêté du ministre chargé de l'agriculture, a été en contact avec un animal reconnu enragé ou pour lequel une enquête des services vétérinaires n'a pu écarter formellement l'hypothèse d'un tel contact. / 4° Animal éventuellement contaminé de rage : (...) b) Ou tout carnivore qui, au cours d'une période dont la durée est définie par un arrêté du ministre chargé de l'agriculture, a été en contact avec un animal suspect de rage ou pour lequel une enquête des services vétérinaires n'a pu écarter formellement l'hypothèse d'un tel contact (...) " ;<br/>
<br/>
              7. Considérant qu'il ressort des termes mêmes de l'article 5 de l'arrêté attaqué du 29 février 2008 que le préfet du Gers a estimé que le chien " Gamin " était suspect de rage au sens des dispositions du 2° de l'article R. 223-25 du code rural et qu'il a ordonné l'euthanasie des animaux éventuellement contaminés au sens des dispositions du 4° du même article ; que, par suite, contrairement à ce qu'a jugé le tribunal administratif de Pau, le préfet pouvait légalement prendre les mesures prévues au deuxième alinéa de l'article L. 223-9 du code rural et, si nécessaire, ordonner l'euthanasie des chiens non identifiés ou non valablement vaccinés en contact avec le chien " Gamin " durant sa période d'excrétion virale, alors même qu'ils n'avaient pas été en contact avec un animal reconnu enragé au sens du 1° de l'article R. 223-25 ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Pau s'est fondé sur les dispositions des quatrième et cinquième alinéas de l'article L. 223-9 du code rural pour annuler l'article 5 de l'arrêté du 29 février 2008 ainsi que les décisions du préfet du Gers ordonnant l'euthanasie des chiennes appartenant à M. A...et à MmeD... ;<br/>
<br/>
              9. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A...et Mme D...devant le tribunal administratif de Pau ;<br/>
<br/>
              Sur l'article 5 de l'arrêté du 29 février 2008 :<br/>
<br/>
              10. Considérant que, pour ordonner l'euthanasie des chiens éventuellement contaminés et non identifiés ou non valablement vaccinés contre la rage ayant été en contact sur le territoire de la commune de Montestruc-sur-Gers avec le chien " Gamin " pendant la période d'excrétion virale de ce dernier, le préfet du Gers s'est fondé sur la mise en évidence, dans le département de Seine-et-Marne, d'un cas de rage concernant un chien contaminé par le chien " Gamin " ; que, toutefois, à la date de l'arrêté attaqué, aucun cas d'animal enragé ou suspecté de l'être dans le département du Gers n'avait été signalé au préfet ; qu'il ressort des pièces du dossier que des mesures autres que l'abattage de ces animaux, notamment la mise sous surveillance de ceux-ci, étaient de nature à permettre de prévenir efficacement les risques de contamination des animaux menacés et de protéger la santé publique ; que, par suite, les motifs retenus par le préfet du Gers ne justifiaient pas, dans les circonstances de l'espèce, l'euthanasie des animaux mentionnés à l'article 5 de l'arrêté attaqué ;<br/>
<br/>
              Sur les décisions ordonnant l'euthanasie des chiennes " Thémis " et " Moonshka " :<br/>
<br/>
              11. Considérant que, si le préfet du Gers a constaté qu'elles avaient été en contact avec le chien " Gamin " pendant sa période d'excrétion virale et n'étaient pas vaccinées contre la rage, il résulte de l'examen vétérinaire pratiqué le 28 février 2008 que la chienne " Thémis " ne présentait aucun symptôme de rage ni aucun signe de dangerosité et qu'elle n'avait été ni griffée ni mordue par le chien " Gamin " ; qu'il n'est pas contesté que la chienne " Moonshka " a fait l'objet des mêmes constatations ; que la seule circonstance que ces animaux, au demeurant qualifiés à tort de contaminés de rage au sens du 3° de l'article R. 223-25 du code rural dans les arrêtés attaqués alors qu'ils n'étaient qu'éventuellement contaminés au sens du b) du 4° du même article, aient été en contact avec un chien suspect de rage pendant sa période d'excrétion virale n'est pas de nature à faire regarder comme proportionnées les décisions de les faire abattre dès lors qu'il était possible de les maintenir sous surveillance sans risque avéré pour la santé publique ; que, par suite, le préfet a fait, dans les circonstances de l'espèce, une inexacte application des dispositions du deuxième alinéa de l'article L. 223-9 du code rural en ordonnant l'euthanasie de ces chiennes ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que le ministre de l'agriculture n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Pau a, d'une part, annulé l'article 5 de l'arrêté du 29 février 2008 du préfet du Gers ainsi que les décisions du même jour ordonnant l'euthanasie des chiennes appartenant à M. A... et à Mme D... et, d'autre part, condamné l'Etat à verser à M. A...une indemnité de 300 euros au titre de son préjudice résultant de la perte de son animal ;<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser, respectivement, à M. A...et à Mme D...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 13 mars 2012 est annulé. <br/>
<br/>
Article 2 : Le recours présenté par le ministre de l'alimentation, de l'agriculture et de la pêche devant la cour administrative d'appel de Bordeaux est rejeté.<br/>
<br/>
Article 3 : L'Etat versera à M. A...et à Mme D...chacun une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'agriculture, de l'agroalimentaire et de la forêt, à M. B...A...et à Mme C...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-02 POLICE. POLICES SPÉCIALES. POLICE SANITAIRE (VOIR AUSSI : SANTÉ PUBLIQUE). - LUTTE CONTRE LES MALADIES DES ANIMAUX - LUTTE CONTRE LA RAGE (ART. L. 223-9 DU CODE RURAL) - POSSIBILITÉ POUR LE PRÉFET, EN L'ABSENCE DE DÉCLARATION D'INFECTION PRÉALABLE, D'ORDONNER L'ABATTAGE D'UN ANIMAL SUSPECTÉ DE RAGE OU ATTEINT PAR CETTE MALADIE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL DE PROPORTIONNALITÉ - ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CHOIX D'ABATTRE UN ANIMAL SUSPECTÉ DE RAGE OU ATTEINT PAR CETTE MALADIE (ART. L. 223-9 DU CODE RURAL) - ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01-01 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. - LUTTE CONTRE LES MALADIES DES ANIMAUX - LUTTE CONTRE LA RAGE (ART. L. 223-9 DU CODE RURAL) - POSSIBILITÉ POUR LE PRÉFET, EN L'ABSENCE DE DÉCLARATION D'INFECTION PRÉALABLE, D'ORDONNER L'ABATTAGE D'UN ANIMAL SUSPECTÉ DE RAGE OU ATTEINT PAR CETTE MALADIE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL DE PROPORTIONNALITÉ - ESPÈCE.
</SCT>
<ANA ID="9A"> 49-05-02 Le préfet peut, dans les conditions fixées au deuxième alinéa de l'article L. 223-9 du code rural, ordonner l'abattage des animaux suspects de rage ou atteints par cette maladie sans avoir pris préalablement un arrêté de déclaration d'infection en application de l'article L. 223-8, dès lors que cette mesure est proportionnée au risque que les animaux en cause présentent pour la santé publique.,,,En l'espèce, chiens non vaccinés contre la rage ayant été en contact avec un chien suspect de rage pendant sa période d'excrétion virale, mais ne présentant aucun symptôme de rage ni aucun signe de dangerosité, et n'ayant été ni griffés ni mordus par cet autre chien. La décision de les faire abattre n'était pas proportionnée, dès lors qu'il était possible de les maintenir sous surveillance sans risque avéré pour la santé publique. Par suite, le préfet a fait, dans les circonstances de l'espèce, une inexacte application des dispositions du deuxième alinéa de l'article L. 223-9 du code rural.</ANA>
<ANA ID="9B"> 54-07-02-03 Le préfet peut, dans les conditions fixées au deuxième alinéa de l'article L. 223-9 du code rural, ordonner l'abattage des animaux suspects de rage ou atteints par cette maladie sans avoir pris préalablement un arrêté de déclaration d'infection en application de l'article L. 223-8, dès lors que cette mesure est proportionnée au risque que les animaux en cause présentent pour la santé publique.,,,En l'espèce, chiens non vaccinés contre la rage ayant été en contact avec un chien suspect de rage pendant sa période d'excrétion virale, mais ne présentant aucun symptôme de rage ni aucun signe de dangerosité, et n'ayant été ni griffés ni mordus par cet autre chien. La décision de les faire abattre n'était pas proportionnée, dès lors qu'il était possible de les maintenir sous surveillance sans risque avéré pour la santé publique. Par suite, le préfet a fait, dans les circonstances de l'espèce, une inexacte application des dispositions du deuxième alinéa de l'article L. 223-9 du code rural.</ANA>
<ANA ID="9C"> 61-01-01 Le préfet peut, dans les conditions fixées au deuxième alinéa de l'article L. 223-9 du code rural, ordonner l'abattage des animaux suspects de rage ou atteints par cette maladie sans avoir pris préalablement un arrêté de déclaration d'infection en application de l'article L. 223-8, dès lors que cette mesure est proportionnée au risque que les animaux en cause présentent pour la santé publique.,,,En l'espèce, chiens non vaccinés contre la rage ayant été en contact avec un chien suspect de rage pendant sa période d'excrétion virale, mais ne présentant aucun symptôme de rage ni aucun signe de dangerosité, et n'ayant été ni griffés ni mordus par cet autre chien. La décision de les faire abattre n'était pas proportionnée, dès lors qu'il était possible de les maintenir sous surveillance sans risque avéré pour la santé publique. Par suite, le préfet a fait, dans les circonstances de l'espèce, une inexacte application des dispositions du deuxième alinéa de l'article L. 223-9 du code rural.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
