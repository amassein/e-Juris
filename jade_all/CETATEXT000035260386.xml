<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035260386</ID>
<ANCIEN_ID>JG_L_2017_07_000000409440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/26/03/CETATEXT000035260386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 21/07/2017, 409440, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409440.20170721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 409440, par un mémoire, enregistré le 12 juin 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'Union dentaire demande au Conseil d'État, à l'appui de sa requête tendant à l'annulation de l'arrêté du 29 mars 2017 du ministre de l'économie et des finances et du ministre des affaires sociales et de la santé portant approbation du règlement arbitral organisant les rapports entre les chirurgiens-dentistes libéraux et l'assurance maladie, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article 75 de la loi n° 2016-1827 du 23 décembre 2016 de financement de la sécurité sociale pour 2017. <br/>
<br/>
<br/>
              2° Sous le n° 410797, par un mémoire, enregistré le 8 juin 2017 au secrétariat du contentieux du Conseil d'Etat et rectifié le 12 juin suivant, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la Fédération des syndicats dentaires libéraux demande au Conseil d'État, à l'appui de sa requête tendant à l'annulation du même arrêté du 29 mars 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article 75 de la loi n° 2016-1827 du 23 décembre 2016 de financement de la sécurité sociale pour 2017. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 410856, par un mémoire, enregistré le 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la Confédération nationale des syndicats dentaires demande au Conseil d'État, à l'appui de sa requête tendant à l'annulation du même arrêté du 29 mars 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article 75 de la loi n° 2016-1827 du 23 décembre 2016 de financement de la sécurité sociale pour 2017. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              4° Sous le n° 410995, par un mémoire, enregistré le 30 mai 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, le Syndicat des femmes chirurgiens-dentistes demande au Conseil d'État, à l'appui de sa requête tendant à l'annulation du même arrêté du 29 mars 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article 75 de la loi n° 2016-1827 du 23 décembre 2016 de financement de la sécurité sociale pour 2017. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2016-1827 du 23 décembre 2016, notamment le I de son article 75 ;<br/>
              - le code de justice administrative, notamment son article R. 771-15 ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 juillet 2016, présentée pour la Confédération nationale des syndicats dentaires, sous le n° 410856.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les organisations requérantes soulèvent la même question prioritaire de constitutionnalité à l'appui de leurs recours pour excès de pouvoir dirigés contre l'arrêté du 29 mars 2017 portant approbation du règlement arbitral organisant les rapports entre les chirurgiens-dentistes libéraux et l'assurance maladie. Il y a lieu de joindre ces instances et de statuer par une seule décision sur la question soulevée.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. Aux termes de l'article L. 162-9 du code de la sécurité sociale : " Les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, les sages-femmes et les auxiliaires médicaux sont définis par des conventions nationales conclues entre l'Union nationale des caisses d'assurance maladie et une ou plusieurs des organisations syndicales nationales les plus représentatives de chacune de ces professions. (...) ". Aux termes du I de l'article 75 de la loi du 23 décembre 2016 de financement de la sécurité sociale pour 2017 : " A défaut de signature avant le 1er février 2017 d'un avenant à la convention nationale des chirurgiens-dentistes en vigueur mentionnée à l'article L. 162-9 du code de la sécurité sociale, un arbitre arrête un projet de convention dans le respect du cadre financier pluriannuel des dépenses d'assurance maladie. / Ce projet de convention reconduit la convention nationale des chirurgiens-dentistes en vigueur, en modifiant ses articles 4.2.1 et 4.3.3 et ses annexes I et V, pour déterminer les tarifs mentionnés au 1° du I de l'article L. 162-14-1 du code de la sécurité sociale et la limite applicable aux dépassements autorisés sur tout ou partie de ces tarifs. Les dispositions de la convention antérieure continuent de produire leurs effets jusqu'à la date d'entrée en vigueur du règlement arbitral qui la remplace. / L'arbitre est désigné avant le 1er février 2017 par l'Union nationale des caisses d'assurance maladie et au moins une organisation syndicale représentative des chirurgiens-dentistes. A défaut, l'arbitre est désigné par le président du Haut Conseil pour l'avenir de l'assurance maladie, sur proposition du directeur général de l'Union nationale des caisses d'assurance maladie, dans un délai de huit jours. Le nom de l'arbitre est notifié aux partenaires conventionnels ainsi qu'aux ministres chargés de la santé et de la sécurité sociale. / L'arbitre dispose d'un délai d'un mois à compter de sa désignation pour transmettre un projet de règlement arbitral aux ministres chargés de la santé et de la sécurité sociale. Il auditionne les représentants de l'Union nationale des caisses d'assurance maladie, des organisations syndicales représentatives des professionnels de santé et de l'Union nationale des organismes d'assurance maladie complémentaire. / Le I de l'article L. 162-14-2 du code de la sécurité sociale s'applique aux conditions de transmission, d'approbation et de mise en oeuvre du règlement arbitral. / La procédure d'approbation de l'avenant mentionné au premier alinéa du présent article est mise en oeuvre sans appliquer le délai prévu à l'avant-dernier alinéa de l'article L. 162-14-3 du même code ".<br/>
<br/>
              4. En premier lieu, s'il est soutenu que les dispositions contestées n'avaient pas leur place dans une loi de financement de la sécurité sociale, le grief tiré de la méconnaissance de la procédure d'adoption d'une loi ne peut être invoqué à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution.<br/>
<br/>
              5. En deuxième lieu, aux termes de l'article 34 de la Constitution : " (...) La loi détermine les principes fondamentaux : (...) de la sécurité sociale (...) ". Il est soutenu que le législateur, faute de préciser les pouvoirs de l'arbitre et le déroulement de la procédure arbitrale, aurait méconnu l'étendue de sa compétence dans des conditions portant atteinte au droit à la protection de la santé et au principe d'égalité. Toutefois, si figure, au nombre des principes fondamentaux relevant de la compétence du législateur, celui d'après lequel le tarif des honoraires pour les soins délivrés aux assurés sociaux est fixé par voie de convention passée avec les professionnels de santé concernés ou leurs organisations représentatives ou, à défaut, par voie d'autorité, en revanche, ressortit à la compétence du pouvoir réglementaire la détermination des modalités de mise en oeuvre des principes fondamentaux posés par le législateur. Les dispositions du I de l'article 75 de la loi de financement de la sécurité sociale pour 2017 ont défini avec une précision suffisante les conditions dans lesquelles un arbitre pouvait, le cas échéant, être désigné pour arrêter un projet de règlement arbitral modifiant les stipulations de la convention nationale des chirurgiens-dentistes en vigueur relatives aux tarifs et à la limite applicable aux dépassements autorisés sur ces tarifs. Dans ces conditions, le moyen tiré de l'incompétence négative doit être écarté. <br/>
<br/>
              6. En troisième lieu, il est loisible au législateur d'apporter à la liberté contractuelle et à la liberté d'entreprendre, qui découlent de l'article 4 de la Déclaration de 1789, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi. Par ailleurs, le législateur ne saurait porter aux contrats légalement conclus une atteinte qui ne soit justifiée par un motif d'intérêt général suffisant sans méconnaître les exigences résultant des articles 4 et 16 de la Déclaration de 1789.<br/>
<br/>
              7. Les dispositions du I de l'article 75 de la loi de financement de la sécurité sociale pour 2017 sont critiquées, d'une part, en ce qu'elles prévoient la substitution d'un processus arbitral à la négociation conventionnelle, en cas d'échec de cette dernière dans un délai très court. Toutefois, ces dispositions, qui tirent les conséquences de la difficulté, dans le cadre de négociations engagées en septembre 2016, à parvenir à une évolution conventionnelle des tarifs et à une modération des dépassements autorisés permettant une meilleure couverture de certains soins dentaires par l'assurance maladie, alors qu'un nombre notable d'assurés sociaux sont conduits à renoncer à ces soins pour des raisons financières, n'ont pas porté à la liberté contractuelle une atteinte disproportionnée au regard des objectifs de protection de la santé publique et d'équilibre financier de la sécurité sociale qu'elles poursuivent. <br/>
<br/>
              8. Ces dispositions sont critiquées, d'autre part, en ce qu'elles permettent la modification, par le règlement arbitral qu'elles prévoient, des tarifs fixés par la convention conclue le 11 mai 2006 entre les organisations représentatives des chirurgiens-dentistes et l'assurance maladie, sur le fondement de l'article L. 162-9 précité du code de la sécurité sociale, et tacitement reconduite depuis lors. Toutefois, eu égard tant à l'objectif poursuivi qu'à la nature particulière de cette convention, dont l'approbation par les ministres chargés de la santé et de la sécurité sociale en vertu de l'article L. 162-15 du même code, nécessaire à son entrée en vigueur, a pour effet de conférer un caractère réglementaire à ses stipulations, le législateur pouvait permettre, par le I de l'article 75 précité, qu'il soit unilatéralement procédé à sa modification sans porter atteinte au principe de liberté contractuelle, alors même qu'il prévoyait, à défaut d'accord entre l'Union nationale des caisses d'assurance maladie et au moins une organisation syndicale représentative des chirurgiens-dentistes, que l'arbitre puisse être désigné par une décision unilatérale du président du Haut Conseil pour l'avenir de l'assurance maladie, sur proposition du directeur général de l'union nationale.<br/>
<br/>
              9. Enfin, les règles de fixation des tarifs des honoraires des chirurgiens-dentistes dus par les assurés sociaux et des dépassements autorisés sur ces tarifs qui résultent du I de l'article 75 de la loi du 23 décembre 2016 ne portent pas, au regard des objectifs mentionnés au point 7, une atteinte disproportionnée à la liberté d'entreprendre. <br/>
<br/>
              10. En quatrième lieu, aux termes de l'article 16 de la Déclaration de 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ". Il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou d'abroger ceux-ci en leur substituant, le cas échéant, d'autres dispositions. Ce faisant, il ne saurait toutefois priver de garanties légales des exigences constitutionnelles. En particulier, il ne saurait, sans motif d'intérêt général suffisant, ni porter atteinte aux situations légalement acquises ni remettre en cause les effets qui peuvent légitimement être attendus de telles situations. Il est soutenu que le I de l'article 75 de la loi de financement de la sécurité sociale pour 2017, en permettant une modification des tarifs qui y figurent, a porté à l'attente légitime découlant de la reconduction de la convention conclue entre les représentants des chirurgiens dentistes et l'Union nationale des caisses d'assurance maladie, intervenue tacitement le 19 juin 2016 pour une durée de cinq ans en application des articles L. 162-14-1 et L. 162-15-2 du code de la sécurité sociale, une atteinte qui n'est justifiée par aucun objectif d'intérêt général. Toutefois, il résulte de ce qui a été dit au point 8 que la convention, qui avait au demeurant déjà fait l'objet de plusieurs avenants, signés les 6 avril 2007, 16 avril 2012 et 31 juillet 2013, et dont la modification était recherchée par des négociations ouvertes en septembre 2016, n'a pas fait naître de situations légalement acquises auxquelles seraient susceptibles de porter atteinte les dispositions contestées, qui sont applicables à défaut de signature d'un nouvel avenant avant le 1er février 2017 et sont dépourvues de portée rétroactive.<br/>
<br/>
              11. En cinquième lieu, les exigences d'impartialité découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen ne s'appliquent pas à une autorité telle que " l'arbitre " qui, aux termes du I de l'article 75 de la loi du 23 décembre 2016, pouvait être chargé d'arrêter un projet de nouvelle convention nationale des chirurgiens-dentistes à défaut de signature avant le 1er février 2017 d'un avenant à la convention en vigueur. Par suite, les dispositions législatives relatives à sa désignation ne peuvent être utilement critiquées au motif qu'elles ne comporteraient pas de garanties suffisantes pour assurer le respect du principe d'impartialité découlant de l'article 16.<br/>
<br/>
              12. En sixième lieu, le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Le syndicat requérant soutient que les dispositions en litige sont contraires au principe d'égalité devant la loi garanti par les articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen et par l'article 1er de la Constitution, au motif qu'elles permettent une modification non conventionnelle des tarifs concernant les actes des chirurgiens-dentistes, et non de ceux des actes pratiqués par les médecins stomatologues, et instaurent ainsi entre ces deux spécialités, dont les actes se recouvrent pour partie, un traitement différent. Toutefois, la soumission de la profession de chirurgien-dentiste et celle de médecin stomatologue à des régimes différents résulte non des dispositions critiquées mais de celles des articles L. 162-5 et L. 162-9 du code de la sécurité sociale, prévoyant des régimes conventionnels distincts pour les médecins et pour les chirurgiens-dentistes, lesquels, au demeurant, eu égard à leurs missions, leurs qualifications et leurs responsabilités, ne peuvent être regardés comme se trouvant dans la même situation. <br/>
<br/>
              13. En dernier lieu, s'il est également soutenu que les dispositions du I de l'article 75 de la loi de financement de la sécurité sociale pour 2017 méconnaîtraient des principes de négociation conventionnelle, de non-commercialité des professions médicales, de solidarité nationale et d'égal accès aux soins, ces griefs ne sont, en tout état de cause, pas assortis de précisions suffisantes pour en apprécier le bien-fondé.<br/>
<br/>
              14. Il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le I de l'article 75 de la loi du 23 décembre 2016 de financement de la sécurité sociale pour 2017 porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'Union dentaire, la Fédération des syndicats dentaires libéraux, la Confédération nationale des syndicats dentaires et le Syndicat des femmes chirurgiens-dentistes.<br/>
Article 2 : La présente décision sera notifiée à l'Union dentaire, à la Fédération des syndicats dentaires libéraux, à la Confédération nationale des syndicats dentaires et au Syndicat des femmes chirurgiens-dentistes.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la ministre des solidarités et de la santé et à l'Union nationale des caisses d'assurance maladie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
