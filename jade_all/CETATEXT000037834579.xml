<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834579</ID>
<ANCIEN_ID>JG_L_2018_12_000000408504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834579.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 19/12/2018, 408504</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408504.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris, d'une part, de condamner l'Etat à lui verser la somme de 230 238,80 euros en réparation du préjudice financier qu'il estime avoir subi en raison des sommes acquittées au titre de l'assurance de son activité entre 1998 et 2012 à raison des missions qui lui ont été confiées par l'Etat et, d'autre part, d'annuler la note de service du 8 janvier 2014 relative à l'obligation d'assurance des maîtres d'oeuvre fonctionnaires de l'Etat rétribués par honoraires. Par un jugement n° 1318594 du 1er juillet 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03330 du 22 février 2017, la cour administrative d'appel de Paris a, sur appel de M.B..., annulé ce jugement et rejeté sa demande de première instance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 février et 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette sa demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du patrimoine ;<br/>
              - la loi du 31 décembre 1913 sur les monuments historiques ;<br/>
              - la loi n° 77-2 du 3 janvier 1977 ;<br/>
              - le décret n° 80-911 du 20 novembre 1980 ;<br/>
              - le décret n° 2007-1405 du 28 septembre 2007 ;<br/>
              - le décret n° 2009-749 du 22 juin 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...et à la SCP Lyon-Caen, Thiriez, avocat du ministre de la culture ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par un jugement du 1er juillet 2014, le tribunal administratif de Paris a rejeté la demande de M. B..., architecte en chef des monuments historiques, tendant, d'une part, à la condamnation de l'Etat à lui verser la somme de 230 238,80 euros en réparation du préjudice financier qu'il estime avoir subi en raison des cotisations d'assurance professionnelle qu'il a acquittées entre 1998 et 2012 à raison des missions de maîtrise d'oeuvre de travaux de restauration sur des monuments historiques classés qui lui ont été confiées au cours de cette période et, d'autre part, à l'annulation pour excès de pouvoir de la note de service du 8 janvier 2014 relative à l'obligation d'assurance des maîtres d'oeuvre fonctionnaires de l'Etat rétribués par honoraires ; qu'il se pourvoit en cassation contre l'arrêt du 22 février 2017 par lequel la cour administrative d'appel de Paris a annulé ce jugement et rejeté sa demande de première instance ; que, par les moyens qu'il invoque, M. B...doit être regardé comme ne demandant l'annulation de cet arrêt qu'en tant qu'il rejette ses conclusions indemnitaires présentées devant le tribunal administratif ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 16 de la loi du 3 janvier 1977 sur l'architecture : " Tout architecte, personne physique ou morale, dont la responsabilité peut être engagée en raison des actes qu'il accomplit à titre professionnel ou des actes de ses préposés, doit être couvert par une assurance (...) Lorsque l'architecte intervient en qualité d'agent public, (...) la personne qui l'emploie (...) est seule civilement responsable des actes professionnels accomplis pour son compte et souscrit l'assurance garantissant les conséquences de ceux-ci " ;<br/>
<br/>
              3. Considérant qu'en vertu des dispositions statutaires qui régissent le corps des architectes en chef des monuments historiques, résultant successivement du décret du 20 novembre 1980 et du décret du 28 septembre 2007 portant statut particulier de ce corps, ceux-ci sont des architectes, fonctionnaires de l'Etat, ayant notamment pour mission d'assurer la maîtrise d'oeuvre des travaux de restauration sur les immeubles classés au titre des monuments historiques dont ils assurent la surveillance ; que les architectes en chef des monuments historiques, qui interviennent alors dans le cadre d'un contrat de maîtrise d'oeuvre et dont la responsabilité peut être engagée en raison des actes qu'ils accomplissent à titre professionnel ou des actes de leurs préposés, n'interviennent pas, nonobstant leur qualité de fonctionnaires, en qualité d'agents publics au sens des dispositions citées ci-dessus de la loi du 3 janvier 1977 sur l'architecture ; que ces dispositions ne créent, par suite, aucune obligation pour l'Etat de souscrire l'assurance garantissant les architectes en chef des conséquences des actes professionnels en question ;<br/>
<br/>
              4. Considérant que, pour juger que l'Etat n'avait pas commis de faute de nature à engager sa responsabilité, la cour administrative d'appel s'est notamment fondée sur le fait que M. B...ne pouvait utilement invoquer les dispositions citées ci-dessus de l'article 16 de la loi du 3 janvier 1977, au motif qu'il n'était pas intervenu en qualité d'agent public, au sens de ces dispositions, pour l'exécution des travaux litigieux ; que, contrairement à ce que soutient le requérant, elle a, ce faisant, exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, au surplus, qu'en relevant, à titre surabondant, que la réalité du préjudice dont M. B...demande réparation n'était pas établie, la cour administrative d'appel n'a, ainsi que le soutient en défense la ministre de la culture, pas entaché de dénaturation son appréciation souveraine des pièces du dossier ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros à verser à l'Etat au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : M. B...versera à l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-01-02 FONCTIONNAIRES ET AGENTS PUBLICS. QUALITÉ DE FONCTIONNAIRE OU D'AGENT PUBLIC. QUALITÉ DE FONCTIONNAIRE. - ARCHITECTES EN CHEF DES MONUMENTS HISTORIQUES - MISSION DE MAÎTRISE D'OEUVRE DES TRAVAUX DE RESTAURATION SUR LES IMMEUBLES CLASSÉS - MISSION EXERCÉE EN QUALITÉ D'AGENTS PUBLICS AU SENS DE L'ARTICLE 16 DE LA LOI DU 3 JANVIER 1977- ABSENCE, NONOBSTANT LEUR QUALITÉ DE FONCTIONNAIRES - CONSÉQUENCE - ABSENCE D'OBLIGATION POUR L'ETAT DE SOUSCRIRE L'ASSURANCE LES GARANTISSANT DES CONSÉQUENCES DES ACTES EFFECTUÉS DANS LE CADRE DE CETTE MISSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">41-01-02 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. TRAVAUX SUR LES MONUMENTS HISTORIQUES. - ARCHITECTES EN CHEF DES MONUMENTS HISTORIQUES - MISSION DE MAÎTRISE D'OEUVRE DES TRAVAUX DE RESTAURATION SUR LES IMMEUBLES CLASSÉS - MISSION EXERCÉE EN QUALITÉ D'AGENTS PUBLICS AU SENS DE L'ARTICLE 16 DE LA LOI DU 3 JANVIER 1977- ABSENCE, NONOBSTANT LEUR QUALITÉ DE FONCTIONNAIRES - CONSÉQUENCE - ABSENCE D'OBLIGATION POUR L'ETAT DE SOUSCRIRE L'ASSURANCE LES GARANTISSANT DES CONSÉQUENCES DES ACTES EFFECTUÉS DANS LE CADRE DE CETTE MISSION [RJ1].
</SCT>
<ANA ID="9A"> 36-01-02 En vertu des dispositions statutaires qui régissent le corps des architectes en chef des monuments historiques, résultant successivement du décret n° 80-911 du 20 novembre 1980 et du décret n° 2007-1405 du 28 septembre 2007 portant statut particulier de ce corps, ceux-ci sont des architectes, fonctionnaires de l'Etat, ayant notamment pour mission d'assurer la maîtrise d'oeuvre des travaux de restauration sur les immeubles classés au titre des monuments historiques dont ils assurent la surveillance. Les architectes en chef des monuments historiques, qui interviennent alors dans le cadre d'un contrat de maîtrise d'oeuvre et dont la responsabilité peut être engagée en raison des actes qu'ils accomplissent à titre professionnel ou des actes de leurs préposés, n'interviennent pas, nonobstant leur qualité de fonctionnaires, en qualité d'agents publics au sens de l'article 16 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture. Cet article ne crée, par suite, aucune obligation pour l'Etat de souscrire l'assurance garantissant les architectes en chef des conséquences des actes professionnels en question.</ANA>
<ANA ID="9B"> 41-01-02 En vertu des dispositions statutaires qui régissent le corps des architectes en chef des monuments historiques, résultant successivement du décret n° 80-911 du 20 novembre 1980 et du décret n° 2007-1405 du 28 septembre 2007 portant statut particulier de ce corps, ceux-ci sont des architectes, fonctionnaires de l'Etat, ayant notamment pour mission d'assurer la maîtrise d'oeuvre des travaux de restauration sur les immeubles classés au titre des monuments historiques dont ils assurent la surveillance. Les architectes en chef des monuments historiques, qui interviennent alors dans le cadre d'un contrat de maîtrise d'oeuvre et dont la responsabilité peut être engagée en raison des actes qu'ils accomplissent à titre professionnel ou des actes de leurs préposés, n'interviennent pas, nonobstant leur qualité de fonctionnaires, en qualité d'agents publics au sens de l'article 16 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture. Cet article ne crée, par suite, aucune obligation pour l'Etat de souscrire l'assurance garantissant les architectes en chef des conséquences des actes professionnels en question.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des vérificateurs des monuments historiques, CE, 17 octobre 2016, M. Tinchant, n° 389131, inédit.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
