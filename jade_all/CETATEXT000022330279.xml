<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022330279</ID>
<ANCIEN_ID>JG_L_2009_12_000000333918</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/33/02/CETATEXT000022330279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/12/2009, 333918, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>333918</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Christian  Vigouroux</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 octobre 2009 au secrétariat du contentieux du Conseil d'État, présentée par le SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION (SNU-TEFI), dont le siège est sis 43-45 rue de Javel à Paris (75015), représenté par son secrétaire général en exercice, demeurant en cette qualité audit siège ; le SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret n° 2009-1128 du 17 septembre 2009 portant adaptation des dispositions applicables aux agents contractuels de droit public de Pôle emploi ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que la condition d'urgence est satisfaite dès lors que le décret contesté porte une atteinte à la protection constitutionnelle de travailleurs en ce qu'il prive les agents publics contractuels de Pôle Emploi de la possibilité de participer, par l'intermédiaire de leurs représentants, à la détermination des conditions collectives de travail alors que d'importantes décisions en matière de recrutement, d'avancement, de reclassement ou de licenciement peuvent d'ores et déjà être prises par le seul directeur général de Pôle Emploi ; qu'il existe un doute sérieux quant à la légalité de la décision litigieuse ; qu'elle est entachée d'un vice de procédure qui a eu une influence sur le sens de la décision litigieuse dès lors que la consultation du comité central d'entreprise (CCE) de Pôle Emploi a été menée de manière irrégulière ; qu'elle porte une atteinte grave aux droits constitutionnellement garantis des agents contractuels de droit public dès lors que le décret litigieux a pour objet de supprimer la consultation obligatoire du comité consultatif paritaire national (CCPN), instance représentant le personnel au sein de l'ancienne agence nationale pour l'emploi (ANPE) sans prévoir pour la nouvelle institution son remplacement par la consultation obligatoire du CCE ; <br/>
<br/>
<br/>
              Vu le décret dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête en annulation ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 17 décembre 2009, présenté par le ministre de l'économie, de l'industrie et de l'emploi, qui conclut au rejet de la requête ; il soutient que la condition d'urgence n'est pas remplie dès lors qu'en vertu de la Constitution, la détermination des principes fondamentaux du droit du travail ressort du domaine de la loi et qu'un décret pris en application de la loi du 18 février 2008 qui confiait au comité d'entreprise de Pôle Emploi la compétence générale en matière de relations collectives ne saurait porter atteinte à un tel principe ; que les décisions individuelles de reclassement et de licenciement prises par le directeur général de Pôle Emploi font l'objet d'une consultation au sein des commissions nationales paritaires, composées pour moitié de représentants du personnel ; que, s'agissant des règles relatives aux relations collectives de travail, le comité central d'entreprise (CCE) est consulté en lieu et place du comité consultatif national en vertu de l'article L. 2323-1 du code du travail ; que le requérant ne saurait se prévaloir d'irrégularités de la procédure de consultation du CCE dès lors que le décret y a été présenté le 15 juin 2009 et que la direction générale de Pôle Emploi a accompli les diligences nécessaires ; que l'absence d'avis formel ne résulte pas d'une inertie, d'une négligence ou d'une obstruction de l'administration, mais de la seule volonté des représentants du personnel, qui ont expressément refusé de présenter leur avis, estimant notamment que la durée de réunion était trop courte et que l'information qui leur avait été fournie n'était pas suffisante ; que le moyen tiré de la suppression de la consultation obligatoire du comité central paritaire national doit être écarté dès lors que le décret litigieux conserve les garanties individuelles et collectives des agents publics de Pôle Emploi en maintenant les commissions nationales paritaires et en respectant la pleine compétence du comité d'entreprise s'agissant des situations collectives de travail ; que la suppression du comité central paritaire national est sans incidence sur la possibilité pour les travailleurs de participer à la détermination des conditions collectives de travail dès lors que le directeur général de Pôle Emploi ne peut, sans méconnaître les dispositions de l'article L. 2323-1 du code du travail, prendre des décisions dans le champ des règles collectives de travail sans avoir préalablement consulté le comité central d'entreprise ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 21 décembre 2009, présenté par le SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION qui reprend les conclusions de sa requête et les mêmes moyens ; il soutient en outre que, contrairement à ce qu'affirme le ministre, les commissions paritaires nationales ne sont consultées que pour les décisions individuelles et non dans le champ des garanties collectives ; que c'est à tort que le ministre estime que le comité central d'entreprise serait compétent pour les affaires examinées auparavant par le comité consultatif paritaire national dès lors que les mentions relatives à la consultation obligatoire du CCPN dans le décret n°2003-1370 n'ont pas été remplacées par la consultation obligatoire du CCE ; que le ministre ne saurait faire valoir que la loi du 13 février 2008 aurait donné pleine compétence au comité central d'entreprise dans ce domaine ; qu'il est manifeste que l'administration de Pôle Emploi n'a pas fait le nécessaire pour que le CCE rende un avis formel ; que la loi du 13 février 2008 se borne à énoncer que les règles collectives de travail du code du travail s'appliquent à tous les agents de l'institution sauf en matière de garanties résultant de la situation particulière des agents contractuels de droit public, qui se trouvent dans une situation particulière ; que le décret devait nécessairement déterminer les conditions dans lesquelles interviendrait la consultation des représentants du personnel ; qu'à défaut de le faire, le décret litigieux laisse le directeur général de Pôle Emploi seul compétent pour prendre des décisions en matière d'avancement et de licenciement ; <br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la loi n° 2008-126 du 13 février 2008 relative au service public de l'emploi ; <br/>
<br/>
              Vu le décret n° 2003-1370 du 31 décembre 2003 modifié fixant les dispositions applicables aux agents contractuels de droit public de l'agence nationale pour l'emploi ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION et, d'autre part, le ministre de l'économie, de l'industrie et de l'emploi ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 21 décembre 2009 à 16 heures 30 au cours de laquelle ont été entendus :<br/>
              - les représentants du SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION ;<br/>
              - la représentante du ministre de l'économie, de l'industrie et de l'emploi ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative :  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision  ; <br/>
<br/>
              Considérant qu'en vertu de l'article L. 5312-9 du code du travail résultant de la loi du 13 février 2008 relative à l'organisation du service public de l'emploi :  Les agents de l'Institution nationale, qui sont chargés d'une mission de service public, sont régis par le présent code dans les conditions particulières prévues par une convention collective étendue et agréée par les ministres chargés de l'emploi et du budget. Cette convention comporte des stipulations, notamment en matière de stabilité de l'emploi et de protection à l'égard des influences extérieures nécessaires à l'accomplissement de cette mission. / Les règles relatives aux  relations collectives de travail prévues par la deuxième partie du présent code s'appliquent à tous les agents de l'institution, sous réserve des garanties justifiées par la situation particulière de ceux qui restent contractuels de droit public. Ces garanties sont définies par décret en Conseil d'Etat ;<br/>
<br/>
              Considérant que, conformément à ces dispositions, le décret attaqué abroge le décret du 6 mai 1995 portant institution d'organismes consultatifs à l'Agence nationale pour l'emploi et supprime les  comités consultatifs paritaires  qui existaient à cette agence ; <br/>
<br/>
              Considérant toutefois, que, d'une part, le décret attaqué fait bénéficier les contractuels de droit public de la nouvelle Institution Pôle Emploi des dispositions de la deuxième partie du code du travail, notamment pour les institutions représentatives du personnel, dont les comités d'entreprise et les délégués du personnel ; qu'à ce titre, le directeur général de Pôle emploi  précisait aux organisations syndicales, par courrier du 17 juin 2009 antérieur à la publication du décret attaqué, que relèveraient de procédures d'information et de consultations préalables du comité central d'entreprise les principales décisions du directeur général relatives aux règles collectives de travail dont  la classification des emplois, la composition, les attributions et les modalités de fonctionnement des commissions paritaires(...) la formation continue des personnels, les principes de l'évaluation des personnels de droit public et la mise en oeuvre d'un éventuel plan social de reclassement  ; que, d'autre part, il maintient, sans en changer la compétence, la garantie que constitue pour les agents de droit public l'avis des  commissions paritaires   obligatoirement consultées sur les décisions individuelles intéressant lesdits agents et leur carrière ; qu'enfin, l'accord préalable conclu le 22 décembre 2008 entre la direction et plusieurs organisations syndicales, dans l'attente d'une convention collective qui vient d'être adoptée et se trouve en cours d'agrément par l'administration, a permis la mise en place sans délai d'instances transitoires représentatives de l'ensemble du personnel de Pôle Emploi et notamment  du comité central d'entreprise ;<br/>
<br/>
              Considérant que le décret litigieux est intervenu à la suite et au vu de pièces desquelles il résulte que ces instances transitoires représentatives du personnel de Pôle Emploi étaient consultées ; <br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les moyens invoqués susceptibles de faire douter de la légalité du décret attaqué, le dispositif issu de la loi du 13 février 2008 ne porte pas aux intérêts des agents contractuels de droit public de la nouvelle institution et des organisations syndicales qui les représentent une atteinte grave et immédiate constitutive d'une situation d'urgence au sens des dispositions précitées de l'article L.521-1 du code de justice administrative ; qu'ainsi les conclusions du SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION tendant à la suspension du décret du 17 septembre 2009 ne peuvent être accueillies ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse la somme que le SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION demande sur leur fondement ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au SYNDICAT NATIONAL UNITAIRE TRAVAIL-EMPLOI-FORMATION-INSERTION, au Premier ministre et au ministre de l'économie, de l'industrie et de l'emploi.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
