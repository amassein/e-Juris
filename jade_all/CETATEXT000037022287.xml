<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022287</ID>
<ANCIEN_ID>JG_L_2018_06_000000408398</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022287.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème chambres réunies, 06/06/2018, 408398</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408398</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408398.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le directeur général de l'Office français de protection des réfugiés et apatrides a présenté à la Cour nationale du droit d'asile un recours en révision de la décision n° 04020864 du 28 avril 2005 par laquelle la Commission des recours des réfugiés a annulé sa décision du 14 avril 2004 rejetant la demande d'admission au bénéfice de l'asile présentée par M. A...et lui a reconnu la qualité de réfugié.<br/>
<br/>
              Par une décision n°15000389 du 19 octobre 2016, la Cour nationale du droit d'asile a admis le recours en révision de l'Office français de protection des réfugiés et apatrides, a déclaré nulle et non avenue la décision du 28 avril 2005 de la Commission des recours des réfugiés et a rejeté le recours de M.A....<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 27 février 2017 et 23 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande :<br/>
<br/>
              1) d'annuler cette décision;<br/>
              2) de mettre à la charge de l'OFPRA la somme de 3 000 euros au titre de l'article L. 761 -1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              -  le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...A...et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides  ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
<br/>
              1. Aux termes du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ".<br/>
<br/>
              2. Aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, " le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : a) la peine de mort ou une exécution ; b) la torture ou des peines ou traitements inhumains ou dégradants ; c) s'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que M. A...avait obtenu, par une décision du 28 avril 2005 de la Commission des recours des réfugiés, la qualité de réfugié en raison, conformément au principe d'unité de la famille, de la reconnaissance de cette même qualité à son père. Toutefois, à la suite d'informations transmises par le parquet de la cour d'appel d'Amiens, l'Office français de protection des réfugiés et des apatrides, OFPRA, a demandé à la Cour nationale du droit d'asile la révision de la décision de la Commission des recours des réfugiés au motif qu'elle était fondée sur des éléments frauduleux sur l'état-civil de l'intéressé, relatifs  notamment à son âge et que l'intéressé, contrairement à ce qu'il avait soutenu  à l'appui de sa demande d'asile, n'était  pas mineur à la date de la décision de la Commission. Par une décision du 19 octobre 2016, la Cour nationale du droit d'asile a admis le recours en révision de l'OFPRA,  déclaré nulle et non avenue la décision du 28 avril 2005 de la Commission des recours des réfugiés et  rejeté le recours de M. A...contre la décision de l'OFPRA du 14 avril 2004 rejetant sa demande d'asile.<br/>
<br/>
              4. Aux termes de l'article R 733-36 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au recours en révision de l'OFPRA : "La cour peut être saisie d'un recours en révision dans le cas où il est soutenu que sa décision est fondée sur des circonstances de fait établies de façon frauduleuse. / Le recours doit être exercé dans le délai de deux mois après que la fraude a été constatée (...)". <br/>
<br/>
              5. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que le parquet de la cour d'appel d'Amiens a transmis à l'OFPRA, le 30 juin 2014, la demande d'extradition dont M. A...faisait l'objet de la part des autorités arméniennes, qui comportait une date de naissance antérieure à celle qu'avait fournie M. A...à l'appui de sa demande d'asile, puis, le 20 août 2014, la fiche de recherche d'Interpol, avec la photographie de M.A..., confirmant son identité ainsi que divers autres documents attestant cette date de naissance. Au vu de ces éléments, l'OFPRA a convoqué, le 20 octobre 2014, M. A...à un entretien, qui a eu lieu le 7 novembre 2014. Dès lors que l'OFPRA a convoqué M. A..., avant l'expiration du délai de deux mois dont l'office disposait pour former un recours en révision et qui courait à compter de la réception des éléments d'informations permettant de caractériser l'existence d'une fraude, pour un entretien qu'il a estimé nécessaire pour lui permettre de regarder la fraude comme constatée , la Cour n'a pas commis d'erreur de droit en ne soulevant pas d'office la tardiveté du recours en révision de l'OFPRA, enregistré devant elle le 8 janvier 2015, soit dans le délai de deux mois courant à compter de la date à laquelle a eu lieu l'entretien de M.A.... <br/>
<br/>
              6. Il ressort des énonciations de la décision attaquée que, pour retenir l'existence d'une fraude de M. A...sur sa date de naissance, la cour a, par une appréciation souveraine qui n'est pas entachée de dénaturation, pris en compte l'ensemble des éléments produits par l'OFPRA et écarté l'argumentation de M. A...tenant tant à l'existence d'une vengeance d'un ancien compagnon de son épouse qu'à une erreur sur la date d'obtention de son permis de conduire. La cour n'a dès lors pas commis d'erreur de droit en jugeant que M. A...avait fourni de fausses informations à l'appui de sa demande d'asile, qui avaient été déterminantes pour la reconnaissance de la qualité de réfugié, cette dernière résultant de l'application du principe de l'unité de la famille compte tenu de sa minorité à la date de la décision de la Commission des recours des réfugiés. <br/>
<br/>
              7. Il ressort par ailleurs des pièces du dossier soumis à la Cour que, pour demander le bénéfice du statut de réfugié, M.A..., qui avait soutenu initialement être de nationalité azerbaïdjanaise, n'a invoqué que des risques en cas de retour en Azerbaïdjan ou en Russie, sans se prévaloir de craintes en cas de retour en Arménie. Dès lors qu'au vu des éléments produits devant elle par l'OFPRA sur le véritable état-civil de M.A..., la cour a estimé, par une appréciation souveraine qui n'est pas arguée  de dénaturation, qu'il était de nationalité arménienne, elle n'a ni commis d'erreur de droit, ni insuffisamment motivé sa décision en regardant cette nationalité comme établie et en écartant les risques invoqués par M. A...en cas de retour tant en Azerbaïdjan qu'en Russie, sans se prononcer sur d'éventuelles craintes en cas de retour en Arménie, dont le requérant ne se prévalait pas. <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au  directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
