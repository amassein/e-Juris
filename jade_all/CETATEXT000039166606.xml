<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039166606</ID>
<ANCIEN_ID>JG_L_2019_09_000000422437</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/66/CETATEXT000039166606.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 25/09/2019, 422437</TITRE>
<DATE_DEC>2019-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422437</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:422437.20190925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 20 juillet 2018 au secrétariat du contentieux du Conseil d'État, Mme D... V..., M. M... F..., Mme I... G..., Mme C... H..., M. N... U..., M. W... B..., Mme E... J..., Mme P... K..., Mme T... R..., M. S... O..., Mme L... A... et Mme Q... X... demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de la garde des sceaux, ministre de la justice, refusant l'abrogation ou la réformation de l'annexe 4 de la circulaire du 14 novembre 2017 relative à la gestion de l'indemnité de fonctions, de sujétions et d'expertise des corps interministériels et corps à statut commun relevant du ministère de la justice dans le cadre du régime indemnitaire tenant compte des fonctions, des sujétions, de l'expertise et de l'engagement professionnel ;<br/>
<br/>
              2°) d'enjoindre sous astreinte à la garde des sceaux, ministre de la justice, de prendre une nouvelle circulaire classant les personnels de l'Ecole nationale de la magistrature au sein de la catégorie " administration centrale, établissements et services assimilés " et révisant la cartographie des fonctions exercées par les attachés d'administration de l'État au sein de l'École nationale de la magistrature.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 72-355 du 4 mai 1972 ;<br/>
              - le décret n° 2014-513 du 20 mai 2014 ;<br/>
              - le décret n° 2015-510 du 7 mai 2015 ;<br/>
              - l'arrêté du 3 juin 2015 pris pour l'application au corps interministériel des attachés d'administration de l'État des dispositions du décret n° 2014-513 du 20 mai 2014 portant création d'un régime indemnitaire tenant compte des fonctions, des sujétions, de l'expertise et de l'engagement professionnel dans la fonction publique de l'État ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le décret du 20 mai 2014 portant création d'un régime indemnitaire tenant compte des fonctions, des sujétions, de l'expertise et de l'engagement professionnel dans la fonction publique de l'Etat institue un nouveau régime indemnitaire de référence commun à l'ensemble de la fonction publique d'Etat, qui est appelé à se substituer aux différents régimes de primes en vigueur. En vertu de l'article 2 de ce décret, un arrêté du ministre chargé de la fonction publique et du ministre chargé du budget et, le cas échéant, du ministre intéressé fixe les montants minimaux et maximaux de l'indemnité par groupe de fonctions exercées par les fonctionnaires au sein d'un même corps ou statut d'emploi selon le niveau de responsabilité et d'expertise requis dans l'exercice des fonctions. L'arrêté du 3 juin 2015 pris pour l'application du décret au corps interministériel des attachés d'administration de l'État fixe les plafonds annuels et montants minimaux annuels de l'indemnité en opérant une distinction entre ces attachés selon qu'ils sont affectés en " administration centrale, établissements et services assimilés " ou bien dans des " services déconcentrés, établissements et services assimilés ". La circulaire du garde des sceaux, ministre de la justice du 14 novembre 2017 relative à la gestion de cette indemnité pour les corps interministériels et corps à statut commun relevant du ministère de la justice prise pour la mise en oeuvre de ce nouveau régime indemnitaire opère, selon son annexe 4 intitulée " Cartographie des fonctions, socles indemnitaires et montants forfaitaires applicables ", une distinction, s'agissant des attachés relevant, pour leur gestion, de ce département ministériel, entre les fonctions exercées respectivement, d'une part, en administration centrale et, d'autre part, dans les services déconcentrés et les établissements publics. Elle classe les fonctions des attachés affectés à l'Ecole nationale de la magistrature dans cette seconde catégorie, ce qui conduit à ne pas appliquer aux intéressés les socles indemnitaires et montants maximum d'indemnité plus favorables prévus pour l'administration centrale.  <br/>
<br/>
              2. Mme V... et autres, attachés d'administration de l'État relevant pour leur gestion du ministère de la justice et affectés à l'École nationale de la magistrature, demandent, d'une part, l'annulation pour excès de pouvoir de la décision implicite par laquelle la garde des sceaux, ministre de la justice, a refusé de faire droit à leur demande tendant à l'abrogation ou à la réformation de l'annexe 4 de la circulaire en tant qu'elle n'assimile pas les fonctions qu'ils exercent à des fonctions exercées à l'administration centrale du ministère de la justice et, d'autre part, à ce qu'il soit enjoint sous astreinte à la garde des sceaux, ministre de la justice, de prendre une nouvelle circulaire classant les personnels de cette école au sein de la catégorie " administration centrale, établissements et services assimilés " et révisant la cartographie des fonctions exercées par les attachés d'administration de l'État au sein de l'École nationale de la magistrature.<br/>
<br/>
              3. L'autorité administrative compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date. De même, lorsqu'elle est saisie d'une demande tendant à la réformation d'un règlement illégal, l'autorité compétente est tenue d'y substituer des dispositions de nature à mettre fin à cette illégalité.<br/>
<br/>
              4. En premier lieu, ainsi qu'il a été dit au point 1, l'arrêté interministériel du 3 juin 2015 pris pour l'application au corps interministériel des attachés d'administration de l'État des dispositions du décret du 20 mai 2014 fixe le plafond annuel et le montant minimal annuel de l'indemnité en opérant une distinction entre ces attachés selon qu'ils sont affectés en administration centrale, établissements et services assimilés ou bien dans des services déconcentrés, établissements et services assimilés. En distinguant, d'une part, les fonctions exercées en administration centrale et, d'autre part, celles exercées dans les services déconcentrés et les établissements publics du ministère de la justice et en classant l'Ecole nationale de la magistrature dans cette seconde catégorie, le garde des sceaux, ministre de la justice, s'agissant de la gestion des personnels relevant des services placés sous son autorité et afin de permettre l'attribution de l'indemnité prévue par les dispositions précitées du décret du 20 mai 2014 et de l'arrêté interministériel du 3 juin 2015, a assuré l'application de ces dispositions sans les méconnaître. <br/>
<br/>
              5. En deuxième lieu, en application des dispositions de l'article 3 du décret du 7 mai 2015 portant charte de la déconcentration, les administrations centrales assurent, au niveau national, un rôle de conception, d'animation, d'appui des services déconcentrés, d'orientation, d'évaluation et de contrôle. En vertu du décret du 4 mai 1972 relatif à l'École nationale de la magistrature, cette école est un établissement public à caractère administratif, placé sous la tutelle du garde des sceaux, ministre de la justice, dont la mission principale est la formation initiale et continue des magistrats de l'ordre judiciaire français. En établissant, eu égard à la nature de leurs missions respectives et aux sujétions qui s'y attachent, une distinction entre les attachés d'administration affectés au sein des services de l'administration centrale du ministère de la justice et ceux affectés à l'École nationale de la magistrature, la circulaire litigieuse n'a, en tout état de cause, pas méconnu le principe d'égalité.<br/>
<br/>
              6. En dernier lieu, si Mme V... et autres soutiennent qu'en plaçant dans les groupes de fonctions 2 et 3 les différentes fonctions de chef de service de l'École nationale de la magistrature alors que ces fonctions devraient relever des groupes immédiatement supérieurs, l'annexe 4 de la circulaire attaquée serait entachée d'erreur de droit, ils n'apportent au soutien de ce moyen aucune précision permettant d'en apprécier le bien-fondé<br/>
<br/>
              7. Il résulte de tout ce qui précède que Mme V... et autres ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent. Par suite, leur requête doit être rejetée, y compris leurs conclusions à fin d'injonction.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme V... et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D... V..., représentante unique, pour l'ensemble des requérants et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03-12 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. MINISTRE DE LA JUSTICE. - DÉCRET INSTITUANT UNE INDEMNITÉ DE FONCTIONS, DE SUJÉTIONS ET D'EXPERTISE - ARRÊTÉ INTERMINISTÉRIEL ENCADRANT LES MONTANTS DE CETTE INDEMNITÉ EN DISTINGUANT ENTRE ADMINISTRATIONS CENTRALES ET SERVICES DÉCONCENTRÉS - COMPÉTENCE DU MINISTRE DE LA JUSTICE, CHEF DE SERVICE [RJ1], POUR DISTINGUER LES FONCTIONS EXERCÉES EN ADMINISTRATION CENTRALE DE CELLES EXERCÉES DANS LES SERVICES DÉCONCENTRÉS ET LES ÉTABLISSEMENTS PUBLICS DU MINISTÈRE ET POUR CLASSER L'ENM DANS LA SECONDE CATÉGORIE - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - DÉCRET INSTITUANT UNE INDEMNITÉ DE FONCTIONS, DE SUJÉTIONS ET D'EXPERTISE - ARRÊTÉ INTERMINISTÉRIEL ENCADRANT LES MONTANTS DE CETTE INDEMNITÉ EN DISTINGUANT ENTRE ADMINISTRATIONS CENTRALES ET SERVICES DÉCONCENTRÉS - COMPÉTENCE DU MINISTRE DE LA JUSTICE, CHEF DE SERVICE [RJ1], POUR DISTINGUER LES FONCTIONS EXERCÉES EN ADMINISTRATION CENTRALE DE CELLES EXERCÉES DANS LES SERVICES DÉCONCENTRÉS ET LES ÉTABLISSEMENTS PUBLICS DU MINISTÈRE ET POUR CLASSER L'ENM DANS LA SECONDE CATÉGORIE - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-02-02-01-03-12 Arrêté interministériel du 3 juin 2015, pris pour l'application au corps interministériel des attachés d'administration de l'État des dispositions du décret n° 2014-513 du 20 mai 2014, fixant le plafond annuel et le montant minimal annuel de l'indemnité en opérant une distinction entre ces attachés selon qu'ils sont affectés en administration centrale, établissements et services assimilés ou bien dans des services déconcentrés, établissements et services assimilés.,,,En distinguant, d'une part, les fonctions exercées en administration centrale et, d'autre part, celles exercées dans les services déconcentrés et les établissements publics du ministère de la justice et en classant l'Ecole nationale de la magistrature (ENM) dans cette seconde catégorie, le garde des sceaux, ministre de la justice, s'agissant de la gestion des personnels relevant des services placés sous son autorité et afin de permettre l'attribution de l'indemnité prévue par le décret du 20 mai 2014 et l'arrêté interministériel du 3 juin 2015, a assuré l'application de ces textes sans les méconnaître.</ANA>
<ANA ID="9B"> 36-08-03 Arrêté interministériel du 3 juin 2015, pris pour l'application au corps interministériel des attachés d'administration de l'État des dispositions du décret n° 2014-513 du 20 mai 2014, fixant le plafond annuel et le montant minimal annuel de l'indemnité en opérant une distinction entre ces attachés selon qu'ils sont affectés en administration centrale, établissements et services assimilés ou bien dans des services déconcentrés, établissements et services assimilés.,,,En distinguant, d'une part, les fonctions exercées en administration centrale et, d'autre part, celles exercées dans les services déconcentrés et les établissements publics du ministère de la justice et en classant l'Ecole nationale de la magistrature (ENM) dans cette seconde catégorie, le garde des sceaux, ministre de la justice, s'agissant de la gestion des personnels relevant des services placés sous son autorité et afin de permettre l'attribution de l'indemnité prévue par le décret du 20 mai 2014 et l'arrêté interministériel du 3 juin 2015, a assuré l'application de ces textes sans les méconnaître.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 7 février 1936, Jamart, n° 43321, p. 172.,,[RJ2] Rappr. CE, 9 novembre 2018,,, n° 412640, T. pp. 514-738.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
