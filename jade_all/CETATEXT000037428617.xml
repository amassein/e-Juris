<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037428617</ID>
<ANCIEN_ID>JG_L_2018_09_000000419757</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/42/86/CETATEXT000037428617.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 24/09/2018, 419757</TITRE>
<DATE_DEC>2018-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419757</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419757.20180924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté du préfet de Mayotte du 7 mars 2018 l'obligeant à quitter le territoire français sans délai à destination des Comores et lui interdisant de revenir sur le territoire français pendant trois ans. <br/>
<br/>
              Par une ordonnance n° 1800481 du 10 mars 2018, le juge des référés du tribunal administratif de Mayotte, statuant par application de l'article L. 522-3 du code de justice administrative, a rejeté sa demande et l'a condamné au paiement d'une amende pour recours abusif d'un montant de 5 000 euros. <br/>
<br/>
              Par un pourvoi, enregistré le 10 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle l'a condamné au paiement d'une amende pour recours abusif ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la SCP Anne Sevaux et Paul Mathonnet, son avocat, d'une somme de 2 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article R. 741-12 du code de justice administrative : " Le juge peut infliger à l'auteur d'une requête qu'il estime abusive une amende dont le montant ne peut excéder 10 000 euros " ; que le pouvoir conféré au juge administratif d'assortir, le cas échéant, sa décision d'une amende pour recours abusif n'est pas soumis à l'exigence d'une motivation spéciale ; que la qualification juridique à laquelle il se livre pour estimer qu'une requête présente un caractère abusif peut être utilement discutée devant le juge de cassation ; que le montant de l'amende relève, en revanche, de son pouvoir souverain d'appréciation et n'est susceptible d'être remis en cause par le juge de cassation qu'en cas de dénaturation ; <br/>
<br/>
              2.	Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le préfet de Mayotte a pris, le 2 mars 2017, à l'encontre de M. B...un arrêté portant obligation de quitter le territoire français à destination des Comores ; que la demande en référé formée par l'intéressé sur le fondement de l'article L. 521-2 du code de justice administrative a été rejetée par une ordonnance du juge des référés du tribunal administratif de Mayotte en date du 6 mars 2017 ; que le recours pour excès de pouvoir de l'intéressé a été rejeté par un jugement du tribunal administratif en date du 19 décembre 2017 ; que, l'intéressé étant revenu à Mayotte après avoir été reconduit aux Comores, le préfet de Mayotte a pris à son encontre, le 7 mars 2018, un nouvel arrêté portant obligation de quitter le territoire français sans délai à destination des Comores et lui interdisant de retourner sur le territoire français pendant trois ans ; que, par une ordonnance du 10 mars 2018, le juge des référés du tribunal administratif de Mayotte a rejeté la demande de l'intéressé, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à la suspension de l'exécution de l'arrêté du 7 mars 2018 et l'a condamné à une amende de 5 000 euros pour recours abusif ; que M. B...se pourvoit en cassation contre cette ordonnance en tant qu'elle lui a infligé une amende pour recours abusif ;<br/>
<br/>
              3.	Considérant que la demande de M. B...sur laquelle a statué l'ordonnance attaquée était dirigée contre l'arrêté du 7 mars 2018 lui faisant obligation de quitter le territoire français et n'avait pas le même objet que les requêtes précédemment présentées par l'intéressé, lesquelles étaient dirigées contre des décisions d'éloignement distinctes ; qu'eu égard à l'objet de cette nouvelle demande et à son contenu, et quand bien même les moyens soulevés s'apparentaient à ceux sur le bien-fondé desquels le tribunal s'était auparavant prononcé, le juge des référés ne pouvait, sans erreur de qualification juridique, qualifier d'abusive la demande qui lui était soumise ; que son ordonnance doit, par suite, être annulée en tant qu'elle a condamné M. B... à payer une amende pour recours abusif ;<br/>
<br/>
              4.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme à verser à la SCP Anne Sevaux et Paul Mathonnet, avocat de M.B..., sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 2 de l'ordonnance du 10 mars 2018 du juge des référés du tribunal administratif de Mayotte est annulé. <br/>
Article 2 : Le surplus des conclusions du pourvoi de M. B...est rejeté. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-055 PROCÉDURE. JUGEMENTS. AMENDE POUR RECOURS ABUSIF. - FIXATION DU MONTANT DE L'AMENDE - CONTRÔLE DU JUGE DE CASSATION - APPRÉCIATION SOUVERAINE DES JUGES DU FOND, SOUS RÉSERVE DE DÉNATURATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - FIXATION DU MONTANT D'UNE AMENDE POUR RECOURS ABUSIF, SOUS RÉSERVE DE DÉNATURATION [RJ1].
</SCT>
<ANA ID="9A"> 54-06-055 Le montant de l'amende pour recours abusif dont le juge administratif peut assortir, le cas échéant, sa décision relève de son pouvoir souverain d'appréciation et n'est susceptible d'être remis en cause par le juge de cassation qu'en cas de dénaturation.</ANA>
<ANA ID="9B"> 54-08-02-02-01-03 Le montant de l'amende pour recours abusif dont le juge administratif peut assortir, le cas échéant, sa décision relève de son pouvoir souverain d'appréciation et n'est susceptible d'être remis en cause par le juge de cassation qu'en cas de dénaturation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Section, 9 novembre 2007, Mme,, n° 293987, p. 444.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
