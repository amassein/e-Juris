<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675185</ID>
<ANCIEN_ID>JG_L_2020_02_000000439006</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/51/CETATEXT000041675185.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/02/2020, 439006, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439006</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439006.20200226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A..., ressortissante ivoirienne, a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Nord de procéder à l'enregistrement de sa demande d'asile en procédure normale, à la suite du refus qui lui a été opposé le 12 décembre 2019, et de lui délivrer l'attestation correspondante ainsi que le dossier destiné à l'Office français de protection des réfugiés et apatrides (OFPRA) dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 150 euros par jour de retard. Par ailleurs, elle a demandé qu'il soit enjoint à l'Office français d'immigration et d'intégration (OFII) de rétablir à son profit le bénéfice des conditions matérielles d'accueil, et de procéder au règlement à titre rétroactif de l'allocation pour demandeur d'asile. Par une ordonnance n° 2000874 du 7 février 2020, le juge des référés du tribunal administratif de Lille a rejeté sa demande.<br/>
              Par une requête, enregistrée le 21 février 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de l'admettre provisoirement au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
              2°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Lille ;<br/>
<br/>
              3°) d'enjoindre au préfet du Nord, d'une part, d'enregistrer sa demande d'asile en procédure normale, sous un délai de quarante-huit heures à compter de la notification de l'ordonnance à venir et, d'autre part, de lui remettre une attestation de demande d'asile ainsi que le dossier à transmettre à l'OFPRA, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              4°) d'enjoindre à l'Office français de l'immigration et de l'intégration de rétablir les conditions matérielles d'accueil dans un délai de quarante-huit heures à compter de l'ordonnance à intervenir avec effet rétroactif au 16 janvier 2020, sinon à compter de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie, en premier lieu, dès lors que la décision de transfert vers l'Allemagne est exécutable à tout moment, en deuxième lieu, eu égard aux effets s'attachant au refus d'enregistrement de sa demande d'asile et, en dernier lieu, au regard de la précarité dans laquelle ses filles et elle sont placées du fait de la perte du bénéfice des conditions matérielles d'accueil ;<br/>
              -  il est porté une atteinte grave et manifestement illégale au droit d'asile dès lors que la fuite au sens de l'article 29 du règlement " Dublin III " n'est pas caractérisée et que, par suite, la France est responsable de sa demande d'asile en application de ce règlement.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 février 2020, l'Office français de l'immigration et de l'intégration conclut au non-lieu à statuer dès lors que le bénéfice des conditions matérielles d'accueil sera rétabli en faveur de la requérante à compter, rétroactivement, du 16 janvier 2020.<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 février 2020, le ministre de l'intérieur conclut au non-lieu à statuer, dès lors que la requérante est convoquée en préfecture pour se voir délivrer une attestation de demande d'asile lui permettant de déposer son dossier auprès de l'OFPRA, et au rejet des conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A..., d'autre part, le ministre de l'intérieur et l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              Vu la lettre informant les parties de la radiation de l'affaire du rôle de l'audience publique du 26 février 2020 à 11 heures ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Mme A..., ressortissante ivoirienne accompagnée de ses deux filles mineures, a déposé une demande d'asile à la préfecture du Nord, enregistrée le 29 mars 2019 en procédure Dublin. Le 8 avril 2019, les autorités allemandes ont accepté par accord explicite leur responsabilité pour l'examen de la demande. Mme A... a fait l'objet d'un arrêté de transfert du préfet du Nord le 29 avril 2019 dont elle a demandé l'annulation devant le tribunal administratif de Lille. Suite au rejet de sa demande le 3 juin 2019, elle s'est vu remettre les titres de transport nécessaires à son transfert vers l'Allemagne, prévu le 3 décembre 2019. La requérante ne s'est toutefois pas présentée à la gare d'Arras pour prendre son train. Pour justifier son absence, elle soutient avoir été admise au service des urgences d'Arras le 3 décembre 2019 puis s'être rendue à la préfecture l'après-midi pour exposer sa situation. Le 4 décembre 2019, le préfet du Nord a informé les autorités allemandes de la prolongation du délai de transfert de la requérante. Par une décision du 12 décembre 2019, il a refusé l'enregistrement de la demande d'asile en procédure normale, au motif que Mme A... avait été considérée comme en fuite au sens de l'article 29 du règlement " Dublin III ". Par une décision du 16 janvier 2020, l'Office français d'immigration et d'intégration a suspendu ses conditions matérielles d'accueil. Mme A... a demandé au juge des référés du tribunal administratif de Lille, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet du Nord de procéder à l'enregistrement de sa demande d'asile en procédure normale et de lui délivrer l'attestation correspondante ainsi que le dossier destiné à l'Office français de protection des réfugiés et apatrides (OFPRA) dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 150 euros par jour de retard. Elle lui a en outre demandé d'enjoindre à l'Office français d'immigration et d'intégration de rétablir à son profit le bénéfice des conditions matérielles d'accueil et de procéder au règlement à titre rétroactif de l'allocation pour demandeur d'asile. Par une ordonnance n° 2000874 du 7 février 2020, le juge des référés du tribunal administratif de Lille a rejeté sa demande.<br/>
              3. Il résulte de l'instruction que, par courrier du 24 février 2020, la préfecture du Nord a convoqué Mme A... à un rendez-vous le 17 mars 2020 en vue de la requalification de sa demande d'asile en procédure normale et de la délivrance d'une attestation de demande d'asile et de formulaire de demande d'asile lui permettant de déposer sa demande auprès de l'OFPRA. En conséquence, l'Office français d'immigration et d'intégration a pris acte de cette convocation et s'engage à rétablir rétroactivement le bénéfice des conditions matérielles d'accueil en faveur de Mme A... à compter du 16 janvier 2020, dès qu'elle aura été mise en possession d'une attestation de demande d'asile.<br/>
<br/>
              4. Dans ces conditions, les conclusions de Mme A... tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
              5. Dans les circonstances de l'espèce, il n'y a pas lieu, sans qu'il soit besoin de se prononcer sur la demande d'admission provisoire à l'aide juridictionnelle, de faire droit aux conclusions présentées au titre des dispositions des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête présentées sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
Article 2 : Les conclusions présentées par Mme A... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A..., au ministre de l'intérieur et à l'Office français d'immigration et d'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
