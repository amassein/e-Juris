<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227991</ID>
<ANCIEN_ID>JG_L_2019_03_000000418949</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 13/03/2019, 418949</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418949</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418949.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société PMD Vallon, la société Serret mécanique, la société Courbis industries, l'E.U.R.L. Verlingue Gué et Fils, la société Tecnis, la société 26H7, la société AST, la société Iltec, la société Chaudibois-Jarillon, M. L...O..., M. T...D..., M. M...J..., M. S...H..., la société DRFI, la société PFI, Mme AC...I..., M. U...F..., Mme N...W..., M. C...X..., Mme Q...X..., Mme AB...R..., M. AD...V..., M. AA...V..., Mme AE...B..., M. U...K..., M. E...P..., Mme G...Z...et M. A...Y...ont demandé au tribunal administratif de Grenoble d'annuler l'arrêté du 6 août 2012 par lequel le préfet de la Drôme a autorisé la société SIPER à exploiter un centre de méthanisation de déchets sur le territoire de la commune de Bourg-de-Péage (Drôme). Par un jugement n° 1304271 du 3 novembre 2015, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16LY00015 du 11 janvier 2018, la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif de Grenoble et l'arrêté du 6 août 2012.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 mars et 13 juin 2018 au secrétariat du contentieux du Conseil d'État, le ministre d'État, ministre de la transition écologique et solidaire demande au Conseil d'État d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2008/50/CE du Parlement européen et du Conseil du 21 mai 2008 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société PMD Vallon.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 6 août 2012, le préfet de la Drôme a autorisé la société SIPER à exploiter un centre de méthanisation de biodéchets sur le territoire de la commune de Bourg-de-Péage. Par un jugement du 3 novembre 2015, le tribunal administratif de Grenoble a rejeté la demande de la société PMD Vallon et autres tendant à l'annulation de l'arrêté préfectoral. Par un arrêt du 11 janvier 2018, contre lequel le ministre d'État, ministre de la transition écologique et solidaire se pourvoit en cassation, la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif et l'arrêté du 6 août 2012 au motif que l'omission d'analyse, dans l'étude d'impact, de la quantité de particules PM 2,5 émises par l'installation entachait d'irrégularité la procédure d'élaboration de l'arrêté.<br/>
<br/>
              2.	D'une part, les articles L. 221-1 et suivants du code de l'environnement définissent les modalités selon lesquelles l'État et les collectivités territoriales doivent assurer la surveillance de la qualité de l'air et de ses effets sur la santé et l'environnement et, mettre en oeuvre des mesures préventives et correctrices, y compris à l'égard des installations classées pour la protection de l'environnement, afin de réduire les émissions polluantes dans les zones couvertes par un plan de prévention de l'atmosphère. Le II de l'article R. 221-1 du même code définit les normes de qualité de l'air mentionnées à l'article L. 221-1, notamment les " valeurs limites " pour les particules PM 10 et les particules PM 2,5.<br/>
<br/>
              3.	D'autre part, aux termes de l'article R. 512-6 du code de l'environnement, dans sa rédaction alors en vigueur : " I. - A chaque exemplaire de la demande d'autorisation doivent être jointes les pièces suivantes : / (...) / 4° L'étude d'impact prévue à l'article L. 122-1 dont le contenu, par dérogation aux dispositions de l'article R. 122-3, est défini par les dispositions de l'article R. 512-8 ; / (...) ". L'article R. 512-8 du même code, dans sa rédaction alors en vigueur dispose : " I. - Le contenu de l'étude d'impact mentionnée à l'article R. 512-6 doit être en relation avec l'importance de l'installation projetée et avec ses incidences prévisibles sur l'environnement, au regard des intérêts mentionnés aux articles L. 211-1 et L. 511-1. / II. - Elle présente successivement : / (...) / 2° Une analyse des effets directs et indirects, temporaires et permanents de l'installation sur l'environnement et, en particulier, sur les sites et paysages, la faune et la flore, les milieux naturels et les équilibres biologiques, sur la commodité du voisinage (bruits, vibrations, odeurs, émissions lumineuses) ou sur l'agriculture, l'hygiène, la santé, la salubrité et la sécurité publiques, sur la protection des biens matériels et du patrimoine culturel. Cette analyse précise notamment, en tant que de besoin, l'origine, la nature et la gravité des pollutions de l'air, de l'eau et des sols, les effets sur le climat le volume et le caractère polluant des déchets, le niveau acoustique des appareils qui seront employés ainsi que les vibrations qu'ils peuvent provoquer, le mode et les conditions d'approvisionnement en eau et d'utilisation de l'eau ; / (...) ". Il résulte de ces dispositions que le contenu de l'étude d'impact doit être proportionné à l'importance du projet et de ses risques prévisibles pour la santé et l'environnement.<br/>
<br/>
              4. Les effets sur l'environnement d'un projet d'installation classée qui doivent, conformément à l'article R. 512-8 du code de l'environnement, faire l'objet d'une analyse spécifique dans l'étude d'impact doivent être déterminés au regard de la nature de l'installation projetée, de son emplacement et de ses incidences prévisibles sur l'environnement. En ce qui concerne plus particulièrement les effets sur la qualité de l'air, il y a lieu, pour procéder ainsi qu'il vient d'être dit, alors même que les dispositions du code de l'environnement mentionnées au point 2 n'ont pas pour objet de fixer des prescriptions relatives à la demande d'autorisation d'une installation classée pour la protection de l'environnement, de prendre en compte les normes de qualité de l'air qu'elles fixent et, le cas échéant, les mesures prises par le préfet, sur le fondement des mêmes dispositions, dans la zone concernée. Il résulte de ce qui précède que, en jugeant que le défaut, dans l'étude d'impact, d'analyse spécifique relative aux particules PM 2,5 susceptibles d'êtres émises par l'installation projetée avait nui à l'information de la population et, par suite, entaché d'irrégularité la procédure d'adoption de l'arrêté attaqué, sans rechercher si les incidences prévisibles de ces émissions justifiaient une telle analyse, la cour a entaché son arrêt d'erreur de droit.<br/>
<br/>
              5.	Dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre d'État, ministre de la transition écologique et solidaire est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 janvier 2018 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions présentées par la société PMD Vallon et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre d'État, ministre de la transition écologique et solidaire et à la société PMD Vallon, première dénommée, pour l'ensemble des défendeurs. Copie en sera adressée à la société SIPER.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-006-03-01-02 NATURE ET ENVIRONNEMENT. - DÉTERMINATION DES EFFETS SUR L'ENVIRONNEMENT D'UN PROJET D'INSTALLATION CLASSÉE DEVANT FAIRE L'OBJET D'UNE ANALYSE SPÉCIFIQUE (ART. R.512-8 DU CODE DE L'ENVIRONNEMENT) - 1) CRITÈRES - NATURE DE L'INSTALLATION, EMPLACEMENT ET INCIDENCES PRÉVISIBLES [RJ1] - 2) APPLICATION AUX EFFETS SUR LA QUALITÉ DE L'AIR - PRISE EN COMPTE DES NORMES APPLICABLES (ART. L. 221-1 ET S. DU CODE DE L'ENVIRONNEMENT) ET DES MESURES PRISES PAR LE PRÉFET DANS LA ZONE CONCERNÉE - EXISTENCE - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-02-02-01-01 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÉGIME JURIDIQUE. POUVOIRS DU PRÉFET. INSTRUCTION DES DEMANDES D'AUTORISATION. - COMPOSITION DU DOSSIER - ETUDE D'IMPACT - DÉTERMINATION DES EFFETS SUR L'ENVIRONNEMENT D'UN PROJET D'INSTALLATION CLASSÉE DEVANT FAIRE L'OBJET D'UNE ANALYSE SPÉCIFIQUE - 1) CRITÈRES - NATURE DE L'INSTALLATION, EMPLACEMENT ET INCIDENCES PRÉVISIBLES [RJ1] - 2) APPLICATION AUX EFFETS SUR LA QUALITÉ DE L'AIR - PRISE EN COMPTE DES NORMES APPLICABLES (ART. L. 221-1 ET S. DU CODE DE L'ENVIRONNEMENT) ET DES MESURES PRISES PAR LE PRÉFET DANS LA ZONE CONCERNÉE - EXISTENCE - 3) ESPÈCE.
</SCT>
<ANA ID="9A"> 44-006-03-01-02 1) Les effets sur l'environnement d'un projet d'installation classée qui doivent, conformément à l'article R. 512-8 du code de l'environnement, faire l'objet d'une analyse spécifique dans l'étude d'impact doivent être déterminés au regard de la nature de l'installation projetée, de son emplacement et de ses incidences prévisibles sur l'environnement.... ...2) En ce qui concerne plus particulièrement les effets sur la qualité de l'air, il y a lieu, pour procéder ainsi qu'il vient d'être dit, alors même que les articles L. 221-1 et suivants du code de l'environnement n'ont pas pour objet de fixer des prescriptions relatives à la demande d'autorisation d'une installation classée pour la protection de l'environnement, de prendre en compte les normes de qualité de l'air qu'elles fixent et, le cas échéant, les mesures prises par le préfet, sur le fondement des mêmes dispositions, dans la zone concernée.... ...3) Commet une erreur de droit la cour qui juge que le défaut, dans l'étude d'impact, d'analyse spécifique relative aux particules PM 2,5 susceptibles d'êtres émises par l'installation projetée avait nui à l'information de la population et, par suite, entaché d'irrégularité la procédure d'adoption de l'arrêté attaqué, sans rechercher si les incidences prévisibles de ces émissions justifiaient une telle analyse.</ANA>
<ANA ID="9B"> 44-02-02-01-01 1) Les effets sur l'environnement d'un projet d'installation classée qui doivent, conformément à l'article R. 512-8 du code de l'environnement, faire l'objet d'une analyse spécifique dans l'étude d'impact doivent être déterminés au regard de la nature de l'installation projetée, de son emplacement et de ses incidences prévisibles sur l'environnement.... ...2) En ce qui concerne plus particulièrement les effets sur la qualité de l'air, il y a lieu, pour procéder ainsi qu'il vient d'être dit, alors même que les articles L. 221-1 et suivants du code de l'environnement n'ont pas pour objet de fixer des prescriptions relatives à la demande d'autorisation d'une installation classée pour la protection de l'environnement, de prendre en compte les normes de qualité de l'air qu'elles fixent et, le cas échéant, les mesures prises par le préfet, sur le fondement des mêmes dispositions, dans la zone concernée.... ...3) Commet une erreur de droit la cour qui juge que le défaut, dans l'étude d'impact, d'analyse spécifique relative aux particules PM 2,5 susceptibles d'êtres émises par l'installation projetée avait nui à l'information de la population et, par suite, entaché d'irrégularité la procédure d'adoption de l'arrêté attaqué, sans rechercher si les incidences prévisibles de ces émissions justifiaient une telle analyse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 11 décembre 1996, Association de défense de l'environnement orangeois, du patrimoine naturel, historique et du cadre de vie (A.D.E.O.), n° 173212, p. 485.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
