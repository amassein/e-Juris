<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789051</ID>
<ANCIEN_ID>JG_L_2016_12_000000399900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/12/2016, 399900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vivien David</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:399900.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 19 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande tendant à la modification de l'article R. 422-43 du code de l'environnement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vivien David, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. B...doit être regardé comme demandant au Conseil d'Etat d'annuler le refus opposé par le Premier Ministre à sa demande tendant à ce que soient abrogées les dispositions de l'article R. 422-43 du code de l'environnement en tant qu'elles ne comportent pas de définition de la notion d'étang isolé ni ne précisent que la superficie d'un tel étang doit être appréciée en tenant compte de ses rives non immergées ;<br/>
<br/>
              2. Considérant que l'article L. 422-10 du code de l'environnement dispose : " L'association communale [de chasse agréée] est constituée sur les terrains autres que ceux : 1° Situés dans un rayon de 150 mètres autour de toute habitation / (...) / 3° Ayant fait l'objet de l'opposition des propriétaires ou détenteurs de droits de chasse sur des superficies d'un seul tenant supérieures aux superficies minimales mentionnées à l'article L. 422-13 " ; qu'aux termes de l'article L. 422-13 du même code : " I. - Pour être recevable, l'opposition des propriétaires ou détenteurs de droits de chasse mentionnés au 3° de l'article L. 422-10 doit porter sur des terrains d'un seul tenant et d'une superficie minimum de vingt hectares. / II. - Ce minimum est abaissé pour la chasse au gibier d'eau : / 1° A trois hectares pour les marais non asséchés ; / 2° A un hectare pour les étangs isolés ; / 3° A cinquante ares pour les étangs dans lesquels existaient, au 1er septembre 1963, des installations fixes, huttes et gabions. (...) " ; qu'enfin aux termes de l'article R. 422-43 de ce code : " Pour l'application de l'article L. 422-13, sont considérés comme marais non asséchés les terrains périodiquement inondés sur lesquels se trouve une végétation aquatique. / Tout marais dont la superficie est inférieure au minimum prévu pour sa catégorie de terrain de chasse et qui est attenant à un étang ouvrant droit à opposition, tout étang dont la superficie est inférieure au minimum prévu pour sa catégorie de terrain de chasse et qui est attenant à un marais ouvrant droit à opposition suit le sort de cet étang ou de ce marais. / L'opposition concernant le droit de chasse dans les marais et les étangs n'est valable que pour le gibier d'eau. (...) " ; <br/>
<br/>
              3. Considérant, d'une part, qu'il résulte des dispositions de l'article R. 422-43 du code de l'environnement que doit être regardé comme isolé au sens de l'article L. 422-10 de ce code un étang qui n'est pas attenant à un marais ou à un autre étang ouvrant droit à opposition ; que M. B...n'est, par suite, pas fondé à soutenir que l'article R. 422-43 de ce code serait illégal faute de permettre l'application du 2° du II de l'article L. 422-13 de ce code en ne définissant pas cette catégorie d'étang ;<br/>
<br/>
              4. Considérant, d'autre part, que l'article L. 422-13 du code de l'environnement n'impose pas que les rives non immergées d'un étang soient prises en compte dans le calcul de sa superficie ; que le moyen tiré de ce que l'article R. 422-43 du même code méconnaîtrait cette disposition, dès lors qu'il ne prévoit pas la prise en compte d'une bande de terre autour de la surface en eau de l'étang dans le calcul de sa superficie, ne peut dès lors qu'être écarté ; qu'il ne ressort pas des pièces du dossier qu'en ne prévoyant pas l'inclusion des rives non immergées des étangs isolés pour le calcul de leur superficie, l'article R. 422-43 serait entaché d'erreur manifeste d'appréciation ; <br/>
<br/>
              5. Considérant, enfin, que, si M. B...soutient que le II de l'article L. 422-13 introduirait une rupture d'égalité, non justifiée par un motif d'intérêt général, entre propriétaires d'étangs compris entre 1 hectare et 50 ares selon qu'existaient ou non dans ces étangs des installations fixes, huttes et gabions au 1er septembre 1963, la conformité de cette disposition législative à la Constitution ne saurait être contestée devant le juge administratif en dehors de la procédure prévue à l'article 61-1 de la Constitution ; que ce moyen ne peut, par suite, qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
<br/>
      Article 2 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée au Premier ministre et à la ministre de l'environnement de l'énergie et de la mer, chargée des relations internationales sur le climat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
