<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031360898</ID>
<ANCIEN_ID>JG_L_2015_10_000000382378</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/36/08/CETATEXT000031360898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 23/10/2015, 382378, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382378</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382378.20151023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nîmes de prononcer la décharge de l'obligation de payer la somme de 502 euros procédant de la mise en demeure valant commandement de payer qui lui a été notifiée le 11 septembre 2012 pour avoir paiement d'impôt sur le revenu et de contributions sociales au titre de l'année 2006 et de taxe foncière au titre de l'année 2007 ainsi que des majorations et frais correspondants. Par un jugement n° 1300690 du 10 avril 2014, le tribunal a fait droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 14MA02494 du 16 juillet 2014, enregistrée le 21 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, les conclusions du pourvoi présenté par le ministre des finances et des comptes publics enregistré le 6 juin 2014 au greffe de cette cour contre le jugement du 10 avril 2014 en tant qu'il s'est prononcé sur la demande de M. A...tendant à la décharge de l'obligation de payer procédant de la mise en demeure valant commandement de payer qui lui a été notifiée le 11 septembre 2012 pour avoir paiement de la taxe foncière au titre de l'année 2007 ainsi que des majorations et frais correspondants. <br/>
<br/>
              Par ce pourvoi, et un mémoire enregistré au greffe de la section du contentieux le 10 avril 2015, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement en tant qu'il a, d'une part, déchargé M. A...de l'obligation de payer la taxe foncière correspondant à l'année 2007 ainsi que les majorations et frais correspondants, d'autre part, mis à la charge de l'Etat la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter dans cette mesure la demande de M. A... ;<br/>
<br/>
              3°) d'ordonner le remboursement de la somme versée en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, pour juger qu'aucun acte interruptif de prescription n'était intervenu depuis la mise en recouvrement de la taxe foncière due au titre de l'année 2007 et qu'en conséquence, cette imposition était prescrite à la date de la mise en demeure valant commandement de payer du 11 septembre 2012, le tribunal administratif de Nîmes a dénaturé les pièces du dossier qui lui était soumis en estimant que l'administration fiscale n'avait apporté aucun élément sur l'existence de diligences du comptable public depuis la mise en recouvrement de la taxe foncière, alors qu'elle avait joint à son mémoire du 14 juin 2013 une copie du commandement de payer émis le 17 octobre 2008 relatif, notamment, à cette imposition ainsi que l'avis de réception présenté à M. A...et signé par lui le 23 octobre 2008 et que ces pièces n'étaient pas contestées par ce dernier ;<br/>
<br/>
              2. Considérant qu'il résulte de ce qui précède que le ministre est fondé à demander l'annulation du jugement qu'il attaque, en tant qu'il prononce la décharge de l'obligation de payer la somme de 86 euros procédant de la mise en demeure valant commandement de payer qui a été notifiée à M. A...le 11 septembre 2012 pour avoir paiement de la taxe foncière correspondant à l'année 2007 ainsi que des majorations et frais correspondants ; que ses conclusions tendant à l'annulation de l'article 2 du jugement par lequel l'Etat a été condamné à payer une somme de 100 euros, lesquelles portent à la fois sur la taxe foncière et sur des impositions pour lesquelles le Conseil d'Etat statuant au contentieux n'est pas compétent, doivent être accueillies dans la mesure où elles concernent la taxe foncière à concurrence d'une somme fixée à 50 euros ;<br/>
<br/>
              2. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              3. Considérant qu'ainsi qu'il a été dit précédemment, le moyen tiré de la prescription de l'action en recouvrement doit être écarté ;  que, par suite, la demande de M. A... doit être rejetée dans cette mesure, y compris en ce qui concerne ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative à l'appui de sa contestation de l'obligation de payer la taxe foncière ;  qu'il n'appartient pas au juge d'enjoindre à M. A...de rembourser à l'Etat la somme que celui-ci lui a versée au titre de cet article en application du jugement attaqué ; <br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nîmes du 10 avril 2014 est annulé en tant qu'il prononce la décharge de l'obligation de payer la somme de 86 euros procédant de la mise en demeure valant commandement de payer qui a été notifiée à M. A...le 11 septembre 2012 pour avoir paiement de la taxe foncière correspondant à l'année 2007 ainsi que des majorations et frais correspondants et en tant qu'il se prononce sur l'octroi d'une somme fixée à 50 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative au titre des poursuites portant sur cette imposition.  <br/>
<br/>
Article 2 : La demande de M. A...en tant qu'elle porte sur la taxe foncière due au titre de 2007 est rejetée, y compris ses conclusions fixées à 50 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : Les conclusions en injonction du ministre des finances et des comptes publics sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B...A....<br/>
Copie en sera adressée, pour information, à la cour administrative d'appel de Marseille. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
