<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037096542</ID>
<ANCIEN_ID>JG_L_2018_06_000000420952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/09/65/CETATEXT000037096542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/06/2018, 420952, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:420952.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 25 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le Fonds d'assurance formation du travail temporaire demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision exprimée dans le courrier du 6 février 2018 du directeur de la législation fiscale, relatif à la détermination de la contribution des employeurs à la formation professionnelle continue ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - sa requête est recevable, dès lors que la décision contestée lui fait grief en ce qu'elle constitue, d'une part, une interprétation formelle de dispositions fiscales opposable à l'administration fiscale et, d'autre part, une instruction impérative qui lui est donnée, en sa qualité d'organisme paritaire collecteur agréé (OPCA) de la contribution des employeurs à la formation professionnelle continue pour la branche des entreprises de travail temporaire ;<br/>
              - la condition d'urgence est remplie ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - c'est par une interprétation erronée en droit que le directeur de la législation fiscale a considéré que, pour le calcul des effectifs des entreprises de travail temporaire, il convient de cumuler les deux critères prévus aux articles L. 1251-54 et R. 6331-1 du code du travail.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
Vu :<br/>
- le code du travail ;<br/>
- le code général des impôts ;<br/>
- le code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". La condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'article L. 522-3 du même code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la demande ne présente pas un caractère d'urgence.<br/>
<br/>
              2. Pour caractériser l'urgence qu'il y a à suspendre la décision qu'il estime révélée par le courrier que le directeur de la législation fiscale lui a adressé le 6 février 2018, le Fonds d'assurance formation du travail temporaire, agissant en qualité d'organisme paritaire collecteur agréé (OPCA) de la contribution des employeurs à la formation professionnelle continue pour la branche des entreprises de travail temporaire, soutient que le changement dans l'interprétation des textes applicables exprimé dans cette lettre emporte pour lui des conséquences financières très importantes, dans la mesure où ses ressources résultent exclusivement de la collecte des contributions dont il a la charge. Toutefois, il n'assortit cette allégation d'aucun élément circonstancié et précis et ne fournit aucune pièce ni donnée qui permettrait de la regarder comme établie. Si l'organisme requérant soutient également que l'interprétation contenue dans ce courrier pourrait entraîner des réclamations sur les collectes déjà réalisées par le passé, il n'y a pas lieu pour le juge des référés, lorsqu'il recherche s'il y a urgence à prendre, avant tout jugement au fond, les mesures conservatoires prévues par l'article L. 521-1 du code de justice administrative, de se fonder sur la seule perspective de la multiplication des contestations administratives ou contentieuses qui seraient suscitées par l'illégalité alléguée de la décision contestée.<br/>
<br/>
              3. Il résulte de ce qui précède, et sans qu'il soit besoin de se prononcer sur l'existence d'une décision révélée par le courrier du 6 février 2018 du directeur de la législation fiscale, que la condition d'urgence posée par les dispositions de l'article L. 521-1 du code de justice administrative ne peut, en tout état de cause, être regardée comme satisfaite. Par suite, la requête présentée par le Fonds d'assurance formation du travail temporaire doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du Fonds d'assurance formation du travail temporaire est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Fonds d'assurance formation du travail temporaire.<br/>
Copie en sera adressée au ministre de l'économie et des finances et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
