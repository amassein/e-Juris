<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028398039</ID>
<ANCIEN_ID>JG_L_2013_12_000000367356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/39/80/CETATEXT000028398039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/12/2013, 367356, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:367356.20131223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société SADEF, dont le siège est 34, rue de Reuilly à Paris (75012), représentée par son président directeur général en exercice ; la société SADEF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1575 T/1581 T du 17 janvier 2013 par laquelle la Commission nationale d'aménagement commercial a rejeté sa requête tendant à l'annulation de la décision du 22 août 2012 de la commission départementale d'aménagement commercial du Finistère autorisant la société L'Immobilière Leroy Merlin France et la Société Leroy Merlin France à procéder à la création d'un magasin de bricolage et d'aménagement de la maison à l'enseigne Leroy Merlin, d'une surface de vente de 13 100 m², au sein de la ZAC de Kervouyec à Quimper (Finistère) ; <br/>
<br/>
              2°) de mettre à la charge des sociétés précitées le versement de la somme de 6 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la régularité de la procédure devant la Commission nationale d'aménagement commercial :<br/>
<br/>
              1. Considérant qu'il ne résulte d'aucune disposition législative ou réglementaire ni d'aucun principe, que les décisions de la Commission nationale d'aménagement commercial doivent comporter des mentions attestant de la convocation régulière de ses membres ou de l'envoi dans les délais de l'ordre du jour et des documents nécessaires à ses délibérations ; qu'ainsi, le moyen tiré de l'irrégularité de la décision attaquée au regard des termes de l'article R. 752-49 du code de commerce ne peut qu'être écarté ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, sur le fondement des dispositions de l'article R. 752-51 du code de commerce, la Commission nationale d'aménagement commercial a sollicité l'avis des ministres intéressés ; que, contrairement à ce qui est soutenu par la société requérante, il ne résulte d'aucune disposition législative ou réglementaire, ni d'aucun principe, que la Commission nationale d'aménagement commercial soit tenue de justifier que ces avis ont été signés par des personnes habilitées lorsque l'arrêté de délégation a été régulièrement publié ; qu'en l'espèce, il ressort des pièces du dossier que les avis de ces ministres, ont bien été signés par des personnes dûment habilitées à cet effet, ; que, dès lors, la société SADEF n'est pas fondée à soutenir que la décision attaquée a été adoptée au terme d'une procédure irrégulière ; <br/>
<br/>
              En ce qui concerne la maîtrise foncière du projet : <br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 752-6 du code de commerce : " La demande d'autorisation prévue à l'article L. 752-1 (...) est présentée soit par le propriétaire de l'immeuble, soit par une personne justifiant d'un titre l'habilitant à construire sur le terrain ou à exploiter commercialement l'immeuble " ; <br/>
<br/>
              4. Considérant que si la société requérante soutient que le pétitionnaire ne justifiait pas de la maîtrise foncière du terrain d'assiette du projet, il ressort des pièces du dossier, en particulier de l'acte notarial du 9 janvier 2012, qu'une promesse de vente synallagmatique, sous conditions suspensives, a été signée entre la communauté de communes " Quimper communauté " et la société pétitionnaire dont la validité expirait le 28 juillet 2013 ; que le président de cette communauté était régulièrement autorisé à signer cet acte ; que, dès lors, le moyen tiré d'une absence de maîtrise foncière de l'ensemble du terrain d'assiette du projet par la société pétitionnaire doit être écarté ;<br/>
<br/>
              En ce qui concerne l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              5. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ; <br/>
<br/>
              6. Considérant que si la société requérante soutient que la décision attaquée a méconnu l'objectif fixé par le législateur en matière d'aménagement du territoire du fait de son impact sur l'animation de la vie urbaine et rurale et sur la sécurité des flux de circulation, il ressort des pièces du dossier, d'une part, que le projet contesté permet de compléter l'offre commerciale dans la zone nord de Quimper et de diminuer ainsi l'évasion des clients de la zone de chalandise vers les autres pôles commerciaux de la région ; que, d'autre part, il ressort des pièces du dossier que le projet est situé à proximité du giratoire reliant la RD 100 et la RD 39 dont une des branches accède directement au futur magasin, qu'ainsi, la commission nationale a pu estimer, sans erreur d'appréciation que ces aménagements permettent d'absorber, de manière suffisamment sécurisée, les flux de transports additionnels ;<br/>
<br/>
              7. Considérant que si la société requérante soutient que la commission nationale aurait apprécié de façon erronée les effets du projet sur le développement durable en raison de l'absence d'insertion du projet dans un réseau de transport doux ou collectif, il ressort des pièces du dossier que le site comporte un accès piétonnier et n'est pas dépourvu d'accès par transports collectifs ; que si la société requérante soutient que le projet aura un impact négatif sur la protection des espèces naturelles faunistiques et floristiques, tel le lézard vert, protégé au niveau national, et d'autres espèces d'oiseaux protégés, il ressort en particulier de l'étude d'impact versée au dossier que le projet, qui est enclavé entre une zone urbaine et un échangeur routier, n'aura qu'un impact limité sur les espèces protégées ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la Commission nationale d'aménagement commercial n'a pas fait, par la décision attaquée, une inexacte application des dispositions rappelées ; qu'ainsi, la société SADEF n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société L'immobilière Leroy Merlin France et de la société Leroy Merlin France, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'en revanche, il y a lieu de mettre à la charge de la société SADEF le versement de la somme de 2 500 euros à chacune des sociétés L'immobilière Leroy Merlin France et Leroy Merlin France au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête présentée par la société SADEF est rejetée. <br/>
Article 2 : La société SADEF versera la somme de 2 500 euros à chacune des sociétés L'immobilière Leroy Merlin France et Leroy Merlin France au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société SADEF, à la société L'immobilière Leroy Merlin France, à la société Leroy Merlin France et à la Commission nationale d'aménagement commercial.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
