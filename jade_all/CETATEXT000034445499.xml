<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034445499</ID>
<ANCIEN_ID>JG_L_2017_04_000000396174</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/44/54/CETATEXT000034445499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/04/2017, 396174</TITRE>
<DATE_DEC>2017-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396174</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396174.20170419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Angles et Fils a demandé au tribunal administratif de Montpellier la condamnation du département de l'Hérault à lui verser une somme de 87 220, 18 euros en paiement des prestations réalisées en qualité de sous-traitante des travaux d'extension du centre d'exploitation du Caylar et de construction d'un bâtiment pour forestiers sapeurs. Par un jugement n° 1204573 du 23 mai 2014, le tribunal administratif de Montpellier a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14MA03107 du 16 novembre 2015, la cour administrative d'appel de Marseille, sur l'appel de la société Angles et Fils, a annulé ce jugement, condamné le département de l'Hérault à verser à cette société une somme de 62 220,18 euros assortie des intérêts au taux légal à compter du 1er septembre 2009, et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 janvier 2016, 18 avril 2016 et 23 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le département de l'Hérault demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a fait droit partiellement aux conclusions d'appel de la société Angles et Fils ; <br/>
<br/>
              2°) de rejeter le pourvoi incident formé par la société Angles et Fils ;<br/>
<br/>
              3°) réglant l'affaire au fond, de rejeter l'appel de la société Angles et Fils ;<br/>
<br/>
              4°) de mettre à la charge de la société Angles et Fils la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics en vigueur avant le 1er avril 2016 ;<br/>
              - la loi n° 75-1334 du 31 décembre 1975 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département de l'Hérault, et à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Angles et fils.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 avril 2017, présentée par la société Angles et Fils.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'un marché de travaux publics, notifié le 15 mai 2008, portant sur l'extension du centre d'exploitation du Caylar et la construction d'un bâtiment pour forestiers sapeurs a été conclu par le département de l'Hérault avec la société Kairos ; que le département de l'Hérault a accepté l'intervention de la société Angles et Fils en qualité de sous-traitante de la société Kairos et agréé ses conditions de paiement, selon un acte spécial de sous-traitance du 7 mai 2008, notifié le 16 mai 2008 ; qu'en raison de la liquidation judiciaire de la société Kairos, le marché de travaux publics conclu par le département de l'Hérault avec cette entreprise a fait l'objet d'une résiliation ; qu'à la suite de cette décision de résiliation, la société Angles et Fils a demandé au tribunal administratif de Montpellier de condamner le département de l'Hérault à lui verser la somme de 87 220, 18 euros augmentée des intérêts légaux au titre du paiement direct, outre une somme de 10 000 euros à titre de dommages et intérêts ; que, par jugement du 23 mai 2014, le tribunal administratif de Montpellier a rejeté la demande de la société Angles et Fils ; que, saisie d'une requête tendant à l'annulation dudit jugement, la cour administrative d'appel de Marseille a, par un arrêt du 16 novembre 2015, annulé ce jugement, condamné le département de l'Hérault à verser à la société Angles et Fils une somme de 62 220,18 euros, assortie des intérêts au taux légal à compter du 1er septembre 2009, et rejeté le surplus des conclusions présentées par la société Angles et Fils ; que le département de l'Hérault se pourvoit en cassation contre cet arrêt ; que, par la voie du pourvoi incident, la société Angles et Fils conclut à l'annulation de cet arrêt en tant qu'il a rejeté le surplus de ses conclusions ;<br/>
<br/>
              Sur le pourvoi principal du département de l'Hérault :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la loi du 31 décembre 1975 relative à la sous-traitance : " L'entrepreneur principal dispose d'un délai de quinze jours, comptés à partir de la réception des pièces justificatives servant de base au paiement direct, pour les revêtir de son acceptation ou pour signifier au sous-traitant son refus motivé d'acceptation. / Passé ce délai, l'entrepreneur principal est réputé avoir accepté celles des pièces justificatives ou des parties de pièces justificatives qu'il n'a pas expressément acceptées ou refusées. / Les notifications prévues à l'alinéa 1er sont adressées par lettre recommandée avec accusé de réception " ; qu'aux termes de l'article 116 du code des marchés publics en vigueur à la date du litige, repris à l'exception de son avant-dernier alinéa au I de l'article 136 du décret du 25 mars 2016 relatif aux marchés publics : " Le sous-traitant adresse sa demande de paiement libellée au nom du pouvoir adjudicateur au titulaire du marché, sous pli recommandé avec accusé de réception, ou la dépose auprès du titulaire contre récépissé. / Le titulaire dispose d'un délai de quinze jours à compter de la signature de l'accusé de réception ou du récépissé pour donner son accord ou notifier un refus, d'une part, au sous-traitant et, d'autre part, au pouvoir adjudicateur ou à la personne désignée par lui dans le marché. / Le sous-traitant adresse également sa demande de paiement au pouvoir adjudicateur ou à la personne désignée dans le marché par le pouvoir adjudicateur, accompagnée des factures et de l'accusé de réception ou du récépissé attestant que le titulaire a bien reçu la demande ou de l'avis postal attestant que le pli a été refusé ou n'a pas été réclamé. / Le pouvoir adjudicateur ou la personne désignée par lui dans le marché adresse sans délai au titulaire une copie des factures produites par le sous-traitant. / Le pouvoir adjudicateur procède au paiement du sous-traitant dans le délai prévu par l'article 98. Ce délai court à compter de la réception par le pouvoir adjudicateur de l'accord, total ou partiel, du titulaire sur le paiement demandé, ou de l'expiration du délai mentionné au deuxième alinéa si, pendant ce délai, le titulaire n'a notifié aucun accord ni aucun refus, ou encore de la réception par le pouvoir adjudicateur de l'avis postal mentionné au troisième alinéa. / Le pouvoir adjudicateur informe le titulaire des paiements qu'il effectue au sous-traitant " ; <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions que, pour obtenir le paiement direct par le maître d'ouvrage de tout ou partie des prestations qu'il a exécutées dans le cadre de son contrat de sous-traitance, le sous-traitant régulièrement agréé doit adresser sa demande de paiement direct à l'entrepreneur principal, titulaire du marché ; qu'il appartient ensuite au titulaire du marché de donner son accord à la demande de paiement direct ou de signifier son refus dans un délai de quinze jours à compter de la réception de cette demande ; que le titulaire du marché est réputé avoir accepté cette demande s'il garde le silence pendant plus de quinze jours à compter de sa réception ; qu'à l'issue de cette procédure, le maître d'ouvrage procède au paiement direct du sous-traitant régulièrement agréé si le titulaire du marché a donné son accord ou s'il est réputé avoir accepté la demande de paiement direct ; que cette procédure a pour objet de permettre au titulaire du marché d'exercer un contrôle sur les pièces transmises par le sous-traitant et de s'opposer, le cas échéant, au paiement direct ; que sa méconnaissance par le sous-traitant fait ainsi obstacle à ce qu'il puisse se prévaloir, auprès du maître d'ouvrage, d'un droit à ce paiement ;<br/>
<br/>
              4. Considérant que la cour administrative d'appel de Marseille, ainsi qu'il ressort des énonciations de l'arrêt attaqué, a estimé que le sous-traitant régulièrement agréé, quand bien même il n'aurait pas respecté cette procédure, ne saurait pour autant être définitivement privé du bénéfice du paiement direct que dans la seule hypothèse où le maître d'ouvrage, faute d'avoir été saisi par le sous-traitant en temps utile d'une demande de paiement, aurait été amené à payer les prestations en cause à l'entreprise principale ; qu'en statuant ainsi, alors que, comme il a été dit au point 3, le bénéfice du paiement direct est subordonné au  respect de la procédure prévue par les dispositions de l'article 8 de la loi du 31 décembre 1975 et de l'article 116 du code des marchés publics et que, faute d'avoir respecté une telle procédure, un sous-traitant ne peut utilement se prévaloir d'un droit au paiement direct, la cour a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le département de l'Hérault est fondé à demander l'annulation de l'arrêt attaqué en tant qu'il fait droit partiellement aux conclusions d'appel de la société Angles et Fils ; <br/>
<br/>
              Sur le pourvoi incident de la société Angles et Fils :<br/>
<br/>
              5. Considérant que, pour rejeter les conclusions de la société Angles et Fils tendant au versement du solde des situations de travaux n°s 1, 2 et 3, la cour a relevé que son montant avait été versé au titulaire du marché par le département avant que celui-ci ne soit saisi d'une demande de paiement direct par le sous-traitant ; que cette appréciation n'est, en tout état de cause, entachée, contrairement à ce qui est soutenu, ni de dénaturation, ni d'insuffisance de motivation ; que, par suite, le pourvoi incident de la société Angles et Fils doit être rejeté ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par la société Angles et Fils soit mise à ce titre à la charge du département de l'Hérault qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Angles et Fils le versement au département de l'Hérault d'une somme de 3000 euros au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1 à 3 et 5 de l'arrêt de la cour administrative d'appel de Marseille du 16 novembre 2015 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, devant la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions du pourvoi incident de la société Angles et Fils ainsi que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La société Angles et Fils versera une somme de 3 000 euros au département de l'Hérault en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée au département de l'Hérault et à la société Angles et Fils.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-03-01-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. CONDITIONS D'EXÉCUTION DES ENGAGEMENTS CONTRACTUELS EN L'ABSENCE D'ALÉAS. MARCHÉS. SOUS-TRAITANCE. - DROIT AU PAIEMENT DIRECT - OBLIGATION POUR LE SOUS-TRAITANT D'ADRESSER SA DEMANDE DE PAIEMENT DIRECT À L'ENTREPRENEUR PRINCIPAL - MÉCONNAISSANCE - CONSÉQUENCE - IMPOSSIBILITÉ DE SE PRÉVALOIR D'UN DROIT AU PAIEMENT DIRECT AUPRÈS DU MAÎTRE D'OUVRAGE.
</SCT>
<ANA ID="9A"> 39-03-01-02-03 Il résulte de la combinaison de l'article 8 de la loi n° 75-1334 du 31 décembre 1975 relative à la sous-traitance et de l'article 116 du code des marchés publics, aujourd'hui repris, à l'exception de son avant-dernier alinéa, au I de l'article 136 du décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics, que, pour obtenir le paiement direct par le maître d'ouvrage de tout ou partie des prestations qu'il a exécutées dans le cadre de son contrat de sous-traitance, le sous-traitant régulièrement agréé doit adresser sa demande de paiement direct à l'entrepreneur principal, titulaire du marché. Il appartient ensuite au titulaire du marché de donner son accord à la demande de paiement direct ou de signifier son refus dans un délai de quinze jours à compter de la réception de cette demande. Le titulaire du marché est réputé avoir accepté cette demande s'il garde le silence pendant plus de quinze jours à compter de sa réception. A l'issue de cette procédure, le maître d'ouvrage procède au paiement direct du sous-traitant régulièrement agréé si le titulaire du marché a donné son accord ou s'il est réputé avoir accepté la demande de paiement direct.... ,,Cette procédure a pour objet de permettre au titulaire du marché d'exercer un contrôle sur les pièces transmises par le sous-traitant et de s'opposer, le cas échéant, au paiement direct. Sa méconnaissance par le sous-traitant fait ainsi obstacle à ce qu'il puisse se prévaloir, auprès du maître d'ouvrage, d'un droit à ce paiement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
